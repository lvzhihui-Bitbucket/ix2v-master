#!/bin/sh

# 固件目录
updateFolder1="/mnt/sdcard/vtkDownload/"
updateFolder2="/mnt/nand1-2/vtkDownload/"

# 等待指令执行完毕函数
WaitingForExecution()
{
	if [ $? -eq 0 ]; then
		echo "$1 succeed"
	else
		echo "$1 failed"
	fi
}

# 文件删除
FileDelete()
{
	chmod 777 -R $1
	rm -rf $1
	WaitingForExecution "rm -rf $1"
}

# 自动更新固件函数，带一个参数固件包所在目录
FirmwareUpdateFun()
{
	updateShFile="upgrade.sh"
	updateFolder=$1;

	# 搜索目录下所有zip的固件包
	for updatefile in `ls $updateFolder`
	do 
	{
		# 如果是文件
		if [ -f $updateFolder$updatefile ]; then
		{
			zipUpdateSh=$(unzip -l $updateFolder$updatefile | grep -w "$updateShFile" | awk -F ' ' '{print $NF}')
			updateSh=$(echo $zipUpdateSh | awk -F '/' '{print $NF}')

			# 如果zip文件中存在更新脚本文件
			if [ $updateSh = $updateShFile ]; then
			{
				echo "update $updateFolder$updatefile start-------------------------------------------"

				zipUpdateShFolder=$(echo $zipUpdateSh | sed "s/$updateSh//g")
				
				updateShFolder=$updateFolder$zipUpdateShFolder

				# 解压出固件包中的脚本文件
				unzip -oq $updateFolder$updatefile $zipUpdateShFolder$updateSh -d $updateFolder
				if [ $? -eq 0 ]; then
				{
					zipRootDirectory=$(echo $zipUpdateShFolder | awk -F '/' '{print $1}')

					chmod 777 -R $updateFolder$zipRootDirectory

					newUpdateShFile="$updatefile.sh"

					# 把脚本文件移动到固件包所在目录下
					mv $updateShFolder$updateSh $updateFolder$newUpdateShFile
					WaitingForExecution  "mv $updateShFolder$updateSh $updateFolder$newUpdateShFile"
			
					# 删除解压的临时文件夹
					FileDelete $updateFolder$zipRootDirectory
                
			        # 执行解压出来的脚本文件
                    $updateFolder$newUpdateShFile

					# 删除脚本文件
					FileDelete $updateFolder$newUpdateShFile
				}
				else
				{
					echo "$updateFolder$updatefile Error----------------------------------------------"
					
					zipRootDirectory=$(echo $zipUpdateShFolder | awk -F '/' '{print $1}')

					# 删除解压的临时文件夹
					FileDelete $updateFolder$zipRootDirectory

					# 删除错误压缩包
					FileDelete $updateFolder$updatefile
				}
				fi
                				
			}
			fi
		}
		fi
	}
	done
}

# IAP固件更新
if [ -b "/dev/mmcblk0" ] && [ -d $updateFolder1 ]; then 
	FirmwareUpdateFun $updateFolder1
elif [ -d $updateFolder2 ]; then 
	FirmwareUpdateFun $updateFolder2
fi

BUZZER_KO="/usr/modules/buzzerdrv.ko"

WIFI_KO1="/usr/wifi/cfg80211.ko"
WIFI_KO2="/usr/wifi/mac80211.ko"
WIFI_KO3="/usr/wifi/rtl8189ftv.ko"

dhcpClientPath="/mnt/nand1-1/App/"
wifiPath="/usr/wifi/"

myApp="/mnt/nand1-1/App/IX47"

assist_process="/mnt/nand1-1/App/assist_process"
autoip_assist="/mnt/nand1-1/App/autoip_assist"
ota_client="/mnt/nand1-1/App/ota_client"
route_sh="/mnt/nand1-1/App/route.sh"
videodoor="/mnt/nand1-1/App/videodoor"

#insmod $BUZZER_KO
insmod $WIFI_KO1
insmod $WIFI_KO2
insmod $WIFI_KO3

$assist_process $wifiPath &
$autoip_assist $dhcpClientPath &
$ota_client &

sleep 1

$route_sh &

ifconfig wlan0 192.168.233.222 up

sleep 1
source /mnt/wlan-chk.sh

sleep 1
$myApp &

sleep 10
$videodoor -C &
