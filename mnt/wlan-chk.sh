#!/bin/sh

function reset_wlan()
{
	rmmod $WIFI_KO
	insmod $WIFI_KO
	ifconfig wlan0 192.168.233.222 up
}

WIFI_KO="/usr/wifi/rtl8189ftv.ko"

check_result=$(ifconfig |grep wlan0)
if [ -z "$check_result" ];then
	reset_wlan
	echo wlan0 reset once!
else
	echo wlan0 just ok!
fi
 
