#!/bin/sh

if [ $# -eq 0 -o $# -gt 1 ]
then
	echo "You should input appname to be killed and NFS SERVER IP!"
	exit 1
else
	NAME=$1
fi
ALLID=$(ps|grep $NAME|grep -v grep|awk '{print $1}')
echo $ALLID
KILLID=$(echo $ALLID|awk '{print $1}')
echo $KILLID
kill $KILLID	
sleep 1
cd /mnt/nand1-1/App
./$NAME &
#for KILLID in $ALLID
#do
#	echo $KILLID
#	kill $KILLID	
#done
exit 1

