
#SET(CROSS_COMPILE 0)
SET(CROSS_COMPILE 1)

IF(CROSS_COMPILE) 
  
SET(CMAKE_SYSTEM_NAME Linux)
SET(CMAKE_SYSTEM_PROCESSOR arm)
SET(TOOLCHAIN_DIR "/opt/arm-anykav500-linux-uclibcgnueabi")
set(CMAKE_CXX_COMPILER ${TOOLCHAIN_DIR}/bin/arm-linux-g++)
set(CMAKE_C_COMPILER   ${TOOLCHAIN_DIR}/bin/arm-linux-gcc)
set(GNU_FLAGS "-fPIC -D_GNU_SOURCE -std=c99 -mlittle-endian -fno-builtin -nostdlib")
set(CMAKE_CXX_FLAGS "${GNU_FLAGS} ")
set(CMAKE_C_FLAGS "${GNU_FLAGS}  ")

SET(CMAKE_FIND_ROOT_PATH  ${TOOLCHAIN_DIR} ${TOOLCHAIN_DIR}/include ${TOOLCHAIN_DIR}/lib )


SET(ROOT "/opt/arm-anykav500-linux-uclibcgnueabi/usr")
SET(SYSINC $(ROOT)/arm-anykav500-linux-uclibcgnueabi/sysroot/usr/include)
SET(SYSLIB0 $(ROOT)/arm-anykav500-linux-uclibcgnueabi/sysroot/usr/lib)
SET(SYSLIB1 $(ROOT)/arm-anykav500-linux-uclibcgnueabi/sysroot/lib)
SET(GCCLIB  $(ROOT)/lib/gcc/arm-anykav500-linux-uclibcgnueabi/4.9.4)

include_directories(${SYSINC})
link_directories(${SYSLIB})
link_directories(${SYSLIB0})
link_directories(${SYSLIB1})
  
ENDIF(CROSS_COMPILE)
