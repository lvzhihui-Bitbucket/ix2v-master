#ifndef LV_SYMBOL_DEF_H
#define LV_SYMBOL_DEF_H

#ifdef __cplusplus
extern "C" {
#endif

#include "../lv_conf_internal.h"

/*-------------------------------
 * Symbols from "normal" font
 *-----------------------------*/
#if !defined LV_SYMBOL_BULLET
#define LV_SYMBOL_BULLET          "\xE2\x80\xA2" /*20042, 0x2022*/
#endif

/*-------------------------------
 * Symbols from FontAwesome font
 *-----------------------------*/

/*In the font converter use this list as range:
      61441, 61448, 61451, 61452, 61453, 61457, 61459, 61461, 61465, 61468,
      61473, 61478, 61479, 61480, 61502, 61507, 61512, 61515, 61516, 61517,
      61521, 61522, 61523, 61524, 61543, 61544, 61550, 61552, 61553, 61556,
      61559, 61560, 61561, 61563, 61587, 61589, 61636, 61637, 61639, 61641,
      61664, 61671, 61674, 61683, 61724, 61732, 61787, 61931, 62016, 62017,
      62018, 62019, 62020, 62087, 62099, 62189, 62212, 62810, 63426, 63650
*/

/* These symbols can be prefined in the lv_conf.h file.
 * If they are not predefined, they will use the following values
 */


#if !defined LV_SYMBOL_AUDIO
#define LV_SYMBOL_AUDIO           "\xEF\x80\x81" /*61441, 0xF001*/
#endif

#if !defined LV_SYMBOL_VIDEO
#define LV_SYMBOL_VIDEO           "\xEF\x80\x88" /*61448, 0xF008*/
#endif

#if !defined LV_SYMBOL_LIST
#define LV_SYMBOL_LIST            "\xEF\x80\x8B" /*61451, 0xF00B*/
#endif

#if !defined LV_SYMBOL_OK
#define LV_SYMBOL_OK              "\xEF\x80\x8C" /*61452, 0xF00C*/
#endif

#if !defined LV_SYMBOL_CLOSE
#define LV_SYMBOL_CLOSE           "\xEF\x80\x8D" /*61453, 0xF00D*/
#endif

#if !defined LV_SYMBOL_POWER
#define LV_SYMBOL_POWER           "\xEF\x80\x91" /*61457, 0xF011*/
#endif

#if !defined LV_SYMBOL_SETTINGS
#define LV_SYMBOL_SETTINGS        "\xEF\x80\x93" /*61459, 0xF013*/
#endif

#if !defined LV_SYMBOL_HOME
#define LV_SYMBOL_HOME            "\xEF\x80\x95" /*61461, 0xF015*/
#endif

#if !defined LV_SYMBOL_DOWNLOAD
#define LV_SYMBOL_DOWNLOAD        "\xEF\x80\x99" /*61465, 0xF019*/
#endif

#if !defined LV_SYMBOL_DRIVE
#define LV_SYMBOL_DRIVE           "\xEF\x80\x9C" /*61468, 0xF01C*/
#endif

#if !defined LV_SYMBOL_REFRESH
#define LV_SYMBOL_REFRESH         "\xEF\x80\xA1" /*61473, 0xF021*/
#endif

#if !defined LV_SYMBOL_MUTE
#define LV_SYMBOL_MUTE            "\xEF\x80\xA6" /*61478, 0xF026*/
#endif

#if !defined LV_SYMBOL_VOLUME_MID
#define LV_SYMBOL_VOLUME_MID      "\xEF\x80\xA7" /*61479, 0xF027*/
#endif

#if !defined LV_SYMBOL_VOLUME_MAX
#define LV_SYMBOL_VOLUME_MAX      "\xEF\x80\xA8" /*61480, 0xF028*/
#endif

#if !defined LV_SYMBOL_IMAGE
#define LV_SYMBOL_IMAGE           "\xEF\x80\xBE" /*61502, 0xF03E*/
#endif

#if !defined LV_SYMBOL_TINT
#define LV_SYMBOL_TINT            "\xEF\x81\x83" /*61507, 0xF043*/
#endif

#if !defined LV_SYMBOL_PREV
#define LV_SYMBOL_PREV            "\xEF\x81\x88" /*61512, 0xF048*/
#endif

#if !defined LV_SYMBOL_PLAY
#define LV_SYMBOL_PLAY            "\xEF\x81\x8B" /*61515, 0xF04B*/
#endif

#if !defined LV_SYMBOL_PAUSE
#define LV_SYMBOL_PAUSE           "\xEF\x81\x8C" /*61516, 0xF04C*/
#endif

#if !defined LV_SYMBOL_STOP
#define LV_SYMBOL_STOP            "\xEF\x81\x8D" /*61517, 0xF04D*/
#endif

#if !defined LV_SYMBOL_NEXT
#define LV_SYMBOL_NEXT            "\xEF\x81\x91" /*61521, 0xF051*/
#endif

#if !defined LV_SYMBOL_EJECT
#define LV_SYMBOL_EJECT           "\xEF\x81\x92" /*61522, 0xF052*/
#endif

#if !defined LV_SYMBOL_LEFT
#define LV_SYMBOL_LEFT            "\xEF\x81\x93" /*61523, 0xF053*/
#endif

#if !defined LV_SYMBOL_RIGHT
#define LV_SYMBOL_RIGHT           "\xEF\x81\x94" /*61524, 0xF054*/
#endif

#if !defined LV_SYMBOL_PLUS
#define LV_SYMBOL_PLUS            "\xEF\x81\xA7" /*61543, 0xF067*/
#endif

#if !defined LV_SYMBOL_MINUS
#define LV_SYMBOL_MINUS           "\xEF\x81\xA8" /*61544, 0xF068*/
#endif

#if !defined LV_SYMBOL_EYE_OPEN
#define LV_SYMBOL_EYE_OPEN        "\xEF\x81\xAE" /*61550, 0xF06E*/
#endif

#if !defined LV_SYMBOL_EYE_CLOSE
#define LV_SYMBOL_EYE_CLOSE       "\xEF\x81\xB0" /*61552, 0xF070*/
#endif

#if !defined LV_SYMBOL_WARNING
#define LV_SYMBOL_WARNING         "\xEF\x81\xB1" /*61553, 0xF071*/
#endif

#if !defined LV_SYMBOL_SHUFFLE
#define LV_SYMBOL_SHUFFLE         "\xEF\x81\xB4" /*61556, 0xF074*/
#endif

#if !defined LV_SYMBOL_UP
#define LV_SYMBOL_UP              "\xEF\x81\xB7" /*61559, 0xF077*/
#endif

#if !defined LV_SYMBOL_DOWN
#define LV_SYMBOL_DOWN            "\xEF\x81\xB8" /*61560, 0xF078*/
#endif

#if !defined LV_SYMBOL_LOOP
#define LV_SYMBOL_LOOP            "\xEF\x81\xB9" /*61561, 0xF079*/
#endif

#if !defined LV_SYMBOL_DIRECTORY
#define LV_SYMBOL_DIRECTORY       "\xEF\x81\xBB" /*61563, 0xF07B*/
#endif

#if !defined LV_SYMBOL_UPLOAD
#define LV_SYMBOL_UPLOAD          "\xEF\x82\x93" /*61587, 0xF093*/
#endif

#if !defined LV_SYMBOL_CALL
#define LV_SYMBOL_CALL            "\xEF\x82\x95" /*61589, 0xF095*/
#endif

#if !defined LV_SYMBOL_CUT
#define LV_SYMBOL_CUT             "\xEF\x83\x84" /*61636, 0xF0C4*/
#endif

#if !defined LV_SYMBOL_COPY
#define LV_SYMBOL_COPY            "\xEF\x83\x85" /*61637, 0xF0C5*/
#endif

#if !defined LV_SYMBOL_SAVE
#define LV_SYMBOL_SAVE            "\xEF\x83\x87" /*61639, 0xF0C7*/
#endif

#if !defined LV_SYMBOL_BARS
#define LV_SYMBOL_BARS            "\xEF\x83\x89" /*61641, 0xF0C9*/
#endif

#if !defined LV_SYMBOL_ENVELOPE
#define LV_SYMBOL_ENVELOPE        "\xEF\x83\xA0" /*61664, 0xF0E0*/
#endif

#if !defined LV_SYMBOL_CHARGE
#define LV_SYMBOL_CHARGE          "\xEF\x83\xA7" /*61671, 0xF0E7*/
#endif

#if !defined LV_SYMBOL_PASTE
#define LV_SYMBOL_PASTE           "\xEF\x83\xAA" /*61674, 0xF0EA*/
#endif

#if !defined LV_SYMBOL_BELL
#define LV_SYMBOL_BELL            "\xEF\x83\xB3" /*61683, 0xF0F3*/
#endif

#if !defined LV_SYMBOL_KEYBOARD
#define LV_SYMBOL_KEYBOARD        "\xEF\x84\x9C" /*61724, 0xF11C*/
#endif

#if !defined LV_SYMBOL_GPS
#define LV_SYMBOL_GPS             "\xEF\x84\xA4" /*61732, 0xF124*/
#endif

#if !defined LV_SYMBOL_FILE
#define LV_SYMBOL_FILE            "\xEF\x85\x9B" /*61787, 0xF158*/
#endif

#if !defined LV_SYMBOL_WIFI
#define LV_SYMBOL_WIFI            "\xEF\x87\xAB" /*61931, 0xF1EB*/
#endif

#if !defined LV_SYMBOL_BATTERY_FULL
#define LV_SYMBOL_BATTERY_FULL    "\xEF\x89\x80" /*62016, 0xF240*/
#endif

#if !defined LV_SYMBOL_BATTERY_3
#define LV_SYMBOL_BATTERY_3       "\xEF\x89\x81" /*62017, 0xF241*/
#endif

#if !defined LV_SYMBOL_BATTERY_2
#define LV_SYMBOL_BATTERY_2       "\xEF\x89\x82" /*62018, 0xF242*/
#endif

#if !defined LV_SYMBOL_BATTERY_1
#define LV_SYMBOL_BATTERY_1       "\xEF\x89\x83" /*62019, 0xF243*/
#endif

#if !defined LV_SYMBOL_BATTERY_EMPTY
#define LV_SYMBOL_BATTERY_EMPTY   "\xEF\x89\x84" /*62020, 0xF244*/
#endif

#if !defined LV_SYMBOL_USB
#define LV_SYMBOL_USB             "\xEF\x8a\x87" /*62087, 0xF287*/
#endif

#if !defined LV_SYMBOL_BLUETOOTH
#define LV_SYMBOL_BLUETOOTH       "\xEF\x8a\x93" /*62099, 0xF293*/
#endif

#if !defined LV_SYMBOL_TRASH
#define LV_SYMBOL_TRASH           "\xEF\x8B\xAD" /*62189, 0xF2ED*/
#endif

#if !defined LV_SYMBOL_EDIT
#define LV_SYMBOL_EDIT            "\xEF\x8C\x84" /*62212, 0xF304*/
#endif

#if !defined LV_SYMBOL_BACKSPACE
#define LV_SYMBOL_BACKSPACE       "\xEF\x95\x9A" /*62810, 0xF55A*/
#endif

#if !defined LV_SYMBOL_SD_CARD
#define LV_SYMBOL_SD_CARD         "\xEF\x9F\x82" /*63426, 0xF7C2*/
#endif

#if !defined LV_SYMBOL_NEW_LINE
#define LV_SYMBOL_NEW_LINE        "\xEF\xA2\xA2" /*63650, 0xF8A2*/
#endif

#if !defined LV_SYMBOL_DUMMY
/** Invalid symbol at (U+F8FF). If written before a string then `lv_img` will show it as a label*/
#define LV_SYMBOL_DUMMY           "\xEF\xA3\xBF"
#endif

/*VTK SYMBOL*/
//update time 2024-07-24
//INTERCOM
#define LV_VTK_INTERCOM1          "\xEE\x9A\xA9" 
#define LV_VTK_INTERCOM2          "\xEE\x99\xBA"
#define LV_VTK_INTERCOM3          "\xEE\x99\x94"
#define LV_VTK_INTERCOM4          "\xEE\x99\x84"
//CALL RECORD
#define LV_VTK_MISSED             "\xEE\x98\x94"
#define LV_VTK_INCOMING           "\xEE\x98\x9A"
#define LV_VTK_OUTGOING           "\xEE\x98\xA0"
#define LV_VTK_PLAYBACK           "\xEE\x9A\x82"
#define LV_VTK_PLAYBACK_VIDEO     "\xEE\x9A\x8D"
#define LV_VTK_PLAYBACK_PIC       "\xEE\x9A\x90"
#define LV_VTK_DELETE             "\xEE\xA2\xB6"
//MONITOR
#define LV_VTK_MON                "\xEE\x99\xB6"
#define LV_VTK_MON_OFF            "\xEE\x99\x92"
#define LV_VTK_QUAD               "\xEE\x99\xA1"
#define LV_VTK_FAVORITE1          "\xEE\x9A\x92"
#define LV_VTK_NAME               "\xEE\x9A\x9B"
#define LV_VTK_EDIT1              "\xEE\x9A\x8B"
#define LV_VTK_EDIT2              "\xEE\x99\xB7"
//CALL SCENE
#define LV_VTK_CALL_SCENE         "\xEE\x9A\x93"
#define LV_VTK_NORMAL_USE         "\xEE\x98\x96"
#define LV_VTK_NO_DISTURB         "\xEE\x9A\x85"
//SETTINGS
#define LV_VTK_SETTING            "\xEE\x9A\xBE"
#define LV_VTK_CALL_TUNE          "\xEE\x99\xA9"
#define LV_VTK_GENERAL            "\xEE\x9A\x91"
#define LV_VTK_INSTALLER          "\xEE\x9A\x81"
#define LV_VTK_MANAGER            "\xEE\x9A\xA7"
#define LV_VTK_EXIT_UNIT          "\xEE\x98\xB1"
#define LV_VTK_CLOSE              "\xEE\x9A\xA2"
#define LV_VTK_ABOUT              "\xEE\x99\x88"
//CALL TUNE
#define LV_VTK_RING VOL           "\xEE\x98\xB4"
#define LV_VTK_MUTE               "\xEE\x98\xA6"
#define LV_VTK_VOL_LOW            "\xEE\x98\xA8"
#define LV_VTK_VOL_MID            "\xEE\x98\xA9"
#define LV_VTK_VOL_HIHG           "\xEE\x98\xA7"
//General
#define LV_VTK_DATE_AND_TIME      "\xEE\x99\x87"
#define LV_VTK_DATE               "\xEE\x99\x99"
#define LV_VTK_TIME               "\xEE\x99\x90"
#define LV_VTK_LANGUAGE           "\xEE\x99\xA0"
#define LV_VTK_SHORTCUT           "\xEE\x99\xB5"
#define LV_VTK_SD_CARD            "\xEE\x99\x8D"
#define LV_VTK_DISABLE            "\xEE\x9A\xA6"
#define LV_VTK_ENABLE             "\xEE\x9A\x97"
//INSTALLER
#define LV_VTK_CALL_Nbr           "\xEE\x99\xAB"
#define LV_VTK_PARAMETER          "\xEE\x9A\xA1"
#define LV_VTK_UPDATE             "\xEE\x99\x8E"
#define LV_VTK_SERVER             "\xEE\x98\x8E"
#define LV_VTK_CHECK              "\xEE\x9A\x9D"
#define LV_VTK_REBOOT             "\xEE\x98\xB8"
#define LV_VTK_BACKUP             "\xEE\x99\xB2"
#define LV_VTK_RESTORE            "\xEE\x98\xB9"
#define LV_VTK_QR_CODE            "\xEE\x9A\x84"
#define LV_VTK_LIFT               "\xEE\x99\x8F"
#define LV_VTK_WAITING1           "\xEE\x99\xAE"
#define LV_VTK_WAITING2           "\xEE\x9A\x94"
#define LV_VTK_ADD                "\xEE\x9A\x8A"
#define LV_VTK_SEARCH             "\xEE\x99\xA7"
#define LV_VTK_CLOUD_ABNORMAL     "\xEE\x98\x91"
#define LV_VTK_CLOUD_OFFLINE      "\xEE\x98\x93"
#define LV_VTK_CLOUD_ONLINE       "\xEE\x88\x8F"
#define LV_VTK_CLOUD_DOWNLOAD     "\xEE\x99\x9D"
#define LV_VTK_CLOUD_UPLOAD       "\xEE\x99\x8A"
#define LV_VTK_WIFI_ON            "\xEE\x99\x81"
#define LV_VTK_WIFI_OFF           "\xEE\x98\xBA"
#define LV_VTK_WIFI_ABNORMAL      "\xEE\x98\xAB"
#define LV_VTK_ROUTE              "\xEE\x9A\xAB"
#define LV_VTK_SIGNAL             "\xEE\x98\x90"
#define LV_VTK_INTERNET           "\xEE\x99\xAA"
#define LV_VTK_DOWNLOAD           "\xEE\x99\x9E"
#define LV_VTK_UPLOAD             "\xEE\x9A\x9A"
#define LV_VTK_LINKED             "\xEE\x98\x9E"
#define LV_VTK_LINK_OFF           "\xEE\x99\xAD"
//camera
#define LV_VTK_CAM1               "\xEE\x98\xA2"
#define LV_VTK_CAM_OFF            "\xEE\x98\xA3"
#define LV_VTK_CAM_ADD            "\xEE\x98\xA4"
#define LV_VTK_CAM2               "\xEE\x99\x97"
#define LV_VTK_CAM3               "\xEE\x99\x86"
#define LV_VTK_VOICEMAIL          "\xEE\x98\xA5"
#define LV_VTK_RECORD1            "\xEE\x99\x83"
#define LV_VTK_RECORD2            "\xEE\x99\xB0"
#define LV_VTK_RECORD4            "\xEE\x98\x8D"
#define LV_VTK_RECORD_OFF         "\xEE\x99\x89"
//Lock and light
#define LV_VTK_LIGHT_OFF          "\xEE\x99\xBF"
#define LV_VTK_LIGHT_ON           "\xEE\x99\xBE"
#define LV_VTK_LOCK1              "\xEE\x98\x98"
#define LV_VTK_LOCK2              "\xEE\x98\x8C"
#define LV_VTK_LOCKED             "\xEE\x9A\x89"
#define LV_VTK_UNLOCK             "\xEE\x98\x95"
#define LV_VTK_MUSIC1             "\xEE\x9A\x83"
#define LV_VTK_MUSIC_OFF1         "\xEE\x99\xAF"
//bell and mic
#define LV_VTK_BELL_ABNORMAL      "\xEE\x99\x93"
#define LV_VTK_BELL               "\xEE\x98\xBB"
#define LV_VTK_BELL_RING          "\xEE\x98\xBF"
#define LV_VTK_BELL_MUTE          "\xEE\x99\x8C"
#define LV_VTK_MIC_ON             "\xEE\x9A\xA0"
#define LV_VTK_MIC_OFF            "\xEE\x99\xB8"
//alarm
#define LV_VTK_EMERG              "\xEE\x99\xA2"
#define LV_VTK_EMERG_ON           "\xEE\x99\xB9"
#define LV_VTK_SENSOR_ON          "\xEE\x9A\x9F"
#define LV_VTK_SENSOR             "\xEE\x9A\x86"
#define LV_VTK_SENSOR_FIRE        "\xEE\x9A\x8E"
#define LV_VTK_SENSOR_ABNORMAL    "\xEE\x9A\x87"
//entrance guard 
#define LV_VTK_BLUETOOTH          "\xEE\x99\x82"
#define LV_VTK_FINGERPRINT        "\xEE\x99\x96"
#define LV_VTK_TAG                "\xEE\x99\x8D"
#define LV_VTK_NUM_KP             "\xEE\x99\x9F"
#define LV_VTK_FACE               "\xEE\x99\xAC"
//pattern
#define LV_VTK_HOME               "\xEE\x98\x97"
#define LV_VTK_LEAVE              "\xEE\x99\xA5"
#define LV_VTK_RETURN             "\xEE\x99\xBC"
#define LV_VTK_NINGT              "\xEE\x99\xA8"
#define LV_VTK_DAY                "\xEE\x9A\xA3"
#define LV_VTK_COLOR              "\xEE\x9A\x88"
#define LV_VTK_BRIGHTNESS_HIGH    "\xEE\x9A\x8C"
#define LV_VTK_BRIGHTNESS_LOW     "\xEE\x99\x8B"
//APP related
#define LV_VTK_IOS                "\xEE\x98\xB7"
#define LV_VTK_ANDROID            "\xEE\x98\xB3"
#define LV_VTK_APP_STORE          "\xEE\x98\xB5"
#define LV_VTK_GOOGLEPLAY         "\xEE\x99\xB4"
#define LV_VTK_LINUX              "\xEE\x9A\xA4"
//Indicator button
#define LV_VTK_DOWN               "\xEE\x98\xAC"
#define LV_VTK_LEFT1              "\xEE\x98\xAA"
#define LV_VTK_RIGHT1             "\xEE\x98\xB2"
#define LV_VTK_UP                 "\xEE\x98\xB6"
#define LV_VTK_EXIT               "\xEE\x9A\x9E"
#define LV_VTK_BACKWARD           "\xEE\x9A\x9C"
#define LV_VTK_FOREWARD           "\xEE\x99\xA6"
#define LV_VTK_NUMERIC_ORDER      "\xEE\x98\x8D"
#define LV_VTK_ALP_ORDER          "\xEE\x99\x80"
#define LV_VTK_START              "\xEE\x99\x85"
#define LV_VTK_PAUSE              "\xEE\x99\x9C"
#define LV_VTK_STOP               "\xEE\x99\x95"
//equipment
#define LV_VTK_IM                 "\xEE\x9A\x99"
#define LV_VTK_GUADRUNIT          "\xEE\x9A\xB0"
#define LV_VTK_DS                 "\xEE\x9A\xA8"
#define LV_VTK_TELE               "\xEE\x9A\x8F"
#define LV_VTK_PHONE1             "\xEE\x99\xB3"
#define LV_VTK_PHONE2             "\xEE\x99\xA4"
#define LV_VTK_PHONE_WIFI         "\xEE\x9A\xA5"
#define LV_VTK_KEYPAD             "\xEE\x99\xA3"
#define LV_VTK_GUADR              "\xEE\x9A\x95"
#define LV_VTK_UERS1              "\xEE\x8A\xAA"
#define LV_VTK_UERS2              "\xEE\x9A\x98"
//2023 8 31 add
#define LV_VTK_BACK               "\xEE\x9A\xB1"
#define LV_VTK_BACKSPACE          "\xEE\x9A\xB2"
#define LV_VTK_CONFIM             "\xEE\x9A\xB7"
#define LV_VTK_LEFT_PAGE          "\xEE\x9A\xB8"
#define LV_VTK_RIGHT_PAGE         "\xEE\x9A\xB9"
//2023 11 07 add
#define LV_VTK_CLOSE_VOL          "\xEE\xA8\x8B"
#define LV_VTK_OPEN_VOL           "\xEE\xA8\xBD"
#define LV_VTK_QUIT               "\xEE\x9A\xBC"
#define LV_VTK_OPEN_DIVERT        "\xEE\x9A\xBD"
#define LV_VTK_MONNEXT            "\xEE\x9C\xBC"
#define LV_VTK_TURN_BRIGHT        "\xEE\x9B\x80"
#define LV_VTK_DELETE_VIDEO       "\xEE\xA2\xB6"
#define LV_VTK_PLAY_NEXT_VIDEO    "\xEE\x9B\x81"
#define LV_VTK_OPEN_LOCK1         "\xEF\x8B\x8F"
#define LV_VTK_OPEN_LOCK2         "\xEE\xA2\x82"
#define LV_VTK_CLOSE_DIVERT       "\xEF\x8B\x93"
#define LV_VTK_RESET_PWD          "\xEE\x9B\x82"
#define LV_VTK_PHONE_FILP         "\xEE\x99\xBA"
#define LV_VTK_KEY                "\xEE\x98\x98"
#define LV_VTK_CALL_RECORD1       "\xEE\x98\x86"

#define LV_VTK_CIRCLE             "\xEE\x9A\xBB"
#define LV_VTK_WIZARD             "\xEE\x9B\x83"
#define LV_VTK_EDIT               "\xEE\x9A\xBF"


//#define LV_VTK_CIRCLE_SPACE       "\xEE\x9B\x84
#define LV_VTK_TIPS               "\xEE\x9B\x85"
#define LV_VTK_UPGRADE            "\xEE\x9B\x86"
#define LV_VTK_OPEN               "\xEE\x9B\x87"
#define LV_VTK_ERROR              "\xEE\x9B\x88"
#define LV_VTK_SUCCESS            "\xEE\x9B\x89"
#define LV_VTK_LEFT               "\xEE\x9B\x8B"
#define LV_VTK_RIGHT              "\xEE\x9B\x8A"
//20240620 add
#define LV_VTK_TIPS2              "\xEE\x9B\x8C"
//20240830 add
#define LV_VTK_REMOTE_UNLOCK_OK   "\xEE\x9B\x8E"
#define LV_VTK_REMOTE_UNLOCK_DENY "\xEE\x9B\x8D"
#define LV_VTK_REMOTE_UNLOCK_ER   "\xEE\x9B\x8F"
/*
 * The following list is generated using
 * cat src/font/lv_symbol_def.h | sed -E -n 's/^#define\s+LV_(SYMBOL_\w+).*".*$/    _LV_STR_\1,/p'
 */
enum {
    _LV_STR_SYMBOL_BULLET,
    _LV_STR_SYMBOL_AUDIO,
    _LV_STR_SYMBOL_VIDEO,
    _LV_STR_SYMBOL_LIST,
    _LV_STR_SYMBOL_OK,
    _LV_STR_SYMBOL_CLOSE,
    _LV_STR_SYMBOL_POWER,
    _LV_STR_SYMBOL_SETTINGS,
    _LV_STR_SYMBOL_HOME,
    _LV_STR_SYMBOL_DOWNLOAD,
    _LV_STR_SYMBOL_DRIVE,
    _LV_STR_SYMBOL_REFRESH,
    _LV_STR_SYMBOL_MUTE,
    _LV_STR_SYMBOL_VOLUME_MID,
    _LV_STR_SYMBOL_VOLUME_MAX,
    _LV_STR_SYMBOL_IMAGE,
    _LV_STR_SYMBOL_TINT,
    _LV_STR_SYMBOL_PREV,
    _LV_STR_SYMBOL_PLAY,
    _LV_STR_SYMBOL_PAUSE,
    _LV_STR_SYMBOL_STOP,
    _LV_STR_SYMBOL_NEXT,
    _LV_STR_SYMBOL_EJECT,
    _LV_STR_SYMBOL_LEFT,
    _LV_STR_SYMBOL_RIGHT,
    _LV_STR_SYMBOL_PLUS,
    _LV_STR_SYMBOL_MINUS,
    _LV_STR_SYMBOL_EYE_OPEN,
    _LV_STR_SYMBOL_EYE_CLOSE,
    _LV_STR_SYMBOL_WARNING,
    _LV_STR_SYMBOL_SHUFFLE,
    _LV_STR_SYMBOL_UP,
    _LV_STR_SYMBOL_DOWN,
    _LV_STR_SYMBOL_LOOP,
    _LV_STR_SYMBOL_DIRECTORY,
    _LV_STR_SYMBOL_UPLOAD,
    _LV_STR_SYMBOL_CALL,
    _LV_STR_SYMBOL_CUT,
    _LV_STR_SYMBOL_COPY,
    _LV_STR_SYMBOL_SAVE,
    _LV_STR_SYMBOL_BARS,
    _LV_STR_SYMBOL_ENVELOPE,
    _LV_STR_SYMBOL_CHARGE,
    _LV_STR_SYMBOL_PASTE,
    _LV_STR_SYMBOL_BELL,
    _LV_STR_SYMBOL_KEYBOARD,
    _LV_STR_SYMBOL_GPS,
    _LV_STR_SYMBOL_FILE,
    _LV_STR_SYMBOL_WIFI,
    _LV_STR_SYMBOL_BATTERY_FULL,
    _LV_STR_SYMBOL_BATTERY_3,
    _LV_STR_SYMBOL_BATTERY_2,
    _LV_STR_SYMBOL_BATTERY_1,
    _LV_STR_SYMBOL_BATTERY_EMPTY,
    _LV_STR_SYMBOL_USB,
    _LV_STR_SYMBOL_BLUETOOTH,
    _LV_STR_SYMBOL_TRASH,
    _LV_STR_SYMBOL_EDIT,
    _LV_STR_SYMBOL_BACKSPACE,
    _LV_STR_SYMBOL_SD_CARD,
    _LV_STR_SYMBOL_NEW_LINE,
    _LV_STR_SYMBOL_DUMMY,
};

#ifdef __cplusplus
} /*extern "C"*/
#endif

#endif /*LV_SYMBOL_DEF_H*/
