/**
 * @file lv_btn.c
 *
 */

/*********************
 *      INCLUDES
 *********************/

#include "lv_btn_usr1.h"
#if LV_USE_BTN != 0

#include "../extra/layouts/flex/lv_flex.h"

/*********************
 *      DEFINES
 *********************/
#define MY_CLASS &lv_btn_usr1_class

static void lv_btn_usr1_event(const lv_obj_class_t* class_p, lv_event_t* e);

static lv_style_t style_icon_userbtn;
static lv_style_t style_bg_color_primary;
static lv_style_t style_bg_color_secondary;
static lv_style_t style_transition_delayed;
static lv_style_t style_transition_normal;
static lv_style_t style_pressed;
static lv_style_t style_disabled;
static lv_style_t style_outline_primary;
static lv_style_t style_dark;
static lv_style_t style_grow;

#define BORDER_WIDTH            (1)
#define OUTLINE_WIDTH           (1)

static lv_color_t dark_color_filter_cb(const lv_color_filter_dsc_t* f, lv_color_t c, lv_opa_t opa)
{
    return lv_color_darken(c, opa);
}
static lv_color_t grey_filter_cb(const lv_color_filter_dsc_t* f, lv_color_t color, lv_opa_t opa)
{
    return lv_color_mix(lv_palette_darken(LV_PALETTE_GREY, 2), color, opa);
    //return lv_color_mix(lv_palette_lighten(LV_PALETTE_GREY, 2), color, opa);
}

static const lv_style_prop_t trans_props[] = {
//    LV_STYLE_BG_OPA, LV_STYLE_BG_COLOR,
    LV_STYLE_TRANSFORM_WIDTH,
//      LV_STYLE_TRANSFORM_HEIGHT,
//    LV_STYLE_TRANSLATE_Y, LV_STYLE_TRANSLATE_X,
//    LV_STYLE_TRANSFORM_ZOOM, LV_STYLE_TRANSFORM_ANGLE,
//    LV_STYLE_COLOR_FILTER_OPA, LV_STYLE_COLOR_FILTER_DSC,
    0
};

static void style_init(void)
{
    static bool style_init_flag = false;
    if (style_init_flag)
        return;

    style_init_flag = true;
    lv_style_init(&style_icon_userbtn);
    lv_style_set_radius(&style_icon_userbtn, 8);
    lv_style_set_bg_opa(&style_icon_userbtn, LV_OPA_0);
    lv_style_set_bg_color(&style_icon_userbtn, lv_palette_main(LV_PALETTE_RED));

    lv_style_set_shadow_color(&style_icon_userbtn, lv_palette_main(LV_PALETTE_GREY));
    lv_style_set_shadow_width(&style_icon_userbtn, LV_DPX(1));
    lv_style_set_shadow_opa(&style_icon_userbtn, LV_OPA_0);
    lv_style_set_shadow_ofs_y(&style_icon_userbtn, LV_DPX(1));

    lv_style_set_pad_hor(&style_icon_userbtn, 1);
    lv_style_set_pad_ver(&style_icon_userbtn, 1);
    lv_style_set_pad_column(&style_icon_userbtn, 1);
    lv_style_set_pad_row(&style_icon_userbtn, 1);
    //
    lv_style_init(&style_bg_color_primary);
    lv_style_set_bg_color(&style_bg_color_primary, lv_palette_main(LV_PALETTE_GREEN));
    lv_style_set_text_color(&style_bg_color_primary, lv_color_white());
    lv_style_set_bg_opa(&style_bg_color_primary, LV_OPA_0);
    //
    lv_style_init(&style_bg_color_secondary);
    lv_style_set_bg_color(&style_bg_color_secondary, lv_palette_main(LV_PALETTE_BLUE));
    lv_style_set_text_color(&style_bg_color_secondary, lv_color_white());
    lv_style_set_bg_opa(&style_bg_color_secondary, LV_OPA_COVER);
    //
    lv_style_init(&style_transition_delayed);
    static lv_style_transition_dsc_t trans_delayed;
    lv_style_transition_dsc_init(&trans_delayed, trans_props, lv_anim_path_linear, 80, 60, NULL);
    lv_style_set_transition(&style_transition_delayed, &trans_delayed); /*Go back to default state with delay*/
    lv_style_init(&style_transition_normal);
    static lv_style_transition_dsc_t trans_normal;
    lv_style_transition_dsc_init(&trans_normal, trans_props, lv_anim_path_linear, 80, 0, NULL);
    lv_style_set_transition(&style_transition_normal, &trans_normal); /*Go back to default state with delay*/
    //
    lv_style_init(&style_grow);
    lv_style_set_transform_width(&style_grow, (3));
    lv_style_set_transform_height(&style_grow, (3));
    //
    static lv_color_filter_dsc_t dark_filter;
    lv_color_filter_dsc_init(&dark_filter, dark_color_filter_cb);
    lv_style_init(&style_pressed);
    lv_style_set_color_filter_dsc(&style_pressed, &dark_filter);
    lv_style_set_color_filter_opa(&style_pressed, 35);
    //
    static lv_color_filter_dsc_t grey_filter;
    lv_color_filter_dsc_init(&grey_filter, grey_filter_cb);
    lv_style_init(&style_disabled);
    lv_style_set_color_filter_dsc(&style_disabled, &grey_filter);
    lv_style_set_color_filter_opa(&style_disabled, LV_OPA_50);
    //
    lv_style_init(&style_outline_primary);
    lv_style_set_outline_color(&style_outline_primary, lv_palette_main(LV_PALETTE_GREY));
    lv_style_set_outline_width(&style_outline_primary, OUTLINE_WIDTH);
    lv_style_set_outline_pad(&style_outline_primary, OUTLINE_WIDTH);
    lv_style_set_outline_opa(&style_outline_primary, LV_OPA_0);
    //
    lv_style_init(&style_dark);
    lv_style_set_bg_opa(&style_dark, LV_OPA_COVER);
    lv_style_set_bg_color(&style_dark, lv_palette_main(LV_PALETTE_GREY));
    lv_style_set_line_width(&style_dark, 1);
    lv_style_set_line_color(&style_dark, lv_palette_main(LV_PALETTE_GREY));
    lv_style_set_arc_width(&style_dark, 2);
    lv_style_set_arc_color(&style_dark, lv_palette_main(LV_PALETTE_GREY));
}


/**********************
 *      TYPEDEFS
 **********************/

/**********************
 *  STATIC PROTOTYPES
 **********************/
static void lv_btn_usr1_constructor(const lv_obj_class_t * class_p, lv_obj_t * obj);

/**********************
 *  STATIC VARIABLES
 **********************/
const lv_obj_class_t lv_btn_usr1_class  = {
    .constructor_cb = lv_btn_usr1_constructor,
    .event_cb = lv_btn_usr1_event,
    .width_def = LV_SIZE_CONTENT,
    .height_def = LV_SIZE_CONTENT,
    .group_def = LV_OBJ_CLASS_GROUP_DEF_TRUE,
    .instance_size = sizeof(lv_btn_usr1_t),
    .base_class = &lv_obj_class
};

/**********************
 *      MACROS
 **********************/

/**********************
 *   GLOBAL FUNCTIONS
 **********************/

lv_obj_t * lv_btn_usr1_create(lv_obj_t * parent)
{
    LV_LOG_INFO("begin");

    style_init();

    lv_obj_t * obj = lv_obj_class_create_obj(MY_CLASS, parent);
    lv_obj_class_init_obj(obj);

    lv_obj_remove_style_all(obj);

    lv_obj_add_style(obj, &style_icon_userbtn, 0);
    lv_obj_add_style(obj, &style_bg_color_primary, 0);
    lv_obj_add_style(obj, &style_transition_delayed, 0);
    lv_obj_add_style(obj, &style_transition_normal, LV_STATE_PRESSED);
    lv_obj_add_style(obj, &style_pressed, LV_STATE_PRESSED);
    lv_obj_add_style(obj, &style_grow, LV_STATE_PRESSED);
    lv_obj_add_style(obj, &style_disabled, LV_STATE_DISABLED);
    lv_obj_add_style(obj, &style_bg_color_secondary, LV_STATE_CHECKED);
    lv_obj_add_style(obj, &style_outline_primary, LV_STATE_FOCUSED);

    //lv_obj_set_style_bg_opa(obj, LV_OPA_0, 0);

    return obj;
}

/**********************
 *   STATIC FUNCTIONS
 **********************/
static void lv_btn_usr1_constructor(const lv_obj_class_t * class_p, lv_obj_t * obj)
{
    LV_UNUSED(class_p);
    LV_TRACE_OBJ_CREATE("begin");

    lv_obj_clear_flag(obj, LV_OBJ_FLAG_SCROLLABLE);
    lv_obj_add_flag(obj, LV_OBJ_FLAG_SCROLL_ON_FOCUS);

    LV_TRACE_OBJ_CREATE("finished");
}

static void lv_btn_usr1_event(const lv_obj_class_t* class_p, lv_event_t* e)
{
    LV_UNUSED(class_p);

    lv_res_t res;

    /*Call the ancestor's event handler*/
    res = lv_obj_event_base(MY_CLASS, e);
    if (res != LV_RES_OK) return;

    lv_event_code_t code = lv_event_get_code(e);
    lv_obj_t* obj = lv_event_get_target(e);

    if (code == LV_EVENT_PRESSED) {
        lv_obj_set_style_outline_opa(obj, LV_OPA_50, LV_STATE_FOCUSED);
    }
    else if (code == LV_EVENT_RELEASED) {
        lv_obj_set_style_outline_opa(obj, LV_OPA_0, LV_STATE_FOCUSED);
    }
}

#endif
