#!/bin/bash

function recursive_dir()
{
	local curdir subdir filter_dir std_dir std_filter_dir
	curdir=$(pwd)
	for subdir in $(ls ${curdir})
	do
		#echo $subdir
		#echo $PID_CODE
		result=$(echo $subdir | grep "${PID_CODE}")
		if [ "$result" != "" ];then
			filter_dir+=" "
			filter_dir+=$subdir
		fi
		result1=$(echo $subdir | grep "${PID_FLAG}")
		if [ "$result1" == "" ];then
			std_dir+=" "
			std_dir+=$subdir
		fi
	done
	#echo $curdir
	#echo $filter_dir
	#echo $std_dir
	for subdir in $std_dir
	do
		n=0
		#subdir="-${subdir}"
		for subdir1 in $filter_dir
		do
			result=$(echo $subdir1 | grep "${subdir}")
			if [ "$result" != "" ];then
			#if [ $subdir1 =~ $subdir ];then
				n=1
			fi
		done
		if [ $n -eq 0 ];then
			std_filter_dir+=" "
			std_filter_dir+=$subdir
		fi
	done
	filter_dir+=$std_filter_dir
	echo $filter_dir
	for subdir in $filter_dir
	do
		if test -d ${subdir}
		then
			cd ${subdir}
			recursive_dir
			cd ..
		else
			file=${curdir}/${subdir}
			filename=`basename ${file}`
			name=${filename%%.*}
			ext=${filename##*.}
			if [ ${ext} == "h" ];then				
				ln -s ${file} ${INCPATH}/${filename}
				echo ${INCPATH}/${filename}
			fi
		fi
	done
}
SUBMF=$(pwd)/Makefile_sub
INCPATH=$(pwd)/include
APPPATH=$(pwd)/app
GUIPATH=$(pwd)/lvgl_menu

para_all=$@
para_num=$#
PID_CODE=$1
PID_FLAG=PID_


cd ${INCPATH}
echo $(pwd)
rm *.h
cd ..
cd ${APPPATH}
recursive_dir
cd ..
cd ${GUIPATH}
recursive_dir

