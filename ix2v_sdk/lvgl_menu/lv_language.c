﻿#include "lv_ix850.h"
#include "define_file.h"
#include "obj_IoInterface.h"
cJSON* language = NULL;
static int EnNoTranslationPrefix = 0;

#define NoTranslationPrefix 	(EnNoTranslationPrefix?"--":"")

cJSON* GetLanguageJson(char* languageName)
{
	char tempString[100];
	cJSON* ret = NULL;
	snprintf(tempString, 100, "%s/%s.json", CustomerizedLanguageNandFolder, languageName);
	ret = GetJsonFromFile(tempString);
	if(!ret)
	{
		snprintf(tempString, 100, "%s/%s.json", ResLanguageNandFolder, languageName);
		ret = GetJsonFromFile(tempString);
	}
	return ret;
}

void vtk_label_set_text(lv_obj_t * obj, const char * text)
{
	char tempString[100];
	char* labelText;
	if(language == NULL)
	{
		language = GetLanguageJson(API_Para_Read_String2(CurLanguage));
	}
	if(language)
	{
		if(!GetJsonDataPro(language, text, tempString))
		{
			
				snprintf(tempString, 100, "%s%s", NoTranslationPrefix, text);
		}
	}
	else
	{
		snprintf(tempString, 100, "%s%s", NoTranslationPrefix, text);
	}

	labelText = lv_label_get_text(obj);
	if(strcmp(labelText, tempString))
	{
	    lv_label_set_long_mode(obj, LV_LABEL_LONG_DOT);
		lv_label_set_text(obj, tempString);
	}
}

void vtk_table_set_cell_text(lv_obj_t * obj, const int row,const int col,const char * text)
{
	char tempString[100];
	if(language == NULL)
	{
		language = GetLanguageJson(API_Para_Read_String2(CurLanguage));
	}
	if(language)
	{
		if(!GetJsonDataPro(language, text, tempString))
		{
			snprintf(tempString, 100, "%s%s", NoTranslationPrefix, text);
		}
	}
	else
	{
		snprintf(tempString, 100, "%s%s", NoTranslationPrefix, text);
	}

	if(strcmp(lv_table_get_cell_value(obj, row, col), tempString))
	{
		lv_table_set_cell_value(obj, row,col, tempString);
	}
}

char* get_language_text(char * text)
{
	char* ret = NULL;
	char tempString[100];
	if(language == NULL)
	{
		language = GetLanguageJson(API_Para_Read_String2(CurLanguage));
	}
	if(language)
	{
		ret = cJSON_GetStringValue(cJSON_GetObjectItemCaseSensitive(language, text));
	}
	return ret;
}

char* get_language_text2(const char* text, char* output, int outputLen)
{
	if(!output)
	{
		return output;
	}
	char* ret = get_language_text(text);
	if(ret)
	{
		snprintf(output, outputLen, "%s", ret);
	}
	else
	{
		snprintf(output, outputLen, "%s%s", NoTranslationPrefix, text);
	}

	return output;
}

void set_language(void)
{
	if(language)
	{
		cJSON_Delete(language);
	}
	EnNoTranslationPrefix=API_PublicInfo_Read_Bool("NoTranslationPrefix");
	language = GetLanguageJson(API_Para_Read_String2(CurLanguage));;
}