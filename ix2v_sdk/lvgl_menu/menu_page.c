#include "menu_page.h"

extern void vtk_lvgl_lock(void);
extern void vtk_lvgl_unlock(void);

extern lv_style_t style_main_menu;
extern lv_style_t style_menu_page;
extern lv_style_t style_menu_bkgd;

char* get_io_para_bkgd_file(void)
{
    return NULL;
}

bool load_icon_list(PAGE_INS* ppage, const cJSON* piconlist, const char* icon_img_path);

static lv_obj_t* page_bkgd;
static int  page_type;
static bool page_sidebar;
lv_obj_t* create_menu_page_obj(lv_obj_t* parent, cJSON* page_info, char* res_path)
{
    char* ext_file;
    char fullname[200];
    lv_obj_t* page_obj;
    cJSON* json_page_type = cJSON_GetObjectItem(page_info, get_global_key_string(GLOBAL_MENU_KEYS_TYPE));
    cJSON* json_page_title = cJSON_GetObjectItem(page_info, get_global_key_string(GLOBAL_MENU_KEYS_PAGE_TITLE));
    cJSON* json_page_name = cJSON_GetObjectItem(page_info, get_global_key_string(GLOBAL_MENU_KEYS_PAGE_NAME));
    cJSON* json_sidebar = cJSON_GetObjectItem(page_info, get_global_key_string(GLOBAL_MENU_KEYS_SIDEBAR));
    cJSON* w = cJSON_GetObjectItem(page_info, get_global_key_string(GLOBAL_MENU_KEYS_SRC_WIDTH));
    cJSON* h = cJSON_GetObjectItem(page_info, get_global_key_string(GLOBAL_MENU_KEYS_SRC_HEIGHT));
    cJSON* b = cJSON_GetObjectItem(page_info, get_global_key_string(GLOBAL_MENU_KEYS_BACKGROUND));
    cJSON* bg_color = cJSON_GetObjectItem(page_info, get_global_key_string(GLOBAL_ICON_KEYS_BACKGROUND_COLOR));
    // get page type
    if (strcmp(json_page_type->valuestring, "image") == 0)
        page_type = MENU_TYPE_IMAGE;
    else if (strcmp(json_page_type->valuestring, "standard") == 0)
        page_type = MENU_TYPE_STANDARD;
    else if (strcmp(json_page_type->valuestring, "setting") == 0)
        page_type = MENU_TYPE_SETTING;
    else
        page_type = MENU_TYPE_UNKNOWN;

    // create page obj or common obj
    if (json_page_title == NULL || (strlen(json_page_title->valuestring) == 0))
        page_obj = lv_menu_page_create(parent, NULL);    // page head hidden
    else
        page_obj = lv_menu_page_create(parent, json_page_title->valuestring);

    // check page sidebar
    if (cJSON_IsBool(json_sidebar) && cJSON_IsTrue(json_sidebar))
        page_sidebar = true;
    else
        page_sidebar = false;

    lv_obj_add_style(page_obj, &style_menu_page, 0);

    // init menu and background
    if( page_type != MENU_TYPE_SETTING )
    {
        lv_obj_clear_flag( page_obj, LV_OBJ_FLAG_SCROLLABLE);
        lv_obj_t* bkgd_cont = lv_menu_cont_create( page_obj );
        lv_obj_add_style(bkgd_cont, &style_menu_bkgd, 0);
        lv_obj_clear_flag(bkgd_cont, LV_OBJ_FLAG_SCROLLABLE);           
            
        page_bkgd = lv_obj_create(bkgd_cont);
        if (w && h)
            lv_obj_set_size(page_bkgd, w->valueint, h->valueint);
        else
            lv_obj_set_size(page_bkgd, lv_disp_get_hor_res(NULL), lv_disp_get_ver_res(NULL));

        if (b)
        {
            if( ext_file = get_io_para_bkgd_file() == NULL )
            {
    #if OP_TYPE_WINDOW       // window
                sprintf(fullname, "%s/%s/%s", res_path, PAGE_RES_DIR_NAME, b->valuestring);
    #else
                sprintf(fullname, "S:%s/%s/%s", res_path, PAGE_RES_DIR_NAME, b->valuestring);
    #endif                
            }
            else
            {
    #if OP_TYPE_WINDOW       // window
                sprintf(fullname, "%s", ext_file);
    #else
                sprintf(fullname, "S:%s", ext_file);
    #endif                
            }
            lv_obj_t* bkgd_img = lv_img_create(page_bkgd);
            lv_img_set_src(bkgd_img, fullname);
        }
        lv_obj_add_style(page_bkgd, &style_menu_bkgd, 0);
        if(bg_color)
        {
            lv_obj_set_style_bg_color(page_bkgd, lv_color_hex(bg_color->valueint), LV_PART_MAIN);
        }
        if( page_type != MENU_TYPE_STANDARD ) lv_obj_clear_flag(page_bkgd, LV_OBJ_FLAG_SCROLLABLE);  
    }
    else
    {
        page_bkgd = NULL;
    }
    return page_obj;
}


PAGE_INS* new_one_page_ins( lv_obj_t* parent, char* res_path, cJSON* page_info, cJSON* icon_info)
{
    PAGE_INS* ppage = NULL;
    // initial menu background
    ppage = (PAGE_INS*)lv_mem_alloc(sizeof(PAGE_INS));
    lv_memset_00(ppage, sizeof(PAGE_INS));
    if (ppage)
    {
        ppage->page_obj  = create_menu_page_obj(parent, page_info, res_path);
        ppage->page_bkgd = page_bkgd;
        ppage->page_type = page_type;
        ppage->page_sidebar = page_sidebar;
        if( ppage->page_sidebar == true )
        {
            //lv_obj_set_style_pad_hor(page_obj, lv_obj_get_style_pad_left(lv_menu_get_main_header(ppage->page_owner), 0), 0);
            //lv_menu_separator_create(page_obj);
        }

        // create icon list
        ppage->page_icons = cJSON_CreateObject();       
        //printf("---------0-----load_icon_list---------\n");
        load_icon_list(ppage, icon_info, res_path);
        //printf("---------1-----load_icon_list---------\n");
    }
    return ppage;
}

bool destroy_page_icons(PAGE_INS* ppage)
{
    int i, count;
    if(ppage && ppage->page_icons)
    {
        if (ppage->page_icons)
        {
            cJSON* piconjson;
            cJSON_ArrayForEach(piconjson, ppage->page_icons)
            {
                destroy_one_icon((struct ICON_INS*)piconjson->valueint);
            }
            cJSON_Delete(ppage->page_icons);
            ppage->page_icons = NULL;
            return true;
        }
    }

    return false;
}
bool dele_one_page_ins(PAGE_INS* ppage)
{
    int i, count;
    if (ppage)
    {
        destroy_page_icons(ppage);
        if(ppage->page_obj)
        {
            lv_obj_del(ppage->page_obj);
            ppage->page_obj = NULL;
        }

        if (ppage->page_info)
        {
            cJSON_Delete(ppage->page_info);
            ppage->page_info = NULL;
        }

        if (ppage->res_file)
        {
            lv_mem_free(ppage->res_file);
            ppage->res_file = NULL;
        }
        lv_mem_free(ppage);
        ppage = NULL;
        return true;
    }
    else
        return false;
}

cJSON* get_files_from_path(const char* page_path,int mode)
{
    int i, size;
    cJSON* files;
    cJSON* dirs;
    cJSON* jsontemp;
    char* strtemp;
    char* extend;
    char fullname[200];

#if OP_TYPE_WINDOW
    sprintf(fullname, "%s/*", page_path);
#else
    sprintf(fullname, "%s", page_path);
#endif
    // get resource file and dir
    files = cJSON_CreateObject();
    dirs = cJSON_CreateObject();
    if (get_files_and_dirs(fullname, files, dirs) == false)
    {
        cJSON_Delete(files);
        cJSON_Delete(dirs);
        return NULL;
    }

    //strtemp = cJSON_Print(files);
    //printf("files[%s]\n",strtemp);
    //free(strtemp);
    //strtemp = cJSON_Print(dirs);
    //printf("dirs[%s]\n",strtemp);
    //free(strtemp);
    // have no fix res dir
    cJSON* jsondir = cJSON_GetArrayItem(dirs, 0);
    size = cJSON_GetArraySize(dirs);
    for (i = 0; i < size; i++)
    {
        jsontemp = cJSON_GetArrayItem(dirs, i);
        strtemp = jsontemp->valuestring;
        //printf("======================[%s]:[%s]============================\n",strtemp,PAGE_RES_DIR_NAME);
        if( strcmp(strtemp,PAGE_RES_DIR_NAME) == 0 )
            break;
        if( i == size )
        {
            cJSON_Delete(files);
            cJSON_Delete(dirs);
            return NULL;
        } 
    }
     
    // have json file

    size = cJSON_GetArraySize(files);
    for (i = 0; i < size; i++)
    {
        jsontemp = cJSON_GetArrayItem(files, i);
        strtemp = jsontemp->valuestring;
        extend = strchr(strtemp,'.');
        //printf("===extend[%s]===\n",extend);
        if( extend && extend != strtemp )
        {
        	if(mode==1)
        	{
	            if( strcmp( extend+1, "installer.json") == 0 )
	            {
	                //printf("get json file[%s]\n",strtemp);
	                jsontemp = cJSON_CreateString(strtemp);
	                cJSON_Delete(files);
	                cJSON_Delete(dirs);
	                return jsontemp;
	            }
        	}
		else
		{
	            if( strcmp( extend+1, "json") == 0 )
	            {
	                //printf("get json file[%s]\n",strtemp);
	                jsontemp = cJSON_CreateString(strtemp);
	                cJSON_Delete(files);
	                cJSON_Delete(dirs);
	                return jsontemp;
	            }
        	}
        }
    }
    cJSON_Delete(files);
    cJSON_Delete(dirs);
    return NULL;
}

/*------------------------------------------------------------------------------------------------------
menu page driver interface
------------------------------------------------------------------------------------------------------*/

/// @brief              - init_one_menu
/// @param menu_name    - menu name
/// @param menu_cname   - menu config name
/// @param screen       - parent window, default is screen
/// @return             - NOT NULL: MENU instance pointer, NULL: failed
MENU_INS* init_one_menu( const char* menu_name, const char* menu_cname, lv_obj_t* screen )
{
    MENU_INS* pmenu;
    pmenu = (MENU_INS*)lv_mem_alloc(sizeof(MENU_INS));
    lv_memset_00(pmenu, sizeof(MENU_INS));
    if (pmenu)
    {
        pmenu->res_root = NULL;
        pmenu->res_icon = NULL;
        pmenu->res_pages = NULL;
        pmenu->res_main = NULL;
        pmenu->menu_screen = screen;
        pmenu->menu_name = NULL;
        if( menu_name != NULL )
        {
            pmenu->menu_name = lv_mem_alloc(strlen(menu_name) + 1);
            strcpy(pmenu->menu_name, menu_name);            
        }
        if( menu_cname != NULL )
        {
            pmenu->menu_cname = lv_mem_alloc(strlen(menu_cname) + 1);
            strcpy(pmenu->menu_cname, menu_cname);
        }
        pmenu->menu_root = lv_menu_create( pmenu->menu_screen);
        lv_menu_set_mode_root_back_btn(pmenu->menu_root, LV_MENU_ROOT_BACK_BTN_DISABLED);
        lv_obj_set_size(pmenu->menu_root, lv_disp_get_hor_res(NULL), lv_disp_get_ver_res(NULL));
        lv_obj_add_style(pmenu->menu_root, &style_main_menu, 0);
        lv_obj_align(pmenu->menu_root, LV_ALIGN_TOP_LEFT, 0, 0);

        pmenu->menu_pages = cJSON_CreateObject();
    }
    return pmenu;
}

extern MENU_INS *MainPageIns;
/// @brief          - deinit_one_menu
/// @param pmenu    - menu instance pointer
/// @return         - true: ok, flase: error
bool deinit_one_menu( MENU_INS* pmenu )
{
    if( pmenu == NULL ) 
        return false;    
	//ShellCmdPrintf("11111%s:%d\n",__func__,__LINE__);
    if( pmenu->menu_name )
    {
        lv_mem_free(pmenu->menu_name);
        pmenu->menu_name = NULL;
    }
	//ShellCmdPrintf("11111%s:%d\n",__func__,__LINE__);
    if( pmenu->menu_cname )
    {
        lv_mem_free(pmenu->menu_cname);
        pmenu->menu_cname = NULL;
    }
	//ShellCmdPrintf("11111%s:%d\n",__func__,__LINE__);
    kill_all_menu_pages(pmenu);
	//ShellCmdPrintf("11111%s:%d\n",__func__,__LINE__);
    if( pmenu->menu_root ) 
    {
        lv_obj_del(pmenu->menu_root);
        pmenu->menu_root = NULL;
    }

	//ShellCmdPrintf("11111%s:%d\n",__func__,__LINE__);
	 if( pmenu->header) 
	 {
	 	del_header(pmenu->header);
		lv_obj_del(pmenu->header);	
	 }
	// ShellCmdPrintf("11111%s:%d\n",__func__,__LINE__);
    pmenu->menu_screen = NULL;
    lv_mem_free(pmenu);
    pmenu = NULL;
	//ShellCmdPrintf("11111%s:%d\n",__func__,__LINE__);
    //Clear global variables
#if !defined( PID_IX622 )	    
    //MainPageIns = NULL;
#endif    
}

static const char* MENU_CFG_DIR[] = {MENU_CONFIG_SD_PATH, MENU_CONFIG_CUS_PATH, MENU_CONFIG_FIX_PATH, NULL};

int menu_personal_control(cJSON* json_data, const char* page_dir){
    if (json_data == NULL)
        return -1;
    cJSON* page_info = cJSON_GetObjectItem(json_data, get_global_key_string(GLOBAL_MENU_KEYS_SCREEN));
    if (page_info == NULL)
        return -1;

    cJSON* icon_info = cJSON_GetObjectItem(json_data, get_global_key_string(GLOBAL_MENU_KEYS_ICONLIST));
    if (icon_info == NULL)
        return -1;

    char* page_name = GetEventItemString(page_info, get_global_key_string(GLOBAL_MENU_KEYS_PAGE_NAME));
    if(strcmp(page_name,MAIN_PAGE_MAIN) != 0)
        return -1;
    int size = cJSON_GetArraySize(icon_info);
    char menu_config_path[500];
    char buf[500];
    for(int i=0;i<size;i++){
        cJSON* icon = cJSON_GetArrayItem(icon_info,i);
        if(icon == NULL)
            break;
        cJSON* icon_name = cJSON_GetObjectItem(icon, "ICON_NAME");            
        if(icon_name){          
            cJSON* iconPara = API_Para_Read_Public(icon_name->valuestring);
            if(iconPara){
                
                char* text = API_GetObjectString(iconPara, "TEXT");
                char* nbr = API_GetObjectString(iconPara, "NBR");
                char* pic = API_GetObjectString(iconPara, "PIC");
                int size = API_GetObjectInt(iconPara, "SIZE");
                int color = API_GetObjectInt(iconPara, "COLOR");

                for(int index = 0; MENU_CFG_DIR[index]; index++)
                {
                    snprintf(buf, 500, "%s/%s/%s", MENU_CFG_DIR[index], INFO_RES_DIR_NAME,pic);
                    
                    if(IsFileExist(buf))
                    {
                        snprintf(menu_config_path, 500, "S:%s",buf);
                        cJSON_ReplaceItemInObject(icon,"PIC",cJSON_CreateString(menu_config_path)); 
                        break;
                    }
                }
             
                cJSON_ReplaceItemInObject(icon,"CallBtn_Number",cJSON_CreateString(nbr));
                cJSON* icon_text = cJSON_GetObjectItem(icon, "TEXT");
                if(icon_text){
                    cJSON_ReplaceItemInObject(icon_text,"Label_text",cJSON_CreateString(text));
                    cJSON_ReplaceItemInObject(icon_text,"text_size",cJSON_CreateNumber(size));
                    cJSON_ReplaceItemInObject(icon_text,"text_color",cJSON_CreateNumber(color));
                }  
            }
        }
    }
    return 1;
}

/// @brief          - create_one_menu_page
/// @param pmenu    - menu instance pointer
/// @param page_dir - page resource directory
/// @return         - NOT NULL: PAGE instance pointer, NULL: failed
PAGE_INS* create_one_menu_page(MENU_INS* pmenu, const char* page_dir)
{
    cJSON* json_file;
    cJSON* json_data;
    cJSON* page_info;
    cJSON* icon_info;
    PAGE_INS* ppage;

    char fullname[240];

    if (page_dir == NULL)
        return NULL;
	    
    json_file = get_files_from_path(page_dir,0);
    if( json_file == NULL ){
        return NULL;
    }
        
    sprintf(fullname, "%s/%s", page_dir, json_file->valuestring);
	//printf("%s:%d:%s\n",__func__,__LINE__,fullname);
    cJSON_Delete(json_file);

    json_data = get_cjson_from_file(fullname);
    if (json_data == NULL)
        return NULL;
    menu_personal_control(json_data,page_dir);   

    page_info = cJSON_GetObjectItem(json_data, get_global_key_string(GLOBAL_MENU_KEYS_SCREEN));
    if (page_info == NULL)
    {
        cJSON_Delete(json_data);
        return NULL;
    }

    icon_info = cJSON_GetObjectItem(json_data, get_global_key_string(GLOBAL_MENU_KEYS_ICONLIST));
    if (icon_info == NULL)
    {
        cJSON_Delete(json_data);
        return NULL;
    }

    char* page_name = GetEventItemString(page_info, get_global_key_string(GLOBAL_MENU_KEYS_PAGE_NAME));
    ppage = (PAGE_INS*)GetEventItemInt(pmenu->menu_pages, page_name);
    if(ppage)
    {
        kill_one_menu_page(pmenu, page_name);
        ppage = NULL;
    }

    vtk_lvgl_lock();
    ppage = new_one_page_ins(pmenu->menu_root, page_dir, page_info, icon_info);
    vtk_lvgl_unlock();
    if( ppage )
    {
        // set owner
        ppage->page_owner = pmenu;
        // save info
        ppage->page_info = cJSON_Duplicate(page_info,true);
        // save file name
        ppage->res_file = lv_mem_alloc(strlen(fullname) + 1);
        strcpy(ppage->res_file, fullname);
        // store in menu
        cJSON_AddNumberToObject(pmenu->menu_pages, page_name, (int)ppage);
        API_PublicInfo_Write_String(page_name, page_dir);
    }

    cJSON_Delete(json_data);
    return ppage;
}

PAGE_INS* create_one_menu_page_installer(MENU_INS* pmenu, const char* page_dir)
{
    cJSON* json_file;
    cJSON* json_data;
    cJSON* page_info;
    cJSON* icon_info;
    PAGE_INS* ppage;

    char fullname[240];

    if (page_dir == NULL)
        return NULL;
	    
    json_file = get_files_from_path(page_dir,1);
    if( json_file == NULL ){
        return NULL;
    }
        
    sprintf(fullname, "%s/%s", page_dir, json_file->valuestring);
	//printf("%s:%d:%s\n",__func__,__LINE__,fullname);
    cJSON_Delete(json_file);

    json_data = get_cjson_from_file(fullname);
    if (json_data == NULL)
        return NULL;
    menu_personal_control(json_data,page_dir);
        
    page_info = cJSON_GetObjectItem(json_data, get_global_key_string(GLOBAL_MENU_KEYS_SCREEN));
    if (page_info == NULL)
    {
        cJSON_Delete(json_data);
        return NULL;
    }

    icon_info = cJSON_GetObjectItem(json_data, get_global_key_string(GLOBAL_MENU_KEYS_ICONLIST));
    if (icon_info == NULL)
    {
        cJSON_Delete(json_data);
        return NULL;
    }

    char* page_name = GetEventItemString(page_info, get_global_key_string(GLOBAL_MENU_KEYS_PAGE_NAME));
    ppage = (PAGE_INS*)GetEventItemInt(pmenu->menu_pages, page_name);

    if(!ppage)
    {
	    vtk_lvgl_lock();
	    ppage = new_one_page_ins(pmenu->menu_root, page_dir, page_info, icon_info);
	    vtk_lvgl_unlock();	
		cJSON_DeleteItemFromObject(pmenu->menu_pages, page_name);
		cJSON_AddNumberToObject(pmenu->menu_pages, page_name, (int)ppage);
    }
	else
	{
        destroy_page_icons(ppage);
        ppage->page_icons = cJSON_CreateObject(); 
		load_icon_list(ppage, icon_info, page_dir);
	}

    if( ppage )
    {
        // set owner
        ppage->page_owner = pmenu;
        // save info
         if(ppage->page_info)
		 	cJSON_Delete(ppage->page_info);
        ppage->page_info = cJSON_Duplicate(page_info,true);
        // save file name
        if(ppage->res_file)
		 	lv_mem_free(ppage->res_file);
        ppage->res_file = lv_mem_alloc(strlen(fullname) + 1);
        strcpy(ppage->res_file, fullname);

        API_PublicInfo_Write_String(page_name, page_dir);
    }
    cJSON_Delete(json_data);
    return ppage;
}


/// @brief              - kill_one_menu_page
/// @param pmenu        - menu instance pointer
/// @param page_name    - page name
/// @return             - true: ok, flase: error
bool kill_one_menu_page(MENU_INS* pmenu, const char* page_name)
{
    if( pmenu == NULL || page_name == NULL ) return false;

    PAGE_INS* ppage = (PAGE_INS*)GetEventItemInt(pmenu->menu_pages, page_name);
    if (ppage)
    {
        vtk_lvgl_lock();
        if( lv_menu_get_cur_main_page(pmenu->menu_root) == ppage->page_obj ) 
            lv_menu_set_page(pmenu->menu_root,NULL);
        dele_one_page_ins(ppage);
        API_PublicInfo_Write_String(page_name,"");
        vtk_lvgl_unlock();
        cJSON_DeleteItemFromObjectCaseSensitive(pmenu->menu_pages, page_name);
        return true;
    }

    return false;
}

/// @brief          - get_name_of_one_page
/// @param pmenu    - menu instance pointer
/// @param index    - menu page index
/// @return         - page name
char* get_name_of_one_page(MENU_INS* pmenu, int index)
{
    int i, count;

    if( pmenu == NULL ) return NULL;

    if (pmenu->menu_pages)
    {
        count = cJSON_GetArraySize(pmenu->menu_pages);
        if( index <= (count-1) )
        {
            cJSON* pjson = cJSON_GetArrayItem(pmenu->menu_pages, index);
            PAGE_INS* ppage = (PAGE_INS*)pjson->valueint;
            pjson = cJSON_GetObjectItem(ppage->page_info, get_global_key_string(GLOBAL_MENU_KEYS_PAGE_NAME));
            return pjson->valuestring;
        }
    }
    else
        return NULL;
}

/// @brief              - get_res_name_of_one_page
/// @param pmenu        - menu instance pointer 
/// @param page_name    - page name
/// @return             - NULL: NONE, NOT NULL: res direcotry name
char* get_res_name_of_one_page(MENU_INS* pmenu, const char* page_name)
{
    int i, count;

    if( pmenu == NULL || page_name == NULL ) return NULL;

    if (pmenu->menu_pages)
    {
        count = cJSON_GetArraySize(pmenu->menu_pages);
        for (i = 0; i < count; i++)
        {
            cJSON* piconjson = cJSON_GetArrayItem(pmenu->menu_pages, i);
            if (strcmp(piconjson->string, page_name) == 0)
            {
                PAGE_INS* ppage = (PAGE_INS*)piconjson->valueint;
                return ppage->res_file;
            }
        }
    }
    else
        return NULL;
}

/// @brief          - kill_all_menu_pages
/// @param pmenu    - menu instance pointer 
/// @return         - true: ok, flase: error
bool kill_all_menu_pages(MENU_INS* pmenu)
{
    int i, count;

    if( pmenu == NULL ) return false;

    if (pmenu->menu_pages)
    {
        cJSON* piconjson;
        vtk_lvgl_lock();
        if(pmenu->menu_root)
        {
            lv_menu_set_page(pmenu->menu_root,NULL);
        }
        cJSON_ArrayForEach(piconjson, pmenu->menu_pages)
        {
            dele_one_page_ins((PAGE_INS*)piconjson->valueint);
        }
        cJSON_Delete(pmenu->menu_pages);
        pmenu->menu_pages = NULL;
        vtk_lvgl_unlock();
        return true;
    }        

    return false;
}

/// @brief           - is_page_of_one_menu
/// @param pmenu     - menu instance pointer 
/// @param page_name - page name
/// @return          - true: IS OK, flase: IS NOT   
bool is_page_of_one_menu(MENU_INS* pmenu, const char* page_name)
{
    if( pmenu == NULL ) return false;
    return get_menu_page_obj(pmenu, page_name);
}

/// @brief          - get_total_pages_of_one_menu
/// @param pmenu    - menu instance pointer
/// @return         - -1: pmenu is NULL, x page number
int get_total_pages_of_one_menu(MENU_INS* pmenu)
{
    if( pmenu == NULL ) return -1;
    if( pmenu->menu_pages == NULL ) return 0;
    return cJSON_GetArraySize(pmenu->menu_pages);
}

/// @brief          - get_name_of_one_menu
/// @param pmenu    - menu instance pointer
/// @return         - menu name ptr
char* get_name_of_one_menu(MENU_INS* pmenu)
{
    if( pmenu == NULL ) return NULL;
    return pmenu->menu_name;
}

/// @brief          - get_name_of_one_menu_config
/// @param pmenu    - menu instance pointer
/// @return         - menu config name ptr
char* get_name_of_one_menu_config(MENU_INS* pmenu)
{
    if( pmenu == NULL ) return NULL;
    return pmenu->menu_cname;
}

void page_switch_start(const char* pagename);
/// @brief              - switch_one_menu_page
/// @param pmenu        - menu instance pointer
/// @param page_name    - page name
/// @return             - NULL - switch error, otherwise return the page object
lv_obj_t* switch_one_menu_page(MENU_INS* pmenu, const char* page_name)
{
    
    PAGE_INS* cur_page = get_menu_page_obj(pmenu, page_name);
    
    if( cur_page != NULL )
    {
    	if(API_PublicInfo_Read_Bool("OnInstaller")){
            printf("switch_one_menu_page 111111111111 \n");
            MainHeaderHide();
		#ifdef PID_IX850
            Main_Header_Reset();
		#endif
        }           
        else if(strcmp(page_name,"main")==0||strcmp(page_name,"call")==0)
                MainHeaderShow();
        else
            MainHeaderHide();
	
        page_switch_start(page_name);
        vtk_lvgl_lock();
        lv_menu_set_page(pmenu->menu_root, cur_page->page_obj);
        API_PublicInfo_Write_String("cur_page",page_name);
        vtk_lvgl_unlock();
        cur_page->page_owner = pmenu;
        return cur_page->page_obj;
    }
    else
        return NULL;
}

/// @brief           - is_active_page_of_one_menu
/// @param pmenu     - menu instance pointer 
/// @param page_name - page name
/// @return          - true: IS ACTIVE, flase: IS NOT ACTIVE   
bool is_active_page_of_one_menu(MENU_INS* pmenu, const char* page_name)
{
    PAGE_INS* ppage = get_menu_page_obj(pmenu, page_name);
    if( ppage != NULL )
    {
        if( lv_menu_get_cur_main_page(pmenu->menu_root) == ppage->page_obj )
            return true;
        else
            return false;
    }
    else
        return false;
}

/*-----------------------------------------------------------------------------------------------*/

/// @brief                      - menu_config_parse
/// @param menu_config_path     - menu config file path
/// @return                     - pages array, json format, user should delete the json object
char* add_path(const char* filename)
{
    char strbuf1[250];
	char strbuf2[250];
    char* menu_config_path;
    // search and match
    sprintf(strbuf1,"%s/%s", MENU_CONFIG_CUS_PATH, filename);
    sprintf(strbuf2,"%s/%s", MENU_CONFIG_FIX_PATH, filename);
    if( access(strbuf1, F_OK) == 0 )
        menu_config_path = strbuf1;
    else if( access(strbuf2, F_OK) != 0 )
        menu_config_path = strbuf2;
    else
    {
        printf("HAVE NO THIS MENU CONFIG FILE!!!\n");
        return NULL;
    }
}
