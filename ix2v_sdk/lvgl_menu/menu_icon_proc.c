#include "menu_icon_proc.h"
#include "ix850_icon.h"
#include "ix850_info.h"
#include "MainList.h"
#include "MainPage_code.h"
#include "MainPage_digtal.h"
#include "main_header.h"

extern const lv_font_t* font_small;
extern const lv_font_t* font_medium;
extern const lv_font_t* font_larger;

extern lv_style_t style_icon_callbtn;
extern lv_style_t style_icon_function;
extern lv_style_t style_menu_cont;
extern lv_style_t style_menu_item_text;
extern lv_style_t style_menu_item_text_checked;
extern lv_style_t style_menu_item_btn;

void* icon_create_IMG_CallBtn(lv_obj_t* parent, struct ICON_INS* picon);
void* icon_create_IMG_Icon(lv_obj_t* parent, struct ICON_INS* picon);
void* icon_create_IMG_Label(lv_obj_t* parent, struct ICON_INS* picon);
void* icon_create_ITEM_text(lv_obj_t* parent, struct ICON_INS* picon);
void* icon_create_ITEM_switch(lv_obj_t* parent, struct ICON_INS* picon);
void* icon_create_ITEM_btn(lv_obj_t* parent, struct ICON_INS* picon);
void* icon_create_ITEM_ssid(lv_obj_t* parent, struct ICON_INS* picon);

cJSON* recursion_cJSON_GetObjectItemCaseSensitive(const cJSON* psource, const char* name);

const ICON_CREATE_CB_T ICON_CREAT_CB_TABLE[] =
{     
#if !defined( PID_IX622 )	&& !defined( PID_IX611 ) && !defined( PID_IX821 )	    
    {"IMG_Label",       img_creat_label,},    
    {"IMG_Icon",        img_creat_fcon,},
    {"IMG_CodeBtn",     Img_add_codebtn,},
    {"IMG_CallBtn",     Img_add_callbtn,},
    {"IMG_List",        Img_add_list,},
    {"IMG_Btn",         Img_add_btn,},
    {"IMG_Header",      Img_add_header,},
    {"IMG_Password",    Img_add_password,},
    {"IMG_Call",        img_creat_callbtn,},
    {"IMG_Info",        img_creat_info,}, 
#endif    
};

lv_obj_t* display_one_icon(lv_obj_t* parent, struct ICON_INS* picon)
{
    int i, size;
    cJSON* piconjson = recursion_cJSON_GetObjectItemCaseSensitive(picon->icon_data, get_global_key_string(GLOBAL_ICON_KEYS_TYPE));
    size = sizeof(ICON_CREAT_CB_TABLE) / sizeof(ICON_CREATE_CB_T);
    for (i = 0; i < size; i++)
    {        
        if (strcmp(ICON_CREAT_CB_TABLE[i].type, piconjson->valuestring) == 0)
        {
        	//if(picon->icon_data)
        	//	printf_json(picon->icon_data,piconjson->valuestring);
        	//printf("%d,display_one_icon type:%s\n",i,ICON_CREAT_CB_TABLE[i].type);	
            return (*ICON_CREAT_CB_TABLE[i].cb)(parent, picon);
        }
    }
    return NULL;
}


static void filter_click_cb(lv_event_t* e)
{
    MENU_INS* pmenu;
    PAGE_INS* target_page;
    cJSON* plink;
    cJSON* preturn;
    lv_event_code_t code = lv_event_get_code(e);
    lv_obj_t* ta = lv_event_get_target(e);
    struct ICON_INS* picon = lv_event_get_user_data(e);
    if (code == LV_EVENT_CLICKED)
    {
    	printf_json(picon->icon_data,__func__);
        if (picon->icon_data)
        {
            // sidebar mode: from "NOT setting menu" link to "setting menu"
            plink = recursion_cJSON_GetObjectItemCaseSensitive(picon->icon_data, get_global_key_string(GLOBAL_ICON_KEYS_LINK));
            if (plink)
            {
                if (picon->icon_owner->page_type != MENU_TYPE_SETTING)
                {
                    pmenu = picon->icon_owner->page_owner;
                    target_page = get_menu_page_obj(pmenu, plink->valuestring);
                    if ( (target_page != 0) && (target_page->page_type == MENU_TYPE_SETTING) && target_page->page_sidebar )
                    {
                        lv_menu_set_page(pmenu->menu_root, NULL);
                        lv_menu_set_sidebar_page(pmenu->menu_root, get_menu_page_obj(pmenu, plink->valuestring)->page_obj);
                        lv_event_send(lv_obj_get_child(lv_obj_get_child(lv_menu_get_cur_sidebar_page(pmenu->menu_root), 1), 0), LV_EVENT_CLICKED, NULL);
                    }
                }
            }
            // sidebar mode: from "setting menu"/"NOT setting menu" return to "NOT setting menu"
            preturn = recursion_cJSON_GetObjectItemCaseSensitive(picon->icon_data, get_global_key_string(GOLBAL_ICON_KEYS_RETURN));
            if (preturn)
            {
                // from "setting menu" return to "NOT setting menu"
                if (preturn->valueint == 0)
                {
                    pmenu = picon->icon_owner->page_owner;
                    if( pmenu )
                    {
                        lv_menu_set_sidebar_page(pmenu->menu_root, NULL);
                        lv_menu_clear_history(pmenu->menu_root);
                        PAGE_INS* root_page = get_menu_page_obj(pmenu, COMMON_MENU_ENTRY_PAGE);
                        if( root_page ) lv_menu_set_page(pmenu->menu_root, root_page->page_obj);
                    }
                }
                // from "NOT setting menu" return to "NOT setting menu"
                else  if(preturn->valueint == 1)
                {
                    pmenu = picon->icon_owner->page_owner;
                    lv_event_send(lv_obj_get_child(lv_menu_get_main_header(pmenu->menu_root), 0), LV_EVENT_CLICKED, NULL);
                }
            }
        }
    }
}

/*
function:  ͨ��struct ICON_INS�ṹ����һ��icon���������ϣ�������icon�������úûص�����������Ϊicon id������
para1��parent - icon������
Para2�� picon -icon��ʵ��
return��icon����
*/

void* icon_create_IMG_CallBtn(lv_obj_t* parent, struct ICON_INS* picon)
{
    cJSON* x;
    cJSON* y;
    cJSON* w;
    cJSON* h;
    cJSON* t;
    cJSON* c;
    cJSON* s;

    x = recursion_cJSON_GetObjectItemCaseSensitive(picon->icon_data, get_global_key_string(GLOBAL_ICON_KEYS_POS_X));
    y = recursion_cJSON_GetObjectItemCaseSensitive(picon->icon_data, get_global_key_string(GLOBAL_ICON_KEYS_POS_Y));
    w = recursion_cJSON_GetObjectItemCaseSensitive(picon->icon_data, get_global_key_string(GLOBAL_ICON_KEYS_WIDTH));
    h = recursion_cJSON_GetObjectItemCaseSensitive(picon->icon_data, get_global_key_string(GLOBAL_ICON_KEYS_HEIGHT));
    t = recursion_cJSON_GetObjectItemCaseSensitive(picon->icon_data, get_global_key_string(GLOBAL_ICON_KEYS_LABEL_TEXT));
    c = recursion_cJSON_GetObjectItemCaseSensitive(picon->icon_data, get_global_key_string(GLOBAL_ICON_KEYS_TEXT_COLOR));
    s = recursion_cJSON_GetObjectItemCaseSensitive(picon->icon_data, get_global_key_string(GLOBAL_ICON_KEYS_TEXT_SIZE));

    // create button
    picon->icon_cont = lv_btn_usr1_create(parent);
    lv_obj_set_pos(picon->icon_cont, x->valueint, y->valueint);
    lv_obj_set_size(picon->icon_cont, w->valueint, h->valueint);

    // create text
    lv_obj_t* label = lv_label_create(picon->icon_cont);
    if (s->valueint == 0)
        lv_obj_set_style_text_font(label, font_small, 0);
    else if (s->valueint == 1)
        lv_obj_set_style_text_font(label, font_medium, 0);
    else
        lv_obj_set_style_text_font(label, font_larger, 0);
    lv_obj_set_style_text_color(label, lv_color_hex(c->valueint), 0);
    lv_label_set_text(label, t->valuestring);
    lv_obj_center(label);

    // add event cb
    lv_obj_add_event_cb(picon->icon_cont, filter_click_cb, LV_EVENT_CLICKED, picon);
    return NULL;
}

void* icon_create_IMG_Icon(lv_obj_t* parent, struct ICON_INS* picon)
{
    cJSON* x;
    cJSON* y;
    cJSON* w;
    cJSON* h;
    cJSON* t;
    cJSON* c;
    cJSON* s;
    cJSON* f;
    x = recursion_cJSON_GetObjectItemCaseSensitive(picon->icon_data, get_global_key_string(GLOBAL_ICON_KEYS_POS_X));
    y = recursion_cJSON_GetObjectItemCaseSensitive(picon->icon_data, get_global_key_string(GLOBAL_ICON_KEYS_POS_Y));
    w = recursion_cJSON_GetObjectItemCaseSensitive(picon->icon_data, get_global_key_string(GLOBAL_ICON_KEYS_WIDTH));
    h = recursion_cJSON_GetObjectItemCaseSensitive(picon->icon_data, get_global_key_string(GLOBAL_ICON_KEYS_HEIGHT));
    t = recursion_cJSON_GetObjectItemCaseSensitive(picon->icon_data, get_global_key_string(GLOBAL_ICON_KEYS_LABEL_TEXT));
    c = recursion_cJSON_GetObjectItemCaseSensitive(picon->icon_data, get_global_key_string(GLOBAL_ICON_KEYS_TEXT_COLOR));
    s = recursion_cJSON_GetObjectItemCaseSensitive(picon->icon_data, get_global_key_string(GLOBAL_ICON_KEYS_TEXT_SIZE));
    f = recursion_cJSON_GetObjectItemCaseSensitive(picon->icon_data, get_global_key_string(GLOBAL_ICON_KEYS_FUNCTION));

    // create button
    picon->icon_cont = lv_btn_usr1_create(parent);
    lv_obj_set_pos(picon->icon_cont, x->valueint, y->valueint);
    lv_obj_set_size(picon->icon_cont, w->valueint, h->valueint);

    if (!recursion_cJSON_GetObjectItemCaseSensitive(picon->icon_data, get_global_key_string(GLOBAL_ICON_KEYS_LINK)))
    {
        lv_obj_add_event_cb(picon->icon_cont, filter_click_cb, LV_EVENT_CLICKED, picon);
    }

	#if 0
	creat_icon_for_type(parent,picon);
	#endif

	#if defined(PID_IX850)
        img_creat_fcon(parent, picon);
    #endif

    return NULL;
}

void* icon_create_IMG_Label(lv_obj_t* parent, struct ICON_INS* picon)
{
    cJSON* x;
    cJSON* y;
    cJSON* w;
    cJSON* h;
    cJSON* t;
    cJSON* c;
    cJSON* s;

    x = recursion_cJSON_GetObjectItemCaseSensitive(picon->icon_data, get_global_key_string(GLOBAL_ICON_KEYS_POS_X));
    y = recursion_cJSON_GetObjectItemCaseSensitive(picon->icon_data, get_global_key_string(GLOBAL_ICON_KEYS_POS_Y));
    w = recursion_cJSON_GetObjectItemCaseSensitive(picon->icon_data, get_global_key_string(GLOBAL_ICON_KEYS_WIDTH));
    h = recursion_cJSON_GetObjectItemCaseSensitive(picon->icon_data, get_global_key_string(GLOBAL_ICON_KEYS_HEIGHT));
    t = recursion_cJSON_GetObjectItemCaseSensitive(picon->icon_data, get_global_key_string(GLOBAL_ICON_KEYS_LABEL_TEXT));
    c = recursion_cJSON_GetObjectItemCaseSensitive(picon->icon_data, get_global_key_string(GLOBAL_ICON_KEYS_TEXT_COLOR));
    s = recursion_cJSON_GetObjectItemCaseSensitive(picon->icon_data, get_global_key_string(GLOBAL_ICON_KEYS_TEXT_SIZE));

    // create label
    picon->icon_cont = lv_label_create(parent);
    lv_obj_set_pos(picon->icon_cont, x->valueint, y->valueint);
    lv_obj_set_size(picon->icon_cont, w->valueint, h->valueint);

    if (s->valueint == 0)
        lv_obj_set_style_text_font(picon->icon_cont, font_small, 0);
    else if (s->valueint == 1)
        lv_obj_set_style_text_font(picon->icon_cont, font_medium, 0);
    else
        lv_obj_set_style_text_font(picon->icon_cont, font_larger, 0);
    lv_obj_set_style_text_color(picon->icon_cont, lv_color_hex(c->valueint), 0);
    lv_label_set_text(picon->icon_cont, t->valuestring);
    lv_obj_set_style_text_align(picon->icon_cont, LV_TEXT_ALIGN_CENTER, 0);

    return NULL;
}

void* icon_create_ITEM_text(lv_obj_t* parent, struct ICON_INS* picon)
{
    cJSON* x;
    cJSON* y;
    cJSON* w;
    cJSON* h;
    cJSON* t;
    cJSON* c;
    cJSON* s;

    x = recursion_cJSON_GetObjectItemCaseSensitive(picon->icon_data, get_global_key_string(GLOBAL_ICON_KEYS_POS_X));
    y = recursion_cJSON_GetObjectItemCaseSensitive(picon->icon_data, get_global_key_string(GLOBAL_ICON_KEYS_POS_Y));
    w = recursion_cJSON_GetObjectItemCaseSensitive(picon->icon_data, get_global_key_string(GLOBAL_ICON_KEYS_WIDTH));
    h = recursion_cJSON_GetObjectItemCaseSensitive(picon->icon_data, get_global_key_string(GLOBAL_ICON_KEYS_HEIGHT));
    t = recursion_cJSON_GetObjectItemCaseSensitive(picon->icon_data, get_global_key_string(GLOBAL_ICON_KEYS_LABEL_TEXT));
    c = recursion_cJSON_GetObjectItemCaseSensitive(picon->icon_data, get_global_key_string(GLOBAL_ICON_KEYS_TEXT_COLOR));
    s = recursion_cJSON_GetObjectItemCaseSensitive(picon->icon_data, get_global_key_string(GLOBAL_ICON_KEYS_TEXT_SIZE));

    picon->icon_type = 5;
    lv_obj_t* menu_cont = lv_menu_cont_create(parent);
    lv_obj_add_style(menu_cont, &style_menu_cont, 0);
    // create cont
    picon->icon_cont = lv_obj_create(menu_cont);
    lv_obj_set_size(picon->icon_cont, LV_PCT(100), 80);
    lv_obj_clear_flag(picon->icon_cont, LV_OBJ_FLAG_SCROLLABLE);
    lv_obj_add_style(picon->icon_cont, &style_menu_item_text, 0);
    lv_obj_add_style(picon->icon_cont, &style_menu_item_text_checked, LV_STATE_CHECKED);
    // create label
    lv_obj_t* obj_cont = lv_label_create(picon->icon_cont);
    if (s->valueint == 0)
        lv_obj_set_style_text_font(obj_cont, font_small, 0);
    else if (s->valueint == 1)
        lv_obj_set_style_text_font(obj_cont, font_medium, 0);
    else
        lv_obj_set_style_text_font(obj_cont, font_larger, 0);
    lv_obj_set_style_text_color(obj_cont, lv_color_hex(c->valueint), 0);
    lv_label_set_text(obj_cont, t->valuestring);
    lv_obj_align(obj_cont, LV_ALIGN_LEFT_MID, 60, 0);

    if (!recursion_cJSON_GetObjectItemCaseSensitive(picon->icon_data, get_global_key_string(GLOBAL_ICON_KEYS_LINK)))
    {
        lv_obj_add_event_cb(picon->icon_cont, filter_click_cb, LV_EVENT_CLICKED, picon);
    }
    return NULL;
}

void* icon_create_ITEM_switch(lv_obj_t* parent, struct ICON_INS* picon)
{
    cJSON* x;
    cJSON* y;
    cJSON* w;
    cJSON* h;
    cJSON* t;
    cJSON* c;
    cJSON* s;

    x = recursion_cJSON_GetObjectItemCaseSensitive(picon->icon_data, get_global_key_string(GLOBAL_ICON_KEYS_POS_X));
    y = recursion_cJSON_GetObjectItemCaseSensitive(picon->icon_data, get_global_key_string(GLOBAL_ICON_KEYS_POS_Y));
    w = recursion_cJSON_GetObjectItemCaseSensitive(picon->icon_data, get_global_key_string(GLOBAL_ICON_KEYS_WIDTH));
    h = recursion_cJSON_GetObjectItemCaseSensitive(picon->icon_data, get_global_key_string(GLOBAL_ICON_KEYS_HEIGHT));
    t = recursion_cJSON_GetObjectItemCaseSensitive(picon->icon_data, get_global_key_string(GLOBAL_ICON_KEYS_LABEL_TEXT));
    c = recursion_cJSON_GetObjectItemCaseSensitive(picon->icon_data, get_global_key_string(GLOBAL_ICON_KEYS_TEXT_COLOR));
    s = recursion_cJSON_GetObjectItemCaseSensitive(picon->icon_data, get_global_key_string(GLOBAL_ICON_KEYS_TEXT_SIZE));

    // create menu cont
    picon->icon_cont = lv_menu_cont_create(parent);
    lv_obj_add_style(picon->icon_cont, &style_menu_cont, 0);

    lv_obj_t* obj_cont = lv_obj_create(picon->icon_cont);
    lv_obj_set_size(obj_cont, LV_PCT(100), 80);
    lv_obj_clear_flag(obj_cont, LV_OBJ_FLAG_SCROLLABLE);
    lv_obj_add_style(obj_cont, &style_menu_item_text, 0);
    //label
    lv_obj_t* label = lv_label_create(obj_cont);
    if (s->valueint == 0)
        lv_obj_set_style_text_font(label, font_small, 0);
    else if (s->valueint == 1)
        lv_obj_set_style_text_font(label, font_medium, 0);
    else
        lv_obj_set_style_text_font(label, font_larger, 0);
    lv_obj_set_style_text_color(label, lv_color_hex(c->valueint), 0);
    lv_label_set_text(label, t->valuestring);
    lv_obj_align(label, LV_ALIGN_LEFT_MID, 60, 0);
    // switch
    lv_obj_t* switch_obj = lv_switch_create(obj_cont);
    lv_obj_align_to(switch_obj, label, LV_ALIGN_OUT_LEFT_MID, LV_HOR_RES - 100, 0);

    return NULL;
}

void* icon_create_ITEM_btn(lv_obj_t* parent, struct ICON_INS* picon)
{
    cJSON* x;
    cJSON* y;
    cJSON* w;
    cJSON* h;
    cJSON* t;
    cJSON* c;
    cJSON* s;

    x = recursion_cJSON_GetObjectItemCaseSensitive(picon->icon_data, get_global_key_string(GLOBAL_ICON_KEYS_POS_X));
    y = recursion_cJSON_GetObjectItemCaseSensitive(picon->icon_data, get_global_key_string(GLOBAL_ICON_KEYS_POS_Y));
    w = recursion_cJSON_GetObjectItemCaseSensitive(picon->icon_data, get_global_key_string(GLOBAL_ICON_KEYS_WIDTH));
    h = recursion_cJSON_GetObjectItemCaseSensitive(picon->icon_data, get_global_key_string(GLOBAL_ICON_KEYS_HEIGHT));
    t = recursion_cJSON_GetObjectItemCaseSensitive(picon->icon_data, get_global_key_string(GLOBAL_ICON_KEYS_LABEL_TEXT));
    c = recursion_cJSON_GetObjectItemCaseSensitive(picon->icon_data, get_global_key_string(GLOBAL_ICON_KEYS_TEXT_COLOR));
    s = recursion_cJSON_GetObjectItemCaseSensitive(picon->icon_data, get_global_key_string(GLOBAL_ICON_KEYS_TEXT_SIZE));

    picon->icon_type = 6;
    lv_obj_t* menu_cont = lv_menu_cont_create(parent);
    lv_obj_add_style(menu_cont, &style_menu_cont, 0);
    //
    picon->icon_cont = lv_btn_create(menu_cont);
    //if( w && h )
    //    lv_obj_set_size(confimBtn, w->valueint, h->valueint);
    //else
    lv_obj_set_size(picon->icon_cont, LV_PCT(100), 80);
    lv_obj_add_style(picon->icon_cont, &style_menu_item_btn, 0);

    lv_obj_t* label = lv_label_create(picon->icon_cont);
    if (s->valueint == 0)
        lv_obj_set_style_text_font(label, font_small, 0);
    else if (s->valueint == 1)
        lv_obj_set_style_text_font(label, font_medium, 0);
    else
        lv_obj_set_style_text_font(label, font_larger, 0);
    lv_obj_set_style_text_color(label, lv_color_hex(c->valueint), 0);
    lv_label_set_text(label, t->valuestring);
    lv_obj_align(label, LV_ALIGN_LEFT_MID, 60, 0);
    //lv_obj_center(label);

    if (!recursion_cJSON_GetObjectItemCaseSensitive(picon->icon_data, get_global_key_string(GLOBAL_ICON_KEYS_LINK)))
    {
        lv_obj_add_event_cb(picon->icon_cont, filter_click_cb, LV_EVENT_CLICKED, picon);
    }
    return NULL;
}

void* icon_create_ITEM_ssid(lv_obj_t* parent, struct ICON_INS* picon)
{
    cJSON* x;
    cJSON* y;
    cJSON* w;
    cJSON* h;
    cJSON* t;
    cJSON* c;
    cJSON* s;

    x = recursion_cJSON_GetObjectItemCaseSensitive(picon->icon_data, get_global_key_string(GLOBAL_ICON_KEYS_POS_X));
    y = recursion_cJSON_GetObjectItemCaseSensitive(picon->icon_data, get_global_key_string(GLOBAL_ICON_KEYS_POS_Y));
    w = recursion_cJSON_GetObjectItemCaseSensitive(picon->icon_data, get_global_key_string(GLOBAL_ICON_KEYS_WIDTH));
    h = recursion_cJSON_GetObjectItemCaseSensitive(picon->icon_data, get_global_key_string(GLOBAL_ICON_KEYS_HEIGHT));
    t = recursion_cJSON_GetObjectItemCaseSensitive(picon->icon_data, get_global_key_string(GLOBAL_ICON_KEYS_LABEL_TEXT));
    c = recursion_cJSON_GetObjectItemCaseSensitive(picon->icon_data, get_global_key_string(GLOBAL_ICON_KEYS_TEXT_COLOR));
    s = recursion_cJSON_GetObjectItemCaseSensitive(picon->icon_data, get_global_key_string(GLOBAL_ICON_KEYS_TEXT_SIZE));

    return NULL;
}
