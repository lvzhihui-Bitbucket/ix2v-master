﻿#ifndef LV_DEMO_MSG_H
#define LV_DEMO_MSG_H

#include "lv_ix850.h"
typedef void (*MsgBoxCb)(void*);

lv_timer_t* API_TIPS(const char* string);
void API_TIPS_Close(lv_timer_t* timer);

static void msg_timer_cb(lv_timer_t* timer);
static void msg_click_event_cb(lv_event_t* e);
static void msg_close(lv_obj_t* obj, lv_timer_t* timer);
lv_timer_t* LV_API_TIPS(const char* string,int type);
lv_timer_t* LV_API_TIPS_Prefix(const char* string, char* data,int type);
lv_timer_t* LV_API_TIPS_Time(const char* string,int ts,int type);
void API_MSGBOX(const char* string, int type, MsgBoxCb callback, void*data);
#endif
