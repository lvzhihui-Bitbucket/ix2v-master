#include "menu_common.h"
#include "icon_common.h"
#include "obj_lvgl_msg.h"

extern lv_obj_t* maincont;
extern lv_obj_t* digitalCont;
extern lv_obj_t* codeCont;
extern lv_obj_t* roomCont;
extern lv_obj_t* nameCont;

void ix850_msg_init(void);
int ShellCmd_IX850_test(cJSON *cmd);
void lock_control(void* s, lv_msg_t* m);
void STD_mode_set_layout(void);
MENU_INS* mainins;
void MainMemu_Pressed_event_cb(lv_event_t* e);

lv_obj_t* load_Main_menu(const char* res_path, lv_obj_t* screen)
{

    if (screen == NULL)
        return NULL;  
 
    if (mainins != NULL){
        dele_one_menu(mainins);       
        mainins = NULL; 
    }   
    
    global_style_init();

    //mainins = load_one_menu(res_path, screen);
    

    //if(IX850nameToLVObj != NULL)
    //{
        //cJSON_Delete(IX850nameToLVObj);
       //IX850nameToLVObj = NULL;
    //}


    mainins = load_one_menu(res_path, screen);
    int count = cJSON_GetArraySize(mainins->menu_pages);
    for (int i = 0; i < count; i++)
    {
        cJSON* piconjson = cJSON_GetArrayItem(mainins->menu_pages, i);
        PAGE_INS* ppage = (PAGE_INS*)piconjson->valueint;
        maincont = ppage->page_obj;
        //printf("load_Main_menu 11111111111111111111111111 res_file = %s\n",ppage->res_file);
        
    }    
    //STD_mode_set_layout();

	AddOneMenuCommonClickedCb(screen,MainMemu_Pressed_event_cb);
    
	return mainins->menu_root;
}

void STD_mode_set_layout(void)
{
}

int ShellCmd_IX850_test(cJSON *cmd)
{
	char* jsonstr = GetShellCmdInputString(cmd, 1);
	char *qr_str;
	char acc[40];
	char pwd[40];
    if( jsonstr != NULL )
    {
        switch( *jsonstr )
        {
            case '0':
                //restart_menu();
                printf("restart successful \n");
                break;
            case '1':

                break;
            case '2':

                break;
        }
    }
    else
    {
        ShellCmdPrintf("ShellCmd_IX850_test para err!\n");
    }
}

void lock_control(void* s, lv_msg_t* m)
{
    LV_UNUSED(s);
    cJSON* number = NULL;
    char* result = NULL;
    switch (lv_msg_get_id(m)) {
    case IX850_MSG_LOCK:
	 result = (char*)lv_msg_get_payload(m);	
	 if(strcmp(result,"Unlock1")==0)
        LV_API_TIPS("THE LOCK1 IS OPEN",3);
        //API_TIPS("THE LOCK1 IS OPEN");

    // lzh_20240827_s
	 else if(strcmp(result,"REMOTE_UNLOCK_OK")==0)
        LV_API_TIPS("REMOTE UNLOCK OK",10);
	 else if(strcmp(result,"REMOTE_UNLOCK_DENY")==0)
        LV_API_TIPS("REMOTE UNLOCK DENY",11);
	 else if(strcmp(result,"REMOTE_UNLOCK_ER")==0)
        LV_API_TIPS("REMOTE UNLOCK ER",12);
    // lzh_20240827_e

	 else
        LV_API_TIPS("THE LOCK2 IS OPEN",3);
	 	//API_TIPS("THE LOCK2 IS OPEN");
        break;
    case IX850_MSG_CALL:
        number = lv_msg_get_payload(m);

        //char* a = cJSON_Print(number);

       // printf("lock_control 111111111111111 %s\n", a);

        //Api_LvCallBusiness_StartCall(number);
	 MainMenu_Process(MAINMENU_MAIN_StartCall,number);
        break;
    case IX850_MSG_UNCALL:

        Api_LvCallBusiness_CancelCall();

        break;
    }
}

