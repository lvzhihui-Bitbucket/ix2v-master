#ifndef _MENUWIFI_H_
#define _MENUWIFI_H_

#include "lv_ix850.h"
#include "KeyboardResource.h"

typedef struct 
{    
    lv_obj_t* back_scr;
    lv_obj_t* scr;
    lv_obj_t* menu;        
    lv_obj_t* table;        
    cJSON* view;
    lv_obj_t* curLabel;  
    int value;
    int cnt_row;
    void* msg0;
    void* msg1;
    VTK_KB_S* kb;
}WIFI_VIEW;

static void lv_msg_wifi_handler(void* s, lv_msg_t* m);

void Menu_Wifi_Init(void);
void Wifi_MenuDelete(WIFI_VIEW** rc);
#endif 