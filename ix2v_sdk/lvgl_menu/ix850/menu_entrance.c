#include "lv_ix850.h"
#include "time.h"
#include "obj_lvgl_msg.h"
#include "task_Beeper.h"
#include "lvgl.h"
#include "obj_IoInterface.h"

void menu_initial()
{ 
    font_init();
	lvgl_msg_init();
    //menu_config_load("MenuPage_Config");
    MainMenu_Init();
    //ui_init();
	ScrBr_Reset_Time();
    //ui_call_init();
    
}

static int IfObjectHasCallback(lv_obj_t * obj, lv_event_code_t filter)
{
    int ret = 0;
    lv_obj_t * parentObj;

    for(parentObj = obj; !ret && parentObj; parentObj = lv_obj_get_parent(parentObj))
    {
        if(parentObj && parentObj->spec_attr)
        {
            for(int i = 0; !ret && i < parentObj->spec_attr->event_dsc_cnt; i++)
            {
                if((parentObj->spec_attr->event_dsc[i].filter == filter) && parentObj->spec_attr->event_dsc[i].cb)
                {
                    ret = 1;
                    break;
                }
            }
        }
    }
    return ret;
}
static int KeyBeepEn=-1;
void UpdateKeyBeepEn(void)
{
	if(API_Para_Read_Int(KeyBeepDis))
		KeyBeepEn=0;
	else
		KeyBeepEn=1;
}
int IfKeyBeep(void)
{
	if(KeyBeepEn<0)
	{
		UpdateKeyBeepEn();
	}
	return KeyBeepEn;
}
void btn_Pressed_handle(void* s, lv_msg_t* m)
{
    int id = lv_msg_get_id(m);
    lv_obj_t * obj = lv_msg_get_payload(m);
    //if(id == MENU_CLICK && (IfObjectHasCallback(obj, LV_EVENT_CLICKED) || IfObjectHasCallback(obj, LV_EVENT_VALUE_CHANGED)))
    if(IfKeyBeep())
    {
        BEEP_KEY();
    }
}
