#include "Common_TableView.h"
#include "define_file.h"
#include "utility.h"
#include "task_Event.h"
#include "obj_TableSurver.h"
#include "task_Beeper.h"
#include "lv_demo_msg.h"
#include "obj_CommonSelectMenu.h"
#include "obj_PublicInformation.h"

typedef struct 
{   
    char* satFlag;
	char* satFolder;
} SATFLAG_TO_FOLDER;

#define SAT_SELECT_PARA		"{\"HeaderEnable\":true,\"CellCallback\":0,\"ColumnWidth\":[360, 120, 360]}"
#define SAT_SELECT_FIELD_NAME			"NAME"
#define SAT_SELECT_FIELD_DESCRIBE		"DESCRIBE"
#define SAT_SELECT_FIELD_SOURCE			"SOURCE"

static TB_VIWE* satSelect = NULL;
static const SATFLAG_TO_FOLDER SAT_FOLDER[] = {
	{"CUR", TB_HYBRID_SAT_FILE_NAME},
	{"FW", Factory_HYBRID_SAT_Folder},
	{"CUS", Customerized_HYBRID_SAT_Folder},
	{"BAK", Backup_HYBRID_SAT_Folder},
	{"SD Card", SDCARD_HYBRID_SAT_Folder},
};

static const int SAT_FOLDER_NUM = sizeof(SAT_FOLDER)/sizeof(SAT_FOLDER[0]);

static void SatSelectFilterCallback(const cJSON* filter);
static void FunctionClick(const char* funcName);
void MenuSAT_HYBRID_Select_Close(void);

static const  char* FlagToFolder(const char *flag)
{
	int i;
	for(i = 0; i < SAT_FOLDER_NUM; i++)
	{
		if(flag && SAT_FOLDER[i].satFlag && !strcmp(flag, SAT_FOLDER[i].satFlag))
		{
			return SAT_FOLDER[i].satFolder;
		}
	}
    return NULL;
}

static int StrcmpEnd(const char *str1, const char *str2)
{
	int ret = 0;
	if(str1 == str2)
	{
		return ret;
	}

	if(str1 && str2)
	{
		int start1 = strlen(str1);
		int start2 = strlen(str2);
	
		while (start1 >= 0 && start2 >= 0)
		{
			if (str1[start1] != str2[start2])
			{
				ret = str1[start1] - str2[start2];
				return ret;
			}
			start1--;
			start2--;
		}
	}
	else
	{
		ret = (str1 ? 1 : -1);
		return ret;
	}

    return ret;
}

static void SatUpdate(void* data)
{
	int* pLine = (int*)data;
	if(satSelect)
	{
		
		cJSON* record = cJSON_GetArrayItem(satSelect->view, *pLine);
		if(cJSON_IsObject(record))
		{
			char temp[500];
			char* fileName = GetEventItemString(record,  SAT_SELECT_FIELD_NAME);
			char* describe = GetEventItemString(record,  SAT_SELECT_FIELD_DESCRIBE);
			char* source = GetEventItemString(record,  SAT_SELECT_FIELD_SOURCE);
			char* path = FlagToFolder(source);
			int fileType = CheckFileType(path);
			if(fileType == 1)
			{
				snprintf(temp, 500, "%s/%s", path, fileName);
			}
			else if(fileType == 2)
			{
				snprintf(temp, 500, "%s", path);
			}
			else
			{
				API_TIPS_Ext_Time("Update error", 1);
				return;
			}

			if(source && !strcmp(source, "SD Card"))
			{
				MakeDir(FlagToFolder("CUS"));
				CopyFile(temp, FlagToFolder("CUS"));
			}

			CopyFile(temp, FlagToFolder("CUR"));
			API_TB_ReloadByName(TB_NAME_HYBRID_SAT,NULL);
			snprintf(temp, 500, "%s update", ((describe && describe[0]) ? describe : fileName));
			API_TIPS_Ext_Time(temp, 1);
		}
		MenuSAT_HYBRID_Select_Close();
	}
}

static void SatCellClick(int line, int col)
{
    //dprintf("line = %d, col =%d\n", line, col);
	if(satSelect)
	{
		cJSON* record = cJSON_GetArrayItem(satSelect->view, line);
		if(cJSON_IsObject(record))
		{
			char* source = GetEventItemString(record,  SAT_SELECT_FIELD_SOURCE);
			if(source && !strcmp(source, "CUR"))
			{
				TB_MenuDelete(&satSelect);
			}
			else
			{
				char temp[500];
				static int clickLine;
				clickLine = line;
				char* fileName = GetEventItemString(record,  SAT_SELECT_FIELD_NAME);
				char* describe = GetEventItemString(record,  SAT_SELECT_FIELD_DESCRIBE);
				snprintf(temp, 500, "%s update", ((describe && describe[0]) ? describe : fileName));
				API_MSGBOX(temp, 2, SatUpdate, &clickLine);
			}
		}
	}
}


static cJSON* SatSelectCreateView(const char* satFlag)
{   
    cJSON* view = cJSON_CreateArray();
	if(!satFlag)
	{
		return view;
	}

	int folderIndex;
	for(folderIndex = 0; folderIndex < SAT_FOLDER_NUM; folderIndex++)
	{
		if(!strcmp(satFlag, "ALL") || !strcmp(satFlag, SAT_FOLDER[folderIndex].satFlag))
		{
			cJSON* file;
			cJSON* fileList = cJSON_CreateArray();	
			int fileType = CheckFileType(SAT_FOLDER[folderIndex].satFolder);
			if(fileType == 1)
			{
				GetFileAndDirList(SAT_FOLDER[folderIndex].satFolder, fileList, 0);
			}
			else if(fileType == 2)
			{
				char* fileName = GetFilenameFromPath(SAT_FOLDER[folderIndex].satFolder);
				file = cJSON_CreateArray();
				cJSON_AddItemToArray(file, cJSON_CreateString(fileName));
				cJSON_AddItemToArray(file, cJSON_CreateString(fileName));
				cJSON_AddItemToArray(file, cJSON_CreateString(fileName));
				cJSON_AddItemToArray(file, cJSON_CreateString(fileName));
				cJSON_AddItemToArray(fileList, file);
			}
			else
			{
				continue;
			}

			cJSON_ArrayForEach(file, fileList)
			{
				cJSON* fileName = cJSON_GetArrayItem(file, 3);
				if(cJSON_IsString(fileName))
				{
					if((!StrcmpEnd(fileName->valuestring, ".json") || !StrcmpEnd(fileName->valuestring, ".JSON")))
					{
						char temp[500];
						if(fileType == 1)
						{
							snprintf(temp, 500, "%s/%s", SAT_FOLDER[folderIndex].satFolder, fileName->valuestring);
						}
						else if(fileType == 2)
						{
							snprintf(temp, 500, "%s", SAT_FOLDER[folderIndex].satFolder);
						}
						else
						{
							continue;
						}

						cJSON* sat = GetJsonFromFile(temp);
						char* disc = GetEventItemString(cJSON_GetObjectItemCaseSensitive(sat, TB_DESC), PB_SAT_DH_HYBRID_TB_DESC);
						cJSON* record = cJSON_CreateObject();
						cJSON_AddStringToObject(record, SAT_SELECT_FIELD_DESCRIBE, disc);
						cJSON_AddStringToObject(record, SAT_SELECT_FIELD_SOURCE, SAT_FOLDER[folderIndex].satFlag);
						cJSON_AddStringToObject(record, SAT_SELECT_FIELD_NAME, fileName->valuestring);
						cJSON_AddItemToArray(view, record);
						cJSON_Delete(sat);
					}
				}
			}
			cJSON_Delete(fileList);
		}
	}
	return view;
}

static void SatSelectReturnCallback(void)
{
	MenuSAT_HYBRID_Select_Close();
}

static cJSON* SatSelectCreatePara(const char* satFlag)
{   
    cJSON* fliter = cJSON_CreateObject();
    cJSON_AddStringToObject(fliter, "Name", "SAT path");
    cJSON_AddStringToObject(fliter, "Value", satFlag);
    cJSON_AddNumberToObject(fliter, "Callback", (int)SatSelectFilterCallback);
	cJSON* para = cJSON_Parse(SAT_SELECT_PARA);
    cJSON_AddItemToObject(para,"Filter", fliter);
	cJSON_ReplaceItemInObjectCaseSensitive(para, "CellCallback", cJSON_CreateNumber((int)SatCellClick));
	cJSON_AddNumberToObject(para, "ReturnCallback", (int)SatSelectReturnCallback);
	
	cJSON* func = cJSON_CreateObject();
	cJSON* funcName = cJSON_AddArrayToObject(func, "Name");
	cJSON_AddItemToArray(funcName, cJSON_CreateString("Delete BAK"));
	cJSON_AddItemToArray(funcName, cJSON_CreateString("Delete CUS"));
	cJSON_AddNumberToObject(func, "Callback", (int)FunctionClick);
	cJSON_AddItemToObject(para, "Function", func);

	return para;
}

static void SatSelectFilterProcess(const char* chose, void* userdata)
{   
    if(!chose)
    {
        return;
    }

    if(!strcmp("ALL",chose) || !strcmp("CUR",chose) || !strcmp("FW",chose) || !strcmp("BAK",chose) || !strcmp("CUS",chose) || !strcmp("SD Card",chose))
	{
        cJSON* para = SatSelectCreatePara(chose);
		cJSON* view = SatSelectCreateView(chose);
		TB_MenuDisplay(&satSelect, view, "SAT Select", para);
		cJSON_Delete(view);
		cJSON_Delete(para);
    }
}

static void SatSelectFilterCallback(const cJSON* filter)
{   
    cJSON* value = cJSON_GetObjectItem(filter,"Value");
    static CHOSE_MENU_T callback = {.callback = 0, .cbData = NULL};
	callback.callback = SatSelectFilterProcess;
	callback.cbData = NULL;
    cJSON* choseList = cJSON_CreateArray();
    cJSON_AddItemToArray(choseList, cJSON_CreateString("ALL"));
    cJSON_AddItemToArray(choseList, cJSON_CreateString("CUR"));
    cJSON_AddItemToArray(choseList, cJSON_CreateString("FW"));
    cJSON_AddItemToArray(choseList, cJSON_CreateString("BAK"));
    cJSON_AddItemToArray(choseList, cJSON_CreateString("CUS"));
    cJSON_AddItemToArray(choseList, cJSON_CreateString("SD Card"));
    char* choseString = cJSON_IsString(value) ? value->valuestring : NULL;
    CreateChooseMenu("SAT path", choseList, choseString, &callback);
    cJSON_Delete(choseList);
}


static void SatDeleteBak(void* data)
{
	DeleteFileProcess(FlagToFolder("BAK"), "*.json");
	DeleteFileProcess(FlagToFolder("BAK"), "*.JSON");
	API_TIPS_Ext_Time("Delete BAK", 1);
	TB_MenuDelete(&satSelect);
}

static void SatDeleteCus(void* data)
{
	DeleteFileProcess(FlagToFolder("CUS"), "*.json");
	DeleteFileProcess(FlagToFolder("CUS"), "*.JSON");
	API_TIPS_Ext_Time("Delete CUS", 1);
	TB_MenuDelete(&satSelect);
}

static void FunctionClick(const char* funcName)
{
	if(funcName)
	{
		if(!strcmp(funcName, "Delete BAK"))
		{
			API_MSGBOX("Delete BAK", 2, SatDeleteBak, NULL);
		}
		else if(!strcmp(funcName, "Delete CUS"))
		{
			API_MSGBOX("Delete CUS", 2, SatDeleteCus, NULL);
		}
	}
}



void MenuSAT_HYBRID_Select_Init(void)
{
	SatSelectFilterProcess("ALL", NULL);
}

void MenuSAT_HYBRID_Select_Close(void)
{
	TB_MenuDelete(&satSelect);
	API_Event_NameAndMsg(EventMenuSettingProcess, "SettingRefresh", NULL);
}