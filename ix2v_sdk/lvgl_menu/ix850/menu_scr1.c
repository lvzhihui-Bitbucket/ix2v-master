﻿#include "lv_ix850.h"

#include "task_Beeper.h"

lv_obj_t * ui_main;
static lv_obj_t * ui_main_panel=NULL;
extern lv_font_t * fontlog;
lv_timer_t *mainMenu_Timer;


static lv_obj_t* add_menu(lv_obj_t* parent, const void* src_bg, const char* txt, const void* cb);
static void Mon_clicked_event(lv_event_t* e);
static void Call_clicked_event(lv_event_t* e);
static void SIP_Manager_clicked_event(lv_event_t* e);
static void Card_Manager_clicked_event(lv_event_t* e);
static void Setting_clicked_event(lv_event_t* e);
static void Shell_clicked_event(lv_event_t* e);
static void Log_clicked_event(lv_event_t* e);
static void About_clicked_event(lv_event_t* e);
lv_obj_t* IX2V_main_menu_test(lv_obj_t* parent);
static void MainMemu_Timer_cb(lv_timer_t* timer);
void MainMemu_Pressed_event_cb(lv_event_t* e);

void ui_main_init(void)
{
    vtk_lvgl_lock(); 
    ui_main = lv_obj_create(NULL);
    lv_obj_clear_flag(ui_main, LV_OBJ_FLAG_SCROLLABLE);
    lv_obj_remove_style_all(ui_main);
    lv_obj_set_size(ui_main, lv_disp_get_hor_res(NULL), lv_disp_get_ver_res(NULL));
    lv_obj_align(ui_main, LV_ALIGN_TOP_LEFT, 0, 0);
    lv_obj_set_style_bg_color(ui_main, lv_palette_main(LV_PALETTE_GREY), 0);
    lv_obj_set_style_bg_opa(ui_main, LV_OPA_100, 0);
//lv_obj_add_event_cb(ui_main, MainMemu_Pressed_event_cb, LV_EVENT_PRESSED, NULL);
    vtk_lvgl_unlock(); 
}

#include "menu_common.h"

MENU_INS* one_main_menu;
MENU_INS* one_setting_menu;
void global_style_init(void);

void scr_main(void)
{
	//MainMenu_Init();
#if 0
	char act_dir[200];
    #if 1
	vtk_lvgl_lock(); 
	//MainMenu_Reset();
    lv_disp_load_scr(ui_main);

    if(ui_main_panel == NULL)
    {
        ShellCmdPrintf("scr_main menu start!\n");
        //ui_main_panel = load_Main_menu("/mnt/nand1-1/App/res/UI-DOCS/PID-IX850S/MenuResourcePack1",ui_main);
        //ui_main_panel = choose_dir_enter(ui_main);
#if 1
	API_Para_Read_String("UI_Documents",act_dir);

    	ui_main_panel = load_Main_menu(act_dir,ui_main);
#else
        ui_main_panel = load_Main_menu("/mnt/nand1-1/App/res/UI-DOCS/PID-IX850S/MenuResourcePack1",ui_main);
#endif
	    mainMenu_Timer=lv_timer_create(MainMemu_Timer_cb, 1000, NULL); 	
	    //AddOneMenuCommonClickedCb(ui_main,MainMemu_Pressed_event_cb);
    }
	 vtk_lvgl_unlock();
    #else
    global_style_init();
    one_main_menu = load_one_menu("/mnt/nand1-1/App/res/ix850_menu_package2", lv_scr_act());
    #endif
#endif
}


static void Setting_clicked_event(lv_event_t* e)
{
    MenuScr2Test();
}

static void Shell_clicked_event(lv_event_t* e)
{

}

static void Log_clicked_event(lv_event_t* e)
{
    scr_log();
}

static void About_clicked_event(lv_event_t* e)
{

}


void MainMemu_Pressed_event_cb(lv_event_t* e)
{
	BEEP_KEY();
	MainMenu_Reset_Time();
}
void MainMenu_InitExt(void)
{
  	//mainMenu_Timer=lv_timer_create(MainMemu_Timer_cb, 1000, NULL); 	
	//AddOneMenuCommonClickedCb(ui_main,MainMemu_Pressed_event_cb);
}