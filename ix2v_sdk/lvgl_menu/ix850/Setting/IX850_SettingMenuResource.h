#ifndef IX850_SettingMenuResource_H_
#define IX850_SettingMenuResource_H_

typedef void (*SettingMenuInit)(void);
typedef void (*SettingMenuProcess)(void* argc);
typedef void (*SettingMenuProcessExt)(int msg_type,void* argc);
typedef void (*SettingMenuClose)(void);

typedef struct
{
    char* name;
    SettingMenuInit init;
    SettingMenuProcess process;
    SettingMenuClose close;
	SettingMenuProcessExt process_ext;
} SettingMenuResource_S;


SettingMenuResource_S GetSettingMenuResByName(const char* name);

void MenuIXGTbView_Init(void);
void MenuIXGTbView_Close(void);

void MenuIXGManage_Init(void);
void MenuIXGManage_Close(void);

void MenuInstallProcess(void* data);

void MenuPassword1_Init(void);
void MenuPassword2_Init(void);
void MenuPassword3_Init(void);
void MenuPassword4_Init(void);
void MenuPassword_Close(void);

extern void API_MenuIXGManage_Process(void* arg);
extern void API_MenuIXGTbView_Process(void* arg);
extern void API_MenuCardTable_Process(void* arg);
extern void MenuPassword_Process(void* arg);

void MenuCardTable_Init(void);
void MenuCardTable_Close(void);
void MenuIpcManage_Init(void);
void MenuIpcManage_Close(void);
void MenuNDMManage_Init(void);
void MenuNDMManage_Close(void);
void MenuCertManage_Init(void);
void MenuCertManage_Close(void);

void MenuTbView_Init(void);
void API_MenuTbViewCommon_Process(void* arg);
void MenuTbViewCommon_Close(void);

void API_SettingMenuProcess(void* data);
void API_SettingMenuClose(void);
void API_SettingMenuReturn(void);

void RtcSettingProcess(int msg_type,void* data);
void SipConfigProcess(int msg_type,void* data);
void SipConfig_Close(void);
void SipConfigExt_Init(void);

void MenuSipIMAccView_Init(void);
void MenuSipIMAccView_Close(void);
extern void API_MenuSipIMAccView_Process(void* arg);

void MenuSipCallLogView_Init(void);
void MenuSipCallLogView_Close(void);
extern void API_MenuSipCallLogView_Process(void* arg);

void LanConfigProcess(int msg_type,void* data);

void MenuCallRecord_Init(void);
void MenuCallRecord_Close(void);
extern void API_MenuCallRecord_Process(void* arg);

void MenuDtIm_Init(void);
void MenuDtIm_Close(void);
extern void API_MenuDtIm_Process(void* arg);

extern void MenuCfg_list_init(void);


void MenuRES_Select_Init(void);
void MenuRES_Select_Close(void);

void EnterCardTableMenu(void);
void CardTableMenuClose(void);
void EnterDxgImMenu(void);
void DxgImMenuClose(void);


void dxgmanage_show(void);
void dxgmanage_close(void);

void EnterIpcManageMenu(void);
void IpcManageMenuClose(void);

void CreateNewRes_Init(void);
void CreateNewRes_Close(void);

void MenuRES_Editor_Init(void);
void MenuRES_Editor_Close(void);
void MenuCfg_Select_Close(void);
void MenuCfg_Select_init(void);

void Menu_Wifi_Init(void);
void Menu_Wifi_Close(void);
void Menu_CheckDtImResult_Init(void);
void Menu_CheckDtImResult_Close(void);
void Menu_CheckDtImOnline_Process(void* data);
void Menu_CheckDtImOnline_Close(void);

void RadarConfigProcess(int msg_type,void* data);
void RadarConfigClose(void);

void MenuSAT_HYBRID_Select_Init(void);
void MenuSAT_HYBRID_Select_Close(void);

void MenuSAT_HYBRID_Editor_Init(void);
void MenuSAT_HYBRID_Editor_Close(void);

void MenuSAT_HYBRID_Prog_Init(void);
void MenuSAT_HYBRID_Prog_Close(void);
void Menu_CallNumberSetting_Process(void* data);

void MenuSAT_HYBRID_Check_Process(void* data);
void MenuSAT_HYBRID_Check_Close(void);

void MenuSAT_HYBRID_Report_Init(void);
void MenuSAT_HYBRID_Report_Close(void);

void MenuAPT_MAT_Select_Init(void);
void MenuAPT_MAT_Select_Close(void);

void Menu_ManagerAbout_Init(void);
void Menu_ManagerAbout_Close(void);
void Menu_UserAbout_Init(void);
void Menu_UserAbout_Close(void);

void Menu_CreateNewSat_Close(void);
void Menu_CreateNewSat_Init(void);

void Menu_DH_IM_FW_UPDATE_Process(void* data);
void Menu_DH_IM_FW_UPDATE_Close(void);
void Menu_DH_IM_FW_UPDATE_Init(void);

void Menu_CreateNewAptMat_Init(void);
void Menu_CreateNewAptMat_Close(void);

void MenuAPT_MAT_Editor_Init(void);
void MenuAPT_MAT_Editor_Close(void);

void MenuAPT_MAT_Check_Process(void* data);
void MenuAPT_MAT_Check_Close(void);

void MenuAPT_MAT_Report_Init(void);
void MenuAPT_MAT_Report_Close(void);

void MenuVM_MAT_Select_Init(void);
void MenuVM_MAT_Select_Close(void);
void MenuVM_MAT_Create_Init(void);
void MenuVM_MAT_Create_Close(void);
void MenuVM_MAT_Editor_Init(void);
void MenuVM_MAT_Editor_Close(void);
void MenuVM_User_Editor_Init(void);
void MenuVM_User_Editor_Close(void);
void Btn_Common_Click_Process(char *icon_data);

#endif 
