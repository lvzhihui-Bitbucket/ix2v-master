#ifndef IX850_SETTING_ROOT_H_
#define IX850_SETTING_ROOT_H_

#include "lv_ix850.h"

lv_obj_t* create_setting_menu_root(const char* rootName);
lv_obj_t* create_setting_page(lv_obj_t* menu, cJSON* page, cJSON* data);
void ui_root_init(void);
void ui_root_exit(void);

#endif 