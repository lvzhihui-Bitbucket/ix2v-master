#include "IX850_Setting_Page.h"
#include "menu_utility.h"
#include "obj_lvgl_msg.h"
#include "KeyboardResource.h"
#include "utility.h"
#include "define_file.h"

static lv_obj_t* kb;
static cJSON* page_json;
static lv_obj_t* page_back_to_scr;
static lv_obj_t* page_scr;
static cJSON* nameToLVObj;
static lv_obj_t * ui_keyboard;
static lv_obj_t * editor_keyboard_return;
static cJSON* description=NULL;

static void page_back_event_handler(lv_event_t* e);
static int* setting_text_edit_event_cb(const char* val,void *user_data);
static lv_obj_t* ui_setting_keyboard_init(void);
static void IX2V_setting_switch_handler(lv_event_t* e);
//static void IX2V_setting_slider_value_handler(lv_event_t* e);
static void IX2V_setting_value_refresh_event(lv_event_t* e);
static void combox_click_event(lv_event_t* e);
static void combox_exit_event(lv_event_t* e);
static void choose_combox_event(lv_event_t* e);
void setting_slink_page(lv_event_t* e);
extern void AddOneMenuCommonClickedCb(lv_obj_t* menu,lv_event_cb_t cb);
extern void MainMemu_Pressed_event_cb(lv_event_t* e);

static lv_obj_t* setting_page_menu=NULL;

lv_obj_t* create_setting_menu_page(const char* name)
{
    char buf[200];
    snprintf(buf, 200, "%s/%s.json", SETTING_PAGE_DIR, name);
    page_json = get_cjson_from_file(buf);
    if(page_json == NULL) 
    {
        return NULL;
    }
    SETTING_VIWE* sv = (SETTING_VIWE*)lv_mem_alloc(sizeof(SETTING_VIWE)); 
    lv_memset_00(sv, sizeof(SETTING_VIWE));

    sv->back_scr = lv_scr_act();

    sv->scr = ui_page_init();

    lv_obj_t* menu = lv_menu_create(sv->scr);
	setting_page_menu=menu;
    lv_obj_set_size(menu, 480, 800);
    set_font_size(menu,0);
    lv_obj_set_style_text_color(menu, lv_color_white(), 0);
    lv_obj_set_style_bg_color(menu, lv_color_hex(0x41454D), 0);
    sv->menu = menu;

    lv_obj_t* header = lv_menu_get_main_header(menu);
    lv_obj_set_style_bg_opa(header,LV_OPA_MAX,0);
    lv_obj_set_size(header,LV_PCT(100),70);
    lv_obj_set_style_bg_color(header,lv_color_make(20,80,198),0);
    lv_obj_add_flag(header, LV_OBJ_FLAG_CLICKABLE);
    lv_obj_set_style_pad_all(header,0,0);

    lv_obj_t* btn = lv_menu_get_main_header_back_btn(menu);
    lv_obj_remove_style_all(btn);
    lv_obj_set_style_bg_opa(btn,LV_OPA_MAX,0);
    lv_obj_set_size(btn,160,70);
    lv_obj_set_style_bg_color(btn,lv_color_make(19,132,250),0);
    lv_obj_clear_flag(btn, LV_OBJ_FLAG_EVENT_BUBBLE);
    lv_obj_set_style_text_color(btn, lv_color_white(), 0);
    lv_obj_add_event_cb(btn, page_back_event_handler, LV_EVENT_CLICKED, sv);
    lv_obj_t* img = lv_obj_get_child(btn,0);
	lv_img_set_src(img,LV_VTK_LEFT);
    set_font_size(img,1);
    lv_obj_center(img);

    lv_obj_t* label = lv_obj_get_child(header,1);
    lv_obj_align(label, LV_ALIGN_LEFT_MID, 160, 0);
    lv_obj_set_size(label, 320, 30);
    lv_obj_set_style_text_align(label,LV_TEXT_ALIGN_CENTER,0);
   
    lv_obj_add_event_cb(header, header_click_event_cb, LV_EVENT_CLICKED, btn);

    lv_menu_set_mode_root_back_btn(menu, LV_MENU_ROOT_BACK_BTN_ENABLED);

    int count = cJSON_GetArraySize(page_json);

    for (int i = 0; i < count; i++) {
        cJSON* page_data = cJSON_GetArrayItem(page_json, i);
        if(page_data == NULL){
            break;
        }
        cJSON* pages = cJSON_GetObjectItem(page_data, "PageTitle");

        if (pages)
        //if (pages&&strcmp(page_data->string,name)==0)
        {
            create_setting_page(menu, page_data,page_json);
        }
    }

    if (nameToLVObj == NULL)
    {
        lv_disp_load_scr(page_scr);
        return menu;
    }

#if 1
    for (int j = 0; j < count; j++) {
        cJSON* item_data = cJSON_GetArrayItem(page_json, j);

        cJSON* link = cJSON_GetObjectItem(item_data, "link");
        
        if (link)
        {
            lv_obj_t* link_obj = Get_lv_Obj_by_name(item_data->string);
            lv_obj_t* link_page = Get_lv_Obj_by_name(link->valuestring);
            printf("link:%s:%s\n", item_data->string, link->valuestring);

            if (link_obj != NULL && link_page != NULL)
                lv_menu_set_load_page_event(menu, link_obj, link_page);
            else
            {
                if (link_obj == NULL)
                    printf("link obj is NULL\n");
                if (link_page == NULL)
                    printf("link page is NULL\n");
            }
        }
    }
#endif

    lv_menu_clear_history(menu);
    lv_obj_t* page = Get_lv_Obj_by_name(name);
    if(page)
        lv_menu_set_page(menu, page);
    lv_disp_load_scr(sv->scr);
    return menu;
}
void setting_menu_page_SendRefreshEventAll(void)
{
	//if(page_scr)
		//SendRefreshEventAll(page_scr);
}
#if 0
void close_setting_menu_page(void)
{

	
	if(ui_keyboard)
	{
		lv_obj_t *ta=lv_obj_get_user_data(ui_keyboard);
		 lv_event_send(ta, LV_EVENT_CANCEL, NULL);
	}
	
	if(page_scr){
        lv_obj_del(page_scr);
        page_scr = NULL;
    }
    if(page_json){
        cJSON_Delete(page_json);
        page_json = NULL;
    }
    if(nameToLVObj)
    {
        cJSON_Delete(nameToLVObj);
        nameToLVObj = NULL;
    }    
	
}
#endif
static void page_back_event_handler(lv_event_t* e)
{
    lv_obj_t* obj = lv_event_get_target(e);
    SETTING_VIWE* sv = lv_event_get_user_data(e);

    if (lv_menu_back_btn_is_root(sv->menu, obj)) {

        ui_page_exit(sv);
    }
}

void header_click_event_cb(lv_event_t* e)
{
    lv_obj_t* btn = lv_event_get_user_data(e);

    lv_event_send(btn, LV_EVENT_CLICKED, NULL);
}

cJSON* IX2V_IO_parameter_decomposition(char* para)
{      
    cJSON* item = cJSON_GetObjectItem(GetSettingIoData(), para);
    
    if(item == NULL)
        return NULL;
    cJSON* io_para = cJSON_Duplicate(item,true);

    char* type = cJSON_GetObjectItem(io_para, "type")->valuestring;   

    if (strcmp("ITEM_switch", type) == 0)
    {
    	cJSON *sw_item=cJSON_GetObjectItem(io_para, "sw_item");
        if(sw_item!=NULL)
        {
            char buff[50];
            API_Para_Read_String(para, buff);
            if(strcmp(cJSON_GetObjectItem(sw_item,"on")->valuestring,buff)==0)
                cJSON_AddItemToObject(io_para, "state", cJSON_CreateTrue());
            else
                cJSON_AddItemToObject(io_para, "state", cJSON_CreateFalse());
        }
        else
        {
                int io_cur_data = API_Para_Read_Int(para);     
            printf("====IX2V_IO_parameter_decomposition:%s=%d\n",para,io_cur_data);
                cJSON_AddItemToObject(io_para, "state", cJSON_CreateBool(io_cur_data));
        }
    }
    else if (strcmp("ITEM_slider", type) == 0)
    {
        int io_cur_data = API_Para_Read_Int(para);
        cJSON_AddItemToObject(io_para, "slider_value", cJSON_CreateNumber(io_cur_data));
    }
    else if (strcmp("ITEM_text", type) == 0)
    {
        char* io_cur_data = lv_mem_alloc(100);
        int result = API_Para_Read_String(para,io_cur_data);
        if(result == 0){
            int intvalue = API_Para_Read_Int(para);
            sprintf(io_cur_data,"%d",intvalue);
        }
        cJSON_AddItemToObject(io_para, "value", cJSON_CreateString(io_cur_data));
        lv_mem_free(io_cur_data);
    }
    return io_para;
}


lv_obj_t* create_setting_text(lv_obj_t* parent, cJSON* item)
{
    lv_obj_t* obj = IX2V_lv_cont_create(parent, item->string);
    lv_obj_set_size(obj, LV_PCT(100), LV_SIZE_CONTENT);
    lv_obj_set_style_border_side(obj, 0, 0);
    lv_obj_set_style_bg_color(obj, lv_color_hex(0x52545A), 0);   
    lv_obj_set_style_text_color(obj, lv_color_white(), 0);
    lv_obj_set_flex_flow(obj, LV_FLEX_FLOW_ROW);
    lv_obj_clear_flag(obj, LV_OBJ_FLAG_SCROLLABLE);

    cJSON* txt = cJSON_GetObjectItem(item, "text");
    cJSON* text_color = cJSON_GetObjectItem(item, "text_color");

    cJSON* value = cJSON_GetObjectItem(item, "value");
    cJSON* value_color = cJSON_GetObjectItem(item, "value_color");

    cJSON* elink = cJSON_GetObjectItem(item, "ext-link");
    cJSON* link = cJSON_GetObjectItem(item, "link");
	//cJSON* soft_link = cJSON_GetObjectItem(item, "soft-link");
    lv_obj_t* link_label;
    lv_obj_t* text_label;
    lv_obj_t* value_label;
    if (txt) {
        text_label = lv_label_create(obj);
        //lv_label_set_text(text_label, txt->valuestring);
        vtk_label_set_text(text_label, txt->valuestring);
        lv_obj_set_size(text_label, 180, LV_SIZE_CONTENT);
        lv_obj_set_flex_grow(text_label, 1);
        if (text_color)
        {
            lv_obj_set_style_text_color(text_label, lv_color_hex(value_color->valueint), 0);
        }

    }

    //create value
    if (value) {
        value_label = lv_label_create(obj);
        lv_label_set_text(value_label, value->valuestring);
        lv_obj_set_size(value_label, 200, LV_SIZE_CONTENT);
        lv_obj_add_flag(value_label, LV_OBJ_FLAG_CLICKABLE);
        lv_obj_set_ext_click_area(value_label, 30);
        lv_obj_set_style_text_align(value_label,LV_TEXT_ALIGN_CENTER,0);
	//printf_json(item, "33333");
        lv_obj_add_event_cb(value_label, value_click_event, LV_EVENT_CLICKED, item);
        lv_obj_add_event_cb(value_label, IX2V_setting_value_refresh_event, LV_EVENT_REFRESH, item);
        if (value_color)
        {
            lv_obj_set_style_text_color(value_label, lv_color_hex(value_color->valueint), 0);
        }
    }
    if (link) {
        link_label = lv_label_create(obj);
        lv_label_set_text(link_label, LV_VTK_RIGHT);
        lv_obj_set_size(link_label, 20, LV_SIZE_CONTENT);
	    //lv_obj_add_event_cb(obj, setting_slink_page, LV_EVENT_CLICKED, link);	
    }
    //create symbol
    if (elink) {
        link_label = lv_label_create(obj);
        lv_label_set_text(link_label, LV_VTK_RIGHT);
        lv_obj_add_event_cb(obj, setting_extern_page, LV_EVENT_CLICKED, elink);
        lv_obj_set_size(link_label, 20, LV_SIZE_CONTENT);
    }
	#if 0
	 if (soft_link) {
        link_label = lv_label_create(obj);
        lv_label_set_text(link_label, LV_SYMBOL_RIGHT);
        lv_obj_set_size(link_label, 20, LV_SIZE_CONTENT);
	lv_obj_add_event_cb(obj, setting_slink_page, LV_EVENT_CLICKED, soft_link);	
    }
	#endif
    return obj;
}

static int* setting_text_edit_event_cb(const char* val,void *user_data)
{
    lv_label_set_text(user_data,val);

    cJSON* para_type = cJSON_GetObjectItemCaseSensitive(description, "para_type");
	cJSON* msg = cJSON_GetObjectItemCaseSensitive(description, "msg");

    if(para_type && strcmp(para_type->valuestring,"int")==0)
    {
        API_Para_Write_Int(description->string, atoi(val));
    }
    else if(para_type && strcmp(para_type->valuestring,"pb_str")==0)
    {
        ;
    }
    else
    {
        API_Para_Write_String(description->string, val);                  
    }

    if(msg)
    {
        cJSON *menu_msg=cJSON_CreateObject();
        if(menu_msg)
        {
            cJSON_AddStringToObject(menu_msg,"msg",msg->valuestring);
            cJSON_AddStringToObject(menu_msg,"input",val);
            //printf_json(menu_msg, "11111");
            API_SettingMenuTextEditProcess(menu_msg);
            cJSON_Delete(menu_msg);
        }
    }       
}

void setting_create_string_keyboard_menu(lv_obj_t* target, char* val)
{
    #if 0
    editor_keyboard_return = lv_scr_act();
    ui_keyboard = ui_setting_keyboard_init();   

    lv_obj_t* keyMenu = lv_obj_create(ui_keyboard);
    lv_obj_set_style_pad_row(keyMenu, 0, 0);
    lv_obj_set_size(keyMenu, 480, 800);
    lv_obj_set_flex_flow(keyMenu, LV_FLEX_FLOW_COLUMN);
    lv_obj_clear_flag(keyMenu, LV_OBJ_FLAG_SCROLLABLE);

    lv_obj_t* nameLabel = lv_obj_get_child(lv_obj_get_parent(target), 0);
    char* name = lv_label_get_text(nameLabel);

    lv_obj_t* name_label = lv_label_create(keyMenu);
    lv_label_set_text(name_label, name);
    lv_obj_set_size(name_label, LV_PCT(100), 80);
    lv_obj_set_style_text_font(name_label, &lv_font_montserrat_32, 0);


    lv_obj_t* ta = lv_textarea_create(keyMenu);
    lv_obj_set_style_text_font(ta, &lv_font_montserrat_26, 0);
    lv_textarea_add_text(ta, val);
    lv_obj_set_size(ta, LV_PCT(100), 300);
    //lv_obj_set_y(ta, 100);
    lv_obj_set_user_data(ui_keyboard, ta);

    kb = keyboard_create(ui_keyboard);

    lv_obj_add_event_cb(ta, setting_text_edit_event_cb, LV_EVENT_ALL, target);
    lv_event_send(ta, LV_EVENT_FOCUSED, NULL);
    
    lv_obj_t* nameLabel = lv_obj_get_child(lv_obj_get_parent(target), 0);
    char* name = lv_label_get_text(nameLabel);

    cJSON* pattern = cJSON_GetObjectItem(description, "pattern");
    //if(pattern == NULL)
    Vtk_KbMode1Init(Vtk_KbMode1,NULL,name,20,pattern->valuestring,);
    #endif
}

void value_click_event(lv_event_t* e)
{
    description = lv_event_get_user_data(e);
    lv_obj_t* valueLabel = lv_event_get_target(e);
    char* text = lv_label_get_text(valueLabel);
    //printf_json(description,"description00000");
    //setting_create_string_keyboard_menu(valueLabel, text);   

    lv_obj_t* nameLabel = lv_obj_get_child(lv_obj_get_parent(valueLabel), 0);
    char* name = lv_label_get_text(nameLabel);
    char* regex_pattern;
    cJSON* para_type = cJSON_GetObjectItemCaseSensitive(description, "para_type");
	cJSON* kp_type = cJSON_GetObjectItemCaseSensitive(description, "kp_type");
    cJSON* pattern = cJSON_GetObjectItem(description, "pattern");
    if(pattern == NULL)
        regex_pattern = NULL;
    else
        regex_pattern = pattern->valuestring;

    if((kp_type&&strcmp(kp_type->valuestring,"num")==0)||(para_type && strcmp(para_type->valuestring,"int")==0))
    {
        //Vtk_KbMode2Init(Vtk_KbMode2,NULL,name,20,regex_pattern,text,"tips:",setting_text_edit_event_cb,valueLabel);
    }else
    {
        //Vtk_KbMode1Init(Vtk_KbMode1,"English",name,20,regex_pattern,text,"tips:",setting_text_edit_event_cb,valueLabel);
    }   
}

lv_obj_t* create_setting_btn(lv_obj_t* parent, cJSON* item)
{
    lv_obj_t* obj = IX2V_lv_cont_create(parent, item->string);
    lv_obj_set_size(obj, LV_PCT(100), LV_SIZE_CONTENT);
    lv_obj_set_style_border_side(obj, 0, 0);
    lv_obj_set_style_bg_color(obj, lv_color_hex(0x52545A), 0);   
    lv_obj_set_style_text_color(obj, lv_color_white(), 0);
    lv_obj_add_flag(obj, LV_OBJ_FLAG_CLICKABLE);

    cJSON* txt = cJSON_GetObjectItem(item, "text");
    cJSON* msg = cJSON_GetObjectItem(item, "msg");
    lv_obj_t* text_label;
    if (txt) {
        text_label = lv_label_create(obj);
        //lv_label_set_text(text_label, txt->valuestring);
        vtk_label_set_text(text_label, txt->valuestring);
        lv_obj_align(text_label, LV_ALIGN_LEFT_MID, 0, 0);

    }
    if(msg){
        lv_obj_add_event_cb(obj, normal_btn_clicked_event, LV_EVENT_CLICKED, msg);
    }
   
    return obj;
}

void normal_btn_clicked_event(lv_event_t* e)
{  
    cJSON* msg = lv_event_get_user_data(e);
    if (msg)
    {
        printf("msg = [%s]\n",msg->valuestring);
        API_SettingMenuProcess(msg->valuestring);
    }  
}

static char* ReadPB_To_String(const char* name, char* string, int len)
{
    if(!string || !name || !len)
    {
        return string;
    }

    cJSON* value = API_PublicInfo_Read(name);
    if(cJSON_IsString(value))
    {
        snprintf(string, len, "%s", value->valuestring);
    }
    else if(cJSON_IsNumber(value))
    {
        snprintf(string, len, "%d", value->valueint);
    }
    else
    {
        char* pTemp = cJSON_PrintUnformatted(value);
        snprintf(string, len, "%s", pTemp);
        if(pTemp)
        {
            free(pTemp);
        }
    }

    return string;
}

lv_obj_t* create_setting_PublicInfo(lv_obj_t* parent, cJSON* item)
{
    char valueString[100] = {0};

	cJSON* ParaName = cJSON_GetObjectItem(item, "ParaName");
	if(ParaName)
	{
		cJSON* edit_item=cJSON_GetObjectItem(item, ParaName->valuestring);
		if(edit_item)
		{
			cJSON_AddStringToObject(edit_item,"value", ReadPB_To_String(ParaName->valuestring, valueString, 100));
			return create_setting_text(parent, edit_item);
		}
	}
    lv_obj_t* obj = IX2V_lv_cont_create(parent, item->string);
    lv_obj_set_size(obj, LV_PCT(100), LV_SIZE_CONTENT);
    lv_obj_set_style_border_side(obj, 0, 0);
    lv_obj_set_style_bg_color(obj, lv_color_hex(0x52545A), 0);   
    lv_obj_set_style_text_color(obj, lv_color_white(), 0);
    lv_obj_set_flex_flow(obj, LV_FLEX_FLOW_ROW);

    //cJSON* ParaName = cJSON_GetObjectItemCaseSensitive(item, "ParaName");
	cJSON* text = cJSON_GetObjectItem(item, "text");
    if(ParaName){
        lv_obj_t* label = lv_label_create(obj);
        lv_obj_set_size(label, 180, LV_SIZE_CONTENT);
        if(text)
            //lv_label_set_text(label, text->valuestring);
            vtk_label_set_text(label, text->valuestring);
        else
            lv_label_set_text(label, ParaName->valuestring);
		

        lv_obj_t* stateLabel = lv_label_create(obj);
        lv_obj_set_size(stateLabel, 200, LV_SIZE_CONTENT);
        
        lv_label_set_text(stateLabel, ReadPB_To_String(ParaName->valuestring, valueString, 100));

	    lv_obj_add_event_cb(stateLabel, IX2V_setting_value_refresh_event, LV_EVENT_REFRESH, item);	
    }
    
    return obj;
}

lv_obj_t* create_setting_switch(lv_obj_t* parent, cJSON* item)
{
	int chk;
	char buff[100];

    cJSON* state = cJSON_GetObjectItem(item, "state");
    cJSON* txt = cJSON_GetObjectItem(item, "text");    

    lv_obj_t* obj = IX2V_lv_cont_create(parent, item->string);
    lv_obj_set_size(obj, LV_PCT(100), LV_SIZE_CONTENT);
    lv_obj_set_style_border_side(obj, 0, 0);
    lv_obj_set_style_bg_color(obj, lv_color_hex(0x52545A), 0);   
    lv_obj_set_style_text_color(obj, lv_color_white(), 0);
    lv_obj_set_flex_flow(obj, LV_FLEX_FLOW_ROW);

    if(txt){
        lv_obj_t* label = lv_label_create(obj);
        lv_obj_set_size(label, 200, LV_SIZE_CONTENT);
        //lv_label_set_text(label, txt->valuestring);
        vtk_label_set_text(label, txt->valuestring);
    }
    if(state){
        chk= cJSON_IsTrue(state)?1:0;

        lv_obj_t* sw = lv_switch_create(obj);
        lv_obj_set_size(sw, 100, 50);
        lv_obj_add_state(sw, chk ? LV_STATE_CHECKED : 0);
        
        //sprintf(buff,"%s-SW",item->string);
        //cJSON_AddNumberToObject(nameToLVObj, buff, (int)sw);

        lv_obj_add_event_cb(sw, IX2V_setting_switch_handler, LV_EVENT_VALUE_CHANGED, item);
        lv_obj_add_event_cb(sw, IX2V_setting_value_refresh_event, LV_EVENT_REFRESH, item);
		
    }
    return obj;
}

static void IX2V_setting_switch_handler(lv_event_t* e)
{
    cJSON* para = lv_event_get_user_data(e);
    lv_obj_t* obj = lv_event_get_target(e);

	cJSON *update=cJSON_GetObjectItem(para,"update");
	cJSON *msg=cJSON_GetObjectItem(para,"msg");
	if(update!=NULL&&update->valueint==0)
		return;
	cJSON *sw_item=cJSON_GetObjectItem(para, "sw_item");
	if(sw_item)
		{
			
		}
	else
	{
	    if (!lv_obj_has_state(obj, LV_STATE_CHECKED)) {	        
            printf("%s off\n", para->string);
            API_Para_Write_Int(para->string,0);           
	    }
	    else {	        
            printf("%s on\n", para->string);
            API_Para_Write_Int(para->string, 1);	        
	    }
		
		if(msg)
		{
			cJSON *menu_msg=cJSON_CreateObject();
			if(menu_msg)
			{
				cJSON_AddStringToObject(menu_msg,"msg",msg->valuestring);
				cJSON_AddStringToObject(menu_msg,"input",lv_obj_has_state(obj, LV_STATE_CHECKED)?"on":"off");
				//printf_json(menu_msg, "11111");
				API_SettingMenuTextEditProcess(menu_msg);
				cJSON_Delete(menu_msg);
			}
		}
	}
}

lv_obj_t* create_setting_combox(lv_obj_t* parent, cJSON* item)
{
    cJSON* txt = cJSON_GetObjectItem(item, "text");

    lv_obj_t* obj = IX2V_lv_cont_create(parent, item->string);
    lv_obj_set_size(obj, LV_PCT(100), LV_SIZE_CONTENT);
    lv_obj_set_style_border_side(obj, 0, 0);
    lv_obj_set_style_bg_color(obj, lv_color_hex(0x52545A), 0);   
    lv_obj_set_style_text_color(obj, lv_color_white(), 0);
    lv_obj_set_flex_flow(obj, LV_FLEX_FLOW_ROW);
    lv_obj_add_flag(obj, LV_OBJ_FLAG_CLICKABLE);

    if (txt) {
        lv_obj_t* text_label = lv_label_create(obj);
        //lv_label_set_text(text_label, txt->valuestring);
        vtk_label_set_text(text_label, txt->valuestring);
        lv_obj_set_size(text_label, 200, LV_SIZE_CONTENT);
        lv_obj_set_flex_grow(text_label, 1);

        lv_obj_t* link_label = lv_label_create(obj);
        lv_label_set_text(link_label, LV_VTK_RIGHT);
        //lv_obj_align(link_label, LV_ALIGN_RIGHT_MID, -10, 0);
        lv_obj_set_size(link_label, 20, LV_SIZE_CONTENT);
    }   
    lv_obj_add_event_cb(obj, combox_click_event, LV_EVENT_CLICKED,item);

    return obj;
}

static void combox_click_event(lv_event_t* e)
{   
    cJSON* item = lv_event_get_user_data(e);

    cJSON* choose = API_Para_Read_Public(item->string);  
    char* a = cJSON_Print(choose);
    printf("%s:\n%s\n", item->string,a);
	free(a);
    show_combox_menu(choose, item);

}

void show_combox_menu(cJSON* choose, cJSON* item)
{   
    
    cJSON* title = cJSON_GetObjectItemCaseSensitive(item, "text");
    cJSON* com_item = cJSON_GetObjectItemCaseSensitive(item, "com_item");

    lv_obj_t* combox = lv_obj_create(lv_scr_act()); 
    lv_obj_set_size(combox, 480, 800);
    //lv_obj_align(combox, LV_ALIGN_CENTER, 0, 0);
    //lv_obj_set_style_pad_all(combox, 0, 0);
    lv_obj_clear_flag(combox, LV_OBJ_FLAG_SCROLLABLE);
    lv_obj_set_style_bg_color(combox, lv_color_hex(0x41454D), 0);
    lv_obj_set_style_border_side(combox, 0, 0);

    lv_obj_t* titleCont = lv_obj_create(combox);
    lv_obj_set_size(titleCont, LV_PCT(100), 80);
    lv_obj_set_style_pad_all(titleCont, 0, 0);
    lv_obj_add_flag(titleCont, LV_OBJ_FLAG_CLICKABLE);
    lv_obj_set_style_bg_color(titleCont, lv_color_hex(0x41454D), 0);
    lv_obj_add_event_cb(titleCont, combox_exit_event, LV_EVENT_CLICKED,combox);
    lv_obj_set_style_border_side(titleCont, 0, 0);

    lv_obj_t* titlelabel = lv_label_create(titleCont);
    lv_obj_set_style_text_font(titlelabel, &lv_font_montserrat_32, 0);
    lv_obj_set_style_text_color(titlelabel, lv_color_white(), 0);
    if(title){
        lv_label_set_text_fmt(titlelabel, "%s  %s",LV_SYMBOL_LEFT ,get_language_text(title->valuestring)? get_language_text(title->valuestring) : title->valuestring);
    }   

    lv_obj_t* cont = lv_obj_create(combox);
    lv_obj_set_size(cont, LV_PCT(100), 720);
    lv_obj_set_y(cont,80);
    lv_obj_set_style_bg_color(cont, lv_color_hex(0x52545A), 0);
    lv_obj_set_style_pad_all(cont, 0, 0);
    lv_obj_add_flag(cont, LV_OBJ_FLAG_CLICKABLE);   
    lv_obj_set_style_text_color(cont, lv_color_white(), 0);
    lv_obj_set_style_border_side(cont,0,0);
    lv_obj_set_flex_flow(cont, LV_FLEX_FLOW_COLUMN);   
    lv_obj_set_style_border_side(cont, 0, 0);

    int length = cJSON_GetArraySize(com_item);
    int index = -1;
    for (int i = 0; i < length; i++)
    {
        cJSON* element = cJSON_GetArrayItem(com_item, i);

        if(element == NULL)
            continue;
        cJSON* value = cJSON_GetArrayItem(element, 0);  

        if(choose){
            if (cJSON_IsNumber(value))
            {
                if (value->valueint == choose->valueint)
                {
                    index = i;
                }
            }
        else
            {
                if (strcmp(value->valuestring, choose->valuestring) == 0)
                {
                    index = i;               
                }
            }
        }    

        cJSON* combox_item = cJSON_GetArrayItem(element, 1);
        char* com_text = NULL;
        if (cJSON_IsNumber(combox_item))
        {
            char txt[100];
            sprintf(txt, "%d", combox_item->valueint);
            com_text = txt;
        }
        else
        {
            com_text = combox_item->valuestring;
        }    

        lv_obj_t* box = lv_checkbox_create(cont);
        lv_checkbox_set_text(box, com_text);
        lv_obj_set_height(box, 80);
        lv_obj_set_style_text_font(box, &lv_font_montserrat_28, 0);

        lv_obj_add_flag(box, LV_OBJ_FLAG_EVENT_BUBBLE);
        //lv_obj_set_style_radius(box,LV_RADIUS_CIRCLE,LV_PART_INDICATOR);
        //lv_obj_add_style(box, &style_radio_chk, LV_PART_INDICATOR | LV_STATE_CHECKED);     
    }

    lv_obj_add_event_cb(cont, choose_combox_event, LV_EVENT_CLICKED, item);   
    if (index>=0)
        lv_obj_add_state(lv_obj_get_child(cont, index), LV_STATE_CHECKED);

}

static void combox_exit_event(lv_event_t* e)
{
    lv_obj_t* obj = lv_event_get_user_data(e);
    if(obj)
        lv_obj_del(obj);
}

static void choose_combox_event(lv_event_t* e)
{
    cJSON* item = lv_event_get_user_data(e);
    lv_obj_t* cont = lv_event_get_current_target(e);
    lv_obj_t* act_cb = lv_event_get_target(e);

    if (act_cb == cont) return;

    int cnt = lv_obj_get_child_cnt(cont);
    for(int i = 0;i < cnt;i++)
    { 
        lv_obj_clear_state(lv_obj_get_child(cont,i), LV_STATE_CHECKED);   
    } 
    lv_obj_add_state(act_cb, LV_STATE_CHECKED);     

    int active_id = lv_obj_get_index(act_cb);

    cJSON* combox = cJSON_GetObjectItemCaseSensitive(item, "com_item");
    cJSON* index = cJSON_GetArrayItem(combox,active_id);
    cJSON* value = cJSON_GetArrayItem(index,0);

    if (cJSON_IsNumber(value)){
        printf("choose = %d\t", value->valueint);
        printf("text = %s\n", item->string);
        API_Para_Write_Int(item->string,value->valueint);
    }
    else{
        printf("choose = %s\t", value->valuestring);
        printf("text = %s\n", item->string);
        API_Para_Write_String(item->string,value->valuestring);       
    } 
}

static void choose2_combox_event(lv_event_t* e)
{
    cJSON* item = lv_event_get_user_data(e);
    lv_obj_t* cont = lv_event_get_current_target(e);
    lv_obj_t* act_cb = lv_event_get_target(e);

    if (act_cb == cont) return;

    int cnt = lv_obj_get_child_cnt(cont);
    for(int i = 0;i < cnt;i++)
    { 
        lv_obj_clear_state(lv_obj_get_child(cont,i), LV_STATE_CHECKED);   
    } 
    lv_obj_add_state(act_cb, LV_STATE_CHECKED);     

    int active_id = lv_obj_get_index(act_cb);
    cJSON* com_select = API_Para_Read_Public(cJSON_GetObjectItemCaseSensitive(item, "ParaSelect")->valuestring);
    cJSON* element = cJSON_GetArrayItem(com_select, active_id);
    API_Para_Write_String(item->string,element->valuestring); 
    set_language();
}
static void combox2_exit_event(lv_event_t* e)
{
    //API_SettingMenuClose();
    MainMenu_Process(MAINMENU_Reload,NULL); 
}
static void combox2_click_event(lv_event_t* e)
{
    cJSON* item = lv_event_get_user_data(e);
    //printf("combox2_click_event:%s=%s\n", item->string,API_Para_Read_String2(item->string));
    cJSON* com_select = API_Para_Read_Public(cJSON_GetObjectItemCaseSensitive(item, "ParaSelect")->valuestring);
    cJSON* element;
    int index = -1;
    int i;

    lv_obj_t* combox = lv_obj_create(lv_scr_act()); 
    lv_obj_set_size(combox, 480, 800);
    lv_obj_clear_flag(combox, LV_OBJ_FLAG_SCROLLABLE);
    lv_obj_set_style_bg_color(combox, lv_color_hex(0x41454D), 0);
    lv_obj_set_style_border_side(combox, 0, 0);

    lv_obj_t* titleCont = lv_obj_create(combox);
    lv_obj_set_size(titleCont, LV_PCT(100), 80);
    lv_obj_set_style_pad_all(titleCont, 0, 0);
    lv_obj_add_flag(titleCont, LV_OBJ_FLAG_CLICKABLE);
    lv_obj_set_style_bg_color(titleCont, lv_color_hex(0x41454D), 0);
    lv_obj_add_event_cb(titleCont, combox2_exit_event, LV_EVENT_CLICKED,combox);
    lv_obj_set_style_border_side(titleCont, 0, 0);
    lv_obj_t* titlelabel = lv_label_create(titleCont);
    lv_obj_set_style_text_font(titlelabel, &lv_font_montserrat_32, 0);
    lv_obj_set_style_text_color(titlelabel, lv_color_white(), 0);
    char*  titlename= cJSON_GetStringValue(cJSON_GetObjectItemCaseSensitive(item, "text"));
    lv_label_set_text_fmt(titlelabel, "%s  %s",LV_SYMBOL_LEFT ,get_language_text(titlename)? get_language_text(titlename) : titlename);

    lv_obj_t* cont = lv_obj_create(combox);
    lv_obj_set_size(cont, LV_PCT(100), 720);
    lv_obj_set_y(cont,80);
    lv_obj_set_style_bg_color(cont, lv_color_hex(0x52545A), 0);
    lv_obj_set_style_pad_all(cont, 0, 0);
    lv_obj_add_flag(cont, LV_OBJ_FLAG_CLICKABLE);   
    lv_obj_set_style_text_color(cont, lv_color_white(), 0);
    lv_obj_set_style_border_side(cont,0,0);
    lv_obj_set_flex_flow(cont, LV_FLEX_FLOW_COLUMN);   
    lv_obj_set_style_border_side(cont, 0, 0);

    for (i = 0; i < cJSON_GetArraySize(com_select); i++)
    {
        cJSON* element = cJSON_GetArrayItem(com_select, i);
        if(element == NULL)
            continue;
        if (strcmp(API_Para_Read_String2(item->string), element->valuestring) == 0)
        {
            index = i;               
        }    
        lv_obj_t* box = lv_checkbox_create(cont);
        lv_checkbox_set_text(box, element->valuestring);
        lv_obj_set_height(box, 80);
        lv_obj_set_style_text_font(box, &lv_font_montserrat_28, 0);

        lv_obj_add_flag(box, LV_OBJ_FLAG_EVENT_BUBBLE);
    }
    lv_obj_add_event_cb(cont, choose2_combox_event, LV_EVENT_CLICKED, item);   
    if (index>=0)
        lv_obj_add_state(lv_obj_get_child(cont, index), LV_STATE_CHECKED);
}

lv_obj_t* create_setting_combox2(lv_obj_t* parent, cJSON* item)
{
    cJSON* txt = cJSON_GetObjectItem(item, "text");

    lv_obj_t* obj = IX2V_lv_cont_create(parent, item->string);
    lv_obj_set_size(obj, LV_PCT(100), LV_SIZE_CONTENT);
    lv_obj_set_style_border_side(obj, 0, 0);
    lv_obj_set_style_bg_color(obj, lv_color_hex(0x52545A), 0);   
    lv_obj_set_style_text_color(obj, lv_color_white(), 0);
    lv_obj_set_flex_flow(obj, LV_FLEX_FLOW_ROW);
    lv_obj_add_flag(obj, LV_OBJ_FLAG_CLICKABLE);

    if (txt) 
    {
        lv_obj_t* text_label = lv_label_create(obj);
        //lv_label_set_text(text_label, txt->valuestring);
        vtk_label_set_text(text_label, txt->valuestring);
        lv_obj_set_size(text_label, 200, LV_SIZE_CONTENT);
        lv_obj_set_flex_grow(text_label, 1);
        lv_obj_t* value_label = lv_label_create(obj);
        lv_label_set_text(value_label, API_Para_Read_String2(item->string));
        lv_obj_set_size(value_label, 200, LV_SIZE_CONTENT);
        //lv_obj_add_event_cb(value_label, IX2V_setting_value_refresh_event, LV_EVENT_REFRESH, item);
    }   
    lv_obj_add_event_cb(obj, combox2_click_event, LV_EVENT_CLICKED,item);

    return obj;
}
#if 0
lv_obj_t* create_setting_slider(lv_obj_t* parent, cJSON* item)
{

    lv_obj_t* obj = IX2V_lv_cont_create(parent, item->string);
    lv_obj_set_size(obj, LV_PCT(100), LV_SIZE_CONTENT);
    lv_obj_set_style_border_side(obj, 0, 0);
    lv_obj_set_style_bg_color(obj, lv_color_hex(0x52545A), 0);   
    lv_obj_set_style_text_color(obj, lv_color_white(), 0);
    lv_obj_set_flex_flow(obj, LV_FLEX_FLOW_ROW);

    int min = cJSON_GetObjectItemCaseSensitive(item, "minimum")->valueint;
    int max = cJSON_GetObjectItemCaseSensitive(item, "maximum")->valueint;
    int val = cJSON_GetObjectItemCaseSensitive(item, "slider_value")->valueint;

    lv_obj_t* slider = lv_slider_create(obj);
    lv_obj_set_height(slider, 20);
    lv_obj_set_flex_grow(slider, 1);
    lv_slider_set_range(slider, min, max);
    lv_slider_set_value(slider, val, LV_ANIM_OFF);
    lv_obj_add_flag(slider, LV_OBJ_FLAG_FLEX_IN_NEW_TRACK);

    lv_obj_add_event_cb(slider, IX2V_setting_slider_value_handler, LV_EVENT_PRESSED, item->string);

    return obj;
}

static void IX2V_setting_slider_value_handler(lv_event_t* e)
{
    lv_obj_t* obj = lv_event_get_target(e);
    char* paraNmae = lv_event_get_user_data(e);
    int32_t value = lv_slider_get_value(obj);
    API_Para_Write_Int(paraNmae,value);
    printf("%s value = %d\n", paraNmae,value);
}
#endif
void setting_extern_page(lv_event_t* e)
{
    cJSON* link = lv_event_get_user_data(e);
    if (link == NULL)
    {
        return;
    }

    lv_msg_send(SETTING_MSG_SettingChangePage,link->valuestring);
}
void setting_slink_page(lv_event_t* e)
{
	lv_obj_t *obj=lv_event_get_target(e);
    cJSON* link = lv_event_get_user_data(e);
    if (link == NULL)
    {
        return;
    }
	
  cJSON* page_data= cJSON_GetObjectItem(page_json, link->valuestring);
   lv_obj_t *link_page=create_setting_page(setting_page_menu, page_data,page_json);
      lv_menu_set_load_page_event(setting_page_menu, obj, link_page);
	AddOneMenuCommonClickedCb(link_page, MainMemu_Pressed_event_cb);
	lv_obj_remove_event_cb(obj, setting_slink_page);	
}
void ui_page_exit(SETTING_VIWE* sv)
{
    if(sv == NULL)
        return;
    lv_disp_load_scr(sv->back_scr);
    if(sv->scr){
        lv_obj_del(sv->scr);
        sv->scr = NULL;
    }
    if(page_json){
        cJSON_Delete(page_json);
        page_json = NULL;
    }
    if(nameToLVObj)
    {
        cJSON_Delete(nameToLVObj);
        nameToLVObj = NULL;
    }    
}

lv_obj_t* ui_page_init(void)
{
    lv_obj_t* page_scr = lv_obj_create(NULL);
    lv_obj_clear_flag(page_scr, LV_OBJ_FLAG_SCROLLABLE);
    lv_obj_remove_style_all(page_scr);
    lv_obj_set_size(page_scr, lv_disp_get_hor_res(NULL), lv_disp_get_ver_res(NULL));
    lv_obj_align(page_scr, LV_ALIGN_TOP_LEFT, 0, 0);
    lv_obj_set_style_bg_color(page_scr, lv_palette_main(LV_PALETTE_GREY), 0);
    lv_obj_set_style_bg_opa(page_scr, LV_OPA_100, 0);
    //lv_disp_load_scr(page_scr);
    return page_scr;
}

static void IX2V_setting_value_refresh_event(lv_event_t* e)
{
	cJSON* data = lv_event_get_user_data(e);
	lv_obj_t *target = lv_event_get_target(e);
    char valueString[100]; 
	 cJSON* ParaName = cJSON_GetObjectItemCaseSensitive(data, "ParaName");
	 cJSON* para_type = cJSON_GetObjectItemCaseSensitive(data, "para_type");
	 if(ParaName!=NULL)
	 {
       	 lv_label_set_text(target, ReadPB_To_String(ParaName->valuestring, valueString, 100));
	 }
	 else if(para_type!=NULL&&strcmp(para_type,"pb_str"))
	 {
		lv_label_set_text(target, ReadPB_To_String(data->string, valueString, 100));
	 }
	 else
	 {
		 char* type = cJSON_GetObjectItemCaseSensitive(data, "type")->valuestring;   

		    if (strcmp("ITEM_switch", type) == 0)
		    {
		    	cJSON *sw_item=cJSON_GetObjectItemCaseSensitive(data, "sw_item");	
			if(sw_item!=NULL)
			{
				char buff[50];
				API_Para_Read_String(data->string, buff);
				if(strcmp(cJSON_GetObjectItemCaseSensitive(sw_item,"on")->valuestring,buff)==0)
					lv_obj_add_state(target, LV_STATE_CHECKED);
				else
					lv_obj_clear_state(target, LV_STATE_CHECKED);
			}
			else
			{
			        int io_cur_data = API_Para_Read_Int(data->string);     
					if(io_cur_data)
						lv_obj_add_state(target, LV_STATE_CHECKED);
					else
						lv_obj_clear_state(target, LV_STATE_CHECKED);
			}
			
		    }
		    else if (strcmp("ITEM_slider", type) == 0)
		    {
		       // int io_cur_data = API_Para_Read_Int(para);
		       // cJSON_AddItemToObject(io_para, "slider_value", cJSON_CreateNumber(io_cur_data));
		    }
		    else if (strcmp("ITEM_text", type) == 0)
		    {
		        char* io_cur_data = lv_mem_alloc(100);
		        int result = API_Para_Read_String(data->string,io_cur_data);
		        if(result == 0){
		            int intvalue = API_Para_Read_Int(data->string);
		            sprintf(io_cur_data,"%d",intvalue);
		        }
		        //cJSON_AddItemToObject(io_para, "value", cJSON_CreateString(io_cur_data));
		        lv_label_set_text(target, io_cur_data);
		        lv_mem_free(io_cur_data);
		    }
	 }
}

lv_obj_t* IX2V_lv_page_create(lv_obj_t* menu, const char* title, const char* menuName)
{
    if (nameToLVObj == NULL)
    {
        nameToLVObj = cJSON_CreateObject();
    }

    lv_obj_t* page = lv_menu_page_create(menu, title);

    if (page)
    {
        cJSON_AddNumberToObject(nameToLVObj, menuName, (int)page);
    }

    return page;
}

lv_obj_t* IX2V_lv_cont_create(lv_obj_t* parent, const char* contName)
{
    if (nameToLVObj == NULL)
    {
        nameToLVObj = cJSON_CreateObject();
    }

    lv_obj_t* cont = lv_obj_create(parent);

    if (cont)
    {
        cJSON_AddNumberToObject(nameToLVObj, contName, (int)cont);
    }

    return cont;
}

lv_obj_t* Get_lv_Obj_by_name(const char* menuName)
{
    cJSON* json;
    lv_obj_t* ret = NULL;

    json = cJSON_GetObjectItemCaseSensitive(nameToLVObj, menuName);
    if (json)
    {
        ret = (lv_obj_t*)(json->valueint);
    }

    return ret;
}

static lv_obj_t* ui_setting_keyboard_init(void)
{
    lv_obj_t* ui_keyboard = lv_obj_create(NULL);
    lv_obj_clear_flag(ui_keyboard, LV_OBJ_FLAG_SCROLLABLE);
    lv_obj_remove_style_all(ui_keyboard);
    lv_obj_set_size(ui_keyboard, lv_disp_get_hor_res(NULL), lv_disp_get_ver_res(NULL));
    lv_obj_align(ui_keyboard, LV_ALIGN_TOP_LEFT, 0, 0);
    lv_obj_set_style_bg_color(ui_keyboard, lv_palette_main(LV_PALETTE_GREY), 0);
    lv_obj_set_style_bg_opa(ui_keyboard, LV_OPA_100, 0);
    lv_disp_load_scr(ui_keyboard);
    return ui_keyboard;
}
