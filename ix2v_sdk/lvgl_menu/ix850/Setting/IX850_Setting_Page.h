#ifndef IX850_SETTING_PAGE_H_
#define IX850_SETTING_PAGE_H_

#include "lv_ix850.h"

typedef struct 
{   
    lv_obj_t* back_scr;
    lv_obj_t* scr;
    lv_obj_t* menu;
}SETTING_VIWE;

lv_obj_t* create_setting_menu_page(const char* name);
lv_obj_t* create_setting_text(lv_obj_t* parent, cJSON* item);
lv_obj_t* create_setting_btn(lv_obj_t* parent, cJSON* item);
lv_obj_t* create_setting_PublicInfo(lv_obj_t* parent, cJSON* item);
lv_obj_t* ui_page_init(void);
void ui_page_exit(SETTING_VIWE* sv);
void header_click_event_cb(lv_event_t* e);
void setting_extern_page(lv_event_t* e);
lv_obj_t* Get_lv_Obj_by_name(const char* menuName);
lv_obj_t* IX2V_lv_page_create(lv_obj_t* menu, const char* title, const char* menuName);
lv_obj_t* IX2V_lv_cont_create(lv_obj_t* parent, const char* contName);
void normal_btn_clicked_event(lv_event_t* e);
cJSON* IX2V_IO_parameter_decomposition(char* para);
void value_click_event(lv_event_t* e);
void setting_create_string_keyboard_menu(lv_obj_t* target, char* val);
void value_click_event(lv_event_t* e);
lv_obj_t* create_setting_switch(lv_obj_t* parent, cJSON* item);
//lv_obj_t* create_setting_slider(lv_obj_t* parent, cJSON* item);
lv_obj_t* create_setting_combox(lv_obj_t* parent, cJSON* item);
void show_combox_menu(cJSON* choose, cJSON* item);
#endif 
