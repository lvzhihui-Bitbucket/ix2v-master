#include "IX850_Setting_Root.h"
#include "menu_utility.h"
#include "utility.h"
#include "IX850_Setting_Page.h"
#include "obj_lvgl_msg.h"
#include "obj_ConnectPhone.h"
#include "IX850_SettingMenuResource.h"
#include "obj_IoInterface.h"
#include "task_Event.h"
#include "task_Beeper.h"
#include "define_file.h"

static cJSON* root_json = NULL;
static cJSON* io_json = NULL;
static lv_obj_t* root_scr = NULL;
static lv_obj_t* root_back_to_scr = NULL;

static void *setting_menu_tips=NULL;

static void root_back_event_handler(lv_event_t* e);
extern void AddOneMenuCommonClickedCb(lv_obj_t* menu,lv_event_cb_t cb);
extern void MainMemu_Pressed_event_cb(lv_event_t* e);
static void ExitSettingRoot(void);

static void LoadSettingIoData(void)
{
    if(!io_json)
    {
        io_json = get_cjson_from_file(SETTING_PAGE_DIR "io.json");
    }
}

const cJSON* GetSettingIoData(void)
{
    return io_json;
}


lv_obj_t* create_setting_menu_root(const char* rootName)
{
    char buf[200];
    snprintf(buf, 200, "%s/%s.json", SETTING_PAGE_DIR, rootName);

    root_json = get_cjson_from_file(buf);
    if(!root_json)
    {
        return NULL;
    }

    root_back_to_scr = lv_scr_act();
    ui_root_init();

    lv_obj_t* menu = lv_menu_create(root_scr);
    lv_obj_set_size(menu, 480, 800);
    set_font_size(menu,0);
    //lv_obj_set_style_text_font(menu, ft_2.font, 0);
    lv_obj_set_style_text_color(menu, lv_color_white(), 0);
    lv_obj_set_style_bg_color(menu, lv_color_hex(0x41454D), 0);  

    lv_obj_t* header = lv_menu_get_main_header(menu);   
    lv_obj_set_style_bg_opa(header,LV_OPA_MAX,0);
    lv_obj_set_size(header,LV_PCT(100),70);
    lv_obj_set_style_bg_color(header,lv_color_make(26,82,196),0);
    lv_obj_add_flag(header, LV_OBJ_FLAG_CLICKABLE);
    lv_obj_set_style_pad_all(header,0,0);

    lv_obj_t* btn = lv_menu_get_main_header_back_btn(menu);
    lv_obj_remove_style_all(btn);
    lv_obj_set_style_bg_opa(btn,LV_OPA_MAX,0);
    lv_obj_set_size(btn,160,70);
    lv_obj_set_style_bg_color(btn,lv_color_make(19,132,250),0);
    lv_obj_clear_flag(btn, LV_OBJ_FLAG_EVENT_BUBBLE);
    lv_obj_set_style_text_color(btn, lv_color_white(), 0);
    lv_obj_add_event_cb(btn, root_back_event_handler, LV_EVENT_CLICKED, menu);
    lv_obj_t* img = lv_obj_get_child(btn,0);
	lv_img_set_src(img,LV_VTK_CALL_RECORD1);
    set_font_size(img,1);
    lv_obj_center(img);

    lv_obj_t* label = lv_obj_get_child(header,1);
    lv_obj_align(label, LV_ALIGN_LEFT_MID, 160, 0);
    lv_obj_set_size(label, 320, 30);
    lv_obj_set_style_text_align(label,LV_TEXT_ALIGN_CENTER,0);

    lv_obj_add_event_cb(header, header_click_event_cb, LV_EVENT_CLICKED, btn);

    lv_menu_set_mode_root_back_btn(menu, LV_MENU_ROOT_BACK_BTN_ENABLED);
    
    lv_obj_t* page = NULL;
    uint8_t count = cJSON_GetArraySize(root_json);
    for (int i = 0; i < count; i++) {
        cJSON* page_data = cJSON_GetArrayItem(root_json, i);
        if(cJSON_IsObject(page_data))
        {
            cJSON* pages = cJSON_GetObjectItem(page_data, "PageTitle");
            if (pages)
            {
                page = create_setting_page(menu, page_data, root_json);
            }
        }
    }
    if(page != NULL)
        lv_menu_set_page(menu, page);
    lv_disp_load_scr(root_scr);
    return menu;
}

static void root_back_event_handler(lv_event_t* e)
{
    lv_obj_t* obj = lv_event_get_target(e);
    lv_obj_t* crr_menu = lv_event_get_user_data(e);

    if (lv_menu_back_btn_is_root(crr_menu, obj)) {
        lv_disp_load_scr(root_back_to_scr);
        //ExitSettingRoot();
    }
}

lv_obj_t* create_setting_page(lv_obj_t* menu, cJSON* page, cJSON* data)
{     
    char* title = cJSON_GetObjectItem(page, "PageTitle")->valuestring;
    cJSON* page_items = cJSON_GetObjectItem(page, "Items");
    char* titleTranslate = get_language_text(title);

    lv_obj_t* sub_page = IX2V_lv_page_create(menu, titleTranslate ? titleTranslate : title, page->string);
    lv_obj_set_style_pad_hor(sub_page, 20, 0);
    lv_obj_clear_flag(sub_page, LV_OBJ_FLAG_SCROLLABLE);

    lv_obj_t* section = NULL;
    section = lv_menu_section_create(sub_page);
    lv_obj_set_style_pad_ver(section,15,0);
    lv_obj_set_style_bg_opa(section, LV_OPA_TRANSP, 0);
    lv_obj_set_style_pad_row(section, 15, 0);

    uint8_t size = cJSON_GetArraySize(page_items);
    for (int i = 0; i < size; i++)
    {
        cJSON* item = cJSON_GetArrayItem(page_items, i);
        if(cJSON_IsString(item))
        {
            cJSON* page_item = cJSON_GetObjectItem(data, item->valuestring);              
            if (cJSON_IsObject(page_item))
            {
                cJSON* io_name = cJSON_GetObjectItemCaseSensitive(page_item, "IO_name");        
                if(cJSON_IsString(io_name))
                {
                    page_item = IX2V_IO_parameter_decomposition(io_name->valuestring);
                    if(page_item)
                    {
                        cJSON_ReplaceItemInObject(data,item->valuestring,page_item);
                    }
                }

                cJSON* type = cJSON_GetObjectItemCaseSensitive(page_item, "type");
                if(type)
                {
                    if(cJSON_IsString(type))
                    {
                        if (strcmp("ITEM_text", type->valuestring) == 0)
                        {       
                            create_setting_text(section, page_item);
                        }
                        else if (strcmp("ITEM_btn", type->valuestring) == 0)
                        {
                            create_setting_btn(section, page_item);
                        }
                        else if (strcmp("Item_PB_Para", type->valuestring) == 0)
                        {
                            create_setting_PublicInfo(section, page_item);
                        }
                        else if (strcmp("ITEM_switch", type->valuestring) == 0)
                        {
                            create_setting_switch(section, page_item);
                        }
                        else if (strcmp("ITEM_combox", type->valuestring) == 0)
                        {
                            create_setting_combox(section,page_item);
                        }
                        else if (strcmp("ITEM_combox2", type->valuestring) == 0)
                        {
                            create_setting_combox2(section,page_item);
                        }
                    }
                }
            }
        }         
    }

    return sub_page;
}

void ui_root_exit(void)
{
    lv_disp_load_scr(root_back_to_scr);
    if(root_scr){
        lv_obj_del(root_scr);
        root_scr = NULL;
    }
    if(root_json){
        cJSON_Delete(root_json);
        root_json = NULL;
    }
}

void ui_root_init(void)
{
    root_scr = lv_obj_create(NULL);
    lv_obj_clear_flag(root_scr, LV_OBJ_FLAG_SCROLLABLE);
    lv_obj_remove_style_all(root_scr);
    lv_obj_set_size(root_scr, lv_disp_get_hor_res(NULL), lv_disp_get_ver_res(NULL));
    lv_obj_align(root_scr, LV_ALIGN_TOP_LEFT, 0, 0);
    lv_obj_set_style_bg_color(root_scr, lv_palette_main(LV_PALETTE_GREY), 0);
    lv_obj_set_style_bg_opa(root_scr, LV_OPA_100, 0);
    //lv_disp_load_scr(root_scr);
}

static void ExitSettingRoot(void)
{
    //close_setting_menu_page(); 
	ui_root_exit();
}

#if 0

void lv_msg_setting_handler(void* s, lv_msg_t* m)
{
    LV_UNUSED(s);
    char* name;
    int cnt;
	char *btn_msg;
    static char currentMenuName[100] = {0};
    static lv_obj_t* back_to_scr = NULL;
	static char backMenuName[100] = {0};

    SettingMenuResource_S mRes;
	lv_obj_t *setting_page=NULL;
    switch (lv_msg_get_id(m))
    {
        case SETTING_MSG_SettingChangePage:
            name = (char*)lv_msg_get_payload(m);
            setting_page = create_setting_menu_page(name);
            if(setting_page)
            {
        		AddOneMenuCommonClickedCb(setting_page, MainMemu_Pressed_event_cb);
                strcpy(currentMenuName, name);
                back_to_scr = NULL;
            }
            //跳转到自定义菜单
            else
            {
                mRes = GetSettingMenuResByName(name);
                if(mRes.init)
                {
                    back_to_scr = lv_scr_act();
                    (*mRes.init)();
                    if(lv_scr_act() != back_to_scr)
                    {
                    	strcpy(backMenuName,currentMenuName);
                        strcpy(currentMenuName, name);
                    }
                }
            }
            dprintf("lv_msg_setting_handler -----------------SETTING_MSG_SettingChangePage: %s, currentMenuName: %s\n",name, currentMenuName);
            break;

	    case SETTING_MSG_SettingTextEdit:
        case SETTING_MSG_SettingBtn:

            mRes = GetSettingMenuResByName(currentMenuName);
            printf("name = [%s] \n",currentMenuName);
            if(mRes.process_ext)
            {
                printf("lv_msg_setting_handler 222222222222222 \n");
                (*mRes.process_ext)(lv_msg_get_id(m),lv_msg_get_payload(m));
            }
            //else if(mRes.process)
            if(mRes.process)
            {
                printf("lv_msg_setting_handler 333333333333333 \n");
                (*mRes.process)(lv_msg_get_payload(m));
            }
            break;
	case SETTING_MSG_NormalTip:
		API_TIPS_Close_Ext(setting_menu_tips);
		if(lv_msg_get_payload(m))
		{
			setting_menu_tips=API_TIPS_Ext_Time(lv_msg_get_payload(m),3);	
		}
		break;
	case SETTING_MSG_LongTip:
		API_TIPS_Close_Ext(setting_menu_tips);
		if(lv_msg_get_payload(m))
		{
			setting_menu_tips=API_TIPS_Ext_Time(lv_msg_get_payload(m),20);	
		}
		break;
	case SETTING_MSG_ErrorTip:
		BEEP_ERROR();

		API_TIPS_Close_Ext(setting_menu_tips);
		if(lv_msg_get_payload(m))
		{
			setting_menu_tips=API_TIPS_Ext_Time(lv_msg_get_payload(m),3);	
		}
		break;
	case SETTING_MSG_SuccTip:
		BEEP_CONFIRM();
		API_TIPS_Close_Ext(setting_menu_tips);
		if(lv_msg_get_payload(m))
		{
			setting_menu_tips=API_TIPS_Ext_Time(lv_msg_get_payload(m),3);	
		}
		break;	
	case SETTING_MSG_ValueFresh:
		setting_menu_page_SendRefreshEventAll();	
		break;

        case SETTING_MSG_SettingReturn:
		mRes = GetSettingMenuResByName(currentMenuName);	
            if(back_to_scr)
            {
            		//strcpy(currentMenuName,backMenuName);
			if(back_to_scr!=lv_scr_act())			
                		lv_disp_load_scr(back_to_scr);
            }
            
            if(mRes.close)
            {
                (*mRes.close)();
			
            }
		if(back_to_scr)
        	{
        		strcpy(currentMenuName,backMenuName);
			back_to_scr=NULL;
		}
		else
            		strcpy(currentMenuName, "");	
            break;

        case SETTING_MSG_SettingClose:
            mRes = GetSettingMenuResByName(currentMenuName);
            if(mRes.close)
            {
                (*mRes.close)();
                strcpy(currentMenuName, "");
            }
            ExitSettingRoot();
            break;
    }
}


void API_EnterSettingRoot(const char* rootName)
{
    LoadSettingIoData();
    if(root_scr)
    {
        root_back_to_scr = lv_scr_act();
        lv_disp_load_scr(root_scr);
    }
    else
    {
        AddOneMenuCommonClickedCb(create_setting_menu_root(rootName), MainMemu_Pressed_event_cb);		
    }
}


void API_SettingMenuProcess(void* data)
{
    lv_msg_send(SETTING_MSG_SettingBtn, data);
}

void API_SettingMenuTextEditProcess(void* data)
{
    lv_msg_send(SETTING_MSG_SettingTextEdit, data);
}

void API_SettingMenuClose(void)
{
    vtk_lvgl_lock();
    lv_msg_send(SETTING_MSG_SettingClose, NULL);
    vtk_lvgl_unlock();
}

void API_SettingMenuReturn(void)
{
    vtk_lvgl_lock();
    lv_msg_send(SETTING_MSG_SettingReturn, NULL);
    vtk_lvgl_unlock();
}

void API_SettingLongTip(char *tip)
{
	vtk_lvgl_lock();
    lv_msg_send(SETTING_MSG_LongTip, tip);
	vtk_lvgl_unlock();
}
void API_SettingNormalTip(char *tip)
{
	vtk_lvgl_lock();
    lv_msg_send(SETTING_MSG_NormalTip, tip);
	vtk_lvgl_unlock();
}
void API_SettingSuccTip(char *tip)
{
	vtk_lvgl_lock();
    lv_msg_send(SETTING_MSG_SuccTip, tip);
	vtk_lvgl_unlock();
}

void API_SettingErrorTip(char *tip)
{
	vtk_lvgl_lock();
    lv_msg_send(SETTING_MSG_ErrorTip, tip);
	vtk_lvgl_unlock();
}
void API_SettingValueFresh(char *para)
{
	vtk_lvgl_lock();
    lv_msg_send(SETTING_MSG_ValueFresh, para);
	vtk_lvgl_unlock();
}

void API_SettingChangePage(const char* pageName)
{
    static char name[100];
    vtk_lvgl_lock();
    snprintf(name, 100, "%s", pageName);
    LoadSettingIoData();

    lv_msg_send(SETTING_MSG_SettingChangePage, name);
    vtk_lvgl_unlock();
}

#endif
