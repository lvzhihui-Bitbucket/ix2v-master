#include <stddef.h>
#include "IX850_SettingMenuResource.h"
#include "obj_IoInterface.h"
#include "obj_lvgl_msg.h"
#include "task_Beeper.h"
#include "task_Event.h"
#include "define_file.h"

static SettingMenuResource_S SET_M_TAB[] = {
    {"WLANConfig", NULL, MenuInstallProcess, NULL,NULL},
    {"FWGrade", NULL, MenuInstallProcess, NULL,NULL},
    {"IXGEditor", MenuIXGTbView_Init, API_MenuIXGTbView_Process, MenuIXGTbView_Close,NULL},
   
    {"IXGManage", MenuIXGManage_Init, API_MenuIXGManage_Process, MenuIXGManage_Close,NULL},
    
    {"CardSetting", EnterCardTableMenu, NULL, CardTableMenuClose,NULL},
    {"PBPassword1", MenuPassword1_Init, MenuPassword_Process, MenuPassword_Close,NULL},
    {"PBPassword2", MenuPassword2_Init, MenuPassword_Process, MenuPassword_Close,NULL},
    {"PBPassword3", MenuPassword3_Init, MenuPassword_Process, MenuPassword_Close,NULL},
    {"PBPassword4", MenuPassword4_Init, MenuPassword_Process, MenuPassword_Close,NULL},
   	{"RtcSetting",NULL,NULL,NULL,RtcSettingProcess},
   	{"InternetTime",NULL,NULL,NULL,RtcSettingProcess},
    {"IPCConfig", EnterIpcManageMenu, NULL, IpcManageMenuClose,NULL},
    {"NDMManage", MenuNDMManage_Init, NULL, MenuNDMManage_Close,NULL},
    {"CERTManage", MenuCertManage_Init, NULL, MenuCertManage_Close,NULL},
    {"SIPManage", MenuTbView_Init, API_MenuTbViewCommon_Process, MenuTbViewCommon_Close,NULL},

    {"SIPConfig",NULL,NULL,SipConfig_Close,SipConfigProcess},
    {"DisSIPConfig",NULL,NULL,SipConfig_Close,SipConfigProcess},

    //{"TlogConfig", MenuTlogConfig_Init, NULL, MenuTlogConfig_Close,NULL},
    {"Sip_Sub_Acc", MenuSipIMAccView_Init, API_MenuSipIMAccView_Process, MenuSipIMAccView_Close,NULL},
    {"Sip_Call_Record", MenuSipCallLogView_Init, API_MenuSipCallLogView_Process, MenuSipCallLogView_Close,NULL},
    {"LANConfig",NULL,NULL,NULL,LanConfigProcess},
    {"CallRecord", MenuCallRecord_Init, API_MenuCallRecord_Process, MenuCallRecord_Close,NULL},
    {"DXG_IM_Edit", EnterDxgImMenu, NULL, DxgImMenuClose,NULL},
    {"DXGManager", dxgmanage_show, NULL, dxgmanage_close, NULL},
    {"MenuChoose", MenuCfg_Select_init, NULL, MenuCfg_Select_Close, NULL},
    {"WLANSetting", Menu_Wifi_Init, NULL, Menu_Wifi_Close, NULL},

    {"RaradConfig",NULL,NULL,RadarConfigClose,RadarConfigProcess},

	{"CallNumberSetting", NULL, Menu_CallNumberSetting_Process, NULL, NULL},

    {"HYBRID_MAT_Choose", MenuRES_Select_Init, NULL, MenuRES_Select_Close, NULL},
    {"HYBRID_MAT_CreateNew", CreateNewRes_Init, NULL, CreateNewRes_Close, NULL},
    {"HYBRID_MAT_Editor", MenuRES_Editor_Init, NULL, MenuRES_Editor_Close, NULL},
    {"HYBRID_MAT_CheckOnline", NULL, Menu_CheckDtImOnline_Process, Menu_CheckDtImOnline_Close, NULL},
    {"HYBRID_MAT_CheckResult", Menu_CheckDtImResult_Init, NULL, Menu_CheckDtImResult_Close, NULL},

    {"HYBRID_SAT_Choose", MenuSAT_HYBRID_Select_Init, NULL, MenuSAT_HYBRID_Select_Close, NULL},
    {"HYBRID_SAT_Editor", MenuSAT_HYBRID_Editor_Init, NULL, MenuSAT_HYBRID_Editor_Close, NULL},
    {"HYBRID_SAT_Prog", MenuSAT_HYBRID_Prog_Init, NULL, MenuSAT_HYBRID_Prog_Close, NULL},
	{"HYBRID_SAT_Check", NULL, MenuSAT_HYBRID_Check_Process, MenuSAT_HYBRID_Check_Close, NULL},
	{"HYBRID_SAT_Report", MenuSAT_HYBRID_Report_Init, NULL, MenuSAT_HYBRID_Report_Close, NULL},
	{"APT_MAT_Choose", MenuAPT_MAT_Select_Init, NULL, MenuAPT_MAT_Select_Close, NULL},
	{"APT_MAT_CreateNew", Menu_CreateNewAptMat_Init, NULL, Menu_CreateNewAptMat_Close, NULL},
	{"APT_MAT_Editor", MenuAPT_MAT_Editor_Init, NULL, MenuAPT_MAT_Editor_Close, NULL},
	{"APT_MAT_Check", NULL, MenuAPT_MAT_Check_Process, MenuAPT_MAT_Check_Close, NULL},
	{"APT_MAT_Report", MenuAPT_MAT_Report_Init, NULL, MenuAPT_MAT_Report_Close, NULL},

	{"VM_MAT_Choose", MenuVM_MAT_Select_Init, NULL, MenuVM_MAT_Select_Close, NULL},
	{"VM_MAT_CreateNew", MenuVM_MAT_Create_Init, NULL, MenuVM_MAT_Create_Close, NULL},
	{"VM_MAT_Editor", MenuVM_MAT_Editor_Init, NULL, MenuVM_MAT_Editor_Close, NULL},
	{"VM_User_Editor", MenuVM_User_Editor_Init, NULL, MenuVM_User_Editor_Close, NULL},

	{"ManagerAbout", Menu_ManagerAbout_Init, NULL, Menu_ManagerAbout_Close, NULL},
	{"UserAbout", Menu_UserAbout_Init, NULL, Menu_UserAbout_Close, NULL},
	{"HYBRID_SAT_CreateNew", Menu_CreateNewSat_Init, NULL, Menu_CreateNewSat_Close, NULL},
	{"UpdateAllDhImFw", Menu_DH_IM_FW_UPDATE_Init, Menu_DH_IM_FW_UPDATE_Process, Menu_DH_IM_FW_UPDATE_Close, NULL},
	{"APT-QuickRoot", NULL, Btn_Common_Click_Process, NULL, NULL},
	{"HYBRID-QuickRoot", NULL, Btn_Common_Click_Process, NULL, NULL},
};

static int SET_M_TAB_CNT = sizeof(SET_M_TAB)/sizeof(SET_M_TAB[0]);

SettingMenuResource_S GetSettingMenuResByName(const char* name)
{
    SettingMenuResource_S ret = {NULL, NULL, NULL, NULL, NULL};
    int i;

    if(name)
    {
        for(i = 0; i < SET_M_TAB_CNT; i ++)
        {
            if(SET_M_TAB[i].name && !strcmp(name, SET_M_TAB[i].name))
            {
                ret = SET_M_TAB[i];
                break;
            }
        }
    }
    return ret;
}


void RtcSettingProcess(int msg_type,void* data)
{
	int year = 0;
	int mon = 0;
	int date = 0;
	char setValue[100];	
	int i;
	if(msg_type == SETTING_MSG_SettingTextEdit)
	{
		cJSON *item=(cJSON *)data;
        char* msg = GetEventItemString(item, "msg");
        char* value = GetEventItemString(item, "Value");
        if(!strcmp(msg, "SET_DATE"))
        {
			for(i = 0; value[i]; i++)
			{
				if(value[i] == '-')
				{
					sscanf(value, "%d-%d-%d", &year, &mon, &date);
					snprintf(setValue, 100, "%04d%02d%02d", year, mon, date);
					DateSave(setValue);
					API_Event_NameAndMsg(EventMenuSettingProcess, "SettingRefresh", NULL);           
					return;
				}
			}
			DateSave(value);
			API_Event_NameAndMsg(EventMenuSettingProcess, "SettingRefresh", NULL);           
        }
        if(!strcmp(msg, "SET_TIME"))
        {
			for(i = 0; value[i]; i++)
			{
				if(value[i] == ':')
				{
					sscanf(value, "%d:%d:%d", &year, &mon, &date);
					snprintf(setValue, 100, "%02d%02d%02d", year, mon, date);
					TimeSave(setValue);
					API_Event_NameAndMsg(EventMenuSettingProcess, "SettingRefresh", NULL);           
					return;
				}
			}
			TimeSave(value);
			API_Event_NameAndMsg(EventMenuSettingProcess, "SettingRefresh", NULL);           
        }
	}
	else if(msg_type == SETTING_MSG_SettingBtn)
	{
		if(strcmp((char *)data,"TimeUpdate")==0)
		{
			int ret=sync_time_from_sntp_server(inet_addr(API_Para_Read_String2(TIME_SERVER)), API_Para_Read_Int(TIME_ZONE));
		        if(ret == 0){
		            LV_API_TIPS("Upate sucess",5);
		        }else{
		            LV_API_TIPS("Upate fail",4);
		        }
		}
	}
}

void LanConfigProcess(int msg_type,void* data)
{
	if(msg_type==SETTING_MSG_SettingBtn)
	{
		if(strcmp((char *)data,"LAN_confirm")==0)
		{
			API_SettingLongTip("Waitting...");
			cJSON *set_para=cJSON_CreateObject();
            cJSON_AddStringToObject(set_para, Lan_policy, API_Para_Read_String2(Lan_policy));
            API_Set_Lan_Config(set_para);
			cJSON_Delete(set_para);
		}
	}
	
}
static int  raradtest_start=0;
//static int raradtest_start_time=0;
static void *radar_tips_haddle=NULL;
static lv_timer_t* radar_test_timer=NULL;
static void radar_test_timer_cb(lv_timer_t* timer)
{
	raradtest_start=0;
	lv_timer_del(timer);
	radar_test_timer=NULL;
	API_TIPS_Close_Ext(radar_tips_haddle);
	API_TIPS_Ext_Time("Rarad test end", 2);
}
void RadarConfigProcess(int msg_type,void* data)
{
	//printf("1111111111%d:%s\n",msg_type,(char *)data);
	if(msg_type==SETTING_MSG_SettingTextEdit)
	{
		
		cJSON *menu_msg=(cJSON *)data;
		printf_json(menu_msg,"22222");
		if(menu_msg!=NULL)
		{
			cJSON *msg=cJSON_GetObjectItem(menu_msg,"msg");
			cJSON *input=cJSON_GetObjectItem(menu_msg,"input");
			if(msg)
			{
				
				if(strcmp(msg->valuestring,"SET_RADAR_DISTANCE")==0)
				{
					api_radar_set_distance_level(API_Para_Read_Int(RADAR_DISTANCE_LEVEL));
				}
			}
		}
	}
	if(msg_type == SETTING_MSG_SettingBtn)
	{
		if(strcmp((char *)data,"radar test")==0)
		{
			if(raradtest_start==0)
			{
				char buff[100];
				raradtest_start=1;
				//raradtest_start_time=time(NULL);
				radar_test_timer = lv_timer_create(radar_test_timer_cb, 1000*30, NULL);
				sprintf(buff,"Rarad test start(%d)",Get_Radar_Status());

				API_TIPS_Close_Ext(radar_tips_haddle);
				radar_tips_haddle=API_TIPS_Ext_Time(buff, 2);
			}
			else
			{
				if(radar_test_timer)
					lv_timer_del(radar_test_timer);
				radar_test_timer=NULL;
				raradtest_start=0;
				API_TIPS_Close_Ext(radar_tips_haddle);
				radar_tips_haddle=API_TIPS_Ext_Time("Rarad test end", 2);
			}
		}
		else if(strcmp((char *)data,"SET_RADAR_DISTANCE")==0)
		{
			api_radar_set_distance_level(API_Para_Read_Int(RADAR_DISTANCE_LEVEL));
		}	
	}
}
void RadarConfigClose(void)
{
	if(raradtest_start)
	{
		raradtest_start=0;
		if(radar_test_timer)
			lv_timer_del(radar_test_timer);
		radar_test_timer=NULL;
	}
}
void RadarStateChange(int state)
{
	//printf("2222222:%s:%d\n",__func__,state);
	vtk_lvgl_lock();
	if(raradtest_start==1)
	{
		MainMenu_Reset_Time();
		
		
		char buff[100];
		sprintf(buff,"radar state is %d",state);
		API_TIPS_Close_Ext(radar_tips_haddle);
		radar_tips_haddle=API_TIPS_Ext_Time(buff, 2);
		BEEP_KEY();
	
	}
	vtk_lvgl_unlock();
}

static void MenuFactoryResroeValid(void* data)
{
	API_TIPS_Ext_Time("Waiting...",10);
	XDRemoteDisconnect();
    	API_Event_By_Name("EventFactoryDefault");
}
static void MenuCardResroe(void* data)
{
	Api_Restore(data);
	API_TIPS_Ext_Time("Resroe success", 2);
}
void SettingDefaultMsgProcess(int msg_type,void* data)
{
	if(msg_type==SETTING_MSG_SettingBtn)
	{
		if(strcmp((char *)data,"FactoryResroe")==0)
		{
			API_MSGBOX("Restore?", 2, MenuFactoryResroeValid, NULL);
		}
		if(strcmp((char *)data,"set_language")==0)
		{
			API_TIPS_Ext_Time("Take effect after exit installer mode", 3);
		}
		if(strcmp((char *)data,"CardBackupToSD")==0)
		{
			//API_TIPS_Ext_Time("Take effect after exit installer mode", 3);
			time_t t;
			struct tm tblock;	
			time(&t); 
			localtime_r(&t, &tblock);
			//tblock=localtime(&t);
			char bak_file[200];
			char *backup_path;
			cJSON *tb_list;
			tb_list=cJSON_CreateArray();
			cJSON_AddItemToArray(tb_list,cJSON_CreateString("CardTable"));
			if(Judge_SdCardLink() == 0)
			{

				API_TIPS_Ext_Time("Please insert SD card", 3);
				cJSON_Delete(tb_list);
				//BEEP_ERROR();
				return;
			}
			else
			{
				backup_path=SDCARD_Backup_Path;
				create_multi_dir(backup_path);
				snprintf(bak_file, 200, "%sBAK.TB.20%02d%02d%02d%02d%02d.tar", backup_path, tblock.tm_year-100,tblock.tm_mon+1,tblock.tm_mday,tblock.tm_hour,tblock.tm_min);
			}
			Api_Backup(tb_list,bak_file);
			cJSON_Delete(tb_list);
			API_TIPS_Ext_Time("Backup success", 2);
		}
		if(strcmp((char *)data,"CardRestoreFromSD")==0)
		{
			if(Judge_SdCardLink() == 0)
			{

				API_TIPS_Ext_Time("Please insert SD card", 3);
				//BEEP_ERROR();
				return;
			}
			cJSON* fileList = cJSON_CreateArray();
			if(GetFileAndDirList(SDCARD_Backup_Path, fileList, 0)==0&&cJSON_GetArraySize(fileList))
			{
				cJSON *fileRecord;
			    cJSON_ArrayForEach(fileRecord, fileList)
			    {
			        cJSON* fileName = cJSON_GetArrayItem(fileRecord, 3);
			        if(cJSON_IsString(fileName) && strcmp(fileName->valuestring, "BAK.TB.tar")==0)
			        {
			            static char temp[100];
					snprintf(temp,100,"Restore %s",fileName->valuestring);
					//Api_Restore(temp);
					API_MSGBOX(temp, 2, MenuCardResroe, temp);
					snprintf(temp,100,"%s%s",SDCARD_Backup_Path,fileName->valuestring);
					cJSON_Delete(fileList);
					return 0;
			        }
			    }
			}
			API_TIPS_Ext_Time("Have no BAK.TB.tar", 3);
			cJSON_Delete(fileList);
		}
		
	}
	
}