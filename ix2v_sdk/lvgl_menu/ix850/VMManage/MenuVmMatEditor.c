#include "menu_utility.h"
#include "MenuHybridMatCreateNew.h"
#include "cJSON.h"
#include "obj_TableSurver.h"
#include "define_string.h"
#include "obj_PublicInformation.h"
#include "utility.h"
#include "task_Event.h"
#include "Common_TableView.h"
#include "obj_IoInterface.h"
#include "define_file.h"
#include "vtk_tcp_auto_reg.h"

#define VM_MAT_EDITOR_PARA		"{\"HeaderEnable\":true,\"CellCallback\":0,\"ColumnWidth\":[120, 200, 140, 200, 200, 200, 200],\"Function\":{\"Name\":[],\"Callback\":0}}}"
#define VM_MAT_EDITOR_VIEW_TEMPALE "{\"VM_ID\":\"\",\"VM_ACC\":\"\",\"VM_PWD\":\"\",\"IX_NAME\":\"\",\"VM_UserPWD\":\"\",\"L_NBR\":\"\",\"G_NBR\":\"\"}"
#define VM_MAT_RECORD  "{\"VM START\":1,\"VM END\":8,\"NAME PREFIX\":\"DH-VM\",\"Describe\":\"List auto\"}" //编辑的记录
static TB_VIWE* VmMatEditorTb = NULL;

typedef struct 
{   
    int imId;
    int lNbr;
    int lNbrLen;
    int gNbr;
    int gNbrLen;
    char name[100];
    char usrPwd[20];
}VM_EDIT_ONE_T;

static VM_EDIT_ONE_T editOne = {.imId = 1, .lNbr = 1, .lNbrLen = 1, .gNbr = 1, .gNbrLen = 1, .name = "DH-VM", .usrPwd = ""};

static cJSON* batchCreateMatRecord = NULL;
static RECORD_CONTROL* batchCreateMatMenu = NULL;
static cJSON* editMatRecord = NULL;
static RECORD_CONTROL* editMatMenu = NULL;

void MenuVM_MAT_Editor_Refresh(void);
static void AddMatMenu(RECORD_CONTROL** menu, const cJSON* record, const cJSON* editEnable, BtnCallback btnCallback, const char* title, char* btnString, ...);
static cJSON *vmAccTable=NULL;
static lv_obj_t *vmQrObj=NULL;

int GetAccountCnt(void)
{
	cJSON *Account=GetJsonFromFile(VM_ACC2_FILE_NAME);
	int cnt = cJSON_GetArraySize(Account);
	cJSON_Delete(Account);
	return cnt;
}
int GetAccountFromCloud(int cnt)
{
	char *mfg_sn = GetSysVerInfo_Sn();
	char* server = API_Para_Read_String2(SIP_SERVER);
	char acc[24];
	char md5_buff[16];
	char pwd[24];
	char managePwd[10];
	int i, ret;
	int accCnt=0;
	cJSON* view = cJSON_CreateArray();
	cJSON *item = NULL;
	for(i=0; i<cnt; i++)
	{
		ret = remote_account_manage( ACCOUNT_MANAGE_CMD_APPLY_NEW_ACCOUNT,server,mfg_sn,acc,NULL );//"47.106.104.38"
		if(ret !=0)
		{
			continue;
		}
		StringMd5_Calculate(acc,md5_buff);
		snprintf(pwd, 24, "%d", (md5_buff[14]<<8) | md5_buff[15]);
		snprintf(managePwd, 10, "%04d", ((md5_buff[12]<<8) | md5_buff[13])%10000);
		ret = remote_account_manage( ACCOUNT_MANAGE_CMD_RESTORE_DEFAULT,server,acc,pwd,NULL );
		printf("GetAccountFromCloud ret=%d acc=%s pwd=%s\n", ret, acc, pwd);
		if(ret == 1)
		{
			item = cJSON_CreateObject();
			cJSON_AddStringToObject(item, "VM_ACC", acc);
			cJSON_AddStringToObject(item, "VM_PWD", pwd);
			cJSON_AddStringToObject(item, "VM_UserPWD", managePwd);
			cJSON_AddItemToArray(view, item);
			accCnt++;
		}
	}
	if(accCnt)
	{
		SetJsonToFile(VM_ACC2_FILE_NAME, view);
		SetJsonToFile(VM_ACC_FILE_NAME, view);
	}
	cJSON_Delete(view);
	API_TIPS_Mode_Close();
	char tempString[100];
	snprintf(tempString, 100, "Get account %d", accCnt);
	API_TIPS_Ext_Time(tempString, 2);
}

int EventMenuVmManageCallback(cJSON *cmd)
{
	char* msg = GetEventItemString(cmd, EVENT_KEY_Message);
	if(!strcmp(msg, "get"))
	{
		GetAccountFromCloud(10);
	}
}

int GetVmAccCnt(void)
{
	if(vmAccTable)
	{
		cJSON_Delete(vmAccTable);
	}
	vmAccTable=GetJsonFromFile(VM_ACC_FILE_NAME);
	return cJSON_GetArraySize(vmAccTable);
}
void EditVmAcc(void)
{
	int edit=0;
	cJSON* view = cJSON_CreateArray();
	cJSON* where = cJSON_CreateObject();
	cJSON_AddNumberToObject(where, "VM_ID", GetEventItemInt(editMatRecord,  "VM_ID"));
	API_TB_SelectBySortByName(TB_NAME_VM_MAT, view, where, 0);
	if(view)
	{
		cJSON *editRecord = cJSON_GetArrayItem(view, 0);
		char* vmAcc = GetEventItemString(editRecord, "VM_ACC");
		printf("EditVmAcc =%s \n", vmAcc);
		if(vmAcc[0] != 0)
		{
			cJSON_DeleteItemFromObjectCaseSensitive(where, "VM_ID");
			cJSON_AddStringToObject(where, "VM_ACC", vmAcc);
			if(API_TB_SelectBySort(vmAccTable, view, where, 0) > 0)
			{
				edit=0;
			}
			else
			{
				edit=1;
				cJSON_AddStringToObject(where, "VM_PWD", GetEventItemString(editRecord, "VM_PWD"));
				cJSON_AddStringToObject(where, "VM_UserPWD", GetEventItemString(editRecord, "VM_UserPWD"));
				API_TB_Add(vmAccTable, where);
			}
		}
	}
	
	printf("EditVmAcc edit=%d \n", edit);
	if(edit)
	{
		ShellCmdPrintJson(vmAccTable);
		SetJsonToFile(VM_ACC_FILE_NAME, vmAccTable);
	}
	cJSON_Delete(where);
	cJSON_Delete(view);
}

int VmAssignment(void)
{
	int accNum = GetVmAccCnt();
	printf("GetVmAccCnt =%d \n", accNum);
	if( accNum == 0 )
	{
		API_TIPS_Ext_Time("Have no account to assign", 2);
		return 0;
	}
	cJSON* view = cJSON_CreateArray();
	cJSON* where = cJSON_CreateObject();
	cJSON_AddNumberToObject(where, "VM_ID", 1);
	cJSON *editRecord;
	int cnt=0;
	API_TB_SelectBySortByName(TB_NAME_VM_MAT, view, NULL, 0);
	if(view)
	{
		cJSON_ArrayForEach(editRecord, view)
		{
			char* vmAcc = GetEventItemString(editRecord, "VM_ACC");
			if(vmAcc[0] != 0)
			{
				continue;
			}
			cJSON* accitem = cJSON_GetArrayItem(vmAccTable, 0);
			if(cJSON_IsObject(accitem))
			{
				cJSON_ReplaceItemInObjectCaseSensitive(where, "VM_ID", cJSON_CreateNumber(GetEventItemInt(editRecord,  "VM_ID")));
				cJSON_ReplaceItemInObjectCaseSensitive(editRecord, "VM_ACC", cJSON_CreateString(GetEventItemString(accitem, "VM_ACC")));
				cJSON_ReplaceItemInObjectCaseSensitive(editRecord, "VM_PWD", cJSON_CreateString(GetEventItemString(accitem, "VM_PWD")));
				cJSON_ReplaceItemInObjectCaseSensitive(editRecord, "VM_UserPWD", cJSON_CreateString(GetEventItemString(accitem, "VM_UserPWD")));
				//ShellCmdPrintJson(editRecord);
				if(API_TB_UpdateByName(TB_NAME_VM_MAT, where, editRecord))
				{
					cnt++;
					cJSON_DeleteItemFromArray(vmAccTable, 0);
				}
			}
		}
	}
	char tempString[100];
	snprintf(tempString, 100, "Account assigned %d", cnt);
	API_TIPS_Ext_Time(tempString, 2);
	if(cnt)
	{
		MenuVM_MAT_Editor_Refresh();
		API_TableModificationInform(TB_NAME_VM_MAT, NULL);
		SetJsonToFile(VM_ACC_FILE_NAME, vmAccTable);
	}
	cJSON_Delete(where);
	cJSON_Delete(view);
	return cnt;
} 


static void vm_del_one(void* data)
{
	EditVmAcc();
	cJSON* where = cJSON_CreateObject();
	cJSON_AddNumberToObject(where, "VM_ID", GetEventItemInt(editMatRecord,  "VM_ID"));
	if(API_TB_DeleteByName(TB_NAME_VM_MAT, where))
	{
		DelVmQrcode(); 
		RC_MenuDelete(&editMatMenu);
		API_TableModificationInform(TB_NAME_VM_MAT, NULL);
		MenuVM_MAT_Editor_Refresh();
		API_TIPS_Ext_Time("Delete success", 2);
		API_Event_By_Name(Event_MAT_Update);
	}
	else
	{
		API_TIPS_Ext_Time("Delete error", 2);
	}
	cJSON_Delete(editMatRecord);
	editMatRecord = NULL;
	cJSON_Delete(where);
}
static void EditMatFunction(const char* type, void* user_data)
{
	if(editMatRecord)
	{
		if(!strcmp(type, "Save"))
		{
			cJSON* where = cJSON_CreateObject();
			cJSON_AddNumberToObject(where, "VM_ID", GetEventItemInt(editMatRecord,  "VM_ID"));
			if(API_TB_UpdateByName(TB_NAME_VM_MAT, where, editMatRecord))
			{
				DelVmQrcode(); 
				RC_MenuDelete(&editMatMenu);
				API_TableModificationInform(TB_NAME_VM_MAT, NULL);
				MenuVM_MAT_Editor_Refresh();
				API_TIPS_Ext_Time("Save success", 2);
				cJSON_Delete(editMatRecord);
				editMatRecord = NULL;
				API_Event_By_Name(Event_MAT_Update);
			}
			else
			{
				API_TIPS_Ext_Time("Save error", 2);
			}
			cJSON_Delete(where);
		}
		else if(!strcmp(type, "Delete"))
		{
			API_MSGBOX("Delete?", 2, vm_del_one, NULL);			
		}
		else if(!strcmp(type, "Test"))
		{
			cJSON* view = cJSON_CreateArray();
			cJSON* where = cJSON_CreateObject();
			cJSON_AddNumberToObject(where, "VM_ID", GetEventItemInt(editMatRecord,  "VM_ID"));
			if(API_TB_SelectBySortByName(TB_NAME_VM_MAT, view, where, 0)>0)
			{
				char* vmAcc = GetEventItemString(cJSON_GetArrayItem(view, 0), "VM_ACC");
				if(vmAcc[0] != 0)
				{
					API_SettingLongTip("Waitting...");
					cJSON *call_event=cJSON_CreateObject();
					cJSON_AddStringToObject(call_event, EVENT_KEY_EventName,EventBECall);
					cJSON_AddStringToObject(call_event, "CallType","SipCall");
					cJSON_AddStringToObject(call_event, "Call_Tar_SipAcc",vmAcc);
					API_Event_Json(call_event);
					cJSON_Delete(call_event);
				}
			}
			cJSON_Delete(where);
			cJSON_Delete(view);			
		}
	}
}
static void EditMatCancel(const char* type, void* user_data)
{
	CloseVmMatEditRecordMenu();
}

lv_obj_t *VmAccQrDisplay(char *qr_str)
{
	vmQrObj = lv_obj_create(editMatMenu->scr);
	lv_obj_remove_style_all(vmQrObj);
	lv_obj_set_size(vmQrObj,  210, 210);
	lv_obj_align(vmQrObj, LV_ALIGN_BOTTOM_MID, 0, -140);
	lv_obj_set_style_bg_color(vmQrObj, lv_color_hex(0x41454D), 0);
	lv_obj_set_style_bg_opa(vmQrObj, LV_OPA_100, 0);
	lv_color_t qr_bg_color=lv_color_make(0xff, 0xff, 0xef); // lv_palette_lighten(LV_PALETTE_LIGHT_BLUE, 5);
	lv_color_t qr_fg_color=lv_color_make(0x00, 0x00, 0x10); //lv_palette_darken(LV_PALETTE_BLUE, 4);
    lv_obj_t* qr = lv_qrcode_create(vmQrObj, 200, qr_fg_color, qr_bg_color);
    /*Set data*/
    lv_qrcode_update(qr, qr_str, strlen(qr_str));
    lv_obj_center(qr);
	
    /*Add a border with bg_color*/
    lv_obj_set_style_border_color(qr, qr_bg_color, 0);
    lv_obj_set_style_border_width(qr, 5, 0);

}
void CreateVmQrcode(char *acc, char *pwd)
{
	if(acc[0] == 0)
	{
		return;
	}
	if(editMatMenu)
	{
		char *qr_code=create_sip_phone_qrcode(acc,pwd);
		VmAccQrDisplay(qr_code);
		free(qr_code);
	}
}
void DelVmQrcode(void)
{
	if(vmQrObj)
	{
		lv_obj_del(vmQrObj);
	}
	vmQrObj=NULL;
}

void EditVmMatRecord(cJSON* record)
{
	const char* editEnable[] = {"IX_NAME", "L_NBR", "G_NBR", "VM_UserPWD"};
	if(editMatRecord)
	{
		cJSON_Delete(editMatRecord);
	}
	editMatRecord =  cJSON_CreateObject();
	
	cJSON_AddNumberToObject(editMatRecord, "VM_ID", GetEventItemInt(record,  "VM_ID"));
	cJSON_AddStringToObject(editMatRecord, "IX_NAME", GetEventItemString(record,  "IX_NAME"));
	cJSON_AddStringToObject(editMatRecord, "L_NBR", GetEventItemString(record,  "L_NBR"));
	cJSON_AddStringToObject(editMatRecord, "G_NBR", GetEventItemString(record,  "G_NBR"));
	cJSON_AddStringToObject(editMatRecord, "VM_UserPWD", GetEventItemString(record,  "VM_UserPWD"));

	cJSON* editorTable =  cJSON_CreateStringArray(editEnable, sizeof(editEnable)/sizeof(editEnable[0]));
	AddMatMenu(&editMatMenu, editMatRecord, editorTable, EditMatFunction, "Edit VM", "Save", "Delete", "Test", NULL);
	cJSON_Delete(editorTable);
	CreateVmQrcode(GetEventItemString(record,  "VM_ACC"), GetEventItemString(record,  "VM_PWD"));
}

static void ResCellClick(int line, int col)
{
	if(VmMatEditorTb)
	{
		cJSON* record = cJSON_GetArrayItem(VmMatEditorTb->view, line);
		if(cJSON_IsObject(record))
		{
			EditVmMatRecord(record);
		}
	}
}

static void ResReturnCallback(void)
{
	MenuVM_MAT_Editor_Close();
}

static void BatchAddMatFunction(const char* type, void* user_data)
{
    if(!strcmp(type, "Batch add"))
    {
		cJSON* record;
		cJSON* view = CreateVmMatRecords(batchCreateMatRecord, 256);
		if(cJSON_GetArraySize(view))
		{
			int matRecords = 0;
			char tempString[200];
			char translate[100];

			cJSON_ArrayForEach(record, view)
			{
				if(API_TB_AddByName(TB_NAME_VM_MAT, record))
				{
					matRecords++;
				}
			}
			snprintf(tempString, 200, "%s %d", get_language_text2("Batch add vm list", translate, 100), matRecords);
			API_TIPS_Ext_Time(tempString, 2);
			if(matRecords)
			{
				API_TableModificationInform(TB_NAME_VM_MAT, NULL);
				API_TB_SaveByName(TB_NAME_VM_MAT);
				API_Event_By_Name(Event_MAT_Update);
				MenuVM_MAT_Editor_Refresh();
			}
		}
		cJSON_Delete(view);
    }
}

static void AddMatMenu(RECORD_CONTROL** menu, const cJSON* record, const cJSON* editEnable, BtnCallback btnCallback, const char* title, char* btnString, ...)
{
	char* name;
	const char* editCheck = "{\"Check\":{\"VM_ID\":\"(^[1-9]$)|(^[1-5][0-9]$)|(^6[0-4]$)\",\"VM_UserPWD\":\"^[0-9]{4}$\",\"L_NBR\":\"^[0-9]{0,6}$\",\"G_NBR\":\"^[0-9]{0,10}$\"},\"Translate\":{}}";
	cJSON* editorTable =  cJSON_Parse(editCheck);

	cJSON* func = cJSON_CreateObject();
	cJSON* btnname = cJSON_AddArrayToObject(func,"BtnName");

	va_list valist;
	va_start(valist, btnString);
	for(name = btnString; name; name = va_arg(valist, char*))
	{
		cJSON_AddItemToArray(btnname, cJSON_CreateString(name));
	}
	va_end(valist);

	cJSON_AddNumberToObject(func, "BtnCallback", (int)btnCallback);
	cJSON_AddNumberToObject(func, "BackCallback", (int)EditMatCancel);

    RC_MenuDisplay(menu, record, editEnable, editorTable, func, title);
    cJSON_Delete(editorTable);
    cJSON_Delete(func);
}

static void EditOneRecordProcessingInitialValues(const cJSON* lastRecord)
{
	editOne.imId = GetEventItemInt(lastRecord,  "VM_ID");
	char* tempString = GetEventItemString(lastRecord,  "L_NBR");
	editOne.lNbrLen = strlen(tempString);
	editOne.lNbr = atoi(tempString);

	tempString = GetEventItemString(lastRecord,  "G_NBR");
	editOne.gNbrLen = strlen(tempString);
	editOne.gNbr = atoi(tempString);

	if(++editOne.imId > 64)
	{
		editOne.imId = 1;
	}
	editOne.gNbr++;
	editOne.lNbr++;
	snprintf(editOne.name, 100, "%s", GetEventItemString(lastRecord,  "IX_NAME"));
	snprintf(editOne.usrPwd, 20, "%s", GetEventItemString(lastRecord,  "VM_UserPWD"));
}

static void AddMatFunction(const char* type, void* user_data)
{
    if(!strcmp(type, "Save"))
	{
		if(editMatRecord)
		{
			int imId = GetEventItemInt(editMatRecord,  "VM_ID");
			char ixAddr[11];
			snprintf(ixAddr, 11, "009900%02d49", imId);
			cJSON_AddStringToObject(editMatRecord, "IX_ADDR", ixAddr);
			cJSON_AddStringToObject(editMatRecord, "SYS_TYPE", "DH-APT");
			cJSON_AddStringToObject(editMatRecord, "IX_TYPE", "VM");
			cJSON_AddStringToObject(editMatRecord, "VM_ACC", "");
			cJSON_AddStringToObject(editMatRecord, "VM_PWD", "");

			if(API_TB_AddByName(TB_NAME_VM_MAT, editMatRecord))
			{
				RC_MenuDelete(&editMatMenu);
				API_TableModificationInform(TB_NAME_VM_MAT, NULL);
				EditOneRecordProcessingInitialValues(editMatRecord);
				MenuVM_MAT_Editor_Refresh();
				API_TIPS_Ext_Time("Add success", 2);
				cJSON_Delete(editMatRecord);
				editMatRecord = NULL;
				API_Event_By_Name(Event_MAT_Update);
            }
			else
			{
				API_TIPS_Ext_Time("Add error", 2);
			}
		}
	}
}

static void FunctionClick(const char* funcName)
{
	if(funcName)
	{
		if(!strcmp(funcName, "Add"))
		{
			char format[20];
			char temp[100];
			const char* editEnable[] = {"VM_ID", "IX_NAME", "L_NBR", "G_NBR", "VM_UserPWD"};

			if(editMatRecord)
			{
				cJSON_Delete(editMatRecord);
			}
			editMatRecord =  cJSON_CreateObject();
			cJSON_AddNumberToObject(editMatRecord, "VM_ID", editOne.imId);
			cJSON_AddStringToObject(editMatRecord, "IX_NAME", editOne.name);

			snprintf(format, 20, "%%0%dd", editOne.lNbrLen);
			snprintf(temp, 100, format, editOne.lNbr);
			cJSON_AddStringToObject(editMatRecord, "L_NBR", temp);

			snprintf(format, 20, "%%0%dd", editOne.gNbrLen);
			snprintf(temp, 100, format, editOne.gNbr);
			cJSON_AddStringToObject(editMatRecord, "G_NBR", temp);
			cJSON_AddStringToObject(editMatRecord, "VM_UserPWD", editOne.usrPwd);

			cJSON* editorTable =  cJSON_CreateStringArray(editEnable, sizeof(editEnable)/sizeof(editEnable[0]));
			AddMatMenu(&editMatMenu, editMatRecord, editorTable, AddMatFunction, "Add VM", "Save", NULL);
			cJSON_Delete(editorTable);
		}
		else if (!strcmp(funcName, "Batch add"))
		{
			if(batchCreateMatRecord)
			{
				cJSON_Delete(batchCreateMatRecord);
			}
			batchCreateMatRecord = cJSON_Parse(VM_MAT_RECORD);
			BatchCreateVmMatMenu(batchCreateMatRecord, &batchCreateMatMenu, NULL, BatchAddMatFunction, "Batch add VM", "Batch add", NULL);
		}
		else if(!strcmp(funcName, "ACC get"))
		{
			if(GetAccountCnt()==0)
			{
				API_TIPS_Mode("Wait for account get from cloud");
				API_Event_NameAndMsg(EventVmManage, "get", NULL);
				//GetAccountFromCloud(5);
			}
			else
			{
				API_TIPS_Ext_Time("Have account already", 2);
			}
		}
		else if(!strcmp(funcName, "ACC assign"))
		{
			VmAssignment();
		}
	}
}

void MenuVM_MAT_Editor_Refresh(void)
{
	if(VmMatEditorTb)
	{
		cJSON* view = cJSON_CreateArray();
		cJSON_AddItemToArray(view, cJSON_Parse(VM_MAT_EDITOR_VIEW_TEMPALE));
		API_TB_SelectBySortByName(TB_NAME_VM_MAT, view, NULL, 0);
		cJSON_DeleteItemFromArray(view, 0);
		TB_MenuDisplay(&VmMatEditorTb, view, "VM editor", NULL);
		cJSON_Delete(view);
	}
}

void MenuVM_MAT_Editor_Init(void)
{
	cJSON* para = cJSON_Parse(VM_MAT_EDITOR_PARA);
	cJSON_ReplaceItemInObjectCaseSensitive(para, "CellCallback", cJSON_CreateNumber((int)ResCellClick));
	cJSON_AddNumberToObject(para, "ReturnCallback", (int)ResReturnCallback);

	cJSON* func = cJSON_CreateObject();
	cJSON* funcName = cJSON_AddArrayToObject(func, "Name");
	cJSON_AddItemToArray(funcName, cJSON_CreateString("Add"));
	cJSON_AddItemToArray(funcName, cJSON_CreateString("Batch add"));
	cJSON_AddItemToArray(funcName, cJSON_CreateString("ACC get"));
	cJSON_AddItemToArray(funcName, cJSON_CreateString("ACC assign"));
	cJSON_AddNumberToObject(func, "Callback", (int)FunctionClick);
	cJSON_ReplaceItemInObjectCaseSensitive(para, "Function", func);

	cJSON* view = cJSON_CreateArray();
	cJSON_AddItemToArray(view, cJSON_Parse(VM_MAT_EDITOR_VIEW_TEMPALE));
	API_TB_SelectBySortByName(TB_NAME_VM_MAT, view, NULL, 0);
	cJSON_DeleteItemFromArray(view, 0);

	TB_MenuDisplay(&VmMatEditorTb, view, "VM editor", para);
	cJSON_Delete(view);
	cJSON_Delete(para);
}

void CloseVmMatEditRecordMenu(void)
{     
	DelVmQrcode(); 
    if(editMatMenu)
    {
        RC_MenuDelete(&editMatMenu);
    }

    if(editMatRecord)
	{
		cJSON_Delete(editMatRecord);
		editMatRecord = NULL;
	}
}

void MenuVM_MAT_Editor_Close(void)
{
	CloseVmMatEditRecordMenu();

	RC_MenuDelete(&batchCreateMatMenu);

	if(batchCreateMatRecord)
	{
		cJSON_Delete(batchCreateMatRecord);
		batchCreateMatRecord = NULL;
	}
	TB_MenuDelete(&VmMatEditorTb);
	API_Event_NameAndMsg(EventMenuSettingProcess, "SettingRefresh", NULL);
}