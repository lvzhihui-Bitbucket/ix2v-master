#include "Common_TableView.h"
#include "define_file.h"
#include "utility.h"
#include "task_Event.h"
#include "obj_TableSurver.h"
#include "task_Beeper.h"
#include "lv_demo_msg.h"
#include "obj_CommonSelectMenu.h"

typedef struct 
{   
    char* flag;
	char* folder;
} VM_MAT_FLAG_TO_FOLDER;

#define VM_MAT_SELECT_PARA		"{\"HeaderEnable\":true,\"CellCallback\":0,\"ColumnWidth\":[360, 120, 360]}"
#define VM_MAT_SELECT_FIELD_NAME			"NAME"
#define VM_MAT_SELECT_FIELD_DESCRIBE		"DESCRIBE"
#define VM_MAT_SELECT_FIELD_SOURCE			"SOURCE"

static TB_VIWE* vmMatChose = NULL;
static const VM_MAT_FLAG_TO_FOLDER VM_MAT_FOLDER[] = {
	{"CUR", TB_VM_MAT_FILE_NAME},
	{"FW", Factory_VM_MAT_Folder},
	{"CUS", Customerized_VM_MAT_Folder},
	{"BAK", Backup_VM_MAT_Folder},
	{"SD Card", SDCARD_VM_MAT_Folder},
};

static int clickLine;
static const int VM_MAT_FOLDER_NUM = sizeof(VM_MAT_FOLDER)/sizeof(VM_MAT_FOLDER[0]);

static void VmMatSelectFilterCallback(const cJSON* filter);
static void FunctionClick(const char* funcName);
void MenuVM_MAT_Select_Close(void);

static const  char* FlagToFolder(const char *flag)
{
	int i;
	for(i = 0; i < VM_MAT_FOLDER_NUM; i++)
	{
		if(flag && VM_MAT_FOLDER[i].flag && !strcmp(flag, VM_MAT_FOLDER[i].flag))
		{
			return VM_MAT_FOLDER[i].folder;
		}
	}
    return NULL;
}

static int StrcmpEnd(const char *str1, const char *str2)
{
	int ret = 0;
	if(str1 == str2)
	{
		return ret;
	}

	if(str1 && str2)
	{
		int start1 = strlen(str1);
		int start2 = strlen(str2);
	
		while (start1 >= 0 && start2 >= 0)
		{
			if (str1[start1] != str2[start2])
			{
				ret = str1[start1] - str2[start2];
				return ret;
			}
			start1--;
			start2--;
		}
	}
	else
	{
		ret = (str1 ? 1 : -1);
		return ret;
	}

    return ret;
}

static void VmMatUpdate(void* data)
{
	int* pLine = (int*)data;
	if(vmMatChose)
	{
		
		cJSON* record = cJSON_GetArrayItem(vmMatChose->view, *pLine);
		if(cJSON_IsObject(record))
		{
			char temp[500];
			char* fileName = GetEventItemString(record,  VM_MAT_SELECT_FIELD_NAME);
			char* describe = GetEventItemString(record,  VM_MAT_SELECT_FIELD_DESCRIBE);
			char* source = GetEventItemString(record,  VM_MAT_SELECT_FIELD_SOURCE);
			char* path = FlagToFolder(source);
			int fileType = CheckFileType(path);
			if(fileType == 1)
			{
				snprintf(temp, 500, "%s/%s", path, fileName);
			}
			else if(fileType == 2)
			{
				snprintf(temp, 500, "%s", path);
			}
			else
			{
				API_TIPS_Ext_Time("Update error", 1);
				return;
			}

			if(source && !strcmp(source, "SD Card"))
			{
				MakeDir(Customerized_VM_MAT_Folder);
				CopyFile(temp, Customerized_VM_MAT_Folder);
			}

			CopyFile(temp, TB_VM_MAT_FILE_NAME);
			API_TB_ReloadByName(TB_NAME_VM_MAT,NULL);
			API_Event_By_Name(Event_MAT_Update);
			snprintf(temp, 500, "%s update", ((describe && describe[0]) ? describe : fileName));
			API_TIPS_Ext_Time(temp, 1);
		}
		MenuVM_MAT_Select_Close();
	}
}

static void VmMatCellClick(int line, int col)
{
    dprintf("line = %d, col =%d\n", line, col);
	if(vmMatChose)
	{
		cJSON* record = cJSON_GetArrayItem(vmMatChose->view, line);
		if(cJSON_IsObject(record))
		{
			char* source = GetEventItemString(record,  VM_MAT_SELECT_FIELD_SOURCE);
			if(source && !strcmp(source, "CUR"))
			{
				TB_MenuDelete(&vmMatChose);
			}
			else
			{
				char temp[500];
				clickLine = line;
				char* fileName = GetEventItemString(record,  VM_MAT_SELECT_FIELD_NAME);
				char* describe = GetEventItemString(record,  VM_MAT_SELECT_FIELD_DESCRIBE);
				snprintf(temp, 500, "%s update", ((describe && describe[0]) ? describe : fileName));
				API_MSGBOX(temp, 2, VmMatUpdate, &clickLine);
			}
		}
	}
}


static cJSON* VmMatSelectCreateView(const char* flag)
{   
    cJSON* view = cJSON_CreateArray();
	if(!flag)
	{
		return view;
	}

	int folderIndex;
	for(folderIndex = 0; folderIndex < VM_MAT_FOLDER_NUM; folderIndex++)
	{
		if(!strcmp(flag, "ALL") || !strcmp(flag, VM_MAT_FOLDER[folderIndex].flag))
		{
			cJSON* file;
			cJSON* fileList = cJSON_CreateArray();	
			int fileType = CheckFileType(VM_MAT_FOLDER[folderIndex].folder);
			if(fileType == 1)
			{
				GetFileAndDirList(VM_MAT_FOLDER[folderIndex].folder, fileList, 0);
			}
			else if(fileType == 2)
			{
				char* fileName = GetFilenameFromPath(VM_MAT_FOLDER[folderIndex].folder);
				file = cJSON_CreateArray();
				cJSON_AddItemToArray(file, cJSON_CreateString(fileName));
				cJSON_AddItemToArray(file, cJSON_CreateString(fileName));
				cJSON_AddItemToArray(file, cJSON_CreateString(fileName));
				cJSON_AddItemToArray(file, cJSON_CreateString(fileName));
				cJSON_AddItemToArray(fileList, file);
			}
			else
			{
				continue;
			}

			cJSON_ArrayForEach(file, fileList)
			{
				cJSON* fileName = cJSON_GetArrayItem(file, 3);
				if(cJSON_IsString(fileName))
				{
					if((!StrcmpEnd(fileName->valuestring, ".json") || !StrcmpEnd(fileName->valuestring, ".JSON")))
					{
						char temp[500];
						if(fileType == 1)
						{
							snprintf(temp, 500, "%s/%s", VM_MAT_FOLDER[folderIndex].folder, fileName->valuestring);
						}
						else if(fileType == 2)
						{
							snprintf(temp, 500, "%s", VM_MAT_FOLDER[folderIndex].folder);
						}
						else
						{
							continue;
						}

						cJSON* res = GetJsonFromFile(temp);
						char* disc = GetEventItemString(cJSON_GetObjectItemCaseSensitive(res, TB_DESC), "MAT_TB_TB_DESC");
						cJSON* record = cJSON_CreateObject();
						cJSON_AddStringToObject(record, VM_MAT_SELECT_FIELD_DESCRIBE, disc);
						cJSON_AddStringToObject(record, VM_MAT_SELECT_FIELD_SOURCE, VM_MAT_FOLDER[folderIndex].flag);
						cJSON_AddStringToObject(record, VM_MAT_SELECT_FIELD_NAME, fileName->valuestring);
						cJSON_AddItemToArray(view, record);
						cJSON_Delete(res);
					}
				}
			}
			cJSON_Delete(fileList);
		}
	}
	return view;
}

static void VmMatSelectReturnCallback(void)
{
	MenuVM_MAT_Select_Close();
}

static cJSON* VmMatSelectCreatePara(const char* flag)
{   
    cJSON* fliter = cJSON_CreateObject();
    cJSON_AddStringToObject(fliter, "Name", "VM path");
    cJSON_AddStringToObject(fliter, "Value", flag);
    cJSON_AddNumberToObject(fliter, "Callback", (int)VmMatSelectFilterCallback);
	cJSON* para = cJSON_Parse(VM_MAT_SELECT_PARA);
    cJSON_AddItemToObject(para,"Filter", fliter);
	cJSON_ReplaceItemInObjectCaseSensitive(para, "CellCallback", cJSON_CreateNumber((int)VmMatCellClick));
	cJSON_AddNumberToObject(para, "ReturnCallback", (int)VmMatSelectReturnCallback);
	
	cJSON* func = cJSON_CreateObject();
	cJSON* funcName = cJSON_AddArrayToObject(func, "Name");
	cJSON_AddItemToArray(funcName, cJSON_CreateString("Delete BAK"));
	cJSON_AddItemToArray(funcName, cJSON_CreateString("Delete CUS"));
	cJSON_AddNumberToObject(func, "Callback", (int)FunctionClick);
	cJSON_AddItemToObject(para, "Function", func);

	return para;
}

static void VmMatSelectFilterProcess(const char* chose, void* userdata)
{   
    if(!chose)
    {
        return;
    }

    if(!strcmp("ALL",chose) || !strcmp("CUR",chose) || !strcmp("FW",chose) || !strcmp("BAK",chose) || !strcmp("CUS",chose) || !strcmp("SD Card",chose))
	{
        cJSON* para = VmMatSelectCreatePara(chose);
		cJSON* view = VmMatSelectCreateView(chose);
		TB_MenuDisplay(&vmMatChose, view, "VM choose", para);
		cJSON_Delete(view);
		cJSON_Delete(para);
    }
}

static void VmMatSelectFilterCallback(const cJSON* filter)
{   
    cJSON* value = cJSON_GetObjectItem(filter,"Value");
    static CHOSE_MENU_T callback = {.callback = 0, .cbData = NULL};
	callback.callback = VmMatSelectFilterProcess;
	callback.cbData = NULL;
    cJSON* choseList = cJSON_CreateArray();
    cJSON_AddItemToArray(choseList, cJSON_CreateString("ALL"));
    cJSON_AddItemToArray(choseList, cJSON_CreateString("CUR"));
    cJSON_AddItemToArray(choseList, cJSON_CreateString("FW"));
    cJSON_AddItemToArray(choseList, cJSON_CreateString("BAK"));
    cJSON_AddItemToArray(choseList, cJSON_CreateString("CUS"));
    cJSON_AddItemToArray(choseList, cJSON_CreateString("SD Card"));
    char* choseString = cJSON_IsString(value) ? value->valuestring : NULL;
    CreateChooseMenu("VM path", choseList, choseString, &callback);
    cJSON_Delete(choseList);
}


static void VmMatDeleteBak(void* data)
{
	DeleteFileProcess(Backup_VM_MAT_Folder, "*.json");
	DeleteFileProcess(Backup_VM_MAT_Folder, "*.JSON");
	API_TIPS_Ext_Time("Delete BAK", 1);
	TB_MenuDelete(&vmMatChose);
}

static void VmMatDeleteCus(void* data)
{
	DeleteFileProcess(Customerized_VM_MAT_Folder, "*.json");
	DeleteFileProcess(Customerized_VM_MAT_Folder, "*.JSON");
	API_TIPS_Ext_Time("Delete CUS", 1);
	TB_MenuDelete(&vmMatChose);
}

static void FunctionClick(const char* funcName)
{
	if(funcName)
	{
		if(!strcmp(funcName, "Delete BAK"))
		{
			API_MSGBOX("Delete BAK", 2, VmMatDeleteBak, NULL);
		}
		else if(!strcmp(funcName, "Delete CUS"))
		{
			API_MSGBOX("Delete CUS", 2, VmMatDeleteCus, NULL);
		}
	}
}



void MenuVM_MAT_Select_Init(void)
{
	VmMatSelectFilterProcess("ALL", NULL);
}

void MenuVM_MAT_Select_Close(void)
{
	TB_MenuDelete(&vmMatChose);
	API_Event_NameAndMsg(EventMenuSettingProcess, "SettingRefresh", NULL);
}