#include "cJSON.h"
#include "Common_Record.h"
#include "menu_utility.h"
#include "MenuHybridMatCreateNew.h"
#include "cJSON.h"
#include "obj_TableSurver.h"
#include "define_string.h"
#include "obj_PublicInformation.h"
#include "utility.h"
#include "task_Event.h"
#include "define_file.h"
#include "obj_IoInterface.h"

#define VM_MAT_IM_START  				"VM START"
#define VM_MAT_IM_END  				"VM END"
#define VM_MAT_NAME_PREFIX  			"NAME PREFIX"
#define VM_MAT_DESC  					"Describe"

#define VM_MAT_RECORD  "{\"VM START\":1,\"VM END\":8,\"NAME PREFIX\":\"DH-VM\"}" //编辑的记录
#define VM_MAT_PARA  "{\"Check\":{\"VM START\":\"(^[1-9]$)|(^[1-5][0-9]$)|(^6[0-4]$)\", \"VM END\":\"(^[1-9]$)|(^[1-5][0-9]$)|(^6[0-4]$)\"},\"Translate\":{}}" //编辑的记录校验


static cJSON* batchCreateVmMatRecord = NULL;
static RECORD_CONTROL* batchCreateVmMatMenu = NULL;

void MenuVM_MAT_Create_Close(void);

void BatchCreateVmMatMenu(cJSON* record, RECORD_CONTROL** menu, BtnCallback cancelCallback, BtnCallback btnCallback, const char* title, char* btnString, ...)
{
	char* name;
	//可编辑字段
	const char* editEnable[] = {VM_MAT_IM_START, VM_MAT_IM_END, VM_MAT_NAME_PREFIX, VM_MAT_DESC};
	cJSON* editorTable =  cJSON_CreateStringArray(editEnable, sizeof(editEnable)/sizeof(editEnable[0]));
	cJSON* resEditordescription = cJSON_Parse(VM_MAT_PARA);

	cJSON* func = cJSON_CreateObject();
	cJSON* btnname = cJSON_AddArrayToObject(func,"BtnName");

	va_list valist;
	va_start(valist, btnString);
	for(name = btnString; name; name = va_arg(valist, char*))
	{
		cJSON_AddItemToArray(btnname, cJSON_CreateString(name));
	}
	va_end(valist);

	cJSON_AddNumberToObject(func, "BtnCallback", (int)btnCallback);
	cJSON_AddNumberToObject(func, "BackCallback", (int)cancelCallback);

    RC_MenuDisplay(menu, record, editorTable, resEditordescription, func, title);
    cJSON_Delete(editorTable);
    cJSON_Delete(resEditordescription);
    cJSON_Delete(func);
}

cJSON* CreateVmMatRecords(const cJSON* para, int maxRecordNbr)
{
	cJSON* retView = NULL;
	char tempString[100];
	int imStart = GetEventItemInt(para,  VM_MAT_IM_START);
	int imEnd = GetEventItemInt(para,  VM_MAT_IM_END);
	char* namePrefix = GetEventItemString(para,  VM_MAT_NAME_PREFIX);
	int index;
	int recordNbr = 0;

	#define VM_MAT_IM_MAX  				64
	#define VM_MAT_IM_MIN  				1

	retView = cJSON_CreateArray();

	if(imStart >= 1 && imEnd <= VM_MAT_IM_MAX && imStart >= 1 && imEnd <= VM_MAT_IM_MAX)
	{
		for(index = imStart; index <= imEnd && recordNbr <= maxRecordNbr; index++, recordNbr++)
		{
			cJSON* record = cJSON_CreateObject();
			cJSON_AddNumberToObject(record, "VM_ID", index);
			cJSON_AddStringToObject(record, "SYS_TYPE", "DH-APT");
			snprintf(tempString, 100, "009900%02d49", index);
			cJSON_AddStringToObject(record, "IX_ADDR", tempString);
			cJSON_AddStringToObject(record, "IX_TYPE", "VM");
			snprintf(tempString, 100, "%d", index);
			cJSON_AddStringToObject(record, "G_NBR", tempString);
			cJSON_AddStringToObject(record, "L_NBR", tempString);
			snprintf(tempString, 100, "%s%d", namePrefix, index);
			cJSON_AddStringToObject(record, "IX_NAME", tempString);
			cJSON_AddStringToObject(record, "VM_ACC", "");
			cJSON_AddStringToObject(record, "VM_PWD", "");
			cJSON_AddStringToObject(record, "VM_UserPWD", "");
			cJSON_AddItemToArray(retView, record);
		}
	}
	return retView;
}

static void CreateNewVmMatCancel(const char* type, void* user_data)
{  
	MenuVM_MAT_Create_Close();
}

static void VmMatClearAndRecreate(const cJSON* para)
{
	cJSON* record;
	cJSON* view = CreateVmMatRecords(para, 256);
	if(cJSON_GetArraySize(view))
	{
		int satRecords = 0;
		char tempString[200];
		char targetFile[200];
		char translate[100];

		API_TB_SaveByName(TB_NAME_VM_MAT);
		MakeDir(Backup_VM_MAT_Folder);
		snprintf(targetFile, 200, "%s/VM_%s.json", Backup_VM_MAT_Folder, GetCurrentTime(tempString, 100, "%Y-%m-%d_%H:%M:%S"));
		CopyFile(TB_VM_MAT_FILE_NAME, targetFile);
		if(GetAccountCnt())
			CopyFile(VM_ACC2_FILE_NAME, VM_ACC_FILE_NAME);
		API_TB_DeleteByName(TB_NAME_VM_MAT, NULL);
		cJSON_ArrayForEach(record, view)
		{
			if(API_TB_AddByName(TB_NAME_VM_MAT, record))
			{
				satRecords++;
			}
		}

		snprintf(tempString, 200, "%s %d", get_language_text2("Recreate MAT list", translate, 100), satRecords);
		API_TIPS_Ext_Time(tempString, 1);
		API_TableModificationInform(TB_NAME_VM_MAT, GetEventItemString(para, VM_MAT_DESC));
		API_TB_SaveByName(TB_NAME_VM_MAT);
		API_Event_By_Name(Event_MAT_Update);
	}
	cJSON_Delete(view);

	MenuVM_MAT_Create_Close();
} 

static void CreateNewVmMatFunction(const char* type, void* user_data)
{
    if(!strcmp(type, "Create"))
    {
		API_MSGBOX("About to clear VM and recreate", 2, VmMatClearAndRecreate, batchCreateVmMatRecord);
    }
} 


void MenuVM_MAT_Create_Init(void)
{
	char tempString[100];
	if(batchCreateVmMatRecord)
	{
		cJSON_Delete(batchCreateVmMatRecord);
	}
	batchCreateVmMatRecord = cJSON_Parse(VM_MAT_RECORD);
	cJSON_AddStringToObject(batchCreateVmMatRecord, "Describe", GetCurrentTime(tempString, 100, "%Y-%m-%d_%H:%M"));
	BatchCreateVmMatMenu(batchCreateVmMatRecord, &batchCreateVmMatMenu, CreateNewVmMatCancel, CreateNewVmMatFunction, "VM create new", "Create", NULL);
}

void MenuVM_MAT_Create_Close(void)
{
	if(batchCreateVmMatMenu)
	{
		RC_MenuDelete(&(batchCreateVmMatMenu));
		batchCreateVmMatMenu = NULL;
	}

	if(batchCreateVmMatRecord)
	{
		cJSON_Delete(batchCreateVmMatRecord);
		batchCreateVmMatRecord = NULL;
	}
	API_Event_NameAndMsg(EventMenuSettingProcess, "SettingRefresh", NULL);
}