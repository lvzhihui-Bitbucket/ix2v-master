﻿#include "cJSON.h"
#include "Common_Record.h"
#include "menu_utility.h"
#include "cJSON.h"
#include "obj_TableSurver.h"
#include "define_string.h"
#include "obj_PublicInformation.h"
#include "utility.h"
#include "task_Event.h"
#include "define_file.h"
#include "obj_IoInterface.h"

static cJSON* editVmRecord = NULL;
static RECORD_CONTROL* editVmMenu = NULL;
static cJSON* viewVmRecord = NULL;
static lv_obj_t *vmUserQrObj=NULL;
static int vmUserId=0;

static void SaveVmUnlockPwd(void)
{
	char* pwd = GetEventItemString(editVmRecord, "LOCK_PWD");
	if(pwd[0] != 0)
	{
		cJSON* where=cJSON_CreateObject();
		cJSON_AddStringToObject(where,"IX_ADDR",GetEventItemString(viewVmRecord,  "IX_ADDR"));
		cJSON *pwd_item=cJSON_CreateObject();
		cJSON_AddStringToObject(pwd_item,"IX_ADDR", GetEventItemString(viewVmRecord,  "IX_ADDR"));
		cJSON_AddStringToObject(pwd_item,"L_NBR", GetEventItemString(editVmRecord,  "L_NBR"));
		cJSON_AddStringToObject(pwd_item,"CODE", pwd);
		if(API_TB_UpdateByName(TB_NAME_PRIVATE_PWD_BY_L_NBR, where, pwd_item))
		{

		}
		else
		{
			API_TB_AddByName(TB_NAME_PRIVATE_PWD_BY_L_NBR, pwd_item);
		}
		cJSON_Delete(where);
		cJSON_Delete(pwd_item);
	}

}

static void EditVmFunction(const char* type, void* user_data)
{
	if(editVmRecord)
	{
		if(!strcmp(type, "Save"))
		{
			cJSON* where = cJSON_CreateObject();
			cJSON_AddNumberToObject(where, "VM_ID", GetEventItemInt(editVmRecord,  "VM_ID"));
			if(API_TB_UpdateByName(TB_NAME_VM_MAT, where, editVmRecord))
			{
				SaveVmUnlockPwd();
				API_TableModificationInform(TB_NAME_VM_MAT, NULL);
				API_Event_By_Name(Event_MAT_Update);
				API_TIPS_Ext_Time("Save success", 2);
			}
			else
			{
				API_TIPS_Ext_Time("Save error", 2);
			}
			cJSON_Delete(where);
		}
		else if(!strcmp(type, "Test"))
		{
			char* vmAcc = GetEventItemString(viewVmRecord, "VM_ACC");
			if(vmAcc[0] != 0)
			{
				API_SettingLongTip("Waitting...");
				cJSON *call_event=cJSON_CreateObject();
				cJSON_AddStringToObject(call_event, EVENT_KEY_EventName,EventBECall);
				cJSON_AddStringToObject(call_event, "CallType","SipCall");
				cJSON_AddStringToObject(call_event, "Call_Tar_SipAcc",vmAcc);
				API_Event_Json(call_event);
				cJSON_Delete(call_event);
			}	
		}		
	}

}
static void EditVmCancel(const char* type, void* user_data)
{
    MenuVM_User_Editor_Close();
}

static void VmAccQrDisplay(char *qr_str)
{
	vmUserQrObj = lv_obj_create(editVmMenu->scr);
	lv_obj_remove_style_all(vmUserQrObj);
	lv_obj_set_size(vmUserQrObj,  170, 170);
	lv_obj_align(vmUserQrObj, LV_ALIGN_BOTTOM_MID, 0, -120);
	lv_obj_set_style_bg_color(vmUserQrObj, lv_color_hex(0x41454D), 0);
	lv_obj_set_style_bg_opa(vmUserQrObj, LV_OPA_100, 0);
	lv_color_t qr_bg_color=lv_color_make(0xff, 0xff, 0xef); // lv_palette_lighten(LV_PALETTE_LIGHT_BLUE, 5);
	lv_color_t qr_fg_color=lv_color_make(0x00, 0x00, 0x10); //lv_palette_darken(LV_PALETTE_BLUE, 4);
    lv_obj_t* qr = lv_qrcode_create(vmUserQrObj, 160, qr_fg_color, qr_bg_color);
    /*Set data*/
    lv_qrcode_update(qr, qr_str, strlen(qr_str));
    lv_obj_center(qr);
	
    /*Add a border with bg_color*/
    lv_obj_set_style_border_color(qr, qr_bg_color, 0);
    lv_obj_set_style_border_width(qr, 5, 0);

}
static void CreateUserQrcode(char *acc, char *pwd)
{
	if(acc[0] == 0)
	{
		return;
	}
	if(editVmMenu)
	{
		char *qr_code=create_sip_phone_qrcode(acc,pwd);
		VmAccQrDisplay(qr_code);
		free(qr_code);
	}
}
static void DelUserQrcode(void)
{
	if(vmUserQrObj)
	{
		lv_obj_del(vmUserQrObj);
	}
	vmUserQrObj=NULL;
}
void GetVmRecord(void)
{
	cJSON* view = cJSON_CreateArray();
	cJSON* where = cJSON_CreateObject();
	cJSON_AddNumberToObject(where, "VM_ID", vmUserId);
	API_TB_SelectBySortByName(TB_NAME_VM_MAT, view, where, 0);
	viewVmRecord = cJSON_Duplicate(cJSON_GetArrayItem(view, 0), 1);
	cJSON_Delete(where);
	cJSON_Delete(view);
}
void MenuVM_User_Editor_Init(void)
{
    const char* editEnable[] = {"IX_NAME", "L_NBR", "G_NBR", "VM_UserPWD", "LOCK_PWD"};
	cJSON* editorEnable =  cJSON_CreateStringArray(editEnable, sizeof(editEnable)/sizeof(editEnable[0]));
	const char* editCheck = "{\"Check\":{\"VM_UserPWD\":\"^[0-9]{4}$\",\"LOCK_PWD\":\"^[0-9]{4}$\",\"L_NBR\":\"^[0-9]{0,6}$\",\"G_NBR\":\"^[0-9]{0,10}$\"},\"Translate\":{}}";
	cJSON* editorCheck =  cJSON_Parse(editCheck);
	if(editVmRecord)
	{
		cJSON_Delete(editVmRecord);
	}
	editVmRecord =  cJSON_CreateObject();
	GetVmRecord();
    ShellCmdPrintJson(viewVmRecord);
	cJSON_AddNumberToObject(editVmRecord, "VM_ID", GetEventItemInt(viewVmRecord,  "VM_ID"));
	cJSON_AddStringToObject(editVmRecord, "IX_NAME", GetEventItemString(viewVmRecord,  "IX_NAME"));
	cJSON_AddStringToObject(editVmRecord, "L_NBR", GetEventItemString(viewVmRecord,  "L_NBR"));
	cJSON_AddStringToObject(editVmRecord, "G_NBR", GetEventItemString(viewVmRecord,  "G_NBR"));
	cJSON_AddStringToObject(editVmRecord, "VM_UserPWD", GetEventItemString(viewVmRecord,  "VM_UserPWD"));
	
	cJSON* where=cJSON_CreateObject();
	cJSON_AddStringToObject(where,"IX_ADDR",GetEventItemString(viewVmRecord,  "IX_ADDR"));
	cJSON* view=cJSON_CreateArray();
	if(API_TB_SelectBySortByName(TB_NAME_PRIVATE_PWD_BY_L_NBR, view, where, 0))
	{
		cJSON_AddStringToObject(editVmRecord, "LOCK_PWD", GetEventItemString(cJSON_GetArrayItem(view,0),"CODE"));
	}
	else
	{
		cJSON_AddStringToObject(editVmRecord, "LOCK_PWD", "");
	}
	cJSON_Delete(where);
	cJSON_Delete(view);
	cJSON* func = cJSON_CreateObject();
	cJSON* btnname = cJSON_AddArrayToObject(func,"BtnName");
    cJSON_AddItemToArray(btnname, cJSON_CreateString("Save"));
    cJSON_AddItemToArray(btnname, cJSON_CreateString("Test"));
	cJSON_AddNumberToObject(func, "BtnCallback", (int)EditVmFunction);
	cJSON_AddNumberToObject(func, "BackCallback", (int)EditVmCancel);
    RC_MenuDisplay(&editVmMenu, editVmRecord, editorEnable, editorCheck, func, "Virtual User");
    cJSON_Delete(editorEnable);
    cJSON_Delete(editorCheck);
    cJSON_Delete(func);
	CreateUserQrcode(GetEventItemString(viewVmRecord,  "VM_ACC"), GetEventItemString(viewVmRecord,  "VM_PWD"));

}

void MenuVM_User_Editor_Close(void)
{
	DelUserQrcode();
    if(editVmMenu)
    {
        RC_MenuDelete(&editVmMenu);
    }

    if(editVmRecord)
	{
		cJSON_Delete(editVmRecord);
		editVmRecord = NULL;
	}
    if(viewVmRecord)
	{
		cJSON_Delete(viewVmRecord);
		viewVmRecord = NULL;
	}
	API_Event_NameAndMsg(EventMenuSettingProcess, "SettingRefresh", NULL);
}


int VMUserPsw_Verify(char* input, int input_Len)
{
	int ret = 0;

	if(input == NULL || (input_Len<=4))
	{
		return ret;
	}
	char pwd_buff[5]={0};
	char nbr_buff[20]={0};
	memcpy(pwd_buff,input+input_Len-4,4);
	memcpy(nbr_buff,input,input_Len-4);
	cJSON* Where = cJSON_CreateObject();
	cJSON_AddNumberToObject(Where, "VM_ID", atoi(nbr_buff));
	cJSON_AddStringToObject(Where, "VM_UserPWD", pwd_buff);
	cJSON* view = cJSON_CreateArray();

	if(API_TB_SelectBySortByName(TB_NAME_VM_MAT, view, Where, 0))
	{
		ret=1;
		vmUserId = GetEventItemInt(cJSON_GetArrayItem(view, 0),  "VM_ID");
	}

	cJSON_Delete(Where);
	cJSON_Delete(view);

	return ret;
}