#include "menu_utility.h"
#include "cJSON.h"
#include "obj_TableSurver.h"
#include "define_string.h"
#include "obj_PublicInformation.h"
#include "utility.h"
#include "task_Event.h"
#include "Common_TableView.h"
#include "obj_IoInterface.h"
#include "obj_IXS_Proxy.h"
#include "obj_CommonSelectMenu.h"

#define SAT_HYBRID_REPORT_PARA		"{\"HeaderEnable\":true,\"ColumnWidth\":[200, 180, 180, 180, 180, 180, 180, 180, 180, 180, 180, 180, 180, 180, 180]}"
#define SAT_HYBRID_REPORT_VIEW_TEMPALE		"{\"IX_ADDR\":\"\",\"IX_TYPE\":\"\",\"DEV_ID\":1,\"STATE\":\"OFFLINE\",\"PARA_CMP\":\"\",\"SYS_TYPE_CUR\":\"UNKNOWN\",\"ASSO_CUR\":[],\"G_NBR_CUR\":\"\",\"L_NBR_CUR\":\"\",\"IX_NAME_CUR\":\"\",\"SYS_TYPE\":\"UNKNOWN\",\"ASSO\":[],\"G_NBR\":\"\",\"L_NBR\":\"\",\"IX_NAME\":\"\"}"

static TB_VIWE* satHybridReportTb = NULL;
static int satHybridCheckFlag = 0;

static cJSON* SatHybridReportCreatePara(const char* filterValue);

static void SatHybridReportReturnCallback(void)
{
	MenuSAT_HYBRID_Report_Close();
}

static void SatHybridReportFilterProcess(const char* chose, void* userdata)
{   
    if(!chose)
    {
        return;
    }

    if(!strcmp("ALL",chose) || !strcmp("ONLINE",chose) || !strcmp("OFFLINE",chose) || !strcmp("ADDED",chose))
	{
        cJSON* para = SatHybridReportCreatePara(chose);
		cJSON* where = cJSON_CreateObject();
		cJSON* view = cJSON_CreateArray();
		cJSON_AddItemToArray(view, cJSON_Parse(SAT_HYBRID_REPORT_VIEW_TEMPALE));
		cJSON_AddStringToObject(where, "STATE", chose);
		API_TB_SelectBySortByName(TB_NAME_HYBRID_SAT_CHECK, view, (!strcmp("ALL",chose)) ? NULL : where, 0);
		cJSON_DeleteItemFromArray(view, 0);
		TB_MenuDisplay(&satHybridReportTb, view, "SAT hybrid report", para);
		cJSON_Delete(where);
		cJSON_Delete(view);
		cJSON_Delete(para);
    }
}

static void SatHybridReportFilterCallback(const cJSON* filter)
{   
    cJSON* value = cJSON_GetObjectItem(filter,"Value");
    static CHOSE_MENU_T callback = {.callback = 0, .cbData = NULL};
	callback.callback = SatHybridReportFilterProcess;
	callback.cbData = NULL;
    cJSON* choseList = cJSON_CreateArray();
    cJSON_AddItemToArray(choseList, cJSON_CreateString("ALL"));
    cJSON_AddItemToArray(choseList, cJSON_CreateString("ONLINE"));
    cJSON_AddItemToArray(choseList, cJSON_CreateString("OFFLINE"));
    cJSON_AddItemToArray(choseList, cJSON_CreateString("ADDED"));
    char* choseString = cJSON_IsString(value) ? value->valuestring : NULL;
    CreateChooseMenu("IM state", choseList, choseString, &callback);
    cJSON_Delete(choseList);
}

static cJSON* SatHybridReportCreatePara(const char* filterValue)
{   
    cJSON* fliter = cJSON_CreateObject();
    cJSON_AddStringToObject(fliter, "Name", "state");
    cJSON_AddStringToObject(fliter, "Value", filterValue);
    cJSON_AddNumberToObject(fliter, "Callback", (int)SatHybridReportFilterCallback);
	cJSON* para = cJSON_Parse(SAT_HYBRID_REPORT_PARA);
    cJSON_AddItemToObject(para,"Filter", fliter);
	cJSON_AddNumberToObject(para, "ReturnCallback", (int)SatHybridReportReturnCallback);

	return para;
}


void MenuSAT_HYBRID_Report_Init(void)
{
	SatHybridReportFilterProcess("ALL", NULL);
}

void MenuSAT_HYBRID_Report_Close(void)
{
	TB_MenuDelete(&satHybridReportTb);
	API_Event_NameAndMsg(EventMenuSettingProcess, "SettingRefresh", NULL);
}

static int SatHybridCheckRefleshTimerCb(int time)
{
	if(satHybridCheckFlag)
	{
		if(time < 10)
		{
			char tempString[10];
			snprintf(tempString, 10, "%ds", 10 - time);
			API_TIPS_Mode_Title(NULL, tempString);
			MainMenu_Reset_Time();
			return 0;
		}
		else
		{
			API_Event_NameAndMsg(EventMenuSatHybridReport, "SAT_Check", NULL);
		}
	}

	return 2;
}

static int SatHybridCheckRecord(const cJSON* record)
{
	const char* checkString[] = {"SYS_TYPE", "ASSO", "L_NBR", "G_NBR", "IX_NAME", NULL};
	char tempString[100];
	int i;
	int ret = 1;
	for(i = 0; checkString[i]; i++)
	{
		snprintf(tempString, 100, "%s_CUR", checkString[i]);
		cJSON* current =  cJSON_GetObjectItemCaseSensitive(record, tempString);
		cJSON* compare =  cJSON_GetObjectItemCaseSensitive(record, checkString[i]);
		if(!cJSON_Compare(current, compare, 1))
		{
			ret = 0;
			break;
		}
	}

	return ret;
}

int EventMenuSatHybridReportCallback(cJSON* cmd)
{
	if(satHybridCheckFlag)
	{
		char* msg = GetEventItemString(cmd, EVENT_KEY_Message);
		if(!strcmp(msg, "SAT_Check"))
		{
			cJSON* onlineDevice = NULL;
			cJSON* viewElement;
			int recordCnt;
			cJSON* record;

			API_TB_DeleteByName(TB_NAME_HYBRID_SAT_CHECK, NULL);
			cJSON* view = cJSON_CreateArray();
			API_TB_SelectBySortByName(TB_NAME_HYBRID_SAT, view, NULL, 0);
			cJSON_ArrayForEach(viewElement, view)
			{
				API_TB_AddByName(TB_NAME_HYBRID_SAT_CHECK, viewElement);
			}
			cJSON_Delete(view);

			IXS_ProxyGetTb(&onlineDevice, NULL, Search);

			cJSON_ArrayForEach(viewElement, onlineDevice)
			{
				char* ixAddr = GetEventItemString(viewElement, "IX_ADDR");
				char* ixType = GetEventItemString(viewElement, "IX_TYPE");
				if((!strcmp(ixType, "DS") && !strncmp(ixAddr, "00990000", 8)) || (!strcmp(ixType, "DXG") || !strcmp(ixType, "IXG") && !strncmp(ixAddr, "0099", 4) && !strncmp(ixAddr+6, "0001", 4)))
				{
					int targetIp = inet_addr(GetEventItemString(viewElement, "IP_ADDR"));

					cJSON* where = cJSON_CreateObject();
					cJSON_AddStringToObject(where, IX2V_IX_ADDR, ixAddr);
					view = cJSON_CreateArray();
					recordCnt = API_TB_SelectBySortByName(TB_NAME_HYBRID_SAT, view, where, 0);
					if(recordCnt)
					{
						record = cJSON_Duplicate(cJSON_GetArrayItem(view, 0), 1);
					}
					else
					{
						record = cJSON_CreateObject();
					}
					
					cJSON* ioRead = cJSON_CreateArray();
					cJSON_AddItemToArray(ioRead, cJSON_CreateString(SysType));
					cJSON_AddItemToArray(ioRead, cJSON_CreateString(ASSO));
					cJSON_AddItemToArray(ioRead, cJSON_CreateString(IX_NAME));
					cJSON_AddItemToArray(ioRead, cJSON_CreateString(G_NBR));
					cJSON_AddItemToArray(ioRead, cJSON_CreateString(L_NBR));

					cJSON* ioResult = API_ReadRemoteIo(targetIp, ioRead);
					if(ioResult && !strcmp(GetEventItemString(ioResult, "Result"), "Succ"))
					{
						cJSON* ioValue =  cJSON_GetObjectItemCaseSensitive(ioResult, "READ");
						cJSON_AddItemToObject(record, "SYS_TYPE_CUR", cJSON_Duplicate(cJSON_GetObjectItemCaseSensitive(ioValue, SysType), 1));
						cJSON_AddItemToObject(record, "ASSO_CUR", cJSON_Duplicate(cJSON_GetObjectItemCaseSensitive(ioValue, ASSO), 1));
						cJSON_AddItemToObject(record, "G_NBR_CUR", cJSON_Duplicate(cJSON_GetObjectItemCaseSensitive(ioValue, G_NBR), 1));
						cJSON_AddItemToObject(record, "L_NBR_CUR", cJSON_Duplicate(cJSON_GetObjectItemCaseSensitive(ioValue, L_NBR), 1));
						cJSON_AddItemToObject(record, "IX_NAME_CUR", cJSON_Duplicate(cJSON_GetObjectItemCaseSensitive(ioValue, IX_NAME), 1));
					}

					cJSON_AddStringToObject(record, "PARA_CMP", SatHybridCheckRecord(record) ? "same" : "different");

					if(recordCnt)
					{
						cJSON_AddStringToObject(record, "STATE", "ONLINE");
						API_TB_UpdateByName(TB_NAME_HYBRID_SAT_CHECK, where, record);
					}
					else
					{
						char tempString[10] = {0};
						!strcmp(ixType, "DS") ? strcpy(tempString, ixAddr+8) : strncpy(tempString, ixAddr+4, 2);
						int id = atoi(tempString);
						cJSON_AddStringToObject(record, "IX_ADDR", ixAddr);
						cJSON_AddStringToObject(record, "IX_TYPE", (!strcmp(ixType, "DS")) ? "DS" : "DXG");
						cJSON_AddNumberToObject(record, "DEV_ID", id);
						cJSON_AddStringToObject(record, "STATE", "ADDED");
						API_TB_AddByName(TB_NAME_HYBRID_SAT_CHECK, record);
					}
					cJSON_Delete(where);
					cJSON_Delete(record);
					cJSON_Delete(view);
					cJSON_Delete(ioRead);
					cJSON_Delete(ioResult);
				}
			}
			cJSON_Delete(onlineDevice);
			API_SatHybridCheckModificationInform();
			API_Event_NameAndMsg(EventMenuSettingProcess, "SettingRefresh", NULL);
			API_TIPS_Mode_Close(NULL);
			satHybridCheckFlag = 0;
		}
		
	}
}

//IXS表有更新
int SatHybridCheckListChangeProcess(cJSON *cmd)
{
	if(satHybridCheckFlag)
	{
		API_Del_TimingCheck(SatHybridCheckRefleshTimerCb);
		API_Event_NameAndMsg(EventMenuSatHybridReport, "SAT_Check", NULL);
	}
	return 1;
}

static void CheckSat(void* para)
{
	satHybridCheckFlag = 1;
	API_Event_By_Name(Event_IXS_ReqUpdateTb);
	API_Add_TimingCheck(SatHybridCheckRefleshTimerCb, 1);
	if(!API_TIPS_Mode("Please wait"));
	{
		API_TIPS_Mode_Fresh(NULL, "Please wait");
	}
	API_TIPS_Mode_Title(NULL, "10s");
}


void MenuSAT_HYBRID_Check_Process(void* data)
{
	if(data)
	{
		if(!strcmp(data, "SAT_Check") && !satHybridCheckFlag)
		{
			API_MSGBOX("About to check SAT", 2, CheckSat, NULL);
		}
	}
}

void MenuSAT_HYBRID_Check_Close(void)
{
	if(satHybridCheckFlag)
	{
		satHybridCheckFlag = 0;
		API_TIPS_Mode_Close(NULL);
	}
}