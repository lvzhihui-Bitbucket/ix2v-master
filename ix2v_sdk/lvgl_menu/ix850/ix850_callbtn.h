#ifndef _IX850_CALLBTN_H_
#define _IX850_CALLBTN_H_

#include "lv_ix850.h"
#include "icon_common.h"
#include "menu_common.h"

lv_obj_t* img_creat_callbtn(lv_obj_t* parent, struct ICON_INS* picon);
lv_obj_t* add_Call_Btn(lv_obj_t* parent, struct ICON_INS* picon);

#endif
