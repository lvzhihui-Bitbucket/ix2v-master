#include "lv_ix850.h"

static lv_obj_t * ui_qrcode;
static lv_obj_t * ui_qrcode_panel = NULL;
static lv_obj_t * ui_qrcode_item[4]={NULL};
static lv_obj_t * ui_qrcode_value[4]={NULL};
static lv_obj_t *ui_qrcode_btn=NULL;
const char *ui_qrcode_item_text[4]=
{
	"Addr:",
	"Server:",	
	"Account:",
	"Pwd:"
};
lv_obj_t* create_QRcode(lv_obj_t* parent, const char* data,int size);

void ui_qrcode_init(void)
{
	int i;
	lv_obj_t* p_text;

    vtk_lvgl_lock(); 
    ui_qrcode = lv_obj_create(NULL);
    lv_obj_remove_style_all(ui_qrcode);
    lv_obj_set_size(ui_qrcode, lv_disp_get_hor_res(NULL), lv_disp_get_ver_res(NULL));
    lv_obj_align(ui_qrcode, LV_ALIGN_TOP_LEFT, 0, 0);
   // lv_obj_set_style_bg_color(ui_qrcode, lv_palette_main(LV_PALETTE_LIGHT_BLUE), 0);//lv_color_hex(0x102030)
    lv_obj_set_style_bg_color(ui_qrcode, lv_color_hex(0x41454D), 0);
    lv_obj_set_style_bg_opa(ui_qrcode, LV_OPA_100, 0);
	 lv_obj_set_style_text_font(ui_qrcode, &lv_font_montserrat_26, 0);
	 lv_obj_set_style_text_color(ui_qrcode, lv_color_white(), 0);
	for(i=0;i<4;i++)
	{
		p_text=lv_label_create(ui_qrcode);
		lv_obj_set_size(p_text, 140, 40);
		lv_obj_set_align(p_text, LV_ALIGN_TOP_LEFT);
		lv_obj_set_pos(p_text, 290, 160+i*60);
		lv_label_set_long_mode(p_text, LV_LABEL_LONG_SCROLL_CIRCULAR);
		 lv_label_set_text(p_text, ui_qrcode_item_text[i]);
		 ui_qrcode_item[i]=p_text;
	}
	for(i=0;i<4;i++)
	{
		p_text=lv_label_create(ui_qrcode);
		lv_obj_set_size(p_text, 290, 40);
		lv_obj_set_align(p_text, LV_ALIGN_TOP_LEFT);
		lv_obj_set_pos(p_text, 420, 160+i*60);
		lv_label_set_long_mode(p_text, LV_LABEL_LONG_SCROLL_CIRCULAR);
		 lv_label_set_text(p_text, "");
		 ui_qrcode_value[i]=p_text;
	}
	ui_qrcode_btn=lv_btn_create(ui_qrcode);
	 lv_obj_set_width(ui_qrcode_btn, 100);
	    lv_obj_set_height(ui_qrcode_btn, 50);
		 lv_obj_set_style_bg_color(ui_qrcode_btn, lv_color_hex(0x41454D), 0);
	    //lv_obj_align(ui_qrcode_btn, LV_ALIGN_CENTER,0,0);
		lv_obj_set_pos(ui_qrcode_btn, 550, 155+3*60);
		lv_obj_set_style_border_width(ui_qrcode_btn, 4, 0);
		lv_obj_set_style_border_color(ui_qrcode_btn,lv_color_white(),0);
		lv_obj_set_style_border_opa(ui_qrcode_btn,LV_OPA_100, 0);
	    //lv_obj_add_event_cb(btn, next_clicked_event, LV_EVENT_CLICKED, 0);

	    lv_obj_t* ui_Label = lv_label_create(ui_qrcode_btn);
		//lv_obj_set_size(ui_Label, 100, 50);
	    lv_obj_align(ui_Label, LV_ALIGN_CENTER, 0, 0);

	    lv_label_set_text(ui_Label, "Edit");
	    vtk_lvgl_unlock(); 

}
static lv_color_t qr_bg_color ;
static lv_color_t qr_fg_color;
void scr_qrcode(char *qr_str,char* addr,char *ser,char *acc,char *pwd)
{
	//printf("11111111111111scr_qrcode\n");
   
	//printf("2222222222222222scr_qrcode\n");
	static lv_obj_t* qr_obj=NULL;
	lv_obj_t* p_text;
	vtk_lvgl_lock();
    if(ui_qrcode_panel == NULL)
    {
    	 lv_disp_load_scr(ui_qrcode);
        ShellCmdPrintf("scr_qrcode menu start!\n");
	if(qr_obj!=NULL)
		lv_qrcode_delete(qr_obj);
        qr_obj = create_QRcode(ui_qrcode,qr_str,240);
	#if 0
	 p_text = lv_label_create(ui_qrcode);
       
	lv_obj_set_size(p_text, 180, 40);
	lv_obj_set_align(p_text, LV_ALIGN_TOP_LEFT);
	lv_obj_set_pos(p_text, 340, 80);

	//lv_obj_set_style_text_color(p_text, lv_color_white(), 0);
	// lv_obj_set_style_text_font(menu, fontlog == NULL? &lv_font_montserrat_24 : fontlog, 0);
        lv_label_set_long_mode(p_text, LV_LABEL_LONG_SCROLL_CIRCULAR);	
	 lv_label_set_text(p_text, "hello world");
	 #endif
	 lv_label_set_text(ui_qrcode_value[0], addr?addr:"-");
	  	lv_label_set_text(ui_qrcode_value[1], ser?ser:"-");
	   lv_label_set_text(ui_qrcode_value[2], acc?acc:"-");
	    lv_label_set_text(ui_qrcode_value[3], pwd?pwd:"-");
    }
	
	vtk_lvgl_unlock();
}


lv_obj_t* create_QRcode(lv_obj_t* parent, const char* data,int size)
{
    //lv_color_t bg_color = lv_palette_lighten(LV_PALETTE_LIGHT_BLUE, 5);
   // lv_color_t fg_color = lv_palette_darken(LV_PALETTE_BLUE, 4);
   
	qr_bg_color=lv_palette_lighten(LV_PALETTE_LIGHT_BLUE, 5);
	qr_fg_color=lv_palette_darken(LV_PALETTE_BLUE, 4);
   lv_obj_t* qr = lv_qrcode_create(parent, size, qr_fg_color, qr_bg_color);

    /*Set data*/
    lv_qrcode_update(qr, data, strlen(data));
    //lv_obj_center(qr);
    lv_obj_align(qr, LV_ALIGN_LEFT_MID, 25, -10);
	
    /*Add a border with bg_color*/
    lv_obj_set_style_border_color(qr, qr_bg_color, 0);
    lv_obj_set_style_border_width(qr, 5, 0);
	return qr;
}