#include "menu_utility.h"
#include "MenuHybridMatCreateNew.h"
#include "cJSON.h"
#include "obj_TableSurver.h"
#include "define_string.h"
#include "obj_PublicInformation.h"
#include "utility.h"
#include "task_Event.h"
#include "define_file.h"
#include "obj_IoInterface.h"
#include "obj_IXS_Proxy.h"

static RECORD_CONTROL* managerMenuAbout = NULL;
static RECORD_CONTROL* userMenuAbout = NULL;
void Menu_ManagerAbout_Close(void);
void Menu_UserAbout_Close(void);

static void ManagerAbout_Close(const char* type, void* user_data)
{  
	Menu_ManagerAbout_Close();
}

void Menu_ManagerAbout_Init(void)
{
	cJSON* func = cJSON_CreateObject();
	cJSON_AddNumberToObject(func, "BackCallback", (int)ManagerAbout_Close);
	RC_MenuDisplay(&managerMenuAbout, cJSON_GetArrayItem(API_PublicInfo_Read(PB_MANAGER_ABOUT_PAGE), 0), NULL, NULL, func, "Manager about");
    cJSON_Delete(func);
}

void Menu_ManagerAbout_Close(void)
{
	RC_MenuDelete(&managerMenuAbout);
	API_Event_NameAndMsg(EventMenuSettingProcess, "SettingRefresh", NULL);
}

static void UserAbout_Close(const char* type, void* user_data)
{  
	Menu_UserAbout_Close();
}

void Menu_UserAbout_Init(void)
{
	cJSON* func = cJSON_CreateObject();
	cJSON_AddNumberToObject(func, "BackCallback", (int)UserAbout_Close);
	RC_MenuDisplay(&userMenuAbout, cJSON_GetArrayItem(API_PublicInfo_Read(PB_USER_ABOUT_PAGE), 0), NULL, NULL, func, "User about");
    cJSON_Delete(func);
}

void Menu_UserAbout_Close(void)
{
	if(userMenuAbout)
	{
		RC_MenuDelete(&userMenuAbout);
	}
	API_Event_NameAndMsg(EventMenuSettingProcess, "SettingRefresh", NULL);
}
