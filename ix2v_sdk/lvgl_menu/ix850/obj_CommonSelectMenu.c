#include <stdio.h>
#include "obj_CommonSelectMenu.h"
#include "lv_ix850.h"

static void combox_HeadReturn(lv_event_t* e)
{
    lv_obj_t* obj = lv_event_get_user_data(e);
    lv_obj_del(obj);
}

static void ChooseMenuCallback(lv_event_t* e)
{   
    lv_obj_t* act_cb = lv_event_get_target(e);
    lv_obj_t* cont = lv_event_get_current_target(e);
    if(act_cb != cont)
    {
        CHOSE_MENU_T* userData = (CHOSE_MENU_T*)lv_event_get_user_data(e);
        int cnt = lv_obj_get_child_cnt(cont);
        for(int i = 0;i < cnt;i++)
        { 
            lv_obj_clear_state(lv_obj_get_child(cont,i), LV_STATE_CHECKED);   
        } 
        lv_obj_add_state(act_cb, LV_STATE_CHECKED);     


        if(userData && (userData->callback))
        {
            char* text = lv_checkbox_get_text(act_cb);
            (userData->callback)(text, userData->cbData);
        }
        lv_obj_del(lv_obj_get_parent(cont));
    }
}

void CreateChooseMenu(const char* title, const cJSON* choseList, const char* choseString, const CHOSE_MENU_T* userData)
{   
    lv_obj_t* combox = lv_obj_create(lv_scr_act()); 
    lv_obj_set_size(combox, 480, 800);
    //lv_obj_align(combox, LV_ALIGN_CENTER, 0, 0);
    lv_obj_set_style_pad_all(combox, 0, 0);
    lv_obj_clear_flag(combox, LV_OBJ_FLAG_SCROLLABLE);
    lv_obj_set_style_bg_color(combox, lv_color_hex(0x41454D), 0);
    lv_obj_set_style_border_side(combox, 0, 0);

    lv_obj_t* btn = lv_obj_create(combox);
    lv_obj_set_size(btn,480, 70);   
    lv_obj_set_style_bg_color(btn, lv_color_make(26,82,196), 0);
    lv_obj_set_style_pad_all(btn,0,0);
    lv_obj_set_style_border_width(btn, 0, 0);

    lv_obj_add_event_cb(btn, combox_HeadReturn, LV_EVENT_CLICKED, combox);
    lv_obj_add_flag(btn, LV_OBJ_FLAG_CLICKABLE);
    lv_obj_clear_flag(btn, LV_OBJ_FLAG_SCROLLABLE);

    lv_obj_t* obj = lv_obj_create(btn);
    lv_obj_remove_style_all(obj);
    lv_obj_add_flag(obj, LV_OBJ_FLAG_EVENT_BUBBLE);
    lv_obj_set_style_bg_opa(obj,LV_OPA_MAX,0);
    lv_obj_set_size(obj,160,70);
    lv_obj_set_style_bg_color(obj,lv_color_make(19,132,250),0);
    lv_obj_set_style_text_color(obj, lv_color_white(), 0);
    lv_obj_t* img = lv_img_create(obj);
	lv_img_set_src(img,LV_VTK_LEFT);
    set_font_size(img,0);
    lv_obj_center(img);

    lv_obj_t* titlelabel = lv_label_create(btn); 
    lv_obj_set_style_text_align(titlelabel,LV_TEXT_ALIGN_CENTER,0);
    //lv_obj_align(titlelabel, LV_ALIGN_CENTER, 20, 0); 
    lv_obj_align(titlelabel, LV_ALIGN_LEFT_MID, 160, 0); 
    set_font_size(titlelabel,0);
    //lv_obj_set_style_text_font(titlelabel, TB_VIEW_TextSize, 0); 
    lv_obj_set_style_text_color(titlelabel, lv_color_white(), 0);
    //lv_label_set_text(titlelabel, txt);
    vtk_label_set_text(titlelabel, title); 

    lv_obj_t* cont = lv_obj_create(combox);
    lv_obj_set_flex_flow(cont, LV_FLEX_FLOW_COLUMN);
    set_font_size(cont,0);
    lv_obj_set_size(cont, LV_PCT(100), 730);
    lv_obj_set_y(cont,70);
    lv_obj_set_style_bg_color(cont, lv_color_hex(0x52545A), 0);
    //lv_obj_set_style_pad_all(cont, 0, 0);
    lv_obj_add_flag(cont, LV_OBJ_FLAG_CLICKABLE);   
    lv_obj_set_style_text_color(cont, lv_color_white(), 0);
    lv_obj_set_style_border_side(cont,0,0);
    lv_obj_set_flex_flow(cont, LV_FLEX_FLOW_COLUMN);   
    lv_obj_set_style_border_side(cont, 0, 0);

    cJSON* choseItem;
    cJSON_ArrayForEach(choseItem, choseList)
    {
        if(cJSON_IsString(choseItem))
        {
            lv_obj_t* box1 = lv_checkbox_create(cont);
            lv_checkbox_set_text(box1, choseItem->valuestring);
            lv_obj_set_size(box1, LV_PCT(100), 80);
            set_font_size(box1, 0);
            lv_obj_add_flag(box1, LV_OBJ_FLAG_EVENT_BUBBLE);
            if(choseString && !strcmp(choseItem->valuestring, choseString))
            {
                lv_obj_add_state(box1, LV_STATE_CHECKED);
            }
        }
    }

    lv_obj_add_event_cb(cont, ChooseMenuCallback, LV_EVENT_CLICKED, userData); 
}