#ifndef Common_TableView_H_
#define Common_TableView_H_

#include "lvgl.h"
#include "cJSON.h"

typedef void (*CellClickCallback)(int line, int col);

typedef void (*ReturnCallback)(void);
typedef void (*FilterClickCallback)(const cJSON* filter);
typedef void (*FunctionClickCallback)(const char* fancName);

#define TB_VIEW_FUNC_NUM_MAX     6      //最多6个功能按键
#define TB_VIEW_FUNC_NAME_LEN    100     //最多100个字符

typedef struct 
{   
    lv_obj_t* back_scr;
    lv_obj_t* scr;
    lv_obj_t* menu;        
    lv_obj_t* filter;          
    lv_obj_t* filterName;          
    lv_obj_t* filterValue;          
    lv_obj_t* table;          
    lv_obj_t* pageCtrl;
    lv_obj_t* pageLabel;
    lv_obj_t* cntLabel;
    lv_obj_t* function;
    char* functionName[TB_VIEW_FUNC_NUM_MAX+1];
    char* functionNameTranslate[TB_VIEW_FUNC_NUM_MAX+2];
    int functionCnt;
    cJSON* view;
	cJSON *para;
    int currentPage;
    int pageNbr;
    int recordCnt;
    int lineNbr;
    int filterEnable;
    int headerEnable;
    ReturnCallback returnCallback;
    CellClickCallback cellCallback;
    FunctionClickCallback funCallback;
}TB_VIWE;

void TB_MenuDelete(TB_VIWE** disp);
void TB_MenuDisplay(TB_VIWE** tbDisp, cJSON* view, char* title, cJSON* para);

#endif 