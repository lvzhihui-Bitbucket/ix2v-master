#include <stdio.h>
#include "Common_Setting_Record.h"
#include "menu_utility.h"
#include "Common_Setting_TableView.h"

static TABLE_UTILITY *table_sip = NULL;

void MenuTbView_Init(void)
{
	cJSON* sip = get_cjson_from_file("/mnt/nand1-1/App/res/test/sip.json");
	table_sip = create_utility_table_display_menu(sip,"SIP Manage","Acc");
}
void MenuIXGTbViewCommon_Process(int msg_type,void* arg)
{
	switch(msg_type)
	{		
		case TABLE_MSG_EXIT:
			MenuTbViewCommon_Close();
			break;
	}
}

void MenuTbViewCommon_Close(void)
{
	if(table_sip){
		delete_table(table_sip);
		table_sip = NULL;
	}
}


void API_MenuTbViewCommon_Process(void* arg)
{
	TABLE_UTILITY* tu = (TABLE_UTILITY*)arg;
	MenuIXGTbView_Process(tu->msg_type,tu->msg_data);
}