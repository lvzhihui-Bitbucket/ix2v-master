#include "Common_Setting_Record.h"
#include "menu_utility.h"

static RECORD_CONTROL* rc = NULL;
lv_style_t style_btn;
lv_obj_t* keyb = NULL;

static void text_edit_event_cb(lv_event_t* e);

static void number_edit_event_cb(lv_event_t* e)
{
    lv_event_code_t code = lv_event_get_code(e);
    lv_obj_t* ta = lv_event_get_target(e);
    lv_obj_t* label = lv_event_get_user_data(e);

    if (code == LV_EVENT_FOCUSED) {
        if (lv_indev_get_type(lv_indev_get_act()) != LV_INDEV_TYPE_KEYPAD) {

            lv_obj_clear_flag(keyb, LV_OBJ_FLAG_HIDDEN);
            lv_keyboard_set_textarea(keyb, ta);

            lv_keyboard_set_mode(keyb, LV_KEYBOARD_MODE_NUMBER);
        }
    }
    else if (code == LV_EVENT_CANCEL) {

        keyboard_exit();
        keyb = NULL;
    }
    else if (code == LV_EVENT_READY)
    {
        lv_obj_t* nameLabel = lv_obj_get_child(lv_obj_get_parent(label), 0);
        char* name = lv_label_get_text(nameLabel);

        char* text = lv_textarea_get_text(ta);
        if(rc->EditCheck)
        {
            if(API_Data_Edit_Check(name,text,rc->EditCheck)==0)
            {
                LV_API_TIPS(" Error! Please enter it again!",4);
                //API_TIPS(" Error! Please enter it again!");
                return;
            }
        }
	    cJSON *	new_item=cJSON_CreateNumber(atoi(text));

        lv_label_set_text(label, text);
	    cJSON_ReplaceItemInObject(rc->rec_data, name, new_item);

        keyboard_exit();
        keyb = NULL;
    }
}


static void number_click_event(lv_event_t* e)
{
    //cJSON* description = lv_event_get_user_data(e);
    lv_obj_t* valueLabel = lv_event_get_target(e);
    char* text = lv_label_get_text(valueLabel);

    keyb = keyboard_menu_init(valueLabel,text,number_edit_event_cb);
}

static void text_click_event(lv_event_t* e)
{
    //cJSON* description = lv_event_get_user_data(e);
    lv_obj_t* valueLabel = lv_event_get_target(e);
    char* text = lv_label_get_text(valueLabel);

    keyb = keyboard_menu_init(valueLabel,text,text_edit_event_cb);
}

static void text_edit_event_cb(lv_event_t* e)
{
    lv_event_code_t code = lv_event_get_code(e);
    lv_obj_t* ta = lv_event_get_target(e);
    lv_obj_t* label = lv_event_get_user_data(e);    

    if (code == LV_EVENT_FOCUSED) {
        if (lv_indev_get_type(lv_indev_get_act()) != LV_INDEV_TYPE_KEYPAD) {

            lv_obj_clear_flag(keyb, LV_OBJ_FLAG_HIDDEN);
            lv_keyboard_set_textarea(keyb, ta);

            lv_keyboard_set_mode(keyb, LV_KEYBOARD_MODE_TEXT_LOWER);
        }
    }
    else if (code == LV_EVENT_CANCEL) {

        keyboard_exit();
        keyb = NULL;
    }
    else if (code == LV_EVENT_READY)
    {

        lv_obj_t* nameLabel = lv_obj_get_child(lv_obj_get_parent(label), 0);
        char* name = lv_label_get_text(nameLabel);

        char* text = lv_textarea_get_text(ta);

        if(rc->EditCheck)
        {
            if(API_Data_Edit_Check(name,text,rc->EditCheck)==0)
            {
                LV_API_TIPS(" Error! Please enter it again!",4);
                //API_TIPS(" Error! Please enter it again!");
                return;
            }
        }
	    cJSON*	new_item=cJSON_CreateString(text);

        lv_label_set_text(label, text);
        cJSON_ReplaceItemInObject(rc->rec_data, name, new_item);

        keyboard_exit();
        keyb = NULL;
    }
}

lv_obj_t* common_text_editor(lv_obj_t* parent, char* name, char* val, int able)
{
    lv_obj_t* obj = lv_obj_create(parent);
    lv_obj_set_style_pad_row(obj, 0, 0);
    lv_obj_set_size(obj, LV_PCT(100), LV_SIZE_CONTENT);
    lv_obj_set_flex_flow(obj, LV_FLEX_FLOW_ROW);
    lv_obj_set_style_text_font(obj, &lv_font_montserrat_26, 0);
    lv_obj_clear_flag(obj, LV_OBJ_FLAG_SCROLLABLE);

    lv_obj_t* text_label = lv_label_create(obj);
    lv_label_set_text(text_label, name);
    lv_obj_set_size(text_label, 200, LV_SIZE_CONTENT);

    lv_obj_t* value_label = lv_label_create(obj);
    lv_label_set_text(value_label, val);   
    lv_obj_set_ext_click_area(value_label, 30);
    //lv_label_set_long_mode(value_label, LV_LABEL_LONG_DOT);
    lv_obj_set_size(value_label, 200, LV_SIZE_CONTENT);
    if(able){

        lv_obj_add_flag(value_label, LV_OBJ_FLAG_CLICKABLE);
    
        lv_obj_add_event_cb(value_label, text_click_event, LV_EVENT_CLICKED, NULL);
	}
	
    return value_label;
}

lv_obj_t* common_number_editor(lv_obj_t* parent, char* name, int val,int able)
{
    lv_obj_t* obj = lv_obj_create(parent);
    lv_obj_set_style_pad_row(obj, 0, 0);
    lv_obj_set_size(obj, LV_PCT(100), LV_SIZE_CONTENT);
    lv_obj_set_flex_flow(obj, LV_FLEX_FLOW_ROW);
    lv_obj_set_style_text_font(obj, &lv_font_montserrat_32, 0);
    lv_obj_clear_flag(obj, LV_OBJ_FLAG_SCROLLABLE);

    lv_obj_t* text_label = lv_label_create(obj);
    lv_label_set_text(text_label, name);
    lv_obj_set_size(text_label, 200, LV_SIZE_CONTENT);

    lv_obj_t* value_label = lv_label_create(obj);
    lv_label_set_text_fmt(value_label, "%d", val);
    lv_obj_add_flag(value_label, LV_OBJ_FLAG_CLICKABLE);
    lv_obj_set_ext_click_area(value_label, 30);
    //lv_label_set_long_mode(value_label, LV_LABEL_LONG_DOT);
    lv_obj_set_size(value_label, 200, LV_SIZE_CONTENT);
	 if(able){

        lv_obj_add_flag(value_label, LV_OBJ_FLAG_CLICKABLE);
    
        lv_obj_add_event_cb(value_label, number_click_event, LV_EVENT_CLICKED, NULL);
    }
	
    return value_label;
}


void btn_style_init(void)
{
    lv_style_init(&style_btn);
    lv_style_set_text_font(&style_btn,&lv_font_montserrat_32);
    lv_style_set_radius(&style_btn,LV_RADIUS_CIRCLE);
    lv_style_set_bg_color(&style_btn,lv_color_make(230, 231, 232));
    lv_style_set_text_color(&style_btn,lv_color_hex(0xff0000));
}


RECORD_CONTROL* common_record_editor_menu(cJSON* rec_data, const cJSON* editabel, const cJSON* description,const cJSON* func)
{
    rc = (RECORD_CONTROL*)lv_mem_alloc(sizeof(RECORD_CONTROL));
    lv_memset_00(rc, sizeof(RECORD_CONTROL));

	rc->back_scr = lv_scr_act();
	rc->scr = ui_record_init();
    rc->rec_data = rec_data;
    rc->EditCheck = cJSON_GetObjectItem(description,"Check");

    lv_obj_t* editmenu = lv_obj_create(rc->scr);
    lv_obj_set_size(editmenu, 480, 800);
    lv_obj_set_style_pad_all(editmenu, 0, 0);
    lv_obj_clear_flag(editmenu, LV_OBJ_FLAG_SCROLLABLE);

    lv_obj_t* btnCont = lv_obj_create(editmenu);
    lv_obj_set_size(btnCont, 480, 100);
    lv_obj_set_style_pad_all(btnCont, 0, 0);
    lv_obj_set_y(btnCont, 700);
    btn_style_init();

    cJSON* save = cJSON_GetObjectItem(func,"CB_FOP_BTN_1");
    cJSON* del = cJSON_GetObjectItem(func,"CB_FOP_BTN_2");
    cJSON* cancel = cJSON_GetObjectItem(func,"CB_FOP_BTN_3");

    if(save){
        lv_obj_t* saveBtn = lv_btn_create(btnCont);
        lv_obj_add_style(saveBtn,&style_btn,0);
        lv_obj_set_size(saveBtn,120,70);
        lv_obj_add_event_cb(saveBtn, save_editor_event, LV_EVENT_CLICKED, NULL);
        lv_obj_align(saveBtn, LV_ALIGN_LEFT_MID, 20, 0);
        lv_obj_t* savelabel = lv_label_create(saveBtn);
        lv_label_set_text(savelabel, save->valuestring);
        lv_obj_center(savelabel);
    }
    if(del){
        lv_obj_t* delBtn = lv_btn_create(btnCont);
        lv_obj_add_style(delBtn,&style_btn,0);
        lv_obj_set_size(delBtn,120,70);
        lv_obj_add_event_cb(delBtn, del_record_event, LV_EVENT_CLICKED, NULL);
        lv_obj_align(delBtn, LV_ALIGN_CENTER, 0, 0);
        lv_obj_t* dellabel = lv_label_create(delBtn);
        lv_label_set_text(dellabel, del->valuestring);
        lv_obj_center(dellabel);
    }
    if(cancel){
        lv_obj_t* cancelBtn = lv_btn_create(btnCont);
        lv_obj_set_size(cancelBtn,120,70);
        lv_obj_align(cancelBtn, LV_ALIGN_RIGHT_MID, -20, 0);
        lv_obj_add_event_cb(cancelBtn, cancel_editor_event, LV_EVENT_CLICKED, NULL);
        lv_obj_add_style(cancelBtn,&style_btn,0);
        lv_obj_t* cancellabel = lv_label_create(cancelBtn);
        lv_label_set_text(cancellabel, cancel->valuestring);
        lv_obj_center(cancellabel);
    }

    lv_obj_t* editCont = lv_obj_create(editmenu);
    lv_obj_set_size(editCont, 480, 700);
    lv_obj_set_flex_flow(editCont, LV_FLEX_FLOW_COLUMN);
    lv_obj_set_style_pad_all(editCont, 0, 0);
    lv_obj_set_style_pad_row(editCont, 0, 0);

    int rec_size = cJSON_GetArraySize(rec_data);
    int able_size = cJSON_GetArraySize(editabel);
    int val;	

    for (int i = 0; i < rec_size; i++)
    {
    	char name_buff[100];
        //获取一个字段
        cJSON* field = cJSON_GetArrayItem(rec_data, i);
	    val=0;	
#if 1
        for (int j = 0; j < able_size; j++)
        {   //获取是否编辑的字段
            cJSON* able = cJSON_GetArrayItem(editabel, j);
            //获取比较结果
            if(strcmp(able->valuestring, field->string)==0)
            {
                val=1;
                break;
            }

        }  
#endif	
        if(cJSON_IsString(field))
            common_text_editor(editCont, field->string, field->valuestring, val);
        if(cJSON_IsNumber(field))
            common_number_editor(editCont, field->string, field->valueint, val);
    }
    lv_disp_load_scr(rc->scr);
    return rc;
}

static void save_editor_event(lv_event_t* e)
{  
    if(rc == NULL)
        return;
    //lv_disp_load_scr(rc->back_scr);

    rc->msg_data =  NULL;
    rc->msg_type = RECORD_FUN_1;
    API_SettingMenuProcess(rc); 
}

static void del_record_event(lv_event_t* e)
{  
    if(rc == NULL)
        return;
    //lv_disp_load_scr(rc->back_scr);

    rc->msg_data =  NULL;
    rc->msg_type = RECORD_FUN_2;
    API_SettingMenuProcess(rc); 
}


static void cancel_editor_event(lv_event_t* e)
{   
    if(rc == NULL)
        return;
    //lv_disp_load_scr(rc->back_scr);

    rc->msg_data =  NULL;
    rc->msg_type = RECORD_FUN_3;
    API_SettingMenuProcess(rc); 
}

/*****************
 * 删除record编辑页面
 * **************/
void delete_recodr(RECORD_CONTROL* rec)
{
    if (rec->scr) {
        lv_obj_del(rec->scr);
        rec->scr = NULL;
    }
#if 0
    if (rec->msg_data) {
        cJSON_Delete(rec->msg_data);
        rec->msg_data = NULL;
    }
#endif
    lv_mem_free(rec);
    rec = NULL;
    rc = NULL;
}

static lv_obj_t* ui_record_init(void)
{
    lv_obj_t* ui_record = lv_obj_create(NULL);
    lv_obj_clear_flag(ui_record, LV_OBJ_FLAG_SCROLLABLE);
    lv_obj_remove_style_all(ui_record);
    lv_obj_set_size(ui_record, lv_disp_get_hor_res(NULL), lv_disp_get_ver_res(NULL));
    lv_obj_align(ui_record, LV_ALIGN_TOP_LEFT, 0, 0);
    lv_obj_set_style_bg_color(ui_record, lv_palette_main(LV_PALETTE_GREY), 0);
    lv_obj_set_style_bg_opa(ui_record, LV_OPA_100, 0);
    return ui_record;
}
