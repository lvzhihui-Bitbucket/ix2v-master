#include "Common_TableView.h"
#include "icon_common.h"
#include "utility.h"

#define TB_VIEW_FunctionColor       lv_color_hex(0x414500)
#define TB_VIEW_FliterColor         lv_color_hex(0x41004D)
#define TB_VIEW_BackgroundColor     lv_color_hex(0x41454D)
#define TB_VIEW_TextColor           lv_color_white()
#define TB_VIEW_TextHeight          30
#define TB_VIEW_TitleHeight         73
#define TB_VIEW_FilterHeight        70
#define TB_VIEW_LineHeight          70
#define TB_VIEW_TextSize            0
#define TB_VIEW_ReturnWidth         160
#define TB_VIEW_ScrWidth            480

static void TableView_UpdateData(TB_VIWE* disp);
void MainMemu_Pressed_event_cb(lv_event_t* e);

/*****************
 * 初始化表浏览器活动用的scr
 * **************/
static lv_obj_t* TableView_CreatSrc(void)
{
    lv_obj_t* ui_table = lv_obj_create(NULL);
    lv_obj_remove_style_all(ui_table);
    lv_obj_set_size(ui_table, lv_disp_get_hor_res(NULL), lv_disp_get_ver_res(NULL));
    lv_obj_align(ui_table, LV_ALIGN_TOP_LEFT, 0, 0);
    lv_obj_set_style_bg_color(ui_table, TB_VIEW_BackgroundColor, 0);
    lv_obj_clear_flag(ui_table, LV_OBJ_FLAG_SCROLLABLE);
    return ui_table;
}

static void TableView_HeadReturn(lv_event_t* e)
{
    TB_VIWE** tbDisp = lv_event_get_user_data(e);
    if(tbDisp && (*tbDisp) && ((*tbDisp)->returnCallback))
    {
        ((*tbDisp)->returnCallback)();
    }
    TB_MenuDelete(tbDisp);
}

static void TableView_AddTitle(TB_VIWE** disp, char* txt)
{
    TB_VIWE* tu;

    tu = *disp;

    lv_obj_t* btn = lv_obj_create(tu->menu);
    lv_obj_set_size(btn, TB_VIEW_ScrWidth, TB_VIEW_TitleHeight);   
    lv_obj_set_style_bg_color(btn, lv_color_make(26,82,196), 0);
    lv_obj_set_style_pad_all(btn,0,0);
    lv_obj_set_style_border_width(btn, 0, 0);

    lv_obj_add_event_cb(btn, TableView_HeadReturn, LV_EVENT_CLICKED, disp);
    lv_obj_add_flag(btn, LV_OBJ_FLAG_CLICKABLE);
    lv_obj_clear_flag(btn, LV_OBJ_FLAG_SCROLLABLE);
#if 0
    lv_obj_t* img = lv_img_create(btn);
    lv_obj_set_style_text_color(img, TB_VIEW_TextColor, 0);
    lv_obj_align(img, LV_ALIGN_LEFT_MID, 0, 0);
	lv_img_set_src(img, LV_VTK_CALL_RECORD1);
    set_font_size(img,2);
#endif
    lv_obj_t* obj = lv_obj_create(btn);
    lv_obj_remove_style_all(obj);
    lv_obj_add_flag(obj, LV_OBJ_FLAG_EVENT_BUBBLE);
    lv_obj_set_style_bg_opa(obj,LV_OPA_MAX,0);
    lv_obj_set_size(obj,TB_VIEW_ReturnWidth,TB_VIEW_TitleHeight);
    lv_obj_set_style_bg_color(obj,lv_color_make(19,132,250),0);
    lv_obj_set_style_text_color(obj, lv_color_white(), 0);
    lv_obj_t* img = lv_img_create(obj);
	lv_img_set_src(img,LV_VTK_LEFT);
    set_font_size(img,1);
    lv_obj_center(img);

    lv_obj_t* titlelabel = lv_label_create(btn); 
    lv_obj_align(titlelabel, LV_ALIGN_LEFT_MID, TB_VIEW_ReturnWidth, 0);
    lv_obj_set_size(titlelabel, TB_VIEW_ScrWidth-TB_VIEW_ReturnWidth, TB_VIEW_TextHeight);
    lv_obj_set_style_text_align(titlelabel,LV_TEXT_ALIGN_CENTER,0);
    lv_obj_set_style_text_font(titlelabel, vtk_get_font_by_size(TB_VIEW_TextSize), 0); 
    lv_obj_set_style_text_color(titlelabel, TB_VIEW_TextColor, 0);
    vtk_label_set_text(titlelabel,txt);
}

/*****************
 * 左翻页
 * **************/
static void TableView_LeftClick(lv_event_t* e)
{
    TB_VIWE* tu = (TB_VIWE*)lv_event_get_user_data(e);
    if(tu == NULL)
        return;
    if (tu->currentPage)
    {
        tu->currentPage--;
    }
    else
    {
        tu->currentPage = tu->pageNbr - 1;
    }
    TableView_UpdateData(tu);
}
/*****************
 * 右翻页
 * **************/
static void TableView_RightClick(lv_event_t* e)
{
    TB_VIWE* tu = (TB_VIWE*)lv_event_get_user_data(e);
    if(tu == NULL)
        return;

    if(++tu->currentPage >= tu->pageNbr)
    {
        tu->currentPage = 0;
    }
    TableView_UpdateData(tu);
}


static void TableView_PageCtrl(TB_VIWE* disp)
{
    if(!disp)
    {
        return;
    }

    if(disp->pageNbr > 1)
    {
        if(!disp->pageCtrl)
        {
            disp->pageCtrl = lv_obj_create(disp->menu);
            lv_obj_set_style_border_width(disp->pageCtrl, 3, 0);
            lv_obj_set_style_border_color(disp->pageCtrl,lv_color_make(55,55,55),0);
            lv_obj_set_size(disp->pageCtrl, TB_VIEW_ScrWidth, TB_VIEW_LineHeight);
            lv_obj_set_style_pad_all(disp->pageCtrl, 0, 0);
            //标题，filter，表头，表数据
            lv_obj_set_y(disp->pageCtrl, TB_VIEW_TitleHeight + (disp->filterEnable ? TB_VIEW_FilterHeight : 0) + ((disp->headerEnable ? 1 : 0) + disp->lineNbr)*TB_VIEW_LineHeight);
            lv_obj_set_style_bg_color(disp->pageCtrl, TB_VIEW_BackgroundColor, 0);
            lv_obj_clear_flag(disp->pageCtrl, LV_OBJ_FLAG_SCROLLABLE);
            lv_obj_set_style_text_color(disp->pageCtrl, TB_VIEW_TextColor, 0);

            lv_obj_t* leftBtn = lv_btn_create(disp->pageCtrl);
            lv_obj_align(leftBtn, LV_ALIGN_LEFT_MID, 20, 0);
            lv_obj_set_style_text_color(leftBtn, TB_VIEW_TextColor, 0);
            //lv_obj_set_style_bg_color(leftBtn, lv_color_make(230, 231, 232), 0);
            lv_obj_set_size(leftBtn, TB_VIEW_LineHeight+50, TB_VIEW_LineHeight-14);
            lv_obj_set_style_text_font(leftBtn, vtk_get_font_by_size(TB_VIEW_TextSize), 0);
            lv_obj_add_event_cb(leftBtn, TableView_LeftClick, LV_EVENT_CLICKED, disp);
            lv_obj_set_style_radius(leftBtn, 15, 0);
            lv_obj_t* leftlabel = lv_label_create(leftBtn);
            lv_label_set_text(leftlabel, LV_VTK_LEFT);
            lv_obj_center(leftlabel);

            disp->pageLabel = lv_label_create(disp->pageCtrl);
            lv_label_set_text_fmt(disp->pageLabel,"%d/%d", disp->currentPage+1, disp->pageNbr);
            lv_obj_set_style_text_font(disp->pageLabel, vtk_get_font_by_size(TB_VIEW_TextSize), 0);
            //lv_obj_set_style_text_color(disp->pageLabel, TB_VIEW_TextColor, 0);
            lv_obj_align(disp->pageLabel, LV_ALIGN_CENTER, -60, 0);

            lv_obj_t* rightBtn = lv_btn_create(disp->pageCtrl);
            lv_obj_align(rightBtn, LV_ALIGN_CENTER, 50, 0);
            lv_obj_set_style_text_color(rightBtn, TB_VIEW_TextColor, 0);
            //lv_obj_set_style_bg_color(rightBtn, lv_color_make(230, 231, 232), 0);
            lv_obj_set_size(rightBtn, TB_VIEW_LineHeight+50, TB_VIEW_LineHeight-14);
            lv_obj_set_style_text_font(rightBtn, vtk_get_font_by_size(TB_VIEW_TextSize), 0);
            lv_obj_add_event_cb(rightBtn, TableView_RightClick, LV_EVENT_CLICKED, disp);
            lv_obj_set_style_radius(rightBtn, 15, 0);
            lv_obj_t* rightlabel = lv_label_create(rightBtn);
            lv_label_set_text(rightlabel, LV_VTK_RIGHT);
            lv_obj_center(rightlabel);

            disp->cntLabel = lv_label_create(disp->pageCtrl);
            lv_label_set_text_fmt(disp->cntLabel,"%d",disp->recordCnt);
            //lv_obj_set_style_text_color(disp->cntLabel, TB_VIEW_TextColor, 0);
            lv_obj_set_style_text_font(disp->cntLabel, vtk_get_font_by_size(TB_VIEW_TextSize), 0);
            lv_obj_align(disp->cntLabel, LV_ALIGN_RIGHT_MID, -50, 0);
        }
        else
        {
            if(disp->cntLabel)
            {
                lv_label_set_text_fmt(disp->cntLabel,"%d",disp->recordCnt);
            }
            if(disp->pageLabel)
            {
                lv_label_set_text_fmt(disp->pageLabel,"%d/%d",disp->currentPage+1, disp->pageNbr);        
            }
        }
        
    }
    else
    {
        if(disp->pageCtrl)
        {
            lv_obj_del(disp->pageCtrl);
            disp->pageLabel = NULL;
            disp->cntLabel = NULL;
            disp->pageCtrl = NULL;
        }
    }
}

static void draw_table_event_cb(lv_event_t* e)
{
    lv_obj_t* obj = lv_event_get_target(e);
    lv_obj_draw_part_dsc_t* dsc = lv_event_get_draw_part_dsc(e);
    TB_VIWE* tu = (TB_VIWE*)lv_event_get_user_data(e);
    lv_table_t* table = (lv_table_t*)obj;

    if (dsc->part == LV_PART_ITEMS) 
    {
        uint32_t row = dsc->id / lv_table_get_col_cnt(obj);
        uint32_t col = dsc->id - row * lv_table_get_col_cnt(obj);

        dsc->label_dsc->font = vtk_get_font_by_size(TB_VIEW_TextSize);
        dsc->label_dsc->align = LV_TEXT_ALIGN_CENTER;
        dsc->label_dsc->color = TB_VIEW_TextColor;
        dsc->rect_dsc->bg_color = TB_VIEW_BackgroundColor;
        dsc->rect_dsc->border_width = 3;
        dsc->rect_dsc->border_color = lv_color_make(55,55,55);
        //dsc->rect_dsc->outline_width = 10;

        char* text = lv_table_get_cell_value(table, row, col);
        int len = lv_txt_get_width(text, strlen(text), vtk_get_font_by_size(TB_VIEW_TextSize), lv_obj_get_style_text_letter_space(table, LV_PART_ITEMS), LV_TEXT_FLAG_NONE);
        if(len > (table->col_w[col] - 32))
        {
            dsc->label_dsc->ofs_y = -((TB_VIEW_LineHeight - lv_font_get_line_height(vtk_get_font_by_size(TB_VIEW_TextSize)))/2);
        }
        else
        {
            dsc->label_dsc->ofs_y = -((TB_VIEW_LineHeight - lv_font_get_line_height(vtk_get_font_by_size(TB_VIEW_TextSize))*2 - dsc->label_dsc->line_space)/2);
        }
    }
}

static void TableView_FilterClick(lv_event_t* e)
{
    lv_event_code_t code = lv_event_get_code(e);
    lv_obj_t* ta = lv_event_get_target(e);
    TB_VIWE* disp = (TB_VIWE*)lv_event_get_user_data(e);
    cJSON* filter = cJSON_GetObjectItemCaseSensitive(disp->para, "Filter");
    FilterClickCallback callback = (FilterClickCallback)GetEventItemInt(filter, "Callback");
    if(callback)
    {
        callback(filter);
    }
}

static void TableView_FilterDisplay(TB_VIWE* disp)
{
    if(disp->filterEnable)
    {
        cJSON* filter = cJSON_GetObjectItemCaseSensitive(disp->para, "Filter");
        if(!disp->filter)
        {
            disp->filter = lv_obj_create(disp->menu);
            lv_obj_set_style_pad_row(disp->filter, 0, 0);
            lv_obj_set_size(disp->filter, LV_PCT(100), TB_VIEW_FilterHeight);
            lv_obj_set_flex_flow(disp->filter, LV_FLEX_FLOW_ROW);
            lv_obj_set_style_text_font(disp->filter, vtk_get_font_by_size(TB_VIEW_TextSize), 0);
            lv_obj_clear_flag(disp->filter, LV_OBJ_FLAG_SCROLLABLE);
            lv_obj_set_y(disp->filter, TB_VIEW_TitleHeight);
            lv_obj_set_style_bg_color(disp->filter, TB_VIEW_FliterColor, 0);
            lv_obj_add_event_cb(disp->filter, TableView_FilterClick, LV_EVENT_CLICKED, disp);
            lv_obj_set_style_border_width(disp->filter, 0, 0);
            lv_obj_set_style_text_color(disp->filter, TB_VIEW_TextColor, 0);

            disp->filterName = lv_label_create(disp->filter);
            lv_obj_align(disp->filterName, LV_ALIGN_LEFT_MID, 0, 0);           
            lv_obj_set_style_text_align(disp->filterName, LV_TEXT_ALIGN_LEFT,0);
            lv_label_set_text(disp->filterName, GetEventItemString(filter,  "Name"));
            lv_obj_set_size(disp->filterName, 200, LV_SIZE_CONTENT);

            disp->filterValue = lv_label_create(disp->filter);
            lv_obj_set_style_text_align(disp->filterValue, LV_TEXT_ALIGN_LEFT,0);
            lv_obj_align(disp->filterValue, LV_ALIGN_LEFT_MID, 200, 0);
            lv_label_set_text(disp->filterValue, GetEventItemString(filter,  "Value"));
            lv_obj_set_size(disp->filterValue, 240, LV_SIZE_CONTENT);
        }
        else
        {
            lv_label_set_text(disp->filterName, GetEventItemString(filter,  "Name"));
            lv_label_set_text(disp->filterValue, GetEventItemString(filter,  "Value"));
        }
    }
    else
    {
        if(disp->filter)
        {
            lv_obj_del(disp->filter);
            disp->filter = NULL;
        }
    }
}

static void table_edit_event_cb(lv_event_t* e)
{
    TB_VIWE* tu = (TB_VIWE*)lv_event_get_user_data(e);
    lv_obj_t* obj = lv_event_get_target(e);

    uint16_t col;
    uint16_t row;
    lv_table_get_selected_cell(obj, &row, &col);    //获取格子所在的行,列
    uint16_t cnt = lv_table_get_row_cnt(obj);
    if (row >= cnt)
    {
        return;
    }

    if(tu->headerEnable && row == 0)
    {
        return;
    }

    int line = tu->currentPage*tu->lineNbr +  (tu->headerEnable ? row - 1 : row);
    if(tu->cellCallback && line < tu->recordCnt)
    {
        ((CellClickCallback)(tu->cellCallback))(line, col);
    }
}

static void TableView_UpdateData(TB_VIWE* disp)
{
    cJSON* element;
    int col;

    if(!disp)
    {
        return;
    }

    if(!disp->table)
    {
        disp->table = lv_table_create(disp->menu);
        lv_obj_set_style_bg_color(disp->table, TB_VIEW_BackgroundColor, 0);
        lv_obj_add_event_cb(disp->table, draw_table_event_cb, LV_EVENT_DRAW_PART_BEGIN, disp);
        lv_obj_add_event_cb(disp->table, table_edit_event_cb, LV_EVENT_VALUE_CHANGED, disp);
        lv_table_set_row_height(disp->table,TB_VIEW_LineHeight);
        lv_obj_add_flag(disp->table, LV_OBJ_FLAG_SCROLL_CHAIN_HOR);
        lv_obj_set_scroll_dir(disp->table,LV_DIR_HOR);
        lv_obj_set_style_border_width(disp->table, 0, 0);
    }

    lv_obj_set_size(disp->table, TB_VIEW_ScrWidth, (disp->lineNbr + (disp->headerEnable ? 1 : 0))*TB_VIEW_LineHeight);
    lv_obj_set_y(disp->table, TB_VIEW_TitleHeight + (disp->filterEnable ? TB_VIEW_FilterHeight : 0));
    
    lv_table_set_row_cnt(disp->table, 0);
    lv_table_set_col_cnt(disp->table, 0);

    cJSON* record0 = cJSON_GetArrayItem(disp->view, 0);
    if(!record0)
    {
        lv_table_set_col_width(disp->table, 0, TB_VIEW_ScrWidth);
        for (int i = 0; i < disp->lineNbr + (disp->headerEnable ? 1 : 0); i++)
        {
            lv_table_set_cell_value(disp->table, i, 0, "");
        }
    }
    else
    {
        col = 0;
        cJSON_ArrayForEach(element, cJSON_GetObjectItem(disp->para,"ColumnWidth"))
        {
            if(cJSON_IsNumber(element) && element->valueint > 0)
            {
                lv_table_set_col_width(disp->table, col++, element->valueint);
            }
        }

        if(disp->headerEnable)
        {
            col = 0;
            cJSON_ArrayForEach(element, record0)
            {
                vtk_table_set_cell_text(disp->table, 0, col, element->string ? element->string : "");
                col++;
            }
        }

        for (int i = 0; i < disp->lineNbr; i++)
        {
            cJSON* item = cJSON_GetArrayItem(disp->view, disp->currentPage*disp->lineNbr+i);
            int line = (disp->headerEnable ? i + 1 : i);
            if (item)
            {
                col = cJSON_GetArraySize(item);
                for (int j = 0; j < col; j++)
                {
                    cJSON* val = cJSON_GetArrayItem(item, j);
                    if (cJSON_IsString(val))
                    {
                        lv_table_set_cell_value(disp->table, line, j, val->valuestring);
                    }
                    else if (cJSON_IsNumber(val))
                    {
                        lv_table_set_cell_value_fmt(disp->table, line, j, "%d", val->valueint);
                    }
                    else
                    {
                        char tempString[200] = {0};
                        cJSON_PrintPreallocated(val, tempString, 200, 0);
                        lv_table_set_cell_value(disp->table, line, j, tempString);
                    }
                }
            }
        }
    }

    TableView_PageCtrl(disp);
}

static void FunCb(lv_event_t * e)
{
    TB_VIWE* tb = lv_event_get_user_data(e);

    if(tb && tb->funCallback)
    {
        lv_obj_t * obj = lv_event_get_target(e);
        uint32_t id = lv_btnmatrix_get_selected_btn(obj);
        //const char* txt = lv_btnmatrix_get_btn_text(obj, id);
        if(id < TB_VIEW_FUNC_NUM_MAX)
        {
            ((FunctionClickCallback)(tb->funCallback))(tb->functionName[id]);
        }
    }
}

static void TableView_FunctionDisplay(TB_VIWE* disp)
{
    int functionY;
    if(!disp)
    {
        return;
    }

    if(disp->function)
    {
        lv_obj_del(disp->function);
        disp->function = NULL;
    }

    cJSON* function = cJSON_GetObjectItemCaseSensitive(disp->para, "Function");
    cJSON* functionName = cJSON_GetObjectItemCaseSensitive(function, "Name");
    cJSON* functionCallback = cJSON_GetObjectItemCaseSensitive(function, "Callback");
    if(cJSON_IsArray(functionName) && cJSON_IsNumber(functionCallback))
    {
        cJSON* name;
        int i = 0, j = 0;
        cJSON_ArrayForEach(name, functionName)
        {
            if(cJSON_IsString(name))
            {
                disp->functionName[i] = name->valuestring;
                disp->functionNameTranslate[j] = get_language_text(name->valuestring);
                disp->functionNameTranslate[j] = (disp->functionNameTranslate[j] ? disp->functionNameTranslate[j] : name->valuestring);

                if(((disp->functionCnt == 4 || disp->functionCnt == 5) && (i == 1)) || (disp->functionCnt == 6 && i == 2))
                {
                    disp->functionNameTranslate[++j] = "\n";
                }

                j++;

                if(++i >= TB_VIEW_FUNC_NUM_MAX)
                {
                    break;
                }
            }
        }
        disp->functionName[i] = NULL;
        disp->functionNameTranslate[j] = NULL;

        if(i)
        {
            disp->function = lv_btnmatrix_create(disp->menu);
            lv_obj_set_style_shadow_width(disp->function,0,LV_PART_ITEMS);
            lv_obj_set_style_text_color(disp->function, TB_VIEW_TextColor, LV_PART_ITEMS);
            lv_obj_set_style_border_color(disp->function,lv_color_make(55,55,55), 0);
            lv_obj_set_style_bg_color(disp->function, lv_color_make(150, 150, 150),LV_PART_ITEMS);
            set_font_size(disp->function,0);
            lv_obj_set_style_bg_color(disp->function, TB_VIEW_FunctionColor, 0);
            functionY = TB_VIEW_TitleHeight + (disp->filterEnable ? TB_VIEW_FilterHeight : 0) + ((disp->headerEnable ? 1 : 0) + (disp->pageNbr > 1 ? 1 : 0)+ disp->lineNbr)*TB_VIEW_LineHeight;
            lv_obj_set_size(disp->function, TB_VIEW_ScrWidth, 803 - functionY);
            lv_obj_set_y(disp->function, functionY);
            lv_obj_set_style_height(disp->function, 70, LV_PART_ITEMS);
            lv_btnmatrix_set_map(disp->function, disp->functionNameTranslate);
            lv_btnmatrix_set_one_checked(disp->function,true);
            lv_obj_add_event_cb(disp->function, FunCb, LV_EVENT_VALUE_CHANGED, disp);
            disp->funCallback = functionCallback->valueint;
        }
    }
}

/*****************
 * tbDisp: 显示的表配置指针
 * view ： 表显示的所用的json数据
 * title ： 初始化时显示的页数
 * para ：
 {
    "Filter": {
        "Name":"filter",
        "Value":"value",
        "Callback":0
    },
    "HeaderEnable": false,
    "CellCallback": 0,
    "ReturnCallback" 0,
    "ColumnWidth":[100, 100, 100, 100, 100]
 }
 * **************/
void TB_MenuDisplay(TB_VIWE** tbDisp, cJSON* view, char* title, cJSON* para)
{
    int updateFlag = 0;
    TB_VIWE* tu = NULL;
    if(!tbDisp)
    {
        return;
    }

    vtk_lvgl_lock();
    if(*tbDisp)
    {
        tu = *tbDisp;
    }
    else
    {
        tu = (TB_VIWE*)lv_mem_alloc(sizeof(TB_VIWE));
        *tbDisp = tu;
        lv_memset_00(tu, sizeof(TB_VIWE));
        tu->scr = TableView_CreatSrc();
        tu->menu = lv_obj_create(tu->scr);
        lv_obj_set_size(tu->menu, 490, 810);
        lv_obj_align(tu->menu, LV_ALIGN_TOP_MID, 3, -3);
        lv_obj_set_style_pad_all(tu->menu, 0, 0);
        lv_obj_set_style_bg_color(tu->menu, TB_VIEW_BackgroundColor, 0);
        lv_obj_clear_flag(tu->menu, LV_OBJ_FLAG_SCROLLABLE);
        TableView_AddTitle(tbDisp, title);
        updateFlag =1;
        tu->back_scr = lv_scr_act();
        tu->currentPage = 0;
    }

    if(view && !cJSON_Compare(view, tu->view, 1))
    {
        cJSON_Delete(tu->view);
        tu->view = cJSON_Duplicate(view, 1);
        tu->recordCnt = cJSON_GetArraySize(tu->view);
        updateFlag =1;
    }

    if(para && !cJSON_Compare(para, tu->para, 1))
    {
        cJSON_Delete(tu->para);
        tu->para = cJSON_Duplicate(para, 1);

        tu->filterEnable = (cJSON_GetObjectItemCaseSensitive(tu->para, "Filter") ? 1 : 0);
        tu->headerEnable = GetEventItemInt(tu->para,  "HeaderEnable");
        tu->cellCallback = GetEventItemInt(tu->para,  "CellCallback");
        tu->returnCallback = GetEventItemInt(tu->para,  "ReturnCallback");
        updateFlag =1;
    }

    if(updateFlag)
    {
        cJSON* functionName = cJSON_GetObjectItemCaseSensitive(cJSON_GetObjectItemCaseSensitive(tu->para, "Function"), "Name");
        tu->functionCnt = cJSON_GetArraySize(functionName);

        int scrHeight = 803 - TB_VIEW_TitleHeight - (tu->filterEnable ? TB_VIEW_FilterHeight : 0) - (tu->headerEnable ? TB_VIEW_LineHeight : 0) - (tu->functionCnt > 3 ? TB_VIEW_LineHeight + 100 : (tu->functionCnt ? 100 : 0));
        tu->lineNbr = scrHeight/TB_VIEW_LineHeight;
        if(tu->recordCnt > tu->lineNbr)
        {
            tu->lineNbr--;
        }
        tu->pageNbr = tu->recordCnt/tu->lineNbr + (tu->recordCnt%tu->lineNbr ? 1 : 0);
        tu->currentPage = ((tu->currentPage >= tu->pageNbr) ? (tu->pageNbr > 0 ? tu->pageNbr - 1 : 0) : tu->currentPage);

        TableView_FilterDisplay(tu);
        TableView_UpdateData(tu);
        TableView_FunctionDisplay(tu);
    }
    
    if(tu->scr != lv_scr_act())
    {
        lv_disp_load_scr(tu->scr);
    }
    vtk_lvgl_unlock();
}

void TB_MenuDelete(TB_VIWE** tb)
{
    TB_VIWE* disp = NULL;
    if(!tb)
    {
        return;
    }

    vtk_lvgl_lock();
    if(*tb)
    {
        disp = *tb;
        *tb = NULL;

        if(disp->scr == lv_scr_act())
        {
            lv_scr_load(disp->back_scr);
        }
        
        if (disp->scr)
        {
            lv_obj_del(disp->scr);
            disp->scr = NULL;
        }

        if (disp->view)
        {
            cJSON_Delete(disp->view);
            disp->view = NULL;
        }

        if (disp->para)
        {
            cJSON_Delete(disp->para);
            disp->para= NULL;
        }
        
        lv_mem_free(disp);
    }
    vtk_lvgl_unlock();
}