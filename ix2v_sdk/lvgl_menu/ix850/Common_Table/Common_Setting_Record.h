#ifndef Common_Setting_Record_H_
#define Common_Setting_Record_H_

#include "lvgl.h"
#include "cJSON.h"

#define RECORD_FUN_1 23121810901
#define RECORD_FUN_2 23121810902
#define RECORD_FUN_3 23121810903

typedef struct 
{   
    cJSON* msg_data;
    int msg_type;  
    lv_obj_t* back_scr;
    lv_obj_t* scr;                  
    cJSON* rec_data;
    cJSON* EditCheck;

}RECORD_CONTROL;

static lv_obj_t* ui_record_init(void);
static void save_editor_event(lv_event_t* e);
static void del_record_event(lv_event_t* e);
static void cancel_editor_event(lv_event_t* e);
void btn_style_init(void);
static void number_edit_event_cb(lv_event_t* e);
static void number_click_event(lv_event_t* e);
static void text_edit_event_cb(lv_event_t* e);
static void text_click_event(lv_event_t* e);
lv_obj_t* common_text_editor(lv_obj_t* parent, char* name, char* val, int able);
lv_obj_t* common_number_editor(lv_obj_t* parent, char* name, int val,int able);
RECORD_CONTROL* common_record_editor_menu(cJSON* rec_data, const cJSON* editabel, const cJSON* description,const cJSON* func);
void delete_recodr(RECORD_CONTROL* rec);

#endif 