#ifndef Common_Setting_Record_H_
#define Common_Setting_Record_H_

#include "lvgl.h"
#include "cJSON.h"
#include "KeyboardResource.h"

typedef void (*BtnClickCallback)(const char* type, void* user_data);

typedef void (*BackCallback)(const char* type, void* user_data);
#define RECORD_FUNC_NUM_MAX     6      //最多6个功能按键
#define RECORD_FUNC_NAME_LEN    100     //最多100个字符

typedef struct 
{   
    lv_obj_t* back_scr;
    lv_obj_t* scr;                  
    cJSON* rec_data;
    cJSON* EditCheck;
    cJSON* editabel;
    int editType;
    cJSON* description;
    cJSON* func;
    lv_obj_t* menu; 
    lv_obj_t* table;
    int cnt_row;    
    lv_obj_t* function;
    char* functionName[RECORD_FUNC_NUM_MAX+1];
    char* functionNameTranslate[RECORD_FUNC_NUM_MAX+2];
    int functionCnt;
    BtnClickCallback btnCallback;
    BackCallback backCallback;
    VTK_KB_S* kb;
}RECORD_CONTROL;

static lv_obj_t* ui_record_init(void);
static void save_editor_event(lv_event_t* e);
static void del_record_event(lv_event_t* e);
static void cancel_editor_event(lv_event_t* e);

static int* number_edit_event_cb(const char* val,void *user_data);
static void number_click_event(lv_event_t* e);
static int* text_edit_event_cb(const char* val,void *user_data);
static void text_click_event(lv_event_t* e);
static void event_handler(lv_event_t * e);

void RC_MenuDisplay(RECORD_CONTROL** rcDisp,cJSON* rec_data, const cJSON* editabel, const cJSON* description,const cJSON* func,const char* title);
void RC_MenuDelete(RECORD_CONTROL** rc);

#endif 