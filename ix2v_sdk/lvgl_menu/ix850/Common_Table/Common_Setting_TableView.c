#include "Common_Setting_TableView.h"


/*****************
 * 初始化IXG表浏览器
 * view ： 表显示的所用的json数据
 * page ： 初始化时显示的页数
 * Translate ：
 * **************/
TABLE_UTILITY* create_utility_table_display_menu(cJSON* view,char* title,char* fiter,cJSON* para)
{
    lv_obj_t* scr_table = ui_table_init();
    
    TABLE_UTILITY* tu = (TABLE_UTILITY*)lv_mem_alloc(sizeof(TABLE_UTILITY));
    lv_memset_00(tu, sizeof(TABLE_UTILITY));

    lv_obj_t* displaymenu = lv_obj_create(scr_table);
    lv_obj_set_size(displaymenu, 480, 800);
    lv_obj_set_style_pad_all(displaymenu, 0, 0);
    lv_obj_clear_flag(displaymenu, LV_OBJ_FLAG_SCROLLABLE);

    tu->back_scr = lv_scr_act();
    tu->scr = scr_table;
    tu->menu = displaymenu;   

    int size = cJSON_GetArraySize(view);
    char buf[100];
    int cnt = size % 7;
    if (cnt == 0)
    {
        cnt = size / 7;
    }
    else {
        cnt = size / 7 + 1;
    }

    tu->cnt_page = 1;
    tu->min_page = 1;
    tu->max_page = cnt;
    tu->table = NULL;
	tu->cnt_data = cJSON_Duplicate(view,true);
	if(para)
		tu->para=cJSON_Duplicate(para,true);

    SIP_add_title(tu,title);

    if(fiter)
        SIP_add_filter(tu, fiter);   

    SIP_add_change_page(tu, 1, cnt, size);
	
    SIP_add_control(tu);
	
    update_SIP_account_menu(tu, view, tu->cnt_page);

    lv_disp_load_scr(scr_table);
    return tu;
}
void utility_table_fresh(TABLE_UTILITY *disp,cJSON* view)
{
	int size = cJSON_GetArraySize(view);
    
    int cnt = size % 7;
    if (cnt == 0)
    {
        cnt = size / 7;
    }
    else {
        cnt = size / 7 + 1;
    }

    disp->cnt_page = 1;
    disp->min_page = 1;
    disp->max_page = cnt;
	cJSON_Delete(disp->cnt_data);
    disp->cnt_data = cJSON_Duplicate(view,true);
	
	update_SIP_account_menu(disp, view, disp->cnt_page);
}
void SIP_add_title(TABLE_UTILITY* disp,char* txt)
{
    lv_obj_t* titleCont = lv_obj_create(disp->menu);
    lv_obj_align(titleCont,LV_ALIGN_OUT_TOP_MID,0,0);
    lv_obj_set_size(titleCont, LV_PCT(100), 80);
    lv_obj_set_style_pad_all(titleCont, 0, 0);
    //lv_obj_set_style_bg_color(titleCont, lv_palette_main(LV_PALETTE_GREY), 0);
    lv_obj_t* titlelabel = lv_label_create(titleCont);   
    lv_obj_set_style_text_font(titlelabel, &lv_font_montserrat_32, 0); 
    lv_obj_center(titlelabel);
    lv_label_set_text(titlelabel, txt);
}

/*****************
 * 创建过滤器栏
 * parent ：父对象
 * id ： 当前过滤器使用的IXG-ID
 * **************/
void SIP_add_filter(TABLE_UTILITY* disp,char* fiter)
{
	char text[100];
    lv_obj_t* filterCont = lv_obj_create(disp->menu);
    lv_obj_set_style_pad_row(filterCont, 0, 0);
    lv_obj_set_size(filterCont, LV_PCT(100), 100);
    lv_obj_set_flex_flow(filterCont, LV_FLEX_FLOW_ROW);
    lv_obj_set_style_text_font(filterCont, &lv_font_montserrat_32, 0);
    lv_obj_clear_flag(filterCont, LV_OBJ_FLAG_SCROLLABLE);
    lv_obj_set_y(filterCont, 80);

    lv_obj_t* filter_label = lv_label_create(filterCont);
    lv_label_set_text(filter_label, fiter);
    lv_obj_set_size(filter_label, 200, LV_SIZE_CONTENT);

    lv_obj_t* ta = lv_textarea_create(filterCont);
    //lv_textarea_add_text(ta, id);
    lv_obj_set_size(ta, 200, LV_SIZE_CONTENT);
    lv_obj_clear_state(ta, LV_STATE_FOCUSED);

    disp->kb = keyboard_create(disp->scr);
    lv_obj_set_style_text_font(disp->kb, &lv_font_montserrat_26, 0);
    lv_obj_add_flag(disp->kb, LV_OBJ_FLAG_HIDDEN);

    lv_obj_add_event_cb(ta, fiter_edit_event_cb, LV_EVENT_ALL, disp);
}

/*****************
 *调用键盘修改IXG-ID进行过滤
 * **************/
static void fiter_edit_event_cb(lv_event_t* e)
{
    lv_event_code_t code = lv_event_get_code(e);
    lv_obj_t* ta = lv_event_get_target(e);
    TABLE_UTILITY* tu = (TABLE_UTILITY*)lv_event_get_user_data(e);

    //cJSON* where;
	int i;
    if (code == LV_EVENT_CLICKED) {
        if (lv_indev_get_type(lv_indev_get_act()) != LV_INDEV_TYPE_KEYPAD) {
            lv_obj_add_state(ta, LV_STATE_FOCUSED);
            lv_keyboard_set_textarea(tu->kb, ta);

            lv_keyboard_set_mode(tu->kb, LV_KEYBOARD_MODE_NUMBER);      //设置键盘模式为纯数字

            lv_obj_clear_flag(tu->kb, LV_OBJ_FLAG_HIDDEN);
        }
    }
    else if (code == LV_EVENT_CANCEL) {

        lv_obj_add_flag(tu->kb, LV_OBJ_FLAG_HIDDEN);

    }
    else if (code == LV_EVENT_READY)
    {
        char* text = lv_textarea_get_text(ta);
        int i=0;
        while(text[i]!=0&&text[i]>='0'&&text[i]<='9')
            i++;
        
        if(text[i]==0&&i>0)                  //按IXG-ID过滤 
        {
            //where = cJSON_CreateObject();
            //cJSON_AddNumberToObject(where, "IXG_ID", atoi(text));


            lv_obj_add_flag(tu->kb, LV_OBJ_FLAG_HIDDEN);

            tu->msg_data =  text;
            tu->msg_type = TABLE_MSG_FILTER;
            API_SettingMenuProcess(tu);  

           // cJSON_Delete(where);
            lv_obj_clear_state(ta,LV_STATE_FOCUSED);
        }
        else if(text[i]==0&&i==0)             //显示全部记录 
        {
                //where = cJSON_CreateObject();

                lv_obj_add_flag(tu->kb, LV_OBJ_FLAG_HIDDEN);
                tu->msg_data =  NULL;
                tu->msg_type = TABLE_MSG_UNFILTER;
                API_SettingMenuProcess(tu);

                //cJSON_Delete(where);	
                lv_obj_clear_state(ta,LV_STATE_FOCUSED);
        }
        else
        {
            LV_API_TIPS(" Error! Please enter it again!",4);
                //API_TIPS(" Error! Please enter it again!");
        }	  	       
    }
}

/*****************
 * 创建页面显示栏
 * parent : 父对象
 * cnt_page : 当前页
 * max_page ：最大页数
 * size ： 表格总记录数
 * **************/
void SIP_add_change_page(TABLE_UTILITY* disp, int cnt_page,int max_page,int size)
{
    //
    lv_obj_t* pageControl = lv_obj_create(disp->menu);
    lv_obj_set_size(pageControl, 480, 90);
    lv_obj_set_style_pad_all(pageControl, 0, 0);
    lv_obj_set_y(pageControl, 620);

    lv_obj_t* leftBtn = lv_btn_create(pageControl);
    lv_obj_align(leftBtn, LV_ALIGN_LEFT_MID, 20, 0);
    lv_obj_set_style_text_color(leftBtn, lv_color_hex(0xff0000), 0);
    lv_obj_set_style_bg_color(leftBtn, lv_color_make(230, 231, 232), 0);
    lv_obj_set_size(leftBtn, 80, 70);
    lv_obj_set_style_text_font(leftBtn, &lv_font_montserrat_32, 0);
    lv_obj_add_event_cb(leftBtn, left_click_event, LV_EVENT_CLICKED, disp);
    lv_obj_set_style_radius(leftBtn, LV_RADIUS_CIRCLE, 0);
    lv_obj_t* leftlabel = lv_label_create(leftBtn);
    lv_label_set_text(leftlabel, LV_SYMBOL_LEFT);
    lv_obj_center(leftlabel);

    char buf[100];
    sprintf(buf, "%d/%d", cnt_page, max_page);

    lv_obj_t* dispagelabel = lv_label_create(pageControl);
    lv_label_set_text(dispagelabel, buf);
    lv_obj_set_style_text_font(dispagelabel, &lv_font_montserrat_32, 0);
    lv_obj_align(dispagelabel, LV_ALIGN_CENTER, -60, 0);
    disp->pageLabel = dispagelabel;

    lv_obj_t* rightBtn = lv_btn_create(pageControl);
    lv_obj_align(rightBtn, LV_ALIGN_CENTER, 50, 0);
    lv_obj_set_style_text_color(rightBtn, lv_color_hex(0xff0000), 0);
    lv_obj_set_style_bg_color(rightBtn, lv_color_make(230, 231, 232), 0);
    lv_obj_set_size(rightBtn, 80, 70);
    lv_obj_set_style_text_font(rightBtn, &lv_font_montserrat_32, 0);
    lv_obj_add_event_cb(rightBtn, right_click_event, LV_EVENT_CLICKED, disp);
    lv_obj_set_style_radius(rightBtn, LV_RADIUS_CIRCLE, 0);
    lv_obj_t* rightlabel = lv_label_create(rightBtn);
    lv_label_set_text(rightlabel, LV_SYMBOL_RIGHT);
    lv_obj_center(rightlabel);

    sprintf(buf, "%d", size);
    lv_obj_t* discntlabel = lv_label_create(pageControl);
    lv_label_set_text(discntlabel, buf);
    lv_obj_set_style_text_font(discntlabel, &lv_font_montserrat_32, 0);
    lv_obj_align(discntlabel, LV_ALIGN_RIGHT_MID, -50, 0);

    disp->cntLabel = discntlabel;
}

/*****************
 * 创建底部表控制栏
 * parent : 父对象
 * **************/
void SIP_add_control(TABLE_UTILITY* tu)
{
    lv_obj_t* btnCont = lv_obj_create(tu->menu);
    lv_obj_set_size(btnCont, 480, 90);
    lv_obj_set_style_pad_all(btnCont, 0, 0);
    lv_obj_set_y(btnCont, 710);
    //返回
    lv_obj_t* backBtn = lv_btn_create(btnCont);
    lv_obj_align(backBtn, LV_ALIGN_LEFT_MID, 50, 0);
    //lv_obj_set_style_bg_color(backBtn, lv_color_make(230, 231, 232), 0);
    lv_obj_set_size(backBtn, 120, 70);
    lv_obj_set_style_text_font(backBtn, &lv_font_montserrat_32, 0);
    lv_obj_add_event_cb(backBtn, table_exit_event, LV_EVENT_CLICKED, tu);
    lv_obj_set_style_radius(backBtn, LV_RADIUS_CIRCLE, 0);
    lv_obj_t* backlabel = lv_label_create(backBtn);
    lv_label_set_text(backlabel, LV_SYMBOL_BACKSPACE);
    lv_obj_center(backlabel);
    //添加
    if(tu->para==NULL||cJSON_IsTrue(cJSON_GetObjectItem(tu->para,"add_btn")))
    {
	    lv_obj_t* addBtn = lv_btn_create(btnCont);
	    lv_obj_set_size(addBtn, 120, 70);
	    lv_obj_set_style_text_font(addBtn, &lv_font_montserrat_32, 0);
	    //lv_obj_add_event_cb(addBtn, table_add_event, LV_EVENT_CLICKED, NULL);
	    lv_obj_align(addBtn, LV_ALIGN_RIGHT_MID, -50, 0);
	    lv_obj_set_style_radius(addBtn, LV_RADIUS_CIRCLE, 0);
	    //lv_obj_set_style_bg_color(addBtn, lv_color_make(230, 231, 232), 0);
	    lv_obj_t* addlabel = lv_label_create(addBtn);
	    lv_label_set_text(addlabel, "+");
	    lv_obj_center(addlabel);
    }
}

/*****************
 * 退出表浏览器
 * **************/
static void table_exit_event(lv_event_t* e)
{
    TABLE_UTILITY* tu = (TABLE_UTILITY*)lv_event_get_user_data(e);
    lv_disp_load_scr(tu->back_scr);

    tu->msg_data =  NULL;
    tu->msg_type = TABLE_MSG_EXIT;
    API_SettingMenuProcess(tu); 
}

static void draw_table_event_cb(lv_event_t* e)
{
    lv_obj_t* obj = lv_event_get_target(e);
    lv_obj_draw_part_dsc_t* dsc = lv_event_get_draw_part_dsc(e);

    if (dsc->part == LV_PART_ITEMS) {
        uint32_t row = dsc->id / lv_table_get_col_cnt(obj);
        uint32_t col = dsc->id - row * lv_table_get_col_cnt(obj);

        dsc->label_dsc->font = &lv_font_montserrat_24;
        dsc->label_dsc->align = LV_TEXT_ALIGN_CENTER;

        if (row == 0) {
            dsc->label_dsc->align = LV_TEXT_ALIGN_CENTER;
            dsc->rect_dsc->bg_color = lv_color_mix(lv_palette_main(LV_PALETTE_BLUE), dsc->rect_dsc->bg_color, LV_OPA_20);
            dsc->rect_dsc->bg_opa = LV_OPA_COVER;
        }
    }
}
/*****************
 * 更新table控件
 * disp ： table页面对象
 * json ： about页面显示的json数据
 * page ： 当前页
 * **************/
lv_obj_t* update_SIP_account_menu(TABLE_UTILITY* disp,cJSON* json,int page)
{  
    //disp->cnt_data = cJSON_Duplicate(json,true);

    if(disp->table == NULL){
        lv_obj_t* table = lv_table_create(disp->menu);
        lv_obj_set_size(table, 480, 440);
        lv_obj_set_y(table,180);
        //lv_obj_clear_flag(table, LV_OBJ_FLAG_SCROLLABLE);
        lv_obj_add_event_cb(table, draw_table_event_cb, LV_EVENT_DRAW_PART_BEGIN, NULL);
        lv_obj_add_event_cb(table, table_edit_event_cb, LV_EVENT_VALUE_CHANGED, disp);
        lv_table_set_row_height(table,55);
        disp->table = table;
    }
    
    lv_table_set_row_cnt(disp->table, 0);
    lv_table_set_col_cnt(disp->table, 0);
	cJSON *col_w=cJSON_GetObjectItem(disp->para,"col_w");
    if(col_w)
    {
    		int i;
		for(i=0;i<cJSON_GetArraySize(col_w);i++)
		{
			lv_table_set_col_width(disp->table, i, cJSON_GetArrayItem(col_w,i)->valueint);
		}
	}

    update_table_from_json(disp,json,page);

    return disp->table;
}

/*****************
 * 加载表的数据
 * obj ：table对象
 * json ： 用于加载的json数据
 * page ： 当前页
 * **************/
void update_table_from_json(TABLE_UTILITY* disp, cJSON* json,int page)
{
    int size = cJSON_GetArraySize(json);
    char text[100];
    for (int i = 0; i < 7; i++)
    {
        int cnt = ((page - 1) * 7) + i;
        cJSON* item = cJSON_GetArrayItem(json, cnt);
        if (item == NULL)
        {
            break;
        }
        int length = cJSON_GetArraySize(item);
        for (int j = 0; j < length; j++)
        {
            cJSON* val = cJSON_GetArrayItem(item, j);
            if (val == NULL)
            {
                break;
            }
            if (i == 0)
            {             	
                lv_table_set_cell_value(disp->table, i, j, val->string);
            }
            if (cJSON_IsString(val))
            {
                lv_table_set_cell_value(disp->table, i + 1, j, val->valuestring);
            }
            else if (cJSON_IsNumber(val))
            {
                sprintf(text, "%d", val->valueint);
                lv_table_set_cell_value(disp->table, i + 1, j, text);
				
            }
        }
    }
    if(disp->cntLabel)
        lv_label_set_text_fmt(disp->cntLabel,"%d",size);
    if(disp->pageLabel)
        lv_label_set_text_fmt(disp->pageLabel,"%d/%d",page,disp->max_page);
}

/*****************
 * 表编辑事件,表格子被点击以后触发
 * **************/
static void table_edit_event_cb(lv_event_t* e)
{
    TABLE_UTILITY* tu = (TABLE_UTILITY*)lv_event_get_user_data(e);
    lv_obj_t* obj = lv_event_get_target(e);

    uint16_t col;
    uint16_t row;
    lv_table_get_selected_cell(obj, &row, &col);    //获取格子所在的行,列
    uint16_t cnt = lv_table_get_row_cnt(obj);
    if (row >= cnt || row == 0)
    {
        return;
    }

    cJSON* data = cJSON_GetArrayItem(tu->cnt_data, (tu->cnt_page-1)*7+row-1);       //获取格子所在的整条记录

    tu->msg_data =  data;
    tu->msg_type = TABLE_MSG_EDIT_RECORD;
    API_SettingMenuProcess(tu); 
}

/*****************
 * 左翻页
 * **************/
static void left_click_event(lv_event_t* e)
{
    TABLE_UTILITY* tu = (TABLE_UTILITY*)lv_event_get_user_data(e);
    if(tu == NULL)
        return;
    tu->cnt_page--;
    if (tu->cnt_page <= 0)
    {
        tu->cnt_page = tu->max_page;
    }
    update_SIP_account_menu(tu,tu->cnt_data,tu->cnt_page);
}
/*****************
 * 右翻页
 * **************/
static void right_click_event(lv_event_t* e)
{
    TABLE_UTILITY* tu = (TABLE_UTILITY*)lv_event_get_user_data(e);
    if(tu == NULL)
        return;
    tu->cnt_page++;
    if (tu->cnt_page >tu->max_page)
    {
        tu->cnt_page = 1;
    }
    update_SIP_account_menu(tu,tu->cnt_data,tu->cnt_page);
}

/*****************
 * 删除about页面
 * **************/
void delete_table(TABLE_UTILITY* disp)
{
	if(disp->scr==lv_scr_act())
		lv_scr_load(disp->back_scr);
    if (disp->table) {
        lv_obj_del(disp->table);
        disp->table = NULL;
    }
    if (disp->kb) {
        lv_obj_del(disp->kb);
        disp->kb = NULL;
    }
    if (disp->menu) {
        lv_obj_del(disp->menu);
        disp->menu = NULL;
    }
    if (disp->scr) {
        lv_obj_del(disp->scr);
        disp->scr = NULL;
    }
    if (disp->cnt_data) {
        cJSON_Delete(disp->cnt_data);
        disp->cnt_data = NULL;
    }
	if (disp->para) {
        cJSON_Delete(disp->para);
        disp->para= NULL;
    }
#if 0
    if (disp->msg_data) {
        cJSON_Delete(disp->msg_data);
        disp->msg_data = NULL;
    }

#endif
    lv_mem_free(disp);
    disp = NULL;
}

/*****************
 * 初始化表浏览器活动用的scr
 * **************/
static lv_obj_t* ui_table_init(void)
{
    lv_obj_t* ui_table = lv_obj_create(NULL);
    lv_obj_remove_style_all(ui_table);
    lv_obj_clear_flag(ui_table, LV_OBJ_FLAG_SCROLLABLE);
    lv_obj_set_size(ui_table, lv_disp_get_hor_res(NULL), lv_disp_get_ver_res(NULL));
    lv_obj_align(ui_table, LV_ALIGN_TOP_LEFT, 0, 0);
    lv_obj_set_style_bg_color(ui_table, lv_color_white(), 0);
    //lv_obj_set_style_bg_opa(ui_table, LV_OPA_100, 0);
    return ui_table;
}