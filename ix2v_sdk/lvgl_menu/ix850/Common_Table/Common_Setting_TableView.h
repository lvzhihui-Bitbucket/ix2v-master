#ifndef Common_Setting_TableView_H_
#define Common_Setting_TableView_H_

#include "lvgl.h"
#include "cJSON.h"

#define TABLE_MSG_EXIT 23121510901
#define TABLE_MSG_EDIT_RECORD 23121810905
#define TABLE_MSG_FILTER 231220601
#define TABLE_MSG_UNFILTER 231220602


typedef struct 
{   
    cJSON* msg_data;
    int msg_type;  
    lv_obj_t* back_scr;
    lv_obj_t* scr;
    lv_obj_t* menu;        
    lv_obj_t* table;          
    cJSON* cnt_data;
    lv_obj_t* pageLabel;
    lv_obj_t* cntLabel;
    lv_obj_t* kb;
	cJSON *para;
    int cnt_page;
    int min_page;
    int max_page;
}TABLE_UTILITY;

static void draw_table_event_cb(lv_event_t* e);
static lv_obj_t* ui_table_init(void);
void delete_table(TABLE_UTILITY* disp);
void SIP_add_title(TABLE_UTILITY* disp,char* txt);
void SIP_add_filter(TABLE_UTILITY* disp,char* fiter);
void SIP_add_change_page(TABLE_UTILITY* disp, int cnt_page,int max_page,int size);
void SIP_add_control(TABLE_UTILITY* parent);
static void draw_table_event_cb(lv_event_t* e);
lv_obj_t* update_SIP_account_menu(TABLE_UTILITY* disp,cJSON* json,int page);
void update_table_from_json(TABLE_UTILITY* disp, cJSON* json,int page);
static void table_edit_event_cb(lv_event_t* e);
static void left_click_event(lv_event_t* e);
static void right_click_event(lv_event_t* e);
static void table_exit_event(lv_event_t* e);
static void fiter_edit_event_cb(lv_event_t* e);
void utility_table_fresh(TABLE_UTILITY *disp,cJSON* view);

#endif 