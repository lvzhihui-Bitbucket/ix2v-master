#include "menu_utility.h"
#include "MenuHybridMatCreateNew.h"
#include "cJSON.h"
#include "obj_TableSurver.h"
#include "define_string.h"
#include "obj_PublicInformation.h"
#include "utility.h"
#include "task_Event.h"
#include "Common_TableView.h"
#include "obj_IoInterface.h"

#define SAT_HYBRID_EDITOR_PARA		"{\"HeaderEnable\":true,\"CellCallback\":0,\"ColumnWidth\":[120, 120, 180, 200, 180, 160, 160, 160],\"Function\":{\"Name\":[],\"Callback\":0}}}"
#define SAT_HYBRID_EDITOR_VIEW_TEMPALE "{\"IX_TYPE\":\"\",\"DEV_ID\":1,\"ASSO\":[],\"IX_ADDR\":\"\",\"SYS_TYPE\":\"\",\"IX_NAME\":\"\",\"L_NBR\":\"\",\"G_NBR\":\"\"}"

static TB_VIWE* satHybridEditorTb = NULL;
#ifdef IX_LINK
#define HYBRID_SAT_RECORD  "{\"IXG START\":1,\"IXG END\":8,\"IXG NAME PREFIX\":\"IXG\",\"DS START\":1,\"DS END\":4,\"DS NAME PREFIX\":\"DS\",\"Describe\":\"List auto\"}"
#else
#define HYBRID_SAT_RECORD  "{\"DXG START\":1,\"DXG END\":8,\"DXG NAME PREFIX\":\"DXG\",\"DS START\":1,\"DS END\":4,\"DS NAME PREFIX\":\"DS\",\"Describe\":\"List auto\"}" //编辑的记录
#endif
#define APT_SAT_RECORD  "{\"DS START\":1,\"DS END\":4,\"DS NAME PREFIX\":\"DS\",\"Describe\":\"List auto\"}" //编辑的记录

typedef struct 
{   
    char devType[10];
    int devId;
    int lNbr;
    int lNbrLen;
    int gNbr;
    int gNbrLen;
    char name[100];
}SAT_EDIT_ONE_T;

static SAT_EDIT_ONE_T editOne = {.devType = "DS", .devId = 1, .lNbr = 1, .lNbrLen = 1, .gNbr = 1, .gNbrLen = 1, .name = "DS"};

static cJSON* batchCreateSatRecord = NULL;
static RECORD_CONTROL* batchCreateSatMenu = NULL;
static cJSON* editSatRecord = NULL;
static RECORD_CONTROL* editSatMenu = NULL;

void MenuSAT_HYBRID_Editor_Refresh(void);
static void AddSatMenu(RECORD_CONTROL** menu, const cJSON* record, const cJSON* editEnable, BtnCallback btnCallback, const char* title, char* btnString, ...);

static void EditSatFunction(const char* type, void* user_data)
{
	if(editSatRecord)
	{
		char* ixType = GetEventItemString(editSatRecord,  "IX_TYPE");
		int devid = GetEventItemInt(editSatRecord,  "DEV_ID");
		char ixAddr[11];
		
		if(!strcmp(ixType, "DS"))
		{
			snprintf(ixAddr, 11, "00990000%02d", devid);
		}
		else
		{
			snprintf(ixAddr, 11, "0099%02d0001", devid);
		}
		
		cJSON* where = cJSON_CreateObject();
		cJSON_AddStringToObject(where, "IX_ADDR", ixAddr);

		if(!strcmp(type, "Save"))
		{
			cJSON_AddStringToObject(editSatRecord, "IX_ADDR", ixAddr);
			cJSON_AddStringToObject(editSatRecord, "SYS_TYPE", "DH-APT");
			if(API_TB_UpdateByName(TB_NAME_HYBRID_SAT, where, editSatRecord))
			{
				RC_MenuDelete(&editSatMenu);
				API_TableModificationInform(TB_NAME_HYBRID_SAT, NULL);
				MenuSAT_HYBRID_Editor_Refresh();
				API_TIPS_Ext_Time("Save success", 2);
				cJSON_Delete(editSatRecord);
				editSatRecord = NULL;
			}
			else
			{
				API_TIPS_Ext_Time("Save error", 2);
			}
		}
		else if(!strcmp(type, "Delete"))
		{
			if(API_TB_DeleteByName(TB_NAME_HYBRID_SAT, where))
			{
				RC_MenuDelete(&editSatMenu);
				API_TableModificationInform(TB_NAME_HYBRID_SAT, NULL);
				MenuSAT_HYBRID_Editor_Refresh();
				API_TIPS_Ext_Time("Delete success", 2);
			}
			else
			{
				API_TIPS_Ext_Time("Delete error", 2);
			}
			cJSON_Delete(editSatRecord);
			editSatRecord = NULL;
		}
		cJSON_Delete(where);
	}
}

static void ResCellClick(int line, int col)
{
	if(satHybridEditorTb)
	{
		cJSON* record = cJSON_GetArrayItem(satHybridEditorTb->view, line);
		if(cJSON_IsObject(record))
		{
			const char* editEnable[] = {"ASSO", "IX_NAME", "L_NBR", "G_NBR"};
			if(editSatRecord)
			{
				cJSON_Delete(editSatRecord);
			}
			editSatRecord =  cJSON_CreateObject();
			
			cJSON_AddStringToObject(editSatRecord, "IX_TYPE", GetEventItemString(record,  "IX_TYPE"));
			cJSON_AddNumberToObject(editSatRecord, "DEV_ID", GetEventItemInt(record,  "DEV_ID"));
			cJSON_AddItemToObject(editSatRecord, "ASSO", cJSON_Duplicate(cJSON_GetObjectItemCaseSensitive(record, "ASSO"), 1));
			cJSON_AddStringToObject(editSatRecord, "IX_NAME", GetEventItemString(record,  "IX_NAME"));
			cJSON_AddStringToObject(editSatRecord, "L_NBR", GetEventItemString(record,  "L_NBR"));
			cJSON_AddStringToObject(editSatRecord, "G_NBR", GetEventItemString(record,  "G_NBR"));

			cJSON* editorTable =  cJSON_CreateStringArray(editEnable, sizeof(editEnable)/sizeof(editEnable[0]));
			AddSatMenu(&editSatMenu, editSatRecord, editorTable, EditSatFunction, "Edit SAT", "Delete", "Save", NULL);
			cJSON_Delete(editorTable);
		}
	}
}

static void ResReturnCallback(void)
{
	MenuSAT_HYBRID_Editor_Close();
}

static void BatchAddSatFunction(const char* type, void* user_data)
{
    if(!strcmp(type, "Batch add"))
    {
		cJSON* record;
		cJSON* view = CreateSatRecords(batchCreateSatRecord, 256);
		if(cJSON_GetArraySize(view))
		{
			int satRecords = 0;
			char tempString[200];
			char translate[100];

			cJSON_ArrayForEach(record, view)
			{
				if(API_TB_AddByName(TB_NAME_HYBRID_SAT, record))
				{
					satRecords++;
				}
			}
			snprintf(tempString, 200, "%s %d", get_language_text2("Batch add SAT list", translate, 100), satRecords);
			API_TIPS_Ext_Time(tempString, 2);
			API_TableModificationInform(TB_NAME_HYBRID_SAT, NULL);
			API_TB_SaveByName(TB_NAME_HYBRID_SAT);
			MenuSAT_HYBRID_Editor_Refresh();
		}
		cJSON_Delete(view);
    }
}

static void AddSatMenu(RECORD_CONTROL** menu, const cJSON* record, const cJSON* editEnable, BtnCallback btnCallback, const char* title, char* btnString, ...)
{
	char* name;
	#ifdef IX_LINK
	const char* editCheck = "{\"Check\":{\"IX_TYPE\":\"(^DS$)|(^IXG$)\", \"DEV_ID\":\"(^[1-9]{1}$)|(^(1|2)[0-9]{1}$)|(^3[0-2]{1}$)\",\"ASSO\":\"^[[0-9,]{0,50}]$\",\"IX_ADDR\":\"^[0-9]{10}$\",\"L_NBR\":\"^[0-9]{0,6}$\",\"G_NBR\":\"^[0-9]{0,10}$\",\"SYS_TYPE\":\"(^DH-APT$)|(^DH-SS$)\"},\"Translate\":{}}";
	#else
	const char* editCheck = "{\"Check\":{\"IX_TYPE\":\"(^DS$)|(^DXG$)\", \"DEV_ID\":\"(^[1-9]{1}$)|(^(1|2)[0-9]{1}$)|(^3[0-2]{1}$)\",\"ASSO\":\"^[[0-9,]{0,50}]$\",\"IX_ADDR\":\"^[0-9]{10}$\",\"L_NBR\":\"^[0-9]{0,6}$\",\"G_NBR\":\"^[0-9]{0,10}$\",\"SYS_TYPE\":\"(^DH-APT$)|(^DH-SS$)\"},\"Translate\":{}}";
	#endif
	cJSON* editorTable =  cJSON_Parse(editCheck);
	cJSON* func = cJSON_CreateObject();
	cJSON* btnname = cJSON_AddArrayToObject(func,"BtnName");

	va_list valist;
	va_start(valist, btnString);
	for(name = btnString; name; name = va_arg(valist, char*))
	{
		cJSON_AddItemToArray(btnname, cJSON_CreateString(name));
	}
	va_end(valist);

	cJSON_AddNumberToObject(func, "BtnCallback", (int)btnCallback);

    RC_MenuDisplay(menu, record, editEnable, editorTable, func, title);
    cJSON_Delete(editorTable);
    cJSON_Delete(func);
}

static void EditOneRecordProcessingInitialValues(const cJSON* lastRecord)
{
	snprintf(editOne.devType, 10, "%s", GetEventItemString(lastRecord,  "IX_TYPE"));
	editOne.devId = GetEventItemInt(lastRecord,  "DEV_ID");
	char* tempString = GetEventItemString(lastRecord,  "L_NBR");
	editOne.lNbrLen = strlen(tempString);
	editOne.lNbr = atoi(tempString);

	tempString = GetEventItemString(lastRecord,  "G_NBR");
	editOne.gNbrLen = strlen(tempString);
	editOne.gNbr = atoi(tempString);

	if(++editOne.devId > 32)
	{
		editOne.devId = 1;
	}
	editOne.gNbr++;
	editOne.lNbr++;
	snprintf(editOne.name, 100, "%s", GetEventItemString(lastRecord,  "IX_NAME"));
}

static void AddSatFunction(const char* type, void* user_data)
{
    if(!strcmp(type, "Save"))
	{
		if(editSatRecord)
		{
			char* ixType = GetEventItemString(editSatRecord,  "IX_TYPE");
			int devid = GetEventItemInt(editSatRecord,  "DEV_ID");
			char ixAddr[11];
			
			if(!strcmp(ixType, "DS"))
			{
				snprintf(ixAddr, 11, "00990000%02d", devid);
			}
			else
			{
				snprintf(ixAddr, 11, "0099%02d0001", devid);
			}
			cJSON_AddStringToObject(editSatRecord, "IX_ADDR", ixAddr);
			cJSON_AddStringToObject(editSatRecord, "SYS_TYPE", "DH-APT");
							
			if(API_TB_AddByName(TB_NAME_HYBRID_SAT, editSatRecord))
			{
				RC_MenuDelete(&editSatMenu);
				API_TableModificationInform(TB_NAME_HYBRID_SAT, NULL);
				EditOneRecordProcessingInitialValues(editSatRecord);
				MenuSAT_HYBRID_Editor_Refresh();
				API_TIPS_Ext_Time("Add success", 2);
				cJSON_Delete(editSatRecord);
				editSatRecord = NULL;
            }
			else
			{
				API_TIPS_Ext_Time("Add error", 2);
			}
		}
	}
}

static void FunctionClick(const char* funcName)
{
	if(funcName)
	{
		if(!strcmp(funcName, "Add"))
		{
			char format[20];
			char temp[100];
			const char* editEnable[] = {"IX_TYPE", "DEV_ID", "ASSO", "IX_NAME", "L_NBR", "G_NBR"};

			if(editSatRecord)
			{
				cJSON_Delete(editSatRecord);
			}
			editSatRecord =  cJSON_CreateObject();
			cJSON_AddStringToObject(editSatRecord, "IX_TYPE", editOne.devType);
			cJSON_AddNumberToObject(editSatRecord, "DEV_ID", editOne.devId);
			cJSON_AddItemToObject(editSatRecord, "ASSO", cJSON_CreateArray());
			cJSON_AddStringToObject(editSatRecord, "IX_NAME", editOne.name);

			snprintf(format, 20, "%%0%dd", editOne.lNbrLen);
			snprintf(temp, 100, format, editOne.lNbr);
			cJSON_AddStringToObject(editSatRecord, "L_NBR", temp);

			snprintf(format, 20, "%%0%dd", editOne.gNbrLen);
			snprintf(temp, 100, format, editOne.gNbr);
			cJSON_AddStringToObject(editSatRecord, "G_NBR", temp);

			cJSON* editorTable =  cJSON_CreateStringArray(editEnable, sizeof(editEnable)/sizeof(editEnable[0]));
			AddSatMenu(&editSatMenu, editSatRecord, editorTable, AddSatFunction, "Add SAT", "Save", NULL);
			cJSON_Delete(editorTable);
		}
		else if (!strcmp(funcName, "Batch add"))
		{
			if(batchCreateSatRecord)
			{
				cJSON_Delete(batchCreateSatRecord);
			}
			batchCreateSatRecord = cJSON_Parse(GetMenuType() ? APT_SAT_RECORD : HYBRID_SAT_RECORD);
			BatchCreateSatMenu(batchCreateSatRecord, &batchCreateSatMenu, NULL, BatchAddSatFunction, "Batch add SAT", "Batch add", NULL);
		}
	}
}

void MenuSAT_HYBRID_Editor_Refresh(void)
{
	if(satHybridEditorTb)
	{
		cJSON* view = cJSON_CreateArray();
		cJSON_AddItemToArray(view, cJSON_Parse(SAT_HYBRID_EDITOR_VIEW_TEMPALE));
		API_TB_SelectBySortByName(TB_NAME_HYBRID_SAT, view, NULL, 0);
		cJSON_DeleteItemFromArray(view, 0);
		TB_MenuDisplay(&satHybridEditorTb, view, "SAT editor", NULL);
		cJSON_Delete(view);
	}
}

void MenuSAT_HYBRID_Editor_Init(void)
{
	cJSON* para = cJSON_Parse(SAT_HYBRID_EDITOR_PARA);
	cJSON_ReplaceItemInObjectCaseSensitive(para, "CellCallback", cJSON_CreateNumber((int)ResCellClick));
	cJSON_AddNumberToObject(para, "ReturnCallback", (int)ResReturnCallback);

	cJSON* func = cJSON_CreateObject();
	cJSON* funcName = cJSON_AddArrayToObject(func, "Name");
	cJSON_AddItemToArray(funcName, cJSON_CreateString("Add"));
	cJSON_AddItemToArray(funcName, cJSON_CreateString("Batch add"));
	cJSON_AddNumberToObject(func, "Callback", (int)FunctionClick);
	cJSON_ReplaceItemInObjectCaseSensitive(para, "Function", func);

	cJSON* view = cJSON_CreateArray();
	cJSON_AddItemToArray(view, cJSON_Parse(SAT_HYBRID_EDITOR_VIEW_TEMPALE));
	API_TB_SelectBySortByName(TB_NAME_HYBRID_SAT, view, NULL, 0);
	cJSON_DeleteItemFromArray(view, 0);

	TB_MenuDisplay(&satHybridEditorTb, view, "SAT editor", para);
	cJSON_Delete(view);
	cJSON_Delete(para);
}

void MenuSAT_HYBRID_Editor_Close(void)
{
	RC_MenuDelete(&editSatMenu);
	RC_MenuDelete(&batchCreateSatMenu);
	
	if(editSatRecord)
	{
		cJSON_Delete(editSatRecord);
		editSatRecord = NULL;
	}

	if(batchCreateSatRecord)
	{
		cJSON_Delete(batchCreateSatRecord);
		batchCreateSatRecord = NULL;
	}
	TB_MenuDelete(&satHybridEditorTb);
	API_Event_NameAndMsg(EventMenuSettingProcess, "SettingRefresh", NULL);
}

//0-hybrid; 1-apt;
int GetMenuType(void)
{
	int ret;
	char* menuType = API_Para_Read_String2(MenuType);
	if(menuType && !strcmp(menuType, "DH-HYBRID"))
	{
		ret = 0;
	}
	else 
	{
		ret = 1;
	}

	return ret;
}
