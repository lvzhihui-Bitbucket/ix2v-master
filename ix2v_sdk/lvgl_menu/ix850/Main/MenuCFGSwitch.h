#ifndef MENUCFGSWITCH_H_
#define MENUCFGSWITCH_H_

#include "lv_ix850.h"
#include "obj_CommonSelectMenu.h"

cJSON* GetMenuCFGListName(const char * dir);

void MenuCfg_Select_init(void);
lv_obj_t* MenuCfg_preview_init(char* name);
static void choose_menuCfg_pos(const cJSON* filter);
static void choose_pos_event(const char* chose, void* userdata);
#endif 