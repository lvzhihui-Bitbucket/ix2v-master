#include <stdio.h>
#include "task_CallServer.h"
#include "elog_forcall.h"
#include "obj_PublicInformation.h"
#include "task_Event.h"
#include "ix850_icon.h"
#include "obj_lvgl_msg.h"
#include "obj_IXS_Proxy.h"
#include "VtkListCont.h"
#include "obj_TableSurver.h"
#include "obj_IoInterface.h"
#include "menu_page.h"

static cJSON *roomList_Cache=NULL;
//static cJSON *nameList_Cache=NULL; 
static cJSON *roomList_Test_Cache=NULL;
static cJSON *nameList_Test_Cache=NULL;
static lv_obj_t* NameListCont=NULL;
static lv_obj_t* RoomListCont=NULL;

void common_click_event(lv_event_t* e);

void list_scroll_event2(lv_event_t* e);
int TbAlphabeticalOrder(cJSON* tb, const char *key_name );
static pthread_mutex_t  GetNamelist_lock = PTHREAD_MUTEX_INITIALIZER;
int GetNamelistCallback(cJSON *cmd)
{
	cJSON *NameList_temp=NULL;
	pthread_mutex_lock(&GetNamelist_lock);//vtk_lvgl_lock();
	#if 0
	if(API_Para_Read_Int(IXRListEnable))
	{
		NameList_temp = cJSON_CreateArray();
		API_TB_SelectBySortByName(TB_NAME_IXRLIST, NameList_temp, NULL, 0);
	}
	else
	#endif
	{
		//IXS_ProxyGetTb(&NameList_temp, NULL, R8001AndSearch);
		NameList_temp=GetDataSetList();
	}
	cJSON *Filter_list=ListFilterByHybridASSO(NameList_temp);
	
	//NameList_Update();
	NamelistDataUpate(Filter_list);
	
	cJSON_Delete(Filter_list);
	cJSON_Delete(NameList_temp);
	//vtk_lvgl_unlock();
	pthread_mutex_unlock(&GetNamelist_lock);
}

void lv_msg_NamelistReload_Callback(void* s, lv_msg_t* m)
{
    LV_UNUSED(s);
    cJSON* number = NULL;
    char* result = NULL;
	struct ICON_INS* icon;
	cJSON* cache;
	cJSON* icon_json;
	PAGE_INS* page;
    switch (lv_msg_get_id(m)) 
	{
	    	case IX850_MSG_Namelist_Reload:

			cache = lv_msg_get_payload(m);

			MainMenuUpdate_list(MAIN_PAGE_NAMELIST,NAMELIST_LIST,cache);

#if 0
			//printf_json(nameList_Cache, __func__);
			page = Get_Page_by_name(MAIN_PAGE_NAMELIST);
			if(page)
			{
				 icon_json = cJSON_GetObjectItem(page->page_icons,NAMELIST_LIST);
				if(icon_json)
				{
					icon = (struct ICON_INS*)icon_json->valueint;

					if(icon->icon_cont)
					{
						//printf("11111111%08x:%08x",icon->icon_cont,NameListCont);

						if(nameList_Test_Cache!=NULL)
							VtkListContFresh(icon->icon_cont,nameList_Test_Cache);
						else if(nameList_Cache!=NULL)
							VtkListContFresh(icon->icon_cont,nameList_Cache);

					}
				}
			}

			page = Get_Page_by_name(MAIN_PAGE_ROOMLIST);
			if(page)
			{
			 	icon_json = cJSON_GetObjectItem(page->page_icons,ROOMLIST_LIST);
				if(icon_json)
				{
					icon = (struct ICON_INS*)icon_json->valueint;

					if(icon->icon_cont)
					{
						//printf("11111111%08x:%08x",icon->icon_cont,NameListCont);
						//lv_obj_clean(icon->icon_cont);
						if(roomList_Test_Cache!=NULL)
							VtkListContFresh(icon->icon_cont,roomList_Test_Cache);
						else if(roomList_Cache!=NULL)
							VtkListContFresh(icon->icon_cont,roomList_Cache);
							
						//VtkListContFresh(icon->icon_cont,nameList_Test_Cache);
					}
				}
			}
#endif
	}
}

void NameListPageReset(void)
{
	#if 1
	PAGE_INS* page;
	struct ICON_INS* icon;
	cJSON* icon_json;
	
	page = Get_Page_by_name(MAIN_PAGE_NAMELIST);
	icon_json = cJSON_GetObjectItem(page->page_icons,NAMELIST_LIST);
	if(icon_json)
	{
		icon = (struct ICON_INS*)icon_json->valueint;

		if(icon->icon_cont)
		{
			VtkListReset(icon->icon_cont);	
		}
	}
	#endif
}

cJSON *NamelistDataFilter(const cJSON *origin_tb,const cJSON *filter)
{
	cJSON* view = cJSON_CreateArray();
	cJSON* where = cJSON_CreateObject();
	cJSON_AddStringToObject(where, "IX_TYPE", "IM");
	cJSON_AddStringToObject(where, "IX_ADDR", GetSysVerInfo_bd());
	//cJSON_AddStringToObject(where, "Platform", "IX1/2");
	API_TB_SelectBySort(origin_tb, view, where, 0);
	cJSON_Delete(where);

	return view;
	
}



cJSON *CreateRoomlistByConfig(const cJSON* dev_tb,cJSON *config)
{
	int size=cJSON_GetArraySize(dev_tb);
	int i;
	cJSON *item;
	cJSON *one_node;
	cJSON *List;
	char ix_addr[20];
	char name[100];
	char buff[100];
	if(size==0)
		return NULL;
	int num_disp_type=API_Para_Read_Int(RoomNumDispType);
	int list_type=API_Para_Read_Int(MainListType);
	if(list_type==0)
		SortTableView(dev_tb,"IX_ADDR",0);
	else
		SortTableView(dev_tb,"IX_NAME",0);
	List=cJSON_CreateArray();
	for(i=0;i<size;i++)
	{
		item=cJSON_GetArrayItem(dev_tb,i);
		one_node=cJSON_CreateObject();
		cJSON_AddItemToArray(List,one_node);
		
		if(GetJsonDataPro(item,"IX_ADDR",ix_addr)==1)
		{
			cJSON_AddStringToObject(one_node,LIST_LV_LOAD,ix_addr);
		}
		else
			cJSON_AddStringToObject(one_node,LIST_LV_LOAD,"-");
		name[0]=0;
		if(list_type==0)
		{
			if(num_disp_type==0)
			{
				memcpy(buff,ix_addr+4,4);
				buff[4]=0;
				sprintf(name,"[%d]",atoi(buff));
			}
			else if(num_disp_type==1)
			{
				if(GetJsonDataPro(item,"L_NBR",buff)==1)
					sprintf(name,"[%s]",buff);		
			}
			else if(num_disp_type==2)
			{
				if(GetJsonDataPro(item,"G_NBR",buff)==1)
					sprintf(name,"[%s]",buff);		
			}
					
		}
		
		
		
		if(GetJsonDataPro(item,"IX_NAME",buff)==1)
		{
			strcat(name,buff);
			cJSON_AddStringToObject(one_node,LIST_SHOW,name);
		}
		else
			cJSON_AddStringToObject(one_node,LIST_SHOW,"-");
		
		cJSON_AddItemToObject(one_node, "ListKey", cJSON_Duplicate(cJSON_GetObjectItemCaseSensitive(item, "ListKey"), 1));		 
		 
	}
	//TbAlphabeticalOrder(List,LIST_LV_LOAD);

	return List;
}

cJSON *CreateNamelistByConfig(const cJSON* dev_tb,cJSON *config)
{
	int size=cJSON_GetArraySize(dev_tb);
	int i;
	cJSON *item;
	cJSON *one_node;
	cJSON *List;
	char ix_addr[20];
	char name[50];
	char buff[40];
	if(size==0)
		return NULL;
	List=cJSON_CreateArray();
	for(i=0;i<size;i++)
	{
		item=cJSON_GetArrayItem(dev_tb,i);
		one_node=cJSON_CreateObject();
		cJSON_AddItemToArray(List,one_node);
		
		if(GetJsonDataPro(item,"IX_ADDR",ix_addr)==1)
		{
			cJSON_AddStringToObject(one_node,LIST_LV_LOAD,ix_addr);
		}
		else
			cJSON_AddStringToObject(one_node,LIST_LV_LOAD,"-");

		
		
		if(GetJsonDataPro(item,"IX_NAME",name)==1)
		{
			//strcat(name,buff);
			cJSON_AddStringToObject(one_node,LIST_SHOW,name);
		}
		else
			cJSON_AddStringToObject(one_node,LIST_SHOW,"-");
		
		cJSON_AddItemToObject(one_node, "ListKey", cJSON_Duplicate(cJSON_GetObjectItemCaseSensitive(item, "ListKey"), 1));		 
	}
	TbAlphabeticalOrder(List,LIST_SHOW);
	return List;
}

void NamelistDataUpate(cJSON* origin_tb)
{
	//cJSON *filter_tb;
	cJSON *list_temp;
	int update_flag=0;

	#if 0
	if(!API_Para_Read_Int(IXRListEnable))
	{
		filter_tb=NamelistDataFilter(origin_tb,NULL);
	}	
	else
	#endif
	{
		//filter_tb=cJSON_Duplicate(origin_tb,1);
	}
#if 0	
	list_temp=CreateRoomlistByConfig(filter_tb,NULL);
	
	if(!cJSON_Compare(roomList_Cache,list_temp,1))
	{
		if(roomList_Cache!=NULL)
		{
			cJSON_Delete(roomList_Cache);
		}
		roomList_Cache=list_temp;
		update_flag=1;
	}
	else
		cJSON_Delete(list_temp);
#endif	
	
	cJSON *nameList_Cache_Save;//=nameList_Cache;
	
	nameList_Cache_Save=CreateRoomlistByConfig(origin_tb,NULL);
	#if 0
	if(!cJSON_Compare(nameList_Cache,list_temp,1))
	{
		if(nameList_Cache!=NULL)
		{
			cJSON_Delete(nameList_Cache);
		}
		nameList_Cache=list_temp;
		update_flag=1;
	}
	else
		cJSON_Delete(list_temp);
	#endif
	//printf_json(nameList_Cache,__func__);
	//if(update_flag)
	//nameList_Cache = CreateRoomlistByConfig(filter_tb,NULL);
	vtk_lvgl_lock();
	lv_msg_send(IX850_MSG_Namelist_Reload,nameList_Cache_Save);
	vtk_lvgl_unlock();
	//cJSON_Delete(filter_tb);
	if(nameList_Cache_Save)
		cJSON_Delete(nameList_Cache_Save);
}

static void MainNamelist_back(lv_event_t* e)
{
	MainMenu_Process(MAINMENU_NL_BACK,NULL);
}

static void MainNamelist_search(lv_event_t* e)
{
	MainMenu_Process(MAINMENU_NL_SEARCH,NULL);
}
static void MainNamelist_jump(lv_event_t* e)
{
	MainMenu_Process(MAINMENU_NL_JUMP,NULL);
	//MainMenu_Process(MAINMENU_MAIN_ROOMLIST,NULL);
}
lv_obj_t*CreateMainNamelist(void)
{
	lv_obj_t* bg = lv_obj_create(lv_scr_act());
	lv_obj_remove_style_all(bg);
    lv_obj_set_size(bg, 480, 800);
    lv_obj_set_style_pad_all(bg, 0, 0);
    lv_obj_clear_flag(bg, LV_OBJ_FLAG_SCROLLABLE);
	lv_obj_t* bkgd_img = lv_img_create(bg);
	lv_img_set_src(bkgd_img, "S:/mnt/nand1-1/App/res/bg.png");
    //lv_obj_align(bkgd_img, LV_ALIGN_TOP_MID, 0, -20);
	
	lv_obj_t* header = lv_obj_create(bg);
	lv_obj_remove_style_all(header);
    lv_obj_align(header, LV_ALIGN_TOP_MID, 0, 0);
    //lv_obj_set_size(header, 480, 50);
    lv_obj_set_size(header, LV_PCT(100), 90);
    //lv_obj_set_style_pad_all(header, 0, 0);
    lv_obj_set_style_border_side(header, LV_BORDER_SIDE_BOTTOM, 0);
    lv_obj_set_style_border_width(header, 2, 0);
	lv_obj_set_style_border_color(header,lv_color_make(63,63,63),0);
    //lv_obj_set_style_radius(header, 0, 0);
    //lv_obj_set_style_bg_opa(header, LV_OPA_0, 0);

   lv_obj_t* backBtn = lv_btn_create(header);
   lv_obj_remove_style_all(backBtn);
    lv_obj_add_event_cb(backBtn, MainNamelist_back, LV_EVENT_CLICKED, NULL);
    lv_obj_align(backBtn, LV_ALIGN_LEFT_MID, 0, 0);
    lv_obj_set_size(backBtn, 80, 80);
    lv_obj_t* backLabel = lv_label_create(backBtn);
    lv_label_set_text(backLabel, LV_VTK_BACK);
    lv_obj_align(backLabel, LV_ALIGN_CENTER, 0, 0);
    lv_obj_set_style_text_font(backLabel, font_larger, 0);
	lv_obj_set_style_text_color(backLabel,lv_color_white(),0);
#if 0
    lv_obj_t* seachText = lv_textarea_create(header);
    lv_obj_align(seachText, LV_ALIGN_CENTER, 0, 0);
    lv_obj_set_size(seachText, 250, 80);
    lv_obj_add_event_cb(seachText, search, LV_EVENT_ALL, NULL);
#endif
    lv_obj_t* jumpBtn = lv_btn_create(header);
	lv_obj_remove_style_all(jumpBtn);
    lv_obj_align(jumpBtn, LV_ALIGN_RIGHT_MID, 0, 0);
    lv_obj_set_size(jumpBtn, 80, 80);
    lv_obj_add_event_cb(jumpBtn, MainNamelist_jump, LV_EVENT_CLICKED, NULL);
    lv_obj_t* jumpLabel = lv_label_create(jumpBtn);
    lv_label_set_text(jumpLabel, "123");
    lv_obj_align(jumpLabel, LV_ALIGN_CENTER, 0, 0);
    lv_obj_set_style_text_font(jumpLabel, font_larger, 0);
	lv_obj_set_style_text_color(jumpLabel,lv_color_white(),0);

	 lv_obj_t* list = lv_obj_create(bg);
	 lv_obj_remove_style_all(list);
    lv_obj_set_size(list, 480, 710);
    lv_obj_set_y(list,90); 
    lv_obj_set_flex_flow(list, LV_FLEX_FLOW_COLUMN);
    //lv_obj_clear_flag(list, LV_OBJ_FLAG_SCROLL_MOMENTUM);
    lv_obj_set_scroll_snap_y(list, LV_SCROLL_SNAP_NONE);
    //lv_obj_clear_flag(list, LV_OBJ_FLAG_SNAPPABLE);
    //lv_obj_clear_flag(list, LV_OBJ_FLAG_SCROLLABLE);
    lv_obj_set_style_bg_opa(list, LV_OPA_MIN, 0);
    //lv_obj_set_style_pad_all(list, 0, 0);
    //lv_obj_set_style_pad_column(list, 0, 0);
    lv_obj_set_style_pad_row(list, 0, 0);
    //lv_obj_set_style_border_width(list,3,0);
    //lv_obj_set_style_bg_img_src(list, "S:/mnt/nand1-1/App/res/bg.png", 0);
    //lv_obj_set_style_bg_img_src(list, &bg_test, 0);

    lv_obj_add_event_cb(list, list_scroll_event2, LV_EVENT_ALL, NULL);
lv_obj_set_user_data(list,NULL);
	NameListCont=list;

  	
	
	return bg;
}

void DeleteMainNamelist(lv_obj_t *namelist)
{
	if(namelist)
		lv_obj_del(namelist);
	NameListCont=NULL;
}

static void MainRoomlist_back(lv_event_t* e)
{
	MainMenu_Process(MAINMENU_RL_BACK,NULL);
}

static void MainRoomlist_search(lv_event_t* e)
{
	MainMenu_Process(MAINMENU_RL_SEARCH,NULL);
}
static void MainRoomlist_jump(lv_event_t* e)
{
	MainMenu_Process(MAINMENU_RL_JUMP,NULL);
	//MainMenu_Process(MAINMENU_MAIN_NAMELIST,NULL);
}

static void MainMenu_return_main(lv_event_t* e)
{
	
	MainMenu_Process(MAINMENU_Return_MainPage,NULL);
}

lv_obj_t*CreateMainRoomlist(void)
{
	lv_obj_t* bg = lv_obj_create(lv_scr_act());
	lv_obj_remove_style_all(bg);
    lv_obj_set_size(bg, 480, 800);
    lv_obj_set_style_pad_all(bg, 0, 0);
    lv_obj_clear_flag(bg, LV_OBJ_FLAG_SCROLLABLE);
	lv_obj_t* bkgd_img = lv_img_create(bg);
	lv_img_set_src(bkgd_img, "S:/mnt/nand1-1/App/res/bg.png");
    //lv_obj_align(bkgd_img, LV_ALIGN_TOP_MID, 0, -20);
	
	lv_obj_t* header = lv_obj_create(bg);
	lv_obj_remove_style_all(header);
    lv_obj_align(header, LV_ALIGN_TOP_MID, 0, 0);
    //lv_obj_set_size(header, 480, 50);
    lv_obj_set_size(header, LV_PCT(100), 90);
    //lv_obj_set_style_pad_all(header, 0, 0);
    lv_obj_set_style_border_side(header, LV_BORDER_SIDE_BOTTOM, 0);
    lv_obj_set_style_border_width(header, 2, 0);
	lv_obj_set_style_border_color(header,lv_color_make(63,63,63),0);

    lv_obj_t* backBtn = lv_btn_create(header);
	lv_obj_remove_style_all(backBtn);
    lv_obj_add_event_cb(backBtn, MainRoomlist_back, LV_EVENT_CLICKED, NULL);
    lv_obj_align(backBtn, LV_ALIGN_LEFT_MID, 0, 0);
    lv_obj_set_size(backBtn, 80, 80);
    lv_obj_t* backLabel = lv_label_create(backBtn);
    lv_label_set_text(backLabel, LV_VTK_BACK);
    lv_obj_align(backLabel, LV_ALIGN_CENTER, 0, 0);
    lv_obj_set_style_text_font(backLabel, font_larger, 0);
	lv_obj_set_style_text_color(backLabel,lv_color_white(),0);
#if 0
    lv_obj_t* seachText = lv_textarea_create(header);
    lv_obj_align(seachText, LV_ALIGN_CENTER, 0, 0);
    lv_obj_set_size(seachText, 250, 80);
    lv_obj_add_event_cb(seachText, search, LV_EVENT_ALL, NULL);
#endif
    lv_obj_t* jumpBtn = lv_btn_create(header);
	lv_obj_remove_style_all(jumpBtn);
    lv_obj_align(jumpBtn, LV_ALIGN_RIGHT_MID, 0, 0);
    lv_obj_set_size(jumpBtn, 80, 80);
    lv_obj_add_event_cb(jumpBtn, MainRoomlist_jump, LV_EVENT_CLICKED, NULL);
    lv_obj_t* jumpLabel = lv_label_create(jumpBtn);
    lv_label_set_text(jumpLabel, "ABC");
    lv_obj_align(jumpLabel, LV_ALIGN_CENTER, 0, 0);
    lv_obj_set_style_text_font(jumpLabel, &lv_font_montserrat_32, 0);
	lv_obj_set_style_text_color(jumpLabel,lv_color_white(),0);

	 lv_obj_t* list = lv_obj_create(bg);
	 lv_obj_remove_style_all(list);
    lv_obj_set_size(list, 480, 710);
    lv_obj_set_y(list,90); 
    lv_obj_set_flex_flow(list, LV_FLEX_FLOW_COLUMN);
    //lv_obj_clear_flag(list, LV_OBJ_FLAG_SCROLL_MOMENTUM);
    lv_obj_set_scroll_snap_y(list, LV_SCROLL_SNAP_NONE);
    //lv_obj_clear_flag(list, LV_OBJ_FLAG_SNAPPABLE);
    lv_obj_clear_flag(list, LV_OBJ_FLAG_SCROLLABLE);
    lv_obj_set_style_bg_opa(list, LV_OPA_MIN, 0);
    //lv_obj_set_style_pad_all(list, 0, 0);
    //lv_obj_set_style_pad_column(list, 0, 0);
    lv_obj_set_style_pad_row(list, 0, 0);
    //lv_obj_set_style_border_width(list,3,0);
    //lv_obj_set_style_bg_img_src(list, "S:/mnt/nand1-1/App/res/bg.png", 0);
    //lv_obj_set_style_bg_img_src(list, &bg_test, 0);

    lv_obj_add_event_cb(list, list_scroll_event2, LV_EVENT_ALL, NULL);
lv_obj_set_user_data(list,NULL);
	RoomListCont=list;

  	
	
	return bg;
}
void DeleteMainRoomlist(lv_obj_t *roomlist)
{
	if(roomlist)
		lv_obj_del(roomlist);
	RoomListCont=NULL;
}

int TbAlphabeticalOrder(cJSON* tb, const char *key_name )	//czn_20190506
{
	int j, k,i;	   
	char str1[100];
	char str2[100];
	int len1;
	int len2;
	cJSON *node,*node_cmp;


	int size=cJSON_GetArraySize(tb);
	
	if( tb == NULL ||cJSON_GetArraySize(tb)<2||GetJsonDataPro(cJSON_GetArrayItem(tb,0),key_name,str1) == 0)
	{
		return -1;
	}
	
	for (j = 0; j < size - 1; j++) 	//??????????????????????????????????ptable->record_cnt-1  
	{
		for (k = 0; k < size - 1 - j; k++) 	//?????????????j?????? ????????????????	
		{
			node=cJSON_GetArrayItem(tb,k);
			//len1 = 40;
			//get_one_record_string(&(ptable->precord[k]), key_index, str1, &len1);
			GetJsonDataPro(node,key_name,str1);
			//str1[len1] = 0;
			len1=strlen(str1);
			for(i = 0;i < len1;i++)
			{
				str1[i] = toupper(str1[i]);
			}
			//len2 = 40;
			//get_one_record_string(&(ptable->precord[k+1]), key_index, str2, &len2);
			//str2[len2] = 0;
			node_cmp=cJSON_GetArrayItem(tb,k+1);
			GetJsonDataPro(node_cmp,key_name,str2);
			len2=strlen(str2);
			for(i = 0;i < len2;i++)
			{
				str2[i] = toupper(str2[i]);
			}
			if (strcmp(str1, str2) > 0)
			{
			
				node_cmp=cJSON_DetachItemFromArray(tb,k+1);
				cJSON_InsertItemInArray(tb,k,node_cmp);
			}
		}
	}
	
	
	return 0;
}

#if 1

lv_obj_t* Img_add_list(lv_obj_t* parent, struct ICON_INS* picon)
{
    cJSON* pos = cJSON_GetObjectItem(picon->icon_data, get_global_key_string(GLOBAL_ICON_KEYS_POS));
	if(pos == NULL)
		return NULL;
	cJSON* height = cJSON_GetObjectItemCaseSensitive(pos, get_global_key_string(GLOBAL_ICON_KEYS_HEIGHT));
    cJSON* width = cJSON_GetObjectItemCaseSensitive(pos, get_global_key_string(GLOBAL_ICON_KEYS_WIDTH));
    cJSON* posx = cJSON_GetObjectItemCaseSensitive(pos, get_global_key_string(GLOBAL_ICON_KEYS_POS_X));
    cJSON* posy = cJSON_GetObjectItemCaseSensitive(pos, get_global_key_string(GLOBAL_ICON_KEYS_POS_Y));

	lv_obj_t* list = lv_obj_create(parent);
	//lv_obj_remove_style_all(list);
    //lv_obj_set_flex_flow(list, LV_FLEX_FLOW_COLUMN);	
    lv_obj_clear_flag(list, LV_OBJ_FLAG_SCROLLABLE);
    lv_obj_set_x(list, posx->valueint);
    lv_obj_set_y(list, posy->valueint);
    lv_obj_set_height(list, height->valueint);
    lv_obj_set_width(list, width->valueint);
	lv_obj_set_style_bg_opa(list, LV_OPA_MIN, 0);
	lv_obj_set_style_border_opa(list, 0, 0);
	
	//lv_obj_set_style_pad_all(list, 0, 0);
	//lv_obj_set_style_pad_hor(list,10,0);
	//lv_obj_set_style_pad_ver(list,0,0);

	//create_one_list_page(list);
	init_page_list_para();
	picon->icon_cont = list;
    return list;
}
#else
lv_obj_t* Img_add_list(lv_obj_t* parent, struct ICON_INS* picon)
{
    cJSON* pos = cJSON_GetObjectItem(picon->icon_data, get_global_key_string(GLOBAL_ICON_KEYS_POS));
	if(pos == NULL)
		return NULL;
	cJSON* height = cJSON_GetObjectItemCaseSensitive(pos, get_global_key_string(GLOBAL_ICON_KEYS_HEIGHT));
    cJSON* width = cJSON_GetObjectItemCaseSensitive(pos, get_global_key_string(GLOBAL_ICON_KEYS_WIDTH));
    cJSON* posx = cJSON_GetObjectItemCaseSensitive(pos, get_global_key_string(GLOBAL_ICON_KEYS_POS_X));
    cJSON* posy = cJSON_GetObjectItemCaseSensitive(pos, get_global_key_string(GLOBAL_ICON_KEYS_POS_Y));

    lv_obj_t* tabview = lv_tabview_create(parent,LV_DIR_BOTTOM,10);	
	picon->icon_cont = tabview;
    lv_obj_clear_flag(tabview, LV_OBJ_FLAG_SCROLLABLE);
    lv_obj_set_x(tabview, posx->valueint);
    lv_obj_set_y(tabview, posy->valueint);
    lv_obj_set_height(tabview, height->valueint);
    lv_obj_set_width(tabview, width->valueint);
	lv_obj_set_style_bg_opa(tabview, LV_OPA_MIN, 0);


    lv_obj_t* tabview = lv_tabview_create(parent,LV_DIR_BOTTOM,10);	
	picon->icon_cont = tabview;
    lv_obj_clear_flag(tabview, LV_OBJ_FLAG_SCROLLABLE);
    lv_obj_set_x(tabview, posx->valueint);
    lv_obj_set_y(tabview, posy->valueint);
    lv_obj_set_height(tabview, height->valueint);
    lv_obj_set_width(tabview, width->valueint);
	lv_obj_set_style_bg_opa(tabview, LV_OPA_MIN, 0);

    return tabview;
}
#endif
lv_obj_t* Img_add_btn(lv_obj_t* parent, struct ICON_INS* picon)
{
    cJSON* pos = cJSON_GetObjectItem(picon->icon_data, get_global_key_string(GLOBAL_ICON_KEYS_POS));
    cJSON* text = cJSON_GetObjectItem(picon->icon_data, get_global_key_string(GLOBAL_ICON_KEYS_TEXT));
	cJSON* fun = cJSON_GetObjectItem(picon->icon_data, get_global_key_string(GLOBAL_ICON_KEYS_FUNCTION));

    cJSON* txt = cJSON_GetObjectItem(text,get_global_key_string(GLOBAL_ICON_KEYS_LABEL_TEXT));
    cJSON* size = cJSON_GetObjectItem(text,get_global_key_string(GLOBAL_ICON_KEYS_TEXT_SIZE));
    cJSON* color = cJSON_GetObjectItem(text,get_global_key_string(GLOBAL_ICON_KEYS_TEXT_COLOR));

    lv_obj_t* icon = creat_cont_for_postion(parent, pos);
    picon->icon_cont = icon;
	lv_obj_set_style_text_opa(icon,0,0);
    lv_obj_t* label = lv_label_create(icon);
	lv_obj_center(label);
    if(size)
        set_font_size(label,size->valueint);
    if(txt){
        char* name = get_symbol_key(txt->valuestring);
        lv_label_set_text(label, name);
    }       
    if(color)
        lv_obj_set_style_text_color(label,lv_color_hex(color->valueint),0);  
    
	if(fun){
		if(strcmp(fun->valuestring,"back") == 0){
			lv_obj_add_event_cb(icon, MainMenu_return_main, LV_EVENT_CLICKED, NULL);
		}		
		else if(strcmp(fun->valuestring,"nameJump") == 0)
			lv_obj_add_event_cb(icon, MainNamelist_jump, LV_EVENT_CLICKED, NULL);
		else if(strcmp(fun->valuestring,"roomJump") == 0)
			lv_obj_add_event_cb(icon, MainRoomlist_jump, LV_EVENT_CLICKED, NULL);
		else
			lv_obj_add_event_cb(icon, common_click_event, LV_EVENT_CLICKED, fun->valuestring);
	}

    return icon;
}

int ShellCmd_main_list(cJSON *cmd)
{
    int channel,onoff,win;
    char* para1 = GetShellCmdInputString(cmd, 1);
    char* para2 = GetShellCmdInputString(cmd, 2);
    char* para3= GetShellCmdInputString(cmd, 3);
	char* para4= GetShellCmdInputString(cmd, 4);
	cJSON **p_tb=NULL;
	cJSON *tb=NULL;
	if(para1==NULL||para2==NULL)
		return 1;
	vtk_lvgl_lock();
   if(strcmp(para1,"name")==0)
   	p_tb=&nameList_Test_Cache;
   else
   	p_tb=&roomList_Test_Cache;
   if(strcmp(para2,"del")==0)
   {
	cJSON_Delete(*p_tb);
	*p_tb=NULL;
	lv_msg_send(IX850_MSG_Namelist_Reload,NULL);
	ShellCmdPrintf("delete test data ok");
   }
   else if(strcmp(para2,"tb")==0&&para3!=NULL)
   {
	tb=cJSON_Parse(para3);
	if(tb!=NULL)
	{
		cJSON_Delete(*p_tb);
		*p_tb=tb;
		lv_msg_send(IX850_MSG_Namelist_Reload,NULL);
		ShellCmdPrintf("update tb ok");
	}
	else
	{
		ShellCmdPrintf("parse tb err");
	}
   }
    else if(strcmp(para2,"file")==0&&para3!=NULL)
   {
	tb=GetJsonFromFile(para3);
	if(tb!=NULL)
	{
		cJSON_Delete(*p_tb);
		*p_tb=tb;
		lv_msg_send(IX850_MSG_Namelist_Reload,NULL);
		ShellCmdPrintf("update file ok");
	}
	else
	{
		ShellCmdPrintf("load file err");
	}
   }
	else if(strcmp(para2,"show")==0)
	{
		#if 0
		 if(strcmp(para1,"name")==0)
		 {	
		 	if(nameList_Test_Cache!=NULL)
				p_tb=&nameList_Test_Cache;
			else
				p_tb=&nameList_Cache;
			
		 }
		  else
		    {	
		 	if(roomList_Test_Cache!=NULL)
				p_tb=&roomList_Test_Cache;
			else
				p_tb=&roomList_Cache;
		 }
		  char *str=cJSON_Print(*p_tb);
		  if(str!=NULL)
		  {
			ShellCmdPrintf("%s",str);
		  }
		  else
		  {
			ShellCmdPrintf("show err");
		  }
		  #endif
	}
   vtk_lvgl_unlock();
   return 0;	
}




