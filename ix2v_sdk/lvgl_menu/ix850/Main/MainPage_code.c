#include "MainPage_code.h"
#include "obj_lvgl_msg.h"

static void code_keyboard_click_event(lv_event_t* e);

extern const lv_font_t* font_larger;

PAGE_INS* code_menu_creat(lv_obj_t* parent)
{
    PAGE_INS* ppage = (PAGE_INS*)lv_mem_alloc(sizeof(PAGE_INS));
    lv_memset_00(ppage, sizeof(PAGE_INS));
    ppage->page_icons = cJSON_CreateObject();

    if (ppage == NULL)
        return NULL;
    lv_obj_t* codeCont = lv_obj_create(parent);
    lv_obj_remove_style_all(codeCont);
    lv_obj_set_size(codeCont, 480, 800); 
    ppage->page_obj = codeCont;

    lv_obj_t* bkgd_img = lv_img_create(codeCont);
    lv_img_set_src(bkgd_img, "S:/customer/nand1-1/App/res/bg.bin");
    //lv_img_set_src(bkgd_img, "S:/customer/nand1-1/App/res/bg.png");
    lv_obj_clear_flag(codeCont, LV_OBJ_FLAG_SCROLLABLE); 
    ppage->page_bkgd =  bkgd_img;

    code_keyboard_creat(ppage);

    return ppage;
}

lv_obj_t* code_keyboard_creat(PAGE_INS* ppage)
{
    lv_obj_t* keyCont = lv_obj_create(ppage->page_obj);
    lv_obj_set_size(keyCont, LV_PCT(100), 720);
    lv_obj_set_y(keyCont,80);
    lv_obj_set_style_border_side(keyCont, 0, 0);
    lv_obj_set_style_bg_opa(keyCont,LV_OPA_MIN,0);

    lv_obj_t* codeNum = lv_label_create(keyCont);
    lv_obj_set_style_text_font(codeNum, &lv_font_montserrat_48, 0);
    lv_obj_align(codeNum, LV_ALIGN_TOP_MID, 0, 100);
    lv_label_set_text(codeNum, "");
    lv_obj_set_style_text_color(codeNum,lv_color_white(),0);

    cJSON_AddNumberToObject(ppage->page_icons, CODE_NUM, (int)codeNum);

    lv_obj_t* code1 = add_radiobutton(keyCont, "1", code_keyboard_click_event,codeNum);
    lv_obj_align(code1, LV_ALIGN_TOP_MID, -140, 330);

    lv_obj_t* code2 = add_radiobutton(keyCont, "2", code_keyboard_click_event,codeNum);
    lv_obj_align(code2, LV_ALIGN_TOP_MID, 0, 330);

    lv_obj_t* code3 = add_radiobutton(keyCont, "3", code_keyboard_click_event,codeNum);
    lv_obj_align(code3, LV_ALIGN_TOP_MID, 140, 330);

    lv_obj_t* code4 = add_radiobutton(keyCont, "4", code_keyboard_click_event,codeNum);
    lv_obj_align(code4, LV_ALIGN_TOP_MID, -140, 420);

    lv_obj_t* code5 = add_radiobutton(keyCont, "5", code_keyboard_click_event,codeNum);
    lv_obj_align(code5, LV_ALIGN_TOP_MID, 0, 420);

    lv_obj_t* code6 = add_radiobutton(keyCont, "6", code_keyboard_click_event,codeNum);
    lv_obj_align(code6, LV_ALIGN_TOP_MID, 140, 420);

    lv_obj_t* code7 = add_radiobutton(keyCont, "7", code_keyboard_click_event,codeNum);
    lv_obj_align(code7, LV_ALIGN_TOP_MID, -140, 510);

    lv_obj_t* code8 = add_radiobutton(keyCont, "8", code_keyboard_click_event,codeNum);
    lv_obj_align(code8, LV_ALIGN_TOP_MID, 0, 510);

    lv_obj_t* code9 = add_radiobutton(keyCont, "9", code_keyboard_click_event,codeNum);
    lv_obj_align(code9, LV_ALIGN_TOP_MID, 140, 510);

    lv_obj_t* codeback = add_radiobutton(keyCont, LV_VTK_BACKSPACE, code_keyboard_click_event,codeNum);
    lv_obj_align(codeback, LV_ALIGN_TOP_MID, -140, 600);
    lv_obj_set_style_text_color(codeback,lv_color_make(150,61,53),0);

    lv_obj_t* code0 = add_radiobutton(keyCont, "0", code_keyboard_click_event,codeNum);
    lv_obj_align(code0, LV_ALIGN_TOP_MID, 0, 600);

    lv_obj_t* codeKey = add_radiobutton(keyCont, LV_VTK_CONFIM, code_keyboard_click_event,codeNum);
    lv_obj_align(codeKey, LV_ALIGN_TOP_MID, 140, 600);
    lv_obj_set_style_text_color(codeKey,lv_color_make(46,204,127),0);
    //lv_obj_set_style_bg_color(codeKey, lv_color_make(163, 219, 129), 0);
    //lv_obj_set_style_bg_opa(codeKey, LV_OPA_COVER, 0);

    return keyCont;
}

lv_obj_t* code_control_creat(lv_obj_t* parent)
{
    lv_obj_t* cont = lv_obj_create(parent);
    lv_obj_set_size(cont, LV_PCT(100), 100);
    lv_obj_set_y(cont,700);
    lv_obj_set_style_border_side(cont, 0, 0);
    lv_obj_set_style_bg_opa(cont,LV_OPA_MIN,0);

    return cont;
}

static void code_keyboard_click_event(lv_event_t* e)
{
    lv_obj_t* obj = lv_event_get_target(e);
    lv_obj_t* label = lv_obj_get_child(obj, 0);
    lv_obj_t* codeNum = lv_event_get_user_data(e);
    char* text = lv_label_get_text(label);
	char update_buff[100];
	if(CodeKpInput_Process(text,update_buff,NULL))
	{
        //printf("code_keyboard_click_event 1111111111111111 \n");
        //MainMenuUpdate_password(MAIN_PAGE_CODE,CODE_NUM,update_buff);
        //MainMenuUpdate(MAIN_PAGE_CODE,CODE_NUM,update_buff);
	}

}

/**********************
 * 添加一个密码文本
 * parent ：父对象
 * ********************/
#define LV_VTK_ADDRESS_LIST             "\xEE\x98\x92"
lv_obj_t* img_add_password_label(lv_obj_t* parent)
{
    lv_obj_t* obj = lv_label_create(parent);
    
    //lv_obj_set_size(obj, 35, 35);
    lv_label_set_text(obj,LV_VTK_CIRCLE);
    //lv_obj_set_style_radius(obj, LV_RADIUS_CIRCLE, 0);
    //lv_obj_set_style_border_width(obj, 0, 0);
    lv_obj_set_style_text_color(obj, lv_color_white(), 0);

    return obj;
}

lv_obj_t* Img_add_password(lv_obj_t* parent, struct ICON_INS* picon)
{
    cJSON* pos = cJSON_GetObjectItem(picon->icon_data, get_global_key_string(GLOBAL_ICON_KEYS_POS));

    lv_obj_t* icon = creat_cont_for_postion(parent, pos);
    picon->icon_cont = icon;

    lv_obj_set_flex_flow(icon, LV_FLEX_FLOW_ROW_WRAP);
    set_font_size(icon,2);
    lv_obj_set_style_pad_column(icon, 10, 0);
    lv_obj_set_style_pad_row(icon, 10, 0);
    int Password_Len = 4;
    for (size_t i = 0; i < Password_Len; i++)
    {
        img_add_password_label(icon);
       //lv_obj_t *label= lv_label_create(icon);
	//lv_label_set_text(label,LV_VTK_SETTING);
	//lv_label_set_text(label,"a");
    }
}

lv_obj_t* Img_add_codebtn(lv_obj_t* parent, struct ICON_INS* picon)
{
    cJSON* txt = NULL;
    cJSON* size = NULL;
    cJSON* color = NULL;
    cJSON* pos = cJSON_GetObjectItem(picon->icon_data, get_global_key_string(GLOBAL_ICON_KEYS_POS));
    cJSON* text = cJSON_GetObjectItem(picon->icon_data, get_global_key_string(GLOBAL_ICON_KEYS_TEXT));

    if(text){
        txt = cJSON_GetObjectItem(text,get_global_key_string(GLOBAL_ICON_KEYS_LABEL_TEXT));
        size = cJSON_GetObjectItem(text,get_global_key_string(GLOBAL_ICON_KEYS_TEXT_SIZE));
        color = cJSON_GetObjectItem(text,get_global_key_string(GLOBAL_ICON_KEYS_TEXT_COLOR));
    }

    lv_obj_t* icon = creat_cont_for_postion(parent, pos);
    lv_obj_set_style_text_opa(icon,0,0);
    picon->icon_cont = icon;
    lv_obj_t* label = lv_label_create(icon);

    if(size)
        set_font_size(label,size->valueint);
    if(txt){       
        char* name = get_symbol_key(txt->valuestring);
        lv_label_set_text(label, name);
    }else{
        lv_label_set_text(label, "");
    } 
        
    if(color)
        lv_obj_set_style_text_color(label,lv_color_hex(color->valueint),0);  
    lv_obj_center(label);

    lv_obj_add_event_cb(icon, code_keyboard_click_event, LV_EVENT_CLICKED, NULL);

    return icon;
}

lv_obj_t* add_radiobutton(lv_obj_t* parent, const char* txt, lv_event_cb_t cb,lv_obj_t* target)
{
    lv_obj_t* obj = lv_btn_create(parent);
    lv_obj_set_size(obj, 132, 75);
    lv_obj_add_event_cb(obj, cb, LV_EVENT_CLICKED, target);
    lv_obj_set_style_radius(obj,10,0);
    //lv_obj_set_style_border_color(obj,lv_color_make(90,89,81),0);
    lv_obj_set_style_border_width(obj,0,0);
    //lv_obj_set_style_bg_opa(obj,LV_OPA_50,0);
    lv_obj_set_style_text_color(obj,lv_color_white(),0);
    lv_obj_set_style_text_font(obj,font_larger,0);
    lv_obj_set_style_bg_color(obj, lv_color_make(90,89,81), 0);

    lv_obj_t* label = lv_label_create(obj);
    lv_label_set_text(label, txt);
    lv_obj_center(label);
    
    return obj;
}

void Code_Keyboard_exit(PAGE_INS* ppage)
{
    if(ppage){
        dele_one_page(ppage);
    } 
}