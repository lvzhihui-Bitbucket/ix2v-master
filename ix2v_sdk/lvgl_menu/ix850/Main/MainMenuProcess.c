#include <stdio.h>
#include "task_CallServer.h"
#include "elog_forcall.h"
#include "obj_PublicInformation.h"
#include "task_Event.h"
#include "ix850_icon.h"
#include "obj_lvgl_msg.h"
#include "table_display.h"
#include "define_file.h"
#include "menu_common.h"
#include "obj_IoInterface.h"
#include "task_Beeper.h"
#include "obj_TableSurver.h"
#include "keyboard_test.h"
#include "obj_PasswordCheck.h"
#include "menu_page.h"

static lv_obj_t * MainMenu_Scr=NULL;
static lv_obj_t * MainPage=NULL;
static lv_obj_t * MainHeader=NULL;
static lv_obj_t * MainNamelist=NULL;
static lv_obj_t * MainRoomlist=NULL;
static lv_obj_t * MainDialKp=NULL;
static lv_obj_t * MainCodeKp=NULL;
MENU_INS *MainPageIns = NULL;
static lv_obj_t *MainMenu_Scr_return=NULL;
static lv_obj_t *SettingMenu_Save=NULL;

static lv_timer_t *MainTimer=NULL;

static int MainMenu_Reset_Timer=0;
static int MainMenu_OnInsatller_Timer=0;

static int ScrBr_Reset_Timer=0;
static int ScrBr_Auto_Time=0;
static int ScrBr_State=0;
enum
{
	MainMenu_Stanby=0,
	MainMenu_ToNamelist,	
	MainMenu_ToRoomlist,
	MainMenu_ToDialKp,
	MainMenu_ToCodeKp,
	MainMenu_ToCallMenu,
	MainMenu_ToSetting,
	MainMenu_ToTest,
};
static int MainMenuState=0;
static int MainStartCalling=0;
static int MainStartCallTime=0;

static char DialKpInput_Buff[20]={0};
static int DialKpInput_Len=0;

static char CodeKpInput_Buff[20]={0};
static int CodeKpInput_Len=0;
void *call_tips=NULL;
static const char* MENU_CFG_DIR[] = {MENU_CONFIG_SD_PATH, MENU_CONFIG_CUS_PATH, MENU_CONFIG_FIX_PATH, NULL};
#define MENU_CFG_DEFAULT "{\"menu_pid\":\"850\",\"menu_ver\":\"1.0\",\"menu_desc\":\"202408011748\",\"page_list\":{\"main\":\"MenuPage_normal_main\",\"code\":\"MenuPage_normal_code\",\"digital\":\"MenuPage_normal_digital\",\"namelist\":\"MenuPage_normal_namelist\",\"roomlist\":\"MenuPage_normal_roomlist\"},\"home_page\":\"main\"}"

void main_reset_callTime(void);

extern void AddOneMenuCommonClickedCb(lv_obj_t* menu,lv_event_cb_t cb);
extern void MainMemu_Pressed_event_cb(lv_event_t* e);
void MainTimer_cb(lv_timer_t* timer);
	
lv_obj_t * Create_MainScreen(void)
{
	lv_obj_t * Scr;
	//if(MainMenu_Scr == NULL)
	//{
	    Scr = lv_obj_create(NULL);
	    lv_obj_clear_flag(Scr, LV_OBJ_FLAG_SCROLLABLE);
	    lv_obj_remove_style_all(Scr);
	    lv_obj_set_size(Scr, lv_disp_get_hor_res(NULL), lv_disp_get_ver_res(NULL));
	    lv_obj_align(Scr, LV_ALIGN_TOP_LEFT, 0, 0);
	    lv_obj_set_style_bg_color(Scr, lv_palette_main(LV_PALETTE_GREY), 0);
	    lv_obj_set_style_bg_opa(Scr, LV_OPA_100, 0);
	//}
	return Scr;

}
void MainHeaderHide(void)
{
	if(MainPageIns&&MainPageIns->header)
		lv_obj_add_flag(MainPageIns->header,LV_OBJ_FLAG_HIDDEN);
}
void MainHeaderShow(void)
{
	if(MainPageIns&&MainPageIns->header)
		lv_obj_clear_flag(MainPageIns->header,LV_OBJ_FLAG_HIDDEN);
}
void MainMenu_Init(void)
{
	vtk_lvgl_lock(); 
	if(MainMenu_Scr == NULL)
	{
		MainMenu_Scr = Create_MainScreen();
		MainMenu_Scr_return = lv_scr_act();
	}

	char act_name[200];
	API_Para_Read_String("UI_Documents",act_name);
	menu_config_load(act_name);
	
	
	MainMenuState = MainMenu_Stanby;

	if(MainTimer==NULL)
	{
		main_call_page_init();
		MainTimer = lv_timer_create(MainTimer_cb, 1000, NULL);
	}
	lv_disp_load_scr(MainMenu_Scr);
	MainMenu_Reset();
	vtk_lvgl_unlock(); 
}
void main_menu_release(void)
{
	if(MainPageIns != NULL)
	{
		deinit_one_menu(MainPageIns);
		MainPageIns = NULL;
	}
}
int menu_config_load(const char* filename)
{
	char strbuf1[500];
    char* menu_config_path;
	int index;
	PAGE_INS* ppage;
	// create menu
	if(MainPageIns != NULL)
	{
		deinit_one_menu(MainPageIns);
		MainPageIns = NULL;
	}
	MainPageIns = init_one_menu("MainMenu", filename, MainMenu_Scr);
	if(MainPageIns == NULL){
		printf("Failed to load, please check the file format!");
        return -2;
	}

	for(index = 0, menu_config_path = NULL; MENU_CFG_DIR[index]; index++)
	{
		snprintf(strbuf1, 500, "%s/%s.json", MENU_CFG_DIR[index], filename);
		if(IsFileExist(strbuf1))
		{
			menu_config_path = strbuf1;
			break;
		}
	}

	cJSON* menu_config = GetJsonFromFile(menu_config_path);
    if(!menu_config)
	{
		menu_config =cJSON_Parse(MENU_CFG_DEFAULT);
    }

	cJSON* list = cJSON_GetObjectItem(menu_config,"page_list");
	cJSON* pageName;
	cJSON_ArrayForEach(pageName, list)
	{
		if(cJSON_IsString(pageName) && pageName->string)
		{
			for(index = 0, menu_config_path = NULL; MENU_CFG_DIR[index]; index++)
			{
				snprintf(strbuf1, 500, "%s/%s", MENU_CFG_DIR[index], pageName->valuestring);
				if(IsFileExist(strbuf1))
				{
					menu_config_path = strbuf1;
					break;
				}
			}

			ppage = create_one_menu_page(MainPageIns, menu_config_path);
			if (ppage)
			{
				AddOneMenuCommonClickedCb(ppage->page_obj, MainMemu_Pressed_event_cb);
			}
			else 
			{
				API_PublicInfo_Write_String(pageName->string,"");
			}
		}
	}

	MainPageIns->header = add_header(MainMenu_Scr);
	
	cJSON* home = cJSON_GetObjectItem(menu_config,"Home_page");
	char *home_page;
	if((home_page=API_Para_Read_String2("Home_page"))&&switch_one_menu_page(MainPageIns,home_page)!=NULL)
	{
		API_PublicInfo_Write_String("Home_page",home_page);
	}
	else 
	{
		//char *home_page;
		//if((home_page=API_Para_Read_String2("Home_page"))&&switch_one_menu_page(MainPageIns,home_page)!=NULL)
		if(home&&switch_one_menu_page(MainPageIns,home->valuestring)!=NULL)
		{
			API_PublicInfo_Write_String("Home_page",home->valuestring);
		}
		else
		{
			switch_one_menu_page(MainPageIns,MAIN_PAGE_MAIN);
			API_PublicInfo_Write_String("Home_page",MAIN_PAGE_MAIN);
		}
	}
	cJSON_Delete(menu_config);
	API_PublicInfo_Write_String("cur_menu",get_name_of_one_menu_config(MainPageIns));

	//printf("------------------------ load finish ---------------------\n");
	return 1;
}

void MainMenu_Process(int msg_type,void* arg)
{
	//�յ����ذ������£�����popDisplayLastMenu��������һ��˵�?
	//popDisplayLastMenu();
	//�յ�ĳһ�����ݱ�������Ϣ��׼�����ݣ�����SwitchOneMenu�л�������IXG�ֻ��༭�˵�
	//SwitchOneMenu(MenuOneIXGImEdit,arg,1);
	//printf("222222222%s:%d,msg=%d\n",__func__,__LINE__,msg_type);
	
	switch(msg_type)
	{
		case MAINMENU_MAIN_NAMELIST:
			if(API_PublicInfo_Read_Bool("MenuEdit")&&API_PublicInfo_Read_Bool("OnInstaller"))
			{
				API_EnterSettingRoot("MainListConfig");
				Main_Header_Reset();
				SettingMenu_Save=NULL;
				MainMenuState=MainMenu_ToSetting;
			}
			else
			{
				switch_one_menu_page(MainPageIns,MAIN_PAGE_NAMELIST);
				NameListPageReset();
				if(GetMenuType())
				{
					IXS_ProxyCheckIfListChange();
				}
			}
			break;
		case MAINMENU_NL_BACK:
			switch_one_menu_page(MainPageIns,MAIN_PAGE_MAIN);
			break;
		case MAINMENU_MAIN_ROOMLIST:
			if(API_PublicInfo_Read_Bool("MenuEdit"))
			{
				API_EnterSettingRoot("MainListConfig");
				Main_Header_Reset();
				SettingMenu_Save=NULL;
				MainMenuState=MainMenu_ToSetting;
			}
			else
			{
				switch_one_menu_page(MainPageIns,MAIN_PAGE_ROOMLIST);
				if(GetMenuType())
				{
					IXS_ProxyCheckIfListChange();
				}
			}
			break;
		case MAINMENU_RL_BACK:
			switch_one_menu_page(MainPageIns,MAIN_PAGE_MAIN);
			break;
		case MAINMENU_MAIN_CODEKP:
			switch_one_menu_page(MainPageIns,MAIN_PAGE_CODE);
			CodeKpInput_Init();			
			break;
		case MAINMENU_MAIN_DIALKP:
			switch_one_menu_page(MainPageIns,MAIN_PAGE_DIGTAL);
			DialKpInput_Init();
			break;
		case MAINMENU_MAIN_StartCall:
			//printf_json(arg,"1111111");
			if(arg!=NULL&&(MainStartCalling==0||abs(time(NULL)-MainStartCallTime)>5))
			{
				call_tips=API_TIPS_Ext_Time("Linking...",16);
				//printf("222222222%s:%d\n",__func__,__LINE__);
				Api_LvCallBusiness_StartCall(arg);
				MainStartCalling=1;
				MainStartCallTime=time(NULL);
			}
			break;
		case MAINMENU_MAIN_ToCall:
			if(call_tips!=NULL)
			{
				API_TIPS_Close_Ext(call_tips);
				call_tips=NULL;
			}
			if(MainStartCalling==1||lv_scr_act()==MainMenu_Scr)
			{
				MainMenuState=MainMenu_ToCallMenu;
				MainStartCalling=0;	
			}
			else if(MainMenuState==MainMenu_ToSetting)
			{
				API_SettingNormalTip(NULL);
				SettingMenu_Save=lv_scr_act();
				lv_disp_load_scr(MainMenu_Scr);
			}
			init_main_call_text();
			main_reset_callTime();
			switch_one_menu_page(MainPageIns,MAIN_PAGE_CALL);
			MainHeaderShow();
			break;
		case MAINMENU_MAIN_CallReturn:
			if(MainMenuState==MainMenu_ToCallMenu)
			{
				MainStartCalling=0;
				//MainMenu_Init();
				if(MainMenu_Scr!=lv_scr_act())
					 lv_disp_load_scr(MainMenu_Scr);
				MainMenu_Reset();
			}
			else if(MainMenuState==MainMenu_ToSetting&&SettingMenu_Save)
			{
				lv_disp_load_scr(SettingMenu_Save);
				SettingMenu_Save=NULL;
				char *home_page= API_PublicInfo_Read_String("Home_page");	
				if(home_page!=NULL)
				{
					switch_one_menu_page(MainPageIns,home_page);
					//if(strcmp(home_page,MAIN_PAGE_MAIN)==0)	
					//	MainHeaderShow();
				}
				else
				{
					switch_one_menu_page(MainPageIns,MAIN_PAGE_MAIN);
				}
			}
			break;
		case MAINMENU_MAIN_CallError:
			if(call_tips!=NULL)
			{
				MainMenuUpdate_Tips(MAIN_PAGE_DIGTAL,DIGITAL_TIPS,"Wrong number",1);
				API_TIPS_Close_Ext(call_tips);
				call_tips=NULL;
			}
			 if(MainMenuState==MainMenu_ToSetting)
			{
				//API_SettingErrorTip("Call error");
				LV_API_TIPS("Call error",4);
			 }
			MainStartCalling=0;
			break;
		case MAINMENU_Return_MainPage:
			//MainMenu_Reset();
			switch_one_menu_page(MainPageIns,MAIN_PAGE_MAIN);
			break;
		case MAINMENU_MAIN_ToSetting:
			MainMenu_Reset();
			API_EnterSettingRoot("Root");
			SettingMenu_Save=NULL;
			MainMenuState=MainMenu_ToSetting;
			break;
		case MAINMENU_Reload:
			 if(MainMenuState==MainMenu_ToSetting)
			{
				API_SettingMenuClose();
				MainMenu_Reset();
			}
			MainMenu_Close();
			MainMenu_Init();
			break;
		case MAINMENU_NL_JUMP:
			switch_one_menu_page(MainPageIns,MAIN_PAGE_ROOMLIST);
			break;
		case MAINMENU_RL_JUMP:
			switch_one_menu_page(MainPageIns,MAIN_PAGE_NAMELIST);
			break;
		case MAINMENU_COMMON_CLICK:
			printf("MAINMENU_COMMON_CLICK:%s\n",(char*)arg);
			Btn_Common_Click_Process((char*)arg);
			break;
			
	}
}

void MainMenu_Close(void)
{
	vtk_lvgl_lock();
	if(MainPageIns!=NULL)
	{
		deinit_one_menu(MainPageIns);
		MainPageIns->header = NULL;
		MainPageIns = NULL;
	}
	if(MainTimer!=NULL)
	{
		lv_timer_del(MainTimer);
		MainTimer=NULL;
	}

	if(lv_scr_act()== MainMenu_Scr)
	{
		if(MainMenu_Scr_return)
		{
			lv_disp_load_scr(MainMenu_Scr_return);
		}

		lv_obj_del(MainMenu_Scr);
		MainMenu_Scr=NULL;
	}
	vtk_lvgl_unlock();
}
int menu_sn_eoc_err_disp(cJSON *cmd)
{
	char* pbEocMacState = API_PublicInfo_Read_String(PB_EOC_Mac_State);
	if(!IfHasSn())
	{
		vtk_lvgl_lock();
		API_MSGBOX("Please request SN from server",-1,API_Event_By_Name,EventReqSNFromServer);
		vtk_lvgl_unlock();
		return 0;
	}
	#ifndef IX_LINK
	if(pbEocMacState&& strcmp(pbEocMacState, "Ok")!=0)
	{
		vtk_lvgl_lock();
		API_MSGBOX("Set EOC MAC by sn",-1,API_Event_By_Name,EventSetEocMacBySN);
		vtk_lvgl_unlock();
		return 0;
	}
	#endif
	return 0;
}
void Btn_Common_Click_Process(char *icon_data)
{
	//cJSON* fun = cJSON_GetObjectItem(icon_data, get_global_key_string(GLOBAL_ICON_KEYS_FUNCTION));
	
	char header_min[]="header_min";
	//printf("11111Btn_Common_Click_Process:%s:%s\n",icon_data,header_min);
	if(strcmp(icon_data,"nameBack") == 0)
	{
		//switch_one_menu_page(MainPageIns,MAIN_PAGE_MAIN);
		MainMenu_Reset();
	}
	else if(strcmp(icon_data,"call") == 0)
	{
		//switch_one_menu_page(MainPageIns,MAIN_PAGE_MAIN);
		lv_msg_send(IX850_MSG_UNCALL, NULL);
	}
	else if(strcmp(icon_data,"installer_bar_m") == 0)
	{
		MainMenu_Process(MAINMENU_MAIN_ToSetting,NULL); 
	}
	else if(strcmp(icon_data,"installer_bar_l") == 0)
	{
		if(API_PublicInfo_Read_Bool("MenuEdit"))
		{
			API_PublicInfo_Write_Bool("MenuEdit",false);
		}
		else
		{
			API_PublicInfo_Write_Bool("MenuEdit",true);
		}
		
	}
	else if(strcmp(icon_data,"installer_bar_r") == 0)
	{
		//API_TIPS_Ext("OnInstaller:right");
		//switch_one_menu_page(MainPageIns,MAIN_PAGE_WIZARD);
		API_EnterSettingRoot("QuickRoot");
	}
	else if(strcmp(icon_data,"exit_installer") == 0)
	{
		API_PublicInfo_Write_Bool("OnInstaller",0);
		API_TIPS_Ext_Time("Exiting, please wait", 2);
		API_Event_NameAndMsg(EventMenuSettingProcess, "ExitInstallerMode", NULL);
	}
	else if(strcmp(icon_data,header_min) == 0)
	{
		if(API_PublicInfo_Read_Bool("OnInstaller"))
			MainMenu_Process(MAINMENU_MAIN_ToSetting,NULL); 
		else
		{
			switch_one_menu_page(MainPageIns,MAIN_PAGE_CODE);
			CodeKpInput_Init();
		}
	}
	else if(strcmp(icon_data,"header_left") == 0)
	{
		if(API_PublicInfo_Read_Bool("OnInstaller"))
		{
			API_TIPS_Ext("OnInstaller:left");
			API_PublicInfo_Write_Bool("OnInstaller",0);
			API_SettingMenuClose();
			MainMenuLeave_InstallerMode();
		}
		else
		{
			switch_one_menu_page(MainPageIns,MAIN_PAGE_CODE);
			CodeKpInput_Init();
		}
	}
	else if(strcmp(icon_data,"header_right") == 0)
	{
		if(API_PublicInfo_Read_Bool("OnInstaller"))
			API_TIPS_Ext("OnInstaller:right");
		else
		{
			switch_one_menu_page(MainPageIns,MAIN_PAGE_CODE);
			CodeKpInput_Init();
		}
	}
	else if(strcmp(icon_data,"menu_choose") == 0)
	{
		API_SettingChangePage("MenuChoose");				
	}
	else if(!strcmp(icon_data,"HYBRID_MAT_Choose"))
	{
		API_SettingChangePage(icon_data);
	}
	else
	{
		MainMenu_Reset();
	}
}
void MainMenu_Reset(void)
{
	#if 0
	int i;
	
	lv_obj_t* HideMenuList[4]=
	{
		MainRoomlist,
		MainNamelist,
		MainDialKp,
		MainCodeKp,
	};
	int HideMenuList_Num=4;//sizeof(HideMenuList)/sizeof(HideMenuList[0]);
	if(lv_scr_act()!=MainMenu_Scr)
		return;//scr_main();
	MainMenu_Reset_Timer=0;
	if(MainPage!=NULL)
		lv_obj_clear_flag(MainPage, LV_OBJ_FLAG_HIDDEN);
	for(i=0;i<HideMenuList_Num;i++)
	{
		if(HideMenuList[i]!=NULL)
			lv_obj_add_flag(HideMenuList[i], LV_OBJ_FLAG_HIDDEN);
	}
	MainMenuState=MainMenu_Stanby;
	MainStartCalling=0;
	#endif
	if(lv_scr_act()!=MainMenu_Scr)
		return;//scr_main();
	char *home_page= API_PublicInfo_Read_String("Home_page");	
	if(home_page!=NULL)
	{
		switch_one_menu_page(MainPageIns,home_page);
		if(strcmp(home_page,MAIN_PAGE_NAMELIST)==0)	
			NameListPageReset();
	}
	else
	{
		switch_one_menu_page(MainPageIns,MAIN_PAGE_MAIN);
	}
	
	MainMenuState = MainMenu_Stanby;	
	MainMenu_Reset_Timer = 0;
	MainStartCalling = 0;
}


void MainMenu_Reset_Process(void)
{
	int Menu_Keep_time;

	MainMenu_Reset_Timer++;
	Menu_Keep_time = API_Para_Read_Int(MENU_AUTO_RETURN_TIME);
	Menu_Keep_time = (Menu_Keep_time < 5 ? 5 : Menu_Keep_time);
	Menu_Keep_time = (Menu_Keep_time > 1800 ? 1800 : Menu_Keep_time);
	char *home_page= API_PublicInfo_Read_String("Home_page");
	char *cur_page=API_PublicInfo_Read_String("cur_page");
	if((lv_scr_act()!=MainMenu_Scr || (home_page&&cur_page&&strcmp(home_page,cur_page)!=0)) && MainMenu_Reset_Timer > Menu_Keep_time)
	{
		char* systemState = API_PublicInfo_Read_String(PB_System_State);
		if(strcmp(systemState,"Calling")==0)
		{
			MainMenu_Reset_Time();
		}
		else if(lv_scr_act() != MainMenu_Scr)
		{
			API_SettingMenuClose();
			MainMenu_Reset();
		}
		else
		{
			MainMenu_Reset();
		}
	}
	else if(lv_scr_act()==MainMenu_Scr && (home_page&&cur_page&&strcmp(home_page,cur_page)==0)) 
	{
		if(strcmp(home_page,"namelist")==0&&GetListCurPage()>0&&MainMenu_Reset_Timer > Menu_Keep_time)
			NameListPageReset();
		else
			lvgl_slow_check();
	}
	
	if(API_PublicInfo_Read_Bool("OnInstaller"))
	{
		if(MainMenu_OnInsatller_Timer++>1800 && lv_scr_act()==MainMenu_Scr)
		{
			API_PublicInfo_Write_Bool("OnInstaller", 0);
			MainMenuLeave_InstallerMode();
		}
	}
	
	ScrBr_Reset_Process(home_page,cur_page);
}
void MainTimer_cb(lv_timer_t* timer)
{
	MainMenu_Reset_Process();
}
void MainMenu_Reset_Time(void)
{
	MainMenu_Reset_Timer=0;
}

void ScrBr_Reset_Time(void)
{
	#if 1
	int high_br;
	vtk_lvgl_lock(); 
	ScrBr_Reset_Timer=0;
	if(ScrBr_State==0)
	{
		ScrBr_Auto_Time=API_Para_Read_Int(SCREEN_HIGH_BRIGHT_TIME);
		if(ScrBr_Auto_Time<5)
			ScrBr_Auto_Time=5;
		if(ScrBr_Auto_Time>1800)
			ScrBr_Auto_Time=1800;
		
		high_br=API_Para_Read_Int(SCREEN_HIGH_BRIGHT);
		if(high_br<40)
			high_br=50;
		API_LCD_ctrl(1,high_br);
		ScrBr_State=1;
	}
	vtk_lvgl_unlock(); 
	#endif
}
void ScrBr_Reset_Process(char *home_page, char *cur_page)
{
	#if 1
	int low_br;
	ScrBr_Reset_Timer++;
	if((lv_scr_act()==MainMenu_Scr&&(home_page&&cur_page&&strcmp(home_page,cur_page)==0))&&ScrBr_Reset_Timer>ScrBr_Auto_Time)
	{
		
		if(ScrBr_State==1)
		{
			low_br=API_Para_Read_Int(SCREEN_LOW_BRIGHT);
			if(low_br<20)
				low_br=30;
			API_LCD_ctrl(1,low_br);
			ScrBr_State=0;
		}
	}
	#endif
}

void Radar_Trig_ScrBr(void)
{
	int high_br;
	vtk_lvgl_lock(); 
	if(ScrBr_State==0)
	{
		ScrBr_Reset_Timer=0;
		ScrBr_Auto_Time=API_Para_Read_Int(SCREEN_HIGH_BRIGHT_TIME);
		if(ScrBr_Auto_Time<5)
			ScrBr_Auto_Time=5;
		if(ScrBr_Auto_Time>1800)
			ScrBr_Auto_Time=1800;
		high_br=API_Para_Read_Int(SCREEN_HIGH_BRIGHT);
		if(high_br<40)
			high_br=50;
		API_LCD_ctrl(1,high_br);
		ScrBr_State=1;
	}
	vtk_lvgl_unlock(); 
}

void DialKpInput_Init(void)
{
	DialKpInput_Buff[0]=0;
 	DialKpInput_Len=0;
	
	MainMenuUpdate(MAIN_PAGE_DIGTAL,DIGTAL_NUM,"");
	MainMenuUpdate_Tips(MAIN_PAGE_DIGTAL,DIGITAL_TIPS,"Enter call number",0);
	SearchListStop();
}

int DialKpInput_Process(char *input_ch,char *update_str)
{
	
	if (strcmp(input_ch, LV_VTK_BACKSPACE) == 0)
	{
		if (DialKpInput_Len== 0)
		{
		    	//MainMenu_Process(MAINMENU_Return_MainPage,NULL); 
			return 0;
		}
		else
		{
	   		DialKpInput_Buff[--DialKpInput_Len]=0;
	   		strcpy(update_str,DialKpInput_Buff);
	   		return 1;
		}

	}
	else if (strcmp(input_ch, LV_VTK_PHONE_FILP) == 0)
	{
		if(DialKpInput_Len>0)
		{
			cJSON* object = cJSON_CreateObject();
			cJSON_AddStringToObject(object, "KayPad", DialKpInput_Buff);
		  	MainMenu_Process(MAINMENU_MAIN_StartCall,object);
			cJSON_Delete(object);
			return 0;
		}
	}
	else
	{
		//if(DialKpInput_Len==0)
		//	API_trigOneRec();
		if(DialKpInput_Len<10)
		{
			DialKpInput_Buff[DialKpInput_Len++]=input_ch[0];
			DialKpInput_Buff[DialKpInput_Len]=0;
			strcpy(update_str,DialKpInput_Buff);
	   		return 1;
		}
		
	}
	return 0;
}

void CodeKpInput_Init(void)
{
	CodeKpInput_Buff[0]=0;
 	CodeKpInput_Len=0;
	char disp_buff[50]={0};
			
		//lv_label_set_text(icon->icon_cont,"");
	CodeKpDisp(CodeKpInput_Len,disp_buff);
	//MainMenuUpdate_password(MAIN_PAGE_CODE,CODE_NUM,"");
	MainMenuUpdate(MAIN_PAGE_CODE,DIGTAL_NUM,disp_buff);
	MainMenuUpdate_Tips(MAIN_PAGE_CODE,CODE_TIPS,"Enter password",0);
}
static int IX850S_CheckPrivateUnlockPwd(char* input, int input_Len)
{
	int ret = 0;

	if(input == NULL || (input_Len<=4))
	{
		return ret;
	}
	char pwd_buff[5]={0};
	char nbr_buff[20]={0};
	memcpy(pwd_buff,input+input_Len-4,4);
	memcpy(nbr_buff,input,input_Len-4);
	cJSON* Where = cJSON_CreateObject();
	cJSON_AddStringToObject(Where, "L_NBR", nbr_buff);
	cJSON_AddStringToObject(Where, "CODE", pwd_buff);
	cJSON* view = cJSON_CreateArray();
	cJSON* element;

	if(API_TB_SelectBySortByName(TB_NAME_PRIVATE_PWD_BY_L_NBR, view, Where, 0))
	{
		ret=1;
	}

	cJSON_Delete(Where);
	cJSON_Delete(view);
	return ret;
}

static int IX850S_ChangePrivateUnlockPwd(char* input, int input_Len)
{
	int ret = 0;

	if(input == NULL || (input_Len<=8))
	{
		return ret;
	}
	char pwd_buff[5]={0};
	char nbr_buff[20]={0};
	memcpy(pwd_buff,input+input_Len-8,4);
	memcpy(nbr_buff,input,input_Len-8);
	cJSON* Where = cJSON_CreateObject();
	cJSON_AddStringToObject(Where, "L_NBR", nbr_buff);
	cJSON_AddStringToObject(Where, "CODE", pwd_buff);
	cJSON* view = cJSON_CreateArray();
	cJSON* element;

	if(API_TB_SelectBySortByName(TB_NAME_PRIVATE_PWD_BY_L_NBR, view, Where, 0))
	{
		memcpy(pwd_buff,input+input_Len-4,4);
		cJSON *one_record=cJSON_GetArrayItem(view,0);
		printf_json(one_record,"11111");
		cJSON_ReplaceItemInObject(one_record,"CODE",cJSON_CreateString(pwd_buff));
		printf_json(one_record,"222222");
		API_TB_DeleteByName(TB_NAME_PRIVATE_PWD_BY_L_NBR, Where);
		API_TB_AddByName(TB_NAME_PRIVATE_PWD_BY_L_NBR,one_record);
		ret=1;
	}

	cJSON_Delete(Where);
	cJSON_Delete(view);
	return ret;
}
void CodeKpDisp(int len,char *update_str)
{
	char disp_buff[50]={0};
			int length = API_Para_Read_Int(AutoUnlockPwdLen);
		//lv_label_set_text(icon->icon_cont,"");
			for(int i=0;i<length;i++){
			if (i<len) {
				//if(save[i]!=1)
					//lv_obj_set_style_text_color(btn, lv_color_black(), 0);
					//lv_label_set_text(btn,LV_VTK_GENERAL);
					//lv_obj_clear_flag(btn,LV_OBJ_FLAG_HIDDEN);
				//save[i]=1;
				//lv_label_ins_text(icon->icon_cont, i, LV_VTK_SETTING);
				//strcat(disp_buff,"\xEE\x99\xB6");
				strcat(disp_buff,"\xEE\x9A\xBB"); 
			}else{
				//lv_label_ins_text(icon->icon_cont, i, LV_VTK_LEFT);
				//if(save[i]!=0)
				//lv_obj_set_style_text_color(btn, lv_color_white(), 0);
					//lv_obj_set_style_bg_color(btn, lv_color_white(), 0);
					//lv_label_set_text(btn,LV_VTK_SETTING);
					//lv_obj_add_flag(btn,LV_OBJ_FLAG_HIDDEN);
				//save[i]=0;
				//strcat(disp_buff,"\xEE\x99\x92");
				strcat(disp_buff,"\xEE\x9B\x84");
				
			}
			if(i<length-1)
				strcat(disp_buff,"  ");
		}
			strcpy(update_str,disp_buff);
}

static void manager_setting_timer_out_cb(lv_timer_t* timer)
{
	lv_timer_pause(timer);
    lv_timer_del(timer);
	API_SettingChangePage("Manager_Root");
}

static void timer_out_cb(lv_timer_t* timer)
{
	char* result = (char*)timer->user_data;
    int autoUnlockLen = API_Para_Read_Int(AutoUnlockPwdLen);
    lv_timer_pause(timer);
    lv_timer_del(timer);
	printf("CodeKpInput_Buff  = [%s]\n",CodeKpInput_Buff);
    switch (InputPasswordVerify(CodeKpInput_Buff, autoUnlockLen))
	{
		case PSW_TYPE_INSTALLER:
			MainMenu_OnInsatller_Timer=0;
			API_PublicInfo_Write_Bool("OnInstaller",1);
			MainMenuInto_InstallerMode();
			if(result)
				strcpy(result,"TO_SETTING");
			break;
		case PSW_TYPE_MANAGE:
			//API_SettingMenuClose();
			//API_EnterSettingRoot("Manager_Root");
			API_SettingChangePage("Manager_Root");
			//API_MSGBOX("wiwiiwwwwwwwwwwwwwwwwwwww",1);
			break;
		case PSW_TYPE_UNLOCK1:
			API_Event_Unlock("RL1", "PWD");
			if(result)
				strcpy(result,"COMMON PWD UNLOCK1");
			break;
		case PSW_TYPE_UNLOCK2:
			API_Event_Unlock("RL2", "PWD");
			if(result)
				strcpy(result,"COMMON PWD UNLOCK2");
			break;
		case PSW_TYPE_PRIVATE_UNLOCK:
			API_Event_Unlock("RL1", "PrivatePWD");
			if(result)
				sprintf(result,"PrivatePWD UNLOCK1:%s",CodeKpInput_Buff);
			break;
		case PSW_TYPE_VM_USER:			
			API_SettingChangePage("VM_User_Editor");			
			break;
		default:
			if(CodeKpInput_Len==8&&memcmp(CodeKpInput_Buff,"9350",4)==0)
			{
				if(API_CreatePrivatePasswordTable(CodeKpInput_Buff+4)>0)
				{
					BEEP_CONFIRM();
					if(result)
						sprintf(result,"PrivatePWD CreateTb Succ:%s",CodeKpInput_Buff);
				}
				else
				{
					BEEP_ERROR();
					if(result)
						sprintf(result,"PrivatePWD CreateTb fail:%s",CodeKpInput_Buff);
				}
			}
			else if(CodeKpInput_Len>12&&memcmp(CodeKpInput_Buff,"9305",4)==0)
			{
				if(IX850S_ChangePrivateUnlockPwd(CodeKpInput_Buff+4,CodeKpInput_Len-4)>0)
				{
					BEEP_CONFIRM();
					if(result)
						sprintf(result,"PrivatePWD Change Succ:%s",CodeKpInput_Buff);
				}
				else
				{
					BEEP_ERROR();
					if(result)
						sprintf(result,"PrivatePWD Change Succ:%s",CodeKpInput_Buff);
				}
			}
			else
			{						
				MainMenuUpdate_Tips(MAIN_PAGE_CODE,CODE_TIPS,"Wrong code",1);
				BEEP_ERROR();
				if(result)
					sprintf(result,"Invalid code:%s",CodeKpInput_Buff);						
			}
			break;
	}
	char update_str[50] = {0};		
	CodeKpInput_Buff[0]=0;
	CodeKpInput_Len=0;
	//strcpy(update_str,"");
	CodeKpDisp(CodeKpInput_Len,update_str);
	MainMenuUpdate(MAIN_PAGE_CODE,DIGTAL_NUM,update_str);
}

int CodeKpInput_Process(char *input_ch,char *update_str,char *result)
{
	//cJSON *event_record;
	//char rec_file[200];
	if (strcmp(input_ch, LV_VTK_BACKSPACE) == 0)
	{
		if (CodeKpInput_Len== 0)
		{
		    	//MainMenu_Process(MAINMENU_Return_MainPage,NULL); 
			return 0;
		}
		else
		{
	   		CodeKpInput_Buff[--CodeKpInput_Len]=0;
	   		memset(update_str,'*',CodeKpInput_Len);
			update_str[CodeKpInput_Len]=0;
			CodeKpDisp(CodeKpInput_Len,update_str);
	   		return 1;
		}
	}
	else if (strcmp(input_ch, LV_VTK_CONFIM) == 0||strcmp(input_ch, LV_VTK_KEY) == 0)
	{
		if(CodeKpInput_Len>0)
		{
			switch (InputPasswordVerify(CodeKpInput_Buff, API_Para_Read_Int(AutoUnlockPwdLen)))
			{
				case PSW_TYPE_INSTALLER:
					MainMenu_OnInsatller_Timer=0;
					API_PublicInfo_Write_Bool("OnInstaller",1);
					MainMenuInto_InstallerMode();
					if(result)
						strcpy(result,"TO_SETTING");
					break;
				case PSW_TYPE_MANAGE:
					//API_SettingMenuClose();
					//API_EnterSettingRoot("Manager_Root");
					LV_API_TIPS("Loading, please wait!",1);
					lv_timer_create(manager_setting_timer_out_cb,100,NULL);
					break;
				case PSW_TYPE_UNLOCK1:
					API_Event_Unlock("RL1", "PWD");
					if(result)
						strcpy(result,"COMMON PWD UNLOCK1");
					break;
				case PSW_TYPE_UNLOCK2:
					API_Event_Unlock("RL2", "PWD");
					if(result)
						strcpy(result,"COMMON PWD UNLOCK2");
					break;
				case PSW_TYPE_PRIVATE_UNLOCK:
					API_Event_Unlock("RL1", "PrivatePWD");
					if(result)
						sprintf(result,"PrivatePWD UNLOCK1:%s",CodeKpInput_Buff);
					break;
				case PSW_TYPE_VM_USER:
					API_SettingChangePage("VM_User_Editor");
					break;
				default:
					if(CodeKpInput_Len==8&&memcmp(CodeKpInput_Buff,"9350",4)==0)
					{
						if(API_CreatePrivatePasswordTable(CodeKpInput_Buff+4)>0)
						{
							BEEP_CONFIRM();
							if(result)
								sprintf(result,"PrivatePWD CreateTb Succ:%s",CodeKpInput_Buff);
						}
						else
						{
							BEEP_ERROR();
							if(result)
								sprintf(result,"PrivatePWD CreateTb fail:%s",CodeKpInput_Buff);
						}
					}
					else if(CodeKpInput_Len>12&&memcmp(CodeKpInput_Buff,"9305",4)==0)
					{
						if(IX850S_ChangePrivateUnlockPwd(CodeKpInput_Buff+4,CodeKpInput_Len-4)>0)
						{
							BEEP_CONFIRM();
							if(result)
								sprintf(result,"PrivatePWD Change Succ:%s",CodeKpInput_Buff);
						}
						else
						{
							BEEP_ERROR();
							if(result)
								sprintf(result,"PrivatePWD Change Succ:%s",CodeKpInput_Buff);
						}
					}
					else
					{
						BEEP_ERROR();
						if(result)
							sprintf(result,"Invalid code:%s",CodeKpInput_Buff);
						MainMenuUpdate_Tips(MAIN_PAGE_CODE,CODE_TIPS,"Wrong code",1);
					}
					break;
			}

			CodeKpInput_Buff[0]=0;
 			CodeKpInput_Len=0;
			strcpy(update_str,"");
			CodeKpDisp(CodeKpInput_Len,update_str);
			return 1;
		}
	}
	else
	{
		#if 0
		if(CodeKpInput_Len==0)
			API_trigOneRec();
		#endif
		int autoUnlockLen = API_Para_Read_Int(AutoUnlockPwdLen);

		if(CodeKpInput_Len < autoUnlockLen)
		{
			CodeKpInput_Buff[CodeKpInput_Len++]=input_ch[0];
			CodeKpInput_Buff[CodeKpInput_Len]=0;
			memset(update_str,'*',CodeKpInput_Len);
			update_str[CodeKpInput_Len]=0;
			CodeKpDisp(CodeKpInput_Len,update_str);
			MainMenuUpdate_Tips(MAIN_PAGE_CODE,CODE_TIPS,"Enter password",0);
			
		}

		if(CodeKpInput_Len == autoUnlockLen)
		{

			//CodeKpInput_Buff[CodeKpInput_Len++]=input_ch[0];
			//CodeKpInput_Buff[CodeKpInput_Len]=0;
			memset(update_str,'*',CodeKpInput_Len);
			update_str[CodeKpInput_Len]=0;

			CodeKpDisp(CodeKpInput_Len,update_str);

			MainMenuUpdate(MAIN_PAGE_CODE,DIGTAL_NUM,update_str);

			lv_timer_create(timer_out_cb,200,result); 
			//return 1;			
		}
		if(CodeKpInput_Len < autoUnlockLen)
		{
			return 1;
		}
	}
	return 0;
}

int IX850SMainMenu_Shell_Test(cJSON *cmd)
{
	char* jsonstr = GetShellCmdInputString(cmd, 1);
	int addr;
	int menu_id=1;
	int talk_en=0;
	int ring_type=0;
	int video_type=0;
	int try_cnt=2;
	int i;
	//sem_init(&ixgrm_sem,0,0);
	if(MainMenu_Scr==NULL||MainMenu_Scr!=lv_scr_act())
	{
		ShellCmdPrintf("not in main menu\n");
		return -1;
	}
	ShellCmdPrintf("%s:",jsonstr);
	unsigned long long last_time=time_since_last_call(-1);
	if(strcmp(jsonstr,"code_input")==0)
	{
		char update[30],result[100];
		char *input=GetShellCmdInputString(cmd, 2);
		if(input==NULL||strlen(input)==0)
			return -1;
		vtk_lvgl_lock();
		MainMenu_Process(MAINMENU_MAIN_CODEKP,NULL);
		strcpy(CodeKpInput_Buff,input);
		CodeKpInput_Len=strlen(input);
		CodeKpInput_Process(LV_VTK_CONFIM,update,result);
		ShellCmdPrintf("%s\n",result);
		vtk_lvgl_unlock();
	}	
}

int IX850SMenuCfg_Shell_Test(cJSON *cmd)
{
	char* jsonstr = GetShellCmdInputString(cmd, 1);

	ShellCmdPrintf("%s:",jsonstr);

	if(strcmp(jsonstr,"load")==0)
	{
		char *filename = GetShellCmdInputString(cmd, 2);

		vtk_lvgl_lock();

		menu_config_load(filename);

		vtk_lvgl_unlock();
	}
	if(strcmp(jsonstr,"iload")==0)
	{
		char *filename = GetShellCmdInputString(cmd, 2);

		vtk_lvgl_lock();

		menu_config_load_installer(filename);

		vtk_lvgl_unlock();
	}
	else if(!strcmp(jsonstr,"reload"))
	{
		MainMenu_Close();
		MainMenu_Init();
	}
	if(strcmp(jsonstr,"t1")==0)
	{
		lv_color16_t ch=lv_color_chroma_key();
		lv_color16_t bl=lv_color_black();
		ShellCmdPrintf("1111111111111LV_COLOR_CHROMA_KEY=%08x,%d\n",lv_color_chroma_key().full,
			lv_color_to32(lv_color_make(atoi(GetShellCmdInputString(cmd, 2)),
			atoi(GetShellCmdInputString(cmd, 3)),
			atoi(GetShellCmdInputString(cmd, 4)))));//BEEP_ERROR();
	}
	if(strcmp(jsonstr,"t2")==0)
	{
		//BEEP_CONFIRM();
		lv_point_t size_res;
		char *text=GetShellCmdInputString(cmd, 2);
		lv_font_t * font=getfont(atoi(text));
		text=GetShellCmdInputString(cmd, 3);
		lv_txt_get_size(&size_res,text,font,atoi(GetShellCmdInputString(cmd, 4)),atoi(GetShellCmdInputString(cmd, 5)),atoi(GetShellCmdInputString(cmd, 6)),atoi(GetShellCmdInputString(cmd, 7)));
		ShellCmdPrintf("test:%d:%d\n",size_res.x,size_res.y);
	}
	if(strcmp(jsonstr,"t3")==0)
		vtk_font_reinit();//vtk_lvgl_reinit();
		//BEEP_KEY();
}
unsigned long long time_since_last_call(unsigned long long last);
void lvgl_slow_check(void)
{
	static int init_flag=0;
	static int font_reinit_cnt=0;
	static int check_ok_cnt=0;
	if(check_ok_cnt>20||font_reinit_cnt>20)
		return;
	vtk_lvgl_lock();
	long long timeConsuming=time_since_last_call(-1);
	lv_point_t size_res;
	char text[]={"vtk-test"};
	lv_font_t * font=getfont(0);
	lv_txt_get_size(&size_res,text,font,0,0,200,0);
	
	timeConsuming=time_since_last_call(timeConsuming);
	timeConsuming=timeConsuming/1000;
	bprintf("test:%d:%d:%d ms\n",size_res.x,size_res.y,timeConsuming);
	if(timeConsuming>8)
		init_flag++;
	else
		init_flag=0;
	if(init_flag>=5)
	{
		vtk_font_reinit();
		font_reinit_cnt++;
		check_ok_cnt=0;
		init_flag=0;
		API_PublicInfo_Write_Int("Font_reinit_cnt",font_reinit_cnt);
		if(font_reinit_cnt>15)
			set_lv_read_cb();
	}
	else
	{
		set_lv_read_cb();
		check_ok_cnt++;
	}
	vtk_lvgl_lock();
}
int IX850SPageCfg_Shell_Test(cJSON *cmd)
{
	char* jsonstr = GetShellCmdInputString(cmd, 1);
	char *input = GetShellCmdInputString(cmd, 2);

	if(MainMenu_Scr==NULL||MainMenu_Scr!=lv_scr_act())
	{
		ShellCmdPrintf("not in main menu\n");
		return -1;
	}
	ShellCmdPrintf("%s:",jsonstr);
	vtk_lvgl_lock();
	if(strcmp(jsonstr,"Create")==0)
	{
		create_one_menu_page(MainPageIns,input);	
		char *logic_name = GetShellCmdInputString(cmd, 3);
		if(strlen(logic_name)>0)
		{
			switch_one_menu_page(MainPageIns,logic_name);
		}		
	}
	else if(strcmp(jsonstr,"Switch")==0)
	{
		switch_one_menu_page(MainPageIns,input);
	}
	else if(strcmp(jsonstr,"Delete")==0)
	{
		kill_one_menu_page(MainPageIns,input);
	}
	else if(strcmp(jsonstr,"Setting")==0)
	{
		MainMenu_Process(MAINMENU_MAIN_ToSetting,NULL);
	}
	vtk_lvgl_unlock();			
}

int MainMenuUpdate(char *page_name,char *part_name,void *update_content)
{

	PAGE_INS* page = get_menu_page_obj(MainPageIns,page_name);
	if(page == NULL)
		return -1;		

	cJSON* json = cJSON_GetObjectItem(page->page_icons,part_name);
	if(json==NULL)
		return -1;
	struct ICON_INS* icon = (struct ICON_INS*)json->valueint;
	lv_obj_t* obj = lv_obj_get_parent(icon->icon_cont);
	lv_obj_add_flag(obj,LV_OBJ_FLAG_HIDDEN);
	lv_label_set_text(icon->icon_cont,(char*)update_content);
	lv_obj_clear_flag(obj,LV_OBJ_FLAG_HIDDEN);
	return 1;
}

int MainMenuUpdate_Tips(char *page_name,char *part_name,char *data,int type)
{
	PAGE_INS* page = get_menu_page_obj(MainPageIns,page_name);
	if(page == NULL)
		return -1;		

	cJSON* json = cJSON_GetObjectItem(page->page_icons,part_name);
	if(json==NULL)
		return -1;
	struct ICON_INS* icon = (struct ICON_INS*)json->valueint;
	lv_obj_t* obj = lv_obj_get_parent(icon->icon_cont);
	lv_obj_add_flag(obj,LV_OBJ_FLAG_HIDDEN);

	vtk_label_set_text(icon->icon_cont,data);
	if(type)
		lv_obj_set_style_text_color(icon->icon_cont, lv_color_make(255,18,58), 0);
	else
		lv_obj_set_style_text_color(icon->icon_cont, lv_color_white(), 0);
	
	lv_obj_clear_flag(obj,LV_OBJ_FLAG_HIDDEN);
	return 1;
}

int MainMenuUpdate_password(char *page_name,char *part_name,void *update_content)
{
	//static int save[4]={0};
	PAGE_INS* page = get_menu_page_obj(MainPageIns,page_name);
	if(page == NULL)
		return -1;		

	cJSON* json = cJSON_GetObjectItem(page->page_icons,part_name);
	if(json==NULL)
		return -1;
	lv_obj_add_flag(page->page_obj,LV_OBJ_FLAG_HIDDEN);
	struct ICON_INS* icon = (struct ICON_INS*)json->valueint;
	
	#if 1
		
	if(strcmp(cJSON_GetStringValue(cJSON_GetObjectItemCaseSensitive(icon->icon_data,"type")),"IMG_Label")==0)
	{
		//if(strcmp(cJSON_GetStringValue(cJSON_GetObjectItemCaseSensitive(icon->icon_data,"type")),"IMG_Label")==0)
		int len = strlen(update_content);
		char disp_buff[50]={0};
		
		//lv_label_set_text(icon->icon_cont,"");
			for(int i=0;i<4;i++){
			if (i<len) {
				//if(save[i]!=1)
					//lv_obj_set_style_text_color(btn, lv_color_black(), 0);
					//lv_label_set_text(btn,LV_VTK_GENERAL);
					//lv_obj_clear_flag(btn,LV_OBJ_FLAG_HIDDEN);
				//save[i]=1;
				//lv_label_ins_text(icon->icon_cont, i, LV_VTK_SETTING);
				strcat(disp_buff,"a");
			}else{
				//lv_label_ins_text(icon->icon_cont, i, LV_VTK_LEFT);
				//if(save[i]!=0)
				//lv_obj_set_style_text_color(btn, lv_color_white(), 0);
					//lv_obj_set_style_bg_color(btn, lv_color_white(), 0);
					//lv_label_set_text(btn,LV_VTK_SETTING);
					//lv_obj_add_flag(btn,LV_OBJ_FLAG_HIDDEN);
				//save[i]=0;
				strcat(disp_buff,"b");
			}
		}
		lv_label_set_text(icon->icon_cont,disp_buff);	
	}

	else
	{
		int len = strlen(update_content);
		for(int i=0;i<4;i++){
			lv_obj_t* btn = lv_obj_get_child(icon->icon_cont, i);
			if (i<len) {
				//if(save[i]!=1)
					//lv_obj_set_style_text_color(btn, lv_color_black(), 0);
					//lv_label_set_text(btn,LV_VTK_GENERAL);
					//lv_obj_clear_flag(btn,LV_OBJ_FLAG_HIDDEN);
				//save[i]=1;
				lv_obj_set_style_text_color(btn, lv_color_black(), 0);
			}else{
				//if(save[i]!=0)
				//lv_obj_set_style_text_color(btn, lv_color_white(), 0);
					lv_obj_set_style_text_color(btn, lv_color_white(), 0);
					//lv_label_set_text(btn,LV_VTK_SETTING);
					//lv_obj_add_flag(btn,LV_OBJ_FLAG_HIDDEN);
				//save[i]=0;
			}
		}
	}
	#endif
	lv_obj_clear_flag(page->page_obj,LV_OBJ_FLAG_HIDDEN);
	

	return 1;
}

int MainMenuUpdate_list(char *page_name,char *part_name,void *update_content)
{

	PAGE_INS* page = get_menu_page_obj(MainPageIns,page_name);
	if(page == NULL)
		return -1;		

	cJSON* json = cJSON_GetObjectItem(page->page_icons,part_name);
	if(json==NULL)
		return -1;
	struct ICON_INS* icon = (struct ICON_INS*)json->valueint;

	VtkListContFresh(icon->icon_cont,update_content);

	return 1;
}

int MainMenuLabelUpdate(char *page_name,char *label_name,void *update_content)
{

	PAGE_INS* page = get_menu_page_obj(MainPageIns,page_name);
	if(page == NULL)
		return -1;
	int i;
	int num=cJSON_GetArraySize(page->page_icons);
	cJSON* json;
	struct ICON_INS* icon;
	char *label_str;
	for(i=0;i<num;i++)
	{
		json = cJSON_GetArrayItem(page->page_icons,i);
		 icon = (struct ICON_INS*)json->valueint;
		 label_str=cJSON_GetStringValue(cJSON_GetObjectItem(cJSON_GetObjectItem(icon->icon_data,"TEXT"),"Label_text"));
		 if(label_str!=NULL&&strcmp(label_str,label_name)==0)
		 {
			if(!strcmp(label_name, "state"))
			{
				vtk_label_set_text(icon->icon_cont, (char*)update_content);
			}
			else
			{
				lv_label_set_text(icon->icon_cont,(char*)update_content);
			}
			return 0;
		 }
	}
	
	return 1;
}


PAGE_INS* Get_Page_by_name(const char* menuName)
{
    cJSON* json;
    PAGE_INS* ret = NULL;

    json = cJSON_GetObjectItemCaseSensitive(MainPageIns->menu_pages, menuName);
    if (json)
    {
        ret = (PAGE_INS*)(json->valueint);
    }

    return ret;
}
int menu_config_load_installer(const char* filename)
{
	char strbuf1[500];
    char* menu_config_path;
	int index;

	PAGE_INS* ppage;
	// create menu
	
	if(MainPageIns == NULL)
		MainPageIns = init_one_menu("MainMenu", filename, MainMenu_Scr);
	if(MainPageIns == NULL){
		//elog_error("Failed to load, please check the file format!")
        return -2;
	}

	for(index = 0, menu_config_path = NULL; MENU_CFG_DIR[index]; index++)
	{
		snprintf(strbuf1, 500, "%s/%s.installer.json", MENU_CFG_DIR[index], filename);
		if(IsFileExist(strbuf1))
		{
			menu_config_path = strbuf1;
			break;
		}
	}

	if(!menu_config_path)
	{
		printf("HAVE NO THIS MENU CONFIG FILE!!!\n");
		return -1;
	}

	cJSON* menu_config = GetJsonFromFile(menu_config_path);
    if(menu_config == NULL){
        //elog_warn("Failed to load, Load default scheme!");

		return -1;
    }
	else
	{		
		cJSON* list = cJSON_GetObjectItem(menu_config,"page_list");
		cJSON* pageName;
		cJSON_ArrayForEach(pageName, list)
		{
			if(cJSON_IsString(pageName) && pageName->string)
			{
				for(index = 0, menu_config_path = NULL; MENU_CFG_DIR[index]; index++)
				{
					snprintf(strbuf1, 500, "%s/%s", MENU_CFG_DIR[index], pageName->valuestring);
					if(IsFileExist(strbuf1))
					{
						menu_config_path = strbuf1;
						break;
					}
				}

				ppage = create_one_menu_page_installer(MainPageIns, menu_config_path);
				if (ppage)
				{
					AddOneMenuCommonClickedCb(ppage->page_obj, MainMemu_Pressed_event_cb);
				}
				else 
				{
					API_PublicInfo_Write_String(pageName->string,"");
				}
			}
		}

		cJSON* home = cJSON_GetObjectItem(menu_config,"Home_page");
		if(home&&switch_one_menu_page(MainPageIns,home->valuestring)!=NULL){
			API_PublicInfo_Write_String("Home_page",home->valuestring);
		}
		else 
		{
			char *home_page;
			if((home_page=API_Para_Read_String2("Home_page"))&&switch_one_menu_page(MainPageIns,home_page)!=NULL)
			{
				API_PublicInfo_Write_String("Home_page",home_page);
			}
			else
			{
				switch_one_menu_page(MainPageIns,MAIN_PAGE_MAIN);
				API_PublicInfo_Write_String("Home_page",MAIN_PAGE_MAIN);
			}
		}
		cJSON_Delete(menu_config);
	}
	
	API_PublicInfo_Write_String("cur_menu",get_name_of_one_menu_config(MainPageIns));

	//printf("------------------------ load finish ---------------------\n");
	return 1;
}
void MainMenuInto_InstallerMode(void)
{
	char act_name[200];
	if(API_Para_Read_String("UI_Documents",act_name)==0)
	{
		strcpy(act_name,"MenuPage");
	}
	if(menu_config_load_installer(act_name)<0)
	{
		strcpy(act_name,"MenuPage");
		if(menu_config_load_installer(act_name)<0)
			BEEP_ERROR();
	}
	API_Event_By_Name(EventGetNamelist);
}
void MainMenuLeave_InstallerMode(void)
{
	char act_name[200];
	set_language();
	API_Para_Read_String("UI_Documents",act_name);
	API_PublicInfo_Write_Bool("MenuEdit",false);
	menu_config_load(act_name);
	API_Event_By_Name(EventGetNamelist);
}
void MainMenuReload_InstallerMode(void)
{
	char act_name[200];
	vtk_lvgl_lock(); 
	//MainMenu_Init();
	API_Para_Read_String("UI_Documents",act_name);
	menu_config_load(act_name);
	
	if(API_Para_Read_String("UI_Documents",act_name)==0)
	{
		strcpy(act_name,"MenuPage");
	}
	if(menu_config_load_installer(act_name)<0)
	{
		strcpy(act_name,"MenuPage");
		if(menu_config_load_installer(act_name)<0)
			BEEP_ERROR();
	}
	if(MainMenu_Scr!=lv_scr_act())
		lv_disp_load_scr(MainMenu_Scr);
	
	MainHeaderHide();
	
	API_Event_By_Name(EventGetNamelist);
	API_PublicInfo_Write_Bool("OnInstaller",1);
	API_PublicInfo_Write_Bool("MenuEdit",false);
	vtk_lvgl_unlock(); 
}



void Main_Header_Reset(void)
{
	PAGE_INS* page = get_menu_page_obj(MainPageIns,MAIN_PAGE_MAIN);
	if(page == NULL)
		return -1;
	int i;
	int num = cJSON_GetArraySize(page->page_icons);
	cJSON* json = cJSON_GetObjectItem(page->page_icons,MAIN_HEADER);
	if(json == NULL)
		return -1;

	struct ICON_INS* icon = (struct ICON_INS*)json->valueint;
	API_PublicInfo_Write_Bool("MenuEdit",false);
	lv_obj_set_style_text_color(lv_obj_get_child(icon->icon_cont,2),lv_color_white(),0);
}

int GetNameListpTurnBtnColor(void)
{
	int rev=0;
	PAGE_INS* page = get_menu_page_obj(MainPageIns,MAIN_PAGE_NAMELIST);
	if(page != NULL)
	{

		cJSON* json = cJSON_GetObjectItem(page->page_info,"TurnBtnColor");
		if(json)
			rev=json->valueint;
	}

	if(rev!=0)
		return rev;
	return lv_color_to32(lv_color_make(255,178,64));
}