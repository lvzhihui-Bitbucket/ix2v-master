#include <stdio.h>
#include "task_CallServer.h"
#include "elog_forcall.h"
#include "obj_PublicInformation.h"
#include "task_Event.h"
#include "ix850_icon.h"
#include "obj_lvgl_msg.h"
#include "obj_IXS_Proxy.h"
#include "VtkListCont.h"
#include "obj_ResEditor.h"
#include "obj_IoInterface.h"

typedef struct
{
	lv_obj_t	 *call_list[16];
	lv_obj_t	 *text_list[16];
	lv_obj_t *line_list[16];
	//lv_obj_t *page_text;
	lv_obj_t* page_icon;
	lv_obj_t *listcont;
	lv_obj_t *tab;
	lv_obj_t *listbtn;
	cJSON *list;

	//int line_num;
	//int type;
	int list_color;
	int list_size;
	int list_len;

	int cur_page;
}page_list_para_stru;

page_list_para_stru* page_list_para=NULL;


static lv_obj_t* add_one_item_to_listcont(lv_obj_t* parent,cJSON *item,int height);
static void CallList_click_event2(lv_event_t* e);
void create_one_list_page(lv_obj_t* listcont,cJSON *list);
void update_one_list_page_text(page_list_para_stru* page_list_para);

void VtkListContFresh(lv_obj_t* listcont,cJSON *list)
{
	//printf_json(list,__func__);
	//int i;
	//int size = cJSON_GetArraySize(list);
	//lv_obj_clean(listcont);
	//page_list_para_stru *user_data = lv_obj_get_user_data(listcont);

	int size = API_Para_Read_Int(ListSize);
	int color = API_Para_Read_Int(ListColor);
	int len = API_Para_Read_Int(ListLen);
	
	if(page_list_para==NULL||!cJSON_Compare(page_list_para->list,list,1))
	{	
		if(page_list_para != NULL){
			lv_obj_clean(listcont);
			
			if(page_list_para->list != NULL)
			{
				cJSON_Delete(page_list_para->list);			
			}
			free(page_list_para);
			page_list_para = NULL;
		}
		
		//page_list_para->list = list;		
		//page_list_para->cur_page = 0;
		create_one_list_page(listcont,list);

		//update_one_list_page_text(page_list_para);
	}	
	else if((page_list_para->listcont!=listcont)||(page_list_para->list_size != size) || (page_list_para->list_color != color) || (page_list_para->list_len != len))
	{
		if(page_list_para != NULL){
			lv_obj_clean(listcont);
			
			if(page_list_para->list != NULL)
			{
				cJSON_Delete(page_list_para->list);			
			}
			free(page_list_para);
			page_list_para = NULL;
		}
		create_one_list_page(listcont,list);
	}
	//lv_obj_set_user_data(listcont, NULL);

	//page_list_para_stru* page_list_para = malloc(sizeof(page_list_para_stru));
	//lv_obj_set_user_data(listcont, page_list_para);
	
}

#if 0
static lv_obj_t* add_item_to_tabview(lv_obj_t* parent,cJSON *item,int mode)
{
	lv_obj_t* tab = NULL;
	lv_obj_t* list = NULL;
	lv_obj_t* btn = NULL;
	int cnt = 0;
	int size = cJSON_GetArraySize(item);
	int page = 0;
	cJSON *item_array=cJSON_CreateArray();
	
	if(size % 8 == 0)
		page = size / 8;
	else 
		page = size / 8 + 1;
	switch(mode){
		case 1:
			ShellCmdPrintf("111111112:%d\n",__LINE__);
			btn = lv_tabview_get_tab_btns(parent);
			//cJSON_AddItemToArray(item_array,cJSON_CreateNumber((int)btn));
			lv_obj_set_size(btn, 0, 0);
			tab = lv_tabview_add_tab(parent," ");
			cJSON_AddItemToArray(item_array,cJSON_CreateNumber((int)tab));
			list = lv_obj_create(tab);
			lv_obj_remove_style_all(list); 
			lv_obj_set_size(list, LV_PCT(100), LV_PCT(100));
			lv_obj_set_flex_flow(list, LV_FLEX_FLOW_COLUMN);
			lv_obj_set_style_bg_opa(list, LV_OPA_MIN, 0);
			ShellCmdPrintf("111111112:%d\n",__LINE__);
			for(int j=0;j<size;j++){
				cJSON* icon = cJSON_GetArrayItem(item,j);
				if(icon == NULL)
					break;
				ShellCmdPrintf("111111112:%d\n",__LINE__);
				add_one_item_to_listcont(list,icon,87);
			}
			ShellCmdPrintf("111111112:%d\n",__LINE__);
			break;
		case 2:
			for(int i=0;i<page;i++){
				tab = lv_tabview_add_tab(parent," ");
				cJSON_AddItemToArray(item_array,cJSON_CreateNumber((int)tab));
				list = lv_obj_create(tab);
				lv_obj_remove_style_all(list);
				lv_obj_set_size(list, LV_PCT(100), LV_PCT(100)); 
				lv_obj_set_flex_flow(list, LV_FLEX_FLOW_COLUMN);
				lv_obj_set_style_bg_opa(list, LV_OPA_MIN, 0);
				for(int j=0;j<8;j++){
					cJSON* icon = cJSON_GetArrayItem(item,cnt);
					if(icon == NULL)
						break;
					cnt++;
					add_one_item_to_listcont(list,icon,87);
				}
			}
			
			break;
		default:
			break;
	}
	ShellCmdPrintf("111111112:%d\n",__LINE__);
	lv_obj_set_user_data(parent, item_array);
	ShellCmdPrintf("111111112:%d\n",__LINE__);
    return tab;
}

void VtkListContFresh(lv_obj_t* listcont,cJSON *list)
{
	int i;
	int size=cJSON_GetArraySize(list);
	//lv_obj_clean(listcont);
	//void *user_data=lv_obj_get_user_data(listcont);
	//if(user_data)
		//free(user_data);
	//lv_obj_set_user_data(listcont, NULL);
	ShellCmdPrintf("11111111:%d\n",__LINE__);
	cJSON *item_array=lv_obj_get_user_data(listcont);
	if(cJSON_IsArray(item_array))
	{
		for(i=0;i<cJSON_GetArraySize(item_array);i++)
		{
			cJSON *item=cJSON_GetArrayItem(item_array,i);
			lv_obj_del((lv_obj_t*)item->valueint);
		}
		cJSON_Delete(item_array);
		lv_obj_set_user_data(listcont,NULL);
	}
	if(size <= 16)
	{
		ShellCmdPrintf("11111111:%d\n",__LINE__);
		add_item_to_tabview(listcont,list,1);
	}
	#if 0
	else if((size > 16) && (size <= 40))
	{
		add_item_to_tabview(listcont,list,2);
	}
	#endif
	else
	{
		ShellCmdPrintf("11111111:%d\n",__LINE__);
		page_list_para_stru* page_list_para=malloc(sizeof(page_list_para_stru));
		lv_obj_set_user_data(listcont, page_list_para);
		page_list_para->list=list;
		
		page_list_para->cur_page=0;
		
		lv_obj_clear_flag(listcont, LV_OBJ_FLAG_SCROLLABLE);
		create_one_list_page(listcont);
		update_one_list_page_text(page_list_para);
		#if 0
		for(i=0;i<8;i++)
		{
			add_one_item_to_listcont(listcont,cJSON_GetArrayItem(list,i),75);
		}
		#endif
		
	}
	ShellCmdPrintf("11111111:%d\n",__LINE__);
}


static lv_obj_t* add_item_to_tabview(lv_obj_t* parent,cJSON *item,int mode)
{
	lv_obj_t* tab = NULL;
	lv_obj_t* list = NULL;
	lv_obj_t* btn = NULL;
	int cnt = 0;
	int size = cJSON_GetArraySize(item);
	int page = 0;
	//cJSON *item_array=cJSON_CreateArray();
	page_list_para_stru* page_list_para=lv_obj_get_user_data(parent);
	if(size % 8 == 0)
		page = size / 8;
	else 
		page = size / 8 + 1;
	switch(mode){
		case 1:
			ShellCmdPrintf("111111112:%d\n",__LINE__);
			btn = lv_tabview_get_tab_btns(parent);
			//cJSON_AddItemToArray(item_array,cJSON_CreateNumber((int)btn));
			lv_obj_set_size(btn, 0, 0);
			tab = lv_tabview_add_tab(parent," ");
			//cJSON_AddItemToArray(item_array,cJSON_CreateNumber((int)tab));
			if(page_list_para)
				page_list_para->tab=tab;
			list = lv_obj_create(tab);
			lv_obj_remove_style_all(list); 
			lv_obj_set_size(list, LV_PCT(100), LV_PCT(100));
			lv_obj_set_flex_flow(list, LV_FLEX_FLOW_COLUMN);
			lv_obj_set_style_bg_opa(list, LV_OPA_MIN, 0);
			ShellCmdPrintf("111111112:%d\n",__LINE__);
			for(int j=0;j<size;j++){
				cJSON* icon = cJSON_GetArrayItem(item,j);
				if(icon == NULL)
					break;
				ShellCmdPrintf("111111112:%d\n",__LINE__);
				add_one_item_to_listcont(list,icon,87);
			}
			ShellCmdPrintf("111111112:%d\n",__LINE__);
			break;
		case 2:
			for(int i=0;i<page;i++){
				tab = lv_tabview_add_tab(parent," ");
				if(page_list_para)
					page_list_para->tab=tab;
				//cJSON_AddItemToArray(item_array,cJSON_CreateNumber((int)tab));
				list = lv_obj_create(tab);
				lv_obj_remove_style_all(list);
				lv_obj_set_size(list, LV_PCT(100), LV_PCT(100)); 
				lv_obj_set_flex_flow(list, LV_FLEX_FLOW_COLUMN);
				lv_obj_set_style_bg_opa(list, LV_OPA_MIN, 0);
				for(int j=0;j<8;j++){
					cJSON* icon = cJSON_GetArrayItem(item,cnt);
					if(icon == NULL)
						break;
					cnt++;
					add_one_item_to_listcont(list,icon,87);
				}
			}
			
			break;
		default:
			break;
	}
	ShellCmdPrintf("111111112:%d\n",__LINE__);
	//lv_obj_set_user_data(parent, item_array);
	
	//page_list_para->
	ShellCmdPrintf("111111112:%d\n",__LINE__);
    return tab;
}
void VtkListContFresh(lv_obj_t* listcont,cJSON *list)
{
	int i;
	int size=cJSON_GetArraySize(list);
	//lv_obj_clean(listcont);
	//void *user_data=lv_obj_get_user_data(listcont);
	//if(user_data)
		//free(user_data);
	//lv_obj_set_user_data(listcont, NULL);
	ShellCmdPrintf("11111111:%d\n",__LINE__);
	#if 0
	cJSON *item_array=lv_obj_get_user_data(listcont);
	if(cJSON_IsArray(item_array))
	{
		for(i=0;i<cJSON_GetArraySize(item_array);i++)
		{
			cJSON *item=cJSON_GetArrayItem(item_array,i);
			lv_obj_del((lv_obj_t*)item->valueint);
		}
		cJSON_Delete(item_array);
		lv_obj_set_user_data(listcont,NULL);
	}
	#endif
	page_list_para_stru* page_list_para=lv_obj_get_user_data(listcont);
	if(page_list_para)
	{
		if(page_list_para->type==0&&page_list_para->tab)
		{
			lv_obj_del(page_list_para->tab);
		}
		if(page_list_para->type==1)
		{
			for(i=0;i<page_list_para->line_num;i++)
			{
				if(page_list_para->line_list[i])
					lv_obj_del(page_list_para->line_list[i]);
			}
		}
		free(page_list_para);
	}
	
	if(size <= 16)
	{
		ShellCmdPrintf("11111111:%d\n",__LINE__);
		page_list_para=malloc(sizeof(page_list_para_stru));
		memset(page_list_para,0,sizeof(page_list_para_stru));
		page_list_para->type=0;
		lv_obj_set_user_data(listcont, page_list_para);
		add_item_to_tabview(listcont,list,1);
	}
	#if 0
	else if((size > 16) && (size <= 40))
	{
		add_item_to_tabview(listcont,list,2);
	}
	#endif
	else
	{
		ShellCmdPrintf("11111111:%d\n",__LINE__);
		page_list_para=malloc(sizeof(page_list_para_stru));
		memset(page_list_para,0,sizeof(page_list_para_stru));
		page_list_para->type=1;
		lv_obj_set_user_data(listcont, page_list_para);
		page_list_para->list=list;
		
		page_list_para->cur_page=0;
		page_list_para->listcond=listcont;
		
		lv_obj_clear_flag(listcont, LV_OBJ_FLAG_SCROLLABLE);
		create_one_list_page(listcont);
		vtk_lvgl_lock();
		update_one_list_page_text(page_list_para);
		vtk_lvgl_unlock();
		#if 0
		for(i=0;i<8;i++)
		{
			add_one_item_to_listcont(listcont,cJSON_GetArrayItem(list,i),75);
		}
		#endif
		
	}
	ShellCmdPrintf("11111111:%d\n",__LINE__);
}
#endif

void VtkListReset(lv_obj_t* listcont)
{
	if(page_list_para == NULL)
		return;
	if(page_list_para->list==NULL)
		return;
	if(page_list_para->cur_page)
	{
		page_list_para->cur_page=0;
		update_one_list_page_text(page_list_para);
	}

	if(API_PublicInfo_Read_Bool("OnInstaller")){
		if(page_list_para->listbtn){
			//lv_obj_set_style_bg_color(page_list_para->listbtn,lv_color_make(255,178,64),0);
			lv_obj_t* label = lv_obj_get_child(page_list_para->listbtn,0);
			lv_label_set_text(label,LV_VTK_EDIT);
			lv_obj_set_style_text_color(label,lv_color_white(),0);
			lv_obj_clear_flag(page_list_para->listbtn,LV_OBJ_FLAG_HIDDEN);
		}		
		API_PublicInfo_Write_Bool("ListEdit",false);
	}else{
		if(page_list_para->listbtn){
			lv_obj_t* label = lv_obj_get_child(page_list_para->listbtn,0);
			lv_obj_set_style_text_color(label,lv_color_white(),0);
			lv_label_set_text(label,LV_VTK_LEFT);
		}
		API_PublicInfo_Write_Bool("ListEdit",false);
	}
}

int GetListCurPage(void)
{
	if(page_list_para == NULL)
		return 0;
	if(page_list_para->list==NULL)
		return 0;
	return page_list_para->cur_page;
	
}

void update_one_list_page_text(page_list_para_stru* page_list_para)
{
	int i;
	int size = cJSON_GetArraySize(page_list_para->list);
	cJSON *item;
	char buf[100];
#if 0
	//int size=cJSON_GetArraySize(page_list_para->list);
	int total_page=size/8+(size%8==0?0:1);
	lv_obj_add_flag(page_list_para->listcond, LV_OBJ_FLAG_HIDDEN);
	sprintf(buf,"<%d/%d>",page_list_para->cur_page+1,total_page);
	lv_label_set_text(page_list_para->page_text,buf);
	page_list_para->line_num=8;
	for(i=0;i<8;i++)
	{

		//ShellCmdPrintf("3333333:%d:ifinvtklvgl_thread:%d\n",__LINE__,ifinvtklvgl_thread());
#endif
	lv_obj_t* obj = lv_obj_get_parent(page_list_para->line_list[0]);
	lv_obj_add_flag(obj,LV_OBJ_FLAG_HIDDEN);

	int len = page_list_para->list_len;
	int total_page = size/len+(size%len==0?0:1);
	for(int i=0;i<total_page;i++){
		if(page_list_para->page_icon == NULL)
			break;
		lv_obj_t* btn = lv_obj_get_child(page_list_para->page_icon, i);
		if (i == page_list_para->cur_page) {
			lv_obj_set_style_text_color(btn, lv_color_white(), 0);
		}else{
			lv_obj_set_style_text_color(btn, lv_color_make(85,85,85), 0);
		}
	}
	for(i=0;i<len;i++)
	{
		
		lv_obj_remove_event_cb(page_list_para->line_list[i], CallList_click_event2);
		if((page_list_para->cur_page*len+i)<size)
		{
			item=cJSON_GetArrayItem(page_list_para->list,page_list_para->cur_page*len+i);
			GetJsonDataPro(item,LIST_SHOW,buf);
			lv_label_set_text(page_list_para->text_list[i],buf);
			lv_obj_add_flag(page_list_para->line_list[i], LV_OBJ_FLAG_CLICKABLE);
			lv_obj_add_event_cb(page_list_para->line_list[i], CallList_click_event2, LV_EVENT_ALL, item);
			lv_label_set_text(page_list_para->call_list[i],LV_VTK_INTERCOM2);
		}
		else
		{
			lv_label_set_text(page_list_para->text_list[i],"");
			//lv_obj_remove_event_cb(page_list_para->line_list[i], CallList_click_event2);
			lv_obj_clear_flag(page_list_para->line_list[i], LV_OBJ_FLAG_CLICKABLE);
			lv_label_set_text(page_list_para->call_list[i],"");
		}		
	}
	lv_obj_clear_flag(obj,LV_OBJ_FLAG_HIDDEN);

}
static void turn_down_event(lv_event_t* e)
{
    lv_event_code_t code = lv_event_get_code(e);
    page_list_para_stru *page_list_para = lv_event_get_user_data(e);

    lv_obj_t* target = lv_event_get_target(e);
	int len = page_list_para->list_len;
    if(code == LV_EVENT_SCROLL_BEGIN)
    {
       ;
    }else if (code == LV_EVENT_RELEASED) {
       ;
    } else if (code == LV_EVENT_CLICKED ) {
      
		int size=cJSON_GetArraySize(page_list_para->list);
		int total_page=size/len+(size%len==0?0:1);
		page_list_para->cur_page++;
		if(page_list_para->cur_page>=total_page)
			page_list_para->cur_page=0;
		update_one_list_page_text(page_list_para);

    }
        
}
static void turn_up_event(lv_event_t* e)
{
    lv_event_code_t code = lv_event_get_code(e);
    page_list_para_stru *page_list_para = lv_event_get_user_data(e);

    lv_obj_t* target = lv_event_get_target(e);
	int len = page_list_para->list_len;
    if(code == LV_EVENT_SCROLL_BEGIN)
    {
       ;
    }else if (code == LV_EVENT_RELEASED) {
       ;
    } else if (code == LV_EVENT_CLICKED ) {
		if(API_PublicInfo_Read_Bool("OnInstaller")){

			if(API_PublicInfo_Read_Bool("ListEdit"))
			{
				//lv_obj_set_style_bg_color(page_list_para->listbtn,lv_color_make(255,178,64),0);
				//lv_obj_t* label = lv_obj_get_child(page_list_para->listbtn,0);
				lv_obj_set_style_text_color(lv_obj_get_child(page_list_para->listbtn,0),lv_color_white(),0);
				API_PublicInfo_Write_Bool("ListEdit",false);
				}
			else
			{
				//lv_obj_set_style_bg_color(page_list_para->listbtn,lv_color_make(19,132,250),0);
				lv_obj_set_style_text_color(lv_obj_get_child(page_list_para->listbtn,0),lv_color_black(),0);
				API_PublicInfo_Write_Bool("ListEdit",true);
				}
		}else{
			int size=cJSON_GetArraySize(page_list_para->list);
			int total_page=size/len+(size%len==0?0:1);
			
			if(page_list_para->cur_page==0)
				page_list_para->cur_page=total_page-1;
			else
				page_list_para->cur_page--;
			update_one_list_page_text(page_list_para);
		}					
    }
        
}

static void home_event(lv_event_t* e)
{
    MainMenu_Process(MAINMENU_Return_MainPage,NULL);    
}
void init_page_list_para(void)
{
	if(page_list_para)
	{
		if(page_list_para->list)
			cJSON_Delete(page_list_para->list);
		free(page_list_para);
	}
	page_list_para=NULL;
}

extern lv_ft_info_t ft_0;
extern void MainMemu_Pressed_event_cb(lv_event_t* e);
void create_one_list_page(lv_obj_t* listcont,cJSON *list1)
{
	int i;
	lv_obj_t* one_line;
	lv_obj_t **text_list;
	lv_obj_t **line_list;
	lv_obj_t **call_list;
	lv_obj_t* name_text;
	lv_obj_t* call_text;

	if(page_list_para)
	{
		free(page_list_para);
	}

	char buf[100];
	char* buf1;

	page_list_para = malloc(sizeof(page_list_para_stru));
	memset(page_list_para, 0,sizeof(page_list_para_stru));

	page_list_para->list = cJSON_Duplicate(list1,1);
	page_list_para->listcont=listcont;
	text_list = page_list_para->text_list;
	line_list = page_list_para->line_list;
	call_list = page_list_para->call_list;

	int size = cJSON_GetArraySize(page_list_para->list);
	if(size <= 4){
		page_list_para->list_len = 4;
	}else if(size == 5){
		page_list_para->list_len = 5;
	}else if(size == 6){
		page_list_para->list_len = 6;
	}else if(size == 7){
		page_list_para->list_len = 7;
	}else if(size == 8){
		page_list_para->list_len = 8;
	}else{
		page_list_para->list_len = API_Para_Read_Int(ListLen);
	}
	lv_obj_t* list_obj = lv_obj_create(listcont);	
	lv_obj_remove_style_all(list_obj);
	lv_obj_clear_flag(list_obj, LV_OBJ_FLAG_SCROLLABLE);
	lv_obj_set_x(list_obj,12);
	lv_obj_set_size(list_obj, 427, 650);	
    lv_obj_set_flex_flow(list_obj, LV_FLEX_FLOW_COLUMN);
	for(i=0;i<page_list_para->list_len;i++)
	{
		cJSON *item = cJSON_GetArrayItem(page_list_para->list,i);
		if(item != NULL){
			GetJsonDataPro(item,LIST_SHOW,buf);
			buf1 = LV_VTK_INTERCOM2;
		}else{
			buf[0] = 0;
			buf1 = "";
		}
		one_line = lv_obj_create(list_obj);
		lv_obj_remove_style_all(one_line);
		page_list_para->list_size = API_Para_Read_Int(ListSize);
		set_font_size(one_line,page_list_para->list_size);
		AddOneMenuCommonClickedCb(one_line,MainMemu_Pressed_event_cb);
			switch(page_list_para->list_len){
				case 4 :
					lv_obj_set_size(one_line, LV_PCT(100), 165);
					//set_font_size(one_line,1);
					break;
				case 5 :
					lv_obj_set_size(one_line, LV_PCT(100), 131);
					//set_font_size(one_line,1);
					break;
				case 6 :
					lv_obj_set_size(one_line, LV_PCT(100), 110);
					//set_font_size(one_line,1);
					break;
				case 7 :
					lv_obj_set_size(one_line, LV_PCT(100), 95);
					//set_font_size(one_line,1);
					break;
				case 8 :
					lv_obj_set_size(one_line, LV_PCT(100), 83);
					
					break;
			}

		    lv_obj_set_style_border_side(one_line, LV_BORDER_SIDE_BOTTOM, 0);
		    lv_obj_set_style_border_width(one_line, 3, 0);
			lv_obj_set_style_border_opa(one_line, LV_OPA_70, 0);
			lv_obj_set_style_border_color(one_line,lv_color_make(85,85,85),0);
			lv_obj_add_event_cb(one_line, CallList_click_event2, LV_EVENT_ALL, item);
			
			//lv_obj_set_style_border_opa(one_line,LV_OPA_MIN,0);
	
		    //lv_obj_set_style_bg_opa(call_obj, LV_OPA_50, 0);
		    lv_obj_set_style_radius(one_line, 0, 0);
		    lv_obj_clear_flag(one_line, LV_OBJ_FLAG_SCROLLABLE);
		    //lv_obj_clear_flag(one_line, LV_OBJ_FLAG_CLICKABLE);
		  	name_text = lv_label_create(one_line);
			lv_obj_set_width(name_text,300);
		  	lv_obj_align(name_text, LV_ALIGN_LEFT_MID, 20, 0);
		    lv_label_set_text(name_text,buf);
			page_list_para->list_color = API_Para_Read_Int(ListColor);
			lv_obj_set_style_text_color(name_text,lv_color_hex(page_list_para->list_color),0);
			lv_label_set_long_mode(name_text,LV_LABEL_LONG_SCROLL);
		    
			line_list[i]=one_line;	
			text_list[i]=name_text;

			call_text = lv_label_create(one_line);
			lv_obj_align(call_text, LV_ALIGN_RIGHT_MID, -23, 0);
			lv_label_set_text(call_text,buf1);
			lv_obj_set_style_text_color(call_text,lv_color_make(51,201,121),0);
			call_list[i] = call_text;

	}
	if(size > 8){	
		lv_obj_t* icon = lv_obj_create(listcont);	
		page_list_para->page_icon = icon;
		lv_obj_remove_style_all(icon);	
		//set_font_size(icon,0);
		lv_obj_set_style_text_font(icon, ft_0.font, 0);
		lv_obj_align(icon,LV_ALIGN_BOTTOM_MID,0,-78);
		lv_obj_set_size(icon, LV_SIZE_CONTENT, 30);
		lv_obj_set_flex_flow(icon, LV_FLEX_FLOW_ROW_WRAP);

		int total_page = size/page_list_para->list_len+(size%page_list_para->list_len==0?0:1);
		for (size_t i = 0; i < total_page; i++)
		{
			lv_obj_t* obj = lv_label_create(icon);  
			lv_label_set_text(obj,LV_VTK_CIRCLE);
			lv_obj_set_style_pad_hor(obj,10,0);
			if(i == 0)
				lv_obj_set_style_text_color(obj, lv_color_white(), 0);
			else
				lv_obj_set_style_text_color(obj, lv_color_make(85,85,85), 0);
		}
	}
	lv_obj_t* btn_obj = lv_obj_create(listcont);	
	lv_obj_remove_style_all(btn_obj);
	lv_obj_set_y(btn_obj,650);
    lv_obj_set_size(btn_obj, LV_PCT(100), 150);
    //lv_obj_set_style_pad_all(btn_obj, 0, 0);
	//lv_obj_set_style_bg_opa(btn_obj, LV_OPA_MIN, 0);

	lv_obj_t* delBtn = lv_obj_create(btn_obj);
	lv_obj_clear_flag(delBtn, LV_OBJ_FLAG_SCROLLABLE);
	//lv_obj_remove_style_all(delBtn);
	lv_obj_set_style_border_width(delBtn, 0, 0);
	lv_obj_add_flag(delBtn,LV_OBJ_FLAG_CLICKABLE);
	lv_obj_set_style_shadow_width(delBtn,0,0);		
	//lv_obj_set_style_bg_color(delBtn,lv_color_make(255,178,64),0);
	lv_obj_set_style_bg_color(delBtn,lv_color_hex(GetNameListpTurnBtnColor()),0);
	lv_obj_set_style_radius(delBtn,15,0);
	lv_obj_align(delBtn,LV_ALIGN_LEFT_MID, 15, -4);
	lv_obj_set_size(delBtn, 125, 80);
	set_font_size(delBtn,2);
	lv_obj_add_event_cb(delBtn, turn_up_event, LV_EVENT_CLICKED, page_list_para);
	lv_obj_t* label1 = lv_label_create(delBtn);		
	lv_obj_set_style_text_color(label1,lv_color_white(),0);
	lv_obj_center(label1);
	if(API_PublicInfo_Read_Bool("OnInstaller")){
		lv_label_set_text(label1, LV_VTK_EDIT);
		if(API_PublicInfo_Read_Bool("ListEdit"))
		{
			//lv_obj_set_style_bg_color(delBtn,lv_color_make(19,132,250),0);
			lv_obj_set_style_text_color(label1,lv_color_black(),0);
		}
	}else{
		lv_label_set_text(label1, LV_VTK_LEFT);
	}		
	page_list_para->listbtn = delBtn;

	lv_obj_t* addBtn = lv_obj_create(btn_obj);
	lv_obj_clear_flag(addBtn, LV_OBJ_FLAG_SCROLLABLE);
	//lv_obj_remove_style_all(addBtn);
	lv_obj_set_style_border_width(addBtn, 0, 0);
	lv_obj_add_flag(addBtn,LV_OBJ_FLAG_CLICKABLE);
	lv_obj_set_style_shadow_width(addBtn,0,0);		
	//lv_obj_set_style_bg_color(addBtn,lv_color_make(255,178,64),0);
	lv_obj_set_style_bg_color(addBtn,lv_color_hex(GetNameListpTurnBtnColor()),0);
	lv_obj_set_style_radius(addBtn,15,0);
	lv_obj_align(addBtn, LV_ALIGN_RIGHT_MID, -15, -4);
	lv_obj_set_size(addBtn, 121, 80);
	set_font_size(addBtn,2);
	lv_obj_add_event_cb(addBtn, turn_down_event, LV_EVENT_CLICKED, page_list_para);
	lv_obj_t* label2 = lv_label_create(addBtn);
	lv_label_set_text(label2, LV_VTK_RIGHT);
	lv_obj_set_style_text_color(label2,lv_color_white(),0);
	lv_obj_center(label2);

	if(size <= 8){
		if(!API_PublicInfo_Read_Bool("OnInstaller"))
			lv_obj_add_flag(delBtn,LV_OBJ_FLAG_HIDDEN);	
		lv_obj_add_flag(addBtn,LV_OBJ_FLAG_HIDDEN);
	}	 	
	
	lv_obj_t* homeBtn = lv_btn_create(btn_obj);
	lv_obj_remove_style_all(homeBtn);
	lv_obj_align(homeBtn, LV_ALIGN_CENTER, 0, -4);
	lv_obj_set_size(homeBtn, 125, 80);
	set_font_size(homeBtn,2);
	lv_obj_add_event_cb(homeBtn, home_event, LV_EVENT_CLICKED, NULL);
	//lv_obj_t* label = lv_label_create(homeBtn);
	//lv_label_set_text(label, LV_VTK_CALL_RECORD1);
	//lv_obj_set_style_text_color(label,lv_color_white(),0);
	//lv_obj_center(label);
#if 0
	lv_obj_t* pageinfo = lv_label_create(btn_obj);
	lv_obj_align(pageinfo, LV_ALIGN_CENTER, 0, 0);
	lv_obj_set_style_text_color(pageinfo, lv_color_white(), 0);
	set_font_size(pageinfo,1);
	page_list_para->page_text=pageinfo;
#endif
}
static lv_obj_t* add_one_item_to_listcont(lv_obj_t* parent,cJSON *item,int height)
{

	char buf[100];
    lv_obj_t* call_obj = lv_obj_create(parent);
    lv_obj_set_size(call_obj, LV_PCT(100), height);
    lv_obj_set_style_border_side(call_obj, LV_BORDER_SIDE_BOTTOM, 0);
    lv_obj_set_style_border_width(call_obj, 5, 0);
    lv_obj_set_style_bg_opa(call_obj, LV_OPA_MIN, 0);
    lv_obj_set_style_radius(call_obj, 0, 0);
    lv_obj_clear_flag(call_obj, LV_OBJ_FLAG_SCROLLABLE);
    lv_obj_add_flag(call_obj, LV_OBJ_FLAG_CLICKABLE);
    //lv_obj_clear_flag(call_obj, LV_OBJ_FLAG_SNAPPABLE);
    //lv_obj_add_flag(call_obj, LV_OBJ_FLAG_EVENT_BUBBLE);
    //lv_obj_set_style_bg_color(call_obj, lv_palette_main(LV_PALETTE_GREY) , LV_STATE_PRESSED);

    //lv_obj_set_flex_flow(call_obj, LV_FLEX_FLOW_ROW);
	GetJsonDataPro(item,LIST_SHOW,buf);

	
    //sprintf(buf,"%s                    009900%02d01", LV_SYMBOL_AUDIO,cnt);
    
    lv_obj_add_event_cb(call_obj, CallList_click_event2, LV_EVENT_ALL, item);

    lv_obj_t* type_text = NULL;
    lv_obj_t* name_text = NULL;
    lv_obj_t* num_text = NULL;
    lv_obj_t* call_text = NULL;

    //type_text = lv_label_create(call_obj);
    //lv_label_set_text(type_text, LV_SYMBOL_AUDIO);
    //lv_obj_set_style_text_font(type_text, font_larger, 0);
    //lv_obj_set_width(type_text, 100);      

    name_text = lv_label_create(call_obj);
    lv_label_set_text(name_text,buf);
    lv_obj_set_style_text_font(name_text, font_larger, 0);
    lv_obj_set_style_text_color(name_text,lv_palette_main(LV_PALETTE_BLUE),0);
    //call_text = lv_label_create(call_obj);
    //lv_label_set_text(call_text, LV_SYMBOL_BELL);
    //lv_obj_set_style_text_font(call_text, font_larger, 0);

    lv_obj_align(name_text, LV_ALIGN_CENTER, 0, 0);
    //lv_obj_align(name_text, LV_ALIGN_CENTER, 0, 0);
    //lv_obj_align(call_text, LV_ALIGN_RIGHT_MID, -10, 0);

    return call_obj;
}

static void CallList_click_event2(lv_event_t* e)
{
    lv_event_code_t code = lv_event_get_code(e);
    cJSON *item = lv_event_get_user_data(e);

    lv_obj_t* target = lv_event_get_target(e);
	char buf[20];
    if(code == LV_EVENT_PRESSED)
    {
		lv_obj_set_style_bg_opa(target, LV_OPA_70, 0);
		//lv_obj_set_style_text_color(target, lv_color_make(255,57,61), 0);
   
    }else if (code == LV_EVENT_RELEASED) {

		lv_obj_set_style_bg_opa(target, LV_OPA_MIN, 0);

    } else if (code == LV_EVENT_CLICKED && (!get_is_sliding()))
	{
		cJSON* key = cJSON_GetObjectItemCaseSensitive(item, "ListKey");
		if(cJSON_IsNumber(key))
		{
			cJSON* dataset = GetDataSetList();
			cJSON* where = cJSON_CreateObject();
			cJSON* view = cJSON_CreateArray();
			cJSON_AddNumberToObject(where, "ListKey", key->valueint);
			if(API_TB_SelectBySort(dataset, view, where, 0))
			{
				cJSON* record = cJSON_GetArrayItem(view, 0);
				if(API_PublicInfo_Read_Bool("OnInstaller")&&API_PublicInfo_Read_Bool("ListEdit"))
				{
					char* listType = GetEventItemString(record,  "ListType");
					if(!strcmp(listType, "APT_MAT"))
					{
						EditAptMatRecord(record, 0);
					}
					else if(!strcmp(listType, "HYBRID_MAT"))
					{
						cJSON* editRecord = cJSON_CreateObject();
						cJSON_AddItemToObject(editRecord, IX2V_GW_ID, cJSON_Duplicate(cJSON_GetObjectItemCaseSensitive(record, IX2V_GW_ID), 1));
						cJSON_AddItemToObject(editRecord, IX2V_IM_ID, cJSON_Duplicate(cJSON_GetObjectItemCaseSensitive(record, IX2V_IM_ID), 1));
						cJSON_AddItemToObject(editRecord, IX2V_IX_NAME, cJSON_Duplicate(cJSON_GetObjectItemCaseSensitive(record, IX2V_IX_NAME), 1));
						cJSON_AddItemToObject(editRecord, IX2V_L_NBR, cJSON_Duplicate(cJSON_GetObjectItemCaseSensitive(record, IX2V_L_NBR), 1));
						cJSON_AddItemToObject(editRecord, IX2V_G_NBR, cJSON_Duplicate(cJSON_GetObjectItemCaseSensitive(record, IX2V_G_NBR), 1));
						CreateResEditOneMenu(editRecord, NULL, 0);
						cJSON_Delete(editRecord);
					}
					else
					{
						API_TIPS_Ext_Time("Not allowed to edit", 2);
					}
				}
				else
				{
					cJSON* object = cJSON_CreateObject();
					cJSON_AddItemToObject(object, "DevNum", cJSON_Duplicate(cJSON_GetObjectItemCaseSensitive(record, "IX_ADDR"), 1));
					MainMenu_Process(MAINMENU_MAIN_StartCall,object);
					cJSON_Delete(object);
				}
			}
			cJSON_Delete(dataset);
			cJSON_Delete(where);
			cJSON_Delete(view);
		}
    }       
}

//yxc 20240911 add
void IO_List_Update(cJSON *old, cJSON *new)
{
    if(!cJSON_Compare(old, new, 1))
    {
		API_Event_By_Name(EventGetNamelist);
    }
}