#include "MenuCFGSwitch.h"
#include "menu_page.h"
#include "Common_TableView.h"

#define MENUCFG_SELECT_PARA		"{\"HeaderEnable\":true,\"CellCallback\":0,\"ColumnWidth\":[180, 400]}"
#define MENUCFG_SELECT_FIELD_NAME			"NAME"
#define MENUCFG_SELECT_FIELD_DESCRIBE		"DESCRIBE"

static TB_VIWE* menucfgSelect = NULL;
static char* cnt_dir;

/*****************

 * **************/
static void MenuCfgCellClick(int line, int col)
{   
    //char* name = lv_table_get_cell_value(menucfgSelect->table,line+1,1);    
   	cJSON *name=cJSON_GetObjectItemCaseSensitive(cJSON_GetArrayItem(menucfgSelect->view,line),MENUCFG_SELECT_FIELD_NAME);
	if(name)
    		MenuCfg_preview_init(name->valuestring);
    #if 0
	API_Para_Write_String("UI_Documents",name);
    LV_API_TIPS("Menu restart, please wait!",1);
    lv_timer_create(timer_out_cb,200,NULL);
    #endif
}

void EnterMenuCfgSelectMenu(const char* select)
{
    if(!select)
    {
        return;
    }
    
    char des[100];
	cJSON* para = cJSON_Parse(MENUCFG_SELECT_PARA);
	int callback = MenuCfgCellClick;
    int fiterCallback = choose_menuCfg_pos;
	cJSON_ReplaceItemInObjectCaseSensitive(para, "CellCallback", cJSON_CreateNumber(callback));
    char* dir;

    cJSON* fiter = cJSON_CreateObject();
    cJSON_AddStringToObject(fiter, "Name", "MenuCfg Pos");
    cJSON_AddStringToObject(fiter, "Value", select);
    cJSON_AddNumberToObject(fiter, "Callback", fiterCallback);
    cJSON_AddItemToObject(para,"Filter",fiter);

    if(strcmp("FW",select) == 0){
        dir = MENU_CONFIG_FIX_PATH;
        //dir[1] = NULL;
    }       
    else if(strcmp("SD Card",select) == 0){
        dir = MENU_CONFIG_SD_PATH;
        //dir[1] = NULL;
    } 
    else if(strcmp("Customer",select) == 0){
        dir = MENU_CONFIG_CUS_PATH;
        //dir[1] = NULL;
    }
    else if(strcmp("All",select) == 0){
        dir = MENU_CONFIG_FIX_PATH;
        //dir[1] = MENU_CONFIG_SD_PATH;
        //dir[2] = MENU_CONFIG_CUS_PATH;
        //dir[3] = NULL;
    }
    else{
        //dir[0] = NULL;
    }   

    cJSON* view = cJSON_CreateArray();
    //for(int j = 0;  j < 4 && dir[j]; j++)
    //{
        cJSON* fileList = GetMenuCFGListName(dir);   
        int size = cJSON_GetArraySize(fileList);
        for(int i=0;i<size;i++){
            cJSON* fileName = cJSON_GetArrayItem(fileList, i);
            if(cJSON_IsString(fileName))
            {
                cJSON* record = cJSON_CreateObject();
                sprintf(des,"%s/%s.des.json",dir,fileName->valuestring);
                cJSON* desc = get_cjson_from_file(des);
                if(desc)
                    cJSON_AddItemToObjectCS(record, MENUCFG_SELECT_FIELD_DESCRIBE, cJSON_GetObjectItem(desc,MENUCFG_SELECT_FIELD_DESCRIBE));
                cJSON_AddStringToObject(record, MENUCFG_SELECT_FIELD_NAME, fileName->valuestring);
                cJSON_AddItemToArray(view, record);
            }
        }
    //}
   
	TB_MenuDisplay(&menucfgSelect, view, "MenuCfg Select", para);
	cJSON_Delete(fileList);
	cJSON_Delete(view);
	cJSON_Delete(para);
}

static void choose_pos_event(const char* chose, void* userdata)
{   
    if(!chose)
    {
        return;
    }

    if(strcmp("FW",chose) == 0){
        //TB_MenuDelete(&menucfgSelect);
        EnterMenuCfgSelectMenu("FW");
    }       
    else if(strcmp("SD Card",chose) == 0){
        //TB_MenuDelete(&menucfgSelect);
        EnterMenuCfgSelectMenu("SD Card");
    } 
    else if(strcmp("Customer",chose) == 0){
        //TB_MenuDelete(&menucfgSelect);
        EnterMenuCfgSelectMenu("Customer");
    }
    else if(strcmp("All",chose) == 0){
        //TB_MenuDelete(&menucfgSelect);
        EnterMenuCfgSelectMenu("All");
    }
}

static void choose_menuCfg_pos(const cJSON* filter)
{   
    cJSON* value = cJSON_GetObjectItem(filter,"Value");
    static CHOSE_MENU_T callback = {.callback = 0, .cbData = NULL};
	callback.callback = choose_pos_event;
	callback.cbData = NULL;
    cJSON* choseList = cJSON_CreateArray();
    cJSON_AddItemToArray(choseList, cJSON_CreateString("FW"));
    cJSON_AddItemToArray(choseList, cJSON_CreateString("SD Card"));
    cJSON_AddItemToArray(choseList, cJSON_CreateString("Customer"));
    //cJSON_AddItemToArray(choseList, cJSON_CreateString("All"));
    char* choseString = cJSON_IsString(value) ? value->valuestring : NULL;
    CreateChooseMenu("Choose Path", choseList, choseString, &callback);
    cJSON_Delete(choseList);
}

void MenuCfg_Select_init(void)
{
	cJSON* fileList = GetMenuCFGListName(MENU_CONFIG_CUS_PATH);   
	if(cJSON_GetArraySize(fileList)>0)
		EnterMenuCfgSelectMenu("Customer");
	else
    		EnterMenuCfgSelectMenu("FW");
	cJSON_Delete(fileList);
}

void MenuCfg_Select_Close(void)
{
	TB_MenuDelete(&menucfgSelect);
}

int IX850SMenuCfg_Choose_Shell_Test(cJSON *cmd)
{
	char* jsonstr = GetShellCmdInputString(cmd, 1);

	ShellCmdPrintf("%s:",jsonstr);

	if(strcmp(jsonstr,"load")==0)
	{
		char *filename = GetShellCmdInputString(cmd, 2);

		vtk_lvgl_lock();

        EnterMenuCfgSelectMenu("FW");
		//MenuCfg_list_init();

		vtk_lvgl_unlock();
	}				
}
#if 1
cJSON* GetMenuCFGListName(const char *dir)
{
    if(!dir)
        return;
	char cmd[100];
	char linestr[100];
    int i;
    char* p;
    char* file;
    cJSON* list = cJSON_CreateArray();
  
    cnt_dir = dir;

	snprintf(cmd, 100, "ls -l %s | grep .json | awk '{print $9}'", dir);
	
	FILE *pf = popen(cmd,"r");
	if(pf == NULL)
	{
		return 0;
	}

	while(fgets(linestr,100,pf) != NULL)
	{
		for(i = 0; linestr[i] != 0 && i < 100; i++)
		{
			if(linestr[i] == '\r' || linestr[i] == '\n')
			{
				linestr[i] = 0;
			}
		}

		if(strlen(linestr) == 0)
		{
			continue;
		}
        p = strtok(linestr, ".");
		if(p == NULL)
		{
			p[0] = 0;
		}
        file = strtok(NULL,".");
        if(strcmp(file,"json") == 0)
		    cJSON_AddItemToArray(list,cJSON_CreateString(p));
	}
	
	pclose(pf);

	return list;
}
#endif

static void cancel_click_event(lv_event_t* e)
{
    lv_obj_t* obj = (lv_obj_t*)lv_event_get_user_data(e);
    lv_obj_del(obj);
}

static void timer_out_cb(lv_timer_t* timer)
{
    lv_timer_pause(timer);
    lv_timer_del(timer);

    TB_MenuDelete(&menucfgSelect);
	
    API_PublicInfo_Write_Bool("OnInstaller",0);
	#if 0
    MainMenu_Close();

    MainMenu_Init();
	#else
	API_SettingMenuClose();
        //vtk_lvgl_lock();
	MainMenuLeave_InstallerMode();
        //vtk_lvgl_unlock();
       // MainHeaderShow();
	#endif
}

/*****************

 * **************/
static void confirm_click_event(lv_event_t* e)
{
    char* name = lv_event_get_user_data(e); 
    printf("name = [%s]\n",name);

    API_Para_Write_String("UI_Documents",name);
    LV_API_TIPS("Menu restart, please wait!",1);
    lv_timer_create(timer_out_cb,200,NULL);         
}


lv_obj_t* MenuCfg_preview_init(char* name)
{
    char bg_name[200];
	const char* MENU_CFG_DIR[] = {MENU_CONFIG_SD_PATH, MENU_CONFIG_CUS_PATH, MENU_CONFIG_FIX_PATH, NULL};
	int index;
	for(index = 0; MENU_CFG_DIR[index]; index++)
       {
                //snprintf(buf, 500, "%s/%s/%s", MENU_CFG_DIR[index], INFO_RES_DIR_NAME,pic);
                sprintf(bg_name,"S:%s/%s.bin",MENU_CFG_DIR[index],name);
                if(IsFileExist(bg_name+2))
                {
                    break;
                }
        }
    lv_obj_t* displaymenu = lv_obj_create(lv_scr_act());
    lv_obj_set_size(displaymenu, 490, 810);
    lv_obj_align(displaymenu, LV_ALIGN_TOP_MID, 5, -5);
    lv_obj_set_style_pad_all(displaymenu, 0, 0);
    lv_obj_clear_flag(displaymenu, LV_OBJ_FLAG_SCROLLABLE);

  	
   // sprintf(bg_name,"S:%s/%s.bin",cnt_dir,name);
    printf("------------  bg_name = [%s] -----------\n",bg_name);
    
    lv_obj_t* bkgd_img = lv_img_create(displaymenu);
	lv_img_set_src(bkgd_img, bg_name);

    lv_obj_t* pageControl = lv_obj_create(displaymenu);
    lv_obj_set_size(pageControl, 480, 100);
    lv_obj_set_style_pad_all(pageControl, 0, 0);
    lv_obj_set_y(pageControl, 700);
    set_font_size(pageControl,1);

    lv_obj_t* leftBtn = lv_btn_create(pageControl);    
    lv_obj_align(leftBtn, LV_ALIGN_LEFT_MID, 20, 0);
    lv_obj_set_size(leftBtn, 150, 80);
    lv_obj_add_event_cb(leftBtn, cancel_click_event, LV_EVENT_CLICKED, displaymenu);
    lv_obj_set_style_radius(leftBtn, LV_RADIUS_CIRCLE, 0);
    lv_obj_t* leftlabel = lv_label_create(leftBtn);
    lv_label_set_text(leftlabel, LV_VTK_BACKSPACE);
    lv_obj_center(leftlabel);
    set_font_size(leftlabel,1);

    lv_obj_t* rightBtn = lv_btn_create(pageControl);
    lv_obj_align(rightBtn, LV_ALIGN_CENTER, 150, 0);
    lv_obj_set_size(rightBtn, 150, 80);
    lv_obj_add_event_cb(rightBtn, confirm_click_event, LV_EVENT_CLICKED, name);
    lv_obj_set_style_radius(rightBtn, LV_RADIUS_CIRCLE, 0);
    lv_obj_t* rightlabel = lv_label_create(rightBtn);
    lv_label_set_text(rightlabel, LV_VTK_CONFIM);
    lv_obj_center(rightlabel);
    set_font_size(rightlabel,1);

    return displaymenu;
}

