/**
  ******************************************************************************
  * @file    obj_SearchListByInput.h
  * @author  lzh
  * @version V00.01.00
  * @date    2018.10.27
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 
#include <pthread.h>

//搜索结果回调函数
typedef void (*SearchResultCallback)(const char* intput, const cJSON* resRecord);

typedef struct
{
    pthread_mutex_t	lock;
    char input[21];
    SearchResultCallback callback;
}SEARCH_LIST;



void SearchListByInput(char* intput, SearchResultCallback callback);
void SearchListStop(void);
