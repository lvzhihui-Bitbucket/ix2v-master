#include "MainPage_digtal.h"
#include "MainPage_code.h"
#include "obj_lvgl_msg.h"
#include "obj_SearchListByInput.h"

static void digtal_keyboard_click_event(lv_event_t* e);

PAGE_INS* digital_menu_creat(lv_obj_t* parent)
{
    PAGE_INS* ppage = (PAGE_INS*)lv_mem_alloc(sizeof(PAGE_INS));
    lv_memset_00(ppage, sizeof(PAGE_INS));

    ppage->page_icons = cJSON_CreateObject();

    if (ppage == NULL)
        return NULL;

    lv_obj_t* digitalCont = lv_obj_create(parent);
    lv_obj_remove_style_all(digitalCont);
    lv_obj_set_size(digitalCont, 480, 800);
    ppage->page_obj = digitalCont;

    lv_obj_t* bkgd_img = lv_img_create(digitalCont);
    lv_img_set_src(bkgd_img, "S:/customer/nand1-1/App/res/bg.bin");
    lv_obj_clear_flag(digitalCont, LV_OBJ_FLAG_SCROLLABLE);
    ppage->page_bkgd =  bkgd_img;

    digital_keyboard_creat(ppage);

    return ppage;
}

lv_obj_t* digital_keyboard_creat(PAGE_INS* ppage)
{
    lv_obj_t* keyCont = lv_obj_create(ppage->page_obj);
    lv_obj_set_size(keyCont, LV_PCT(100), 720);
    lv_obj_set_y(keyCont,80);
    lv_obj_set_style_border_side(keyCont, 0, 0);
    lv_obj_set_style_bg_opa(keyCont,LV_OPA_MIN,0);

    lv_obj_t* roomNum = lv_label_create(keyCont);
    lv_obj_set_style_text_font(roomNum, &lv_font_montserrat_48, 0);
    lv_obj_align(roomNum, LV_ALIGN_TOP_MID, 0, 100);
    lv_label_set_text(roomNum, "");
    lv_obj_set_style_text_color(roomNum,lv_color_white(),0);

    cJSON_AddNumberToObject(ppage->page_icons, DIGTAL_NUM, (int)roomNum);

    lv_obj_t* num1 = add_radiobutton(keyCont, "1", digtal_keyboard_click_event,roomNum);
    lv_obj_align(num1, LV_ALIGN_TOP_MID, -140, 330);

    lv_obj_t* num2 = add_radiobutton(keyCont, "2", digtal_keyboard_click_event,roomNum);
    lv_obj_align(num2, LV_ALIGN_TOP_MID, 0, 330);

    lv_obj_t* num3 = add_radiobutton(keyCont, "3", digtal_keyboard_click_event,roomNum);
    lv_obj_align(num3, LV_ALIGN_TOP_MID, 140, 330);

    lv_obj_t* num4 = add_radiobutton(keyCont, "4", digtal_keyboard_click_event,roomNum);
    lv_obj_align(num4, LV_ALIGN_TOP_MID, -140, 420);

    lv_obj_t* num5 = add_radiobutton(keyCont, "5", digtal_keyboard_click_event,roomNum);
    lv_obj_align(num5, LV_ALIGN_TOP_MID, 0, 420);

    lv_obj_t* num6 = add_radiobutton(keyCont, "6", digtal_keyboard_click_event,roomNum);
    lv_obj_align(num6, LV_ALIGN_TOP_MID, 140,420);

    lv_obj_t* num7 = add_radiobutton(keyCont, "7", digtal_keyboard_click_event,roomNum);
    lv_obj_align(num7, LV_ALIGN_TOP_MID, -140, 510);

    lv_obj_t* num8 = add_radiobutton(keyCont, "8", digtal_keyboard_click_event,roomNum);
    lv_obj_align(num8, LV_ALIGN_TOP_MID, 0, 510);

    lv_obj_t* num9 = add_radiobutton(keyCont, "9", digtal_keyboard_click_event,roomNum);
    lv_obj_align(num9, LV_ALIGN_TOP_MID, 140, 510);

    lv_obj_t* numback = add_radiobutton(keyCont, LV_VTK_BACKSPACE, digtal_keyboard_click_event,roomNum);
    lv_obj_align(numback, LV_ALIGN_TOP_MID, -140, 600);
    lv_obj_set_style_text_color(numback,lv_color_make(150,61,53),0);

    lv_obj_t* num0 = add_radiobutton(keyCont, "0", digtal_keyboard_click_event,roomNum);
    lv_obj_align(num0, LV_ALIGN_TOP_MID, 0, 600);

    lv_obj_t* numcall = add_radiobutton(keyCont, LV_SYMBOL_CALL, digtal_keyboard_click_event,roomNum);
    lv_obj_align(numcall, LV_ALIGN_TOP_MID, 140, 600);
    lv_obj_set_style_text_color(numcall,lv_color_make(46,204,127),0);
    //lv_obj_set_style_bg_color(numcall, lv_color_make(163, 219, 129), 0);
    //lv_obj_set_style_bg_opa(numcall, LV_OPA_COVER, 0);

    return keyCont;
}

lv_obj_t* digital_control_creat(lv_obj_t* parent)
{
    lv_obj_t* cont = lv_obj_create(parent);
    lv_obj_set_size(cont, LV_PCT(100), 100);
    lv_obj_set_y(cont,700);
    lv_obj_set_style_border_side(cont, 0, 0);
    lv_obj_set_style_bg_opa(cont,LV_OPA_MIN,0);

    return cont;
}

lv_obj_t* Img_add_callbtn(lv_obj_t* parent, struct ICON_INS* picon)
{
    cJSON* txt = NULL;
    cJSON* size = NULL;
    cJSON* color = NULL;
    cJSON* pos = cJSON_GetObjectItem(picon->icon_data, get_global_key_string(GLOBAL_ICON_KEYS_POS));
    cJSON* text = cJSON_GetObjectItem(picon->icon_data, get_global_key_string(GLOBAL_ICON_KEYS_TEXT));

    if(text){
        txt = cJSON_GetObjectItem(text,get_global_key_string(GLOBAL_ICON_KEYS_LABEL_TEXT));
        size = cJSON_GetObjectItem(text,get_global_key_string(GLOBAL_ICON_KEYS_TEXT_SIZE));
        color = cJSON_GetObjectItem(text,get_global_key_string(GLOBAL_ICON_KEYS_TEXT_COLOR));
    }

    lv_obj_t* icon = creat_cont_for_postion(parent, pos);
    lv_obj_set_style_text_opa(icon,0,0);
    picon->icon_cont = icon;
    lv_obj_t* label = lv_label_create(icon);
    if(size)
        set_font_size(label,size->valueint);
    if(txt){
        char* name = get_symbol_key(txt->valuestring);
        lv_label_set_text(label, name);
    }else{
        lv_label_set_text(label, "");
    }       
    if(color)
        lv_obj_set_style_text_color(label,lv_color_hex(color->valueint),0);  
    lv_obj_center(label);

    lv_obj_add_event_cb(icon, digtal_keyboard_click_event, LV_EVENT_CLICKED, NULL);

    return icon;
}

void* SearchResult(const char* intput, const cJSON* resRecord)
{
    //printf("intput = [%s]\n",intput);
    //MyPrintJson("[resRecord]", resRecord);
    if(resRecord){
        cJSON* res = cJSON_GetObjectItem(resRecord,"IX_NAME");
        if(res)
            MainMenuUpdate_Tips(MAIN_PAGE_DIGTAL,DIGITAL_TIPS,res->valuestring,0);
    }
}

static void digtal_keyboard_click_event(lv_event_t* e)
{
    lv_obj_t* obj = lv_event_get_target(e);
    lv_obj_t* label = lv_obj_get_child(obj, 0);
    lv_obj_t* roomNum = lv_event_get_user_data(e);
    char* text = lv_label_get_text(label);
	char update_buff[100];
	if(strcmp(API_PublicInfo_Read_String("cur_page"),"code")==0)
	{
		if(CodeKpInput_Process(text,update_buff,NULL))
		{
            MainMenuUpdate(MAIN_PAGE_CODE,DIGTAL_NUM,update_buff);
		}
	}
	else
	{
		if(DialKpInput_Process(text,update_buff))
		{
	        MainMenuUpdate(MAIN_PAGE_DIGTAL,DIGTAL_NUM,update_buff);
	        MainMenuUpdate_Tips(MAIN_PAGE_DIGTAL,DIGITAL_TIPS,"Enter call number",0);
            SearchListByInput(update_buff,SearchResult);
		}
	}
}

void Digtal_Keyboard_exit(PAGE_INS* ppage)
{
    if(ppage){
        dele_one_page(ppage);
    }       
}