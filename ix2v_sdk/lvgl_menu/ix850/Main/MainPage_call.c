#include <stdio.h>
#include "task_CallServer.h"
#include "elog_forcall.h"
#include "obj_PublicInformation.h"
#include "task_Event.h"
#include "ix850_icon.h"
#include "obj_lvgl_msg.h"
#include "table_display.h"
#include "define_file.h"
#include "menu_common.h"
#include "obj_IoInterface.h"
#include "task_Beeper.h"
#include "obj_TableSurver.h"
#include "keyboard_test.h"

#include "menu_page.h"
static int sec = 0;
static int min = 0;
void main_reset_callTime(void)
{
 
        //lv_timer_del(timer);
       // timer = NULL;
        sec = 0;
        min = 0;
        //lv_label_set_text(timeLabel, "00:00");
        MainMenuLabelUpdate(MAIN_PAGE_CALL,"time","00:00");
 
}
static void main_update_calltime_cb(lv_timer_t* timer)
{
    char curtime[10];
	
    sprintf(curtime,"%02d:%02d",min,sec);
	//bprintf("11111111 %s\n",curtime);
    //lv_label_set_text(timeLabel, curtime);
    MainMenuLabelUpdate(MAIN_PAGE_CALL,"time",curtime);
    sec++;
    if(sec == 60)
    {
        sec = 0;
        min++; 
    }

}

void main_callNbr_event_cb(void* s, lv_msg_t* m)
{
    //lv_obj_t * label = lv_event_get_target(e);
    //lv_msg_t * m = lv_event_get_msg(e);
	char* state = lv_msg_get_payload(m);
	char *disp;
	char buff[100];
	//static int have_divert=0;
	const char divert_disp[]="(Diverting...)";
	bprintf("2222222 %d\n",lv_msg_get_id(m));
	
    if(lv_msg_get_id(m) == IX850_MSG_CALL_INFO)
    {
    		
		 //lv_label_set_text(label,state);	
		 MainMenuLabelUpdate(MAIN_PAGE_CALL,"info",state);
		return;
	}
    if(lv_msg_get_id(m) == IX850_MSG_CALL_RESULT)
	{
		bprintf("2222222 %s\n",state);
	    if(strcmp(state, "Calling_Busy") == 0)
	    {
            //vtk_label_set_text(label,"Calling_Busy");
		MainMenuLabelUpdate(MAIN_PAGE_CALL,"state","Calling_Busy");
	    }
	    else if(strcmp(state, "Calling_Succ") == 0)
	    {
                //vtk_label_set_text(label,"Calling_Succ");
                MainMenuLabelUpdate(MAIN_PAGE_CALL,"state","Calling_Succ");
	    }
	    else if(strcmp(state, "Calling_Error") == 0)
	    {
	        //lv_label_set_text(label,"Calling_Error");
            //vtk_label_set_text(label,"Calling_Error");
		MainMenuLabelUpdate(MAIN_PAGE_CALL,"state","Calling_Error");	

	    }	
	    else if(strcmp(state, "Calling_Cancel") == 0)
	    {
			
	    }
	    else if(strcmp(state, "Calling_Finish") == 0)
	    {
	        main_reset_callTime();
		
	    }
		else if(strcmp(state, "Calling_Talk") == 0)
	    {
	        main_reset_callTime();
            MainMenuLabelUpdate(MAIN_PAGE_CALL,"state","Calling_Talk");	
		MainMenuUpdate(MAIN_PAGE_CALL,"divert","");	
			
	    }
		else if(strcmp(state, "Phone_Talk") == 0)
	    {
	        main_reset_callTime();
		
            //vtk_label_set_text(label,"Calling_Talk");
			
		 MainMenuLabelUpdate(MAIN_PAGE_CALL,"state","Calling_Talk");
		MainMenuUpdate(MAIN_PAGE_CALL,"divert","\xEE\x9A\xBD");
	    }
	 return;
    }
	 if(lv_msg_get_id(m) == IX850_MSG_DIVERT_INFO)
	 {
	 	//disp=lv_label_get_text(label);
	 	if(strcmp(state, "ON") == 0)
	 	{
	 		//lv_obj_clear_flag(call_menu_divert_img, LV_OBJ_FLAG_HIDDEN);
	 		MainMenuUpdate(MAIN_PAGE_CALL,"divert","\xEE\x9A\xBD");
	 	}
		else
		{
			//lv_obj_add_flag(call_menu_divert_img, LV_OBJ_FLAG_HIDDEN);
			MainMenuUpdate(MAIN_PAGE_CALL,"divert","");
		}
		
	 }
}
void init_main_call_text(void)
{
	
	 MainMenuLabelUpdate(MAIN_PAGE_CALL,"info","");
	 char  *ser_type=API_PublicInfo_Read_String(PB_CALL_SER_TYPE);
	 if(ser_type&&strcmp("VMSipCall",ser_type)==0&&strcmp(API_PublicInfo_Read_String(PB_CALL_SER_STATE),"Ring")!=0)
	 {
		MainMenuLabelUpdate(MAIN_PAGE_CALL,"state","Linking...");
	 }
	 else
		MainMenuLabelUpdate(MAIN_PAGE_CALL,"state","Calling_Succ");
	MainMenuUpdate(MAIN_PAGE_CALL,"divert","");
	//if(call_menu_divert_img!=NULL)
	//	lv_obj_add_flag(call_menu_divert_img, LV_OBJ_FLAG_HIDDEN);
}
void main_call_page_init(void)
{
	static lv_timer_t *call_timer=NULL;
	if(call_timer==NULL)
	{
		lv_msg_subsribe(IX850_MSG_CALL_INFO, main_callNbr_event_cb, NULL);
		lv_msg_subsribe(IX850_MSG_CALL_RESULT, main_callNbr_event_cb, NULL);
		lv_msg_subsribe(IX850_MSG_DIVERT_INFO, main_callNbr_event_cb, NULL);
		
	   	call_timer= lv_timer_create(main_update_calltime_cb, 1000, NULL);	
	}
}
