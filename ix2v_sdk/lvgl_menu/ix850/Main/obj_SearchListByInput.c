
/**
  ******************************************************************************
  * @file    obj_SearchListByInput.c
  * @author  lzh
  * @version V00.01.00
  * @date    2018.10.27
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 


#include <sys/sysinfo.h>
#include "cJSON.h"
#include "obj_SearchListByInput.h"
#include "obj_TableSurver.h"
#include "define_string.h"

static SEARCH_LIST searchList = {.lock = PTHREAD_MUTEX_INITIALIZER};


static int SearchListTimerCallback(int time)
{
	pthread_mutex_lock(&searchList.lock);
	if(searchList.callback)
	{
		//cJSON* Where = cJSON_CreateObject();
		cJSON* view = NULL;//cJSON_CreateArray();
		cJSON* record = NULL;
		//cJSON_AddStringToObject(Where, IX2V_L_NBR, searchList.input);
		int meetCnt; //= API_TB_SelectBySortByName(TB_NAME_HYBRID_MAT, view, Where, 0);
		view=R8001_G_L_NBRSearch(searchList.input);
		meetCnt	=cJSON_GetArraySize(view);
		if(meetCnt == 1)
		{
			record = cJSON_GetArrayItem(view, 0);
		}
		else if(meetCnt > 1)
		{
			cJSON_ArrayForEach(record, view)
			{
				if(!strcmp(GetEventItemString(record, IX2V_L_NBR), searchList.input))
				{
					break;
				}
			}
		}
		if(record && searchList.callback)
		{
			(searchList.callback)(searchList.input, record);
		}
		
		//cJSON_Delete(Where);
		if(view)
			cJSON_Delete(view);
	}
	pthread_mutex_unlock(&searchList.lock);

	return 2;
}

void SearchListByInput(char* intput, SearchResultCallback callback)
{
	if(!intput || !strlen(intput))
	{
		return;
	}
	pthread_mutex_lock(&searchList.lock);
	//1秒钟之后开始搜索
	API_Add_TimingCheck(SearchListTimerCallback, 1);
	snprintf(searchList.input, 21, "%s", intput);
	searchList.callback = callback;
	pthread_mutex_unlock(&searchList.lock);
}

void SearchListStop(void)
{
	pthread_mutex_lock(&searchList.lock);
	API_Del_TimingCheck(SearchListTimerCallback);
	searchList.input[0] = 0;
	searchList.callback = NULL;
	pthread_mutex_unlock(&searchList.lock);
}