#include "MainPage_help.h"
#include "obj_lvgl_msg.h"

static void help_click_event_cb(lv_event_t* e);

lv_obj_t* help_menu_creat(void)
{
    lv_obj_t* cont = lv_obj_create(lv_scr_act());
    //lv_obj_set_style_bg_color(cont, lv_color_make(227, 236, 241), 0);
    lv_obj_set_size(cont, 480, 800);
    lv_obj_set_style_bg_img_src(cont,"S:/mnt/nand1-1/App/res/help.png",0);
    lv_obj_add_flag(cont,LV_OBJ_FLAG_CLICKABLE);
    lv_obj_add_event_cb(cont, help_click_event_cb, LV_EVENT_CLICKED, NULL);
    return cont;
}

static void help_click_event_cb(lv_event_t* e)
{
    MainMenu_Process(MAINMENU_Return_MainPage,0); 
}

void help_exit(lv_obj_t* obj)
{
    if(obj)
        lv_obj_del(obj);
}