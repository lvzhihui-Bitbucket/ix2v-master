#include <stdio.h>
#include "task_CallServer.h"
#include "elog_forcall.h"
#include "obj_PublicInformation.h"
#include "task_Event.h"
#include "ix850_icon.h"
#include "obj_lvgl_msg.h"

lv_obj_t * CreateMainPage(char *res_path,lv_obj_t * screen,MENU_INS **menu_ins)
{
	MENU_INS *ins = load_one_menu(res_path, screen);
	if(ins==NULL)
		return NULL;
    int count = cJSON_GetArraySize(ins->menu_pages);
	if(count<=0)
	{
		dele_one_menu(ins);  
		return NULL;
	}
	if(menu_ins!=NULL)
    		*menu_ins=ins;
        cJSON* piconjson = cJSON_GetArrayItem(ins->menu_pages, 0);
        PAGE_INS* ppage = (PAGE_INS*)piconjson->valueint;
		
        return ppage->page_obj;
 
}
void DeleteMainPage(MENU_INS *menu_ins)
{
	if(menu_ins!=NULL)
		dele_one_menu(menu_ins);
}