#ifndef IX850_HEADER_H_
#define IX850_HEADER_H_

#include "lv_ix850.h"
#include "menu_page.h"

lv_obj_t* Img_add_header(lv_obj_t* parent, struct ICON_INS* picon);
static void IX850_update_time_cb(lv_timer_t* timer);
lv_obj_t* add_header_installer(lv_obj_t* parent);

#endif 
