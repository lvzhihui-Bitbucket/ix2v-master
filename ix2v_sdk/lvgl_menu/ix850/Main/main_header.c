#include "main_header.h"
#include "icon_common.h"
#include "obj_lvgl_msg.h"
#include "obj_IoInterface.h"

#define IX850_TIME_UPDATE 23042610901

lv_obj_t* net_text;
lv_obj_t* lock_text1;
lv_obj_t* lock_text2;
lv_obj_t* xd_text;
lv_obj_t* sip_text;
lv_timer_t *header_timer=NULL;
void *header_msg_handle=NULL;
//lv_obj_t* status;

lv_obj_t* add_header(lv_obj_t* parent);
static void IX850_update_symbol_cb(lv_timer_t* timer);
void IX850_update_symbol(void);
static void update_time_event(lv_event_t* e);


void edit_common_click_event(lv_event_t* e)
{
    //MainMenu_Process(MAINMENU_COMMON_CLICK,"installer_bar_l");
    lv_obj_t* ta = lv_event_get_target(e);
    if(API_PublicInfo_Read_Bool("MenuEdit"))
    {
        lv_obj_set_style_text_color(ta,lv_color_white(),0);
        API_PublicInfo_Write_Bool("MenuEdit",false);
    }
    else
    {
        lv_obj_set_style_text_color(ta,lv_color_black(),0);
        API_PublicInfo_Write_Bool("MenuEdit",true);
    }
}

void setting_common_click_event(lv_event_t* e)
{
    MainMenu_Process(MAINMENU_COMMON_CLICK,"installer_bar_m");
}

void wizard_common_click_event(lv_event_t* e)
{   
    MainMenu_Process(MAINMENU_COMMON_CLICK,"installer_bar_r"); 
}

lv_obj_t* Img_add_header(lv_obj_t* parent, struct ICON_INS* picon)
{
    cJSON* pos = cJSON_GetObjectItem(picon->icon_data, get_global_key_string(GLOBAL_ICON_KEYS_POS));
    cJSON* text = cJSON_GetObjectItem(picon->icon_data, get_global_key_string(GLOBAL_ICON_KEYS_TEXT));

    cJSON* txt = cJSON_GetObjectItem(text,get_global_key_string(GLOBAL_ICON_KEYS_LABEL_TEXT));
    cJSON* size = cJSON_GetObjectItem(text,get_global_key_string(GLOBAL_ICON_KEYS_TEXT_SIZE));
    cJSON* color = cJSON_GetObjectItem(text,get_global_key_string(GLOBAL_ICON_KEYS_TEXT_COLOR));

    lv_obj_t* status = creat_cont_for_postion(parent, pos);
    lv_obj_set_style_bg_opa(status,LV_OPA_MAX,0);
    lv_obj_set_style_text_color(status, lv_color_white(), 0);
    lv_obj_clear_flag(status,LV_OBJ_FLAG_SCROLLABLE);
    set_font_size(status,2);
    lv_obj_set_style_bg_color(status,lv_color_make(19,132,250),0);

    lv_obj_t* time_text = lv_label_create(status); 
	lv_obj_align(time_text, LV_ALIGN_CENTER, 0, 0);
    lv_label_set_text(time_text, LV_VTK_SETTING);
	lv_obj_add_flag(time_text, LV_OBJ_FLAG_CLICKABLE);
	lv_obj_add_event_cb(time_text, setting_common_click_event, LV_EVENT_CLICKED, NULL);

    lv_obj_t* w_text = lv_label_create(status); 
	lv_obj_align(w_text, LV_ALIGN_RIGHT_MID, -20, 0);
    lv_label_set_text(w_text, LV_VTK_WIZARD);
	lv_obj_add_flag(w_text, LV_OBJ_FLAG_CLICKABLE);
	lv_obj_add_event_cb(w_text, wizard_common_click_event, LV_EVENT_CLICKED, NULL);

    lv_obj_t* e_text = lv_label_create(status); 
	lv_obj_align(e_text, LV_ALIGN_LEFT_MID, 20, 0);
    lv_label_set_text(e_text, LV_VTK_EDIT);
	lv_obj_add_flag(e_text, LV_OBJ_FLAG_CLICKABLE);
	lv_obj_add_event_cb(e_text, edit_common_click_event, LV_EVENT_CLICKED, NULL);
#if 0
    lv_obj_t* label = lv_label_create(icon);
    lv_obj_center(label);
    if(size)
        set_font_size(label,size->valueint);
    if(color)
        lv_obj_set_style_text_color(label,lv_color_hex(color->valueint),0); 

    char* curtime = IX850_get_current_time();
    lv_label_set_text(label, curtime);
    lv_mem_free(curtime);

    if(header_timer != NULL){
        lv_timer_del(header_timer);
		header_timer = NULL;
    }        
    header_timer = lv_timer_create(IX850_update_time_cb, 60000, NULL);
#endif
    picon->icon_cont = status;
    return status;
}

void common_click_event(lv_event_t* e);
static lv_obj_t* head_min_obj=NULL;
static lv_obj_t* head_right_obj=NULL;
static lv_obj_t* head_left_obj=NULL;

lv_obj_t* add_header(lv_obj_t* parent)
{
	static char header_min[]="header_min";
	static char header_left[]="header_left";
	static char header_right[]="header_right";
    lv_obj_t* status = lv_obj_create(parent);

    lv_obj_set_size(status, 480, 70);   
    lv_obj_set_style_border_side(status, 0 , 0);
    lv_obj_align(status,LV_ALIGN_TOP_MID,0 , 0);

    lv_obj_set_style_bg_opa(status,LV_OPA_MIN,0);
    lv_obj_set_style_text_color(status, lv_color_white(), 0);
    lv_obj_clear_flag(status,LV_OBJ_FLAG_SCROLLABLE);

    lv_obj_t* time_text = lv_label_create(status);
	header_timer = lv_timer_create(IX850_update_time_cb, 30000, time_text);
    set_font_size(time_text,1);
	
	lv_obj_set_flex_grow(time_text, 12);
	lv_obj_align(time_text, LV_ALIGN_CENTER, 0, 0);

    IX850_update_time_cb(header_timer);
    if(header_msg_handle)
    {
        lv_msg_unsubscribe(header_msg_handle);
        header_msg_handle = NULL;
    }
    header_msg_handle = lv_msg_subsribe_obj(IX850_TIME_UPDATE, time_text, NULL);
    lv_obj_add_event_cb(time_text, update_time_event, LV_EVENT_ALL, NULL);
   
	lv_obj_set_style_bg_color(status,lv_color_make(0,0,200),0);
	lv_obj_add_flag(time_text, LV_OBJ_FLAG_CLICKABLE);
	lv_obj_add_event_cb(time_text, common_click_event, LV_EVENT_CLICKED, header_min);
	head_min_obj=time_text;
    return status;
}

lv_obj_t* add_header_installer(lv_obj_t* parent)
{
    lv_obj_t* status = lv_obj_create(parent);
    lv_obj_set_size(status, 480, 70);   
    lv_obj_set_style_border_side(status, 0 , 0);
    lv_obj_align(status,LV_ALIGN_CENTER,0 , 0);

    lv_obj_set_style_bg_opa(status,LV_OPA_MAX,0);
    lv_obj_set_style_text_color(status, lv_color_white(), 0);
    lv_obj_clear_flag(status,LV_OBJ_FLAG_SCROLLABLE);
    set_font_size(status,2);
    lv_obj_set_style_bg_color(status,lv_color_make(19,132,250),0);

    lv_obj_t* time_text = lv_label_create(status); 
	lv_obj_align(time_text, LV_ALIGN_CENTER, 0, 0);
    lv_label_set_text(time_text, LV_VTK_SETTING);
	lv_obj_add_flag(time_text, LV_OBJ_FLAG_CLICKABLE);
	lv_obj_add_event_cb(time_text, setting_common_click_event, LV_EVENT_CLICKED, NULL);

    lv_obj_t* w_text = lv_label_create(status); 
	lv_obj_align(w_text, LV_ALIGN_RIGHT_MID, 10, 0);
    lv_label_set_text(w_text, LV_VTK_WIZARD);
	lv_obj_add_flag(w_text, LV_OBJ_FLAG_CLICKABLE);
	lv_obj_add_event_cb(w_text, wizard_common_click_event, LV_EVENT_CLICKED, NULL);

    lv_obj_t* e_text = lv_label_create(status); 
	lv_obj_align(e_text, LV_ALIGN_LEFT_MID, -10, 0);
    lv_label_set_text(e_text, LV_VTK_EDIT);
	lv_obj_add_flag(e_text, LV_OBJ_FLAG_CLICKABLE);
	lv_obj_add_event_cb(e_text, edit_common_click_event, LV_EVENT_CLICKED, NULL);

    return status;
}

#if 0
void api_header_open_installer(void)
{
	lv_label_set_text(head_right_obj, "2");
	lv_label_set_text(head_left_obj, "1");
}
void api_header_close_installer(void)
{
	lv_label_set_text(head_right_obj, "");
	lv_label_set_text(head_left_obj, "");
}
#endif
void del_header(lv_obj_t* header)
{
	if(header_timer)
	{
		lv_timer_del(header_timer);
		header_timer = NULL;
	}
    
    if(header_msg_handle)
    {
        lv_msg_unsubscribe(header_msg_handle);
        header_msg_handle = NULL;
    }
}
static void IX850_update_symbol_cb(lv_timer_t* timer)
{
    IX850_update_symbol();
}

void IX850_update_symbol(void)
{
    int headerstate = 2;
    switch (headerstate)
    {
    case 1:
        //lv_obj_set_style_bg_color(status, lv_color_black(), 0);
        break;
    case 2:
        //lv_obj_set_style_bg_color(status, lv_color_make(27, 177, 238), 0);
        break;
    default:
        break;
    }
    int netstate = 3;
    switch (netstate)
    {
    case 1:
        lv_label_set_text(net_text, LV_VTK_SERVER);
        break;
    case 2:
        lv_label_set_text(net_text, LV_SYMBOL_WIFI);
        break;
    case 3:
        lv_label_set_text(net_text, LV_VTK_SIGNAL);
        break;
    default:
        break;
    }

    int lockstate1 = 1;
    if (lockstate1)
    {
        lv_obj_clear_flag(lock_text1, LV_OBJ_FLAG_HIDDEN);
    }
    else
    {
        lv_obj_add_flag(lock_text1, LV_OBJ_FLAG_HIDDEN);
    }

    int lockstate2 = 1;
    if (lockstate1)
    {
        lv_obj_clear_flag(lock_text2, LV_OBJ_FLAG_HIDDEN);
    }
    else
    {
        lv_obj_add_flag(lock_text2, LV_OBJ_FLAG_HIDDEN);
    }

    int xdstate = 1;
    if (lockstate1)
    {
        lv_obj_clear_flag(xd_text, LV_OBJ_FLAG_HIDDEN);
    }
    else
    {
        lv_obj_add_flag(xd_text, LV_OBJ_FLAG_HIDDEN);
    }

    int sipstate = 3;
    switch (sipstate)
    {
    case 1:
        lv_label_set_text(sip_text, LV_VTK_CLOUD_ABNORMAL);
        break;
    case 2:
        lv_label_set_text(sip_text, LV_VTK_CLOUD_OFFLINE);
        break;
    case 3:
        lv_label_set_text(sip_text, LV_VTK_CLOUD_ONLINE);
        break;
    default:
        break;
    }
}
#define SIP_OFFLINE_FONT      "\xEE\x98\x93"
#define SIP_ONLINE_FONT      "\xEE\x98\x8F"
#define SIP_ABNORMAL_FONT     "\xEE\x98\x91"
void IX850_update_Sip_symbol(void)
{
	
	if(sip_text==NULL)
		return;
	if(!API_Para_Read_Int(SIP_ENABLE))
	{
		 lv_label_set_text(sip_text, "");
		 return;
	}
	 int sipstate = Get_SipSer_State();
	    switch (sipstate)
	    {
		    case 1:
		        lv_label_set_text(sip_text, LV_VTK_CLOUD_ONLINE);
		        break;
		    case 2:
		        lv_label_set_text(sip_text, LV_VTK_CLOUD_ABNORMAL);
		        break;
		    case 0:
		        lv_label_set_text(sip_text, LV_VTK_CLOUD_OFFLINE);
		        break;
		    default:
		        break;
	    }
}
static void IX850_update_time_cb(lv_timer_t* timer)
{
	char text_buff[100];
    char* curtime = IX850_get_current_time();

    //MainMenuUpdate(MAIN_PAGE_MAIN,MAIN_HEADER,curtime);
    if(API_Para_Read_Int(SIP_ENABLE))
    {
	    int sipstate = Get_SipSer_State();
	    switch (sipstate)
	    {
		    case 1:
			sprintf(text_buff,"%s  %s",curtime,SIP_ONLINE_FONT);
				//lv_label_set_text(sip_text, LV_VTK_CLOUD_ONLINE);
		        break;
		    case 2:
		        //lv_label_set_text(sip_text, LV_VTK_CLOUD_ABNORMAL);
		        sprintf(text_buff,"%s  %s",curtime,SIP_ABNORMAL_FONT);
		        break;
		    case 0:
		        //lv_label_set_text(sip_text, LV_VTK_CLOUD_OFFLINE);
		        sprintf(text_buff,"%s  %s",curtime,SIP_OFFLINE_FONT);
		        break;
		    default:
		        break;
	    }
    }
	else
	{
		strcpy(text_buff,curtime);
	}
	lv_label_set_text(timer->user_data,text_buff);
    lv_mem_free(curtime);
	//IX850_update_Sip_symbol();

}

static void update_time_event(lv_event_t* e)
{
    lv_event_code_t code = lv_event_get_code(e);
    lv_obj_t* label = lv_event_get_target(e);
    char* payload = NULL;
#if 1
    if (code == LV_EVENT_MSG_RECEIVED) {
        lv_msg_t* m = lv_event_get_msg(e);
        switch (lv_msg_get_id(m)) {
        case IX850_TIME_UPDATE:
            payload = lv_msg_get_payload(m);
            lv_label_set_text(label, payload);
            break;
        }
    }
#endif // 0
}
