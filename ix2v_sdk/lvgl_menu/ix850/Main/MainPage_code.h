#ifndef MAINPAGE_CODE_h
#define MAINPAGE_CODE_h

#include "lv_ix850.h"
#include "menu_page.h"

PAGE_INS* code_menu_creat(lv_obj_t* parent);
lv_obj_t* add_radiobutton(lv_obj_t* parent, const char* txt, lv_event_cb_t cb,lv_obj_t* target);
void Code_Keyboard_exit(PAGE_INS* ppage);
lv_obj_t* code_keyboard_creat(PAGE_INS* ppage);
lv_obj_t* code_control_creat(lv_obj_t* parent);
lv_obj_t* Img_add_codebtn(lv_obj_t* parent, struct ICON_INS* picon);
lv_obj_t* Img_add_password(lv_obj_t* parent, struct ICON_INS* picon);

#endif