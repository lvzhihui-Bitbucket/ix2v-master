#ifndef MAINPAGE_HELP_h
#define MAINPAGE_HELP_h

#include "lv_ix850.h"
#include "menu_page.h"

lv_obj_t* help_menu_creat(void);
void help_exit(lv_obj_t* obj);
#endif