#ifndef _MainList_h
#define _MainList_h

#include "lv_ix850.h"
#include "menu_page.h"

void lv_msg_NamelistUpdate_Callback(void* s, lv_msg_t* m);
void lv_msg_NamelistReload_Callback(void* s, lv_msg_t* m);
lv_obj_t*CreateMainRoomlist(void);
lv_obj_t*CreateMainNamelist(void);
lv_obj_t* Img_add_list(lv_obj_t* parent, struct ICON_INS* picon);
lv_obj_t* Img_add_btn(lv_obj_t* parent, struct ICON_INS* picon);
static void MainRoomlist_jump(lv_event_t* e);
static void MainRoomlist_back(lv_event_t* e);
static void MainNamelist_jump(lv_event_t* e);
static void MainNamelist_back(lv_event_t* e);

#endif
