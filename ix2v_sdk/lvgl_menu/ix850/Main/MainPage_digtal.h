#ifndef MAINPAGE_DIGTAL_h
#define MAINPAGE_DIGTAL_h

#include "lv_ix850.h"
#include "menu_page.h"

PAGE_INS* digital_menu_creat(lv_obj_t* parent);
void Digtal_Keyboard_exit(PAGE_INS* ppage);
lv_obj_t* digital_keyboard_creat(PAGE_INS* parent);
lv_obj_t* digital_control_creat(lv_obj_t* parent);
lv_obj_t* Img_add_callbtn(lv_obj_t* parent, struct ICON_INS* picon);
#endif