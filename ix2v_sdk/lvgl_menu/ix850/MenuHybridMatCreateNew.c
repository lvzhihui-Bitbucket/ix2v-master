#include "menu_utility.h"
#include "MenuHybridMatCreateNew.h"
#include "cJSON.h"
#include "obj_TableSurver.h"
#include "define_string.h"
#include "obj_PublicInformation.h"
#include "utility.h"
#include "task_Event.h"
#include "define_file.h"
#ifdef IX_LINK
#define RES_AUTO_DXG_START  		"IXG START"
#define RES_AUTO_DXG_END  			"IXG END"
#else
#define RES_AUTO_DXG_START  		"DXG START"
#define RES_AUTO_DXG_END  			"DXG END"
#endif

#define RES_AUTO_DT_IM_START  		"DT-IM START"
#define RES_AUTO_DT_IM_END  		"DT-IM END"
#define RES_AUTO_L_NBR_START  		"L_NBR START"
#define RES_AUTO_G_NBR_PREFIX  		"G_NBR PREFIX"
#define RES_AUTO_NAME_PREFIX  		"NAME PREFIX"
#define RES_AUTO_RES_DESC  			"Describe"
#ifdef IX_LINK
#define RES_AUTO_RECORD  "{\"IXG START\":1,\"IXG END\":8,\"DT-IM START\":1,\"DT-IM END\":8,\"L_NBR START\":\"01\",\"G_NBR PREFIX\":\"1\",\"NAME PREFIX\":\"IM_\",\"Describe\":\"List auto\"}" //编辑的记录
#define RES_AUTO_PARA  "{\"Check\":{\"IXG START\":\"(^[1-9]{1}$)|(^(1|2)[0-9]{1}$)|(^3[0-2]{1}$)\", \"IXG END\":\"(^[1-9]{1}$)|(^(1|2)[0-9]{1}$)|(^3[0-2]{1}$)\", \"DT-IM START\":\"(^[1-9]{1}$)|(^(1|2)[0-9]{1}$)|(^3[0-2]{1}$)\",\"DT-IM END\":\"(^[1-9]{1}$)|(^(1|2)[0-9]{1}$)|(^3[0-2]{1}$)\",\"L_NBR START\":\"^[0-9]{0,10}$\", \"G_NBR PREFIX\":\"^[0-9]{0,10}$\"},\"Translate\":{}}" //编辑的记录校验
#define RES_AUTO_EDITOR_EdI_TB  "[\"IXG START\",\"IXG END\",\"DT-IM START\",\"DT-IM END\",\"L_NBR START\",\"G_NBR PREFIX\",\"NAME PREFIX\",\"Describe\"]"	
#else
#define RES_AUTO_RECORD  "{\"DXG START\":1,\"DXG END\":8,\"DT-IM START\":1,\"DT-IM END\":8,\"L_NBR START\":\"01\",\"G_NBR PREFIX\":\"1\",\"NAME PREFIX\":\"IM_\",\"Describe\":\"List auto\"}" //编辑的记录
#define RES_AUTO_PARA  "{\"Check\":{\"DXG START\":\"(^[1-9]{1}$)|(^(1|2)[0-9]{1}$)|(^3[0-2]{1}$)\", \"DXG END\":\"(^[1-9]{1}$)|(^(1|2)[0-9]{1}$)|(^3[0-2]{1}$)\", \"DT-IM START\":\"(^[1-9]{1}$)|(^(1|2)[0-9]{1}$)|(^3[0-2]{1}$)\",\"DT-IM END\":\"(^[1-9]{1}$)|(^(1|2)[0-9]{1}$)|(^3[0-2]{1}$)\",\"L_NBR START\":\"^[0-9]{0,10}$\", \"G_NBR PREFIX\":\"^[0-9]{0,10}$\"},\"Translate\":{}}" //编辑的记录校验
#define RES_AUTO_EDITOR_EdI_TB  "[\"DXG START\",\"DXG END\",\"DT-IM START\",\"DT-IM END\",\"L_NBR START\",\"G_NBR PREFIX\",\"NAME PREFIX\",\"Describe\"]"
#endif
static AUTO_RES_MENU_T autoResMenu = {.resRecord = NULL, .recordCtrl = NULL};

static int AutoClearAndCreateHybridMat(const cJSON* para)
{
	char tempString[100];
	cJSON* view = AutoCreateNewR8001(para, 256);
	cJSON* record;
	int resRecords = 0;

	if(cJSON_GetArraySize(view))
	{
		API_TB_DeleteByName(TB_NAME_HYBRID_MAT, NULL);
		cJSON_ArrayForEach(record, view)
		{
			if(API_TB_AddByName(TB_NAME_HYBRID_MAT, record))
			{
				resRecords++;
			}
		}
		API_Event_By_Name(Event_MAT_Update);
	}

	cJSON_Delete(view);

	return resRecords;
}

static void ResCreateNewCancel(const char* type, void* user_data)
{  
	CreateNewRes_Close();
}

static void ResAutoClearAndCreate(const cJSON* para)
{
	char tempString[200];
	char targetFile[200];
	char translate[100];

	API_TB_SaveByName(TB_NAME_HYBRID_MAT);
	MakeDir(Backup_HYBRID_MAT_Folder);
	snprintf(targetFile, 200, "%s/HYBRID_MAT_%s.json", Backup_HYBRID_MAT_Folder, GetCurrentTime(tempString, 100, "%Y-%m-%d_%H:%M:%S"));
	CopyFile(TB_HYBRID_MAT_FILE_NAME, targetFile);
	int addRecord = AutoClearAndCreateHybridMat(para);

	snprintf(tempString, 200, "%s %d", get_language_text2("Recreate RES list", translate, 100), addRecord);
	API_TIPS_Ext_Time(tempString, 1);
	API_TableModificationInform(TB_NAME_HYBRID_MAT, GetEventItemString(para, RES_AUTO_RES_DESC));
	API_TB_SaveByName(TB_NAME_HYBRID_MAT);
	CreateNewRes_Close();
} 

static void ResCreateNewFunction(const char* type, void* user_data)
{
    if(!strcmp(type, "Create"))
    {
		API_MSGBOX("About to clear RES and recreate", 2, ResAutoClearAndCreate, autoResMenu.resRecord);
    }
} 

void CreateAutoResMenu(AUTO_RES_MENU_T* autoMenu, const char* record, BtnCallback cancelCallback, BtnCallback btnCallback, const char* title, char* btnString, ...)
{
	char* name;
	//编辑的记录
	autoMenu->resRecord = cJSON_Parse(record);

	//可编辑字段
	cJSON* editorTable =  cJSON_Parse(RES_AUTO_EDITOR_EdI_TB);
	cJSON* resEditordescription = cJSON_Parse(RES_AUTO_PARA);


	cJSON* func = cJSON_CreateObject();
	cJSON* btnname = cJSON_AddArrayToObject(func,"BtnName");

	va_list valist;
	va_start(valist, btnString);
	for(name = btnString; name; name = va_arg(valist, char*))
	{
		cJSON_AddItemToArray(btnname, cJSON_CreateString(name));
	}
	va_end(valist);

	cJSON_AddNumberToObject(func, "BtnCallback", (int)btnCallback);
	cJSON_AddNumberToObject(func, "BackCallback", (int)cancelCallback);

    RC_MenuDisplay(&(autoMenu->recordCtrl), autoMenu->resRecord, editorTable, resEditordescription, func, title);
    cJSON_Delete(editorTable);
    cJSON_Delete(resEditordescription);
    cJSON_Delete(func);
}


cJSON* AutoCreateNewR8001(const cJSON* para, int maxRecordNbr)
{
	cJSON* retView = cJSON_CreateArray();

	int dxgStart, dxgEnd, dtImStart, dtImEnd;
	dxgStart = GetEventItemInt(para,  RES_AUTO_DXG_START);
	dxgEnd = GetEventItemInt(para,  RES_AUTO_DXG_END);
	dtImStart = GetEventItemInt(para,  RES_AUTO_DT_IM_START);
	dtImEnd = GetEventItemInt(para,  RES_AUTO_DT_IM_END);
	int lNbrStart = GetEventItemInt(para,  RES_AUTO_L_NBR_START);
	char temp[100];
	char format[50];
	snprintf(format, 50, "%%0%dd", strlen(GetEventItemString(para,  RES_AUTO_L_NBR_START)));
	char* gNbrPrefix = GetEventItemString(para,  RES_AUTO_G_NBR_PREFIX);
	char* namePrefix = GetEventItemString(para,  RES_AUTO_NAME_PREFIX);
	int i, j;
	int recordNbr = 0;

	if(dxgStart < 1 || dxgStart > 32 || dxgEnd < 1 || dxgEnd > 32)
	{
		return retView;
	}

	for(i = dxgStart; (dxgStart > dxgEnd ? i >= dxgEnd : i <= dxgEnd); (dxgStart > dxgEnd ? i-- : i++))
	{
		for(j = dtImStart; (dtImStart > dtImEnd ? j >= dtImEnd : j <= dtImEnd); (dtImStart > dtImEnd ? j-- : j++))
		{
			char l_Nbr[11];
			snprintf(l_Nbr, 11, format, lNbrStart++);
			cJSON* record = cJSON_CreateObject();
			cJSON_AddNumberToObject(record, IX2V_IM_ID, j);
			snprintf(temp, 100, "%s%s", namePrefix, l_Nbr);
			cJSON_AddStringToObject(record, IX2V_IX_NAME, temp);
			cJSON_AddStringToObject(record, IX2V_L_NBR, l_Nbr);
			cJSON_AddNumberToObject(record, IX2V_GW_ID, i);
			cJSON_AddStringToObject(record, IX2V_IX_ADDR, GW_ID_AND_IM_ID_TO_IX_ADDR(i, j));
			cJSON_AddStringToObject(record, IX2V_GW_ADDR, GW_ID_TO_GW_ADDR(i));
			cJSON_AddStringToObject(record, IX2V_IX_TYPE, "IM");
			cJSON_AddStringToObject(record, IX2V_CONNECT, "DT");
			snprintf(temp, 100, "%s%s", gNbrPrefix, l_Nbr);
			cJSON_AddStringToObject(record, IX2V_G_NBR, temp);
			cJSON_AddItemToArray(retView, record);


			recordNbr++;

			if(recordNbr >= maxRecordNbr)
			{
				break;
			}
		}

		if(recordNbr >= maxRecordNbr)
		{
			break;
		}
	}

	return retView;
}

void CreateNewRes_Init(void)
{
	CreateAutoResMenu(&autoResMenu, RES_AUTO_RECORD, ResCreateNewCancel, ResCreateNewFunction, "Create new res", "Create", NULL);
}

void CreateNewRes_Close(void)
{
	if(autoResMenu.recordCtrl)
	{
		RC_MenuDelete(&(autoResMenu.recordCtrl));
		autoResMenu.recordCtrl = NULL;
	}

	if(autoResMenu.resRecord)
	{
		cJSON_Delete(autoResMenu.resRecord);
		autoResMenu.resRecord = NULL;
	}
	API_Event_NameAndMsg(EventMenuSettingProcess, "SettingRefresh", NULL);
}