#include "menu_utility.h"
#include "MenuHybridMatCreateNew.h"
#include "cJSON.h"
#include "obj_TableSurver.h"
#include "define_string.h"
#include "obj_PublicInformation.h"
#include "utility.h"
#include "task_Event.h"
#include "Common_TableView.h"
#include "obj_IoInterface.h"

#define APT_MAT_EDITOR_PARA		"{\"HeaderEnable\":true,\"CellCallback\":0,\"ColumnWidth\":[100, 200, 200, 200],\"Function\":{\"Name\":[],\"Callback\":0}}}"
#define APT_MAT_EDITOR_VIEW_TEMPALE "{\"IM_ID\":\"\",\"IX_NAME\":\"\",\"L_NBR\":\"\",\"G_NBR\":\"\"}"
#define APT_MAT_RECORD  "{\"IM START\":1,\"IM END\":64,\"NAME PREFIX\":\"DH-IM\",\"Describe\":\"List auto\"}" //编辑的记录

static TB_VIWE* aptMatEditorTb = NULL;


typedef struct 
{   
    int imId;
    int lNbr;
    int lNbrLen;
    int gNbr;
    int gNbrLen;
    char name[100];
}MAT_EDIT_ONE_T;

static MAT_EDIT_ONE_T editOne = {.imId = 1, .lNbr = 1, .lNbrLen = 1, .gNbr = 1, .gNbrLen = 1, .name = "DH-IM"};

static cJSON* batchCreateMatRecord = NULL;
static RECORD_CONTROL* batchCreateMatMenu = NULL;
static cJSON* editMatRecord = NULL;
static RECORD_CONTROL* editMatMenu = NULL;

void MenuAPT_MAT_Editor_Refresh(void);
static void AddMatMenu(RECORD_CONTROL** menu, const cJSON* record, const cJSON* editEnable, BtnCallback btnCallback, const char* title, char* btnString, ...);

static void EditMatFunction(const char* type, void* user_data)
{
	if(editMatRecord)
	{
		cJSON* where = cJSON_CreateObject();
		cJSON_AddNumberToObject(where, "IM_ID", GetEventItemInt(editMatRecord,  "IM_ID"));

		if(!strcmp(type, "Save"))
		{
			if(API_TB_UpdateByName(TB_NAME_APT_MAT, where, editMatRecord))
			{
				RC_MenuDelete(&editMatMenu);
				API_TableModificationInform(TB_NAME_APT_MAT, NULL);
				MenuAPT_MAT_Editor_Refresh();
				API_TIPS_Ext_Time("Save success", 2);
				cJSON_Delete(editMatRecord);
				editMatRecord = NULL;
				API_Event_By_Name(Event_MAT_Update);
			}
			else
			{
				API_TIPS_Ext_Time("Save error", 2);
			}
		}
		else if(!strcmp(type, "Delete"))
		{
			if(API_TB_DeleteByName(TB_NAME_APT_MAT, where))
			{
				RC_MenuDelete(&editMatMenu);
				API_TableModificationInform(TB_NAME_APT_MAT, NULL);
				MenuAPT_MAT_Editor_Refresh();
				API_TIPS_Ext_Time("Delete success", 2);
				API_Event_By_Name(Event_MAT_Update);
			}
			else
			{
				API_TIPS_Ext_Time("Delete error", 2);
			}
			cJSON_Delete(editMatRecord);
			editMatRecord = NULL;
		}
		cJSON_Delete(where);
	}
}

void EditAptMatRecord(cJSON* record, int deleteFlag)
{
	const char* editEnable[] = {"IX_NAME", "L_NBR", "G_NBR"};
	if(editMatRecord)
	{
		cJSON_Delete(editMatRecord);
	}
	editMatRecord =  cJSON_CreateObject();
	
	cJSON_AddNumberToObject(editMatRecord, "IM_ID", GetEventItemInt(record,  "IM_ID"));
	cJSON_AddStringToObject(editMatRecord, "IX_NAME", GetEventItemString(record,  "IX_NAME"));
	cJSON_AddStringToObject(editMatRecord, "L_NBR", GetEventItemString(record,  "L_NBR"));
	cJSON_AddStringToObject(editMatRecord, "G_NBR", GetEventItemString(record,  "G_NBR"));

	cJSON* editorTable =  cJSON_CreateStringArray(editEnable, sizeof(editEnable)/sizeof(editEnable[0]));
	AddMatMenu(&editMatMenu, editMatRecord, editorTable, EditMatFunction, "Edit MAT", "Save", deleteFlag ? "Delete" : NULL, NULL);
	cJSON_Delete(editorTable);
}

static void ResCellClick(int line, int col)
{
	if(aptMatEditorTb)
	{
		cJSON* record = cJSON_GetArrayItem(aptMatEditorTb->view, line);
		if(cJSON_IsObject(record))
		{
			EditAptMatRecord(record, 1);
		}
	}
}

static void ResReturnCallback(void)
{
	MenuAPT_MAT_Editor_Close();
}

static void BatchAddMatFunction(const char* type, void* user_data)
{
    if(!strcmp(type, "Batch add"))
    {
		cJSON* record;
		cJSON* view = CreateMatRecords(batchCreateMatRecord, 256);
		if(cJSON_GetArraySize(view))
		{
			int matRecords = 0;
			char tempString[200];
			char translate[100];

			cJSON_ArrayForEach(record, view)
			{
				if(API_TB_AddByName(TB_NAME_APT_MAT, record))
				{
					matRecords++;
				}
			}
			snprintf(tempString, 200, "%s %d", get_language_text2("Batch add MAT list", translate, 100), matRecords);
			API_TIPS_Ext_Time(tempString, 2);
			if(matRecords)
			{
				API_TableModificationInform(TB_NAME_APT_MAT, NULL);
				API_TB_SaveByName(TB_NAME_APT_MAT);
				API_Event_By_Name(Event_MAT_Update);
				MenuAPT_MAT_Editor_Refresh();
			}
		}
		cJSON_Delete(view);
    }
}

static void AddMatMenu(RECORD_CONTROL** menu, const cJSON* record, const cJSON* editEnable, BtnCallback btnCallback, const char* title, char* btnString, ...)
{
	char* name;
	const char* editCheck = "{\"Check\":{\"IM_ID\":\"(^[1-9]$)|(^[1-5][0-9]$)|(^6[0-4]$)\",\"L_NBR\":\"^[0-9]{0,6}$\",\"G_NBR\":\"^[0-9]{0,10}$\"},\"Translate\":{}}";
	cJSON* editorTable =  cJSON_Parse(editCheck);

	cJSON* func = cJSON_CreateObject();
	cJSON* btnname = cJSON_AddArrayToObject(func,"BtnName");

	va_list valist;
	va_start(valist, btnString);
	for(name = btnString; name; name = va_arg(valist, char*))
	{
		cJSON_AddItemToArray(btnname, cJSON_CreateString(name));
	}
	va_end(valist);

	cJSON_AddNumberToObject(func, "BtnCallback", (int)btnCallback);

    RC_MenuDisplay(menu, record, editEnable, editorTable, func, title);
    cJSON_Delete(editorTable);
    cJSON_Delete(func);
}

static void EditOneRecordProcessingInitialValues(const cJSON* lastRecord)
{
	editOne.imId = GetEventItemInt(lastRecord,  "IM_ID");
	char* tempString = GetEventItemString(lastRecord,  "L_NBR");
	editOne.lNbrLen = strlen(tempString);
	editOne.lNbr = atoi(tempString);

	tempString = GetEventItemString(lastRecord,  "G_NBR");
	editOne.gNbrLen = strlen(tempString);
	editOne.gNbr = atoi(tempString);

	if(++editOne.imId > 64)
	{
		editOne.imId = 1;
	}
	editOne.gNbr++;
	editOne.lNbr++;
	snprintf(editOne.name, 100, "%s", GetEventItemString(lastRecord,  "IX_NAME"));
}

static void AddMatFunction(const char* type, void* user_data)
{
    if(!strcmp(type, "Save"))
	{
		if(editMatRecord)
		{
			int imId = GetEventItemInt(editMatRecord,  "IM_ID");
			char ixAddr[11];
			snprintf(ixAddr, 11, "009900%02d01", imId);
			cJSON_AddStringToObject(editMatRecord, "IX_ADDR", ixAddr);
			cJSON_AddStringToObject(editMatRecord, "SYS_TYPE", "DH-APT");
			cJSON_AddStringToObject(editMatRecord, "IX_TYPE", "IM");
			cJSON_AddStringToObject(editMatRecord, "CONNECT", "DH");

			if(API_TB_AddByName(TB_NAME_APT_MAT, editMatRecord))
			{
				RC_MenuDelete(&editMatMenu);
				API_TableModificationInform(TB_NAME_APT_MAT, NULL);
				EditOneRecordProcessingInitialValues(editMatRecord);
				MenuAPT_MAT_Editor_Refresh();
				API_TIPS_Ext_Time("Add success", 2);
				cJSON_Delete(editMatRecord);
				editMatRecord = NULL;
				API_Event_By_Name(Event_MAT_Update);
            }
			else
			{
				API_TIPS_Ext_Time("Add error", 2);
			}
		}
	}
}

static void FunctionClick(const char* funcName)
{
	if(funcName)
	{
		if(!strcmp(funcName, "Add"))
		{
			char format[20];
			char temp[100];
			const char* editEnable[] = {"IM_ID", "IX_NAME", "L_NBR", "G_NBR"};

			if(editMatRecord)
			{
				cJSON_Delete(editMatRecord);
			}
			editMatRecord =  cJSON_CreateObject();
			cJSON_AddNumberToObject(editMatRecord, "IM_ID", editOne.imId);
			cJSON_AddStringToObject(editMatRecord, "IX_NAME", editOne.name);

			snprintf(format, 20, "%%0%dd", editOne.lNbrLen);
			snprintf(temp, 100, format, editOne.lNbr);
			cJSON_AddStringToObject(editMatRecord, "L_NBR", temp);

			snprintf(format, 20, "%%0%dd", editOne.gNbrLen);
			snprintf(temp, 100, format, editOne.gNbr);
			cJSON_AddStringToObject(editMatRecord, "G_NBR", temp);

			cJSON* editorTable =  cJSON_CreateStringArray(editEnable, sizeof(editEnable)/sizeof(editEnable[0]));
			AddMatMenu(&editMatMenu, editMatRecord, editorTable, AddMatFunction, "Add MAT", "Save", NULL);
			cJSON_Delete(editorTable);
		}
		else if (!strcmp(funcName, "Batch add"))
		{
			if(batchCreateMatRecord)
			{
				cJSON_Delete(batchCreateMatRecord);
			}
			batchCreateMatRecord = cJSON_Parse(APT_MAT_RECORD);
			BatchCreateMatMenu(batchCreateMatRecord, &batchCreateMatMenu, NULL, BatchAddMatFunction, "Batch add MAT", "Batch add", NULL);
		}
	}
}

void MenuAPT_MAT_Editor_Refresh(void)
{
	if(aptMatEditorTb)
	{
		cJSON* view = cJSON_CreateArray();
		cJSON_AddItemToArray(view, cJSON_Parse(APT_MAT_EDITOR_VIEW_TEMPALE));
		API_TB_SelectBySortByName(TB_NAME_APT_MAT, view, NULL, 0);
		cJSON_DeleteItemFromArray(view, 0);
		TB_MenuDisplay(&aptMatEditorTb, view, "APT MAT editor", NULL);
		cJSON_Delete(view);
	}
}

void MenuAPT_MAT_Editor_Init(void)
{
	cJSON* para = cJSON_Parse(APT_MAT_EDITOR_PARA);
	cJSON_ReplaceItemInObjectCaseSensitive(para, "CellCallback", cJSON_CreateNumber((int)ResCellClick));
	cJSON_AddNumberToObject(para, "ReturnCallback", (int)ResReturnCallback);

	cJSON* func = cJSON_CreateObject();
	cJSON* funcName = cJSON_AddArrayToObject(func, "Name");
	cJSON_AddItemToArray(funcName, cJSON_CreateString("Add"));
	cJSON_AddItemToArray(funcName, cJSON_CreateString("Batch add"));
	cJSON_AddNumberToObject(func, "Callback", (int)FunctionClick);
	cJSON_ReplaceItemInObjectCaseSensitive(para, "Function", func);

	cJSON* view = cJSON_CreateArray();
	cJSON_AddItemToArray(view, cJSON_Parse(APT_MAT_EDITOR_VIEW_TEMPALE));
	API_TB_SelectBySortByName(TB_NAME_APT_MAT, view, NULL, 0);
	cJSON_DeleteItemFromArray(view, 0);

	TB_MenuDisplay(&aptMatEditorTb, view, "APT MAT editor", para);
	cJSON_Delete(view);
	cJSON_Delete(para);
}

void CloseAptMatEditRecordNenu(void)
{      
    if(editMatMenu)
    {
        RC_MenuDelete(&editMatMenu);
    }

    if(editMatRecord)
	{
		cJSON_Delete(editMatRecord);
		editMatRecord = NULL;
	}
}

void MenuAPT_MAT_Editor_Close(void)
{
	CloseAptMatEditRecordNenu();

	RC_MenuDelete(&batchCreateMatMenu);

	if(batchCreateMatRecord)
	{
		cJSON_Delete(batchCreateMatRecord);
		batchCreateMatRecord = NULL;
	}
	TB_MenuDelete(&aptMatEditorTb);
	API_Event_NameAndMsg(EventMenuSettingProcess, "SettingRefresh", NULL);
}