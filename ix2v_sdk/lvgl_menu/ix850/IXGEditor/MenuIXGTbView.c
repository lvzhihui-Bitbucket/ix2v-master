#include <stdio.h>
#include "task_CallServer.h"
#include "elog_forcall.h"
#include "obj_PublicInformation.h"
#include "task_Event.h"
#include "ix850_icon.h"
#include "obj_lvgl_msg.h"
#include "table_display.h"
#include "define_file.h"
#include "obj_TableSurver.h"

static TABLE_DISPLAY *table_display=NULL;
static cJSON *view_tb=NULL;
static cJSON *edit_record=NULL;
static cJSON *view_filter=NULL;
//static cJSON *IXGIMEditCheck=NULL;
static cJSON *IXGIMEditDesc=NULL;
static lv_obj_t* editor=NULL;
static int add_new_item=0;

lv_obj_t* create_editor_menu(cJSON* rec_data, const cJSON* editabel, const cJSON* tb_description);
int update_one_ixgim_to_R8001tb(cJSON *item);
int delete_one_ixgim_to_R8001tb(cJSON *item);

cJSON *FilterIXGIm_From_R8001(cJSON *filter);
cJSON *Create_New_IXGIM_Item(void);
cJSON *Create_IXGIM_Editable(int new_item);
int Judge_IXGIM_Exist(cJSON *item);


void MenuIXGTbView_Init(void)
{
	view_tb=FilterIXGIm_From_R8001(NULL);
	cJSON *IXGIMEditCheck=NULL;
	cJSON *IXGIMEditTranslate=NULL;
	if(IXGIMEditDesc==NULL)
	{
		IXGIMEditDesc=cJSON_CreateObject();
		IXGIMEditCheck=GetJsonFromFile(IXGIMEditCheckFile);
		if(IXGIMEditCheck)
			cJSON_AddItemToObject(IXGIMEditDesc,"Check",IXGIMEditCheck);
		IXGIMEditTranslate=GetJsonFromFile(IXGIMEditTransFile);
		if(IXGIMEditTranslate)
			cJSON_AddItemToObject(IXGIMEditDesc,"Translate",IXGIMEditTranslate);
		//IXGIMEditTranslate=cJSON_AddObjectToObject(IXGIMEditDesc,"Translate");
		//cJSON_AddStringToObject(IXGIMEditTranslate,"IXG_ID","IXG_BDU_ID");
		//cJSON_AddStringToObject(IXGIMEditTranslate,"IXG_IM_ID","IM_ID");
		
		
	}
	table_display=create_table_display_menu(view_tb,1,IXGIMEditTranslate);
	//printf("11111111111%s:%d\n",__func__,__LINE__);

}
void MenuIXGTbView_Process(int msg_type,void* arg)
{
	//�յ����ذ������£�����popDisplayLastMenu��������һ��˵�
	//popDisplayLastMenu();
	//�յ�ĳһ�����ݱ�������Ϣ��׼�����ݣ�����SwitchOneMenu�л�������IXG�ֻ��༭�˵�
	//SwitchOneMenu(MenuOneIXGImEdit,arg,1);
	//printf("111111111MenuIXGTbView_Process msg:%d\n",msg_type);
	cJSON *edit_able;
	
	switch(msg_type)
	{
		case IXG_MSG_ADD_RECORD:
		
			//if(editor)
				//lv_obj_del(editor);
			if(edit_record!=NULL)
				cJSON_Delete(edit_record);
			edit_record=Create_New_IXGIM_Item();
			edit_able=Create_IXGIM_Editable(1);
			
			//editor=create_editor_menu(edit_record,edit_able,IXGIMEditCheck);
			create_editor_menu(edit_record,edit_able,IXGIMEditDesc);
			cJSON_Delete(edit_able);
			add_new_item=1;
			break;
			
		case IXG_MSG_EDIT_RECORD:
			//printf_json((cJSON*)arg,__func__);
			if(edit_record!=NULL)
				cJSON_Delete(edit_record);
			edit_record=cJSON_Duplicate((cJSON*)arg,1);
			//if(editor)
				//lv_obj_del(editor);
			edit_able=Create_IXGIM_Editable(0);
			
			//editor=create_editor_menu(edit_record,edit_able,IXGIMEditCheck);
			create_editor_menu(edit_record,edit_able,IXGIMEditDesc);
			cJSON_Delete(edit_able);
			add_new_item=0;
			break;
			
		case IXG_MSG_CHANGE_FITER:
			if(view_filter)
				cJSON_Delete(view_filter);
			if(view_tb)
				cJSON_Delete(view_tb);
			printf_json((cJSON*)arg,"FILTER");
			view_filter=cJSON_Duplicate((cJSON*)arg,1);
			view_tb=FilterIXGIm_From_R8001((cJSON*)arg);
			ixgview_data_fresh(table_display,view_tb,1);
			break;
		case IXG_MSG_EXIT:
			MenuIXGTbView_Close();
			break;
			
		case RECORD_MSG_SAVE_EXIT:
			if(add_new_item)
			{
				if(Judge_IXGIM_Exist((cJSON*)arg)>0)
				{
					LV_API_TIPS("The device is aleady exist!",1);
					//API_TIPS("The device is aleady exist!");
					return;
				}
			}
			printf_json((cJSON*)arg,"SAVE_EXIT");
			if(view_tb)
				cJSON_Delete(view_tb);
			update_one_ixgim_to_R8001tb((cJSON*)arg);
			update_one_unlock_code_to_Privatetb((cJSON*)arg);
			view_tb=FilterIXGIm_From_R8001(view_filter);
			ixgview_data_fresh(table_display,view_tb,1);
			break;
		case RECORD_MSG_DEL_EXIT:
			if(add_new_item)
			{
			
				//if(Judge_IXGIM_Exist((cJSON*)arg))>0)
				{
					//API_TIPS("The device is aleady exist!");
					return;
				}
			}
			if(view_tb)
				cJSON_Delete(view_tb);
			delete_one_ixgim_to_R8001tb((cJSON*)arg);
			view_tb=FilterIXGIm_From_R8001(view_filter);
			ixgview_data_fresh(table_display,view_tb,1);
			break;	
			
	}
}

void MenuIXGTbView_Close(void)
{
	//�˵��ر�ʱ�������ñ�༭�����ٺ���
	//�ͷű�����
	//if(editor)
		//lv_obj_del(editor);
	//editor=NULL;
		
	if(table_display!=NULL)
		table_delete(table_display);
	//printf("11111111111%s:%d\n",__func__,__LINE__);
	if(view_tb)
		cJSON_Delete(view_tb);
	
	view_tb=NULL;
//printf("11111111111%s:%d\n",__func__,__LINE__);
	if(edit_record!=NULL)
		cJSON_Delete(edit_record);
	edit_record=NULL;
//printf("11111111111%s:%d\n",__func__,__LINE__);
	if(view_filter)
		cJSON_Delete(view_filter);
	view_filter=NULL;
//printf("11111111111%s:%d\n",__func__,__LINE__);	
	if(IXGIMEditDesc)
		cJSON_Delete(IXGIMEditDesc);
	IXGIMEditDesc=NULL;		
	table_display=NULL;
	//printf("11111111111%s:%d\n",__func__,__LINE__);
}



cJSON *FilterIXGIm_From_R8001(cJSON *filter)
{
	cJSON* view = cJSON_CreateArray();
	cJSON* where = cJSON_CreateObject();
	cJSON* tpl=cJSON_CreateObject();
	
    cJSON_AddStringToObject(tpl,"IX_ADDR","");
	cJSON_AddStringToObject(tpl,"IXG_ID","");
	cJSON_AddStringToObject(tpl,"IXG_IM_ID","");

	

	cJSON_AddStringToObject(tpl,"G_NBR","");
	cJSON_AddStringToObject(tpl,"L_NBR","");
	cJSON_AddStringToObject(tpl,"IX_NAME","");
	cJSON_AddItemToArray(view,tpl);
	
	cJSON_AddStringToObject(where, "DevModel", "IXG-IM");
	cJSON_AddStringToObject(where, "IX_ADDR", GetSysVerInfo_bd());
	cJSON *filter_element;
	cJSON_ArrayForEach(filter_element, filter)
	{
		if(cJSON_IsNumber(filter_element))
			cJSON_AddNumberToObject(where,filter_element->string,filter_element->valueint);
		else if(cJSON_IsString(filter_element))
			cJSON_AddStringToObject(where,filter_element->string,filter_element->valuestring);
	}
	//cJSON_AddStringToObject(where, "Platform", "IX1/2");
	API_TB_SelectBySortByName(TB_NAME_R8001, view, where, 0);

	cJSON_Delete(where);

	cJSON_DeleteItemFromArray(view,0);

	SortTableView(view, "IX_ADDR", 0);
	AddUnlockCode_ToIXGIM_Tb(view);
	return view;
}
void AddUnlockCode_ToIXGIM_Tb(cJSON *tb)
{
	cJSON *one_item;
	cJSON *where;
	cJSON *view;
	char buff[50];
	cJSON_ArrayForEach(one_item, tb)
	{
		if(GetJsonDataPro(one_item, "IX_ADDR", buff))
		{
			where=cJSON_CreateObject();
			cJSON_AddStringToObject(where,"IX_ADDR",buff);
			view=cJSON_CreateArray();
			if(API_TB_SelectBySortByName(TB_NAME_PRIVATE_PWD_BY_L_NBR, view, where, 0))
			{
				if(GetJsonDataPro(cJSON_GetArrayItem(view,0),"CODE",buff))
				{
					cJSON_AddStringToObject(one_item,"UNLOCK_CODE",buff);
				}
				else
					cJSON_AddStringToObject(one_item,"UNLOCK_CODE","-");
			}
			else
				cJSON_AddStringToObject(one_item,"UNLOCK_CODE","-");
			cJSON_Delete(where);
			cJSON_Delete(view);
		}
		else
			cJSON_AddStringToObject(one_item,"UNLOCK_CODE","-");
	}
}

cJSON *Create_New_IXGIM_Item(void)
{
	int ixg_id=1;
	int ixg_im=1;
	int size;
	cJSON* all_tb=FilterIXGIm_From_R8001(NULL);
	if((size=cJSON_GetArraySize(all_tb))>0)
	{
		cJSON *old_item=cJSON_GetArrayItem(all_tb,size-1);
		//printf_json(old_item,"xxx");
		GetJsonDataPro(old_item,"IXG_ID", &ixg_id);
		GetJsonDataPro(old_item,"IXG_IM_ID", &ixg_im);
		if(++ixg_im>=33)
		{
			ixg_id++;
			ixg_im=1;
		}
			
	}
	cJSON_Delete(all_tb);
	cJSON* tpl=cJSON_CreateObject();
	char buf[100];
	sprintf(buf,"%s%02d%02d01",GetSysVerInfo_bd(),ixg_id,ixg_im);
    	cJSON_AddStringToObject(tpl,"IX_ADDR",buf);
	cJSON_AddNumberToObject(tpl,"IXG_ID",ixg_id);
	cJSON_AddNumberToObject(tpl,"IXG_IM_ID",ixg_im);

	cJSON_AddStringToObject(tpl,"G_NBR","-");
	cJSON_AddStringToObject(tpl,"L_NBR","-");
	sprintf(buf,"IXG%d-IM%02d",ixg_id,ixg_im);
	cJSON_AddStringToObject(tpl,"IX_NAME",buf);
	cJSON_AddStringToObject(tpl,"UNLOCK_CODE","-");

	return tpl;
}
cJSON *Create_IXGIM_Editable(int new_item)
{
	cJSON *edit_able=cJSON_CreateArray();
	if(new_item)
	{
		cJSON_AddItemToArray(edit_able,cJSON_CreateString("IXG_ID"));
		cJSON_AddItemToArray(edit_able,cJSON_CreateString("IXG_IM_ID"));
	}
	cJSON_AddItemToArray(edit_able,cJSON_CreateString("G_NBR"));
	cJSON_AddItemToArray(edit_able,cJSON_CreateString("L_NBR"));
	cJSON_AddItemToArray(edit_able,cJSON_CreateString("IX_NAME"));
	cJSON_AddItemToArray(edit_able,cJSON_CreateString("UNLOCK_CODE"));
	return edit_able;
}
int Judge_IXGIM_Exist(cJSON *item)
{
	cJSON *key=cJSON_GetObjectItemCaseSensitive(item,"IX_ADDR");
	if(key==NULL)
		return -1;
	
	cJSON *wh=cJSON_CreateObject();
	cJSON* View=cJSON_CreateArray();
	int ret;
	cJSON_AddStringToObject(wh,"IX_ADDR",key->valuestring);

	ret = API_TB_SelectBySortByName(TB_NAME_R8001, View, wh, 0);

	cJSON_Delete(wh);
	cJSON_Delete(View);

	return ret;
}
int update_one_unlock_code_to_Privatetb(cJSON *item)
{
	//static int id=99;
	cJSON *key=cJSON_GetObjectItemCaseSensitive(item,"IX_ADDR");
	if(key==NULL)
		return -1;
	
	cJSON *wh=cJSON_CreateObject();
	cJSON_AddStringToObject(wh,"IX_ADDR", key->valuestring);
	API_TB_DeleteByName(TB_NAME_PRIVATE_PWD_BY_L_NBR, wh);
	cJSON_Delete(wh);

	cJSON *pwd_item=cJSON_CreateObject();
	char buff[40];
	cJSON_AddStringToObject(pwd_item,"IX_ADDR", key->valuestring);
	if(GetJsonDataPro(item, "L_NBR",buff))
		cJSON_AddStringToObject(pwd_item,"L_NBR", buff);
	
	if(GetJsonDataPro(item, "UNLOCK_CODE",buff))
		cJSON_AddStringToObject(pwd_item,"CODE", buff);

	//cJSON_AddNumberToObject(pwd_item,"ID", id++);

	API_TB_AddByName(TB_NAME_PRIVATE_PWD_BY_L_NBR, pwd_item);
	cJSON_Delete(pwd_item);
	
}
int update_one_ixgim_to_R8001tb(cJSON *item)
{
	cJSON *key=cJSON_GetObjectItemCaseSensitive(item,"IX_ADDR");
	if(key==NULL)
		return -1;
	
	cJSON *wh=cJSON_CreateObject();
	cJSON_AddStringToObject(wh,"IX_ADDR", key->valuestring);
	API_TB_DeleteByName(TB_NAME_R8001, wh);
	cJSON_Delete(wh);

	cJSON *new_item=cJSON_Duplicate(item,1);
	cJSON_DeleteItemFromObject(new_item,"UNLOCK_CODE");
	if(cJSON_GetObjectItemCaseSensitive(new_item,"MFG_SN")==NULL)
		cJSON_AddStringToObject(new_item,"MFG_SN","-");
	if(cJSON_GetObjectItemCaseSensitive(new_item,"IP_ADDR")==NULL)
		cJSON_AddStringToObject(new_item,"IP_ADDR","-");
	if(cJSON_GetObjectItemCaseSensitive(new_item,"G_NBR")==NULL)
		cJSON_AddStringToObject(new_item,"G_NBR","");
	if(cJSON_GetObjectItemCaseSensitive(new_item,"L_NBR")==NULL)
		cJSON_AddStringToObject(new_item,"L_NBR","");
	if(cJSON_GetObjectItemCaseSensitive(new_item,"IXG_ID")==NULL)
		cJSON_AddStringToObject(new_item,"IXG_ID","-");
	if(cJSON_GetObjectItemCaseSensitive(new_item,"IXG_IM_ID")==NULL)
		cJSON_AddStringToObject(new_item,"IXG_IM_ID","-");
	if(cJSON_GetObjectItemCaseSensitive(new_item,"IX_TYPE")==NULL)
		cJSON_AddStringToObject(new_item,"IX_TYPE","IM");
	if(cJSON_GetObjectItemCaseSensitive(new_item,"IX_Model")==NULL)
		cJSON_AddStringToObject(new_item,"IX_Model","IXG-IM");
	if(cJSON_GetObjectItemCaseSensitive(new_item,"DevModel")==NULL)
		cJSON_AddStringToObject(new_item,"DevModel","IXG-IM");
	if(cJSON_GetObjectItemCaseSensitive(new_item,"FW_VER")==NULL)
		cJSON_AddStringToObject(new_item,"FW_VER","-");
	if(cJSON_GetObjectItemCaseSensitive(new_item,"HW_VER")==NULL)
		cJSON_AddStringToObject(new_item,"HW_VER","-");

	API_TB_AddByName(TB_NAME_R8001, new_item);
	cJSON_Delete(new_item);
	return 0;
}

int delete_one_ixgim_to_R8001tb(cJSON *item)
{
	cJSON *key=cJSON_GetObjectItemCaseSensitive(item,"IX_ADDR");
	if(key==NULL)
		return -1;
	
	cJSON *wh=cJSON_CreateObject();
	cJSON_AddStringToObject(wh,"IX_ADDR",key->valuestring);
	API_TB_DeleteByName(TB_NAME_R8001, wh);
	cJSON_Delete(wh);

	return 0;
}

void API_MenuIXGTbView_Process(void* arg)
{
	TABLE_DISPLAY* td = (TABLE_DISPLAY*)arg;
	MenuIXGTbView_Process(td->msg_type,td->msg_data);
}

////////////////////////////////////////////////////////


