#include "menu_utility.h"
#include "table_display.h"
#include "KeyboardResource.h"
#include "ix850_icon.h"
#include "obj_ResEditor.h"
#include "utility.h"

static lv_obj_t* kb=NULL;
static lv_obj_t* editor_menu_return = NULL;
static lv_obj_t* ui_record = NULL;
static lv_obj_t* editor_keyboard_return = NULL;
static lv_obj_t * ui_keyboard=NULL;
static cJSON* rec_field;
//static cJSON* Description=NULL;
static cJSON* EditCheck=NULL;
static cJSON* EditTrans=NULL;
static cJSON* ValueLabelArray=NULL;
static TABLE_DISPLAY td;

lv_obj_t* create_text_editor(lv_obj_t* parent, char* name, char* val, int editable,cJSON* description);
lv_obj_t* create_number_editor(lv_obj_t* parent, char* name, int val, int able,cJSON* description);
lv_obj_t* create_slider_editor(lv_obj_t* parent, char* name, int val, cJSON* description);
lv_obj_t* create_switch_editor(lv_obj_t* parent, char* name, int val, cJSON* description);
lv_obj_t* create_editor_menu(cJSON* rec_data, const cJSON* editabel, const cJSON* tb_description);
static void save_editor_event(lv_event_t* e);
static void cancel_editor_event(lv_event_t* e);
static void del_record_event(lv_event_t* e);
static lv_obj_t* ui_record_init(void);
void create_string_keyboard_menu(lv_obj_t* target, char* val);
static lv_obj_t* ui_keyboard_init(void);
#if 0
static void number_edit_event_cb(lv_event_t* e)
{
    lv_event_code_t code = lv_event_get_code(e);
    lv_obj_t* ta = lv_event_get_target(e);
    lv_obj_t* label = lv_event_get_user_data(e);

    if (code == LV_EVENT_FOCUSED) {
        if (lv_indev_get_type(lv_indev_get_act()) != LV_INDEV_TYPE_KEYPAD) {

            lv_keyboard_set_textarea(kb, ta);

            lv_keyboard_set_mode(kb, LV_KEYBOARD_MODE_NUMBER);

            lv_obj_set_height(ta, 800 - lv_obj_get_height(kb));
            lv_obj_clear_flag(kb, LV_OBJ_FLAG_HIDDEN);
            lv_obj_scroll_to_view_recursive(ta, LV_ANIM_OFF);
        }
    }
    else if (code == LV_EVENT_CANCEL) {

        lv_disp_load_scr(editor_keyboard_return);      
        lv_obj_del(ui_keyboard);
        ui_keyboard = NULL;
    }
    else if (code == LV_EVENT_READY)
    {
    		//lv_obj_t* nameLabel = lv_obj_get_child(lv_obj_get_parent(label), 0);
        //char* name = lv_label_get_text(nameLabel);
         char name[100];
       strcpy(name,((cJSON*) lv_obj_get_user_data(label))->string);
        char* text = lv_textarea_get_text(ta);
	if(EditCheck)
	{
		if(API_Data_Edit_Check(name,text,EditCheck)==0)
		{
            LV_API_TIPS(" Error! Please enter it again!",4);
                    //API_TIPS(" Error! Please enter it again!");
                    return;
                }
	}
	cJSON *	new_item=cJSON_CreateNumber(atoi(text));
	lv_obj_set_user_data(label,new_item);
        lv_label_set_text(label, text);
	cJSON_ReplaceItemInObject(rec_field, name, new_item);
	editor_menu_special_process(name);
        lv_disp_load_scr(editor_keyboard_return);      
        lv_obj_del(ui_keyboard);
        ui_keyboard = NULL;
    }
}

void create_number_keyboard_menu(lv_obj_t* target, char* val)
{
    editor_keyboard_return = lv_scr_act();
    ui_keyboard = ui_keyboard_init();

    lv_obj_t* keyMenu = lv_obj_create(ui_keyboard);
    lv_obj_set_style_pad_row(keyMenu, 0, 0);
    lv_obj_set_size(keyMenu, 480, 800);
    lv_obj_set_flex_flow(keyMenu, LV_FLEX_FLOW_COLUMN);
    lv_obj_clear_flag(keyMenu, LV_OBJ_FLAG_SCROLLABLE);

    lv_obj_t* nameLabel = lv_obj_get_child(lv_obj_get_parent(target), 0);
    char* name = lv_label_get_text(nameLabel);

    lv_obj_t* name_label = lv_label_create(keyMenu);
    lv_label_set_text(name_label, name);
    lv_obj_set_size(name_label, 200, 80);
    lv_obj_set_style_text_font(name_label, &lv_font_montserrat_32, 0);

    lv_obj_t* ta = lv_textarea_create(keyMenu);
    lv_obj_set_style_text_font(ta, &lv_font_montserrat_26, 0);
    lv_textarea_add_text(ta, val);
    lv_obj_set_size(ta, LV_PCT(100), 300);

    kb = keyboard_create(ui_keyboard);
    //lv_obj_set_style_text_font(kb, &lv_font_montserrat_26, 0);
    //lv_obj_add_flag(kb, LV_OBJ_FLAG_HIDDEN);

    lv_obj_add_event_cb(ta, number_edit_event_cb, LV_EVENT_ALL, target);
    lv_event_send(ta, LV_EVENT_FOCUSED, NULL);
}

static void number_click_event(lv_event_t* e)
{
    cJSON* description = lv_event_get_user_data(e);
    lv_obj_t* valueLabel = lv_event_get_target(e);
    char* text = lv_label_get_text(valueLabel);
    create_number_keyboard_menu(valueLabel, text);
}

static void text_edit_event_cb(lv_event_t* e)
{
    lv_event_code_t code = lv_event_get_code(e);
    lv_obj_t* ta = lv_event_get_target(e);
    lv_obj_t* label = lv_event_get_user_data(e);    

    if (code == LV_EVENT_FOCUSED) {
        if (lv_indev_get_type(lv_indev_get_act()) != LV_INDEV_TYPE_KEYPAD) {

            lv_keyboard_set_textarea(kb, ta);

            lv_keyboard_set_mode(kb, LV_KEYBOARD_MODE_TEXT_LOWER);
		lv_obj_clear_flag(kb, LV_OBJ_FLAG_HIDDEN);
            //lv_obj_set_height(ta, 600 - lv_obj_get_height(kb));
            //lv_obj_clear_flag(kb, LV_OBJ_FLAG_HIDDEN);
            //lv_obj_scroll_to_view_recursive(ta, LV_ANIM_OFF);
        }
    }
    else if (code == LV_EVENT_CANCEL) {

        lv_disp_load_scr(editor_keyboard_return);      
        lv_obj_del(ui_keyboard);
        ui_keyboard = NULL;
    }
    else if (code == LV_EVENT_READY)
    {
      //  lv_obj_t* nameLabel = lv_obj_get_child(lv_obj_get_parent(label), 0);
        //char* name = lv_label_get_text(nameLabel);
        char name[100];
       strcpy(name,((cJSON*) lv_obj_get_user_data(label))->string);
        char* text = lv_textarea_get_text(ta);

	if(EditCheck)
	{
		if(API_Data_Edit_Check(name,text,EditCheck)==0)
		{
            LV_API_TIPS(" Error! Please enter it again!",4);
                    //API_TIPS(" Error! Please enter it again!");
                    return;
        }
	}
	cJSON *	new_item=cJSON_CreateString(text);
	lv_obj_set_user_data(label,new_item);
        lv_label_set_text(label, text);
        cJSON_ReplaceItemInObject(rec_field, name, new_item);
      		editor_menu_special_process(name);

        lv_disp_load_scr(editor_keyboard_return);      
        lv_obj_del(ui_keyboard);
        ui_keyboard = NULL;
        
    }
}

void create_string_keyboard_menu(lv_obj_t* target, char* val)
{
    editor_keyboard_return = lv_scr_act();
    ui_keyboard = ui_keyboard_init();   

    lv_obj_t* keyMenu = lv_obj_create(ui_keyboard);
    lv_obj_set_style_pad_row(keyMenu, 0, 0);
    lv_obj_set_size(keyMenu, 480, 800);
    lv_obj_set_flex_flow(keyMenu, LV_FLEX_FLOW_COLUMN);
    lv_obj_clear_flag(keyMenu, LV_OBJ_FLAG_SCROLLABLE);

    lv_obj_t* nameLabel = lv_obj_get_child(lv_obj_get_parent(target), 0);
    char* name = lv_label_get_text(nameLabel);

    lv_obj_t* name_label = lv_label_create(keyMenu);
    lv_label_set_text(name_label, name);
    lv_obj_set_size(name_label, 200, 80);
    lv_obj_set_style_text_font(name_label, &lv_font_montserrat_32, 0);


    lv_obj_t* ta = lv_textarea_create(keyMenu);
    lv_obj_set_style_text_font(ta, &lv_font_montserrat_26, 0);
    lv_textarea_add_text(ta, val);
    lv_obj_set_size(ta, LV_PCT(100), 300);
    //lv_obj_set_y(ta, 100);

    kb = keyboard_create(ui_keyboard);
    //lv_obj_set_style_text_font(kb, &lv_font_montserrat_26, 0);
    //lv_obj_add_flag(kb, LV_OBJ_FLAG_HIDDEN);

    lv_obj_add_event_cb(ta, text_edit_event_cb, LV_EVENT_ALL, target);
    lv_event_send(ta, LV_EVENT_FOCUSED, NULL);
}
int IX_NAMEEdit_Callback(char *text,void *user_data)
{
	if(user_data==NULL)
		return -1;
	lv_obj_t *label=(lv_obj_t *)user_data;
	
	cJSON *	new_item=cJSON_CreateString(text);
	lv_obj_set_user_data(label,new_item);
        lv_label_set_text(label, text);
        cJSON_ReplaceItemInObject(rec_field, "IX_NAME", new_item);
		
	return 0;
}
static void text_click_event(lv_event_t* e)
{
    cJSON* description = lv_event_get_user_data(e);
    lv_obj_t* valueLabel = lv_event_get_target(e);
    char* text = lv_label_get_text(valueLabel);

	if(description&&strcmp(description->string,"IX_NAME")==0)
	{
		  lv_obj_t* nameLabel = lv_obj_get_child(lv_obj_get_parent(valueLabel), 0);
    		char* name = lv_label_get_text(nameLabel);
    		Vtk_KbInitByMode(Vtk_KbMode1,"English",name,20,NULL,text,NULL,IX_NAMEEdit_Callback,valueLabel);
	}
	else
    		create_string_keyboard_menu(valueLabel, text);
}
#else
static int* text_edit_event_cb(const char* val,void *user_data)
{
    lv_obj_t* nameLabel = lv_obj_get_child(lv_obj_get_parent(user_data), 0);
    char* name = lv_label_get_text(nameLabel);
    cJSON* new_item = cJSON_CreateString(val);

    lv_label_set_text(user_data, val);
    cJSON_ReplaceItemInObject(rec_field, name, new_item);
}

static int* number_edit_event_cb(const char* val,void *user_data)
{
    lv_obj_t* nameLabel = lv_obj_get_child(lv_obj_get_parent(user_data), 0);
    char* name = lv_label_get_text(nameLabel);

    cJSON *	new_item = cJSON_CreateNumber(atoi(val));

    lv_label_set_text(user_data, val);
    cJSON_ReplaceItemInObject(rec_field, name, new_item);
}

static void number_click_event(lv_event_t* e)
{
    cJSON* description = lv_event_get_user_data(e);
    lv_obj_t* valueLabel = lv_event_get_target(e);
    char* text = lv_label_get_text(valueLabel);
    lv_obj_t* nameLabel = lv_obj_get_child(lv_obj_get_parent(valueLabel), 0);
    char* name = lv_label_get_text(nameLabel);
    char* regex_pattern;
    if(description == NULL)
        regex_pattern = NULL;
    else
        regex_pattern = description->valuestring;

    //Vtk_KbMode2Init(Vtk_KbMode2,NULL,name,20,regex_pattern,text,"tips:",number_edit_event_cb,valueLabel);
}

static void text_click_event(lv_event_t* e)
{
    cJSON* description = lv_event_get_user_data(e);
    lv_obj_t* valueLabel = lv_event_get_target(e);
    char* text = lv_label_get_text(valueLabel);
    lv_obj_t* nameLabel = lv_obj_get_child(lv_obj_get_parent(valueLabel), 0);
    char* name = lv_label_get_text(nameLabel);
    char* regex_pattern;
    if(description == NULL)
        regex_pattern = NULL;
    else
        regex_pattern = description->valuestring;
}

#endif
lv_obj_t* create_text_editor(lv_obj_t* parent, char* name, char* val, int able,cJSON* description)
{
    lv_obj_t* obj = lv_obj_create(parent);
    lv_obj_set_style_pad_row(obj, 0, 0);
    lv_obj_set_size(obj, LV_PCT(100), LV_SIZE_CONTENT);
    lv_obj_set_flex_flow(obj, LV_FLEX_FLOW_ROW);
    lv_obj_set_style_text_font(obj, font_medium, 0);
    lv_obj_clear_flag(obj, LV_OBJ_FLAG_SCROLLABLE);

    lv_obj_t* text_label = lv_label_create(obj);
    lv_label_set_text(text_label, name);
    lv_obj_set_size(text_label, 200, LV_SIZE_CONTENT);

    lv_obj_t* value_label = lv_label_create(obj);
	lv_obj_set_style_text_font(value_label, font_medium, 0);
    lv_label_set_text(value_label, val);   
    lv_obj_set_ext_click_area(value_label, 30);
    lv_label_set_long_mode(value_label, LV_LABEL_LONG_DOT);
    lv_obj_set_size(value_label, 200, LV_SIZE_CONTENT);
    if(able){
		lv_obj_set_user_data(value_label, description);
        lv_obj_add_flag(value_label, LV_OBJ_FLAG_CLICKABLE);
    

    lv_obj_add_event_cb(value_label, text_click_event, LV_EVENT_CLICKED, description);
	}
	
    return value_label;
}

lv_obj_t* create_number_editor(lv_obj_t* parent, char* name, int val,int able, cJSON* description)
{
    lv_obj_t* obj = lv_obj_create(parent);
    lv_obj_set_style_pad_row(obj, 0, 0);
    lv_obj_set_size(obj, LV_PCT(100), LV_SIZE_CONTENT);
    lv_obj_set_flex_flow(obj, LV_FLEX_FLOW_ROW);
    lv_obj_set_style_text_font(obj, &lv_font_montserrat_32, 0);
    lv_obj_clear_flag(obj, LV_OBJ_FLAG_SCROLLABLE);

    lv_obj_t* text_label = lv_label_create(obj);
    lv_label_set_text(text_label, name);
    lv_obj_set_size(text_label, 200, LV_SIZE_CONTENT);

    lv_obj_t* value_label = lv_label_create(obj);
    lv_label_set_text_fmt(value_label, "%d", val);
    lv_obj_add_flag(value_label, LV_OBJ_FLAG_CLICKABLE);
    lv_obj_set_ext_click_area(value_label, 30);
    lv_label_set_long_mode(value_label, LV_LABEL_LONG_DOT);
    lv_obj_set_size(value_label, 200, LV_SIZE_CONTENT);
	 if(able){
	 	lv_obj_set_user_data(value_label, description);
        lv_obj_add_flag(value_label, LV_OBJ_FLAG_CLICKABLE);
    
    lv_obj_add_event_cb(value_label, number_click_event, LV_EVENT_CLICKED, description);
	 	}
	
    return value_label;
}

lv_obj_t* create_slider_editor(lv_obj_t* parent, char* name, int val, cJSON* description)
{
    int min = cJSON_GetObjectItemCaseSensitive(description, "minimum")->valueint;
    int max = cJSON_GetObjectItemCaseSensitive(description, "maximum")->valueint;

    lv_obj_t* obj = lv_obj_create(parent);
    lv_obj_set_style_pad_row(obj, 0, 0);
    lv_obj_set_size(obj, LV_PCT(100), LV_SIZE_CONTENT);
    lv_obj_clear_flag(obj, LV_OBJ_FLAG_SCROLLABLE);

    lv_obj_t* text_label = lv_label_create(obj);
    lv_label_set_text(text_label, name);
    lv_obj_set_size(text_label, 200, LV_SIZE_CONTENT);

    lv_obj_t* value_label = lv_label_create(obj);
    lv_label_set_text_fmt(value_label, "%d", val);

    lv_obj_t* slider = lv_slider_create(obj);
    lv_obj_set_height(slider, 30);
    lv_slider_set_range(slider, min, max);
    lv_slider_set_value(slider, val, LV_ANIM_OFF);

    lv_obj_add_flag(slider, LV_OBJ_FLAG_FLEX_IN_NEW_TRACK);

    return slider;
}

lv_obj_t* create_switch_editor(lv_obj_t* parent, char* name, int val, cJSON* description)
{
    lv_obj_t* obj = lv_obj_create(parent);
    lv_obj_set_style_pad_row(obj, 0, 0);
    lv_obj_set_size(obj, LV_PCT(100), LV_SIZE_CONTENT);
    lv_obj_set_flex_flow(obj, LV_FLEX_FLOW_ROW);
    lv_obj_clear_flag(obj, LV_OBJ_FLAG_SCROLLABLE);

    lv_obj_t* text_label = lv_label_create(obj);
    lv_label_set_text(text_label, name);
    lv_obj_set_size(text_label, 200, LV_SIZE_CONTENT);

    lv_obj_t* sw = lv_switch_create(obj);
    lv_obj_add_state(sw, val ? LV_STATE_CHECKED : 0);

    return sw;
}

lv_obj_t* create_editor_menu(cJSON* rec_data, const cJSON* editabel, const cJSON* description)
{
	
	editor_menu_return = lv_scr_act();
	ui_record = ui_record_init();
    	
    rec_field = rec_data;
    //Description = tb_description;
    EditCheck=cJSON_GetObjectItem(description,"Check");
	EditTrans=cJSON_GetObjectItem(description,"Translate");
    //������������
    lv_obj_t* editmenu = lv_obj_create(ui_record);
    lv_obj_set_size(editmenu, 480, 800);
    lv_obj_set_style_pad_all(editmenu, 0, 0);
    lv_obj_clear_flag(editmenu, LV_OBJ_FLAG_SCROLLABLE);

    //������
    lv_obj_t* btnCont = lv_obj_create(editmenu);
    lv_obj_set_size(btnCont, 480, 100);
    lv_obj_set_style_pad_all(btnCont, 0, 0);
    lv_obj_set_y(btnCont, 700);
    //���水��
    lv_obj_t* saveBtn = lv_btn_create(btnCont);
    lv_obj_set_size(saveBtn, 120, 70);
    lv_obj_set_style_text_font(saveBtn, &lv_font_montserrat_32, 0);
    lv_obj_add_event_cb(saveBtn, save_editor_event, LV_EVENT_CLICKED, NULL);
    lv_obj_align(saveBtn, LV_ALIGN_LEFT_MID, 20, 0);
    lv_obj_set_style_radius(saveBtn, LV_RADIUS_CIRCLE, 0);
    lv_obj_set_style_bg_color(saveBtn, lv_color_make(230, 231, 232), 0);
    lv_obj_set_style_text_color(saveBtn, lv_color_hex(0xff0000), 0);
    lv_obj_t* savelabel = lv_label_create(saveBtn);
    //lv_label_set_text(savelabel, "save");
    vtk_label_set_text(savelabel, "save");
    lv_obj_center(savelabel);
    //删除按键
    lv_obj_t* delBtn = lv_btn_create(btnCont);
    lv_obj_set_size(delBtn, 120, 70);
    lv_obj_set_style_text_font(delBtn, &lv_font_montserrat_32, 0);
    lv_obj_add_event_cb(delBtn, del_record_event, LV_EVENT_CLICKED, NULL);
    lv_obj_align(delBtn, LV_ALIGN_CENTER, 0, 0);
    lv_obj_set_style_radius(delBtn, LV_RADIUS_CIRCLE, 0);
    lv_obj_set_style_bg_color(delBtn, lv_color_make(230, 231, 232), 0);
    lv_obj_set_style_text_color(delBtn, lv_color_hex(0xff0000), 0);
    lv_obj_t* dellabel = lv_label_create(delBtn);
    //lv_label_set_text(dellabel, "Delete");
    vtk_label_set_text(dellabel, "Delete");
    lv_obj_center(dellabel);
    //ȡ������
    lv_obj_t* cancelBtn = lv_btn_create(btnCont);
    lv_obj_align(cancelBtn, LV_ALIGN_RIGHT_MID, -20, 0);
    lv_obj_set_style_text_color(cancelBtn, lv_color_hex(0xff0000), 0);
    lv_obj_set_style_bg_color(cancelBtn, lv_color_make(230, 231, 232), 0);
    lv_obj_set_size(cancelBtn, 120, 70);
    lv_obj_set_style_text_font(cancelBtn, &lv_font_montserrat_32, 0);
    lv_obj_add_event_cb(cancelBtn, cancel_editor_event, LV_EVENT_CLICKED, NULL);
    lv_obj_set_style_radius(cancelBtn, LV_RADIUS_CIRCLE, 0);
    lv_obj_t* cancellabel = lv_label_create(cancelBtn);
    //lv_label_set_text(cancellabel, "cancel");
    vtk_label_set_text(cancellabel, "cancel");
    lv_obj_center(cancellabel);

    //�����༭��
    lv_obj_t* editCont = lv_obj_create(editmenu);
    lv_obj_set_size(editCont, 480, 700);
    lv_obj_set_flex_flow(editCont, LV_FLEX_FLOW_COLUMN);
    lv_obj_set_style_pad_all(editCont, 0, 0);
    lv_obj_set_style_pad_row(editCont, 0, 0);

    int rec_size = cJSON_GetArraySize(rec_data);
    int able_size = cJSON_GetArraySize(editabel);
int val;	
lv_obj_t* val_lable;
ValueLabelArray=cJSON_CreateObject();

    for (int i = 0; i < rec_size; i++)
    {
    	char name_buff[100];
        //获取一个字段
        cJSON* field = cJSON_GetArrayItem(rec_data, i);
	val=0;	
	#if 1
        for (int j = 0; j < able_size; j++)
        {   //获取是否编辑的字段
            cJSON* able = cJSON_GetArrayItem(editabel, j);
            //获取比较结果
            if(strcmp(able->valuestring, field->string)==0)
            {
			val=1;
			break;
		}

            //create_text_editor(editCont, field->string, field->valuestring, val);
        }  
	#endif
	if(GetJsonDataPro(EditTrans, field->string, name_buff)==0)
	{
		strcpy(name_buff,field->string);
	}
	
	if(cJSON_IsString(field))
		val_lable=create_text_editor(editCont, name_buff, field->valuestring, val,field);
	if(cJSON_IsNumber(field))
		val_lable=create_number_editor(editCont, name_buff, field->valueint, val,field);
	//cJSON_AddNumberToObject(ValueLabelArray,field->string,(int)val_lable);
    }
    return editmenu;
}

void editor_menu_special_process(char *edit_name)
{
	if(strcmp(edit_name,"IXG_ID")==0||strcmp(edit_name,"IXG_IM_ID")==0)
	{
		cJSON *addr_label=cJSON_GetObjectItem(ValueLabelArray,"IX_ADDR");
		cJSON *name_label=cJSON_GetObjectItem(ValueLabelArray,"IX_NAME");
		cJSON *ixg_id_label=cJSON_GetObjectItem(ValueLabelArray,"IXG_ID");
		cJSON *ixg_im_id_label=cJSON_GetObjectItem(ValueLabelArray,"IXG_IM_ID");
		if(ixg_id_label==NULL||ixg_im_id_label==NULL)
			return;
		int ixg_id=atoi(lv_label_get_text((lv_obj_t*)ixg_id_label->valueint));
		int ixg_im=atoi(lv_label_get_text((lv_obj_t*)ixg_im_id_label->valueint));
		char buf[100];
		if(addr_label!=NULL)
		{
			sprintf(buf,"%s%02d%02d01",GetSysVerInfo_bd(),ixg_id,ixg_im);

			lv_label_set_text((lv_obj_t*)addr_label->valueint,buf);
			cJSON_ReplaceItemInObject(rec_field, "IX_ADDR", cJSON_CreateString(buf));
		}
		if(name_label&&RegexCheck("^IXG[0-9]{1,2}-IM[0-9]{1,2}$",lv_label_get_text((lv_obj_t*)name_label->valueint))==1)
		{
			sprintf(buf,"IXG%d-IM%02d",ixg_id,ixg_im);
			lv_label_set_text((lv_obj_t*)name_label->valueint,buf);
			cJSON_ReplaceItemInObject(rec_field, "IX_NAME", cJSON_CreateString(buf));
		}
		
	}
}

static void FreeRecordEditor(void)
{
    if(editor_menu_return)
    {
        lv_disp_load_scr(editor_menu_return);
        editor_menu_return = NULL;
    }
		 
	if(ui_record)
    {
        lv_obj_del(ui_record); 
        ui_record = NULL;
    }
          
	if(ValueLabelArray)
    {
		cJSON_Delete(ValueLabelArray);
    	ValueLabelArray=NULL;
    }
}
static void save_editor_event(lv_event_t* e)
{  
    FreeRecordEditor();
    #if 0
    td.msg_data =  rec_field;
    td.msg_type = RECORD_MSG_SAVE_EXIT;
    API_SettingMenuProcess(&td);
    #endif
    EditOneDtIm(rec_field);
}

static void del_record_event(lv_event_t* e)
{  
    FreeRecordEditor();

    #if 0
    td.msg_data =  rec_field;
    td.msg_type = RECORD_MSG_DEL_EXIT;
    API_SettingMenuProcess(&td);
    #endif
    DeleteOneDtIm(rec_field);
}

static void cancel_editor_event(lv_event_t* e)
{   
    FreeRecordEditor();
    #if 0
    td.msg_data =  NULL;
    td.msg_type = RECORD_MSG_CANCEL_EXIT;
    API_SettingMenuProcess(&td); 
    #endif
    ExitDtImEdit();

    return;
}

static lv_obj_t* ui_record_init(void)
{
    lv_obj_t* recordScr = lv_obj_create(NULL);;
    lv_obj_clear_flag(recordScr, LV_OBJ_FLAG_SCROLLABLE);
    lv_obj_remove_style_all(recordScr);
    lv_obj_set_size(recordScr, lv_disp_get_hor_res(NULL), lv_disp_get_ver_res(NULL));
    lv_obj_align(recordScr, LV_ALIGN_TOP_LEFT, 0, 0);
    lv_obj_set_style_bg_color(recordScr, lv_palette_main(LV_PALETTE_GREY), 0);
    lv_obj_set_style_bg_opa(recordScr, LV_OPA_100, 0);
    lv_disp_load_scr(recordScr);
    return recordScr;
}

static lv_obj_t* ui_keyboard_init(void)
{
    lv_obj_t* ui_keyboard = lv_obj_create(NULL);
    lv_obj_clear_flag(ui_keyboard, LV_OBJ_FLAG_SCROLLABLE);
    lv_obj_remove_style_all(ui_keyboard);
    lv_obj_set_size(ui_keyboard, lv_disp_get_hor_res(NULL), lv_disp_get_ver_res(NULL));
    lv_obj_align(ui_keyboard, LV_ALIGN_TOP_LEFT, 0, 0);
    lv_obj_set_style_bg_color(ui_keyboard, lv_palette_main(LV_PALETTE_GREY), 0);
    lv_obj_set_style_bg_opa(ui_keyboard, LV_OPA_100, 0);
    lv_disp_load_scr(ui_keyboard);
    return ui_keyboard;
}