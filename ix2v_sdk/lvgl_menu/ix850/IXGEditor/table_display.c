#include "table_display.h"
#include "menu_common.h"
#include "obj_IXS_Proxy.h"


void load_table_from_json(lv_obj_t* obj, cJSON* json, int page);
static void draw_table_event_cb(lv_event_t* e);
static void table_exit_event(lv_event_t* e);
static void left_click_event(lv_event_t* e);
static void right_click_event(lv_event_t* e);
static void add_title(lv_obj_t* parent);
static void add_filter(lv_obj_t* parent, char* id);
static void add_table(lv_obj_t* parent, cJSON* data, int cnt_page);
static void add_change_page(lv_obj_t* parent, int cnt_page, int max_page, int size);
static void add_control(lv_obj_t* parent);
static void page_change_event_cb(lv_event_t* e);
static void IXG_edit_event_cb(lv_event_t* e);
static void table_edit_event_cb(lv_event_t* e);
static void table_add_event(lv_event_t* e);
static void cnt_change_event_cb(lv_event_t* e);
static void ui_table_init(void);
static cJSON* IXGTbTransLate=NULL;
static lv_obj_t* kb;
TABLE_DISPLAY* td;
lv_obj_t* ui_table = NULL;
lv_obj_t* back_scr = NULL;
static lv_obj_t* dispagelabel = NULL;
static lv_obj_t* discntlabel = NULL;
/*****************
 * 初始化IXG表浏览器
 * view ： 表显示的所用的json数据
 * page ： 初始化时显示的页数
 * Translate ：
 * **************/
TABLE_DISPLAY* create_table_display_menu(cJSON* view,int page,cJSON *Translate)
{
    back_scr = lv_scr_act();
	IXGTbTransLate=Translate;
    ui_table_init();
    lv_disp_load_scr(ui_table);

    td = (TABLE_DISPLAY*)lv_mem_alloc(sizeof(TABLE_DISPLAY));
    lv_memset_00(td, sizeof(TABLE_DISPLAY));

    lv_obj_t* displaymenu = lv_obj_create(ui_table);
    lv_obj_set_size(displaymenu, 480, 800);
    lv_obj_set_style_pad_all(displaymenu, 0, 0);
    lv_obj_clear_flag(displaymenu, LV_OBJ_FLAG_SCROLLABLE);
    td->table_parent = displaymenu;
  
    td->cnt_data = view;

    int size = cJSON_GetArraySize(view);
    char buf[100];
    int cnt = size % 8;
    if (cnt == 0)
    {
        cnt = size / 8;
    }
    else {
        cnt = size / 8 + 1;
    }

    td->cnt_page = page;
    td->min_page = 1;
    td->max_page = cnt;
    td->table = NULL;

    add_title(displaymenu);
    add_filter(displaymenu, "");     
    add_change_page(displaymenu, page, cnt, size);
    add_control(displaymenu);
    add_table(td->table_parent, view, td->cnt_page);
    return td;
}
/*****************
 * 刷新IXG表显示数据
 * tb_disp ： ixg表浏览器所用结构
 * view ：用于刷新表的数据
 * page ：表格显示的当前页
 * **************/
void ixgview_data_fresh(TABLE_DISPLAY* tb_disp,cJSON* view,int page)
{
	if(tb_disp==NULL)
		return;
	//tb_disp->init_data = view;
    tb_disp->cnt_data = view;
int size = cJSON_GetArraySize(view);
    int cnt = size % 8;
    if (cnt == 0)
    {
        cnt = size / 8;
    }
    else {
        cnt = size / 8 + 1;
    }

    tb_disp->cnt_page = page;
    tb_disp->min_page = 1;
    tb_disp->max_page = cnt;

	  add_table(tb_disp->table_parent, tb_disp->cnt_data, tb_disp->cnt_page);
}
/*****************
 * 创建标题栏
 * parent ：父对象
 * **************/
static void add_title(lv_obj_t* parent)
{
    lv_obj_t* titleCont = lv_obj_create(parent);
    lv_obj_set_size(titleCont, 480, 80);
    lv_obj_set_style_pad_all(titleCont, 0, 0);
    lv_obj_t* titlelabel = lv_label_create(titleCont);
    lv_obj_set_style_text_font(titlelabel, &lv_font_montserrat_32, 0);
    //lv_obj_set_style_text_color(titlelabel, lv_color_hex(0xff0000), 0);
    lv_obj_center(titlelabel);
    //lv_label_set_text(titlelabel, "IXG IM Management");
    vtk_label_set_text(titlelabel, "IXG IM Management");
}
/*****************
 * 创建过滤器栏
 * parent ：父对象
 * id ： 当前过滤器使用的IXG-ID
 * **************/
static void add_filter(lv_obj_t* parent,char* id)
{
	char text[100];
    lv_obj_t* filterCont = lv_obj_create(parent);
    lv_obj_set_style_pad_row(filterCont, 0, 0);
    lv_obj_set_size(filterCont, LV_PCT(100), 100);
    lv_obj_set_flex_flow(filterCont, LV_FLEX_FLOW_ROW);
    lv_obj_set_style_text_font(filterCont, &lv_font_montserrat_32, 0);
    lv_obj_clear_flag(filterCont, LV_OBJ_FLAG_SCROLLABLE);
    lv_obj_set_y(filterCont, 80);

    lv_obj_t* filter_label = lv_label_create(filterCont);
	if(GetJsonDataPro(IXGTbTransLate, "IXG_ID", text)==0)
	{
		strcpy(text,"IXG_ID");
	}	
    //lv_label_set_text(filter_label, text);
    vtk_label_set_text(filter_label, text);
    lv_obj_set_size(filter_label, 200, LV_SIZE_CONTENT);

    lv_obj_t* ta = lv_textarea_create(filterCont);
    lv_textarea_add_text(ta, id);
    lv_obj_set_size(ta, 200, LV_SIZE_CONTENT);
    lv_obj_clear_state(ta, LV_STATE_FOCUSED);

    kb = lv_keyboard_create(ui_table);
    lv_obj_set_style_text_font(kb, &lv_font_montserrat_26, 0);
    lv_obj_add_flag(kb, LV_OBJ_FLAG_HIDDEN);

    lv_obj_add_event_cb(ta, IXG_edit_event_cb, LV_EVENT_ALL, NULL);
}
/*****************
 *调用键盘修改IXG-ID进行过滤
 * **************/
static void IXG_edit_event_cb(lv_event_t* e)
{
    lv_event_code_t code = lv_event_get_code(e);
    lv_obj_t* ta = lv_event_get_target(e);
    cJSON* where;
	int i;
    if (code == LV_EVENT_CLICKED) {
        if (lv_indev_get_type(lv_indev_get_act()) != LV_INDEV_TYPE_KEYPAD) {
            lv_obj_add_state(ta, LV_STATE_FOCUSED);
            lv_keyboard_set_textarea(kb, ta);

            lv_keyboard_set_mode(kb, LV_KEYBOARD_MODE_NUMBER);      //设置键盘模式为纯数字

            lv_obj_clear_flag(kb, LV_OBJ_FLAG_HIDDEN);
        }
    }
    else if (code == LV_EVENT_CANCEL) {

        lv_obj_add_flag(kb, LV_OBJ_FLAG_HIDDEN);

    }
    else if (code == LV_EVENT_READY)
    {
        char* text = lv_textarea_get_text(ta);
        int i=0;
        while(text[i]!=0&&text[i]>='0'&&text[i]<='9')
            i++;
         
       // if(cJSON_IsNumber(value))
       if(text[i]==0&&i>0)                  //按IXG-ID过滤 
        {
            where = cJSON_CreateObject();
	        cJSON_AddNumberToObject(where, "IXG_ID", atoi(text));

	        //lv_msg_send(IXG_MSG_CHANGE_FITER, where);

	        lv_obj_add_flag(kb, LV_OBJ_FLAG_HIDDEN);

            td->msg_data =  where;
            td->msg_type = IXG_MSG_CHANGE_FITER;
            API_SettingMenuProcess(td);  

            cJSON_Delete(where);
            lv_obj_clear_state(ta,LV_STATE_FOCUSED);
        }
	  else if(text[i]==0&&i==0)             //显示全部记录 
	  {
            where = cJSON_CreateObject();
                // cJSON_AddNumberToObject(where, "IXG_ID", atoi(text));

                //lv_msg_send(IXG_MSG_CHANGE_FITER, where);

            lv_obj_add_flag(kb, LV_OBJ_FLAG_HIDDEN);
            td->msg_data =  where;
            td->msg_type = IXG_MSG_CHANGE_FITER;
            API_SettingMenuProcess(td); 
            cJSON_Delete(where);	
            lv_obj_clear_state(ta,LV_STATE_FOCUSED);
	  }
	  else
	  {
          LV_API_TIPS(" Error! Please enter it again!",4);
		    //API_TIPS(" Error! Please enter it again!");
	  }
	  	       
    }
}
/*****************
 * 显示一张表
 * parent ：table的父对象
 * data ： 用于显示的json数据
 * cnt_page ： 当前页
 * **************/
static void add_table(lv_obj_t* parent,cJSON* data,int cnt_page)
{   
	if(td->table == NULL){
        lv_obj_t* table = lv_table_create(parent);
        lv_obj_set_size(table, 480, 440);
        lv_obj_set_y(table, 180);
        lv_obj_add_event_cb(table, draw_table_event_cb, LV_EVENT_DRAW_PART_BEGIN, NULL);
        lv_obj_add_event_cb(table, table_edit_event_cb, LV_EVENT_VALUE_CHANGED, NULL);
        td->table = table; 
    }

    lv_table_set_row_cnt(td->table,0);
    lv_table_set_col_cnt(td->table,0);

    lv_table_set_col_width(td->table, 0, 200);
    lv_table_set_col_width(td->table, 1, 150);
    lv_table_set_col_width(td->table, 2, 150);
    lv_table_set_col_width(td->table, 3, 120);
    lv_table_set_col_width(td->table, 4, 120);
    lv_table_set_col_width(td->table, 5, 200);

    load_table_from_json(td->table, data, cnt_page);   
}
/*****************
 * 表编辑事件,表格子被点击以后触发
 * **************/
static void table_edit_event_cb(lv_event_t* e)
{
    lv_obj_t* obj = lv_event_get_target(e);

    uint16_t col;
    uint16_t row;
    lv_table_get_selected_cell(obj, &row, &col);    //获取格子所在的行,列
    uint16_t cnt = lv_table_get_row_cnt(obj);
    if (row >= cnt || row == 0)
    {
        return;
    }

    cJSON* data = cJSON_GetArrayItem(td->cnt_data, (td->cnt_page-1)*8+row-1);       //获取格子所在的整条记录
	//printf("11111111 row=%d,cnt=%d,td->cnt_page=%d\n",row,cnt,td->cnt_page);
    
    //printf_json((cJSON*)data,__func__);
	//printf_json((cJSON*)td->cnt_data,__func__);
    td->msg_data =  data;
    td->msg_type = IXG_MSG_EDIT_RECORD;
    API_SettingMenuProcess(td); 
}
/*****************
 * 创建页面显示栏
 * parent : 父对象
 * cnt_page : 当前页
 * max_page ：最大页数
 * size ： 表格总记录数
 * **************/
static void add_change_page(lv_obj_t* parent, int cnt_page,int max_page,int size)
{
    //��ҳ���ƿ�
    lv_obj_t* pageControl = lv_obj_create(parent);
    lv_obj_set_size(pageControl, 480, 90);
    lv_obj_set_style_pad_all(pageControl, 0, 0);
    lv_obj_set_y(pageControl, 620);

    lv_obj_t* leftBtn = lv_btn_create(pageControl);
    lv_obj_align(leftBtn, LV_ALIGN_LEFT_MID, 20, 0);
    lv_obj_set_style_text_color(leftBtn, lv_color_hex(0xff0000), 0);
    lv_obj_set_style_bg_color(leftBtn, lv_color_make(230, 231, 232), 0);
    lv_obj_set_size(leftBtn, 80, 70);
    lv_obj_set_style_text_font(leftBtn, &lv_font_montserrat_32, 0);
    lv_obj_add_event_cb(leftBtn, left_click_event, LV_EVENT_CLICKED, NULL);
    lv_obj_set_style_radius(leftBtn, LV_RADIUS_CIRCLE, 0);
    lv_obj_t* leftlabel = lv_label_create(leftBtn);
    lv_label_set_text(leftlabel, LV_SYMBOL_LEFT);
    lv_obj_center(leftlabel);

    char buf[100];
    sprintf(buf, "%d/%d", cnt_page, max_page);

    dispagelabel = lv_label_create(pageControl);
    lv_label_set_text(dispagelabel, buf);
    lv_obj_set_style_text_font(dispagelabel, &lv_font_montserrat_32, 0);
    lv_obj_align(dispagelabel, LV_ALIGN_CENTER, -60, 0);

    lv_obj_t* rightBtn = lv_btn_create(pageControl);
    lv_obj_align(rightBtn, LV_ALIGN_CENTER, 50, 0);
    lv_obj_set_style_text_color(rightBtn, lv_color_hex(0xff0000), 0);
    lv_obj_set_style_bg_color(rightBtn, lv_color_make(230, 231, 232), 0);
    lv_obj_set_size(rightBtn, 80, 70);
    lv_obj_set_style_text_font(rightBtn, &lv_font_montserrat_32, 0);
    lv_obj_add_event_cb(rightBtn, right_click_event, LV_EVENT_CLICKED, NULL);
    lv_obj_set_style_radius(rightBtn, LV_RADIUS_CIRCLE, 0);
    lv_obj_t* rightlabel = lv_label_create(rightBtn);
    lv_label_set_text(rightlabel, LV_SYMBOL_RIGHT);
    lv_obj_center(rightlabel);

    sprintf(buf, "%d", size);
    discntlabel = lv_label_create(pageControl);
    lv_label_set_text(discntlabel, buf);
    lv_obj_set_style_text_font(discntlabel, &lv_font_montserrat_32, 0);
    lv_obj_align(discntlabel, LV_ALIGN_RIGHT_MID, -50, 0);
}

/*****************
 * 创建底部表控制栏
 * parent : 父对象
 * **************/
static void add_control(lv_obj_t* parent)
{
    lv_obj_t* btnCont = lv_obj_create(parent);
    lv_obj_set_size(btnCont, 480, 90);
    lv_obj_set_style_pad_all(btnCont, 0, 0);
    lv_obj_set_y(btnCont, 710);
    //返回
    lv_obj_t* backBtn = lv_btn_create(btnCont);
    lv_obj_align(backBtn, LV_ALIGN_LEFT_MID, 50, 0);
    //lv_obj_set_style_bg_color(backBtn, lv_color_make(230, 231, 232), 0);
    lv_obj_set_size(backBtn, 120, 70);
    lv_obj_set_style_text_font(backBtn, &lv_font_montserrat_32, 0);
    lv_obj_add_event_cb(backBtn, table_exit_event, LV_EVENT_CLICKED, NULL);
    lv_obj_set_style_radius(backBtn, LV_RADIUS_CIRCLE, 0);
    lv_obj_t* backlabel = lv_label_create(backBtn);
    lv_label_set_text(backlabel, LV_SYMBOL_BACKSPACE);
    lv_obj_center(backlabel);
    //添加
    lv_obj_t* addBtn = lv_btn_create(btnCont);
    lv_obj_set_size(addBtn, 120, 70);
    lv_obj_set_style_text_font(addBtn, &lv_font_montserrat_32, 0);
    lv_obj_add_event_cb(addBtn, table_add_event, LV_EVENT_CLICKED, NULL);
    lv_obj_align(addBtn, LV_ALIGN_RIGHT_MID, -50, 0);
    lv_obj_set_style_radius(addBtn, LV_RADIUS_CIRCLE, 0);
    //lv_obj_set_style_bg_color(addBtn, lv_color_make(230, 231, 232), 0);
    lv_obj_t* addlabel = lv_label_create(addBtn);
    lv_label_set_text(addlabel, "+");
    lv_obj_center(addlabel);
}
/*****************
 * 表绘画事件
 * 控制表格字体大小,位置,背景色
 * **************/
static void draw_table_event_cb(lv_event_t* e)
{
    lv_obj_t* obj = lv_event_get_target(e);
    lv_obj_draw_part_dsc_t* dsc = lv_event_get_draw_part_dsc(e);
    /*If the cells are drawn...*/
    if (dsc->part == LV_PART_ITEMS) {
        uint32_t row = dsc->id / lv_table_get_col_cnt(obj);
        uint32_t col = dsc->id - row * lv_table_get_col_cnt(obj);

        dsc->label_dsc->font = &lv_font_montserrat_24;
        dsc->label_dsc->align = LV_TEXT_ALIGN_CENTER;

        /*Make the texts in the first cell center aligned*/
        if (row == 0) {
            dsc->label_dsc->align = LV_TEXT_ALIGN_CENTER;
            dsc->rect_dsc->bg_color = lv_color_mix(lv_palette_main(LV_PALETTE_BLUE), dsc->rect_dsc->bg_color, LV_OPA_20);
            dsc->rect_dsc->bg_opa = LV_OPA_COVER;
        }
    }
}
/*****************
 * 加载表的数据
 * obj ：table对象
 * json ： 用于加载的json数据
 * page ： 当前页
 * **************/
void load_table_from_json(lv_obj_t* obj, cJSON* json,int page)
{
    int size = cJSON_GetArraySize(json);
    char text[100];
    for (int i = 0; i < 8; i++)
    {
        int cnt = ((page - 1) * 8) + i;
        cJSON* item = cJSON_GetArrayItem(json, cnt);
        if (item == NULL)
        {
            break;
        }
        int length = cJSON_GetArraySize(item);
        for (int j = 0; j < length; j++)
        {
            cJSON* val = cJSON_GetArrayItem(item, j);
            if (i == 0)
            {
                if(GetJsonDataPro(IXGTbTransLate, val->string, text)==0)
                {
                    strcpy(text,val->string);
                }	
                lv_table_set_cell_value(obj, i, j, text);
            }
            if (cJSON_IsString(val))
            {
                lv_table_set_cell_value(obj, i + 1, j, val->valuestring);
            }
            else if (cJSON_IsNumber(val))
            {
                sprintf(text, "%d", val->valueint);
                lv_table_set_cell_value(obj, i + 1, j, text);
            }
        }
    }
    if(discntlabel)
        lv_label_set_text_fmt(discntlabel,"%d",size);
    if(dispagelabel)
        lv_label_set_text_fmt(dispagelabel,"%d/%d",page,td->max_page);
}
/*****************
 * 退出表浏览器
 * **************/
static void table_exit_event(lv_event_t* e)
{
    lv_disp_load_scr(back_scr);
    td->msg_data =  NULL;
    td->msg_type = IXG_MSG_EXIT;
    API_SettingMenuProcess(td); 
}
/*****************
 * 添加一条记录,进入表编辑器
 * **************/
static void table_add_event(lv_event_t* e)
{
    td->msg_data =  NULL;
    td->msg_type = IXG_MSG_ADD_RECORD;
    API_SettingMenuProcess(td); 
}
/*****************
 * 左翻页
 * **************/
static void left_click_event(lv_event_t* e)
{
    //printf("%d\n", td->cnt_page);
    td->cnt_page--;
    if (td->cnt_page <= 0)
    {
        td->cnt_page = td->max_page;
    }
    //printf("%d\n", td->cnt_page);

    add_table(td->table_parent, td->cnt_data, td->cnt_page);
}
/*****************
 * 右翻页
 * **************/
static void right_click_event(lv_event_t* e)
{
    //printf("%d\n", td->cnt_page);
    td->cnt_page++;
    if (td->cnt_page >td->max_page)
    {
        td->cnt_page = 1;
    }
    //printf("%d\n", td->cnt_page);
    add_table(td->table_parent, td->cnt_data, td->cnt_page);
}
/*****************
 * 表删除
 * td ：表浏览器初始化时返回的结构指针
 * **************/
bool table_delete(TABLE_DISPLAY* td)
{
    if (td)
    {   

        if (td->table)
        {
            lv_obj_del(td->table);
            td->table = NULL;
        }

        if (td->table_parent)
        {
            lv_obj_del(td->table_parent);
            td->table_parent = NULL;
        }

        if (td->cnt_data)
        {
            //cJSON_Delete(td->cnt_data);
            td->cnt_data = NULL;
        }

        if(kb){
            lv_obj_del(kb);
            kb = NULL;
        }

        if(ui_table){
            lv_obj_del(ui_table);
            ui_table = NULL;
        }

	#if 0

        if(IXGTbTransLate){
            cJSON_Delete(IXGTbTransLate);
            IXGTbTransLate = NULL;
        }

	#endif

        lv_mem_free(td);
        td = NULL;
        return true;
    }
    else
        return false;
}
/*****************
 * 初始化表浏览器活动用的scr
 * **************/
static void ui_table_init(void)
{
    ui_table = lv_obj_create(NULL);
    lv_obj_clear_flag(ui_table, LV_OBJ_FLAG_SCROLLABLE);
    lv_obj_remove_style_all(ui_table);
    lv_obj_set_size(ui_table, lv_disp_get_hor_res(NULL), lv_disp_get_ver_res(NULL));
    lv_obj_align(ui_table, LV_ALIGN_TOP_LEFT, 0, 0);
    lv_obj_set_style_bg_color(ui_table, lv_palette_main(LV_PALETTE_GREY), 0);
    lv_obj_set_style_bg_opa(ui_table, LV_OPA_100, 0);
}