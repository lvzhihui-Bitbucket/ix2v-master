#ifndef RES_EDITOR_H_
#define RES_EDITOR_H_
#include "Common_Record.h"

typedef struct 
{   
    cJSON* resRecord;
    RECORD_CONTROL* recordCtrl;
}RES_Editor_MENU_T;

//resRecord：编辑的记录,{"IX_ADDR":"","IX_NAME":"","G_NBR":"","L_NBR":""}
//editabel:可编辑字段，["IX_NAME","G_NBR","L_NBR"]
void CreateResEditOneMenu(const cJSON* resRecord, const cJSON* editabel, int deleteFlag);
void CreateResRecordEditorMenu(const cJSON* resRecord, const cJSON* editabel, RES_Editor_MENU_T* autoMenu, BackCallback cancelCb, BtnClickCallback btnCb, const char* title, char* btnString, ...);

#endif 

