#include "menu_utility.h"
#include "cJSON.h"
#include "obj_lvgl_msg.h"
#include "obj_ResEditor.h"
#include "task_Event.h"
#include "obj_TableSurver.h"
#include "Common_Record.h"
#include "obj_TableSurver.h"

#define RES_EDITOR_DESCRIPTION  "{\"Check\":{\"G_NBR\":\"^[0-9]{0,10}$\",\"L_NBR\":\"^[0-9]{0,10}$\",\"IX_ADDR\":\"^[0-9]{10}$\",\"GW_ADDR\":\"^[0-9]{10}$\",\"GW_ID\":\"(^[1-9]{1}$)|(^(1|2)[0-9]{1}$)|(^3[0-2]{1}$)\",\"IM_ID\":\"(^[1-9]{1}$)|(^(1|2)[0-9]{1}$)|(^3[0-2]{1}$)\"},\"Translate\":{\"IX_ADDR\":\"IX_ADDR\"}}"
#define RES_EDITOR_EdI_TB  "[\"IX_NAME\",\"G_NBR\",\"L_NBR\"]"

static RES_Editor_MENU_T resEditOneMenu = {.resRecord = NULL, .recordCtrl = NULL}; 


void CreateResRecordEditorMenu(const cJSON* resRecord, const cJSON* editabel, RES_Editor_MENU_T* autoMenu, BackCallback cancelCb, BtnClickCallback btnCb, const char* title, char* btnString, ...)
{
	char* name;
	//编辑的记录
	autoMenu->resRecord = cJSON_Duplicate(resRecord, 1);

    cJSON* editorTable;
    if(editabel)
    {
        editorTable =  cJSON_Duplicate(editabel, 1);
    }
    else
    {
        editorTable =  cJSON_Parse(RES_EDITOR_EdI_TB);
    }
		
	cJSON* resEditordescription = cJSON_Parse(RES_EDITOR_DESCRIPTION);

	cJSON* func = cJSON_CreateObject();
	cJSON* btnname = cJSON_AddArrayToObject(func,"BtnName");

	va_list valist;
	va_start(valist, btnString);
	for(name = btnString; name; name = va_arg(valist, char*))
	{
		cJSON_AddItemToArray(btnname, cJSON_CreateString(name));
	}
	va_end(valist);

	cJSON_AddNumberToObject(func, "BtnCallback", (int)btnCb);
	cJSON_AddNumberToObject(func, "BackCallback", (int)cancelCb);

    RC_MenuDisplay(&(autoMenu->recordCtrl), autoMenu->resRecord, editorTable, resEditordescription, func, title);

    cJSON_Delete(editorTable);
    cJSON_Delete(resEditordescription);
    cJSON_Delete(func);
}


void CloseHybridMatEditRecordMenu(void)
{      
    if(resEditOneMenu.recordCtrl)
    {
        RC_MenuDelete(&resEditOneMenu.recordCtrl);
    }

    if(resEditOneMenu.resRecord)
    {
        cJSON_Delete(resEditOneMenu.resRecord);
        resEditOneMenu.resRecord = NULL;
    }
}

static int SaveResRecord(const cJSON* record)
{
    int ret = 0;

    cJSON* where = cJSON_CreateObject();
    cJSON_AddNumberToObject(where, IX2V_GW_ID, GetEventItemInt(record,  IX2V_GW_ID));
    cJSON_AddNumberToObject(where, IX2V_IM_ID, GetEventItemInt(record,  IX2V_IM_ID));
    if(API_TB_UpdateByName(TB_NAME_HYBRID_MAT, where, record))
    {
	    API_TableModificationInform(TB_NAME_HYBRID_MAT, NULL);
		MenuRES_Editor_Refresh();
        ret = 1;
    }
    cJSON_Delete(where);

    if(ret)
    {
        API_Event_By_Name(Event_MAT_Update);
    }

    return ret;
}

static int DeleteResRecord(const cJSON* record)
{
    int ret = 0;

    cJSON* where = cJSON_CreateObject();
    cJSON_AddNumberToObject(where, IX2V_GW_ID, GetEventItemInt(record,  IX2V_GW_ID));
    cJSON_AddNumberToObject(where, IX2V_IM_ID, GetEventItemInt(record,  IX2V_IM_ID));
    if(API_TB_DeleteByName(TB_NAME_HYBRID_MAT, where))
    {
		MenuRES_Editor_Refresh();
        API_TableModificationInform(TB_NAME_HYBRID_MAT, NULL);
        ret = 1;
    }
    cJSON_Delete(where);

    if(ret)
    {
        API_Event_By_Name(Event_MAT_Update);
    }

    return ret;
}

static void CancelCallback(const char* type, void* user_data)
{  
	CloseHybridMatEditRecordMenu();
}

static void FunctionCallback(const char* type, void* user_data)
{
    if(type)
    {
        if(!strcmp(type, "Save"))
        {
            SaveResRecord(resEditOneMenu.resRecord);
            CloseHybridMatEditRecordMenu();
        }
        else if(!strcmp(type, "Delete"))
        {
            DeleteResRecord(resEditOneMenu.resRecord);
            CloseHybridMatEditRecordMenu();
        }
    }
} 


//resRecord：编辑的记录,{"IX_ADDR":"","IX_NAME":"","G_NBR":"","L_NBR":""}
//editabel:可编辑字段，["IX_NAME","G_NBR","L_NBR"]
void CreateResEditOneMenu(const cJSON* resRecord, const cJSON* editabel, int deleteFlag)
{
    CreateResRecordEditorMenu(resRecord, editabel, &resEditOneMenu, CancelCallback, FunctionCallback, "Edit MAT", "Save", deleteFlag ? "Delete" : NULL, NULL);
}