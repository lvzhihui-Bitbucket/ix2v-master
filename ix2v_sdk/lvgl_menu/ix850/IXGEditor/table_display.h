#ifndef TABLE_DISPLAY_H_
#define TABLE_DISPLAY_H_

#include "lv_ix850.h"

typedef struct 
{   
    cJSON* msg_data;
    int msg_type;           
    lv_obj_t* table_parent;        
    lv_obj_t* table;          
    cJSON* cnt_data;
    int cnt_page;
    int min_page;
    int max_page;
}TABLE_DISPLAY;

void table_test(void);
bool table_delete(TABLE_DISPLAY* td);
void lv_msg_table_handler(void* s, lv_msg_t* m);
TABLE_DISPLAY* create_table_display_menu(cJSON* view, int page,cJSON *Translate);
void ixgview_data_fresh(TABLE_DISPLAY* tb_disp,cJSON* view,int page);

#define IXG_MSG_CHANGE_FITER 		230703901
#define IXG_MSG_CHANGE_PAGE 		230703902
#define IXG_MSG_EXIT 				230703903
#define IXG_MSG_ADD_RECORD 		    230706901
#define IXG_MSG_EDIT_RECORD 		230706902
#define IXG_MSG_CHANGE_CNT          230712901

#define RECORD_MSG_SAVE_EXIT 230707901
#define RECORD_MSG_DEL_EXIT 230707902
#define RECORD_MSG_CANCEL_EXIT 230707903

#endif 
