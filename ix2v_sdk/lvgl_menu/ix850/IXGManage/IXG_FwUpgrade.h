#ifndef IXG_FWUPGRADE_H_
#define IXG_FWUPGRADE_H_

#include "lv_ix850.h"

lv_obj_t* FW_upgrade_menu(void);
static void back_fw_event(lv_event_t* e);
static void code_input_event(lv_event_t* e);
void fw_create_string_keyboard_menu(lv_obj_t* target, char* val);
static void code_edit_event_cb(lv_event_t* e);
void create_fw_menu(lv_obj_t* target);
static void fw_choose_event(lv_event_t* e);
static void fw_exit_event(lv_event_t* e);
static lv_obj_t* IXG_FW_init(void);
static void select_fw_event(lv_event_t* e);
static lv_obj_t* fw_keyboard_init(void);
int GetIXGIp(void);
static void fw_refresh_event(lv_event_t* e);
static void fw_install_event(lv_event_t* e);
void keyboard_delete(void);
void fw_delete(void);
#endif 