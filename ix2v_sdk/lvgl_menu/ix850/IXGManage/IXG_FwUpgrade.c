#include "IXG_FwUpgrade.h"
#include "obj_lvgl_msg.h"

static lv_obj_t* fw_manage = NULL;
static lv_obj_t* fw_back_scr = NULL;
static lv_obj_t* ui_keyboard;
static lv_obj_t* editor_keyboard_return;
static lv_obj_t* kb;
const char *updatestr[2] =
{
	"UpdateServer",
	"UpdateCode",
};
void* ta_msg;
char Code[9] = {0};
char Server[40] = {0};
/**********************
 * 初始化固件升级页面
 * 返回值 ：页面本身的容器对象
 * ********************/
lv_obj_t* FW_upgrade_menu(void)
{
    OnlineFwUpgrade_init();

    fw_back_scr = lv_scr_act();
    fw_manage = IXG_FW_init();

    lv_obj_t* selectmenu = lv_obj_create(fw_manage);
    //lv_obj_remove_style_all(selectmenu);
    lv_obj_set_size(selectmenu, 480, 800);
    lv_obj_set_style_pad_all(selectmenu, 0, 0);
    lv_obj_clear_flag(selectmenu, LV_OBJ_FLAG_SCROLLABLE);

    IXG_add_title(selectmenu, "FW select");

    lv_obj_t* btnCont = lv_obj_create(selectmenu);
    lv_obj_set_size(btnCont, 480, 100);
    lv_obj_set_style_pad_all(btnCont, 0, 0);
    lv_obj_set_y(btnCont, 700);

    lv_obj_t* saveBtn = lv_btn_create(btnCont);
    lv_obj_set_size(saveBtn, 120, 70);
    lv_obj_set_style_text_font(saveBtn, &lv_font_montserrat_28, 0);
    lv_obj_add_event_cb(saveBtn, back_fw_event, LV_EVENT_CLICKED, NULL);
    lv_obj_align(saveBtn, LV_ALIGN_LEFT_MID, 20, 0);
    lv_obj_set_style_radius(saveBtn, LV_RADIUS_CIRCLE, 0);
    lv_obj_set_style_bg_color(saveBtn, lv_color_make(230, 231, 232), 0);
    lv_obj_set_style_text_color(saveBtn, lv_color_black(), 0);
    lv_obj_t* savelabel = lv_label_create(saveBtn);
    lv_label_set_text(savelabel, LV_SYMBOL_BACKSPACE);
    lv_obj_center(savelabel);

    lv_obj_t* cancelBtn = lv_btn_create(btnCont);
    lv_obj_align(cancelBtn, LV_ALIGN_CENTER, 0, 0);
    lv_obj_set_style_text_color(cancelBtn, lv_color_black(), 0);
    lv_obj_set_style_bg_color(cancelBtn, lv_color_make(230, 231, 232), 0);
    lv_obj_set_size(cancelBtn, 120, 70);
    lv_obj_set_style_text_font(cancelBtn, &lv_font_montserrat_28, 0);
    lv_obj_add_event_cb(cancelBtn, fw_install_event, LV_EVENT_CLICKED, NULL);
    lv_obj_set_style_radius(cancelBtn, LV_RADIUS_CIRCLE, 0);
    lv_obj_t* cancellabel = lv_label_create(cancelBtn);
    //lv_label_set_text(cancellabel, "check");
    vtk_label_set_text(cancellabel, "check");
    lv_obj_center(cancellabel);

    lv_obj_t* installBtn = lv_btn_create(btnCont);
    lv_obj_align(installBtn, LV_ALIGN_RIGHT_MID, -20, 0);
    lv_obj_set_style_text_color(installBtn, lv_color_black(), 0);
    lv_obj_set_style_bg_color(installBtn, lv_color_make(230, 231, 232), 0);
    lv_obj_set_size(installBtn, 120, 70);
    lv_obj_set_style_text_font(installBtn, &lv_font_montserrat_28, 0);
    lv_obj_add_event_cb(installBtn, fw_install_event, LV_EVENT_CLICKED, NULL);
    lv_obj_set_style_radius(installBtn, LV_RADIUS_CIRCLE, 0);
    lv_obj_t* installlabel = lv_label_create(installBtn);
    //lv_label_set_text(installlabel, "install");
    vtk_label_set_text(installlabel, "install");
    lv_obj_center(installlabel);

    lv_obj_t* editCont = lv_obj_create(selectmenu);
    lv_obj_remove_style_all(editCont); 
    lv_obj_set_size(editCont, 480, 620);
    lv_obj_set_y(editCont, 80);
    lv_obj_set_flex_flow(editCont, LV_FLEX_FLOW_COLUMN);
    lv_obj_set_style_pad_row(editCont, 0, 0);
    
    IXG_load_text(editCont,"Update Server",Server,select_fw_event);         //创建sever栏,点击值后跳转选择服务器页面
    IXG_load_text(editCont,"Download Code",Code,code_input_event);          //创建code栏,点击值后跳转键盘页面

    //创建文本框,IXG固件更新上报的消息显示在该文本框中
    lv_obj_t* text = lv_textarea_create(editCont);                          
    lv_obj_set_size(text, LV_PCT(100), 400);
    lv_textarea_set_placeholder_text(text, "Software upgrade, please wait!");
    lv_obj_set_style_text_font(text, &lv_font_montserrat_28, 0);
    ta_msg = lv_msg_subsribe_obj(FWUPGRADE_RECEIVE, text, NULL);
    lv_obj_add_event_cb(text, fw_refresh_event, LV_EVENT_ALL, NULL);
     
    lv_disp_load_scr(fw_manage);
    return selectmenu;
}
/**********************
 * 设置固件升级的数据
 * ********************/
void OnlineFwUpgrade_init(void)
{
    //获取IXG的sever、code
    cJSON * ret = API_ReadRemoteIo(GetIXGIp(), cJSON_CreateStringArray(updatestr, 2));
	if(ret)
	{
		cJSON* item = cJSON_GetObjectItemCaseSensitive(ret, "READ");
		GetJsonDataPro(item, "UpdateServer", Server);
        OnlineFwUpgrade_Process(FWUPGRADE_SERVER,Server);
        
		GetJsonDataPro(item, "UpdateCode", Code);
        OnlineFwUpgrade_Process(FWUPGRADE_DLCODE,Code);       
	}   
    OnlineFwUpgrade_Process(FWUPGRADE_INIT,NULL);
}

static void fw_install_event(lv_event_t* e)
{ 
    OnlineFwUpgrade_Process(FWUPGRADE_OPERATION,NULL);
}

static void fw_refresh_event(lv_event_t* e)
{
    lv_event_code_t code = lv_event_get_code(e);
    lv_msg_t* m = lv_event_get_msg(e);
    lv_obj_t* obj = lv_event_get_target(e);
    if(obj == NULL)
        return;
    char* text;
    if(code == LV_EVENT_MSG_RECEIVED) {        
        if(lv_msg_get_id(m) == FWUPGRADE_RECEIVE) {
            text = lv_msg_get_payload(m);
            if(text){
                lv_textarea_add_text(obj,text);
                lv_textarea_add_char(obj,'\n');
            }          
        }    
    }
  
}

static void select_fw_event(lv_event_t* e)
{  
    lv_obj_t* obj = lv_event_get_target(e);
    create_fw_menu(obj);               
}
/**********************
 * 创建键盘页面
 * target : 选择完需要修改文本的对象
 * val ： 调用键盘时显示的当前文本
 * ********************/
static void back_fw_event(lv_event_t* e)
{  
    fw_delete();        
}

void fw_delete(void)
{  
	if(fw_manage)
	{
	    if(ta_msg){
	        lv_msg_unsubscribe(ta_msg);
	        ta_msg = NULL;
	    }       
	    if(fw_back_scr){
	        lv_disp_load_scr(fw_back_scr); 
	    } 
	    if(fw_manage){
	        lv_obj_del(fw_manage);
	        fw_manage = NULL; 
	    }
	    OnlineFwUpgrade_Process(FWUPGRADE_EXIT,NULL); 
	}
}

static void code_input_event(lv_event_t* e)
{
    lv_obj_t* valueLabel = lv_event_get_target(e);
    char* text = lv_label_get_text(valueLabel);

    fw_create_string_keyboard_menu(valueLabel,text);
}
/**********************
 * 创建键盘页面
 * target : 选择完需要修改文本的对象
 * val ： 调用键盘时显示的当前文本
 * ********************/
void fw_create_string_keyboard_menu(lv_obj_t* target, char* val)
{
    editor_keyboard_return = lv_scr_act();
    ui_keyboard = fw_keyboard_init();   

    lv_obj_t* keyMenu = lv_obj_create(ui_keyboard);
    lv_obj_set_style_pad_row(keyMenu, 0, 0);
    lv_obj_set_size(keyMenu, 480, 800);
    lv_obj_set_flex_flow(keyMenu, LV_FLEX_FLOW_COLUMN);
    lv_obj_clear_flag(keyMenu, LV_OBJ_FLAG_SCROLLABLE);

    lv_obj_t* nameLabel = lv_obj_get_child(lv_obj_get_parent(target), 0);
    char* name = lv_label_get_text(nameLabel);

    lv_obj_t* name_label = lv_label_create(keyMenu);
    lv_label_set_text(name_label, name);
    lv_obj_set_size(name_label, 200, 80);
    lv_obj_set_style_text_font(name_label, &lv_font_montserrat_32, 0);      //显示name


    lv_obj_t* ta = lv_textarea_create(keyMenu);
    lv_textarea_set_max_length(ta,8);
    lv_obj_set_style_text_font(ta, &lv_font_montserrat_28, 0);
    lv_textarea_add_text(ta, val);
    lv_obj_set_size(ta, LV_PCT(100), 300);
    //lv_obj_set_y(ta, 100);

    kb = keyboard_create(ui_keyboard);
    //lv_obj_add_flag(kb, LV_OBJ_FLAG_HIDDEN);

    lv_obj_add_event_cb(ta, code_edit_event_cb, LV_EVENT_ALL, target);
    lv_event_send(ta, LV_EVENT_FOCUSED, NULL);
    lv_disp_load_scr(ui_keyboard);
}
//键盘事件,文本框获得焦点时触发该事件
static void code_edit_event_cb(lv_event_t* e)
{
    lv_event_code_t code = lv_event_get_code(e);
    lv_obj_t* ta = lv_event_get_target(e);
    lv_obj_t* label = lv_event_get_user_data(e);    

    if (code == LV_EVENT_FOCUSED) {
        if (lv_indev_get_type(lv_indev_get_act()) != LV_INDEV_TYPE_KEYPAD) {

            lv_keyboard_set_textarea(kb, ta);

            lv_keyboard_set_mode(kb, LV_KEYBOARD_MODE_TEXT_LOWER);
		lv_obj_clear_flag(kb, LV_OBJ_FLAG_HIDDEN);	
        }
    }
    else if (code == LV_EVENT_CANCEL) {

        keyboard_delete();
    }
    else if (code == LV_EVENT_READY)
    {
        char* text = lv_textarea_get_text(ta);
        lv_label_set_text(label,text);
        OnlineFwUpgrade_Process(FWUPGRADE_DLCODE,text);

        keyboard_delete();
        
    }
}
void keyboard_delete(void)
{
    if(editor_keyboard_return)
        lv_disp_load_scr(editor_keyboard_return);    
    if(ui_keyboard){
        lv_obj_del(ui_keyboard);
        ui_keyboard = NULL;
    }    
}

/**********************
 * 创建选择服务器的combox控件
 * target : 选择完需要修改文本的对象
 * ********************/
void create_fw_menu(lv_obj_t* target)
{   
    lv_obj_t* combox = lv_obj_create(lv_scr_act());
    lv_obj_remove_style_all(combox); 
    lv_obj_set_size(combox, 480, 800);
    //lv_obj_align(combox, LV_ALIGN_CENTER, 0, 0);
    //lv_obj_set_style_pad_all(combox, 0, 0);
    lv_obj_clear_flag(combox, LV_OBJ_FLAG_SCROLLABLE);
    lv_obj_set_style_bg_color(combox, lv_color_hex(0x41454D), 0);
    //lv_obj_set_style_border_side(combox, 0, 0);

    lv_obj_t* titleCont = lv_obj_create(combox);
    lv_obj_set_size(titleCont, LV_PCT(100), 80);
    lv_obj_set_style_pad_all(titleCont, 0, 0);
    lv_obj_add_flag(titleCont, LV_OBJ_FLAG_CLICKABLE);
    lv_obj_set_style_bg_color(titleCont, lv_color_hex(0x41454D), 0);
    lv_obj_add_event_cb(titleCont, fw_exit_event, LV_EVENT_CLICKED,combox);
    lv_obj_set_style_border_side(titleCont, 2, 0);

    lv_obj_t* titlelabel = lv_label_create(titleCont);
    lv_obj_set_style_text_font(titlelabel, &lv_font_montserrat_32, 0);
    lv_obj_set_style_text_color(titlelabel, lv_color_white(), 0);
    lv_label_set_text_fmt(titlelabel, "%s  IXG Fw Upgrade",LV_SYMBOL_LEFT);
 

    lv_obj_t* cont = lv_obj_create(combox);
    //lv_obj_remove_style_all(cont); 
    lv_obj_set_size(cont, LV_PCT(100), 720);
    lv_obj_set_y(cont,80);
    lv_obj_set_style_bg_color(cont, lv_color_hex(0x52545A), 0);
    lv_obj_set_style_pad_all(cont, 0, 0);
    lv_obj_add_flag(cont, LV_OBJ_FLAG_CLICKABLE);   
    lv_obj_set_style_text_color(cont, lv_color_white(), 0);
    //lv_obj_set_style_border_side(cont,0,0);
    lv_obj_set_style_text_font(cont, &lv_font_montserrat_28, 0);
    lv_obj_set_flex_flow(cont, LV_FLEX_FLOW_COLUMN);
    lv_obj_add_event_cb(cont, fw_choose_event, LV_EVENT_CLICKED, target);   

    lv_obj_t* box1 = lv_checkbox_create(cont);
    lv_checkbox_set_text(box1, "47.91.88.33");
    lv_obj_set_height(box1, 80);       
    lv_obj_add_flag(box1, LV_OBJ_FLAG_EVENT_BUBBLE);

    lv_obj_t* box2 = lv_checkbox_create(cont);
    lv_checkbox_set_text(box2, "47.106.104.38");
    lv_obj_set_height(box2, 80);   
    lv_obj_add_flag(box2, LV_OBJ_FLAG_EVENT_BUBBLE);

    lv_obj_t* box3 = lv_checkbox_create(cont);
    lv_checkbox_set_text(box3, "SD_Card");
    lv_obj_set_height(box3, 80);   
    lv_obj_add_flag(box3, LV_OBJ_FLAG_EVENT_BUBBLE);

    //勾选对象当前使用的服务器
    if(strcmp(Server,"47.91.88.33") == 0){
        lv_obj_add_state(box1,LV_STATE_CHECKED);
    }else if(strcmp(Server,"47.106.104.38") == 0){
        lv_obj_add_state(box2,LV_STATE_CHECKED);
    }else{
        lv_obj_add_state(box3,LV_STATE_CHECKED);
    }
}
/**********************
 * 服务器切换事件,修改combox勾选框时触发
 * ********************/
static void fw_choose_event(lv_event_t* e)
{
    lv_obj_t* cont = lv_event_get_current_target(e);
    lv_obj_t* act_cb = lv_event_get_target(e);
    lv_obj_t* label = lv_event_get_user_data(e);

    if (act_cb == cont) return;
    //清除所用子控件的勾选状态
    int cnt = lv_obj_get_child_cnt(cont);
    for(int i = 0;i < cnt;i++)
    { 
        lv_obj_clear_state(lv_obj_get_child(cont,i), LV_STATE_CHECKED);   
    } 
    lv_obj_add_state(act_cb, LV_STATE_CHECKED);     //勾选选中的

    int active_id = lv_obj_get_index(act_cb);
    char* text = lv_checkbox_get_text(act_cb);
    lv_label_set_text(label,text);
    strcpy(Server,text);
    OnlineFwUpgrade_Process(FWUPGRADE_SERVER,text); 
}

static void fw_exit_event(lv_event_t* e)
{  
    lv_obj_t* obj = lv_event_get_user_data(e);
    if(obj)
        lv_obj_del(obj);
}

static lv_obj_t* IXG_FW_init(void)
{
    lv_obj_t* fw_scr = lv_obj_create(NULL);
    lv_obj_remove_style_all(fw_scr);
    lv_obj_clear_flag(fw_scr, LV_OBJ_FLAG_SCROLLABLE); 
    lv_obj_set_size(fw_scr, lv_disp_get_hor_res(NULL), lv_disp_get_ver_res(NULL));
    lv_obj_align(fw_scr, LV_ALIGN_TOP_LEFT, 0, 0);
    lv_obj_set_style_bg_color(fw_scr, lv_palette_main(LV_PALETTE_GREY), 0);
    lv_obj_set_style_bg_opa(fw_scr, LV_OPA_100, 0);
    
    return fw_scr;
}

static lv_obj_t* fw_keyboard_init(void)
{
    lv_obj_t* ui_keyboard = lv_obj_create(NULL);
    lv_obj_clear_flag(ui_keyboard, LV_OBJ_FLAG_SCROLLABLE);
    lv_obj_remove_style_all(ui_keyboard);
    lv_obj_set_size(ui_keyboard, lv_disp_get_hor_res(NULL), lv_disp_get_ver_res(NULL));
    lv_obj_align(ui_keyboard, LV_ALIGN_TOP_LEFT, 0, 0);
    lv_obj_set_style_bg_color(ui_keyboard, lv_palette_main(LV_PALETTE_GREY), 0);
    lv_obj_set_style_bg_opa(ui_keyboard, LV_OPA_100, 0);
    
    return ui_keyboard;
}