#ifndef IXG_MANAGE_H_
#define IXG_MANAGE_H_

#include "lv_ix850.h"

#define PAGE_SIZE       32

typedef struct
{
    int      msg_type;       
    char*    ixg_id;        
}IXG_MANAGE;

typedef struct
{
    int     ixg_id;
    int     online;
} IXG_CELL_T;

typedef struct
{
    IXG_CELL_T      all[128];
    int             cnt;
    // disp var
    int    page_all;                //all page
    int    last_page_cnt;           //The number of last pages
    int    page_cur;                //Current page
    int    page_cur_online[32];     // The number of current pages online
} IXG_DATA_T;

lv_obj_t* IXG_menu_creat(cJSON* json);
void IXG_add_title(lv_obj_t* parent, char* txt);
static void IXG_add_filter(lv_obj_t* parent);
lv_obj_t* IXG_add_manage(lv_obj_t* parent, int cnt_page);
lv_obj_t* IXG_load_menu(lv_obj_t* parent,char* id,char* im);
void IXG_add_change_page(lv_obj_t* parent, int cnt_page, int max_page, int size);
static void menu_change_event_cb(lv_event_t* e);
static lv_obj_t* ui_manage_init(void);
void manage_delete(void);
static void manage_exit_event(lv_event_t* e);
static void sw_change_event(lv_event_t* e);
static void IXG_left_click_event(lv_event_t* e);
static void IXG_right_click_event(lv_event_t* e);
static void IXG_cnt_change_event_cb(lv_event_t* e);
static void IXG_page_change_event_cb(lv_event_t* e);
lv_timer_t* IXG_refresh_tips(const char* string,lv_timer_cb_t timer_cb);
static void msg_refresh_event(lv_event_t* e);
static void ixg_resfresh_cb(lv_event_t* e);
static void IXG_refresh_event(lv_event_t* e);
void ixg_load_table_from_json(lv_obj_t* obj, cJSON* json,int page);
static void ixg_draw_table_event_cb(lv_event_t* e);
static void ixg_table_event_cb(lv_event_t* e);
int ixg_disp_data_set_one(IXG_DATA_T* pdata, int ixg_id, int online,int cnt);
int ixg_disp_updata(IXG_DATA_T* pdata,int page);
int ixg_disp_data_set(cJSON* json);
#endif 
