#ifndef IXG_DM_H_
#define IXG_DM_H_

#include "lv_ix850.h"

lv_obj_t* IXG_DM_menu_creat(cJSON* json,char* id);
static lv_obj_t* IXG_DM_init(void);
bool DM_delete(void);
static void back_about_event(lv_event_t* e);
lv_obj_t* IXG_create_btn(lv_obj_t* parent, char* txt, lv_event_cb_t cb,void* user_data);
static void dm_back_event(lv_event_t* e);
static void FW_click_event(lv_event_t* e);
lv_obj_t* IXG_load_text(lv_obj_t* parent,char* name,char* value,lv_event_cb_t cb);
lv_obj_t* About_menu(cJSON* json);
static void Dw_click_event(lv_event_t* e);
static void Ab_click_event(lv_event_t* e);
static void Re_click_event(lv_event_t* e);
#endif