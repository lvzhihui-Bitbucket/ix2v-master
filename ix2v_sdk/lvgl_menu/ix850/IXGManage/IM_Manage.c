#include "IM_Manage.h"
#include "IXG_Manage.h"
#include "IXG_DM.h"
#include "obj_lvgl_msg.h"

lv_obj_t* IM_scr = NULL;
lv_obj_t* IM_back_scr = NULL;
extern bool state;
lv_obj_t* IM_menu = NULL;
extern IXG_MANAGE* ixg_ma;
static lv_obj_t* imcntlabel = NULL;
static lv_obj_t* impagelabel = NULL;
static IM_DATA_T*  im_online = NULL;
static IM_DATA_T*  im_all = NULL;
lv_timer_t* im_timer = NULL;
void* im_msg = NULL;
static void IM_refresh_event(lv_event_t* e);
/**********************
 * 初始化IM在线显示页面
 * json ：IM的显示数据
 * id ：当前IXG-ID
 * ********************/
lv_obj_t* IM_menu_creat(cJSON* json,char* id)
{
    im_all = (IM_DATA_T*)lv_mem_alloc(sizeof(IM_DATA_T));
    lv_memset_00(im_all, sizeof(IM_DATA_T));
    
    im_online = (IM_DATA_T*)lv_mem_alloc(sizeof(IM_DATA_T));
    lv_memset_00(im_online, sizeof(IM_DATA_T)); 

    IM_back_scr = lv_scr_act();
    IM_scr = IM_manage_init();

    lv_obj_t* cont = lv_obj_create(IM_scr);
    lv_obj_set_size(cont, 480, 800);
    lv_obj_set_style_pad_all(cont, 0, 0);
    lv_obj_clear_flag(cont, LV_OBJ_FLAG_SCROLLABLE);

    IXG_add_title(cont, "DT-IM Management"); 
 
    IM_add_filter(cont, id);   

    IM_add_change_page(cont, 1, 1, 0); 

    im_disp_data_set(json);
    IM_add_manage(cont, 1);
     
    lv_disp_load_scr(IM_scr);
    return cont;
}
/**********************
 * 设置IM-Table的数据
 * json ：IM的显示数据
 * ********************/
int im_disp_data_set(cJSON* json)
{    
    cJSON *im = cJSON_GetObjectItem(json,"DT_IM");
    int length = cJSON_GetArraySize(im);

    if(state){
        if(im_all == NULL)
            return;
        im_all->cnt = 0;
        for (size_t i = 0; i < 63; i++)
        {
            im_all->im[i] =  i+1;
            im_all->cnt++;
        }
        for (size_t i = 0; i < length; i++)
        {
            cJSON* im_id = cJSON_GetArrayItem(im, i);

            im_all->im[im_id->valueint-1] =  im_id->valueint;
            im_all->page_cur_online[i] = im_id->valueint;
        }
        printf(" im_disp_data_set  ---------    cnt = %d\n",im_all->cnt);
        im_all->last_page_cnt = im_all->cnt % PAGE_SIZE;

        im_all->page_all = im_all->cnt / PAGE_SIZE + 1;       

    }else{
        if(im_online == NULL)
            return;
        im_online->cnt = 0;     
        for (size_t i = 0; i < length; i++)
        {
            cJSON* im_id = cJSON_GetArrayItem(im, i);

            im_online->im[i] =  im_id->valueint;
            im_online->cnt++;
            im_online->page_cur_online[i] = im_id->valueint;
        }
        printf(" im_disp_data_set  ---------    cnt = %d\n",im_all->cnt);
        im_online->last_page_cnt = im_online->cnt % PAGE_SIZE;

        im_online->page_all = im_online->cnt / PAGE_SIZE + 1;

    }
}

static void IM_add_filter(lv_obj_t* parent,char* id)
{
    lv_obj_t* filterCont = lv_obj_create(parent);
    lv_obj_set_style_pad_row(filterCont, 0, 0);
    lv_obj_set_size(filterCont, LV_PCT(100), 100);
    //lv_obj_set_flex_flow(filterCont, LV_FLEX_FLOW_ROW);
    lv_obj_set_style_text_font(filterCont, &lv_font_montserrat_32, 0);
    lv_obj_clear_flag(filterCont, LV_OBJ_FLAG_SCROLLABLE);
    lv_obj_set_y(filterCont, 80);
    //显示IXG-ID
    lv_obj_t* filter_label = lv_label_create(filterCont);
    lv_label_set_text_fmt(filter_label,"IXG-ID = %s",id);
    lv_obj_set_size(filter_label, LV_SIZE_CONTENT, LV_SIZE_CONTENT);
    lv_obj_align(filter_label, LV_ALIGN_LEFT_MID, 0, 0);
    //进入IXG管理页面按钮
    lv_obj_t* setbtn = lv_btn_create(filterCont);
    lv_obj_set_size(setbtn, 120, 60);
    lv_obj_align(setbtn, LV_ALIGN_CENTER, 30, 0);
    //lv_obj_set_style_bg_color(backBtn, lv_color_make(230, 231, 232), 0);
    lv_obj_set_style_text_font(setbtn, &lv_font_montserrat_28, 0);
    lv_obj_add_event_cb(setbtn, menu_change_event_cb, LV_EVENT_CLICKED, id);
    lv_obj_set_style_radius(setbtn, LV_RADIUS_CIRCLE, 0);
    lv_obj_t* setlabel = lv_label_create(setbtn);
    //lv_label_set_text(setlabel, "Set");
    vtk_label_set_text(setlabel, "Set");
    lv_obj_center(setlabel);
    //IM分机在线显示刷新按钮
    lv_obj_t* refreshBtn = lv_btn_create(filterCont);
    lv_obj_align(refreshBtn, LV_ALIGN_RIGHT_MID, 0, 0);
    //lv_obj_set_style_bg_color(backBtn, lv_color_make(230, 231, 232), 0);
    lv_obj_set_size(refreshBtn, 120, 60);
    lv_obj_set_style_text_font(refreshBtn, &lv_font_montserrat_28, 0); 
    im_msg = lv_msg_subsribe_obj(IXG_LIST_REFRESH, refreshBtn, NULL);   
    lv_obj_add_event_cb(refreshBtn, IM_refresh_event, LV_EVENT_ALL, id);
    lv_obj_set_style_radius(refreshBtn, 0, 0);
    lv_obj_t* refreshlabel = lv_label_create(refreshBtn);
    //lv_label_set_text(refreshlabel, "Refresh");
    vtk_label_set_text(refreshlabel, "Refresh");
    lv_obj_center(refreshlabel);

}
/**********************
 * 通知process启动搜索
 * ********************/
static void IM_refresh_event(lv_event_t* e)
{
    lv_event_code_t code = lv_event_get_code(e);
    lv_msg_t* m = lv_event_get_msg(e);
    if(code == LV_EVENT_CLICKED) { 
        ixg_ma->msg_type = IM_MANAGE_REFRESH_SEND;
        API_SettingMenuProcess(ixg_ma);         
        im_timer = IXG_refresh_tips("Searching, please wait!",im_resfresh_timer_cb);
         
    }
    else if(code == LV_EVENT_MSG_RECEIVED) {        
        if(lv_msg_get_id(m) == IXG_LIST_REFRESH) {
            lv_obj_clear_flag(lv_layer_top(), LV_OBJ_FLAG_CLICKABLE);

            if(im_timer != NULL){
                lv_timer_pause(im_timer);
                lv_obj_t* obj = (lv_obj_t*)im_timer->user_data;
                lv_obj_del(obj);
                lv_timer_del(im_timer);
                im_timer = NULL;
            }

            ixg_ma->msg_type = IM_MANAGE_REFRESH_RECEIVE;
            API_SettingMenuProcess(ixg_ma);         
        }    
    }         
}
/**********************
 * IXG搜索未超时,成功获取结果,通知process更新显示
 * ********************/
static void im_refresh_event(lv_event_t* e)
{
    lv_obj_t* obj = lv_event_get_target(e);
    void* msg = lv_event_get_user_data(e);
    lv_msg_t* m = lv_event_get_msg(e);

    if(lv_msg_get_id(m) == FWUPGRADE_RECEIVE) {
        if(im_timer != NULL){
            lv_timer_pause(im_timer);
            lv_timer_del(im_timer);
            im_timer = NULL;
        }      
        lv_obj_del(obj);
        lv_obj_clear_flag(lv_layer_top(), LV_OBJ_FLAG_CLICKABLE);
        lv_msg_unsubscribe(msg);

        ixg_ma->msg_type = IM_MANAGE_REFRESH_RECEIVE;
        API_SettingMenuProcess(ixg_ma);
    }
}
/**********************
 * IM搜索超时,弹出提示框
 * ********************/
static void im_resfresh_timer_cb(lv_timer_t* timer)
{
    lv_obj_t* obj = (lv_obj_t*)im_timer->user_data;
    lv_timer_del(im_timer);
    im_timer = NULL;
    lv_obj_del(obj);                            //删除提示框
    lv_obj_clear_flag(lv_layer_top(), LV_OBJ_FLAG_CLICKABLE);   //归还屏幕消息

    LV_API_TIPS(" Search failed, please check the device status!",4);
    //API_TIPS("Search failed, please check the device status!");
    ixg_ma->msg_type = IM_MANAGE_REFRESH_RECEIVE;
    API_SettingMenuProcess(ixg_ma);

}

static void menu_change_event_cb(lv_event_t* e)
{
    //printf("menu_change_event_cb ------------------\n");
    char* id = lv_event_get_user_data(e);
    strcpy(ixg_ma->ixg_id,id);
    ixg_ma->msg_type = IXG_MSG_ENTER_DM;
    API_SettingMenuProcess(ixg_ma);
}
/**********************
 * 初始化IM-Table
 * parent ：Table的父对象
 * cur_page ： 当前页
 * ********************/
void IM_add_manage(lv_obj_t* parent, int cur_page)
{
    if(parent == NULL){
        return;
    }
    if(IM_menu == NULL){       
        IM_menu = lv_table_create(parent);
        lv_obj_set_size(IM_menu, 480, 520);
        lv_obj_set_y(IM_menu, 180);
        lv_obj_clear_flag(IM_menu, LV_OBJ_FLAG_SCROLLABLE);
        lv_obj_add_event_cb(IM_menu, im_draw_table_event_cb, LV_EVENT_DRAW_PART_BEGIN, NULL);
        //lv_obj_add_event_cb(IM_menu, ixg_table_event_cb, LV_EVENT_VALUE_CHANGED, NULL);
    }

    //lv_obj_set_style_outline_width(manage_menu,1,LV_PART_ITEMS);
    lv_table_set_row_cnt(IM_menu,0);
    lv_table_set_col_cnt(IM_menu,0);
    lv_table_set_row_height(IM_menu,65);
    //设置表格的列宽
    if(state){
        im_disp_updata(im_all,cur_page);
        im_all->page_cur = cur_page;
        if(impagelabel){
            lv_label_set_text_fmt(impagelabel, "%d/%d", im_all->page_cur, im_all->page_all);
        }
        if(imcntlabel){
            lv_label_set_text_fmt(imcntlabel, "%d", im_all->cnt);
        }
        for(int i=0;i<4;i++){
            lv_table_set_col_width(IM_menu, i, 120);
        }
    }else{
        im_disp_updata(im_online,cur_page);
        im_online->page_cur = cur_page;
        if(impagelabel){
            lv_label_set_text_fmt(impagelabel, "%d/%d", im_online->page_cur,im_online->page_all );
        }
        if(imcntlabel){
            lv_label_set_text_fmt(imcntlabel, "%d", im_online->cnt);
        }
        if(im_online->cnt < 4){     //不满4个时,只设置已有个数的列宽
            for(int i=0;i<im_online->cnt;i++){
                lv_table_set_col_width(IM_menu, i, 120);
            }
        }else{
            for(int i=0;i<4;i++){
                lv_table_set_col_width(IM_menu, i, 120);
            }
        }
    }

}

int im_disp_updata(IM_DATA_T* pdata,int page)
{
    int length = -1;
    if(page == pdata->page_all){
        length = pdata->last_page_cnt;
    }else{
        length = PAGE_SIZE;
    }
    printf("im_disp_updata              length = %d\n  ",length);
    // for all, get online 
    // update page all, page_cur;
    // display    
    for( int i = 0; i < length; i++ )
    {
        int row = i / 4;

        int col = i % 4;

        int cur_im = (page-1)*PAGE_SIZE + i;              
        lv_table_set_cell_value_fmt(IM_menu, row, col, "%d",pdata->im[cur_im]);     //设置单格数据
            
    }

    return 0;
}

static void im_draw_table_event_cb(lv_event_t* e)
{
    lv_obj_t* obj = lv_event_get_target(e);
    int cnt = -1;
    int id = -1;
    lv_obj_draw_part_dsc_t* dsc = lv_event_get_draw_part_dsc(e);
    /*If the cells are drawn...*/
    if (dsc->part == LV_PART_ITEMS) {
        uint32_t row = dsc->id / lv_table_get_col_cnt(obj);
        uint32_t col = dsc->id - row * lv_table_get_col_cnt(obj);

        dsc->label_dsc->font = &lv_font_montserrat_30;
        dsc->label_dsc->align = LV_TEXT_ALIGN_CENTER;

        if(state){           
            for(int i=0;i<PAGE_SIZE;i++){
                cnt = im_all->page_cur_online[i];
                id = (im_all->page_cur-1)*PAGE_SIZE + dsc->id + 1;
                if(cnt == id)
                   dsc->rect_dsc->bg_color = lv_color_make(149,236,105); 
            }
        }else{
            dsc->rect_dsc->bg_color = lv_color_make(149,236,105); 
        }                    
    }
}

lv_obj_t* IM_load_menu(lv_obj_t* parent,char* im)
{
    lv_obj_t* obj = lv_obj_create(parent);
    lv_obj_set_size(obj, LV_PCT(25), 65);
    lv_obj_set_style_pad_row(obj, 0, 0); 
    lv_obj_clear_flag(obj, LV_OBJ_FLAG_SCROLLABLE);
    lv_obj_add_flag(obj, LV_OBJ_FLAG_CLICKABLE);

    lv_obj_t* label1 = lv_label_create(obj);
    lv_label_set_text(label1, im);
    lv_obj_align(label1, LV_ALIGN_CENTER, 0, 0);
    lv_obj_set_style_text_font(label1, &lv_font_montserrat_32, 0);

    return obj;
}
/**********************
 * 初始化翻页栏
 * parent ：父对象
 * cur_page ： 当前页
 * max_page ： 最大页数
 * size ： 最大个数
 * ********************/
void IM_add_change_page(lv_obj_t* parent, int cnt_page, int max_page, int size)
{
    lv_obj_t* pageControl = lv_obj_create(parent);
    lv_obj_set_size(pageControl, 480, 100);
    lv_obj_set_style_pad_all(pageControl, 0, 0);
    lv_obj_set_y(pageControl, 700);

    lv_obj_t* backBtn = lv_btn_create(pageControl);
    lv_obj_align(backBtn, LV_ALIGN_LEFT_MID, 0, 0);
    lv_obj_set_style_text_color(backBtn, lv_color_hex(0xff0000), 0);
    lv_obj_set_style_bg_color(backBtn, lv_color_make(230, 231, 232), 0);
    lv_obj_set_size(backBtn, 100, 70);
    lv_obj_set_style_text_font(backBtn, &lv_font_montserrat_32, 0);
    lv_obj_add_event_cb(backBtn, IM_exit_event, LV_EVENT_CLICKED, NULL);
    lv_obj_set_style_radius(backBtn, LV_RADIUS_CIRCLE, 0);
    lv_obj_t* backlabel = lv_label_create(backBtn);
    lv_label_set_text(backlabel, LV_SYMBOL_BACKSPACE);
    lv_obj_center(backlabel);

    lv_obj_t* leftBtn = lv_btn_create(pageControl);
    lv_obj_align(leftBtn, LV_ALIGN_LEFT_MID, 120, 0);
    lv_obj_set_style_text_color(leftBtn, lv_color_hex(0xff0000), 0);
    lv_obj_set_style_bg_color(leftBtn, lv_color_make(230, 231, 232), 0);
    lv_obj_set_size(leftBtn, 80, 70);
    lv_obj_set_style_text_font(leftBtn, &lv_font_montserrat_32, 0);
    lv_obj_add_event_cb(leftBtn, IM_left_click_event, LV_EVENT_CLICKED, NULL);
    lv_obj_set_style_radius(leftBtn, LV_RADIUS_CIRCLE, 0);
    lv_obj_t* leftlabel = lv_label_create(leftBtn);
    lv_label_set_text(leftlabel, LV_SYMBOL_LEFT);
    lv_obj_center(leftlabel);

    char buf[100];
    sprintf(buf, "%d/%d", cnt_page, max_page);

    impagelabel = lv_label_create(pageControl);
    lv_label_set_text(impagelabel, buf);
    lv_obj_set_style_text_font(impagelabel, &lv_font_montserrat_32, 0);
    lv_obj_align(impagelabel, LV_ALIGN_CENTER, 0, 0);
    //lv_msg_subsribe_obj(IM_MANAGE_CHANGE_PAGE, pagelabel, NULL);
    //lv_obj_add_event_cb(pagelabel, IM_page_change_event_cb, LV_EVENT_MSG_RECEIVED, NULL);

    lv_obj_t* rightBtn = lv_btn_create(pageControl);
    lv_obj_align(rightBtn, LV_ALIGN_CENTER, 90, 0);
    lv_obj_set_style_text_color(rightBtn, lv_color_hex(0xff0000), 0);
    lv_obj_set_style_bg_color(rightBtn, lv_color_make(230, 231, 232), 0);
    lv_obj_set_size(rightBtn, 80, 70);
    lv_obj_set_style_text_font(rightBtn, &lv_font_montserrat_32, 0);
    lv_obj_add_event_cb(rightBtn, IM_right_click_event, LV_EVENT_CLICKED, NULL);
    lv_obj_set_style_radius(rightBtn, LV_RADIUS_CIRCLE, 0);
    lv_obj_t* rightlabel = lv_label_create(rightBtn);
    lv_label_set_text(rightlabel, LV_SYMBOL_RIGHT);
    lv_obj_center(rightlabel);

    sprintf(buf, "%d", size);
    imcntlabel = lv_label_create(pageControl);
    lv_label_set_text(imcntlabel, buf);
    lv_obj_set_style_text_font(imcntlabel, &lv_font_montserrat_32, 0);
    lv_obj_align(imcntlabel, LV_ALIGN_RIGHT_MID, -50, 0);
    //lv_msg_subsribe_obj(IM_MANAGE_CHANGE_CNT, cntlabel, NULL);
    //lv_obj_add_event_cb(cntlabel, IM_cnt_change_event_cb, LV_EVENT_MSG_RECEIVED, NULL);
}

static void IM_left_click_event(lv_event_t* e)
{
    if(IM_menu == NULL)
        return;
    if(state){
        im_all->page_cur++;         //增加当前页
        if(im_all->page_cur > im_all->page_all){
            im_all->page_cur = 1;
        }
        IM_add_manage(lv_obj_get_parent(IM_menu),im_all->page_cur);     //刷新Table
     }else{
        im_online->page_cur++;
        if(im_online->page_cur > im_online->page_all){
            im_online->page_cur = 1;
        }
        IM_add_manage(lv_obj_get_parent(IM_menu),im_online->page_cur);
     }
}

static void IM_right_click_event(lv_event_t* e)
{
    if(IM_menu == NULL)
        return;
    if(state){
        im_all->page_cur--;         //递减当前页
        if(im_all->page_cur <= 0){
            im_all->page_cur = im_all->page_all;
        }
        IM_add_manage(lv_obj_get_parent(IM_menu),im_all->page_cur);     //刷新Table
     }else{
        im_online->page_cur--;
        if(im_online->page_cur <= 0){
            im_online->page_cur = im_online->page_all;
        }
        IM_add_manage(lv_obj_get_parent(IM_menu),im_online->page_cur);
     } 
}

static void IM_cnt_change_event_cb(lv_event_t* e)
{
    lv_obj_t* label = lv_event_get_target(e);
    if(IM_menu == NULL)
        return;
    int cnt =  lv_obj_get_child_cnt(IM_menu);

    char buf[100];
    sprintf(buf, "%d", cnt);
    lv_label_set_text(label, buf);
}

static void IM_page_change_event_cb(lv_event_t* e)
{
    lv_obj_t* label = lv_event_get_target(e);
    if(IM_menu == NULL)
        return;
    int cnt =  lv_obj_get_child_cnt(IM_menu);
    char buf[100];
    lv_msg_t* m = lv_event_get_msg(e);
    int32_t val = (int32_t)lv_msg_get_payload(m);

    if(cnt <= 32){
        sprintf(buf, "%d/%d", val, 1);
        lv_label_set_text(label, buf);
    }else{
        sprintf(buf, "%d/%d", val, 2);
        lv_label_set_text(label, buf);
    }
       
}

static void IM_exit_event(lv_event_t* e)
{
    
    lv_disp_load_scr(IM_back_scr);

    //IM_delete();
    //MenuIXGTbView_Process(IM_MANAGE_EXIT, NULL);
    ixg_ma->msg_type = IM_MANAGE_EXIT;
    API_SettingMenuProcess(ixg_ma);
}


void IM_delete(void)
{
    if(im_msg){
        lv_msg_unsubscribe(im_msg);
        im_msg = NULL;
    }

    if(IM_menu){
        lv_obj_del(IM_menu);
        IM_menu = NULL;
    }
    if (IM_scr)
    {
        lv_obj_del(IM_scr);
        IM_scr = NULL;
    }

     if(im_all){
        
        free(im_all);
        im_all = NULL;
    }

    if(im_online){
        
        free(im_online);
        im_online = NULL;
    }

}

static lv_obj_t* IM_manage_init(void)
{
    lv_obj_t* IM_manage = lv_obj_create(NULL);
    lv_obj_clear_flag(IM_manage, LV_OBJ_FLAG_SCROLLABLE);
    lv_obj_remove_style_all(IM_manage);
    lv_obj_set_size(IM_manage, lv_disp_get_hor_res(NULL), lv_disp_get_ver_res(NULL));
    lv_obj_align(IM_manage, LV_ALIGN_TOP_LEFT, 0, 0);
    lv_obj_set_style_bg_color(IM_manage, lv_palette_main(LV_PALETTE_GREY), 0);
    lv_obj_set_style_bg_opa(IM_manage, LV_OPA_100, 0);
    
    return IM_manage;
}