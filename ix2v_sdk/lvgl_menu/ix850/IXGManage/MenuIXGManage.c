#include "IXG_Manage.h"
#include "IM_Manage.h"
#include "IXG_DM.h"
#include "obj_lvgl_msg.h"
#include "lv_ix850.h"
#include "obj_TableSurver.h"
#include "task_Event.h"

static lv_obj_t* ixg_manage = NULL;
static lv_obj_t* im_manage = NULL;
static lv_obj_t* ixg_dm = NULL;
static char ixgip[40] = {0};

int GetIXGIp(void)
{
    if(ixgip == NULL){
		return 0;
	}        
    else{
        return inet_addr(ixgip);
    }  
}

cJSON* FiterIXG(char* id)
{
	cJSON* where = cJSON_CreateObject();
	cJSON* view = cJSON_CreateArray();
	if(id)
	{
	    cJSON_AddNumberToObject(where, "IXG_ID", atoi(id));
	}

	API_TB_SelectBySortByName(TB_NAME_IXG_LIST, view, where, 0);
	cJSON* ixg =  cJSON_Duplicate(cJSON_GetArrayItem(view,0), 1);

	cJSON *ip = cJSON_GetObjectItem(ixg,"IP_ADDR");
    if(ip)
    {				
        strcpy(ixgip,ip->valuestring);	
    }   
	cJSON_Delete(view);
	cJSON_Delete(where);

	return ixg;
}
static cJSON* GetIXGTb(void)
{
	cJSON* view = cJSON_CreateArray();
	API_TB_SelectBySortByName(TB_NAME_IXG_LIST, view, NULL, 0);
	return view;
}

void MenuIXGManage_Init(void)
{
	cJSON *view_tb = GetIXGTb();
	
	ixg_manage = IXG_menu_creat(view_tb);

	cJSON_Delete(view_tb);
	cJSON *repeat=cJSON_CreateArray();
	int cnt;
	if(cnt=API_CheckIXGIdRepeat(repeat))
	{
		API_SettingNormalTip("Have repeat IXG-ID!!!");
		//printf_json(repeat, "11111");
	}
	cJSON_Delete(repeat);
}

void MenuIXGManage_Process(int msg_type, void* arg)
{
	cJSON *im_data;
	cJSON *ixg_data;
	char* ixg_id = (char*)arg;
	cJSON *send_event;
	char*a;
	switch(msg_type)
	{
		case IXG_MSG_ENTER_IM:

			im_data = FiterIXG(ixg_id);
			im_manage = IM_menu_creat(im_data, ixg_id);

			cJSON_Delete(im_data);
			break;
			
		case IXG_MSG_ENTER_DM:
			im_data = FiterIXG(ixg_id);
			ixg_dm = IXG_DM_menu_creat(im_data,ixg_id);

			cJSON_Delete(im_data);
			break;
		case IXG_MSG_CHANGE_STATE:
			
			ixg_data = GetIXGTb();
			ixg_disp_data_set(ixg_data);
			IXG_add_manage(ixg_manage, 1);
			cJSON_Delete(ixg_data);

			break;
		case IXG_MSG_RESTORE:
			send_event = cJSON_CreateObject();
			cJSON_AddStringToObject(send_event, "EventName","EventFactoryDefault");
			cJSON_AddStringToObject(send_event, "SOURCE", "IXG");
			cJSON_AddStringToObject(send_event, "IX_ADDR", GetSysVerInfo_IP());
			API_XD_EventJson(GetIXGIp(),send_event);
			cJSON_Delete(send_event);
			break;
		case IXG_MSG_REBOOT:

			send_event = cJSON_CreateObject();
			cJSON_AddStringToObject(send_event, "EventName","EventReboot");
			cJSON_AddStringToObject(send_event, "SOURCE", "IXG");
			cJSON_AddStringToObject(send_event, "IX_ADDR", GetSysVerInfo_IP());
			API_XD_EventJson(GetIXGIp(),send_event);
			cJSON_Delete(send_event);

			break;
		case IXG_MANAGE_REFRESH_SEND:
			if(API_UpdateIxgList()==0)
				lv_msg_send(IXG_LIST_REFRESH, NULL);
			break;
		case IM_MANAGE_REFRESH_SEND:
			im_data = FiterIXG(ixg_id);
			
			API_UpdateIxgDtImList(inet_addr(GetEventItemString(im_data, "IP_ADDR")));
			cJSON_Delete(im_data);
			break;
		case IXG_MANAGE_REFRESH_RECEIVE:			

			ixg_data = GetIXGTb();

			ixg_disp_data_set(ixg_data);

			IXG_add_manage(ixg_manage, 1);

			cJSON_Delete(ixg_data);

			break;
		case IM_MANAGE_REFRESH_RECEIVE:
			
			im_data = FiterIXG(ixg_id);
			im_disp_data_set(im_data);
			IM_add_manage(im_manage, 1);
			cJSON_Delete(im_data);

			break;
		case ENTER_FW_UPGRADE:
			//im_data = FiterIXG(ixg_id);
			FW_upgrade_menu();
			//cJSON_Delete(im_data);
			break;
		case ENTER_IXG_ABOUT:
			im_data = FiterIXG(ixg_id);
			About_menu(im_data);
			cJSON_Delete(im_data);
			break;
		case IXG_MANAGE_EXIT:
			MenuIXGManage_Close();
			break;
		case IM_MANAGE_EXIT:
			if(im_manage != NULL){
        		IM_delete();
        		im_manage = NULL;
    		}
			break;
	}
}

void MenuIXGManage_Close(void)
{	
	fw_delete();
	if(ixg_dm != NULL){
        DM_delete();
        ixg_dm = NULL;
    }
	if(im_manage != NULL){
        IM_delete();
        im_manage = NULL;
    }	
	if(ixg_manage != NULL){
        manage_delete();
        ixg_manage = NULL;
    }			
}

void API_MenuIXGManage_Process(void* arg)
{
	IXG_MANAGE* ixg = (IXG_MANAGE*)arg;
	MenuIXGManage_Process(ixg->msg_type,ixg->ixg_id);
}