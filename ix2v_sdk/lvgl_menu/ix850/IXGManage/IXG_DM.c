#include "IXG_DM.h"
#include "IXG_Manage.h"
#include "obj_lvgl_msg.h"
#include "obj_PublicInformation.h"

static lv_obj_t* DM_manage = NULL;
static lv_obj_t* DM_back_scr = NULL;
lv_obj_t* aboutmenu;
extern IXG_MANAGE* ixg_ma;
/**********************
 * 初始化IXG管理页面
 * json ：当前IXG的信息
 * id ：当前IXG-ID
 * ********************/
lv_obj_t* IXG_DM_menu_creat(cJSON* json,char* id)
{
    DM_back_scr = lv_scr_act();
    DM_manage = IXG_DM_init();

    lv_obj_t* editmenu = lv_obj_create(DM_manage);
    lv_obj_set_size(editmenu, 480, 800);
    lv_obj_set_style_pad_all(editmenu, 0, 0);
    lv_obj_clear_flag(editmenu, LV_OBJ_FLAG_SCROLLABLE);

    IXG_add_title(editmenu, "IXG DM");              //添加标题

    lv_obj_t* btnCont = lv_obj_create(editmenu);
    lv_obj_set_size(btnCont, 480, 100);
    lv_obj_set_style_pad_all(btnCont, 0, 0);
    lv_obj_set_y(btnCont, 700);

    lv_obj_t* saveBtn = lv_btn_create(btnCont);     //添加返回按钮
    lv_obj_set_size(saveBtn, 120, 70);
    lv_obj_set_style_text_font(saveBtn, &lv_font_montserrat_32, 0);
    lv_obj_add_event_cb(saveBtn, dm_back_event, LV_EVENT_CLICKED, editmenu);
    lv_obj_align(saveBtn, LV_ALIGN_LEFT_MID, 20, 0);
    lv_obj_set_style_radius(saveBtn, LV_RADIUS_CIRCLE, 0);
    lv_obj_set_style_bg_color(saveBtn, lv_color_make(230, 231, 232), 0);
    lv_obj_set_style_text_color(saveBtn, lv_color_black(), 0);
    lv_obj_t* savelabel = lv_label_create(saveBtn);
    lv_label_set_text(savelabel, LV_SYMBOL_BACKSPACE);
    lv_obj_center(savelabel);   

    lv_obj_t* editCont = lv_obj_create(editmenu);
    lv_obj_set_size(editCont, 480, 620);
    lv_obj_set_y(editCont, 80);
    lv_obj_set_flex_flow(editCont, LV_FLEX_FLOW_COLUMN);
    lv_obj_set_style_pad_all(editCont, 0, 0);
    lv_obj_set_style_pad_row(editCont, 20, 0);
    //添加4个功能按键
    IXG_create_btn(editCont,"FW Upgrade", FW_click_event,NULL);     
    IXG_create_btn(editCont, "Restore",Dw_click_event,NULL);
    IXG_create_btn(editCont, "About",Ab_click_event,id);
    IXG_create_btn(editCont, "Reboot",Re_click_event,NULL);

    lv_disp_load_scr(DM_manage);
    return editmenu;
}

static void FW_click_event(lv_event_t* e)
{

    ixg_ma->msg_type = ENTER_FW_UPGRADE;
        API_SettingMenuProcess(ixg_ma);
}

static void Dw_click_event(lv_event_t* e)
{
    ixg_ma->msg_type = IXG_MSG_RESTORE;
        API_SettingMenuProcess(ixg_ma);
}

static void Ab_click_event(lv_event_t* e)
{
    char* id = lv_event_get_user_data(e);
    strcpy(ixg_ma->ixg_id,id);
    ixg_ma->msg_type = ENTER_IXG_ABOUT;
        API_SettingMenuProcess(ixg_ma);

}

static void Re_click_event(lv_event_t* e)
{
    ixg_ma->msg_type = IXG_MSG_REBOOT;
        API_SettingMenuProcess(ixg_ma);
}
/**********************
 * 创建功能按键
 * parent ：父对象
 * txt ：按键文本
 * cb ： 按键回调
 * user_data ： 需要传递的参数
 * ********************/
lv_obj_t* IXG_create_btn(lv_obj_t* parent, char* txt, lv_event_cb_t cb,void* user_data)
{
    lv_obj_t* obj = lv_obj_create(parent);
    lv_obj_set_size(obj, LV_PCT(100), LV_SIZE_CONTENT);
    lv_obj_set_style_border_side(obj, 0, 0);
    lv_obj_set_style_bg_color(obj, lv_color_make(230, 231, 232), 0);
    //lv_obj_set_style_text_color(obj, lv_color_white(), 0);
    lv_obj_set_style_text_font(obj, &lv_font_montserrat_32, 0);
    lv_obj_add_flag(obj, LV_OBJ_FLAG_CLICKABLE);

    lv_obj_t* text_label;
    if (txt) {
        text_label = lv_label_create(obj);
        //lv_label_set_text(text_label, txt);
        vtk_label_set_text(text_label, txt);
        lv_obj_align(text_label, LV_ALIGN_LEFT_MID, 0, 0);
    }
    if(cb){
        lv_obj_add_event_cb(obj, cb, LV_EVENT_CLICKED, user_data);
    }    
    return obj;
}

static void dm_back_event(lv_event_t* e)
{
    lv_disp_load_scr(DM_back_scr);

    DM_delete();
}
/**********************
 * 创建About显示页面
 * json ：当前IXG的信息
 * ********************/
lv_obj_t* About_menu(cJSON* json)
{
    if(json){
        cJSON* fw = API_GetRemotePb(GetIXGIp(),PB_FW_VER);
        cJSON_AddItemToObjectCS(json,PB_FW_VER,cJSON_Duplicate(fw,true));
        cJSON_Delete(fw);
    }
    aboutmenu = lv_obj_create(lv_scr_act());
    lv_obj_set_size(aboutmenu, 480, 800);
    lv_obj_set_style_pad_all(aboutmenu, 0, 0);
    lv_obj_clear_flag(aboutmenu, LV_OBJ_FLAG_SCROLLABLE);

    IXG_add_title(aboutmenu, "About");          //添加标题

    lv_obj_t* btnCont = lv_obj_create(aboutmenu);
    lv_obj_set_size(btnCont, 480, 100);
    lv_obj_set_style_pad_all(btnCont, 0, 0);
    lv_obj_set_y(btnCont, 700);

    lv_obj_t* saveBtn = lv_btn_create(btnCont);     //添加返回按钮
    lv_obj_set_size(saveBtn, 120, 70);
    lv_obj_set_style_text_font(saveBtn, &lv_font_montserrat_32, 0);
    lv_obj_add_event_cb(saveBtn, back_about_event, LV_EVENT_CLICKED, NULL);
    lv_obj_align(saveBtn, LV_ALIGN_LEFT_MID, 20, 0);
    lv_obj_set_style_radius(saveBtn, LV_RADIUS_CIRCLE, 0);
    lv_obj_set_style_bg_color(saveBtn, lv_color_make(230, 231, 232), 0);
    lv_obj_set_style_text_color(saveBtn, lv_color_black(), 0);
    lv_obj_t* savelabel = lv_label_create(saveBtn);
    lv_label_set_text(savelabel, LV_SYMBOL_BACKSPACE);
    lv_obj_center(savelabel);

    lv_obj_t* editCont = lv_obj_create(aboutmenu);
    lv_obj_set_size(editCont, 480, 620);
    lv_obj_set_y(editCont, 80);
    lv_obj_set_flex_flow(editCont, LV_FLEX_FLOW_COLUMN);
    lv_obj_set_style_pad_all(editCont, 0, 0);
    lv_obj_set_style_pad_row(editCont, 0, 0);

    int length = cJSON_GetArraySize(json);
    char buf1[100];
    //依次添加信息栏
    for (size_t i = 0; i < length; i++)
    {   
        cJSON* item = cJSON_GetArrayItem(json, i);
        if(item == NULL)
            continue;
        if(cJSON_IsString(item)){
            sprintf(buf1,"%s",item->valuestring);

        }else if(cJSON_IsNumber(item)){
            sprintf(buf1,"%d",item->valueint);
   
        }else{
            continue;
        }

        IXG_load_text(editCont,item->string,buf1,NULL);
    }


    return aboutmenu;
}
/**********************
 * 添加信息显示栏
 * parent ：父对象
 * name ：名字
 * value ： 值
 * cb  ：值点击后的回调函数
 * ********************/
lv_obj_t* IXG_load_text(lv_obj_t* parent,char* name,char* value,lv_event_cb_t cb)
{
    lv_obj_t* obj = lv_obj_create(parent);
    lv_obj_set_size(obj, LV_PCT(100), LV_SIZE_CONTENT);
    lv_obj_set_style_pad_row(obj, 0, 0); 
    lv_obj_clear_flag(obj, LV_OBJ_FLAG_SCROLLABLE);
    lv_obj_set_flex_flow(obj, LV_FLEX_FLOW_ROW);
    lv_obj_set_style_text_font(obj, &lv_font_montserrat_32, 0);
   
    lv_obj_t* label1 = lv_label_create(obj);
    lv_label_set_text(label1, name);
    lv_obj_set_size(label1, 200, LV_SIZE_CONTENT);

    lv_obj_t* label2 = lv_label_create(obj);
    lv_label_set_text(label2, value);
    if(cb){
        lv_obj_add_flag(label2, LV_OBJ_FLAG_CLICKABLE);
        lv_obj_add_event_cb(label2, cb, LV_EVENT_CLICKED, NULL);
        lv_obj_set_ext_click_area(label2,50);
    } 

    return obj;
}

static void back_about_event(lv_event_t* e)
{  
    if(aboutmenu){
        lv_obj_del(aboutmenu);
        aboutmenu = NULL;
    }      
}


bool DM_delete(void)
{
    if (DM_manage)
    {
        lv_obj_del(DM_manage);
        DM_manage = NULL;
    }
        
}

static lv_obj_t* IXG_DM_init(void)
{
    lv_obj_t* DM_scr = lv_obj_create(NULL);
    lv_obj_clear_flag(DM_scr, LV_OBJ_FLAG_SCROLLABLE);
    lv_obj_remove_style_all(DM_scr);
    lv_obj_set_size(DM_scr, lv_disp_get_hor_res(NULL), lv_disp_get_ver_res(NULL));
    lv_obj_align(DM_scr, LV_ALIGN_TOP_LEFT, 0, 0);
    lv_obj_set_style_bg_color(DM_scr, lv_palette_main(LV_PALETTE_GREY), 0);
    lv_obj_set_style_bg_opa(DM_scr, LV_OPA_100, 0);
    
    return DM_scr;
}