#include "IXG_Manage.h"
#include "IM_Manage.h"
#include "obj_lvgl_msg.h"
#include "utility.h"
#include "icon_common.h"

lv_obj_t* IXG_manage = NULL;
lv_obj_t* manage_back_scr = NULL;
lv_obj_t* manage_menu = NULL;
static lv_obj_t* cntlabel = NULL;
static lv_obj_t* pagelabel = NULL;
static IXG_DATA_T*  ixg_online = NULL;
static IXG_DATA_T*  ixg_all = NULL;
IXG_MANAGE* ixg_ma = NULL;
bool state = false;
lv_timer_t* ixg_timer = NULL;
void* ixg_msg = NULL;
void printCurrentTime(char* string)
{
    char buf[100];
    printf("%s: %s\n", string, GetCurrentTime(buf, 100, "%Y-%m-%d %H:%M:%S"));
}
/**********************
 * 设置IXG表格的数据
 * json ：所有在线IXG的信息
 * ********************/
int ixg_disp_data_set(cJSON* json)
{     
    char* a = cJSON_Print(json);
    printf("ixg_disp_data_set             %s\n",a);
    free(a);

    int length = cJSON_GetArraySize(json);
    if(state){                          //判断状态为ALL还是ONLINE
        ixg_all->cnt = 0;
        for (size_t i = 0; i < 63; i++)         //添加全部IXG
        {
            ixg_all->all[i].ixg_id =  i+1;
            ixg_all->all[i].online =  0;
            ixg_all->cnt++;
        }
        for (size_t i = 0; i < length; i++)     //修改在线信息
        {
            cJSON* item = cJSON_GetArrayItem(json, i);

            cJSON *id = cJSON_GetObjectItem(item,"IXG_ID");
            cJSON *im = cJSON_GetObjectItem(item,"DT_IM");
            int size = cJSON_GetArraySize(im);

            ixg_all->all[id->valueint-1].ixg_id =  id->valueint;
            ixg_all->all[id->valueint-1].online =  size;
            ixg_all->page_cur_online[i] = id->valueint;
        }

        ixg_all->last_page_cnt = ixg_all->cnt % PAGE_SIZE;
 
        if(ixg_all->last_page_cnt == 0){
            ixg_all->page_all = ixg_all->cnt / PAGE_SIZE + 1;
        }else{
            ixg_all->page_all = ixg_all->cnt / PAGE_SIZE + 1;
        }

    }else{
        ixg_online->cnt = 0;     
        for (size_t i = 0; i < length; i++)         
        {
            cJSON* item = cJSON_GetArrayItem(json, i);

            cJSON *id = cJSON_GetObjectItem(item,"IXG_ID");
            cJSON *im = cJSON_GetObjectItem(item,"DT_IM");
            int size = cJSON_GetArraySize(im);
            ixg_online->all[i].ixg_id =  id->valueint;
            ixg_online->all[i].online =  size;
            ixg_online->cnt++;
            ixg_online->page_cur_online[i] = id->valueint;
        }
        ixg_online->last_page_cnt = ixg_online->cnt % PAGE_SIZE;
        if(ixg_online->last_page_cnt == 0){
            ixg_online->page_all = ixg_online->cnt / PAGE_SIZE + 1;
        }else{
            ixg_online->page_all = ixg_online->cnt / PAGE_SIZE + 1;
        }
    }
}

int ixg_disp_data_set_one(IXG_DATA_T* pdata, int ixg_id, int online,int cnt)
{  
    //pdata->all[cnt].ixg_id =  ixg_id;
    //pdata->all[cnt].online =  online;
    //pdata->cnt = cnt;
    return pdata->cnt;
}
/**********************
 * 初始化IXG在线显示页面
 * json ：所有在线IXG的信息
 * ********************/
lv_obj_t* IXG_menu_creat(cJSON* json)
{
    ixg_ma = (IXG_MANAGE*)lv_mem_alloc(sizeof(IXG_MANAGE));
    lv_memset_00(ixg_ma, sizeof(IXG_MANAGE));
    ixg_ma->ixg_id = lv_mem_alloc(sizeof(char*)*10);   

    ixg_all = (IXG_DATA_T*)lv_mem_alloc(sizeof(IXG_DATA_T));
    lv_memset_00(ixg_all, sizeof(IXG_DATA_T));
    
    ixg_online = (IXG_DATA_T*)lv_mem_alloc(sizeof(IXG_DATA_T));
    lv_memset_00(ixg_online, sizeof(IXG_DATA_T));  

    manage_back_scr = lv_scr_act();
    IXG_manage = ui_manage_init();

    lv_obj_t* cont = lv_obj_create(IXG_manage);
    lv_obj_set_size(cont, 480, 800);
    lv_obj_set_style_pad_all(cont, 0, 0);
    lv_obj_clear_flag(cont, LV_OBJ_FLAG_SCROLLABLE);

    IXG_add_title(cont, "IXG Management");

    IXG_add_filter(cont);

    IXG_add_change_page(cont, 1, 2, 63); 

    ixg_disp_data_set(json);

    IXG_add_manage(cont, 1); 

    lv_disp_load_scr(IXG_manage);
    return cont;
}
/**********************
 * 初始化标题栏
 * parent ：父对象
 * txt ： 标题文本
 * ********************/
void IXG_add_title(lv_obj_t* parent,char* txt)
{
    lv_obj_t* titleCont = lv_obj_create(parent);
    lv_obj_set_size(titleCont, 480, 80);
    lv_obj_set_style_pad_all(titleCont, 0, 0);
    lv_obj_t* titlelabel = lv_label_create(titleCont);
    lv_obj_set_style_text_font(titlelabel, &lv_font_montserrat_32, 0);
    //lv_obj_set_style_text_color(titlelabel, lv_color_hex(0xff0000), 0);
    lv_obj_center(titlelabel);
    //lv_label_set_text(titlelabel, txt);
    vtk_label_set_text(titlelabel, txt);
}
/**********************
 * 初始化控制栏
 * parent ：父对象
 * ********************/
static void IXG_add_filter(lv_obj_t* parent)
{
    lv_obj_t* filterCont = lv_obj_create(parent);
    lv_obj_set_style_pad_row(filterCont, 0, 0);
    lv_obj_set_size(filterCont, LV_PCT(100), 100);
    //lv_obj_set_flex_flow(filterCont, LV_FLEX_FLOW_ROW);
    lv_obj_set_style_text_font(filterCont, font_medium, 0);
    lv_obj_clear_flag(filterCont, LV_OBJ_FLAG_SCROLLABLE);
    lv_obj_set_y(filterCont, 80);
    

    lv_obj_t* filter_label = lv_label_create(filterCont);
    //lv_label_set_text(filter_label, "Mode : ");
    vtk_label_set_text(filter_label, "Mode:");
    lv_obj_set_size(filter_label, 150, LV_SIZE_CONTENT);
    lv_obj_align(filter_label, LV_ALIGN_LEFT_MID, 0, 0);

    lv_obj_t* sw = lv_switch_create(filterCont);        //状态切换开关
    lv_obj_set_size(sw, 120, 60);
    if(state){
        lv_obj_add_state(sw, LV_STATE_CHECKED);
    }else{
        lv_obj_clear_state(sw, LV_STATE_CHECKED);
    }
    
    lv_obj_align(sw, LV_ALIGN_CENTER, -20, 0);
    lv_obj_add_event_cb(sw, sw_change_event, LV_EVENT_VALUE_CHANGED, NULL);

    lv_obj_t* refreshBtn = lv_btn_create(filterCont);           //刷新按钮
    lv_obj_align(refreshBtn, LV_ALIGN_RIGHT_MID, -10, 0);
    //lv_obj_set_style_bg_color(backBtn, lv_color_make(230, 231, 232), 0);
    lv_obj_set_size(refreshBtn, 120, 60);
    lv_obj_set_style_text_font(refreshBtn, font_medium, 0);
    ixg_msg = lv_msg_subsribe_obj(IXG_LIST_REFRESH, refreshBtn, NULL);    
    lv_obj_add_event_cb(refreshBtn, IXG_refresh_event, LV_EVENT_ALL, NULL);
    lv_obj_set_style_radius(refreshBtn, 0, 0);
    lv_obj_t* refreshlabel = lv_label_create(refreshBtn);
    //lv_label_set_text(refreshlabel, "Refresh");
    vtk_label_set_text(refreshlabel, "Refresh");
    lv_obj_center(refreshlabel);
}
/**********************
 * IXG刷新提示框
 * string ：提示文本
 * cb ：定时器回调
 * ********************/
lv_timer_t* IXG_refresh_tips(const char* string,lv_timer_cb_t timer_cb)
{     
    lv_obj_t* msg_cont = lv_obj_create(lv_layer_top());             //创建提示框
    lv_obj_set_size(msg_cont, 300, LV_SIZE_CONTENT);
    lv_obj_add_flag(lv_layer_top(), LV_OBJ_FLAG_CLICKABLE);         //模态窗口
	lv_obj_set_style_bg_color(msg_cont, lv_color_make(74,132,101), 0);
    
    lv_obj_align(msg_cont, LV_ALIGN_CENTER, 0, 0);

    lv_obj_t* msg_txt = lv_label_create(msg_cont);
	lv_obj_set_size(msg_txt, LV_PCT(100), LV_SIZE_CONTENT);
    //lv_label_set_text(msg_txt, string);
    vtk_label_set_text(msg_txt, string);
    lv_obj_set_style_text_font(msg_txt, &lv_font_montserrat_32, LV_STATE_DEFAULT);
	lv_obj_set_style_text_color(msg_txt,lv_color_white(),0);
    lv_obj_center(msg_txt);

    lv_timer_t* timer = lv_timer_create(timer_cb, 50000, msg_cont);         //创建20S的定时器

    //lv_obj_add_event_cb(msg_cont, event_cb, LV_EVENT_MSG_RECEIVED, NULL);

    return timer;
}
/**********************
 * IXG搜索超时,弹出提示框
 * ********************/
static void ixg_resfresh_timer_cb(lv_timer_t* timer)
{
    lv_obj_t* obj = (lv_obj_t*)ixg_timer->user_data;

    lv_obj_del(obj); 
    lv_timer_del(ixg_timer);
    ixg_timer = NULL;
    lv_obj_clear_flag(lv_layer_top(), LV_OBJ_FLAG_CLICKABLE);

    LV_API_TIPS(" Search failed, please check the device status!",4);
    //API_TIPS("Search failed, please check the device status!");
    ixg_ma->msg_type = IXG_MANAGE_REFRESH_RECEIVE;
    API_SettingMenuProcess(ixg_ma);

}
/**********************
 * IXG搜索未超时,成功获取结果,通知process更新显示
 * ********************/
static void ixg_refresh_event(lv_event_t* e)
{
    lv_obj_t* obj = lv_event_get_target(e);
    void* msg = lv_event_get_user_data(e);
    lv_msg_t* m = lv_event_get_msg(e);

    if(lv_msg_get_id(m) == FWUPGRADE_RECEIVE) { 
    
        lv_obj_del(obj);
        lv_obj_clear_flag(lv_layer_top(), LV_OBJ_FLAG_CLICKABLE);
        
        if(ixg_timer != NULL){
            lv_timer_del(ixg_timer);
            ixg_timer = NULL;
        }

        ixg_ma->msg_type = IXG_MANAGE_REFRESH_RECEIVE;
        API_SettingMenuProcess(ixg_ma);       
    }
}
/**********************
 * LV_EVENT_CLICKED : 点击事件
 * LV_EVENT_MSG_RECEIVED ： 接收结果事件
 * ********************/
static void IXG_refresh_event(lv_event_t* e)
{
    lv_event_code_t code = lv_event_get_code(e);
    lv_msg_t* m = lv_event_get_msg(e);
    if(code == LV_EVENT_CLICKED) {
      

        ixg_timer = IXG_refresh_tips("Searching, please wait!",ixg_resfresh_timer_cb);      //生成不可点的模态提示框,并开始计时   
          ixg_ma->msg_type = IXG_MANAGE_REFRESH_SEND;         //通知后台搜索
        API_SettingMenuProcess(ixg_ma);
    }
    else if(code == LV_EVENT_MSG_RECEIVED) {        
        if(lv_msg_get_id(m) == IXG_LIST_REFRESH) {

            lv_obj_clear_flag(lv_layer_top(), LV_OBJ_FLAG_CLICKABLE);                       //解除模态

            if(ixg_timer != NULL){
                lv_timer_pause(ixg_timer);                                                  //暂停计时        
                lv_obj_t* obj = (lv_obj_t*)ixg_timer->user_data;
                lv_obj_del(obj);                                                            //删除提示框
                lv_timer_del(ixg_timer);                                                    //删除定时器
                ixg_timer = NULL;
            }

            ixg_ma->msg_type = IXG_MANAGE_REFRESH_RECEIVE;                                  //重刷页面
            API_SettingMenuProcess(ixg_ma);         
        }    
    }
}

static void sw_change_event(lv_event_t* e)
{
    lv_obj_t* sw = lv_event_get_target(e);
    
    if(lv_obj_has_state(sw,LV_STATE_CHECKED)){
        state = true;
    }
    else
    {
        state = false;
    }

    ixg_ma->msg_type = IXG_MSG_CHANGE_STATE;
    API_SettingMenuProcess(ixg_ma);

}
/**********************
 * 初始化IXG-Table
 * parent ：Table的父对象
 * cur_page ： 当前页
 * ********************/
lv_obj_t* IXG_add_manage(lv_obj_t* parent, int cur_page)
{
    if(parent == NULL){
        return;
    }

    if(manage_menu == NULL){
        manage_menu = lv_table_create(parent);
        lv_obj_set_size(manage_menu, 480, 520);
        lv_obj_set_y(manage_menu, 180);
        lv_obj_clear_flag(manage_menu, LV_OBJ_FLAG_SCROLLABLE);

        lv_obj_add_event_cb(manage_menu, ixg_draw_table_event_cb, LV_EVENT_DRAW_PART_BEGIN, NULL);
        lv_obj_add_event_cb(manage_menu, ixg_table_event_cb, LV_EVENT_VALUE_CHANGED, NULL);
    }
   
    //lv_obj_set_style_outline_width(manage_menu,1,LV_PART_ITEMS);
    lv_table_set_row_cnt(manage_menu,0);
    lv_table_set_col_cnt(manage_menu,0);
    lv_table_set_row_height(manage_menu,65);

    if(state){
        ixg_disp_updata(ixg_all,cur_page);
        ixg_all->page_cur = cur_page;
        if(pagelabel){
            lv_label_set_text_fmt(pagelabel, "%d/%d", ixg_all->page_cur, ixg_all->page_all);
        }
        if(cntlabel){
            lv_label_set_text_fmt(cntlabel, "%d", ixg_all->cnt);
        }
        for(int i=0;i<4;i++){
            lv_table_set_col_width(manage_menu, i, 120);
        }
    }else{
        ixg_disp_updata(ixg_online,cur_page);
        ixg_online->page_cur = cur_page;
        if(pagelabel){
            lv_label_set_text_fmt(pagelabel, "%d/%d", ixg_online->page_cur,ixg_online->page_all );
        }
        if(cntlabel){
            lv_label_set_text_fmt(cntlabel, "%d", ixg_online->cnt);
        }
        if(ixg_online->cnt < 4){
            for(int i=0;i<ixg_online->cnt;i++){
                lv_table_set_col_width(manage_menu, i, 120);
            }
        }else{
            for(int i=0;i<4;i++){
                lv_table_set_col_width(manage_menu, i, 120);
            }
        }
    }   

    return manage_menu;
}
/**********************
 * table数据显示
 * pdata ：需要显示的数据
 * cur_page ： 当前页
 * ********************/
int ixg_disp_updata(IXG_DATA_T* pdata,int page)
{
    int length;
    if(page == pdata->page_all){
        length = pdata->last_page_cnt;
    }else{
        length = PAGE_SIZE;
    }
    // for all, get online 
    // update page all, page_cur;
    // display    
    for( int i = 0; i < length; i++ )
    {
        int row = i / 4;

        int col = i % 4;

        int cur_ixg = (page-1)*PAGE_SIZE + i;              
        lv_table_set_cell_value_fmt(manage_menu, row, col, "%d(%d)",pdata->all[cur_ixg].ixg_id,pdata->all[cur_ixg].online);         //显示单格数据
            
    }

    return 0;
}
/**********************
 * table绘画事件
 * ********************/
static void ixg_draw_table_event_cb(lv_event_t* e)
{
    lv_obj_t* obj = lv_event_get_target(e);
    int cnt = -1;
    int id = -1;
    lv_obj_draw_part_dsc_t* dsc = lv_event_get_draw_part_dsc(e);
    /*If the cells are drawn...*/

    if (dsc->part == LV_PART_ITEMS) {
        uint32_t row = dsc->id / lv_table_get_col_cnt(obj);
        uint32_t col = dsc->id - row * lv_table_get_col_cnt(obj);

        dsc->label_dsc->font = &lv_font_montserrat_30;
        dsc->label_dsc->align = LV_TEXT_ALIGN_CENTER;
        //对在线的格子进行染色
        if(state){           
            for(int i=0;i<PAGE_SIZE;i++){
                cnt = ixg_all->page_cur_online[i];
                id = (ixg_all->page_cur-1)*PAGE_SIZE + dsc->id + 1;
                if(cnt == id)
                   dsc->rect_dsc->bg_color = lv_color_make(149,236,105); 
            }
        }else{
            dsc->rect_dsc->bg_color = lv_color_make(149,236,105); 
        }                    
    }

}

static void ixg_table_event_cb(lv_event_t* e)
{
    lv_obj_t* obj = lv_event_get_target(e);

    uint16_t col;
    uint16_t row;
    lv_table_get_selected_cell(obj, &row, &col);
    char* text =  lv_table_get_cell_value(obj,row,col);   
    if(strcmp(text,"") == 0){
        return;
    }
    char buf[10]; 
    if(state){                  //获取被点击格子的IXG-ID
        int id = 4*row+col;
        sprintf(buf,"%d",ixg_all->all[id].ixg_id);
    }else{
        int id = 4*row+col;
        sprintf(buf,"%d",ixg_online->all[id].ixg_id);
    }
    strcpy(ixg_ma->ixg_id,buf);
    printf(" ixg_table_event_cb                         id = %s\n",buf);
    ixg_ma->msg_type = IXG_MSG_ENTER_IM;
    API_SettingMenuProcess(ixg_ma);
}

lv_obj_t* IXG_load_menu(lv_obj_t* parent,char* id,char* im)
{
    lv_obj_t* obj = lv_obj_create(parent);
    lv_obj_set_size(obj, LV_PCT(25), 65);
    lv_obj_set_style_pad_row(obj, 0, 0); 
    lv_obj_clear_flag(obj, LV_OBJ_FLAG_SCROLLABLE);
    lv_obj_add_flag(obj, LV_OBJ_FLAG_CLICKABLE);
    lv_obj_add_event_cb(obj, menu_change_event_cb, LV_EVENT_CLICKED, NULL); 
   
    lv_obj_t* label1 = lv_label_create(obj);
    lv_label_set_text(label1, id);
    lv_obj_align(label1, LV_ALIGN_LEFT_MID, 0, 0);
    lv_obj_set_style_text_font(label1, &lv_font_montserrat_32, 0);

    lv_obj_t* label2 = lv_label_create(obj);
    lv_label_set_text(label2, im);
    lv_obj_align(label2, LV_ALIGN_RIGHT_MID, 0, 0);
    lv_obj_set_style_text_font(label2, &lv_font_montserrat_24, 0);

    lv_obj_refresh_self_size(obj);
    lv_obj_invalidate(obj);
    
    return obj;
}
/**********************
 * 创建页面切换栏
 * parent ： 父对象
 * cnt_page ： 当前页
 * max_page ：最大页数
 * size ：当前个数
 * ********************/
void IXG_add_change_page(lv_obj_t* parent, int cnt_page, int max_page, int size)
{
    lv_obj_t* pageControl = lv_obj_create(parent);
    lv_obj_set_size(pageControl, 480, 100);
    lv_obj_set_style_pad_all(pageControl, 0, 0);
    lv_obj_set_y(pageControl, 700);

    lv_obj_t* backBtn = lv_btn_create(pageControl);         //添加返回按钮
    lv_obj_align(backBtn, LV_ALIGN_LEFT_MID, 0, 0);
    lv_obj_set_style_text_color(backBtn, lv_color_hex(0xff0000), 0);
    lv_obj_set_style_bg_color(backBtn, lv_color_make(230, 231, 232), 0);
    lv_obj_set_size(backBtn, 100, 70);
    lv_obj_set_style_text_font(backBtn, &lv_font_montserrat_32, 0);
    lv_obj_add_event_cb(backBtn, manage_exit_event, LV_EVENT_CLICKED, NULL);
    lv_obj_set_style_radius(backBtn, LV_RADIUS_CIRCLE, 0);
    lv_obj_t* backlabel = lv_label_create(backBtn);
    lv_label_set_text(backlabel, LV_SYMBOL_BACKSPACE);
    lv_obj_center(backlabel);

    lv_obj_t* leftBtn = lv_btn_create(pageControl);         //添加左翻页按钮
    lv_obj_align(leftBtn, LV_ALIGN_LEFT_MID, 120, 0);
    lv_obj_set_style_text_color(leftBtn, lv_color_hex(0xff0000), 0);
    lv_obj_set_style_bg_color(leftBtn, lv_color_make(230, 231, 232), 0);
    lv_obj_set_size(leftBtn, 80, 70);
    lv_obj_set_style_text_font(leftBtn, &lv_font_montserrat_32, 0);
    lv_obj_add_event_cb(leftBtn, IXG_left_click_event, LV_EVENT_CLICKED, NULL);
    lv_obj_set_style_radius(leftBtn, LV_RADIUS_CIRCLE, 0);
    lv_obj_t* leftlabel = lv_label_create(leftBtn);
    lv_label_set_text(leftlabel, LV_SYMBOL_LEFT);
    lv_obj_center(leftlabel);

    char buf[100];
    sprintf(buf, "%d/%d", cnt_page, max_page);              

    pagelabel = lv_label_create(pageControl);               //页面显示
    lv_label_set_text(pagelabel, buf);
    lv_obj_set_style_text_font(pagelabel, &lv_font_montserrat_32, 0);
    lv_obj_align(pagelabel, LV_ALIGN_CENTER, 0, 0);
    //lv_msg_subsribe_obj(IXG_MANAGE_CHANGE_PAGE, pagelabel, NULL);
    //lv_obj_add_event_cb(pagelabel, IXG_page_change_event_cb, LV_EVENT_MSG_RECEIVED, NULL);

    lv_obj_t* rightBtn = lv_btn_create(pageControl);        //添加右翻页按钮
    lv_obj_align(rightBtn, LV_ALIGN_CENTER, 90, 0);
    lv_obj_set_style_text_color(rightBtn, lv_color_hex(0xff0000), 0);
    lv_obj_set_style_bg_color(rightBtn, lv_color_make(230, 231, 232), 0);
    lv_obj_set_size(rightBtn, 80, 70);
    lv_obj_set_style_text_font(rightBtn, &lv_font_montserrat_32, 0);
    lv_obj_add_event_cb(rightBtn, IXG_right_click_event, LV_EVENT_CLICKED, NULL);
    lv_obj_set_style_radius(rightBtn, LV_RADIUS_CIRCLE, 0);
    lv_obj_t* rightlabel = lv_label_create(rightBtn);
    lv_label_set_text(rightlabel, LV_SYMBOL_RIGHT);
    lv_obj_center(rightlabel);

    sprintf(buf, "%d", size);
    cntlabel = lv_label_create(pageControl);                //个数显示
    lv_label_set_text(cntlabel, buf);
    lv_obj_set_style_text_font(cntlabel, &lv_font_montserrat_32, 0);
    lv_obj_align(cntlabel, LV_ALIGN_RIGHT_MID, -50, 0);
    //lv_msg_subsribe_obj(IXG_MANAGE_CHANGE_CNT, cntlabel, NULL);
    //lv_obj_add_event_cb(cntlabel, IXG_cnt_change_event_cb, LV_EVENT_MSG_RECEIVED, NULL);
}

static void IXG_left_click_event(lv_event_t* e)
{
    if(manage_menu == NULL)
        return;

     if(state){
        ixg_all->page_cur++;                        //增加当前页
        if(ixg_all->page_cur > ixg_all->page_all){
            ixg_all->page_cur = 1;
        }
        IXG_add_manage(lv_obj_get_parent(manage_menu),ixg_all->page_cur);       //刷新表格
     }else{
        ixg_online->page_cur++;
        if(ixg_online->page_cur > ixg_online->page_all){
            ixg_online->page_cur = 1;
        }
        IXG_add_manage(lv_obj_get_parent(manage_menu),ixg_online->page_cur);
     }
}

static void IXG_right_click_event(lv_event_t* e)
{
    if(manage_menu == NULL)
        return;
     if(state){
        ixg_all->page_cur--;                        //减少当前页
        if(ixg_all->page_cur <= 0){
            ixg_all->page_cur = ixg_all->page_all;
        }
        IXG_add_manage(lv_obj_get_parent(manage_menu),ixg_all->page_cur);       //刷新表格
     }else{
        ixg_online->page_cur--;
        if(ixg_online->page_cur <= 0){
            ixg_online->page_cur = ixg_online->page_all;
        }
        IXG_add_manage(lv_obj_get_parent(manage_menu),ixg_online->page_cur);
     } 
}

static void IXG_cnt_change_event_cb(lv_event_t* e)
{
    lv_obj_t* label = lv_event_get_target(e);
    if(manage_menu == NULL)
        return;
    int cnt =  lv_obj_get_child_cnt(manage_menu);

    char buf[100];
    sprintf(buf, "%d", cnt);
    lv_label_set_text(label, buf);
}

static void IXG_page_change_event_cb(lv_event_t* e)
{
    char buf[100];
    lv_obj_t* label = lv_event_get_target(e);
    if(manage_menu == NULL)
        return;
    int cnt =  lv_obj_get_child_cnt(manage_menu);
    lv_msg_t* m = lv_event_get_msg(e);
    int32_t val = (int32_t)lv_msg_get_payload(m);

    if(cnt < 32){
        sprintf(buf, "%d/%d", val, 1);
        lv_label_set_text(label, buf);
    }else{
        sprintf(buf, "%d/%d", val, 2);
        lv_label_set_text(label, buf);
    }
}

static void manage_exit_event(lv_event_t* e)
{
    lv_disp_load_scr(manage_back_scr);

    //manage_delete();

    ixg_ma->msg_type = IXG_MANAGE_EXIT;
    API_SettingMenuProcess(ixg_ma);
}

static void menu_change_event_cb(lv_event_t* e)
{
    lv_obj_t* obj = lv_event_get_target(e);
    lv_obj_t* label = lv_obj_get_child(obj,0);
    char* id = lv_label_get_text(label);
    //printf("menu_change_event_cb11111111111111   = %s\n",id);
    //strcpy(ixg_id,id);
    //printf("menu_change_event_cb222222222222222   = %s\n",ixg_id);

    ixg_ma->ixg_id = id;
    ixg_ma->msg_type = IXG_MSG_ENTER_IM;
    API_SettingMenuProcess(ixg_ma);
}

void manage_delete(void)
{
    if(ixg_msg){
        lv_msg_unsubscribe(ixg_msg);
        ixg_msg = NULL;
    }

    if(manage_menu)
    {
        lv_obj_del(manage_menu);
        manage_menu = NULL;
    }

    if (IXG_manage)
    {
        lv_obj_del(IXG_manage);
        IXG_manage = NULL;
    }

    if (ixg_ma)
    {
        free(ixg_ma->ixg_id);
        ixg_ma->ixg_id = NULL;
        free(ixg_ma);
        ixg_ma = NULL;
    }

    if(ixg_all){
        
        free(ixg_all);
        ixg_all = NULL;
    }

    if(ixg_online){
        
        free(ixg_online);
        ixg_online = NULL;
    }
   
}

static lv_obj_t* ui_manage_init(void)
{
    lv_obj_t* ui_manage = lv_obj_create(NULL);
    lv_obj_clear_flag(ui_manage, LV_OBJ_FLAG_SCROLLABLE);
    lv_obj_remove_style_all(ui_manage);
    lv_obj_set_size(ui_manage, lv_disp_get_hor_res(NULL), lv_disp_get_ver_res(NULL));
    lv_obj_align(ui_manage, LV_ALIGN_TOP_LEFT, 0, 0);
    lv_obj_set_style_bg_color(ui_manage, lv_palette_main(LV_PALETTE_GREY), 0);
    lv_obj_set_style_bg_opa(ui_manage, LV_OPA_100, 0);
    //lv_disp_load_scr(ui_manage);
    return ui_manage;
}
