#include "cJSON.h"
#include "obj_Ftp.h"
#include "obj_lvgl_msg.h"
#include "define_file.h"
#include <stdlib.h>
#include "utility.h"

typedef enum
{
	FwUpgrade_State_Uncheck = 0,
	FwUpgrade_State_Checking,
	FwUpgrade_State_CheckOk,
	FwUpgrade_State_Downloading,
	FwUpgrade_State_Intalling,
	FwUpgrade_State_Canceling,
	FwUpgrade_State_Intalled,
}Update_State_e;

#define	OnlineFwUpgrade_ICON_MAX	5
char onlineFwUpgrade_Code[9] = {0};
char onlineFwUpgrade_Server[40] = {0};
char onlineFwUpgrade_Server_Disp[3][40] = {"SD_Card", "47.91.88.33", "47.106.104.38"};
Update_State_e updateState=FwUpgrade_State_Uncheck;
char update_file_dir[500];
int networkInfo=0;
char serverinput[20];
static int updateTimeCnt = 0;
#define UPDATE_CODE_CLOUD_DIR 	"/home/userota/"

void MENU_146_OnlineFwUpgrade2_Init(void)
{	
	cJSON *view = cJSON_CreateObject();
	cJSON_AddStringToObject(view, "SIP_CONNECT_NETWORK", "PB");
	cJSON * netInfo = API_GetPbIo(GetIXGIp(), NULL, view);
	if(netInfo)
	{
		if(!strcmp(cJSON_GetObjectItemCaseSensitive(netInfo, "SIP_CONNECT_NETWORK")->valuestring, "LAN"))
		{
			networkInfo = 1;
		}
		else
		{
			networkInfo = 0;
		}
	}
	cJSON_Delete(netInfo);
	cJSON_Delete(view);	
	updateState = FwUpgrade_State_Uncheck;

}

void MENU_146_OnlineFwUpgrade2_Exit(void)
{
	//API_MenuIconDisplaySelectOff(ICON_045_Upgrade);
	if(!networkInfo)
	{
		remove_update_tempfile();
	}
}

void Send_UpdateEvent(int opt, int ftp, char* file)
{
	cJSON *send_event=cJSON_CreateObject();
	cJSON_AddStringToObject(send_event, "EventName","EventUpdateRequest");
	cJSON_AddStringToObject(send_event, "OPERATE", opt==0? "CHECK":"INSTALL");
	cJSON_AddStringToObject(send_event, "DOWNLOAD_TYPE", ftp==0? "TFTP":"FTP");
	cJSON_AddStringToObject(send_event, "REPORT_IP",GetSysVerInfo_IP());
	cJSON_AddStringToObject(send_event, "SERVER", ftp==1? onlineFwUpgrade_Server : GetSysVerInfo_IP());
	cJSON_AddStringToObject(send_event, "DOWNLOAD_FILE",file);
	API_XD_EventJson(GetIXGIp(),send_event);
	cJSON_Delete(send_event);
}

static int FTP_Down_CB(int state, FTP_FILE_STATISTICS statistics, void* data)
{
	switch(state)
	{
		case FTP_STATE_DOWN_ING:
			break;
		case FTP_STATE_DOWN_OK:
			snprintf(update_file_dir, 100, "%s%s.txt", IXG_Update_Folder, onlineFwUpgrade_Code);
			Send_UpdateEvent(0, 0, update_file_dir);
			break;	
		case FTP_STATE_PARA_ERR:
		case FTP_STATE_INIT_ERR:
		case FTP_STATE_DOWN_ERR:
			RemoteUpdate_Report("CHECK_ERROR");
			break;			
		default:
			break;
	}
	return 0;
}

static int FTP_Down2_CB(int state, FTP_FILE_STATISTICS statistics, void* data)
{
	char disp[40];
	switch(state)
	{
		case FTP_STATE_DOWN_ING:
			snprintf(disp, 40, "FTP_DOWNLOAD_ING %u%%", (int)(statistics.now*100.0/statistics.total));
			RemoteUpdate_Report(disp);
			break;
		case FTP_STATE_DOWN_OK:
			RemoteUpdate_Report("FTP_DOWNLOAD_OK");
			snprintf(update_file_dir, 100, "%s%s", IXG_Update_Folder, onlineFwUpgrade_Code);
			Send_UpdateEvent(1, 0, update_file_dir);
			break;	
		case FTP_STATE_PARA_ERR:
		case FTP_STATE_INIT_ERR:
		case FTP_STATE_DOWN_ERR:
			RemoteUpdate_Report("FTP_DOWNLOAD_ERROR");
			updateState = FwUpgrade_State_Canceling;
			break;			
		default:
			break;
	}
	return 0;
}
void remove_update_tempfile(void)
{
	char cmd_buff[200];
	if( access( IXG_Update_Folder, F_OK ) == 0 )
	{
		snprintf(cmd_buff, 200, "rm -r %s", IXG_Update_Folder);
		system(cmd_buff);
	}
}

int confirm_set_servercode(const char* input)
{
	if(strlen(input) != 8)
	{
		return 0;
	}
	
	strcpy(onlineFwUpgrade_Code, input);

	cJSON* newitem = cJSON_CreateObject();
	cJSON_AddStringToObject(newitem, "UpdateCode", onlineFwUpgrade_Code);

	API_WriteRemoteIo(GetIXGIp(), newitem);
	
	cJSON_Delete(newitem);
	return 1;
}
void confirm_set_server(char* select)
{
	strcpy(onlineFwUpgrade_Server, select);

	cJSON* newitem = cJSON_CreateObject();
	cJSON_AddStringToObject(newitem, "UpdateServer", onlineFwUpgrade_Server);
	
	API_WriteRemoteIo(GetIXGIp(), newitem);
	cJSON_Delete(newitem);
}
int InputServerSet(char* input)
{
	int temp[4];
	if(sscanf(input, "%d.%d.%d.%d", &temp[0], &temp[1], &temp[2], &temp[3]) == -1)
	{
		return 0;
	}
	if(temp[0] < 0 || temp[0] > 255 || temp[1] < 0 || temp[1] > 255 || temp[2] < 0 || temp[2] > 255 || temp[3] < 0 || temp[3] > 255)
	{
		return 0;
	}
	sprintf(onlineFwUpgrade_Server, "%03d.%03d.%03d.%03d", temp[0], temp[1], temp[2], temp[3]);
	cJSON* newitem = cJSON_CreateObject();
	cJSON_AddStringToObject(newitem, "UpdateServer", onlineFwUpgrade_Server);
	API_WriteRemoteIo(GetIXGIp(), newitem);
	cJSON_Delete(newitem);
	//popDisplayLastNMenu(2);
	return -1;
}


void FwUpgrade2InputServerProcess(void)
{
	//EnterKeypadMenu(KEYPAD_NUM, MESG_TEXT_ICON_FwUpgrage_Server2, serverinput, 15, COLOR_WHITE, onlineFwUpgrade_Server_Disp[0], 1, InputServerSet);
}

void OnlineFwUpgrade_Process(int msg_type,void* arg)
{
	char* select;
	char* input;
	char fileName[500];
	switch(msg_type)
	{
		case FWUPGRADE_SERVER:
			select = arg;
			if(updateState == FwUpgrade_State_Uncheck)
			{
				confirm_set_server(select);
			}
			break;
		case FWUPGRADE_DLCODE:
			input = arg;
			if(updateState == FwUpgrade_State_Uncheck)
			{
				confirm_set_servercode(input);
			}
			break;
		case FWUPGRADE_OPERATION:
			if(updateState == FwUpgrade_State_Uncheck)
			{
				updateState = FwUpgrade_State_Checking;
				if(networkInfo == 0)
				{
					snprintf(update_file_dir, 500, "%s/%s.txt", UPDATE_CODE_CLOUD_DIR, onlineFwUpgrade_Code);
					RemoteUpdate_Report("CHECK_START");
					if( access( IXG_Update_Folder, F_OK ) < 0 )
					{
						//mkdir( IXG_Update_Folder, 0777 );
						create_multi_dir(IXG_Update_Folder);
					}
					//bprintf("1111111:%s:%s\n",onlineFwUpgrade_Server, update_file_dir);
					FTP_StartDownload(onlineFwUpgrade_Server, update_file_dir, IXG_Update_Folder, NULL,  FTP_Down_CB);										
				}
				else
				{
					Send_UpdateEvent(0, 1, onlineFwUpgrade_Code);
				}	
			}
			else if(updateState == FwUpgrade_State_CheckOk)
			{
				LV_API_TIPS("Start the install!",1);
				//API_TIPS("Start the install!");
				//RemoteUpdate_Operation(MESG_TEXT_FwUpgrage_Install);
				updateState = FwUpgrade_State_Downloading;
				updateTimeCnt = 30;
				//OS_RetriggerTimer(&updatetimer);
				//AutoPowerOffReset();
				if(networkInfo == 0)
				{
					RemoteUpdate_Report("FTP_DOWNLOAD_START");
					//bprintf("1111111:%s:%s\n",onlineFwUpgrade_Server, onlineFwUpgrade_Code);
					snprintf(fileName, 500, "%s/%s", UPDATE_CODE_CLOUD_DIR, onlineFwUpgrade_Code);
					FTP_StartDownload(onlineFwUpgrade_Server, fileName, IXG_Update_Folder, NULL,  FTP_Down2_CB);
				}
				else
				{
					Send_UpdateEvent(1, 1, onlineFwUpgrade_Code);
				}
			}
		break;
		case FWUPGRADE_INIT:
			MENU_146_OnlineFwUpgrade2_Init();
			break;
		case FWUPGRADE_EXIT:
			MENU_146_OnlineFwUpgrade2_Exit();
			break;
	}
					
}
#if 0
void RemoteUpdate_Operation(int opt)
{
	uint16 x, y;
	POS pos;
	SIZE hv;
	
	OSD_GetIconInfo(ICON_011_PublicList5, &pos, &hv);
	x = pos.x+DISPLAY_DEVIATION_X;
	y = pos.y+(hv.v - pos.y)/2 - ext_font2_h/2;
	API_OsdStringClearExt(x, y, hv.h - x, CLEAR_STATE_H);
	if(opt != 0)
		API_OsdUnicodeStringDisplay(x, y, DISPLAY_LIST_COLOR, opt, 1, bkgd_w - x);
}
#endif
void RemoteUpdate_Report(char* msg)
{
	vtk_lvgl_lock();
	printf("RemoteUpdate_Report             %s\n",msg);
	if(strstr(msg,"INSTALL_OK")==NULL)
	{
		if(strcmp(msg, "WAITING_REBOOT")==0)
		{
			char buff[100];
			sprintf(buff,"%s,DO NOT POWER DOWN",msg);
			vtk_lvgl_lock();
			lv_msg_send(FWUPGRADE_RECEIVE,buff);
			vtk_lvgl_unlock();
		}
		else
		{
			vtk_lvgl_lock();
			lv_msg_send(FWUPGRADE_RECEIVE,msg);
			vtk_lvgl_unlock();
		}
	}

	if(!strcmp(msg, "CHECK_PASSED"))	
	{

		updateState = FwUpgrade_State_CheckOk;
		//RemoteUpdate_Operation(MESG_TEXT_FwUpgrage_InstallOrNot);
	}
	else if(!strcmp(msg, "CHECK_ERROR"))
	{
		updateState = FwUpgrade_State_Uncheck;

	}	
	else if(!strcmp(msg, "DOWNLOAD_OK") || !strcmp(msg, "INSTALLING"))
	{
		updateState = FwUpgrade_State_Intalling;

	}
	else if(!strcmp(msg, "DOWNLOAD_ERROR") || !strcmp(msg, "DOWNLOAD_STOP"))
	{
		updateState = FwUpgrade_State_Canceling;

	}
	else if(!strcmp(msg, "INSTALL_OK") || !strcmp(msg, "WAITING_REBOOT"))
	{
		updateState = FwUpgrade_State_Intalled;
	}
	vtk_lvgl_unlock();	
}

int CheckUpdateDownload(void)
{
	if(updateState == FwUpgrade_State_Downloading)
	{
		if(updateTimeCnt>0)
		{
			return 1;
		}
		else
		{
			return 0;
		}	
	}
	else
	{
		return 0;
	}
}
