#ifndef IM_MANAGE_H_
#define IM_MANAGE_H_

#include "lv_ix850.h"

typedef struct
{
    int      im[128];
    int      cnt;
    int      page_all;                //all page
    int      last_page_cnt;           //The number of last pages
    int      page_cur;                //Current page
    int      page_cur_online[32];     // The number of current pages online
} IM_DATA_T;

lv_obj_t* IM_menu_creat(cJSON* json,char* id);
static lv_obj_t* IM_manage_init(void);
static void IM_exit_event(lv_event_t* e);
void IM_add_change_page(lv_obj_t* parent, int cnt_page, int max_page, int size);
void IM_add_manage(lv_obj_t* parent, int cnt_page);
static void IM_add_filter(lv_obj_t* parent,char* id);
void IM_delete(void);
static void menu_change_event_cb(lv_event_t* e);
lv_obj_t* IM_load_menu(lv_obj_t* parent,char* im);
static void IM_left_click_event(lv_event_t* e);
static void IM_right_click_event(lv_event_t* e);
static void IM_cnt_change_event_cb(lv_event_t* e);
static void IM_page_change_event_cb(lv_event_t* e);
static void im_draw_table_event_cb(lv_event_t* e);
int im_disp_updata(IM_DATA_T* pdata,int page);
int im_disp_data_set(cJSON* json);
static void im_resfresh_timer_cb(lv_timer_t* e);
static void im_refresh_event(lv_event_t* e);
#endif 