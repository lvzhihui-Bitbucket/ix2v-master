
#include <stdio.h>
#include "lv_ix850.h"
#include "cJSON.h"
#include "define_file.h"

static lv_obj_t * ui_log;
static lv_obj_t * fontlabel;
static lv_style_t style_lable_text;
static lv_font_t * fonttest;
lv_font_t * fontlog=NULL;

void ui_log_init(void)
{
    lv_obj_t* font_win;
    lv_obj_t * cont;

    vtk_lvgl_lock(); 
    ui_log = lv_obj_create(NULL);
    lv_obj_remove_style_all(ui_log);
    lv_obj_set_size(ui_log, lv_disp_get_hor_res(NULL), lv_disp_get_ver_res(NULL));
    lv_obj_align(ui_log, LV_ALIGN_TOP_LEFT, 0, 0);
    //lv_obj_set_style_bg_color(ui_log, lv_color_hex(0x102030), 0);//lv_palette_main(LV_PALETTE_GREY)
    //lv_obj_set_style_bg_opa(ui_log, LV_OPA_100, 0);

    font_win = lv_obj_create(ui_log);
    lv_obj_remove_style_all(font_win);
    lv_obj_set_size(font_win, 680, 550);
	//lv_obj_center(font_win);
    lv_obj_align(font_win, LV_ALIGN_CENTER,-12,0);
    lv_obj_set_style_bg_color(font_win, lv_color_hex(0x102030), 0);//lv_palette_main(LV_PALETTE_BLUE_GREY)
    lv_obj_set_style_bg_opa(font_win, LV_OPA_100, 0);

    //cont = lv_win_get_content(font_win);  
    fontlabel =  lv_label_create(font_win);
    lv_obj_center(fontlabel);
	lv_label_set_long_mode(fontlabel, LV_LABEL_LONG_WRAP);
	lv_obj_set_size(fontlabel, 670, 530);

    lv_group_add_obj(lv_group_get_default() ,fontlabel);

    //lv_obj_t* ui_Label = lv_label_create(ui_log);
    //lv_obj_center(ui_Label);
    //lv_label_set_text_fmt(ui_Label, "ui_log(%d:%d)", lv_obj_get_x(font_win), lv_obj_get_y(font_win));
    vtk_lvgl_unlock(); 
}

void scr_log(void)
{
	FILE* file = NULL;
    char* text = NULL;    
	char filePath[100] = {0}; 

    lv_disp_load_scr(ui_log);

    if(fontlog == NULL)
    {
        ShellCmdPrintf("lv_font_load menu start!\n");
        sprintf(filePath, "S:%s", IX2V_FONT_22);
        fontlog = lv_font_load(filePath);
        if(fontlog == NULL)
        {
            ShellCmdPrintf("lv_font_load %s ERR!\n", IX2V_FONT_22);
            return;
        }
        lv_style_init(&style_lable_text);
        lv_style_set_text_font(&style_lable_text, fontlog);
        lv_style_set_text_color(&style_lable_text, lv_color_hex(0xFFFFFF));//0x1d1d25

        lv_obj_add_style(fontlabel, &style_lable_text, 0);
    }
		


	if((file=fopen(Easylogger_FILE,"r")) != NULL)//
	{
		fseek(file,0, SEEK_END);
		
		int size = ftell(file);
		
		if((text = malloc(size+1)) == NULL )
		{
			fclose(file);
			return;
		}

		fseek(file,0, SEEK_SET);
		
		fread(text, 1, size, file);
		
		text[size] = 0;
        //ShellCmdPrintf("scr_log %s\n", text);
		lv_label_set_text(fontlabel, text);

		fclose(file);
		free(text);
	}
}

void test_font(char* fontfile, char* textfile)
{
	FILE* file = NULL;
	char filePath[100] = {0}; 
    char* text = NULL;    

    if(fonttest)
    {
        lv_font_free(fonttest);	
        lv_style_reset(&style_lable_text);
    }
		
    sprintf(filePath, "S:/mnt/nfs/%s", fontfile);
	fonttest = lv_font_load(filePath);
    if(fonttest == NULL)
    {
        ShellCmdPrintf("lv_font_load %s ERR!\n", fontfile);
        return;
    }

    lv_disp_load_scr(ui_log);
    lv_style_init(&style_lable_text);
	lv_style_set_text_font(&style_lable_text, fonttest);
	lv_style_set_text_color(&style_lable_text, lv_color_hex(0x1d1d25));

    lv_obj_add_style(fontlabel, &style_lable_text, 0);

    sprintf(filePath, "/mnt/nfs/%s", textfile);
	if((file=fopen(filePath,"r")) != NULL)
	{
		fseek(file,0, SEEK_END);
		
		int size = ftell(file);
		
		if((text = malloc(size+1)) == NULL )
		{
			fclose(file);
			return;
		}

		fseek(file,0, SEEK_SET);
		
		fread(text, 1, size, file);
		
		text[size] = 0;
		lv_label_set_text_static(fontlabel, text);

		fclose(file);
		free(text);
	}
}

int ShellCmd_scr_test(cJSON *cmd)
{
	char* jsonstr = GetShellCmdInputString(cmd, 1);
	char *qr_str;
	char acc[40];
	char pwd[40];
    if( jsonstr != NULL )
    {

        switch( *jsonstr )
        {
            case '0':
                //video_turn_on_init();
                break;
            case '1':
                //scr_main();
                break;
            case '2':
            	//MenuScr2Test();

                //scr_settings();
                break;
            case '3':
                //scr_log();
                break;
            case '4':
			jsonstr=GetShellCmdInputString(cmd, 2);
			if(jsonstr==NULL)
			{
				ShellCmdPrintf("have no im_addr!\n");
				return 1;
			}
			if(get_sip_dt_im_acc_from_tb(atoi(jsonstr),acc,pwd)<0)
			{
				ShellCmdPrintf("have no acc\n");
				return 1;
			}
			qr_str=create_sip_phone_qrcode(acc,pwd);
			if(qr_str==NULL)
			{
				ShellCmdPrintf("create qr_code_str err\n");
				return 1;
			}
			ShellCmdPrintf("%s\n",qr_str);
                scr_qrcode(qr_str,NULL,NULL,NULL,NULL);
				free(qr_str);
                break;
		case '6':
			
			vtk_lvgl_lock();
			MenuCertManage_Init(NULL);
			//SwitchOneMenu(1,NULL,0);
			vtk_lvgl_unlock();
			break;
		case '7':
			vtk_lvgl_lock();
			EnterCardTableMenu();
			//popDisplayLastMenu();
			vtk_lvgl_unlock();
			break;
		case '8':
			MainMenu_Init();
			break;
		case '9':
			MainMenu_Close();
			break;
        }
    }
    else
    {
        ShellCmdPrintf("ShellCmd_scr_test para err!\n");
    }
}