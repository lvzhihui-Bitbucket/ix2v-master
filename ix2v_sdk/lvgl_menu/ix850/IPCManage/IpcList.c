﻿#include "menu_common.h"
#include "IpcMenuProcess.h"

extern const lv_font_t* font_small;
extern const lv_font_t* font_medium;
extern const lv_font_t* font_larger;


lv_obj_t* scr_ipclist = NULL;
lv_obj_t* ipclist_back_scr = NULL;
lv_obj_t* ipclistcont = NULL;
static lv_obj_t* title_label;
lv_obj_t* ipc_menulist(lv_obj_t* parent, cJSON* data);

static lv_obj_t* ui_ipclist_init(void)
{
    lv_obj_t* ui_ipclist = lv_obj_create(NULL);
    lv_obj_clear_flag(ui_ipclist, LV_OBJ_FLAG_SCROLLABLE);
    lv_obj_remove_style_all(ui_ipclist);
    lv_obj_set_size(ui_ipclist, lv_disp_get_hor_res(NULL), lv_disp_get_ver_res(NULL));
    lv_obj_align(ui_ipclist, LV_ALIGN_TOP_LEFT, 0, 0);
    lv_obj_set_style_bg_color(ui_ipclist, lv_palette_main(LV_PALETTE_GREY), 0);
    lv_obj_set_style_bg_opa(ui_ipclist, LV_OPA_100, 0);
    return ui_ipclist;
}

static void ipclist_menuback(lv_event_t* e)
{
    lv_disp_load_scr(ipclist_back_scr);
    MenuIpcManage_Process(MSG_IPC_Manage_Exit, NULL);
}
static void ipcsearch_event(lv_event_t* e)
{
    MenuIpcManage_Process(MSG_IPC_SEARCH_REQ, NULL);
}
lv_obj_t* ipclist_menu_creat(cJSON* json)
{
    ipclist_back_scr = lv_scr_act();
    scr_ipclist = ui_ipclist_init();

    lv_obj_t* cont = lv_obj_create(scr_ipclist);
    lv_obj_set_size(cont, 480, 800);
    lv_obj_set_style_pad_all(cont, 0, 0);
    lv_obj_clear_flag(cont, LV_OBJ_FLAG_SCROLLABLE);

    lv_obj_t* titleCont = lv_obj_create(cont);
    lv_obj_set_size(titleCont, 480, 80);
    lv_obj_set_style_pad_all(titleCont, 0, 0);
    lv_obj_clear_flag(titleCont, LV_OBJ_FLAG_SCROLLABLE);

    lv_obj_t* backBtn = lv_btn_create(titleCont);
    lv_obj_remove_style_all(backBtn);
    lv_obj_add_event_cb(backBtn, ipclist_menuback, LV_EVENT_CLICKED, NULL);
    lv_obj_align(backBtn, LV_ALIGN_LEFT_MID, 0, 0);
    lv_obj_set_size(backBtn, 80, 80);
    lv_obj_t* backLabel = lv_label_create(backBtn);
    lv_label_set_text(backLabel, LV_VTK_BACK);
    lv_obj_align(backLabel, LV_ALIGN_CENTER, 0, 0);
    lv_obj_set_style_text_font(backLabel, font_larger, 0);

    lv_obj_t* searchBtn = lv_btn_create(titleCont);
    lv_obj_remove_style_all(searchBtn);
    lv_obj_add_event_cb(searchBtn, ipcsearch_event, LV_EVENT_CLICKED, NULL);
    lv_obj_align(searchBtn, LV_ALIGN_RIGHT_MID, 0, 0);
    lv_obj_set_size(searchBtn, 80, 80);
    lv_obj_t* searchLabel = lv_label_create(searchBtn);
    lv_label_set_text(searchLabel, LV_VTK_SEARCH);
    lv_obj_align(searchLabel, LV_ALIGN_CENTER, 0, 0);
    lv_obj_set_style_text_font(searchLabel, font_larger, 0);

    title_label = lv_label_create(titleCont);
    lv_obj_set_style_text_font(title_label, &lv_font_montserrat_32, 0);
    lv_obj_center(title_label);
    //lv_label_set_text(title_label, "Ipc List");
    vtk_label_set_text(title_label, "Ipc List");
    ipc_menulist(cont, json);
    lv_disp_load_scr(scr_ipclist);
    return cont;
}

void ipcmanage_delete(void)
{
    if(ipclistcont)
    {
        lv_obj_del(ipclistcont);
        ipclistcont = NULL;
        //printf("11111111%s:%d\n",__func__,__LINE__);
    }
    if (scr_ipclist)
    {
        lv_obj_del(scr_ipclist);
        scr_ipclist = NULL;
        //printf("11111111%s:%d\n",__func__,__LINE__);
    }
}

static void ipclist_click_event_cb(lv_event_t* e)
{
    cJSON* table = lv_event_get_user_data(e);
    cJSON *item = NULL;
    char ip[40] = {0};
    lv_event_code_t code = lv_event_get_code(e);
    lv_obj_t* obj = lv_event_get_target(e);
    if (code == LV_EVENT_CLICKED)
    {
        item = cJSON_GetArrayItem(table, lv_obj_get_index(obj));
        if(item != NULL)
        {
            MenuIpcManage_Process(MSG_IPC_LOGIN_MENU, item);
        }
    }
       
}
lv_obj_t* ipc_menulist(lv_obj_t* parent, cJSON* data)
{
    cJSON *item = NULL;
    lv_obj_t * itembtn;
    char name[100] = {0};
    if(ipclistcont)
    {
        lv_obj_del(ipclistcont);
        ipclistcont = NULL;
    }

    ipclistcont = lv_list_create(parent);
    lv_obj_set_size(ipclistcont, 480, 600);
    lv_obj_set_y(ipclistcont, 100);
    lv_obj_set_style_pad_row(ipclistcont, 0, 0);
    lv_obj_set_flex_flow(ipclistcont, LV_FLEX_FLOW_COLUMN);
    cJSON_ArrayForEach(item, data)
    {
        GetJsonDataPro(item, "IPC_NAME", name);
        itembtn = lv_list_add_btn(ipclistcont, LV_SYMBOL_LIST, name);
        lv_obj_set_style_text_font(itembtn, font_medium, 0);
        lv_obj_set_size(itembtn, LV_PCT(95), 80);
        lv_obj_add_event_cb(itembtn, ipclist_click_event_cb, LV_EVENT_CLICKED, data);
    }
}