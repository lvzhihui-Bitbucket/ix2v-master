#include <stdio.h>
#include "define_file.h"
#include "obj_TableSurver.h"
#include "IpcMenuProcess.h"
#include "lv_demo_msg.h"

static lv_obj_t* ipc_manage = NULL;
static lv_obj_t* ipc_login = NULL;
void *tips=NULL;
cJSON *Create_New_Ipcdat(cJSON *login, cJSON *result);
cJSON *filter_ipc_from_tb(cJSON *filter);

void MenuIpcManage_Init(void)
{
	//printf("11111111%s:%d\n",__func__,__LINE__);
	cJSON* view = cJSON_CreateArray();
	API_TB_SelectBySortByName(TB_NAME_IPC_DEV_TB, view, NULL, 0);
	ipc_manage = ipclist_menu_creat(cJSON_Duplicate(view, 1));
	cJSON_Delete(view);
	//printf("11111111%s:%d\n",__func__,__LINE__);
}
void MenuIpcManage_Process(int msg_type,void* arg)
{
	cJSON *ipcsearch = NULL;
	cJSON *ipclogin = NULL;
	cJSON *loginresult = NULL;
	cJSON *ipcsave = NULL;
	cJSON *win = NULL;
	switch(msg_type)
	{
		case MSG_IPC_SEARCH_REQ:
			//timer = API_TIPS("ipc searching...");
			ipcsearch = cJSON_CreateArray();
			IpcDeviceDiscovery(1000, NULL, ipcsearch);
			//API_TIPS_Close(timer);
			printf_json(ipcsearch,"MSG_IPC_SEARCH_REQ");
			ipc_menulist(ipc_manage, cJSON_Duplicate(ipcsearch, 1));
			cJSON_Delete(ipcsearch);
			break;
		case MSG_IPC_LOGIN_MENU:
			ipclogin = filter_ipc_from_tb((cJSON*)arg);
			//printf_json(ipclogin,"MSG_IPC_LOGIN_MENU");
			ipc_login = ipclogin_menu_creat(cJSON_Duplicate(ipclogin, 1));
			cJSON_Delete(ipclogin);
			break;	
		case MSG_IPC_LOGIN_REQ:
			//printf_json((cJSON*)arg,"MSG_IPC_LOGIN_REQ");
			//timer = API_TIPS("ipc login...");
			loginresult = cJSON_CreateArray();
			if(IpcDeviceLogin((cJSON*)arg, loginresult) == 0)//login ok 
			{
				//API_TIPS_Close(timer);
				ipcsave = Create_New_Ipcdat((cJSON*)arg, loginresult);
				//printf_json(ipcsave,"MSG_IPC_LOGIN_OK");
				ipc_menulogin(ipc_login, cJSON_Duplicate(ipcsave, 1));
				cJSON_Delete(ipcsave);
			}
			else//login err
			{
				LV_API_TIPS(" ipc login err!",4);
				//API_TIPS("ipc login err!");
			}
			cJSON_Delete(loginresult);
			
			break;	
		case MSG_IPC_Add:
			//printf_json((cJSON*)arg,"MSG_IPC_Add");
			update_one_ipc_to_tb((cJSON*)arg);
			ipcsearch = cJSON_CreateArray();
			API_TB_SelectBySortByName(TB_NAME_IPC_DEV_TB, ipcsearch, NULL, 0);
			ipc_menulist(ipc_manage, cJSON_Duplicate(ipcsearch, 1));
			cJSON_Delete(ipcsearch);
			break;	
		case MSG_IPC_Del:
			delete_one_ipc_to_tb((cJSON*)arg);
			ipcsearch = cJSON_CreateArray();
			API_TB_SelectBySortByName(TB_NAME_IPC_DEV_TB, ipcsearch, NULL, 0);
			ipc_menulist(ipc_manage, cJSON_Duplicate(ipcsearch, 1));
			cJSON_Delete(ipcsearch);
			break;			
		case MSG_IPC_Manage_Exit:
			MenuIpcManage_Close();
			break;	
		case MSG_IPC_Login_Exit:
			ipclogin_delete();
			break;	
		#if 1
		case MSG_IPC_PREVIEW:
			printf_json((cJSON*)arg,"MSG_IPC_PREVIEW");
			set_menu_with_video_on();
			MainMenu_Reset_Time();
			ipc_preview_on();
			win = cJSON_CreateObject();
			cJSON_AddNumberToObject(win,"X",0);
			cJSON_AddNumberToObject(win,"Y",0);
			cJSON_AddNumberToObject(win,"W",480);
			cJSON_AddNumberToObject(win,"H",320);
			Start_OneIpc_Show(0, (cJSON*)arg, win, 0); 
			cJSON_Delete(win);
			break;	
		case MSG_IPC_PREVIEW_Exit:
			set_menu_with_video_off();
			Stop_OneIpc_Show(0);
			break;	
		#endif

	}
}

void MenuIpcManage_Close(void)
{
	//printf("11111111%s:%d\n",__func__,__LINE__);
	if(ipc_manage != NULL)
	{
        ipcmanage_delete();
        ipc_manage = NULL;
    }	
	if(ipc_login != NULL)
	{
        ipclogin_delete();
        ipc_login = NULL;
    }	
	//printf("11111111%s:%d\n",__func__,__LINE__);
}

cJSON *Create_New_Ipcdat(cJSON *login, cJSON *result)
{
    int rec_ch,full_ch,quad_ch,app_ch;
	cJSON *ipcdata =  cJSON_CreateObject();
	cJSON_AddStringToObject(ipcdata,"IP_ADDR",cJSON_GetObjectItemCaseSensitive(login,"IP_ADDR")->valuestring);
    cJSON_AddStringToObject(ipcdata,"IPC_NAME",cJSON_GetObjectItemCaseSensitive(login,"IPC_NAME")->valuestring);
    cJSON_AddStringToObject(ipcdata,"USR_NAME",cJSON_GetObjectItemCaseSensitive(login,"USR_NAME")->valuestring);
    cJSON_AddStringToObject(ipcdata,"USR_PWD",cJSON_GetObjectItemCaseSensitive(login,"USR_PWD")->valuestring);
    cJSON_AddItemToObject(ipcdata,"CH_DAT",cJSON_Duplicate(result, 1));
	SetIpcChSelect(&rec_ch, &full_ch, &quad_ch, &app_ch, result);
    cJSON_AddNumberToObject(ipcdata, "CH_REC", rec_ch);
    cJSON_AddNumberToObject(ipcdata, "CH_FULL", full_ch);
    cJSON_AddNumberToObject(ipcdata, "CH_QUAD", quad_ch);
    cJSON_AddNumberToObject(ipcdata, "CH_APP", app_ch);

	return ipcdata;
}

cJSON *filter_ipc_from_tb(cJSON *filter)
{
	cJSON *viewdata;
	cJSON* view = cJSON_CreateArray();
	cJSON* where = cJSON_CreateObject();
	cJSON_AddStringToObject(where,"IP_ADDR",cJSON_GetObjectItemCaseSensitive(filter,"IP_ADDR")->valuestring);
	if(API_TB_SelectBySortByName(TB_NAME_IPC_DEV_TB, view, where, 0)==0)
	{
		viewdata = cJSON_CreateObject();
		cJSON_AddStringToObject(viewdata,"IP_ADDR",cJSON_GetObjectItemCaseSensitive(filter,"IP_ADDR")->valuestring);
		cJSON_AddStringToObject(viewdata,"IPC_NAME",cJSON_GetObjectItemCaseSensitive(filter,"IPC_NAME")->valuestring);
		cJSON_AddStringToObject(viewdata,"USR_NAME","admin");
		cJSON_AddStringToObject(viewdata,"USR_PWD","admin");
	}
	else
	{
		viewdata = cJSON_Duplicate(cJSON_GetArrayItem(view,0), 1);
	}
	cJSON_Delete(where);
	cJSON_Delete(view);
	return viewdata;
}
int update_one_ipc_to_tb(cJSON *item)
{
	cJSON *key=cJSON_GetObjectItemCaseSensitive(item,"IP_ADDR");
	if(key==NULL || !strcmp(key->valuestring, ""))
		return -1;
	
	cJSON *where=cJSON_CreateObject();
	cJSON* View=cJSON_CreateArray();
	int ret;
	cJSON_AddStringToObject(where,"IP_ADDR",key->valuestring);
	if(API_TB_SelectBySortByName(TB_NAME_IPC_DEV_TB, View, where, 0))
	{
		ret = API_TB_UpdateByName(TB_NAME_IPC_DEV_TB, where, item);
		ShellCmdPrintf("update_one_ipc_to_tb cnt=%d\n", ret);
	}
	else
	{
		ret = API_TB_AddByName(TB_NAME_IPC_DEV_TB, item);
		ShellCmdPrintf("add_one_ipc_to_tb cnt=%d\n", ret);
	}

	cJSON_Delete(where);
	cJSON_Delete(View);
	return 0;
}

int delete_one_ipc_to_tb(cJSON *item)
{
	cJSON *key=cJSON_GetObjectItemCaseSensitive(item,"IP_ADDR");
	if(key==NULL)
		return -1;
	
	cJSON *where=cJSON_CreateObject();
	cJSON_AddStringToObject(where,"IP_ADDR",key->valuestring);
	API_TB_DeleteByName(TB_NAME_IPC_DEV_TB, where);
	cJSON_Delete(where);

	return 0;
}
