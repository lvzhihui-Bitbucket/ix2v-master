#include "Common_TableView.h"
#include "define_file.h"
#include "utility.h"
#include "task_Event.h"
#include "obj_TableSurver.h"
#include "task_Beeper.h"
#include "KeyboardResource.h"
#include "obj_IoInterface.h"

#define IPC_LIST_PARA		"{\"HeaderEnable\":true,\"CellCallback\":0,\"ColumnWidth\":[240,240]}"
#define IPC_LOGIN_PARA		"{\"HeaderEnable\":false,\"CellCallback\":0,\"ColumnWidth\":[240,240]}"

static TB_VIWE* ipc_manage = NULL;
static TB_VIWE* ipc_search = NULL;
static TB_VIWE* ipc_login = NULL;
cJSON *ipclogin = NULL;
static VTK_KB_S* kb = NULL;
static lv_obj_t* scr_preview_return = NULL;
static lv_obj_t* scr_preview = NULL;
static void exit_clicked_event(lv_event_t* e);

void IpcManageMenuClose(void);
/**********************
 * IpcManage
 * ********************/
static void IpcCellClick(int line, int col)
{
    if(ipc_manage)
    {
        cJSON* record = cJSON_GetArrayItem(ipc_manage->view, line);
        EnterIpcLoginMenu(record);
    }
}
static void ipc_del_all(void* data)
{
    API_TB_DeleteByName(TB_NAME_IPC_DEV_TB, NULL);
    IpcManage_Fresh();
}
static void FunctionClick(const char* funcName)
{
    if(funcName)
    {
        if(!strcmp(funcName, "Search"))
        {
            API_TIPS_Mode("Ipc Search...");
            Send_IpcManageEvent("Search");
        }
        else if(!strcmp(funcName, "Delete all"))
        {
            API_MSGBOX("Delete?", 2, ipc_del_all, NULL);
        }
    }
}
static cJSON* GetIpcTb(void)
{
    cJSON* view = cJSON_CreateArray();
	cJSON* tpl=cJSON_CreateObject();
	cJSON_AddStringToObject(tpl,"IP_ADDR","");
	cJSON_AddStringToObject(tpl,"IPC_NAME","");
	cJSON_AddItemToArray(view,tpl);
	API_TB_SelectBySortByName(TB_NAME_IPC_DEV_TB, view, NULL, 0);
	cJSON_DeleteItemFromArray(view,0);
	return view;
}

void EnterIpcManageMenu(void)
{
	cJSON* para = cJSON_Parse(IPC_LIST_PARA);
	cJSON_ReplaceItemInObjectCaseSensitive(para, "CellCallback", cJSON_CreateNumber((int)IpcCellClick));

    cJSON* func = cJSON_CreateObject();
	cJSON* funcName = cJSON_AddArrayToObject(func, "Name");
	cJSON_AddItemToArray(funcName, cJSON_CreateString("Search"));
	cJSON_AddItemToArray(funcName, cJSON_CreateString("Delete all"));
	cJSON_AddNumberToObject(func, "Callback", (int)FunctionClick);
	cJSON_AddItemToObject(para, "Function", func);
	cJSON_AddItemToObject(para, "ReturnCallback", cJSON_CreateNumber((int)IpcManageMenuClose));

    cJSON* view = GetIpcTb();
    TB_MenuDisplay(&ipc_manage, view, "Ipc Manage", para);    
    cJSON_Delete(view);
	cJSON_Delete(para);
}
void IpcManageMenuClose(void)
{
    API_TIPS_Mode_Close();
	if(scr_preview)
		exit_clicked_event(NULL);
    if(ipclogin)
    {
        cJSON_Delete(ipclogin);
		ipclogin = NULL;
    }
    if(kb)
        Vtk_KbReleaseByMode(&kb); 
    TB_MenuDelete(&ipc_login);
    TB_MenuDelete(&ipc_search);
	TB_MenuDelete(&ipc_manage);
}
void IpcManage_Fresh(void)
{
    cJSON* view;
    if(ipc_manage)
    {
        view = GetIpcTb();
        TB_MenuDisplay(&ipc_manage, view, NULL, NULL);
        cJSON_Delete(view);
    }
}
void return_ipcmanage(void)
{
    TB_MenuDelete(&ipc_login);
    TB_MenuDelete(&ipc_search);
}
/**********************
 * IpcSearch
 * ********************/
static void IpcSearchClick(int line, int col)
{
    if(ipc_search)
    {
        cJSON* record = cJSON_GetArrayItem(ipc_search->view, line);
        EnterIpcLoginMenu(record);
    }
}
void EnterIpcSearchMenu(cJSON* view)
{
	cJSON* para = cJSON_Parse(IPC_LIST_PARA);
	cJSON_ReplaceItemInObjectCaseSensitive(para, "CellCallback", cJSON_CreateNumber((int)IpcSearchClick));
    TB_MenuDisplay(&ipc_search, view, "Ipc Search", para);    
	cJSON_Delete(para);
}

/**********************
 * IpcLogin
 * ********************/
static const char *loginstr[4] =
{
	"IP_ADDR",
	"IPC_NAME",
	"USR_NAME",
	"USR_PWD",
};
static cJSON* GetIpcLoginInfo(cJSON* data)
{
    cJSON* IpcInfo = cJSON_CreateArray();
    cJSON* record;
    int i;
    for (i = 0; i < 4; i++)
    {
        record = cJSON_CreateObject();
        cJSON_AddStringToObject(record, "Name", loginstr[i]);  
        cJSON_AddStringToObject(record, "Value", GetEventItemString(data, loginstr[i]));    
        cJSON_AddItemToArray(IpcInfo, record);
    }
    cJSON *chdat = cJSON_GetObjectItemCaseSensitive(data, "CH_DAT");
    int cnt;
    cJSON *item = NULL;
	char value[40];
    if(cJSON_IsArray(chdat))
    {
        cnt = cJSON_GetArraySize(chdat);
        for(i = 0; i < cnt; i++)
        {
            item = cJSON_GetArrayItem(chdat, i);
            snprintf(value, 40, "Channel%d", i);
            record = cJSON_CreateObject();
            cJSON_AddStringToObject(record, "Name", value);  
            snprintf(value, 40, "%d*%d-%s", GetEventItemInt(item, "RES_W"), GetEventItemInt(item, "RES_H"), (GetEventItemInt(item, "CODEC") == 1? "H265" : "H264"));
            cJSON_AddStringToObject(record, "Value", value);  
            cJSON_AddItemToArray(IpcInfo, record);
        }
    }
    return IpcInfo;
}

static int* confirm_set_ipc_item(const char* val,void *user_data)
{
    char* itemName = (char*)user_data;
    cJSON_ReplaceItemInObjectCaseSensitive(ipclogin, itemName, cJSON_CreateString(val)); 
    IpcLogin_Fresh();
}

/**********************
 * Ipc preview
 * ********************/


static lv_obj_t* ui_video_init(void)
{
    lv_obj_t* ui_table = lv_obj_create(NULL);
    lv_obj_remove_style_all(ui_table);
    lv_obj_set_size(ui_table, lv_disp_get_hor_res(NULL), lv_disp_get_ver_res(NULL));
    lv_obj_align(ui_table, LV_ALIGN_TOP_LEFT, 0, 0);
    lv_obj_set_style_bg_color(ui_table, lv_color_hex(0x41454D), 0);
    lv_obj_clear_flag(ui_table, LV_OBJ_FLAG_SCROLLABLE);
    return ui_table;
}
void ipc_preview_delete(void)
{
	vtk_lvgl_lock();
	if(scr_preview != NULL)
		lv_obj_del(scr_preview);
	scr_preview = NULL;
	vtk_lvgl_unlock();
}
static void exit_clicked_event(lv_event_t* e)
{
	printf("1111111111111:%s:%d\n",__func__,__LINE__);
	api_ipc_stop(0);//Stop_OneIpc_Show(0);
	printf("1111111111111:%s:%d\n",__func__,__LINE__);
    lv_disp_load_scr(scr_preview_return);      
	printf("1111111111111:%s:%d\n",__func__,__LINE__);
    //lv_obj_del(scr_preview);
   // scr_preview = NULL;
	printf("1111111111111:%s:%d\n",__func__,__LINE__);
    set_menu_with_video_off();
	printf("1111111111111:%s:%d\n",__func__,__LINE__);
	
}

static void	ipc_preview_on(void)
{
    scr_preview_return = lv_scr_act();
    if(scr_preview == NULL)
    {
        scr_preview = ui_video_init();
        set_menu_with_video_on();    
        lv_disp_load_scr(scr_preview);
        lv_disp_set_bg_color(NULL, lv_color_hex(0));
        lv_obj_set_style_bg_color(lv_scr_act(),lv_color_hex(0),0);
        lv_obj_t* bkgd_video = lv_obj_create(scr_preview);
        lv_obj_remove_style_all(bkgd_video);
        lv_obj_set_size(bkgd_video, 480, 320);
        lv_obj_set_style_bg_opa(bkgd_video, LV_OPA_0, 0);
        lv_obj_set_style_bg_color(bkgd_video,lv_color_hex(0),0);
        lv_obj_set_pos(bkgd_video, 0, 240);
        #if defined( LVGL_SUPPORT_TDE )	
        set_lvgl_video_parent(0, bkgd_video, 0);
        #endif
        lv_obj_t* exitBtn = lv_btn_create(scr_preview);
        lv_obj_add_flag(exitBtn, LV_OBJ_FLAG_CHECKABLE);
        lv_obj_set_size(exitBtn, 120, 60);
        lv_obj_align(exitBtn, LV_ALIGN_BOTTOM_LEFT, 5, -10);
        lv_obj_t* exitLabel = lv_label_create(exitBtn);
        lv_obj_center(exitLabel);
        lv_label_set_text(exitLabel, LV_VTK_LEFT);
        set_font_size(exitLabel,1);
        lv_obj_add_event_cb(exitBtn, exit_clicked_event, LV_EVENT_CLICKED, NULL);
    }
}
static void IpcLoginClick(int line, int col)
{
    dprintf("line = %d, col =%d\n", line, col);
    cJSON *Channel = NULL;
    cJSON *win = NULL;
    if(ipc_login)
    {
        cJSON* record = cJSON_GetArrayItem(ipc_login->view, line);
        char* itemName = GetEventItemString(record, "Name");
        char* itemValue = GetEventItemString(record, "Value");
        if(itemName)
        {
            if(!strcmp(itemName, "IPC_NAME")||!strcmp(itemName, "USR_NAME")||!strcmp(itemName, "USR_PWD"))
            {
                Vtk_KbMode1Init(&kb,Vtk_KbMode1, API_Para_Read_String2(KeybordLanguage),itemName,30,"^[a-zA-Z0-9_-]{1,20}$",itemValue,NULL,confirm_set_ipc_item,itemName);
            }
            else if(!strcmp(itemName, "IP_ADDR"))
            {
                
            }
            else
            {
                cJSON *chdat = cJSON_GetObjectItemCaseSensitive(ipclogin, "CH_DAT");
                if(!strcmp(itemName, "Channel0"))
                {
                    Channel = cJSON_GetArrayItem(chdat, 0);
                }
                else if(!strcmp(itemName, "Channel1"))
                {
                    Channel = cJSON_GetArrayItem(chdat, 1);
                }
                else if(!strcmp(itemName, "Channel2"))
                {
                    Channel = cJSON_GetArrayItem(chdat, 2);
                }
                MainMenu_Reset_Time();
		if(Get_Monitor_Show_state(0)==0&&scr_preview==NULL)
		{
	                ipc_preview_on();
	                win = cJSON_CreateObject();
	                cJSON_AddNumberToObject(win,"X",0);
	                cJSON_AddNumberToObject(win,"Y",0);
	                cJSON_AddNumberToObject(win,"W",480);
	                cJSON_AddNumberToObject(win,"H",320);
	                Start_OneIpc_Show(0, Channel, win, 0); 
		}
		else
		{
			//api_ipc_stop(0);
		}
                cJSON_Delete(win);
            }             
        }
    }
}
static void ipc_del_one(void* data)
{
    delete_one_ipc_to_tb(ipclogin);
    IpcManage_Fresh();
    return_ipcmanage();
}
static void LoginFunctionClick(const char* funcName)
{
    if(funcName)
    {
        if(!strcmp(funcName, "Login"))
        {
            API_TIPS_Mode("Ipc Login...");
            Send_IpcManageEvent("Login");
        }
        else if(!strcmp(funcName, "Save"))
        {
            update_one_ipc_to_tb(ipclogin);
            IpcManage_Fresh();
            return_ipcmanage();
        }
        else if(!strcmp(funcName, "Delete"))
        {
            API_MSGBOX("Delete?", 2, ipc_del_one, NULL);            
        }
    }
}

void IpcLogin_Fresh(void)
{
    cJSON* view;
    if(ipc_login)
    {
        cJSON* view = GetIpcLoginInfo(ipclogin);
        TB_MenuDisplay(&ipc_login, view, NULL, NULL);
        cJSON_Delete(view);
    }
}

void EnterIpcLoginMenu(cJSON* filter)
{
    if(ipclogin)
    {
        cJSON_Delete(ipclogin);
		ipclogin = NULL;
    }
    ipclogin = filter_ipc_from_tb(filter);
	cJSON* para = cJSON_Parse(IPC_LOGIN_PARA);
	cJSON_ReplaceItemInObjectCaseSensitive(para, "CellCallback", cJSON_CreateNumber((int)IpcLoginClick));
    cJSON* func = cJSON_CreateObject();
	cJSON* funcName = cJSON_AddArrayToObject(func, "Name");
    if(cJSON_GetObjectItemCaseSensitive(ipclogin, "CH_DAT"))
    {
        cJSON_AddItemToArray(funcName, cJSON_CreateString("Login"));
        cJSON_AddItemToArray(funcName, cJSON_CreateString("Save"));
        cJSON_AddItemToArray(funcName, cJSON_CreateString("Delete"));
    }
    else
    {
        cJSON_AddItemToArray(funcName, cJSON_CreateString("Login"));
    }
	cJSON_AddNumberToObject(func, "Callback", (int)LoginFunctionClick);
	cJSON_AddItemToObject(para, "Function", func);

    cJSON* view = GetIpcLoginInfo(ipclogin);
    TB_MenuDisplay(&ipc_login, view, "Ipc Login", para);    
    cJSON_Delete(view);
	cJSON_Delete(para);
}

void EnterIpcInfoMenu(cJSON* data)
{
    if(ipclogin)
    {
        cJSON_Delete(ipclogin);
		ipclogin = NULL;
    }
    ipclogin = cJSON_Duplicate(data, 1);
	cJSON* para = cJSON_Parse(IPC_LOGIN_PARA);
	cJSON_ReplaceItemInObjectCaseSensitive(para, "CellCallback", cJSON_CreateNumber((int)IpcLoginClick));
    cJSON* func = cJSON_CreateObject();
	cJSON* funcName = cJSON_AddArrayToObject(func, "Name");
    cJSON_AddItemToArray(funcName, cJSON_CreateString("Save"));
    cJSON_AddNumberToObject(func, "Callback", (int)LoginFunctionClick);
	cJSON_AddItemToObject(para, "Function", func);

    cJSON* view = GetIpcLoginInfo(data);
    TB_MenuDisplay(&ipc_login, view, "Ipc Info", para);    
    cJSON_Delete(view);
	cJSON_Delete(para);
}

void Send_IpcManageEvent(char* msg)
{
    cJSON* event = cJSON_CreateObject();
    cJSON_AddStringToObject(event, EVENT_KEY_EventName, EventMenuIpcManage);
    cJSON_AddStringToObject(event, "MSG", msg);
    API_Event_Json(event);
    cJSON_Delete(event);
}
int EventMenuIpcManageCallback(cJSON* cmd)
{
    char* msg = GetEventItemString(cmd, "MSG");
    if(!strcmp(msg, "Search"))
    {
        cJSON *ipcsearch = cJSON_CreateArray();
        IpcDeviceDiscovery(1000, NULL, ipcsearch);
        printf_json(ipcsearch,"MSG_IPC_SEARCH_REQ");
        API_TIPS_Mode_Close();
        EnterIpcSearchMenu(ipcsearch);
        cJSON_Delete(ipcsearch);
    }
    else if(!strcmp(msg, "Login"))
    {
        cJSON *loginresult = cJSON_CreateArray();
        if(IpcDeviceLogin(ipclogin, loginresult) == 0)//login ok 
        {
            API_TIPS_Mode_Close();
            cJSON *ipcsave = Create_New_Ipcdat(ipclogin, loginresult);
            //printf_json(ipcsave,"IPC_LOGIN_OK");
            EnterIpcInfoMenu(ipcsave);
            cJSON_Delete(ipcsave);
        }
        else//login err
        {
            API_TIPS_Mode_Close();
            API_TIPS_Ext("Ipc Login err!");
        }
        cJSON_Delete(loginresult);
    }
}
