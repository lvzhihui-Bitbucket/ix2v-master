#ifndef IPCMENUPROCESS_H_
#define IPCMENUPROCESS_H_

#include "lv_ix850.h"

#define MSG_IPC_SEARCH_REQ          230918901
#define MSG_IPC_LOGIN_MENU          230918902
#define MSG_IPC_LOGIN_REQ           230918903
#define MSG_IPC_LOGIN_RESULT        230918904
#define MSG_IPC_PREVIEW             230918905
#define MSG_IPC_Add                 230918907
#define MSG_IPC_Del                 230918908
#define MSG_IPC_Manage_Exit         230918909
#define MSG_IPC_Login_Exit          230918910
#define MSG_IPC_PREVIEW_Exit        230918911

#endif 
