﻿#include "menu_common.h"
#include "IpcMenuProcess.h"

extern const lv_font_t* font_small;
extern const lv_font_t* font_medium;
extern const lv_font_t* font_larger;

typedef struct
{
	int iCon;
	char* iConText;
}LIST_ITEM;
#define	ICON_IPC_IpAddr				1
#define	ICON_IPC_Name			    2
#define	ICON_IPC_UserName			3
#define	ICON_IPC_UserPwd			4

const LIST_ITEM IPC_LoginIconTable[] = 
{
	{ICON_IPC_IpAddr,				"IP_ADDR"},
	{ICON_IPC_Name,					"IPC_NAME"},    
	{ICON_IPC_UserName,				"USR_NAME"},
	{ICON_IPC_UserPwd,				"USR_PWD"},
};
int IPC_LoginIconNum = sizeof(IPC_LoginIconTable)/sizeof(IPC_LoginIconTable[0]);
lv_obj_t* scr_ipclogin = NULL;
lv_obj_t* ipclogin_back_scr = NULL;
lv_obj_t* ipclogincont = NULL;
static cJSON* logindata = NULL;
static lv_obj_t* editor_keyboard_return = NULL;
static lv_obj_t * scr_keyboard=NULL;
static lv_obj_t* kb;
static lv_obj_t* saveBtn;
static lv_obj_t* delBtn;
static lv_obj_t* scr_preview_return = NULL;
static lv_obj_t* scr_preview = NULL;

lv_obj_t* ipc_menulogin(lv_obj_t* parent, cJSON* data);

static lv_obj_t* ui_ipclogin_init(void)
{
    lv_obj_t* ui_ipclogin = lv_obj_create(NULL);
    lv_obj_clear_flag(ui_ipclogin, LV_OBJ_FLAG_SCROLLABLE);
    lv_obj_remove_style_all(ui_ipclogin);
    lv_obj_set_size(ui_ipclogin, lv_disp_get_hor_res(NULL), lv_disp_get_ver_res(NULL));
    lv_obj_align(ui_ipclogin, LV_ALIGN_TOP_LEFT, 0, 0);
    lv_obj_set_style_bg_color(ui_ipclogin, lv_palette_main(LV_PALETTE_GREY), 0);
    lv_obj_set_style_bg_opa(ui_ipclogin, LV_OPA_100, 0);
    return ui_ipclogin;
}
static lv_obj_t* ui_keyboard_init(void)
{
    lv_obj_t* ui_keyboard = lv_obj_create(NULL);
    lv_obj_clear_flag(ui_keyboard, LV_OBJ_FLAG_SCROLLABLE);
    lv_obj_remove_style_all(ui_keyboard);
    lv_obj_set_size(ui_keyboard, lv_disp_get_hor_res(NULL), lv_disp_get_ver_res(NULL));
    lv_obj_align(ui_keyboard, LV_ALIGN_TOP_LEFT, 0, 0);
    lv_obj_set_style_bg_color(ui_keyboard, lv_palette_main(LV_PALETTE_GREY), 0);
    lv_obj_set_style_bg_opa(ui_keyboard, LV_OPA_100, 0);
    lv_disp_load_scr(ui_keyboard);
    return ui_keyboard;
}
void ui_video_init(void)
{
    scr_preview = lv_obj_create(NULL);
    lv_obj_remove_style_all(scr_preview);
    lv_obj_set_size(scr_preview, lv_disp_get_hor_res(NULL), lv_disp_get_ver_res(NULL));
    lv_obj_set_style_bg_opa(scr_preview, LV_OPA_100, 0);
    lv_obj_set_style_bg_color(scr_preview,lv_color_hex(0x102030),0);
    lv_obj_set_x(scr_preview, 0);
    lv_obj_set_y(scr_preview, 0);
}

//login menu
static void ipclogin_menuback(lv_event_t* e)
{
    lv_disp_load_scr(ipclogin_back_scr);
    MenuIpcManage_Process(MSG_IPC_Login_Exit, NULL);
}
static void ipclogin_event(lv_event_t* e)
{
    MenuIpcManage_Process(MSG_IPC_LOGIN_REQ, logindata);
}
static void ipcsave_event(lv_event_t* e)
{
    lv_disp_load_scr(ipclogin_back_scr);
    ipclogin_delete();
    MenuIpcManage_Process(MSG_IPC_Add, logindata);
}
static void ipcdelete_event(lv_event_t* e)
{
    lv_disp_load_scr(ipclogin_back_scr);
    ipclogin_delete();
    MenuIpcManage_Process(MSG_IPC_Del, logindata);
}
lv_obj_t* ipclogin_menu_creat(cJSON* json)
{
    ipclogin_back_scr = lv_scr_act();
    scr_ipclogin = ui_ipclogin_init();

    lv_obj_t* cont = lv_obj_create(scr_ipclogin);
    lv_obj_set_size(cont, 480, 800);
    lv_obj_set_style_pad_all(cont, 0, 0);
    lv_obj_clear_flag(cont, LV_OBJ_FLAG_SCROLLABLE);

    lv_obj_t* titleCont = lv_obj_create(cont);
    lv_obj_set_size(titleCont, 480, 80);
    lv_obj_set_style_pad_all(titleCont, 0, 0);
    lv_obj_clear_flag(titleCont, LV_OBJ_FLAG_SCROLLABLE);

    lv_obj_t* title_label = lv_label_create(titleCont);
    lv_obj_set_style_text_font(title_label, &lv_font_montserrat_32, 0);
    lv_obj_center(title_label);
    //lv_label_set_text(title_label, "Ipc Login");
    vtk_label_set_text(title_label, "Ipc Login");
    lv_obj_t* backBtn = lv_btn_create(titleCont);
    lv_obj_remove_style_all(backBtn);
    lv_obj_add_event_cb(backBtn, ipclogin_menuback, LV_EVENT_CLICKED, NULL);
    lv_obj_align(backBtn, LV_ALIGN_LEFT_MID, 0, 0);
    lv_obj_set_size(backBtn, 80, 80);
    lv_obj_t* backLabel = lv_label_create(backBtn);
    lv_label_set_text(backLabel, LV_VTK_BACK);
    lv_obj_align(backLabel, LV_ALIGN_CENTER, 0, 0);
    lv_obj_set_style_text_font(backLabel, font_larger, 0);

    lv_obj_t* loginBtn = lv_btn_create(cont);
    lv_obj_add_event_cb(loginBtn, ipclogin_event, LV_EVENT_CLICKED, NULL);
    lv_obj_align(loginBtn, LV_ALIGN_BOTTOM_RIGHT, 0, 0);
    lv_obj_set_size(loginBtn, 80, 80);
    lv_obj_t* searchLabel = lv_label_create(loginBtn);
    lv_label_set_text(searchLabel, "login");
    vtk_label_set_text(searchLabel, "login");
    lv_obj_align(searchLabel, LV_ALIGN_CENTER, 0, 0);
    lv_obj_set_style_text_font(searchLabel, font_medium, 0);

    saveBtn = lv_btn_create(cont);
    lv_obj_add_event_cb(saveBtn, ipcsave_event, LV_EVENT_CLICKED, NULL);
    lv_obj_align(saveBtn, LV_ALIGN_BOTTOM_LEFT, 0, 0);
    lv_obj_set_size(saveBtn, 80, 80);
    lv_obj_t* saveLabel = lv_label_create(saveBtn);
    //lv_label_set_text(saveLabel, "save");
    vtk_label_set_text(saveLabel, "save");
    lv_obj_align(saveLabel, LV_ALIGN_CENTER, 0, 0);
    lv_obj_set_style_text_font(saveLabel, font_medium, 0);
    lv_obj_add_flag(saveBtn, LV_OBJ_FLAG_HIDDEN);

    delBtn = lv_btn_create(cont);
    lv_obj_add_event_cb(delBtn, ipcdelete_event, LV_EVENT_CLICKED, NULL);
    lv_obj_align(delBtn, LV_ALIGN_BOTTOM_MID, 0, 0);
    lv_obj_set_size(delBtn, 80, 80);
    lv_obj_t* delLabel = lv_label_create(delBtn);
    //lv_label_set_text(delLabel, "delete");
    vtk_label_set_text(delLabel, "delete");
    lv_obj_align(delLabel, LV_ALIGN_CENTER, 0, 0);
    lv_obj_set_style_text_font(delLabel, font_medium, 0);
    lv_obj_add_flag(delBtn, LV_OBJ_FLAG_HIDDEN);

    ipc_menulogin(cont, json);
    lv_disp_load_scr(scr_ipclogin);
    return cont;
}

void ipclogin_delete(void)
{
    if(ipclogincont)
    {
        lv_obj_del(ipclogincont);
        ipclogincont = NULL;
        //printf("11111111%s:%d\n",__func__,__LINE__);
    }
    if (scr_ipclogin)
    {
        lv_obj_del(scr_ipclogin);
        scr_ipclogin = NULL;
        //printf("11111111%s:%d\n",__func__,__LINE__);
    }
}

//edit menu
static void kb_edit_event_cb(lv_event_t* e)
{
    lv_event_code_t code = lv_event_get_code(e);
    lv_obj_t* ta = lv_event_get_target(e);
    lv_obj_t* label = lv_event_get_user_data(e);    
    lv_obj_t* nameLabel = lv_obj_get_child(lv_obj_get_parent(label), 0);
    char* name = lv_label_get_text(nameLabel);

    if (code == LV_EVENT_FOCUSED) 
    {
        if (lv_indev_get_type(lv_indev_get_act()) != LV_INDEV_TYPE_KEYPAD) 
        {
            lv_keyboard_set_textarea(kb, ta);
            if(strcmp(name,"IP_ADDR")==0)
            {
                lv_keyboard_set_mode(kb, LV_KEYBOARD_MODE_NUMBER);
            }
            else
            {
                lv_keyboard_set_mode(kb, LV_KEYBOARD_MODE_TEXT_LOWER);
            }
        }
    }
    else if (code == LV_EVENT_CANCEL) {

        lv_disp_load_scr(editor_keyboard_return);      
        lv_obj_del(scr_keyboard);
        scr_keyboard = NULL;
    }
    else if (code == LV_EVENT_READY)
    {
        char* text = lv_textarea_get_text(ta);
        lv_label_set_text(label, text);
        cJSON_ReplaceItemInObject(logindata, name, cJSON_CreateString(text));
        lv_disp_load_scr(editor_keyboard_return);      
        lv_obj_del(scr_keyboard);
        scr_keyboard = NULL;
        
    }
}
static void text_click_event(lv_event_t* e)
{
    lv_obj_t* target = lv_event_get_target(e);
    char* val = lv_label_get_text(target);
    
    editor_keyboard_return = lv_scr_act();
    scr_keyboard = ui_keyboard_init();

    lv_obj_t* keyMenu = lv_obj_create(scr_keyboard);
    lv_obj_set_style_pad_row(keyMenu, 0, 0);
    lv_obj_set_size(keyMenu, 480, 800);
    lv_obj_set_flex_flow(keyMenu, LV_FLEX_FLOW_COLUMN);
    lv_obj_clear_flag(keyMenu, LV_OBJ_FLAG_SCROLLABLE);
    lv_obj_t* nameLabel = lv_obj_get_child(lv_obj_get_parent(target), 0);
    char* name = lv_label_get_text(nameLabel);

    lv_obj_t* name_label = lv_label_create(keyMenu);
    lv_label_set_text(name_label, name);
    lv_obj_set_size(name_label, 200, 80);
    lv_obj_set_style_text_font(name_label, &lv_font_montserrat_32, 0);

    lv_obj_t* ta = lv_textarea_create(keyMenu);
    lv_obj_set_style_text_font(ta, &lv_font_montserrat_26, 0);
    lv_textarea_add_text(ta, val);
    lv_obj_set_size(ta, LV_PCT(100), 300);
    kb = lv_keyboard_create(scr_keyboard);
    lv_obj_set_style_text_font(kb, &lv_font_montserrat_26, 0);
    //lv_obj_add_flag(kb, LV_OBJ_FLAG_HIDDEN);

    lv_obj_add_event_cb(ta, kb_edit_event_cb, LV_EVENT_ALL, target);
    lv_event_send(ta, LV_EVENT_FOCUSED, NULL);
}

//preview menu
static void exit_clicked_event(lv_event_t* e)
{
    lv_disp_load_scr(scr_preview_return);      
    MenuIpcManage_Process(MSG_IPC_PREVIEW_Exit, NULL);
    lv_obj_del(scr_preview);
    scr_preview = NULL;
}

int	ipc_preview_on(void)
{
    scr_preview_return = lv_scr_act();
    ui_video_init();

    lv_disp_load_scr(scr_preview);
    lv_disp_set_bg_color(NULL, lv_color_hex(0));
    lv_obj_set_style_bg_color(lv_scr_act(),lv_color_hex(0),0);
    lv_obj_t* bkgd_video = lv_obj_create(scr_preview);
    lv_obj_remove_style_all(bkgd_video);
    lv_obj_set_size(bkgd_video, 480, 320);
    lv_obj_set_style_bg_opa(bkgd_video, LV_OPA_0, 0);
    lv_obj_set_style_bg_color(bkgd_video,lv_color_hex(0),0);
    lv_obj_set_pos(bkgd_video, 0, 0);
    #if defined( LVGL_SUPPORT_TDE )	
    set_lvgl_video_parent(0, bkgd_video, 0);
    #endif
    lv_obj_t* exitBtn = lv_btn_create(scr_preview);
    lv_obj_add_flag(exitBtn, LV_OBJ_FLAG_CHECKABLE);
    lv_obj_set_size(exitBtn, 100, 50);
    lv_obj_align(exitBtn, LV_ALIGN_BOTTOM_LEFT, 0, -30);
    lv_obj_t* exitLabel = lv_label_create(exitBtn);
    lv_obj_center(exitLabel);
    lv_label_set_text(exitLabel, LV_SYMBOL_LEFT);
    lv_obj_add_event_cb(exitBtn, exit_clicked_event, LV_EVENT_CLICKED, NULL);
}

static void ipcpreview_event(lv_event_t* e)
{
    lv_obj_t* obj = lv_event_get_target(e);
    cJSON *chdat = cJSON_GetObjectItemCaseSensitive(logindata, "CH_DAT");
    const cJSON *pSub = NULL;
    pSub = cJSON_GetArrayItem(chdat, lv_obj_get_index(obj)-4);
    if(pSub != NULL)
    {
        MenuIpcManage_Process(MSG_IPC_PREVIEW, pSub);
    }
}
static lv_obj_t* create_oneitem(lv_obj_t* parent, char* name, char* val)
{
    lv_obj_t* obj = lv_obj_create(parent);
    lv_obj_set_style_pad_row(obj, 0, 0);
    lv_obj_set_size(obj, LV_PCT(100), LV_SIZE_CONTENT);
    lv_obj_set_flex_flow(obj, LV_FLEX_FLOW_ROW);
    lv_obj_set_style_text_font(obj, &lv_font_montserrat_26, 0);
    lv_obj_clear_flag(obj, LV_OBJ_FLAG_SCROLLABLE);

    lv_obj_t* text_label = lv_label_create(obj);
    lv_label_set_text(text_label, name);
    lv_obj_set_size(text_label, 180, LV_SIZE_CONTENT);

    lv_obj_t* value_label = lv_label_create(obj);
    lv_label_set_text(value_label, val);   
    lv_obj_set_ext_click_area(value_label, 30);
    lv_label_set_long_mode(value_label, LV_LABEL_LONG_DOT);
    lv_obj_set_size(value_label, 300, LV_SIZE_CONTENT);
    if(!strstr(name,"Channel"))
    {
        lv_obj_add_flag(value_label, LV_OBJ_FLAG_CLICKABLE);
        lv_obj_add_event_cb(value_label, text_click_event, LV_EVENT_CLICKED, NULL);
    }
    else
    {
        lv_obj_add_flag(obj, LV_OBJ_FLAG_CLICKABLE);
        lv_obj_add_event_cb(obj, ipcpreview_event, LV_EVENT_CLICKED, NULL);
    }
	
    return value_label;
}

lv_obj_t* ipc_menulogin(lv_obj_t* parent, cJSON* data)
{
    int i;
    cJSON *item = NULL;
	char name[40];
	char value[40];
    if(ipclogincont)
    {
        lv_obj_del(ipclogincont);
        ipclogincont = NULL;
    }

    logindata = data;
    ipclogincont = lv_obj_create(parent);
    lv_obj_set_size(ipclogincont, 480, 600);
    lv_obj_set_y(ipclogincont, 100);
    lv_obj_set_style_pad_row(ipclogincont, 0, 0);
    lv_obj_set_style_pad_all(ipclogincont, 0, 0);
    lv_obj_set_flex_flow(ipclogincont, LV_FLEX_FLOW_COLUMN);

    for(i = 0; i < IPC_LoginIconNum; i++)
    {
        create_oneitem(ipclogincont,IPC_LoginIconTable[i].iConText,GetEventItemString(data, IPC_LoginIconTable[i].iConText));
    }
    cJSON *chdat = cJSON_GetObjectItemCaseSensitive(data, "CH_DAT");
    if(chdat)
    {
        lv_obj_clear_flag(saveBtn, LV_OBJ_FLAG_HIDDEN);
        lv_obj_clear_flag(delBtn, LV_OBJ_FLAG_HIDDEN);
    }
    if(cJSON_IsArray(chdat))
	{
		for(i = 0; i < cJSON_GetArraySize(chdat); i++)
        {
            item = cJSON_GetArrayItem(chdat, i);
            snprintf(name, 40, "Channel%d", i);
            snprintf(value, 40, "%d*%d-%s", GetEventItemInt(item, "RES_W"), GetEventItemInt(item, "RES_H"), (GetEventItemInt(item, "CODEC") == 1? "H265" : "H264"));
            create_oneitem(ipclogincont, name, value);
        }
	}
}