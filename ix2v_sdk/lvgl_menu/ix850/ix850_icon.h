#ifndef _IX850_ICON_H_
#define _IX850_ICON_H_

#include "lv_ix850.h"
#include "icon_common.h"
#include "menu_common.h"

extern lv_obj_t* maincont;

lv_obj_t* img_creat_fcon(lv_obj_t* parent, struct ICON_INS* picon);
lv_obj_t* add_fcon_group(lv_obj_t* parent, struct ICON_INS* picon);
void reset_codeKey(void);

#endif
