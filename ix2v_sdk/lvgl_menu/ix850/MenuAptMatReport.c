#include "menu_utility.h"
#include "cJSON.h"
#include "obj_TableSurver.h"
#include "define_string.h"
#include "obj_PublicInformation.h"
#include "utility.h"
#include "task_Event.h"
#include "Common_TableView.h"
#include "obj_IoInterface.h"
#include "obj_IXS_Proxy.h"
#include "obj_CommonSelectMenu.h"
#include "obj_GetInfoByIp.h"

#define APT_MAT_REPORT_PARA		"{\"HeaderEnable\":true,\"ColumnWidth\":[120, 180, 180, 180, 180, 180, 180, 180, 180, 180, 180]}"
#define APT_MAT_REPORT_VIEW_TEMPALE		"{\"IM_ID\":1,\"STATE\":\"\",\"PARA_CMP\":\"\",\"SYS_TYPE_CUR\":\"\",\"G_NBR_CUR\":\"\",\"L_NBR_CUR\":\"\",\"IX_NAME_CUR\":\"\",\"SYS_TYPE\":\"\",\"G_NBR\":\"\",\"L_NBR\":\"\",\"IX_NAME\":\"\"}"

static TB_VIWE* aptMatReportTb = NULL;
static int aptMatCheckFlag = 0;

static cJSON* AptMatReportCreatePara(const char* filterValue);

static void AptMatReportReturnCallback(void)
{
	MenuAPT_MAT_Report_Close();
}

static void AptMatReportFilterProcess(const char* chose, void* userdata)
{   
    if(!chose)
    {
        return;
    }

    if(!strcmp("ALL",chose) || !strcmp("ONLINE",chose) || !strcmp("OFFLINE",chose) || !strcmp("ADDED",chose))
	{
        cJSON* para = AptMatReportCreatePara(chose);
		cJSON* where = cJSON_CreateObject();
		cJSON* view = cJSON_CreateArray();
		cJSON_AddItemToArray(view, cJSON_Parse(APT_MAT_REPORT_VIEW_TEMPALE));
		cJSON_AddStringToObject(where, "STATE", chose);
		API_TB_SelectBySortByName(TB_NAME_APT_MAT_CHECK, view, (!strcmp("ALL",chose)) ? NULL : where, 0);
		cJSON_DeleteItemFromArray(view, 0);
		TB_MenuDisplay(&aptMatReportTb, view, "APT MAT report", para);
		cJSON_Delete(where);
		cJSON_Delete(view);
		cJSON_Delete(para);
    }
}

static void AptMatReportFilterCallback(const cJSON* filter)
{   
    cJSON* value = cJSON_GetObjectItem(filter,"Value");
    static CHOSE_MENU_T callback = {.callback = 0, .cbData = NULL};
	callback.callback = AptMatReportFilterProcess;
	callback.cbData = NULL;
    cJSON* choseList = cJSON_CreateArray();
    cJSON_AddItemToArray(choseList, cJSON_CreateString("ALL"));
    cJSON_AddItemToArray(choseList, cJSON_CreateString("ONLINE"));
    cJSON_AddItemToArray(choseList, cJSON_CreateString("OFFLINE"));
    cJSON_AddItemToArray(choseList, cJSON_CreateString("ADDED"));
    char* choseString = cJSON_IsString(value) ? value->valuestring : NULL;
    CreateChooseMenu("IM state", choseList, choseString, &callback);
    cJSON_Delete(choseList);
}

static cJSON* AptMatReportCreatePara(const char* filterValue)
{   
    cJSON* fliter = cJSON_CreateObject();
    cJSON_AddStringToObject(fliter, "Name", "state");
    cJSON_AddStringToObject(fliter, "Value", filterValue);
    cJSON_AddNumberToObject(fliter, "Callback", (int)AptMatReportFilterCallback);
	cJSON* para = cJSON_Parse(APT_MAT_REPORT_PARA);
    cJSON_AddItemToObject(para,"Filter", fliter);
	cJSON_AddNumberToObject(para, "ReturnCallback", (int)AptMatReportReturnCallback);

	return para;
}


void MenuAPT_MAT_Report_Init(void)
{
	AptMatReportFilterProcess("ALL", NULL);
}

void MenuAPT_MAT_Report_Close(void)
{
	TB_MenuDelete(&aptMatReportTb);
	API_Event_NameAndMsg(EventMenuSettingProcess, "SettingRefresh", NULL);
}

static int AptMatCheckRefleshTimerCb(int time)
{
	if(aptMatCheckFlag)
	{
		if(time < 5)
		{
			char tempString[10];
			snprintf(tempString, 10, "%ds", 5 - time);
			API_TIPS_Mode_Title(NULL, tempString);
			MainMenu_Reset_Time();
			return 0;
		}
		else
		{
			API_Event_NameAndMsg(EventMenuAptMatReport, "APT_MAT_Check", NULL);
		}
	}

	return 2;
}

static int AptMatCheckRecord(const cJSON* record)
{
	const char* checkString[] = {"SYS_TYPE", "L_NBR", "G_NBR", "IX_NAME", NULL};
	char tempString[100];
	int i;
	int ret = 1;
	for(i = 0; checkString[i]; i++)
	{
		snprintf(tempString, 100, "%s_CUR", checkString[i]);
		cJSON* current =  cJSON_GetObjectItemCaseSensitive(record, tempString);
		cJSON* compare =  cJSON_GetObjectItemCaseSensitive(record, checkString[i]);
		if(!cJSON_Compare(current, compare, 1))
		{
			ret = 0;
			break;
		}
	}

	return ret;
}

int EventMenuAptMatReportCallback(cJSON* cmd)
{
	if(aptMatCheckFlag)
	{
		aptMatCheckFlag = 0;
		API_TIPS_Mode_Close(NULL);
		char* msg = GetEventItemString(cmd, EVENT_KEY_Message);
		if(!strcmp(msg, "APT_MAT_Check"))
		{
			cJSON* onlineDevice = NULL;
			cJSON* viewElement;
			int recordCnt;
			cJSON* record;

			API_TB_DeleteByName(TB_NAME_APT_MAT_CHECK, NULL);
			cJSON* view = cJSON_CreateArray();
			API_TB_SelectBySortByName(TB_NAME_APT_MAT, view, NULL, 0);
			cJSON_ArrayForEach(viewElement, view)
			{
				API_TB_AddByName(TB_NAME_APT_MAT_CHECK, viewElement);
			}
			cJSON_Delete(view);

			IXS_ProxyGetTb(&onlineDevice, NULL, Search);
			cJSON_ArrayForEach(viewElement, onlineDevice)
			{
				char* ixType = GetEventItemString(viewElement, "IX_TYPE");
				if(!strcmp(ixType, "IM"))
				{
					char* ixAddr = GetEventItemString(viewElement, "IX_ADDR");
					int targetIp = inet_addr(GetEventItemString(viewElement, "IP_ADDR"));
					cJSON* where = cJSON_CreateObject();
					cJSON_AddStringToObject(where, IX2V_IX_ADDR, ixAddr);
					view = cJSON_CreateArray();
					recordCnt = API_TB_SelectBySortByName(TB_NAME_APT_MAT, view, where, 0);
					if(recordCnt)
					{
						record = cJSON_Duplicate(cJSON_GetArrayItem(view, 0), 1);
					}
					else
					{
						record = cJSON_CreateObject();
					}
					
					cJSON* ioRead = cJSON_CreateArray();
					cJSON_AddItemToArray(ioRead, cJSON_CreateString(SysType));

					cJSON* ioResult = API_ReadRemoteIo(targetIp, ioRead);
					if(ioResult && !strcmp(GetEventItemString(ioResult, "Result"), "Succ"))
					{
						cJSON* ioValue =  cJSON_GetObjectItemCaseSensitive(ioResult, "READ");
						cJSON_AddItemToObject(record, "SYS_TYPE_CUR", cJSON_Duplicate(cJSON_GetObjectItemCaseSensitive(ioValue, SysType), 1));
					}
					
					cJSON_AddStringToObject(record, "G_NBR_CUR", GetEventItemString(viewElement, "G_NBR"));
					cJSON_AddStringToObject(record, "L_NBR_CUR", GetEventItemString(viewElement, "L_NBR"));
					cJSON_AddStringToObject(record, "IX_NAME_CUR", GetEventItemString(viewElement, "IX_NAME"));

					cJSON_AddStringToObject(record, "PARA_CMP", AptMatCheckRecord(record) ? "same" : "different");

					if(recordCnt)
					{
						cJSON_AddStringToObject(record, "STATE", "ONLINE");
						API_TB_UpdateByName(TB_NAME_APT_MAT_CHECK, where, record);
					}
					else
					{
						char tempString[10] = {0};
						strncpy(tempString, ixAddr+6, 2);
						int id = atoi(tempString);
						cJSON_AddStringToObject(record, "IX_ADDR", ixAddr);
						cJSON_AddNumberToObject(record, "IM_ID", id);
						cJSON_AddStringToObject(record, "STATE", "ADDED");
						API_TB_AddByName(TB_NAME_APT_MAT_CHECK, record);
					}
					cJSON_Delete(where);
					cJSON_Delete(record);
					cJSON_Delete(view);
					cJSON_Delete(ioRead);
					cJSON_Delete(ioResult);
				}
			}
			cJSON_Delete(onlineDevice);
			API_AptMatCheckModificationInform();
			API_Event_NameAndMsg(EventMenuSettingProcess, "SettingRefresh", NULL);
		}
	}
}

//IXS表有更新
int AptMatCheckListChangeProcess(cJSON *cmd)
{
	if(aptMatCheckFlag)
	{
		API_Del_TimingCheck(AptMatCheckRefleshTimerCb);
		API_Event_NameAndMsg(EventMenuAptMatReport, "APT_MAT_Check", NULL);
	}
	return 1;
}

static void CheckAptMat(void* para)
{
	aptMatCheckFlag = 1;
	API_Event_By_Name(Event_IXS_ReqUpdateTb);
	API_Add_TimingCheck(AptMatCheckRefleshTimerCb, 1);
	if(!API_TIPS_Mode("Please wait"));
	{
		API_TIPS_Mode_Fresh(NULL, "Please wait");
	}
	API_TIPS_Mode_Title(NULL, "5s");
}


void MenuAPT_MAT_Check_Process(void* data)
{
	if(data)
	{
		if(!strcmp(data, "APT_MAT_Check") && !aptMatCheckFlag)
		{
			API_MSGBOX("About to check MAT", 2, CheckAptMat, NULL);
		}
	}
}

void MenuAPT_MAT_Check_Close(void)
{
	if(aptMatCheckFlag)
	{
		aptMatCheckFlag = 0;
		API_TIPS_Mode_Close(NULL);
	}
}