#include "Common_TableView.h"
#include "define_file.h"
#include "utility.h"
#include "task_Event.h"
#include "obj_TableSurver.h"
#include "task_Beeper.h"
#include "KeyboardResource.h"
#include "Common_Record.h"

#define CARD_VIEW_PARA		"{\"Filter\":{\"Name\":\"ROOM\",\"Value\":\"\",\"Callback\":0},\"HeaderEnable\":true,\"CellCallback\":0,\"ColumnWidth\":[200,140,140,150]}"
#define CARD_EDITOR_DESCRIPTION  "{\"Check\":{\"CARD_NUM\":\"^[0-9]{1,10}$\",\"ROOM\":\"^(1[0-9]|2[0-9]|3[0-2]|[1-9])$\",\"DATE\":\"^[0-9]{2,2}[0-9]{2,2}[0-9]{2,2}$\",\"NAME\":\"^[a-zA-Z0-9]{1,20}$\"}}"

static TB_VIWE* cardView = NULL;
static cJSON *edit_record=NULL;
static RECORD_CONTROL* cardRecord = NULL;
static void FilterClick(cJSON* filter);
static void CardCellClick(int line, int col);




static void CardTableFresh(void)
{
    if(cardView)
    {
        cJSON* view = FilterCardData_From_TABLE(NULL);
        cJSON* para = cJSON_Parse(CARD_VIEW_PARA);
        int callback = CardCellClick;
        cJSON_ReplaceItemInObjectCaseSensitive(para, "CellCallback", cJSON_CreateNumber(callback));
        callback = FilterClick;
        cJSON_ReplaceItemInObjectCaseSensitive(cJSON_GetObjectItemCaseSensitive(para, "Filter"), "Callback", cJSON_CreateNumber(callback));
        TB_MenuDisplay(&cardView, view, NULL, NULL);
        cJSON_Delete(view);
        cJSON_Delete(para);

    }
}
void ExitCardEdit(void)
{
    if(cardRecord)
	{
		cardeditor_delete(&cardRecord);
		cardRecord = NULL;
	}
    if(edit_record!=NULL)
    {
        cJSON_Delete(edit_record);
    } 
    edit_record = NULL;  
    setCardSetState(0);
}

int SaveCardRecord(const cJSON* record)
{
    int ret = update_one_carddata_to_tb(record);
    if(ret)
    {
        CardTableFresh();
        API_CardModificationInform();
    }
    ExitCardEdit();
    return ret;
}
int DeleteCardRecord(const cJSON* record)
{
    int ret = delete_one_carddata_to_tb(record);
    if(ret)
    {
        CardTableFresh();
        API_CardModificationInform();
    }
    ExitCardEdit();
    return ret;
}
static void card_del_one(void* data)
{
    DeleteCardRecord(edit_record);
}
static void btn_click(const char* type, void* user_data)
{
    if(!strcmp(type, "Save"))
    {
        SaveCardRecord(edit_record);
    }
    else if(!strcmp(type, "Delete"))
    {
        API_MSGBOX("Delete?", 2, card_del_one, NULL);    
    }
} 
void CreateCardEditMenu(cJSON *edit_able)
{
    cJSON* func = cJSON_CreateObject();
    if(func)
    {
        int callback = btn_click;
        int backcall = ExitCardEdit;
        cJSON* btnname = cJSON_CreateArray();

        cJSON_AddItemToArray(btnname,cJSON_CreateString("Save"));
        cJSON_AddItemToArray(btnname,cJSON_CreateString("Delete"));

        cJSON_AddItemToObject(func,"BtnName",btnname);
        cJSON_AddNumberToObject(func, "BtnCallback", callback);
        cJSON_AddNumberToObject(func, "BackCallback", backcall);
    }
	cJSON *description = cJSON_Parse(CARD_EDITOR_DESCRIPTION);
	cardeditor_menu(&cardRecord,edit_record,edit_able, description,func,"Card Edit");

    cJSON_Delete(description);
    cJSON_Delete(edit_able);
	cJSON_Delete(func);
}

static void card_del_all(void* data)
{
    cJSON *element;
    if(cardView)
    {
	    cJSON_ArrayForEach(element, cardView->view)
        {
            delete_one_carddata_to_tb(element);
        }
        CardTableFresh();
        API_CardModificationInform();
    }
}

static void CardCellClick(int line, int col)
{
    cJSON *edit_able;
    dprintf("line = %d, col =%d\n", line, col);
    if(cardView)
    {
        edit_record=cJSON_Duplicate(cJSON_GetArrayItem(cardView->view, line), 1);
        //printf_json(edit_record,"CardCell");
        #if 0
        edit_able=Create_CardData_Editable(0);
        //create_cardeditor_menu(edit_record,edit_able,API_TB_GetRuleByName(TB_NAME_CardTable));
        cardeditor_menu(edit_record,edit_able,cJSON_Parse(CARD_EDITOR_DESCRIPTION),NULL);
        cJSON_Delete(edit_able);
        #endif
        CreateCardEditMenu(Create_CardData_Editable(0));
    }    	
}
static int* number_edit_event_cb(const char* val,void *user_data)
{
    cJSON* view;
    cJSON* where;
    int room_id=atoi(val);
    where = cJSON_CreateObject();
    if(room_id>=1 && room_id<=32)
    {
	    cJSON_AddNumberToObject(where, "ROOM", room_id);
    }
    if(cardView)
    {  
        cJSON* para = cJSON_Parse(CARD_VIEW_PARA);
        int callback = CardCellClick;
        cJSON_ReplaceItemInObjectCaseSensitive(para, "CellCallback", cJSON_CreateNumber(callback));
        callback = FilterClick;
        cJSON_ReplaceItemInObjectCaseSensitive(cJSON_GetObjectItemCaseSensitive(para, "Filter"), "Callback", cJSON_CreateNumber(callback));
        cJSON_ReplaceItemInObjectCaseSensitive(cJSON_GetObjectItemCaseSensitive(para, "Filter"), "Value", cJSON_CreateString(val));
 
        view = FilterCardData_From_TABLE(where);
        TB_MenuDisplay(&cardView, view, "Card Management", para);
        cJSON_Delete(view);
        cJSON_Delete(para);
    }
    cJSON_Delete(where);    
}
static void FilterClick(cJSON* filter)
{
    //dprintf("Name = %s, Value =%s\n", GetEventItemString(filter,  "Name"), GetEventItemString(filter,  "Value"));
    //Vtk_KbMode2Init(Vtk_KbMode2,NULL,"ROOM",2,"^(1[0-9]|2[0-9]|3[0-2]|[1-9])$",GetEventItemString(filter,"Value"),"1~32",number_edit_event_cb,NULL);
}
static void FunctionClick(const char* funcName)
{
    if(funcName)
    {
        if(!strcmp(funcName, "Add"))
        {
            #if 0
            cJSON *edit_able;
            edit_record=Create_New_Card_Item();
            edit_able=Create_CardData_Editable(1);
            cardeditor_menu(edit_record,edit_able,cJSON_Parse(CARD_EDITOR_DESCRIPTION),NULL);
            cJSON_Delete(edit_able);
            #endif
            edit_record=Create_New_Card_Item();
            CreateCardEditMenu(Create_CardData_Editable(1));
            setCardSetState(1);
        }
        else if(!strcmp(funcName, "Delete all"))
        {
            API_MSGBOX("Delete?", 2, card_del_all, NULL);
        }
    }
}
void EnterCardTableMenu(void)
{
	cJSON* para = cJSON_Parse(CARD_VIEW_PARA);
	int callback = CardCellClick;
	cJSON_ReplaceItemInObjectCaseSensitive(para, "CellCallback", cJSON_CreateNumber(callback));
    callback = FilterClick;
    cJSON_ReplaceItemInObjectCaseSensitive(cJSON_GetObjectItemCaseSensitive(para, "Filter"), "Callback", cJSON_CreateNumber(callback));

    cJSON* func = cJSON_CreateObject();
	cJSON* funcName = cJSON_AddArrayToObject(func, "Name");
	cJSON_AddItemToArray(funcName, cJSON_CreateString("Add"));
	cJSON_AddItemToArray(funcName, cJSON_CreateString("Delete all"));
	cJSON_AddNumberToObject(func, "Callback", (int)FunctionClick);
	cJSON_AddItemToObject(para, "Function", func);

    cJSON* view = FilterCardData_From_TABLE(NULL);
    TB_MenuDisplay(&cardView, view, "Card Management", para);
    
    cJSON_Delete(view);
	cJSON_Delete(para);
}
void CardTableMenuClose(void)
{
    ExitCardEdit();
	TB_MenuDelete(&cardView);
}