#include "cardtable_display.h"
#include "menu_common.h"
#include "obj_IXS_Proxy.h"


static void load_table_from_json(lv_obj_t* obj, cJSON* json, int page);
static void draw_table_event_cb(lv_event_t* e);
static void table_exit_event(lv_event_t* e);
static void left_click_event(lv_event_t* e);
static void right_click_event(lv_event_t* e);
static void add_title(lv_obj_t* parent);
static void add_filter(lv_obj_t* parent, char* id);
static void add_table(lv_obj_t* parent, cJSON* data, int cnt_page);
static void add_change_page(lv_obj_t* parent, int cnt_page, int max_page, int size);
static void add_control(lv_obj_t* parent);
static void page_change_event_cb(lv_event_t* e);
static void IXG_edit_event_cb(lv_event_t* e);
static void table_edit_event_cb(lv_event_t* e);
static void table_add_event(lv_event_t* e);
static void cnt_change_event_cb(lv_event_t* e);
static void ui_table_init(void);

static lv_obj_t* kb;
CARDTABLE_DISPLAY* td;
static lv_obj_t* ui_table = NULL;
static lv_obj_t* back_scr = NULL;
#if 0
void table_test(void)
{
    
    cJSON* where = cJSON_CreateObject();
    cJSON_AddStringToObject(where, "DevModel", "IXG-IM");
	cJSON_AddStringToObject(where, "IX_ADDR", GetSysVerInfo_bd());

    cJSON *NameList_temp=NULL;
	IXS_ProxyGetTb(&NameList_temp, NULL, R8001);

	cJSON* view = cJSON_CreateArray();	
	cJSON* tpl=cJSON_CreateObject();
	
    cJSON_AddStringToObject(tpl,"IX_ADDR","");
	cJSON_AddStringToObject(tpl,"IXG_ID","");
	cJSON_AddStringToObject(tpl,"IXG_IM_ID","");

	cJSON_AddStringToObject(tpl,"G_NBR","");
	cJSON_AddStringToObject(tpl,"L_NBR","");
	cJSON_AddStringToObject(tpl,"IX_NAME","");
	cJSON_AddItemToArray(view,tpl);

	API_TB_SelectBySort(NameList_temp, view, where, 0);

	cJSON_DeleteItemFromArray(view,0);

    td = create_table_display_menu(view,1);
    cJSON_Delete(where);  
    cJSON_Delete(view);
}
#endif
void lv_msg_table_process(void* s, lv_msg_t* m)
{
    LV_UNUSED(s);
    int32_t page;

    switch (lv_msg_get_id(m))
    {
        case TB_MSG_CHANGE_PAGE:
            page = (int32_t)lv_msg_get_payload(m);

            add_table(td->table_parent, td->cnt_data, page);
            break;
        case TB_MSG_EXIT:
            cardtable_delete(td);
            scr_main();
            break;
    }
}

CARDTABLE_DISPLAY* create_cardtable_display_menu(cJSON* view,int page)
{
    back_scr = lv_scr_act();
    ui_table_init();

    lv_msg_subsribe(TB_MSG_CHANGE_PAGE, lv_msg_table_process, NULL);

    td = (CARDTABLE_DISPLAY*)lv_mem_alloc(sizeof(CARDTABLE_DISPLAY));
    lv_memset_00(td, sizeof(CARDTABLE_DISPLAY));

    lv_obj_t* displaymenu = lv_obj_create(ui_table);
    lv_obj_set_size(displaymenu, 480, 800);
    lv_obj_set_style_pad_all(displaymenu, 0, 0);
    lv_obj_clear_flag(displaymenu, LV_OBJ_FLAG_SCROLLABLE);
    td->table_parent = displaymenu;
  
    td->init_data = view;
    td->cnt_data = view;

    int size = cJSON_GetArraySize(view);
    char buf[100];
    int cnt = size % 8;
    if (cnt == 0)
    {
        cnt = size / 8;
    }
    else {
        cnt = size / 8 + 1;
    }

    td->cnt_page = page;
    td->min_page = 1;
    td->max_page = cnt;

    add_title(displaymenu);
    add_filter(displaymenu, "");     
    add_change_page(displaymenu, page, cnt, size);
    add_control(displaymenu);
    add_table(td->table_parent, view, td->cnt_page);
    lv_disp_load_scr(ui_table);
    return td;
}

void card_data_fresh(CARDTABLE_DISPLAY* tb_disp,cJSON* view,int page)
{
	if(tb_disp==NULL)
		return;
	//tb_disp->init_data = view;
    tb_disp->cnt_data = view;
int size = cJSON_GetArraySize(view);
    int cnt = size % 8;
    if (cnt == 0)
    {
        cnt = size / 8;
    }
    else {
        cnt = size / 8 + 1;
    }

    tb_disp->cnt_page = page;
    tb_disp->min_page = 1;
    tb_disp->max_page = cnt;

    lv_msg_send(TB_MSG_CHANGE_CNT,size);
    lv_msg_send(TB_MSG_CHANGE_PAGE,page);
	  add_table(tb_disp->table_parent, tb_disp->cnt_data, tb_disp->cnt_page);
}
static void add_title(lv_obj_t* parent)
{
    //�����
    lv_obj_t* titleCont = lv_obj_create(parent);
    lv_obj_set_size(titleCont, 480, 80);
    lv_obj_set_style_pad_all(titleCont, 0, 0);
    lv_obj_t* titlelabel = lv_label_create(titleCont);
    lv_obj_set_style_text_font(titlelabel, &lv_font_montserrat_32, 0);
    //lv_obj_set_style_text_color(titlelabel, lv_color_hex(0xff0000), 0);
    lv_obj_center(titlelabel);
    //lv_label_set_text(titlelabel, "Card Management");
    vtk_label_set_text(titlelabel, "Card Management");
}

static void add_filter(lv_obj_t* parent,char* id)
{
    //��������
    lv_obj_t* filterCont = lv_obj_create(parent);
    lv_obj_set_style_pad_row(filterCont, 0, 0);
    lv_obj_set_size(filterCont, LV_PCT(100), 100);
    lv_obj_set_flex_flow(filterCont, LV_FLEX_FLOW_ROW);
    lv_obj_set_style_text_font(filterCont, &lv_font_montserrat_32, 0);
    lv_obj_clear_flag(filterCont, LV_OBJ_FLAG_SCROLLABLE);
    lv_obj_set_y(filterCont, 80);

    lv_obj_t* filter_label = lv_label_create(filterCont);
    //lv_label_set_text(filter_label, "Room");
    vtk_label_set_text(filter_label, "ROOM");
    lv_obj_set_size(filter_label, 200, LV_SIZE_CONTENT);

    lv_obj_t* ta = lv_textarea_create(filterCont);
    lv_textarea_add_text(ta, id);
    lv_obj_set_size(ta, 200, LV_SIZE_CONTENT);
    lv_obj_clear_state(ta, LV_STATE_FOCUSED);

    kb = lv_keyboard_create(ui_table);
    lv_obj_set_style_text_font(kb, &lv_font_montserrat_26, 0);
    lv_obj_add_flag(kb, LV_OBJ_FLAG_HIDDEN);

    lv_obj_add_event_cb(ta, IXG_edit_event_cb, LV_EVENT_ALL, NULL);
}

static void IXG_edit_event_cb(lv_event_t* e)
{
    lv_event_code_t code = lv_event_get_code(e);
    lv_obj_t* ta = lv_event_get_target(e);
    cJSON* where;
	int i;
    if (code == LV_EVENT_CLICKED) {
        if (lv_indev_get_type(lv_indev_get_act()) != LV_INDEV_TYPE_KEYPAD) {
            lv_obj_add_state(ta, LV_STATE_FOCUSED);
            lv_keyboard_set_textarea(kb, ta);

            lv_keyboard_set_mode(kb, LV_KEYBOARD_MODE_NUMBER);

            lv_obj_clear_flag(kb, LV_OBJ_FLAG_HIDDEN);
        }
    }
    else if (code == LV_EVENT_CANCEL) {

        lv_obj_add_flag(kb, LV_OBJ_FLAG_HIDDEN);

    }
    else if (code == LV_EVENT_READY)
    {
        char* text = lv_textarea_get_text(ta);
	int i=0;
	while(text[i]!=0&&text[i]>='0'&&text[i]<='9')
		i++;
	
	
       // if(cJSON_IsNumber(value))
       if(text[i]==0&&i>0)
        {
           where = cJSON_CreateObject();
	        cJSON_AddNumberToObject(where, "ROOM", atoi(text));

	        lv_obj_add_flag(kb, LV_OBJ_FLAG_HIDDEN);
            td->msg_data =  where;
            td->msg_type = TB_MSG_CHANGE_FITER;
            API_SettingMenuProcess(td); 
            //MenuCardTable_Process(TB_MSG_CHANGE_FITER,where);	
            cJSON_Delete(where);
            lv_obj_clear_state(ta,LV_STATE_FOCUSED);
        }
	  else if(text[i]==0&&i==0)
	  {
		    where = cJSON_CreateObject();
	        lv_obj_add_flag(kb, LV_OBJ_FLAG_HIDDEN);
            td->msg_data =  where;
            td->msg_type = TB_MSG_CHANGE_FITER;
            API_SettingMenuProcess(td);
            //MenuCardTable_Process(TB_MSG_CHANGE_FITER,where);	
            cJSON_Delete(where);	
            lv_obj_clear_state(ta,LV_STATE_FOCUSED);
	  }
	  else
	  {
        LV_API_TIPS(" Error! Please enter it again!",4);
		//API_TIPS(" Error! Please enter it again!");
	  }
	  	
        
    }
}

static void add_table(lv_obj_t* parent,cJSON* data,int cnt_page)
{
	if(td->table)
		lv_obj_del(td->table);
    lv_obj_t* table = lv_table_create(parent);
    lv_obj_set_size(table, 480, 440);
    lv_obj_set_y(table, 180);

    lv_table_set_col_width(table, 0, 200);
    lv_table_set_col_width(table, 1, 150);
    lv_table_set_col_width(table, 2, 150);
    lv_table_set_col_width(table, 3, 150);
    //lv_table_set_col_width(table, 4, 120);
    //lv_table_set_col_width(table, 5, 200);

    load_table_from_json(table, data, cnt_page);
    lv_obj_add_event_cb(table, draw_table_event_cb, LV_EVENT_DRAW_PART_BEGIN, NULL);
    lv_obj_add_event_cb(table, table_edit_event_cb, LV_EVENT_VALUE_CHANGED, NULL);
    td->table = table;
    
}

static void table_edit_event_cb(lv_event_t* e)
{
    lv_obj_t* obj = lv_event_get_target(e);

    uint16_t col;
    uint16_t row;
    lv_table_get_selected_cell(obj, &row, &col);
    uint16_t cnt = lv_table_get_row_cnt(obj);
    if (row >= cnt || row == 0)
    {
        return;
    }

    cJSON* data = cJSON_GetArrayItem(td->cnt_data, (td->cnt_page-1)*8+row-1);
	//printf("11111111 row=%d,cnt=%d,td->cnt_page=%d\n",row,cnt,td->cnt_page);
    
    //printf_json((cJSON*)data,__func__);
	//printf_json((cJSON*)td->cnt_data,__func__);
    td->msg_data =  data;
    td->msg_type = TB_MSG_EDIT_RECORD;
    API_SettingMenuProcess(td);
	//MenuCardTable_Process(TB_MSG_EDIT_RECORD,data);
}

static void add_change_page(lv_obj_t* parent, int cnt_page,int max_page,int size)
{
    //��ҳ���ƿ�
    lv_obj_t* pageControl = lv_obj_create(parent);
    lv_obj_set_size(pageControl, 480, 90);
    lv_obj_set_style_pad_all(pageControl, 0, 0);
    lv_obj_set_y(pageControl, 620);

    lv_obj_t* leftBtn = lv_btn_create(pageControl);
    lv_obj_align(leftBtn, LV_ALIGN_LEFT_MID, 20, 0);
    lv_obj_set_style_text_color(leftBtn, lv_color_hex(0xff0000), 0);
    lv_obj_set_style_bg_color(leftBtn, lv_color_make(230, 231, 232), 0);
    lv_obj_set_size(leftBtn, 80, 70);
    lv_obj_set_style_text_font(leftBtn, &lv_font_montserrat_32, 0);
    lv_obj_add_event_cb(leftBtn, left_click_event, LV_EVENT_CLICKED, NULL);
    lv_obj_set_style_radius(leftBtn, LV_RADIUS_CIRCLE, 0);
    lv_obj_t* leftlabel = lv_label_create(leftBtn);
    lv_label_set_text(leftlabel, LV_SYMBOL_LEFT);
    lv_obj_center(leftlabel);

    char buf[100];
    sprintf(buf, "%d/%d", cnt_page, max_page);

    lv_obj_t* pagelabel = lv_label_create(pageControl);
    lv_label_set_text(pagelabel, buf);
    lv_obj_set_style_text_font(pagelabel, &lv_font_montserrat_32, 0);
    lv_obj_align(pagelabel, LV_ALIGN_CENTER, -60, 0);
    lv_msg_subsribe_obj(TB_MSG_CHANGE_PAGE, pagelabel, NULL);
    lv_obj_add_event_cb(pagelabel, page_change_event_cb, LV_EVENT_MSG_RECEIVED, NULL);

    lv_obj_t* rightBtn = lv_btn_create(pageControl);
    lv_obj_align(rightBtn, LV_ALIGN_CENTER, 50, 0);
    lv_obj_set_style_text_color(rightBtn, lv_color_hex(0xff0000), 0);
    lv_obj_set_style_bg_color(rightBtn, lv_color_make(230, 231, 232), 0);
    lv_obj_set_size(rightBtn, 80, 70);
    lv_obj_set_style_text_font(rightBtn, &lv_font_montserrat_32, 0);
    lv_obj_add_event_cb(rightBtn, right_click_event, LV_EVENT_CLICKED, NULL);
    lv_obj_set_style_radius(rightBtn, LV_RADIUS_CIRCLE, 0);
    lv_obj_t* rightlabel = lv_label_create(rightBtn);
    lv_label_set_text(rightlabel, LV_SYMBOL_RIGHT);
    lv_obj_center(rightlabel);

    sprintf(buf, "%d", size);
    lv_obj_t* cntlabel = lv_label_create(pageControl);
    lv_label_set_text(cntlabel, buf);
    lv_obj_set_style_text_font(cntlabel, &lv_font_montserrat_32, 0);
    lv_obj_align(cntlabel, LV_ALIGN_RIGHT_MID, -50, 0);
    lv_msg_subsribe_obj(TB_MSG_CHANGE_CNT, cntlabel, NULL);
    lv_obj_add_event_cb(cntlabel, cnt_change_event_cb, LV_EVENT_MSG_RECEIVED, NULL);
}

static void page_change_event_cb(lv_event_t* e)
{
    lv_obj_t* label = lv_event_get_target(e);
    lv_msg_t* m = lv_event_get_msg(e);
    int32_t val = (int32_t)lv_msg_get_payload(m);
    char buf[100];
    sprintf(buf, "%d/%d", val, td->max_page);
    lv_label_set_text(label, buf);
}

static void cnt_change_event_cb(lv_event_t* e)
{
    lv_obj_t* label = lv_event_get_target(e);
    if(label == NULL)
        return;
    lv_msg_t* m = lv_event_get_msg(e);
    int32_t val = (int32_t)lv_msg_get_payload(m);
    char buf[100];
    sprintf(buf, "%d", val);
    lv_label_set_text(label, buf);
}

static void add_control(lv_obj_t* parent)
{
    //������
    lv_obj_t* btnCont = lv_obj_create(parent);
    lv_obj_set_size(btnCont, 480, 90);
    lv_obj_set_style_pad_all(btnCont, 0, 0);
    lv_obj_set_y(btnCont, 710);
    //���ذ���
    lv_obj_t* backBtn = lv_btn_create(btnCont);
    lv_obj_align(backBtn, LV_ALIGN_LEFT_MID, 50, 0);
    //lv_obj_set_style_bg_color(backBtn, lv_color_make(230, 231, 232), 0);
    lv_obj_set_size(backBtn, 120, 70);
    lv_obj_set_style_text_font(backBtn, &lv_font_montserrat_32, 0);
    lv_obj_add_event_cb(backBtn, table_exit_event, LV_EVENT_CLICKED, NULL);
    lv_obj_set_style_radius(backBtn, LV_RADIUS_CIRCLE, 0);
    lv_obj_t* backlabel = lv_label_create(backBtn);
    lv_label_set_text(backlabel, LV_SYMBOL_BACKSPACE);
    lv_obj_center(backlabel);
    //��������
    lv_obj_t* addBtn = lv_btn_create(btnCont);
    lv_obj_set_size(addBtn, 120, 70);
    lv_obj_set_style_text_font(addBtn, &lv_font_montserrat_32, 0);
    lv_obj_add_event_cb(addBtn, table_add_event, LV_EVENT_CLICKED, NULL);
    lv_obj_align(addBtn, LV_ALIGN_RIGHT_MID, -50, 0);
    lv_obj_set_style_radius(addBtn, LV_RADIUS_CIRCLE, 0);
    //lv_obj_set_style_bg_color(addBtn, lv_color_make(230, 231, 232), 0);
    lv_obj_t* addlabel = lv_label_create(addBtn);
    lv_label_set_text(addlabel, "+");
    lv_obj_center(addlabel);
}

static void draw_table_event_cb(lv_event_t* e)
{
    lv_obj_t* obj = lv_event_get_target(e);
    lv_obj_draw_part_dsc_t* dsc = lv_event_get_draw_part_dsc(e);
    /*If the cells are drawn...*/
    if (dsc->part == LV_PART_ITEMS) {
        uint32_t row = dsc->id / lv_table_get_col_cnt(obj);
        uint32_t col = dsc->id - row * lv_table_get_col_cnt(obj);

        dsc->label_dsc->font = &lv_font_montserrat_24;
        dsc->label_dsc->align = LV_TEXT_ALIGN_CENTER;

        /*Make the texts in the first cell center aligned*/
        if (row == 0) {
            dsc->label_dsc->align = LV_TEXT_ALIGN_CENTER;
            dsc->rect_dsc->bg_color = lv_color_mix(lv_palette_main(LV_PALETTE_BLUE), dsc->rect_dsc->bg_color, LV_OPA_20);
            dsc->rect_dsc->bg_opa = LV_OPA_COVER;
        }
    }
}

static void load_table_from_json(lv_obj_t* obj, cJSON* json,int page)
{
    int size = cJSON_GetArraySize(json);

    char text[100];
    for (int i = 0; i < 8; i++)
    {
        int cnt = ((page - 1) * 8) + i;
        cJSON* item = cJSON_GetArrayItem(json, cnt);
        if (item == NULL)
        {
            break;
        }
        int length = cJSON_GetArraySize(item);
        for (int j = 0; j < length; j++)
        {
            cJSON* val = cJSON_GetArrayItem(item, j);
            if (i == 0)
            {
                //lv_table_set_cell_value(obj, i, j, val->string);
                lv_table_set_cell_value(obj, i, j, get_language_text(val->string)? get_language_text(val->string) : val->string);
            }
            if (cJSON_IsString(val))
            {
                lv_table_set_cell_value(obj, i + 1, j, val->valuestring);
            }
            else if (cJSON_IsNumber(val))
            {
                sprintf(text, "%d", val->valueint);
                lv_table_set_cell_value(obj, i + 1, j, text);
            }
        }
    }
}

static void table_exit_event(lv_event_t* e)
{
    lv_disp_load_scr(back_scr);

    td->msg_data =  NULL;
    td->msg_type = TB_MSG_EXIT;
    API_SettingMenuProcess(td); 
    //MenuCardTable_Process(TB_MSG_EXIT, NULL);
}

static void table_add_event(lv_event_t* e)
{
    td->msg_data =  NULL;
    td->msg_type = TB_MSG_ADD_RECORD;
    API_SettingMenuProcess(td); 
    //MenuCardTable_Process(TB_MSG_ADD_RECORD, NULL);
}

static void left_click_event(lv_event_t* e)
{
    //printf("%d\n", td->cnt_page);
    td->cnt_page--;
    if (td->cnt_page <= 0)
    {
        td->cnt_page = td->max_page;
    }
    //printf("%d\n", td->cnt_page);

    lv_msg_send(TB_MSG_CHANGE_PAGE, td->cnt_page);

    //add_table(td->table_parent, td->cnt_data, td->cnt_page);
}

static void right_click_event(lv_event_t* e)
{
    //printf("%d\n", td->cnt_page);
    td->cnt_page++;
    if (td->cnt_page >td->max_page)
    {
        td->cnt_page = 1;
    }
    //printf("%d\n", td->cnt_page);
    lv_msg_send(TB_MSG_CHANGE_PAGE, td->cnt_page);
    //add_table(td->table_parent, td->cnt_data, td->cnt_page);
}

bool cardtable_delete(CARDTABLE_DISPLAY* td)
{
    if (td)
    {
        if (td->table)
        {
            lv_obj_del(td->table);
            td->table = NULL;
        }
        if (td->table_parent)
        {
            lv_obj_del(td->table_parent);
            td->table_parent = NULL;
        }
        if (td->cnt_data)
        {
           // cJSON_Delete(td->cnt_data);
            td->cnt_data = NULL;
        }
        if(kb){
            lv_obj_del(kb);
            kb = NULL;
        }
        if(ui_table){
            lv_obj_del(ui_table);
            ui_table = NULL;
        }
        lv_mem_free(td);
        td = NULL;
        return true;
    }
    else
        return false;
}

static void ui_table_init(void)
{
    ui_table = lv_obj_create(NULL);
    lv_obj_clear_flag(ui_table, LV_OBJ_FLAG_SCROLLABLE);
    lv_obj_remove_style_all(ui_table);
    lv_obj_set_size(ui_table, lv_disp_get_hor_res(NULL), lv_disp_get_ver_res(NULL));
    lv_obj_align(ui_table, LV_ALIGN_TOP_LEFT, 0, 0);
    lv_obj_set_style_bg_color(ui_table, lv_palette_main(LV_PALETTE_GREY), 0);
    lv_obj_set_style_bg_opa(ui_table, LV_OPA_100, 0);
}