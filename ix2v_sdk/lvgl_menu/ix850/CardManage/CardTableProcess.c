#include <stdio.h>
#include "task_CallServer.h"
#include "elog_forcall.h"
#include "obj_PublicInformation.h"
#include "task_Event.h"
#include "ix850_icon.h"
#include "obj_lvgl_msg.h"
#include "cardtable_display.h"
#include "define_file.h"
#include "obj_TableSurver.h"
#include "obj_IoInterface.h"

static CARDTABLE_DISPLAY *table_display=NULL;
static cJSON *view_tb=NULL;
static cJSON *edit_record=NULL;
static cJSON *view_filter=NULL;
static lv_obj_t* editor=NULL;
static int add_new_item=0;
static int cardSetState=0;

lv_obj_t* create_cardeditor_menu(cJSON* rec_data, const cJSON* editabel, const cJSON* tb_description);

cJSON *FilterCardData_From_TABLE(cJSON *filter);
cJSON *Create_New_Card_Item(void);
cJSON *Create_CardData_Editable(int new_item);
int Judge_Card_Exist(cJSON *item);


void MenuCardTable_Init(void* arg)
{
	//cJSON* where = cJSON_CreateObject();
	//cJSON_AddNumberToObject(where, "ROOM", 1);
	view_tb=FilterCardData_From_TABLE(NULL);
	table_display=create_cardtable_display_menu(view_tb,1);
	cardSetState = 1;
	//cJSON_Delete(where);
	//printf("111111111%s:%d\n",__func__,__LINE__);

}
void MenuCardTable_Process(int msg_type,void* arg)
{
	//printf("111111111MenuCardTable_Process msg:%d\n",msg_type);
	cJSON *edit_able;
	switch(msg_type)
	{
		case TB_MSG_ADD_RECORD:
			if(edit_record!=NULL)
				cJSON_Delete(edit_record);
			edit_record=Create_New_Card_Item();
			edit_able=Create_CardData_Editable(1);
			create_cardeditor_menu(edit_record,edit_able,API_TB_GetRuleByName(TB_NAME_CardTable));
			cJSON_Delete(edit_able);
			add_new_item=1;
			break;
			
		case TB_MSG_EDIT_RECORD:
			if(edit_record!=NULL)
				cJSON_Delete(edit_record);
			edit_record=cJSON_Duplicate((cJSON*)arg,1);
			edit_able=Create_CardData_Editable(0);
			create_cardeditor_menu(edit_record,edit_able,API_TB_GetRuleByName(TB_NAME_CardTable));
			cJSON_Delete(edit_able);
			add_new_item=0;
			break;
			
		case TB_MSG_CHANGE_FITER:
			if(view_filter)
				cJSON_Delete(view_filter);
			if(view_tb)
				cJSON_Delete(view_tb);
			//printf_json((cJSON*)arg,"FILTER");
			view_filter=cJSON_Duplicate((cJSON*)arg,1);
			view_tb=FilterCardData_From_TABLE((cJSON*)arg);
			card_data_fresh(table_display,view_tb,1);
			break;
		case TB_MSG_EXIT:
			MenuCardTable_Close();
			break;
			
		case ONE_RECORD_MSG_SAVE_EXIT:
			if(add_new_item)
			{
				if(Judge_Card_Exist((cJSON*)arg)>0)
				{
					LV_API_TIPS(" Error! Please enter it again!",4);
					//API_TIPS("The record is aleady exist!");
					return;
				}
			}
			printf_json((cJSON*)arg,"SAVE_EXIT");
			if(view_tb)
				cJSON_Delete(view_tb);
			update_one_carddata_to_tb((cJSON*)arg);
			view_tb=FilterCardData_From_TABLE(view_filter);
			card_data_fresh(table_display,view_tb,1);
			break;
		case ONE_RECORD_MSG_DEL_EXIT:
			if(add_new_item)
			{
			
				//if(Judge_Card_Exist((cJSON*)arg))>0)
				{
					//API_TIPS("The device is aleady exist!");
					return;
				}
			}
			if(view_tb)
				cJSON_Delete(view_tb);
			delete_one_carddata_to_tb((cJSON*)arg);
			view_tb=FilterCardData_From_TABLE(view_filter);
			card_data_fresh(table_display,view_tb,1);
			break;	
		case MSG_SWIPE_CARD:
			//printf("222222222222MenuCardTable_Process cardnum:%s\n",(char*)arg);
			//if(add_new_item)
			{
				vtk_lvgl_lock();
				lv_msg_send(MSG_SWIPE_CARD_FRESH, (char*)arg);
				vtk_lvgl_unlock();
			}
			break;	
	}
}

void MenuCardTable_Close(void)
{
		
	if(table_display!=NULL)
		cardtable_delete(table_display);
	if(view_tb)
		cJSON_Delete(view_tb);
	view_tb=NULL;

	if(edit_record!=NULL)
		cJSON_Delete(edit_record);
	edit_record=NULL;

	if(view_filter)
		cJSON_Delete(view_filter);
	view_filter=NULL;
		
	table_display=NULL;
	cardSetState = 0;
	//printf("11111111111%s:%d\n",__func__,__LINE__);
}

int getCardSetState(void)
{
	return cardSetState;
}
void setCardSetState(int state)
{
	cardSetState = state;
}
cJSON *FilterCardData_From_TABLE(cJSON *filter)
{
	cJSON* view = cJSON_CreateArray();
	cJSON* where = cJSON_CreateObject();
	cJSON* tpl=cJSON_CreateObject();
	cJSON_AddStringToObject(tpl,"CARD_NUM","");
	cJSON_AddNumberToObject(tpl,"ROOM",1);
	cJSON_AddStringToObject(tpl,"DATE","");
	cJSON_AddStringToObject(tpl,"NAME","");
	cJSON_AddItemToArray(view,tpl);

	cJSON *filter_element;
	cJSON_ArrayForEach(filter_element, filter)
	{
		if(cJSON_IsNumber(filter_element))
			cJSON_AddNumberToObject(where,filter_element->string,filter_element->valueint);
		else if(cJSON_IsString(filter_element))
			cJSON_AddStringToObject(where,filter_element->string,filter_element->valuestring);
	}
	API_TB_SelectBySortByName(TB_NAME_CardTable, view, where, 0);

	cJSON_Delete(where);

	cJSON_DeleteItemFromArray(view,0);

	//SortTableView(view, "ROOM", 0);

	return view;
}

int Check_Card_Exist(char* cardnum)
{	
	int ret;
	cJSON *wh=cJSON_CreateObject();
	cJSON* View=cJSON_CreateArray();
	cJSON_AddStringToObject(wh,"CARD_NUM",cardnum);

	ret = API_TB_SelectBySortByName(TB_NAME_CardTable, View, wh, 0);

	cJSON_Delete(wh);
	cJSON_Delete(View);

	return ret;
}
cJSON *Create_New_Card_Item(void)
{
	int room_id=1;
	time_t t;
	struct tm *tblock; 	
	t = time(NULL); 
	tblock=localtime(&t);
	char buff[50];
	sprintf( buff,"%02d%02d%02d",(tblock->tm_year-100)>0?(tblock->tm_year-100):0,tblock->tm_mon+1,tblock->tm_mday);
	cJSON* tpl=cJSON_CreateObject();
	cJSON_AddStringToObject(tpl,"CARD_NUM","");
	cJSON_AddNumberToObject(tpl,"ROOM",room_id);
	cJSON_AddStringToObject(tpl,"DATE",buff);
	cJSON_AddStringToObject(tpl,"NAME","-");
	return tpl;
}
cJSON *Create_CardData_Editable(int new_item)
{
	cJSON *edit_able=cJSON_CreateArray();
	if(new_item)
	{
		cJSON_AddItemToArray(edit_able,cJSON_CreateString("CARD_NUM"));
	}
	cJSON_AddItemToArray(edit_able,cJSON_CreateString("ROOM"));
	//cJSON_AddItemToArray(edit_able,cJSON_CreateString("DATE"));
	cJSON_AddItemToArray(edit_able,cJSON_CreateString("NAME"));
	return edit_able;
}
int Judge_Card_Exist(cJSON *item)
{
	cJSON *key=cJSON_GetObjectItemCaseSensitive(item,"CARD_NUM");
	if(key==NULL)
		return -1;
	
	cJSON *wh=cJSON_CreateObject();
	cJSON* View=cJSON_CreateArray();
	int ret;
	cJSON_AddStringToObject(wh,"CARD_NUM",key->valuestring);

	ret = API_TB_SelectBySortByName(TB_NAME_CardTable, View, wh, 0);

	cJSON_Delete(wh);
	cJSON_Delete(View);

	return ret;
}
int update_one_carddata_to_tb(cJSON *item)
{
	int ret = 0;

	cJSON *key=cJSON_GetObjectItemCaseSensitive(item,"CARD_NUM");
	if(key==NULL || !strcmp(key->valuestring, ""))
		return ret;
	
	cJSON *wh=cJSON_CreateObject();
	cJSON* View=cJSON_CreateArray();
	cJSON_AddStringToObject(wh,"CARD_NUM",key->valuestring);
	if(API_TB_SelectBySortByName(TB_NAME_CardTable, View, wh, 0))
	{
		ret = API_TB_UpdateByName(TB_NAME_CardTable, wh, item);
	}
	else
	{
		#if 1
		char carddata[11];
		int len;
		len = strlen(key->valuestring);
		if( len < 10 )
		{
			memset(carddata,0,11);
			memset(carddata,'0',10-len);
			strcat(carddata,key->valuestring);
		}
		else
		{
			sprintf(carddata,"%s",key->valuestring);
		}
		cJSON_ReplaceItemInObjectCaseSensitive(item, "CARD_NUM", cJSON_CreateString(carddata));
		#endif
		if(API_TB_CountByName(TB_NAME_CardTable,NULL)<1000)
			ret = API_TB_AddByName(TB_NAME_CardTable, item);

	}

	cJSON_Delete(wh);
	cJSON_Delete(View);
	return ret;
}

int delete_one_carddata_to_tb(cJSON *item)
{
	int ret = 0;
	cJSON *key=cJSON_GetObjectItemCaseSensitive(item,"CARD_NUM");
	if(key==NULL || !strcmp(key->valuestring, ""))
		return ret;
	
	cJSON *wh=cJSON_CreateObject();
	cJSON_AddStringToObject(wh,"CARD_NUM",key->valuestring);
	ret = API_TB_DeleteByName(TB_NAME_CardTable, wh);
	cJSON_Delete(wh);

	return ret;
}

cJSON* GetIxsIpByIX_ADDR(char* ixAddr)
{
	cJSON* devTb = NULL;
	cJSON* Where = cJSON_CreateObject();
	cJSON* View = cJSON_CreateArray();

	cJSON_AddStringToObject(Where, IX2V_IX_ADDR, ixAddr);

	IXS_ProxyGetTb(&devTb, NULL, 1);

	API_TB_SelectBySort(devTb, View, Where, 0);

	cJSON_Delete(devTb);

	return View;
}

int Check_CardContact_Exist(int id, char* cardnum);

void CardSwip_Process(char* cardnum)
{
	int ret=0;
	if(getCardSetState())
	{
		MenuCardTable_Process(MSG_SWIPE_CARD, cardnum);
	}
	else
	{
		if(Check_Card_Exist(cardnum) >0)
		{
			API_Event_Unlock("RL1", "Card");
			API_CardswipesInform(1);
		}
		else
		{
			cJSON *cardContact=API_Para_Read_Public(CardContactPara);
			cJSON* element;
			int cnt,i;
			if(cardContact)
			{
				cnt = cJSON_GetArraySize(cardContact);
				for(i = 0; i < cnt; i++)
				{
					element = cJSON_GetArrayItem(cardContact,i);
					if(cJSON_IsNumber(element))
					{
						ret = Check_CardContact_Exist(element->valueint, cardnum);
						if(ret)
						{
							API_Event_Unlock("RL1", "Card");
							API_CardswipesInform(1);
							return;
						}
					}
				}
			}
			if(ret==0)
			{
				printf("CardSwip_Process ret=%d \n", ret);
				LV_API_TIPS("card not exist!",4);
				API_CardswipesInform(0);
			}
		}
	}
}

void API_MenuCardTable_Process(void* arg)
{
	CARDTABLE_DISPLAY* cd = (CARDTABLE_DISPLAY*)arg;
	MenuCardTable_Process(cd->msg_type,cd->msg_data);
}