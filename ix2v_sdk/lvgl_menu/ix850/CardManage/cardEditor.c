﻿#include "Common_Record.h"
#include "menu_utility.h"
#include "obj_ResEditor.h"
#include "utility.h"
#include "icon_common.h"
#include "cardtable_display.h"
#include "obj_IoInterface.h"

extern lv_ft_info_t ft_small;

#define TB_VIEW_FunctionColor       lv_color_hex(0x414500)
#define TB_VIEW_BackgroundColor     lv_color_hex(0x41454D)
#define TB_VIEW_TextColor           lv_color_white()
#define TB_VIEW_NotEditTextColor    lv_color_hex(0x808080)
#define TB_VIEW_TitleHeight         73
#define TB_VIEW_LineHeight          70
#define TB_VIEW_TextSize            0
#define TB_VIEW_ReturnWidth         160
#define TB_VIEW_ScrWidth            480

//static RECORD_CONTROL* rc = NULL;
static char* rc_map[] = {0};

static int* text_edit_event_cb(const char* val,void *user_data)
{
    RECORD_CONTROL* rc = (RECORD_CONTROL*)user_data;
    char* name = lv_table_get_cell_value(rc->table,rc->cnt_row,0);

    cJSON* new_item = cJSON_CreateString(val);

    lv_table_set_cell_value(rc->table,rc->cnt_row,1,val);           //更新表格数据

    cJSON_ReplaceItemInObject(rc->rec_data, name, new_item);        //更新字段内容
}

static int* number_edit_event_cb(const char* val,void *user_data)
{
    RECORD_CONTROL* rc = (RECORD_CONTROL*)user_data;
    char* name = lv_table_get_cell_value(rc->table,rc->cnt_row,0);

    cJSON *new_item = cJSON_CreateNumber(atoi(val));

    lv_table_set_cell_value(rc->table,rc->cnt_row,1,val);

    cJSON_ReplaceItemInObject(rc->rec_data, name, new_item);
}
static void refresh_event_cb(lv_event_t * e)
{
    RECORD_CONTROL* rc = lv_event_get_user_data(e);
    char* name = lv_table_get_cell_value(rc->table,0,0);
    lv_msg_t* m = lv_event_get_msg(e);
    char* text = lv_msg_get_payload(m);
    if(strcmp(name,"CARD_NUM")==0)
    {
        lv_table_set_cell_value(rc->table,0,1,text);           //更新表格数据
        cJSON_ReplaceItemInObject(rc->rec_data, "CARD_NUM", cJSON_CreateString(text));        //更新字段内容
    }
}

static void number_text_edit(RECORD_CONTROL* disp,char* name,char* text)
{
    char* a = cJSON_Print(disp->EditCheck);
    printf("%s\n",a);
    free(a);
    char* regex_pattern = NULL;
    if(disp->EditCheck){
       cJSON* pattern  = cJSON_GetObjectItem(disp->EditCheck,name);
       if(cJSON_IsString(pattern))
            regex_pattern = pattern->valuestring;
    }

    Vtk_KbMode2Init(&disp->kb,Vtk_KbMode2,NULL,name,20,regex_pattern,text, regex_pattern,number_edit_event_cb,disp);
}

static int ContainsLetter(const char *str)
{
    while (*str != '\0') {
        if (isalpha(*str)) {
            return 1; // 发现字母，返回非零值
        }
        str++; // 移动到下一个字符
    }
    return 0; // 没有发现字母，返回0
}

static void string_text_edit(RECORD_CONTROL* disp,char* name,char* text)
{
    char* a = cJSON_Print(disp->EditCheck);
    printf("%s\n",a);
    free(a);
    char* regex_pattern = NULL;
    if(disp->EditCheck){
       cJSON* pattern  = cJSON_GetObjectItem(disp->EditCheck,name);
       if(cJSON_IsString(pattern))
            regex_pattern = pattern->valuestring;
    }

    //正则表达式中没有字母，则调用数字键盘
    if(regex_pattern && !ContainsLetter(regex_pattern))
    {
        Vtk_KbMode2Init(&disp->kb,Vtk_KbMode2,NULL,name,20,regex_pattern,text, regex_pattern,text_edit_event_cb,disp);
    }
    else
    {
        Vtk_KbMode1Init(&disp->kb,Vtk_KbMode1, API_Para_Read_String2(KeybordLanguage),name,30,regex_pattern,text, regex_pattern,text_edit_event_cb,disp);
    }
}

static void Record_HeadReturn(lv_event_t* e)
{
    RECORD_CONTROL** rcDisp = lv_event_get_user_data(e);

    RECORD_CONTROL* rc;

    rc = *rcDisp;

    if(rc->backCallback)
    {
        ((BackCallback)(rc->backCallback))(NULL, NULL);
    } 

}

static void Record_AddTitle(RECORD_CONTROL** disp, char* txt)
{
    RECORD_CONTROL* rc;

    rc = *disp;

    lv_obj_t* btn = lv_obj_create(rc->menu);
    lv_obj_set_size(btn,TB_VIEW_ScrWidth, TB_VIEW_TitleHeight);   
    lv_obj_set_style_bg_color(btn, lv_color_make(26,82,196), 0);
    lv_obj_set_style_pad_all(btn,0,0);
    lv_obj_set_style_border_width(btn, 0, 0);

    lv_obj_add_event_cb(btn, Record_HeadReturn, LV_EVENT_CLICKED, disp);
    lv_obj_add_flag(btn, LV_OBJ_FLAG_CLICKABLE);
    lv_obj_clear_flag(btn, LV_OBJ_FLAG_SCROLLABLE);

    lv_obj_t* obj = lv_obj_create(btn);
    lv_obj_remove_style_all(obj);
    lv_obj_add_flag(obj, LV_OBJ_FLAG_EVENT_BUBBLE);
    lv_obj_set_style_bg_opa(obj,LV_OPA_MAX,0);
    lv_obj_set_size(obj,160,TB_VIEW_TitleHeight);
    lv_obj_set_style_bg_color(obj,lv_color_make(19,132,250),0);
    lv_obj_set_style_text_color(obj, lv_color_white(), 0);
    lv_obj_t* img = lv_img_create(obj);
	lv_img_set_src(img,LV_VTK_LEFT);
    set_font_size(img,1);
    lv_obj_center(img);

    lv_obj_t* titlelabel = lv_label_create(btn); 
    lv_obj_align(titlelabel, LV_ALIGN_LEFT_MID, TB_VIEW_ReturnWidth, 0);
    lv_obj_set_size(titlelabel, TB_VIEW_ScrWidth-TB_VIEW_ReturnWidth, 30);
    lv_obj_set_style_text_align(titlelabel,LV_TEXT_ALIGN_CENTER,0);
    lv_obj_set_style_text_font(titlelabel, vtk_get_font_by_size(TB_VIEW_TextSize), 0); 
    lv_obj_set_style_text_color(titlelabel, TB_VIEW_TextColor, 0);
    lv_label_set_long_mode(titlelabel, LV_LABEL_LONG_DOT);
    vtk_label_set_text(titlelabel,txt);
}

static void draw_table_event_cb(lv_event_t* e)
{
    lv_obj_t* obj = lv_event_get_target(e);
    lv_obj_draw_part_dsc_t* dsc = lv_event_get_draw_part_dsc(e);
    RECORD_CONTROL* rc = lv_event_get_user_data(e);
    lv_table_t* table = (lv_table_t*)obj;
    int edit = 0;

    if (dsc->part == LV_PART_ITEMS) {
        uint32_t row = dsc->id / lv_table_get_col_cnt(obj);
        uint32_t col = dsc->id - row * lv_table_get_col_cnt(obj);

        char* keyName = lv_table_get_cell_value(rc->table, row, 0);
        if(keyName)
        {
            cJSON* editName;
            cJSON_ArrayForEach(editName, rc->editabel)
            {
                if(cJSON_IsString(editName) && !strcmp(editName->valuestring, keyName))
                {
                    edit = 1;
                    break;
                }
            }
        }

        dsc->label_dsc->font = vtk_get_font_by_size(TB_VIEW_TextSize);
        dsc->label_dsc->align = LV_TEXT_ALIGN_LEFT;
        dsc->label_dsc->color = (edit ? TB_VIEW_TextColor:TB_VIEW_NotEditTextColor);
        dsc->rect_dsc->bg_color = TB_VIEW_BackgroundColor;
        dsc->rect_dsc->border_width = 3;
        dsc->rect_dsc->border_color = lv_color_make(55,55,55);
        //dsc->rect_dsc->outline_width = 10;

        char* text = lv_table_get_cell_value(table, row, col);
        int len = lv_txt_get_width(text, strlen(text), vtk_get_font_by_size(TB_VIEW_TextSize), lv_obj_get_style_text_letter_space(table, LV_PART_ITEMS), LV_TEXT_FLAG_NONE);
        if(len > (table->col_w[col] - 32))
        {
            dsc->label_dsc->ofs_y = -((TB_VIEW_LineHeight - lv_font_get_line_height(vtk_get_font_by_size(TB_VIEW_TextSize)))/2);
        }
        else
        {
            dsc->label_dsc->ofs_y = -((TB_VIEW_LineHeight - lv_font_get_line_height(vtk_get_font_by_size(TB_VIEW_TextSize))*2 - dsc->label_dsc->line_space)/2);
        }
    }
}

static void table_edit_event_cb(lv_event_t* e)
{
    RECORD_CONTROL* rc = lv_event_get_user_data(e);
    lv_obj_t* obj = lv_event_get_target(e);
    int edit = 0;
    uint16_t col;
    uint16_t row;
    lv_table_get_selected_cell(obj, &row, &col);    
    uint16_t cnt = lv_table_get_row_cnt(obj);
    if (row >= cnt)
    {
        return;
    }

    rc->cnt_row = row;
    char* text = lv_table_get_cell_value(obj,row,1);
    char* name = lv_table_get_cell_value(obj,row,0);

    int able_size = cJSON_GetArraySize(rc->editabel);

    for (int i = 0; i < able_size; i++)
    {
        cJSON* able = cJSON_GetArrayItem(rc->editabel, i);
        if(able && strcmp(able->valuestring, name)==0)
        {
			edit=1;
			break;
		}
    }

    if(edit){
        cJSON* field = cJSON_GetObjectItem(rc->rec_data,name);
        if(cJSON_IsString(field))
		   string_text_edit(rc,name,text);
	    else if(cJSON_IsNumber(field))
		   number_text_edit(rc,name,text);
    }
}

static void Record_UpdateDate(RECORD_CONTROL* disp)
{
    cJSON* element;
    int col;

    if(!disp)
    {
        return;
    }

    if(!disp->table)
    {
        disp->table = lv_table_create(disp->menu);
        lv_obj_add_event_cb(disp->table, draw_table_event_cb, LV_EVENT_DRAW_PART_BEGIN, disp);
        lv_obj_add_event_cb(disp->table, table_edit_event_cb, LV_EVENT_VALUE_CHANGED, disp);
        lv_table_set_row_height(disp->table,TB_VIEW_LineHeight);
        lv_obj_add_flag(disp->table, LV_OBJ_FLAG_SCROLL_CHAIN_HOR);
        lv_obj_set_scroll_dir(disp->table,LV_DIR_HOR);
        lv_obj_set_style_border_width(disp->table, 0, 0);
        lv_obj_set_style_bg_color(disp->table, TB_VIEW_BackgroundColor, 0);
    }

    lv_obj_set_size(disp->table, TB_VIEW_ScrWidth, 630);
    lv_obj_set_y(disp->table, TB_VIEW_TitleHeight);
    
    lv_table_set_row_cnt(disp->table, 0);
    lv_table_set_col_cnt(disp->table, 0);

    lv_table_set_col_width(disp->table, 0, 200);
    lv_table_set_col_width(disp->table, 1, 280);

    int rec_size = cJSON_GetArraySize(disp->rec_data);

    for (int i = 0; i < rec_size; i++)
    {
        cJSON* item = cJSON_GetArrayItem(disp->rec_data, i);
        if (item)
        {
            //vtk_table_set_cell_text(disp->table, i, 0, item->string);
            lv_table_set_cell_value(disp->table, i, 0, item->string);
            if (cJSON_IsString(item))
            {
                //vtk_table_set_cell_text(disp->table, i, 1, item->valuestring);
                lv_table_set_cell_value(disp->table, i, 1, item->valuestring);
            }
            else if (cJSON_IsNumber(item))
            {
                //vtk_table_set_cell_text(disp->table, i, 1, "%d", item->valueint);
                lv_table_set_cell_value_fmt(disp->table, i, 1, "%d", item->valueint);
            }
        }
    }
}
/************
* rcDisp: 显示的记录配置指针
 * rec_data ： 需要编辑的json数据
 * title ： 标题
 * editabel ：可编辑的字段
 * 格式：["IX_NAME","G_NBR","L_NBR"]
 * description ：编辑字段时的描述,例如字段的规则
 * func：按键功能及名字
 * 格式：
 {
    "BtnName": ["Save","Delete","Cancel"],     功能按键名字
    "BackCallback": 0,          返回按钮的回调
    "BtnCallback": 0,           功能按键的回调
 }
************/
void cardeditor_menu(RECORD_CONTROL** rcDisp, cJSON* rec_data, const cJSON* editabel, const cJSON* description,const cJSON* func,const char* title)
{
    RECORD_CONTROL* rc = NULL;
    if(!rcDisp)
    {
        return;
    }

    vtk_lvgl_lock();

    if(*rcDisp)
    {
        cardeditor_delete(rcDisp);
    }

    rc = (RECORD_CONTROL*)lv_mem_alloc(sizeof(RECORD_CONTROL));
    lv_memset_00(rc, sizeof(RECORD_CONTROL));
    *rcDisp = rc;
	rc->back_scr = lv_scr_act();
	rc->scr = ui_record_init();
    rc->rec_data = rec_data;
    rc->editabel = cJSON_Duplicate(editabel,true);
    rc->description = cJSON_Duplicate(description,true);
    rc->func = cJSON_Duplicate(func,true);

    rc->EditCheck = cJSON_GetObjectItem(rc->description,"Check");

    rc->menu = lv_obj_create(rc->scr);
    lv_obj_set_size(rc->menu, 490, 810);
    lv_obj_align(rc->menu, LV_ALIGN_TOP_MID, 3, -3);
    lv_obj_set_style_pad_all(rc->menu, 0, 0);
    lv_obj_clear_flag(rc->menu, LV_OBJ_FLAG_SCROLLABLE);
    lv_obj_set_style_bg_color(rc->menu, TB_VIEW_BackgroundColor, 0);

    Record_AddTitle(rcDisp, title);
    Record_UpdateDate(rc);   

    lv_msg_subsribe_obj(MSG_SWIPE_CARD_FRESH, rc->table, NULL);
    lv_obj_add_event_cb(rc->table, refresh_event_cb, LV_EVENT_MSG_RECEIVED, rc);

    cJSON* btncall = cJSON_GetObjectItem(rc->func,"BtnCallback");
    if(btncall)
        rc->btnCallback = btncall->valueint;
    
    cJSON* backcall = cJSON_GetObjectItem(rc->func,"BackCallback");
    if(backcall)
        rc->backCallback = backcall->valueint;

    cJSON* btnname = cJSON_GetObjectItem(rc->func,"BtnName");
    int count = cJSON_GetArraySize(btnname);
    int i;
    for (i = 0; i < count; i++) {
        cJSON* btntext = cJSON_GetArrayItem(btnname, i);
        if(btntext){
            rc_map[i] = btntext->valuestring;         
        }else{
            rc_map[i] = "";
        }
    }
    rc_map[i] = NULL;


    lv_obj_t* btnm1 = lv_btnmatrix_create(rc->menu);
    lv_obj_set_style_shadow_width(btnm1,0,LV_PART_ITEMS);
    lv_obj_set_style_text_color(btnm1, TB_VIEW_TextColor, LV_PART_ITEMS);
    lv_obj_set_style_border_color(btnm1,lv_color_make(55,55,55), 0);
    lv_obj_set_style_bg_color(btnm1, lv_color_make(150, 150, 150),LV_PART_ITEMS);
    set_font_size(btnm1,0);
    lv_obj_set_style_bg_color(btnm1, TB_VIEW_FunctionColor, 0);
    lv_obj_set_size(btnm1, TB_VIEW_ScrWidth, 100);
    lv_obj_set_y(btnm1, TB_VIEW_TitleHeight + 630);
    lv_btnmatrix_set_map(btnm1, rc_map);
    lv_btnmatrix_set_one_checked(btnm1,true);
    lv_obj_add_event_cb(btnm1, event_handler, LV_EVENT_VALUE_CHANGED, rcDisp);
    
    if(rc->scr != lv_scr_act())
    {
        rc->back_scr = lv_scr_act();
        lv_disp_load_scr(rc->scr);
    }
    vtk_lvgl_unlock();
}

static void event_handler(lv_event_t * e)
{
    RECORD_CONTROL** rcRecord = lv_event_get_user_data(e); 
    RECORD_CONTROL* rc;

    rc = *rcRecord;

    lv_obj_t * obj = lv_event_get_target(e);
    uint32_t id = lv_btnmatrix_get_selected_btn(obj);
    const char* txt = lv_btnmatrix_get_btn_text(obj, id);

    if(rc->btnCallback)
    {
        ((BtnClickCallback)(rc->btnCallback))(txt, NULL);
    } 

}

/*****************
 * 删除record编辑页面
 * **************/
void cardeditor_delete(RECORD_CONTROL** rc)
{
    RECORD_CONTROL* disp = NULL;
    if(!rc)
    {
        return;
    }
    vtk_lvgl_lock();
    if(*rc)
    {
        disp = *rc;
        *rc = NULL;
        if(disp->kb)
        {
            Vtk_KbReleaseByMode(&disp->kb);
            disp->kb = NULL;
        }
        if(disp->scr == lv_scr_act())
        {
            lv_scr_load(disp->back_scr);
        }     
        if (disp->scr)
        {
            lv_obj_del(disp->scr);
            disp->scr = NULL;
        }
        if (disp->editabel)
        {
            cJSON_Delete(disp->editabel);
            disp->editabel = NULL;
        }
        if (disp->description)
        {
            cJSON_Delete(disp->description);
            disp->description= NULL;
        }
        if (disp->func)
        {
            cJSON_Delete(disp->func);
            disp->func= NULL;
        }        
        lv_mem_free(disp);
    }
    vtk_lvgl_unlock();
}

static lv_obj_t* ui_record_init(void)
{
    lv_obj_t* ui_record = lv_obj_create(NULL);
    lv_obj_clear_flag(ui_record, LV_OBJ_FLAG_SCROLLABLE);
    lv_obj_remove_style_all(ui_record);
    lv_obj_set_size(ui_record, lv_disp_get_hor_res(NULL), lv_disp_get_ver_res(NULL));
    lv_obj_align(ui_record, LV_ALIGN_TOP_LEFT, 0, 0);
    lv_obj_set_style_bg_color(ui_record, lv_palette_main(LV_PALETTE_GREY), 0);
    lv_obj_set_style_bg_opa(ui_record, LV_OPA_100, 0);
    return ui_record;
}
