#ifndef CARDTABLE_DISPLAY_H_
#define CARDTABLE_DISPLAY_H_

#include "lv_ix850.h"

typedef struct 
{   
    cJSON* msg_data;
    int msg_type;    
    cJSON* init_data;          
    lv_obj_t* table_parent;        
    lv_obj_t* table;          
    cJSON* cnt_data;
    int cnt_page;
    int min_page;
    int max_page;
}CARDTABLE_DISPLAY;

bool cardtable_delete(CARDTABLE_DISPLAY* td);
void lv_msg_table_process(void* s, lv_msg_t* m);
CARDTABLE_DISPLAY* create_cardtable_display_menu(cJSON* view, int page);
void card_data_fresh(CARDTABLE_DISPLAY* tb_disp,cJSON* view,int page);

#define TB_MSG_CHANGE_FITER 		230908901
#define TB_MSG_CHANGE_PAGE 		    230908902
#define TB_MSG_EXIT 				230908903
#define TB_MSG_ADD_RECORD 		    230908904
#define TB_MSG_EDIT_RECORD 		    230908905
#define TB_MSG_CHANGE_CNT           230908906

#define ONE_RECORD_MSG_SAVE_EXIT    230908907
#define ONE_RECORD_MSG_DEL_EXIT     230908908
#define ONE_RECORD_MSG_CANCEL_EXIT  230908909
#define MSG_SWIPE_CARD              230908910
#define MSG_SWIPE_CARD_FRESH        230908911

#endif 
