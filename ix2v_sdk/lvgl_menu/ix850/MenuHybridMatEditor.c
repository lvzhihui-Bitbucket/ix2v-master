#include "menu_utility.h"
#include "MenuHybridMatCreateNew.h"
#include "cJSON.h"
#include "obj_TableSurver.h"
#include "define_string.h"
#include "obj_PublicInformation.h"
#include "utility.h"
#include "task_Event.h"
#include "Common_TableView.h"
#include "obj_ResEditor.h"

#define RES_EDITOR_PARA		"{\"HeaderEnable\":true,\"CellCallback\":0,\"ColumnWidth\":[70, 70, 180, 120, 120],\"Function\":{\"Name\":[],\"Callback\":0}}}"
#define RES_EDITOR_VIEW_TEMPALE "{\"GW_ID\":1,\"IM_ID\":1,\"IX_NAME\":\"\",\"L_NBR\":\"\",\"G_NBR\":\"\"}"
#define RES_EDITOR_EDIT_ENABLE "[\"IX_ADDR\",\"GW_ADDR\",\"IX_TYPE\",\"CONNECT\",\"GW_ID\",\"IM_ID\",\"G_NBR\",\"L_NBR\",\"IX_NAME\"]"
#ifdef IX_LINK
#define RES_EDITOR_AUTO_RECORD  "{\"IXG START\":1,\"IXG END\":8,\"DT-IM START\":1,\"DT-IM END\":8,\"L_NBR START\":\"01\",\"G_NBR PREFIX\":\"1\",\"NAME PREFIX\":\"IM_\"}"
#else
#define RES_EDITOR_AUTO_RECORD  "{\"DXG START\":1,\"DXG END\":8,\"DT-IM START\":1,\"DT-IM END\":8,\"L_NBR START\":\"01\",\"G_NBR PREFIX\":\"1\",\"NAME PREFIX\":\"IM_\"}" //编辑的记录
#endif
typedef struct 
{   
    int imId;
    int gwId;
    int lNbr;
    int lNbrLen;
    int gNbr;
    int gNbrLen;
    char name[100];
}MAT_EDIT_ONE_T;

static MAT_EDIT_ONE_T editOne = {.imId = 1, .gwId = 1, .lNbr = 1, .lNbrLen = 1, .gNbr = 1, .gNbrLen = 1, .name = "IM"};

static TB_VIWE* resEditorTb = NULL;
static AUTO_RES_MENU_T batchAddResMenu = {.resRecord = NULL, .recordCtrl = NULL};
static RES_Editor_MENU_T resEditOneMenu = {.resRecord = NULL, .recordCtrl = NULL}; 


static void ResCellClick(int line, int col)
{
	if(resEditorTb)
	{
		cJSON* record = cJSON_GetArrayItem(resEditorTb->view, line);
		if(cJSON_IsObject(record))
		{
			//resRecord：编辑的记录,{"IX_ADDR":"","IX_NAME":"","G_NBR":"","L_NBR":""}
			//editabel:可编辑字段，["IX_NAME","G_NBR","L_NBR"]
			CreateResEditOneMenu(record, NULL, 1);
		}
	}
}

static void ResReturnCallback(void)
{
	MenuRES_Editor_Close();
}

static void CloseBatchAddResMenu(void)
{      
    if(batchAddResMenu.recordCtrl)
    {
        RC_MenuDelete(&batchAddResMenu.recordCtrl);
    }

    if(batchAddResMenu.resRecord)
    {
        cJSON_Delete(batchAddResMenu.resRecord);
        batchAddResMenu.resRecord = NULL;
    }
}

static void BatchAddResCancel(const char* type, void* user_data)
{  
	CloseBatchAddResMenu();
}

static void BatchAddRes(const cJSON* para)
{
	cJSON* view = AutoCreateNewR8001(para, 256);
	cJSON* record;
	int resRecords = 0;

	cJSON_ArrayForEach(record, view)
	{
		if(API_TB_AddByName(TB_NAME_HYBRID_MAT, record))
		{
			resRecords++;
		}
	}
	cJSON_Delete(view);
	CloseBatchAddResMenu();
	if(resRecords)
	{
		API_Event_By_Name(Event_MAT_Update);
		API_TableModificationInform(TB_NAME_HYBRID_MAT, NULL);
		MenuRES_Editor_Refresh();
	}

	char tempString[200];
	char translate[100];

	snprintf(tempString, 200, "%s %d", get_language_text2("MAT batch Add", translate, 100), resRecords);
	API_TIPS_Ext_Time(tempString, 3);
} 

static void BatchAddResFunction(const char* type, void* user_data)
{
	if(type)
	{
		if(!strcmp(type, "Batch add"))
		{
			BatchAddRes(batchAddResMenu.resRecord);
			//API_MSGBOX("About to clear RES and recreate", 2, BatchAddRes, batchAddResMenu.resRecord);
		}
	}
} 

static void CloseHybridMatAddRecordMenu(void)
{      
    if(resEditOneMenu.recordCtrl)
    {
        RC_MenuDelete(&resEditOneMenu.recordCtrl);
    }

    if(resEditOneMenu.resRecord)
    {
        cJSON_Delete(resEditOneMenu.resRecord);
        resEditOneMenu.resRecord = NULL;
    }
}

static void CancelCallback(const char* type, void* user_data)
{  
	CloseHybridMatAddRecordMenu();
}

static void EditOneRecordProcessingInitialValues(const cJSON* lastRecord)
{
	editOne.imId = GetEventItemInt(lastRecord,  IX2V_IM_ID);
	editOne.gwId = GetEventItemInt(lastRecord,  IX2V_GW_ID);
	char* tempString = GetEventItemString(lastRecord,  IX2V_L_NBR);
	editOne.lNbrLen = strlen(tempString);
	editOne.lNbr = atoi(tempString);

	tempString = GetEventItemString(lastRecord,  IX2V_G_NBR);
	editOne.gNbrLen = strlen(tempString);
	editOne.gNbr = atoi(tempString);

	if(++editOne.imId > 32)
	{
		editOne.imId = 1;
		if(++editOne.gwId > 32)
		{
			editOne.gwId = 1;
		}
	}
	editOne.gNbr++;
	editOne.lNbr++;
	snprintf(editOne.name, 100, "%s", GetEventItemString(lastRecord,  IX2V_IX_NAME));
}

static void FunctionCallback(const char* type, void* user_data)
{
    if(type)
    {
        if(!strcmp(type, "Save"))
        {
			int gwId = GetEventItemInt(resEditOneMenu.resRecord,  IX2V_GW_ID);
			int imId = GetEventItemInt(resEditOneMenu.resRecord,  IX2V_IM_ID);

			cJSON_AddStringToObject(resEditOneMenu.resRecord, IX2V_IX_ADDR, GW_ID_AND_IM_ID_TO_IX_ADDR(gwId, imId));
			cJSON_AddStringToObject(resEditOneMenu.resRecord, IX2V_GW_ADDR, GW_ID_TO_GW_ADDR(gwId));
			cJSON_AddStringToObject(resEditOneMenu.resRecord, IX2V_IX_TYPE, "IM");
			cJSON_AddStringToObject(resEditOneMenu.resRecord, IX2V_CONNECT, "DT");

            if(API_TB_AddByName(TB_NAME_HYBRID_MAT, resEditOneMenu.resRecord))
            {
				EditOneRecordProcessingInitialValues(resEditOneMenu.resRecord);
				API_TableModificationInform(TB_NAME_HYBRID_MAT, NULL);
				API_Event_By_Name(Event_MAT_Update);
				CloseHybridMatAddRecordMenu();
				MenuRES_Editor_Refresh();
				API_TIPS_Ext_Time("Add success", 3);
            }
			else
			{
				API_TIPS_Ext_Time("Add error", 3);
			}
        }
    }
} 


//resRecord：编辑的记录,{"IX_ADDR":"","IX_NAME":"","G_NBR":"","L_NBR":""}
//editabel:可编辑字段，["IX_NAME","G_NBR","L_NBR"]

static void FunctionClick(const char* funcName)
{
	if(funcName)
	{
		if(!strcmp(funcName, "Add"))
		{
			char format[20];
			char temp[100];

			cJSON* record = cJSON_CreateObject();
			cJSON_AddNumberToObject(record, IX2V_GW_ID, editOne.gwId);
			cJSON_AddNumberToObject(record, IX2V_IM_ID, editOne.imId);
			snprintf(format, 20, "%%0%dd", editOne.lNbrLen);
			snprintf(temp, 100, format, editOne.lNbr);
			cJSON_AddStringToObject(record, IX2V_L_NBR, temp);
			snprintf(format, 20, "%%0%dd", editOne.gNbrLen);
			snprintf(temp, 100, format, editOne.gNbr);
			cJSON_AddStringToObject(record, IX2V_G_NBR, temp);
			cJSON_AddStringToObject(record, IX2V_IX_NAME, editOne.name);

			cJSON* editabel = cJSON_Parse(RES_EDITOR_EDIT_ENABLE);
			CreateResRecordEditorMenu(record, editabel, &resEditOneMenu, CancelCallback, FunctionCallback, "Add MAT record", "Save", NULL);
			cJSON_Delete(editabel);
			cJSON_Delete(record);
		}
		else if (!strcmp(funcName, "Batch add"))
		{
			CreateAutoResMenu(&batchAddResMenu, RES_EDITOR_AUTO_RECORD, BatchAddResCancel, BatchAddResFunction, "Batch add", "Batch add", NULL);
		}
	}
}

void MenuRES_Editor_Refresh(void)
{
	if(resEditorTb)
	{
		cJSON* view = cJSON_CreateArray();
		cJSON_AddItemToArray(view, cJSON_Parse(RES_EDITOR_VIEW_TEMPALE));
		API_TB_SelectBySortByName(TB_NAME_HYBRID_MAT, view, NULL, 0);
		cJSON_DeleteItemFromArray(view, 0);
		TB_MenuDisplay(&resEditorTb, view, "MAT editor", NULL);
		cJSON_Delete(view);
	}
}

void MenuRES_Editor_Init(void)
{
	cJSON* para = cJSON_Parse(RES_EDITOR_PARA);
	cJSON_ReplaceItemInObjectCaseSensitive(para, "CellCallback", cJSON_CreateNumber((int)ResCellClick));
	cJSON_AddNumberToObject(para, "ReturnCallback", (int)ResReturnCallback);

	cJSON* func = cJSON_CreateObject();
	cJSON* funcName = cJSON_AddArrayToObject(func, "Name");
	cJSON_AddItemToArray(funcName, cJSON_CreateString("Add"));
	cJSON_AddItemToArray(funcName, cJSON_CreateString("Batch add"));
	cJSON_AddNumberToObject(func, "Callback", (int)FunctionClick);
	cJSON_ReplaceItemInObjectCaseSensitive(para, "Function", func);

	cJSON* view = cJSON_CreateArray();
	cJSON_AddItemToArray(view, cJSON_Parse(RES_EDITOR_VIEW_TEMPALE));
	API_TB_SelectBySortByName(TB_NAME_HYBRID_MAT, view, NULL, 0);
	cJSON_DeleteItemFromArray(view, 0);

	TB_MenuDisplay(&resEditorTb, view, "MAT editor", para);
	cJSON_Delete(view);
	cJSON_Delete(para);
}

void MenuRES_Editor_Close(void)
{
	CloseHybridMatAddRecordMenu();
	CloseBatchAddResMenu();
	CloseHybridMatEditRecordMenu();
	TB_MenuDelete(&resEditorTb);
	API_Event_NameAndMsg(EventMenuSettingProcess, "SettingRefresh", NULL);
}