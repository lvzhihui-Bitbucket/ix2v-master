#ifndef _MenuSetting_H_
#define _MenuSetting_H_

#include "lvgl.h"
#include "cJSON.h"
#include "KeyboardResource.h"

typedef struct 
{   
    lv_obj_t* back_scr;
    lv_obj_t* scr;
    lv_obj_t* menu; 
    lv_obj_t* titleimg;       
    lv_obj_t* titlelabel;       
    lv_obj_t* table;          
    cJSON* pageData;
    cJSON* items;
    char* fileName;
    char* menuName;
    int recordCnt;
    void* child;
    void* parent;
    int cur_row;
    int switch_id;
    int* symbolFlag;
    int* valueFlag;
    int* textFlag;
    int* editDisableFlag;
    VTK_KB_S *kb;
}SETTING_VIEW;

typedef struct 
{
    char* fileName;
    char* menuName;
}SETTING_CHANGE_PAGE_DATA_T;

void lv_msg_setting_handler(void* s, lv_msg_t* m);
void API_EnterSettingRoot(const char* rootName);
void API_SettingMenuProcess(void* data);
void API_SettingMenuTextEditProcess(void* data);
void API_SettingMenuClose(void);
void API_SettingMenuReturn(void);
void API_SettingLongTip(char *tip);
void API_SettingNormalTip(char *tip);
void API_SettingSuccTip(char *tip);
void API_SettingErrorTip(char *tip);
void API_SettingValueFresh(char *para);
void API_SettingChangePage(const char* pageName);
void API_SettingInnerChangePage(const char* fileName, const char* pageName);
void API_SettingReplacePage(const char* fileName, const char* pageName);

typedef void (*ButonClickCallback)(void *);
int API_JsonMenuDisplay(SETTING_VIEW** disp, const cJSON* menuJson, const char* menuName, ButonClickCallback callback);
int API_JsonMenuDelete(SETTING_VIEW** disp);

#endif 