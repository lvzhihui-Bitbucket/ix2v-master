#include "ix850_info.h"
#include "MenuSetting.h"
#include "menu_page.h"
#include "Common_TableView.h"
#include "obj_CommonSelectMenu.h"

#define PIC_SELECT_PARA		"{\"HeaderEnable\":true,\"CellCallback\":0,\"ColumnWidth\":[180, 300]}"
#define MENU_PICTURE_FIX_PATH        "/mnt/nand1-1/App/res/UI-DOCS/PID-850/info_picture"
#define MENU_PICTURE_CUS_PATH        "/mnt/nand1-2/Customerized/UI-DOCS/PID-850/info_picture"
#define MENU_PICTURE_SD_PATH         "/mnt/sdcard/Customerized/UI-DOCS/PID-850/info_picture"

static TB_VIWE* picSelect = NULL;
static char* io_name = NULL;
static void ImgInfo_click_event(lv_event_t* e);
static char* cnt_dir;

lv_obj_t* img_creat_label(lv_obj_t* parent, struct ICON_INS* picon)
{
	//printf_json(picon->icon_data, __func__);
    cJSON* pos = cJSON_GetObjectItemCaseSensitive(picon->icon_data, get_global_key_string(GLOBAL_ICON_KEYS_POS));
    cJSON* text = cJSON_GetObjectItemCaseSensitive(picon->icon_data, get_global_key_string(GLOBAL_ICON_KEYS_TEXT));

    cJSON* color = cJSON_GetObjectItemCaseSensitive(text, get_global_key_string(GLOBAL_ICON_KEYS_TEXT_COLOR));
    cJSON* size = cJSON_GetObjectItemCaseSensitive(text, get_global_key_string(GLOBAL_ICON_KEYS_TEXT_SIZE));
    cJSON* name = cJSON_GetObjectItemCaseSensitive(text, get_global_key_string(GLOBAL_ICON_KEYS_LABEL_TEXT));

    lv_obj_t* imglabel = creat_cont_for_postion(parent, pos);
    
    lv_obj_t* label = lv_label_create(imglabel);
    lv_label_set_text(label, "");
    lv_label_set_long_mode(label,LV_LABEL_LONG_SCROLL);
    lv_obj_center(label);
    picon->icon_cont = label;
    if (text)
    {
        lv_obj_set_style_text_color(label, lv_color_hex(color->valuedouble), 0);
        set_font_size(label, size->valueint);
	
        char* sym = get_symbol_key_plus(name->valuestring);
	    if(sym)
        	lv_label_set_text(label, sym);
     	else
     	{
     		//printf("111111:%s\n",name->valuestring);
     		vtk_label_set_text(label, name->valuestring);
        	//lv_label_set_text(label, name->valuestring);
     	}
    }
    lv_obj_add_event_cb(imglabel, ImgInfo_click_event, LV_EVENT_CLICKED, picon->icon_data);
}

lv_obj_t* img_creat_info(lv_obj_t* parent, struct ICON_INS* picon)
{
    cJSON* pos = cJSON_GetObjectItemCaseSensitive(picon->icon_data, get_global_key_string(GLOBAL_ICON_KEYS_POS));
    cJSON* text = cJSON_GetObjectItemCaseSensitive(picon->icon_data, get_global_key_string(GLOBAL_ICON_KEYS_TEXT));
    cJSON* pic = cJSON_GetObjectItemCaseSensitive(picon->icon_data, "PIC");

    cJSON* color = cJSON_GetObjectItemCaseSensitive(text, get_global_key_string(GLOBAL_ICON_KEYS_TEXT_COLOR));
    cJSON* size = cJSON_GetObjectItemCaseSensitive(text, get_global_key_string(GLOBAL_ICON_KEYS_TEXT_SIZE));
    cJSON* name = cJSON_GetObjectItemCaseSensitive(text, get_global_key_string(GLOBAL_ICON_KEYS_LABEL_TEXT));

    lv_obj_t* imglabel = creat_cont_for_postion(parent, pos);
    lv_obj_t* img = lv_img_create(imglabel);

    if(pic){
        printf("img_creat_info ----------------------    buf = %s\n",pic->valuestring);
        lv_img_set_src(img,pic->valuestring);
    }
 
    lv_obj_t* label = lv_label_create(imglabel);
    //lv_obj_set_size(label,LV_PCT(80),LV_PCT(95));
    lv_label_set_text(label, "");
    lv_obj_align(label,LV_ALIGN_TOP_MID,0,20);
    lv_obj_set_size(label,LV_PCT(88),LV_PCT(95));
    lv_obj_set_style_text_align(label,LV_TEXT_ALIGN_CENTER,0);
    picon->icon_cont = imglabel;
    if (text)
    {
        lv_obj_set_style_text_color(label, lv_color_hex(color->valuedouble), 0);
        set_font_size(label, size->valueint);
	
        char* sym = get_symbol_key(name->valuestring);
	    if(sym)
        	lv_label_set_text(label, sym);
     	else
        	lv_label_set_text(label, name->valuestring);
    }
 
    lv_obj_add_event_cb(imglabel, ImgInfo_click_event, LV_EVENT_CLICKED, picon->icon_data);
}

static SETTING_VIEW* disp = NULL;

static void Info_Edit_click(void* data)
{
    if(!strcmp("pic",data)){
        EnterPicSelectMenu("FW");
    }else if(!strcmp("replace",data)){
        API_JsonMenuDelete(&disp);
        API_PublicInfo_Write_Bool("OnInstaller", 0);
        API_PublicInfo_Write_Bool("MenuEdit", false);
        //MainMenu_Close();

        //MainMenu_Init();
        MainMenuReload_InstallerMode();
        free(io_name);
        io_name = NULL;
    }  
}


static void ImgInfo_click_event(lv_event_t* e)
{
    char name[50];
    char text[50];
    char title[50];
    char pic[50];
    char size[50];
    char color[50];
    cJSON* icon_data = lv_event_get_user_data(e);
	cJSON* icon_name = cJSON_GetObjectItemCaseSensitive(icon_data, "ICON_NAME");
    if(API_PublicInfo_Read_Bool("MenuEdit") && icon_name)
    {
        cJSON* object = cJSON_CreateObject();
        sprintf(name, "%sEdit", icon_name->valuestring);
        sprintf(text, "%s_TEXT", icon_name->valuestring);
        sprintf(title, "%s Editing", icon_name->valuestring);
        sprintf(pic, "%s_PIC", icon_name->valuestring);
        sprintf(size,"%s_SIZE",icon_name->valuestring);
        sprintf(color,"%s_COLOR",icon_name->valuestring);

        if(io_name){
            free(io_name);
            io_name = (char *)lv_mem_alloc(50);
        }else{
            io_name = (char *)lv_mem_alloc(50);
        }
        strcpy(io_name,pic);

        cJSON* item1 = cJSON_CreateObject();
        cJSON_AddStringToObject(item1, "PageTitle", title);
        cJSON* item_arr = cJSON_CreateArray();
        cJSON_AddItemToArray(item_arr, cJSON_CreateString(text));
        cJSON_AddItemToArray(item_arr, cJSON_CreateString(pic));
        cJSON_AddItemToArray(item_arr, cJSON_CreateString(size));
        cJSON_AddItemToArray(item_arr, cJSON_CreateString(color));
        cJSON_AddItemToArray(item_arr, cJSON_CreateString("ITEM_Replace"));
        cJSON_AddItemToObject(item1, "Items", item_arr);
        cJSON_AddItemToObject(object, name, item1);

        cJSON* item2 = cJSON_CreateObject();
        cJSON_AddStringToObject(item2, "type", "Item_IO_Para");
        cJSON_AddStringToObject(item2, "ParaName", text);
        cJSON_AddItemToObject(object, text, item2);

        cJSON* item3 = cJSON_CreateObject();
        cJSON_AddStringToObject(item3, "type", "ITEM_btn");
        cJSON_AddStringToObject(item3, "text", "Select Picture");
        cJSON_AddStringToObject(item3, "msg", "pic");
        cJSON_AddItemToObject(object, pic, item3);

        cJSON* item5 = cJSON_CreateObject();
        cJSON_AddStringToObject(item5, "type", "Item_IO_Para");
        cJSON_AddStringToObject(item5, "ParaName", size);
        cJSON_AddItemToObject(object,size,item5);

        cJSON* item6 = cJSON_CreateObject();
        cJSON_AddStringToObject(item6, "type", "Item_IO_Para");
        cJSON_AddStringToObject(item6, "ParaName", color);
        cJSON_AddItemToObject(object,color,item6);

        cJSON* item4 = cJSON_CreateObject();
        cJSON_AddStringToObject(item4, "type", "ITEM_btn");
        cJSON_AddStringToObject(item4, "text", "Replace");
        cJSON_AddStringToObject(item4, "msg", "replace");
        cJSON_AddItemToObject(object, "ITEM_Replace", item4);
        API_JsonMenuDisplay(&disp,object,name,Info_Edit_click);
        cJSON_Delete(object);
    }
    Main_Header_Reset();
}

cJSON* GetPicListName(const char *dir)
{
    if(!dir)
        return;
	char cmd[100];
	char linestr[100];
    int i;
    char* p;
    char* file;
    cJSON* list = cJSON_CreateArray();

	snprintf(cmd, 100, "ls -l %s | grep .bmp | awk '{print $9}'", dir);
	
    cnt_dir = dir;

	FILE *pf = popen(cmd,"r");
	if(pf == NULL)
	{
		return 0;
	}

	while(fgets(linestr,100,pf) != NULL)
	{
		for(i = 0; linestr[i] != 0 && i < 100; i++)
		{
			if(linestr[i] == '\r' || linestr[i] == '\n')
			{
				linestr[i] = 0;
			}
		}

		if(strlen(linestr) == 0)
		{
			continue;
		}
        p = strtok(linestr, ".");
		if(p == NULL)
		{
			p[0] = 0;
		}
        //file = strtok(NULL,".");
        //if(strcmp(file,"json") == 0)
		    cJSON_AddItemToArray(list,cJSON_CreateString(p));
	}
	
	pclose(pf);

	return list;
}

static void PicCellClick(int line, int col)
{   
    char* name = lv_table_get_cell_value(picSelect->table,line+1,0);    
    
    Pic_preview_init(name);
}

static void choose_pic_pos_event(const char* chose, void* userdata)
{   
    if(!chose)
    {
        return;
    }

    if(strcmp("FW",chose) == 0){
        EnterPicSelectMenu("FW");
    }       
    else if(strcmp("SD Card",chose) == 0){
        EnterPicSelectMenu("SD Card");
    } 
    else if(strcmp("Customer",chose) == 0){
        EnterPicSelectMenu("Customer");
    }
    else if(strcmp("All",chose) == 0){
        EnterPicSelectMenu("All");
    }
}

static void choose_pic_pos(const cJSON* filter)
{   
    cJSON* value = cJSON_GetObjectItem(filter,"Value");
    static CHOSE_MENU_T callback = {.callback = 0, .cbData = NULL};
	callback.callback = choose_pic_pos_event;
	callback.cbData = NULL;
    cJSON* choseList = cJSON_CreateArray();
    cJSON_AddItemToArray(choseList, cJSON_CreateString("FW"));
    cJSON_AddItemToArray(choseList, cJSON_CreateString("SD Card"));
    cJSON_AddItemToArray(choseList, cJSON_CreateString("Customer"));
    //cJSON_AddItemToArray(choseList, cJSON_CreateString("All"));
    char* choseString = cJSON_IsString(value) ? value->valuestring : NULL;
    CreateChooseMenu("Choose Path", choseList, choseString, &callback);
    cJSON_Delete(choseList);
}

void EnterPicSelectMenu(const char* pos)
{
    char* dir;
    char des[100];
	cJSON* para = cJSON_Parse(PIC_SELECT_PARA);
	int callback = PicCellClick;
	cJSON_ReplaceItemInObjectCaseSensitive(para, "CellCallback", cJSON_CreateNumber(callback));

    int fiterCallback = choose_pic_pos;
    cJSON* fiter = cJSON_CreateObject();
    cJSON_AddStringToObject(fiter, "Name", "Picture Pos");
    cJSON_AddStringToObject(fiter, "Value", pos);
    cJSON_AddNumberToObject(fiter, "Callback", fiterCallback);
    cJSON_AddItemToObject(para,"Filter",fiter);

    cJSON* view = cJSON_CreateArray();

    if(strcmp("FW",pos) == 0){
        dir = MENU_PICTURE_FIX_PATH;
    }       
    else if(strcmp("SD Card",pos) == 0){
        dir = MENU_PICTURE_SD_PATH;
    } 
    else if(strcmp("Customer",pos) == 0){
        dir = MENU_PICTURE_CUS_PATH;
    }
    else{
        dir = NULL;
    } 
    cJSON* fileList = GetPicListName(dir);   

    int size = cJSON_GetArraySize(fileList);
    for(int i=0;i<size;i++){
        cJSON* fileName = cJSON_GetArrayItem(fileList, i);
        if(cJSON_IsString(fileName))
		{
            cJSON* record = cJSON_CreateObject();

            cJSON_AddStringToObject(record, "name", fileName->valuestring);
            cJSON_AddItemToArray(view, record);
		}
    }
   
	TB_MenuDisplay(&picSelect, view, "Picture Select", para);
	cJSON_Delete(view);
	cJSON_Delete(para);
}

static void cancel_click_event(lv_event_t* e)
{
    lv_obj_t* obj = (lv_obj_t*)lv_event_get_user_data(e);
    lv_obj_del(obj);
}

static void confirm_click_event(lv_event_t* e)
{
    char* name = lv_event_get_user_data(e); 

    char bg_name[200];
    sprintf(bg_name,"%s.bmp",name);
    API_Para_Write_String(io_name,bg_name);

    TB_MenuDelete(&picSelect);    
}

lv_obj_t* Pic_preview_init(char* name)
{
    char bg_name[200];
    lv_obj_t* displaymenu = lv_obj_create(lv_scr_act());
    lv_obj_set_size(displaymenu, 490, 810);
    lv_obj_align(displaymenu, LV_ALIGN_TOP_MID, 5, -5);
    lv_obj_set_style_pad_all(displaymenu, 0, 0);
    lv_obj_clear_flag(displaymenu, LV_OBJ_FLAG_SCROLLABLE);
	int index;
	const char* MENU_CFG_DIR[] = {MENU_CONFIG_SD_PATH, MENU_CONFIG_CUS_PATH, MENU_CONFIG_FIX_PATH, NULL};
	 for(index = 0; MENU_CFG_DIR[index]; index++)
    {
        snprintf(bg_name, 200, "S:%s/%s/%s.bmp", MENU_CFG_DIR[index], INFO_RES_DIR_NAME,name);
        
        if(IsFileExist(bg_name+2))
        {
            //snprintf(menu_config_path, 500, "S:%s",buf);
            //cJSON_ReplaceItemInObject(icon,"PIC",cJSON_CreateString(menu_config_path)); 
            break;
        }
    }
	if(MENU_CFG_DIR[index]==NULL)
    		sprintf(bg_name,"S:%s/%s.bmp",cnt_dir,name);
    printf("------------  bg_name = [%s] -----------\n",bg_name);
   
    lv_obj_t* bkgd_img = lv_img_create(displaymenu);
	
    lv_obj_center(bkgd_img);
	lv_img_set_src(bkgd_img, bg_name);
	//lv_img_dsc_t * dsc = lv_canvas_get_img(bkgd_img);
	//printf("11111111:%d\n",dsc->header.cf);
	#if 0
	lv_obj_set_style_bg_img_recolor(bkgd_img, lv_color_black(), 0);
	 lv_obj_set_style_bg_img_recolor_opa(bkgd_img, LV_OPA_0, 0);
	 lv_obj_set_style_img_recolor(bkgd_img, lv_color_black(), 0);
	 lv_obj_set_style_img_recolor_opa(bkgd_img, LV_OPA_0, 0);
	lv_img_set_src(bkgd_img, bg_name);
	lv_obj_set_style_bg_img_recolor(bkgd_img, lv_color_black(), 0);
	 lv_obj_set_style_bg_img_recolor_opa(bkgd_img, LV_OPA_0, 0);
	 lv_obj_set_style_img_recolor(bkgd_img, lv_color_black(), 0);
	 lv_obj_set_style_img_recolor_opa(bkgd_img, LV_OPA_0, 0);
	 lv_obj_set_style_bg_opa(bkgd_img, LV_OPA_0, 0);
	 #endif

    lv_obj_t* pageControl = lv_obj_create(displaymenu);
    lv_obj_set_size(pageControl, 480, 100);
    lv_obj_set_style_pad_all(pageControl, 0, 0);
    lv_obj_set_y(pageControl, 700);
    set_font_size(pageControl,1);

    lv_obj_t* leftBtn = lv_btn_create(pageControl);    
    lv_obj_align(leftBtn, LV_ALIGN_LEFT_MID, 20, 0);
    lv_obj_set_size(leftBtn, 150, 80);
    lv_obj_add_event_cb(leftBtn, cancel_click_event, LV_EVENT_CLICKED, displaymenu);
    lv_obj_set_style_radius(leftBtn, LV_RADIUS_CIRCLE, 0);
    lv_obj_t* leftlabel = lv_label_create(leftBtn);
    lv_label_set_text(leftlabel, LV_VTK_BACKSPACE);
    lv_obj_center(leftlabel);
    set_font_size(leftlabel,1);

    lv_obj_t* rightBtn = lv_btn_create(pageControl);
    lv_obj_align(rightBtn, LV_ALIGN_CENTER, 150, 0);
    lv_obj_set_size(rightBtn, 150, 80);
    lv_obj_add_event_cb(rightBtn, confirm_click_event, LV_EVENT_CLICKED, name);
    lv_obj_set_style_radius(rightBtn, LV_RADIUS_CIRCLE, 0);
    lv_obj_t* rightlabel = lv_label_create(rightBtn);
    lv_label_set_text(rightlabel, LV_VTK_CONFIM);
    lv_obj_center(rightlabel);
    set_font_size(rightlabel,1);

    return displaymenu;
}