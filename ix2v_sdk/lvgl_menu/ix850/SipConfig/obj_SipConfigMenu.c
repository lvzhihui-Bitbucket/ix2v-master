#include <stdio.h>
#include "task_CallServer.h"
#include "elog_forcall.h"
#include "obj_PublicInformation.h"
#include "task_Event.h"
#include "ix850_icon.h"
#include "obj_lvgl_msg.h"
#include "table_display.h"
#include "define_file.h"
#include "obj_TableSurver.h"
#include "Common_Setting_Record.h"
#include "Common_Setting_TableView.h"
#include "obj_IoInterface.h"
#include "task_Beeper.h"
#include "Common_TableView.h"
#include "KeyboardResource.h"

#define QR_MSG_BACK 231219601
#define QR_MSG_TEST 231219602

lv_obj_t *Sip_Acc_Qr_Display(char *qr_str,char *sip_acc);

lv_obj_t *master_test_qr=NULL;
void SipConfigExt_Init(void)
{
	printf("11111111%s:%d\n",__func__,__LINE__);
	
	if(API_Para_Read_Int(SIP_ENABLE))
		API_SettingChangePage("SIPConfig");
	else
		API_SettingChangePage("DisSIPConfig");
}
static void iperf_bandwidth_menu_print(int type, void* pbuf,int len )
{
	//printf("%s\n",pbuf);
	#if 0
	int cnt=0;
	char *ch=(char *)pbuf;
	while(cnt<100)
	{
		if(ch[cnt]==0||ch[cnt]!=' ')
			break;
		cnt++;
	}
	if(ch[cnt]!=0)
		API_SettingNormalTip((char *)pbuf);
	else
		API_SettingNormalTip("Starting...");
	#endif
	if(type==0&&len>0)
	{
		bprintf("%s\n",pbuf);
		API_TIPS_Mode_Fresh(NULL, pbuf);
		//usleep(200*1000);
		MainMenu_Reset_Time();
	}
	else if(type==1)
		API_TIPS_Mode_Close(NULL);
	else
		usleep(100*1000);
}
void SipConfigProcess(int msg_type,void* data)
{
	cJSON *event;
	//bprintf("111111111 %d\n",msg_type);
	if(msg_type==SETTING_MSG_SettingTextEdit)
	{
		
		cJSON *menu_msg=(cJSON *)data;
		printf_json(menu_msg,"22222");
		if(menu_msg!=NULL)
		{
			cJSON *msg=cJSON_GetObjectItem(menu_msg,"msg");
			cJSON *input=cJSON_GetObjectItem(menu_msg,"input");
			if(msg)
			{
				if(strcmp(msg->valuestring,"SET_SIP_ACC")==0||strcmp(msg->valuestring,"SET_SIP_SER")==0)
				{
					event=cJSON_CreateObject();
					cJSON_AddStringToObject(event, EVENT_KEY_EventName,EventSipManage);
					cJSON_AddStringToObject(event, "Ctrl","re_reg");
					API_Event_Json(event);
					cJSON_Delete(event);
					API_SettingLongTip("Waitting...");
				}
				if(strcmp(msg->valuestring,"SET_SIP_EN")==0)
				{
					printf("11111111%s:%d\n",__func__,__LINE__);
	
					ui_page_exit();
					API_SettingMenuReturn();
					if(API_Para_Read_Int(SIP_ENABLE))
					{
						API_SettingReplacePage("SIPConfig");
					}
					else
					{
						API_SettingReplacePage("DisSIPConfig");
					}
					printf("11111111%s:%d\n",__func__,__LINE__);
	
					API_Event_By_Name(Event_MAT_Update);
				}
			}
		}
	}
	else if(msg_type==SETTING_MSG_SettingBtn)
	{
		//bprintf("111111111 %d\n",msg_type);
		if(master_test_qr)
		{
			TABLE_UTILITY* td = (TABLE_UTILITY*)data;
			switch(td->msg_type)
			{
				case QR_MSG_BACK:
					if(master_test_qr)
						lv_obj_del(master_test_qr);
					master_test_qr=NULL;
					break;
				case QR_MSG_TEST:
					if(master_test_qr!=NULL)
					{
						API_SettingLongTip("Waitting...");
						cJSON *call_event=cJSON_CreateObject();
						cJSON_AddStringToObject(call_event, EVENT_KEY_EventName,EventBECall);
						cJSON_AddStringToObject(call_event, "CallType","SipCall");
						cJSON_AddStringToObject(call_event, "Call_Tar_SipAcc",API_Para_Read_String2(SIP_DIVERT));
						API_Event_Json(call_event);
						cJSON_Delete(call_event);
					}
					break;
			}
			return;
		}
		if(strcmp((char *)data,"user default")==0)
		{
			event=cJSON_CreateObject();
			cJSON_AddStringToObject(event, EVENT_KEY_EventName,EventSipManage);
			cJSON_AddStringToObject(event, "Ctrl","use_default");
			API_Event_Json(event);
			cJSON_Delete(event);
			API_SettingLongTip("Waitting...");
		}
		if(strcmp((char *)data,"create sub acc")==0)
		{
			event=cJSON_CreateObject();
			cJSON_AddStringToObject(event, EVENT_KEY_EventName,EventSipManage);
			cJSON_AddStringToObject(event, "Ctrl","create_acc_by_res");
			API_Event_Json(event);
			cJSON_Delete(event);
			//API_SettingLongTip("Waitting...");
		}
		if(strcmp((char *)data,"register")==0)
		{
			event=cJSON_CreateObject();
			cJSON_AddStringToObject(event, EVENT_KEY_EventName,EventSipManage);
			cJSON_AddStringToObject(event, "Ctrl","re_reg");
			API_Event_Json(event);
			cJSON_Delete(event);
			API_SettingLongTip("Waitting...");
		}
		if(strcmp((char *)data,"Sip_Call_Test")==0)
		{
			if(master_test_qr)
				lv_obj_del(master_test_qr);
			char *qr_code=create_sip_phone_qrcode(API_Para_Read_String2(SIP_DIVERT),API_Para_Read_String2(SIP_DIV_PWD));
			master_test_qr=Sip_Acc_Qr_Display(qr_code,NULL);
			free(qr_code);
		}
		if(strcmp((char *)data,"Iperf test")==0)
		{
			API_TIPS_Mode("Iperf test start");
			if(api_iperf_client_start_json(NULL,iperf_bandwidth_menu_print)<0)
				API_TIPS_Mode_Close(NULL);
		}
		if(strcmp((char *)data,"SET_SIP_EN")==0)
		{
			printf("11111111%s:%d\n",__func__,__LINE__);

			API_Event_By_Name(Event_MAT_Update);
		}
	}
}
void SipConfig_Close(void)
{
	if(master_test_qr)
		Sip_Acc_Qr_Delete(master_test_qr);
	master_test_qr=NULL;
}

static TABLE_UTILITY *im_acc_table_display=NULL;
static cJSON *im_acc_tb=NULL;
static cJSON *im_acc_edit_record=NULL;
static cJSON *im_acc_view_filter=NULL;
RECORD_CONTROL* im_acc_record_view=NULL;
lv_obj_t *im_acc_qr_view=NULL;
lv_obj_t *im_acc_back=NULL;	
cJSON *FilterSipIMAcc(cJSON *filter)
{
	cJSON* view = cJSON_CreateArray();
	cJSON* where = cJSON_CreateObject();
	cJSON* tpl=cJSON_CreateObject();
	
    cJSON_AddStringToObject(tpl,"IX_ADDR","");
	cJSON_AddStringToObject(tpl,"Phone_Acc","");
	cJSON_AddStringToObject(tpl,"Phone_Pwd","");
	
	cJSON_AddItemToArray(view,tpl);
	
	cJSON_AddStringToObject(where, "IX_ADDR", GetSysVerInfo_bd());
	cJSON *filter_element;
	cJSON_ArrayForEach(filter_element, filter)
	{
		if(cJSON_IsNumber(filter_element))
			cJSON_AddNumberToObject(where,filter_element->string,filter_element->valueint);
		else if(cJSON_IsString(filter_element))
			cJSON_AddStringToObject(where,filter_element->string,filter_element->valuestring);
	}
	//cJSON_AddStringToObject(where, "Platform", "IX1/2");
	API_TB_SelectBySortByName(TB_NAME_TB_Sub_SIP_ACC, view, where, 0);

	cJSON_Delete(where);

	cJSON_DeleteItemFromArray(view,0);
	cJSON_ArrayForEach(filter_element, view)
	{
		
		if(judge_divert_on_by_addr(1,GetEventItemString(filter_element,"IX_ADDR")))
			cJSON_AddStringToObject(filter_element,"Divert","ON");
		else
			cJSON_AddStringToObject(filter_element,"Divert","OFF");
		
			
	}

	SortTableView(view, "IX_ADDR", 0);
	//AddUnlockCode_ToIXGIM_Tb(view);
	return view;
}
void DeleteSipIMAcc(cJSON *filter)
{
	cJSON* view = cJSON_CreateArray();
	//cJSON* where = cJSON_CreateObject();
	cJSON* tpl=cJSON_CreateObject();
	cJSON *filter_element;
	
    cJSON_AddStringToObject(tpl,"IX_ADDR","");
	//cJSON_AddStringToObject(tpl,"Phone_Acc","");
	//cJSON_AddStringToObject(tpl,"Phone_Pwd","");
	
	cJSON_AddItemToArray(view,tpl);
	API_TB_SelectBySort(filter,view,NULL,0);
	cJSON_DeleteItemFromArray(view,0);
	cJSON_ArrayForEach(filter_element, view)
	{
		API_TB_DeleteByName(TB_NAME_TB_Sub_SIP_ACC,filter_element);
		API_TB_DeleteByName(TB_NAME_TB_Divert_On,filter_element);
	}
	cJSON_Delete(view);
}
#if 1
static TB_VIWE* tb_s = NULL;
static VTK_KB_S* kb_s = NULL;

static int* IXADDR_Filter(const char* val,void *user_data)
{
	cJSON *fiter = cJSON_CreateObject();
	cJSON_AddStringToObject(fiter, "IX_ADDR", val);

	cJSON* view = FilterSipIMAcc(fiter);

	TB_MenuDisplay(&tb_s,view,"Sub acc ",NULL);
	lv_label_set_text(tb_s->filterValue,val);
	cJSON_Delete(view);
}

static void MenuSipIMAccView_Filter(const cJSON* filter)
{   
    cJSON* value = cJSON_GetObjectItem(filter,"Value");
	cJSON* name = cJSON_GetObjectItem(filter,"Name");

    char* text = cJSON_IsString(name) ? name->valuestring : NULL;
	char* val = cJSON_IsString(value) ? value->valuestring : NULL;

    Vtk_KbMode2Init(&kb_s,Vtk_KbMode2,NULL,text,20,NULL,val,"Please enter the address",IXADDR_Filter,NULL);
}
void SipAccViewAddCallback(void)
{
	if(im_acc_tb)
		cJSON_Delete(im_acc_tb);
	im_acc_tb = FilterSipIMAcc(NULL);
	TB_MenuDisplay(&tb_s,im_acc_tb,"Sub acc ",NULL);
	lv_label_set_text(tb_s->filterValue,"ALL");
	
}
static void SipAccDeleteByFiler(void* data)
{
    	DeleteSipIMAcc(tb_s->view);
	//API_TIPS_Ext_Time("Delete success",2);

	if(im_acc_tb)
	cJSON_Delete(im_acc_tb);
	im_acc_tb = FilterSipIMAcc(NULL);
	TB_MenuDisplay(&tb_s,im_acc_tb,"Sub acc ",NULL);
	lv_label_set_text(tb_s->filterValue,"All");
}
void SipAccViewFucCallback(const char* fancName)
{
	if(strcmp(fancName,"Add@Res")==0)
	{
		cJSON *event=cJSON_CreateObject();
		cJSON_AddStringToObject(event, EVENT_KEY_EventName,EventSipManage);
		cJSON_AddStringToObject(event, "Ctrl","create_acc_by_res");
		cJSON_AddNumberToObject(event,"Callback",(int)SipAccViewAddCallback);
		API_Event_Json(event);
		cJSON_Delete(event);
	}
	else if(strcmp(fancName,"Delete@Filter")==0)
	{
		API_MSGBOX("Delete?", 2, SipAccDeleteByFiler, NULL);
	}
}
void SipAccViewCellClickCallback(int line, int col)
{
	cJSON *item=cJSON_GetArrayItem(tb_s->view,line);
	if(item)
	{
		char *qr_code=create_sip_phone_qrcode(GetEventItemString(item,"Phone_Acc"),GetEventItemString(item,"Phone_Pwd"));
		im_acc_qr_view=Sip_Acc_Qr_Display(qr_code,GetEventItemString(item,"Phone_Acc"));
		free(qr_code);	
	}
}
void MenuSipIMAccView_Init(void)
{
	im_acc_tb = FilterSipIMAcc(NULL);
	cJSON *para = cJSON_CreateObject();
	int col_w[4] = {200,280,150,150};
	cJSON_AddItemToObject(para,"ColumnWidth",cJSON_CreateIntArray(col_w,4));
	cJSON_AddItemToObject(para,"HeaderEnable",cJSON_CreateBool(true));
	cJSON_AddNumberToObject(para,"CellCallback",(int)SipAccViewCellClickCallback);

	int fiterCallback = MenuSipIMAccView_Filter;
	cJSON* fiter = cJSON_CreateObject();
    cJSON_AddStringToObject(fiter, "Name", "IX_ADDR");
    cJSON_AddStringToObject(fiter, "Value", "");
    cJSON_AddNumberToObject(fiter, "Callback", fiterCallback);
    cJSON_AddItemToObject(para,"Filter",fiter);

	cJSON* func = cJSON_CreateObject();
	cJSON* funcName = cJSON_AddArrayToObject(func, "Name");
	cJSON_AddItemToArray(funcName, cJSON_CreateString("Add@Res"));
	cJSON_AddItemToArray(funcName, cJSON_CreateString("Delete@Filter"));
	cJSON_AddNumberToObject(func, "Callback", (int)SipAccViewFucCallback);
	cJSON_AddItemToObject(para, "Function", func);

	TB_MenuDisplay(&tb_s,im_acc_tb,"Sub acc ",para);
	lv_label_set_text(tb_s->filterValue,"All");

	cJSON_Delete(para);
}

#else
void MenuSipIMAccView_Init(void)
{
	im_acc_back=lv_scr_act();
	im_acc_tb = FilterSipIMAcc(NULL);
	#if 0
	cJSON *IXGIMEditCheck=NULL;
	cJSON *IXGIMEditTranslate=NULL;
	
	if(IXGIMEditDesc==NULL)
	{
		IXGIMEditDesc=cJSON_CreateObject();
		IXGIMEditCheck=GetJsonFromFile(IXGIMEditCheckFile);
		if(IXGIMEditCheck)
			cJSON_AddItemToObject(IXGIMEditDesc,"Check",IXGIMEditCheck);
		IXGIMEditTranslate=GetJsonFromFile(IXGIMEditTransFile);
		if(IXGIMEditTranslate)
			cJSON_AddItemToObject(IXGIMEditDesc,"Translate",IXGIMEditTranslate);
		//IXGIMEditTranslate=cJSON_AddObjectToObject(IXGIMEditDesc,"Translate");
		//cJSON_AddStringToObject(IXGIMEditTranslate,"IXG_ID","IXG_BDU_ID");
		//cJSON_AddStringToObject(IXGIMEditTranslate,"IXG_IM_ID","IM_ID");
		
		
	}
	#endif
	cJSON *para=cJSON_CreateObject();
	int col_w[4]={200,280,150,150};
	cJSON_AddItemToObject(para,"col_w",cJSON_CreateIntArray(col_w,4));
	im_acc_table_display=create_utility_table_display_menu(im_acc_tb,"Sub acc","IX_ADDR",para);
	//im_acc_table_display=create_table_display_menu(im_acc_tb,1,NULL);
	//printf("11111111111%s:%d\n",__func__,__LINE__);

}
#endif
void MenuSipIMAccView_Process(int msg_type,void* arg)
{
	//�յ����ذ������£�����popDisplayLastMenu��������һ��˵�
	//popDisplayLastMenu();
	//�յ�ĳһ�����ݱ�������Ϣ��׼�����ݣ�����SwitchOneMenu�л�������IXG�ֻ��༭�˵�
	//SwitchOneMenu(MenuOneIXGImEdit,arg,1);
	//printf("111111111MenuIXGTbView_Process msg:%d\n",msg_type);
	cJSON *edit_able;
	cJSON *func;
	cJSON *where;
	
	switch(msg_type)
	{
		#if 0
		case IXG_MSG_ADD_RECORD:
		
			//if(editor)
				//lv_obj_del(editor);
			if(edit_record!=NULL)
				cJSON_Delete(edit_record);
			edit_record=Create_New_IXGIM_Item();
			edit_able=Create_IXGIM_Editable(1);
			
			//editor=create_editor_menu(edit_record,edit_able,IXGIMEditCheck);
			create_editor_menu(edit_record,edit_able,IXGIMEditDesc);
			cJSON_Delete(edit_able);
			add_new_item=1;
			break;
		#endif	
		case TABLE_MSG_EDIT_RECORD:
			//printf_json((cJSON*)arg,__func__);
			if(im_acc_edit_record!=NULL)
				cJSON_Delete(im_acc_edit_record);
			im_acc_edit_record=cJSON_Duplicate((cJSON*)arg,1);
			//if(editor)
				//lv_obj_del(editor);
			edit_able=cJSON_CreateArray();
			func=cJSON_CreateObject();
			cJSON_AddStringToObject(func,"CB_FOP_BTN_1","Back");
			cJSON_AddStringToObject(func,"CB_FOP_BTN_2","QR");
			//editor=create_editor_menu(edit_record,edit_able,IXGIMEditCheck);
			im_acc_record_view=common_record_editor_menu(im_acc_edit_record,edit_able,NULL,func);
			//create_editor_menu(im_acc_edit_record,edit_able,NULL);
			cJSON_Delete(edit_able);
			cJSON_Delete(func);
			//add_new_item=0;
			break;
		case RECORD_FUN_1:
			if(im_acc_record_view)
			{
				lv_disp_load_scr(im_acc_record_view->back_scr);
				delete_recodr(im_acc_record_view);
				im_acc_record_view=NULL;
			}
			break;
		case RECORD_FUN_2:
			if(im_acc_edit_record!=NULL)
			{
				char *qr_code=create_sip_phone_qrcode(GetEventItemString(im_acc_edit_record,"Phone_Acc"),GetEventItemString(im_acc_edit_record,"Phone_Pwd"));
				im_acc_qr_view=Sip_Acc_Qr_Display(qr_code,NULL);
				free(qr_code);
			}
			break;
		case QR_MSG_BACK:
			if(im_acc_qr_view)
				lv_obj_del(im_acc_qr_view);
			im_acc_qr_view=NULL;
			break;
		case QR_MSG_TEST:
			if(im_acc_edit_record!=NULL)
			{
				API_SettingLongTip("Waitting...");
				cJSON *call_event=cJSON_CreateObject();
				cJSON_AddStringToObject(call_event, EVENT_KEY_EventName,EventBECall);
				cJSON_AddStringToObject(call_event, "CallType","SipCall");
				cJSON_AddStringToObject(call_event, "Call_Tar_SipAcc",GetEventItemString(im_acc_edit_record,"Phone_Acc"));
				API_Event_Json(call_event);
				cJSON_Delete(call_event);
			}
			break;
		case TABLE_MSG_FILTER:
			if(im_acc_table_display)
			{
				cJSON_Delete(im_acc_tb);
				where=cJSON_CreateObject();
				cJSON_AddStringToObject(where,"IX_ADDR",arg);
				im_acc_tb=FilterSipIMAcc(where);	
				cJSON_Delete(where);
				utility_table_fresh(im_acc_table_display,im_acc_tb);
			}
			break;
		case TABLE_MSG_UNFILTER:
			if(im_acc_table_display)
			{
				cJSON_Delete(im_acc_tb);
				
				im_acc_tb=FilterSipIMAcc(NULL);	
				
				utility_table_fresh(im_acc_table_display,im_acc_tb);
			}
			break;	
		#if 0	
		case IXG_MSG_CHANGE_FITER:
			if(view_filter)
				cJSON_Delete(view_filter);
			if(view_tb)
				cJSON_Delete(view_tb);
			printf_json((cJSON*)arg,"FILTER");
			view_filter=cJSON_Duplicate((cJSON*)arg,1);
			view_tb=FilterIXGIm_From_R8001((cJSON*)arg);
			ixgview_data_fresh(table_display,view_tb,1);
			break;
		#endif
		case TABLE_MSG_EXIT:
			//MenuSipIMAccView_Close();
			//ui_page_exit();
			lv_msg_send(SETTING_MSG_SettingReturn,NULL);
			break;
		#if 0	
		case RECORD_MSG_SAVE_EXIT:
			if(add_new_item)
			{
				if(Judge_IXGIM_Exist((cJSON*)arg)>0)
				{
					API_TIPS("The device is aleady exist!");
					return;
				}
			}
			printf_json((cJSON*)arg,"SAVE_EXIT");
			if(view_tb)
				cJSON_Delete(view_tb);
			update_one_ixgim_to_R8001tb((cJSON*)arg);
			update_one_unlock_code_to_Privatetb((cJSON*)arg);
			view_tb=FilterIXGIm_From_R8001(view_filter);
			ixgview_data_fresh(table_display,view_tb,1);
			break;
		case RECORD_MSG_DEL_EXIT:
			if(add_new_item)
			{
			
				//if(Judge_IXGIM_Exist((cJSON*)arg))>0)
				{
					//API_TIPS("The device is aleady exist!");
					return;
				}
			}
			if(view_tb)
				cJSON_Delete(view_tb);
			delete_one_ixgim_to_R8001tb((cJSON*)arg);
			view_tb=FilterIXGIm_From_R8001(view_filter);
			ixgview_data_fresh(table_display,view_tb,1);
			break;	
		#endif
			
	}
}

void MenuSipIMAccView_Close(void)
{
	//�˵��ر�ʱ�������ñ�༭�����ٺ���
	//�ͷű�����
	//if(editor)
		//lv_obj_del(editor);
	//editor=NULL;

	printf("11111111111%s:%d\n",__func__,__LINE__);	
	if(im_acc_qr_view)
		Sip_Acc_Qr_Delete(im_acc_qr_view);
	im_acc_qr_view=NULL;

	if(kb_s){
		Vtk_KbReleaseByMode(&kb_s);
		kb_s = NULL;
	}
	if(tb_s){
		TB_MenuDelete(&tb_s);
		tb_s = NULL;
	}

	if(im_acc_back&&im_acc_back!=lv_scr_act())
		lv_scr_load(im_acc_back);
	//bprintf("11111 %08x,%08x,%08x\n",im_acc_qr_view,im_acc_record_view,im_acc_table_display);
	
	//if(im_acc_qr_view)
	//	lv_obj_del(im_acc_qr_view);
	//im_acc_qr_view=NULL;
//bprintf("11111 %08x,%08x,%08x\n",im_acc_qr_view,im_acc_record_view,im_acc_table_display);
	if(im_acc_record_view!=NULL)
		delete_recodr(im_acc_record_view);
	im_acc_record_view=NULL;
//bprintf("11111 %08x,%08x,%08x\n",im_acc_qr_view,im_acc_record_view,im_acc_table_display);	
	if(im_acc_table_display!=NULL)
		delete_table(im_acc_table_display);
	im_acc_table_display=NULL;
//bprintf("11111 %08x,%08x,%08x\n",im_acc_qr_view,im_acc_record_view,im_acc_table_display);
	
	
	//printf("11111111111%s:%d\n",__func__,__LINE__);
	if(im_acc_tb)
		cJSON_Delete(im_acc_tb);
	
	im_acc_tb=NULL;
//printf("11111111111%s:%d\n",__func__,__LINE__);
	if(im_acc_edit_record!=NULL)
		cJSON_Delete(im_acc_edit_record);
	im_acc_edit_record=NULL;
//printf("11111111111%s:%d\n",__func__,__LINE__);
	if(im_acc_view_filter)
		cJSON_Delete(im_acc_view_filter);
	im_acc_view_filter=NULL;
//printf("11111111111%s:%d\n",__func__,__LINE__);	
	#if 0
	if(IXGIMEditDesc)
		cJSON_Delete(IXGIMEditDesc);
	IXGIMEditDesc=NULL;	
	#endif
	im_acc_table_display=NULL;
	//printf("11111111111%s:%d\n",__func__,__LINE__);
}

void API_MenuSipIMAccView_Process(void* arg)
{
	TABLE_UTILITY* td = (TABLE_UTILITY*)arg;
	MenuSipIMAccView_Process(td->msg_type,td->msg_data);
}


////////////////////////////////////
cJSON *FilterSipCallLogTb(cJSON *filter)
{
	cJSON* view = cJSON_CreateArray();
	cJSON* where = cJSON_CreateObject();
	#if 0
	cJSON* tpl=cJSON_CreateObject();
	
    cJSON_AddStringToObject(tpl,"IX_ADDR","");
	cJSON_AddStringToObject(tpl,"Phone_Acc","");
	cJSON_AddStringToObject(tpl,"Phone_Pwd","");
	
	cJSON_AddItemToArray(view,tpl);
	
	cJSON_AddStringToObject(where, "IX_ADDR", GetSysVerInfo_bd());
	#endif
	cJSON *filter_element;
	cJSON_ArrayForEach(filter_element, filter)
	{
		if(cJSON_IsNumber(filter_element))
			cJSON_AddNumberToObject(where,filter_element->string,filter_element->valueint);
		else if(cJSON_IsString(filter_element))
			cJSON_AddStringToObject(where,filter_element->string,filter_element->valuestring);
	}
	//cJSON_AddStringToObject(where, "Platform", "IX1/2");
	API_TB_SelectBySortByName(TB_NAME_SipCallLog, view, where, 0);

	cJSON_Delete(where);
	SortTableView(view, "TIME", 1);
	//cJSON_DeleteItemFromArray(view,0);
	

	//SortTableView(view, "IX_ADDR", 0);
	//AddUnlockCode_ToIXGIM_Tb(view);
	return view;
}
void DeleteCallLog(cJSON *filter)
{
	cJSON* view = cJSON_CreateArray();
	//cJSON* where = cJSON_CreateObject();
	cJSON* tpl=cJSON_CreateObject();
	cJSON *filter_element;
	
    cJSON_AddStringToObject(tpl,"IX_ADDR","");
	cJSON_AddStringToObject(tpl,"TIME","");
	
	//cJSON_AddStringToObject(tpl,"Phone_Acc","");
	//cJSON_AddStringToObject(tpl,"Phone_Pwd","");
	
	cJSON_AddItemToArray(view,tpl);
	API_TB_SelectBySort(filter,view,NULL,0);
	cJSON_DeleteItemFromArray(view,0);
	cJSON_ArrayForEach(filter_element, view)
	{
		API_TB_DeleteByName(TB_NAME_SipCallLog,filter_element);
		//API_TB_DeleteByName(TB_NAME_TB_Divert_On,filter_element);
	}
	cJSON_Delete(view);
}

static TABLE_UTILITY *sip_call_log_table_display=NULL;
static cJSON *sip_call_log_tb=NULL;
static cJSON *sip_call_log_edit_record=NULL;
static cJSON *sip_call_log_view_filter=NULL;
static RECORD_CONTROL*sip_call_log_record_view=NULL;
lv_obj_t *sip_call_log_qr_view=NULL;

#if 1
static TB_VIWE* tb = NULL;
static VTK_KB_S* kb = NULL;
static void SipCallLogDeleteByFilter(void* data)
{
    	DeleteCallLog(tb->view);
	//API_TIPS_Ext_Time("Delete success",2);

	if(sip_call_log_tb)
		cJSON_Delete(sip_call_log_tb);
	sip_call_log_tb = FilterSipCallLogTb(NULL);
	TB_MenuDisplay(&tb,sip_call_log_tb,"Sip call log  ",NULL);
	lv_label_set_text(tb->filterValue,"ALL");
}
void SipCallLogFucCallback(const char* fancName)
{
	if(strcmp(fancName,"Delete@Filter")==0)
	{
		
		API_MSGBOX("Delete?", 2, SipCallLogDeleteByFilter, NULL);
	}
}

static int* IXADDR_Log_Filter(const char* val,void *user_data)
{
	cJSON *fiter = cJSON_CreateObject();
	cJSON_AddStringToObject(fiter, "IX_ADDR", val);

	cJSON* view = FilterSipCallLogTb(fiter);

	TB_MenuDisplay(&tb,view,"Sub acc ",NULL);
	lv_label_set_text(tb->filterValue,val);
	cJSON_Delete(view);
}

static void MenuSipCallLogView_Filter(const cJSON* filter)
{   
    cJSON* value = cJSON_GetObjectItem(filter,"Value");
	cJSON* name = cJSON_GetObjectItem(filter,"Name");

    char* text = cJSON_IsString(name) ? name->valuestring : NULL;
	char* val = cJSON_IsString(value) ? value->valuestring : NULL;

    Vtk_KbMode2Init(&kb,Vtk_KbMode2,NULL,text,20,NULL,val,"Please enter the address",IXADDR_Log_Filter,NULL);
}

void MenuSipCallLogView_Init(void)
{
	sip_call_log_tb = FilterSipCallLogTb(NULL);

	cJSON* para = cJSON_CreateObject();
	int col_w[4] = {240,160,280,150};
	cJSON_AddItemToObject(para,"ColumnWidth",cJSON_CreateIntArray(col_w,4));
	cJSON_AddItemToObject(para,"HeaderEnable",cJSON_CreateBool(true));
	cJSON_AddItemToObject(para,"CellCallback",cJSON_CreateNumber(0));

	int fiterCallback = MenuSipCallLogView_Filter;
	cJSON* fiter = cJSON_CreateObject();
    cJSON_AddStringToObject(fiter, "Name", "IX_ADDR");
    cJSON_AddStringToObject(fiter, "Value", "");
    cJSON_AddNumberToObject(fiter, "Callback", fiterCallback);
    cJSON_AddItemToObject(para,"Filter",fiter);

	cJSON* func = cJSON_CreateObject();
	cJSON* funcName = cJSON_AddArrayToObject(func, "Name");
	cJSON_AddItemToArray(funcName, cJSON_CreateString("Delete@Filter"));
	cJSON_AddNumberToObject(func, "Callback", (int)SipCallLogFucCallback);
	cJSON_AddItemToObject(para, "Function", func);

	TB_MenuDisplay(&tb,sip_call_log_tb,"Sip call log ",para);
	lv_label_set_text(tb->filterValue,"ALL");
	cJSON_Delete(para);
}
#else
void MenuSipCallLogView_Init(void)
{
	sip_call_log_tb=FilterSipCallLogTb(NULL);
	#if 0
	cJSON *IXGIMEditCheck=NULL;
	cJSON *IXGIMEditTranslate=NULL;
	
	if(IXGIMEditDesc==NULL)
	{
		IXGIMEditDesc=cJSON_CreateObject();
		IXGIMEditCheck=GetJsonFromFile(IXGIMEditCheckFile);
		if(IXGIMEditCheck)
			cJSON_AddItemToObject(IXGIMEditDesc,"Check",IXGIMEditCheck);
		IXGIMEditTranslate=GetJsonFromFile(IXGIMEditTransFile);
		if(IXGIMEditTranslate)
			cJSON_AddItemToObject(IXGIMEditDesc,"Translate",IXGIMEditTranslate);
		//IXGIMEditTranslate=cJSON_AddObjectToObject(IXGIMEditDesc,"Translate");
		//cJSON_AddStringToObject(IXGIMEditTranslate,"IXG_ID","IXG_BDU_ID");
		//cJSON_AddStringToObject(IXGIMEditTranslate,"IXG_IM_ID","IM_ID");
		
		
	}
	#endif
	cJSON *para=cJSON_CreateObject();
	int col_w[4]={220,160,280,150};
	cJSON_AddItemToObject(para,"col_w",cJSON_CreateIntArray(col_w,4));
	sip_call_log_table_display=create_utility_table_display_menu(sip_call_log_tb,"Sip call log","TAR_ACC",para);
	//sip_call_log_table_display=create_table_display_menu(sip_call_log_tb,1,NULL);
	//printf("11111111111%s:%d\n",__func__,__LINE__);

}
#endif
void MenuSipCallLogView_Process(int msg_type,void* arg)
{
	//�յ����ذ������£�����popDisplayLastMenu��������һ��˵�
	//popDisplayLastMenu();
	//�յ�ĳһ�����ݱ�������Ϣ��׼�����ݣ�����SwitchOneMenu�л�������IXG�ֻ��༭�˵�
	//SwitchOneMenu(MenuOneIXGImEdit,arg,1);
	//printf("111111111MenuIXGTbView_Process msg:%d\n",msg_type);
	cJSON *edit_able;
	cJSON *func;
	cJSON *where;
	
	switch(msg_type)
	{
		case TABLE_MSG_EDIT_RECORD:
			//printf_json((cJSON*)arg,__func__);
			if(sip_call_log_edit_record!=NULL)
				cJSON_Delete(sip_call_log_edit_record);
			sip_call_log_edit_record=cJSON_Duplicate((cJSON*)arg,1);
			//if(editor)
				//lv_obj_del(editor);
			edit_able=cJSON_CreateArray();
			func=cJSON_CreateObject();
			cJSON_AddStringToObject(func,"CB_FOP_BTN_1","Back");
			cJSON_AddStringToObject(func,"CB_FOP_BTN_2","QR");
			cJSON_AddStringToObject(func,"CB_FOP_BTN_3","DEL");
			//editor=create_editor_menu(edit_record,edit_able,IXGIMEditCheck);
			sip_call_log_record_view=common_record_editor_menu(sip_call_log_edit_record,edit_able,NULL,func);
			//create_editor_menu(im_acc_edit_record,edit_able,NULL);
			cJSON_Delete(edit_able);
			cJSON_Delete(func);
			//add_new_item=0;
			break;
		case TABLE_MSG_FILTER:
			if(sip_call_log_table_display)
			{
				cJSON_Delete(sip_call_log_tb);
				sip_call_log_view_filter=cJSON_CreateObject();
				cJSON_AddStringToObject(sip_call_log_view_filter,"TAR_ACC",arg);
				sip_call_log_tb=FilterSipCallLogTb(sip_call_log_view_filter);	
				//cJSON_Delete(where);
				utility_table_fresh(sip_call_log_table_display,sip_call_log_tb);
			}
			break;
		case TABLE_MSG_UNFILTER:
			if(sip_call_log_table_display)
			{
				if(sip_call_log_view_filter)
					cJSON_Delete(sip_call_log_view_filter);
				sip_call_log_view_filter=NULL;
				cJSON_Delete(sip_call_log_tb);
				//where=cJSON_CreateObject();
				//cJSON_AddStringToObject(where,"TAR_ACC",arg);
				sip_call_log_tb=FilterSipCallLogTb(NULL);	
				//cJSON_Delete(where);
				utility_table_fresh(sip_call_log_table_display,sip_call_log_tb);
			}
			break;	
		case RECORD_FUN_1:
			if(sip_call_log_record_view)
			{
				lv_disp_load_scr(sip_call_log_record_view->back_scr);
				delete_recodr(sip_call_log_record_view);
				sip_call_log_record_view=NULL;
			}
			break;
		case RECORD_FUN_2:
			
			#if 1
			if(sip_call_log_edit_record!=NULL)
			{
			
				where=cJSON_CreateObject();
				cJSON_AddStringToObject(where,"Phone_Acc",GetEventItemString(sip_call_log_edit_record,"TAR_ACC"));
				cJSON *sip_view=cJSON_CreateArray();
				if(API_TB_SelectBySortByName(TB_NAME_TB_Sub_SIP_ACC, sip_view, where, 0)==0)
				{
					API_SettingNormalTip("Get QR error");
				}
				else
				{
					char *qr_code=create_sip_phone_qrcode(GetEventItemString(cJSON_GetArrayItem(sip_view,0),"Phone_Acc"),GetEventItemString(cJSON_GetArrayItem(sip_view,0),"Phone_Pwd"));
					sip_call_log_qr_view=Sip_Acc_Qr_Display(qr_code,NULL);
					free(qr_code);
				}
				cJSON_Delete(where);
				cJSON_Delete(sip_view);
			}
			#endif
			break;
		case RECORD_FUN_3:
			if(sip_call_log_edit_record&&sip_call_log_table_display)
			{
				API_TB_DeleteByName(TB_NAME_SipCallLog, sip_call_log_edit_record);
				cJSON_Delete(sip_call_log_edit_record);
				sip_call_log_edit_record=NULL;
				cJSON_Delete(sip_call_log_tb);
				//where=cJSON_CreateObject();
				//cJSON_AddStringToObject(where,"TAR_ACC",arg);
				sip_call_log_tb=FilterSipCallLogTb(sip_call_log_view_filter);	
				//cJSON_Delete(where);
				utility_table_fresh(sip_call_log_table_display,sip_call_log_tb);

				lv_disp_load_scr(sip_call_log_record_view->back_scr);
				delete_recodr(sip_call_log_record_view);
				sip_call_log_record_view=NULL;
			}
			break;
		case TABLE_MSG_EXIT:
			//MenuSipIMAccView_Close();
			//ui_page_exit();
			lv_msg_send(SETTING_MSG_SettingReturn,NULL);
			break;	
		#if 1
		case QR_MSG_BACK:
			if(sip_call_log_qr_view)
				lv_obj_del(sip_call_log_qr_view);
			sip_call_log_qr_view=NULL;
			break;
		case QR_MSG_TEST:
			if(sip_call_log_edit_record!=NULL)
			{
				API_SettingLongTip("Waitting...");
				cJSON *call_event=cJSON_CreateObject();
				cJSON_AddStringToObject(call_event, EVENT_KEY_EventName,EventBECall);
				cJSON_AddStringToObject(call_event, "CallType","SipCall");
				cJSON_AddStringToObject(call_event, "Call_Tar_SipAcc",GetEventItemString(sip_call_log_edit_record,"TAR_ACC"));
				API_Event_Json(call_event);
				cJSON_Delete(call_event);
			}
			break;
		#endif
		#if 0
		case IXG_MSG_ADD_RECORD:
		
			//if(editor)
				//lv_obj_del(editor);
			if(edit_record!=NULL)
				cJSON_Delete(edit_record);
			edit_record=Create_New_IXGIM_Item();
			edit_able=Create_IXGIM_Editable(1);
			
			//editor=create_editor_menu(edit_record,edit_able,IXGIMEditCheck);
			create_editor_menu(edit_record,edit_able,IXGIMEditDesc);
			cJSON_Delete(edit_able);
			add_new_item=1;
			break;
		#endif	
		case IXG_MSG_EDIT_RECORD:
			//printf_json((cJSON*)arg,__func__);
			if(sip_call_log_edit_record!=NULL)
				cJSON_Delete(sip_call_log_edit_record);
			sip_call_log_edit_record=cJSON_Duplicate((cJSON*)arg,1);
			//if(editor)
				//lv_obj_del(editor);
			edit_able=cJSON_CreateArray();;
			
			//editor=create_editor_menu(edit_record,edit_able,IXGIMEditCheck);
			create_editor_menu(sip_call_log_edit_record,edit_able,NULL);
			cJSON_Delete(edit_able);
			//add_new_item=0;
			break;
		#if 0	
		case IXG_MSG_CHANGE_FITER:
			if(view_filter)
				cJSON_Delete(view_filter);
			if(view_tb)
				cJSON_Delete(view_tb);
			printf_json((cJSON*)arg,"FILTER");
			view_filter=cJSON_Duplicate((cJSON*)arg,1);
			view_tb=FilterIXGIm_From_R8001((cJSON*)arg);
			ixgview_data_fresh(table_display,view_tb,1);
			break;
		#endif
		case IXG_MSG_EXIT:
			MenuSipCallLogView_Close();
			break;
		#if 0	
		case RECORD_MSG_SAVE_EXIT:
			if(add_new_item)
			{
				if(Judge_IXGIM_Exist((cJSON*)arg)>0)
				{
					API_TIPS("The device is aleady exist!");
					return;
				}
			}
			printf_json((cJSON*)arg,"SAVE_EXIT");
			if(view_tb)
				cJSON_Delete(view_tb);
			update_one_ixgim_to_R8001tb((cJSON*)arg);
			update_one_unlock_code_to_Privatetb((cJSON*)arg);
			view_tb=FilterIXGIm_From_R8001(view_filter);
			ixgview_data_fresh(table_display,view_tb,1);
			break;
		case RECORD_MSG_DEL_EXIT:
			if(add_new_item)
			{
			
				//if(Judge_IXGIM_Exist((cJSON*)arg))>0)
				{
					//API_TIPS("The device is aleady exist!");
					return;
				}
			}
			if(view_tb)
				cJSON_Delete(view_tb);
			delete_one_ixgim_to_R8001tb((cJSON*)arg);
			view_tb=FilterIXGIm_From_R8001(view_filter);
			ixgview_data_fresh(table_display,view_tb,1);
			break;	
		#endif
			
	}
}

void MenuSipCallLogView_Close(void)
{
	//�˵��ر�ʱ�������ñ�༭�����ٺ���
	//�ͷű�����
	//if(editor)
		//lv_obj_del(editor);
	//editor=NULL;

	if(kb)
	{
		Vtk_KbReleaseByMode(&kb);
		kb = NULL;
	}
	if(tb)
	{
		TB_MenuDelete(&tb);
		tb = NULL;
	}

	if(sip_call_log_qr_view)
		lv_obj_del(sip_call_log_qr_view);
	sip_call_log_qr_view=NULL;
			
	if(sip_call_log_table_display!=NULL)
		delete_table(sip_call_log_table_display);
	sip_call_log_table_display=NULL;
	
	if(sip_call_log_record_view!=NULL)
		delete_recodr(sip_call_log_record_view);
	sip_call_log_record_view=NULL;
	//printf("11111111111%s:%d\n",__func__,__LINE__);
	if(sip_call_log_tb)
		cJSON_Delete(sip_call_log_tb);
	
	sip_call_log_tb=NULL;
//printf("11111111111%s:%d\n",__func__,__LINE__);
	if(sip_call_log_edit_record!=NULL)
		cJSON_Delete(sip_call_log_edit_record);
	sip_call_log_edit_record=NULL;
//printf("11111111111%s:%d\n",__func__,__LINE__);
	if(sip_call_log_view_filter)
		cJSON_Delete(sip_call_log_view_filter);
	sip_call_log_view_filter=NULL;
//printf("11111111111%s:%d\n",__func__,__LINE__);	
	#if 0
	if(IXGIMEditDesc)
		cJSON_Delete(IXGIMEditDesc);
	IXGIMEditDesc=NULL;	
	#endif
	sip_call_log_table_display=NULL;
	//printf("11111111111%s:%d\n",__func__,__LINE__);
}

void API_MenuSipCallLogView_Process(void* arg)
{
	TABLE_UTILITY* td = (TABLE_DISPLAY*)arg;
	MenuSipCallLogView_Process(td->msg_type,td->msg_data);
}
static void qr_back_event(lv_event_t* e)
{
	TABLE_DISPLAY td ;
	lv_obj_t *rm_divert_save = lv_event_get_user_data(e);
	lv_disp_load_scr(rm_divert_save);
    	 td.msg_data =  NULL;
    td.msg_type = QR_MSG_BACK;
    API_SettingMenuProcess(&td); 
}
static void qr_test_event(lv_event_t* e)
{
	char *sip_acc = lv_event_get_user_data(e);
	if(sip_acc==NULL)
	{
		TABLE_DISPLAY td ;
	    	 td.msg_data =  NULL;
	    td.msg_type = QR_MSG_TEST;
	    API_SettingMenuProcess(&td); 
	}
	else
	{
		API_SettingLongTip("Waitting...");
		cJSON *call_event=cJSON_CreateObject();
		cJSON_AddStringToObject(call_event, EVENT_KEY_EventName,EventBECall);
		cJSON_AddStringToObject(call_event, "CallType","SipCall");
		cJSON_AddStringToObject(call_event, "Call_Tar_SipAcc",sip_acc);
		API_Event_Json(call_event);
		cJSON_Delete(call_event);
	}
}
static lv_obj_t *rm_divert_save=NULL;
lv_obj_t *Sip_Acc_Qr_Display(char *qr_str,char *sip_acc)
{
	lv_obj_t *rm_divert=NULL;
	int i;
	
	if(rm_divert==NULL)
	 	rm_divert = lv_obj_create(NULL);
	rm_divert_save=lv_scr_act();   
	 set_menu_with_video_off(); 
   
	
    lv_obj_remove_style_all(rm_divert);
    lv_obj_set_size(rm_divert,  lv_disp_get_hor_res(NULL), lv_disp_get_ver_res(NULL));
    lv_obj_align(rm_divert, LV_ALIGN_TOP_LEFT, 0, 0);
   // lv_obj_set_style_bg_color(ui_qrcode, lv_palette_main(LV_PALETTE_LIGHT_BLUE), 0);//lv_color_hex(0x102030)
    lv_obj_set_style_bg_color(rm_divert, lv_color_hex(0x41454D), 0);
    lv_obj_set_style_bg_opa(rm_divert, LV_OPA_100, 0);
	 lv_obj_set_style_text_font(rm_divert, &lv_font_montserrat_26, 0);
	 lv_obj_set_style_text_color(rm_divert, lv_color_white(), 0);

	  lv_disp_load_scr(rm_divert);
	  
	///////////////////////////////
	  lv_color_t qr_bg_color=lv_color_make(0xff, 0xff, 0xef); // lv_palette_lighten(LV_PALETTE_LIGHT_BLUE, 5);
	lv_color_t qr_fg_color=lv_color_make(0x00, 0x00, 0x10); //lv_palette_darken(LV_PALETTE_BLUE, 4);
   lv_obj_t* qr = lv_qrcode_create(rm_divert, 380, qr_fg_color, qr_bg_color);

    /*Set data*/
    lv_qrcode_update(qr, qr_str, strlen(qr_str));
    //lv_obj_center(qr);
    lv_obj_align(qr, LV_ALIGN_TOP_MID, 0, 80);
	
    /*Add a border with bg_color*/
    lv_obj_set_style_border_color(qr, qr_bg_color, 0);
    lv_obj_set_style_border_width(qr, 5, 0);

	//////////////////////////
	static lv_style_t style_btn;
	 lv_style_init(&style_btn);
    lv_style_set_text_font(&style_btn,&lv_font_montserrat_32);
    lv_style_set_radius(&style_btn,LV_RADIUS_CIRCLE);
    lv_style_set_bg_color(&style_btn,lv_color_make(230, 231, 232));
    lv_style_set_text_color(&style_btn,lv_color_hex(0xff0000));
	
	lv_obj_t *ui_qrcode_btn=lv_btn_create(rm_divert);
	   lv_obj_add_style(ui_qrcode_btn,&style_btn,0);
        lv_obj_set_size(ui_qrcode_btn,120,70);
	 //lv_obj_set_style_bg_color(ui_qrcode_btn, lv_color_hex(0x41454D), 0);
	    //lv_obj_align(ui_qrcode_btn, LV_ALIGN_CENTER,0,0);
		//lv_obj_set_pos(ui_qrcode_btn, 550, 155+3*60);
		 lv_obj_align(ui_qrcode_btn, LV_ALIGN_BOTTOM_LEFT, 20, -20);
		//lv_obj_set_style_border_width(ui_qrcode_btn, 4, 0);
		//lv_obj_set_style_border_color(ui_qrcode_btn,lv_color_white(),0);
		//lv_obj_set_style_border_opa(ui_qrcode_btn,LV_OPA_100, 0);
	    lv_obj_add_event_cb(ui_qrcode_btn, qr_back_event, LV_EVENT_CLICKED, rm_divert_save);

	    lv_obj_t* ui_Label = lv_label_create(ui_qrcode_btn);
		//lv_obj_set_size(ui_Label, 100, 50);
	    lv_obj_align(ui_Label, LV_ALIGN_CENTER, 0, 0);

	    lv_label_set_text(ui_Label, "Back");

		lv_obj_t *ui_qrcode_btn2=lv_btn_create(rm_divert);
	   lv_obj_add_style(ui_qrcode_btn2,&style_btn,0);
        lv_obj_set_size(ui_qrcode_btn2,120,70);
	 //lv_obj_set_style_bg_color(ui_qrcode_btn, lv_color_hex(0x41454D), 0);
	    //lv_obj_align(ui_qrcode_btn, LV_ALIGN_CENTER,0,0);
		//lv_obj_set_pos(ui_qrcode_btn, 550, 155+3*60);
		 lv_obj_align(ui_qrcode_btn2, LV_ALIGN_BOTTOM_RIGHT, -20, -20);
		//lv_obj_set_style_border_width(ui_qrcode_btn, 4, 0);
		//lv_obj_set_style_border_color(ui_qrcode_btn,lv_color_white(),0);
		//lv_obj_set_style_border_opa(ui_qrcode_btn,LV_OPA_100, 0);
	    lv_obj_add_event_cb(ui_qrcode_btn2, qr_test_event, LV_EVENT_CLICKED, sip_acc);

	    lv_obj_t* ui_Label2 = lv_label_create(ui_qrcode_btn2);
		//lv_obj_set_size(ui_Label, 100, 50);
	    lv_obj_align(ui_Label2, LV_ALIGN_CENTER, 0, 0);

	    lv_label_set_text(ui_Label2, "Test");

	return rm_divert;
}
void Sip_Acc_Qr_Delete(lv_obj_t *qr_menu)
{
	lv_disp_load_scr(rm_divert_save);
	lv_obj_del(qr_menu);
}
static TABLE_UTILITY *call_record_table_display=NULL;
static cJSON *call_record_tb=NULL;
static cJSON *call_record_edit_record=NULL;
static cJSON *call_record_view_filter=NULL;
RECORD_CONTROL* call_record_view=NULL;
//lv_obj_t *im_acc_qr_view=NULL;
lv_obj_t	 *call_record_back=NULL;	
static lv_obj_t	 *playback_menu=NULL;	
#define Playback_MSG_BACK 240105601
#define Playback_MSG_REPLAY 240111601

lv_obj_t *PlaybackMenu_init(void);

cJSON *FilterCallRecordTb(cJSON *filter)
{
	cJSON* view = cJSON_CreateArray();
	cJSON* where = cJSON_CreateObject();
	
	cJSON *filter_element;
	#if 1
	cJSON* tpl=cJSON_CreateObject();
	
    cJSON_AddStringToObject(tpl,"Time","");
	cJSON_AddStringToObject(tpl,"Call_Target","");
	
	cJSON_AddStringToObject(tpl,"Rec_File","");
	
	cJSON_AddItemToArray(view,tpl);
	
	//cJSON_AddStringToObject(where, "IX_ADDR", GetSysVerInfo_bd());
	#endif
	cJSON_ArrayForEach(filter_element, filter)
	{
		if(cJSON_IsNumber(filter_element))
			cJSON_AddNumberToObject(where,filter_element->string,filter_element->valueint);
		else if(cJSON_IsString(filter_element))
			cJSON_AddStringToObject(where,filter_element->string,filter_element->valuestring);
	}
	//cJSON_AddStringToObject(where, "Platform", "IX1/2");
	API_TB_SelectBySortByName(TB_NAME_DSCallRecord, view, where, 0);

	cJSON_Delete(where);
	cJSON_DeleteItemFromArray(view,0);
	cJSON *one_node;
	cJSON *tar;
	cJSON *temp;
	cJSON_ArrayForEach(one_node, view)
	{
		tar=cJSON_GetObjectItemCaseSensitive(one_node,"Call_Target");
		
		temp=cJSON_GetObjectItem(tar,"Call_Tar_Name");
		cJSON_AddStringToObject(one_node,"Tar name",temp?temp->valuestring:"-");
		temp=cJSON_GetObjectItem(tar,"Call_Tar_IXAddr");
		cJSON_AddStringToObject(one_node,"Tar addr",temp?temp->valuestring:"-");
		cJSON_AddItemToObject(one_node,"Rec_File",cJSON_DetachItemFromObject(one_node,"Rec_File"));
		cJSON_DeleteItemFromObject(one_node,"Call_Target");
	}

	SortTableView(view, "Time", 1);
	
	

	//SortTableView(view, "IX_ADDR", 0);
	//AddUnlockCode_ToIXGIM_Tb(view);
	return view;
}
static TB_VIWE* call_record_tb_menu = NULL;	
void Playback_CB(int sec, int max);
void CallRecordCellClickCallback(int line, int col)
{
	cJSON *item=cJSON_GetArrayItem(call_record_tb_menu->view,line);
	if(item)
	{
		
		if(get_play_back_state())
		{
			api_playback_event2("Play_Stop",NULL,NULL);
			BEEP_ERROR();
		}
		else
		{
			if(playback_menu)
				lv_obj_del(playback_menu);
			playback_menu=PlaybackMenu_init();
			//api_start_ffmpegPlay(GetEventItemString(call_record_edit_record,"Rec_File"),Playback_CB);
			api_playback_event2("Play_Start",GetEventItemString(item,"Rec_File"),Playback_CB);
		}
	}
}
void MenuCallRecord_Init(void)
{
	call_record_back=lv_scr_act();
	call_record_tb=FilterCallRecordTb(NULL);
	
	//cJSON *para=cJSON_CreateObject();
	//int col_w[4]={200,280,150,150};
	//cJSON_AddItemToObject(para,"col_w",cJSON_CreateIntArray(col_w,4));
	//call_record_table_display=create_utility_table_display_menu(call_record_tb,"Call record ","Tar addr",para);
	
	cJSON *para = cJSON_CreateObject();
	int col_w[4] = {200,280,150,150};
	cJSON_AddItemToObject(para,"ColumnWidth",cJSON_CreateIntArray(col_w,4));
	cJSON_AddItemToObject(para,"HeaderEnable",cJSON_CreateBool(true));
	cJSON_AddNumberToObject(para,"CellCallback",(int)CallRecordCellClickCallback);
#if 0
	int fiterCallback = MenuSipIMAccView_Filter;
	cJSON* fiter = cJSON_CreateObject();
    cJSON_AddStringToObject(fiter, "Name", "IX_ADDR");
    cJSON_AddStringToObject(fiter, "Value", "");
    cJSON_AddNumberToObject(fiter, "Callback", fiterCallback);
    cJSON_AddItemToObject(para,"Filter",fiter);
#endif
#if 0
	cJSON* func = cJSON_CreateObject();
	cJSON* funcName = cJSON_AddArrayToObject(func, "Name");
	cJSON_AddItemToArray(funcName, cJSON_CreateString("Add@Res"));
	cJSON_AddItemToArray(funcName, cJSON_CreateString("Clear"));
	cJSON_AddNumberToObject(func, "Callback", (int)0);
	cJSON_AddItemToObject(para, "Function", func);
#endif
	
	TB_MenuDisplay(&call_record_tb_menu,call_record_tb,"Call record ",para);
	//im_acc_table_display=create_table_display_menu(im_acc_tb,1,NULL);
	//printf("11111111111%s:%d\n",__func__,__LINE__);

}
void Playback_CB(int sec, int max)
{
	//pthread_mutex_lock(&momo_mon_lock);
	//pthread_mutex_unlock(&momo_mon_lock);
	vtk_lvgl_lock();
	if(playback_menu)
	{
		lv_obj_t *time_lable=lv_obj_get_user_data(playback_menu);
		  char curtime[10];
	
	    sprintf(curtime,"%02d:%02d",sec/60,sec%60);

	    lv_label_set_text(time_lable, curtime);
	}
	vtk_lvgl_unlock();
}
void MenuCallRecord_Process(int msg_type,void* arg)
{
	//�յ����ذ������£�����popDisplayLastMenu��������һ��˵�
	//popDisplayLastMenu();
	//�յ�ĳһ�����ݱ�������Ϣ��׼�����ݣ�����SwitchOneMenu�л�������IXG�ֻ��༭�˵�
	//SwitchOneMenu(MenuOneIXGImEdit,arg,1);
	//printf("111111111MenuIXGTbView_Process msg:%d\n",msg_type);
	cJSON *edit_able;
	cJSON *func;
	cJSON *where;
	
	switch(msg_type)
	{
		#if 0
		case IXG_MSG_ADD_RECORD:
		
			//if(editor)
				//lv_obj_del(editor);
			if(edit_record!=NULL)
				cJSON_Delete(edit_record);
			edit_record=Create_New_IXGIM_Item();
			edit_able=Create_IXGIM_Editable(1);
			
			//editor=create_editor_menu(edit_record,edit_able,IXGIMEditCheck);
			create_editor_menu(edit_record,edit_able,IXGIMEditDesc);
			cJSON_Delete(edit_able);
			add_new_item=1;
			break;
		#endif	
		case TABLE_MSG_EDIT_RECORD:
			//printf_json((cJSON*)arg,__func__);
			if(call_record_edit_record!=NULL)
				cJSON_Delete(call_record_edit_record);
			call_record_edit_record=cJSON_Duplicate((cJSON*)arg,1);
			//if(editor)
				//lv_obj_del(editor);
			edit_able=cJSON_CreateArray();
			func=cJSON_CreateObject();
			cJSON_AddStringToObject(func,"CB_FOP_BTN_1","Back");
			if(strstr(GetEventItemString(call_record_edit_record,"Rec_File"),".mp4"))
				cJSON_AddStringToObject(func,"CB_FOP_BTN_2","Play");
			cJSON_AddStringToObject(func,"CB_FOP_BTN_3","Del");
			//editor=create_editor_menu(edit_record,edit_able,IXGIMEditCheck);
			call_record_view=common_record_editor_menu(call_record_edit_record,edit_able,NULL,func);
			//create_editor_menu(im_acc_edit_record,edit_able,NULL);
			cJSON_Delete(edit_able);
			cJSON_Delete(func);
			//add_new_item=0;
			break;
		case RECORD_FUN_1:
			if(call_record_view)
			{
				lv_disp_load_scr(call_record_view->back_scr);
				delete_recodr(call_record_view);
				call_record_view=NULL;
			}
			break;
		case RECORD_FUN_2:
			if(call_record_view)
			{
				if(get_play_back_state())
				{
					api_playback_event2("Play_Stop",NULL,NULL);
					BEEP_ERROR();
				}
				else
				{
					if(playback_menu)
						lv_obj_del(playback_menu);
					playback_menu=PlaybackMenu_init();
					//api_start_ffmpegPlay(GetEventItemString(call_record_edit_record,"Rec_File"),Playback_CB);
					api_playback_event2("Play_Start",GetEventItemString(call_record_edit_record,"Rec_File"),Playback_CB);
				}
			
			}
			break;
		case RECORD_FUN_3:
			if(call_record_edit_record&&call_record_table_display)
			{
				//printf_json(call_record_edit_record, "0000000");
				where=cJSON_CreateObject();
				cJSON_AddItemToObject(where,"Time",cJSON_DetachItemFromObject(call_record_edit_record,"Time"));
				cJSON_AddItemToObject(where,"Rec_File",cJSON_DetachItemFromObject(call_record_edit_record,"Rec_File"));
				//printf_json(where, "1111111111");
				API_TB_DeleteByName(TB_NAME_DSCallRecord, where);
				cJSON_Delete(where);
				cJSON_Delete(call_record_edit_record);
				call_record_edit_record=NULL;
				cJSON_Delete(call_record_tb);
				//where=cJSON_CreateObject();
				//cJSON_AddStringToObject(where,"TAR_ACC",arg);
				call_record_tb=FilterCallRecordTb(call_record_view_filter);	
				//cJSON_Delete(where);
				utility_table_fresh(call_record_table_display,call_record_tb);

				lv_disp_load_scr(call_record_view->back_scr);
				delete_recodr(call_record_view);
				call_record_view=NULL;
			}
			break;
	
		case TABLE_MSG_FILTER:
			if(call_record_table_display)
			{
				cJSON_Delete(call_record_tb);
				if(call_record_view_filter)
					cJSON_Delete(call_record_view_filter);
				call_record_view_filter=cJSON_CreateObject();
				cJSON_AddStringToObject(call_record_view_filter,"Tar addr",arg);
				call_record_tb=FilterCallRecordTb(call_record_view_filter);	
				//cJSON_Delete(where);
				utility_table_fresh(call_record_table_display,call_record_tb);
			}
			break;
		case TABLE_MSG_UNFILTER:
			if(call_record_table_display)
			{
				if(call_record_view_filter)
					cJSON_Delete(call_record_view_filter);
				call_record_view_filter=NULL;
				cJSON_Delete(call_record_tb);
				
				call_record_tb=FilterCallRecordTb(NULL);	
				
				utility_table_fresh(im_acc_table_display,im_acc_tb);
			}
			break;	
		#if 0	
		case IXG_MSG_CHANGE_FITER:
			if(view_filter)
				cJSON_Delete(view_filter);
			if(view_tb)
				cJSON_Delete(view_tb);
			printf_json((cJSON*)arg,"FILTER");
			view_filter=cJSON_Duplicate((cJSON*)arg,1);
			view_tb=FilterIXGIm_From_R8001((cJSON*)arg);
			ixgview_data_fresh(table_display,view_tb,1);
			break;
		#endif
		case TABLE_MSG_EXIT:
			//MenuSipIMAccView_Close();
			//ui_page_exit();
			lv_msg_send(SETTING_MSG_SettingReturn,NULL);
			break;
		case Playback_MSG_BACK:
			if(playback_menu)
			{
				//api_stop_ffmpegPlay();
				api_playback_event2("Play_Stop",NULL,NULL);
				//usleep(100*1000);
				lv_obj_del(playback_menu);
			}
			playback_menu=NULL;
			break;
		case Playback_MSG_REPLAY:	
			if(playback_menu&&call_record_edit_record)
			{
				api_playback_event2("Play_Next",GetEventItemString(call_record_edit_record,"Rec_File"),Playback_CB);
			}
			break;
		#if 0	
		case RECORD_MSG_SAVE_EXIT:
			if(add_new_item)
			{
				if(Judge_IXGIM_Exist((cJSON*)arg)>0)
				{
					API_TIPS("The device is aleady exist!");
					return;
				}
			}
			printf_json((cJSON*)arg,"SAVE_EXIT");
			if(view_tb)
				cJSON_Delete(view_tb);
			update_one_ixgim_to_R8001tb((cJSON*)arg);
			update_one_unlock_code_to_Privatetb((cJSON*)arg);
			view_tb=FilterIXGIm_From_R8001(view_filter);
			ixgview_data_fresh(table_display,view_tb,1);
			break;
		case RECORD_MSG_DEL_EXIT:
			if(add_new_item)
			{
			
				//if(Judge_IXGIM_Exist((cJSON*)arg))>0)
				{
					//API_TIPS("The device is aleady exist!");
					return;
				}
			}
			if(view_tb)
				cJSON_Delete(view_tb);
			delete_one_ixgim_to_R8001tb((cJSON*)arg);
			view_tb=FilterIXGIm_From_R8001(view_filter);
			ixgview_data_fresh(table_display,view_tb,1);
			break;	
		#endif
			
	}
}

void MenuCallRecord_Close(void)
{
	//�˵��ر�ʱ�������ñ�༭�����ٺ���
	//�ͷű�����
	//if(editor)
		//lv_obj_del(editor);
	//editor=NULL;
	if(call_record_back&&call_record_back!=lv_scr_act())
		lv_scr_load(call_record_back);

	if(playback_menu)
	{
		PlaybackMenu_Delete(playback_menu);
		//api_stop_ffmpegPlay();
		api_playback_event2("Play_Stop",NULL,NULL);
		lv_obj_del(playback_menu);
	}
	playback_menu=NULL;
	
	if(call_record_view!=NULL)
		delete_recodr(call_record_view);
	call_record_view=NULL;

	if(call_record_tb_menu){
		TB_MenuDelete(&call_record_tb_menu);
		call_record_tb_menu = NULL;
	}
	if(call_record_table_display!=NULL)
		delete_table(call_record_table_display);
	call_record_table_display=NULL;
	
	if(call_record_tb)
		cJSON_Delete(call_record_tb);
	
	call_record_tb=NULL;
//printf("11111111111%s:%d\n",__func__,__LINE__);
	if(call_record_edit_record!=NULL)
		cJSON_Delete(call_record_edit_record);
	call_record_edit_record=NULL;
//printf("11111111111%s:%d\n",__func__,__LINE__);
	if(call_record_view_filter)
		cJSON_Delete(call_record_view_filter);
	call_record_view_filter=NULL;
//printf("11111111111%s:%d\n",__func__,__LINE__);	
	
	call_record_table_display=NULL;
	
}

void API_MenuCallRecord_Process(void* arg)
{
	TABLE_UTILITY* td = (TABLE_DISPLAY*)arg;
	MenuCallRecord_Process(td->msg_type,td->msg_data);
}

static void Playback_back_event(lv_event_t* e)
{
	TABLE_DISPLAY td ;
	lv_obj_t *ui_video_save = lv_event_get_user_data(e);
	set_lvgl_video_parent(0, NULL, 0);
	lv_disp_load_scr(ui_video_save);
    	 td.msg_data =  NULL;
    td.msg_type = Playback_MSG_BACK;
    API_SettingMenuProcess(&td); 
}
static void Playback_replay_event(lv_event_t* e)
{
	TABLE_DISPLAY td ;
	
    	 td.msg_data =  NULL;
    td.msg_type = Playback_MSG_REPLAY;
    API_SettingMenuProcess(&td); 
}
static lv_obj_t *ui_video_save=NULL;
lv_obj_t *PlaybackMenu_init(void)
{ 
    

	int i;
	lv_obj_t *ui_video=NULL;
	 ui_video_save=lv_scr_act();
	vtk_lvgl_lock(); 
	if(ui_video==NULL)
	{
    			ui_video = lv_obj_create(NULL);
    			lv_obj_remove_style_all(ui_video);
    			lv_obj_set_size(ui_video,  lv_disp_get_hor_res(NULL), lv_disp_get_ver_res(NULL));
    			lv_obj_set_style_bg_opa(ui_video, LV_OPA_100, 0);
    			lv_obj_set_style_bg_color(ui_video,lv_color_hex(0),0);
    			lv_obj_set_x(ui_video, 0);
    			lv_obj_set_y(ui_video, 0);
	}
    set_menu_with_video_on();
    
    
          
    lv_disp_load_scr(ui_video);
    lv_disp_set_bg_color(NULL, lv_color_hex(0));
	lv_obj_set_style_bg_color(lv_scr_act(),lv_color_hex(0),0);
    lv_obj_set_style_bg_opa(lv_scr_act(),LV_OPA_100,0);
	lv_obj_set_style_text_font(ui_video, &lv_font_montserrat_30, 0);
	//lv_obj_set_style_text_color(ui_video, lv_color_make(0,0,255), 0);
	 lv_obj_set_style_text_color(ui_video, lv_color_white(), 0);
	
	vtk_lvgl_unlock();
  

	#if !defined( LVGL_SUPPORT_TDE )
	
	#else
    // initial display video
    vtk_lvgl_lock();
    set_lvgl_video_parent(0, ui_video, 0);
	//set_lvgl_video_parent(1, ui_video, 1);
	//set_lvgl_video_parent(2, ui_video, 1);
	//set_lvgl_video_parent(3, ui_video, 1);
	//set_lvgl_video_parent(4, ui_video, 1);
	//SetLayerPos(AK_VO_LAYER_VIDEO_1,0,0,720,576);
	 vtk_lvgl_unlock();
    printf("=========================================tde view==================\n");
    #endif
	static lv_style_t style_btn;
	 lv_style_init(&style_btn);
    lv_style_set_text_font(&style_btn,&lv_font_montserrat_32);
    lv_style_set_radius(&style_btn,LV_RADIUS_CIRCLE);
    lv_style_set_bg_color(&style_btn,lv_color_make(230, 231, 232));
    lv_style_set_text_color(&style_btn,lv_color_hex(0xff0000));
	
	lv_obj_t *ui_qrcode_btn=lv_btn_create(ui_video);
	   lv_obj_add_style(ui_qrcode_btn,&style_btn,0);
        lv_obj_set_size(ui_qrcode_btn,120,70);
	 //lv_obj_set_style_bg_color(ui_qrcode_btn, lv_color_hex(0x41454D), 0);
	    //lv_obj_align(ui_qrcode_btn, LV_ALIGN_CENTER,0,0);
		//lv_obj_set_pos(ui_qrcode_btn, 550, 155+3*60);
		 lv_obj_align(ui_qrcode_btn, LV_ALIGN_BOTTOM_LEFT, 20, -20);
		//lv_obj_set_style_border_width(ui_qrcode_btn, 4, 0);
		//lv_obj_set_style_border_color(ui_qrcode_btn,lv_color_white(),0);
		//lv_obj_set_style_border_opa(ui_qrcode_btn,LV_OPA_100, 0);
	    lv_obj_add_event_cb(ui_qrcode_btn, Playback_back_event, LV_EVENT_CLICKED, ui_video_save);

	    lv_obj_t* ui_Label = lv_label_create(ui_qrcode_btn);
		//lv_obj_set_size(ui_Label, 100, 50);
	    lv_obj_align(ui_Label, LV_ALIGN_CENTER, 0, 0);

	    lv_label_set_text(ui_Label, "Back");
		#if 1
		lv_obj_t *ui_qrcode_btn2=lv_btn_create(ui_video);
	   lv_obj_add_style(ui_qrcode_btn2,&style_btn,0);
        lv_obj_set_size(ui_qrcode_btn2,120,70);
	 //lv_obj_set_style_bg_color(ui_qrcode_btn, lv_color_hex(0x41454D), 0);
	    //lv_obj_align(ui_qrcode_btn, LV_ALIGN_CENTER,0,0);
		//lv_obj_set_pos(ui_qrcode_btn, 550, 155+3*60);
		 lv_obj_align(ui_qrcode_btn2, LV_ALIGN_BOTTOM_RIGHT, -20, -20);
		//lv_obj_set_style_border_width(ui_qrcode_btn, 4, 0);
		//lv_obj_set_style_border_color(ui_qrcode_btn,lv_color_white(),0);
		//lv_obj_set_style_border_opa(ui_qrcode_btn,LV_OPA_100, 0);
	    lv_obj_add_event_cb(ui_qrcode_btn2, Playback_replay_event, LV_EVENT_CLICKED, NULL);

	    lv_obj_t* ui_Label2 = lv_label_create(ui_qrcode_btn2);
		//lv_obj_set_size(ui_Label, 100, 50);
	    lv_obj_align(ui_Label2, LV_ALIGN_CENTER, 0, 0);

	    lv_label_set_text(ui_Label2, "Replay");
		#endif
		lv_obj_t *time_label=lv_label_create(ui_video);
	lv_obj_set_size(time_label, 100, 40);
	lv_obj_set_align(time_label, LV_ALIGN_TOP_LEFT);
	lv_obj_set_pos(time_label,30, 10);
	lv_label_set_text(time_label,"00:00");
	lv_obj_set_user_data(ui_video, time_label);
    return ui_video;
}
void PlaybackMenu_Delete(lv_obj_t *pb_menu)
{
	lv_disp_load_scr(ui_video_save);
	//lv_obj_del(pb_menu);
}