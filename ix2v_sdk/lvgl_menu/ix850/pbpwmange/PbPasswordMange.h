#ifndef PBPASSWORDMANGE_H_
#define PBPASSWORDMANGE_H_

#include "lv_ix850.h"


lv_obj_t* password_menu_creat(int cnt);
lv_obj_t* password_keyboard_creat(lv_obj_t* parent);
static void password_keyboard_click_event(lv_event_t* e);
lv_obj_t* add_password_btn(lv_obj_t* parent, const char* txt, lv_event_cb_t cb, lv_obj_t* target);
static void password_cnt_change_event(lv_event_t* e);
lv_obj_t* add_password_label(lv_obj_t* parent);
static lv_obj_t* ui_pb_pwd_init(void);
void pwd_delete(void);
#endif 
