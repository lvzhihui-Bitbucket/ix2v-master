#include "PbPasswordMange.h"
#include "menu_common.h"
#include "obj_IoInterface.h"
#include "obj_lvgl_msg.h"

extern const lv_font_t* font_larger;
static char CodeKpInput_Buff[20] = { 0 };
static int CodeKpInput_Len = 0;
static int Password_Len = 0;
static lv_obj_t* pb_pwd = NULL;
static lv_obj_t* pwd_back_scr = NULL;
static char* cnt_pwd;

lv_obj_t* password_menu_creat(int cnt)
{
    pwd_back_scr = lv_scr_act();
    pb_pwd = ui_pb_pwd_init();

    Password_Len = API_Para_Read_Int(AutoUnlockPwdLen);         //读取当前密码的长度
    switch(cnt){                                                //选择需要修改的密码
        case 1: 
            cnt_pwd = Unlock1Pwd1;
            break;
        case 2:
            cnt_pwd = Unlock1Pwd2;
            break;
        case 3:
            cnt_pwd = Unlock2Pwd1;
            break;
        case 4:
            cnt_pwd = Unlock2Pwd2;
            break;
        default:
            break;
    }

    lv_obj_t* codeCont = lv_obj_create(pb_pwd);
    lv_obj_set_size(codeCont, 480, 800);
    lv_obj_clear_flag(codeCont, LV_OBJ_FLAG_SCROLLABLE);
    //添加标题
    lv_obj_t* title = lv_label_create(codeCont);
    lv_obj_align(title, LV_ALIGN_TOP_MID, 0, 20);
    lv_obj_set_size(title, LV_PCT(80), LV_SIZE_CONTENT);
    lv_obj_set_style_text_font(title, &lv_font_montserrat_32, 0);
    lv_label_set_text(title, "Please enter your new password!");        
    lv_obj_set_style_text_color(title, lv_color_black(), 0);

    password_keyboard_creat(codeCont);              //添加密码输入栏
    lv_disp_load_scr(pb_pwd);
    return codeCont;
}

lv_obj_t* password_keyboard_creat(lv_obj_t* parent)
{
    lv_obj_t* keyCont = lv_obj_create(parent);
    lv_obj_set_size(keyCont, LV_PCT(100), 720);
    lv_obj_set_y(keyCont, 80);
    lv_obj_set_style_border_side(keyCont, 0, 0);
    lv_obj_set_style_bg_opa(keyCont, LV_OPA_MIN, 0);
    lv_obj_clear_flag(keyCont, LV_OBJ_FLAG_SCROLLABLE);
    //创建密码显示框,默认为白色
    lv_obj_t* pwCont = lv_obj_create(keyCont);
    lv_obj_remove_style_all(pwCont);
    lv_obj_set_size(pwCont, LV_PCT(40), 100);
    lv_obj_align(pwCont, LV_ALIGN_TOP_MID, 0, 100);
    lv_obj_set_flex_flow(pwCont, LV_FLEX_FLOW_ROW_WRAP);
    lv_obj_set_style_pad_column(pwCont, 10, 0);
    lv_obj_set_style_pad_row(pwCont, 10, 0);
    for (size_t i = 0; i < Password_Len; i++)
    {
        add_password_label(pwCont);
    }
    //创建密码键盘
    lv_obj_t* code_1 = add_password_btn(keyCont, "FOUR", password_cnt_change_event, pwCont);
    lv_obj_align(code_1, LV_ALIGN_TOP_MID, -140, 240);

    lv_obj_t* code_2 = add_password_btn(keyCont, "SIX", password_cnt_change_event, pwCont);
    lv_obj_align(code_2, LV_ALIGN_TOP_MID, 0, 240);

    lv_obj_t* code_3 = add_password_btn(keyCont, "EIGHT", password_cnt_change_event, pwCont);
    lv_obj_align(code_3, LV_ALIGN_TOP_MID, 140, 240);

    lv_obj_t* code1 = add_password_btn(keyCont, "1", password_keyboard_click_event, pwCont);
    lv_obj_align(code1, LV_ALIGN_TOP_MID, -140, 330);

    lv_obj_t* code2 = add_password_btn(keyCont, "2", password_keyboard_click_event, pwCont);
    lv_obj_align(code2, LV_ALIGN_TOP_MID, 0, 330);

    lv_obj_t* code3 = add_password_btn(keyCont, "3", password_keyboard_click_event, pwCont);
    lv_obj_align(code3, LV_ALIGN_TOP_MID, 140, 330);

    lv_obj_t* code4 = add_password_btn(keyCont, "4", password_keyboard_click_event, pwCont);
    lv_obj_align(code4, LV_ALIGN_TOP_MID, -140, 420);

    lv_obj_t* code5 = add_password_btn(keyCont, "5", password_keyboard_click_event, pwCont);
    lv_obj_align(code5, LV_ALIGN_TOP_MID, 0, 420);

    lv_obj_t* code6 = add_password_btn(keyCont, "6", password_keyboard_click_event, pwCont);
    lv_obj_align(code6, LV_ALIGN_TOP_MID, 140, 420);

    lv_obj_t* code7 = add_password_btn(keyCont, "7", password_keyboard_click_event, pwCont);
    lv_obj_align(code7, LV_ALIGN_TOP_MID, -140, 510);

    lv_obj_t* code8 = add_password_btn(keyCont, "8", password_keyboard_click_event, pwCont);
    lv_obj_align(code8, LV_ALIGN_TOP_MID, 0, 510);

    lv_obj_t* code9 = add_password_btn(keyCont, "9", password_keyboard_click_event, pwCont);
    lv_obj_align(code9, LV_ALIGN_TOP_MID, 140, 510);

    lv_obj_t* codeback = add_password_btn(keyCont, LV_VTK_BACKSPACE, password_keyboard_click_event, pwCont);
    lv_obj_align(codeback, LV_ALIGN_TOP_MID, -140, 600);
    //lv_obj_set_style_text_color(codeback, lv_color_make(150, 61, 53), 0);

    lv_obj_t* code0 = add_password_btn(keyCont, "0", password_keyboard_click_event, pwCont);
    lv_obj_align(code0, LV_ALIGN_TOP_MID, 0, 600);

    lv_obj_t* codeKey = add_password_btn(keyCont, LV_VTK_CONFIM, password_keyboard_click_event, pwCont);
    lv_obj_align(codeKey, LV_ALIGN_TOP_MID, 140, 600);
    //lv_obj_set_style_text_color(codeKey, lv_color_make(46, 204, 127), 0);

    return keyCont;
}
/**********************
 * 密码长度更改事件
 * ********************/
static void password_cnt_change_event(lv_event_t* e)
{
    lv_obj_t* obj = lv_event_get_target(e);
    lv_obj_t* label = lv_obj_get_child(obj, 0);
    lv_obj_t* codeNum = lv_event_get_user_data(e);
   
    if (codeNum) {
        lv_obj_clean(codeNum);              //清空密码显示框
        for (size_t i = 0; i < CodeKpInput_Len; i++)    //清空密码
        {
            CodeKpInput_Buff[i] = 0;
        }
        CodeKpInput_Len = 0;                
    }else{
        return;
    }
    char* text = lv_label_get_text(label);
    //设置长度
    if (strcmp(text, "FOUR") == 0)
    {
        Password_Len = 4;       
    }
    else if (strcmp(text, "SIX") == 0)
    {
        Password_Len = 6;
    }
    else
    {
        Password_Len = 8;
    }
    for (size_t i = 0; i < Password_Len; i++)
    {
        add_password_label(codeNum);
    }
}
/**********************
 * 密码输入事件
 * ********************/
static void password_keyboard_click_event(lv_event_t* e)
{
    lv_obj_t* obj = lv_event_get_target(e);
    lv_obj_t* label = lv_obj_get_child(obj, 0);
    lv_obj_t* codeNum = lv_event_get_user_data(e);
    char* text = lv_label_get_text(label);

    if (strcmp(text, LV_VTK_BACKSPACE) == 0)
    {
        if (CodeKpInput_Len == 0)
        {
            API_SettingMenuProcess(MENU_PASSWORD_EXIT); 
        }
        else
        {
            lv_obj_t* btn = lv_obj_get_child(codeNum, CodeKpInput_Len - 1);
            if (btn) {
                lv_obj_set_style_bg_color(btn, lv_color_white(), 0);
                CodeKpInput_Len--;
                CodeKpInput_Buff[CodeKpInput_Len] = 0;
            }            
        }

    }
    else if (strcmp(text, LV_VTK_CONFIM) == 0)
    {
        if (CodeKpInput_Len > 0)
        {
            if(cnt_pwd){
                API_Para_Write_String(cnt_pwd,CodeKpInput_Buff); 
            }            
            API_Para_Write_Int(AutoUnlockPwdLen,Password_Len);
            LV_API_TIPS("Password set successfully!",5);
            //API_TIPS("Password set successfully!");

            API_SettingMenuProcess(MENU_PASSWORD_EXIT);        
        }
    }
    else
    {
        if (CodeKpInput_Len < Password_Len)
        {           
            lv_obj_t* btn = lv_obj_get_child(codeNum, CodeKpInput_Len);
            if (btn) {
                lv_obj_set_style_bg_color(btn, lv_color_black(), 0);
                CodeKpInput_Buff[CodeKpInput_Len] = text[0];               
                CodeKpInput_Len++;
                CodeKpInput_Buff[CodeKpInput_Len] = 0;
            }           
        }
    }
}
/**********************
 * 添加一个密码文本
 * parent ：父对象
 * ********************/
lv_obj_t* add_password_label(lv_obj_t* parent)
{
    lv_obj_t* obj = lv_btn_create(parent);
    lv_obj_set_size(obj, 30, 30);

    lv_obj_set_style_radius(obj, LV_RADIUS_CIRCLE, 0);
    lv_obj_set_style_border_width(obj, 1, 0);
    lv_obj_set_style_bg_color(obj, lv_color_white(), 0);

    return obj;
}
/**********************
 * 添加一个密码按钮
 * parent ：父对象
 * txt ：按钮文本
 * cb ：按钮按下后的回调事件
 * target ：密码框
 * ********************/
lv_obj_t* add_password_btn(lv_obj_t* parent, const char* txt, lv_event_cb_t cb, lv_obj_t* target)
{
    lv_obj_t* obj = lv_btn_create(parent);
    lv_obj_set_size(obj, 132, 75);
    lv_obj_add_event_cb(obj, cb, LV_EVENT_CLICKED, target);
    lv_obj_set_style_radius(obj, 10, 0);
    lv_obj_set_style_border_width(obj, 0, 0);
    lv_obj_set_style_text_color(obj, lv_color_white(), 0);
    lv_obj_set_style_text_font(obj, font_larger, 0);
    lv_obj_set_style_bg_color(obj, lv_color_make(90, 89, 81), 0);

    lv_obj_t* label = lv_label_create(obj);
    lv_label_set_text(label, txt);
    lv_obj_center(label);

    return obj;
}
/**********************
 * 删除页面,清空数据
 * ********************/
void pwd_delete(void)
{
    lv_disp_load_scr(pwd_back_scr);
    if(pb_pwd)
    {
        lv_obj_del(pb_pwd);
        pb_pwd = NULL;            
    }
    for (size_t i = 0; i < CodeKpInput_Len; i++)
    {
        CodeKpInput_Buff[i] = 0;
    }
    CodeKpInput_Len = 0;    
}
/**********************
 * 初始化屏幕
 * ********************/
static lv_obj_t* ui_pb_pwd_init(void)
{
    lv_obj_t* pb_scr = lv_obj_create(NULL);
    lv_obj_clear_flag(pb_scr, LV_OBJ_FLAG_SCROLLABLE);
    lv_obj_remove_style_all(pb_scr);
    lv_obj_set_size(pb_scr, lv_disp_get_hor_res(NULL), lv_disp_get_ver_res(NULL));
    lv_obj_align(pb_scr, LV_ALIGN_TOP_LEFT, 0, 0);
    lv_obj_set_style_bg_color(pb_scr, lv_palette_main(LV_PALETTE_GREY), 0);
    lv_obj_set_style_bg_opa(pb_scr, LV_OPA_100, 0);
    
    return pb_scr;
}