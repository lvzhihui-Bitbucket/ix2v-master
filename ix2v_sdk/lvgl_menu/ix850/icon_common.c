#include "icon_common.h"
#include "obj_IoInterface.h"

extern const lv_font_t* font_small;
extern const lv_font_t* font_medium;
extern const lv_font_t* font_larger;

lv_ft_info_t ft_small;
lv_ft_info_t ft_normal;
lv_ft_info_t ft_large;
lv_ft_info_t ft_xlarge;

lv_obj_t* creat_cont_for_postion(lv_obj_t* parent, cJSON* json)
{
    cJSON* height = cJSON_GetObjectItemCaseSensitive(json, get_global_key_string(GLOBAL_ICON_KEYS_HEIGHT));
    cJSON* width = cJSON_GetObjectItemCaseSensitive(json, get_global_key_string(GLOBAL_ICON_KEYS_WIDTH));
    cJSON* posx = cJSON_GetObjectItemCaseSensitive(json, get_global_key_string(GLOBAL_ICON_KEYS_POS_X));
    cJSON* posy = cJSON_GetObjectItemCaseSensitive(json, get_global_key_string(GLOBAL_ICON_KEYS_POS_Y));

    lv_obj_t* cont = lv_obj_create(parent);
    lv_obj_clear_flag(cont, LV_OBJ_FLAG_SCROLLABLE);
    lv_obj_set_x(cont, posx->valueint);
    lv_obj_set_y(cont, posy->valueint);
    lv_obj_set_height(cont, height->valueint);
    lv_obj_set_width(cont, width->valueint);

    lv_obj_set_style_bg_opa(cont, 0, 0);
    lv_obj_set_style_pad_all(cont, 0, 0);
    lv_obj_set_style_border_opa(cont, 0, 0);

    return cont;
}


char* IX850_get_current_time(void)
{
    time_t rawtime;
    struct tm* info;
    char* buffer = lv_mem_alloc(100);

    time(&rawtime);

    info = localtime(&rawtime);
	
	int date_format=API_Para_Read_Int(DATE_FORMAT);
	/*
	 ["0", "MM_DD_YYYY"],
            ["1", "DD_MM_YYYY"],
            ["2", "YYYY_mm_dd"]
            */
	if(date_format==0)
		strftime(buffer, 20, "%m-%d-%Y ", info);
	else if(date_format==1)
		strftime(buffer, 20, "%d-%m-%Y ", info);
	else
		strftime(buffer, 20, "%Y-%m-%d ", info);

	if(API_Para_Read_Int(TIME_FORMAT))
		strftime(buffer+strlen(buffer), 20, "%H:%M", info);
	else
	{
		strftime(buffer+strlen(buffer), 20, "%I:%M", info);
	    strcat(buffer, (info->tm_hour < 12) ? "AM" : "PM");
	}

	
    return buffer;
}

void set_font_size(lv_obj_t* obj, int size)
{
    switch (size)
    {
    case 0:
         //lv_obj_set_style_text_font(obj, font_small, 0);
        lv_obj_set_style_text_font(obj, ft_small.font, 0);
        break;
    case 1:
        //lv_obj_set_style_text_font(obj, font_medium, 0);
        lv_obj_set_style_text_font(obj, ft_normal.font, 0);
        break;
    case 2:
        //lv_obj_set_style_text_font(obj, font_larger, 0);
        lv_obj_set_style_text_font(obj, ft_large.font, 0);
        break;
    case 3:
         //lv_obj_set_style_text_font(obj, font_small, 0);
        lv_obj_set_style_text_font(obj, ft_xlarge.font, 0);
        break;
    default:
        break;
    }
}

const lv_font_t * vtk_get_font_by_size(int size)
{
    lv_font_t * ret;
    switch (size)
    {
        case 0:
            ret = ft_small.font;
            break;
        case 1:
            ret = ft_normal.font;
            break;
        case 2:
            ret = ft_large.font;
            break;
        case 3:
            ret = ft_xlarge.font;
            break;

        default:
            ret = ft_small.font;
            break;
    }

    return ret;
}

bool ix850s_save_file(const char* filename, char* buf) {
    if (buf == NULL)
        return false;

    FILE* f;
    f = fopen(filename, "w");
    if (f == NULL) {
        printf("Can't open file");
        return 0;
    }

    fseek(f, 0, SEEK_SET);

    fprintf(f, "%s", buf);
    fclose(f);

    return true;
}


