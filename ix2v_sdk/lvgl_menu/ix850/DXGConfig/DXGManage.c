#include "lv_ix850.h"
#include "obj_TableSurver.h"
#include "task_Event.h"
#include "obj_lvgl_msg.h"
#include "Common_TableView.h"
#include "obj_Ftp.h"
#include "define_file.h"
#include "KeyboardResource.h"
#include "obj_PublicInformation.h"
#include "obj_IoInterface.h"

#define DXG_VIEW_PARA		"{\"HeaderEnable\":true,\"CellCallback\":0,\"ColumnWidth\":[140, 140, 200],\"Function\":{\"Name\":[],\"Callback\":0}}"
#define ABOUT_VIEW_PARA		"{\"HeaderEnable\":false,\"CellCallback\":0,\"ColumnWidth\":[240,240],\"Function\":{\"Name\":[],\"Callback\":0}}"
#define DXG_UPDATE_PARA		"{\"HeaderEnable\":false,\"CellCallback\":0,\"ColumnWidth\":[240,240],\"Function\":{\"Name\":[],\"Callback\":0}}"

static TB_VIWE* dxgView = NULL;
static TB_VIWE* dxginfoView = NULL;
static TB_VIWE* dxgupdateView = NULL;
static VTK_KB_S* kb = NULL;
static int dxgip = 0;
lv_obj_t* dxg_tips=NULL;
cJSON *onlineDxgList=NULL;
cJSON *onlineDxgView=NULL;
static int iperfTestDs1IpAddr;
static int iperfTestState = 0;
static 	int iperfTestDxgIndex = 0;
static int batchUpdateState = 0;
static 	int batchUpdateDxgIndex = 0;
static int onlineDxgCnt = 0;
static const char *updatestr[2] =
{
	"DxgFwServer",
	"DxgFwCode",
};
#define UPDATE_CODE_CLOUD_DIR 	"/home/userota/"
static char onlineUpgrade_Code[9] = {0};
static char onlineUpgrade_Server[40] = {0};
char update_file_dir[100];
static int networkInfo=0;
static int updateState=0;
static int updateTimeCnt = 0;
char batchFunc[10]={0};
void dxgmanage_close(void);
static int Countdown = 0;


int ShellCmd_ModeTip(cJSON *cmd)
{
   	char* operate = GetShellCmdInputString(cmd, 1);
   	char* msgString = GetShellCmdInputString(cmd, 2);
    if(!strcmp(operate, "show"))
    {
        API_TIPS_Mode(msgString);
    }
    else if(!strcmp(operate, "close"))
    {
        API_TIPS_Mode_Close();
    }
    else if(!strcmp(operate, "fresh"))
    {
        API_TIPS_Mode_Fresh(dxg_tips, msgString);
    }
    else if(!strcmp(operate, "time"))
    {
        API_TIPS_Mode_CD(atoi(msgString));
    }
    else if(!strcmp(operate, "show2"))
    {
        //API_TIPS_Ext(msgString);
        LV_API_TIPS(msgString, atoi(GetShellCmdInputString(cmd, 3)));
    }
}


/**********************
 * DXG Manage
 * ********************/
static void DxgCellClick(int line, int col)
{
    dprintf("line = %d, col =%d\n", line, col);
    cJSON* record = cJSON_GetArrayItem(dxgView->view, line);
    int id=0;
    if(record)
    {
        id = GetEventItemInt(record, "GW_ID");
    }
    else
    {
        return;
    }
    if(dxgView)
    {
        if(col==2)
        {
            dxg_info_show(id);
        }
        else if(col==1)
        {
            SetDxgId(id);
            EnterDxgImMenu();
        }
        
    }
}

void TipsDisplay(char* msgString)
{
    #if 1
    if(dxg_tips!=NULL)
    {
        API_TIPS_Mode_Fresh(dxg_tips, msgString);
    }
    else
    {
        dxg_tips = API_TIPS_Mode(msgString);
    }
    #endif
}
void TipsClose(void)
{
    #if 1
    //if(dxg_tips!=NULL)
    {
        API_TIPS_Mode_Close(dxg_tips);
        dxg_tips=NULL;
    }
    #endif
}

/**********************
 * DXG Reboot
 * ********************/
int RebootDxg(int ip)
{
	int ret;
	cJSON *send_event=cJSON_CreateObject();
	cJSON_AddStringToObject(send_event, "EventName","EventReboot");
	cJSON_AddStringToObject(send_event, "SOURCE","DXG");
	cJSON_AddStringToObject(send_event, "IX_ADDR",GetSysVerInfo_IP());
	ret = API_XD_EventJson(ip,send_event);
	cJSON_Delete(send_event);
	return ret;
}
static void dxg_reboot_all(void)
{
    onlineDxgCnt = cJSON_GetArraySize(onlineDxgList);
    if(onlineDxgCnt == 0)
    {
        return;
    }
    cJSON *item;
	char* ipString;
    char display[200] = {0};
    int id;
    cJSON_ArrayForEach(item, onlineDxgList)
    {
        id = GetEventItemInt(item, "GW_ID"); 
        ipString = GetEventItemString(item, "IP_ADDR");
        if(RebootDxg(inet_addr(ipString)))
        {
            sprintf(display, "Reboot DXG%d ok", id);
        }
        else
        {
            sprintf(display, "Reboot DXG%d err", id);
        }
        TipsDisplay(display);
        sleep(1);
    }  
    TipsClose();   
}
/**********************
 * DXG Restore
 * ********************/
int RestoreDxg(int ip)
{
	int ret;
	cJSON *send_event=cJSON_CreateObject();
	cJSON_AddStringToObject(send_event, "EventName","EventFactoryDefault");
	cJSON_AddStringToObject(send_event, "SOURCE","DXG");
	cJSON_AddStringToObject(send_event, "IX_ADDR",GetSysVerInfo_IP());
	ret = API_XD_EventJson(ip,send_event);
	cJSON_Delete(send_event);
	return ret;
}
static void dxg_restore_all(void)
{
    onlineDxgCnt = cJSON_GetArraySize(onlineDxgList);
    if(onlineDxgCnt == 0)
    {
        return;
    }
    cJSON *item;
	char* ipString;
    char display[200] = {0};
    int id;
    cJSON_ArrayForEach(item, onlineDxgList)
    {
        id = GetEventItemInt(item, "GW_ID"); 
        ipString = GetEventItemString(item, "IP_ADDR");
        if(RestoreDxg(inet_addr(ipString)))
        {
            sprintf(display, "Restore DXG%d ok", id);
        }
        else
        {
            sprintf(display, "Restore DXG%d err", id);
        }
        TipsDisplay(display);
        sleep(1);
    } 
    TipsClose();           
}
/**********************
 * DXG Upgrade
 * ********************/
static void dxg_update_all(void)
{
    onlineDxgCnt = cJSON_GetArraySize(onlineDxgList);
    if(onlineDxgCnt == 0)
    {
        return;
    }
	batchUpdateState = 1;
    batchUpdateDxgIndex = 0;
    Countdown = 20*onlineDxgCnt;
    MainMenu_Reset_Time();
    dxg_tips=NULL;
    dxgip = inet_addr(GetEventItemString(cJSON_GetArrayItem(onlineDxgList,0), "IP_ADDR"));
    dxg_update_show();
}

static cJSON* GetDXGTb(void)
{
    if(onlineDxgList)
	{
		cJSON_Delete(onlineDxgList);
		onlineDxgList = NULL;
	}
    onlineDxgList = cJSON_CreateArray();
    API_TB_SelectBySortByName(TB_NAME_IXG_LIST, onlineDxgList, NULL, 0);
    SortTableView(onlineDxgList, "GW_ID", 0);
    //printf_json(onlineDxgList,"DxgList");

    if(onlineDxgView)
	{
		cJSON_Delete(onlineDxgView);
		onlineDxgView = NULL;
	}
    onlineDxgView = cJSON_CreateArray();
    cJSON *item;
    int imCnt,imResCnt,id;
    char buf[10];
    cJSON_ArrayForEach(item, onlineDxgList)
    {
        id = GetEventItemInt(item, "GW_ID"); 
        imResCnt=GetDtImCnt(id);
        imCnt = cJSON_GetArraySize(cJSON_GetObjectItemCaseSensitive(item, "DT_IM"));   
        snprintf(buf,10,"%d/%d",imCnt, imResCnt);
        cJSON* record = cJSON_CreateObject();
        cJSON_AddItemToObject(record, "GW_ID", cJSON_Duplicate(cJSON_GetObjectItemCaseSensitive(item, "GW_ID"), 1));
		cJSON_AddItemToObject(record, "DT_IM", cJSON_CreateString(buf));
        cJSON_AddStringToObject(record,"INFO","info");
        cJSON_AddItemToArray(onlineDxgView, record);
    }
	return onlineDxgView;
}

static void dxgview_control(const char* funcName)
{
    if(funcName)
    {
        TipsDisplay("DXG Refresh...");
        API_Event_By_Name(EventDXGListRefresh);
        snprintf(batchFunc, 10, "%s", funcName);        
    }
}
void dxgmanage_show(void)
{
    cJSON* para = cJSON_Parse(DXG_VIEW_PARA);
	int callback = DxgCellClick;
	cJSON_ReplaceItemInObjectCaseSensitive(para, "CellCallback", cJSON_CreateNumber(callback));
    cJSON* func = cJSON_CreateObject();
	cJSON* funcName = cJSON_AddArrayToObject(func, "Name");
	cJSON_AddItemToArray(funcName, cJSON_CreateString("Refresh"));
	cJSON_AddItemToArray(funcName, cJSON_CreateString("Test"));
	cJSON_AddItemToArray(funcName, cJSON_CreateString("Reboot"));
	cJSON_AddItemToArray(funcName, cJSON_CreateString("Restore"));
	cJSON_AddItemToArray(funcName, cJSON_CreateString("Upgrade"));
	cJSON_AddNumberToObject(func, "Callback", (int)dxgview_control);
	cJSON_ReplaceItemInObjectCaseSensitive(para, "Function", func);
	cJSON_AddItemToObject(para, "ReturnCallback", cJSON_CreateNumber((int)dxgmanage_close));
    TB_MenuDisplay(&dxgView, GetDXGTb(), "DXG Manage", para);
	cJSON_Delete(para);
    iperfTestState = 0;
    batchUpdateState = 0;
    API_Event_By_Name(EventDXGListRefresh);
}

void dxgmanage_close(void)
{
    if(onlineDxgView)
	{
		cJSON_Delete(onlineDxgView);
		onlineDxgView = NULL;
	}
    if(onlineDxgView)
	{
		cJSON_Delete(onlineDxgView);
		onlineDxgView = NULL;
	}
    iperfTestState = 0;
    batchUpdateState = 0;
    Vtk_KbReleaseByMode(&kb);
    TB_MenuDelete(&dxgupdateView);
    TB_MenuDelete(&dxginfoView);
    TB_MenuDelete(&dxgView);
}

void return_dxgmanage(void)
{
    if(dxgupdateView)    
        TB_MenuDelete(&dxgupdateView);
    if(dxginfoView)
        TB_MenuDelete(&dxginfoView);
    if(kb)
        Vtk_KbReleaseByMode(&kb); 
}
/**********************
 * DXG Refresh
 * ********************/
void dxgRefresh(void)
{
    if(dxgView)
    {
        TB_MenuDisplay(&dxgView, GetDXGTb(), NULL, NULL);
    }
}
int EventDXGListRefreshCallback(cJSON *cmd)
{
    API_UpdateIxgList2();
}
int EventDXGListReportCallback(cJSON *cmd)
{
    if(dxgView ==NULL)
    {
        return 1;
    }
    vtk_lvgl_lock(); 
    dprintf("EventDXGListReportCallback111 \n");
    TipsClose();
    dxgRefresh();
    if (!strcmp(batchFunc, "Test"))
    {
        IperfTestStart();
    }
    else if (!strcmp(batchFunc, "Reboot"))
    {
        dxg_reboot_all();
    }
    else if (!strcmp(batchFunc, "Restore"))
    {
        dxg_restore_all();
    }
    else if (!strcmp(batchFunc, "Upgrade"))
    {
        dxg_update_all();
    }
    memset(batchFunc,0,10);
    vtk_lvgl_unlock(); 
}


/**********************
 * DXG Info
 * ********************/
static cJSON* GetDXGInfo(int dxgid)
{
	cJSON* where = cJSON_CreateObject();
	cJSON* view = cJSON_CreateArray();
    cJSON* DxgInfo = cJSON_CreateArray();
    cJSON_AddNumberToObject(where, "GW_ID", dxgid);
    API_TB_SelectBySortByName(TB_NAME_IXG_LIST, view, where, 0);
    cJSON* dxg =  cJSON_GetArrayItem(view,0);
    cJSON_DeleteItemFromObject(dxg,"IXG_ID");
    dxgip = inet_addr(GetEventItemString(dxg, "IP_ADDR"));
	char info[100];
    API_GetRemotePbString(dxgip, PB_FW_VER, info);
    cJSON_AddStringToObject(dxg,PB_FW_VER,info);
    int length = cJSON_GetArraySize(dxg);
    for (int i = 0; i < length; i++)
    {   
        cJSON* item = cJSON_GetArrayItem(dxg, i);
        cJSON* record = cJSON_CreateObject();
        if(item == NULL)
            continue;
        cJSON_AddItemToObject(record, "Name", cJSON_CreateString(item->string));    
        if(cJSON_IsString(item))
        {
            cJSON_AddItemToObject(record, "Value", cJSON_CreateString(item->valuestring));    
        }
        else if(cJSON_IsNumber(item))
        {
            cJSON_AddItemToObject(record, "Value", cJSON_CreateNumber(item->valueint));   
        }
        else
        {
            continue;
        }
        cJSON_AddItemToArray(DxgInfo, record);
    }
    //cJSON_Delete(dxg);
    cJSON_Delete(where);
    cJSON_Delete(view);
    return DxgInfo;
}

static void dxginfo_control(const char* funcName)
{
    if(funcName)
    {
        if(!strcmp(funcName, "Reboot"))
        {
            if(dxgip)
            {
                RebootDxg(dxgip);
                return_dxgmanage();
            }  
        }
        else if (!strcmp(funcName, "Restore"))
        {
            if(dxgip)
            {
                RestoreDxg(dxgip);
                return_dxgmanage();
            }
        }
        else if (!strcmp(funcName, "Upgrade"))
        {
            if(dxgip)
            {
                batchUpdateState = 0;
                dxg_update_show();
            }
        }
    }
}
void dxg_info_show(int dxgid)
{
	//char buf[10];
    cJSON* para = cJSON_Parse(ABOUT_VIEW_PARA);
    cJSON* view = GetDXGInfo(dxgid);
	//sprintf(buf,"%d",dxgid);
    //cJSON_ReplaceItemInObjectCaseSensitive(cJSON_GetObjectItemCaseSensitive(para, "Filter"), "Value", cJSON_CreateString(buf));
    cJSON* func = cJSON_CreateObject();
	cJSON* funcName = cJSON_AddArrayToObject(func, "Name");
	cJSON_AddItemToArray(funcName, cJSON_CreateString("Reboot"));
	cJSON_AddItemToArray(funcName, cJSON_CreateString("Restore"));
	cJSON_AddItemToArray(funcName, cJSON_CreateString("Upgrade"));
	cJSON_AddNumberToObject(func, "Callback", (int)dxginfo_control);
	cJSON_ReplaceItemInObjectCaseSensitive(para, "Function", func);
    TB_MenuDisplay(&dxginfoView, view, "Info", para);
    
    cJSON_Delete(view);
	cJSON_Delete(para);
}

/**********************
 * DXG Upgrade show
 * ********************/
static cJSON* GetDxgUpdatePara(void)
{
    cJSON* UpdateInfo = cJSON_CreateArray();
    //获取IXG的sever、code
    API_Para_Read_String(updatestr[0], onlineUpgrade_Server);
    API_Para_Read_String(updatestr[1], onlineUpgrade_Code);
    for (int i = 0; i < 2; i++)
    {
        cJSON* record = cJSON_CreateObject();
        cJSON_AddStringToObject(record, "Name", updatestr[i]);  
        cJSON_AddStringToObject(record, "Value", i==0? onlineUpgrade_Server : onlineUpgrade_Code);    
        cJSON_AddItemToArray(UpdateInfo, record);
    }
    return UpdateInfo;
}

void update_servercode(void)
{
    //cJSON* para = cJSON_Parse(DXG_UPDATE_PARA);
	//int callback = updateCellClick;
	//cJSON_ReplaceItemInObjectCaseSensitive(para, "CellCallback", cJSON_CreateNumber(callback));
    cJSON* view = GetDxgUpdatePara();
    TB_MenuDisplay(&dxgupdateView, view, NULL, NULL);
    cJSON_Delete(view);
	//cJSON_Delete(para);
}
static int* confirm_set_servercode(const char* val,void *user_data)
{
    if(strlen(val) != 8)
	{
		return 0;
	}
    API_Para_Write_String(updatestr[1], val);
    update_servercode();
}
static int* confirm_set_server(const char* val,void *user_data)
{
    //dprintf("confirm_set_server = %s\n", val);
    API_Para_Write_String(updatestr[0], val);
    update_servercode();
}

void updateCellClick(int line, int col)
{
    dprintf("line = %d, col =%d\n", line, col);
    if(updateState)
    {
        return;
    }
    if(line == 0)
    {
        Vtk_KbMode2Init(&kb,Vtk_KbMode2,NULL,updatestr[0],15,"^[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}$",onlineUpgrade_Server,"tip",confirm_set_server,NULL);
    }
    else if(line == 1)
    {
        Vtk_KbMode1Init(&kb,Vtk_KbMode1, API_Para_Read_String2(KeybordLanguage),updatestr[1],8,NULL,onlineUpgrade_Code,"tip",confirm_set_servercode,NULL);
    }
}
static void dxgupdate_control(const char* funcName)
{
    if(funcName)
    {
        if(!strcmp(funcName, "Install"))
        {
            FwUpgrade_Process();
        }
    }    
}
void dxg_update_show(void)
{
    networkInfo = 0; 
    updateState = 0;
    cJSON* para = cJSON_Parse(DXG_UPDATE_PARA);
	int callback = updateCellClick;
	cJSON_ReplaceItemInObjectCaseSensitive(para, "CellCallback", cJSON_CreateNumber(callback));
    cJSON* func = cJSON_CreateObject();
	cJSON* funcName = cJSON_AddArrayToObject(func, "Name");
	cJSON_AddItemToArray(funcName, cJSON_CreateString("Install"));
	cJSON_AddNumberToObject(func, "Callback", (int)dxgupdate_control);
	cJSON_ReplaceItemInObjectCaseSensitive(para, "Function", func);
    cJSON* view = GetDxgUpdatePara();
    TB_MenuDisplay(&dxgupdateView, view, "Upgrade", para);
    
    cJSON_Delete(view);
	cJSON_Delete(para);
}


static void Send_UpdateEvent(int opt, int ftp, char* file)
{
	cJSON *send_event=cJSON_CreateObject();
	cJSON_AddStringToObject(send_event, "EventName","EventUpdateRequest");
	cJSON_AddStringToObject(send_event, "OPERATE", opt==0? "CHECK":"INSTALL");
	cJSON_AddStringToObject(send_event, "DOWNLOAD_TYPE", ftp==0? "TFTP":"FTP");
	cJSON_AddStringToObject(send_event, "REPORT_IP",GetSysVerInfo_IP());
	cJSON_AddStringToObject(send_event, "SERVER", ftp==1? onlineUpgrade_Server : GetSysVerInfo_IP());
	cJSON_AddStringToObject(send_event, "DOWNLOAD_FILE",file);
	API_XD_EventJson(dxgip,send_event);
	cJSON_Delete(send_event);
}

static void Send_UpdateReport(char* msg)
{
    cJSON* event = cJSON_CreateObject();
    cJSON_AddStringToObject(event, EVENT_KEY_EventName, EventUpdateReportToRemote);
    cJSON_AddStringToObject(event, "MSG", msg);
    API_Event_Json(event);
    cJSON_Delete(event);
}

static int FTP_Down_CB(int state, FTP_FILE_STATISTICS statistics, void* data)
{
	switch(state)
	{
		case FTP_STATE_DOWN_ING:
			break;
		case FTP_STATE_DOWN_OK:
			snprintf(update_file_dir, 100, "%s%s.txt", IXG_Update_Folder, onlineUpgrade_Code);
			Send_UpdateEvent(0, 0, update_file_dir);
			break;	
		case FTP_STATE_PARA_ERR:
		case FTP_STATE_INIT_ERR:
		case FTP_STATE_DOWN_ERR:
			Send_UpdateReport("CHECK_ERROR");
			break;			
		default:
			break;
	}
	return 0;
}
static int FTP_Down2_CB(int state, FTP_FILE_STATISTICS statistics, void* data)
{
	char disp[40];
	switch(state)
	{
		case FTP_STATE_DOWN_ING:
			snprintf(disp, 40, "FTP_DOWNLOAD_ING %u%%", (int)(statistics.now*100.0/statistics.total));
			Send_UpdateReport(disp);
			break;
		case FTP_STATE_DOWN_OK:
			Send_UpdateReport("FTP_DOWNLOAD_OK");
			snprintf(update_file_dir, 100, "%s%s", IXG_Update_Folder, onlineUpgrade_Code);
			Send_UpdateEvent(1, 0, update_file_dir);
			break;	
		case FTP_STATE_PARA_ERR:
		case FTP_STATE_INIT_ERR:
		case FTP_STATE_DOWN_ERR:
			Send_UpdateReport("FTP_DOWNLOAD_ERROR");
			break;			
		default:
			break;
	}
	return 0;
}
static int UpdateTimeout(int timing)
{
    if(timing >= 1)
	{
        if(Countdown)
        {
            API_TIPS_Mode_CD(Countdown);
            Countdown--;
        }
		if(updateState)
		{
            MainMenu_Reset_Time();
			updateTimeCnt++;
			if (updateTimeCnt>=15)	//
			{
				Send_UpdateReport("CHECK_TIMEOUT");                
				return 2;
			}
		}
		
		return 1;
	}
	return 0;
}
void FwUpgrade_Process(void)
{
    if(updateState == 0)
    {
        updateState = 1;
        Send_UpdateReport("CHECK_START");
        if(networkInfo == 0)
        {
            snprintf(update_file_dir, 100, "%s/%s.txt", UPDATE_CODE_CLOUD_DIR, onlineUpgrade_Code);
            if( access( IXG_Update_Folder, F_OK ) < 0 )
            {
                create_multi_dir(IXG_Update_Folder);
            }
            FTP_StartDownload(onlineUpgrade_Server, update_file_dir, IXG_Update_Folder, NULL,  FTP_Down_CB);
        }
        else
        {
            Send_UpdateEvent(0, 1, onlineUpgrade_Code);
        }	
	updateTimeCnt = 0;
	API_Add_TimingCheck(UpdateTimeout, 1);	
    }
}
void delete_update_tempfile(void)
{
	char cmd_buff[200];
	if( access( IXG_Update_Folder, F_OK ) == 0 )
	{
		snprintf(cmd_buff, 200, "rm -r %s", IXG_Update_Folder);
		system(cmd_buff);
	}
}
void DxgUpdate_Report(char* msg)
{
    char display[200] = {0};
    int ret = 0;
	updateTimeCnt = 0;
    dprintf("DxgUpdate_Report=%s UpdateDxgIndex=%d\n", msg, batchUpdateDxgIndex);
    //vtk_lvgl_lock();
    if(dxgupdateView)
    {
        //dprintf("batchUpdateDxgIndex=%d\n", batchUpdateDxgIndex);
        if(batchUpdateState)
        {
            if(batchUpdateDxgIndex < onlineDxgCnt)
            {
                sprintf(display, "DXG%d %s", GetEventItemInt(cJSON_GetArrayItem(onlineDxgList,batchUpdateDxgIndex), "IXG_ID"), msg);
                TipsDisplay(display);
            }
        }
        else
        {
            TipsDisplay(msg);
        }
        
        if(!strcmp(msg, "CHECK_PASSED"))	
        {
            if(networkInfo == 0)
            {
                Send_UpdateReport("FTP_DOWNLOAD_START");
                snprintf(update_file_dir, 100, "%s/%s", UPDATE_CODE_CLOUD_DIR, onlineUpgrade_Code);
                FTP_StartDownload(onlineUpgrade_Server, update_file_dir, IXG_Update_Folder, NULL,  FTP_Down2_CB);
            }
            else
            {
                Send_UpdateEvent(1, 1, onlineUpgrade_Code);
            }
        }
        else if(!strcmp(msg, "CHECK_ERROR") || !strcmp(msg, "CHECK_TIMEOUT"))
        {
            updateState = 0;
            //TipsClose();         
        }
        else if(!strcmp(msg, "DOWNLOAD_OK") || !strcmp(msg, "INSTALLING"))
        {
            
        }
        else if(!strcmp(msg, "DOWNLOAD_ERROR") || !strcmp(msg, "DOWNLOAD_STOP"))
        {
            updateState = 0;
            //TipsClose();
        }
        else if(!strcmp(msg, "INSTALL_OK"))
        {
            updateState = 2;
            //delete_update_tempfile();
        }

        if(updateState==2 || updateState==0)
        {
            if(batchUpdateState)
            {
                batchUpdateDxgIndex++;
                if(batchUpdateDxgIndex >= onlineDxgCnt)
                {
                    ret = 1;
                    goto end;                    
                }
                usleep(500*1000);
                dxgip = inet_addr(GetEventItemString(cJSON_GetArrayItem(onlineDxgList,batchUpdateDxgIndex), "IP_ADDR"));
                updateState = 0;
                Countdown = 20*(onlineDxgCnt-batchUpdateDxgIndex);
                FwUpgrade_Process();
            }
            else
            {
                ret = 1;
            }
        }
end:
        if(ret)
        {
            updateState = 0;
            batchUpdateState = 0;
            Countdown = 0;
            API_Del_TimingCheck(UpdateTimeout);
            delete_update_tempfile();
            TipsClose();
            return_dxgmanage();
            API_TIPS_Ext("Upgrade end");
        }    
    } 
    //vtk_lvgl_unlock();       
}

int getbatchState(void)
{
    return batchUpdateState;
}

/**********************
 * DXG IperfTest
 * ********************/

static int IperfTestTimeout(int timing)
{
    if(timing >= 1)
	{
		if(iperfTestState)
		{
			updateTimeCnt++;
			if (updateTimeCnt>=10)	//5
			{
				cJSON* event = cJSON_CreateObject();
                cJSON_AddStringToObject(event, EVENT_KEY_EventName, Event_IPERF_TEST_REPORT);
                cJSON_AddStringToObject(event, "SOURCE_IP", GetEventItemString(cJSON_GetArrayItem(onlineDxgList,iperfTestDxgIndex), "IP_ADDR"));
                cJSON_AddStringToObject(event, "TARGET_IP", my_inet_ntoa2(iperfTestDs1IpAddr));
                cJSON_AddStringToObject(event, "REPORT_IP", GetSysVerInfo_IP_by_device(GetNetDeviceNameByTargetIp(iperfTestDs1IpAddr)));
                cJSON_AddStringToObject(event, "REPORT_MSG", "Error");
                API_Event_Json(event);
                cJSON_Delete(event);
				return 2;
			}
		}
		
		return 1;
	}
	return 0;
}
static int IperfTestDs1AndIm(int ds1IpAddr, int imIndex)
{
	char display[200] = {0};
	char ipAddr[200];
	int id;
	int ret = 0;
	cJSON *item = cJSON_GetArrayItem(onlineDxgList, imIndex);
	if(item)
	{
		GetJsonDataPro(item, "IP_ADDR", ipAddr);
		GetJsonDataPro(item, "IXG_ID", &id);
        sprintf(display, "Iperf DXG%d start", id);
        TipsDisplay(display);
		API_IperfTestStart(inet_addr(ipAddr), ds1IpAddr);
        API_Add_TimingCheck(IperfTestTimeout, 1);
	}
	updateTimeCnt = 0;
    MainMenu_Reset_Time();
	return ret;
}
void IperfTestStart(void)
{
    onlineDxgCnt = cJSON_GetArraySize(onlineDxgList);
    if(onlineDxgCnt == 0)
    {
        return;
    }	
    iperfTestState = 1;
    iperfTestDs1IpAddr = inet_addr(GetSysVerInfo_IP());
    
    iperfTestDxgIndex = 0;
    IperfTestDs1AndIm(iperfTestDs1IpAddr, iperfTestDxgIndex);
}

static void DisplayIperfTestResult(int id, char* bandwidth, char* packetLossRate)
{
    int i,ixgid;
    cJSON *item = NULL;
    if(dxgView)
    {
        for(i = 0; i < onlineDxgCnt; i++)
        {
            item = cJSON_GetArrayItem(onlineDxgView, i);
            ixgid = GetEventItemInt(item, "GW_ID");
            if(id == ixgid)
            {
                cJSON_ReplaceItemInObjectCaseSensitive(item, "INFO", cJSON_CreateString(bandwidth));  
            }
        }        
        TB_MenuDisplay(&dxgView, onlineDxgView, NULL, NULL);        
    }
}
int EventIperfTestReportToDxgconfig(cJSON *cmd)
{
    char display[200] = {0};
	char* msg = GetEventItemString(cmd, "REPORT_MSG");
	int id = 0;
	dprintf("IperfTestReport = %s iperfTestDxgIndex=%d\n", msg, iperfTestDxgIndex);
    if(dxgView ==NULL || iperfTestState==0)
    {
        return 1;
    }
    updateTimeCnt = 0;
    GetJsonDataPro(cJSON_GetArrayItem(onlineDxgList,iperfTestDxgIndex), "IXG_ID", &id);
	if(!strcmp(msg, "Start"))
	{

	}
	else if(!strcmp(msg, "End") || !strcmp(msg, "Error") || !strcmp(msg, "Target ip error"))
	{
		if(!strcmp(msg, "Error"))
		{
			sprintf(display, "Iperf DXG%d error", id);
		}
		else if(!strcmp(msg, "Target ip error"))
		{
			sprintf(display, "Iperf DS1 ip error");
		}
		else if(!strcmp(msg, "End"))
		{
			sprintf(display, "Iperf DXG%d end", id);
		}
		TipsDisplay(display);
		//sleep(1);
        iperfTestDxgIndex++;
        if(iperfTestDxgIndex >= onlineDxgCnt)
        {
            iperfTestState = 0;
            API_Del_TimingCheck(IperfTestTimeout);
            //TipsDisplay("Iperf test end");
            TipsClose();
            return 1;
        }
		IperfTestDs1AndIm(iperfTestDs1IpAddr, iperfTestDxgIndex);
	}
	else
	{
		char bandwidth[50] = {0};
		char packetLossRate[50] = {0};
		sscanf(msg, "%*s%*s%s%s", bandwidth, packetLossRate);
        //dprintf("bandwidth = %s, packetLossRate =%s\n", bandwidth, packetLossRate);
        sprintf(display, "Iperf DXG%d %s %s", id, bandwidth, packetLossRate);
		TipsDisplay(display);
        DisplayIperfTestResult(id, bandwidth, packetLossRate);
	}

	return 1;
}