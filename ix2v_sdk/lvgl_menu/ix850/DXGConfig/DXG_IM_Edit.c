#include <stdio.h>
#include "table_display.h"
#include "menu_utility.h"
#include "Common_Setting_TableView.h"
#include "obj_TableSurver.h"
#include "obj_lvgl_msg.h"
#include "task_Event.h"
#include "Common_TableView.h"
#include "Common_Record.h"

#define DTIM_VIEW_PARA		"{\"Filter\":{\"Name\":\"GW_ID\",\"Value\":\" \",\"Callback\":0},\"HeaderEnable\":true,\"CellCallback\":0,\"ColumnWidth\":[140,200,140,140],\"Function\":{\"Name\":[],\"Callback\":0}}"
#define RES_EDITOR_DESCRIPTION  "{\"Check\":{\"IX_ADDR\":\"^[0-9]{10}$\",\"GW_ADDR\":\"^[0-9]{10}$\",\"GW_ID\":\"(^[1-9]{1}$)|(^(1|2)[0-9]{1}$)|(^3[0-2]{1}$)\",\"IM_ID\":\"(^[1-9]{1}$)|(^(1|2)[0-9]{1}$)|(^3[0-2]{1}$)\"},\"Translate\":{\"IX_ADDR\":\"IX_ADDR\"}}"

static TB_VIWE* dtimView = NULL;
static RECORD_CONTROL* dtimRecord = NULL;

static int dxg_id;
static cJSON *edit_record=NULL;

int GetDtImCnt(int id)
{
	cJSON* where = cJSON_CreateObject();
    cJSON_AddNumberToObject(where, "GW_ID", id);
    cJSON_AddStringToObject(where, "IX_TYPE", "IM");
	int imResCnt = API_TB_CountByName(TB_NAME_HYBRID_MAT, where);
	cJSON_Delete(where);
	return imResCnt;
}

cJSON *FilterDtImTb(int id)
{
	cJSON* where = cJSON_CreateObject();
    cJSON_AddNumberToObject(where, "GW_ID", id);
    cJSON_AddStringToObject(where, "IX_TYPE", "IM");
	cJSON* view = cJSON_CreateArray();
	cJSON* tpl=cJSON_CreateObject();
	//cJSON_AddNumberToObject(tpl,"GW_ID",1);
	cJSON_AddNumberToObject(tpl,"IM_ID",1);
	//cJSON_AddStringToObject(tpl,"IX_ADDR","");
	cJSON_AddStringToObject(tpl,"IX_NAME","");
	cJSON_AddStringToObject(tpl,"G_NBR","");
	cJSON_AddStringToObject(tpl,"L_NBR","");
	cJSON_AddItemToArray(view,tpl);

	API_TB_SelectBySortByName(TB_NAME_HYBRID_MAT, view, where, 0);
	cJSON_Delete(where);
	cJSON_DeleteItemFromArray(view,0);
	#if 0
	char *str=cJSON_Print(view);
	printf("FilterDtImTb =%s\n",str);
	free(str);
	#endif
	return view;
}
cJSON *CreateDtIm_Editable(int new)
{
	cJSON *edit_able=cJSON_CreateArray();
	if(new)
	{
		cJSON_AddItemToArray(edit_able,cJSON_CreateString("IM_ID"));
	}
	cJSON_AddItemToArray(edit_able,cJSON_CreateString("G_NBR"));
	cJSON_AddItemToArray(edit_able,cJSON_CreateString("L_NBR"));
	cJSON_AddItemToArray(edit_able,cJSON_CreateString("IX_NAME"));
	return edit_able;
}
cJSON *CreateNewDtIm_Item(void)
{
	char buf[100];
	int im_id=1;
	cJSON* tpl=cJSON_CreateObject();
	cJSON* dxg_tb=FilterDtImTb(dxg_id);
	int size;
	if((size=cJSON_GetArraySize(dxg_tb))>0)
	{
		cJSON *old_item=cJSON_GetArrayItem(dxg_tb,size-1);
		GetJsonDataPro(old_item,"IM_ID", &im_id);
		if(++im_id>=33)
		{
			im_id=1;
		}
			
	}
	cJSON_Delete(dxg_tb);
	//cJSON_AddNumberToObject(tpl,"GW_ID",dxg_id);
	cJSON_AddNumberToObject(tpl,"IM_ID",im_id);
	//sprintf(buf,"0099%02d%02d01", dxg_id,im_id);
	//cJSON_AddStringToObject(tpl,"IX_ADDR",buf);
	sprintf(buf,"DT-IM-%02d",im_id);
	cJSON_AddStringToObject(tpl,"IX_NAME",buf);
	cJSON_AddStringToObject(tpl,"G_NBR","-");
	cJSON_AddStringToObject(tpl,"L_NBR","-");
	return tpl;
}
void ExitDtImEdit(void)
{
    if(edit_record!=NULL)
    {
        cJSON_Delete(edit_record);
    } 
    edit_record = NULL;  
}
int EditOneDtIm(cJSON *item)
{
	//cJSON *key1=cJSON_GetObjectItemCaseSensitive(item,"GW_ID");
	cJSON *key=cJSON_GetObjectItemCaseSensitive(item,"IM_ID");
	if(key==NULL)
		return -1;
	cJSON *where=cJSON_CreateObject();
	cJSON* View=cJSON_CreateArray();
	int ret=0;
	cJSON_AddNumberToObject(where,"GW_ID",dxg_id);
	cJSON_AddNumberToObject(where,"IM_ID",key->valueint);
	if(API_TB_SelectBySortByName(TB_NAME_HYBRID_MAT, View, where, 0))
	{
		ret = API_TB_UpdateByName(TB_NAME_HYBRID_MAT, where, item);
	}
	else
	{
		char ixAddr[20];
		cJSON *dtImRecord = cJSON_CreateObject();
		snprintf(ixAddr, 20, "0099%02d%02d01", dxg_id, key->valueint);
		cJSON_AddStringToObject(dtImRecord, "IX_ADDR", ixAddr);
		snprintf(ixAddr, 20, "0099%02d0001", dxg_id);
		cJSON_AddStringToObject(dtImRecord, "GW_ADDR", ixAddr);
		cJSON_AddStringToObject(dtImRecord, "IX_TYPE", "IM");
		cJSON_AddStringToObject(dtImRecord, "CONNECT", "DT");
		cJSON_AddNumberToObject(dtImRecord, "GW_ID", dxg_id);
		cJSON_AddNumberToObject(dtImRecord, "IM_ID", key->valueint);
		cJSON_AddStringToObject(dtImRecord, "G_NBR", cJSON_GetObjectItemCaseSensitive(item,"G_NBR")->valuestring);
		cJSON_AddStringToObject(dtImRecord, "L_NBR", cJSON_GetObjectItemCaseSensitive(item,"L_NBR")->valuestring);
		cJSON_AddStringToObject(dtImRecord, "IX_NAME", cJSON_GetObjectItemCaseSensitive(item,"IX_NAME")->valuestring);
		ret = API_TB_AddByName(TB_NAME_HYBRID_MAT, dtImRecord);
		cJSON_Delete(dtImRecord);
	}
	cJSON_Delete(where);
	cJSON_Delete(View);

	if(ret)
	{
		MenuDtIm_Fresh();
        API_Event_By_Name(Event_MAT_Update);
	}
	ExitDtImEdit();
	return ret;
}
int DeleteOneDtIm(cJSON *item)
{
	//cJSON *key1=cJSON_GetObjectItemCaseSensitive(item,"GW_ID");
	cJSON *key=cJSON_GetObjectItemCaseSensitive(item,"IM_ID");
	if(key==NULL)
		return -1;
	cJSON *where=cJSON_CreateObject();
	cJSON_AddNumberToObject(where,"GW_ID",dxg_id);
	cJSON_AddNumberToObject(where,"IM_ID",key->valueint);
	if(API_TB_DeleteByName(TB_NAME_HYBRID_MAT, where))
	{
		MenuDtIm_Fresh();
        API_Event_By_Name(Event_MAT_Update);
	}
	cJSON_Delete(where);
	ExitDtImEdit();
	return 0;
}

void MenuDtIm_Fresh(void)
{
    cJSON* view;
    if(dtimView)
    {
        view = FilterDtImTb(dxg_id);
        TB_MenuDisplay(&dtimView, view, NULL, NULL);
        cJSON_Delete(view);
    }
}
void SetDxgId(int id)
{
	dxg_id = id;
}

/*****************************************/
static void save_record(void)
{     
	RC_MenuDelete(&dtimRecord);
	EditOneDtIm(edit_record);
}

static void del_record(void)
{  
	RC_MenuDelete(&dtimRecord);
	DeleteOneDtIm(edit_record);
}

static void cancel_record(const char* type, void* user_data)
{  
	RC_MenuDelete(&dtimRecord);
	ExitDtImEdit();
}

static void btn_click(const char* type, void* user_data)
{
    if(!strcmp(type, "Save"))
    {
        save_record();
    }else if(!strcmp(type, "Delete")){
        del_record();
    }
    else if(!strcmp(type, "Cancel")){
        cancel_record(NULL,NULL);
    }
} 
/***********************/

static void dtim_add_event(void)
{
    cJSON *edit_able;
    edit_record=CreateNewDtIm_Item();
    edit_able=CreateDtIm_Editable(1);
    //create_editor_menu(edit_record,edit_able,cJSON_Parse(RES_EDITOR_DESCRIPTION));

	cJSON* func = cJSON_CreateObject();
    if(func)
    {
        int callback = btn_click;
        int backcall = cancel_record;
        cJSON* btnname = cJSON_CreateArray();

        cJSON_AddItemToArray(btnname,cJSON_CreateString("Save"));
        cJSON_AddItemToArray(btnname,cJSON_CreateString("Delete"));
        //cJSON_AddItemToArray(btnname,cJSON_CreateString("Cancel"));

        cJSON_AddItemToObject(func,"BtnName",btnname);
        cJSON_AddNumberToObject(func, "BtnCallback", callback);
        cJSON_AddNumberToObject(func, "BackCallback", backcall);
    }
	cJSON *description = cJSON_Parse(RES_EDITOR_DESCRIPTION);
	RC_MenuDisplay(&dtimRecord,edit_record,edit_able, description,func,"DT-IM");

    cJSON_Delete(description);
    cJSON_Delete(edit_able);
	cJSON_Delete(func);
}

static void ImCellClick(int line, int col)
{
    cJSON *edit_able;
    dprintf("line = %d, col =%d\n", line, col);
    if(dtimView)
    {
        edit_record=cJSON_Duplicate(cJSON_GetArrayItem(dtimView->view, line), 1);
        //printf_json(edit_record,"CardCell");
        edit_able=CreateDtIm_Editable(0);
		//create_editor_menu(edit_record,edit_able,cJSON_Parse(RES_EDITOR_DESCRIPTION));

		cJSON* func = cJSON_CreateObject();
		if(func)
		{
			int callback = btn_click;
			int backcall = cancel_record;
			cJSON* btnname = cJSON_CreateArray();

			cJSON_AddItemToArray(btnname,cJSON_CreateString("Save"));
			cJSON_AddItemToArray(btnname,cJSON_CreateString("Delete"));
			//cJSON_AddItemToArray(btnname,cJSON_CreateString("Cancel"));

			cJSON_AddItemToObject(func,"BtnName",btnname);
			cJSON_AddNumberToObject(func, "BtnCallback", callback);
			cJSON_AddNumberToObject(func, "BackCallback", backcall);
		}

		cJSON *description = cJSON_Parse(RES_EDITOR_DESCRIPTION);
		RC_MenuDisplay(&dtimRecord,edit_record,edit_able, description,func,"DT-IM");

        cJSON_Delete(description);
        cJSON_Delete(edit_able);
		cJSON_Delete(func);
    }    	
}
static void FilterClick(cJSON* filter)
{
    dprintf("Name = %s, Value =%s\n", GetEventItemString(filter,  "Name"), GetEventItemString(filter,  "Value"));
}
static void dxgIm_control(const char* funcName)
{
    if(funcName)
    {
        if(!strcmp(funcName, "Add"))
        {
            dtim_add_event();
        }
    }    
}
static void dxgIm_return(void)
{
	dxgRefresh();
}
void EnterDxgImMenu(void)
{
	char buf[10];
	cJSON* para = cJSON_Parse(DTIM_VIEW_PARA);
	int callback = ImCellClick;
	cJSON_ReplaceItemInObjectCaseSensitive(para, "CellCallback", cJSON_CreateNumber(callback));
    callback = FilterClick;
    cJSON_ReplaceItemInObjectCaseSensitive(cJSON_GetObjectItemCaseSensitive(para, "Filter"), "Callback", cJSON_CreateNumber(callback));
	sprintf(buf,"%d",dxg_id);
    cJSON_ReplaceItemInObjectCaseSensitive(cJSON_GetObjectItemCaseSensitive(para, "Filter"), "Value", cJSON_CreateString(buf));
    cJSON* func = cJSON_CreateObject();
	cJSON* funcName = cJSON_AddArrayToObject(func, "Name");
	cJSON_AddItemToArray(funcName, cJSON_CreateString("Add"));
	//cJSON_AddItemToArray(funcName, cJSON_CreateString("Delete"));
	cJSON_AddNumberToObject(func, "Callback", (int)dxgIm_control);
	cJSON_ReplaceItemInObjectCaseSensitive(para, "Function", func);
	cJSON_AddItemToObject(para, "ReturnCallback", cJSON_CreateNumber((int)dxgIm_return));
    cJSON* view = FilterDtImTb(dxg_id);
    TB_MenuDisplay(&dtimView, view, "DT-IM Manage", para);
    
    cJSON_Delete(view);
	cJSON_Delete(para);
}
void DxgImMenuClose(void)
{
	TB_MenuDelete(&dtimView);
}