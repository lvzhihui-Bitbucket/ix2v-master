#include "cJSON.h"
#include "Common_Record.h"
#include "menu_utility.h"
#include "MenuHybridMatCreateNew.h"
#include "cJSON.h"
#include "obj_TableSurver.h"
#include "define_string.h"
#include "obj_PublicInformation.h"
#include "utility.h"
#include "task_Event.h"
#include "define_file.h"
#include "obj_IoInterface.h"

#define APT_MAT_IM_START  				"IM START"
#define APT_MAT_IM_END  				"IM END"
#define APT_MAT_NAME_PREFIX  			"NAME PREFIX"
#define APT_MAT_DESC  					"Describe"

#define APT_MAT_RECORD  "{\"IM START\":1,\"IM END\":8,\"NAME PREFIX\":\"DH-IM\"}" //编辑的记录
#define APT_MAT_PARA  "{\"Check\":{\"IM START\":\"(^[1-9]$)|(^[1-5][0-9]$)|(^6[0-4]$)\", \"IM END\":\"(^[1-9]$)|(^[1-5][0-9]$)|(^6[0-4]$)\"},\"Translate\":{}}" //编辑的记录校验


static cJSON* batchCreateAptMatRecord = NULL;
static RECORD_CONTROL* batchCreateAptMatMenu = NULL;

void Menu_CreateNewAptMat_Close(void);

void BatchCreateMatMenu(cJSON* record, RECORD_CONTROL** menu, BtnCallback cancelCallback, BtnCallback btnCallback, const char* title, char* btnString, ...)
{
	char* name;
	//可编辑字段
	const char* editEnable[] = {APT_MAT_IM_START, APT_MAT_IM_END, APT_MAT_NAME_PREFIX, APT_MAT_DESC};
	cJSON* editorTable =  cJSON_CreateStringArray(editEnable, sizeof(editEnable)/sizeof(editEnable[0]));
	cJSON* resEditordescription = cJSON_Parse(APT_MAT_PARA);

	cJSON* func = cJSON_CreateObject();
	cJSON* btnname = cJSON_AddArrayToObject(func,"BtnName");

	va_list valist;
	va_start(valist, btnString);
	for(name = btnString; name; name = va_arg(valist, char*))
	{
		cJSON_AddItemToArray(btnname, cJSON_CreateString(name));
	}
	va_end(valist);

	cJSON_AddNumberToObject(func, "BtnCallback", (int)btnCallback);
	cJSON_AddNumberToObject(func, "BackCallback", (int)cancelCallback);

    RC_MenuDisplay(menu, record, editorTable, resEditordescription, func, title);
    cJSON_Delete(editorTable);
    cJSON_Delete(resEditordescription);
    cJSON_Delete(func);
}

cJSON* CreateMatRecords(const cJSON* para, int maxRecordNbr)
{
	cJSON* retView = NULL;
	char tempString[100];
	int imStart = GetEventItemInt(para,  APT_MAT_IM_START);
	int imEnd = GetEventItemInt(para,  APT_MAT_IM_END);
	char* namePrefix = GetEventItemString(para,  APT_MAT_NAME_PREFIX);
	int index;
	int recordNbr = 0;

	#define APT_MAT_IM_MAX  				64
	#define APT_MAT_IM_MIN  				1

	retView = cJSON_CreateArray();

	if(imStart >= 1 && imEnd <= APT_MAT_IM_MAX && imStart >= 1 && imEnd <= APT_MAT_IM_MAX)
	{
		for(index = imStart; index <= imEnd && recordNbr <= maxRecordNbr; index++, recordNbr++)
		{
			cJSON* record = cJSON_CreateObject();
			cJSON_AddStringToObject(record, "SYS_TYPE", "DH-APT");

			snprintf(tempString, 100, "009900%02d01", index);
			cJSON_AddStringToObject(record, "IX_ADDR", tempString);
			cJSON_AddNumberToObject(record, "IM_ID", index);
			cJSON_AddStringToObject(record, "IX_TYPE", "IM");
			cJSON_AddStringToObject(record, "CONNECT", "DH");
			cJSON_AddStringToObject(record, "G_NBR", "");
			cJSON_AddStringToObject(record, "L_NBR", "");
			snprintf(tempString, 100, "%s%d", namePrefix, index);
			cJSON_AddStringToObject(record, "IX_NAME", tempString);
			cJSON_AddItemToArray(retView, record);
		}
	}
	return retView;
}

static void CreateNewAptMatCancel(const char* type, void* user_data)
{  
	Menu_CreateNewAptMat_Close();
}

static void AptMatClearAndRecreate(const cJSON* para)
{
	cJSON* record;
	cJSON* view = CreateMatRecords(para, 256);
	if(cJSON_GetArraySize(view))
	{
		int satRecords = 0;
		char tempString[200];
		char targetFile[200];
		char translate[100];

		API_TB_SaveByName(TB_NAME_APT_MAT);
		MakeDir(Backup_APT_MAT_Folder);
		snprintf(targetFile, 200, "%s/MAT_%s.json", Backup_APT_MAT_Folder, GetCurrentTime(tempString, 100, "%Y-%m-%d_%H:%M:%S"));
		CopyFile(TB_APT_MAT_FILE_NAME, targetFile);

		API_TB_DeleteByName(TB_NAME_APT_MAT, NULL);
		cJSON_ArrayForEach(record, view)
		{
			if(API_TB_AddByName(TB_NAME_APT_MAT, record))
			{
				satRecords++;
			}
		}

		snprintf(tempString, 200, "%s %d", get_language_text2("Recreate MAT list", translate, 100), satRecords);
		API_TIPS_Ext_Time(tempString, 1);
		API_TableModificationInform(TB_NAME_APT_MAT, GetEventItemString(para, APT_MAT_DESC));
		API_TB_SaveByName(TB_NAME_APT_MAT);
		API_Event_By_Name(Event_MAT_Update);
	}
	cJSON_Delete(view);

	Menu_CreateNewAptMat_Close();
} 

static void CreateNewAptMatFunction(const char* type, void* user_data)
{
    if(!strcmp(type, "Create"))
    {
		API_MSGBOX("About to clear MAT and recreate", 2, AptMatClearAndRecreate, batchCreateAptMatRecord);
    }
} 


void Menu_CreateNewAptMat_Init(void)
{
	char tempString[100];
	if(batchCreateAptMatRecord)
	{
		cJSON_Delete(batchCreateAptMatRecord);
	}
	batchCreateAptMatRecord = cJSON_Parse(APT_MAT_RECORD);
	cJSON_AddStringToObject(batchCreateAptMatRecord, "Describe", GetCurrentTime(tempString, 100, "%Y-%m-%d_%H:%M"));
	BatchCreateMatMenu(batchCreateAptMatRecord, &batchCreateAptMatMenu, CreateNewAptMatCancel, CreateNewAptMatFunction, "Create new MAT", "Create", NULL);
}

void Menu_CreateNewAptMat_Close(void)
{
	if(batchCreateAptMatMenu)
	{
		RC_MenuDelete(&(batchCreateAptMatMenu));
		batchCreateAptMatMenu = NULL;
	}

	if(batchCreateAptMatRecord)
	{
		cJSON_Delete(batchCreateAptMatRecord);
		batchCreateAptMatRecord = NULL;
	}
	API_Event_NameAndMsg(EventMenuSettingProcess, "SettingRefresh", NULL);
}