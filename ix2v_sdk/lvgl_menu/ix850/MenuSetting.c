#include "MenuSetting.h"
#include "utility.h"
#include "define_file.h"
#include "obj_lvgl_msg.h"
#include "IX850_SettingMenuResource.h"
#include "task_Beeper.h"
#include "cJSON.h"
#include "common_keyboard.h"
#include "obj_CommonSelectMenu.h"
#include "task_Event.h"
#include "obj_IoInterface.h"

#define SETTING_VIEW_BackgroundColor     lv_color_hex(0x41454D)
#define SETTING_VIEW_TextColor           lv_color_white()
#define SETTING_VIEW_NotEditTextColor    lv_color_hex(0x808080)
#define SETTING_VIEW_TextHeight          29
#define SETTING_VIEW_TitleHeight         73
#define SETTING_VIEW_LineHeight          73
#define SETTING_VIEW_TextSize            0
#define SETTING_VIEW_SYMBOL_TextSize     1
#define SETTING_VIEW_ReturnWidth         160
#define SETTING_VIEW_ScrWidth            480
#define SETTING_VIEW_HALF_SCR_W          (SETTING_VIEW_ScrWidth/2)

#define SETTING_VIEW_SYMBOL_COL             0
#define SETTING_VIEW_TEXT_COL(symbolFlag)   ((symbolFlag) ? 1 : 0)
#define SETTING_VIEW_VALUE_COL(valueFlag)   ((valueFlag) ? 2 : 3)
#define SETTING_VIEW_SYMBOL_W                           60
#define SETTING_VIEW_NEXT_PAGE_SYMBOL_W                 36
#define SETTING_VIEW_TEXT_W(symbolFlag, valueFlag)      (SETTING_VIEW_ScrWidth - ((symbolFlag) ? SETTING_VIEW_SYMBOL_W : 0) - ((valueFlag) ? SETTING_VIEW_HALF_SCR_W : SETTING_VIEW_NEXT_PAGE_SYMBOL_W) - 32)
#define SETTING_VIEW_VALUE_W                            (SETTING_VIEW_HALF_SCR_W - 32)

static SETTING_VIEW* settingRoot = NULL;
static SETTING_VIEW* settingCurPage = NULL;
static cJSON* ioParaFormat = NULL;
static char currentMenuName[100] = {0};
static SETTING_CHANGE_PAGE_DATA_T changePageData;

static SETTING_VIEW* SettingMenuDelete(SETTING_VIEW** tb);
static SETTING_VIEW* SettingMenuDisplay(SETTING_VIEW** disp, const char* fileName, const char* menuName, void* parent);
static void SettingMenuRefresh(SETTING_VIEW* disp);
static char* GetComboxDisplay(char* ioName, cJSON* ioData, char* buff, int buffLen);

static int* setting_text_edit_event_cb(const char* val,void *user_data);
static void table_edit_event_cb(lv_event_t* e);
cJSON *CreateVoiceCusCombox(void);
cJSON *CreateLanguageCusCombox(void);
cJSON *CreateVoiceCusCombox(void)
{
  cJSON* fileList = cJSON_CreateArray();
    cJSON* choseList = NULL;
	
    GetFileAndDirList(CustomerizedPromtDir, fileList, 1);
    cJSON* fileRecord;
	if(cJSON_GetArraySize(fileList)>0)
		choseList=cJSON_CreateArray();
    cJSON_ArrayForEach(fileRecord, fileList)
    {
        cJSON* fileName = cJSON_GetArrayItem(fileRecord, 3);
        if(cJSON_IsString(fileName))
        {
            char temp[200];
		cJSON *one_item=cJSON_CreateArray();
            strcpy(temp, fileName->valuestring);
		 cJSON_AddItemToArray(one_item, cJSON_CreateString(temp));
		 cJSON_AddItemToArray(one_item, cJSON_CreateString(temp));
            cJSON_AddItemToArray(choseList, one_item);
        }
    }
    cJSON_Delete(fileList);
	return choseList;
}

cJSON *CreateLanguageCusCombox(void)
{
  cJSON* fileList = cJSON_CreateArray();
    cJSON* choseList = NULL;
	cJSON* fileRecord;
    GetFileAndDirList(CustomerizedLanguageNandFolder, fileList, 0);
    
	if(cJSON_GetArraySize(fileList)>0)
		choseList=cJSON_CreateArray();
    cJSON_ArrayForEach(fileRecord, fileList)
    {
        cJSON* fileName = cJSON_GetArrayItem(fileRecord, 3);
        if(cJSON_IsString(fileName) && !StrcmpEnd(fileName->valuestring, ".json"))
        {
            char temp[200];
		cJSON *one_item=cJSON_CreateArray();	
            strcpy(temp, fileName->valuestring);
            temp[strlen(temp) - strlen(".json")] = 0;
            cJSON_AddItemToArray(one_item, cJSON_CreateString(temp));
		 cJSON_AddItemToArray(one_item, cJSON_CreateString(temp));
            cJSON_AddItemToArray(choseList, one_item);
        }
    }
    cJSON_Delete(fileList);
	return choseList;
}

static void SetSettingCurMenuName(const char* menuName)
{
    if(menuName)
    {
        snprintf(currentMenuName, 100, "%s", menuName);
    }
    else
    {
        snprintf(currentMenuName, 100, "%s", "");
    }
    //dprintf("----------------------------- currentMenuName = %s\n", currentMenuName);
}

static const char* GetSettingCurtMenuName(void)
{
    return currentMenuName;
}

static void LoadSettingIoData(void)
{
    if(!ioParaFormat)
    {
        char buf[200];
	cJSON *one_combox;
	cJSON *one_io;
	cJSON *cus_io;
	
        snprintf(buf, 200, "%s/io.json", SETTING_PAGE_DIR);
        ioParaFormat = GetJsonFromFile(buf);
	snprintf(buf, 200, "%s/io.json", CUS_SETTING_PAGE_DIR);
        cus_io = GetJsonFromFile(buf);
	if(cus_io)
	{
		cJSON_ArrayForEach(one_io,cus_io)
		{
			if(cJSON_GetObjectItemCaseSensitive(ioParaFormat,one_io->string))
			{
				cJSON_ReplaceItemInArray(ioParaFormat,one_io->string,cJSON_Duplicate(one_io,1));			
			}
			else
				cJSON_AddItemToObject(ioParaFormat,one_io->string,cJSON_Duplicate(one_io,1));
		}
		cJSON_Delete(cus_io);
		
	}
	one_combox=CreateVoiceCusCombox();
	if(one_combox)
	{
		one_io=cJSON_GetObjectItemCaseSensitive(ioParaFormat,VOICE_LANGUAGE_SELECT_DIR);
		if(one_io)
		{
			cJSON_ReplaceItemInObjectCaseSensitive(one_io,"com_item",one_combox);
		}
		else
			cJSON_Delete(one_combox);
	}
	one_combox=CreateLanguageCusCombox();
	if(one_combox)
	{
		one_io=cJSON_GetObjectItemCaseSensitive(ioParaFormat,CurLanguage);
		if(one_io)
		{
			cJSON_ReplaceItemInObjectCaseSensitive(one_io,"com_item",one_combox);
		}
		else
			cJSON_Delete(one_combox);
	}
    }
}

static cJSON* GetSettingIoData(void)
{
    LoadSettingIoData();
    return ioParaFormat;
}

static void FreeSettingIoData(void)
{
    if(ioParaFormat)
    {
        cJSON_Delete(ioParaFormat);
        ioParaFormat = NULL;
    }
}

static void SettingView_UpdateData(SETTING_VIEW* disp);

/*****************
 * 初始化表浏览器活动用的scr
 * **************/
static lv_obj_t* SettingView_CreatSrc(void)
{
    lv_obj_t* ui_table = lv_obj_create(NULL);
    lv_obj_remove_style_all(ui_table);
    lv_obj_set_size(ui_table, lv_disp_get_hor_res(NULL), lv_disp_get_ver_res(NULL));
    lv_obj_align(ui_table, LV_ALIGN_TOP_LEFT, 0, 0);
    lv_obj_set_style_bg_color(ui_table, SETTING_VIEW_BackgroundColor, 0);
    lv_obj_clear_flag(ui_table, LV_OBJ_FLAG_SCROLLABLE);
    return ui_table;
}

static void SettingView_HeadReturn(lv_event_t* e)
{
    SETTING_VIEW** disp = lv_event_get_user_data(e);
    if(!disp)
    {
        return;
    }

    if(!(*disp))
    {
        return;
    }

    SETTING_VIEW* tu = (*disp);

    if(tu->parent == tu)
    {
        SettingMenuDelete(disp);
    }
    else
    {
        API_SettingMenuReturn();
    }
}

static void SettingView_AddTitle(SETTING_VIEW** disp, char* txt)
{

    if(!disp || !(*disp))
    {
        return;
    }

    SETTING_VIEW* tu = (*disp);

    if(!tu->titlelabel)
    {
        lv_obj_t* btn = lv_obj_create(tu->menu);
        lv_obj_set_size(btn, SETTING_VIEW_ScrWidth, SETTING_VIEW_TitleHeight);   
        lv_obj_set_style_bg_color(btn, lv_color_make(26,82,196), 0);
        lv_obj_set_style_pad_all(btn,0,0);
        lv_obj_set_style_border_width(btn, 0, 0);

        lv_obj_add_event_cb(btn, SettingView_HeadReturn, LV_EVENT_CLICKED, disp);
        lv_obj_add_flag(btn, LV_OBJ_FLAG_CLICKABLE);
        lv_obj_clear_flag(btn, LV_OBJ_FLAG_SCROLLABLE);

        lv_obj_t* obj = lv_obj_create(btn);
        lv_obj_remove_style_all(obj);
        lv_obj_add_flag(obj, LV_OBJ_FLAG_EVENT_BUBBLE);
        lv_obj_set_style_bg_opa(obj,LV_OPA_MAX,0);
        lv_obj_set_size(obj,SETTING_VIEW_ReturnWidth,SETTING_VIEW_TitleHeight);
        lv_obj_set_style_bg_color(obj,lv_color_make(19,132,250),0);
        lv_obj_set_style_text_color(obj, lv_color_white(), 0);
        if(tu->titleimg)
        {
            lv_obj_del(tu->titleimg);
        }
        tu->titleimg = lv_img_create(obj);
        set_font_size(tu->titleimg,1);
        lv_obj_center(tu->titleimg);

        tu->titlelabel = lv_label_create(btn); 
        lv_obj_align(tu->titlelabel, LV_ALIGN_LEFT_MID, SETTING_VIEW_ReturnWidth, 0);
        lv_obj_set_size(tu->titlelabel, SETTING_VIEW_ScrWidth-SETTING_VIEW_ReturnWidth, SETTING_VIEW_TextHeight);
        lv_obj_set_style_text_align(tu->titlelabel,LV_TEXT_ALIGN_CENTER,0);
        set_font_size(tu->titlelabel, SETTING_VIEW_TextSize);
        lv_obj_set_style_text_color(tu->titlelabel, SETTING_VIEW_TextColor, 0);
    }    

    lv_img_set_src(tu->titleimg, tu->parent ? LV_VTK_LEFT : LV_VTK_CALL_RECORD1);
    vtk_label_set_text(tu->titlelabel,txt);
}

static const char* Read_PB_Or_IO_To_String(int isPb, const char* name, char* string, int len)
{
    if(!string || !name || !len)
    {
        return string;
    }
    string[0] = 0;
    cJSON* value = (isPb ? API_PublicInfo_Read(name) : API_Para_Read_Public(name));
    if(cJSON_IsString(value))
    {
        snprintf(string, len, "%s", value->valuestring);
    }
    else
    {
        cJSON_PrintPreallocated(value, string, len, 0);
    }

    return string;
}

static void draw_table_event_cb(lv_event_t* e)
{
    lv_obj_t* obj = lv_event_get_target(e);
    lv_obj_draw_part_dsc_t* dsc = lv_event_get_draw_part_dsc(e);
    SETTING_VIEW* disp = (SETTING_VIEW*)lv_event_get_user_data(e);
    lv_table_t* table = (lv_table_t*)obj;
    int cnt = lv_table_get_row_cnt(obj);
    
    if (dsc->part == LV_PART_ITEMS) {
        uint32_t row = dsc->id / lv_table_get_col_cnt(obj);
        uint32_t col = dsc->id - row * lv_table_get_col_cnt(obj);

        dsc->label_dsc->font = vtk_get_font_by_size(((col == SETTING_VIEW_SYMBOL_COL) && disp->symbolFlag[row]) ? SETTING_VIEW_SYMBOL_TextSize : SETTING_VIEW_TextSize);
        dsc->label_dsc->align = ((col == SETTING_VIEW_VALUE_COL(disp->valueFlag[row])) ? LV_TEXT_ALIGN_RIGHT : LV_TEXT_ALIGN_LEFT);

        //不可编辑的行，name列和value列字颜色变灰。
        dsc->label_dsc->color = (disp->editDisableFlag[row] && (col == SETTING_VIEW_TEXT_COL(disp->symbolFlag[row]) || col == SETTING_VIEW_VALUE_COL(disp->valueFlag[row]))) ? SETTING_VIEW_NotEditTextColor : SETTING_VIEW_TextColor;

        //超长字符串，设置显示偏移量
        if(((col == SETTING_VIEW_VALUE_COL(disp->valueFlag[row])) && (disp->textFlag[row] & 0x02)) || 
            ((col == SETTING_VIEW_TEXT_COL(disp->symbolFlag[row])) && (disp->textFlag[row] & 0x01)))
        {
            dsc->label_dsc->ofs_y = -((SETTING_VIEW_LineHeight - lv_font_get_line_height(vtk_get_font_by_size(SETTING_VIEW_TextSize)))/2);
        }
        else if((col == SETTING_VIEW_SYMBOL_COL) && disp->symbolFlag[row])
        {
            dsc->label_dsc->ofs_y = -12;
        }
        else
        {
            dsc->label_dsc->ofs_y = -((SETTING_VIEW_LineHeight - lv_font_get_line_height(vtk_get_font_by_size(SETTING_VIEW_TextSize))*2 - dsc->label_dsc->line_space)/2);
        }

        dsc->rect_dsc->bg_color = SETTING_VIEW_BackgroundColor;
        dsc->rect_dsc->border_width = 3;
        dsc->rect_dsc->border_color = lv_color_make(55,55,55);
        dsc->rect_dsc->border_side = LV_BORDER_SIDE_BOTTOM;
    }

    //for (int i = 0; i < cnt; i++) {
        //table->row_h[i] = (disp->textFlag[i] ? 80 : SETTING_VIEW_LineHeight);
    //}
}

static int* setting_text_edit_event_cb(const char* val,void *user_data)
{
    SETTING_VIEW* sv = (SETTING_VIEW*)user_data;
    int writeFlag = 0;
    cJSON* itemName = cJSON_GetArrayItem(sv->items, sv->cur_row);
    if(cJSON_IsString(itemName))
    {
        cJSON* item = cJSON_GetObjectItemCaseSensitive(sv->pageData, itemName->valuestring);
        if(cJSON_IsObject(item))
        {
            cJSON* ioName = cJSON_GetObjectItemCaseSensitive(item, "ParaName");
            if(cJSON_IsString(ioName))
            {
                cJSON* ioItem = cJSON_GetObjectItemCaseSensitive(GetSettingIoData(), ioName->valuestring);

                cJSON* para_type = cJSON_GetObjectItemCaseSensitive(ioItem, "para_type");
                char* msg = GetEventItemString(ioItem, "msg"); 
                if(para_type && strcmp(para_type->valuestring,"int")==0)
                {
                    writeFlag = API_Para_Write_Int(ioName->valuestring, atoi(val));
                }
                else
                {
                    cJSON* myValue = API_Para_Read_Public(ioName->valuestring);
                    if(myValue)
                    {
                        int type = myValue->type;
                        switch (type)
                        {
                            case cJSON_String:
                                writeFlag = API_Para_Write_String(ioName->valuestring, val); 
                                break;

                            default:
                                myValue = cJSON_Parse(val);
                                if(myValue && type == myValue->type)
                                {
                                    writeFlag = API_Para_Write_PublicByName(ioName->valuestring, myValue);
                                }
                                cJSON_Delete(myValue);
                                break;
                        }
                    }
                }
                
                if(writeFlag)
                {
                    API_SettingMenuProcess(msg);                  
                    lv_table_set_cell_value(sv->table,sv->cur_row, SETTING_VIEW_VALUE_COL(sv->valueFlag[sv->cur_row]), val); 
                }
                else
                {
                    API_TIPS_Ext_Time("Input error", 2);
                }
            }
        }

    }
}


static int ItemEditKpCb(const char* val,void *user_data)
{
    SETTING_VIEW* disp = (SETTING_VIEW*)user_data;
    cJSON* itemName = cJSON_GetArrayItem(disp->items, disp->cur_row);
    if(cJSON_IsString(itemName))
    {
        cJSON* item = cJSON_GetObjectItemCaseSensitive(disp->pageData, itemName->valuestring);
        if(cJSON_IsObject(item))
        {
            if(!strcmp(GetEventItemString(item, "type"), "ITEM_edit"))
            {
                cJSON_ReplaceOrAddItemInObjectCaseSensitive(item, "Value", cJSON_CreateString(val));
                lv_table_set_cell_value(disp->table, disp->cur_row, SETTING_VIEW_VALUE_COL(disp->valueFlag[disp->cur_row]), val);
                API_SettingMenuTextEditProcess(item);
            }
        }
    }
}

static void SelectSettingValueCallback(const char* chose, void* userdata)
{   
    if(!chose)
    {
        return;
    }
    SETTING_VIEW* disp = (SETTING_VIEW*)userdata;
    cJSON* element;
    char buff[200];

    cJSON* itemName = cJSON_GetArrayItem(disp->items, disp->cur_row);
    if(cJSON_IsString(itemName))
    {
        cJSON* item = cJSON_GetObjectItemCaseSensitive(disp->pageData, itemName->valuestring);
        if(cJSON_IsObject(item))
        {
            char* ioName = GetEventItemString(item, "ParaName");
            cJSON* ioItem = cJSON_GetObjectItemCaseSensitive(GetSettingIoData(), ioName);
            cJSON* comitem = cJSON_GetObjectItem(ioItem, "com_item");
            char* msg = GetEventItemString(ioItem, "msg");

            lv_table_set_cell_value(disp->table,disp->cur_row, SETTING_VIEW_VALUE_COL(disp->valueFlag[disp->cur_row]), chose);  
            cJSON_ArrayForEach(element, comitem)
            {
                cJSON* show = cJSON_GetArrayItem(element, 1);
                if(cJSON_IsString(show))
                {
                    snprintf(buff, 200, "%s", show->valuestring);
                }
                else
                {
                    cJSON_PrintPreallocated(show, buff, 200, 0);
                }
                
                if(!strcmp(buff, chose))
                {
                    cJSON* value = cJSON_GetArrayItem(element, 0);
                    API_Para_Write_PublicByName(ioName, value);
                    API_SettingMenuProcess(msg);
                    API_Event_NameAndMsg(EventMenuSettingProcess, "SettingRefresh", NULL);
                    break;
                }
            }
        }
    }
}

static void SelectSettingValue(const char* title, const char* ioName, const cJSON* ioData, SETTING_VIEW* disp)
{   
    if(!disp || !ioData || !ioName)
        return;
    static CHOSE_MENU_T choseMenuData;
    char valueBuff[200] = {0};
    cJSON* element;

    cJSON* comitem = cJSON_GetObjectItem(ioData, "com_item");
    cJSON* choseList = cJSON_CreateArray();
    cJSON_ArrayForEach(element, comitem)
    {
        cJSON* show = cJSON_GetArrayItem(element, 1);
        if(cJSON_IsString(show))
        {
            snprintf(valueBuff, 200, "%s", show->valuestring);
        }
        else
        {
            cJSON_PrintPreallocated(show, valueBuff, 200, 0);
        }
        cJSON_AddItemToArray(choseList, cJSON_CreateString(valueBuff));
    }

    choseMenuData.callback = SelectSettingValueCallback;
    choseMenuData.cbData = disp;
    CreateChooseMenu(title, choseList, GetComboxDisplay(ioName, ioData, valueBuff, 200), &choseMenuData);
    cJSON_Delete(choseList);
}


static void table_edit_event_cb(lv_event_t* e)
{
    SETTING_VIEW* disp = (SETTING_VIEW*)lv_event_get_user_data(e);
    SETTING_VIEW* result = NULL;
    lv_obj_t* obj = lv_event_get_target(e);
    uint16_t col;
    uint16_t row;
    lv_table_get_selected_cell(obj, &row, &col);
    uint16_t cnt = lv_table_get_row_cnt(obj);
    char valueBuff[200] = {0};

    //char* val = lv_table_get_cell_value(obj,row,1);
    //char* name = lv_table_get_cell_value(obj,row,0);
    char* name;
    char* val;

    char* pattern;
    if (row >= cnt)
    {
        return;
    }

    if(disp->editDisableFlag[row])
    {
        return;
    }

    disp->cur_row = row;
    cJSON* itemName = cJSON_GetArrayItem(disp->items, row);
    if(cJSON_IsString(itemName))
    {
        cJSON* item = cJSON_GetObjectItemCaseSensitive(disp->pageData, itemName->valuestring);
        if(cJSON_IsObject(item))
        {
            char* type = GetEventItemString(item, "type");          
            if(!strcmp(type, "ITEM_text"))
            {
                if(disp->parent == disp)
                {
                    //使用json参数显示的页面不能跳转
                    cJSON* link = cJSON_GetObjectItemCaseSensitive(item, "link");
                    if(cJSON_IsString(link))
                    {
                        API_JsonMenuDisplay(&disp, NULL, link->valuestring, NULL);
                    }
                }
                else
                {
                    cJSON* elink = cJSON_GetObjectItemCaseSensitive(item, "ext-link");
                    cJSON* link = cJSON_GetObjectItemCaseSensitive(item, "link");
                    if(cJSON_IsString(elink))
                    {
                        API_SettingChangePage(elink->valuestring);
                    }
                    else if(cJSON_IsString(link))
                    {
                        API_SettingInnerChangePage(disp->fileName, link->valuestring);
                    }
                }
            }
            else if(!strcmp(type, "ITEM_btn"))
            {
                if(disp->parent == disp)
                {
                    if(disp->child)
                    {
                        ((ButonClickCallback)(disp->child))(GetEventItemString(item, "msg"));
                    }
                }
                else
                {
                    API_SettingMenuProcess(GetEventItemString(item, "msg"));
                }
            }
            else if(!strcmp(type, "Item_IO_Para"))
            {
                char* paraName = GetEventItemString(item, "ParaName");
                cJSON* ioItem = cJSON_GetObjectItemCaseSensitive(GetSettingIoData(), paraName);
                if(cJSON_IsObject(ioItem))
                {
                    name = GetEventItemString(ioItem, "text");
                    char* io_type = GetEventItemString(ioItem, "type");
                    char* msg = GetEventItemString(ioItem, "msg");
                    if(!strcmp(io_type, "ITEM_text"))
                    {
                        val = Read_PB_Or_IO_To_String(0, paraName, valueBuff, 200);
                        pattern = GetEventItemString(ioItem, "pattern");
                        if(!strcmp(GetEventItemString(ioItem, "kp_type"),"num") || !strcmp(GetEventItemString(ioItem, "kp_type"), "int"))
                        {
                            Vtk_KbMode2Init(&disp->kb,Vtk_KbMode2,NULL,name,20,pattern, val, pattern,setting_text_edit_event_cb,disp);
                        }else
                        {
                            Vtk_KbMode1Init(&disp->kb,Vtk_KbMode1, API_Para_Read_String2(KeybordLanguage), name,30,pattern, val, pattern,setting_text_edit_event_cb,disp);
                        }
                    }
                    else if(!strcmp(io_type, "ITEM_combox"))
                    {
                        SelectSettingValue(name, paraName, ioItem, disp);
                    }
                    else if(!strcmp(io_type, "ITEM_switch"))
                    {
                        bool chk = lv_table_has_cell_ctrl(obj, row, SETTING_VIEW_VALUE_COL(disp->valueFlag[disp->cur_row]), LV_TABLE_CELL_CTRL_CUSTOM_1);
                        if(chk) {
                            lv_table_clear_cell_ctrl(obj, row, SETTING_VIEW_VALUE_COL(disp->valueFlag[disp->cur_row]), LV_TABLE_CELL_CTRL_CUSTOM_1);
                            API_Para_Write_Int(paraName, 0);
                        }                       
                        else {
                            lv_table_add_cell_ctrl(obj, row, SETTING_VIEW_VALUE_COL(disp->valueFlag[disp->cur_row]), LV_TABLE_CELL_CTRL_CUSTOM_1);
                            API_Para_Write_Int(paraName, 1);
                        } 
                        if(msg[0])
                        {
                            API_SettingMenuProcess(msg);
                        }          
                        API_Event_NameAndMsg(EventMenuSettingProcess, "SettingRefresh", NULL);                                    
                    }
                }
            }               
            else if(!strcmp(type, "ITEM_edit"))
            {
                name = GetEventItemString(item, "text");
                pattern = GetEventItemString(item, "pattern");
                if(!strcmp(GetEventItemString(item, "kp_type"),"num"))
                {
                    Vtk_KbMode2Init(&disp->kb,Vtk_KbMode2,NULL, name, 20, pattern, GetEventItemString(item, "Value"), pattern, ItemEditKpCb,disp);
                }else
                {
                    Vtk_KbMode1Init(&disp->kb,Vtk_KbMode1, API_Para_Read_String2(KeybordLanguage), name, 30, pattern, GetEventItemString(item, "Value"), pattern, ItemEditKpCb,disp);
                }
            }
        }
    }
}

static void draw_switc_cb(lv_event_t * e)
{
    SETTING_VIEW* disp = (SETTING_VIEW*)lv_event_get_user_data(e);
    lv_obj_t* obj = lv_event_get_target(e);
    lv_obj_draw_part_dsc_t * dsc = lv_event_get_draw_part_dsc(e);
    /*If the cells are drawn...*/
    if(dsc->part == LV_PART_ITEMS) {
        uint32_t row = dsc->id / lv_table_get_col_cnt(obj);
        uint32_t col = dsc->id - row * lv_table_get_col_cnt(obj);
        
        if(row == disp->switch_id && col == SETTING_VIEW_VALUE_COL(disp->valueFlag[disp->switch_id]))
        {     
            bool chk = lv_table_has_cell_ctrl(obj, row, col, LV_TABLE_CELL_CTRL_CUSTOM_1);      
            lv_draw_rect_dsc_t rect_dsc;
            lv_draw_rect_dsc_init(&rect_dsc);
            rect_dsc.bg_color = chk ? lv_theme_get_color_primary(obj) : lv_palette_lighten(LV_PALETTE_GREY, 2);
            rect_dsc.radius = LV_RADIUS_CIRCLE;

            lv_area_t sw_area;
            sw_area.x1 = dsc->draw_area->x2 - 100;
            sw_area.x2 = sw_area.x1 + 80;
            sw_area.y1 = dsc->draw_area->y1 + lv_area_get_height(dsc->draw_area) / 2 - 20;
            sw_area.y2 = sw_area.y1 + 40;
            lv_draw_rect(dsc->draw_ctx, &rect_dsc, &sw_area);

            rect_dsc.bg_color = lv_color_white();
            if (chk) {
                sw_area.x2 -= 5;
                sw_area.x1 = sw_area.x2 - 30;
            }
            else {
                sw_area.x1 += 5;
                sw_area.x2 = sw_area.x1 + 30;
            }
            sw_area.y1 += 5;
            sw_area.y2 -= 5;
            lv_draw_rect(dsc->draw_ctx, &rect_dsc, &sw_area);
        }       
    }
}

static char* GetComboxDisplay(char* ioName, cJSON* ioData, char* buff, int buffLen)
{
    if(!buffLen || !buff)
    {
        return NULL;
    }

    cJSON* comitem = cJSON_GetObjectItem(ioData, "com_item");
    cJSON* ioVal = API_Para_Read_Public(ioName);
    cJSON* element = NULL;
    cJSON_ArrayForEach(element, comitem)
    {
        cJSON* value = cJSON_GetArrayItem(element, 0);
        cJSON* show = cJSON_GetArrayItem(element, 1);
        if(cJSON_Compare(value, ioVal, 1))
        {
            if(cJSON_IsString(show))
            {
                snprintf(buff, buffLen, "%s", show->valuestring);
            }
            else
            {
                cJSON_PrintPreallocated(show, buff, buffLen, 0);
            }
        }
    }

    return buff;
}

static void SettingView_SetLongTextFlag(SETTING_VIEW* disp, int row, const char* text, const char* value)
{
    if(!disp || row < 0 || row > disp->recordCnt)
    {
        return;
    }
    lv_coord_t len;

    if(text)
    {
        len = lv_txt_get_width(text, strlen(text), vtk_get_font_by_size(SETTING_VIEW_TextSize), lv_obj_get_style_text_letter_space(disp->table, LV_PART_ITEMS), LV_TEXT_FLAG_NONE);
        //dprintf("text = %s, len = %d, cell_w = %d, letter space = %d, line_space = %d\n", text, len, SETTING_VIEW_TEXT_W(disp->symbolFlag[row], disp->valueFlag[row]), lv_obj_get_style_text_letter_space(disp->table, LV_PART_ITEMS), lv_obj_get_style_text_line_space(disp->table, LV_PART_ITEMS));
        if(len > SETTING_VIEW_TEXT_W(disp->symbolFlag[row], disp->valueFlag[row]))
        {
            if(disp->textFlag)
            {
                disp->textFlag[row] |= 0x01;
            }
        }
        else
        {
            if(disp->textFlag)
            {
                disp->textFlag[row] &= (~0x01);
            }
        }
    }

    if(value)
    {
        len = lv_txt_get_width(value, strlen(value), vtk_get_font_by_size(SETTING_VIEW_TextSize), 0, LV_TEXT_FLAG_NONE);
        //dprintf("value = %s, len = %d, cell_w = %d, letter space = %d, line_space = %d\n", value, len, SETTING_VIEW_TEXT_W(disp->symbolFlag[row], disp->valueFlag[row]), lv_obj_get_style_text_letter_space(disp->table, LV_PART_ITEMS), lv_obj_get_style_text_line_space(disp->table, LV_PART_ITEMS));
        if(len > SETTING_VIEW_VALUE_W)
        {
            if(disp->textFlag)
            {
                disp->textFlag[row] |= 0x02;
            }
        }
        else
        {
            if(disp->textFlag)
            {
                disp->textFlag[row] &= (~0x02);
            }
        }
    }
}

static void SettingView_UpdateData(SETTING_VIEW* disp)
{
    cJSON* itemName;
    int col;
    char valueBuff[200] = {0};
    char textBuff[200] = {0};
    disp->switch_id = -1;

    if(!disp)
    {
        return;
    }

    if(!disp->table)
    {
        disp->table = lv_table_create(disp->menu);
        lv_obj_set_style_bg_color(disp->table, SETTING_VIEW_BackgroundColor, 0);
        lv_obj_add_event_cb(disp->table, draw_table_event_cb, LV_EVENT_DRAW_PART_BEGIN, disp);
        lv_obj_add_event_cb(disp->table, draw_switc_cb, LV_EVENT_DRAW_PART_END, disp);
        lv_obj_add_event_cb(disp->table, table_edit_event_cb, LV_EVENT_VALUE_CHANGED, disp);
        lv_table_set_row_height(disp->table,SETTING_VIEW_LineHeight);
        //lv_obj_add_flag(disp->table, LV_OBJ_FLAG_SCROLL_CHAIN_HOR);
        //lv_obj_set_scroll_dir(disp->table,LV_DIR_HOR);
        lv_obj_set_style_border_width(disp->table, 0, 0);
        lv_obj_set_style_radius(disp->table,15,0);
        lv_obj_set_style_pad_all(disp->table, 0, 0);
    }

    //lv_obj_set_width(disp->table, 480);

    lv_obj_set_size(disp->table,480,800 - SETTING_VIEW_TitleHeight);

    lv_obj_set_y(disp->table, SETTING_VIEW_TitleHeight);
    
    lv_table_set_row_cnt(disp->table, disp->recordCnt);
    lv_table_set_col_cnt(disp->table, 4);

    lv_table_set_col_width(disp->table, 0, SETTING_VIEW_SYMBOL_W);
    lv_table_set_col_width(disp->table, 1, SETTING_VIEW_HALF_SCR_W - SETTING_VIEW_SYMBOL_W);
    lv_table_set_col_width(disp->table, 2, SETTING_VIEW_HALF_SCR_W - SETTING_VIEW_NEXT_PAGE_SYMBOL_W);
    lv_table_set_col_width(disp->table, 3, SETTING_VIEW_NEXT_PAGE_SYMBOL_W);

    int i = 0;
    cJSON_ArrayForEach(itemName, disp->items)
    {
        if(cJSON_IsString(itemName))
        {
            cJSON* item = cJSON_GetObjectItemCaseSensitive(disp->pageData, itemName->valuestring);
            if(cJSON_IsObject(item))
            {
                char* value;
                char* type = GetEventItemString(item, "type");   
                //char* text = GetEventItemString(item, "text");
                char* text = get_language_text2(GetEventItemString(item, "text"), textBuff, 200);
                char* symbol = GetEventItemString(item, "symbol"); 
                disp->symbolFlag[i] = (symbol[0] ? 1 : 0);
                disp->textFlag[i] = 0;
                disp->editDisableFlag[i] = 0;

                if(!strcmp(type, "ITEM_text") || !strcmp(type, "ITEM_btn"))
                {
                    disp->valueFlag[i] = 0;
                }
                else
                {
                    disp->valueFlag[i] = 1;
                }

                lv_table_add_cell_ctrl(disp->table, i, disp->valueFlag[i] ? 2 : 1, LV_TABLE_CELL_CTRL_MERGE_RIGHT);

                if(disp->symbolFlag[i])
                {
                    lv_table_set_cell_value(disp->table, i, SETTING_VIEW_SYMBOL_COL, get_symbol_key(GetEventItemString(item, "symbol")));
                }
                else
                {
                    lv_table_add_cell_ctrl(disp->table, i, SETTING_VIEW_SYMBOL_COL, LV_TABLE_CELL_CTRL_MERGE_RIGHT);
                }
                

                //统一填写左边按键名
                if(text[0])
                {
                    lv_table_set_cell_value(disp->table, i, SETTING_VIEW_TEXT_COL(disp->symbolFlag[i]), text);
                    SettingView_SetLongTextFlag(disp, i, text, NULL);
                }                
                
                
                if(!strcmp(type, "ITEM_text"))
                {
                    cJSON* elink = cJSON_GetObjectItemCaseSensitive(item, "ext-link");
                    cJSON* link = cJSON_GetObjectItemCaseSensitive(item, "link");
                    if(cJSON_IsString(elink) || cJSON_IsString(link))
                    {
                        lv_table_set_cell_value(disp->table, i, SETTING_VIEW_VALUE_COL(disp->valueFlag[i]), LV_VTK_RIGHT);
                    }
                    else
                    {
                        disp->editDisableFlag[i] = 1;
                    }
                }
                else if(!strcmp(type, "Item_PB_Para"))
                {
                    lv_table_set_cell_value(disp->table, i, SETTING_VIEW_VALUE_COL(disp->valueFlag[i]), Read_PB_Or_IO_To_String(1, GetEventItemString(item, "ParaName"), valueBuff, 200));
                    SettingView_SetLongTextFlag(disp, i, NULL, valueBuff);
                    disp->editDisableFlag[i] = 1;
                }
                else if(!strcmp(type, "ITEM_switch"))
                {
                    char* paraName = GetEventItemString(item, "ParaName");
                    cJSON* ioItem = cJSON_GetObjectItemCaseSensitive(GetSettingIoData(), paraName);
                    if(cJSON_IsObject(ioItem))
                    {
                        disp->switch_id = i;
                        lv_table_set_cell_value(disp->table,i, SETTING_VIEW_VALUE_COL(disp->valueFlag[i]),"");
                        int intvalue = API_Para_Read_Int(paraName);
                        if(intvalue)
                            lv_table_add_cell_ctrl(disp->table, i, SETTING_VIEW_VALUE_COL(disp->valueFlag[i]), LV_TABLE_CELL_CTRL_CUSTOM_1);
                        else
                            lv_table_clear_cell_ctrl(disp->table, i, SETTING_VIEW_VALUE_COL(disp->valueFlag[i]), LV_TABLE_CELL_CTRL_CUSTOM_1);
                        if(cJSON_GetObjectItemCaseSensitive(ioItem, "EditDisable"))
                        {
                            disp->editDisableFlag[i] = 1;
                        }
                    }
                }
                else if(!strcmp(type, "Item_IO_Para"))
                {
                    char* paraName = GetEventItemString(item, "ParaName");
                    cJSON* ioItem = cJSON_GetObjectItemCaseSensitive(GetSettingIoData(), paraName);
                    if(cJSON_IsObject(ioItem))
                    {
                        text = GetEventItemString(ioItem, "text");
                        value = Read_PB_Or_IO_To_String(0, paraName, valueBuff, 200);

                        vtk_table_set_cell_text(disp->table, i, SETTING_VIEW_TEXT_COL(disp->symbolFlag[i]), text);
                        lv_table_set_cell_value(disp->table, i, SETTING_VIEW_VALUE_COL(disp->valueFlag[i]), value);
                        SettingView_SetLongTextFlag(disp, i, text, value);
                        char* iotype = GetEventItemString(ioItem, "type");
                        if(!strcmp(iotype, "ITEM_switch"))
                        {
                            disp->switch_id = i;
                            lv_table_set_cell_value(disp->table,i,SETTING_VIEW_VALUE_COL(disp->valueFlag[i]),"");
                            int intvalue = API_Para_Read_Int(paraName);
                            if(intvalue)
                                lv_table_add_cell_ctrl(disp->table, i, SETTING_VIEW_VALUE_COL(disp->valueFlag[i]), LV_TABLE_CELL_CTRL_CUSTOM_1);
                            else
                                lv_table_clear_cell_ctrl(disp->table, i, SETTING_VIEW_VALUE_COL(disp->valueFlag[i]), LV_TABLE_CELL_CTRL_CUSTOM_1);
                        }
                        else if(!strcmp(iotype, "ITEM_combox"))
                        {
                            value = GetComboxDisplay(paraName, ioItem, valueBuff, 200);
                            lv_table_set_cell_value(disp->table, i, SETTING_VIEW_VALUE_COL(disp->valueFlag[i]), GetComboxDisplay(paraName, ioItem, valueBuff, 200));
                            SettingView_SetLongTextFlag(disp, i, NULL, value);
                        }
                        if(cJSON_GetObjectItem(ioItem, "EditDisable"))
                        {
                            disp->editDisableFlag[i] = 1;
                        }
                    }

                }
                else if(!strcmp(type, "ITEM_edit"))
                {
                    cJSON* pbName = cJSON_GetObjectItemCaseSensitive(item, "PB_Name");
                    cJSON* ioName = cJSON_GetObjectItemCaseSensitive(item, "IO_Name");
                    if(cJSON_IsString(pbName))
                    {
                        cJSON_ReplaceOrAddItemInObjectCaseSensitive(item, "Value", cJSON_CreateString(Read_PB_Or_IO_To_String(1, pbName->valuestring, valueBuff, 200)));
                    }
                    else if(cJSON_IsString(ioName))
                    {
                        cJSON_ReplaceOrAddItemInObjectCaseSensitive(item, "Value", cJSON_CreateString(Read_PB_Or_IO_To_String(0, ioName->valuestring, valueBuff, 200)));
                    }
                    value = GetEventItemString(item,  "Value");
                    lv_table_set_cell_value(disp->table, i, SETTING_VIEW_VALUE_COL(disp->valueFlag[i]), value);
                    SettingView_SetLongTextFlag(disp, i, NULL, value);
                }
                i++;
            }
        }
    }
}

static void SettingView_UpdatePageName(SETTING_VIEW* tu, const char* menuName)
{
    char* pageNameString = NULL;
    cJSON* pageNameJson = NULL;
    if(!tu)
    {
        return;
    }

    pageNameString = (menuName ? menuName : tu->menuName);
    if(!pageNameString)
    {
        return;
    }

    cJSON* menu = cJSON_GetObjectItemCaseSensitive(tu->pageData, pageNameString);
    if(cJSON_IsObject(menu))
    {
        cJSON* pageCtrl =  cJSON_GetObjectItemCaseSensitive(menu, "PageCtrl");
        if(cJSON_IsObject(pageCtrl))
        {
            cJSON* ioValue = NULL;
            cJSON* ioName =  cJSON_GetObjectItemCaseSensitive(pageCtrl, "IO_Name");
            if(cJSON_IsString(ioName))
            {
                ioValue = API_Para_Read_Public(ioName->valuestring);
            }
            cJSON* valueToPages =  cJSON_GetObjectItemCaseSensitive(pageCtrl, "ValueToPage");
            if(cJSON_IsArray(valueToPages) && ioValue)
            {
                cJSON* valueToPage;
                cJSON* tempValue;
                cJSON_ArrayForEach(valueToPage, valueToPages)
                {
                    tempValue = cJSON_GetArrayItem(valueToPage, 0);
                    if(cJSON_Compare(tempValue, ioValue, 1))
                    {
                        pageNameJson = cJSON_GetArrayItem(valueToPage, 1);
                        if(cJSON_IsString(pageNameJson))
                        {
                            pageNameString = pageNameJson->valuestring;
                            break;
                        }
                    }
                }
            }
        }
    }

    if(pageNameString)
    {
        if(tu->menuName)
        {
            tu->menuName = realloc(tu->menuName, strlen(pageNameString) + 1);
        }
        else
        {
            tu->menuName = malloc(strlen(pageNameString) + 1);
        }
        strcpy(tu->menuName, pageNameString);
    }
}

static void CreateIntFlag(int** flag, int cnt)
{
    int* myFlag;

    if(!flag)
    {
        return;
    }

    myFlag = *flag;
    
    if(cnt)
    {
        if(myFlag)
        {
            myFlag = realloc(myFlag, sizeof(int) * cnt);
        }
        else
        {
            myFlag = malloc(sizeof(int) * cnt);
        }
    }
    else
    {
        if(myFlag)
        {
            free(myFlag);
            myFlag = NULL;
        }
    }

    *flag = myFlag;
}

static SETTING_VIEW* SettingMenuDisplay(SETTING_VIEW** disp, const char* fileName, const char* menuName, void* parent)
{
    SETTING_VIEW* tu = NULL;
    cJSON* pageData = NULL;
    cJSON* title = NULL;

    if(!disp)
    {
        dprintf("error:disp = NULL\n");
        return NULL;
    }

    if(fileName)
    {
        char buf[200];
		snprintf(buf, 200, "%s/%s.json", CUS_SETTING_PAGE_DIR, fileName);		
        pageData = GetJsonFromFile(buf);
        if(!cJSON_IsObject(pageData))
        {
        	snprintf(buf, 200, "%s/%s.json", SETTING_PAGE_DIR, fileName);	
        	pageData = GetJsonFromFile(buf);
       	 if(!cJSON_IsObject(pageData))
       	 {
	            dprintf("error:%s\n", buf);
	            return NULL;
       	 }
        }
    }

    vtk_lvgl_lock();
    if(*disp)
    {
        tu = *disp;
    }
    else
    {
        tu = (SETTING_VIEW*)lv_mem_alloc(sizeof(SETTING_VIEW));
        *disp = tu;
        lv_memset_00(tu, sizeof(SETTING_VIEW));
        tu->scr = SettingView_CreatSrc();
        tu->menu = lv_obj_create(tu->scr);
        lv_obj_set_size(tu->menu, 490, 810);
        lv_obj_align(tu->menu, LV_ALIGN_TOP_MID, 3, -3);
        lv_obj_set_style_pad_all(tu->menu, 0, 0);
        //lv_obj_set_style_bg_color(tu->menu, SETTING_VIEW_BackgroundColor, 0);
        lv_obj_set_style_bg_color(tu->menu, lv_color_make(44,44,44), 0);
        lv_obj_clear_flag(tu->menu, LV_OBJ_FLAG_SCROLLABLE);
        tu->back_scr = lv_scr_act();
    }

    if(pageData)
    {
        if(tu->pageData)
        {
            cJSON_Delete(tu->pageData);
        }
        tu->pageData = pageData;
    }
    
    if(fileName)
    {
        if(tu->fileName)
        {
            tu->fileName = realloc(tu->fileName, strlen(fileName) + 1);
        }
        else
        {
            tu->fileName = malloc(strlen(fileName) + 1);
        }
        strcpy(tu->fileName, fileName);
    }

    if(parent)
    {
        tu->parent = parent;
    }

    SettingView_UpdatePageName(tu, menuName);

    cJSON* menu = cJSON_GetObjectItemCaseSensitive(tu->pageData, tu->menuName);
    tu->items = cJSON_GetObjectItemCaseSensitive(menu, "Items");

    title = cJSON_GetObjectItemCaseSensitive(menu, "PageTitle");
    if(cJSON_IsString(title))
    {
        SettingView_AddTitle(disp, title->valuestring);
    }

    tu->recordCnt = cJSON_GetArraySize(tu->items);

    CreateIntFlag((int**)&(tu->symbolFlag), tu->recordCnt);
    CreateIntFlag((int**)&(tu->valueFlag), tu->recordCnt);
    CreateIntFlag((int**)&(tu->textFlag), tu->recordCnt);
    CreateIntFlag((int**)&(tu->editDisableFlag), tu->recordCnt);

    //dprintf("fileName = %s, menuName = %s, child = %d, parent = %d\n", tu->fileName, tu->menuName, tu->child, tu->parent);


    SettingView_UpdateData(tu);
    
    if(tu->scr != lv_scr_act())
    {
        lv_disp_load_scr(tu->scr);
    }

    vtk_lvgl_unlock();

    return tu;
}

static SETTING_VIEW* SettingMenuDelete(SETTING_VIEW** tb)
{
    SETTING_VIEW* disp = NULL;
    SETTING_VIEW* parent = NULL;
    if(!tb)
    {
        return parent;
    }

    if(!(*tb))
    {
        return parent;
    }

    vtk_lvgl_lock();
    if(*tb)
    {
        disp = *tb;
        *tb = NULL;

        //dprintf("fileName = %s, menuName = %s, child = %d, parent = %d\n", disp->fileName, disp->menuName, disp->child, disp->parent);
        if(disp->kb)
        {
            Vtk_KbReleaseByMode(&disp->kb);
            disp->kb = NULL;
        }
        if(disp->child && disp->parent != disp)
        {
            SettingMenuDelete((SETTING_VIEW**)(&(disp->child)));
        }

        if(disp->scr == lv_scr_act())
        {
            lv_scr_load(disp->back_scr);
            disp->back_scr = NULL;
        }
  
        if(disp->scr)
        {
            lv_obj_del(disp->scr);
            disp->scr = NULL;
        }

        if (disp->pageData)
        {
            cJSON_Delete(disp->pageData);
            disp->pageData = NULL;
        }
   
        if(disp->fileName)
        {
            free(disp->fileName);
            disp->fileName = NULL;
        }

        if(disp->menuName)
        {
            free(disp->menuName);
            disp->menuName= NULL;
        }

        if(disp->symbolFlag)
        {
            free(disp->symbolFlag);
            disp->symbolFlag= NULL;
        }

        if(disp->valueFlag)
        {
            free(disp->valueFlag);
            disp->valueFlag= NULL;
        }

        parent = (SETTING_VIEW*)(disp->parent);
        if(parent)
        {
            parent->child = NULL;
        }

        lv_mem_free(disp);
    }
    vtk_lvgl_unlock();

    return parent;
}

static void SettingMenuRefresh(SETTING_VIEW* disp)
{
    if(disp)
    {
        //dprintf("fileName = %s, menuName = %s, child = %d, parent = %d\n", disp->fileName, disp->menuName, disp->child, disp->parent);
        SettingMenuDisplay(&disp, NULL, NULL, NULL);
    }
}

void lv_msg_setting_handler(void* s, lv_msg_t* m)
{
    LV_UNUSED(s);
    SETTING_VIEW* settingTempPage = NULL;
    static void *setting_menu_tips=NULL;
    SettingMenuResource_S mRes;
    SETTING_CHANGE_PAGE_DATA_T * page;
    switch (lv_msg_get_id(m))
    {
        case SETTING_MSG_SettingReplacePage:
            page = (SETTING_CHANGE_PAGE_DATA_T*)lv_msg_get_payload(m);
            mRes = GetSettingMenuResByName(GetSettingCurtMenuName());	
            if(mRes.close)
            {
                (*mRes.close)();
                SetSettingCurMenuName(settingCurPage ? settingCurPage->menuName : NULL);
            }
            if(settingCurPage)
            {
                settingTempPage = SettingMenuDisplay((SETTING_VIEW**)(&settingCurPage), page->fileName, page->menuName, NULL);
            }

            settingCurPage = settingTempPage ? settingTempPage : settingCurPage;
            SetSettingCurMenuName(settingCurPage ? settingCurPage->menuName : NULL);

            mRes = GetSettingMenuResByName(page->menuName);
            if(mRes.init)
            {
                SetSettingCurMenuName(page->menuName);
                (*mRes.init)();
            }
            break;

        case SETTING_MSG_SettingChangePage:
            page = (SETTING_CHANGE_PAGE_DATA_T*)lv_msg_get_payload(m);
            mRes = GetSettingMenuResByName(GetSettingCurtMenuName());	
            if(mRes.close)
            {
                (*mRes.close)();
                SetSettingCurMenuName(settingCurPage ? settingCurPage->menuName : NULL);
            }
            if(settingCurPage)
            {
                settingTempPage = SettingMenuDisplay((SETTING_VIEW**)(&settingCurPage->child), page->fileName, page->menuName, settingCurPage);
            }
            else
            {
                SettingMenuDelete(&settingRoot);
                settingTempPage = SettingMenuDisplay(&settingRoot, page->fileName, page->menuName, NULL);
            }

            settingCurPage = settingTempPage ? settingTempPage : settingCurPage;
            SetSettingCurMenuName(settingCurPage ? settingCurPage->menuName : NULL);

            mRes = GetSettingMenuResByName(page->menuName);
            if(mRes.init)
            {
                SetSettingCurMenuName(page->menuName);
                (*mRes.init)();
            }
            break;

	    case SETTING_MSG_SettingTextEdit:
        case SETTING_MSG_SettingBtn:
            mRes = GetSettingMenuResByName(GetSettingCurtMenuName());
            if(mRes.process_ext)
            {
                (*mRes.process_ext)(lv_msg_get_id(m),lv_msg_get_payload(m));
            }
            if(mRes.process)
            {
                (*mRes.process)(lv_msg_get_payload(m));
            }
		if(mRes.process==NULL&&mRes.process==NULL)
		{
			SettingDefaultMsgProcess(lv_msg_get_id(m),lv_msg_get_payload(m));
		}
            break;
        case SETTING_MSG_NormalTip:
            API_TIPS_Close_Ext(setting_menu_tips);
            if(lv_msg_get_payload(m))
            {
                setting_menu_tips=API_TIPS_Ext_Time(lv_msg_get_payload(m),3);	
            }
            break;
        case SETTING_MSG_LongTip:
            API_TIPS_Close_Ext(setting_menu_tips);
            if(lv_msg_get_payload(m))
            {
                setting_menu_tips=API_TIPS_Ext_Time(lv_msg_get_payload(m),20);	
            }
            break;
        case SETTING_MSG_ErrorTip:
            BEEP_ERROR();

            API_TIPS_Close_Ext(setting_menu_tips);
            if(lv_msg_get_payload(m))
            {
                setting_menu_tips=API_TIPS_Ext_Time(lv_msg_get_payload(m),3);	
            }
            break;
        case SETTING_MSG_SuccTip:
            BEEP_CONFIRM();
            API_TIPS_Close_Ext(setting_menu_tips);
            if(lv_msg_get_payload(m))
            {
                setting_menu_tips=API_TIPS_Ext_Time(lv_msg_get_payload(m),3);	
            }
            break;	

        case SETTING_MSG_ValueFresh:
            if(settingCurPage && (settingCurPage->scr == lv_scr_act()))
            {
                mRes = GetSettingMenuResByName(settingCurPage->menuName);	
                if(mRes.close)
                {
                    (*mRes.close)();
                }
                SettingMenuRefresh(settingCurPage);
                SetSettingCurMenuName(settingCurPage->menuName);
                mRes = GetSettingMenuResByName(GetSettingCurtMenuName());
                if(mRes.init)
                {
                    (*mRes.init)();
                }
            }
            break;

        case SETTING_MSG_SettingReturn:
            mRes = GetSettingMenuResByName(GetSettingCurtMenuName());	
            if(mRes.close)
            {
                (*mRes.close)();
            }

            if(settingCurPage && settingCurPage->menuName && !strcmp(settingCurPage->menuName, GetSettingCurtMenuName()))
            {
                settingCurPage = SettingMenuDelete((settingCurPage != settingRoot) ? &settingCurPage : &settingRoot);
            }

            SettingMenuRefresh(settingCurPage);
            SetSettingCurMenuName(settingCurPage ? settingCurPage->menuName : NULL);
            break;

        case SETTING_MSG_SettingClose:
            mRes = GetSettingMenuResByName(GetSettingCurtMenuName());
            if(mRes.close)
            {
                (*mRes.close)();
            }
            settingCurPage = SettingMenuDelete(&settingRoot);
            FreeSettingIoData();
            SetSettingCurMenuName(NULL);
            PublicSetingClose();
            break;
    }
}
void PublicSetingClose(void)
{
    CloseHybridMatEditRecordMenu();
    CloseAptMatEditRecordNenu();
}

void API_EnterSettingRoot(const char* rootName)
{
    API_SettingMenuClose();
    API_SettingChangePage(rootName);
}

void API_SettingMenuProcess(void* data)
{
vtk_lvgl_lock();
    lv_msg_send(SETTING_MSG_SettingBtn, data);
vtk_lvgl_unlock();	
}

void API_SettingMenuTextEditProcess(void* data)
{
vtk_lvgl_lock();
    lv_msg_send(SETTING_MSG_SettingTextEdit, data);
vtk_lvgl_unlock();
}

void API_SettingMenuClose(void)
{
    vtk_lvgl_lock();
    lv_msg_send(SETTING_MSG_SettingClose, NULL);
    vtk_lvgl_unlock();
}

void API_SettingMenuReturn(void)
{
    vtk_lvgl_lock();
    lv_msg_send(SETTING_MSG_SettingReturn, NULL);
    vtk_lvgl_unlock();
}

void API_SettingLongTip(char *tip)
{
	vtk_lvgl_lock();
    lv_msg_send(SETTING_MSG_LongTip, tip);
	vtk_lvgl_unlock();
}
void API_SettingNormalTip(char *tip)
{
	vtk_lvgl_lock();
    lv_msg_send(SETTING_MSG_NormalTip, tip);
	vtk_lvgl_unlock();
}
void API_SettingSuccTip(char *tip)
{
	vtk_lvgl_lock();
    lv_msg_send(SETTING_MSG_SuccTip, tip);
	vtk_lvgl_unlock();
}

void API_SettingErrorTip(char *tip)
{
	vtk_lvgl_lock();
    lv_msg_send(SETTING_MSG_ErrorTip, tip);
	vtk_lvgl_unlock();
}
void API_SettingValueFresh(char *para)
{
	vtk_lvgl_lock();
    lv_msg_send(SETTING_MSG_ValueFresh, para);
	vtk_lvgl_unlock();
}

void API_SettingChangePage(const char* pageName)
{
    vtk_lvgl_lock();
    changePageData.fileName = pageName;
    changePageData.menuName = pageName;
    lv_msg_send(SETTING_MSG_SettingChangePage, &changePageData);
    vtk_lvgl_unlock();
}

void API_SettingInnerChangePage(const char* fileName, const char* pageName)
{
    vtk_lvgl_lock();
    changePageData.fileName = fileName;
    changePageData.menuName = pageName;
    lv_msg_send(SETTING_MSG_SettingChangePage, &changePageData);
    vtk_lvgl_unlock();
}

void API_SettingReplacePage(const char* fileName, const char* pageName)
{
    vtk_lvgl_lock();
    changePageData.fileName = fileName;
    changePageData.menuName = pageName;
    lv_msg_send(SETTING_MSG_SettingReplacePage, &changePageData);
    vtk_lvgl_unlock();
}


int API_JsonMenuDisplay(SETTING_VIEW** disp, const cJSON* menuJson, const char* menuName, ButonClickCallback callback)
{
    int ret = 0;
    SETTING_VIEW* tu = NULL;
    cJSON* pageData = NULL;
    cJSON* title = NULL;
    char* myMenuName = menuName;

    if(!disp)
    {
        dprintf("error:disp = NULL\n");
        return ret;
    }

    if((*disp) && (*disp)->pageData && !cJSON_IsObject(menuJson))
    {
        pageData = NULL;
    }
    else
    {
        if(!cJSON_IsObject(menuJson))
        {
            dprintf("menuJson error\n");
            return ret;
        }
        pageData = cJSON_Duplicate(menuJson, 1);
    }

    if(!myMenuName)
    {
        cJSON* element;
        cJSON_ArrayForEach(element, menuJson)
        {
            title = cJSON_GetObjectItemCaseSensitive(element, "PageTitle");
            if(cJSON_IsString(title))
            {
                myMenuName = title->valuestring;
                break;
            }
        }
    }

    if(!myMenuName)
    {
        if(pageData)
        {
            cJSON_Delete(pageData);
            pageData = NULL;
        }
        dprintf("menuName error\n");
        return ret;
    }

    vtk_lvgl_lock();
    if(*disp)
    {
        tu = *disp;
    }
    else
    {
        tu = (SETTING_VIEW*)lv_mem_alloc(sizeof(SETTING_VIEW));
        *disp = tu;
        lv_memset_00(tu, sizeof(SETTING_VIEW));
        tu->scr = SettingView_CreatSrc();
        tu->menu = lv_obj_create(tu->scr);
        lv_obj_set_size(tu->menu, 490, 810);
        lv_obj_align(tu->menu, LV_ALIGN_TOP_MID, 3, -3);
        lv_obj_set_style_pad_all(tu->menu, 0, 0);
        //lv_obj_set_style_bg_color(tu->menu, SETTING_VIEW_BackgroundColor, 0);
        lv_obj_set_style_bg_color(tu->menu, lv_color_make(44,44,44), 0);
        lv_obj_clear_flag(tu->menu, LV_OBJ_FLAG_SCROLLABLE);
        tu->back_scr = lv_scr_act();
    }

    tu->parent = tu;

    if(callback)
    {
        tu->child  = callback;
    }

    if(pageData)
    {
        if(tu->pageData)
        {
            cJSON_Delete(tu->pageData);
        }
        tu->pageData = pageData;
    }
    
    SettingView_UpdatePageName(tu, myMenuName);

    cJSON* menu = cJSON_GetObjectItemCaseSensitive(tu->pageData, tu->menuName);
    tu->items = cJSON_GetObjectItemCaseSensitive(menu, "Items");

    title = cJSON_GetObjectItemCaseSensitive(menu, "PageTitle");
    if(cJSON_IsString(title))
    {
        SettingView_AddTitle(disp, title->valuestring);
    }

    tu->recordCnt = cJSON_GetArraySize(tu->items);

    CreateIntFlag((int**)&(tu->symbolFlag), tu->recordCnt);
    CreateIntFlag((int**)&(tu->valueFlag), tu->recordCnt);
    CreateIntFlag((int**)&(tu->textFlag), tu->recordCnt);
    CreateIntFlag((int**)&(tu->editDisableFlag), tu->recordCnt);

    //dprintf("fileName = %s, menuName = %s, child = %d, parent = %d\n", tu->fileName, tu->menuName, tu->child, tu->parent);


    SettingView_UpdateData(tu);
    
    if(tu->scr != lv_scr_act())
    {
        lv_disp_load_scr(tu->scr);
    }
    ret = 1;
    vtk_lvgl_unlock();

    return ret;
}

int API_JsonMenuDelete(SETTING_VIEW** disp)
{
    int ret = 0;

    if(!disp)
    {
        return ret;
    }

    if(!(*disp))
    {
        return ret;
    }

    SettingMenuDelete(disp);

    ret = 1;

    return ret;
}

int Menu_Setting_Shell_Test(cJSON *cmd)
{
	char* jsonstr = GetShellCmdInputString(cmd, 1);

	ShellCmdPrintf("%s:",jsonstr);

    if(strcmp(jsonstr,"close")==0)
	{
        SettingMenuDelete(&settingRoot);
	}    
}

int MenuSettingProcess_Callback(cJSON *cmd)
{
    char* msg = GetEventItemString(cmd,  EVENT_KEY_Message);
	if(!strcmp(msg, "ExitInstallerMode"))
	{
		API_SettingMenuClose();
        vtk_lvgl_lock();
		MainMenuLeave_InstallerMode();
        vtk_lvgl_unlock();
	}
	else if(!strcmp(msg, "SettingRefresh"))
	{
		API_SettingValueFresh(NULL);
	}
	return 1;
}
