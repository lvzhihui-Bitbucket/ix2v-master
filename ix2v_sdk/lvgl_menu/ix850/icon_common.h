#ifndef ICON_COMMON_H_
#define ICON_COMMON_H_

#include <stdio.h>
#include <time.h>
#include "menu_common.h"
#include "lv_ix850.h"
#include "ix850_icon.h"
#include "ix850_callbtn.h"
#include "ix850_info.h"

extern const lv_font_t* font_small;
extern const lv_font_t* font_medium;
extern const lv_font_t* font_larger;

void set_font_size(lv_obj_t* obj, int size);
const lv_font_t * vtk_get_font_by_size(int size);
lv_obj_t* creat_cont_for_postion(lv_obj_t* parent, cJSON* json);
char* IX850_get_current_time(void);
bool ix850s_save_file(const char* filename, char* buf);
#endif 

