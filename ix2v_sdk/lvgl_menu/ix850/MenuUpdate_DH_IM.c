#include "menu_utility.h"
#include "MenuHybridMatCreateNew.h"
#include "cJSON.h"
#include "obj_TableSurver.h"
#include "define_string.h"
#include "obj_PublicInformation.h"
#include "utility.h"
#include "task_Event.h"
#include "define_file.h"
#include "obj_IoInterface.h"
#include "obj_IXS_Proxy.h"

static int updateFlag = 0;

static void UpdateDhImDisplayMessage(char* string)
{
	if(string)
	{
		if(!API_TIPS_Mode(string));
		{
			API_TIPS_Mode_Fresh(NULL, string);
		}
	}
}


static int UpdateDhImFlashTimeOut(int time)
{
	API_Event_By_Name(EventMenuUpdateDhIM);
	return 2;
}

static void UpdateDhIm(void* para)
{
	updateFlag = 1;
	UpdateDhImDisplayMessage("Please wait");
	API_Add_TimingCheck(UpdateDhImFlashTimeOut, 10);
	API_Event_By_Name(Event_IXS_ReqUpdateTb);
}

static int API_Event_UpdateRemoteStart(int targetIp, const char* serverIp, const char* code, int checkTime)
{
	int ret = 0;
	cJSON *event = cJSON_CreateObject();
	cJSON_AddStringToObject(event, EVENT_KEY_EventName, EventUpdateStart);
	cJSON_AddStringToObject(event, "Server", serverIp);
	cJSON_AddStringToObject(event, "Code", code);
	cJSON_AddNumberToObject(event, "CheckTime", checkTime);
	cJSON_AddStringToObject(event, "Source", GetSysVerInfo_IP_by_device(GetNetDeviceNameByTargetIp(targetIp)));
	ret = API_XD_EventJson(targetIp, event);
	cJSON_Delete(event);
	return ret;
}

int MenuUpdateDhImProcess(cJSON *cmd)
{
	int updateCnt = 0;
	if(updateFlag)
	{
		char tempString[100];
		updateFlag = 0;
		API_Del_TimingCheck(UpdateDhImFlashTimeOut);
		cJSON* onlineDevice = NULL;
		cJSON* element = NULL;
		IXS_ProxyGetTb(&onlineDevice, NULL, Search);
		cJSON_ArrayForEach(element, onlineDevice)
		{
			char* ixAddr = GetEventItemString(element,  IX2V_IX_ADDR);
			char* devModel = GetEventItemString(element,  IX2V_DevModel);
			char* ixType = GetEventItemString(element,  IX2V_IX_TYPE);
			if(!strcmp(devModel, "DH-IM") && !strcmp(ixType, "IM") && RegexCheck("^009900[0-9][1-9][0-9]{2}$", ixAddr))
			{
				char* ipAddr = GetEventItemString(element,  IX2V_IP_ADDR);
				if(API_Event_UpdateRemoteStart(inet_addr(ipAddr), API_Para_Read_String2(DH_IM_FW_SERVER), API_Para_Read_String2(DH_IM_FW_CODE), 0))
				{
					updateCnt++;
				}
			}
		}

		snprintf(tempString, 100, "Update request %d IM", updateCnt);
		UpdateDhImDisplayMessage(tempString);
		sleep(3);
		API_TIPS_Mode_Close(NULL);
	}
	return 1;
}

void Menu_DH_IM_FW_UPDATE_Process(void* data)
{
	if(data)
	{
		if(!strcmp(data, "DhImFwUpdate"))
		{
			API_MSGBOX("Update all DH IM", 2, UpdateDhIm, NULL);
		}
	}
}

void Menu_DH_IM_FW_UPDATE_Close(void)
{
	if(updateFlag)
	{
		updateFlag = 0;
		API_TIPS_Mode_Close(NULL);
	}
}

void Menu_DH_IM_FW_UPDATE_Init(void)
{
	if(updateFlag)
	{
		updateFlag = 0;
		API_TIPS_Mode_Close(NULL);
	}
}
