#include "cJSON.h"
#include "Common_Record.h"

typedef void (*BtnCallback)(const char*, void*);

typedef struct 
{   
    cJSON* resRecord;
    RECORD_CONTROL* recordCtrl;
}AUTO_RES_MENU_T;

cJSON* AutoCreateNewR8001(const cJSON* para, int maxRecordNbr);
void CreateNewRes_Init(void);
void CreateNewRes_Close(void);