#include "lvgl.h"
 
lv_obj_t* keyboard_return = NULL;
lv_obj_t* ui_keyboard = NULL;

static const char* kb_map_spec[] = { "1","2", "3", "4", "5", "6",LV_SYMBOL_BACKSPACE, "\n",
                                    "7", "8", "9", "0", "+","-", "/", "\n",
                                    "abc","*", "=", "%", "!", "?" ,"#" ,"\\" , "\n",
                                    "@", "$", "(",")","{","}", "[","]","\n",
                                    ";", "\"", "'", "<", ">", LV_SYMBOL_LEFT, LV_SYMBOL_RIGHT,"\n",
                                    LV_SYMBOL_KEYBOARD," ", LV_SYMBOL_OK, ""
};
static const char* kb_map_lc[] = { "1#","q", "w", "e", "r", "t",LV_SYMBOL_BACKSPACE, "\n",
                                "y", "u", "i", "o", "p","a", LV_SYMBOL_NEW_LINE, "\n",
                                "ABC","s", "d", "f", "g", "h",".",":", "\n",
                                "j", "k", "l","z","x","_", "-",",","\n",
                                "c", "v", "b", "n", "m", LV_SYMBOL_LEFT, LV_SYMBOL_RIGHT,"\n",
                                LV_SYMBOL_KEYBOARD," ", LV_SYMBOL_OK, ""
};
static const char* kb_map_uc[] = {"1#","Q", "W", "E", "R", "T",LV_SYMBOL_BACKSPACE, "\n",
                                "Y", "U", "I", "O", "P","A", LV_SYMBOL_NEW_LINE, "\n",
                                "abc","S", "D", "F", "G", "H",".",":", "\n",
                                "J", "K", "L", "Z","X", "_", "-",",","\n",
                                "C", "V", "B", "N", "M", LV_SYMBOL_LEFT, LV_SYMBOL_RIGHT,"\n",
                                LV_SYMBOL_KEYBOARD," ", LV_SYMBOL_OK, ""
};


static const lv_btnmatrix_ctrl_t kb_ctrl_uc[] = { LV_KEYBOARD_CTRL_BTN_FLAGS | 4, 4, 4, 4, 4, 4, LV_KEYBOARD_CTRL_BTN_FLAGS | 6,
                                                4, 4, 4, 4, 4, 4, LV_KEYBOARD_CTRL_BTN_FLAGS | 6,
                                                LV_KEYBOARD_CTRL_BTN_FLAGS | 4, 4, 4, 4, 4, 4, 4,4,
                                                4, 4, 4, 4, 4, 4, 4,4,
                                                4, 4, 4, 4, 4, LV_KEYBOARD_CTRL_BTN_FLAGS | 4, LV_KEYBOARD_CTRL_BTN_FLAGS | 4,
                                                LV_KEYBOARD_CTRL_BTN_FLAGS | 2, 6, LV_KEYBOARD_CTRL_BTN_FLAGS | 2
};

static const lv_btnmatrix_ctrl_t kb_ctrl_lc[] = { LV_KEYBOARD_CTRL_BTN_FLAGS | 4, 4, 4, 4, 4, 4, LV_KEYBOARD_CTRL_BTN_FLAGS | 6,
                                                4, 4, 4, 4, 4, 4, LV_KEYBOARD_CTRL_BTN_FLAGS | 6,
                                                LV_KEYBOARD_CTRL_BTN_FLAGS | 4, 4, 4, 4, 4, 4, 4,4,
                                                4, 4, 4, 4, 4, 4, 4,4,
                                                4, 4, 4, 4, 4, LV_KEYBOARD_CTRL_BTN_FLAGS | 4, LV_KEYBOARD_CTRL_BTN_FLAGS | 4,
                                                LV_KEYBOARD_CTRL_BTN_FLAGS | 2, 6, LV_KEYBOARD_CTRL_BTN_FLAGS | 2
};
static const lv_btnmatrix_ctrl_t kb_ctrl_spec[] = {4, 4, 4, 4, 4, 4, LV_KEYBOARD_CTRL_BTN_FLAGS | 6,
                                                4, 4, 4, 4, 4, 4, 4,
                                                LV_KEYBOARD_CTRL_BTN_FLAGS | 4, 4, 4, 4, 4, 4, 4,4,
                                                4, 4, 4, 4, 4, 4, 4,4,
                                                4, 4, 4, 4, 4, LV_KEYBOARD_CTRL_BTN_FLAGS | 4, LV_KEYBOARD_CTRL_BTN_FLAGS | 4,
                                                LV_KEYBOARD_CTRL_BTN_FLAGS | 2, 6, LV_KEYBOARD_CTRL_BTN_FLAGS | 2
};

static lv_obj_t* keyboard_scr_init(void)
{
    lv_obj_t* ui_keyboard = lv_obj_create(NULL);
    lv_obj_clear_flag(ui_keyboard, LV_OBJ_FLAG_SCROLLABLE);
    lv_obj_remove_style_all(ui_keyboard);
    lv_obj_set_size(ui_keyboard, lv_disp_get_hor_res(NULL), lv_disp_get_ver_res(NULL));
    lv_obj_align(ui_keyboard, LV_ALIGN_TOP_LEFT, 0, 0);
    lv_obj_set_style_bg_color(ui_keyboard, lv_palette_main(LV_PALETTE_GREY), 0);
    lv_obj_set_style_bg_opa(ui_keyboard, LV_OPA_100, 0);   
    return ui_keyboard;
}

lv_obj_t* keyboard_create(lv_obj_t* parent)
{
    lv_obj_t* kb = lv_keyboard_create(parent);
    lv_obj_set_style_text_font(kb, &lv_font_montserrat_26, 0);
    lv_obj_set_height(kb, 500);
    lv_keyboard_set_map(kb, LV_KEYBOARD_MODE_TEXT_UPPER, kb_map_uc, kb_ctrl_uc);
    lv_keyboard_set_map(kb, LV_KEYBOARD_MODE_TEXT_LOWER, kb_map_lc, kb_ctrl_lc);
    lv_keyboard_set_map(kb, LV_KEYBOARD_MODE_SPECIAL, kb_map_spec, kb_ctrl_spec);
    lv_obj_add_flag(kb, LV_OBJ_FLAG_HIDDEN);
    return kb;
}

lv_obj_t* keyboard_menu_init(lv_obj_t* target, char* val,lv_event_cb_t event_cb)
{
    keyboard_return = lv_scr_act();
    ui_keyboard = keyboard_scr_init();

    lv_obj_t* keyMenu = lv_obj_create(ui_keyboard);
    lv_obj_set_style_pad_row(keyMenu, 0, 0);
    lv_obj_set_size(keyMenu, 480, 800);
    lv_obj_set_flex_flow(keyMenu, LV_FLEX_FLOW_COLUMN);
    lv_obj_clear_flag(keyMenu, LV_OBJ_FLAG_SCROLLABLE);

    lv_obj_t* nameLabel = lv_obj_get_child(lv_obj_get_parent(target), 0);
    char* name = lv_label_get_text(nameLabel);

    lv_obj_t* name_label = lv_label_create(keyMenu);
    lv_label_set_text(name_label, name);
    lv_obj_set_size(name_label, LV_PCT(100), 60);
    lv_obj_set_style_text_font(name_label, &lv_font_montserrat_32, 0);

    lv_obj_t* ta = lv_textarea_create(keyMenu);
    lv_obj_set_style_text_font(ta, &lv_font_montserrat_26, 0);
    lv_textarea_add_text(ta, val);
    lv_obj_set_size(ta, LV_PCT(100), 240);

    lv_obj_t* kb = keyboard_create(ui_keyboard);

    lv_obj_add_event_cb(ta, event_cb, LV_EVENT_ALL, target);
    //lv_event_send(ta, LV_EVENT_FOCUSED, NULL);

    lv_disp_load_scr(ui_keyboard);
    return kb;
}

void keyboard_exit(void)
{
    lv_disp_load_scr(keyboard_return);      
    lv_obj_del(ui_keyboard);
    ui_keyboard = NULL;
}