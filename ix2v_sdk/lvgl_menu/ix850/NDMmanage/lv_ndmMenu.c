#include "obj_lvgl_msg.h"
#include "lv_ix850.h"
#include "common_table_display.h"
#include "MenuNDMManage.h"
#include "obj_NDM.h"

static lv_obj_t* ui_table_init(void);
static lv_obj_t* ui_register = NULL;
static lv_obj_t* register_back = NULL;
static lv_obj_t* ui_planning = NULL;
static lv_obj_t* planning_back = NULL;
static lv_obj_t* ui_log = NULL;
static lv_obj_t* log_back = NULL;
static lv_obj_t* ui_chart = NULL;
static lv_obj_t* chart_back = NULL;
static COM_TABLE_DISPLAY *register_display=NULL;
static cJSON *view_register=NULL;
static COM_TABLE_DISPLAY *planning_display=NULL;
static cJSON *view_planning=NULL;
static COM_TABLE_DISPLAY *log_display=NULL;
static cJSON *view_log=NULL;
static void add_register_control(lv_obj_t* parent);
static void add_planning_control(lv_obj_t* parent);
static void add_log_control(lv_obj_t* parent);
lv_obj_t* ndm_chart_show(cJSON * data);

COM_TABLE_DISPLAY* ndm_register_show(void)
{
	//printf("11111111111%s:%d\n",__func__,__LINE__);	
	view_register = GetNDMTb(NULL);

    register_back = lv_scr_act();
    ui_register = ui_table_init();
    lv_disp_load_scr(ui_register);
	register_display = common_table_display_menu(ui_register, view_register, "NDM_Register", MSG_NDM_VIEW_LOG);
	add_register_control(ui_register);
	return register_display;
}

COM_TABLE_DISPLAY* ndm_planning_show(void)
{
	//printf("11111111111%s:%d\n",__func__,__LINE__);	
	view_planning = GetNDMPlanning(NULL);

    planning_back = lv_scr_act();
    ui_planning = ui_table_init();
    lv_disp_load_scr(ui_planning);
	planning_display = common_table_display_menu(ui_planning, view_planning, "NDM_Online", MSG_NDM_SELECT_ONE);
	add_planning_control(ui_planning);
	return planning_display;
}

COM_TABLE_DISPLAY* ndm_log_show(cJSON *item)
{
	//printf("11111111111%s:%d\n",__func__,__LINE__);	
	view_log = GetNDMLog(item);

    log_back = lv_scr_act();
    ui_log = ui_table_init();
    lv_disp_load_scr(ui_log);
	log_display = common_table_display_menu(ui_log, view_log, "NDM_Log", 0);
	add_log_control(ui_log);
	return log_display;
}

void ndm_register_close(void)
{
	//printf("11111111111%s:%d\n",__func__,__LINE__);	
    lv_disp_load_scr(register_back);
	if(register_display!=NULL)
	{
		common_table_delete(register_display);
        register_display = NULL;
	}
	if(view_register)
	{
		cJSON_Delete(view_register);
		view_register=NULL;
	}
	if(ui_register)
	{
		lv_obj_del(ui_register);
		ui_register = NULL;
	}
}
void ndm_planning_close(void)
{
	//printf("11111111111%s:%d\n",__func__,__LINE__);	
    lv_disp_load_scr(planning_back);
	if(planning_display!=NULL)
	{
		common_table_delete(planning_display);
        planning_display = NULL;
	}
	if(view_planning)
	{
		cJSON_Delete(view_planning);
		view_planning=NULL;
	}
	if(ui_planning)
	{
		lv_obj_del(ui_planning);
		ui_planning = NULL;
	}
}
void ndm_log_close(void)
{
	//printf("11111111111%s:%d\n",__func__,__LINE__);	
    lv_disp_load_scr(log_back);
	if(log_display!=NULL)
	{
		common_table_delete(log_display);
        log_display = NULL;
	}
	if(view_log)
	{
		cJSON_Delete(view_log);
		view_log=NULL;
	}
	if(ui_log)
	{
		lv_obj_del(ui_log);
		ui_log = NULL;
	}
}

void ndm_register_fresh(void)
{
    //printf("11111111111%s:%d\n",__func__,__LINE__);	
    if(register_display==NULL)
    {
        return;
    }
	view_register = GetNDMTb(NULL);
    common_table_fresh(register_display, view_register, 1);
}





/*****************
 * 创建底部表控制栏
 * **************/
static void register_exit_event(lv_event_t* e)
{
    //ndm_register_close();
    MenuNDMManage_Process(MSG_NDM_REGISTER_EXIT, NULL);
}
static void register_add_event(lv_event_t* e)
{
	//ndm_planning_show();
    MenuNDMManage_Process(MSG_NDM_PLANNING, NULL);
}
static void register_start_event(lv_event_t* e)
{
    MenuNDMManage_Process(MSG_NDM_SERVICE_START, NULL);
}
static void register_stop_event(lv_event_t* e)
{
    MenuNDMManage_Process(MSG_NDM_SERVICE_STOP, NULL);
}
static void ndm_tlog_event(lv_event_t* e)
{
    MenuNDMManage_Process(MSG_NDM_TLOG_CFG, NULL);
}
static void add_register_control(lv_obj_t* parent)
{
    lv_obj_t* btnCont = lv_obj_create(parent);
    lv_obj_set_size(btnCont, 480, 80);
    lv_obj_set_style_pad_all(btnCont, 0, 0);
    lv_obj_set_y(btnCont, 720);
    //返回
    lv_obj_t* backBtn = lv_btn_create(btnCont);
    lv_obj_align(backBtn, LV_ALIGN_LEFT_MID, 0, 0);
    //lv_obj_set_style_bg_color(backBtn, lv_color_make(230, 231, 232), 0);
    lv_obj_set_size(backBtn, 96, 65);
    lv_obj_set_style_text_font(backBtn, &lv_font_montserrat_24, 0);
    lv_obj_add_event_cb(backBtn, register_exit_event, LV_EVENT_CLICKED, NULL);
    lv_obj_set_style_radius(backBtn, LV_RADIUS_CIRCLE, 0);
    lv_obj_t* backlabel = lv_label_create(backBtn);
    lv_label_set_text(backlabel, LV_SYMBOL_BACKSPACE);
    lv_obj_center(backlabel);
    //添加
    lv_obj_t* addBtn = lv_btn_create(btnCont);
    lv_obj_set_size(addBtn, 96, 65);
    lv_obj_set_style_text_font(addBtn, &lv_font_montserrat_24, 0);
    lv_obj_add_event_cb(addBtn, register_add_event, LV_EVENT_CLICKED, NULL);
    lv_obj_align(addBtn, LV_ALIGN_RIGHT_MID, 0, 0);
    lv_obj_set_style_radius(addBtn, LV_RADIUS_CIRCLE, 0);
    //lv_obj_set_style_bg_color(addBtn, lv_color_make(230, 231, 232), 0);
    lv_obj_t* addlabel = lv_label_create(addBtn);
    lv_label_set_text(addlabel, LV_SYMBOL_PLUS);
    lv_obj_center(addlabel);
	//启动
    lv_obj_t* checkBtn = lv_btn_create(btnCont);
    lv_obj_set_size(checkBtn, 96, 65);
    lv_obj_set_style_text_font(checkBtn, &lv_font_montserrat_24, 0);
    lv_obj_add_event_cb(checkBtn, register_start_event, LV_EVENT_CLICKED, NULL);
    lv_obj_align(checkBtn, LV_ALIGN_CENTER, -96, 0);
    lv_obj_set_style_radius(checkBtn, LV_RADIUS_CIRCLE, 0);
    lv_obj_t* checklabel = lv_label_create(checkBtn);
    lv_label_set_text(checklabel, "Start");
    lv_obj_center(checklabel);
    //停止
    lv_obj_t* stopBtn = lv_btn_create(btnCont);
    lv_obj_set_size(stopBtn, 96, 65);
    lv_obj_set_style_text_font(stopBtn, &lv_font_montserrat_24, 0);
    lv_obj_add_event_cb(stopBtn, register_stop_event, LV_EVENT_CLICKED, NULL);
    lv_obj_align(stopBtn, LV_ALIGN_CENTER, 96, 0);
    lv_obj_set_style_radius(stopBtn, LV_RADIUS_CIRCLE, 0);
    lv_obj_t* stoplabel = lv_label_create(stopBtn);
    lv_label_set_text(stoplabel, "Stop");
    lv_obj_center(stoplabel);
    //tlog
    lv_obj_t* delBtn = lv_btn_create(btnCont);
    lv_obj_set_size(delBtn, 96, 65);
    lv_obj_set_style_text_font(delBtn, &lv_font_montserrat_24, 0);
    lv_obj_add_event_cb(delBtn, ndm_tlog_event, LV_EVENT_CLICKED, NULL);
    lv_obj_align(delBtn, LV_ALIGN_CENTER, 0, 0);
    lv_obj_set_style_radius(delBtn, LV_RADIUS_CIRCLE, 0);
    lv_obj_t* dellabel = lv_label_create(delBtn);
    lv_label_set_text(dellabel, "Tlog");
    lv_obj_center(dellabel);
}

static void planning_exit_event(lv_event_t* e)
{
    //ndm_planning_close();
    MenuNDMManage_Process(MSG_NDM_SELECT_EXIT,NULL);
}
static void planning_add_event(lv_event_t* e)
{
    MenuNDMManage_Process(MSG_NDM_SELECT_ALL,view_planning);
}
static void planning_delete_event(lv_event_t* e)
{
    MenuNDMManage_Process(MSG_NDM_DELETE_ALL,view_planning);
}
static void add_planning_control(lv_obj_t* parent)
{
    lv_obj_t* btnCont = lv_obj_create(parent);
    lv_obj_set_size(btnCont, 480, 80);
    lv_obj_set_style_pad_all(btnCont, 0, 0);
    lv_obj_set_y(btnCont, 720);
    //返回
    lv_obj_t* backBtn = lv_btn_create(btnCont);
    lv_obj_align(backBtn, LV_ALIGN_LEFT_MID, 50, 0);
    //lv_obj_set_style_bg_color(backBtn, lv_color_make(230, 231, 232), 0);
    lv_obj_set_size(backBtn, 120, 65);
    lv_obj_set_style_text_font(backBtn, &lv_font_montserrat_32, 0);
    lv_obj_add_event_cb(backBtn, planning_exit_event, LV_EVENT_CLICKED, NULL);
    lv_obj_set_style_radius(backBtn, LV_RADIUS_CIRCLE, 0);
    lv_obj_t* backlabel = lv_label_create(backBtn);
    lv_label_set_text(backlabel, LV_SYMBOL_BACKSPACE);
    lv_obj_center(backlabel);
    //添加
    lv_obj_t* addBtn = lv_btn_create(btnCont);
    lv_obj_set_size(addBtn, 120, 65);
    lv_obj_set_style_text_font(addBtn, &lv_font_montserrat_32, 0);
    lv_obj_add_event_cb(addBtn, planning_add_event, LV_EVENT_CLICKED, NULL);
    lv_obj_align(addBtn, LV_ALIGN_RIGHT_MID, -50, 0);
    lv_obj_set_style_radius(addBtn, LV_RADIUS_CIRCLE, 0);
    //lv_obj_set_style_bg_color(addBtn, lv_color_make(230, 231, 232), 0);
    lv_obj_t* addlabel = lv_label_create(addBtn);
    lv_label_set_text(addlabel, LV_SYMBOL_PLUS);
    lv_obj_center(addlabel);
	//删除
    lv_obj_t* checkBtn = lv_btn_create(btnCont);
    lv_obj_set_size(checkBtn, 120, 65);
    lv_obj_set_style_text_font(checkBtn, &lv_font_montserrat_32, 0);
    lv_obj_add_event_cb(checkBtn, planning_delete_event, LV_EVENT_CLICKED, NULL);
    lv_obj_center(checkBtn);
    lv_obj_set_style_radius(checkBtn, LV_RADIUS_CIRCLE, 0);
    lv_obj_t* checklabel = lv_label_create(checkBtn);
    lv_label_set_text(checklabel, LV_SYMBOL_MINUS);
    lv_obj_center(checklabel);
}

static void log_exit_event(lv_event_t* e)
{
    //ndm_log_close();
    MenuNDMManage_Process(MSG_NDM_LOG_EXIT,NULL);
}
static void log_del_event(lv_event_t* e)
{
    MenuNDMManage_Process(MSG_NDM_DELETE_ONE,view_log);
}
static void log_chart_event(lv_event_t* e)
{
    ndm_chart_show(view_log);
    //MenuNDMManage_Process(MSG_NDM_LOG_EXIT,NULL);
}
static void add_log_control(lv_obj_t* parent)
{
    lv_obj_t* btnCont = lv_obj_create(parent);
    lv_obj_set_size(btnCont, 480, 80);
    lv_obj_set_style_pad_all(btnCont, 0, 0);
    lv_obj_set_y(btnCont, 720);
    //返回
    lv_obj_t* backBtn = lv_btn_create(btnCont);
    lv_obj_align(backBtn, LV_ALIGN_LEFT_MID, 50, 0);
    //lv_obj_set_style_bg_color(backBtn, lv_color_make(230, 231, 232), 0);
    lv_obj_set_size(backBtn, 120, 65);
    lv_obj_set_style_text_font(backBtn, &lv_font_montserrat_32, 0);
    lv_obj_add_event_cb(backBtn, log_exit_event, LV_EVENT_CLICKED, NULL);
    lv_obj_set_style_radius(backBtn, LV_RADIUS_CIRCLE, 0);
    lv_obj_t* backlabel = lv_label_create(backBtn);
    lv_label_set_text(backlabel, LV_SYMBOL_BACKSPACE);
    lv_obj_center(backlabel);
    //chart
    lv_obj_t* chartBtn = lv_btn_create(btnCont);
    lv_obj_set_size(chartBtn, 120, 65);
    lv_obj_set_style_text_font(chartBtn, &lv_font_montserrat_32, 0);
    lv_obj_add_event_cb(chartBtn, log_chart_event, LV_EVENT_CLICKED, NULL);
    lv_obj_align(chartBtn, LV_ALIGN_RIGHT_MID, -50, 0);
    lv_obj_set_style_radius(chartBtn, LV_RADIUS_CIRCLE, 0);
    lv_obj_t* chartlabel = lv_label_create(chartBtn);
    lv_label_set_text(chartlabel, "Chart");
    lv_obj_center(chartlabel);
    //删除
    lv_obj_t* checkBtn = lv_btn_create(btnCont);
    lv_obj_set_size(checkBtn, 120, 65);
    lv_obj_set_style_text_font(checkBtn, &lv_font_montserrat_32, 0);
    lv_obj_add_event_cb(checkBtn, log_del_event, LV_EVENT_CLICKED, NULL);
    lv_obj_center(checkBtn);
    lv_obj_set_style_radius(checkBtn, LV_RADIUS_CIRCLE, 0);
    lv_obj_t* checklabel = lv_label_create(checkBtn);
    lv_label_set_text(checklabel, "Delete");
    lv_obj_center(checklabel);
}

static lv_obj_t* ui_table_init(void)
{	
    lv_obj_t* ui_table = lv_obj_create(NULL);
    lv_obj_clear_flag(ui_table, LV_OBJ_FLAG_SCROLLABLE);
    lv_obj_remove_style_all(ui_table);
    lv_obj_set_size(ui_table, lv_disp_get_hor_res(NULL), lv_disp_get_ver_res(NULL));
    lv_obj_align(ui_table, LV_ALIGN_TOP_LEFT, 0, 0);
    lv_obj_set_style_bg_color(ui_table, lv_color_white(), 0);//lv_palette_main(LV_PALETTE_GREY)
    lv_obj_set_style_bg_opa(ui_table, LV_OPA_100, 0);
	return ui_table;
}

void ndm_chart_close(void)
{
    lv_disp_load_scr(chart_back);
	
	if(ui_chart)
	{
		lv_obj_del(ui_chart);
		ui_chart = NULL;
	}
}
static void chart_exit_event(lv_event_t* e)
{
    ndm_chart_close();
}
static void add_title(lv_obj_t* parent, char* name)
{
    lv_obj_t* titleCont = lv_obj_create(parent);
    lv_obj_set_size(titleCont, 480, 80);
    lv_obj_set_style_pad_all(titleCont, 0, 0);
    lv_obj_t* titlelabel = lv_label_create(titleCont);
    lv_obj_set_style_text_font(titlelabel, &lv_font_montserrat_32, 0);
    lv_obj_center(titlelabel);
    lv_label_set_text(titlelabel, name);
}
lv_obj_t* ndm_chart_show(cJSON * data)
{
    chart_back = lv_scr_act();
    ui_chart = ui_table_init();
    lv_disp_load_scr(ui_chart);
    add_title(ui_chart, "NDM Chart");
    
    chart_display(ui_chart, data);

    lv_obj_t* btnCont = lv_obj_create(ui_chart);
    lv_obj_set_size(btnCont, 480, 80);
    lv_obj_set_style_pad_all(btnCont, 0, 0);
    lv_obj_set_y(btnCont, 720);
    //返回
    lv_obj_t* backBtn = lv_btn_create(btnCont);
    lv_obj_align(backBtn, LV_ALIGN_LEFT_MID, 50, 0);
    //lv_obj_set_style_bg_color(backBtn, lv_color_make(230, 231, 232), 0);
    lv_obj_set_size(backBtn, 120, 65);
    lv_obj_set_style_text_font(backBtn, &lv_font_montserrat_32, 0);
    lv_obj_add_event_cb(backBtn, chart_exit_event, LV_EVENT_CLICKED, NULL);
    lv_obj_set_style_radius(backBtn, LV_RADIUS_CIRCLE, 0);
    lv_obj_t* backlabel = lv_label_create(backBtn);
    lv_label_set_text(backlabel, LV_SYMBOL_BACKSPACE);
    lv_obj_center(backlabel);

    return ui_chart;
}

