#include "obj_lvgl_msg.h"
#include "lv_ix850.h"
#include "common_table_display.h"
#include "MenuNDMManage.h"

static lv_obj_t* ui_table_init(void);
static lv_obj_t* ui_tlogEvent = NULL;
static lv_obj_t* tlogEvent_back = NULL;
static lv_obj_t* ui_event_select = NULL;
static lv_obj_t* event_select_back = NULL;
static COM_TABLE_DISPLAY *tlogEvent_display=NULL;
static cJSON *view_tlogEvent_all=NULL;
static COM_TABLE_DISPLAY *event_select_display=NULL;
static cJSON *event_select_view=NULL;
static lv_obj_t* keyboard_back = NULL;
static lv_obj_t * ui_keyboard=NULL;
static lv_obj_t* kb;
static void add_tlogEvent_control(lv_obj_t* parent);

/*****************
 * tlogEvent cfg
 * **************/
COM_TABLE_DISPLAY* tlogEvent_manage_show(void)
{
	//printf("11111111111%s:%d\n",__func__,__LINE__);	
	view_tlogEvent_all = GetTlogEventCfg();

    tlogEvent_back = lv_scr_act();
    ui_tlogEvent = ui_table_init();
    lv_disp_load_scr(ui_tlogEvent);
	tlogEvent_display = common_table_display_menu(ui_tlogEvent, view_tlogEvent_all, "tlogEvent", 0);
	add_tlogEvent_control(ui_tlogEvent);
	return tlogEvent_display;
}


void tlogEvent_manage_close(void)
{
	//printf("11111111111%s:%d\n",__func__,__LINE__);	
    lv_disp_load_scr(tlogEvent_back);
	if(tlogEvent_display!=NULL)
	{
		common_table_delete(tlogEvent_display);
        tlogEvent_display = NULL;
	}
	if(view_tlogEvent_all)
	{
		cJSON_Delete(view_tlogEvent_all);
		view_tlogEvent_all=NULL;
	}
	if(ui_tlogEvent)
	{
		lv_obj_del(ui_tlogEvent);
		ui_tlogEvent = NULL;
	}
}
void tlogEvent_manage_fresh(void)
{
    //printf("11111111111%s:%d\n",__func__,__LINE__);	
    if(tlogEvent_display==NULL)
    {
        return;
    }
	view_tlogEvent_all = GetTlogEventCfg();
    common_table_fresh(tlogEvent_display, view_tlogEvent_all, 1);
}


static void tlogEvent_exit_event(lv_event_t* e)
{
    tlogEvent_manage_close();
}
static void tlogEvent_add_by_select(lv_event_t* e)
{
	MenuNDMManage_Process(MSG_TLOG_ADD_BY_SELECT, NULL);
}
static void tlogEvent_del_event(lv_event_t* e)
{
    MenuNDMManage_Process(MSG_TLOG_DELETE_ALL, NULL);
}
static void add_tlogEvent_control(lv_obj_t* parent)
{
    lv_obj_t* btnCont = lv_obj_create(parent);
    lv_obj_set_size(btnCont, 480, 80);
    lv_obj_set_style_pad_all(btnCont, 0, 0);
    lv_obj_set_y(btnCont, 720);
    //返回
    lv_obj_t* backBtn = lv_btn_create(btnCont);
    lv_obj_align(backBtn, LV_ALIGN_LEFT_MID, 50, 0);
    //lv_obj_set_style_bg_color(backBtn, lv_color_make(230, 231, 232), 0);
    lv_obj_set_size(backBtn, 120, 65);
    lv_obj_set_style_text_font(backBtn, &lv_font_montserrat_32, 0);
    lv_obj_add_event_cb(backBtn, tlogEvent_exit_event, LV_EVENT_CLICKED, NULL);
    lv_obj_set_style_radius(backBtn, LV_RADIUS_CIRCLE, 0);
    lv_obj_t* backlabel = lv_label_create(backBtn);
    lv_label_set_text(backlabel, LV_SYMBOL_BACKSPACE);
    lv_obj_center(backlabel);
    //add
    lv_obj_t* addBtn = lv_btn_create(btnCont);
    lv_obj_set_size(addBtn, 120, 65);
    lv_obj_set_style_text_font(addBtn, &lv_font_montserrat_32, 0);
    lv_obj_add_event_cb(addBtn, tlogEvent_add_by_select, LV_EVENT_CLICKED, NULL);
    lv_obj_align(addBtn, LV_ALIGN_RIGHT_MID, -50, 0);
    lv_obj_set_style_radius(addBtn, LV_RADIUS_CIRCLE, 0);
    //lv_obj_set_style_bg_color(addBtn, lv_color_make(230, 231, 232), 0);
    lv_obj_t* addlabel = lv_label_create(addBtn);
    lv_label_set_text(addlabel, "ADD");
    lv_obj_center(addlabel);
    //del
    lv_obj_t* delBtn = lv_btn_create(btnCont);
    lv_obj_set_size(delBtn, 120, 65);
    lv_obj_set_style_text_font(delBtn, &lv_font_montserrat_32, 0);
    lv_obj_add_event_cb(delBtn, tlogEvent_del_event, LV_EVENT_CLICKED, NULL);
    lv_obj_align(delBtn, LV_ALIGN_CENTER, 0, 0);
    lv_obj_set_style_radius(delBtn, LV_RADIUS_CIRCLE, 0);
    lv_obj_t* dellabel = lv_label_create(delBtn);
    lv_label_set_text(dellabel, "DEL");
    lv_obj_center(dellabel);
}


/*****************
 * EventList 
 * **************/
static void number_edit_event_cb(lv_event_t* e)
{
    lv_event_code_t code = lv_event_get_code(e);
    lv_obj_t* ta = lv_event_get_target(e);

    if (code == LV_EVENT_FOCUSED) 
    {
        if (lv_indev_get_type(lv_indev_get_act()) != LV_INDEV_TYPE_KEYPAD) 
        {
            lv_keyboard_set_textarea(kb, ta);
            lv_keyboard_set_mode(kb, LV_KEYBOARD_MODE_NUMBER);
            lv_obj_set_height(ta, 800 - lv_obj_get_height(kb));
            lv_obj_clear_flag(kb, LV_OBJ_FLAG_HIDDEN);
            lv_obj_scroll_to_view_recursive(ta, LV_ANIM_OFF);
        }
    }
    else if (code == LV_EVENT_CANCEL) 
    {
        lv_disp_load_scr(keyboard_back);      
        lv_obj_del(ui_keyboard);
        ui_keyboard = NULL;
    }
    else if (code == LV_EVENT_READY)
    {
        char* text = lv_textarea_get_text(ta);
    	if(strlen(text) != 4)
        {
            LV_API_TIPS(" Error code! Please enter it again!",4);
            //API_TIPS(" Error code! Please enter it again!");
            return;
        }
        MenuNDMManage_Process(MSG_TLOG_INPUT_ONE,text);
    }
}
static void create_number_keyboard_menu(void)
{
    keyboard_back = lv_scr_act();
    ui_keyboard = ui_table_init();
    lv_disp_load_scr(ui_keyboard);
    lv_obj_t* keyMenu = lv_obj_create(ui_keyboard);
    lv_obj_set_style_pad_row(keyMenu, 0, 0);
    lv_obj_set_size(keyMenu, 480, 800);
    lv_obj_set_flex_flow(keyMenu, LV_FLEX_FLOW_COLUMN);
    lv_obj_clear_flag(keyMenu, LV_OBJ_FLAG_SCROLLABLE);

    lv_obj_t* name_label = lv_label_create(keyMenu);
    lv_label_set_text(name_label, "EventCode");
    lv_obj_set_size(name_label, 200, 80);
    lv_obj_set_style_text_font(name_label, &lv_font_montserrat_32, 0);

    lv_obj_t* ta = lv_textarea_create(keyMenu);
    lv_obj_set_style_text_font(ta, &lv_font_montserrat_26, 0);
    lv_textarea_add_text(ta, "1000");
    lv_obj_set_size(ta, LV_PCT(100), 300);

    kb = lv_keyboard_create(ui_keyboard);
    lv_obj_set_style_text_font(kb, &lv_font_montserrat_26, 0);
    lv_obj_add_flag(kb, LV_OBJ_FLAG_HIDDEN);

    lv_obj_add_event_cb(ta, number_edit_event_cb, LV_EVENT_ALL, NULL);
    lv_event_send(ta, LV_EVENT_FOCUSED, NULL);
}

static void event_select_exit_event(lv_event_t* e)
{
    event_select_close();
}
static void event_select_input_event(lv_event_t* e)
{    
    create_number_keyboard_menu();
}
static void add_event_select_control(lv_obj_t* parent)
{
    lv_obj_t* btnCont = lv_obj_create(parent);
    lv_obj_set_size(btnCont, 480, 80);
    lv_obj_set_style_pad_all(btnCont, 0, 0);
    lv_obj_set_y(btnCont, 720);
    //返回
    lv_obj_t* backBtn = lv_btn_create(btnCont);
    lv_obj_align(backBtn, LV_ALIGN_LEFT_MID, 50, 0);
    //lv_obj_set_style_bg_color(backBtn, lv_color_make(230, 231, 232), 0);
    lv_obj_set_size(backBtn, 120, 65);
    lv_obj_set_style_text_font(backBtn, &lv_font_montserrat_32, 0);
    lv_obj_add_event_cb(backBtn, event_select_exit_event, LV_EVENT_CLICKED, NULL);
    lv_obj_set_style_radius(backBtn, LV_RADIUS_CIRCLE, 0);
    lv_obj_t* backlabel = lv_label_create(backBtn);
    lv_label_set_text(backlabel, LV_SYMBOL_BACKSPACE);
    lv_obj_center(backlabel);
    //input
    lv_obj_t* addBtn = lv_btn_create(btnCont);
    lv_obj_set_size(addBtn, 120, 65);
    lv_obj_set_style_text_font(addBtn, &lv_font_montserrat_32, 0);
    lv_obj_add_event_cb(addBtn, event_select_input_event, LV_EVENT_CLICKED, NULL);
    lv_obj_align(addBtn, LV_ALIGN_RIGHT_MID, -50, 0);
    lv_obj_set_style_radius(addBtn, LV_RADIUS_CIRCLE, 0);
    //lv_obj_set_style_bg_color(addBtn, lv_color_make(230, 231, 232), 0);
    lv_obj_t* addlabel = lv_label_create(addBtn);
    lv_label_set_text(addlabel, "input");
    lv_obj_center(addlabel);
}

COM_TABLE_DISPLAY* event_select_show(void)
{
	//printf("11111111111%s:%d\n",__func__,__LINE__);	
	event_select_view = GetTlogEventList();

    event_select_back = lv_scr_act();
    ui_event_select = ui_table_init();
    lv_disp_load_scr(ui_event_select);
	event_select_display = common_table_display_menu(ui_event_select, event_select_view, "EventList", MSG_TLOG_SELECT_ONE);
	add_event_select_control(ui_event_select);
	return event_select_display;
}
void event_select_close(void)
{
	//printf("11111111111%s:%d\n",__func__,__LINE__);	
    lv_disp_load_scr(event_select_back);
	if(event_select_display!=NULL)
	{
		common_table_delete(event_select_display);
        event_select_display = NULL;
	}
	if(event_select_view)
	{
		cJSON_Delete(event_select_view);
		event_select_view=NULL;
	}
	if(ui_event_select)
	{
		lv_obj_del(ui_event_select);
		ui_event_select = NULL;
	}
    if(ui_keyboard)
    {
        lv_obj_del(ui_keyboard);
        ui_keyboard = NULL;
    }
}


static lv_obj_t* ui_table_init(void)
{	
    lv_obj_t* ui_table = lv_obj_create(NULL);
    lv_obj_clear_flag(ui_table, LV_OBJ_FLAG_SCROLLABLE);
    lv_obj_remove_style_all(ui_table);
    lv_obj_set_size(ui_table, lv_disp_get_hor_res(NULL), lv_disp_get_ver_res(NULL));
    lv_obj_align(ui_table, LV_ALIGN_TOP_LEFT, 0, 0);
    lv_obj_set_style_bg_color(ui_table, lv_palette_main(LV_PALETTE_GREY), 0);
    lv_obj_set_style_bg_opa(ui_table, LV_OPA_100, 0);
	return ui_table;
}

