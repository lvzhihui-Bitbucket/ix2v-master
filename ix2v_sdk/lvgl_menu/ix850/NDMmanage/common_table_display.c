#include "common_table_display.h"
#include "menu_common.h"

#define COM_TB_CHANGE_PAGE 		    231103901
#define COM_TB_CHANGE_NUM 		    231103902

static void add_title(lv_obj_t* parent, char* name);
static void add_filter(lv_obj_t* parent, char* id);
static void add_table(COM_TABLE_DISPLAY* tb_disp, cJSON* data, int cnt_page);
static void add_change_page(COM_TABLE_DISPLAY* tb_disp, int cnt_page, int max_page, int size);
static void load_table_from_json(COM_TABLE_DISPLAY* tb_disp, cJSON* json, int page);
static void draw_table_event_cb(lv_event_t* e);
static void left_click_event(lv_event_t* e);
static void right_click_event(lv_event_t* e);
static void page_change_event_cb(lv_event_t* e);
static void name_edit_event_cb(lv_event_t* e);
static void table_edit_event_cb(lv_event_t* e);
static void table_add_event(lv_event_t* e);

static lv_obj_t* kb;
/*****************
 * 初始化IXG表浏览器
 * view ： 表显示的所用的json数据
 * page ： 初始化时显示的页数
 * Translate ：
 * **************/
COM_TABLE_DISPLAY* common_table_display_menu(lv_obj_t* parent, cJSON* view, char* name, int msg)
{
	//printf("11111111111%s:%d\n",__func__,__LINE__);	

    COM_TABLE_DISPLAY* td = (COM_TABLE_DISPLAY*)lv_mem_alloc(sizeof(COM_TABLE_DISPLAY));
    lv_memset_00(td, sizeof(COM_TABLE_DISPLAY));

    lv_obj_t* displaymenu = lv_obj_create(parent);
    lv_obj_set_size(displaymenu, 480, 800);
    lv_obj_set_style_pad_all(displaymenu, 0, 0);
    lv_obj_clear_flag(displaymenu, LV_OBJ_FLAG_SCROLLABLE);
    td->table_parent = displaymenu;
  
    td->view_data = view;
    td->msg_type = msg;
    int size = cJSON_GetArraySize(view);    
    td->max_page = size/8 + (size%8 ? 1 : 0);
    td->table = NULL;
    td->cnt_page = 1;

    add_title(displaymenu, name);
    //add_filter(displaymenu, "");
    if(size)  
    {
        add_table(td, view, td->cnt_page);
    }   
    add_change_page(td, 1, td->max_page, size);
	//printf("11111111111%s:%d\n",__func__,__LINE__);	
    return td;
}
/*****************
 * 刷新IXG表显示数据
 * tb_disp ： ixg表浏览器所用结构
 * view ：用于刷新表的数据
 * page ：表格显示的当前页
 * **************/
void common_table_fresh(COM_TABLE_DISPLAY* tb_disp,cJSON* view,int page)
{
	if(tb_disp==NULL)
		return;

    tb_disp->view_data = view;
    int size = cJSON_GetArraySize(view);
    
    tb_disp->cnt_page = page;
    tb_disp->max_page = size/8 + (size%8 ? 1 : 0);
    lv_msg_send(COM_TB_CHANGE_NUM,size);
    lv_msg_send(COM_TB_CHANGE_PAGE,page);
	add_table(tb_disp, tb_disp->view_data, tb_disp->cnt_page);
}
/*****************
 * 创建标题栏
 * parent ：父对象
 * **************/
static void add_title(lv_obj_t* parent, char* name)
{
    lv_obj_t* titleCont = lv_obj_create(parent);
    lv_obj_set_size(titleCont, 480, 80);
    lv_obj_set_style_pad_all(titleCont, 0, 0);
    lv_obj_t* titlelabel = lv_label_create(titleCont);
    lv_obj_set_style_text_font(titlelabel, &lv_font_montserrat_32, 0);
    //lv_obj_set_style_text_color(titlelabel, lv_color_hex(0xff0000), 0);
    lv_obj_center(titlelabel);
    lv_label_set_text(titlelabel, name);
}
/*****************
 * 创建过滤器栏
 * parent ：父对象
 * id ： 当前过滤器使用的IXG-ID
 * **************/
static void add_filter(lv_obj_t* parent,char* id)
{
	char text[100];
    lv_obj_t* filterCont = lv_obj_create(parent);
    lv_obj_set_style_pad_row(filterCont, 0, 0);
    lv_obj_set_size(filterCont, LV_PCT(100), 100);
    lv_obj_set_flex_flow(filterCont, LV_FLEX_FLOW_ROW);
    lv_obj_set_style_text_font(filterCont, &lv_font_montserrat_32, 0);
    lv_obj_clear_flag(filterCont, LV_OBJ_FLAG_SCROLLABLE);
    lv_obj_set_y(filterCont, 80);

    lv_obj_t* filter_label = lv_label_create(filterCont);
    lv_label_set_text(filter_label, text);
    lv_obj_set_size(filter_label, 200, LV_SIZE_CONTENT);

    lv_obj_t* ta = lv_textarea_create(filterCont);
    lv_textarea_add_text(ta, id);
    lv_obj_set_size(ta, 200, LV_SIZE_CONTENT);
    lv_obj_clear_state(ta, LV_STATE_FOCUSED);

    kb = lv_keyboard_create(parent);
    lv_obj_set_style_text_font(kb, &lv_font_montserrat_26, 0);
    lv_obj_add_flag(kb, LV_OBJ_FLAG_HIDDEN);

    //lv_obj_add_event_cb(ta, IXG_edit_event_cb, LV_EVENT_ALL, NULL);
}

/*****************
 * 显示一张表
 * parent ：table的父对象
 * data ： 用于显示的json数据
 * cnt_page ： 当前页
 * **************/
static void add_table(COM_TABLE_DISPLAY* tb_disp,cJSON* data,int cnt_page)
{   
	if(tb_disp->table == NULL)
    {
        lv_obj_t* table = lv_table_create(tb_disp->table_parent);
        lv_obj_set_size(table, 480, 560);
        lv_obj_set_y(table, 80);
        lv_obj_add_event_cb(table, draw_table_event_cb, LV_EVENT_DRAW_PART_BEGIN, NULL);
        lv_obj_add_event_cb(table, table_edit_event_cb, LV_EVENT_VALUE_CHANGED, tb_disp);
        tb_disp->table = table; 
    }
    lv_table_set_row_cnt(tb_disp->table,0);
    lv_table_set_col_cnt(tb_disp->table,0);
    lv_table_set_row_height(tb_disp->table,65);
    for(int i=0;i<4;i++)
    {
        lv_table_set_col_width(tb_disp->table, i, 230);
    }
    #if 0
    lv_table_set_row_cnt(tb_disp->table,0);
    lv_table_set_col_cnt(tb_disp->table,0);

    lv_table_set_col_width(tb_disp->table, 0, 220);
    lv_table_set_col_width(tb_disp->table, 1, 220);
    lv_table_set_col_width(tb_disp->table, 2, 200);
    lv_table_set_col_width(tb_disp->table, 3, 220);
    //lv_table_set_col_width(tb_disp->table, 4, 120);
    //lv_table_set_col_width(tb_disp->table, 5, 200);
    #endif
    load_table_from_json(tb_disp, data, cnt_page);   
}
/*****************
 * 表绘画事件
 * 控制表格字体大小,位置,背景色
 * **************/
static void draw_table_event_cb(lv_event_t* e)
{
    lv_obj_t* obj = lv_event_get_target(e);
    lv_obj_draw_part_dsc_t* dsc = lv_event_get_draw_part_dsc(e);
    /*If the cells are drawn...*/
    if (dsc->part == LV_PART_ITEMS) {
        uint32_t row = dsc->id / lv_table_get_col_cnt(obj);
        uint32_t col = dsc->id - row * lv_table_get_col_cnt(obj);

        dsc->label_dsc->font = &lv_font_montserrat_24;
        dsc->label_dsc->align = LV_TEXT_ALIGN_CENTER;
        //dsc->label_dsc->flag = LV_TEXT_FLAG_EXPAND;

        /*Make the texts in the first cell center aligned*/
        if (row == 0) {
            dsc->label_dsc->align = LV_TEXT_ALIGN_CENTER;
            dsc->rect_dsc->bg_color = lv_color_mix(lv_palette_main(LV_PALETTE_BLUE), dsc->rect_dsc->bg_color, LV_OPA_20);
            dsc->rect_dsc->bg_opa = LV_OPA_COVER;
        }
    }
}
/*****************
 * 表编辑事件,表格子被点击以后触发
 * **************/
static void table_edit_event_cb(lv_event_t* e)
{
    lv_obj_t* obj = lv_event_get_target(e);
    COM_TABLE_DISPLAY* td = lv_event_get_user_data(e);

    uint16_t col;
    uint16_t row;
    lv_table_get_selected_cell(obj, &row, &col);    //获取格子所在的行,列
    uint16_t cnt = lv_table_get_row_cnt(obj);
    if (row >= cnt || row == 0)
    {
        return;
    }

    cJSON* data = cJSON_GetArrayItem(td->view_data, (td->cnt_page-1)*8+row-1);       //获取格子所在的整条记录
	//printf("11111111 row=%d,cnt=%d,td->cnt_page=%d\n",row,cnt,td->cnt_page);    
    //printf_json((cJSON*)data,__func__);
	MenuNDMManage_Process(td->msg_type, data);
}
/*****************
 * 创建页面显示栏
 * parent : 父对象
 * cnt_page : 当前页
 * max_page ：最大页数
 * size ： 表格总记录数
 * **************/
static void page_change_event_cb(lv_event_t* e)
{
    lv_obj_t* label = lv_event_get_target(e);
    COM_TABLE_DISPLAY* td = lv_event_get_user_data(e);
    lv_msg_t* m = lv_event_get_msg(e);
    int32_t val = (int32_t)lv_msg_get_payload(m);
    char buf[100];
    sprintf(buf, "%d/%d", val, td->max_page);
    lv_label_set_text(label, buf);
}
static void cnt_change_event_cb(lv_event_t* e)
{
    lv_obj_t* label = lv_event_get_target(e);
    if(label == NULL)
        return;
    lv_msg_t* m = lv_event_get_msg(e);
    int32_t val = (int32_t)lv_msg_get_payload(m);
    char buf[100];
    sprintf(buf, "%d", val);
    lv_label_set_text(label, buf);
}
static void add_change_page(COM_TABLE_DISPLAY* tb_disp, int cnt_page,int max_page,int size)
{
    //��ҳ���ƿ�
    lv_obj_t* pageControl = lv_obj_create(tb_disp->table_parent);
    lv_obj_set_size(pageControl, 480, 80);
    lv_obj_set_style_pad_all(pageControl, 0, 0);
    lv_obj_set_y(pageControl, 640);

    lv_obj_t* leftBtn = lv_btn_create(pageControl);
    lv_obj_align(leftBtn, LV_ALIGN_LEFT_MID, 20, 0);
    lv_obj_set_style_text_color(leftBtn, lv_color_hex(0xff0000), 0);
    lv_obj_set_style_bg_color(leftBtn, lv_color_make(230, 231, 232), 0);
    lv_obj_set_size(leftBtn, 80, 65);
    lv_obj_set_style_text_font(leftBtn, &lv_font_montserrat_32, 0);
    lv_obj_add_event_cb(leftBtn, left_click_event, LV_EVENT_CLICKED, tb_disp);
    lv_obj_set_style_radius(leftBtn, LV_RADIUS_CIRCLE, 0);
    lv_obj_t* leftlabel = lv_label_create(leftBtn);
    lv_label_set_text(leftlabel, LV_SYMBOL_LEFT);
    lv_obj_center(leftlabel);

    char buf[100];
    sprintf(buf, "%d/%d", cnt_page, max_page);

    lv_obj_t* dispagelabel = lv_label_create(pageControl);
    lv_label_set_text(dispagelabel, buf);
    lv_obj_set_style_text_font(dispagelabel, &lv_font_montserrat_32, 0);
    lv_obj_align(dispagelabel, LV_ALIGN_CENTER, -60, 0);
    lv_msg_subsribe_obj(COM_TB_CHANGE_PAGE, dispagelabel, NULL);
    lv_obj_add_event_cb(dispagelabel, page_change_event_cb, LV_EVENT_MSG_RECEIVED, tb_disp);

    lv_obj_t* rightBtn = lv_btn_create(pageControl);
    lv_obj_align(rightBtn, LV_ALIGN_CENTER, 50, 0);
    lv_obj_set_style_text_color(rightBtn, lv_color_hex(0xff0000), 0);
    lv_obj_set_style_bg_color(rightBtn, lv_color_make(230, 231, 232), 0);
    lv_obj_set_size(rightBtn, 80, 65);
    lv_obj_set_style_text_font(rightBtn, &lv_font_montserrat_32, 0);
    lv_obj_add_event_cb(rightBtn, right_click_event, LV_EVENT_CLICKED, tb_disp);
    lv_obj_set_style_radius(rightBtn, LV_RADIUS_CIRCLE, 0);
    lv_obj_t* rightlabel = lv_label_create(rightBtn);
    lv_label_set_text(rightlabel, LV_SYMBOL_RIGHT);
    lv_obj_center(rightlabel);

    sprintf(buf, "%d", size);
    lv_obj_t* discntlabel = lv_label_create(pageControl);
    lv_label_set_text(discntlabel, buf);
    lv_obj_set_style_text_font(discntlabel, &lv_font_montserrat_32, 0);
    lv_obj_align(discntlabel, LV_ALIGN_RIGHT_MID, -50, 0);
    lv_msg_subsribe_obj(COM_TB_CHANGE_NUM, discntlabel, NULL);
    lv_obj_add_event_cb(discntlabel, cnt_change_event_cb, LV_EVENT_MSG_RECEIVED, NULL);
}

/*****************
 * 左翻页
 * **************/
static void left_click_event(lv_event_t* e)
{
    COM_TABLE_DISPLAY* td = lv_event_get_user_data(e);
    td->cnt_page--;
    if (td->cnt_page <= 0)
    {
        td->cnt_page = td->max_page;
    }
    lv_msg_send(COM_TB_CHANGE_PAGE,td->cnt_page);
    add_table(td, td->view_data, td->cnt_page);
}
/*****************
 * 右翻页
 * **************/
static void right_click_event(lv_event_t* e)
{
    COM_TABLE_DISPLAY* td = lv_event_get_user_data(e);
    td->cnt_page++;
    if (td->cnt_page >td->max_page)
    {
        td->cnt_page = 1;
    }
    lv_msg_send(COM_TB_CHANGE_PAGE,td->cnt_page);
    add_table(td, td->view_data, td->cnt_page);
}

/*****************
 * 加载表的数据
 * obj ：table对象
 * json ： 用于加载的json数据
 * page ： 当前页
 * **************/
static void load_table_from_json(COM_TABLE_DISPLAY* tb_disp, cJSON* json,int page)
{
    int size = cJSON_GetArraySize(json);
    char text[100];
    for (int i = 0; i < 8; i++)
    {
        int cnt = ((page - 1) * 8) + i;
        cJSON* item = cJSON_GetArrayItem(json, cnt);
        if (item == NULL)
        {
            break;
        }
        int length = cJSON_GetArraySize(item);
        if(length > 5) length = 5;
        for (int j = 0; j < length; j++)
        {
            cJSON* val = cJSON_GetArrayItem(item, j);
            if (val == NULL)
            {
                continue;
            }
            if (i == 0)
            {
                lv_table_set_cell_value(tb_disp->table, i, j, val->string);
            }
            if (cJSON_IsString(val))
            {
                lv_table_set_cell_value(tb_disp->table, i + 1, j, val->valuestring);
            }
            else if (cJSON_IsNumber(val))
            {
                sprintf(text, "%f", val->valuedouble);
                lv_table_set_cell_value(tb_disp->table, i + 1, j, text);
            }
            else if(cJSON_IsBool(val))
            {
                sprintf(text, "%s", cJSON_IsTrue(val)? "true" : "false");
                lv_table_set_cell_value(tb_disp->table, i + 1, j, text);
            }
            else if(cJSON_IsObject(val))
            {
                char *str=cJSON_PrintUnformatted(val);
                //printf("load_table_from_json =%s\n",str);
                snprintf(text, 100, "%s", str);
                lv_table_set_cell_value(tb_disp->table, i + 1, j, text);
                free(str);
                #if 0
                if (cJSON_IsString(val2))
                    snprintf(text, 100, "%s:%s", val2->string, val2->valuestring);
                else if(cJSON_IsNumber(val2)) 
                    snprintf(text, 100, "%s:%d", val2->string, val2->valueint); 
                else
                    snprintf(text, 100, "%s:%s", val2->string, cJSON_IsTrue(val2)? "true":"false");   
                lv_table_set_cell_value(tb_disp->table, i + 1, j, text);
                #endif
            }
        }
    }
}


/*****************
 * 表删除
 * td ：表浏览器初始化时返回的结构指针
 * **************/
bool common_table_delete(COM_TABLE_DISPLAY* td)
{
    if (td)
    {   

        if (td->table)
        {
            lv_obj_del(td->table);
            td->table = NULL;
        }

        if (td->table_parent)
        {
            lv_obj_del(td->table_parent);
            td->table_parent = NULL;
        }

        if (td->view_data)
        {
            //cJSON_Delete(td->view_data);
            td->view_data = NULL;
        }

        if(kb){
            lv_obj_del(kb);
            kb = NULL;
        }
        lv_mem_free(td);
        td = NULL;
        return true;
    }
    else
        return false;
}
