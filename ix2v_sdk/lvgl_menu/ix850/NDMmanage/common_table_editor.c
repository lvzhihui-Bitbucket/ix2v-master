#include "menu_utility.h"
#include "MenuNDMManage.h"

static lv_obj_t* kb;
static lv_obj_t* editor_menu_return = NULL;
static lv_obj_t* ui_record = NULL;
static lv_obj_t* editor_keyboard_return = NULL;
static lv_obj_t * ui_keyboard=NULL;
static cJSON* rec_field;
static cJSON* Description=NULL;

lv_obj_t* create_record_editor_menu(cJSON* rec_data, const cJSON* editabel, const cJSON* tb_description);
static lv_obj_t* create_text_editor(lv_obj_t* parent, char* name, char* val, int editable,cJSON* description);
static lv_obj_t* create_number_editor(lv_obj_t* parent, char* name, int val, int able,cJSON* description);
static void save_editor_event(lv_event_t* e);
static void cancel_editor_event(lv_event_t* e);
static void del_record_event(lv_event_t* e);
static lv_obj_t* ui_record_init(void);


static void number_edit_event_cb(lv_event_t* e)
{
    lv_event_code_t code = lv_event_get_code(e);
    lv_obj_t* ta = lv_event_get_target(e);
    lv_obj_t* label = lv_event_get_user_data(e);

    if (code == LV_EVENT_FOCUSED) 
    {
        if (lv_indev_get_type(lv_indev_get_act()) != LV_INDEV_TYPE_KEYPAD) 
        {
            lv_keyboard_set_textarea(kb, ta);
            lv_keyboard_set_mode(kb, LV_KEYBOARD_MODE_NUMBER);
            lv_obj_set_height(ta, 800 - lv_obj_get_height(kb));
            lv_obj_clear_flag(kb, LV_OBJ_FLAG_HIDDEN);
            lv_obj_scroll_to_view_recursive(ta, LV_ANIM_OFF);
        }
    }
    else if (code == LV_EVENT_CANCEL) 
    {
        lv_disp_load_scr(editor_keyboard_return);      
        lv_obj_del(ui_keyboard);
        ui_keyboard = NULL;
    }
    else if (code == LV_EVENT_READY)
    {
    	lv_obj_t* nameLabel = lv_obj_get_child(lv_obj_get_parent(label), 0);
        char* name = lv_label_get_text(nameLabel);
        char* text = lv_textarea_get_text(ta);
        if(Description)
        {
            if(API_Data_Edit_Check(name,text,Description)==0)
            {
                LV_API_TIPS(" Error! Please enter it again!",4);
                //API_TIPS(" Error! Please enter it again!");
                return;
            }
        }
        lv_label_set_text(label, text);
        cJSON_ReplaceItemInObject(rec_field, name, cJSON_CreateNumber(atoi(text)));
        lv_disp_load_scr(editor_keyboard_return);      
        lv_obj_del(ui_keyboard);
        ui_keyboard = NULL;
    }
}

static void create_number_keyboard_menu(lv_obj_t* target, char* val)
{
    editor_keyboard_return = lv_scr_act();
    ui_keyboard = ui_record_init();
    lv_disp_load_scr(ui_keyboard);
    lv_obj_t* keyMenu = lv_obj_create(ui_keyboard);
    lv_obj_set_style_pad_row(keyMenu, 0, 0);
    lv_obj_set_size(keyMenu, 480, 800);
    lv_obj_set_flex_flow(keyMenu, LV_FLEX_FLOW_COLUMN);
    lv_obj_clear_flag(keyMenu, LV_OBJ_FLAG_SCROLLABLE);

    lv_obj_t* nameLabel = lv_obj_get_child(lv_obj_get_parent(target), 0);
    char* name = lv_label_get_text(nameLabel);

    lv_obj_t* name_label = lv_label_create(keyMenu);
    lv_label_set_text(name_label, name);
    lv_obj_set_size(name_label, 200, 80);
    lv_obj_set_style_text_font(name_label, &lv_font_montserrat_32, 0);

    lv_obj_t* ta = lv_textarea_create(keyMenu);
    lv_obj_set_style_text_font(ta, &lv_font_montserrat_26, 0);
    lv_textarea_add_text(ta, val);
    lv_obj_set_size(ta, LV_PCT(100), 300);

    kb = lv_keyboard_create(ui_keyboard);
    lv_obj_set_style_text_font(kb, &lv_font_montserrat_26, 0);
    lv_obj_add_flag(kb, LV_OBJ_FLAG_HIDDEN);

    lv_obj_add_event_cb(ta, number_edit_event_cb, LV_EVENT_ALL, target);
    lv_event_send(ta, LV_EVENT_FOCUSED, NULL);
}

static void number_click_event(lv_event_t* e)
{
    cJSON* description = lv_event_get_user_data(e);
    lv_obj_t* valueLabel = lv_event_get_target(e);
    char* text = lv_label_get_text(valueLabel);
    create_number_keyboard_menu(valueLabel, text);
}

static void text_edit_event_cb(lv_event_t* e)
{
    lv_event_code_t code = lv_event_get_code(e);
    lv_obj_t* ta = lv_event_get_target(e);
    lv_obj_t* label = lv_event_get_user_data(e);    
    lv_obj_t* nameLabel = lv_obj_get_child(lv_obj_get_parent(label), 0);
    char* name = lv_label_get_text(nameLabel);

    if (code == LV_EVENT_FOCUSED) 
    {
        if (lv_indev_get_type(lv_indev_get_act()) != LV_INDEV_TYPE_KEYPAD) 
        {
            lv_keyboard_set_textarea(kb, ta);
            if(strcmp(name,"IX_NAME")==0)
            {
                lv_keyboard_set_mode(kb, LV_KEYBOARD_MODE_TEXT_LOWER);
            }
            else
            {
                lv_keyboard_set_mode(kb, LV_KEYBOARD_MODE_NUMBER);
            }
        }
    }
    else if (code == LV_EVENT_CANCEL) 
    {

        lv_disp_load_scr(editor_keyboard_return);      
        lv_obj_del(ui_keyboard);
        ui_keyboard = NULL;
    }
    else if (code == LV_EVENT_READY)
    {
        char* text = lv_textarea_get_text(ta);
        if(Description)
        {
            if(API_Data_Edit_Check(name,text,Description)==0)
            {
                LV_API_TIPS(" Error! Please enter it again!",4);
                //API_TIPS(" Error! Please enter it again!");
                return;
            }
        }
        lv_label_set_text(label, text);
        cJSON_ReplaceItemInObject(rec_field, name, cJSON_CreateString(text));
        lv_disp_load_scr(editor_keyboard_return);      
        lv_obj_del(ui_keyboard);
        ui_keyboard = NULL;            
    }
}

static void create_string_keyboard_menu(lv_obj_t* target, char* val)
{
    editor_keyboard_return = lv_scr_act();
    ui_keyboard = ui_record_init();   
    lv_disp_load_scr(ui_keyboard);
    lv_obj_t* keyMenu = lv_obj_create(ui_keyboard);
    lv_obj_set_style_pad_row(keyMenu, 0, 0);
    lv_obj_set_size(keyMenu, 480, 800);
    lv_obj_set_flex_flow(keyMenu, LV_FLEX_FLOW_COLUMN);
    lv_obj_clear_flag(keyMenu, LV_OBJ_FLAG_SCROLLABLE);

    lv_obj_t* nameLabel = lv_obj_get_child(lv_obj_get_parent(target), 0);
    char* name = lv_label_get_text(nameLabel);

    lv_obj_t* name_label = lv_label_create(keyMenu);
    lv_label_set_text(name_label, name);
    lv_obj_set_size(name_label, 200, 80);
    lv_obj_set_style_text_font(name_label, &lv_font_montserrat_32, 0);


    lv_obj_t* ta = lv_textarea_create(keyMenu);
    lv_obj_set_style_text_font(ta, &lv_font_montserrat_26, 0);
    lv_textarea_add_text(ta, val);
    lv_obj_set_size(ta, LV_PCT(100), 300);
    //lv_obj_set_y(ta, 100);

    kb = lv_keyboard_create(ui_keyboard);
    lv_obj_set_style_text_font(kb, &lv_font_montserrat_26, 0);
    //lv_obj_add_flag(kb, LV_OBJ_FLAG_HIDDEN);

    lv_obj_add_event_cb(ta, text_edit_event_cb, LV_EVENT_ALL, target);
    lv_event_send(ta, LV_EVENT_FOCUSED, NULL);
}

static void text_click_event(lv_event_t* e)
{
    cJSON* description = lv_event_get_user_data(e);
    lv_obj_t* valueLabel = lv_event_get_target(e);
    char* text = lv_label_get_text(valueLabel);
    
    create_string_keyboard_menu(valueLabel, text);
}

static lv_obj_t* create_text_editor(lv_obj_t* parent, char* name, char* val, int able,cJSON* description)
{
    lv_obj_t* obj = lv_obj_create(parent);
    lv_obj_set_style_pad_row(obj, 0, 0);
    lv_obj_set_size(obj, LV_PCT(100), LV_SIZE_CONTENT);
    lv_obj_set_flex_flow(obj, LV_FLEX_FLOW_ROW);
    lv_obj_set_style_text_font(obj, &lv_font_montserrat_26, 0);
    lv_obj_clear_flag(obj, LV_OBJ_FLAG_SCROLLABLE);

    lv_obj_t* text_label = lv_label_create(obj);
    lv_label_set_text(text_label, name);
    lv_obj_set_size(text_label, 200, LV_SIZE_CONTENT);

    lv_obj_t* value_label = lv_label_create(obj);
    lv_label_set_text(value_label, val);   
    lv_obj_set_ext_click_area(value_label, 30);
    lv_label_set_long_mode(value_label, LV_LABEL_LONG_DOT);
    lv_obj_set_size(value_label, 200, LV_SIZE_CONTENT);
    if(able)
    {
        lv_obj_add_flag(value_label, LV_OBJ_FLAG_CLICKABLE);
        lv_obj_add_event_cb(value_label, text_click_event, LV_EVENT_CLICKED, description);
        
	}
	
    return value_label;
}

static lv_obj_t* create_number_editor(lv_obj_t* parent, char* name, int val,int able, cJSON* description)
{
    lv_obj_t* obj = lv_obj_create(parent);
    lv_obj_set_style_pad_row(obj, 0, 0);
    lv_obj_set_size(obj, LV_PCT(100), LV_SIZE_CONTENT);
    lv_obj_set_flex_flow(obj, LV_FLEX_FLOW_ROW);
    lv_obj_set_style_text_font(obj, &lv_font_montserrat_32, 0);
    lv_obj_clear_flag(obj, LV_OBJ_FLAG_SCROLLABLE);

    lv_obj_t* text_label = lv_label_create(obj);
    lv_label_set_text(text_label, name);
    lv_obj_set_size(text_label, 200, LV_SIZE_CONTENT);

    lv_obj_t* value_label = lv_label_create(obj);
    lv_label_set_text_fmt(value_label, "%d", val);
    lv_obj_add_flag(value_label, LV_OBJ_FLAG_CLICKABLE);
    lv_obj_set_ext_click_area(value_label, 30);
    lv_label_set_long_mode(value_label, LV_LABEL_LONG_DOT);
    lv_obj_set_size(value_label, 200, LV_SIZE_CONTENT);
	if(able)
    {
        lv_obj_add_flag(value_label, LV_OBJ_FLAG_CLICKABLE);    
        lv_obj_add_event_cb(value_label, number_click_event, LV_EVENT_CLICKED, description);
	}
	
    return value_label;
}

lv_obj_t* create_record_editor_menu(cJSON* rec_data, const cJSON* editabel, const cJSON* tb_description)
{
	editor_menu_return = lv_scr_act();
	ui_record = ui_record_init();
    lv_disp_load_scr(ui_record);
    	
    rec_field = rec_data;
    Description = tb_description;
    lv_obj_t* editmenu = lv_obj_create(ui_record);
    lv_obj_set_size(editmenu, 480, 800);
    lv_obj_set_style_pad_all(editmenu, 0, 0);
    lv_obj_clear_flag(editmenu, LV_OBJ_FLAG_SCROLLABLE);

    lv_obj_t* btnCont = lv_obj_create(editmenu);
    lv_obj_set_size(btnCont, 480, 100);
    lv_obj_set_style_pad_all(btnCont, 0, 0);
    lv_obj_set_y(btnCont, 700);
    //save
    lv_obj_t* saveBtn = lv_btn_create(btnCont);
    lv_obj_set_size(saveBtn, 120, 70);
    lv_obj_set_style_text_font(saveBtn, &lv_font_montserrat_32, 0);
    lv_obj_add_event_cb(saveBtn, save_editor_event, LV_EVENT_CLICKED, NULL);
    lv_obj_align(saveBtn, LV_ALIGN_RIGHT_MID, 20, 0);
    lv_obj_set_style_radius(saveBtn, LV_RADIUS_CIRCLE, 0);
    lv_obj_set_style_bg_color(saveBtn, lv_color_make(230, 231, 232), 0);
    lv_obj_set_style_text_color(saveBtn, lv_color_hex(0xff0000), 0);
    lv_obj_t* savelabel = lv_label_create(saveBtn);
    lv_label_set_text(savelabel, "save");
    lv_obj_center(savelabel);
    //del
    lv_obj_t* delBtn = lv_btn_create(btnCont);
    lv_obj_set_size(delBtn, 120, 70);
    lv_obj_set_style_text_font(delBtn, &lv_font_montserrat_32, 0);
    lv_obj_add_event_cb(delBtn, del_record_event, LV_EVENT_CLICKED, NULL);
    lv_obj_align(delBtn, LV_ALIGN_CENTER, 0, 0);
    lv_obj_set_style_radius(delBtn, LV_RADIUS_CIRCLE, 0);
    lv_obj_set_style_bg_color(delBtn, lv_color_make(230, 231, 232), 0);
    lv_obj_set_style_text_color(delBtn, lv_color_hex(0xff0000), 0);
    lv_obj_t* dellabel = lv_label_create(delBtn);
    lv_label_set_text(dellabel, "delete");
    lv_obj_center(dellabel);
    //exit
    lv_obj_t* cancelBtn = lv_btn_create(btnCont);
    lv_obj_align(cancelBtn, LV_ALIGN_LEFT_MID, -20, 0);
    lv_obj_set_style_text_color(cancelBtn, lv_color_hex(0xff0000), 0);
    lv_obj_set_style_bg_color(cancelBtn, lv_color_make(230, 231, 232), 0);
    lv_obj_set_size(cancelBtn, 120, 70);
    lv_obj_set_style_text_font(cancelBtn, &lv_font_montserrat_32, 0);
    lv_obj_add_event_cb(cancelBtn, cancel_editor_event, LV_EVENT_CLICKED, NULL);
    lv_obj_set_style_radius(cancelBtn, LV_RADIUS_CIRCLE, 0);
    lv_obj_t* cancellabel = lv_label_create(cancelBtn);
    lv_label_set_text(cancellabel, LV_SYMBOL_BACKSPACE);
    lv_obj_center(cancellabel);

    lv_obj_t* editCont = lv_obj_create(editmenu);
    lv_obj_set_size(editCont, 480, 700);
    lv_obj_set_flex_flow(editCont, LV_FLEX_FLOW_COLUMN);
    lv_obj_set_style_pad_all(editCont, 0, 0);
    lv_obj_set_style_pad_row(editCont, 0, 0);

    int rec_size = cJSON_GetArraySize(rec_data);
    int able_size = cJSON_GetArraySize(editabel);
    int val;	
    lv_obj_t* val_lable;
    for (int i = 0; i < rec_size; i++)
    {
        //获取一个字段
        cJSON* field = cJSON_GetArrayItem(rec_data, i);
	    val=0;	
        for (int j = 0; j < able_size; j++)
        {   //获取是否编辑的字段
            cJSON* able = cJSON_GetArrayItem(editabel, j);
            //获取比较结果
            if(strcmp(able->valuestring, field->string)==0)
            {
                val=1;
                break;
		    }
        }  
        if(cJSON_IsString(field))
            val_lable=create_text_editor(editCont, field->string, field->valuestring, val,tb_description);
        if(cJSON_IsNumber(field))
            val_lable=create_number_editor(editCont, field->string, field->valueint, val,tb_description);
    }
    return editmenu;
}
void record_editor_close(void)
{
    if(editor_menu_return)
    {
        lv_disp_load_scr(editor_menu_return);
        editor_menu_return=NULL;
    }
    if(ui_record)
    {
        lv_obj_del(ui_record); 
        ui_record = NULL;
    }   
}

static void save_editor_event(lv_event_t* e)
{  
    MenuNDMManage_Process(MSG_IXRLIST_EDIT_SAVE,rec_field);
}

static void del_record_event(lv_event_t* e)
{  
	MenuNDMManage_Process(MSG_IXRLIST_DELETE_ONE,rec_field);
}

static void cancel_editor_event(lv_event_t* e)
{   
    record_editor_close();
}

static lv_obj_t* ui_record_init(void)
{
    lv_obj_t* ui_record = lv_obj_create(NULL);
    lv_obj_clear_flag(ui_record, LV_OBJ_FLAG_SCROLLABLE);
    lv_obj_remove_style_all(ui_record);
    lv_obj_set_size(ui_record, lv_disp_get_hor_res(NULL), lv_disp_get_ver_res(NULL));
    lv_obj_align(ui_record, LV_ALIGN_TOP_LEFT, 0, 0);
    lv_obj_set_style_bg_color(ui_record, lv_palette_main(LV_PALETTE_GREY), 0);
    lv_obj_set_style_bg_opa(ui_record, LV_OPA_100, 0);
    return ui_record;
}
