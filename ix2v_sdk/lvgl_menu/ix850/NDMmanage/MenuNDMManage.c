#include "obj_lvgl_msg.h"
#include "lv_ix850.h"
#include "obj_TableSurver.h"
#include "task_Event.h"
#include "obj_PublicInformation.h"
#include "define_file.h"
#include "MenuNDMManage.h"
#include "common_table_display.h"
#include "obj_IoInterface.h"

static cJSON *edit_record=NULL;
static int add_new_item=0;
void *ndm_server_tips=NULL;
/*****************
 * CERT MANAGE
 * **************/
int certCnt=0;
int nocertCnt=0;
static cJSON *certlist_all=NULL;
void *cert_manage_tips=NULL;

int GetCertCnt(void)
{
	return certCnt;
}
int GetNocertCnt(void)
{
	return nocertCnt;
}
int GetOnlineCnt(void)
{
	#if 0
	static int list_init=0;
	if(list_init==0)
	{
		list_init=1;
		API_Event_By_Name(Event_IXS_ReqUpdateTb);
	}
	#endif
	cJSON* devTb = NULL;
	IXS_ProxyGetTb(&devTb, NULL, 1);
	int num = API_TB_Count(devTb, NULL);
	printf("GetOnlineCnt = %d\n", num);
	cJSON_Delete(devTb);
	return num;
}

cJSON* GetCertState(void)
{
	cJSON* element;
	char* ipString;
	char* pbCert;
	cJSON* cert_State;
	cJSON* devTb = NULL;
	cJSON* view = cJSON_CreateArray();
	IXS_ProxyGetTb(&devTb, NULL, 1);
	certCnt = 0;
	nocertCnt = 0;
	cJSON_ArrayForEach(element, devTb)
	{
		ipString = GetEventItemString(element, "IP_ADDR");
		if(ipString)
		{
			cJSON* cert_view=cJSON_CreateObject();

			if(!strcmp(ipString, GetSysVerInfo_IP()))
			{
				pbCert = API_PublicInfo_Read_String(PB_CERT_STATE);
				if(!strcmp(pbCert, "Delivered"))
				{
					certCnt ++;
				}
				else
				{
					nocertCnt ++;
				}
				cJSON_AddStringToObject(cert_view, "CERT_State", pbCert);
			}
			else
			{
				cert_State = API_GetRemotePb(inet_addr(ipString), PB_CERT_STATE);
				if(cert_State)
				{
					if(!strcmp(cert_State->valuestring, "Delivered"))
					{
						certCnt ++;
					}
					else
					{
						nocertCnt ++;
					}
					cJSON_AddStringToObject(cert_view, "CERT_State", cert_State->valuestring);
					cJSON_Delete(cert_State);
				}
				else
				{
					nocertCnt ++;
					cJSON_AddStringToObject(cert_view, "CERT_State", "Configurable");
				}
			}
			cJSON_AddStringToObject(cert_view, "IP_ADDR", ipString);
			cJSON_AddStringToObject(cert_view, "IX_ADDR", GetEventItemString(element, "IX_ADDR"));
			cJSON_AddStringToObject(cert_view, "MFG_SN", GetEventItemString(element, "MFG_SN"));
			cJSON_AddItemToArray(view,cert_view);
		}
	}
	#if 0
	char *str=cJSON_Print(view);
	printf("GetCertState =%s\n",str);
	free(str);
	#endif
	cJSON_Delete(devTb);
	return view;
}
cJSON* GetCertList(int mode)
{
	printf("11111111111%s mode=%d:%d\n",__func__, mode, __LINE__);	
	if(certlist_all)
	{
		if(mode == 0)
		{
			return cJSON_Duplicate(certlist_all,1);
		}
		else 
		{
			cJSON* view = cJSON_CreateArray();
			cJSON* where = cJSON_CreateObject();
			cJSON_AddStringToObject(where, "CERT_State", mode==1 ? "Delivered" : "Configurable");
			API_TB_SelectBySort(certlist_all, view, where, 0);
			#if 0
			char *str=cJSON_Print(view);
			printf("GetCertList =%s\n",str);
			free(str);
			#endif
			cJSON_Delete(where);
			return view;
		}
	}
	else
	{

	}
}

int API_Event_CertManage(char* cmd)
{
	int ret = 0;
	cJSON *event = cJSON_CreateObject();
	cJSON_AddStringToObject(event, EVENT_KEY_EventName, EventCertManage);
	cJSON_AddStringToObject(event, "CMD", cmd);
	ret = API_Event_Json(event);
	cJSON_Delete(event);

	return ret;
}
static int CertManage_TimerCallback(int timing)
{
	if(timing == 2)
	{
		API_Event_CertManage("update");
		return 2;
	}
	return 0;
}
int CertManageCallback(cJSON *cmd)
{
	char* opcode = GetEventItemString(cmd, "CMD");

	if(!strcmp(opcode, "update"))
	{
		if(certlist_all)
		{
			cJSON_Delete(certlist_all);
			certlist_all=NULL;
		}
		certlist_all = GetCertState();
		if(cert_manage_tips!=NULL)
		{
			API_TIPS_Close_Ext(cert_manage_tips);
			cert_manage_tips=NULL;
		}
		cert_update_cnt();
	}
	else if(!strcmp(opcode, "create"))
	{
		create_all_cert();
		API_Add_TimingCheck(CertManage_TimerCallback, 2);
	}
	else if(!strcmp(opcode, "cancel"))
	{
		cancel_all_cert();
		API_Add_TimingCheck(CertManage_TimerCallback, 2);
	}
	
}

void Send_CertEvent(char* ip, char* cmd)
{
	cJSON *send_event=cJSON_CreateObject();
	cJSON_AddStringToObject(send_event, EVENT_KEY_EventName, EventCertConfig);
	cJSON_AddStringToObject(send_event, "OPCODE", cmd);
	if(!strcmp(ip, GetSysVerInfo_IP()))
	{
		API_Event_Json(send_event);
	}
	else
	{
		API_XD_EventJson(inet_addr(ip),send_event);
	}
	cJSON_Delete(send_event);
}

int create_one_cert(cJSON *item)
{
	char* ipString = GetEventItemString(item, "IP_ADDR");
	if(ipString)
	{
		Send_CertEvent(ipString, "CreateCertSelf");
	}
}
int cancel_one_cert(cJSON *item)
{
	char* ipString = GetEventItemString(item, "IP_ADDR");
	if(ipString)
	{
		Send_CertEvent(ipString, "DeleteCert");
	}
}
int create_all_cert(void)
{
	cJSON *cert_item;
	char* cert_State;
	cJSON_ArrayForEach(cert_item, certlist_all)
	{
		cert_State = GetEventItemString(cert_item, "CERT_State");
		if(!strcmp(cert_State, "Configurable"))
		{
			create_one_cert(cert_item);
		}
	}
}
int cancel_all_cert(void)
{
	cJSON *cert_item;
	char* cert_State;
	cJSON_ArrayForEach(cert_item, certlist_all)
	{
		cert_State = GetEventItemString(cert_item, "CERT_State");
		if(!strcmp(cert_State, "Delivered"))
		{
			cancel_one_cert(cert_item);
		}
	}
}
cJSON * get_one_cert(cJSON *item)
{
	cJSON *cert=NULL;
	cJSON *certPb;
	char* ipString = GetEventItemString(item, "IP_ADDR");
	MakeDir(TEMP_Folder);
	if(ipString)
	{
		if(!strcmp(ipString, GetSysVerInfo_IP()))
		{
			if(!strcmp(API_PublicInfo_Read_String(PB_CERT_STATE), "Delivered"))
			{
				cert = GetJsonFromFile(CERT_FILE);
			}
		}
		else
		{
			certPb = API_GetRemotePb(inet_addr(ipString), PB_CERT_STATE);
			if(certPb && !strcmp(certPb->valuestring, "Delivered"))
			{
				int ret = Api_TftpReadFileFromDevice(inet_addr(ipString), UserDataFolder, "cert.json", TEMP_Folder, "certTemp.json");
				cert = GetJsonFromFile(CERT_TEMP_FILE);
			}
			cJSON_Delete(certPb);
		}
	}
	return cert;
}

cJSON* GetCertLog(cJSON *filter)
{
	cJSON* view = cJSON_CreateArray();
	cJSON* where = cJSON_CreateObject();
	char* ipString = GetEventItemString(filter, "IP_ADDR");
	if(ipString)
	{
		if(!strcmp(ipString, GetSysVerInfo_IP()))
		{
			API_TB_SelectBySortByName(TB_NAME_CertLog, view, where, 0);
		}
		else
		{
			API_RemoteTableSelect(inet_addr(ipString), TB_NAME_CertLog, view, where, 0);
		}
	}
	cJSON_Delete(where);
	return view;
}

/*****************
 * NDM MANAGE
 * **************/
cJSON* GetNDMTb(cJSON *filter)
{
	cJSON* view = cJSON_CreateArray();
	cJSON* where = cJSON_CreateObject();
	cJSON* tpl=cJSON_CreateObject();

    cJSON_AddStringToObject(tpl,"IP_ADDR","");
    cJSON_AddStringToObject(tpl,"NDM_State","");
    cJSON_AddStringToObject(tpl,"IX_ADDR","");
    cJSON_AddStringToObject(tpl,"MFG_SN","");
	cJSON_AddItemToArray(view,tpl);

	cJSON *filter_element;
	cJSON_ArrayForEach(filter_element, filter)
	{
		if(cJSON_IsNumber(filter_element))
			cJSON_AddNumberToObject(where,filter_element->string,filter_element->valueint);
		else if(cJSON_IsString(filter_element))
			cJSON_AddStringToObject(where,filter_element->string,filter_element->valuestring);
	}

	API_TB_SelectBySortByName(TB_NAME_NDM_Register, view, where, 0);
	cJSON_DeleteItemFromArray(view,0);
	#if 0
	char *str=cJSON_Print(view);
	printf("GetNDMTb =%s\n",str);
	free(str);
	#endif
	//cJSON_Delete(tpl);
	cJSON_Delete(where);
	return view;
}

cJSON* GetNDMPlanning(cJSON *filter)
{
	cJSON* devTb = NULL;
	IXS_ProxyGetTb(&devTb, NULL, 1);
	cJSON* view = cJSON_CreateArray();
	cJSON* where = cJSON_CreateObject();
	cJSON* tpl=cJSON_CreateObject();

    cJSON_AddStringToObject(tpl,"IP_ADDR","");
    cJSON_AddStringToObject(tpl,"IX_ADDR","");
    cJSON_AddStringToObject(tpl,"IX_NAME","");
    cJSON_AddStringToObject(tpl,"MFG_SN","");
	cJSON_AddItemToArray(view,tpl);

	cJSON *filter_element;
	cJSON_ArrayForEach(filter_element, filter)
	{
		if(cJSON_IsNumber(filter_element))
			cJSON_AddNumberToObject(where,filter_element->string,filter_element->valueint);
		else if(cJSON_IsString(filter_element))
			cJSON_AddStringToObject(where,filter_element->string,filter_element->valuestring);
	}

	API_TB_SelectBySort(devTb, view, where, 0);
	cJSON_DeleteItemFromArray(view,0);
	cJSON* del = cJSON_CreateObject();
	cJSON_AddStringToObject(del, "IP_ADDR", GetSysVerInfo_IP());
	API_TB_Delete(view, del);//本机去掉
	cJSON_Delete(del);
	#if 0
	char *str=cJSON_Print(view);
	printf("GetNDMTb =%s\n",str);
	free(str);
	#endif
	cJSON_Delete(devTb);
	//cJSON_Delete(tpl);
	cJSON_Delete(where);
	return view;
}

cJSON* GetNDMLog(cJSON *filter)
{
	cJSON* view = cJSON_CreateArray();
	cJSON* where = cJSON_CreateObject();
	#if 0
	cJSON* tpl=cJSON_CreateObject();
    cJSON_AddStringToObject(tpl,"DATE","");
    cJSON_AddStringToObject(tpl,"IP_ADDR","");
    cJSON_AddStringToObject(tpl,"MFG_SN","");
    cJSON_AddStringToObject(tpl,"NDM_CONN","");
    cJSON_AddStringToObject(tpl,"NDM_ITEM","");
	cJSON_AddItemToArray(view,tpl);
	printf_json(filter,"FILTER");
	cJSON *filter_element;
	cJSON_ArrayForEach(filter_element, filter)
	{
		if(cJSON_IsNumber(filter_element))
			cJSON_AddNumberToObject(where,filter_element->string,filter_element->valueint);
		else if(cJSON_IsString(filter_element))
			cJSON_AddStringToObject(where,filter_element->string,filter_element->valuestring);
	}
	#endif
	cJSON_AddStringToObject(where,"IP_ADDR",cJSON_GetObjectItemCaseSensitive(filter,"IP_ADDR")->valuestring);
	API_TB_SelectBySortByName(TB_NAME_NDM_S_Log, view, where, 0);

	cJSON* element;
	cJSON_ArrayForEach(element, view)
	{
		cJSON* ndmItem = cJSON_GetObjectItemCaseSensitive(element, "NDM_ITEM");
		cJSON_AddItemToObject(element, "PING_CHECK", cJSON_Duplicate(cJSON_GetObjectItemCaseSensitive(ndmItem, "PING_CHECK"), 1));
		cJSON_AddItemToObject(element, "UDP_CHECK", cJSON_Duplicate(cJSON_GetObjectItemCaseSensitive(ndmItem, "UDP_CHECK"), 1));
		cJSON_AddItemToObject(element, "CONNECT", cJSON_Duplicate(cJSON_GetObjectItemCaseSensitive(element, "NDM_CONN"), 1));
		cJSON_DeleteItemFromObjectCaseSensitive(element, "NDM_CONN");
		cJSON_DeleteItemFromObjectCaseSensitive(element, "NDM_ITEM");
	}
	#if 0
	char *str=cJSON_Print(view);
	printf("GetNDMLog =%s\n",str);
	free(str);
	#endif
	//cJSON_Delete(tpl);
	cJSON_Delete(where);
	return view;
}

int add_one_to_ndm(cJSON *item)
{
	cJSON *key=cJSON_GetObjectItemCaseSensitive(item,"IP_ADDR");
	cJSON *where=cJSON_CreateObject();
	cJSON* View=cJSON_CreateArray();
	int ret=0;
	cJSON_AddStringToObject(where,"IP_ADDR",key->valuestring);
	if(API_TB_SelectBySortByName(TB_NAME_NDM_Register, View, where, 0))
	{

	}
	else
	{
		cJSON* ndmitem = cJSON_CreateObject();
		cJSON_AddStringToObject(ndmitem,"IP_ADDR",cJSON_GetObjectItemCaseSensitive(item,"IP_ADDR")->valuestring);
		cJSON_AddStringToObject(ndmitem,"MFG_SN",cJSON_GetObjectItemCaseSensitive(item,"MFG_SN")->valuestring);
		cJSON_AddStringToObject(ndmitem,"IX_ADDR",cJSON_GetObjectItemCaseSensitive(item,"IX_ADDR")->valuestring);
		cJSON_AddItemToObject(ndmitem, "ENABLE", cJSON_CreateTrue());
		cJSON_AddStringToObject(ndmitem,"NDM_State","");
		ret = API_TB_AddByName(TB_NAME_NDM_Register, ndmitem);
		cJSON_Delete(ndmitem);
	}
	cJSON_Delete(where);
	cJSON_Delete(View);
	return ret;
}
int del_one_to_ndm(cJSON *item)
{
	cJSON *ndm_item;
	if(cJSON_IsArray(item))
	{
		ndm_item = cJSON_GetArrayItem(item, 0);
	}
	char* ipString = cJSON_GetObjectItemCaseSensitive(ndm_item,"IP_ADDR")->valuestring;
	API_Event_NDM_Remote_C_Ctrl(inet_addr(ipString), "NDM_STOP");
	cJSON *where=cJSON_CreateObject();
	cJSON_AddStringToObject(where,"IP_ADDR",ipString);
	API_TB_DeleteByName(TB_NAME_NDM_Register, where);
	cJSON_Delete(where);
	return 0;
}
int add_all_to_ndm(cJSON *item)
{
	cJSON *ndm_item;
	cJSON_ArrayForEach(ndm_item, item)
	{
		add_one_to_ndm(ndm_item);
	}
}
int del_all_to_ndm(void)
{
	cJSON* View = cJSON_CreateArray();
	cJSON* Where = cJSON_CreateObject();
	cJSON* element;
	char* ipString;
	cJSON_AddStringToObject(Where, "NDM_State", "ON");
	if(API_TB_SelectBySortByName(TB_NAME_NDM_Register, View, Where, 0))
	{
		cJSON_ArrayForEach(element, View)
		{
			ipString = GetEventItemString(element, "IP_ADDR");
			API_Event_NDM_Remote_C_Ctrl(inet_addr(ipString), "NDM_STOP");
		}
		
	}
	API_TB_DeleteByName(TB_NAME_NDM_Register, NULL);
	cJSON_Delete(Where);
	cJSON_Delete(View);
}
int enable_ndm_all(int en)
{
	cJSON* View = cJSON_CreateArray();
	cJSON* Where = cJSON_CreateObject();
	cJSON* element;
	cJSON_AddItemToObject(Where, "ENABLE", en? cJSON_CreateFalse() : cJSON_CreateTrue());
	if(API_TB_SelectBySortByName(TB_NAME_NDM_Register, View, Where, 0))
	{
		
		cJSON_DeleteItemFromObjectCaseSensitive(Where, "ENABLE");
		cJSON_AddStringToObject(Where, "MFG_SN", "");
		cJSON_ArrayForEach(element, View)
		{
			cJSON_ReplaceItemInObjectCaseSensitive(Where, "MFG_SN",cJSON_CreateString(cJSON_GetObjectItemCaseSensitive(element,"MFG_SN")->valuestring));
			cJSON_ReplaceItemInObjectCaseSensitive(element, "ENABLE", en? cJSON_CreateTrue() : cJSON_CreateFalse());
			API_TB_UpdateByName(TB_NAME_NDM_Register, Where, element);
		}
	}
	cJSON_Delete(View);
	cJSON_Delete(Where);
	return 1;
}

int NDM_S_CheckEndCallback(cJSON *cmd)
{
	if(ndm_server_tips!=NULL)
	{
		API_TIPS_Close_Ext(ndm_server_tips);
		ndm_server_tips=NULL;
	}
	ndm_register_fresh();
}

/*****************
 * IXRLIST MANAGE
 * **************/
cJSON* GetIXRListSelectSource(char *name)
{
	cJSON* devTb = NULL;
	IXS_ProxyGetTb(&devTb, NULL, strcmp(name, "R8001")==0? 0:1);
	cJSON* view = cJSON_CreateArray();
	cJSON* where = cJSON_CreateObject();
	cJSON* tpl=cJSON_CreateObject();

    cJSON_AddStringToObject(tpl,"IX_ADDR","");
    cJSON_AddStringToObject(tpl,"IX_NAME","");
    cJSON_AddStringToObject(tpl,"G_NBR","");
    cJSON_AddStringToObject(tpl,"L_NBR","");
	cJSON_AddItemToArray(view,tpl);
	
	cJSON_AddStringToObject(where, "IX_TYPE", "IM");
	cJSON_AddStringToObject(where, "IX_ADDR", GetSysVerInfo_bd());

	API_TB_SelectBySort(devTb, view, where, 0);
	cJSON_DeleteItemFromArray(view,0);

	cJSON_Delete(devTb);
	cJSON_Delete(where);
	return view;
}
cJSON* GetIXRListTb(cJSON *filter)
{
	cJSON* view = cJSON_CreateArray();
	cJSON* where = cJSON_CreateObject();
	cJSON *filter_element;
	cJSON_ArrayForEach(filter_element, filter)
	{
		if(cJSON_IsNumber(filter_element))
			cJSON_AddNumberToObject(where,filter_element->string,filter_element->valueint);
		else if(cJSON_IsString(filter_element))
			cJSON_AddStringToObject(where,filter_element->string,filter_element->valuestring);
	}
	API_TB_SelectBySortByName(TB_NAME_IXRLIST, view, where, 0);

	cJSON_Delete(where);

	return view;
}
cJSON *CreateIXRList_Editable(int new)
{
	cJSON *edit_able=cJSON_CreateArray();
	if(new)
	{
		cJSON_AddItemToArray(edit_able,cJSON_CreateString("IX_ADDR"));
	}
	cJSON_AddItemToArray(edit_able,cJSON_CreateString("IX_NAME"));
	cJSON_AddItemToArray(edit_able,cJSON_CreateString("G_NBR"));
	cJSON_AddItemToArray(edit_able,cJSON_CreateString("L_NBR"));
	return edit_able;
}
cJSON *CreateNewIXRList_Item(void)
{
	cJSON* tpl=cJSON_CreateObject();
	cJSON_AddStringToObject(tpl,"IX_ADDR","");
	cJSON_AddStringToObject(tpl,"IX_NAME","-");
	cJSON_AddStringToObject(tpl,"G_NBR","-");
	cJSON_AddStringToObject(tpl,"L_NBR","-");
	return tpl;
}
int add_one_to_ixrlist(cJSON *item)
{
	cJSON *key=cJSON_GetObjectItemCaseSensitive(item,"IX_ADDR");
	if(key==NULL || !strcmp(key->valuestring, ""))
		return -1;
	cJSON *where=cJSON_CreateObject();
	cJSON* View=cJSON_CreateArray();
	int ret=0;
	cJSON_AddStringToObject(where,"IX_ADDR",key->valuestring);
	if(API_TB_SelectBySortByName(TB_NAME_IXRLIST, View, where, 0))
	{
		API_TB_UpdateByName(TB_NAME_IXRLIST, where, item);
	}
	else
	{
		API_TB_AddByName(TB_NAME_IXRLIST, item);
	}
	cJSON_Delete(where);
	cJSON_Delete(View);
	return ret;
}
int del_one_to_ixrlist(cJSON *item)
{
	cJSON *key=cJSON_GetObjectItemCaseSensitive(item,"IX_ADDR");
	if(key==NULL || !strcmp(key->valuestring, ""))
		return -1;
	cJSON *where=cJSON_CreateObject();
	cJSON_AddStringToObject(where,"IX_ADDR",key->valuestring);
	API_TB_DeleteByName(TB_NAME_IXRLIST, where);
	cJSON_Delete(where);
	return 0;
}
int add_all_to_ixrlist(cJSON *item)
{
	cJSON *list_item;
	cJSON_ArrayForEach(list_item, item)
	{
		add_one_to_ixrlist(list_item);
	}
}
int del_all_to_ixrlist(void)
{	
	API_TB_DeleteByName(TB_NAME_IXRLIST, NULL);
}

/*****************
 * TLOG CONFIG
 * **************/
cJSON* GetTlogEventList(void)
{
	cJSON *tlogEventList = GetJsonFromFile(TlogEventList_FILE_NAME);
	return tlogEventList;
}
cJSON* GetTlogEventCfg(void)
{
	cJSON* view = cJSON_CreateArray();
	cJSON* tpl;
	cJSON* typeElement;
	char* cfgEventType;
	char* cfgEventName;
	int typeEventSize;
	int typeEventCnt;
	cJSON* eventLogCfg = API_Para_Read_Public(TLogEventCfg);
	if(cJSON_IsArray(eventLogCfg))
	{
		cJSON_ArrayForEach(typeElement, eventLogCfg)
		{
			if(cJSON_IsArray(typeElement))
			{
				typeEventSize = cJSON_GetArraySize(typeElement);
				if(typeEventSize > 1)
				{
					cfgEventType = cJSON_GetStringValue(cJSON_GetArrayItem(typeElement, 0));
					if(cfgEventType)
					{
						for(typeEventCnt = 1; typeEventCnt < typeEventSize; typeEventCnt++)
						{
							cfgEventName = cJSON_GetStringValue(cJSON_GetArrayItem(typeElement, typeEventCnt));
							tpl=cJSON_CreateObject();
							cJSON_AddStringToObject(tpl,"EventType",cfgEventType);
    						cJSON_AddStringToObject(tpl,"EventName",cfgEventName);
							cJSON_AddItemToArray(view, tpl);
						}
					}
				}
			}				
		}
	}

	return view;
}

int SetTlogEvent(cJSON *item)
{
	int eventCnt;
	int typeCnt;
	int typeIndex;
	int eventIndex;
	int addFlag = 0;
	cJSON* typeElement;
	cJSON* eventElement;
	cJSON* typeJson;
	char* type = cJSON_GetObjectItemCaseSensitive(item,"EventType")->valuestring;
	char* name = cJSON_GetObjectItemCaseSensitive(item,"EventName")->valuestring;
	if(!type || !name)
	{
		return addFlag;
	}

	cJSON* eventConfig = cJSON_Duplicate(API_Para_Read_Public(TLogEventCfg), 1);
	typeCnt = cJSON_GetArraySize(eventConfig);
	for(typeIndex = typeCnt-1; typeIndex >= 0 && addFlag == 0; typeIndex--)
	{
		typeElement = cJSON_GetArrayItem(eventConfig, typeIndex);
		eventCnt = cJSON_GetArraySize(typeElement);
		if(eventCnt >= 2)
		{
			typeJson = cJSON_GetArrayItem(typeElement, 0);
			for(eventIndex = eventCnt-1; eventIndex >= 1; eventIndex--)
			{
				eventElement = cJSON_GetArrayItem(typeElement, eventIndex);
				if(eventElement && eventElement->valuestring && !strcmp(eventElement->valuestring, name))
				{
					if(typeJson && typeJson->valuestring && !strcmp(typeJson->valuestring, type))
					{
						//已经存在，并且类型相同
						addFlag = 2;
						break;
					}
					else
					{
						cJSON_DeleteItemFromArray(typeElement, eventIndex);
					}
				}
			}
		}
		else
		{
			cJSON_DeleteItemFromArray(eventConfig, typeIndex);
		}
	}

	if(!addFlag)
	{
		cJSON_ArrayForEach(typeElement, eventConfig)
		{
			eventElement = cJSON_GetArrayItem(typeElement, 0);
			if(eventElement && eventElement->valuestring && !strcmp(eventElement->valuestring, type))
			{
				cJSON_AddItemToArray(typeElement, cJSON_CreateString(name));
				//添加成功
				addFlag = 1;
				break;
			}
		}
	}

	if(!addFlag)
	{
		typeJson = cJSON_CreateArray();
		cJSON_AddItemToArray(typeJson, cJSON_CreateString(type));
		cJSON_AddItemToArray(typeJson, cJSON_CreateString(name));
		cJSON_AddItemToArray(eventConfig, typeJson);
		//添加成功
		addFlag = 1;
	}

	if(addFlag == 1)
	{
		API_Para_Write_PublicByName(TLogEventCfg,eventConfig);
	}

	return addFlag;
}
int InputTlogEvent(char *code)
{
	cJSON *tlogEventList = GetJsonFromFile(TlogEventList_FILE_NAME);
	cJSON* where = cJSON_CreateObject();
	cJSON* view = cJSON_CreateArray();
	cJSON_AddStringToObject(where, "EventCode", code);
	if(API_TB_SelectBySort(tlogEventList, view, where, 0)>0)
	{
		SetTlogEvent(cJSON_GetArrayItem(view, 0));
	}
	cJSON_Delete(where);
	cJSON_Delete(view);
	cJSON_Delete(tlogEventList);
	return 0;
}

void DelTlogEvent(void)
{
	cJSON *para_temp=cJSON_CreateArray();
	API_Para_Write_PublicByName(TLogEventCfg,para_temp);
	cJSON_Delete(para_temp);
}

void MenuNDMManage_Init(void)
{
	API_Event_By_Name(Event_IXS_ReqUpdateTb);
	ndm_register_show();
}
void MenuCertManage_Init(void)
{
	API_Event_By_Name(Event_IXS_ReqUpdateTb);
	cert_manage_show();
}
void MenuIXRListManage_Init(void)
{
	ixrlist_manage_show();
}


void MenuNDMManage_Process(int msg_type, void* arg)
{
	cJSON *edit_able;
	switch(msg_type)
	{
		case MSG_NDM_VIEW_LOG:
			ndm_log_show((cJSON*)arg);
		break;
		case MSG_NDM_PLANNING:
			ndm_planning_show();
		break;
		case MSG_NDM_SERVICE_START:
			ndm_server_tips = API_TIPS_Ext("NDM_START");
			enable_ndm_all(1);
			API_Event_NDM_Ctrl(1, "NDM_START");
			API_Para_Write_Int("NDM_S_BOOT", 1);
			//API_NDM_S_Ctrl("NDM_START", 0);
		break;
		case MSG_NDM_SERVICE_STOP:
			ndm_server_tips = API_TIPS_Ext("NDM_STOP");
			API_Para_Write_Int("NDM_S_BOOT", 0);
			API_NDM_S_Ctrl("NDM_STOP", 0);
			enable_ndm_all(0);
		break;
		case MSG_NDM_SELECT_ONE:
			add_one_to_ndm((cJSON*)arg);
			ndm_register_fresh();
			ndm_planning_close();
		break;
		case MSG_NDM_SELECT_ALL:
			add_all_to_ndm((cJSON*)arg);
			ndm_register_fresh();
			ndm_planning_close();
		break;
		case MSG_NDM_DELETE_ALL:
			del_all_to_ndm();
			ndm_register_fresh();
			ndm_planning_close();
		break;
		case MSG_NDM_DELETE_ONE:
			del_one_to_ndm((cJSON*)arg);
			ndm_register_fresh();
			ndm_log_close();
		break;
		case MSG_NDM_SELECT_EXIT:
			ndm_register_fresh();
			ndm_planning_close();
		break;
		case MSG_NDM_LOG_EXIT:
			ndm_register_fresh();
			ndm_log_close();
		break;
		case MSG_NDM_REGISTER_EXIT:
			ndm_register_close();
		break;
		case MSG_CERT_MANAGE_EXIT:
			MenuCertManage_Close();
		break;
		case MSG_CERT_MANAGE_UPDATE:
			cert_manage_tips = API_TIPS_Ext_Time("Cert Manage Update...", 10);
			API_Event_CertManage("update");
		break;	
		case MSG_CERT_CREATEALL:
			if(certlist_all)
			{
				cert_manage_tips = API_TIPS_Ext_Time("Cert Manage Create All...", 20);
				API_Event_CertManage("create");
			}
			else
			{
				API_TIPS_Ext("Please click update first!");
			}
		break;
		case MSG_CERT_CANCELALL:
			if(certlist_all)
			{
				cert_manage_tips = API_TIPS_Ext_Time("Cert Manage Cancel All...", 20);
				API_Event_CertManage("cancel");
			}
			else
			{
				API_TIPS_Ext("Please click update first!");
			}
		break;
		case MSG_CERT_LSIT_ALL:
			if(certlist_all)
			{
				cert_list_show(0);
			}
			else
			{
				API_TIPS_Ext("Please click update first!");
			}
		break;
		case MSG_CERT_LSIT_HAVE:
			if(certlist_all)
			{
				cert_list_show(1);
			}
			else
			{
				API_TIPS_Ext("Please click update first!");
			}
		break;
		case MSG_CERT_LSIT_NONE:
			if(certlist_all)
			{
				cert_list_show(2);
			}
			else
			{
				API_TIPS_Ext("Please click update first!");
			}
		break;
		case MSG_CERT_VIEW:
			cert_view_show((cJSON*)arg);
		break;
		case MSG_CERT_CREATEONE:
			create_one_cert((cJSON*)arg);
			usleep(500*1000);
			cert_view_fresh((cJSON*)arg);
		break;
		case MSG_CERT_CANCELONE:
			cancel_one_cert((cJSON*)arg);
			usleep(500*1000);
			cert_view_fresh((cJSON*)arg);
		break;
		case MSG_CERT_LOG:
			cert_log_show((cJSON*)arg);
		break;
		case MSG_CERT_VIEW_EXIT:
			cert_view_close();
		break;
		case MSG_IXRLIST_ADD_BY_ONLINE:
			list_select_show("Online");
		break;
		case MSG_IXRLIST_ADD_BY_R8001:
			list_select_show("R8001");
		break;
		case MSG_IXRLIST_ADD_BY_INPUT:
			if(edit_record!=NULL)
				cJSON_Delete(edit_record);
			edit_record=CreateNewIXRList_Item();
			edit_able=CreateIXRList_Editable(1);
			create_record_editor_menu(edit_record,edit_able,API_TB_GetRuleByName(TB_NAME_IXRLIST));
			cJSON_Delete(edit_able);
			add_new_item=1;
		break;
		case MSG_IXRLIST_EDIT:
			if(edit_record!=NULL)
				cJSON_Delete(edit_record);
			edit_record=cJSON_Duplicate((cJSON*)arg,1);
			edit_able=CreateIXRList_Editable(0);
			create_record_editor_menu(edit_record,edit_able,API_TB_GetRuleByName(TB_NAME_IXRLIST));
			cJSON_Delete(edit_able);
			add_new_item=0;
		break;
		case MSG_IXRLIST_EDIT_SAVE:
			add_one_to_ixrlist((cJSON*)arg);
			ixrlist_manage_fresh();
			record_editor_close();
		break;
		case MSG_IXRLIST_DELETE_ONE:
			if(add_new_item)
			{
				return;
			}
			del_one_to_ixrlist((cJSON*)arg);
			ixrlist_manage_fresh();
			record_editor_close();
		break;
		case MSG_IXRLIST_DELETE_ALL:
			del_all_to_ixrlist();
			ixrlist_manage_fresh();
		break;
		case MSG_IXRLIST_SELECT_ONE:
			add_one_to_ixrlist((cJSON*)arg);
			ixrlist_manage_fresh();
			list_select_close();
		break;
		case MSG_IXRLIST_SELECT_ALL:
			add_all_to_ixrlist((cJSON*)arg);
			ixrlist_manage_fresh();
			list_select_close();
		break;
		case MSG_IXRLIST_EXIT:
			MenuIXRListManage_Close();
		break;
		case MSG_NDM_TLOG_CFG:
			tlogEvent_manage_show();
		break;
		case MSG_TLOG_ADD_BY_SELECT:
			event_select_show();
		break;
		case MSG_TLOG_DELETE_ALL:
			DelTlogEvent();
			tlogEvent_manage_fresh();
		break;
		case MSG_TLOG_SELECT_ONE:
			SetTlogEvent((cJSON*)arg);
			tlogEvent_manage_fresh();
			event_select_close();
		break;
		case MSG_TLOG_INPUT_ONE:
			InputTlogEvent((char*)arg);
			tlogEvent_manage_fresh();
			event_select_close();
		break;
	}
}

void MenuNDMManage_Close(void)
{	
	ndm_register_close();		
}
void MenuCertManage_Close(void)
{	
	if(certlist_all)
	{
		cJSON_Delete(certlist_all);
		certlist_all=NULL;
	}
	cert_manage_close();
}
void MenuIXRListManage_Close(void)
{	
	if(edit_record!=NULL)
	{
		cJSON_Delete(edit_record);
		edit_record=NULL;	
	}
	ixrlist_manage_close();	
}
void API_MenuNDMManage_Process(void* arg)
{
	
}