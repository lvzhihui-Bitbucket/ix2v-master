#ifndef MENUNDMMANAGE_H_
#define MENUNDMMANAGE_H_

#include "lv_ix850.h"

#define MSG_NDM_SELECT_ONE          231103101
#define MSG_NDM_SELECT_ALL          231103102
#define MSG_NDM_DELETE_ALL          231103103
#define MSG_NDM_SELECT_EXIT         231103104
#define MSG_NDM_VIEW_LOG            231103105
#define MSG_NDM_SERVICE_START       231103106
#define MSG_NDM_SERVICE_STOP        231103107
#define MSG_NDM_LOG_EXIT            231103108
#define MSG_NDM_PLANNING            231103109
#define MSG_NDM_REGISTER_EXIT       231103110
#define MSG_CERT_CREATEALL          231103111
#define MSG_CERT_CANCELALL          231103112
#define MSG_CERT_VIEW               231103113
#define MSG_CERT_CREATEONE          231103114
#define MSG_CERT_CANCELONE          231103115
#define MSG_CERT_LOG                231103116
#define MSG_CERT_VIEW_EXIT          231103117
#define MSG_NDM_DELETE_ONE          231103118

#define MSG_IXRLIST_EDIT            231214101
#define MSG_IXRLIST_ADD_BY_ONLINE   231214102
#define MSG_IXRLIST_ADD_BY_R8001    231214103
#define MSG_IXRLIST_ADD_BY_INPUT    231214104
#define MSG_IXRLIST_SELECT_ONE      231214105
#define MSG_IXRLIST_SELECT_ALL      231214106
#define MSG_IXRLIST_DELETE_ONE      231214107
#define MSG_IXRLIST_DELETE_ALL      231214108
#define MSG_IXRLIST_EDIT_SAVE       231214109
#define MSG_IXRLIST_EXIT            231214110

#define MSG_NDM_TLOG_CFG            231220101
#define MSG_TLOG_ADD_BY_SELECT      231220102
#define MSG_TLOG_DELETE_ALL         231220103
#define MSG_TLOG_SELECT_ONE         231220104
#define MSG_TLOG_INPUT_ONE          231220105

#define MSG_CERT_MANAGE_UPDATE      240327101
#define MSG_CERT_MANAGE_EXIT        240327102
#define MSG_CERT_LSIT_ALL           240327103
#define MSG_CERT_LSIT_HAVE          240327104
#define MSG_CERT_LSIT_NONE          240327105

#endif 
