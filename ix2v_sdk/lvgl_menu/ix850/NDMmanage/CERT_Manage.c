#include "obj_lvgl_msg.h"
#include "lv_ix850.h"
#include "common_table_display.h"
#include "MenuNDMManage.h"

static lv_obj_t* ui_table_init(void);
static lv_obj_t* ui_cert = NULL;
static lv_obj_t* cert_back = NULL;
static lv_obj_t* ui_certview = NULL;
static lv_obj_t* certview_back = NULL;
static lv_obj_t* ui_certlog = NULL;
static lv_obj_t* certlog_back = NULL;
static lv_obj_t* ui_certmanage = NULL;
static lv_obj_t* certmanage_back = NULL;
static COM_TABLE_DISPLAY *cert_display=NULL;
static cJSON *view_cert_all=NULL;
static COM_TABLE_DISPLAY *certlog_display=NULL;
static cJSON *view_certlog=NULL;
static cJSON *view_cert_info=NULL;
static lv_obj_t * certInfolabel;
static void certList_exit_event(lv_event_t* e);

COM_TABLE_DISPLAY* cert_list_show(int mode)
{
	//printf("11111111111%s:%d\n",__func__,__LINE__);	
	view_cert_all = GetCertList(mode);

    cert_back = lv_scr_act();
    ui_cert = ui_table_init();
    lv_disp_load_scr(ui_cert);
	cert_display = common_table_display_menu(ui_cert, view_cert_all, "Cert List", MSG_CERT_VIEW);
	
    lv_obj_t* btnCont = lv_obj_create(ui_cert);
    lv_obj_set_size(btnCont, 480, 80);
    lv_obj_set_style_pad_all(btnCont, 0, 0);
    lv_obj_set_y(btnCont, 720);
    //返回
    lv_obj_t* backBtn = lv_btn_create(btnCont);
    lv_obj_align(backBtn, LV_ALIGN_LEFT_MID, 50, 0);
    //lv_obj_set_style_bg_color(backBtn, lv_color_make(230, 231, 232), 0);
    lv_obj_set_size(backBtn, 120, 65);
    lv_obj_set_style_text_font(backBtn, &lv_font_montserrat_32, 0);
    lv_obj_add_event_cb(backBtn, certList_exit_event, LV_EVENT_CLICKED, NULL);
    lv_obj_set_style_radius(backBtn, LV_RADIUS_CIRCLE, 0);
    lv_obj_t* backlabel = lv_label_create(backBtn);
    lv_label_set_text(backlabel, LV_SYMBOL_BACKSPACE);
    lv_obj_center(backlabel);

	return cert_display;
}


void cert_list_close(void)
{
	//printf("11111111111%s:%d\n",__func__,__LINE__);	
    lv_disp_load_scr(cert_back);
	if(cert_display!=NULL)
	{
		common_table_delete(cert_display);
        cert_display = NULL;
	}
	if(view_cert_all)
	{
		cJSON_Delete(view_cert_all);
		view_cert_all=NULL;
	}
	if(ui_cert)
	{
		lv_obj_del(ui_cert);
		ui_cert = NULL;
	}
	//printf("11111111111%s:%d\n",__func__,__LINE__);	
}
void cert_list_fresh(void)
{
    //printf("11111111111%s:%d\n",__func__,__LINE__);	
    if(cert_display==NULL)
    {
        return;
    }
	//view_cert_all = GetCertState(NULL);
    common_table_fresh(cert_display, view_cert_all, 1);
}
static void certList_exit_event(lv_event_t* e)
{
    cert_list_close();
}

static void log_exit_event(lv_event_t* e)
{
    cert_log_close();
    //MenuNDMManage_Process(MSG_NDM_LOG_EXIT,NULL);
}
static void add_log_control(lv_obj_t* parent)
{
    lv_obj_t* btnCont = lv_obj_create(parent);
    lv_obj_set_size(btnCont, 480, 80);
    lv_obj_set_style_pad_all(btnCont, 0, 0);
    lv_obj_set_y(btnCont, 720);
    //返回
    lv_obj_t* backBtn = lv_btn_create(btnCont);
    lv_obj_align(backBtn, LV_ALIGN_LEFT_MID, 50, 0);
    //lv_obj_set_style_bg_color(backBtn, lv_color_make(230, 231, 232), 0);
    lv_obj_set_size(backBtn, 120, 65);
    lv_obj_set_style_text_font(backBtn, &lv_font_montserrat_32, 0);
    lv_obj_add_event_cb(backBtn, log_exit_event, LV_EVENT_CLICKED, NULL);
    lv_obj_set_style_radius(backBtn, LV_RADIUS_CIRCLE, 0);
    lv_obj_t* backlabel = lv_label_create(backBtn);
    lv_label_set_text(backlabel, LV_SYMBOL_BACKSPACE);
    lv_obj_center(backlabel);
}

COM_TABLE_DISPLAY* cert_log_show(cJSON *item)
{
	//printf("11111111111%s:%d\n",__func__,__LINE__);	
	view_certlog = GetCertLog(item);

    certlog_back = lv_scr_act();
    ui_certlog = ui_table_init();
    lv_disp_load_scr(ui_certlog);
	certlog_display = common_table_display_menu(ui_certlog, view_certlog, "CERT_Log", 0);
	add_log_control(ui_certlog);
	return certlog_display;
}
void cert_log_close(void)
{
	//printf("11111111111%s:%d\n",__func__,__LINE__);	
    lv_disp_load_scr(certlog_back);
	if(certlog_display!=NULL)
	{
		common_table_delete(certlog_display);
        certlog_display = NULL;
	}
	if(view_certlog)
	{
		cJSON_Delete(view_certlog);
		view_certlog=NULL;
	}
	if(ui_certlog)
	{
		lv_obj_del(ui_certlog);
		ui_certlog = NULL;
	}
}


static lv_obj_t* ui_table_init(void)
{	
    lv_obj_t* ui_table = lv_obj_create(NULL);
    lv_obj_clear_flag(ui_table, LV_OBJ_FLAG_SCROLLABLE);
    lv_obj_remove_style_all(ui_table);
    lv_obj_set_size(ui_table, lv_disp_get_hor_res(NULL), lv_disp_get_ver_res(NULL));
    lv_obj_align(ui_table, LV_ALIGN_TOP_LEFT, 0, 0);
    lv_obj_set_style_bg_color(ui_table, lv_palette_main(LV_PALETTE_GREY), 0);
    lv_obj_set_style_bg_opa(ui_table, LV_OPA_100, 0);
	return ui_table;
}

static void add_title(lv_obj_t* parent, char* name)
{
    lv_obj_t* titleCont = lv_obj_create(parent);
    lv_obj_set_size(titleCont, 480, 80);
    lv_obj_set_style_pad_all(titleCont, 0, 0);
    lv_obj_t* titlelabel = lv_label_create(titleCont);
    lv_obj_set_style_text_font(titlelabel, &lv_font_montserrat_32, 0);
    lv_obj_center(titlelabel);
    lv_label_set_text(titlelabel, name);
}
void cert_view_close(void)
{
	//printf("11111111111%s:%d\n",__func__,__LINE__);	
    lv_disp_load_scr(certview_back);
	
	if(ui_certview)
	{
		lv_obj_del(ui_certview);
		ui_certview = NULL;
	}
}
static void one_cert_exit_event(lv_event_t* e)
{
    //cert_view_close();
    MenuNDMManage_Process(MSG_CERT_VIEW_EXIT, NULL);
}
static void one_cert_create_event(lv_event_t* e)
{
	MenuNDMManage_Process(MSG_CERT_CREATEONE, view_cert_info);
}
static void one_cert_cancel_event(lv_event_t* e)
{
    MenuNDMManage_Process(MSG_CERT_CANCELONE, view_cert_info);
}
static void one_cert_log_event(lv_event_t* e)
{
    MenuNDMManage_Process(MSG_CERT_LOG, view_cert_info);
}

static void add_one_cert_control(lv_obj_t* parent, int pbcert)
{
    lv_obj_t* btnCont = lv_obj_create(parent);
    lv_obj_set_size(btnCont, 480, 80);
    lv_obj_set_style_pad_all(btnCont, 0, 0);
    lv_obj_set_y(btnCont, 720);
    //返回
    lv_obj_t* backBtn = lv_btn_create(btnCont);
    lv_obj_align(backBtn, LV_ALIGN_LEFT_MID, 50, 0);
    //lv_obj_set_style_bg_color(backBtn, lv_color_make(230, 231, 232), 0);
    lv_obj_set_size(backBtn, 120, 65);
    lv_obj_set_style_text_font(backBtn, &lv_font_montserrat_32, 0);
    lv_obj_add_event_cb(backBtn, one_cert_exit_event, LV_EVENT_CLICKED, NULL);
    lv_obj_set_style_radius(backBtn, LV_RADIUS_CIRCLE, 0);
    lv_obj_t* backlabel = lv_label_create(backBtn);
    lv_label_set_text(backlabel, LV_SYMBOL_BACKSPACE);
    lv_obj_center(backlabel);
    //log
    lv_obj_t* addBtn = lv_btn_create(btnCont);
    lv_obj_set_size(addBtn, 120, 65);
    lv_obj_set_style_text_font(addBtn, &lv_font_montserrat_32, 0);
    lv_obj_add_event_cb(addBtn, one_cert_log_event, LV_EVENT_CLICKED, NULL);
    lv_obj_align(addBtn, LV_ALIGN_RIGHT_MID, -50, 0);
    lv_obj_set_style_radius(addBtn, LV_RADIUS_CIRCLE, 0);
    //lv_obj_set_style_bg_color(addBtn, lv_color_make(230, 231, 232), 0);
    lv_obj_t* addlabel = lv_label_create(addBtn);
    lv_label_set_text(addlabel, "Log");
    lv_obj_center(addlabel);
	#if 1
    //create
    lv_obj_t* checkBtn = lv_btn_create(btnCont);
    lv_obj_set_size(checkBtn, 120, 65);
    lv_obj_set_style_text_font(checkBtn, &lv_font_montserrat_32, 0);
    lv_obj_add_event_cb(checkBtn, pbcert? one_cert_cancel_event : one_cert_create_event, LV_EVENT_CLICKED, NULL);
    lv_obj_center(checkBtn);
    lv_obj_set_style_radius(checkBtn, LV_RADIUS_CIRCLE, 0);
    lv_obj_t* checklabel = lv_label_create(checkBtn);
    lv_label_set_text(checklabel, pbcert? "Cancel" : "Create");
    lv_obj_center(checklabel);
    #endif
}

lv_obj_t* cert_view_show(cJSON * view)
{
	//printf("11111111111%s:%d\n",__func__,__LINE__);	
    cJSON * cert;
    certview_back = lv_scr_act();
    ui_certview = ui_table_init();
    lv_disp_load_scr(ui_certview);
	
	view_cert_info = view;
    add_title(ui_certview, "Cert View");
    certInfolabel = lv_label_create(ui_certview);
    lv_label_set_long_mode(certInfolabel, LV_LABEL_LONG_WRAP);
	lv_obj_set_size(certInfolabel, 480, 640);
    lv_obj_set_style_pad_all(certInfolabel, 0, 0);
    lv_obj_set_y(certInfolabel, 80);
    lv_obj_set_style_text_font(certInfolabel, &lv_font_montserrat_24, 0);

    cert = get_one_cert(view);
    char* certInfo = cJSON_Print(cert);
    lv_label_set_text(certInfolabel, certInfo? certInfo : " ");
    if(certInfo)
    {
        free(certInfo);
    }
    if(cert)
    {
        cJSON_Delete(cert);
    }
    add_one_cert_control(ui_certview, cert==NULL? 0 : 1);
	return ui_certview;
}

void cert_view_fresh(cJSON * view)
{
    //printf("11111111111%s:%d\n",__func__,__LINE__);	
    if(ui_certview==NULL)
    {
        return;
    }
	
    cJSON * cert = get_one_cert(view);
    char* certInfo = cJSON_Print(cert);
    lv_label_set_text(certInfolabel, certInfo? certInfo : " ");
    if(certInfo)
    {
        free(certInfo);
    }
    if(cert)
    {
        cJSON_Delete(cert);
    }
}


static void certManage_exit_event(lv_event_t* e)
{
	MenuNDMManage_Process(MSG_CERT_MANAGE_EXIT, NULL);
}
static void certManage_update_event(lv_event_t* e)
{
	MenuNDMManage_Process(MSG_CERT_MANAGE_UPDATE, NULL);
}
static void certManage_create_event(lv_event_t* e)
{
	MenuNDMManage_Process(MSG_CERT_CREATEALL, NULL);
}
static void certManage_cancel_event(lv_event_t* e)
{
    MenuNDMManage_Process(MSG_CERT_CANCELALL, NULL);
}

static void add_cert_control(lv_obj_t* parent)
{
    lv_obj_t* btnCont = lv_obj_create(parent);
    lv_obj_set_size(btnCont, 480, 100);
    lv_obj_set_style_pad_all(btnCont, 0, 0);
    lv_obj_set_y(btnCont, 700);
    //返回
    lv_obj_t* backBtn = lv_btn_create(btnCont);
    lv_obj_align(backBtn, LV_ALIGN_LEFT_MID, 0, 0);
    //lv_obj_set_style_bg_color(backBtn, lv_color_make(230, 231, 232), 0);
    lv_obj_set_size(backBtn, 120, 80);
    lv_obj_set_style_text_font(backBtn, &lv_font_montserrat_32, 0);
    lv_obj_add_event_cb(backBtn, certManage_exit_event, LV_EVENT_CLICKED, NULL);
    lv_obj_set_style_radius(backBtn, LV_RADIUS_CIRCLE, 0);
    lv_obj_t* backlabel = lv_label_create(backBtn);
    lv_label_set_text(backlabel, LV_SYMBOL_BACKSPACE);
    lv_obj_center(backlabel);
    //update
    lv_obj_t* checkBtn = lv_btn_create(btnCont);
    lv_obj_set_size(checkBtn, 120, 80);
    lv_obj_set_style_text_font(checkBtn, &lv_font_montserrat_24, 0);
    lv_obj_add_event_cb(checkBtn, certManage_update_event, LV_EVENT_CLICKED, NULL);
    lv_obj_align(checkBtn, LV_ALIGN_CENTER, -60, 0);
    lv_obj_set_style_radius(checkBtn, LV_RADIUS_CIRCLE, 0);
    lv_obj_t* checklabel = lv_label_create(checkBtn);
    lv_label_set_text(checklabel, LV_SYMBOL_REFRESH);
    lv_obj_center(checklabel);
    //Create
    lv_obj_t* addBtn = lv_btn_create(btnCont);
    lv_obj_set_size(addBtn, 120, 80);
    lv_obj_set_style_text_font(addBtn, &lv_font_montserrat_24, 0);
    lv_obj_add_event_cb(addBtn, certManage_create_event, LV_EVENT_CLICKED, NULL);
    lv_obj_align(addBtn, LV_ALIGN_CENTER, 60, 0);
    lv_obj_set_style_radius(addBtn, LV_RADIUS_CIRCLE, 0);
    //lv_obj_set_style_bg_color(addBtn, lv_color_make(230, 231, 232), 0);
    lv_obj_t* addlabel = lv_label_create(addBtn);
    lv_label_set_text(addlabel, "Create All");
    lv_obj_center(addlabel);
	//cancel    
    lv_obj_t* delBtn = lv_btn_create(btnCont);
    lv_obj_set_size(delBtn,120, 80);
    lv_obj_set_style_text_font(delBtn, &lv_font_montserrat_24, 0);
    lv_obj_add_event_cb(delBtn, certManage_cancel_event, LV_EVENT_CLICKED, NULL);
    lv_obj_align(delBtn, LV_ALIGN_RIGHT_MID, 0, 0);
    lv_obj_set_style_radius(delBtn, LV_RADIUS_CIRCLE, 0);
    lv_obj_t* dellabel = lv_label_create(delBtn);
    lv_label_set_text(dellabel, "Cancel All");
    lv_obj_center(dellabel);
}

static void online_click_event(lv_event_t* e)
{
    MenuNDMManage_Process(MSG_CERT_LSIT_ALL, NULL);
}
static void certHave_click_event(lv_event_t* e)
{
    MenuNDMManage_Process(MSG_CERT_LSIT_HAVE, NULL);
}
static void certWithout_click_event(lv_event_t* e)
{
    MenuNDMManage_Process(MSG_CERT_LSIT_NONE, NULL);
}
lv_obj_t* create_oneitem(lv_obj_t* parent, char* name, int val, lv_event_cb_t cb)
{
    lv_obj_t* obj = lv_obj_create(parent);
    lv_obj_set_style_pad_row(obj, 0, 0);
    lv_obj_set_size(obj, LV_PCT(100), LV_SIZE_CONTENT);
    lv_obj_set_flex_flow(obj, LV_FLEX_FLOW_ROW);
    lv_obj_set_style_text_font(obj, &lv_font_montserrat_24, 0);
    lv_obj_clear_flag(obj, LV_OBJ_FLAG_SCROLLABLE);

    lv_obj_t* text_label = lv_label_create(obj);
    lv_label_set_text(text_label, name);
    lv_obj_set_size(text_label, 360, LV_SIZE_CONTENT);

    lv_obj_t* value_label = lv_label_create(obj);
    lv_label_set_text_fmt(value_label, "%d", val); 
    lv_obj_set_size(value_label, 120, LV_SIZE_CONTENT);
    lv_obj_add_flag(obj, LV_OBJ_FLAG_CLICKABLE);
    lv_obj_add_event_cb(obj, cb, LV_EVENT_CLICKED, NULL);
	
    return value_label;
}

static lv_obj_t* certHaveLabel;
static lv_obj_t* certWithoutLabel;
static void certHave_refresh_event(lv_event_t* e)
{
    lv_event_code_t code = lv_event_get_code(e);
    lv_obj_t* obj = lv_event_get_target(e);
    if (code == LV_EVENT_REFRESH)
    {
        lv_label_set_text_fmt(obj, "%d", GetCertCnt()); 
    }
}
static void certWithout_refresh_event(lv_event_t* e)
{
    lv_event_code_t code = lv_event_get_code(e);
    lv_obj_t* obj = lv_event_get_target(e);
    if (code == LV_EVENT_REFRESH)
    {
        lv_label_set_text_fmt(obj, "%d", GetNocertCnt()); 
    }
}
lv_obj_t* cert_manage_show(void)
{
    certmanage_back = lv_scr_act();
    ui_certmanage = ui_table_init();
    lv_disp_load_scr(ui_certmanage);

    add_title(ui_certmanage, "Cert Manage");
    lv_obj_t* cont = lv_obj_create(ui_certmanage);
    lv_obj_set_size(cont, 480, 620);
    lv_obj_set_y(cont, 80);
    lv_obj_set_style_pad_row(cont, 0, 0);
    lv_obj_set_style_pad_all(cont, 0, 0);
    lv_obj_set_flex_flow(cont, LV_FLEX_FLOW_COLUMN);

    create_oneitem(cont, "Online", GetOnlineCnt(), online_click_event);
    certHaveLabel = create_oneitem(cont, "Have Cert", GetCertCnt(), certHave_click_event);
    certWithoutLabel = create_oneitem(cont, "Without Cert", GetNocertCnt(), certWithout_click_event);
    lv_obj_add_event_cb(certHaveLabel, certHave_refresh_event, LV_EVENT_REFRESH, NULL);
    lv_obj_add_event_cb(certWithoutLabel, certWithout_refresh_event, LV_EVENT_REFRESH, NULL);

    add_cert_control(ui_certmanage);
    return ui_certmanage;
}

void cert_manage_close(void)
{
	printf("11111111111%s:%d\n",__func__,__LINE__);	
    lv_disp_load_scr(certmanage_back);
	
	if(ui_certmanage)
	{
		lv_obj_del(ui_certmanage);
		ui_certmanage = NULL;
	}
}

void cert_update_cnt(void)
{
    if(ui_certmanage)
    {
        lv_event_send(certHaveLabel, LV_EVENT_REFRESH, NULL);
        lv_event_send(certWithoutLabel, LV_EVENT_REFRESH, NULL);
    }
}