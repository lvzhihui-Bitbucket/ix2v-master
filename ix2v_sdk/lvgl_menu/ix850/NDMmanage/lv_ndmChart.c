﻿#include "lv_ix850.h"
#include "menu_common.h"

static lv_obj_t * chart;
static lv_chart_series_t * pingTime;
static lv_chart_cursor_t * cursor;

static void event_cb(lv_event_t * e)
{
    static int32_t last_id = -1;
    lv_event_code_t code = lv_event_get_code(e);
    lv_obj_t * obj = lv_event_get_target(e);

    if(code == LV_EVENT_VALUE_CHANGED) {
        last_id = lv_chart_get_pressed_point(obj);
        if(last_id != LV_CHART_POINT_NONE) {
            lv_chart_set_cursor_point(obj, cursor, NULL, last_id);
        }
    }
    else if(code == LV_EVENT_DRAW_PART_END) {
        lv_obj_draw_part_dsc_t * dsc = lv_event_get_draw_part_dsc(e);
        if(!lv_obj_draw_part_check_type(dsc, &lv_chart_class, LV_CHART_DRAW_PART_CURSOR)) return;
        if(dsc->p1 == NULL || dsc->p2 == NULL || dsc->p1->y != dsc->p2->y || last_id < 0) return;

        lv_coord_t * data_array = lv_chart_get_y_array(chart, pingTime);
        lv_coord_t v = data_array[last_id];
        char buf[16];
        lv_snprintf(buf, sizeof(buf), "%d", v);

        lv_point_t size;
        lv_txt_get_size(&size, buf, LV_FONT_DEFAULT, 0, 0, LV_COORD_MAX, LV_TEXT_FLAG_NONE);

        lv_area_t a;
        a.y2 = dsc->p1->y - 5;
        a.y1 = a.y2 - size.y - 10;
        a.x1 = dsc->p1->x + 10;
        a.x2 = a.x1 + size.x + 10;

        lv_draw_rect_dsc_t draw_rect_dsc;
        lv_draw_rect_dsc_init(&draw_rect_dsc);
        draw_rect_dsc.bg_color = lv_palette_main(LV_PALETTE_BLUE);
        draw_rect_dsc.radius = 3;

        lv_draw_rect(dsc->draw_ctx, &draw_rect_dsc, &a);

        lv_draw_label_dsc_t draw_label_dsc;
        lv_draw_label_dsc_init(&draw_label_dsc);
        draw_label_dsc.color = lv_color_white();
        a.x1 += 5;
        a.x2 -= 5;
        a.y1 += 5;
        a.y2 -= 5;
        lv_draw_label(dsc->draw_ctx, &draw_label_dsc, &a, buf, NULL);
    }
}

void chart_display(lv_obj_t* parent, cJSON * data)
{
    //printf("11111111111%s:%d\n",__func__,__LINE__);	
    /*Create a chart*/
    chart = lv_chart_create(parent);
    lv_obj_set_size(chart, 400, 560);
    lv_obj_center(chart);

    lv_chart_set_range(chart, LV_CHART_AXIS_PRIMARY_Y, 0, 25);
    lv_chart_set_range(chart, LV_CHART_AXIS_SECONDARY_Y, 0, 2);
    lv_chart_set_axis_tick(chart, LV_CHART_AXIS_PRIMARY_Y, 10, 5, 6, 5, true, 40);
    lv_chart_set_axis_tick(chart, LV_CHART_AXIS_SECONDARY_Y, 10, 5, 3, 1, true, 40);

    lv_obj_add_event_cb(chart, event_cb, LV_EVENT_ALL, NULL);
    lv_obj_refresh_ext_draw_size(chart);

    lv_chart_series_t * online = lv_chart_add_series(chart, lv_palette_main(LV_PALETTE_RED), LV_CHART_AXIS_SECONDARY_Y);
    cursor = lv_chart_add_cursor(chart, lv_palette_main(LV_PALETTE_BLUE), LV_DIR_LEFT | LV_DIR_BOTTOM);
    pingTime = lv_chart_add_series(chart, lv_palette_main(LV_PALETTE_BLUE), LV_CHART_AXIS_PRIMARY_Y);

    int size = cJSON_GetArraySize(data);
    lv_chart_set_point_count(chart, size);
    lv_obj_t * label = lv_label_create(parent);
    int i, time;
    cJSON *item = NULL;
    cJSON * pingT;
    char* connect;
    for(i = 0; i < size; i++) 
    {
        item = cJSON_GetArrayItem(data, i);
        if(i == 0)
        {
            lv_label_set_text(label, cJSON_GetStringValue(cJSON_GetObjectItemCaseSensitive(item, "DATE")));
            lv_obj_align_to(label, chart, LV_ALIGN_OUT_BOTTOM_LEFT, 0, 0);
        }
        
        connect = cJSON_GetStringValue(cJSON_GetObjectItemCaseSensitive(item, "CONNECT"));
        if(connect && !strcmp(connect, "ON"))
        {
            lv_chart_set_next_value(chart, online, 1);
        }
        else
        {
            lv_chart_set_next_value(chart, online, 0);
        }
        
        pingT = cJSON_GetObjectItemCaseSensitive(item, "PING_CHECK");
        if(pingT)
        {
            if(pingT->valuedouble == -1)
            {
                time = 0;
            }
            else
            {
                time = (int)(pingT->valuedouble + 0.5);
                time = time > 25? 25 : time;
            }
            
            lv_chart_set_next_value(chart, pingTime, time);
        }
        else
        {
            lv_chart_set_next_value(chart, pingTime, 0);
        }
    }

    //lv_chart_set_zoom_x(chart, 500);

}

