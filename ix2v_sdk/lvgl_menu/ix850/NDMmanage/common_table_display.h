#ifndef COMMON_TABLE_DISPLAY_H_
#define COMMON_TABLE_DISPLAY_H_

#include "lv_ix850.h"

typedef struct 
{   
    lv_obj_t* table_parent;        
    lv_obj_t* table;          
    cJSON* view_data;
    int cnt_page;
    int max_page;
    int msg_type;    
}COM_TABLE_DISPLAY;

bool common_table_delete(COM_TABLE_DISPLAY* td);
COM_TABLE_DISPLAY* common_table_display_menu(lv_obj_t* parent, cJSON* view, char* name, int msg);
void common_table_fresh(COM_TABLE_DISPLAY* tb_disp,cJSON* view,int page);


#endif 
