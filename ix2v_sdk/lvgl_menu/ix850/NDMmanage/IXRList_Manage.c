#include "obj_lvgl_msg.h"
#include "lv_ix850.h"
#include "common_table_display.h"
#include "MenuNDMManage.h"

static lv_obj_t* ui_table_init(void);
static lv_obj_t* ui_ixrlist = NULL;
static lv_obj_t* ixrlist_back = NULL;
static lv_obj_t* ui_list_select = NULL;
static lv_obj_t* list_select_back = NULL;
static COM_TABLE_DISPLAY *ixrlist_display=NULL;
static cJSON *view_ixrlist_all=NULL;
static COM_TABLE_DISPLAY *list_select_display=NULL;
static cJSON *list_select_view=NULL;
static void add_ixrlist_control(lv_obj_t* parent);

COM_TABLE_DISPLAY* ixrlist_manage_show(void)
{
	//printf("11111111111%s:%d\n",__func__,__LINE__);	
	view_ixrlist_all = GetIXRListTb(NULL);

    ixrlist_back = lv_scr_act();
    ui_ixrlist = ui_table_init();
    lv_disp_load_scr(ui_ixrlist);
	ixrlist_display = common_table_display_menu(ui_ixrlist, view_ixrlist_all, "IXRList Manage", MSG_IXRLIST_EDIT);
	add_ixrlist_control(ui_ixrlist);
	return ixrlist_display;
}


void ixrlist_manage_close(void)
{
	//printf("11111111111%s:%d\n",__func__,__LINE__);	
    lv_disp_load_scr(ixrlist_back);
	if(ixrlist_display!=NULL)
	{
		common_table_delete(ixrlist_display);
        ixrlist_display = NULL;
	}
	if(view_ixrlist_all)
	{
		cJSON_Delete(view_ixrlist_all);
		view_ixrlist_all=NULL;
	}
	if(ui_ixrlist)
	{
		lv_obj_del(ui_ixrlist);
		ui_ixrlist = NULL;
	}
}
void ixrlist_manage_fresh(void)
{
    //printf("11111111111%s:%d\n",__func__,__LINE__);	
    if(ixrlist_display==NULL)
    {
        return;
    }
	view_ixrlist_all = GetIXRListTb(NULL);
    common_table_fresh(ixrlist_display, view_ixrlist_all, 1);
}

/*****************
 * 创建底部表控制栏
 * **************/
static void ixrlist_exit_event(lv_event_t* e)
{
    //ixrlist_manage_close();
    MenuNDMManage_Process(MSG_IXRLIST_EXIT, NULL);
}
static void ixrlist_add_by_input(lv_event_t* e)
{
	MenuNDMManage_Process(MSG_IXRLIST_ADD_BY_INPUT, NULL);
}
static void ixrlist_add_by_online(lv_event_t* e)
{
	MenuNDMManage_Process(MSG_IXRLIST_ADD_BY_ONLINE, NULL);
}
static void ixrlist_add_by_r8001(lv_event_t* e)
{
	MenuNDMManage_Process(MSG_IXRLIST_ADD_BY_R8001, NULL);
}
static void ixrlist_del_event(lv_event_t* e)
{
    MenuNDMManage_Process(MSG_IXRLIST_DELETE_ALL, NULL);
}
static void add_ixrlist_control(lv_obj_t* parent)
{
    lv_obj_t* btnCont = lv_obj_create(parent);
    lv_obj_set_size(btnCont, 480, 80);
    lv_obj_set_style_pad_all(btnCont, 0, 0);
    lv_obj_set_y(btnCont, 720);
    //返回
    lv_obj_t* backBtn = lv_btn_create(btnCont);
    lv_obj_align(backBtn, LV_ALIGN_LEFT_MID, 0, 0);
    //lv_obj_set_style_bg_color(backBtn, lv_color_make(230, 231, 232), 0);
    lv_obj_set_size(backBtn, 96, 65);
    lv_obj_set_style_text_font(backBtn, &lv_font_montserrat_24, 0);
    lv_obj_add_event_cb(backBtn, ixrlist_exit_event, LV_EVENT_CLICKED, NULL);
    lv_obj_set_style_radius(backBtn, LV_RADIUS_CIRCLE, 0);
    lv_obj_t* backlabel = lv_label_create(backBtn);
    lv_label_set_text(backlabel, LV_SYMBOL_BACKSPACE);
    lv_obj_center(backlabel);
    //input
    lv_obj_t* addBtn = lv_btn_create(btnCont);
    lv_obj_set_size(addBtn, 96, 65);
    lv_obj_set_style_text_font(addBtn, &lv_font_montserrat_24, 0);
    lv_obj_add_event_cb(addBtn, ixrlist_add_by_input, LV_EVENT_CLICKED, NULL);
    lv_obj_align(addBtn, LV_ALIGN_RIGHT_MID, 0, 0);
    lv_obj_set_style_radius(addBtn, LV_RADIUS_CIRCLE, 0);
    //lv_obj_set_style_bg_color(addBtn, lv_color_make(230, 231, 232), 0);
    lv_obj_t* addlabel = lv_label_create(addBtn);
    lv_label_set_text(addlabel, "input");
    lv_obj_center(addlabel);
	//online
    lv_obj_t* checkBtn = lv_btn_create(btnCont);
    lv_obj_set_size(checkBtn, 96, 65);
    lv_obj_set_style_text_font(checkBtn, &lv_font_montserrat_24, 0);
    lv_obj_add_event_cb(checkBtn, ixrlist_add_by_online, LV_EVENT_CLICKED, NULL);
    lv_obj_align(checkBtn, LV_ALIGN_CENTER, -96, 0);
    lv_obj_set_style_radius(checkBtn, LV_RADIUS_CIRCLE, 0);
    lv_obj_t* checklabel = lv_label_create(checkBtn);
    lv_label_set_text(checklabel, "online");
    lv_obj_center(checklabel);
    //r8001
    lv_obj_t* stopBtn = lv_btn_create(btnCont);
    lv_obj_set_size(stopBtn, 96, 65);
    lv_obj_set_style_text_font(stopBtn, &lv_font_montserrat_24, 0);
    lv_obj_add_event_cb(stopBtn, ixrlist_add_by_r8001, LV_EVENT_CLICKED, NULL);
    lv_obj_align(stopBtn, LV_ALIGN_CENTER, 96, 0);
    lv_obj_set_style_radius(stopBtn, LV_RADIUS_CIRCLE, 0);
    lv_obj_t* stoplabel = lv_label_create(stopBtn);
    lv_label_set_text(stoplabel, "r8001");
    lv_obj_center(stoplabel);
    //del
    lv_obj_t* delBtn = lv_btn_create(btnCont);
    lv_obj_set_size(delBtn, 96, 65);
    lv_obj_set_style_text_font(delBtn, &lv_font_montserrat_24, 0);
    lv_obj_add_event_cb(delBtn, ixrlist_del_event, LV_EVENT_CLICKED, NULL);
    lv_obj_align(delBtn, LV_ALIGN_CENTER, 0, 0);
    lv_obj_set_style_radius(delBtn, LV_RADIUS_CIRCLE, 0);
    lv_obj_t* dellabel = lv_label_create(delBtn);
    lv_label_set_text(dellabel, "DEL");
    lv_obj_center(dellabel);
}

static void list_select_exit_event(lv_event_t* e)
{
    list_select_close();
}
static void list_select_addall_event(lv_event_t* e)
{    
    MenuNDMManage_Process(MSG_IXRLIST_SELECT_ALL,list_select_view);
}
static void add_list_select_control(lv_obj_t* parent)
{
    lv_obj_t* btnCont = lv_obj_create(parent);
    lv_obj_set_size(btnCont, 480, 80);
    lv_obj_set_style_pad_all(btnCont, 0, 0);
    lv_obj_set_y(btnCont, 720);
    //返回
    lv_obj_t* backBtn = lv_btn_create(btnCont);
    lv_obj_align(backBtn, LV_ALIGN_LEFT_MID, 50, 0);
    //lv_obj_set_style_bg_color(backBtn, lv_color_make(230, 231, 232), 0);
    lv_obj_set_size(backBtn, 120, 65);
    lv_obj_set_style_text_font(backBtn, &lv_font_montserrat_32, 0);
    lv_obj_add_event_cb(backBtn, list_select_exit_event, LV_EVENT_CLICKED, NULL);
    lv_obj_set_style_radius(backBtn, LV_RADIUS_CIRCLE, 0);
    lv_obj_t* backlabel = lv_label_create(backBtn);
    lv_label_set_text(backlabel, LV_SYMBOL_BACKSPACE);
    lv_obj_center(backlabel);
    //add all
    lv_obj_t* addBtn = lv_btn_create(btnCont);
    lv_obj_set_size(addBtn, 120, 65);
    lv_obj_set_style_text_font(addBtn, &lv_font_montserrat_32, 0);
    lv_obj_add_event_cb(addBtn, list_select_addall_event, LV_EVENT_CLICKED, NULL);
    lv_obj_align(addBtn, LV_ALIGN_RIGHT_MID, -50, 0);
    lv_obj_set_style_radius(addBtn, LV_RADIUS_CIRCLE, 0);
    //lv_obj_set_style_bg_color(addBtn, lv_color_make(230, 231, 232), 0);
    lv_obj_t* addlabel = lv_label_create(addBtn);
    lv_label_set_text(addlabel, LV_SYMBOL_PLUS);
    lv_obj_center(addlabel);
}

COM_TABLE_DISPLAY* list_select_show(char* name)
{
	//printf("11111111111%s:%d\n",__func__,__LINE__);	
	list_select_view = GetIXRListSelectSource(name);

    list_select_back = lv_scr_act();
    ui_list_select = ui_table_init();
    lv_disp_load_scr(ui_list_select);
	list_select_display = common_table_display_menu(ui_list_select, list_select_view, name, MSG_IXRLIST_SELECT_ONE);
	add_list_select_control(ui_list_select);
	return list_select_display;
}
void list_select_close(void)
{
	//printf("11111111111%s:%d\n",__func__,__LINE__);	
    lv_disp_load_scr(list_select_back);
	if(list_select_display!=NULL)
	{
		common_table_delete(list_select_display);
        list_select_display = NULL;
	}
	if(list_select_view)
	{
		cJSON_Delete(list_select_view);
		list_select_view=NULL;
	}
	if(ui_list_select)
	{
		lv_obj_del(ui_list_select);
		ui_list_select = NULL;
	}
}


static lv_obj_t* ui_table_init(void)
{	
    lv_obj_t* ui_table = lv_obj_create(NULL);
    lv_obj_clear_flag(ui_table, LV_OBJ_FLAG_SCROLLABLE);
    lv_obj_remove_style_all(ui_table);
    lv_obj_set_size(ui_table, lv_disp_get_hor_res(NULL), lv_disp_get_ver_res(NULL));
    lv_obj_align(ui_table, LV_ALIGN_TOP_LEFT, 0, 0);
    lv_obj_set_style_bg_color(ui_table, lv_palette_main(LV_PALETTE_GREY), 0);
    lv_obj_set_style_bg_opa(ui_table, LV_OPA_100, 0);
	return ui_table;
}

