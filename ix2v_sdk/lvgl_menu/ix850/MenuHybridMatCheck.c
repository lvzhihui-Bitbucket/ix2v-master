#include "menu_utility.h"
#include "MenuHybridMatCreateNew.h"
#include "cJSON.h"
#include "obj_TableSurver.h"
#include "define_string.h"
#include "obj_PublicInformation.h"
#include "utility.h"
#include "task_Event.h"
#include "Common_TableView.h"
#include "obj_CommonSelectMenu.h"

#define RES_CHECK_PARA		"{\"HeaderEnable\":true,\"ColumnWidth\":[160, 70, 70, 180, 120, 120]}"
#define RES_CHECK_VIEW_TEMPALE "{\"STATE\":\"\",\"GW_ID\":1,\"IM_ID\":1,\"IX_NAME\":\"\",\"L_NBR\":\"\",\"G_NBR\":\"\"}"

static TB_VIWE* resCheck = NULL;
static void ResCheckFilterProcess(const char* chose, void* userdata);
static cJSON* ResCheckCreatePara(const char* filterValue);
static void ResCheckFilterCallback(const cJSON* filter);

static void ResCheckReturnCallback(void)
{
	Menu_CheckDtImResult_Close();
}

static cJSON* ResCheckCreatePara(const char* filterValue)
{   
    cJSON* fliter = cJSON_CreateObject();
    cJSON_AddStringToObject(fliter, "Name", "IM state");
    cJSON_AddStringToObject(fliter, "Value", filterValue);
    cJSON_AddNumberToObject(fliter, "Callback", (int)ResCheckFilterCallback);
	cJSON* para = cJSON_Parse(RES_CHECK_PARA);
    cJSON_AddItemToObject(para,"Filter", fliter);
	cJSON_AddNumberToObject(para, "ReturnCallback", (int)ResCheckReturnCallback);

	return para;
}

static void ResCheckFilterProcess(const char* chose, void* userdata)
{   
    if(!chose)
    {
        return;
    }

    if(!strcmp("ALL",chose) || !strcmp("ONLINE",chose) || !strcmp("OFFLINE",chose) || !strcmp("ADDED",chose))
	{
        cJSON* para = ResCheckCreatePara(chose);
		cJSON* where = cJSON_CreateObject();
		cJSON* view = cJSON_CreateArray();
		cJSON_AddItemToArray(view, cJSON_Parse(RES_CHECK_VIEW_TEMPALE));
		cJSON_AddStringToObject(where, "STATE", chose);
		API_TB_SelectBySortByName(TB_NAME_DT_IM_CHECK, view, (!strcmp("ALL",chose)) ? NULL : where, 0);
		cJSON_DeleteItemFromArray(view, 0);
		TB_MenuDisplay(&resCheck, view, "Check IM result", para);
		cJSON_Delete(where);
		cJSON_Delete(view);
		cJSON_Delete(para);
    }
}

static void ResCheckFilterCallback(const cJSON* filter)
{   
    cJSON* value = cJSON_GetObjectItem(filter,"Value");
    static CHOSE_MENU_T callback = {.callback = 0, .cbData = NULL};
	callback.callback = ResCheckFilterProcess;
	callback.cbData = NULL;
    cJSON* choseList = cJSON_CreateArray();
    cJSON_AddItemToArray(choseList, cJSON_CreateString("ALL"));
    cJSON_AddItemToArray(choseList, cJSON_CreateString("ONLINE"));
    cJSON_AddItemToArray(choseList, cJSON_CreateString("OFFLINE"));
    cJSON_AddItemToArray(choseList, cJSON_CreateString("ADDED"));
    char* choseString = cJSON_IsString(value) ? value->valuestring : NULL;
    CreateChooseMenu("IM state", choseList, choseString, &callback);
    cJSON_Delete(choseList);
}


void Menu_CheckDtImResult_Init(void)
{
	ResCheckFilterProcess("ALL", NULL);
}

void Menu_CheckDtImResult_Close(void)
{
	TB_MenuDelete(&resCheck);
	API_Event_NameAndMsg(EventMenuSettingProcess, "SettingRefresh", NULL);
}



static lv_obj_t* resCheckTip = NULL;
static int dxgReport[100];
static int dxgWait[100];
static int resCheckTime = 0;

static void CreateCheckDtImOnlineResult(void);
static int ResCheckTipDisplay(const char* string, int time);

static int ResCheckTipCloseTimerCb(int time)
{
	if(resCheckTip)
	{
		if(time >= resCheckTime)
		{
			API_Event_NameAndMsg(EventMenuResCheckOnline, "Checking timeout", NULL);
			return 2;
		}
		else
		{
			char tempString[50];
			snprintf(tempString, 50, "%ds", resCheckTime - time);
			API_TIPS_Mode_Title(resCheckTip, tempString);
			MainMenu_Reset_Time();
			return 0;
		}
	}

	return 2;
}

static int ResCheckTipDisplay(const char* string, int time)
{
	int ret = 0;
	if(time)
	{
		resCheckTime = time;
		API_Add_TimingCheck(ResCheckTipCloseTimerCb, 1);
	}

	if(resCheckTip)
	{
		API_TIPS_Mode_Fresh(resCheckTip, string);
	}
	else
	{
		resCheckTip = API_TIPS_Mode(string);
		ret = 1;
	}

	return ret;
} 

static int ResCheckTipClose(void)
{
	int ret = 0;
	if(resCheckTip)
	{
		API_TIPS_Mode_Close(resCheckTip);
		API_Del_TimingCheck(ResCheckTipCloseTimerCb);
		resCheckTip = NULL;
		ret = 1;
	}

	return ret;
} 

static void IfResCheckCompleted(void)
{
	int i, j;

	for (i = 0; dxgWait[i]; i++)
	{
		for (j = 0; dxgReport[j]; j++)
		{
			if(dxgReport[j] == dxgWait[i])
			{
				break;
			}
		}

		//还没等待到dxgWait[i]回复，继续等
		if(!dxgReport[j])
		{
			break;
		}
	}

	//没有DXG需要等待了
	if(!dxgWait[i])
	{
		API_Event_NameAndMsg(EventMenuResCheckOnline, "Checking completed", NULL);
	}
} 

int API_DxgReportDtIm(int dxgId)
{
	int i;
	if(dxgId > 0 && dxgId < 100 && resCheckTip)
	{
		int waitFlag = 0;
		for (i = 0; dxgReport[i]; i++)
		{
			if(dxgReport[i] == dxgId)
			{
				return;
			}
		}
		dxgReport[i] = dxgId;
		dxgReport[i+1] = 0;

		char tip[500];
		snprintf(tip, 500, "Reported DXG:");
		for (i = 0; dxgReport[i]; i++)
		{
			char temp[5];
			snprintf(temp, 5, "%d ", dxgReport[i]);
			strcat(tip, temp);
		}
		ResCheckTipDisplay(tip, 0);

		//还没来得及标记需要等待的DXG
		if(!dxgWait[0])
		{
			return;
		}

		IfResCheckCompleted();
	}
}

static void CheckDtImOnline(void* para)
{
	char tip[50];
	dxgReport[0] = 0;
	dxgWait[0] = 0;

	ResCheckTipDisplay("Search DXG", 120);
	snprintf(tip, 50, "%ds", resCheckTime);
	API_TIPS_Mode_Title(resCheckTip, tip);

	API_Event_NameAndMsg(EventMenuResCheckOnline, "Find DXG", NULL);
}

int EventMenuResCheckOnlineCallback(cJSON* cmd)
{
	if(resCheckTip)
	{
		char tip[500];
		char* msg = GetEventItemString(cmd, EVENT_KEY_Message);
		if(!strcmp(msg, "Find DXG"))
		{
			int onlineDxgCnt = API_UpdateIxgList();
			if(onlineDxgCnt)
			{
				int i = 0;
				cJSON* view = cJSON_CreateArray();
				if(API_TB_SelectBySortByName(TB_NAME_IXG_LIST, view, NULL, 0))
				{
					cJSON* dxgElement;
					cJSON_ArrayForEach(dxgElement, view)
					{
						int id = GetEventItemInt(dxgElement, "GW_ID"); 
						if((id > 0) && (id < 100))
						{
							dxgWait[i++] = id;
						}
					}
				}
				cJSON_Delete(view);
				dxgWait[i] = 0;
				snprintf(tip, 500, "Find %d DXG", onlineDxgCnt);
				ResCheckTipDisplay(tip, 0);
				IfResCheckCompleted();
			}
			else
			{
				ResCheckTipClose();
				snprintf(tip, 500, "Find %d DXG, checking completed", onlineDxgCnt);
				API_TIPS_Ext_Time(tip, 2);
				CreateCheckDtImOnlineResult();
				API_Event_NameAndMsg(EventMenuSettingProcess, "SettingRefresh", NULL);
			}
		}
		else if(!strcmp(msg, "Checking completed"))
		{
			sleep(2);
			ResCheckTipClose();
			API_TIPS_Ext_Time("Checking completed", 2);
			CreateCheckDtImOnlineResult();
			API_Event_NameAndMsg(EventMenuSettingProcess, "SettingRefresh", NULL);
		}
		else if(!strcmp(msg, "Checking timeout"))
		{
			ResCheckTipClose();
			API_TIPS_Ext_Time("Checking timeout", 2);
			CreateCheckDtImOnlineResult();
			API_Event_NameAndMsg(EventMenuSettingProcess, "SettingRefresh", NULL);			
		}
	}
}


static void CreateCheckDtImOnlineResult(void)
{
	cJSON* dtImElement;
	cJSON* view = cJSON_CreateArray();

	API_TB_DeleteByName(TB_NAME_DT_IM_CHECK, NULL);

	API_TB_SelectBySortByName(TB_NAME_HYBRID_MAT, view, NULL, 0);
	cJSON_ArrayForEach(dtImElement, view)
	{
		cJSON_AddStringToObject(dtImElement, IX2V_STATE, IX2V_OFFLINE);
		API_TB_AddByName(TB_NAME_DT_IM_CHECK, dtImElement);
	}
	cJSON_Delete(view);
	
	view = cJSON_CreateArray();
    int dxgCnt = API_TB_SelectBySortByName(TB_NAME_IXG_LIST, view, NULL, 0);
	if(dxgCnt)
	{
		cJSON* dxgElement;
		cJSON* onlineState = cJSON_CreateObject();
		cJSON_AddStringToObject(onlineState, IX2V_STATE, IX2V_ONLINE);
		cJSON_ArrayForEach(dxgElement, view)
		{
			cJSON* dtIm = cJSON_GetObjectItemCaseSensitive(dxgElement, "DT_IM");
			int dxgId = GetEventItemInt(dxgElement, "GW_ID"); 
			cJSON_ArrayForEach(dtImElement, dtIm)
			{
				if(cJSON_IsNumber(dtImElement))
				{
					cJSON* record = cJSON_CreateObject();
					cJSON_AddNumberToObject(record, IX2V_IM_ID, dtImElement->valueint);
					cJSON_AddNumberToObject(record, IX2V_GW_ID, dxgId);
					if(API_TB_CountByName(TB_NAME_HYBRID_MAT, record))
					{
						API_TB_UpdateByName(TB_NAME_DT_IM_CHECK, record, onlineState);
					}
					else
					{
						cJSON_AddStringToObject(record, IX2V_IX_NAME, "");
						cJSON_AddStringToObject(record, IX2V_L_NBR, "");
						cJSON_AddStringToObject(record, IX2V_IX_ADDR, GW_ID_AND_IM_ID_TO_IX_ADDR(dxgId, dtImElement->valueint));
						cJSON_AddStringToObject(record, IX2V_GW_ADDR, GW_ID_TO_GW_ADDR(dxgId));
						cJSON_AddStringToObject(record, IX2V_IX_TYPE, "IM");
						cJSON_AddStringToObject(record, IX2V_CONNECT, "DT");
						cJSON_AddStringToObject(record, IX2V_G_NBR, "");
						cJSON_AddStringToObject(record, IX2V_STATE, IX2V_ADDED);
						API_TB_AddByName(TB_NAME_DT_IM_CHECK, record);
					}
					cJSON_Delete(record);
				}
			}
		}
		cJSON_Delete(onlineState);
	}
	cJSON_Delete(view);

	API_DtImCheckModificationInform();
} 








void Menu_CheckDtImOnline_Process(void* data)
{
	if(data)
	{
		if(!strcmp(data, "HYBRID_MAT_Check"))
		{
			API_MSGBOX("About to check DT IM online", 2, CheckDtImOnline, NULL);
		}
	}
}

void Menu_CheckDtImOnline_Close(void)
{
	ResCheckTipClose();
}


	

