#include "cJSON.h"
#include "Common_Record.h"
#include "menu_utility.h"
#include "MenuHybridMatCreateNew.h"
#include "cJSON.h"
#include "obj_TableSurver.h"
#include "define_string.h"
#include "obj_PublicInformation.h"
#include "utility.h"
#include "task_Event.h"
#include "define_file.h"
#include "obj_IoInterface.h"
#ifdef IX_LINK
#define SAT_DXG_START  				"IXG START"
#define SAT_DXG_END  				"IXG END"
#define SAT_DXG_NAME_PREFIX  		"IXG NAME PREFIX"
#else
#define SAT_DXG_START  				"DXG START"
#define SAT_DXG_END  				"DXG END"
#define SAT_DXG_NAME_PREFIX  		"DXG NAME PREFIX"
#endif
#define SAT_DS_START  				"DS START"
#define SAT_DS_END  				"DS END"
#define SAT_DS_NAME_PREFIX  		"DS NAME PREFIX"
#define SAT_DESC  					"Describe"
#ifdef IX_LINK
#define HYBRID_SAT_RECORD  "{\"IXG START\":1,\"IXG END\":8,\"IXG NAME PREFIX\":\"IXG\",\"DS START\":1,\"DS END\":4,\"DS NAME PREFIX\":\"DS\"}" //编辑的记录
#define APT_SAT_RECORD  "{\"DS START\":1,\"DS END\":8,\"DS NAME PREFIX\":\"DS\"}" //编辑的记录
#define SAT_PARA  "{\"Check\":{\"IXG START\":\"(^[1-9]{1}$)|(^(1|2)[0-9]{1}$)|(^3[0-2]{1}$)\", \"IXG END\":\"(^[1-9]{1}$)|(^(1|2)[0-9]{1}$)|(^3[0-2]{1}$)\", \"DS START\":\"^[1-8]{1}$\",\"DS END\":\"^[1-8]{1}$\"},\"Translate\":{}}" //编辑的记录校验
#define SAT_EDITOR_EdI_TB  "[\"IXG START\",\"IXG END\",\"IXG NAME PREFIX\",\"DS START\",\"DS END\",\"DS NAME PREFIX\",\"Describe\"]"	
#else
#define HYBRID_SAT_RECORD  "{\"DXG START\":1,\"DXG END\":8,\"DXG NAME PREFIX\":\"DXG\",\"DS START\":1,\"DS END\":4,\"DS NAME PREFIX\":\"DS\"}" //编辑的记录
#define APT_SAT_RECORD  "{\"DS START\":1,\"DS END\":8,\"DS NAME PREFIX\":\"DS\"}" //编辑的记录
#define SAT_PARA  "{\"Check\":{\"DXG START\":\"(^[1-9]{1}$)|(^(1|2)[0-9]{1}$)|(^3[0-2]{1}$)\", \"DXG END\":\"(^[1-9]{1}$)|(^(1|2)[0-9]{1}$)|(^3[0-2]{1}$)\", \"DS START\":\"^[1-8]{1}$\",\"DS END\":\"^[1-8]{1}$\"},\"Translate\":{}}" //编辑的记录校验
#define SAT_EDITOR_EdI_TB  "[\"DXG START\",\"DXG END\",\"DXG NAME PREFIX\",\"DS START\",\"DS END\",\"DS NAME PREFIX\",\"Describe\"]"
#endif

static cJSON* batchCreateSatRecord = NULL;
static RECORD_CONTROL* batchCreateSatMenu = NULL;

void Menu_CreateNewSat_Close(void);

void BatchCreateSatMenu(cJSON* record, RECORD_CONTROL** menu, BtnCallback cancelCallback, BtnCallback btnCallback, const char* title, char* btnString, ...)
{
	char* name;
	//可编辑字段
	cJSON* editorTable =  cJSON_Parse(SAT_EDITOR_EdI_TB);
	cJSON* resEditordescription = cJSON_Parse(SAT_PARA);


	cJSON* func = cJSON_CreateObject();
	cJSON* btnname = cJSON_AddArrayToObject(func,"BtnName");

	va_list valist;
	va_start(valist, btnString);
	for(name = btnString; name; name = va_arg(valist, char*))
	{
		cJSON_AddItemToArray(btnname, cJSON_CreateString(name));
	}
	va_end(valist);

	cJSON_AddNumberToObject(func, "BtnCallback", (int)btnCallback);
	cJSON_AddNumberToObject(func, "BackCallback", (int)cancelCallback);

    RC_MenuDisplay(menu, record, editorTable, resEditordescription, func, title);
    cJSON_Delete(editorTable);
    cJSON_Delete(resEditordescription);
    cJSON_Delete(func);
}


cJSON* CreateSatRecords(const cJSON* para, int maxRecordNbr)
{
	cJSON* retView = NULL;
	char tempString[100];
	int dxgStart = GetEventItemInt(para,  SAT_DXG_START);
	int dxgEnd = GetEventItemInt(para,  SAT_DXG_END);
	int dsStart = GetEventItemInt(para,  SAT_DS_START);
	int dsEnd = GetEventItemInt(para,  SAT_DS_END);

	char* dsNamePrefix = GetEventItemString(para,  SAT_DS_NAME_PREFIX);
	char* dxgNamePrefix = GetEventItemString(para,  SAT_DXG_NAME_PREFIX);
	int index;
	int recordNbr = 0;

	retView = cJSON_CreateArray();

	#define SAT_DS_MAX  				8
	#define SAT_DS_MIN  				1
	#define SAT_DXG_MAX  				32
	#define SAT_DXG_MIN  				1

	if(dsStart >= SAT_DS_MIN && dsStart <= SAT_DS_MAX && dsEnd >= SAT_DS_MIN && dsEnd <= SAT_DS_MAX)
	{
		for(index = dsStart; index <= dsEnd && recordNbr <= maxRecordNbr; index++, recordNbr++)
		{
			cJSON* record = cJSON_CreateObject();
			snprintf(tempString, 100, "00990000%02d", index);
			cJSON_AddStringToObject(record, "IX_ADDR", tempString);
			cJSON_AddStringToObject(record, "SYS_TYPE", "DH-APT");
			cJSON_AddStringToObject(record, "IX_TYPE", "DS");
			cJSON_AddNumberToObject(record, "DEV_ID", index);
			cJSON_AddItemToObject(record, "ASSO", cJSON_CreateArray());
			cJSON_AddStringToObject(record, "G_NBR", "");
			cJSON_AddStringToObject(record, "L_NBR", "");
			snprintf(tempString, 100, "%s%d", dsNamePrefix, index);
			cJSON_AddStringToObject(record, "IX_NAME", tempString);
			cJSON_AddItemToArray(retView, record);
		}
	}

	if(dxgStart >= SAT_DXG_MIN && dxgStart <= SAT_DXG_MAX && dxgEnd >= SAT_DXG_MIN && dxgEnd <= SAT_DXG_MAX)
	{
		for(index = dxgStart; index <= dxgEnd && recordNbr <= maxRecordNbr; index++, recordNbr++)
		{
			cJSON* record = cJSON_CreateObject();
			snprintf(tempString, 100, "0099%02d0001", index);
			cJSON_AddStringToObject(record, "IX_ADDR", tempString);
			cJSON_AddStringToObject(record, "SYS_TYPE", "DH-APT");
			#ifdef IX_LINK
			cJSON_AddStringToObject(record, "IX_TYPE", "IXG");
			#else
			cJSON_AddStringToObject(record, "IX_TYPE", "DXG");
			#endif
			cJSON_AddNumberToObject(record, "DEV_ID", index);
			cJSON_AddItemToObject(record, "ASSO", cJSON_CreateArray());
			cJSON_AddStringToObject(record, "G_NBR", "");
			cJSON_AddStringToObject(record, "L_NBR", "");
			snprintf(tempString, 100, "%s%d", dxgNamePrefix, index);
			cJSON_AddStringToObject(record, "IX_NAME", tempString);
			cJSON_AddItemToArray(retView, record);
			
		}
	}

	return retView;
}

static void CreateNewSatCancel(const char* type, void* user_data)
{  
	Menu_CreateNewSat_Close();
}

static void SatClearAndRecreate(const cJSON* para)
{
	cJSON* record;
	cJSON* view = CreateSatRecords(para, 256);
	if(cJSON_GetArraySize(view))
	{
		int satRecords = 0;
		char tempString[200];
		char targetFile[200];
		char translate[100];

		API_TB_SaveByName(TB_NAME_HYBRID_SAT);
		MakeDir(Backup_HYBRID_SAT_Folder);
		snprintf(targetFile, 200, "%s/SAT_%s.json", Backup_HYBRID_SAT_Folder, GetCurrentTime(tempString, 100, "%Y-%m-%d_%H:%M:%S"));
		CopyFile(TB_HYBRID_SAT_FILE_NAME, targetFile);

		API_TB_DeleteByName(TB_NAME_HYBRID_SAT, NULL);
		cJSON_ArrayForEach(record, view)
		{
			if(API_TB_AddByName(TB_NAME_HYBRID_SAT, record))
			{
				satRecords++;
			}
		}

		snprintf(tempString, 200, "%s %d", get_language_text2("Recreate SAT list", translate, 100), satRecords);
		API_TIPS_Ext_Time(tempString, 1);
		API_TableModificationInform(TB_NAME_HYBRID_SAT, GetEventItemString(para, SAT_DESC));
		API_TB_SaveByName(TB_NAME_HYBRID_SAT);

	}
	cJSON_Delete(view);

	Menu_CreateNewSat_Close();
} 

static void CreateNewSatFunction(const char* type, void* user_data)
{
    if(!strcmp(type, "Create"))
    {
		API_MSGBOX("About to clear SAT and recreate", 2, SatClearAndRecreate, batchCreateSatRecord);
    }
} 


void Menu_CreateNewSat_Init(void)
{
	char tempString[100];
	if(batchCreateSatRecord)
	{
		cJSON_Delete(batchCreateSatRecord);
	}
	batchCreateSatRecord = cJSON_Parse(GetMenuType() ? APT_SAT_RECORD : HYBRID_SAT_RECORD);
	cJSON_AddStringToObject(batchCreateSatRecord, "Describe", GetCurrentTime(tempString, 100, "%Y-%m-%d_%H:%M"));
	BatchCreateSatMenu(batchCreateSatRecord, &batchCreateSatMenu, CreateNewSatCancel, CreateNewSatFunction, "Create new SAT", "Create", NULL);
}

void Menu_CreateNewSat_Close(void)
{
	if(batchCreateSatMenu)
	{
		RC_MenuDelete(&(batchCreateSatMenu));
		batchCreateSatMenu = NULL;
	}

	if(batchCreateSatRecord)
	{
		cJSON_Delete(batchCreateSatRecord);
		batchCreateSatRecord = NULL;
	}
	API_Event_NameAndMsg(EventMenuSettingProcess, "SettingRefresh", NULL);
}