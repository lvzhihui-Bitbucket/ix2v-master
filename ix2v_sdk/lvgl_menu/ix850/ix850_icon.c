#include "ix850_icon.h"
#include "obj_lvgl_msg.h"
#include "MainList.h"

lv_obj_t* digitalCont = NULL;
lv_obj_t* codeCont = NULL;
lv_obj_t* helpCont = NULL;
lv_obj_t* roomCont = NULL;
lv_obj_t* nameCont = NULL;
lv_obj_t* maincont = NULL;
static lv_obj_t* roomNum = NULL;
static lv_obj_t*codeNum=NULL;
static lv_obj_t*codeTitle=NULL;
cJSON *NamelistSetting=NULL;
cJSON *RoomlistSetting=NULL;


void AddOneMenuCommonClickedCb(lv_obj_t* menu,lv_event_cb_t cb);

void MainMemu_Pressed_event_cb(lv_event_t* e);
static lv_obj_t* fcon_creat(lv_obj_t* parent, cJSON* json, const void* cb, const char* str, const char* symbol);
static lv_obj_t* Digital_Keyboard_creat(void);
static void help_click_event(lv_event_t* e);
static void digtal_click_event(lv_event_t* e);
static lv_obj_t* radiobutton_create(lv_obj_t* parent, const char* txt, lv_event_cb_t cb);
static void digtal_keyboard_click_event(lv_event_t* e);
static lv_obj_t* Code_Keyboard_creat(void);
static void code_keyboard_click_event(lv_event_t* e);
static void code_click_event(lv_event_t* e);
static void nameList_click_event(lv_event_t* e);
static void roomList_click_event(lv_event_t* e);
static void nameList_back_event(lv_event_t* e);
static void roomList_back_event(lv_event_t* e);
static lv_obj_t* roomList_creat(void);
static lv_obj_t* nameList_creat(void);
static void Jump_to_nameList(lv_event_t* e);
static void Jump_to_roomList(lv_event_t* e);
static void search_event_cb(lv_event_t* e);
static void lock_click_event(lv_event_t* e);
void common_click_event(lv_event_t* e);

lv_obj_t* img_creat_fcon(lv_obj_t* parent, struct ICON_INS* picon)
{
    cJSON* txt = NULL;
    cJSON* size = NULL;
    cJSON* color = NULL;
    cJSON* pos = cJSON_GetObjectItemCaseSensitive(picon->icon_data, get_global_key_string(GLOBAL_ICON_KEYS_POS));
    cJSON* text = cJSON_GetObjectItemCaseSensitive(picon->icon_data, get_global_key_string(GLOBAL_ICON_KEYS_TEXT));
    cJSON* function = cJSON_GetObjectItemCaseSensitive(picon->icon_data, get_global_key_string(GLOBAL_ICON_KEYS_FUNCTION));

    if(text){
        txt = cJSON_GetObjectItem(text,get_global_key_string(GLOBAL_ICON_KEYS_LABEL_TEXT));
        size = cJSON_GetObjectItem(text,get_global_key_string(GLOBAL_ICON_KEYS_TEXT_SIZE));
        color = cJSON_GetObjectItem(text,get_global_key_string(GLOBAL_ICON_KEYS_TEXT_COLOR));
    }

    lv_obj_t* icon = creat_cont_for_postion(parent, pos);
    picon->icon_cont = icon;
    lv_obj_t* label = lv_label_create(icon);

    if(size)
        set_font_size(label,size->valueint);
    if(txt){       
        char* name = get_symbol_key(txt->valuestring);
        lv_label_set_text(label, name);
    }else{
        lv_label_set_text(label, "");
    } 
        
    if(color)
        lv_obj_set_style_text_color(label,lv_color_hex(color->valueint),0);  
    lv_obj_center(label);

    if (strcmp(function->valuestring, "Code_kp") == 0)
    {
        lv_obj_add_event_cb(icon, code_click_event, LV_EVENT_CLICKED, NULL);
    }
    else if (strcmp(function->valuestring, "NameList") == 0)
    {
        lv_obj_add_event_cb(icon, nameList_click_event, LV_EVENT_CLICKED, NULL);
    }
    else if (strcmp(function->valuestring, "RoomList") == 0)
    {
        lv_obj_add_event_cb(icon, roomList_click_event, LV_EVENT_CLICKED, NULL);
    }
    else if (strcmp(function->valuestring, "Digital_kp") == 0)
    {
        lv_obj_add_event_cb(icon, digtal_click_event, LV_EVENT_CLICKED, NULL);
    }
    else if (strcmp(function->valuestring, "Help") == 0)
    {
        lv_obj_add_event_cb(icon, help_click_event, LV_EVENT_CLICKED, NULL);
    }

	else
	{
		lv_obj_add_event_cb(icon, common_click_event, LV_EVENT_CLICKED, function->valuestring);
	}
}

static void nameList_click_event(lv_event_t* e)
{
	MainMenu_Process(MAINMENU_MAIN_NAMELIST,NULL);
}


static void roomList_click_event(lv_event_t* e)
{
    //scr_scroll_test();
    MainMenu_Process(MAINMENU_MAIN_ROOMLIST,NULL);
}


static void help_click_event(lv_event_t* e)
{
	MainMenu_Process(MAINMENU_MAIN_HELP,NULL);
}

static void digtal_click_event(lv_event_t* e)
{
	MainMenu_Process(MAINMENU_MAIN_DIALKP,NULL);
}

static void code_click_event(lv_event_t* e)
{
	MainMenu_Process(MAINMENU_MAIN_CODEKP,NULL);
#if 0
    if (codeCont == NULL)
    {
        Code_Keyboard_creat();
	    AddOneMenuCommonClickedCb	(codeCont,MainMemu_Pressed_event_cb);
    }
    else
    {
        lv_obj_clear_flag(codeCont, LV_OBJ_FLAG_HIDDEN);
          
    }
    lv_obj_add_flag(maincont, LV_OBJ_FLAG_HIDDEN);
	KeyPad_Manage_Init(0);
#else
	//MenuIXGTbView_Init(NULL);
    //create_setting_menu_root();
#endif

}
void common_click_event(lv_event_t* e)
{
	void *user_data=lv_event_get_user_data(e);
	MainMenu_Process(MAINMENU_COMMON_CLICK,user_data);
#if 0
    if (digitalCont == NULL)
    {
        Digital_Keyboard_creat();
	AddOneMenuCommonClickedCb	(digitalCont,MainMemu_Pressed_event_cb);	
    }
    else
    {
    	lv_label_set_text(roomNum,"");
        lv_obj_clear_flag(digitalCont, LV_OBJ_FLAG_HIDDEN);
    }
    lv_obj_add_flag(maincont, LV_OBJ_FLAG_HIDDEN);
#endif
}


void AddOneMenuCommonClickedCb(lv_obj_t* menu,lv_event_cb_t cb)
{
	lv_obj_t* one_onde;
	int child_cnt=lv_obj_get_child_cnt(menu);
	int i;
	int j;
	int filter;
	//if(lv_obj_has_flag(menu,LV_OBJ_FLAG_CLICKABLE))
	//	lv_obj_add_event_cb(menu, MainMemu_Pressed_event_cb, LV_EVENT_PRESSED, NULL);
	#if 0
	for(i=0;i<child_cnt;i++)
	{
		one_onde=lv_obj_get_child(menu,i);
		AddOneMenuCommonClickedCb(one_onde,cb);
		if(lv_obj_has_flag(one_onde,LV_OBJ_FLAG_CLICKABLE))
		{
			if(one_onde->spec_attr==NULL)
				continue;
			if(one_onde->spec_attr->event_dsc_cnt==0)
				continue;
			lv_obj_remove_event_cb(one_onde,cb);
			lv_obj_add_event_cb(one_onde, cb, LV_EVENT_CLICKED, NULL);
			#if 
			for(j=0;j<one_onde->spec_attr->event_dsc_cnt;j++)
			{
				filter=(int)one_onde->spec_attr->event_dsc[j].filter;
				if(filter==0)//LV_EVENT_CLICKED)
				{
					lv_obj_add_event_cb(one_onde, cb, LV_EVENT_PRESSED, NULL);
					break;
				}
			}
			#endif
		}
		
	}
	#endif
}




