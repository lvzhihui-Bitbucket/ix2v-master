#include "Common_TableView.h"
#include "define_file.h"
#include "utility.h"
#include "task_Event.h"
#include "obj_TableSurver.h"
#include "task_Beeper.h"
#include "lv_demo_msg.h"
#include "obj_CommonSelectMenu.h"

typedef struct 
{   
    char* resFlag;
	char* resFolder;
} RESFLAG_TO_FOLDER;

#define RES_SELECT_PARA		"{\"HeaderEnable\":true,\"CellCallback\":0,\"ColumnWidth\":[360, 120, 360]}"
#define RES_SELECT_FIELD_NAME			"NAME"
#define RES_SELECT_FIELD_DESCRIBE		"DESCRIBE"
#define RES_SELECT_FIELD_SOURCE			"SOURCE"

static TB_VIWE* resSelect = NULL;
static const RESFLAG_TO_FOLDER RES_FOLDER[] = {
	{"CUR", TB_HYBRID_MAT_FILE_NAME},
	{"FW", Factory_HYBRID_MAT_Folder},
	{"CUS", Customerized_HYBRID_MAT_Folder},
	{"BAK", Backup_HYBRID_MAT_Folder},
	{"SD Card", SDCARD_HYBRID_MAT_Folder},
};

static int clickLine;
static const int RES_FOLDER_NUM = sizeof(RES_FOLDER)/sizeof(RES_FOLDER[0]);

static void ResSelectFilterCallback(const cJSON* filter);
static void FunctionClick(const char* funcName);
void MenuRES_Select_Close(void);

static const  char* FlagToFolder(const char *flag)
{
	int i;
	for(i = 0; i < RES_FOLDER_NUM; i++)
	{
		if(flag && RES_FOLDER[i].resFlag && !strcmp(flag, RES_FOLDER[i].resFlag))
		{
			return RES_FOLDER[i].resFolder;
		}
	}
    return NULL;
}

static int StrcmpEnd(const char *str1, const char *str2)
{
	int ret = 0;
	if(str1 == str2)
	{
		return ret;
	}

	if(str1 && str2)
	{
		int start1 = strlen(str1);
		int start2 = strlen(str2);
	
		while (start1 >= 0 && start2 >= 0)
		{
			if (str1[start1] != str2[start2])
			{
				ret = str1[start1] - str2[start2];
				return ret;
			}
			start1--;
			start2--;
		}
	}
	else
	{
		ret = (str1 ? 1 : -1);
		return ret;
	}

    return ret;
}

static void ResUpdate(void* data)
{
	int* pLine = (int*)data;
	if(resSelect)
	{
		
		cJSON* record = cJSON_GetArrayItem(resSelect->view, *pLine);
		if(cJSON_IsObject(record))
		{
			char temp[500];
			char* fileName = GetEventItemString(record,  RES_SELECT_FIELD_NAME);
			char* describe = GetEventItemString(record,  RES_SELECT_FIELD_DESCRIBE);
			char* source = GetEventItemString(record,  RES_SELECT_FIELD_SOURCE);
			char* path = FlagToFolder(source);
			int fileType = CheckFileType(path);
			if(fileType == 1)
			{
				snprintf(temp, 500, "%s/%s", path, fileName);
			}
			else if(fileType == 2)
			{
				snprintf(temp, 500, "%s", path);
			}
			else
			{
				API_TIPS_Ext_Time("Update error", 1);
				return;
			}

			if(source && !strcmp(source, "SD Card"))
			{
				MakeDir(Customerized_HYBRID_MAT_Folder);
				CopyFile(temp, Customerized_HYBRID_MAT_Folder);
			}

			CopyFile(temp, TB_HYBRID_MAT_FILE_NAME);
			API_TB_ReloadByName(TB_NAME_HYBRID_MAT,NULL);
			API_Event_By_Name(Event_MAT_Update);
			snprintf(temp, 500, "%s update", ((describe && describe[0]) ? describe : fileName));
			API_TIPS_Ext_Time(temp, 1);
		}
		MenuRES_Select_Close();
	}
}

static void ResCellClick(int line, int col)
{
    dprintf("line = %d, col =%d\n", line, col);
	if(resSelect)
	{
		cJSON* record = cJSON_GetArrayItem(resSelect->view, line);
		if(cJSON_IsObject(record))
		{
			char* source = GetEventItemString(record,  RES_SELECT_FIELD_SOURCE);
			if(source && !strcmp(source, "CUR"))
			{
				TB_MenuDelete(&resSelect);
			}
			else
			{
				char temp[500];
				clickLine = line;
				char* fileName = GetEventItemString(record,  RES_SELECT_FIELD_NAME);
				char* describe = GetEventItemString(record,  RES_SELECT_FIELD_DESCRIBE);
				snprintf(temp, 500, "%s update", ((describe && describe[0]) ? describe : fileName));
				API_MSGBOX(temp, 2, ResUpdate, &clickLine);
			}
		}
	}
}


static cJSON* ResSelectCreateView(const char* resFlag)
{   
    cJSON* view = cJSON_CreateArray();
	if(!resFlag)
	{
		return view;
	}

	int folderIndex;
	for(folderIndex = 0; folderIndex < RES_FOLDER_NUM; folderIndex++)
	{
		if(!strcmp(resFlag, "ALL") || !strcmp(resFlag, RES_FOLDER[folderIndex].resFlag))
		{
			cJSON* file;
			cJSON* fileList = cJSON_CreateArray();	
			int fileType = CheckFileType(RES_FOLDER[folderIndex].resFolder);
			if(fileType == 1)
			{
				GetFileAndDirList(RES_FOLDER[folderIndex].resFolder, fileList, 0);
			}
			else if(fileType == 2)
			{
				char* fileName = GetFilenameFromPath(RES_FOLDER[folderIndex].resFolder);
				file = cJSON_CreateArray();
				cJSON_AddItemToArray(file, cJSON_CreateString(fileName));
				cJSON_AddItemToArray(file, cJSON_CreateString(fileName));
				cJSON_AddItemToArray(file, cJSON_CreateString(fileName));
				cJSON_AddItemToArray(file, cJSON_CreateString(fileName));
				cJSON_AddItemToArray(fileList, file);
			}
			else
			{
				continue;
			}

			cJSON_ArrayForEach(file, fileList)
			{
				cJSON* fileName = cJSON_GetArrayItem(file, 3);
				if(cJSON_IsString(fileName))
				{
					if((!StrcmpEnd(fileName->valuestring, ".json") || !StrcmpEnd(fileName->valuestring, ".JSON")))
					{
						char temp[500];
						if(fileType == 1)
						{
							snprintf(temp, 500, "%s/%s", RES_FOLDER[folderIndex].resFolder, fileName->valuestring);
						}
						else if(fileType == 2)
						{
							snprintf(temp, 500, "%s", RES_FOLDER[folderIndex].resFolder);
						}
						else
						{
							continue;
						}

						cJSON* res = GetJsonFromFile(temp);
						char* disc = GetEventItemString(cJSON_GetObjectItemCaseSensitive(res, TB_DESC), "NEW_R8001_TB_DESC");
						cJSON* record = cJSON_CreateObject();
						cJSON_AddStringToObject(record, RES_SELECT_FIELD_DESCRIBE, disc);
						cJSON_AddStringToObject(record, RES_SELECT_FIELD_SOURCE, RES_FOLDER[folderIndex].resFlag);
						cJSON_AddStringToObject(record, RES_SELECT_FIELD_NAME, fileName->valuestring);
						cJSON_AddItemToArray(view, record);
						cJSON_Delete(res);
					}
				}
			}
			cJSON_Delete(fileList);
		}
	}
	return view;
}

static void ResSelectReturnCallback(void)
{
	MenuRES_Select_Close();
}

static cJSON* ResSelectCreatePara(const char* resFlag)
{   
    cJSON* fliter = cJSON_CreateObject();
    cJSON_AddStringToObject(fliter, "Name", "RES path");
    cJSON_AddStringToObject(fliter, "Value", resFlag);
    cJSON_AddNumberToObject(fliter, "Callback", (int)ResSelectFilterCallback);
	cJSON* para = cJSON_Parse(RES_SELECT_PARA);
    cJSON_AddItemToObject(para,"Filter", fliter);
	cJSON_ReplaceItemInObjectCaseSensitive(para, "CellCallback", cJSON_CreateNumber((int)ResCellClick));
	cJSON_AddNumberToObject(para, "ReturnCallback", (int)ResSelectReturnCallback);
	
	cJSON* func = cJSON_CreateObject();
	cJSON* funcName = cJSON_AddArrayToObject(func, "Name");
	cJSON_AddItemToArray(funcName, cJSON_CreateString("Delete BAK"));
	cJSON_AddItemToArray(funcName, cJSON_CreateString("Delete CUS"));
	cJSON_AddNumberToObject(func, "Callback", (int)FunctionClick);
	cJSON_AddItemToObject(para, "Function", func);

	return para;
}

static void ResSelectFilterProcess(const char* chose, void* userdata)
{   
    if(!chose)
    {
        return;
    }

    if(!strcmp("ALL",chose) || !strcmp("CUR",chose) || !strcmp("FW",chose) || !strcmp("BAK",chose) || !strcmp("CUS",chose) || !strcmp("SD Card",chose))
	{
        cJSON* para = ResSelectCreatePara(chose);
		cJSON* view = ResSelectCreateView(chose);
		TB_MenuDisplay(&resSelect, view, "HYBRID_MAT_Choose", para);
		cJSON_Delete(view);
		cJSON_Delete(para);
    }
}

static void ResSelectFilterCallback(const cJSON* filter)
{   
    cJSON* value = cJSON_GetObjectItem(filter,"Value");
    static CHOSE_MENU_T callback = {.callback = 0, .cbData = NULL};
	callback.callback = ResSelectFilterProcess;
	callback.cbData = NULL;
    cJSON* choseList = cJSON_CreateArray();
    cJSON_AddItemToArray(choseList, cJSON_CreateString("ALL"));
    cJSON_AddItemToArray(choseList, cJSON_CreateString("CUR"));
    cJSON_AddItemToArray(choseList, cJSON_CreateString("FW"));
    cJSON_AddItemToArray(choseList, cJSON_CreateString("BAK"));
    cJSON_AddItemToArray(choseList, cJSON_CreateString("CUS"));
    cJSON_AddItemToArray(choseList, cJSON_CreateString("SD Card"));
    char* choseString = cJSON_IsString(value) ? value->valuestring : NULL;
    CreateChooseMenu("RES path", choseList, choseString, &callback);
    cJSON_Delete(choseList);
}


static void ResDeleteBak(void* data)
{
	DeleteFileProcess(Backup_HYBRID_MAT_Folder, "*.json");
	DeleteFileProcess(Backup_HYBRID_MAT_Folder, "*.JSON");
	API_TIPS_Ext_Time("Delete BAK", 1);
	TB_MenuDelete(&resSelect);
}

static void ResDeleteCus(void* data)
{
	DeleteFileProcess(Customerized_HYBRID_MAT_Folder, "*.json");
	DeleteFileProcess(Customerized_HYBRID_MAT_Folder, "*.JSON");
	API_TIPS_Ext_Time("Delete CUS", 1);
	TB_MenuDelete(&resSelect);
}

static void FunctionClick(const char* funcName)
{
	if(funcName)
	{
		if(!strcmp(funcName, "Delete BAK"))
		{
			API_MSGBOX("Delete BAK", 2, ResDeleteBak, NULL);
		}
		else if(!strcmp(funcName, "Delete CUS"))
		{
			API_MSGBOX("Delete CUS", 2, ResDeleteCus, NULL);
		}
	}
}



void MenuRES_Select_Init(void)
{
	ResSelectFilterProcess("ALL", NULL);
}

void MenuRES_Select_Close(void)
{
	TB_MenuDelete(&resSelect);
	API_Event_NameAndMsg(EventMenuSettingProcess, "SettingRefresh", NULL);
}