#include "ix850_callbtn.h"
#include "obj_lvgl_msg.h"
#include "MenuSetting.h"

static void ImgCallBtn_click_event(lv_event_t* e);

lv_obj_t* img_creat_callbtn(lv_obj_t* parent, struct ICON_INS* picon)
{
    cJSON* pos = cJSON_GetObjectItemCaseSensitive(picon->icon_data, get_global_key_string(GLOBAL_ICON_KEYS_POS));
    cJSON* text = cJSON_GetObjectItemCaseSensitive(picon->icon_data, get_global_key_string(GLOBAL_ICON_KEYS_TEXT));
    cJSON* name = cJSON_GetObjectItemCaseSensitive(picon->icon_data, get_global_key_string(GLOBAL_ICON_KEYS_CALLBTN_NAME));
    cJSON* number = cJSON_GetObjectItemCaseSensitive(picon->icon_data, get_global_key_string(GLOBAL_ICON_KEYS_CALLBTN_NUMBER));

    cJSON* color = cJSON_GetObjectItemCaseSensitive(text, get_global_key_string(GLOBAL_ICON_KEYS_TEXT_COLOR));
    cJSON* size = cJSON_GetObjectItemCaseSensitive(text, get_global_key_string(GLOBAL_ICON_KEYS_TEXT_SIZE));
    cJSON* labelName = cJSON_GetObjectItemCaseSensitive(text, get_global_key_string(GLOBAL_ICON_KEYS_LABEL_TEXT));
	cJSON* pos_y_offset = cJSON_GetObjectItemCaseSensitive(text, "pos_y_offset");
	
    lv_obj_t* callbtn = creat_cont_for_postion(parent, pos);
    lv_obj_add_event_cb(callbtn, ImgCallBtn_click_event, LV_EVENT_CLICKED, picon->icon_data);

    lv_obj_t* label = lv_label_create(callbtn);
    lv_label_set_text(label, "");
    //lv_obj_center(label);
    if(pos_y_offset)
	lv_obj_align(label,LV_ALIGN_BOTTOM_MID,0,pos_y_offset->valueint);	
	else
    	lv_obj_align(label,LV_ALIGN_BOTTOM_MID,0,-30);
    if (text)
    {
        lv_obj_set_style_text_color(label, lv_color_hex(color->valueint), 0);
        set_font_size(label, size->valueint);

        if (name->valueint)
        {
            lv_label_set_text(label, labelName->valuestring);
        }
    }
}
static SETTING_VIEW* disp = NULL;

static void CallBtn_Edit_click(void* data)
{

    API_JsonMenuDelete(&disp);

    API_PublicInfo_Write_Bool("OnInstaller", 0);
    API_PublicInfo_Write_Bool("MenuEdit", false);
    MainMenu_Close();

    MainMenu_Init();
}

static void ImgCallBtn_click_event(lv_event_t* e)
{
    char name[50];
    char text[50];
    char nbr[50];
    char title[50];
    char size[50];
    char color[50];
    cJSON* icon_data = lv_event_get_user_data(e);
	cJSON* icon_name = cJSON_GetObjectItemCaseSensitive(icon_data, "ICON_NAME");
	cJSON* labelName = cJSON_GetObjectItemCaseSensitive(cJSON_GetObjectItemCaseSensitive(icon_data, get_global_key_string(GLOBAL_ICON_KEYS_TEXT)), get_global_key_string(GLOBAL_ICON_KEYS_LABEL_TEXT));
    cJSON* number = cJSON_GetObjectItemCaseSensitive(icon_data, get_global_key_string(GLOBAL_ICON_KEYS_CALLBTN_NUMBER));
    if(API_PublicInfo_Read_Bool("MenuEdit"))
    {
        cJSON* object = cJSON_CreateObject();
        sprintf(name,"%sEdit",icon_name->valuestring);
        sprintf(text,"%s_TEXT",icon_name->valuestring);
        sprintf(nbr,"%s_NBR",icon_name->valuestring);
        sprintf(title,"%s Editing",icon_name->valuestring);
        sprintf(size,"%s_SIZE",icon_name->valuestring);
        sprintf(color,"%s_COLOR",icon_name->valuestring);

        cJSON* item1 = cJSON_CreateObject();
        cJSON_AddStringToObject(item1, "PageTitle", title);
        cJSON* item_arr = cJSON_CreateArray();
        cJSON_AddItemToArray(item_arr,cJSON_CreateString(text));
        cJSON_AddItemToArray(item_arr,cJSON_CreateString(nbr));
        cJSON_AddItemToArray(item_arr,cJSON_CreateString(size));
        cJSON_AddItemToArray(item_arr, cJSON_CreateString(color));
        cJSON_AddItemToArray(item_arr,cJSON_CreateString("ITEM_Replace"));
        cJSON_AddItemToObject(item1,"Items",item_arr);
        cJSON_AddItemToObject(object,name,item1);

        cJSON* item2 = cJSON_CreateObject();
        cJSON_AddStringToObject(item2, "type", "Item_IO_Para");
        cJSON_AddStringToObject(item2, "ParaName", text);
        cJSON_AddItemToObject(object,text,item2);

        cJSON* item3 = cJSON_CreateObject();
        cJSON_AddStringToObject(item3, "type", "Item_IO_Para");
        cJSON_AddStringToObject(item3, "ParaName", nbr);
        cJSON_AddItemToObject(object,nbr,item3);

        cJSON* item5 = cJSON_CreateObject();
        cJSON_AddStringToObject(item5, "type", "Item_IO_Para");
        cJSON_AddStringToObject(item5, "ParaName", size);
        cJSON_AddItemToObject(object,size,item5);

        cJSON* item6 = cJSON_CreateObject();
        cJSON_AddStringToObject(item6, "type", "Item_IO_Para");
        cJSON_AddStringToObject(item6, "ParaName", color);
        cJSON_AddItemToObject(object,color,item6);

        cJSON* item4 = cJSON_CreateObject();
        cJSON_AddStringToObject(item4, "type", "ITEM_btn");
        cJSON_AddStringToObject(item4, "text", "Replace");
        cJSON_AddStringToObject(item4, "msg", "replace");
        cJSON_AddItemToObject(object,"ITEM_Replace",item4);
        API_JsonMenuDisplay(&disp,object,name,CallBtn_Edit_click);
        cJSON_Delete(object);
    }else{
        cJSON* object = cJSON_CreateObject();
        cJSON_AddStringToObject(object, "DevNum", number->valuestring);
        printf("Number = %s \n",number->valuestring);
        if(labelName!=NULL)
            cJSON_AddStringToObject(object, "Name", labelName->valuestring);
        MainMenu_Process(MAINMENU_MAIN_StartCall,object);
        
        cJSON_Delete(object);
    }
    Main_Header_Reset();  
}

