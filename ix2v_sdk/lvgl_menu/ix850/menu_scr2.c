#include <stdio.h>
#include "lv_ix850.h"
//#include "lv_demo_menu.h"

lv_obj_t * ui_settings=NULL;

static void btn_clicked_event_handler(lv_event_t* e);
static void Call_scroll_event_cb(lv_event_t* e);
static void Call_list_menu_back_event_cb(lv_event_t* e);
static void Call_list_pagedn_event_cb(lv_event_t* e);
static void Call_list_pageup_event_cb(lv_event_t* e);
static void Call_refresh_event_cb(lv_event_t* e);


void MainMemu_Pressed_event_cb(lv_event_t* e);
void ui_setting_init(void)
{
    vtk_lvgl_lock(); 
    ui_settings = lv_obj_create(NULL);
    lv_obj_remove_style_all(ui_settings);
    lv_obj_set_size(ui_settings, lv_disp_get_hor_res(NULL), lv_disp_get_ver_res(NULL));
    lv_obj_align(ui_settings, LV_ALIGN_TOP_LEFT, 0, 0);
    lv_obj_set_style_bg_color(ui_settings, lv_color_hex(0x41454D), 0);//lv_color_hex(0x102030)
    lv_obj_set_style_bg_opa(ui_settings, LV_OPA_100, 0);
    vtk_lvgl_unlock();
}

void scr_settings(void)
{
	//update_menu_stytle_choose(NULL);
	vtk_lvgl_lock();  

	vtk_lvgl_unlock();
              
}


void MenuScr2Test(void)
{
    pthread_t	    tid; 

	pthread_create(&tid, NULL, scr_settings, NULL);
}
