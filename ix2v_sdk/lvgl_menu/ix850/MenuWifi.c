#include "MenuWifi.h"
#include "icon_common.h"
#include "KeyboardResource.h"
#include "menu_utility.h"
#include "obj_lvgl_msg.h"
#include "obj_IoInterface.h"

extern lv_ft_info_t ft_small;

#define TB_VIEW_FunctionColor       lv_color_hex(0x414500)
#define TB_VIEW_BackgroundColor     lv_color_hex(0x41454D)
#define TB_VIEW_TextColor           lv_color_white()
#define TB_VIEW_NotEditTextColor    lv_color_hex(0x41004D)
#define TB_VIEW_TitleHeight         73
#define TB_VIEW_LineHeight          70
#define TB_VIEW_TextSize            (ft_small.font)
#define TB_VIEW_ReturnWidth         160
#define TB_VIEW_ScrWidth            480

static lv_obj_t* ui_wifi_init(void)
{
    lv_obj_t* ui_wifi = lv_obj_create(NULL);
    lv_obj_clear_flag(ui_wifi, LV_OBJ_FLAG_SCROLLABLE);
    lv_obj_remove_style_all(ui_wifi);
    lv_obj_set_size(ui_wifi, lv_disp_get_hor_res(NULL), lv_disp_get_ver_res(NULL));
    lv_obj_align(ui_wifi, LV_ALIGN_TOP_LEFT, 0, 0);
    lv_obj_set_style_bg_color(ui_wifi, lv_palette_main(LV_PALETTE_GREY), 0);
    lv_obj_set_style_bg_opa(ui_wifi, LV_OPA_100, 0);
    return ui_wifi;
}

static void switch_handler(lv_event_t* e)
{
    lv_obj_t* obj = lv_event_get_target(e);
    WIFI_VIEW* disp = (WIFI_VIEW*)lv_event_get_user_data(e);
    if(!disp)
    {
        return;
    }
    bool chk = lv_obj_has_state(obj, LV_STATE_CHECKED);
    if(chk){
        lv_obj_clear_flag(disp->table,LV_OBJ_FLAG_HIDDEN);
        lv_obj_clear_flag(disp->curLabel,LV_OBJ_FLAG_HIDDEN);
        disp->value = 1;
        API_Para_Write_Int("WIFI_SWITCH",1);
        API_PublicInfo_Write_Int("WIFI_SWITCH",1);
        API_Wlan_Open(NULL);
        LV_API_TIPS("Searching, please wait!",1); 
        API_Wlan_Search();
    }      
    else{
        lv_obj_add_flag(disp->table,LV_OBJ_FLAG_HIDDEN);
        lv_obj_add_flag(disp->curLabel,LV_OBJ_FLAG_HIDDEN);
        disp->value = 0;
        API_Para_Write_Int("WIFI_SWITCH",0);
        API_PublicInfo_Write_Int("WIFI_SWITCH",0);
        API_Wlan_Close();
    }
}

static void Wifi_AddSwitch(WIFI_VIEW* disp,const char* name ,int val)
{
    if(!disp)
    {
        return;
    }

    lv_obj_t* obj = lv_obj_create(disp->menu);
    lv_obj_set_size(obj, TB_VIEW_ScrWidth, TB_VIEW_TitleHeight);
    lv_obj_set_style_border_color(obj, lv_color_make(55,55,55), 0);
    lv_obj_set_style_bg_color(obj, TB_VIEW_BackgroundColor, 0);   
    lv_obj_set_style_text_color(obj, TB_VIEW_TextColor, 0);
    lv_obj_set_style_pad_all(obj,0,0);
    lv_obj_set_y(obj,TB_VIEW_TitleHeight);
    lv_obj_set_style_text_font(obj, TB_VIEW_TextSize, 0); 

    lv_obj_t* label = lv_label_create(obj);
    lv_obj_align(label, LV_ALIGN_LEFT_MID,20,0);
    lv_obj_set_size(label, 240, LV_SIZE_CONTENT);
    vtk_label_set_text(label, name);

    lv_obj_t* sw = lv_switch_create(obj);
    lv_obj_align(sw, LV_ALIGN_RIGHT_MID,-20,0);
    lv_obj_set_size(sw, 100, 40);
    lv_obj_add_state(sw, val ? LV_STATE_CHECKED : 0);

    lv_obj_add_event_cb(sw, switch_handler, LV_EVENT_VALUE_CHANGED, disp);		
    if(val){
        LV_API_TIPS("Searching, please wait!",1);          
        API_Wlan_Search();
    }
}


static void Wifi_HeadReturn(lv_event_t* e)
{
    WIFI_VIEW** rcDisp = lv_event_get_user_data(e);

    Wifi_MenuDelete(rcDisp);
}

static void Wifi_AddTitle(WIFI_VIEW** disp, char* txt)
{
    WIFI_VIEW* rc;

    rc = *disp;

    lv_obj_t* btn = lv_obj_create(rc->menu);
    lv_obj_set_size(btn,TB_VIEW_ScrWidth, TB_VIEW_TitleHeight);   
    lv_obj_set_style_bg_color(btn, lv_color_make(26,82,196), 0);
    lv_obj_set_style_pad_all(btn,0,0);
    lv_obj_set_style_border_width(btn, 0, 0);

    lv_obj_add_event_cb(btn, Wifi_HeadReturn, LV_EVENT_CLICKED, disp);
    lv_obj_add_flag(btn, LV_OBJ_FLAG_CLICKABLE);
    lv_obj_clear_flag(btn, LV_OBJ_FLAG_SCROLLABLE);

    lv_obj_t* obj = lv_obj_create(btn);
    lv_obj_remove_style_all(obj);
    lv_obj_add_flag(obj, LV_OBJ_FLAG_EVENT_BUBBLE);
    lv_obj_set_style_bg_opa(obj,LV_OPA_MAX,0);
    lv_obj_set_size(obj,160,TB_VIEW_TitleHeight);
    lv_obj_set_style_bg_color(obj,lv_color_make(19,132,250),0);
    lv_obj_set_style_text_color(obj, lv_color_white(), 0);
    lv_obj_t* img = lv_img_create(obj);
	lv_img_set_src(img,LV_VTK_LEFT);
    set_font_size(img,1);
    lv_obj_center(img);

    lv_obj_t* titlelabel = lv_label_create(btn); 
    lv_obj_align(titlelabel, LV_ALIGN_LEFT_MID, TB_VIEW_ReturnWidth, 0);
    lv_obj_set_size(titlelabel, TB_VIEW_ScrWidth-TB_VIEW_ReturnWidth, 30);
    lv_obj_set_style_text_align(titlelabel,LV_TEXT_ALIGN_CENTER,0);
    lv_obj_set_style_text_font(titlelabel, TB_VIEW_TextSize, 0); 
    lv_obj_set_style_text_color(titlelabel, TB_VIEW_TextColor, 0);
    lv_label_set_long_mode(titlelabel, LV_LABEL_LONG_DOT);
    vtk_label_set_text(titlelabel,txt);
}

static void Wifi_AddWifiName(WIFI_VIEW* disp,const char* name)
{
    if(!disp)
        return;
    lv_obj_t* obj = lv_obj_create(disp->menu);
    lv_obj_set_size(obj, TB_VIEW_ScrWidth, TB_VIEW_TitleHeight);
    lv_obj_set_style_border_color(obj, lv_color_make(55,55,55), 0);
    lv_obj_set_style_bg_color(obj, TB_VIEW_BackgroundColor, 0);   
    lv_obj_set_style_text_color(obj, TB_VIEW_TextColor, 0);
    lv_obj_set_style_pad_all(obj,0,0);
    lv_obj_set_style_text_font(obj, TB_VIEW_TextSize, 0); 
    lv_obj_set_y(obj,TB_VIEW_TitleHeight*2);

    lv_obj_t* label = lv_label_create(obj);
    lv_obj_align(label, LV_ALIGN_LEFT_MID,10,0);
    lv_obj_set_size(label, 240, LV_SIZE_CONTENT);
    //lv_obj_set_style_text_align(vallabel, LV_TEXT_ALIGN_LEFT, 0);
    vtk_label_set_text(label, name);

    lv_obj_t* vallabel = lv_label_create(obj);
    lv_obj_align(vallabel, LV_ALIGN_RIGHT_MID,-10,0);
    lv_obj_set_size(vallabel, 240, LV_SIZE_CONTENT);
    lv_obj_set_style_text_align(vallabel, LV_TEXT_ALIGN_RIGHT, 0);
    vtk_label_set_text(vallabel, "");

    disp->curLabel = obj;
    if(!disp->value)
        lv_obj_add_flag(obj,LV_OBJ_FLAG_HIDDEN);
    else{
        cJSON* jpara = get_wlan_ssid();
        if(jpara){
            cJSON* ssid = cJSON_GetArrayItem(jpara, 0);
            if(ssid){
                char* ssid_name = GetEventItemString(ssid, "SSID_NAME");
                lv_label_set_text(vallabel, ssid_name);
            }
        }
    }       
}

static int* text_edit_event_cb(const char* val,void *user_data)
{
    WIFI_VIEW* disp = (WIFI_VIEW*)user_data;

    cJSON* item = cJSON_GetArrayItem(disp->view, disp->cnt_row);
    if(!item)
        return;

    API_Para_Write_String("Mobile_SSID_NAME1", item->valuestring); 
    API_Para_Write_String("Mobile_SSID_PWD1", val); 

    API_Para_Write_String("Mobile_SSID_NAME", item->valuestring); 
    API_Para_Write_String("Mobile_SSID_PWD", val); 

	cJSON *ssid_arr=cJSON_Duplicate(API_Para_Read_Public(WIFI_SSID_PARA),1);
	cJSON *node;
	cJSON_ArrayForEach(node,ssid_arr)
	{
		cJSON *name=cJSON_GetObjectItemCaseSensitive(node,"SSID_NAME");
		if(name&&strcmp(name->valuestring,item->valuestring)==0)
		{
			cJSON_ReplaceItemInObject(node,"SSID_PWD",cJSON_CreateString(val));
			break;
		}
	}
	if(node==NULL)
	{
		node = cJSON_CreateObject();
		cJSON_AddStringToObject(node, "SSID_NAME", item->valuestring);
		cJSON_AddStringToObject(node, "SSID_PWD", val);
		cJSON_InsertItemInArray(ssid_arr,0,node);
		int size;
		if((size=cJSON_GetArraySize(ssid_arr))>3)
		{
			cJSON_DeleteItemFromArray(ssid_arr,size-1);
		}
	}
	API_Para_Write_PublicByName(WIFI_SSID_PARA,ssid_arr);
	cJSON_Delete(ssid_arr);

    IX850S_StartConnectPhoneProcess();
}

static void draw_table_event_cb(lv_event_t* e)
{
    lv_obj_t* obj = lv_event_get_target(e);
    lv_obj_draw_part_dsc_t* dsc = lv_event_get_draw_part_dsc(e);
    WIFI_VIEW* rc = lv_event_get_user_data(e);
    int edit = 1;

    if (dsc->part == LV_PART_ITEMS) {
        uint32_t row = dsc->id / lv_table_get_col_cnt(obj);
        uint32_t col = dsc->id - row * lv_table_get_col_cnt(obj);

        dsc->label_dsc->font = vtk_get_font_by_size(0);
        //dsc->label_dsc->font = (col == 1) ? vtk_get_font_by_size(2) : vtk_get_font_by_size(0);
        dsc->label_dsc->align = (col == 1) ? LV_TEXT_ALIGN_RIGHT : LV_TEXT_ALIGN_LEFT;
        dsc->label_dsc->color = (edit ? TB_VIEW_TextColor:TB_VIEW_NotEditTextColor);
        dsc->rect_dsc->bg_color = TB_VIEW_BackgroundColor;
        dsc->rect_dsc->border_width = 3;
        dsc->rect_dsc->border_color = lv_color_make(55,55,55);
        dsc->rect_dsc->border_side = LV_BORDER_SIDE_BOTTOM;
    }
}

static void table_edit_event_cb(lv_event_t* e)
{
    WIFI_VIEW* disp = lv_event_get_user_data(e);
    lv_obj_t* obj = lv_event_get_target(e);

    uint16_t col;
    uint16_t row;
    lv_table_get_selected_cell(obj, &row, &col);    
    uint16_t cnt = lv_table_get_row_cnt(obj);
    if (row >= cnt)
    {
        return;
    }
    disp->cnt_row = row;
    char* name = lv_table_get_cell_value(obj,row,0);

    Vtk_KbMode1Init(&disp->kb,Vtk_KbMode1, API_Para_Read_String2(KeybordLanguage),name,30,NULL,NULL, "Please enter Wifi password",text_edit_event_cb,disp);
}

static void Wifi_UpdateDate(WIFI_VIEW* disp, const cJSON* view)
{
    cJSON* element;
    int col;

    if(!disp)
    {
        return;
    }

    cJSON_Delete(disp->view);
    disp->view = cJSON_Duplicate(view,true);
    
    if(!disp->table){
        disp->table = lv_table_create(disp->menu);
        lv_obj_add_event_cb(disp->table, draw_table_event_cb, LV_EVENT_DRAW_PART_BEGIN, disp);
        lv_obj_add_event_cb(disp->table, table_edit_event_cb, LV_EVENT_VALUE_CHANGED, disp);
        lv_table_set_row_height(disp->table,TB_VIEW_LineHeight);
        //lv_obj_set_scroll_dir(disp->table,LV_DIR_HOR);
        lv_obj_set_style_border_width(disp->table, 0, 0);
        lv_obj_set_style_bg_color(disp->table, TB_VIEW_BackgroundColor, 0);
        if(!disp->value)
            lv_obj_add_flag(disp->table,LV_OBJ_FLAG_HIDDEN);       
    }

    lv_obj_set_size(disp->table, TB_VIEW_ScrWidth, 800 - TB_VIEW_TitleHeight*3);
    lv_obj_set_y(disp->table, TB_VIEW_TitleHeight*3);
    
    lv_table_set_row_cnt(disp->table, 0);
    lv_table_set_col_cnt(disp->table, 0);

    lv_table_set_col_width(disp->table, 0, 480);
    //lv_table_set_col_width(disp->table, 1, 80);

    int rec_size = cJSON_GetArraySize(view);

    for (int i = 0; i < rec_size; i++)
    {
        cJSON* item = cJSON_GetArrayItem(view, i);
        if (item)
        {
            lv_table_set_cell_value(disp->table, i, 0, item->valuestring);
        }
    }
}

void Wifi_MenuDisplay(WIFI_VIEW** rcDisp, const cJSON* view,const char* title)
{
    WIFI_VIEW* rc = NULL;
    if(!rcDisp)
    {
        return;
    }

    vtk_lvgl_lock();

    if(*rcDisp)
    {
        Wifi_MenuDelete(rcDisp);
    }

    rc = (WIFI_VIEW*)lv_mem_alloc(sizeof(WIFI_VIEW));
    lv_memset_00(rc, sizeof(WIFI_VIEW));
    *rcDisp = rc;

	rc->back_scr = lv_scr_act();
	rc->scr = ui_wifi_init();
    rc->view = cJSON_Duplicate(view,true);
    rc->msg0 = lv_msg_subsribe(MSG_WLANCONFIG_RESULT,lv_msg_wifi_handler,NULL);
    rc->msg1 = lv_msg_subsribe(MSG_WLAN_RESULT,lv_msg_wifi_handler,NULL);
    rc->value = API_Para_Read_Int("WIFI_SWITCH");

    rc->menu = lv_obj_create(rc->scr);
    lv_obj_set_size(rc->menu, 490, 810);
    lv_obj_align(rc->menu, LV_ALIGN_TOP_MID, 3, -3);
    lv_obj_set_style_pad_all(rc->menu, 0, 0);
    lv_obj_clear_flag(rc->menu, LV_OBJ_FLAG_SCROLLABLE);
    lv_obj_set_style_bg_color(rc->menu, TB_VIEW_BackgroundColor, 0);

    Wifi_AddTitle(rcDisp, title);
    Wifi_AddWifiName(rc,"Currently connected SSID:");
    Wifi_AddSwitch(rc,"Wifi Enable" ,rc->value);

    Wifi_UpdateDate(rc,view);   
    
    if(rc->scr != lv_scr_act())
    {
        rc->back_scr = lv_scr_act();
        lv_disp_load_scr(rc->scr);
    }
    vtk_lvgl_unlock();
}

/*****************
 * 删除record编辑页面
 * **************/
void Wifi_MenuDelete(WIFI_VIEW** rc)
{
    WIFI_VIEW* disp = NULL;
    if(!rc)
    {
        return;
    }
    vtk_lvgl_lock();
    if(*rc)
    {
        disp = *rc;
        *rc = NULL;
        if(disp->kb)
        {
            Vtk_KbReleaseByMode(&disp->kb);
            disp->kb = NULL;
        } 
        if(disp->scr == lv_scr_act())
        {
            lv_scr_load(disp->back_scr);
        }     
        if (disp->scr)
        {
            lv_obj_del(disp->scr);
            disp->scr = NULL;
        }
        if (disp->view)
        {
            cJSON_Delete(disp->view);
            disp->view= NULL;
        }
        if(disp->msg0){
            lv_msg_unsubscribe(disp->msg0);
            disp->msg0 = NULL;
        }
        if(disp->msg1){
            lv_msg_unsubscribe(disp->msg1);
            disp->msg1 = NULL;
        }
        lv_mem_free(disp);
    }
    vtk_lvgl_unlock();
}


static WIFI_VIEW* wv = NULL;

static void lv_msg_wifi_handler(void* s, lv_msg_t* m)
{
    LV_UNUSED(s);
    cJSON *wlan_result;
    lv_obj_t* label;
    char* ssid;
    switch (lv_msg_get_id(m))
    {
        case MSG_WLANCONFIG_RESULT:
            wlan_result = (cJSON*)lv_msg_get_payload(m);
            Wifi_UpdateDate(wv,wlan_result);
        break;
        case MSG_WLAN_RESULT:
            ssid = lv_msg_get_payload(m);
            label = lv_obj_get_child(wv->curLabel,1);
            lv_label_set_text(label, ssid);
        break;
    }
}

void Menu_Wifi_Init(void)
{  
    Wifi_MenuDisplay(&wv,NULL,"Wifi");
}

void Menu_Wifi_Close(void)
{  
    Wifi_MenuDelete(&wv);
}

int Menu_Wifi_Shell_Test(cJSON *cmd)
{
	char* jsonstr = GetShellCmdInputString(cmd, 1);

	ShellCmdPrintf("%s:",jsonstr);

	if(strcmp(jsonstr,"load")==0)
	{
        Menu_Wifi_Init();
	}
    else if(strcmp(jsonstr,"close")==0)
	{
        Menu_Wifi_Close();
	}
    
}