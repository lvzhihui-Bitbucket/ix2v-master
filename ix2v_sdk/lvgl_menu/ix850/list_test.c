#include "lv_ix850.h"
#include "menu_common.h"
#include "obj_lvgl_msg.h"

static bool is_sliding = false;
int act_page = 0;
extern const lv_font_t* font_small;
extern const lv_font_t* font_medium;
extern const lv_font_t* font_larger;

LV_IMG_DECLARE(bg_test);
lv_obj_t* add_to_list(lv_obj_t* parent,int cnt);
static void CallList_click_event(lv_event_t* e);
static void back_click_event(lv_event_t* e);
static void list_scroll_event(lv_event_t* e);
static void left_click_event(lv_event_t* e);
static void right_click_event(lv_event_t* e);
static void list_timer_cb(lv_timer_t* timer);

lv_obj_t* enter_list(lv_obj_t* parent)
{
    lv_obj_t* bg = lv_obj_create(parent);
    lv_obj_set_size(bg, 480, 800);
    lv_obj_set_style_pad_all(bg, 0, 0);
    lv_obj_clear_flag(bg, LV_OBJ_FLAG_SCROLLABLE);
   
    //add_header(parent);

    lv_obj_t* list = lv_obj_create(bg);
    lv_obj_set_size(list, 480, 600);
    lv_obj_set_y(list,50); 
    lv_obj_set_flex_flow(list, LV_FLEX_FLOW_COLUMN);
    //lv_obj_clear_flag(list, LV_OBJ_FLAG_SCROLL_MOMENTUM);
    lv_obj_set_scroll_snap_y(list, LV_SCROLL_SNAP_NONE);
    //lv_obj_clear_flag(list, LV_OBJ_FLAG_SNAPPABLE);
    //lv_obj_clear_flag(list, LV_OBJ_FLAG_SCROLLABLE);
    //lv_obj_set_style_bg_opa(list, LV_OPA_50, 0);
    //lv_obj_set_style_pad_all(list, 0, 0);
    //lv_obj_set_style_pad_column(list, 0, 0);
    lv_obj_set_style_pad_row(list, 0, 0);
    //lv_obj_set_style_border_width(list,3,0);
    //lv_obj_set_style_bg_img_src(list, "S:/mnt/nand1-1/App/res/bg.png", 0);
    //lv_obj_set_style_bg_img_src(list, &bg_test, 0);

    lv_obj_add_event_cb(list, list_scroll_event, LV_EVENT_ALL, NULL);

    for (int i = 0; i < 18; i++)
    {
        add_to_list(list,i);
    }
#if 0
    lv_obj_t* btn_obj = lv_obj_create(bg);
    //lv_obj_set_style_bg_opa(btn_obj,LV_OPA_50,0);
    lv_obj_set_size(btn_obj, 480, 150);
    lv_obj_set_y(btn_obj,650);
    lv_obj_set_style_pad_all(btn_obj, 0, 0);

    lv_obj_t* saveBtn = lv_btn_create(btn_obj);
    lv_obj_set_size(saveBtn, 120, 70);
    lv_obj_set_style_text_font(saveBtn, &lv_font_montserrat_32, 0);
    lv_obj_add_event_cb(saveBtn, back_click_event, LV_EVENT_CLICKED, NULL);
    lv_obj_align(saveBtn, LV_ALIGN_LEFT_MID, 20, 0);
    lv_obj_set_style_radius(saveBtn, LV_RADIUS_CIRCLE, 0);
    lv_obj_set_style_bg_color(saveBtn, lv_color_make(230, 231, 232), 0);
    lv_obj_set_style_text_color(saveBtn, lv_color_hex(0xff0000), 0);
    lv_obj_t* savelabel = lv_label_create(saveBtn);
    lv_label_set_text(savelabel, LV_SYMBOL_BACKSPACE);
    lv_obj_center(savelabel);

    lv_obj_t* delBtn = lv_btn_create(btn_obj);
    lv_obj_align(delBtn, LV_ALIGN_CENTER, 0, 0);
    lv_obj_set_style_text_color(delBtn, lv_color_hex(0xff0000), 0);
    lv_obj_set_style_bg_color(delBtn, lv_color_make(230, 231, 232), 0);
    lv_obj_set_size(delBtn, 120, 70);
    lv_obj_set_style_text_font(delBtn, &lv_font_montserrat_32, 0);
    lv_obj_add_event_cb(delBtn, left_click_event, LV_EVENT_CLICKED, list);
    lv_obj_set_style_radius(delBtn, LV_RADIUS_CIRCLE, 0);
    lv_obj_t* dellabel = lv_label_create(delBtn);
    lv_label_set_text(dellabel, LV_SYMBOL_LEFT);
    lv_obj_center(dellabel);

    lv_obj_t* addBtn = lv_btn_create(btn_obj);
    lv_obj_align(addBtn, LV_ALIGN_RIGHT_MID, -20, 0);
    lv_obj_set_style_text_color(addBtn, lv_color_hex(0xff0000), 0);
    lv_obj_set_style_bg_color(addBtn, lv_color_make(230, 231, 232), 0);
    lv_obj_set_size(addBtn, 120, 70);
    lv_obj_set_style_text_font(addBtn, &lv_font_montserrat_32, 0);
    lv_obj_add_event_cb(addBtn, right_click_event, LV_EVENT_CLICKED, list);
    lv_obj_set_style_radius(addBtn, LV_RADIUS_CIRCLE, 0);
    lv_obj_t* addlabel = lv_label_create(addBtn);
    lv_label_set_text(addlabel, LV_SYMBOL_RIGHT);
    lv_obj_center(addlabel);
#endif
return bg;
    //return list;
}

static void left_click_event(lv_event_t* e)
{
    lv_obj_t* list = lv_event_get_user_data(e);
    int cnt = lv_obj_get_child_cnt(list);
    
    act_page -= 5;
    if(act_page < 0)
    {
        act_page += 5;
        return;
    }
    //printf("left_click_event      -------------------------    act_page = [%d]",act_page);

    lv_obj_t* item = lv_obj_get_child(list,act_page);
    lv_obj_scroll_to_view(item,LV_ANIM_OFF);  
}

static void right_click_event(lv_event_t* e)
{
    lv_obj_t* list = lv_event_get_user_data(e);
    int cnt = lv_obj_get_child_cnt(list);
    act_page += 5;

    if(act_page > cnt)
    {
        act_page -= 5;
        return;
    }
    //printf("right_click_event      -------------------------    act_page = [%d]",act_page);
    lv_obj_t* item = lv_obj_get_child(list,act_page);
    lv_obj_scroll_to_view(item,LV_ANIM_OFF);  
}

static void list_scroll_event(lv_event_t* e)
{
    lv_event_code_t code = lv_event_get_code(e);
    lv_obj_t* obj = lv_event_get_target(e);
   if(code == LV_EVENT_SCROLL_BEGIN)
    {
        //printf("list_scroll_event   00000000000000000000000000000\n");
        is_sliding = true;
    }else if(code == LV_EVENT_SCROLL_END){
        
        is_sliding = false;
        lv_timer_t* timer = lv_timer_create(list_timer_cb, 500, obj);
        int cnt = lv_obj_get_child_cnt(obj);
        for (size_t i = 0; i < cnt; i++)
        {
            lv_obj_t* child = lv_obj_get_child(obj, i);
            if(child != NULL)       
                lv_obj_add_state(child,LV_STATE_DISABLED);
        }
        //printf("list_scroll_event   11111111111111111111111111111\n");
    }
}

static void list_timer_cb(lv_timer_t* timer)
{
    lv_obj_t* obj = (lv_obj_t*)timer->user_data;
    int cnt = lv_obj_get_child_cnt(obj);
    for (size_t i = 0; i < cnt; i++)
    {
        lv_obj_t* child = lv_obj_get_child(obj, i);
        if(child != NULL)       
            lv_obj_clear_state(child,LV_STATE_DISABLED);
    }
    //printf("list_timer_cb   11111111111111111111111111111\n");  
    lv_timer_del(timer);
}

lv_obj_t* add_to_list(lv_obj_t* parent,int cnt)
{
    lv_obj_t* call_obj = lv_obj_create(parent);
    lv_obj_set_size(call_obj, LV_PCT(100), 120);
    lv_obj_set_style_border_side(call_obj, LV_BORDER_SIDE_BOTTOM, 0);
    lv_obj_set_style_border_width(call_obj, 5, 0);
    //lv_obj_set_style_bg_opa(call_obj, LV_OPA_50, 0);
    lv_obj_set_style_radius(call_obj, 0, 0);
    lv_obj_clear_flag(call_obj, LV_OBJ_FLAG_SCROLLABLE);
    lv_obj_add_flag(call_obj, LV_OBJ_FLAG_CLICKABLE);
    //lv_obj_clear_flag(call_obj, LV_OBJ_FLAG_SNAPPABLE);
    //lv_obj_add_flag(call_obj, LV_OBJ_FLAG_EVENT_BUBBLE);
    //lv_obj_set_style_bg_color(call_obj, lv_palette_main(LV_PALETTE_GREY) , LV_STATE_PRESSED);

    //lv_obj_set_flex_flow(call_obj, LV_FLEX_FLOW_ROW);

    char* buf = lv_mem_alloc(50);
    sprintf(buf,"%s                    009900%02d01", LV_SYMBOL_AUDIO,cnt);
    
    lv_obj_add_event_cb(call_obj, CallList_click_event, LV_EVENT_ALL, buf);

    lv_obj_t* type_text = NULL;
    lv_obj_t* name_text = NULL;
    lv_obj_t* num_text = NULL;
    lv_obj_t* call_text = NULL;

    //type_text = lv_label_create(call_obj);
    //lv_label_set_text(type_text, LV_SYMBOL_AUDIO);
    //lv_obj_set_style_text_font(type_text, font_larger, 0);
    //lv_obj_set_width(type_text, 100);      

    name_text = lv_label_create(call_obj);
    lv_label_set_text(name_text,buf);
    lv_obj_set_style_text_font(name_text, font_larger, 0);
    lv_obj_set_style_text_color(name_text,lv_palette_main(LV_PALETTE_BLUE),0);
    //call_text = lv_label_create(call_obj);
    //lv_label_set_text(call_text, LV_SYMBOL_BELL);
    //lv_obj_set_style_text_font(call_text, font_larger, 0);

    lv_obj_align(name_text, LV_ALIGN_LEFT_MID, 10, 0);
    //lv_obj_align(name_text, LV_ALIGN_CENTER, 0, 0);
    //lv_obj_align(call_text, LV_ALIGN_RIGHT_MID, -10, 0);

    return call_obj;
}

static lv_obj_t* target_buf=NULL;
static void CallList_click_event(lv_event_t* e)
{
    lv_event_code_t code = lv_event_get_code(e);
    char* num = lv_event_get_user_data(e);

    lv_obj_t* target = lv_event_get_target(e);

    if(code == LV_EVENT_SCROLL_BEGIN)
    {
        //printf("CallList_click_event   00000000000000000000000000000\n");
        is_sliding = true;
    }else if (code == LV_EVENT_RELEASED) {
        //printf("CallList_click_event   1111111111111111111111111111111\n");
        is_sliding = false;

    } else if (code == LV_EVENT_CLICKED && (!is_sliding)) {
        //printf("CallList_click_event   222222222222222222222222222222222\n");
        //cJSON* number = lv_event_get_user_data(e);
        #if 1
        cJSON* object = cJSON_CreateObject();

        cJSON_AddStringToObject(object, "KayPad", num);
        if( target != target_buf )
        {
lv_msg_send(IX850_MSG_CALL, object);
            target_buf = target;
        }
        cJSON_Delete(object);
        #endif
    }
        
}

static void back_click_event(lv_event_t* e)
{
    //scr_main();
}

#if 1
 lv_timer_t* scroll_timer=NULL;
void list_timer_cb2(lv_timer_t* timer);
void list_scroll_event2(lv_event_t* e)
{
    lv_event_code_t code = lv_event_get_code(e);
    lv_obj_t* obj = lv_event_get_target(e);
   if(code == LV_EVENT_SCROLL_BEGIN)
    {
       // printf("list_scroll_event   00000000000000000000000000000\n");
        is_sliding = true;
		
    }else if(code == LV_EVENT_SCROLL_END){
        
        is_sliding = false;
		if(scroll_timer==NULL)
        		scroll_timer = lv_timer_create(list_timer_cb2, 500, obj);
		else
			lv_timer_set_period(scroll_timer,500);
        int cnt = lv_obj_get_child_cnt(obj);
        for (size_t i = 0; i < cnt; i++)
        {
            lv_obj_t* child = lv_obj_get_child(obj, i);
            if(child != NULL)       
                lv_obj_add_state(child,LV_STATE_DISABLED);
        }
       // printf("list_scroll_event   11111111111111111111111111111\n");
    }
}

void list_timer_cb2(lv_timer_t* timer)
{
    lv_obj_t* obj = (lv_obj_t*)timer->user_data;
    int cnt = lv_obj_get_child_cnt(obj);
    for (size_t i = 0; i < cnt; i++)
    {
        lv_obj_t* child = lv_obj_get_child(obj, i);
        if(child != NULL)       
            lv_obj_clear_state(child,LV_STATE_DISABLED);
    }
   // printf("list_timer_cb   11111111111111111111111111111\n");  
    lv_timer_del(timer);
	scroll_timer=NULL;
}
int get_is_sliding(void)
{
	return ((int)is_sliding);
}
#endif