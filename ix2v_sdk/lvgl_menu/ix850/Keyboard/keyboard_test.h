#if 0
#ifndef KEYBOARD_TEST_H_
#define KEYBOARD_TEST_H_

#include "lvgl.h"
#include "cJSON.h"


lv_obj_t* langauge_test_menu_init(void);
static void tes1_event(lv_event_t* e);
static void tes2_event(lv_event_t* e);
static void tes3_event(lv_event_t* e);
static void tes4_event(lv_event_t* e);
static void tes5_event(lv_event_t* e);
static lv_obj_t* test_scr_init(void);
#endif
#endif