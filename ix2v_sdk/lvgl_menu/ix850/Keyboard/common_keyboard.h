#ifndef COMMON_KEYBOARD_H_
#define COMMON_KEYBOARD_H_

#include "lvgl.h"
#include "cJSON.h"
#include "KeyboardResource.h"


lv_obj_t* langauge_menu_init(VTK_KB_S* vtk_kb);
void load_keyboard(VTK_KB_S* vtk_kb,int page);
static void choose_keyboard_language(VTK_KB_S* vtk_kb);

#endif