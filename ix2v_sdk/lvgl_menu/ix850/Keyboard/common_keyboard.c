#include "common_keyboard.h"
#include "menu_utility.h"
#include "menu_common.h"
#include "obj_CommonSelectMenu.h"
#include "define_file.h"
#include "obj_IoInterface.h"

extern lv_font_t* font_small;
extern lv_font_t* font_medium;
extern lv_font_t* font_larger;

static const char* vtk_key_map_spec[] = {"1","2", "3", "_", LV_SYMBOL_BACKSPACE, "\n",
                                "4", "5", "6", ".", "1#", "\n",
                                "7", "8", "9", "/", "abc", "\n",
                                "*", "0", "#", "'\'", LV_SYMBOL_LEFT,"\n",
                                "+", "-", "=", "|", LV_SYMBOL_RIGHT, "\n",
                                "[", "]", ":", ",", LV_VTK_INTERNET, "\n",
                                "%", "&", "~", "?", LV_SYMBOL_KEYBOARD, "\n",
                                "&","@","Space", LV_SYMBOL_OK, ""
};
static const char* vtk_key_map_lc[] = {"a","b", "c", "d", LV_SYMBOL_BACKSPACE, "\n",
                                "e", "f", "g", "h", "1#", "\n",
                                "i", "j", "k", "l", "ABC", "\n",
                                "m", "n", "o", "p", LV_SYMBOL_LEFT,"\n",
                                "q", "r", "s", "t", LV_SYMBOL_RIGHT, "\n",
                                "u", "v", "w", "x", LV_VTK_INTERNET, "\n",
                                "y", "z", " ", " ", LV_SYMBOL_KEYBOARD, "\n",
                                " "," ","Space", LV_SYMBOL_OK, ""
};
static const char* vtk_key_map_uc[] = { "A","B", "C", "D", LV_SYMBOL_BACKSPACE, "\n",
                                "E", "F", "G", "H", "1#", "\n",
                                "I", "J", "K", "L", "abc", "\n",
                                "M", "N", "O", "P", LV_SYMBOL_LEFT,"\n",
                                "Q", "R", "S", "T", LV_SYMBOL_RIGHT, "\n",
                                "U", "V", "W", "X", LV_VTK_INTERNET, "\n",
                                "Y", "Z", " ", " ", LV_SYMBOL_KEYBOARD, "\n",
                                " "," ","Space", LV_SYMBOL_OK, ""
};

static const char* vtk_key_map_num[] = { "1","2", "3", "\n",
                                "4", "5", "6",  "\n",
                                "7", "8", "9", "\n",
                                ".", ",",LV_SYMBOL_KEYBOARD, "\n",
                                LV_SYMBOL_BACKSPACE,"0", LV_SYMBOL_OK, ""
};

static const lv_btnmatrix_ctrl_t vtk_key_ctrl_uc[] = {  1, 1, 1, 1, 1, 
                                                        1, 1, 1, 1, 1,
                                                        1, 1, 1, 1, 1,
                                                        1, 1, 1, 1, 1,
                                                        1, 1, 1, 1, 1,
                                                        1, 1, 1, 1, 1,
                                                        1, 1, 1, 1, 1,
                                                        1, 1, 2, 1
};

static const lv_btnmatrix_ctrl_t vtk_key_ctrl_num[] = { 1, 1, 1, 
                                                        1, 1, 1,
                                                        1, 1, 1,
                                                        1, 1, 1, 
                                                        1, 1, 1
};

static const char * * kb_map[9] = {
    (const char * *)vtk_key_map_lc,
    (const char * *)vtk_key_map_uc,
    (const char * *)vtk_key_map_spec,
    (const char * *)vtk_key_map_lc,
    (const char * *)vtk_key_map_lc,
    (const char * *)vtk_key_map_lc,
    (const char * *)vtk_key_map_lc,
    (const char * *)vtk_key_map_lc,
    (const char * *)vtk_key_map_lc,
};
static const lv_btnmatrix_ctrl_t * kb_ctrl[9] = {
    vtk_key_ctrl_uc,
    vtk_key_ctrl_uc,
    vtk_key_ctrl_uc,
    vtk_key_ctrl_uc,
    vtk_key_ctrl_uc,
    vtk_key_ctrl_uc,
    vtk_key_ctrl_uc,
    vtk_key_ctrl_uc,
    vtk_key_ctrl_uc,
};

static lv_obj_t* keyboard_scr_init(void)
{
    lv_obj_t* ui_keyboard = lv_obj_create(NULL);
    lv_obj_clear_flag(ui_keyboard, LV_OBJ_FLAG_SCROLLABLE);
    lv_obj_remove_style_all(ui_keyboard);
    lv_obj_set_size(ui_keyboard, lv_disp_get_hor_res(NULL), lv_disp_get_ver_res(NULL));
    lv_obj_align(ui_keyboard, LV_ALIGN_TOP_LEFT, 0, 0);
    lv_obj_set_style_bg_color(ui_keyboard, lv_palette_main(LV_PALETTE_GREY), 0);
    lv_obj_set_style_bg_opa(ui_keyboard, LV_OPA_100, 0);   
    return ui_keyboard;
}

int Vtk_KbMode1Rule(VTK_KB_S* vtk_kb)
{
	return 0;
}

void Vtk_KbMode1Release(VTK_KB_S* vtk_kb)
{
    if (vtk_kb)
    {          
        lv_disp_load_scr(vtk_kb->kb_return);
        if (vtk_kb->kb_scr)
        {
            lv_obj_del(vtk_kb->kb_scr);
            vtk_kb->kb_scr = NULL;
        }
        if (vtk_kb->key)
        {
            cJSON_Delete(vtk_kb->key);
            vtk_kb->key = NULL;
        }
        if (vtk_kb->language)
        {
            lv_mem_free(vtk_kb->language);
            vtk_kb->language = NULL;
        }         
        lv_mem_free(vtk_kb);
        vtk_kb = NULL;
    }
}

void load_keyboard(VTK_KB_S* vtk_kb,int page)
{
    if(vtk_kb->key == NULL)
        return;
    uint8_t count = cJSON_GetArraySize(vtk_kb->key);
    int max = count % 30;
    if (max == 0)
    {
        max = count / 30;
    }
    else {
        max = count / 30 + 1;
    }
    vtk_kb->max_page = max;

    int cnt = 0;
    for (int i = 0; i < 30; i++) {
        cJSON* key = cJSON_GetArrayItem(vtk_kb->key, (page-1)*30+i);
        if(key == NULL){
            vtk_key_map_lc[cnt] = " ";
            vtk_key_map_uc[cnt] = " ";
        }else{
            cJSON* small = cJSON_GetObjectItem(key, "small");
            if (small)
            {
                vtk_key_map_lc[cnt] = small->valuestring;                 
            }
            cJSON* cap = cJSON_GetObjectItem(key, "cap");
            if (cap)
            {
                vtk_key_map_uc[cnt] = cap->valuestring;
            }
        }                
        cnt++;
        if((i+1)%4 == 0){
            cnt += 2;
        }         
    }
}

static void lv_keyboard_update_ctrl_map(lv_obj_t * obj)
{
    lv_keyboard_t * keyboard = (lv_keyboard_t *)obj;

    if(keyboard->popovers) {
        /*Apply the current control map (already includes LV_BTNMATRIX_CTRL_POPOVER flags)*/
        lv_btnmatrix_set_ctrl_map(obj, kb_ctrl[keyboard->mode]);
    }
    else {

        /*Make a copy of the current control map*/
        lv_btnmatrix_t * btnm = (lv_btnmatrix_t *)obj;
        lv_btnmatrix_ctrl_t * ctrl_map = lv_mem_alloc(btnm->btn_cnt * sizeof(lv_btnmatrix_ctrl_t));
        lv_memcpy(ctrl_map, kb_ctrl[keyboard->mode], sizeof(lv_btnmatrix_ctrl_t) * btnm->btn_cnt);

        /*Remove all LV_BTNMATRIX_CTRL_POPOVER flags*/
        for(uint16_t i = 0; i < btnm->btn_cnt; i++) {
            ctrl_map[i] &= (~LV_BTNMATRIX_CTRL_POPOVER);
        }
        /*Apply new control map and clean up*/
        lv_btnmatrix_set_ctrl_map(obj, ctrl_map);
        lv_mem_free(ctrl_map);
    }
}

void keyboard_clear_sel(VTK_KB_S* vtk_kb, int inputFlag)
{
    if(vtk_kb == NULL)
        return;
    if(vtk_kb->sel)
    {
        vtk_kb->sel = 0;
        if(inputFlag)
        {
            lv_textarea_set_text(vtk_kb->textarea,"");
        }
        lv_obj_t* lab = lv_textarea_get_label(vtk_kb->textarea);
        lv_obj_set_style_bg_color(lab, lv_color_white(), LV_PART_SELECTED);
    }
}

void vtk_keyboard_def_event_cb(lv_event_t * e)
{
    VTK_KB_S** vtk_kb_s = lv_event_get_user_data(e);

    VTK_KB_S* vtk_kb;

    vtk_kb = *vtk_kb_s;

	VTKKBModeCallback_S *kb_callbak;
	if(vtk_kb)
	{
		kb_callbak=GetVTKKBModeCallbackByMode(vtk_kb->mode_id);
		if(kb_callbak)
		{
			if(kb_callbak->rule)
			 	if((*kb_callbak->rule)(vtk_kb))
					return;
		}
	}
    

    lv_obj_t * obj = lv_event_get_target(e);

    lv_keyboard_t * keyboard = (lv_keyboard_t *)obj;
    uint16_t btn_id   = lv_btnmatrix_get_selected_btn(obj);
    if(btn_id == LV_BTNMATRIX_BTN_NONE) return;

    const char * txt = lv_btnmatrix_get_btn_text(obj, lv_btnmatrix_get_selected_btn(obj));
    if(txt == NULL) return;

    if(strcmp(txt, "abc") == 0) {
        keyboard->mode = LV_KEYBOARD_MODE_TEXT_LOWER;
        lv_btnmatrix_set_map(obj, kb_map[LV_KEYBOARD_MODE_TEXT_LOWER]);
        lv_keyboard_update_ctrl_map(obj);
        return;
    }
    else if(strcmp(txt, "ABC") == 0) {
        keyboard->mode = LV_KEYBOARD_MODE_TEXT_UPPER;
        lv_btnmatrix_set_map(obj, kb_map[LV_KEYBOARD_MODE_TEXT_UPPER]);
        lv_keyboard_update_ctrl_map(obj);
        return;
    }
    else if(strcmp(txt, "1#") == 0) {
        keyboard->mode = LV_KEYBOARD_MODE_SPECIAL;
        lv_btnmatrix_set_map(obj, kb_map[LV_KEYBOARD_MODE_SPECIAL]);
        lv_keyboard_update_ctrl_map(obj);
        return;
    }
    else if(strcmp(txt, LV_SYMBOL_CLOSE) == 0 || strcmp(txt, LV_SYMBOL_KEYBOARD) == 0) {
        
        Vtk_KbReleaseByMode(vtk_kb_s);
        return;
    }
    else if(strcmp(txt, LV_SYMBOL_OK) == 0) {
        if(keyboard->ta) {
            char* text = lv_textarea_get_text(keyboard->ta);
            if(RegexCheck(vtk_kb->regex_pattern, text))
            {
		        if(vtk_kb->InputOK)
                {
                	vtk_kb->InputOK(text,vtk_kb->user_data);
                }
                //printf("vtk_keyboard_def_event_cb 11111111111 \n");	
                Vtk_KbReleaseByMode(vtk_kb_s);
            }else
            {
                LV_API_TIPS("Incorrect, please re-enter", 4);
                return;
            }          
        }   
        return;
    }

    /*Add the characters to the text area if set*/
    if(keyboard->ta == NULL) return;

    if(strcmp(txt, "Enter") == 0 || strcmp(txt, LV_SYMBOL_NEW_LINE) == 0) {
        keyboard_clear_sel(vtk_kb, 1);
        lv_textarea_add_char(keyboard->ta, '\n');
        if(lv_textarea_get_one_line(keyboard->ta)) {
            lv_res_t res = lv_event_send(keyboard->ta, LV_EVENT_READY, NULL);
            if(res != LV_RES_OK) return;
        }
    }
    else if(strcmp(txt, LV_SYMBOL_LEFT) == 0) {    
        vtk_kb->cnt_page--;
        if(vtk_kb->cnt_page <= 0)
            vtk_kb->cnt_page = vtk_kb->max_page;
        load_keyboard(vtk_kb,vtk_kb->cnt_page);
    }
    else if(strcmp(txt, LV_SYMBOL_RIGHT) == 0) {
        vtk_kb->cnt_page++;
        if(vtk_kb->cnt_page > vtk_kb->max_page)
            vtk_kb->cnt_page = 1;
        load_keyboard(vtk_kb,vtk_kb->cnt_page);
    }
    else if(strcmp(txt, LV_SYMBOL_BACKSPACE) == 0) {
        keyboard_clear_sel(vtk_kb, 1);
        lv_textarea_del_char(keyboard->ta);
    }
    else if(strcmp(txt, "+/-") == 0) {
        uint16_t cur  = lv_textarea_get_cursor_pos(keyboard->ta);
        const char * ta_txt = lv_textarea_get_text(keyboard->ta);
        if(ta_txt[0] == '-') {
            lv_textarea_set_cursor_pos(keyboard->ta, 1);
            lv_textarea_del_char(keyboard->ta);
            lv_textarea_add_char(keyboard->ta, '+');
            lv_textarea_set_cursor_pos(keyboard->ta, cur);
        }
        else if(ta_txt[0] == '+') {
            lv_textarea_set_cursor_pos(keyboard->ta, 1);
            lv_textarea_del_char(keyboard->ta);
            lv_textarea_add_char(keyboard->ta, '-');
            lv_textarea_set_cursor_pos(keyboard->ta, cur);
        }
        else {
            lv_textarea_set_cursor_pos(keyboard->ta, 0);
            lv_textarea_add_char(keyboard->ta, '-');
            lv_textarea_set_cursor_pos(keyboard->ta, cur + 1);
        }
    }
    else if(strcmp(txt, LV_VTK_INTERNET) == 0) {
        choose_keyboard_language(vtk_kb);
    }
    else if(strcmp(txt, " ") == 0) {
        ;
    }
    else if(strcmp(txt, "Space") == 0) {
        keyboard_clear_sel(vtk_kb, 1);
        lv_textarea_add_text(keyboard->ta," ");
    }
    else {
        keyboard_clear_sel(vtk_kb, 1);
        lv_textarea_add_text(keyboard->ta, txt);
    }
}

static void KeyboardEditClick(lv_event_t* e)
{
    VTK_KB_S* vtk_kb = lv_event_get_user_data(e);
    keyboard_clear_sel(vtk_kb, 0);
}

VTK_KB_S *Vtk_KbMode1Init(VTK_KB_S** vtk_kb_s,int mode,void *mode_para,char *title_str,int limit_len,char *regex_pattern,char *init_str,char *prompt_str,InputOKCallback *inputok,void *user_data)
{
    if(mode != Vtk_KbMode1)
        return NULL;
    VTK_KB_S* vtk_kb = NULL;
    if(!vtk_kb_s)
        return;
    if(*vtk_kb_s)
    {
        Vtk_KbReleaseByMode(vtk_kb_s);
    }
   
	vtk_kb = (VTK_KB_S*)lv_mem_alloc(sizeof(VTK_KB_S));  
    *vtk_kb_s = vtk_kb;
    lv_memset_00(vtk_kb, sizeof(VTK_KB_S));
    vtk_kb->language = lv_mem_alloc(50);

    vtk_kb->kb_return = lv_scr_act();
    vtk_kb->kb_scr = keyboard_scr_init();
    vtk_kb->mode_id = mode;
	vtk_kb->user_data = user_data;
    if(regex_pattern != NULL)
    {
        snprintf(vtk_kb->regex_pattern, 200, "%s", regex_pattern);
    }
    vtk_kb->InputOK = inputok;

    lv_obj_t* keyMenu = lv_obj_create(vtk_kb->kb_scr);
    lv_obj_set_size(keyMenu, 480, 800);
    lv_obj_clear_flag(keyMenu, LV_OBJ_FLAG_SCROLLABLE);

    lv_obj_t* name_label = lv_label_create(keyMenu);
    lv_label_set_text(name_label, title_str);
    lv_obj_set_size(name_label, LV_PCT(100), 60);
    lv_obj_set_style_text_font(name_label, &lv_font_montserrat_32, 0);
    //set_font_size(name_label,1);

    lv_obj_t* ta = lv_textarea_create(keyMenu);
    lv_textarea_set_max_length(ta,limit_len);
    //lv_obj_set_style_text_font(ta, font_larger, 0);
    set_font_size(ta,1);
    lv_obj_add_state(ta, LV_STATE_FOCUSED);
    lv_textarea_set_text_selection(ta,true);   

    if(prompt_str){
        char tempString[200];
        lv_textarea_set_placeholder_text(ta, get_language_text2(prompt_str, tempString, 200));
    }
    if(init_str){
        lv_textarea_add_text(ta, init_str);
        lv_obj_t* lab = lv_textarea_get_label(ta);
        lv_label_set_text_sel_start(lab, 0);
        lv_label_set_text_sel_end(lab, strlen(init_str));
        lv_obj_set_style_bg_color(lab, lv_color_make(153, 201, 239), LV_PART_SELECTED);
        vtk_kb->sel = 1;
    }    

    lv_obj_set_size(ta, LV_PCT(100), 90);
    lv_obj_set_y(ta,60);
    vtk_kb->textarea = ta;
    lv_obj_add_event_cb(ta, KeyboardEditClick, LV_EVENT_CLICKED, vtk_kb);

    char buf[100];
    snprintf(buf, 100, "%s/%s.json", KEYBORD_LANGUAGE_PATH, mode_para);
    cJSON* lang = GetJsonFromFile(buf);
    if(lang != NULL){
        vtk_kb->key = cJSON_Duplicate(lang,true);       
        strcpy(vtk_kb->language,mode_para);
        vtk_kb->cnt_page = 1;
        load_keyboard(vtk_kb,vtk_kb->cnt_page);
        cJSON_Delete(lang);
    }else{
        vtk_kb->cnt_page = 1;
        vtk_kb->max_page = 1;
        LV_API_TIPS("Language pack does not exist, please check.",4);
    }   

    lv_obj_t* kb = lv_keyboard_create(keyMenu);
    lv_obj_set_style_text_font(kb, font_larger, 0);
    //set_font_size(kb,1);
    lv_obj_set_height(kb, 600);
    lv_keyboard_set_map(kb, LV_KEYBOARD_MODE_TEXT_UPPER, vtk_key_map_uc, vtk_key_ctrl_uc);
    lv_keyboard_set_map(kb, LV_KEYBOARD_MODE_TEXT_LOWER, vtk_key_map_lc, vtk_key_ctrl_uc);
    lv_keyboard_set_map(kb, LV_KEYBOARD_MODE_SPECIAL, vtk_key_map_spec, vtk_key_ctrl_uc);
    lv_obj_remove_event_cb(kb,lv_keyboard_def_event_cb);
    lv_obj_add_event_cb(kb, vtk_keyboard_def_event_cb, LV_EVENT_VALUE_CHANGED, vtk_kb_s);
    lv_keyboard_set_textarea(kb, ta);

    lv_disp_load_scr(vtk_kb->kb_scr);
    return vtk_kb;
}


VTK_KB_S *Vtk_KbMode2Init(VTK_KB_S** vtk_kb_s,int mode,void *mode_para,char *title_str,int limit_len,char *regex_pattern,char *init_str,char *prompt_str,InputOKCallback *inputok,void *user_data)
{
    if(mode != Vtk_KbMode2)
        return NULL;
    VTK_KB_S* vtk_kb = NULL;
    if(!vtk_kb_s)
        return;
    if(*vtk_kb_s)
    {
        Vtk_KbReleaseByMode(vtk_kb_s);
    }

	vtk_kb = (VTK_KB_S*)lv_mem_alloc(sizeof(VTK_KB_S)); 
    *vtk_kb_s = vtk_kb;
    lv_memset_00(vtk_kb, sizeof(VTK_KB_S));
    vtk_kb->language = lv_mem_alloc(50);

    vtk_kb->kb_return = lv_scr_act();
    vtk_kb->kb_scr = keyboard_scr_init();
    vtk_kb->mode_id = mode;
	vtk_kb->user_data=user_data;
    if(regex_pattern != NULL)
    {
        snprintf(vtk_kb->regex_pattern, 200, "%s", regex_pattern);
    }
    vtk_kb->InputOK = inputok;

    lv_obj_t* keyMenu = lv_obj_create(vtk_kb->kb_scr);
    lv_obj_set_size(keyMenu, 480, 800);
    lv_obj_clear_flag(keyMenu, LV_OBJ_FLAG_SCROLLABLE);

    lv_obj_t* name_label = lv_label_create(keyMenu);
    lv_label_set_text(name_label, title_str);
    lv_obj_set_size(name_label, LV_PCT(100), 60);
    lv_obj_set_style_text_font(name_label, &lv_font_montserrat_32, 0);
    //set_font_size(name_label,1);

    lv_obj_t* ta = lv_textarea_create(keyMenu);
    lv_textarea_set_max_length(ta,limit_len);
    lv_obj_set_style_text_font(ta, font_larger, 0);
    lv_textarea_set_text_selection(ta,true); 
    lv_obj_add_state(ta, LV_STATE_FOCUSED);
    //set_font_size(ta,1);
    if(prompt_str){
        char tempString[200];
        lv_textarea_set_placeholder_text(ta, get_language_text2(prompt_str, tempString, 200));
    }
    if(init_str){
        lv_textarea_add_text(ta, init_str);
        lv_obj_t* lab = lv_textarea_get_label(ta);
        lv_label_set_text_sel_start(lab, 0);
        lv_label_set_text_sel_end(lab, strlen(init_str));
        lv_obj_set_style_bg_color(lab, lv_color_make(153, 201, 239), LV_PART_SELECTED);
        vtk_kb->sel = 1;
    }   
    vtk_kb->textarea = ta;

    lv_obj_set_size(ta, LV_PCT(100), 90);
    lv_obj_set_y(ta,60);
    lv_obj_add_event_cb(ta, KeyboardEditClick, LV_EVENT_CLICKED, vtk_kb);

    lv_obj_t* kb = lv_keyboard_create(keyMenu);
    lv_obj_set_style_text_font(kb, font_larger, 0);
    //set_font_size(kb,1);
    lv_obj_set_size(kb,LV_PCT(100) ,600);
    lv_keyboard_set_map(kb, LV_KEYBOARD_MODE_NUMBER, vtk_key_map_num, vtk_key_ctrl_num);
    lv_keyboard_set_mode(kb, LV_KEYBOARD_MODE_NUMBER);
    lv_obj_remove_event_cb(kb,lv_keyboard_def_event_cb);
    lv_obj_add_event_cb(kb, vtk_keyboard_def_event_cb, LV_EVENT_VALUE_CHANGED, vtk_kb_s);
    lv_keyboard_set_textarea(kb, ta);

    lv_disp_load_scr(vtk_kb->kb_scr);
    return vtk_kb;
}

static void ChooseLanguageCallback(const char* chose, void* userdata)
{   
    VTK_KB_S* vtk_kb_s = (VTK_KB_S*)userdata;

    char buf[100];
    snprintf(buf, 100, "%s/%s.json", KEYBORD_LANGUAGE_PATH, chose);
    cJSON* lang = GetJsonFromFile(buf);
    if(lang != NULL){
        vtk_kb_s->key = cJSON_Duplicate(lang,true);
        vtk_kb_s->cnt_page = 1;
        load_keyboard(vtk_kb_s,vtk_kb_s->cnt_page);
        cJSON_Delete(lang);
        strcpy(vtk_kb_s->language,chose);
        API_Para_Write_String(KeybordLanguage, chose);
    }else{
        LV_API_TIPS("Language pack does not exist, please check.",4);
    }   
}

static void choose_keyboard_language(VTK_KB_S* vtk_kb)
{   
    if(!vtk_kb)
        return;
    static CHOSE_MENU_T callback = {.callback = 0, .cbData = NULL};
	callback.callback = ChooseLanguageCallback;
	callback.cbData = vtk_kb;
    cJSON* fileList = cJSON_CreateArray();
    cJSON* choseList = cJSON_CreateArray();
    GetFileAndDirList(KEYBORD_LANGUAGE_PATH, fileList, 0);
    cJSON* fileRecord;
    cJSON_ArrayForEach(fileRecord, fileList)
    {
        cJSON* fileName = cJSON_GetArrayItem(fileRecord, 3);
        if(cJSON_IsString(fileName) && !StrcmpEnd(fileName->valuestring, ".json"))
        {
            char temp[200];
            strcpy(temp, fileName->valuestring);
            temp[strlen(temp) - strlen(".json")] = 0;
            cJSON_AddItemToArray(choseList, cJSON_CreateString(temp));
        }
    }
    cJSON_Delete(fileList);
    CreateChooseMenu("Choose Language", choseList, vtk_kb->language, &callback);
    cJSON_Delete(choseList);
}

static void Language_HeadReturn(lv_event_t* e)
{
    VTK_KB_S* vtk_kb_s = (VTK_KB_S*)lv_event_get_user_data(e);
    lv_obj_del(vtk_kb_s->lanuage_menu);
    vtk_kb_s->lanuage_menu = NULL;
}

static void CreateLanguageMenu(const char* title, const cJSON* choseList, const char* choseString, VTK_KB_S* vtk_kb)
{   
    lv_obj_t* combox = lv_obj_create(lv_scr_act()); 
    lv_obj_set_size(combox, 480, 800);
    //lv_obj_align(combox, LV_ALIGN_CENTER, 0, 0);
    lv_obj_set_style_pad_all(combox, 0, 0);
    lv_obj_clear_flag(combox, LV_OBJ_FLAG_SCROLLABLE);
    lv_obj_set_style_bg_color(combox, lv_color_hex(0x41454D), 0);
    lv_obj_set_style_border_side(combox, 0, 0);
    vtk_kb->lanuage_menu = combox;

    lv_obj_t* btn = lv_obj_create(combox);
    lv_obj_set_size(btn,480, 70);   
    lv_obj_set_style_bg_color(btn, lv_color_make(26,82,196), 0);
    lv_obj_set_style_pad_all(btn,0,0);
    lv_obj_set_style_border_width(btn, 0, 0);

    lv_obj_add_event_cb(btn, Language_HeadReturn, LV_EVENT_CLICKED, vtk_kb);
    lv_obj_add_flag(btn, LV_OBJ_FLAG_CLICKABLE);
    lv_obj_clear_flag(btn, LV_OBJ_FLAG_SCROLLABLE);

    lv_obj_t* obj = lv_obj_create(btn);
    lv_obj_remove_style_all(obj);
    lv_obj_add_flag(obj, LV_OBJ_FLAG_EVENT_BUBBLE);
    lv_obj_set_style_bg_opa(obj,LV_OPA_MAX,0);
    lv_obj_set_size(obj,160,70);
    lv_obj_set_style_bg_color(obj,lv_color_make(19,132,250),0);
    lv_obj_set_style_text_color(obj, lv_color_white(), 0);
    lv_obj_t* img = lv_img_create(obj);
	lv_img_set_src(img,LV_VTK_LEFT);
    set_font_size(img,0);
    lv_obj_center(img);

    lv_obj_t* titlelabel = lv_label_create(btn); 
    lv_obj_set_style_text_align(titlelabel,LV_TEXT_ALIGN_CENTER,0);
    lv_obj_align(titlelabel, LV_ALIGN_LEFT_MID, 160, 0); 
    set_font_size(titlelabel,0);
    lv_obj_set_size(titlelabel, 360, 30);
    //lv_obj_set_style_text_font(titlelabel, TB_VIEW_TextSize, 0); 
    lv_obj_set_style_text_color(titlelabel, lv_color_white(), 0);
    //lv_label_set_text(titlelabel, txt);
    vtk_label_set_text(titlelabel, title); 

    lv_obj_t* cont = lv_obj_create(combox);
    lv_obj_set_flex_flow(cont, LV_FLEX_FLOW_COLUMN);
    set_font_size(cont,0);
    lv_obj_set_size(cont, LV_PCT(100), 730);
    lv_obj_set_y(cont,70);
    lv_obj_set_style_bg_color(cont, lv_color_hex(0x52545A), 0);
    //lv_obj_set_style_pad_all(cont, 0, 0);
    lv_obj_add_flag(cont, LV_OBJ_FLAG_CLICKABLE);   
    lv_obj_set_style_text_color(cont, lv_color_white(), 0);
    lv_obj_set_style_border_side(cont,0,0);
    lv_obj_set_flex_flow(cont, LV_FLEX_FLOW_COLUMN);   
    lv_obj_set_style_border_side(cont, 0, 0);

    cJSON* choseItem;
    cJSON_ArrayForEach(choseItem, choseList)
    {
        if(cJSON_IsString(choseItem))
        {
            lv_obj_t* box1 = lv_checkbox_create(cont);
            lv_checkbox_set_text(box1, choseItem->valuestring);
            lv_obj_set_size(box1, LV_PCT(100), 80);
            set_font_size(box1, 0);
            lv_obj_add_flag(box1, LV_OBJ_FLAG_EVENT_BUBBLE);
            if(choseString && !strcmp(choseItem->valuestring, choseString))
            {
                lv_obj_add_state(box1, LV_STATE_CHECKED);
            }
        }
    }

    lv_obj_add_event_cb(cont, ChooseLanguageCallback, LV_EVENT_CLICKED, vtk_kb); 
}