#if 0
#include "keyboard_test.h"
#include "KeyboardResource.h"
#include "menu_utility.h"
#include "menu_common.h"

extern lv_font_t* font_small;
extern lv_font_t* font_medium;
extern lv_font_t* font_larger;

lv_obj_t* test_scr;
lv_obj_t* test_return;
lv_obj_t* label1;
lv_obj_t* label2;
lv_obj_t* label3;
lv_obj_t* label4;
lv_obj_t* label5;

lv_obj_t* langauge_test_menu_init(void)
{
    test_return = lv_scr_act();
    test_scr = test_scr_init();

    lv_obj_t* cont = lv_obj_create(test_scr);
    lv_obj_set_size(cont, 480, 800);    
    lv_obj_set_flex_flow(cont, LV_FLEX_FLOW_COLUMN);  
  
    lv_obj_t* box1 = lv_obj_create(cont);
    lv_obj_set_flex_flow(box1, LV_FLEX_FLOW_ROW);
    lv_obj_set_size(box1, LV_PCT(100),LV_SIZE_CONTENT);
    lv_obj_set_style_text_font(box1, font_larger, 0);
    lv_obj_t*text_label1 = lv_label_create(box1);
    lv_label_set_text(text_label1, "model1.0");
    lv_obj_set_size(text_label1, 200, LV_SIZE_CONTENT);
    label1 = lv_label_create(box1);
    lv_label_set_text(label1, "Test1");
    lv_obj_set_size(label1, 180, LV_SIZE_CONTENT);
    lv_obj_add_flag(label1, LV_OBJ_FLAG_CLICKABLE);
    lv_obj_add_event_cb(label1, tes1_event, LV_EVENT_CLICKED, NULL); 

    lv_obj_t* box2 = lv_obj_create(cont);
    lv_obj_set_flex_flow(box2, LV_FLEX_FLOW_ROW);
    lv_obj_set_size(box2, LV_PCT(100),LV_SIZE_CONTENT);
    lv_obj_set_style_text_font(box2, font_larger, 0);
    lv_obj_t*text_label2 = lv_label_create(box2);
    lv_label_set_text(text_label2, "model2.0");
    lv_obj_set_size(text_label2, 200, LV_SIZE_CONTENT);
    label2 = lv_label_create(box2);
    lv_label_set_text(label2, "Test2");
    lv_obj_set_size(label2, 180, LV_SIZE_CONTENT);
    lv_obj_add_flag(label2, LV_OBJ_FLAG_CLICKABLE);
    lv_obj_add_event_cb(label2, tes2_event, LV_EVENT_CLICKED, NULL); 

    lv_obj_t* box3 = lv_obj_create(cont);
    lv_obj_set_flex_flow(box3, LV_FLEX_FLOW_ROW);
    lv_obj_set_size(box3, LV_PCT(100),LV_SIZE_CONTENT);
    lv_obj_set_style_text_font(box3, font_larger, 0);
    lv_obj_t*text_label3 = lv_label_create(box3);
    lv_label_set_text(text_label3, "model3.0");
    lv_obj_set_size(text_label3, 200, LV_SIZE_CONTENT);
    label3 = lv_label_create(box3);
    lv_label_set_text(label3, "Test3");
    lv_obj_set_size(label3, 180, LV_SIZE_CONTENT);
    lv_obj_add_flag(label3, LV_OBJ_FLAG_CLICKABLE);
    lv_obj_add_event_cb(label3, tes3_event, LV_EVENT_CLICKED, NULL);  

    lv_obj_t* box4 = lv_obj_create(cont);
    lv_obj_set_flex_flow(box4, LV_FLEX_FLOW_ROW);
    lv_obj_set_size(box4, LV_PCT(100),LV_SIZE_CONTENT);
    lv_obj_set_style_text_font(box4, font_larger, 0);
    lv_obj_t*text_label4 = lv_label_create(box4);
    lv_label_set_text(text_label4, "model4.0");
    lv_obj_set_size(text_label4, 200, LV_SIZE_CONTENT);
    label4 = lv_label_create(box4);
    lv_label_set_text(label4, "Test4");
    lv_obj_set_size(label4, 180, LV_SIZE_CONTENT);
    lv_obj_add_flag(label4, LV_OBJ_FLAG_CLICKABLE);
    lv_obj_add_event_cb(label4, tes4_event, LV_EVENT_CLICKED, NULL); 

    lv_obj_t* box5 = lv_obj_create(cont);
    lv_obj_set_flex_flow(box5, LV_FLEX_FLOW_ROW);
    lv_obj_set_size(box5, LV_PCT(100),LV_SIZE_CONTENT);
    lv_obj_set_style_text_font(box5, font_larger, 0);
    lv_obj_t*text_label5 = lv_label_create(box5);
    lv_label_set_text(text_label5, "model5.0");
    lv_obj_set_size(text_label5, 200, LV_SIZE_CONTENT);
    label5 = lv_label_create(box5);
    lv_label_set_text(label5, "Test5");
    lv_obj_set_size(label5, 180, LV_SIZE_CONTENT);
    lv_obj_add_flag(label5, LV_OBJ_FLAG_CLICKABLE);
    lv_obj_add_event_cb(label5, tes5_event, LV_EVENT_CLICKED, NULL); 

    lv_disp_load_scr(test_scr);
    return cont;
}

int* Callback1(const char* val,void *user_data){
    lv_label_set_text(label1,val);
}

int* Callback2(const char* val,void *user_data){
    lv_label_set_text(label2,val);
}
int* Callback3(const char* val,void *user_data){
    lv_label_set_text(label3,val);
}

int* Callback4(const char* val,void *user_data){
    lv_label_set_text(label4,val);
}

int* Callback5(const char* val,void *user_data){
    lv_label_set_text(label5,val);
}

static void tes1_event(lv_event_t* e)
{
    lv_obj_t * obj = lv_event_get_target(e);
    char* val = lv_label_get_text(obj);
    lv_obj_t* nameLabel = lv_obj_get_child(lv_obj_get_parent(obj), 0);
    char* name = lv_label_get_text(nameLabel);

    Vtk_KbInitByMode(Vtk_KbMode1,"English",name,10,"^[a-zA-Z0-9]{5,20}$",val,"Test",Callback1,NULL);
}

static void tes2_event(lv_event_t* e)
{
    lv_obj_t * obj = lv_event_get_target(e);
    char* val = lv_label_get_text(obj);
    lv_obj_t* nameLabel = lv_obj_get_child(lv_obj_get_parent(obj), 0);
    char* name = lv_label_get_text(nameLabel);
    Vtk_KbInitByMode(Vtk_KbMode1,"English",name,20,"^[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}$",val,"Test",Callback2,NULL);
}

static void tes3_event(lv_event_t* e)
{
    lv_obj_t * obj = lv_event_get_target(e);
    char* val = lv_label_get_text(obj);
    lv_obj_t* nameLabel = lv_obj_get_child(lv_obj_get_parent(obj), 0);
    char* name = lv_label_get_text(nameLabel);
    Vtk_KbInitByMode(Vtk_KbMode2,NULL,name,20,NULL,val,"Test",Callback3,NULL);
}

static void tes4_event(lv_event_t* e)
{
    lv_obj_t * obj = lv_event_get_target(e);
    char* val = lv_label_get_text(obj);
    lv_obj_t* nameLabel = lv_obj_get_child(lv_obj_get_parent(obj), 0);
    char* name = lv_label_get_text(nameLabel);
    Vtk_KbInitByMode(Vtk_KbMode2,NULL,name,30,NULL,val,"Test",Callback4,NULL);
}

static void tes5_event(lv_event_t* e)
{
    lv_obj_t * obj = lv_event_get_target(e);
    char* val = lv_label_get_text(obj);
    lv_obj_t* nameLabel = lv_obj_get_child(lv_obj_get_parent(obj), 0);
    char* name = lv_label_get_text(nameLabel);
    Vtk_KbInitByMode(Vtk_KbMode1,"Greek",name,30,NULL,val,"Test",Callback5,NULL);
}

static lv_obj_t* test_scr_init(void)
{
    lv_obj_t* ui_test = lv_obj_create(NULL);
    lv_obj_clear_flag(ui_test, LV_OBJ_FLAG_SCROLLABLE);
    lv_obj_remove_style_all(ui_test);
    lv_obj_set_size(ui_test, lv_disp_get_hor_res(NULL), lv_disp_get_ver_res(NULL));
    lv_obj_align(ui_test, LV_ALIGN_TOP_LEFT, 0, 0);
    lv_obj_set_style_bg_color(ui_test, lv_palette_main(LV_PALETTE_GREY), 0);
    lv_obj_set_style_bg_opa(ui_test, LV_OPA_100, 0);   
    return ui_test;
}
#endif