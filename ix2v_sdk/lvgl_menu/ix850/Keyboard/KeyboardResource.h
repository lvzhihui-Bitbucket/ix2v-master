#ifndef KEYBOARDMenuResource_H_
#define KEYBOARDMenuResource_H_

#include "lvgl.h"
#include "cJSON.h"

typedef int (*InputOKCallback)(const char*,void *);

typedef struct
{
	int mode_id;
	int limit_len;	
	char regex_pattern[200];
	InputOKCallback InputOK;

	int sel;
    int cnt_page;
    int max_page;
    cJSON* key;
    char* language;
    lv_obj_t* lanuage_menu;
    lv_obj_t* kb_scr;        
    lv_obj_t* kb_return;
	lv_obj_t* textarea;

	void *resource;		//不同模式下所需的额外资源，如多语言键盘的布局参数和字符列表等
	void *user_data;
}VTK_KB_S;

typedef VTK_KB_S* (*VTK_KBInit)(VTK_KB_S** vtk_kb_s,int mode,void *mode_para,char *title_str,int limit_len,char *regex_pattern,char *init_str,char *prompt_str,InputOKCallback *inputok,void *user_data);
typedef int (*VTK_KBRule)(VTK_KB_S* vtk_kb);
typedef void (*VTK_KBRelease)(VTK_KB_S* vtk_kb);

typedef struct
{
    int mode_id;
    VTK_KBInit init;
    VTK_KBRule rule;
	VTK_KBRelease release;
} VTKKBModeCallback_S;

enum
{
	Vtk_KbMode1=0,
	Vtk_KbMode2,
	Vtk_KbModeMax
};

VTKKBModeCallback_S* GetVTKKBModeCallbackByMode(int mode);
VTK_KB_S *Vtk_KbInitByMode(VTK_KB_S** vtk_kb_s,int mode,void *mode_para,char *title_str,int limit_len,char *regex_pattern,char *init_str,char *prompt_str,InputOKCallback *inputok,void *user_data);
void Vtk_KbReleaseByMode(VTK_KB_S** vtk_kb_s);

int Vtk_KbMode1Rule(VTK_KB_S* vtk_kb);
VTK_KB_S *Vtk_KbMode1Init(VTK_KB_S** vtk_kb_s,int mode,void *mode_para,char *title_str,int limit_len,char *regex_pattern,char *init_str,char *prompt_str,InputOKCallback *inputok,void *user_data);
void Vtk_KbMode1Release(VTK_KB_S* vtk_kb);

VTK_KB_S *Vtk_KbMode2Init(VTK_KB_S** vtk_kb_s,int mode,void *mode_para,char *title_str,int limit_len,char *regex_pattern,char *init_str,char *prompt_str,InputOKCallback *inputok,void *user_data);
#endif 