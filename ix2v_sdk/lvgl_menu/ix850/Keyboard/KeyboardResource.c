#include "KeyboardResource.h"

const VTKKBModeCallback_S VTKKBModeCallbackTb[] = {
    {Vtk_KbMode1, Vtk_KbMode1Init, Vtk_KbMode1Rule, Vtk_KbMode1Release},
    {Vtk_KbMode2, Vtk_KbMode2Init, Vtk_KbMode1Rule, Vtk_KbMode1Release}
};
const int VTKKBModeCallbackTb_Num= sizeof(VTKKBModeCallbackTb)/sizeof(VTKKBModeCallbackTb[0]);

VTKKBModeCallback_S* GetVTKKBModeCallbackByMode(int mode)
{
    VTKKBModeCallback_S* ret =NULL;
    int i;

  
        for(i = 0; i < VTKKBModeCallbackTb_Num; i ++)
        {
            if(VTKKBModeCallbackTb[i].mode_id==mode)
            {
                ret = &VTKKBModeCallbackTb[i];
                break;
            }
        }
   
    return ret;
}

VTK_KB_S *Vtk_KbInitByMode(VTK_KB_S** vtk_kb_s,int mode,void *mode_para,char *title_str,int limit_len,char *regex_pattern,char *init_str,char *prompt_str,InputOKCallback *inputok,void *user_data)
{
    VTK_KB_S* vtk_kb = NULL;
    if(!vtk_kb_s)
    {
        return;
    }

	VTKKBModeCallback_S *kb_callbak;

	kb_callbak = GetVTKKBModeCallbackByMode(mode);

	if(kb_callbak)
	{
		if(kb_callbak->init){
            vtk_kb=(*(kb_callbak->init))(vtk_kb_s,mode,mode_para,title_str,limit_len,regex_pattern,init_str,prompt_str,inputok,user_data);
        }		 	
	}
	return vtk_kb;
}

void Vtk_KbReleaseByMode(VTK_KB_S** vtk_kb_s)
{	
	VTK_KB_S* vtk_kb = NULL;
    if(!vtk_kb_s)
    {
        return;
    }

	VTKKBModeCallback_S *kb_callbak;
	if(*vtk_kb_s)
	{
		vtk_kb = *vtk_kb_s;
        *vtk_kb_s = NULL;

		kb_callbak = GetVTKKBModeCallbackByMode(vtk_kb->mode_id);
		if(kb_callbak)
		{
			if(kb_callbak->release)
			 	(*kb_callbak->release)(vtk_kb);
		}
	}
}