#ifndef _IX850_INFO_H_
#define _IX850_INFO_H_

#include "lv_ix850.h"
#include "icon_common.h"
#include "menu_common.h"

lv_obj_t* img_creat_label(lv_obj_t* parent, struct ICON_INS* picon);
lv_obj_t* img_creat_info(lv_obj_t* parent, struct ICON_INS* picon);

void EnterPicSelectMenu(const char* dir);
lv_obj_t* Pic_preview_init(char* name);
#endif
