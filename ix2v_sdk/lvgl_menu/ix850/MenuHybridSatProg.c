#include "menu_utility.h"
#include "MenuHybridMatCreateNew.h"
#include "cJSON.h"
#include "obj_TableSurver.h"
#include "define_string.h"
#include "obj_PublicInformation.h"
#include "utility.h"
#include "task_Event.h"
#include "Common_TableView.h"
#include "obj_IXS_Proxy.h"
#include "obj_IoInterface.h"
#include "obj_Certificate.h"
#include "define_file.h"

#define SAT_HYBRID_PROG_PARA		"{\"HeaderEnable\":true,\"CellCallback\":0,\"ColumnWidth\":[120, 180, 180],\"Function\":{\"Name\":[],\"Callback\":0}}}"

static TB_VIWE* satHybridProgTb = NULL;
static TB_VIWE* satHybridOthersTb = NULL;
static cJSON* progView = NULL;
static int refreshFlag = 0;
static char* progBusyFlag = NULL;
static cJSON* progOneRecord = NULL;

static cJSON* GetOthersList(void);
static void UpdateProgList(void);
static int SatHybridProgTimeoutCb(int time);
static int  ProcOnlineDsOrDXGAndTip(cJSON* viewElement);
static void ConfirmSatProc(void* data);
static void UpdateProgListDisplay(void);
static void SatHybridProgCloseMessage(int time);

static void ConfirmSatProcOne(void* data)
{
	cJSON* record = (cJSON*)data;
	if(record)
	{
		progBusyFlag = "Prog one";
		progOneRecord = record;
		API_Event_NameAndMsg(EventMenuSatHybridProg, "Prog one", NULL);
	}
}

static void SatHybridProgCellClick(int line, int col)
{
	if(satHybridProgTb)
	{
		cJSON* record = cJSON_GetArrayItem(progView, line);
		if(cJSON_IsObject(record))
		{
			char* deviceType = GetEventItemString(record, "IX_TYPE");
			if(!strcmp(deviceType, "DS") || !strcmp(deviceType, "DXG")|| !strcmp(deviceType, "IXG"))
			{
				char tempString[50];
				snprintf(tempString, 50, "Prog %s%d", deviceType, GetEventItemInt(record, "DEV_ID"));
				API_MSGBOX(tempString, 2, ConfirmSatProcOne, record);
			}
		}
	}
}

static void SatHybridProgDisplayMessage(char* string)
{
	if(string)
	{
		if(!API_TIPS_Mode(string));
		{
			API_TIPS_Mode_Fresh(NULL, string);
		}
	}
}

static int SatHybridProgCloseMessageTimerCb(int time)
{
	API_TIPS_Mode_Close(NULL);
	return 2;
}

static void SatHybridProgCloseMessage(int time)
{
	if(time)
	{
		API_Add_TimingCheck(SatHybridProgCloseMessageTimerCb, time);
	}
	else
	{
		API_TIPS_Mode_Close(NULL);
	}
}


static void SatHybridProgReturnCallback(void)
{
	MenuSAT_HYBRID_Prog_Close();
}

static int CountTableByIX_ADDR(cJSON* tb, const char* ixAddr)
{
	int ret = 0;
	if(ixAddr)
	{
		cJSON* where = cJSON_CreateObject();
		cJSON_AddStringToObject(where, "IX_ADDR", ixAddr);
		ret = API_TB_Count(tb, where);
		cJSON_Delete(where);
	}
	return ret;
}

cJSON* GetTableRecordByIX_ADDR(cJSON* tb, const char* ixAddr)
{
	cJSON* ret = NULL;
	if(ixAddr)
	{
		cJSON* view = cJSON_CreateArray();
		cJSON* where = cJSON_CreateObject();
		cJSON_AddStringToObject(where, "IX_ADDR", ixAddr);
		if(API_TB_SelectBySort(tb, view, where, 0))
		{
			ret = cJSON_Duplicate(cJSON_GetArrayItem(view, 0), 1);
		}
		cJSON_Delete(view);
		cJSON_Delete(where);
	}
	return ret;
}

static cJSON* CreateProgDisplayList(void)
{

	cJSON* view = cJSON_CreateArray();

	cJSON* viewElement;
	cJSON_ArrayForEach(viewElement, progView)
	{
		char tempString[50];
		cJSON* record = cJSON_CreateObject();

		snprintf(tempString, 50, "%s%d", GetEventItemString(viewElement, "IX_TYPE"), GetEventItemInt(viewElement, "DEV_ID"));
		cJSON_AddStringToObject(record, "DEVICE", tempString);
		cJSON_AddStringToObject(record, "STATE", GetEventItemString(viewElement, "STATE"));
		cJSON_AddStringToObject(record, "RESULT", GetEventItemString(viewElement, "RESULT"));
		cJSON_AddItemToArray(view, record);
	}
	return view;
}

static int SatHybridProgRefleshTimerCb(int time)
{
	if(refreshFlag)
	{
		if(time < 6)
		{
			char tempString[10];
			snprintf(tempString, 10, "%ds", 6 - time);
			API_TIPS_Mode_Title(NULL, tempString);
			MainMenu_Reset_Time();
			return 0;
		}
		else
		{
			refreshFlag = 0;
			SatHybridProgCloseMessage(0);
		}
	}

	return 2;
}

static int SatHybridProgTimeoutCb(int time)
{
	if(progBusyFlag)
	{
		API_Event_NameAndMsg(EventMenuSatHybridProg, "Prog timeout", NULL);
	}

	return 2;
}

static void ConfirmSatProc(void* data)
{
	progBusyFlag = (char*)data;
	if(progBusyFlag)
	{
		API_Event_NameAndMsg(EventMenuSatHybridProg, progBusyFlag, NULL);
	}
}


static void FunctionClick(const char* funcName)
{
	if(funcName)
	{
		if(refreshFlag || progBusyFlag)
		{
			SatHybridProgDisplayMessage("Busy, please wait");
			return;
		}

		if(!strcmp(funcName, "Refresh"))
		{
			refreshFlag = 1;
			API_Event_By_Name(Event_IXS_ReqUpdateTb);
			SatHybridProgDisplayMessage("Please wait while refreshing");
			API_Add_TimingCheck(SatHybridProgRefleshTimerCb, 1);
		}
		else if(!strcmp(funcName, "Prog self"))
		{
			API_MSGBOX("Prog self", 2, ConfirmSatProc, "Prog self");
		}
		else if(!strcmp(funcName, "Prog DS"))
		{
			API_MSGBOX("Prog DS", 2, ConfirmSatProc, "Prog DS");
		}
		else if(!strcmp(funcName, "Prog DXG"))
		{
			API_MSGBOX("Prog DXG", 2, ConfirmSatProc, "Prog DXG");
			
		}
		else if(!strcmp(funcName, "Prog all"))
		{
			API_MSGBOX("Prog all", 2, ConfirmSatProc, "Prog all");
		}
		else if(!strcmp(funcName, "View others"))
		{
			cJSON* para = cJSON_Parse(SAT_HYBRID_PROG_PARA);
			cJSON* view = GetOthersList();
			TB_MenuDisplay(&satHybridOthersTb, view, "SAT hybrid others", para);
			cJSON_Delete(view);
			cJSON_Delete(para);
		}
	}
}

static void UpdateProgListDisplay(void)
{
	UpdateProgList();
	cJSON* view = CreateProgDisplayList();
	TB_MenuDisplay(&satHybridProgTb, view, NULL, NULL);
	cJSON_Delete(view);
}

static int  ProcOnlineDsOrDXG(cJSON* record)
{
	int ret;
	char* deviceType = GetEventItemString(record, "IX_TYPE");
	int targetIp = inet_addr(GetEventItemString(record, "IP_ADDR"));
	char* myIpString = GetSysVerInfo_IP_by_device(GetNetDeviceNameByTargetIp(targetIp));

	cJSON* event = cJSON_CreateObject();
	cJSON_AddStringToObject(event, IX2V_EventName, EventDH_HybridProgOnline);

	if(!strcmp(deviceType, "DS"))
	{
		cJSON_AddStringToObject(event, "TARGET_IP_ADDR", myIpString);
		cJSON* table = cJSON_AddArrayToObject(event, "SYNC_TABLE");
		cJSON_AddItemToArray(table, cJSON_CreateString(TB_NAME_HYBRID_MAT));
		cJSON_AddItemToArray(table, cJSON_CreateString(TB_NAME_HYBRID_SAT));
	}
	else if(!strcmp(deviceType, "DXG")||!strcmp(deviceType, "IXG"))
	{
		cJSON_AddItemToObject(event, "PROG_RECORD", cJSON_Duplicate(record, 1));
	}

	cJSON_AddStringToObject(event, "REPORT_IP_ADDR", myIpString);

	ret = API_XD_EventJson(targetIp, event);
	cJSON_Delete(event);

	return ret;
}

static int  ProcOnlineDsOrDXGAndTip(cJSON* viewElement)
{
	int ret = 0;
	char tip[50];
	int result;

	char* state = GetEventItemString(viewElement, "STATE");
	if(!strcmp(state, "self"))
	{
		char* certState = API_PublicInfo_Read_String(PB_CERT_STATE);
		if(certState && !strcmp(certState, "Delivered"))
		{
			API_Delete_CERT();
		}

		result = SatHibridProgByRecord(viewElement, 1);

		if(result)
		{
			result = ((API_Create_DCFG_SELF(SysType, NULL) && API_Create_CERT(CERT_DCFG_SELF_NAME) && API_Check_CERT()) ? 1 : 0);
		}

		cJSON_ReplaceItemInObjectCaseSensitive(viewElement, "RESULT", cJSON_CreateString(result ? "success" : "error"));
		SatHybridProgDisplayMessage(result ? "Prog self success" : "Prog self error");
	}
	else if(!strcmp(state, "online"))
	{
		cJSON_ReplaceItemInObjectCaseSensitive(viewElement, "RESULT", cJSON_CreateString("waiting"));
		snprintf(tip, 50, "Prog %s%d waiting", GetEventItemString(viewElement, "IX_TYPE"), GetEventItemInt(viewElement, "DEV_ID"));
		SatHybridProgDisplayMessage(tip);
		usleep(10*1000);
		if(ProcOnlineDsOrDXG(viewElement))
		{
			ret = 1;
		}
		else
		{
			cJSON_ReplaceItemInObjectCaseSensitive(viewElement, "RESULT", cJSON_CreateString("error"));
			snprintf(tip, 50, "Prog %s%d error", GetEventItemString(viewElement, "IX_TYPE"), GetEventItemInt(viewElement, "DEV_ID"));
			SatHybridProgDisplayMessage(tip);
		}
	}
	else if(!strcmp(state, "offline"))
	{
		cJSON_ReplaceItemInObjectCaseSensitive(viewElement, "RESULT", cJSON_CreateString("error"));
		snprintf(tip, 50, "Prog %s%d error", GetEventItemString(viewElement, "IX_TYPE"), GetEventItemInt(viewElement, "DEV_ID"));
		SatHybridProgDisplayMessage(tip);
	}
	UpdateProgListDisplay();

	return ret;
}


int EventDH_HybridProgOnlineReportCallback(cJSON* cmd)
{
	cJSON* viewElement;
	int result = 0;
	int waitFlag = 0;
	if(progBusyFlag)
	{
		cJSON* record = cJSON_GetObjectItemCaseSensitive(cmd, "PROG_RECORD");
		char* ixAddr = GetEventItemString(record, "IX_ADDR");
		cJSON_ArrayForEach(viewElement, progView)
		{
			if(!strcmp(GetEventItemString(viewElement, "IX_ADDR"), ixAddr))
			{
				if(progBusyFlag && !strcmp(progBusyFlag, "Prog one"))
				{
					progBusyFlag = NULL;
					API_Del_TimingCheck(SatHybridProgTimeoutCb);
				}

				char tip[50];
				char* result = GetEventItemString(cmd, "RESULT");
				cJSON_ReplaceItemInObjectCaseSensitive(viewElement, "RESULT", cJSON_CreateString(result));
				snprintf(tip, 50, "Prog %s%d %s", GetEventItemString(viewElement, "IX_TYPE"), GetEventItemInt(viewElement, "DEV_ID"), result);
				SatHybridProgDisplayMessage(tip);
				UpdateProgListDisplay();
				break;
			}
		}
	}

	if(!progBusyFlag)
	{
		SatHybridProgCloseMessage(1);
	}
}

int EventMenuSatHybridProgCallback(cJSON* cmd)
{
	if(progBusyFlag)
	{
		cJSON* viewElement;
		char* deviceType;
		int result = 0;
		int waitFlag = 0;
		char tip[500];
		char* msg = GetEventItemString(cmd, EVENT_KEY_Message);
		if(!strcmp(msg, "Prog self"))
		{
			char* myIxAddr = GetSysVerInfo_BdRmMs();
			cJSON_ArrayForEach(viewElement, progView)
			{
				if(myIxAddr && !strcmp(GetEventItemString(viewElement, "IX_ADDR"), myIxAddr))
				{
					cJSON_ReplaceItemInObjectCaseSensitive(viewElement, "RESULT", cJSON_CreateString(""));
					UpdateProgListDisplay();

					char* certState = API_PublicInfo_Read_String(PB_CERT_STATE);
					if(certState && !strcmp(certState, "Delivered"))
					{
						API_Delete_CERT();
					}

					result = SatHibridProgByRecord(viewElement, 1);
					
					if(result)
					{
						result = ((API_Create_DCFG_SELF(SysType, NULL) && API_Create_CERT(CERT_DCFG_SELF_NAME) && API_Check_CERT()) ? 1 : 0);
					}

					cJSON_ReplaceItemInObjectCaseSensitive(viewElement, "RESULT", cJSON_CreateString(result ? "success" : "error"));
					UpdateProgListDisplay();
					break;
				}
			}
			SatHybridProgDisplayMessage(result ? "Prog self success" : "Prog self error");
			progBusyFlag = NULL;
		}
		else if(!strcmp(msg, "Prog DS"))
		{
			cJSON_ArrayForEach(viewElement, progView)
			{
				deviceType = GetEventItemString(viewElement, "IX_TYPE");
				if(!strcmp(deviceType, "DS"))
				{
					cJSON_ReplaceItemInObjectCaseSensitive(viewElement, "RESULT", cJSON_CreateString(""));
				}
			}
			UpdateProgListDisplay();

			cJSON_ArrayForEach(viewElement, progView)
			{
				deviceType = GetEventItemString(viewElement, "IX_TYPE");
				if(!strcmp(deviceType, "DS"))
				{
					waitFlag = (ProcOnlineDsOrDXGAndTip(viewElement) ? 1 : waitFlag);
					MainMenu_Reset_Time();
				}
			}
			
			if(waitFlag)
			{
				API_Add_TimingCheck(SatHybridProgTimeoutCb, 10);
			}
			else
			{
				progBusyFlag = NULL;
				SatHybridProgDisplayMessage("Prog complete");
			}
		}
		else if(!strcmp(msg, "Prog DXG"))
		{
			cJSON_ArrayForEach(viewElement, progView)
			{
				deviceType = GetEventItemString(viewElement, "IX_TYPE");
				if(!strcmp(deviceType, "DXG")||!strcmp(deviceType, "IXG"))
				{
					cJSON_ReplaceItemInObjectCaseSensitive(viewElement, "RESULT", cJSON_CreateString(""));
				}
			}
			UpdateProgListDisplay();

			cJSON_ArrayForEach(viewElement, progView)
			{
				deviceType = GetEventItemString(viewElement, "IX_TYPE");
				if(!strcmp(deviceType, "DXG")||!strcmp(deviceType, "IXG"))
				{
					waitFlag = (ProcOnlineDsOrDXGAndTip(viewElement) ? 1 : waitFlag);
					MainMenu_Reset_Time();
				}
			}
			if(waitFlag)
			{
				API_Add_TimingCheck(SatHybridProgTimeoutCb, 10);
			}
			else
			{
				progBusyFlag = NULL;
				SatHybridProgDisplayMessage("Prog complete");
			}
		}
		else if(!strcmp(msg, "Prog all"))
		{
			cJSON_ArrayForEach(viewElement, progView)
			{
				deviceType = GetEventItemString(viewElement, "IX_TYPE");
				if(!strcmp(deviceType, "DS") || !strcmp(deviceType, "DXG")||!strcmp(deviceType, "IXG"))
				{
					cJSON_ReplaceItemInObjectCaseSensitive(viewElement, "RESULT", cJSON_CreateString(""));
				}
			}
			UpdateProgListDisplay();

			cJSON_ArrayForEach(viewElement, progView)
			{
				deviceType = GetEventItemString(viewElement, "IX_TYPE");
				if(!strcmp(deviceType, "DS") || !strcmp(deviceType, "DXG")||!strcmp(deviceType, "IXG"))
				{
					waitFlag = (ProcOnlineDsOrDXGAndTip(viewElement) ? 1 : waitFlag);
					MainMenu_Reset_Time();
				}
			}

			if(waitFlag)
			{
				API_Add_TimingCheck(SatHybridProgTimeoutCb, 10);
			}
			else
			{
				progBusyFlag = NULL;
				SatHybridProgDisplayMessage("Prog complete");
			}
		}
		else if(!strcmp(msg, "Prog timeout"))
		{
			cJSON_ArrayForEach(viewElement, progView)
			{
				if(!strcmp(GetEventItemString(viewElement, "RESULT"), "waiting"))
				{
					cJSON_ReplaceItemInObjectCaseSensitive(viewElement, "RESULT", cJSON_CreateString("timeout"));
					waitFlag = 1;
				}
			}

			API_Del_TimingCheck(SatHybridProgTimeoutCb);
			if(waitFlag)
			{
				UpdateProgListDisplay();
			}
			progBusyFlag = NULL;
			SatHybridProgDisplayMessage("Prog complete");
			
		}
		else if(!strcmp(msg, "Prog one"))
		{
			if(ProcOnlineDsOrDXGAndTip(progOneRecord))
			{
				if(progBusyFlag && !strcmp(progBusyFlag, "Prog one"))
				{
					API_Add_TimingCheck(SatHybridProgTimeoutCb, 10);
				}
			}
			else
			{
				progBusyFlag = NULL;
			}
			progOneRecord = NULL;
		}

		if(!progBusyFlag)
		{
			SatHybridProgCloseMessage(1);
		}
	}
}

//IXS表有更新
int SatHybridProgListChangeProcess(cJSON *cmd)
{
	if(refreshFlag)
	{
		refreshFlag = 0;
		if(progBusyFlag)
		{
			return 0;
		}
		UpdateProgListDisplay();
		SatHybridProgCloseMessage(0);
	}
	return 1;
}
//IXS表无更新
int SatHybridProgListUnChangeProcess(cJSON *cmd)
{
	if(refreshFlag)
	{
		refreshFlag = 0;
		SatHybridProgCloseMessage(0);
	}
	return 1;
}

static void UpdateProgList(void)
{
	cJSON* onlineDevice = NULL;
	if(!progView)
	{
		progView = cJSON_CreateArray();
	}

	cJSON* view = cJSON_CreateArray();
	API_TB_SelectBySortByName(TB_NAME_HYBRID_SAT, view, NULL, 0);

	IXS_ProxyGetTb(&onlineDevice, NULL, Search);

	cJSON* viewElement;
	cJSON_ArrayForEach(viewElement, view)
	{
		char* ixAddr = GetEventItemString(viewElement, "IX_ADDR");
		char* myIxAddr = GetSysVerInfo_BdRmMs();
		char* state;
		cJSON* onlineRecord = NULL;
		char* ipAddr;

		int onlineCnt = CountTableByIX_ADDR(onlineDevice, ixAddr);
		if(onlineCnt > 1)
		{
			state = "repeat";
		}
		else
		{
			if(myIxAddr && !strcmp(myIxAddr, ixAddr))
			{
				state = "self";
			}
			else
			{
				state = onlineCnt ? "online" : "offline";
			}
		}

		if(!strcmp(state, "online"))
		{
			onlineRecord = GetTableRecordByIX_ADDR(onlineDevice, ixAddr);
		}
		ipAddr = GetEventItemString(onlineRecord,  "IP_ADDR");

		if(CountTableByIX_ADDR(progView, ixAddr))
		{
			cJSON* progElement;
			cJSON_ArrayForEach(progElement, progView)
			{
				myIxAddr = GetEventItemString(progElement,  "IX_ADDR");
				if(!strcmp(myIxAddr, ixAddr))
				{
					cJSON_ReplaceItemInObjectCaseSensitive(progElement, "STATE", cJSON_CreateString(state));
					cJSON_ReplaceItemInObjectCaseSensitive(progElement, "IP_ADDR", cJSON_CreateString(ipAddr));
					break;
				}
			}
		}
		else
		{
			cJSON* record = cJSON_Duplicate(viewElement, 1);
			cJSON_AddStringToObject(record, "STATE", state);
			cJSON_AddStringToObject(record, "IP_ADDR", ipAddr);
			cJSON_AddStringToObject(record, "RESULT", "");
			cJSON_AddItemToArray(progView, record);
		}

		if(onlineRecord)
		{
			cJSON_Delete(onlineRecord);
			onlineRecord = NULL;
		}
	}
	cJSON_Delete(view);
	cJSON_Delete(onlineDevice);
}

static cJSON* GetOthersList(void)
{
	cJSON* onlineDevice = NULL;
	cJSON* view = cJSON_CreateArray();
	cJSON* ret = cJSON_CreateArray();
	API_TB_SelectBySortByName(TB_NAME_HYBRID_SAT, view, NULL, 0);

	IXS_ProxyGetTb(&onlineDevice, NULL, Search);

	cJSON* viewElement;
	cJSON_ArrayForEach(viewElement, onlineDevice)
	{
		char* ixAddr = GetEventItemString(viewElement, "IX_ADDR");
		char* ixType = GetEventItemString(viewElement, "IX_TYPE");
		if((!strcmp(ixType, "DS") && !strncmp(ixAddr, "00990000", 8)) || (!strcmp(ixType, "DXG") || !strcmp(ixType, "IXG") && !strncmp(ixAddr, "0099", 4) && !strncmp(ixAddr+6, "0001", 4)))
		{
			if(!CountTableByIX_ADDR(view, ixAddr))
			{
				char tempString[50] = {0};
				char* state = (CountTableByIX_ADDR(onlineDevice, ixAddr) > 1 ? "repeat" : "online");
				cJSON* record = cJSON_CreateObject();
				!strcmp(ixType, "DS") ? strcpy(tempString, ixAddr+8) : strncpy(tempString, ixAddr+4, 2);
				int id = atoi(tempString);
				#ifdef IX_LINK
				snprintf(tempString, 50, "%s%d", ((!strcmp(ixType, "DS")) ? ixType : "IXG"), id);
				#else
				snprintf(tempString, 50, "%s%d", ((!strcmp(ixType, "DS")) ? ixType : "DXG"), id);
				#endif
				cJSON_AddStringToObject(record, "DEVICE", tempString);
				cJSON_AddStringToObject(record, "STATE", state);
				cJSON_AddStringToObject(record, "RESULT", "");
				cJSON_AddItemToArray(ret, record);
			}
		}
	}
	cJSON_Delete(view);
	cJSON_Delete(onlineDevice);
	return ret;
}

void MenuSAT_HYBRID_Prog_Init(void)
{
	cJSON* para = cJSON_Parse(SAT_HYBRID_PROG_PARA);
	cJSON_ReplaceItemInObjectCaseSensitive(para, "CellCallback", cJSON_CreateNumber((int)SatHybridProgCellClick));
	cJSON_AddNumberToObject(para, "ReturnCallback", (int)SatHybridProgReturnCallback);

	cJSON* func = cJSON_CreateObject();
	cJSON* funcName = cJSON_AddArrayToObject(func, "Name");
	cJSON_AddItemToArray(funcName, cJSON_CreateString("Refresh"));
	cJSON_AddItemToArray(funcName, cJSON_CreateString("Prog self"));
	//cJSON_AddItemToArray(funcName, cJSON_CreateString("Prog DS"));
	//cJSON_AddItemToArray(funcName, cJSON_CreateString("Prog DXG"));
	cJSON_AddItemToArray(funcName, cJSON_CreateString("View others"));
	cJSON_AddItemToArray(funcName, cJSON_CreateString("Prog all"));
	cJSON_AddNumberToObject(func, "Callback", (int)FunctionClick);
	cJSON_ReplaceItemInObjectCaseSensitive(para, "Function", func);

	UpdateProgList();
	cJSON* view = CreateProgDisplayList();
	TB_MenuDisplay(&satHybridProgTb, view, "SAT hybrid prog", para);
	cJSON_Delete(para);
	cJSON_Delete(view);
}

void MenuSAT_HYBRID_Prog_Close(void)
{
	SatHybridProgCloseMessage(0);
	TB_MenuDelete(&satHybridOthersTb);
	TB_MenuDelete(&satHybridProgTb);
	if(progView)
	{
		cJSON_Delete(progView);
		progView = NULL;
	}

	if(refreshFlag)
	{
		API_Del_TimingCheck(SatHybridProgRefleshTimerCb);
		refreshFlag = 0;
	}

	if(progBusyFlag)
	{
		progBusyFlag = NULL;
	}
}

static void ResetCallNumber(void* para)
{
	API_Delete_CERT();
	API_Para_Restore_Default(IX_ADDR);
	API_Para_Restore_Default(IX_NAME);
	API_Para_Restore_Default(G_NBR);
	API_Para_Restore_Default(L_NBR);
	API_Para_Restore_Default(G_NBR_SEARCH_ALLOW);
	API_Para_Restore_Default(L_NBR_SEARCH_ALLOW);
	API_Event_NameAndMsg(EventMenuSettingProcess, "SettingRefresh", NULL);
}

void Menu_CallNumberSetting_Process(void* data)
{
	if(data)
	{
		if(!strcmp(data, "ResetCallNumber"))
		{
			API_MSGBOX("Reset call number", 2, ResetCallNumber, NULL);
		}
	}
}
