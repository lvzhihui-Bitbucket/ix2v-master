#include <stdio.h>
#include "task_CallServer.h"
#include "elog_forcall.h"
#include "obj_PublicInformation.h"
#include "task_Event.h"
#include "ix850_icon.h"
#include "obj_lvgl_msg.h"
#if 0
#define IX850_MSG_CALL_RESULT        2302171094
#define IX850_MSG_CALL_INFO   		2302286601
#define IX850_MSG_Namelist_Reload   	2303016601
#endif
void Api_LvCallBusiness_StartCall(cJSON *lv_call_para)
{
	char buff[100];
	cJSON *call_event;
	char  *event_str;
	cJSON *tar_list;
	cJSON *call_tar;
	call_event=cJSON_CreateObject();
	cJSON_AddStringToObject(call_event, EVENT_KEY_EventName,EventBECall);
	cJSON_AddStringToObject(call_event, "CallType","IxMainCall");
	cJSON_AddStringToObject(call_event, "From","Menu");
	call_tar=cJSON_AddObjectToObject(call_event,"Target");
	if(GetJsonDataPro(lv_call_para,"DevNum",buff)==1)
	{
		cJSON_AddStringToObject(call_tar, PB_CALL_TAR_INPUT,buff);
		
		#if 0
		tar_list=GetIxCallTargetByInput(buff);
		if(tar_list==NULL)
		{
			//will_add inform menu:search no dev
			cJSON_Delete(call_event);
			return;
		}
		//cJSON_AddStringToObject(call_tar, PB_CALL_TAR_INPUT,buff);
		cJSON_AddItemToObject(call_tar,PB_CALL_TAR_IxDev,tar_list);
		#endif
	}
	else if(GetJsonDataPro(lv_call_para,"KayPad",buff)==1)
	{
		//tar_list=GetIxCallTargetByInput(buff);
		//if(tar_list==NULL)
		//{
		//	//will_add inform menu:search no dev
		//	cJSON_Delete(call_event);
		//	return;
		//}
		cJSON_AddStringToObject(call_tar, PB_CALL_TAR_INPUT,buff);
		//cJSON_AddItemToObject(call_tar,PB_CALL_TAR_IxDev,tar_list);
	}
	else
	{
		//will_add inform menu:invalid para
		cJSON_Delete(call_event);
		return;
	}
	if(GetJsonDataPro(lv_call_para,"Name",buff)==1)
	{
		cJSON_AddStringToObject(call_tar, PB_CALL_TAR_NAME,buff);
	}
	event_str=cJSON_Print(call_event);
	if(event_str)
	{
		API_Event(event_str);
		free(event_str);
	}
	
	cJSON_Delete(call_event);
}

void Api_LvCallBusiness_CancelCall(void)
{
	char  *event_str;
	cJSON *call_event;
	char* callserverState = cJSON_GetStringValue(API_PublicInfo_Read(PB_CALL_SER_STATE));
	if(!strcmp(callserverState, "Wait"))
	{
		//lv_msg_send(IX850_MSG_CALL_RESULT,"Calling_Finish");
		MainMenu_Process(MAINMENU_MAIN_CallReturn,NULL);
	}
	call_event=cJSON_CreateObject();
	cJSON_AddStringToObject(call_event, EVENT_KEY_EventName,EventBECallClose);
	cJSON_AddStringToObject(call_event, "CallType","IxMainCall");
	cJSON_AddStringToObject(call_event, "From","Menu");

	event_str=cJSON_Print(call_event);
	if(event_str)
	{
		API_Event(event_str);
		free(event_str);
	}
	
	cJSON_Delete(call_event);
	
	
}
//char Calling_Start[]="Calling_Start";
int Menu_DisplayEventCall(cJSON *cmd)
{
    char* eventName = cJSON_GetStringValue(cJSON_GetObjectItemCaseSensitive(cmd, EVENT_KEY_EventName));
    char* callserverStateTemp = cJSON_GetStringValue(API_PublicInfo_Read(PB_CALL_SER_STATE));
    char callserverState[100] = {0};
	snprintf(callserverState, 100, "%s", callserverStateTemp ? callserverStateTemp : "");
    int Call_Part = API_PublicInfo_Read_Bool(PB_CALL_PART);
	 int Call_Sip = API_PublicInfo_Read_Bool(PB_CALL_SIP);
	 int ix2divert = API_PublicInfo_Read_Bool(PB_CALL_IX2Divert);
  
    char roomNumber[10];
    int number;
   
    if(callserverState[0] == 0)
    {
        return 0;
    }
	vtk_lvgl_lock();

    if(eventName && !strcmp(eventName, EventCall))
    {
        char* callEvent = cJSON_GetStringValue(cJSON_GetObjectItemCaseSensitive(cmd, "CALL_EVENT"));
        if(callEvent)
        {
            if(!strcmp(callEvent, "Calling_Start"))
            {
                lv_msg_send(IX850_MSG_CALL_RESULT,"Calling_Start");
            }
            else if(!strcmp(callEvent, "Calling_Busy"))
            {
				MainMenu_Process(MAINMENU_MAIN_CallError,NULL);
            }
            else if(!strcmp(callEvent, "Calling_Succ"))
            {
				MainMenu_Process(MAINMENU_MAIN_ToCall,NULL);
				char* tar_name_temp=API_PublicInfo_Read_String(PB_CALL_TAR_NAME);
				char tar_name[100] = {0};
				snprintf(tar_name, 100, "%s", tar_name_temp ? tar_name_temp : "");
				if(tar_name[0])
				{
					lv_msg_send(IX850_MSG_CALL_INFO,tar_name);
				}
				 if(Call_Sip||ix2divert)
					lv_msg_send(IX850_MSG_DIVERT_INFO,"ON");
            }
			else if(!strcmp(callEvent, "Calling_Error"))
            {
				MainMenu_Process(MAINMENU_MAIN_CallError,NULL);
            }	
            else if(!strcmp(callEvent, "Calling_Cancel"))
            {
                
            }
			else if(!strcmp(callEvent, "Calling_App_Succ"))
			{
				lv_msg_send(IX850_MSG_DIVERT_INFO,"ON");
			}
        }
    }
    else if(callserverState[0])
    {
        if(!strcmp(callserverState, "Wait"))
        {
            MainMenu_Process(MAINMENU_MAIN_CallReturn,NULL);
        }
		else if(Call_Part && !strcmp(callserverState, "Invite"))
        {
        	 char  *ser_type=API_PublicInfo_Read_String(PB_CALL_SER_TYPE);
	 	if(ser_type&&strcmp("VMSipCall",ser_type)==0)
	 	{
	        	MainMenu_Process(MAINMENU_MAIN_ToCall,NULL);
			char* tar_name_temp=API_PublicInfo_Read_String(PB_CALL_TAR_NAME);
			char tar_name[100] = {0};
			snprintf(tar_name, 100, "%s", tar_name_temp ? tar_name_temp : "");
			if(tar_name[0])
			{
				lv_msg_send(IX850_MSG_CALL_INFO,tar_name);
			}
	 	}
           if(Call_Sip||ix2divert)
			lv_msg_send(IX850_MSG_DIVERT_INFO,"ON");
		else 
			lv_msg_send(IX850_MSG_DIVERT_INFO,"OFF");
        }	
        else if(Call_Part && !strcmp(callserverState, "Ring"))
        {
        	if(Call_Sip||ix2divert)
				lv_msg_send(IX850_MSG_DIVERT_INFO,"ON");
			else 
				lv_msg_send(IX850_MSG_DIVERT_INFO,"OFF");
        }
        else if(Call_Part && (!strcmp(callserverState, "Ack") || !strcmp(callserverState, "RemoteAck")))
        {
             if(Call_Sip||ix2divert)
				lv_msg_send(IX850_MSG_CALL_RESULT,"Phone_Talk"); 	
			else
            	lv_msg_send(IX850_MSG_CALL_RESULT,"Calling_Talk");
        }
    }
vtk_lvgl_unlock();
	return 1;
}


#include "obj_MDS_DR.h"
void CodePadDisplay(int mode, char* input, int len)
{
	char buff[40]={0};
	switch(mode)
	{
	case STANDBY:
		//SetCodePadTitleText("Please Input");
		//memset(buff,'*',len);
		//SetCodePadNumText("");
		break;
	case INPUT_PASSWORD:
		//SetCodePadTitleText("Please Input");
		memset(buff,'*',len);
		//SetCodePadNumText(buff);
		break;
	case INPUT_M_PASSWORD:
		//SetCodePadTitleText("Manager Pwd");
		memset(buff,'*',len);
		//SetCodePadNumText(buff);
		break;
	case INPUT_SETTING_CODE:
		#if 0
		//SetCodePadTitleText("Setting Code");
		//memset(buff,'*',len);
		//SetCodePadNumText(input);
		#endif
		//vtk_lvgl_lock();
		scr_settings();
		//vtk_lvgl_unlock();
		break;
	case CHANGE_UNLOCK_PASSWORD:
		if(len==1)
			//SetCodePadTitleText("Unlock1Pwd1");
		if(len==2)
			//SetCodePadTitleText("Unlock1Pwd2");
		if(len==3)
			//SetCodePadTitleText("Unlock2Pwd1");
		if(len==4)
			//SetCodePadTitleText("Unlock2Pwd2");
		//SetCodePadNumText("");
		break;
	case DISPLAY_SETTING_VALUE:
		if(len==1)
			//SetCodePadTitleText("Read Para");
		if(len==2)
			//SetCodePadTitleText("Write Para");
		if(len==3)
			//SetCodePadTitleText("Restore Para");
		if(len==4)
			//SetCodePadTitleText("Read Info");
		//SetCodePadNumText(input);
		break;
		
	}
		
}
int MenuEventLock(cJSON *cmd)
{
    char* name = cJSON_GetStringValue(cJSON_GetObjectItemCaseSensitive(cmd, "name"));	
	// lzh_20240827_s
		printf("----------MenuEventLock----------name[%s]-------------------\n",name);
	if(!strcmp(name, "REMOTE_UNLOCK_OK"))
	{
    	vtk_lvgl_lock();
		lv_msg_send(IX850_MSG_LOCK,"REMOTE_UNLOCK_OK");				// test
		printf("----------REMOTE_UNLOCK_OK------------------\n");
		vtk_lvgl_unlock();			
		return 1;
	}
	else if(!strcmp(name, "REMOTE_UNLOCK_DENY"))
	{
    	vtk_lvgl_lock();
    	lv_msg_send(IX850_MSG_LOCK,"REMOTE_UNLOCK_DENY");			// test
		printf("----------REMOTE_UNLOCK_DENY------------------\n");
		vtk_lvgl_unlock();			
		return 1;
	}
	else if(!strcmp(name, "REMOTE_UNLOCK_ER"))
	{
    	vtk_lvgl_lock();
		lv_msg_send(IX850_MSG_LOCK,"REMOTE_UNLOCK_ER");	// test
		printf("----------REMOTE_UNLOCK_ER------------------\n");
		vtk_lvgl_unlock();			
		return 1;
	}
	// lzh_20240827_e
    if(name && cJSON_IsTrue(cJSON_GetObjectItemCaseSensitive(cmd, "ctrl")))
    {
    	vtk_lvgl_lock();		
        if(!strcmp(name, "RL1"))
        {
    		//DR_UnlockDisplayProcess(DR_UNLOCK1);
    		lv_msg_send(IX850_MSG_LOCK,"Unlock1");
        }
        else if(!strcmp(name, "RL2"))
        {
    		//DR_UnlockDisplayProcess(DR_UNLOCK2);
    		lv_msg_send(IX850_MSG_LOCK,"Unlock2");
        }
	vtk_lvgl_unlock();	
    }

	return 1;
}	

#include "ffmpeg_play.h"
static pthread_mutex_t Playback_lock = PTHREAD_MUTEX_INITIALIZER;
void Playback_CB(int sec, int max);
static int play_back_state=0;
int EventPlayback_Process(cJSON *cmd)
{
	char* eventName = cJSON_GetStringValue(cJSON_GetObjectItemCaseSensitive(cmd, EVENT_KEY_EventName));
	char* playEvent = cJSON_GetStringValue(cJSON_GetObjectItemCaseSensitive(cmd, "PLAY_EVENT"));
	char* file = GetEventItemString(cmd, "PLAY_FILE");
	PlaybackCallBack cb=(PlaybackCallBack)GetEventItemInt(cmd, "CB");
	
	//int im_addr=0;
	
	//int ip = inet_addr(GetEventItemString(cmd,  "SOURCE_IP"));
	//char src_addr[11];
	//GetJsonDataPro(cmd, "SOURCE_ADDR",src_addr);
	//GetJsonDataPro(cmd, "IM_ADDR",&im_addr);
	//char fileTemp[200]= {0};

	//bprintf("111111111\n");
	pthread_mutex_lock(&Playback_lock);
	if(eventName && !strcmp(eventName, EventPlayback))
	{
	//bprintf("111111111\n");
        if(playEvent)
		{
		
			if(!strcmp(playEvent, "Play_Start"))
			{
				//bprintf("111111111\n");
				api_ds_stop_force(0);
				if(  access( file, F_OK )== 0)
				{
					//bprintf("111111111\n");
					play_back_state=1;
					api_start_ffmpegPlay(file, cb);
					//lv_msg_send(MSG_PLAY_MENU_ON, targFile);
					//UpdateRecFileCache(fileTemp);
				}
				else
				{
					//bprintf("111111111\n");
					LV_API_TIPS("get file error!",4);
                   			 //API_TIPS("get file error!");
					//ShellCmdPrintf("EventPlayback__Start file= %s get error!\n", file);
				}
			}
			else if(!strcmp(playEvent, "Play_Stop"))
			{
				if(play_back_state)
				{
					api_stop_ffmpegPlay();
					play_back_state=0;
				}
			}
			else if(!strcmp(playEvent, "Play_Pause"))
			{
				api_ffmpegPlayPause();
			}
			else if(!strcmp(playEvent, "Play_Next"))
			{
				api_ffmpegPlayPause();
				if( access( file, F_OK )== 0)
				{
					//API_RmMsgString("close_tip",NULL);
					api_change_file_ffmpegPlay(file, cb);
					//lv_msg_send(MSG_PLAY_MENU_ON, targFile);
					//UpdateRecFileCache(fileTemp);
				}
				else
				{
					LV_API_TIPS("get file error!",4);
					// API_TIPS("get file error!");
				}
				
			}
            else if(!strcmp(playEvent, "Play_Slider"))
			{
                //int time = GetEventItemInt(cmd, "Play_Time");
                //api_ffmpegPlayFf(time, 1);
            }
		}
	}
	pthread_mutex_unlock(&Playback_lock);
}

void api_playback_event2(char* ctrl, char *file_path,void *call_back)
{
	cJSON *play_event;
	char recfile[200] = {0};
	char ip[100] = {0};
	play_event=cJSON_CreateObject();
	cJSON_AddStringToObject(play_event, EVENT_KEY_EventName,EventPlayback);
	cJSON_AddStringToObject(play_event, "PLAY_EVENT",ctrl);
	if(file_path)
		cJSON_AddStringToObject(play_event, "PLAY_FILE",file_path);
	if(call_back)
		cJSON_AddNumberToObject(play_event, "CB",(int)call_back);
	
	#if 1
	API_Event_Json(play_event);
	#else
	EventPlayback_Process(play_event);
	#endif
	
	cJSON_Delete(play_event);
}
int get_play_back_state(void)

{
	return play_back_state;
}
////////////////////////////////////////////////////////
 
#if 0
cJSON *NameList_Cache_Tb=NULL;
static lv_obj_t** nameListInfo = NULL;
static int nameListEnter=0;
void nameListInfo_init(void)
{
	if(nameListEnter>0&&nameListInfo!=NULL)
	{
		printf("222222%s:%d\n",__func__,__LINE__);
		#if 0
		for(i=0;i<nameListEnter;i++)
		{
			lv_obj_del(nameListInfo[i]);
		}
		#endif
		free(nameListInfo);
		nameListInfo=NULL;
		nameListEnter=0;
		printf("222222%s:%d\n",__func__,__LINE__);
	}
}
void NameList_Reload(lv_obj_t *parent,cJSON *setting)
{
	int i;
	static cJSON *temp_list=NULL;
	cJSON *one_node;
	cJSON *item;
	char buff[100];
	//printf("222222%s:%d\n",__func__,__LINE__);
	if(nameListEnter>0&&nameListInfo!=NULL)
	{
	
		for(i=0;i<nameListEnter;i++)
		{
			//printf("222222%s:%d\n",__func__,__LINE__);
			lv_obj_del(nameListInfo[i]);
		}
		//printf("222222%s:%d\n",__func__,__LINE__);
		free(nameListInfo);
		nameListInfo=NULL;
		nameListEnter=0;
	}
	//printf("222222%s:%d\n",__func__,__LINE__);
	if(temp_list!=NULL)
		cJSON_Delete(temp_list);
	nameListEnter=cJSON_GetArraySize(NameList_Cache_Tb);
	if(nameListEnter==0)
		return;
	temp_list=cJSON_CreateArray();
	nameListInfo=malloc(sizeof(lv_obj_t*)*nameListEnter);
	char *number=get_global_key_string(GLOBAL_ICON_KEYS_NUMBER);
	char *text=get_global_key_string(GLOBAL_ICON_KEYS_STD_TEXT);
	for(i=0;i<nameListEnter;i++)
	{
		item=cJSON_GetArrayItem(NameList_Cache_Tb,i);
		one_node=cJSON_CreateObject();
		cJSON_AddItemToArray(temp_list,one_node);
		
		if(GetJsonDataPro(item,"IX_NAME",buff)==1)
		{
			cJSON_AddStringToObject(one_node,text,buff);
		}
		else
			cJSON_AddStringToObject(one_node,text,"-");
		if(GetJsonDataPro(item,"IX_ADDR",buff)==1)
		{
			cJSON_AddStringToObject(one_node,number,buff);
		}
		else
			cJSON_AddStringToObject(one_node,number,"-");
		 nameListInfo[i]=add_Btn_to_list(parent, setting, one_node);
	}
	//printf("222222%s:%d\n",__func__,__LINE__);
}

cJSON *roomList_Cache_Tb=NULL;
static lv_obj_t** roomListInfo = NULL;
static int roomListEnter=0;
void RoomListInfo_init(void)
{
	if(roomListEnter>0&&roomListInfo!=NULL)
	{
		printf("222222%s:%d\n",__func__,__LINE__);
		#if 0
		for(i=0;i<nameListEnter;i++)
		{
			lv_obj_del(nameListInfo[i]);
		}
		#endif
		free(roomListInfo);
		roomListInfo=NULL;
		roomListEnter=0;
		printf("222222%s:%d\n",__func__,__LINE__);
	}
}
void RoomList_Reload(lv_obj_t *parent,cJSON *setting)
{
	int i;
	static cJSON *temp_list=NULL;
	cJSON *one_node;
	cJSON *item;
	char buff[41];
	char ix_addr[11]={0};
	char name[60];
	//printf("222222%s:%d\n",__func__,__LINE__);
	if(roomListEnter>0&&roomListInfo!=NULL)
	{
	
		for(i=0;i<roomListEnter;i++)
		{
			//printf("222222%s:%d\n",__func__,__LINE__);
			lv_obj_del(roomListInfo[i]);
		}
		//printf("222222%s:%d\n",__func__,__LINE__);
		free(roomListInfo);
		roomListInfo=NULL;
		roomListEnter=0;
	}
	//printf("222222%s:%d\n",__func__,__LINE__);
	if(temp_list!=NULL)
		cJSON_Delete(temp_list);
	roomListEnter=cJSON_GetArraySize(roomList_Cache_Tb);
	if(roomListEnter==0)
		return;
	temp_list=cJSON_CreateArray();
	roomListInfo=malloc(sizeof(lv_obj_t*)*nameListEnter);
	char *number=get_global_key_string(GLOBAL_ICON_KEYS_NUMBER);
	char *text=get_global_key_string(GLOBAL_ICON_KEYS_STD_TEXT);
	for(i=0;i<roomListEnter;i++)
	{
		item=cJSON_GetArrayItem(roomList_Cache_Tb,i);
		one_node=cJSON_CreateObject();
		cJSON_AddItemToArray(temp_list,one_node);
		
		if(GetJsonDataPro(item,"IX_ADDR",ix_addr)==1)
		{
			cJSON_AddStringToObject(one_node,number,ix_addr);
		}
		else
			cJSON_AddStringToObject(one_node,number,"-");

		memcpy(buff,ix_addr+4,4);
		buff[4]=0;
		sprintf(name,"[%d]",atoi(buff));
		
		if(GetJsonDataPro(item,"IX_NAME",buff)==1)
		{
			strcat(name,buff);
			cJSON_AddStringToObject(one_node,text,name);
		}
		else
			cJSON_AddStringToObject(one_node,text,"-");
		
		 roomListInfo[i]=add_Btn_to_list(parent, setting, one_node);
	}
	//printf("222222%s:%d\n",__func__,__LINE__);
}
int TbAlphabeticalOrder(cJSON* tb, const char *key_name )	//czn_20190506
{
	int j, k,i;	   
	char str1[100];
	char str2[100];
	int len1;
	int len2;
	cJSON *node,*node_cmp;


	int size=cJSON_GetArraySize(tb);
	
	if( tb == NULL ||cJSON_GetArraySize(tb)<2||GetJsonDataPro(cJSON_GetArrayItem(tb,0),key_name,str1) == 0)
	{
		return -1;
	}
	
	for (j = 0; j < size - 1; j++) 	//���ѭ������������������Ϊ ptable->record_cnt-1  
	{
		for (k = 0; k < size - 1 - j; k++) 	//�ڲ�ѭ��Ϊ��ǰj���� ����Ҫ�ȽϵĴ���	
		{
			node=cJSON_GetArrayItem(tb,k);
			//len1 = 40;
			//get_one_record_string(&(ptable->precord[k]), key_index, str1, &len1);
			GetJsonDataPro(node,key_name,str1);
			//str1[len1] = 0;
			len1=strlen(str1);
			for(i = 0;i < len1;i++)
			{
				str1[i] = toupper(str1[i]);
			}
			//len2 = 40;
			//get_one_record_string(&(ptable->precord[k+1]), key_index, str2, &len2);
			//str2[len2] = 0;
			node_cmp=cJSON_GetArrayItem(tb,k+1);
			GetJsonDataPro(node_cmp,key_name,str2);
			len2=strlen(str2);
			for(i = 0;i < len2;i++)
			{
				str2[i] = toupper(str2[i]);
			}
			if (strcmp(str1, str2) > 0)
			{
			
				node_cmp=cJSON_DetachItemFromArray(tb,k+1);
				cJSON_InsertItemInArray(tb,k,node_cmp);
			}
		}
	}
	
	
	return 0;
}
void NameList_Update(void)
{

	cJSON *NameList_temp=NULL;
	IXS_ProxyGetTb(&NameList_temp, NULL, R8001AndSearch);

	cJSON* view = cJSON_CreateArray();
	cJSON* where = cJSON_CreateObject();
	cJSON_AddStringToObject(where, "IX_TYPE", "IM");
	cJSON_AddStringToObject(where, "IX_ADDR", GetSysVerInfo_bd());
	//cJSON_AddStringToObject(where, "Platform", "IX1/2");
	API_TB_SelectBySort(NameList_temp, view, where, 0);
	cJSON_Delete(NameList_temp);
	cJSON_Delete(where);
	#if 0
	NameList_temp=view;
	view = cJSON_CreateArray();
	where = cJSON_CreateObject();
	cJSON_AddStringToObject(where, "IX_ADDR", GetSysVerInfo_bd());
	//cJSON_AddStringToObject(where, "Platform", "IX1/2");
	API_TB_SelectBySort(NameList_temp, view, where, 0);
	cJSON_Delete(NameList_temp);
	cJSON_Delete(where);
	#endif
	if(NameList_Cache_Tb!=NULL)
	{
		cJSON_Delete(NameList_Cache_Tb);
		NameList_Cache_Tb=NULL;
	}
	NameList_Cache_Tb=view;
	TbAlphabeticalOrder(NameList_Cache_Tb,"IX_NAME");
	if(roomList_Cache_Tb!=NULL)
	{
		cJSON_Delete(roomList_Cache_Tb);
		roomList_Cache_Tb=NULL;
	}
	roomList_Cache_Tb=cJSON_Duplicate(NameList_Cache_Tb,1);
	TbAlphabeticalOrder(roomList_Cache_Tb,"IX_ADDR");
	//IXS_ProxyGetTb(&NameList_Cache_Tb, NULL, Search);
	 lv_msg_send(IX850_MSG_Namelist_Reload,NULL);
	
}

#endif