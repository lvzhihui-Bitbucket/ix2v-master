#ifndef obj_CommonSelectMenu
#define obj_CommonSelectMenu

#include "cJSON.h"

typedef void (*ChoseCallback)(const char*, void*);

typedef struct 
{
    ChoseCallback callback;
    void* cbData;
}CHOSE_MENU_T;

void CreateChooseMenu(const char* title, const cJSON* choseList, const char* choseString, const CHOSE_MENU_T* userData);
#endif 

