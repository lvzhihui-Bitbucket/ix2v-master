#ifndef _MENU_UTILITY_H_
#define _MENU_UTILITY_H_

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#define OP_TYPE_WINDOW          0       // 0/linux, 1/window

#if OP_TYPE_WINDOW       // window
#include <windows.h>
#include <io.h>
#include "ix850_main.h"
#include "lv_demo_msg.h"
#include "cJSON/cJSON.h"
#else       // linux
#include <dirent.h>
#include "lvgl.h"
#include "cJSON.h"
#endif

#define MENU_TYPE_UNKNOWN       0
#define MENU_TYPE_IMAGE         1
#define MENU_TYPE_STANDARD      2
#define MENU_TYPE_SETTING       3

#define COMMON_ICON_DESCIPTION   "Menu_MainPage.json"
#define COMMON_ICON_RESOURCE     "Icons_Resource"
#define COMMON_MENU_BACKGROUND   "Menu_Background"
#define COMMON_MENU_ENTRY_PAGE   "MenuEntrance"

#define GLOBAL_MENU_KEYS_SCREEN                 0
#define GLOBAL_MENU_KEYS_BACKGROUND             1
#define GLOBAL_MENU_KEYS_TYPE                   2
#define GLOBAL_MENU_KEYS_SRC_HEIGHT             3
#define GLOBAL_MENU_KEYS_SRC_WIDTH              4
#define GLOBAL_MENU_KEYS_PAGE_TITLE             5
#define GLOBAL_MENU_KEYS_PAGE_NAME              6
#define GLOBAL_MENU_KEYS_SIDEBAR                7

#define GLOBAL_MENU_KEYS_ICONLIST               8
#define GLOBAL_ICON_KEYS_TYPE                   9
#define GLOBAL_ICON_KEYS_FUNCTION               10
#define GLOBAL_ICON_KEYS_POS                    11
#define GLOBAL_ICON_KEYS_WIDTH                  12
#define GLOBAL_ICON_KEYS_HEIGHT                 13
#define GLOBAL_ICON_KEYS_POS_X                  14
#define GLOBAL_ICON_KEYS_POS_Y                  15
#define GLOBAL_ICON_KEYS_TEXT                   16
#define GLOBAL_ICON_KEYS_LABEL_TEXT             17
#define GLOBAL_ICON_KEYS_TEXT_COLOR             18
#define GLOBAL_ICON_KEYS_TEXT_SIZE              19
#define GLOBAL_ICON_KEYS_STD_TEXT               20
#define GLOBAL_ICON_KEYS_NUMBER                 21
#define GLOBAL_ICON_KEYS_IO_NAME                22
#define GLOBAL_ICON_KEYS_LINK                   23
#define GOLBAL_ICON_KEYS_RETURN                 24

#define GLOBAL_ICON_KEYS_BACKGROUND_COLOR       25
#define GLOBAL_ICON_KEYS_SYMBOL_SUFFIX          26
#define GLOBAL_ICON_KEYS_SYMBOL_SUFFIX_COLOR    27
#define GLOBAL_ICON_KEYS_SYMBOL_PREFIX          28
#define GLOBAL_ICON_KEYS_SYMBOL_PREFIX_COLOR    29
#define GLOBAL_ICON_KEYS_ICONLIST_STD           30
#define GLOBAL_ICON_KEYS_LAYOUT                 31
#define GLOBAL_ICON_KEYS_SYMBOL_COLOR           32
#define GLOBAL_ICON_KEYS_TEXT_ENABLE            33
#define GLOBAL_ICON_KEYS_CALLBTN_NUMBER         34
#define GLOBAL_ICON_KEYS_CALLBTN_NAME           35
#define GLOBAL_ICON_KEYS_STD_HEIGHT             36
#define GLOBAL_ICON_KEYS_SHODOW_WIDTH           37
#define GLOBAL_ICON_KEYS_BOARD_WIDTH           38
#define GOLBAL_ICON_KEYS_BOARD_COLOR           39
#define GLOBAL_ICON_KEYS_SYMBOL_BACKGROUND_COLOR 40
#define GLOBAL_MENU_KEYS_STYLE                  41
#define GOLBAL_ICON_KEYS_IMAGE                  42

#define GLOBAL_ICON_KEYS_MAX                    43

#define GLOBAL_SYMBOL_KEYS                      11

bool get_files_and_dirs(const char* input_path, cJSON* file_list, cJSON* dir_list);
cJSON* recursion_cJSON_GetObjectItemCaseSensitive(const cJSON* psource, const char* name);
char* get_global_key_string(int index);
cJSON* get_cjson_from_file(const char* filename);
int swapCont(cJSON *contSrc,cJSON *contDest);
void unicodeToUtf8(unsigned int codepoint,unsigned char* bytes); 
#endif
