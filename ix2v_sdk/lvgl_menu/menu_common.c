#include "menu_common.h"

const lv_font_t* font_small;
const lv_font_t* font_medium;
const lv_font_t* font_larger;

LV_FONT_DECLARE(verdana_20);

#if !defined( PID_IX622 )	&&!defined( PID_IX821 )	
LV_FONT_DECLARE(verdana_26);
LV_FONT_DECLARE(verdana_32);
#endif

bool create_one_menu(MENU_INS* pmenu, lv_obj_t* screen);
static struct ICON_INS* get_one_icon(PAGE_INS* ppage, char* icon_name);
bool add_one_icon(PAGE_INS* ppage, struct ICON_INS* picon);
bool del_one_icon(PAGE_INS* ppage, char* icon_name);
bool mod_one_icon(PAGE_INS* ppage, struct ICON_INS* picon);

bool load_icon_list(PAGE_INS* ppage, const cJSON* piconlist, const char* icon_img_path);
bool save_icon_list(PAGE_INS* ppage, cJSON* piconlist);

lv_style_t style_main_menu;
lv_style_t style_menu_page;
lv_style_t style_menu_bkgd;

lv_style_t style_icon_callbtn;
lv_style_t style_icon_function;
lv_style_t style_menu_cont;
lv_style_t style_menu_item_text;
lv_style_t style_menu_item_text_checked;
lv_style_t style_menu_item_btn;
lv_style_t style_arc_item_btn;

lv_style_t style_freetype_small;
lv_style_t style_freetype_normal;
lv_style_t style_freetype_large;
lv_style_t style_freetype_small2;
lv_style_t style_freetype_normal2;
lv_style_t style_freetype_large2;

lv_ft_info_t ft_0;
lv_ft_info_t ft_1;
lv_ft_info_t ft_2;
lv_ft_info_t ft_3;
lv_ft_info_t ft_4;
lv_ft_info_t ft_5;
lv_ft_info_t ft_6;
lv_ft_info_t ft_7;
lv_ft_info_t ft_8;
lv_ft_info_t ft_9;
lv_ft_info_t ft_10;

lv_ft_info_t ft_small;
lv_ft_info_t ft_normal;
lv_ft_info_t ft_large;
lv_ft_info_t ft_xlarge;

#define TTF_FONT_PATH   "/mnt/nand1-1/App/res/unicode_font1.ttf"

void font_load(void)
{
    /*Create a font*/
    void* font_memo = NULL;
    int   font_size = 0;
    FILE* file;
	if((file=fopen(TTF_FONT_PATH,"r")) != NULL)
	{
		fseek(file,0, SEEK_END);		
		font_size = ftell(file);		
		if((font_memo = malloc(font_size)) == NULL )
		{
			fclose(file);
		}
        else
        {
            fseek(file,0, SEEK_SET);            
            fread(font_memo, 1, font_size, file);
            fclose(file);
        }
	}
    
    ft_0.name = TTF_FONT_PATH; // arial.ttf";
    ft_0.weight = 12;
    ft_0.style = FT_FONT_STYLE_NORMAL;
    ft_0.mem = font_memo; //NULL;
    ft_0.mem_size = font_size;
    if (!lv_ft_font_init(&ft_0)) {
        LV_LOG_ERROR("create failed.");
        printf("create failed.");
    }

    /*FreeType uses C standard file system, so no driver letter is required.*/
    ft_1.name = TTF_FONT_PATH; // arial.ttf";
    ft_1.weight = 20;
    ft_1.style = FT_FONT_STYLE_NORMAL;
    ft_1.mem = font_memo; //NULL;
    ft_1.mem_size = font_size;
    if (!lv_ft_font_init(&ft_1)) {
        LV_LOG_ERROR("create failed.");
        printf("create failed.");
    }
    ft_2.name = TTF_FONT_PATH; // arial.ttf";
    ft_2.weight = 24;
    ft_2.style = FT_FONT_STYLE_NORMAL;
    ft_2.mem = font_memo; //NULL;
    ft_2.mem_size = font_size;
    if (!lv_ft_font_init(&ft_2)) {
        LV_LOG_ERROR("create failed.");
        printf("create failed.");
    }
	#if 0
    ft_3.name = TTF_FONT_PATH; // arial.ttf";
    ft_3.weight = 28;
    ft_3.style = FT_FONT_STYLE_NORMAL;
    ft_3.mem = font_memo; //NULL;
    ft_3.mem_size = font_size;
    if (!lv_ft_font_init(&ft_3)) {
        LV_LOG_ERROR("create failed.");
        printf("create failed.");
    }
	#endif
    ft_4.name = TTF_FONT_PATH; // arial.ttf";
    ft_4.weight = 32;
    ft_4.style = FT_FONT_STYLE_NORMAL;
    ft_4.mem = font_memo; //NULL;
    ft_4.mem_size = font_size;
    if (!lv_ft_font_init(&ft_4)) {
        LV_LOG_ERROR("create failed.");
        printf("create failed.");
    }
	#if 0
    ft_5.name = TTF_FONT_PATH; // arial.ttf";
    ft_5.weight = 36;
    ft_5.style = FT_FONT_STYLE_NORMAL;
    ft_5.mem = font_memo; //NULL;
    ft_5.mem_size = font_size;
    if (!lv_ft_font_init(&ft_5)) {
        LV_LOG_ERROR("create failed.");
        printf("create failed.");
    }
    ft_6.name = TTF_FONT_PATH; // arial.ttf";
    ft_6.weight = 40;
    ft_6.style = FT_FONT_STYLE_NORMAL;
    ft_6.mem = font_memo; //NULL;
    ft_6.mem_size = font_size;
    if (!lv_ft_font_init(&ft_6)) {
        LV_LOG_ERROR("create failed.");
        printf("create failed.");
    }
    ft_7.name = TTF_FONT_PATH; // arial.ttf";
    ft_7.weight = 44;
    ft_7.style = FT_FONT_STYLE_NORMAL;
    ft_7.mem = font_memo; //NULL;
    ft_7.mem_size = font_size;
    if (!lv_ft_font_init(&ft_7)) {
        LV_LOG_ERROR("create failed.");
        printf("create failed.");
    }
	#endif
    ft_8.name = TTF_FONT_PATH; // arial.ttf";
    ft_8.weight = 48;
    ft_8.style = FT_FONT_STYLE_NORMAL;
    ft_8.mem = font_memo; //NULL;
    ft_8.mem_size = font_size;
    if (!lv_ft_font_init(&ft_8)) {
        LV_LOG_ERROR("create failed.");
        printf("create failed.");
    }
	#if 0
    ft_9.name = TTF_FONT_PATH; // arial.ttf";
    ft_9.weight = 52;
    ft_9.style = FT_FONT_STYLE_NORMAL;
    ft_9.mem = font_memo; //NULL;
    ft_9.mem_size = font_size;
    if (!lv_ft_font_init(&ft_9)) {
        LV_LOG_ERROR("create failed.");
        printf("create failed.");
    }
	#endif
    ft_10.name = TTF_FONT_PATH; // arial.ttf";
    ft_10.weight = 100;
    ft_10.style = FT_FONT_STYLE_NORMAL;
    ft_10.mem = font_memo; //NULL;
    ft_10.mem_size = font_size;
    if (!lv_ft_font_init(&ft_10)) {
        LV_LOG_ERROR("create failed.");
        printf("create failed.");
    }
}

void font_deinit(void)	
{
	lv_ft_font_destroy(ft_1.font);
	lv_ft_font_destroy(ft_2.font);
	//lv_ft_font_destroy(ft_3.font);
	lv_ft_font_destroy(ft_4.font);
	//lv_ft_font_destroy(ft_5.font);
	//lv_ft_font_destroy(ft_6.font);
	//lv_ft_font_destroy(ft_7.font);
	lv_ft_font_destroy(ft_8.font);
	//lv_ft_font_destroy(ft_9.font);
	lv_ft_font_destroy(ft_10.font);
	free( ft_10.mem);
}
void font_init(void)
{
    font_load();
    int mode = 1;
    switch(mode){
        case 1:
            ft_small.font = ft_2.font;
            ft_normal.font = ft_4.font;
            ft_large.font = ft_8.font;
            ft_xlarge.font = ft_10.font;
            break;
        case 2:
            ft_small.font = ft_1.font;
            ft_normal.font = ft_3.font;
            ft_large.font = ft_6.font;
            ft_xlarge.font = ft_10.font;
            break;
        default:
            break;
    }
}
lv_font_t * getfont(int index)
{
	return  ft_normal.font;
}
void global_style_init(void)
{
    font_small = &verdana_20;
#if !defined( PID_IX622 )	&&!defined( PID_IX611 )&&!defined( PID_IX821 )	 
    font_medium = &verdana_26;
    font_larger = &verdana_32;  
#endif    
    /*Create style with the new font*/
    lv_style_init(&style_freetype_small);
    lv_style_set_text_font(&style_freetype_small, ft_small.font);
    lv_style_set_text_align(&style_freetype_small, LV_TEXT_ALIGN_CENTER);
    lv_style_init(&style_freetype_normal);
    lv_style_set_text_font(&style_freetype_normal, ft_normal.font);
    lv_style_set_text_align(&style_freetype_normal, LV_TEXT_ALIGN_CENTER);
    lv_style_init(&style_freetype_large);
    lv_style_set_text_font(&style_freetype_large, ft_large.font);
    lv_style_set_text_align(&style_freetype_large, LV_TEXT_ALIGN_CENTER);

    lv_style_init(&style_freetype_small2);
    lv_style_set_text_font(&style_freetype_small2, ft_small.font);
    lv_style_set_text_align(&style_freetype_small2, LV_TEXT_ALIGN_LEFT);
    lv_style_init(&style_freetype_normal2);
    lv_style_set_text_font(&style_freetype_normal2, ft_normal.font);
    lv_style_set_text_align(&style_freetype_normal2, LV_TEXT_ALIGN_LEFT);
    lv_style_init(&style_freetype_large2);
    lv_style_set_text_font(&style_freetype_large2, ft_large.font);
    lv_style_set_text_align(&style_freetype_large2, LV_TEXT_ALIGN_LEFT);

    lv_style_init(&style_arc_item_btn);
    lv_style_set_bg_color(&style_arc_item_btn, lv_color_hex(0xff0000));
    lv_style_set_pad_all(&style_arc_item_btn, 0);
    lv_style_set_border_width(&style_arc_item_btn, 0);
    lv_style_set_border_color(&style_arc_item_btn, lv_color_black());
    //
    lv_style_init(&style_icon_function);
    lv_style_set_bg_opa(&style_icon_function, LV_OPA_0);
    //
    lv_style_init(&style_icon_callbtn);
    lv_style_set_border_width(&style_icon_callbtn, 0);
    lv_style_set_border_side(&style_icon_callbtn, 0);
    lv_style_set_border_opa(&style_icon_callbtn, LV_OPA_100);
    lv_style_set_border_color(&style_icon_callbtn, lv_color_black());
    //
    lv_style_init(&style_main_menu);
    lv_style_set_pad_all(&style_main_menu, 0);
    lv_style_set_pad_gap(&style_main_menu, 0);
    lv_style_set_border_width(&style_main_menu, 0);
    lv_style_set_radius(&style_main_menu, 0);
    lv_style_set_bg_color(&style_main_menu, lv_color_hex(0xaaaaaa));
    //lv_style_set_text_font(&style_main_menu, info1.font);
    //lv_style_set_text_font(&style_main_menu, font_larger);
    //
    lv_style_init(&style_menu_page);
    lv_style_set_pad_all(&style_menu_page, 0);
    lv_style_set_border_width(&style_menu_page, 0);
    lv_style_set_border_side(&style_menu_page, 0);
    lv_style_set_bg_color(&style_menu_page, lv_color_hex(0xa1a0a2));
    lv_style_set_border_opa(&style_menu_page, LV_OPA_COVER);
    lv_style_set_border_color(&style_menu_page, lv_color_black());
    lv_style_set_radius(&style_menu_page, 0);
    //
    lv_style_init(&style_menu_bkgd);
    lv_style_set_pad_all(&style_menu_bkgd, 0);
    lv_style_set_border_width(&style_menu_bkgd, 0);
    lv_style_set_border_side(&style_menu_bkgd, 0);
    lv_style_set_radius(&style_menu_bkgd, 0);
    lv_style_set_bg_color(&style_menu_bkgd, lv_color_hex(0));
    //
    lv_style_init(&style_menu_cont);
    lv_style_set_pad_all(&style_menu_cont, 1);
    lv_style_set_border_width(&style_menu_cont, 0);
    lv_style_set_border_side(&style_menu_cont, 0);
    lv_style_set_radius(&style_menu_cont, 0);
    //lv_style_set_bg_color(&style_menu_cont, lv_color_hex(0x00ff00));
    //    
    lv_style_init(&style_menu_item_btn);
    lv_style_set_bg_color(&style_menu_item_btn, lv_color_hex(0x818082));
    lv_style_set_pad_all(&style_menu_item_btn, 0);
    lv_style_set_border_width(&style_menu_item_btn, 0);
    lv_style_set_border_color(&style_menu_item_btn, lv_color_black());
    //
    lv_style_init(&style_menu_item_text);
    lv_style_set_bg_color(&style_menu_item_text, lv_color_hex(0x818082));
    lv_style_set_pad_all(&style_menu_item_text, 0);
    lv_style_set_border_width(&style_menu_item_text, 0);
    lv_style_set_border_color(&style_menu_item_text, lv_color_black());

    lv_style_init(&style_menu_item_text_checked);
    lv_style_set_bg_color(&style_menu_item_text_checked, lv_color_hex(0x0000ff));
    lv_style_set_pad_all(&style_menu_item_text_checked, 0);
    lv_style_set_border_width(&style_menu_item_text_checked, 0);
    lv_style_set_border_color(&style_menu_item_text_checked, lv_color_black());
    
}

struct ICON_INS* create_one_icon(cJSON* icon_data)
{
    struct ICON_INS* picon = lv_mem_alloc(sizeof(struct ICON_INS));
    picon->icon_data = cJSON_Duplicate(icon_data, true);
    picon->icon_cont = NULL;
    picon->icon_owner = NULL;
    return picon;
}

void destroy_one_icon(struct ICON_INS* picon)
{
    if (picon)
    {
        if (picon->icon_data)
        {
            cJSON_Delete(picon->icon_data);
            picon->icon_data = NULL;           
        }
        if (picon->icon_cont)
        {
            lv_obj_del(picon->icon_cont);
            picon->icon_cont = NULL;
        }       
        lv_mem_free(picon);
        picon = NULL;
    }
}

static lv_obj_t* create_one_menu_obj(lv_obj_t* parent, bool root_back_btn_en)
{
    lv_obj_t* menu = lv_menu_create(parent);
    lv_menu_set_mode_root_back_btn(menu, LV_MENU_ROOT_BACK_BTN_DISABLED);
    lv_obj_set_size(menu, 480, 800);
    lv_obj_add_style(menu, &style_main_menu, 0);
    lv_obj_align(menu, LV_ALIGN_TOP_LEFT, 0, 0);
    return menu;
}

/*******************************************************************************************
* ���ݹ�������api
********************************************************************************************/
/*
function:   ����һ���˵�Ҳ
Para:      page_type   - �˵�ҳ����
para:      file_name   - �˵�ҳ�����ļ���(���浽�˵�ҳ)
para:      bkgd_path   - �˵�ҳ�ı�����ͼ��ԴĿ¼
para:      icon_path   - �˵�ҳ��icon��ͼ��ԴĿ¼
para:      page_info   - �˵�ҳinfo
para:      icon_info   - �˵�ҳiconlist
para:      parent      - iconlist�ĸ�����
return:     ����һ���˵�ʵ��
*/

PAGE_INS* load_one_page(int page_type, char* file_name, char* bkgd_path, char* icon_path, cJSON* page_info, cJSON* icon_info, MENU_INS* pmenu)
{
    char fullname[200];
    PAGE_INS* ppage = NULL;

    lv_obj_t* parent = pmenu->menu_root;

    // initial menu background
    ppage = (PAGE_INS*)lv_mem_alloc(sizeof(PAGE_INS));
    lv_memset_00(ppage, sizeof(PAGE_INS));
	char *str=cJSON_Print(page_info);
	printf("%s:\n%s\n",__func__,str);
	free(str);
    if (ppage)
    {
        ppage->page_owner = pmenu;
        // save type
        ppage->page_type = page_type;
        // save info
        ppage->page_info = cJSON_Duplicate(page_info,true);
        // save res file
        ppage->res_file = lv_mem_alloc(strlen(file_name) + 1);
        memset(ppage->res_file, 0, strlen(file_name) + 1);
        strcpy(ppage->res_file, file_name);

        cJSON* json_page_title = cJSON_GetObjectItem(ppage->page_info, get_global_key_string(GLOBAL_MENU_KEYS_PAGE_TITLE));
        cJSON* json_page_name = cJSON_GetObjectItem(ppage->page_info, get_global_key_string(GLOBAL_MENU_KEYS_PAGE_NAME));
        cJSON* json_sidebar = cJSON_GetObjectItem(ppage->page_info, get_global_key_string(GLOBAL_MENU_KEYS_SIDEBAR));
        cJSON* w = cJSON_GetObjectItem(ppage->page_info, get_global_key_string(GLOBAL_MENU_KEYS_SRC_WIDTH));
        cJSON* h = cJSON_GetObjectItem(ppage->page_info, get_global_key_string(GLOBAL_MENU_KEYS_SRC_HEIGHT));
        cJSON* b = cJSON_GetObjectItem(ppage->page_info, get_global_key_string(GLOBAL_MENU_KEYS_BACKGROUND));
        cJSON* bg_color = cJSON_GetObjectItem(ppage->page_info, get_global_key_string(GLOBAL_ICON_KEYS_BACKGROUND_COLOR));

        // check page sidebar
        if (cJSON_IsBool(json_sidebar) && cJSON_IsTrue(json_sidebar))
        {
            printf("------------load_one_page, just page_sidebar-----------------\n");
            ppage->page_sidebar = true;
        }
        else
            ppage->page_sidebar = false;

        // create page obj or common obj
        if (json_page_title == NULL || (strlen(json_page_title->valuestring) == 0))
            ppage->page_obj = lv_menu_page_create(parent, NULL);    // page head hidden
        else
            ppage->page_obj = lv_menu_page_create(parent, json_page_title->valuestring);

        if (ppage->page_sidebar)
        {
            lv_obj_set_style_pad_hor(ppage->page_obj, lv_obj_get_style_pad_left(lv_menu_get_main_header(parent), 0), 0);
            lv_menu_separator_create(ppage->page_obj);
        }
        lv_obj_add_style(ppage->page_obj, &style_menu_page, 0);

        // init menu and background
        if(ppage->page_type != MENU_TYPE_SETTING)
        {
            lv_obj_clear_flag(ppage->page_obj, LV_OBJ_FLAG_SCROLLABLE);
            lv_obj_t* bkgd_cont = lv_menu_cont_create(ppage->page_obj);
            lv_obj_add_style(bkgd_cont, &style_menu_bkgd, 0);
            lv_obj_clear_flag(bkgd_cont, LV_OBJ_FLAG_SCROLLABLE);           
               
            ppage->page_bkgd = lv_obj_create(bkgd_cont);
            if (w && h)
                lv_obj_set_size(ppage->page_bkgd, w->valueint, h->valueint);
            else
                lv_obj_set_size(ppage->page_bkgd, lv_disp_get_hor_res(NULL), lv_disp_get_ver_res(NULL));
            if (b)
            {
#if OP_TYPE_WINDOW       // window
                sprintf(fullname, "%s/%s", bkgd_path, b->valuestring);
#else
                sprintf(fullname, "S:%s/%s", bkgd_path, b->valuestring);
#endif                
                lv_obj_t* bkgd_img = lv_img_create(ppage->page_bkgd);
                lv_img_set_src(bkgd_img, fullname);
            }
            lv_obj_add_style(ppage->page_bkgd, &style_menu_bkgd, 0);
            if(bg_color){
                lv_obj_set_style_bg_color(ppage->page_bkgd, lv_color_hex(bg_color->valueint), LV_PART_MAIN);
                //printf(" --------------------------------    bg_color = %d\n",bg_color->valuedouble);
            }
            if(ppage->page_type != MENU_TYPE_STANDARD) lv_obj_clear_flag(ppage->page_bkgd, LV_OBJ_FLAG_SCROLLABLE);  
        }
        else
        {
            ppage->page_bkgd = NULL;
        }
        // create icon list
        ppage->page_icons = cJSON_CreateObject();

        load_icon_list(ppage, icon_info, icon_path);
    }
    return ppage;
}

bool dele_one_page(PAGE_INS* ppage)
{
    int i, count;
    if (ppage)
    {
        if (ppage->res_file)
        {
            lv_mem_free(ppage->res_file);
            ppage->res_file = NULL;
        }
        if (ppage->page_info)
        {
            cJSON_Delete(ppage->page_info);
            ppage->page_info = NULL;
        }
        #if 1
        if (ppage->page_icons)
        {
            count = cJSON_GetArraySize(ppage->page_icons);
            for (i = 0; i < count; i++)
            {
                cJSON* piconjson = cJSON_GetArrayItem(ppage->page_icons, i);
                struct ICON_INS* picon = (struct ICON_INS*)piconjson->valueint;
                destroy_one_icon(picon);
            }
        }
        #endif
        if (ppage->page_obj)
        {
            lv_obj_del(ppage->page_obj);
            ppage->page_obj = NULL;
        }
        lv_mem_free(ppage);
        ppage = NULL;
        return true;
    }
    else
        return false;
}

bool save_one_page(PAGE_INS* ppage)
{
    int i, size;
    FILE* f;
    
    cJSON* json_file = cJSON_CreateObject();
    cJSON_AddItemToObject(json_file, get_global_key_string(GLOBAL_MENU_KEYS_SCREEN), cJSON_Duplicate(ppage->page_info, true));
    cJSON* piconlist = cJSON_CreateObject();
    size = cJSON_GetArraySize(ppage->page_icons);
    for (i = 0; i < size; i++)
    {
        cJSON* piconjson = cJSON_GetArrayItem(ppage->page_icons, i);
        struct ICON_INS* picon = (struct ICON_INS*)piconjson->valueint;
        cJSON_AddItemToObject(piconlist, picon->icon_data->string, cJSON_Duplicate(picon->icon_data, true));
    }
    cJSON_AddItemToObject(json_file, get_global_key_string(GLOBAL_MENU_KEYS_ICONLIST), piconlist);
    // save to file
    f = fopen(ppage->res_file, "w");
    if (f == NULL)
    {
        printf("Can't open file\n");
        return false;
    }
    char* pjsonbuf = cJSON_Print(json_file);
    fseek(f, 0, SEEK_SET);
    fwrite(pjsonbuf, 1, strlen(pjsonbuf) + 1, f);
    fclose(f);
    free(pjsonbuf);
    cJSON_Delete(json_file);
    return true;
}

MENU_INS* create_menu_ins_from_files(char* res_path)
{
    int i, size;
    cJSON* files;
    cJSON* dirs;
    cJSON* jsontemp;
    char* strtemp;
    MENU_INS* pmenu;
    char fullname[200];

#if OP_TYPE_WINDOW
    sprintf(fullname, "%s/*", res_path);
#else
    sprintf(fullname, "%s", res_path);
#endif
    // get resource file and dir
    files = cJSON_CreateObject();
    dirs = cJSON_CreateObject();
    if (get_files_and_dirs(fullname, files, dirs) == false)
    {
        cJSON_Delete(files);
        cJSON_Delete(dirs);
        return NULL;
    }

    cJSON_Print(files);
    cJSON_Print(dirs);

    size = cJSON_GetArraySize(files);
    if (size == 0)
    {
        cJSON_Delete(files);
        cJSON_Delete(dirs);
        return NULL;
    }

    size = cJSON_GetArraySize(dirs);
    if (size == 0)
    {
        cJSON_Delete(files);
        cJSON_Delete(dirs);
        return NULL;
    }

    // initial menu background
    pmenu = (MENU_INS*)lv_mem_alloc(sizeof(MENU_INS));
    lv_memset_00(pmenu, sizeof(MENU_INS));
    if (pmenu)
    {
        // create resc path
        sprintf(fullname, "%s", res_path);
        pmenu->res_root = lv_mem_alloc(strlen(fullname) + 1);
        strcpy(pmenu->res_root, fullname);
        sprintf(fullname, "%s%s", res_path, COMMON_ICON_RESOURCE);
        pmenu->res_icon = lv_mem_alloc(strlen(fullname) + 1);
        strcpy(pmenu->res_icon, fullname);
        pmenu->res_bkgd = lv_mem_alloc(strlen(fullname) + 1);
        sprintf(fullname, "%s%s", res_path, COMMON_MENU_BACKGROUND);
        strcpy(pmenu->res_bkgd, fullname);
        //
        pmenu->res_main = NULL;
        pmenu->res_pages = cJSON_CreateObject();
        size = cJSON_GetArraySize(files);
        for (i = 0; i < size; i++)
        {
            jsontemp = cJSON_GetArrayItem(files, i);
            strtemp = jsontemp->valuestring;
            if (strcmp(strtemp, COMMON_ICON_DESCIPTION) == 0)
                pmenu->res_main = cJSON_CreateString(strtemp);
            else if (strcmp(strtemp, "Project_Description.json") == 0)
                ;
            else
                cJSON_AddItemToArray(pmenu->res_pages, cJSON_CreateString(strtemp));
        }
    }
    cJSON_Delete(files);
    cJSON_Delete(dirs);

    return pmenu;
}

/*
ppage->page_obj -- menu_page
ppage->page_obj->parent->parent -- menu obj
*/
PAGE_INS* get_menu_page_obj(MENU_INS* pmenu, char* page_name)
{
    int i, page_cnt;
    cJSON* json_page_data;
    PAGE_INS* ppage;

    page_cnt = cJSON_GetArraySize(pmenu->menu_pages);
    for (i = 0; i < page_cnt; i++)
    {
        json_page_data = cJSON_GetArrayItem(pmenu->menu_pages, i);
        if (strcmp(json_page_data->string, page_name) == 0)
        {
            ppage = (PAGE_INS*)json_page_data->valueint;
            return ppage;
        }
    }
    return NULL;
}

/*
*/
bool one_menu_layout(MENU_INS* pmenu)
{
    int i, j, page_cnt, icon_cnt;
    cJSON* json_page_data;
    cJSON* json_icon_data;
    cJSON* json_icon_link;
    PAGE_INS* ppage;
    PAGE_INS* ppage_main = NULL;
    struct ICON_INS* picon;

    page_cnt = cJSON_GetArraySize(pmenu->menu_pages);
    for (i = 0; i < page_cnt; i++)
    {
        json_page_data = cJSON_GetArrayItem(pmenu->menu_pages, i);
        if(json_page_data == NULL)
            continue;
        ppage = (PAGE_INS*)json_page_data->valueint;
        icon_cnt = cJSON_GetArraySize(ppage->page_icons);

        for (j = 0; j < icon_cnt; j++)
        {
            json_icon_data = cJSON_GetArrayItem(ppage->page_icons, j);
            if(json_icon_data == NULL)
                continue;

            picon = (struct ICON_INS*)json_icon_data->valueint;
            json_icon_link = recursion_cJSON_GetObjectItemCaseSensitive(picon->icon_data, get_global_key_string(GLOBAL_ICON_KEYS_LINK));
            if (json_icon_link)
            {
                PAGE_INS* target = get_menu_page_obj(pmenu, json_icon_link->valuestring);
                if (target )     // target point to menu_page,target->parent->parent point to menu_root
                {
                    // sidebar mode: from "NOT setting menu" link to "setting menu", NOT to set load page event
                    if ( ppage->page_type != MENU_TYPE_SETTING && target->page_type == MENU_TYPE_SETTING && target->page_sidebar )
                        continue;
                    lv_menu_set_load_page_event(pmenu->menu_root, picon->icon_cont, target->page_obj);
                    
                }
            }
        }
    }
    return true;
}

/*
function:   ������Դ·��������Ļ��Ϣ������ʼ��һ����Ļ���������ص�ͼ
para1:      res_path - ��Դ�ļ���·��
Para2:      screen - ���ص���Ļ����
return:     ����һ���˵�ʵ��
*/
MENU_INS* load_one_menu(char* res_path, lv_obj_t* screen)
{
    MENU_INS* pmenu = create_menu_ins_from_files(res_path);
    if (pmenu == NULL)
        return NULL;

    create_one_menu(pmenu, screen);

    return pmenu;
}

PAGE_INS* create_one_page_ins(MENU_INS* pmenu, cJSON* json_file, char* res_root, char* res_bkgd, char* res_icon)
{
    int menu_type;
    cJSON* json_data;
    cJSON* page_info;
    cJSON* icon_info;
    cJSON* json_page_type;
    cJSON* menu_style;
    PAGE_INS* ppage;
    char fullname[240];

    if (json_file == NULL)
        return NULL;
	
    sprintf(fullname, "%s/%s", res_root, json_file->valuestring);
	//printf("11111%s:%d:%s\n",__func__,__LINE__,fullname);
    
    json_data = get_cjson_from_file(fullname);

    if (json_data == NULL)
        return NULL;

    page_info = cJSON_GetObjectItem(json_data, get_global_key_string(GLOBAL_MENU_KEYS_SCREEN));
    if (page_info == NULL)
    {
        cJSON_Delete(json_data);
        return NULL;
    }

    icon_info = cJSON_GetObjectItem(json_data, get_global_key_string(GLOBAL_MENU_KEYS_ICONLIST));
    if (icon_info == NULL)
    {
        cJSON_Delete(json_data);
        return NULL;
    }

    // get menu type
    json_page_type = cJSON_GetObjectItem(page_info, get_global_key_string(GLOBAL_MENU_KEYS_TYPE));
    if (strcmp(json_page_type->valuestring, "image") == 0)
        menu_type = MENU_TYPE_IMAGE;
    else if (strcmp(json_page_type->valuestring, "standard") == 0)
        menu_type = MENU_TYPE_STANDARD;
    else if (strcmp(json_page_type->valuestring, "setting") == 0)
        menu_type = MENU_TYPE_SETTING;
    else
        menu_type = MENU_TYPE_UNKNOWN;


    menu_style = cJSON_GetObjectItem(page_info, get_global_key_string(GLOBAL_MENU_KEYS_STYLE));    
    if (menu_style != NULL)
    {
        if(!strcmp(cJSON_GetStringValue(menu_style), "black"))
        {
            //API_PublicInfo_Write_Int(PB_MENU_THEME_MODE, 0);
        }
        else
        {
            //API_PublicInfo_Write_Int(PB_MENU_THEME_MODE, 1);
        }
    }

    ppage = load_one_page(menu_type, fullname, res_bkgd, res_icon, page_info, icon_info, pmenu);

    cJSON_Delete(json_data);

    return ppage;
}

/*
function:   ���ݲ˵�ʵ������һ���˵�
para1:      res_path - ��Դ�ļ���·��
Para2:      screen - ���ص���Ļ����
return:     ����һ���˵�ʵ��
*/
bool create_one_menu(MENU_INS* pmenu, lv_obj_t * screen)
{
    int i, size;
    cJSON* json_file;
    PAGE_INS* ppage;
    cJSON* json_page_name;

    // create menu
    pmenu->menu_screen = screen;
    // first create one menu
    pmenu->menu_root = create_one_menu_obj(pmenu->menu_screen, false);
    // create pages list
    pmenu->menu_pages = cJSON_CreateObject();

    // �������˵�ҳ
    ppage = create_one_page_ins(pmenu, pmenu->res_main, pmenu->res_root, pmenu->res_bkgd, pmenu->res_icon);
    if (ppage)
    {
        // set main page
        lv_menu_set_page(pmenu->menu_root, ppage->page_obj);
        // get menu name
        json_page_name = cJSON_GetObjectItem(ppage->page_info, get_global_key_string(GLOBAL_MENU_KEYS_PAGE_NAME));
        // store ins to menu_pages
        cJSON_AddNumberToObject(pmenu->menu_pages, json_page_name->valuestring, (int)ppage);
        ppage->page_owner = pmenu;
    }

    // ���������˵�ҳ
    size = cJSON_GetArraySize(pmenu->res_pages);
    //printf("create_one_menu  -------------------------- size = %d\n",size);
    for (i = 0; i < size; i++)
    {
        // get menu file name and menu info
        json_file = cJSON_GetArrayItem(pmenu->res_pages, i);
        ppage = create_one_page_ins(pmenu, json_file, pmenu->res_root, pmenu->res_bkgd, pmenu->res_icon);
        if (ppage)
        {
            // get menu name
            json_page_name = cJSON_GetObjectItem(ppage->page_info, get_global_key_string(GLOBAL_MENU_KEYS_PAGE_NAME));
            // store ins to menu_pages
            cJSON_AddNumberToObject(pmenu->menu_pages, json_page_name->valuestring, (int)ppage);
            ppage->page_owner = pmenu;
        }
    }

    // create page link relation
    return one_menu_layout(pmenu);
}

/*
function:   ɾ��һ���˵�
para1:      pmenu - �˵�ʵ��ָ��
return:     true/ok�� false/err
*/
bool dele_one_menu(MENU_INS* pmenu)
{
    int i, count;
    if (pmenu)
    {
        if (pmenu->menu_screen)
        {
            //lv_obj_clean(pmenu->menu_screen);
            pmenu->menu_screen = NULL;
        }
        if (pmenu->res_root)
        {
            lv_mem_free(pmenu->res_root);
            pmenu->res_root = NULL;
        }
        if (pmenu->res_icon)
        {
            lv_mem_free(pmenu->res_icon);
            pmenu->res_icon = NULL;
        }
        if (pmenu->res_bkgd)
        {
            lv_mem_free(pmenu->res_bkgd);
            pmenu->res_bkgd = NULL;
        }
        if (pmenu->menu_pages)
        {
            count = cJSON_GetArraySize(pmenu->menu_pages);
            for (i = 0; i < count; i++)
            {
                cJSON* piconjson = cJSON_GetArrayItem(pmenu->menu_pages, i);
                PAGE_INS* ppage = (PAGE_INS*)piconjson->valueint;
                //printf("dele_one_menu  1111111111111   \n");
                dele_one_page(ppage);
            }
        }        
        if (pmenu->res_main)
        {
            cJSON_Delete(pmenu->res_main);
            pmenu->res_main = NULL;
        }
        if (pmenu->res_pages)
        {
            cJSON_Delete(pmenu->res_pages);
            pmenu->res_pages = NULL;
        }
        lv_mem_free(pmenu);
        pmenu = NULL;
        return true;
    }
    else
        return false;
}

/*
function��   ����һ���˵�����
para1:      pmenu - �˵�ʵ��ָ��
return:     true/ok�� false/err
*/
bool save_one_menu(MENU_INS* pmenu)
{
    bool ret = false;
    if (pmenu->menu_pages)
    {
        int i,size;
        size = cJSON_GetArraySize(pmenu->menu_pages);
        for ( i = 0; i < size; i++)
        {
            cJSON* jsonpage = cJSON_GetArrayItem(pmenu->menu_pages, i);
            PAGE_INS* ppage = (PAGE_INS*)jsonpage->valueint;
            ret = save_one_page(ppage);
        }
    }
    return ret;
}

/*
function:   ����json���ݼ��ز˵�iconlist
para1:      pmenu - �˵�ʵ��
return:     true/ok�� false/err
*/
bool load_icon_list(PAGE_INS* ppage, const cJSON* piconlist, const char* icon_img_path)
{

    int i, count;
    cJSON* piconjson;
    struct ICON_INS* picon;
    if (ppage != NULL)
    {
        count = cJSON_GetArraySize(piconlist);
        for (i = 0; i < count; i++)
        {
            
            piconjson = cJSON_GetArrayItem(piconlist, i);
            if (piconjson)
            {
                picon = create_one_icon(piconjson);
                
                picon->icon_owner = ppage;
                // follow initial lvgl objs and update display

                if(ppage->page_bkgd == NULL)
                {                   
                    display_one_icon(ppage->page_obj, picon);
                }
                else
                {                    
                    display_one_icon(ppage->page_bkgd, picon);
                }
                add_one_icon(ppage, picon);           
            }
        }
        return true;
    }
    else
        return false;
}

/*
function:   ���˵���̬���ݱ��浽json������
para1:      pmenu - �˵�ʵ��
return:     true/ok�� false/err
*/
bool save_icon_list(PAGE_INS* ppage, cJSON* piconlist)
{
    int i, count;
    cJSON* piconjson;
    if (ppage && ppage->page_icons)
    {
        count = cJSON_GetArraySize(ppage->page_icons);
        for (i = 0; i < count; i++)
        {
            piconjson = cJSON_GetArrayItem(ppage->page_icons, i);
            if (piconjson)
            {
                cJSON_AddItemToObject(piconlist, piconjson->string, piconjson);
            }
        }
        return true;
    }
    else
        return false;
}


/*
function:  �Ӳ˵�menu icons�л�ȡָ��name��iconʵ��ָ��
para1��pmenu - �˵�����
Para2�� picon -icon name
return��icon jsonָ��
*/
static struct ICON_INS* get_one_icon(PAGE_INS* ppage, char* icon_name)
{
    int i, count;
    cJSON* piconjson;
    if (ppage && ppage->page_icons)
    {
        count = cJSON_GetArraySize(ppage->page_icons);
        for (i = 0; i < count; i++)
        {
            piconjson = cJSON_GetArrayItem(ppage->page_icons, i);
            if (piconjson)
            {
                if (strcmp(piconjson->string, icon_name) == 0)
                    return (struct ICON_INS*)piconjson->valueint;
            }
        }
    }
    return NULL;
}

/*
function:  �˵�menu icons������һ��iconʵ��ָ��
para1��pmenu - �˵�����
Para2�� picon -icon����
return��0/ok, 1/err
*/
bool add_one_icon(PAGE_INS* ppage, struct ICON_INS* picon)
{   
    // ����
    if (get_one_icon(ppage, picon->icon_data->string))
        return false;

    // ���ӵ�menu_icons
    cJSON_AddNumberToObject(ppage->page_icons, picon->icon_data->string, (int)picon);


    return true;
}

/*
function:  ɾ��һ��icon����
para1��pmenu - �˵�����
Para2�� icon_name -icon id
return��0/ok, 1/err
*/
bool del_one_icon(PAGE_INS* ppage, char* icon_name)
{
    if (ppage != NULL)
    {
        cJSON_DeleteItemFromObject(ppage->page_icons, icon_name);
        return true;
    }
    else
        return false;
}

/*
function:  ����һ��icon����
para1��pmenu - �˵�����
Para2�� picon -icon����
return��0/ok, 1/err
*/
bool mod_one_icon(PAGE_INS* ppage, struct ICON_INS* picon)
{
    if(del_one_icon(ppage, picon->icon_data->string))
        destroy_one_icon(picon); 
    //return add_one_icon(ppage, picon);
    return true;
}
