
#include "menu_utility.h"

bool is_dir(char* fullpath)
{
	struct stat sb;
	if (stat(fullpath, &sb) == -1) 
        return false;
	return 
        S_ISDIR(sb.st_mode);
}

bool is_file(char* fullpath)
{
	struct stat sb;
	if (stat(fullpath, &sb) == -1) 
        return false;
	return 
        S_ISREG(sb.st_mode);
}

bool get_files_and_dirs(const char* input_path, cJSON* file_list, cJSON* dir_list)
{
#if !OP_TYPE_WINDOW       // window
    char filefullname[300];
    DIR* dir;
    struct dirent* dt;

    dir = opendir(input_path);

    if (dir == NULL)
    {
        //closedir(dir);
        return false;
    }
    while ((dt = readdir(dir)) != NULL)
    {
#if 1
        if( !strcmp(dt->d_name,".") || !strcmp(dt->d_name,"..") || !strcmp(dt->d_name,"simhei.ttf") )
            continue;
        memset(filefullname, 0 ,300);
        snprintf( filefullname, 300, "%s/%s", input_path, dt->d_name );
        if( is_dir(filefullname) )
        {
                //printf("Found dir[%s]\n", filefullname);  
                cJSON_AddItemToArray(dir_list, cJSON_CreateString((char*)dt->d_name));
        }   
        else
        {
                //printf("Found file[%s]\n", filefullname);  
                cJSON_AddItemToArray(file_list, cJSON_CreateString((char*)dt->d_name));
        }   
#else        
        switch (dt->d_type) 
        {
        case DT_DIR:
            if (dir_list)
            {
                cJSON_AddItemToArray(dir_list, cJSON_CreateString((char*)dt->d_name));
            }
            break;
        case DT_REG:
            if (file_list)
            {
                cJSON_AddItemToArray(file_list, cJSON_CreateString((char*)dt->d_name));
            }
            break;
        }
#endif        
    }
    closedir(dir);
#else

    WIN32_FIND_DATAA p;
    HANDLE h = FindFirstFileA(input_path, &p);
    if (h == -1)
        return false;
    do
    {
        if (p.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
        {
            if (strcmp((char*)p.cFileName, ".") && strcmp((char*)p.cFileName, ".."))
            {
                if (dir_list)
                {
                    cJSON_AddItemToArray(dir_list, cJSON_CreateString((char*)p.cFileName));
                }
            }
        }
        else
        {
            if (file_list)
            {
                cJSON_AddItemToArray(file_list, cJSON_CreateString((char*)p.cFileName));
            }
        }
    } while (FindNextFileA(h, &p));
    FindClose(h);
#endif
    return true;
}

cJSON* recursion_cJSON_GetObjectItemCaseSensitive(const cJSON* psource, const char* name)
{
    int i, size;
    cJSON* pitem = NULL;
    cJSON* psubitem = NULL;
    size = cJSON_GetArraySize(psource);
    for (i = 0; i < size; i++)
    {
        pitem = cJSON_GetArrayItem(psource, i);
        if (pitem && strcmp(pitem->string, name) == 0)
            return pitem;
        else
        {
            psubitem = recursion_cJSON_GetObjectItemCaseSensitive(pitem, name);
            if (psubitem)
                break;
        }
    }
    return psubitem;
}
#if 0
char* GLOBAL_MENU_KEYS_TAB[] =
{
    // menu screen
    "ascreen",
    "background",
    "menu_type",
    "src_height",
    "src_width",
    "page_title",
    "page_name",
    "sidebar",
    // icons
    "iconlist",
    "type",
    "Function",
    "POS",
    "Width",
    "Height",
    "Pos.x",
    "Pos.y",
    "TEXT",
    "Label_text",
    "text_color",
    "text_size",
    "CallBtn_Name",
    "CallBtn_Number",
    "IO_name",
    "Link",
    "Return",
    // add
    "CallPanel_Item_Height",
    "CallPanel_BG_Colour",
    "CallPanel_Symbol_E",
    "CallPanel_Symbol_E_Colour",
    "CallPanel_Symbol_S",
    "CallPanel_Symbol_S_Colour",
    "CallPanel_Text",
    "CallPanel_Text_Colour",
    "CallPanel_Text_Size",
    "icons",
    "layout",
    "cont_color",
    "symbol_color",
    "text_enable",
    "CallBtn_Height",
    "CallBtn_BG_Colour",
    "CallBtn_Symbol_S" ,
    "CallBtn_Symbol_S_Colour",
    "CallBtn_Symbol_E" ,
    "CallBtn_Symbol_E_Colour",
    "CallBtn_Text",
    "CallBtn_Text_Colour",
    "CallBtn_Text_Size",
    "text"
};
#else
struct lv_menu_global_key_band
{
	int		id;
	char* 	str;
	//char*	tra_ptr;
} ;
struct lv_menu_global_key_band GLOBAL_MENU_KEYS_TAB_BAND[]=
{
{GLOBAL_MENU_KEYS_SCREEN,"ascreen"},
{GLOBAL_MENU_KEYS_BACKGROUND             ,	    "background"},
{GLOBAL_MENU_KEYS_SRC_HEIGHT             ,	    "src_height"},
{GLOBAL_MENU_KEYS_SRC_WIDTH              ,	    "src_width"},
{GLOBAL_MENU_KEYS_ICONLIST               ,	    "iconlist"},
{GLOBAL_ICON_KEYS_TYPE                   ,	    "type"},
{GLOBAL_ICON_KEYS_FUNCTION               ,	    "Function"},
{GLOBAL_ICON_KEYS_POS                    ,	    "POS"},
{GLOBAL_ICON_KEYS_WIDTH                  ,	    "Width"},
{GLOBAL_ICON_KEYS_HEIGHT                 ,	    "Height"},
{GLOBAL_ICON_KEYS_POS_X                  ,	    "Pos.x"},
{GLOBAL_ICON_KEYS_POS_Y                  ,	    "Pos.y"},
{GLOBAL_ICON_KEYS_TEXT                   ,	    "TEXT"},
{GLOBAL_ICON_KEYS_LABEL_TEXT             ,	    "Label_text"},
{GLOBAL_ICON_KEYS_TEXT_COLOR             ,	    "text_color"},
{GLOBAL_ICON_KEYS_TEXT_SIZE              ,	    "text_size"},
{GLOBAL_ICON_KEYS_STD_TEXT               ,	    "text"},
{GLOBAL_ICON_KEYS_NUMBER                 ,	    "number"},
{GLOBAL_ICON_KEYS_STD_HEIGHT                 ,	    "height"},
{GLOBAL_ICON_KEYS_BACKGROUND_COLOR         ,	    "bg_color"},
{GLOBAL_ICON_KEYS_SYMBOL_SUFFIX            ,	    "symbol_suffix"},
{GLOBAL_ICON_KEYS_SYMBOL_SUFFIX_COLOR      ,	    "symbol_suffix_color"},
{GLOBAL_ICON_KEYS_SYMBOL_PREFIX            ,	    "symbol_prefix"},
{GLOBAL_ICON_KEYS_SYMBOL_PREFIX_COLOR      ,	    "symbol_prefix_color"},
{GLOBAL_ICON_KEYS_ICONLIST_STD                       ,	    "icons"},
{GLOBAL_ICON_KEYS_LAYOUT                             ,	    "layout"},
{GLOBAL_ICON_KEYS_SYMBOL_COLOR                       ,	    "symbol_color"},
{GLOBAL_ICON_KEYS_TEXT_ENABLE                        ,	    "text_enable"},
{GLOBAL_ICON_KEYS_CALLBTN_NUMBER         ,      "CallBtn_Number"},
{GLOBAL_ICON_KEYS_CALLBTN_NAME           ,     "CallBtn_Name"},
{GLOBAL_MENU_KEYS_TYPE                   ,	    "menu_type"},
{GLOBAL_MENU_KEYS_PAGE_TITLE             ,	    "page_title"},
{GLOBAL_MENU_KEYS_PAGE_NAME              ,	    "page_name"},
{GLOBAL_MENU_KEYS_SIDEBAR                ,	    "sidebar"},
{GLOBAL_ICON_KEYS_IO_NAME                ,	    "IO_name"},
{GLOBAL_ICON_KEYS_LINK                   ,	    "Link"},
{GOLBAL_ICON_KEYS_RETURN                 ,	    "Return"},
{GLOBAL_ICON_KEYS_SHODOW_WIDTH               ,	    "shadow_width"},
{GLOBAL_ICON_KEYS_BOARD_WIDTH                ,	"board_width"},
{GOLBAL_ICON_KEYS_BOARD_COLOR                ,	"board_color"},
{GLOBAL_ICON_KEYS_SYMBOL_BACKGROUND_COLOR     ,	 "symbol_bg_color" },
{GLOBAL_MENU_KEYS_STYLE                   ,	    "menu_style"},
{GOLBAL_ICON_KEYS_IMAGE                   ,	    "image"},
	{-1,NULL}
};
#endif
char* get_global_key_string(int index)
{
#if 0
    if (index >= GLOBAL_ICON_KEYS_MAX)
        return NULL;
    else
    	{
    		printf("%s:%s\n",__func__,GLOBAL_MENU_KEYS_TAB[index]);
        return GLOBAL_MENU_KEYS_TAB[index];
    	}
#else
	int i;
    //printf("==================get_global_key_string,index[%d]\n",index);
	for(i=0;i<GLOBAL_ICON_KEYS_MAX;i++)
	{
		if(GLOBAL_MENU_KEYS_TAB_BAND[i].id==index)
		{
			//printf("%s:%s\n",__func__,GLOBAL_MENU_KEYS_TAB_BAND[i].str);
       	 	return GLOBAL_MENU_KEYS_TAB_BAND[i].str;
		}
		if(GLOBAL_MENU_KEYS_TAB_BAND[i].id<0)
		{
			printf("have no fine global_key_string !!!!!\n");
			return NULL;
		}
		
	}
	
	printf("have no fine global_key_string 22222222!!!!!\n");
	return NULL;
#endif
}

struct vtk_symbol
{
    char*       key;       // �˵���Դ��Ŀ¼
    char*       val;       // �˵�icon��ԴĿ¼
};

struct vtk_symbol GLOBL_SYMBOL_KEYS[] = 
{
    {"LV_SYMBOL_CALL",          LV_SYMBOL_CALL}, 
    {"LV_VTK_INTERCOM1",      LV_VTK_INTERCOM1},
    {"LV_VTK_INTERCOM2",      LV_VTK_INTERCOM2},
    {"LV_VTK_INTERCOM3",      LV_VTK_INTERCOM3},
    {"LV_VTK_INTERCOM4",      LV_VTK_INTERCOM4},
    {"LV_VTK_MISSED",      LV_VTK_MISSED},
    {"LV_VTK_INCOMING",      LV_VTK_INCOMING},
    {"LV_VTK_OUTGOING",      LV_VTK_OUTGOING},
    {"LV_VTK_PLAYBACK",      LV_VTK_PLAYBACK},
    {"LV_VTK_PLAYBACK_VIDEO",      LV_VTK_PLAYBACK_VIDEO},
    {"LV_VTK_PLAYBACK_PIC",      LV_VTK_PLAYBACK_PIC},
    {"LV_VTK_DELETE",      LV_VTK_DELETE},
    {"LV_VTK_MON",      LV_VTK_MON},      
    {"LV_VTK_MON_OFF",      LV_VTK_MON_OFF},
    {"LV_VTK_QUAD",      LV_VTK_QUAD},
    {"LV_VTK_FAVORITE1",      LV_VTK_FAVORITE1},
    {"LV_VTK_NAME",      LV_VTK_NAME},
    {"LV_VTK_CALL_SCENE",      LV_VTK_CALL_SCENE},
    {"LV_VTK_NORMAL_USE",      LV_VTK_NORMAL_USE},
    {"LV_VTK_NO_DISTURB",      LV_VTK_NO_DISTURB},
    {"LV_VTK_CALL_TUNE",      LV_VTK_CALL_TUNE},
    {"LV_VTK_GENERAL",      LV_VTK_GENERAL},
    {"LV_VTK_INSTALLER",      LV_VTK_INSTALLER},
    {"LV_VTK_MANAGER",      LV_VTK_MANAGER},
    {"LV_VTK_EXIT_UNIT",      LV_VTK_EXIT_UNIT},
    {"LV_VTK_CLOSE",      LV_VTK_CLOSE},
    {"LV_VTK_ABOUT",      LV_VTK_ABOUT},
    {"LV_VTK_MUTE",      LV_VTK_MUTE},
    {"LV_VTK_VOL_LOW",      LV_VTK_VOL_LOW},
    {"LV_VTK_VOL_MID",      LV_VTK_VOL_MID},
    {"LV_VTK_VOL_HIHG",      LV_VTK_VOL_HIHG},
    {"LV_VTK_DATE_AND_TIME",      LV_VTK_DATE_AND_TIME},
    {"LV_VTK_DATE",      LV_VTK_DATE},
    {"LV_VTK_TIME",      LV_VTK_TIME},
    {"LV_VTK_LANGUAGE",      LV_VTK_LANGUAGE},
    {"LV_VTK_SHORTCUT",      LV_VTK_SHORTCUT},
    {"LV_VTK_SD_CARD",      LV_VTK_SD_CARD},
    {"LV_VTK_DISABLE",      LV_VTK_DISABLE},
    {"LV_VTK_ENABLE",      LV_VTK_ENABLE},
    {"LV_VTK_CALL_Nbr",      LV_VTK_CALL_Nbr},
    {"LV_VTK_PARAMETER",      LV_VTK_PARAMETER},
    {"LV_VTK_UPDATE",      LV_VTK_UPDATE},
    {"LV_VTK_SERVER",      LV_VTK_SERVER},
    {"LV_VTK_CHECK",      LV_VTK_CHECK},
    {"LV_VTK_REBOOT",      LV_VTK_REBOOT},
    {"LV_VTK_BACKUP",      LV_VTK_BACKUP},
    {"LV_VTK_RESTORE",      LV_VTK_RESTORE},
    {"LV_VTK_QR_CODE",      LV_VTK_QR_CODE},
    {"LV_VTK_LIFT",      LV_VTK_LIFT},
    {"LV_VTK_WAITING1",      LV_VTK_WAITING1},
    {"LV_VTK_WAITING2",      LV_VTK_WAITING2},
    {"LV_VTK_ADD",      LV_VTK_ADD},
    {"LV_VTK_SEARCH",      LV_VTK_SEARCH},
    {"LV_VTK_SERVER",      LV_VTK_SERVER},
    {"LV_VTK_CLOUD_ABNORMAL",      LV_VTK_CLOUD_ABNORMAL},
    {"LV_VTK_CLOUD_OFFLINE",      LV_VTK_CLOUD_OFFLINE},
    {"LV_VTK_CLOUD_ONLINE",      LV_VTK_CLOUD_ONLINE},
    {"LV_VTK_CLOUD_DOWNLOAD",      LV_VTK_CLOUD_DOWNLOAD},
    {"LV_VTK_CLOUD_UPLOAD",      LV_VTK_CLOUD_UPLOAD},
    {"LV_VTK_WIFI_ON",      LV_VTK_WIFI_ON},
    {"LV_VTK_WIFI_OFF",      LV_VTK_WIFI_OFF},
    {"LV_VTK_WIFI_ABNORMAL",      LV_VTK_WIFI_ABNORMAL},
    {"LV_VTK_ROUTE",      LV_VTK_ROUTE},
    {"LV_VTK_SIGNAL",      LV_VTK_SIGNAL},
    {"LV_VTK_INTERNET",      LV_VTK_INTERNET},
    {"LV_VTK_DOWNLOAD",      LV_VTK_DOWNLOAD},
    {"LV_VTK_UPLOAD",      LV_VTK_UPLOAD},
    {"LV_VTK_LINKED",      LV_VTK_LINKED},
    {"LV_VTK_LINK_OFF",      LV_VTK_LINK_OFF},
    {"LV_VTK_CAM1",      LV_VTK_CAM1},
    {"LV_VTK_CAM_OFF",      LV_VTK_CAM_OFF},
    {"LV_VTK_CAM_ADD",      LV_VTK_CAM_ADD},
    {"LV_VTK_CAM2",      LV_VTK_CAM2},
    {"LV_VTK_CAM3",      LV_VTK_CAM3},
    {"LV_VTK_VOICEMAIL",      LV_VTK_VOICEMAIL},
    {"LV_VTK_RECORD1",      LV_VTK_RECORD1},
    {"LV_VTK_RECORD2",      LV_VTK_RECORD2},
    {"LV_VTK_RECORD4",      LV_VTK_RECORD4},
    {"LV_VTK_RECORD_OFF",      LV_VTK_RECORD_OFF},
    {"LV_VTK_LIGHT_OFF",      LV_VTK_LIGHT_OFF},
    {"LV_VTK_LIGHT_ON",      LV_VTK_LIGHT_ON},
    {"LV_VTK_LOCK1",      LV_VTK_LOCK1},
    {"LV_VTK_LOCK2",      LV_VTK_LOCK2},
    {"LV_VTK_LOCKED",      LV_VTK_LOCKED},
    {"LV_VTK_UNLOCK",      LV_VTK_UNLOCK},
    {"LV_VTK_MUSIC1",      LV_VTK_MUSIC1},
    {"LV_VTK_MUSIC_OFF1",      LV_VTK_MUSIC_OFF1},
    {"LV_VTK_BELL_ABNORMAL",      LV_VTK_BELL_ABNORMAL},
    {"LV_VTK_BELL",      LV_VTK_BELL},
    {"LV_VTK_BELL_RING",      LV_VTK_BELL_RING},
    {"LV_VTK_BELL_MUTE",      LV_VTK_BELL_MUTE},
    {"LV_VTK_MIC_ON",      LV_VTK_MIC_ON},
    {"LV_VTK_MIC_OFF",      LV_VTK_MIC_OFF},
    {"LV_VTK_EMERG",      LV_VTK_EMERG},
    {"LV_VTK_EMERG_ON",      LV_VTK_EMERG_ON},
    {"LV_VTK_SENSOR_ON",      LV_VTK_SENSOR_ON},
    {"LV_VTK_SENSOR",      LV_VTK_SENSOR},
    {"LV_VTK_SENSOR_FIRE",      LV_VTK_SENSOR_FIRE},
    {"LV_VTK_SENSOR_ABNORMAL",      LV_VTK_SENSOR_ABNORMAL},
    {"LV_VTK_BLUETOOTH",      LV_VTK_BLUETOOTH},
    {"LV_VTK_FINGERPRINT",      LV_VTK_FINGERPRINT},
    {"LV_VTK_TAG",      LV_VTK_TAG},
    {"LV_VTK_NUM_KP",      LV_VTK_NUM_KP},
    {"LV_VTK_FACE",      LV_VTK_FACE},
    {"LV_VTK_HOME",      LV_VTK_HOME},
    {"LV_VTK_LEAVE",      LV_VTK_LEAVE},
    {"LV_VTK_RETURN",      LV_VTK_RETURN},
    {"LV_VTK_NINGT",      LV_VTK_NINGT},
    {"LV_VTK_DAY",      LV_VTK_DAY},
    {"LV_VTK_COLOR",      LV_VTK_COLOR},
    {"LV_VTK_BRIGHTNESS_HIGH",      LV_VTK_BRIGHTNESS_HIGH},
    {"LV_VTK_BRIGHTNESS_LOW",      LV_VTK_BRIGHTNESS_LOW},
    {"LV_VTK_IOS",      LV_VTK_IOS},
    {"LV_VTK_ANDROID",      LV_VTK_ANDROID},
    {"LV_VTK_APP_STORE",      LV_VTK_APP_STORE},
    {"LV_VTK_GOOGLEPLAY",      LV_VTK_GOOGLEPLAY},
    {"LV_VTK_LINUX",      LV_VTK_LINUX},
    {"LV_VTK_DOWN",      LV_VTK_DOWN},
    {"LV_VTK_UP",      LV_VTK_UP},
    {"LV_VTK_EXIT",      LV_VTK_EXIT},
    {"LV_VTK_BACKWARD",      LV_VTK_BACKWARD},
    {"LV_VTK_FOREWARD",      LV_VTK_FOREWARD},
    {"LV_VTK_NUMERIC_ORDER",      LV_VTK_NUMERIC_ORDER},
    {"LV_VTK_ALP_ORDER",      LV_VTK_ALP_ORDER},
    {"LV_VTK_START",      LV_VTK_START},
    {"LV_VTK_PAUSE",      LV_VTK_PAUSE},
    {"LV_VTK_STOP",      LV_VTK_STOP},
    {"LV_VTK_IM",      LV_VTK_IM},
    {"LV_VTK_GUADRUNIT",      LV_VTK_GUADRUNIT},
    {"LV_VTK_DS",      LV_VTK_DS},
    {"LV_VTK_TELE",      LV_VTK_TELE},
    {"LV_VTK_PHONE1",      LV_VTK_PHONE1},
    {"LV_VTK_PHONE2",      LV_VTK_PHONE2},
    {"LV_VTK_PHONE_WIFI",      LV_VTK_PHONE_WIFI},
    {"LV_VTK_KEYPAD",      LV_VTK_KEYPAD},
    {"LV_VTK_GUADR",      LV_VTK_GUADR},
    {"LV_VTK_UERS1",      LV_VTK_UERS1},
    {"LV_VTK_UERS2",      LV_VTK_UERS2},
    {"LV_VTK_BACK",      LV_VTK_BACK},
    {"LV_VTK_BACKSPACE",      LV_VTK_BACKSPACE},
    {"LV_VTK_CONFIM",      LV_VTK_CONFIM},
    {"LV_VTK_LEFT_PAGE",      LV_VTK_LEFT_PAGE},
    {"LV_VTK_RIGHT_PAGE",      LV_VTK_RIGHT_PAGE},
    {"LV_VTK_CLOSE_VOL",      LV_VTK_CLOSE_VOL},
    {"LV_VTK_OPEN_VOL",      LV_VTK_OPEN_VOL},
    {"LV_VTK_QUIT",      LV_VTK_QUIT},
    {"LV_VTK_OPEN_DIVERT",      LV_VTK_OPEN_DIVERT},
    {"LV_VTK_MONNEXT",      LV_VTK_MONNEXT},
    {"LV_VTK_TURN_BRIGHT",      LV_VTK_TURN_BRIGHT},
    {"LV_VTK_DELETE_VIDEO",      LV_VTK_DELETE_VIDEO},
    {"LV_VTK_PLAY_NEXT_VIDEO",      LV_VTK_PLAY_NEXT_VIDEO},
    {"LV_VTK_OPEN_LOCK1",      LV_VTK_OPEN_LOCK1},
    {"LV_VTK_OPEN_LOCK2",      LV_VTK_OPEN_LOCK2},
    {"LV_VTK_CLOSE_DIVERT",      LV_VTK_CLOSE_DIVERT},
    {"LV_VTK_RESET_PWD",      LV_VTK_RESET_PWD},
    {"LV_VTK_PHONE_FILP",      LV_VTK_PHONE_FILP},
    {"LV_VTK_KEY",      LV_VTK_KEY},
    {"LV_VTK_CALL_RECORD1",      LV_VTK_CALL_RECORD1},
    {"LV_VTK_CIRCLE",      LV_VTK_CIRCLE},
    {"LV_VTK_WIZARD",      LV_VTK_WIZARD},
    {"LV_VTK_EDIT",      LV_VTK_EDIT},
    {"LV_VTK_SETTING",      LV_VTK_SETTING},
    {"LV_VTK_TIPS",      LV_VTK_TIPS},
    {"LV_VTK_UPGRADE",      LV_VTK_UPGRADE},
    {"LV_VTK_OPEN",      LV_VTK_OPEN},
    {"LV_VTK_ERROR",      LV_VTK_ERROR},
    {"LV_VTK_SUCCESS",      LV_VTK_SUCCESS},
    {"LV_VTK_LEFT",      LV_VTK_LEFT},
    {"LV_VTK_RIGHT",      LV_VTK_RIGHT},
    {"LV_VTK_TIPS2",      LV_VTK_TIPS2},
    {"LV_VTK_REMOTE_UNLOCK_OK",      LV_VTK_REMOTE_UNLOCK_OK},
    {"LV_VTK_REMOTE_UNLOCK_DENY",      LV_VTK_REMOTE_UNLOCK_DENY},
    {"LV_VTK_REMOTE_UNLOCK_ER",      LV_VTK_REMOTE_UNLOCK_ER},
};

const int GLOBL_SYMBOL_KEYS_CNT = sizeof(GLOBL_SYMBOL_KEYS)/sizeof(GLOBL_SYMBOL_KEYS[0]);

char* get_symbol_key(char* index)
{
	int i;
	for(i=0; i < GLOBL_SYMBOL_KEYS_CNT; i++)
	{
		if(strcmp(GLOBL_SYMBOL_KEYS[i].key,index) == 0)
		{
       	 	return GLOBL_SYMBOL_KEYS[i].val;
		}		
	}	
	//printf("have no fine symbol_key 22222222!!!!!\n");
	return index;
}
char* get_symbol_key_plus(char* index)
{
	int i;
	for(i=0; i < GLOBL_SYMBOL_KEYS_CNT; i++)
	{
		if(strcmp(GLOBL_SYMBOL_KEYS[i].key,index) == 0)
		{
       	 	return GLOBL_SYMBOL_KEYS[i].val;
		}		
	}	
	//printf("have no fine symbol_key 22222222!!!!!\n");
	return NULL;
}
cJSON* get_cjson_from_file(const char* filename)
{
    FILE* f;

    f = fopen(filename, "r");

    if (f == NULL) {
        printf("Can't open file\n");
        return NULL;
    }
    fseek(f, 0, SEEK_END);
    int size = ftell(f);

    fseek(f, 0, SEEK_SET);

    char* dat = (char*)lv_mem_alloc(size);

    fread(dat, size, 1, f);
    fclose(f);

    cJSON* json = cJSON_Parse(dat);
    lv_mem_free(dat);
    return json;
}

/*只交换cJSON中的数据内容，不交换next，prev 指针，前后顺数不变*/
int swapCont(cJSON *contSrc,cJSON *contDest)
{
	if(contSrc == NULL || contDest == NULL)
	{
		return -1;
	}

	cJSON tmpData;
	memset(&tmpData,0,sizeof(tmpData));
	tmpData.child = contSrc->child;
	tmpData.type = contSrc->type;
	tmpData.valuestring = contSrc->valuestring;
	tmpData.valueint = contSrc->valueint;
	tmpData.valuedouble = contSrc->valuedouble;
	tmpData.string = contSrc->string;

	contSrc->child = contDest->child;
	contSrc->type = contDest->type;
	contSrc->valuestring = contDest->valuestring;
	contSrc->valueint = contDest->valueint;
	contSrc->valuedouble = contDest->valuedouble;
	contSrc->string = contDest->string;

	contDest->child = tmpData.child;
	contDest->type = tmpData.type;
	contDest->valuestring = tmpData.valuestring;
	contDest->valueint = tmpData.valueint;
	contDest->valuedouble = tmpData.valuedouble;
	contDest->string = tmpData.string;

	return 0;
}