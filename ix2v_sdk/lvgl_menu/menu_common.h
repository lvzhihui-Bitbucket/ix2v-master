#ifndef _MENU_COMMON_H_
#define _MENU_COMMON_H_

#include "menu_utility.h" 

typedef struct
{
    char*       res_root;       // �˵���Դ��Ŀ¼
    char*       res_icon;       // �˵�icon��ԴĿ¼
    char*       res_bkgd;       // �˵���ͼ��ԴĿ¼
    cJSON*      res_main;       // �˵���ҳ���ļ���������������ʾ���˵���
    cJSON*      res_pages;      // �˵���ҳ�б��ļ���������
    cJSON*      menu_pages;     // lvgl��̬���ص�PAGE_INS*�б�
    lv_obj_t*   menu_screen;    // lvgl�˵�������
    lv_obj_t*   menu_root;      // lvgl�˵�����
    lv_obj_t*   header;
    char*       menu_name;      // menu name
    char*       menu_cname;     // menu config name
}MENU_INS;

typedef struct
{
    int         page_type;      // menu page type
    char*       res_file;       // page json file name
    cJSON*      page_info;      // page json file struct
    cJSON*      page_icons;     // lvgl loaded icon list ptr    
    lv_obj_t*   page_obj;       // page object
    lv_obj_t*   page_bkgd;      // page background 
    MENU_INS*   page_owner;     // page's owner menu
    bool        page_sidebar;   // sidebar enable
}PAGE_INS;

typedef struct ICON_INS
{
    int         icon_type;          // icon���ͣ�ȱʡΪbutton��
    cJSON*      icon_data;          // icon����
    lv_obj_t*   icon_cont;          // icon����
    PAGE_INS*   icon_owner;         // icon����˵�ҳ
};

PAGE_INS* get_menu_page_obj(MENU_INS* pmenu, char* page_name);

#endif // LV_MAIN_MENU_H
