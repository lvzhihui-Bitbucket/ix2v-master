#ifndef _MENU_PAGE_H_
#define _MENU_PAGE_H_

#include <errno.h>
#include "menu_common.h"

#define PAGE_RES_DIR_NAME           "Menu_Background"
#define INFO_RES_DIR_NAME           "info_picture"

#define MENU_CONFIG_FIX_PATH        "/mnt/nand1-1/App/res/UI-DOCS/PID-850"
#define MENU_CONFIG_CUS_PATH        "/mnt/nand1-2/Customerized/UI-DOCS/PID-850"
#define MENU_CONFIG_SD_PATH         "/mnt/sdcard/Customerized/UI-DOCS/PID-850"


#define MAIN_PAGE_CODE            "code"
#define MAIN_PAGE_DIGTAL          "digital"
#define MAIN_PAGE_MAIN            "main"
#define MAIN_PAGE_ROOMLIST        "roomlist"
#define MAIN_PAGE_NAMELIST        "namelist"
#define MAIN_PAGE_HELP            "help"
#define MAIN_PAGE_CALL            "call"
#define MAIN_PAGE_WIZARD           "wizard"
#define MAIN_PAGE_DXG           "dxg_manager"

#define DIGTAL_NUM                "digtal_num"
#define CODE_NUM                  "code_num"
#define NAMELIST_HEADER           "nameList_header"
#define NAMELIST_BACK             "nameList_back"
#define NAMELIST_JUMP             "nameList_jump"
#define NAMELIST_LIST             "nameList_list"
#define ROOMLIST_HEADER           "roomList_header"
#define ROOMLIST_BACK             "roomList_back"
#define ROOMLIST_JUMP             "roomList_jump"
#define ROOMLIST_LIST             "roomList_list"
#define MAIN_HEADER               "header"
#define CODE_TIPS                 "code_tips"
#define DIGITAL_TIPS              "digtal_tips"

/// @brief              - init_one_menu
/// @param menu_name    - menu name
/// @param menu_cname   - menu config name
/// @param screen       - parent window, default is screen
/// @return             - NOT NULL: MENU instance pointer, NULL: failed
MENU_INS* init_one_menu( const char* menu_name, const char* menu_cname, lv_obj_t* screen );

/// @brief          - deinit_one_menu
/// @param pmenu    - menu instance pointer
/// @return         - true: ok, flase: error
bool deinit_one_menu( MENU_INS* pmenu );

/// @brief          - create_one_menu_page
/// @param pmenu    - menu instance pointer
/// @param page_dir - page resource directory
/// @return         - NOT NULL: PAGE instance pointer, NULL: failed
PAGE_INS* create_one_menu_page(MENU_INS* pmenu, const char* page_dir);

PAGE_INS* create_one_menu_page(MENU_INS* pmenu, const char* page_dir);
/// @brief              - kill_one_menu_page
/// @param pmenu        - menu instance pointer
/// @param page_name    - page name
/// @return             - true: ok, flase: error
bool kill_one_menu_page(MENU_INS* pmenu, const char* page_name);

/// @brief          - kill_all_menu_pages
/// @param pmenu    - menu instance pointer 
/// @return         - true: ok, flase: error
bool kill_all_menu_pages(MENU_INS* pmenu);

/// @brief          - get_name_of_one_page
/// @param pmenu    - menu instance pointer
/// @param index    - menu page index
/// @return         - page name
char* get_name_of_one_page(MENU_INS* pmenu, int index);

/// @brief              - get_res_name_of_one_page
/// @param pmenu        - menu instance pointer 
/// @param page_name    - page name
/// @return             - NULL: NONE, NOT NULL: res direcotry name
char* get_res_name_of_one_page(MENU_INS* pmenu, const char* page_name);

/// @brief           - is_page_of_one_menu
/// @param pmenu     - menu instance pointer 
/// @param page_name - page name
/// @return          - true: IS OK, flase: IS NOT   
bool is_page_of_one_menu(MENU_INS* pmenu, const char* page_name);

/// @brief           - is_active_page_of_one_menu
/// @param pmenu     - menu instance pointer 
/// @param page_name - page name
/// @return          - true: IS ACTIVE, flase: IS NOT ACTIVE   
bool is_active_page_of_one_menu(MENU_INS* pmenu, const char* page_name);

/// @brief          - get_total_pages_of_one_menu
/// @param pmenu    - menu instance pointer
/// @return         - -1: pmenu is NULL, x page number
int get_total_pages_of_one_menu(MENU_INS* pmenu);

/// @brief          - get_name_of_one_menu
/// @param pmenu    - menu instance pointer
/// @return         - menu name ptr
char* get_name_of_one_menu(MENU_INS* pmenu);

/// @brief          - get_name_of_one_menu_config
/// @param pmenu    - menu instance pointer
/// @return         - menu config name ptr
char* get_name_of_one_menu_config(MENU_INS* pmenu);

/// @brief              - switch_one_menu_page
/// @param pmenu        - menu instance pointer
/// @param page_name    - page name
/// @return             - NULL - switch error, otherwise return the page object
lv_obj_t* switch_one_menu_page(MENU_INS* pmenu, const char* page_name);

#endif
