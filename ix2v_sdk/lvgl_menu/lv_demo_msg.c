﻿#include "lv_demo_msg.h"

static int flag = 0;

lv_timer_t* API_TIPS(const char* string)
{     
    lv_obj_t* msg_cont = lv_obj_create(lv_layer_top());
    lv_obj_set_size(msg_cont, 300, 200);
    lv_obj_add_flag(msg_cont, LV_OBJ_FLAG_CLICKABLE);
    lv_obj_add_flag(lv_layer_top(), LV_OBJ_FLAG_CLICKABLE);
	lv_obj_set_style_bg_opa(msg_cont, LV_OPA_90, 0);
	lv_obj_set_style_radius(msg_cont, 20, 0);

	lv_obj_set_style_bg_color(msg_cont, lv_color_white(), 0);

    if (!flag)
    {
        lv_obj_align(msg_cont, LV_ALIGN_CENTER, 0, 0);
    }
    else
    {
        lv_obj_align(msg_cont, LV_ALIGN_CENTER, 10, 10);       
        lv_obj_move_background(msg_cont);
    }

	lv_obj_t* title_txt = lv_label_create(msg_cont);
	lv_obj_set_size(title_txt, LV_PCT(50), LV_SIZE_CONTENT);
	set_font_size(title_txt,3);
	lv_obj_align(title_txt,LV_ALIGN_TOP_MID,0,20);
	lv_label_set_text(title_txt, "Tips");
	lv_obj_set_style_text_color(title_txt,lv_color_make(245,157,20),0);
	
    lv_obj_t* msg_txt = lv_label_create(msg_cont);
	lv_obj_set_size(msg_txt, LV_SIZE_CONTENT, 80);
    //lv_label_set_text(msg_txt, string);
	vtk_label_set_text(msg_txt, string);
	set_font_size(msg_txt,1);
	lv_obj_set_style_text_color(msg_txt,lv_color_black(),0);
	lv_obj_align(msg_txt,LV_ALIGN_CENTER,0,20);

    lv_timer_t* timer = lv_timer_create(msg_timer_cb, 5000, msg_cont);

    lv_obj_add_event_cb(msg_cont, msg_click_event_cb, LV_EVENT_CLICKED, timer);
    flag++;

    return timer;
}

static lv_timer_t* API_TIPS_Time(const char* string,int ts)
{     
    lv_obj_t* msg_cont = lv_obj_create(lv_layer_top());
    lv_obj_set_size(msg_cont, 300, LV_SIZE_CONTENT);
    lv_obj_add_flag(msg_cont, LV_OBJ_FLAG_CLICKABLE);
    lv_obj_add_flag(lv_layer_top(), LV_OBJ_FLAG_CLICKABLE);
	#ifdef PID_IX850
	lv_obj_set_style_bg_color(msg_cont, lv_color_make(74,132,101), 0);
	#endif
    if (!flag)
    {
        lv_obj_align(msg_cont, LV_ALIGN_CENTER, 0, 0);
    }
    else
    {
        lv_obj_align(msg_cont, LV_ALIGN_CENTER, 10, 10);       
        lv_obj_move_background(msg_cont);
    }

    lv_obj_t* msg_txt = lv_label_create(msg_cont);
	lv_obj_set_size(msg_txt, LV_PCT(100), LV_SIZE_CONTENT);
    //lv_label_set_text(msg_txt, string);
	vtk_label_set_text(msg_txt, string);
    lv_obj_set_style_text_font(msg_txt, &lv_font_montserrat_32, LV_STATE_DEFAULT);
	lv_obj_set_style_text_color(msg_txt,lv_color_white(),0);
    lv_obj_center(msg_txt);

    lv_timer_t* timer = lv_timer_create(msg_timer_cb, 1000*ts, msg_cont);
	if(ts<20)
    		lv_obj_add_event_cb(msg_cont, msg_click_event_cb, LV_EVENT_CLICKED, timer);
    flag++;

    return timer;
}

lv_timer_t* API_TIPS_Prefix(const char* string, char* data)
{     
    lv_obj_t* msg_cont = lv_obj_create(lv_layer_top());
    lv_obj_set_size(msg_cont, 300, LV_SIZE_CONTENT);
    lv_obj_add_flag(msg_cont, LV_OBJ_FLAG_CLICKABLE);
    lv_obj_add_flag(lv_layer_top(), LV_OBJ_FLAG_CLICKABLE);
	lv_obj_set_style_bg_color(msg_cont, lv_color_make(74,132,101), 0);
    if (!flag)
    {
        lv_obj_align(msg_cont, LV_ALIGN_CENTER, 0, 0);
    }
    else
    {
        lv_obj_align(msg_cont, LV_ALIGN_CENTER, 10, 10);       
        lv_obj_move_background(msg_cont);
    }

    lv_obj_t* msg_txt = lv_label_create(msg_cont);
	lv_obj_set_size(msg_txt, LV_PCT(100), LV_SIZE_CONTENT);
	if(data)
	{
		lv_label_set_text_fmt(msg_txt, "%s %s", get_language_text(string)? get_language_text(string):string, data);
	}
	else
	{
		vtk_label_set_text(msg_txt, string);
	}
	
    lv_obj_set_style_text_font(msg_txt, &lv_font_montserrat_32, LV_STATE_DEFAULT);
	lv_obj_set_style_text_color(msg_txt,lv_color_white(),0);
    lv_obj_center(msg_txt);

    lv_timer_t* timer = lv_timer_create(msg_timer_cb, 5000, msg_cont);

    lv_obj_add_event_cb(msg_cont, msg_click_event_cb, LV_EVENT_CLICKED, timer);
    flag++;

    return timer;
}

void API_TIPS_Close(lv_timer_t* timer)
{
	if(timer)
	{
	    msg_timer_cb(timer);
	}
}
static void msg_timer_cb(lv_timer_t* timer)
{
    lv_obj_t* msg = (lv_obj_t*)timer->user_data;

    msg_close(msg, timer);
}

static void msg_click_event_cb(lv_event_t* e)
{
    lv_obj_t* msg = lv_event_get_target(e);
    lv_timer_t* timer = lv_event_get_user_data(e);
    msg_close(msg, timer);
}

static void msg_close(lv_obj_t* obj, lv_timer_t* timer)
{
	API_TIPS_BeClose_Ext((void *)timer);
    lv_obj_del(obj); 
    lv_timer_del(timer);
    flag--;
    lv_obj_clear_flag(lv_layer_top(), LV_OBJ_FLAG_CLICKABLE);
}
#define TIPS_Ext_Handle_Max	3
static void *TIPS_Ext_Handle[TIPS_Ext_Handle_Max]={0};
void* API_TIPS_Ext(const char* string)
{
	int i;
	void *ret = NULL;

	#ifdef PID_IX850
	if(getbatchState())
		return ret;
	#endif
	vtk_lvgl_lock();
	#if 1
	for(i=0;i<TIPS_Ext_Handle_Max;i++)
	{
		if(TIPS_Ext_Handle[i]==NULL)
		{
			TIPS_Ext_Handle[i] = LV_API_TIPS(string,1);//API_TIPS(string);
			ret = TIPS_Ext_Handle[i];
			break;
		}
	}
	#endif
	//LV_API_TIPS(string,1);
	vtk_lvgl_unlock();

	return ret;
}
void* API_TIPS_Ext_Time(const char* string,int ts)
{
	int i;
	void *ret = NULL;
	#ifdef PID_IX850
	if(getbatchState())
		return ret;
	vtk_lvgl_lock();
	for(i=0;i<TIPS_Ext_Handle_Max;i++)
	{
		if(TIPS_Ext_Handle[i]==NULL)
		{
			TIPS_Ext_Handle[i]= LV_API_TIPS_Time(string,ts,1);
			ret = TIPS_Ext_Handle[i];
			break;
		}
	}
	vtk_lvgl_unlock();
	#endif

	return ret;
}
void* API_TIPS_Ext_Prefix(const char* string, char* data)
{
	int i;
	void *ret = NULL;
	#ifdef PID_IX850
	if(getbatchState())
		return ret;
	#endif
	vtk_lvgl_lock();
	for(i=0;i<TIPS_Ext_Handle_Max;i++)
	{
		if(TIPS_Ext_Handle[i]==NULL)
		{
			TIPS_Ext_Handle[i] = LV_API_TIPS_Prefix(string, data,1);
			ret = TIPS_Ext_Handle[i];
			break;
		}
	}
	vtk_lvgl_unlock();

	return ret;
}
void API_TIPS_Close_Ext(void *handle)
{
	int i;
	if(handle==NULL)
		return;
	
	vtk_lvgl_lock();
	for(i=0;i<TIPS_Ext_Handle_Max;i++)
	{
		if(TIPS_Ext_Handle[i]==handle)
		{
			API_TIPS_Close((lv_timer_t*)handle);
			TIPS_Ext_Handle[i]=NULL;
			break;
		}
	}
	vtk_lvgl_unlock();
}
void API_TIPS_BeClose_Ext(void *handle)
{
	int i;
	if(handle==NULL)
		return;
	for(i=0;i<TIPS_Ext_Handle_Max;i++)
	{
		if(TIPS_Ext_Handle[i]==handle)
		{
			TIPS_Ext_Handle[i]=NULL;
			break;
		}
	}
}

static lv_obj_t* modeTip=NULL;
lv_obj_t* API_TIPS_Mode(char* string)
{
	vtk_lvgl_lock();
	if(modeTip == NULL)
	{
		modeTip = lv_obj_create(lv_layer_top());
		lv_obj_set_size(modeTip, 380, 280);
		lv_obj_add_flag(modeTip, LV_OBJ_FLAG_CLICKABLE);
		lv_obj_add_flag(lv_layer_top(), LV_OBJ_FLAG_CLICKABLE);
		lv_obj_set_style_radius(modeTip, 15, 0);
		lv_obj_clear_flag(modeTip, LV_OBJ_FLAG_SCROLLABLE);
		lv_obj_set_style_bg_color(modeTip, lv_color_white(), 0);
		lv_obj_align(modeTip, LV_ALIGN_CENTER, 0, 0);

		lv_obj_t* title_txt = lv_label_create(modeTip);
		lv_obj_set_size(title_txt, LV_SIZE_CONTENT, LV_SIZE_CONTENT);	
		set_font_size(title_txt,2);
		lv_obj_align(title_txt,LV_ALIGN_TOP_MID,0,-10);
		lv_label_set_text(title_txt, LV_VTK_TIPS);
		lv_obj_set_style_text_color(title_txt,lv_color_make(255,152,0),0);

		lv_obj_t* msg_txt = lv_label_create(modeTip);
		lv_obj_set_size(msg_txt, 340, 180);
		lv_obj_set_style_text_align(msg_txt,LV_TEXT_ALIGN_CENTER,0);
		lv_label_set_text(msg_txt, string);
		//vtk_label_set_text(msg_txt, string);
		set_font_size(msg_txt,1);	
		lv_obj_set_style_text_color(msg_txt,lv_color_black(),0);
		lv_obj_align(msg_txt,LV_ALIGN_CENTER,0,20);
	}	
	vtk_lvgl_unlock();
	return modeTip;
}

void API_TIPS_Mode_Title(lv_obj_t* msg_cont, char* string)
{
	vtk_lvgl_lock();
	if(modeTip && string)
	{
		lv_obj_t* title = lv_obj_get_child(modeTip, 0);
		lv_label_set_text(title, string);
	}
	vtk_lvgl_unlock();
}

void API_TIPS_Mode_Fresh(lv_obj_t* msg_cont, char* string)
{
	vtk_lvgl_lock();
	if(modeTip)
	{
		lv_obj_t* msg_txt = lv_obj_get_child(modeTip, 1);
		if(msg_txt)
			lv_label_set_text(msg_txt, string);
	}
	vtk_lvgl_unlock();
}
void API_TIPS_Mode_Close(lv_obj_t* msg_cont)
{
	vtk_lvgl_lock();
	if(modeTip)
	{
		lv_obj_del(modeTip); 
		modeTip = NULL;
    	lv_obj_clear_flag(lv_layer_top(), LV_OBJ_FLAG_CLICKABLE);
	}
	vtk_lvgl_unlock();
}
void API_TIPS_Mode_CD(int time)
{
	vtk_lvgl_lock();
	if(modeTip)
	{
		lv_obj_t* time_txt = lv_obj_get_child(modeTip, 0);
		if(time_txt)
			lv_label_set_text_fmt(time_txt, "%s %ds", LV_VTK_WAITING1, time);
	}	
	vtk_lvgl_unlock();
}

extern lv_ft_info_t ft_10;
lv_timer_t* LV_API_TIPS(const char* string,int type)
{     
	vtk_lvgl_lock();
    lv_obj_t* msg_cont = lv_obj_create(lv_layer_top());
    lv_obj_set_size(msg_cont, 360, 260);
    lv_obj_add_flag(msg_cont, LV_OBJ_FLAG_CLICKABLE);
    lv_obj_add_flag(lv_layer_top(), LV_OBJ_FLAG_CLICKABLE);
	//lv_obj_set_style_bg_opa(msg_cont, LV_OPA_90, 0);
	lv_obj_set_style_radius(msg_cont, 15, 0);
	lv_obj_clear_flag(msg_cont, LV_OBJ_FLAG_SCROLLABLE);

	lv_obj_set_style_bg_color(msg_cont, lv_color_white(), 0);

    if (!flag)
    {
        lv_obj_align(msg_cont, LV_ALIGN_CENTER, 0, 0);
    }
    else
    {
        lv_obj_align(msg_cont, LV_ALIGN_CENTER, 10, 10);       
        lv_obj_move_background(msg_cont);
    }

	lv_obj_t* title_txt = lv_label_create(msg_cont);
	lv_obj_set_size(title_txt, LV_SIZE_CONTENT, LV_SIZE_CONTENT);	
	set_font_size(title_txt,3);
	//lv_obj_set_style_text_font(title_txt,ft_10.font,0);
	lv_obj_align(title_txt,LV_ALIGN_TOP_MID,0,-10);
	switch(type){
		case 1:
			lv_label_set_text(title_txt, LV_VTK_TIPS);
			lv_obj_set_style_text_color(title_txt,lv_color_make(255,152,0),0);
			break;
		case 2:
			lv_label_set_text(title_txt, LV_VTK_UPGRADE);
			lv_obj_set_style_text_color(title_txt,lv_color_make(21,130,255),0);
			break;
		case 3:
			lv_label_set_text(title_txt, LV_VTK_OPEN);
			lv_obj_set_style_text_color(title_txt,lv_color_make(30,220,100),0);
			break;
		case 4:
			lv_label_set_text(title_txt, LV_VTK_ERROR);
			lv_obj_set_style_text_color(title_txt,lv_color_make(255,51,37),0);
			break;
		case 5:
			lv_label_set_text(title_txt, LV_VTK_SUCCESS);
			lv_obj_set_style_text_color(title_txt,lv_color_make(30,220,100),0);
			break;
		// lzh_20240827_s
		// #24D200绿色	(36 210 00)
		case 10:	
			lv_label_set_text(title_txt, LV_VTK_REMOTE_UNLOCK_OK);
			lv_obj_set_style_text_color(title_txt,lv_color_make(36,210,0),0);
			break;
		//#F90000 红色	(249 00 00)
		case 11:
			lv_label_set_text(title_txt, LV_VTK_REMOTE_UNLOCK_DENY);
			lv_obj_set_style_text_color(title_txt,lv_color_make(249,0,0),0);
			break;
		//#F90000 红色	(249 00 00)
		case 12:
			lv_label_set_text(title_txt, LV_VTK_REMOTE_UNLOCK_ER);
			lv_obj_set_style_text_color(title_txt,lv_color_make(249,0,0),0);
			break;
		// lzh_20240827_e
	}
	
    lv_obj_t* msg_txt = lv_label_create(msg_cont);
	lv_obj_set_size(msg_txt, 330, 80);
	lv_obj_set_style_text_align(msg_txt,LV_TEXT_ALIGN_CENTER,0);
    //lv_label_set_text(msg_txt, string);
	vtk_label_set_text(msg_txt, string);
	set_font_size(msg_txt,1);	
	lv_obj_set_style_text_color(msg_txt,lv_color_black(),0);
	lv_obj_align(msg_txt,LV_ALIGN_CENTER,0,60);

    lv_timer_t* timer = lv_timer_create(msg_timer_cb, 5000, msg_cont);

    lv_obj_add_event_cb(msg_cont, msg_click_event_cb, LV_EVENT_CLICKED, timer);
    flag++;
	vtk_lvgl_unlock();
    return timer;
}

lv_timer_t* LV_API_TIPS_Prefix(const char* string, char* data,int type)
{   
	vtk_lvgl_lock();
	lv_obj_t* msg_cont = lv_obj_create(lv_layer_top());
    lv_obj_set_size(msg_cont, 360, 260);
    lv_obj_add_flag(msg_cont, LV_OBJ_FLAG_CLICKABLE);
    lv_obj_add_flag(lv_layer_top(), LV_OBJ_FLAG_CLICKABLE);
	lv_obj_set_style_radius(msg_cont, 15, 0);
	lv_obj_clear_flag(msg_cont, LV_OBJ_FLAG_SCROLLABLE);

	lv_obj_set_style_bg_color(msg_cont, lv_color_white(), 0);

    if (!flag)
    {
        lv_obj_align(msg_cont, LV_ALIGN_CENTER, 0, 0);
    }
    else
    {
        lv_obj_align(msg_cont, LV_ALIGN_CENTER, 10, 10);       
        lv_obj_move_background(msg_cont);
    }

	lv_obj_t* title_txt = lv_label_create(msg_cont);
	lv_obj_set_size(title_txt, LV_SIZE_CONTENT, LV_SIZE_CONTENT);
	set_font_size(title_txt,3);	
	//lv_obj_set_style_text_font(title_txt,ft_10.font,0);
	lv_obj_align(title_txt,LV_ALIGN_TOP_MID,0,-10);
	switch(type){
		case 1:
			lv_label_set_text(title_txt, LV_VTK_TIPS);
			lv_obj_set_style_text_color(title_txt,lv_color_make(255,152,0),0);
			break;
		case 2:
			lv_label_set_text(title_txt, LV_VTK_UPGRADE);
			lv_obj_set_style_text_color(title_txt,lv_color_make(21,130,255),0);
			break;
		case 3:
			lv_label_set_text(title_txt, LV_VTK_OPEN);
			lv_obj_set_style_text_color(title_txt,lv_color_make(30,220,100),0);
			break;
		case 4:
			lv_label_set_text(title_txt, LV_VTK_ERROR);
			lv_obj_set_style_text_color(title_txt,lv_color_make(255,51,37),0);
			break;
		case 5:
			lv_label_set_text(title_txt, LV_VTK_SUCCESS);
			lv_obj_set_style_text_color(title_txt,lv_color_make(30,220,100),0);
			break;
	}
	
    lv_obj_t* msg_txt = lv_label_create(msg_cont);
	lv_obj_set_size(msg_txt, 330, 80);
	lv_obj_set_style_text_align(msg_txt,LV_TEXT_ALIGN_CENTER,0);
	set_font_size(msg_txt,1);	
	lv_obj_set_style_text_color(msg_txt,lv_color_black(),0);
	lv_obj_align(msg_txt,LV_ALIGN_CENTER,0,60);
	if(data)
	{
		lv_label_set_text_fmt(msg_txt, "%s %s", get_language_text(string)? get_language_text(string):string, data);
	}
	else
	{
		vtk_label_set_text(msg_txt, string);
	}

    lv_timer_t* timer = lv_timer_create(msg_timer_cb, 5000, msg_cont);

    lv_obj_add_event_cb(msg_cont, msg_click_event_cb, LV_EVENT_CLICKED, timer);
    flag++;
	vtk_lvgl_unlock();
    return timer;
}

lv_timer_t* LV_API_TIPS_Time(const char* string,int ts,int type)
{    
	vtk_lvgl_lock();
	lv_obj_t* msg_cont = lv_obj_create(lv_layer_top());
    lv_obj_set_size(msg_cont, 360, 260);
    lv_obj_add_flag(msg_cont, LV_OBJ_FLAG_CLICKABLE);
    lv_obj_add_flag(lv_layer_top(), LV_OBJ_FLAG_CLICKABLE);
	//lv_obj_set_style_bg_opa(msg_cont, LV_OPA_90, 0);
	lv_obj_set_style_radius(msg_cont, 15, 0);
	lv_obj_clear_flag(msg_cont, LV_OBJ_FLAG_SCROLLABLE);

	lv_obj_set_style_bg_color(msg_cont, lv_color_white(), 0);

    if (!flag)
    {
        lv_obj_align(msg_cont, LV_ALIGN_CENTER, 0, 0);
    }
    else
    {
        lv_obj_align(msg_cont, LV_ALIGN_CENTER, 10, 10);       
        lv_obj_move_background(msg_cont);
    }

	lv_obj_t* title_txt = lv_label_create(msg_cont);
	lv_obj_set_size(title_txt, LV_SIZE_CONTENT, LV_SIZE_CONTENT);	
	set_font_size(title_txt,3);
	//lv_obj_set_style_text_font(title_txt,ft_10.font,0);
	lv_obj_align(title_txt,LV_ALIGN_TOP_MID,0,-10);
	switch(type){
		case 1:
			lv_label_set_text(title_txt, LV_VTK_TIPS);
			lv_obj_set_style_text_color(title_txt,lv_color_make(255,152,0),0);
			break;
		case 2:
			lv_label_set_text(title_txt, LV_VTK_UPGRADE);
			lv_obj_set_style_text_color(title_txt,lv_color_make(21,130,255),0);
			break;
		case 3:
			lv_label_set_text(title_txt, LV_VTK_OPEN);
			lv_obj_set_style_text_color(title_txt,lv_color_make(30,220,100),0);
			break;
		case 4:
			lv_label_set_text(title_txt, LV_VTK_ERROR);
			lv_obj_set_style_text_color(title_txt,lv_color_make(255,51,37),0);
			break;
		case 5:
			lv_label_set_text(title_txt, LV_VTK_SUCCESS);
			lv_obj_set_style_text_color(title_txt,lv_color_make(30,220,100),0);
			break;
	}
	
    lv_obj_t* msg_txt = lv_label_create(msg_cont);
	lv_obj_set_size(msg_txt, 330, 80);
	lv_obj_set_style_text_align(msg_txt,LV_TEXT_ALIGN_CENTER,0);
    //lv_label_set_text(msg_txt, string);
	vtk_label_set_text(msg_txt, string);
	set_font_size(msg_txt,1);	
	lv_obj_set_style_text_color(msg_txt,lv_color_black(),0);
	lv_obj_align(msg_txt,LV_ALIGN_CENTER,0,60);

    lv_timer_t* timer = lv_timer_create(msg_timer_cb, 1000*ts, msg_cont);
	if(ts<20)
    		lv_obj_add_event_cb(msg_cont, msg_click_event_cb, LV_EVENT_CLICKED, timer);
    flag++;
	vtk_lvgl_unlock();
    return timer;
}

typedef struct 
{   
    MsgBoxCb callback;
	void* callbackData;
	lv_timer_t* timer;
	lv_obj_t* msg_cont;	
} MSGBOX_CB;

static void msgbox_exit_event(lv_event_t* e)
{
	MSGBOX_CB* msg = lv_event_get_user_data(e);
	if(msg == NULL)
		return;

	if(msg->timer)
		lv_timer_del(msg->timer);
	if(msg->msg_cont){
		lv_obj_del(msg->msg_cont);
		lv_obj_clear_flag(lv_layer_top(), LV_OBJ_FLAG_CLICKABLE);
	}
	lv_mem_free(msg);	
}

static void msgbox_confim_event(lv_event_t* e)
{
	MSGBOX_CB* boxCb = lv_event_get_user_data(e);
	if(boxCb == NULL)
		return;

	if(boxCb->timer)
		lv_timer_del(boxCb->timer);
	if(boxCb->msg_cont){
		lv_obj_del(boxCb->msg_cont);
		lv_obj_clear_flag(lv_layer_top(), LV_OBJ_FLAG_CLICKABLE);
	}
	
	if(boxCb->callback)
	{
		(boxCb->callback)(boxCb->callbackData);		
	}
	lv_mem_free(boxCb);
}

static void msgbox_timer_cb(lv_timer_t* timer)
{
    MSGBOX_CB* box = (lv_obj_t*)timer->user_data;

	if(box->timer)
		lv_timer_del(box->timer);
	if(box->msg_cont){
		lv_obj_del(box->msg_cont);
		lv_obj_clear_flag(lv_layer_top(), LV_OBJ_FLAG_CLICKABLE);
	}
	lv_mem_free(box);
}

void API_MSGBOX(const char* string, int type, MsgBoxCb callback, void*data)
{
	MSGBOX_CB* msgCb = lv_mem_alloc(sizeof(MSGBOX_CB));
	if(msgCb == NULL)
	{	
		return;					
	}
	msgCb->callback = callback;
	msgCb->callbackData = data;

	lv_obj_t* msg_cont = lv_obj_create(lv_layer_top());
	lv_obj_set_style_pad_all(msg_cont,0,0);
    lv_obj_set_size(msg_cont, 360, 330);
    lv_obj_add_flag(msg_cont, LV_OBJ_FLAG_CLICKABLE);
    lv_obj_add_flag(lv_layer_top(), LV_OBJ_FLAG_CLICKABLE);
	lv_obj_set_style_radius(msg_cont, 15, 0);
	lv_obj_clear_flag(msg_cont, LV_OBJ_FLAG_SCROLLABLE);
	lv_obj_set_style_bg_color(msg_cont, lv_color_white(), 0);
	lv_obj_align(msg_cont, LV_ALIGN_CENTER, 0, 0);
	msgCb->msg_cont = msg_cont;

	lv_obj_t* title_txt = lv_label_create(msg_cont);
	lv_obj_set_size(title_txt, LV_SIZE_CONTENT, LV_SIZE_CONTENT);	
	set_font_size(title_txt,3);
	lv_obj_align(title_txt,LV_ALIGN_TOP_MID,0,0);
	lv_label_set_text(title_txt, LV_VTK_TIPS2);

	switch(type){
		case 1:
			lv_obj_set_style_text_color(title_txt,lv_color_hex(0x4F98ED),0);
			break;
		case 2:
			lv_obj_set_style_text_color(title_txt,lv_color_hex(0x24D200),0);
			break;
		case 3:
			lv_obj_set_style_text_color(title_txt,lv_color_hex(0xFFB240),0);
			break;
		case 4:		
			lv_obj_set_style_text_color(title_txt,lv_color_hex(0xF94400),0);
			break;
	}
	
    lv_obj_t* msg_txt = lv_label_create(msg_cont);
	lv_obj_set_size(msg_txt, 330, 80);
	lv_obj_set_style_text_align(msg_txt,LV_TEXT_ALIGN_CENTER,0);
    //lv_label_set_text(msg_txt, string);
	vtk_label_set_text(msg_txt, string);
	set_font_size(msg_txt,1);	
	lv_obj_set_style_text_color(msg_txt,lv_color_black(),0);
	lv_obj_align(msg_txt,LV_ALIGN_CENTER,0,0);
	if(type!=-1)
	{
		lv_obj_t* delBtn = lv_btn_create(msg_cont);
		lv_obj_set_pos(delBtn,10,260);
		lv_obj_set_style_bg_color(delBtn,lv_color_hex(0x777777),0);
		lv_obj_set_style_radius(delBtn,15,0);
		lv_obj_set_size(delBtn, 165, 60);
		set_font_size(delBtn,2);
		lv_obj_add_event_cb(delBtn,msgbox_exit_event, LV_EVENT_CLICKED, msgCb);
		lv_obj_t* label1 = lv_label_create(delBtn);		
		lv_obj_set_style_text_color(label1,lv_color_white(),0);
		lv_label_set_text(label1, LV_VTK_ERROR);
		lv_obj_center(label1);
	}

	lv_obj_t* addBtn = lv_btn_create(msg_cont);
	lv_obj_set_pos(addBtn,185,260);
	lv_obj_set_style_border_width(addBtn, 0, 0);		
	lv_obj_set_style_bg_color(addBtn,lv_color_hex(0xFFB240),0);
	lv_obj_set_style_radius(addBtn,15,0);
	lv_obj_set_size(addBtn, 165, 60);
	set_font_size(addBtn,2);
	if(type==-1)
		msgCb->timer=NULL;
	else
	{
		lv_timer_t* timer = lv_timer_create(msgbox_timer_cb, 10000, msgCb);
		msgCb->timer = timer;	
	}
	
	lv_obj_add_event_cb(addBtn, msgbox_confim_event, LV_EVENT_CLICKED, msgCb);
	lv_obj_t* label2 = lv_label_create(addBtn);	
	lv_label_set_text(label2, LV_VTK_SUCCESS);
	lv_obj_set_style_text_color(label2,lv_color_white(),0);
	lv_obj_center(label2);	
}