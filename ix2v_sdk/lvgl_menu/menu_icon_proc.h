#ifndef _MENU_ICON_PROC_H_
#define _MENU_ICON_PROC_H_

#include "menu_common.h"

typedef void* (*icon_create_cb)(lv_obj_t* parent, struct ICON_INS* picon);

typedef struct _ICON_CREATE_CB_T
{
    char* type;
    icon_create_cb  cb;
}ICON_CREATE_CB_T;

#endif
