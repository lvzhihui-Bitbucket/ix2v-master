#!/bin/bash

function recursive_dir()
{
	local curdir subdir
	curdir=$(pwd)
	for subdir in $(ls ${curdir})
	do
		if test -d ${subdir}
		then
			cd ${subdir}
			recursive_dir
			cd ..
		else
			file=${curdir}/${subdir}
			filename=`basename ${file}`
			name=${filename%%.*}
			ext=${filename##*.}
			if [ ${ext} == "h" ];then				
				ln -s ${file} ${INCPATH}/${filename}
				echo ${INCPATH}/${filename}
			fi
		fi
	done
}

INCPATH=$(pwd)/include
APPPATH=$(pwd)/app
GUIPATH=$(pwd)/lvgl_menu

cd ${INCPATH}
rm *.h
cd ..
cd ${APPPATH}
recursive_dir
cd ..
cd ${GUIPATH}
recursive_dir
cd ..

