/**
  ******************************************************************************
  * @file    task_Event.h
  * @author  czb
  * @version V00.01.00
  * @date    2022.5.17
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 

#ifndef _task_Event_H
#define _task_Event_H

#include "cJSON.h"
#include "RTOS.h"
#include "utility.h"

// Define Object Property-------------------------------------------------------

typedef int (*EventCmdCallBack)(cJSON *cmd);
typedef int (*EventFilterCallBack)(cJSON *cmd);

typedef struct
{
		vdp_task_t			  		    task;
		Loop_vdp_common_buffer  	msgQ;
		vdp_task_t			  		    loopTask;
		Loop_vdp_common_buffer  	loopMsgQ;
		Loop_vdp_common_buffer  	loopSyncQ;
    cJSON                     *data;
    cJSON                     *eventCfg;
}EVENT_RUN_S;

typedef struct
{
    pthread_t	                tid;
  	EventCmdCallBack	        callback;
  	cJSON	                    *jsonPara;
}ParallelEventRun_S;

extern EVENT_RUN_S eventRun;

int vtk_TaskInit_Event(void);
int API_Event(const char* jsonString);
int API_Event_By_Name(const char* eventName);

int API_EventCallbackFunctionAdd(const char* callBackName, EventCmdCallBack callBack, const char* notes);
int API_EventCallbackFunctionDelete(const char* callBackName);

const char* GetEventItemString(cJSON* event,  const char* itemName);
int GetEventItemInt(cJSON* event,  const char* itemName);

#define EVENT_KEY_EventName			"EventName"
#define EVENT_KEY_Debug			    "Debug"
#define EVENT_KEY_Message			  "Message"



#define EventLockReq              "EventLockReq"            //本地开锁
#define EventUnlock_Request       "EventUnlock_Request"     //请求开远程的锁
#define EventLinkState            "EventLinkState"
#define EventCallState            "EventCallState"
#define EventCall                 "EventCall"
#define EventKey_A                "EventKEY_A"
#define EventKey_B                "EventKEY_B"
#define EventLockAction                     "EventLockAction"
#define Event_IPC_AUTO_CONFIG               "Event_IPC_AUTO_CONFIG"
#define EventTouchKeyDisplay                "EventTouchKeyDisplay"
#define Event_XD_REQ_LAN_RECONFIG           "XD-REQ-LAN-RECONFIG"
#define Event_XD_RSP_LAN_RECONFIG           "XD-RSP-LAN-RECONFIG"
#define Event_XD_REQ_ADD_WIFI               "XD-REQ-ADD-WIFI"
#define Event_XD_RSP_ADD_WIFI               "XD-RSP-ADD-WIFI"
#define EventCallAction                     "EventCallAction"

#define EventNetLinkState                     "EventNetLinkState"

#define EventUpdateStart                    "EventUpdateStart"
#define EventUpdateCancel                   "EventUpdateCancel"
#define EventUpdateReport                   "EventUpdateReport"
#define EventCard                           "EventCard"
#define EventCardState                      "EventCardState"
#define EventCardRoom                       "EventCardRoom"
#define EventManageCard                     "EventManageCard"
#define EventCardReport                     "EventCardReport"

#define Event_XD_REQ_CHECK_IM_ONLINE            "Event_XD_REQ_CHECK_IM_ONLINE"    //请求检测分机在线
#define Event_XD_RSP_CHECK_IM_ONLINE            "Event_XD_RSP_CHECK_IM_ONLINE"    //分机在线报告
#define Event_XD_REQ_Check_Password             "Event_XD-REQ-Check-Password"    //测试开锁密码是否正确
#define Event_XD_RSP_Check_Password             "Event_XD-RSP-Check-Password"    //测试开锁密码是否正确
#define Event_XD_RSP_Card_Management            "Event_XD-RSP-Card_Management"    //卡管理
#define Event_XD_RSP_Message                    "Event_XD_RSP_Message"            //app信息汇报
#define Event_XD_REQ_SIP_Management             "Event_XD_REQ_SIP_Management"    //请求检测分机在线

#define EventIXProg                     "EventIXProg"
#define EventIXProgCheck                "EventIXProgCheck"
#define EventIXSearch                   "EventIXSearch"

#define EventBECall                     		"EventBECall"
#define EventBECallRetrig                     	"EventBECallRetrig"
#define EventBECallClose                     	"EventBECallClose"
#define EventBECallBye                     	"EventBECallBye"
#define EventBECallRing                     	"EventBECallRing"
#define EventBECallTalk                     	"EventBECallTalk"
#define EventBECallDivert                     	"EventBECallDivert"
#define EventBECallAnswer				                 "EventBECallAnswer"

#define Event_IXS_GetRemoteTb				              "IXS_GetRemoteTb"
#define Event_IXS_ReqUpdateTb				              "IXS_ReqUpdateTb"
#define Event_IXS_ReportTbChanged				          "IXS_ReportTbChanged"
#define Event_IXS_ReportTbUnchanged				        "IXS_ReportTbUnchanged"
#define Event_DatasetUpdate				                "DatasetUpdate"
#define Event_IXGListUpdate				                "IXGListUpdate"
#define Event_MAT_Update				                  "MAT_Update"

#define EventMonOpen                     		"EventMonOpen"
#define EventMonClose                     	"EventMonClose"
#define EventMonState                     	"EventMonState"
#define EventGetMonList                    	"EventGetMonList"
#define EventGetNamelist                   	"EventGetNamelist"
#define EventCallRecord                   	"EventCallRecord"
#define EventPlayback                   	  "EventPlayback"

#define EventRecFileReq                   	"EventRecFileReq"
#define EventRecFileRsp                   	"EventRecFileRsp"
#define EventIX_HomeCall                   	"EventIX_HomeCall"
#define EventSetDivertState                 "EventSetDivertState"
#define EventRebootApp              "EventRebootApp"
#define EventHaveRec                   	"EventHaveRec"
#define EventMissCall                   	"EventMissCall"
#define EventNoDisturb              "EventNoDisturb"
#define EventReboot                 "EventReboot"
#define EventFactoryDefault         "EventFactoryDefault"
#define EventUpdateRequest          "EventUpdateRequest"
#define EventUpdateReportToRemote   "EventUpdateReportToRemote"
#define EventUnlockCodeReport       "EventUnlockCodeReport"
#define EventIO_Setting             "EventIO_Setting"
#define EventInputCancel            "EventInputCancel"
#define EventRemoteElog             "EventRemoteElog"
#define EventWillReboot             "EventWillReboot"
#define EventPowerOnAndConnected    "EventPowerOnAndConnected"
#define EventIXGCheckIn             "EventIXGCheckIn"
#define EventUpdateDT_IM_List       "EventUpdateDT_IM_List"
#define EventReportDT_IM_List       "EventReportDT_IM_List"
#define EventStartComplete          "EventStartComplete"            //启动完成
#define EventNetworkedReadiness     "EventNetworkedReadiness"       //网络就绪
#define EventAutoTest               "EventAutoTest"       //网络就绪
#define EventCertConfig             "EventCertConfig"       //证书配置
#define EventTransmission           "EventTransmission"       //转发事件

#define EventDFRMKeyInput                     	"EventDFRMKeyInput"

#define EventNDM_S_Ctrl             "EventNDM_S_Ctrl"         //NDM服务器控制
#define EventNDM_S_CheckEnd         "EventNDM_S_CheckEnd"    //NDM服务器定时检测客户端在线情况结束
#define EventNDM_C_Ctrl             "EventNDM_C_Ctrl"       //NDM客户端控制
#define EventNDM_C_CtrlResponse     "EventNDM_C_CtrlResponse"       //NDM客户端控制应答
#define EventSipManage   "EventSipManage"
#define EventCardSetupLed           "EventCardSetupLed"
#define EventCardIdReport           "EventCardIdReport"
#define Event_XD_SaveUserCard               "Event_XD_SaveUserCard"

#define Event_VideoSerError               "Event_VideoSerError"

#define EventCertManage   "EventCertManage"
#define Event_IPERF_TEST_START				            "IPERF_TEST_START"
#define Event_IPERF_TEST_REPORT				            "IPERF_TEST_REPORT"
#define EventDXGListRefresh				                "EventDXGListRefresh"
#define EventMenuResCheckOnline				            "EventMenuResCheckOnline"
#define EventMenuIpcManage				                "EventMenuIpcManage"

#define EventMenuSatHybridProg				            "MenuSatHybridProg"
#define EventDH_HybridProgOnline                  "DH_HybridProgOnline"
#define EventDH_HybridProgOnlineReport            "DH_HybridProgOnlineReport"
#define EventMenuSatHybridReport				          "MenuSatHybridReport"
#define EventMenuAptMatReport				              "MenuAptMatReport"
#define EventIM_PROG_REPORT				                "IM_PROG_REPORT"
#define EventIM_Regist				                    "IM_Regist"
#define EventMenuUpdateDhIM				                "MenuUpdateDhIM"
#define EventMenuSettingProcess				            "MenuSettingProcess"
#define EventReqSNFromServer						"ReqSNFromServer"
#define EventSetEocMacBySN						"SetEocMacBySN"
#define EventSnErrDisp								"SnErrDisp"
#define EventUpdateResFromSD								"UpdateResFromSD"
#define EventM4LEDDisplayBySort								"M4LEDDisplayBySort"
#define EventM4LEDDisplayImOnline								"M4LEDDisplayImOnline"
#define EventCmr7610Config				              "EventCmr7610Config"
#define EventBackupReq						"EventBackupReq"
#define EventRestoreReq						"EventRestoreReq"
#define EventVmManage             "EventVmManage"

int API_Event(const char* jsonString);
int API_Event_Json(cJSON* jsonEvent);
int API_Event_By_Name(const char* eventName);
int API_Event_NameAndMsg(const char* name, const char* format, ...);
int API_Event_UpdateReport(const char* state, const char* msg);
int API_Event_UpdateStart(const char* serverIp, const char* code, int checkTime, const char* source);
int API_EventDebug(const char* eventName, const char* format, ...);
int API_Event_NameAndDebugAndMsg(const char* name, const char* debugInfo, const char* format, ...);
#endif


