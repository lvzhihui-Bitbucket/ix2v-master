/**
  ******************************************************************************
  * @file    task_Event.c
  * @author  czb
  * @version V00.01.00
  * @date    2022.5.17
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 

#include <stdio.h>
#include <sys/sysinfo.h>
#include "task_Event.h"
#include "task_survey.h"
#include "define_file.h"
#include "elog.h"
#include "obj_IoInterface.h"

#define EVENT_KEY_Callback			"Callback"
#define EVENT_KEY_Notes				"Notes"
#define EVENT_KEY_Function			"Function"
#define EVENT_KEY_ExecutionMode		"ExecutionMode"
#define EVENT_KEY_ExecutionFilter	"ExecutionFilter"		//执行过滤函数，空函数或者返回1执行，返回0不执行

EVENT_RUN_S eventRun = {.data = NULL, .eventCfg = NULL};



void* vdp_event_loop_task( void* arg )
{
	vdp_task_t*	 ptask 		= (vdp_task_t*)arg;
	p_vdp_common_buffer pdb 	= 0;
	int	size;
	thread_log_add(__func__);
	pthread_detach(pthread_self());
	ptask->task_StartCompleted = 1;
	while( ptask->task_run_flag )
	{
		size = pop_vdp_common_queue( ptask->p_msg_buf, &pdb, 10000);
		if( size > 0 )
		{
			(*ptask->p_msg_buf->process)(pdb,size);
			purge_vdp_common_queue(ptask->p_msg_buf);
		}
		#if 0
		else
		{
			ptask->task_run_flag = 0;
		}
		#endif
		usleep(50*1000);
	}

	return 0;
}

static int API_EventLoop(const char* jsonString)
{
	int ret = 0;
	
	if(jsonString)
	{
		ret = !push_vdp_common_queue(&eventRun.loopMsgQ,  jsonString, strlen(jsonString)+1);
		if(!eventRun.loopTask.task_run_flag)
		{
			init_vdp_common_task(&eventRun.loopTask, MSG_ID_EVENT_LOOP, vdp_event_loop_task, &eventRun.loopMsgQ, &eventRun.loopSyncQ);
		}
	}

    return ret;
}

static void* parallelEventCallbackProcess(void* arg)
{
    ParallelEventRun_S *paralleEventRun = (ParallelEventRun_S*)arg;
	pthread_detach(pthread_self());
	if(paralleEventRun == NULL)
	{
		return;
	}

	if(paralleEventRun->callback)
	{
		(*paralleEventRun->callback)(paralleEventRun->jsonPara);
	}

	cJSON_Delete(paralleEventRun->jsonPara);
	free(paralleEventRun);
}

void vdp_event_mesg_data_process(char* msg_data, int len)
{
	int i;
	char* string;

    cJSON * cmdJson = cJSON_Parse(msg_data);
	cJSON * eventName = cJSON_GetObjectItemCaseSensitive(cmdJson, EVENT_KEY_EventName);

	cJSON * eventCfg[2];
	cJSON * eventCfgElement;
	cJSON * eventCallbackName;
	cJSON * eventCallback;
	cJSON * eventCallbackType;
	int loopEventFlag = 0;
	
	MyElogJson(ELOG_LVL_DEBUG, "Event center recv", cmdJson);

	if(cJSON_IsString(eventName))
	{
		char eventCfgName[200];
		snprintf(eventCfgName, 200, "CB_%s", eventName->valuestring);
		eventCfg[0] = cJSON_GetObjectItemCaseSensitive(eventRun.eventCfg, eventCfgName);
		eventCfg[1] = API_Para_Read_Public(eventCfgName);
		EventLog(cmdJson);

		for(i = 0; i < 2; i++)
		{
			cJSON_ArrayForEach(eventCfgElement, eventCfg[i])
			{
				//过滤事件
				eventCallbackName = cJSON_GetObjectItemCaseSensitive(eventRun.data, cJSON_GetStringValue(cJSON_GetObjectItemCaseSensitive(eventCfgElement, EVENT_KEY_ExecutionFilter)));
				if(eventCallbackName)
				{
					eventCallback = cJSON_GetObjectItemCaseSensitive(eventCallbackName, EVENT_KEY_Callback);
					if(eventCallback != NULL && eventCallback->valueint != 0)
					{
						if((*(EventFilterCallBack)eventCallback->valueint)(cmdJson) == 0)
						{
							continue;
						}
					}
				}

				//处理事件
				eventCallbackName = cJSON_GetObjectItemCaseSensitive(eventRun.data, cJSON_GetStringValue(cJSON_GetObjectItemCaseSensitive(eventCfgElement, EVENT_KEY_Function)));
				if(eventCallbackName)
				{
					eventCallbackType = cJSON_GetObjectItemCaseSensitive(eventCfgElement, EVENT_KEY_ExecutionMode);
					//排队处理事件
					if(eventCallbackType == NULL || eventCallbackType->valueint == 0)
					{
						if(loopEventFlag == 0)
						{
							loopEventFlag = API_EventLoop(msg_data);
						}
					}
					//并行处理事件
					else
					{
						eventCallback = cJSON_GetObjectItemCaseSensitive(eventCallbackName, EVENT_KEY_Callback);
						if(eventCallback != NULL && eventCallback->valueint != 0)
						{
							ParallelEventRun_S *paralleEventRun = malloc(sizeof(ParallelEventRun_S));
							if(paralleEventRun)
							{
								paralleEventRun->callback = eventCallback->valueint;
								paralleEventRun->jsonPara = cJSON_Duplicate(cmdJson, 1);
								if(pthread_create(&paralleEventRun->tid, NULL, parallelEventCallbackProcess, (void*)paralleEventRun) != 0)
								{
									cJSON_Delete(paralleEventRun->jsonPara);
									free(paralleEventRun);
									paralleEventRun = NULL;
									printf("Create event thread Failure\n");
								}
							}
						}
					}
				}
			}
		}
	}
	
	cJSON_Delete(cmdJson);
}



void vdp_event_loop_mesg_data_process(char* msg_data, int len)
{
	int i;
    cJSON * cmdJson = cJSON_Parse(msg_data);
	cJSON * eventName = cJSON_GetObjectItemCaseSensitive(cmdJson, EVENT_KEY_EventName);
	cJSON * eventCfg[2];
	cJSON * eventCfgElement;
	cJSON * eventCallbackName;
	cJSON * eventCallback;
	cJSON * eventCallbackType;

	if(cJSON_IsString(eventName))
	{
		char eventCfgName[200];
		snprintf(eventCfgName, 200, "CB_%s", eventName->valuestring);
		eventCfg[0] = cJSON_GetObjectItemCaseSensitive(eventRun.eventCfg, eventCfgName);
		eventCfg[1] = API_Para_Read_Public(eventCfgName);

		for(i = 0; i < 2; i++)
		{
			cJSON_ArrayForEach(eventCfgElement, eventCfg[i])
			{
				eventCallbackName = cJSON_GetObjectItemCaseSensitive(eventRun.data, cJSON_GetStringValue(cJSON_GetObjectItemCaseSensitive(eventCfgElement, EVENT_KEY_Function)));
				if(eventCallbackName)
				{
					eventCallbackType = cJSON_GetObjectItemCaseSensitive(eventCfgElement, EVENT_KEY_ExecutionMode);
					eventCallback = cJSON_GetObjectItemCaseSensitive(eventCallbackName, EVENT_KEY_Callback);
					if(eventCallbackType == NULL || eventCallbackType->valueint == 0)
					{
						if(eventCallback != NULL && eventCallback->valueint != 0)
						{
							(*(EventCmdCallBack)(eventCallback->valueint))(cmdJson);
						}
					}
				}
			}
		}
	}

	cJSON_Delete(cmdJson);
}

void EventCallbackFunctionRegister(void);

static void* vdp_Event_task( void* arg )
{
	vdp_task_t*	 ptask 		= (vdp_task_t*)arg;
	p_vdp_common_buffer pdb 	= 0;
	int	size;
	thread_log_add(__func__);
	EventCallbackFunctionRegister();
	API_Event_By_Name(EventCallState);

	ptask->task_StartCompleted = 1;
	while( ptask->task_run_flag )
	{
		size = pop_vdp_common_queue( ptask->p_msg_buf, &pdb, VDP_QUEUE_POLLING_TIME);
		if( size > 0 )
		{
			(*ptask->p_msg_buf->process)(pdb,size);
			purge_vdp_common_queue( ptask->p_msg_buf );
		}
		usleep(50*1000);
	}
	return 0;
}

int vtk_TaskInit_Event(void)
{
	init_vdp_common_queue(&eventRun.loopMsgQ, 10000, (msg_process)vdp_event_loop_mesg_data_process, &eventRun.loopTask);
	init_vdp_common_queue(&eventRun.loopSyncQ, 100, NULL,&eventRun.loopTask);
	init_vdp_common_queue(&eventRun.msgQ, 10000, (msg_process)vdp_event_mesg_data_process, &eventRun.task);
	init_vdp_common_task(&eventRun.task, MSG_ID_EVENT, vdp_Event_task, &eventRun.msgQ, NULL);

	//MyPrintJson("eventRun.eventCfg", eventRun.eventCfg);
}

int API_EventCallbackFunctionAdd(const char* callBackName, EventCmdCallBack callBack, const char* notes)
{
	cJSON *temp;
	cJSON *cmdObj;

    if(eventRun.data == NULL)
	{
		eventRun.data = cJSON_CreateObject();
	}

	temp = cJSON_GetObjectItemCaseSensitive(eventRun.data, callBackName);
	if(temp == NULL)
	{
		cmdObj = cJSON_CreateObject();
		cJSON_AddItemToObject(cmdObj, EVENT_KEY_Callback, cJSON_CreateNumber((int)callBack));
		cJSON_AddItemToObject(cmdObj, EVENT_KEY_Notes, cJSON_CreateString(notes));
		cJSON_AddItemToObject(eventRun.data, callBackName, cmdObj);

		return 1;
	}

	return 0;
}

int API_EventFilterCallbackFunctionAdd(const char* callBackName, EventFilterCallBack callBack, const char* notes)
{
	cJSON *temp;
	cJSON *cmdObj;

    if(eventRun.data == NULL)
	{
		eventRun.data = cJSON_CreateObject();
	}

	temp = cJSON_GetObjectItemCaseSensitive(eventRun.data, callBackName);
	if(temp == NULL)
	{
		cmdObj = cJSON_CreateObject();
		cJSON_AddItemToObject(cmdObj, EVENT_KEY_Callback, cJSON_CreateNumber((int)callBack));
		cJSON_AddItemToObject(cmdObj, EVENT_KEY_Notes, cJSON_CreateString(notes));
		cJSON_AddItemToObject(eventRun.data, callBackName, cmdObj);

		return 1;
	}

	return 0;
}

int API_EventCallbackFunctionDelete(const char* callBackName)
{
    if(eventRun.data != NULL)
	{
		cJSON_DeleteItemFromObjectCaseSensitive(eventRun.data, callBackName);
	}

	return 1;
}



/*
代码中配置事件回调函数
eventName -- 事件名
callBackName -- 回调函数名
executionMode -- 执行顺序， 0排队执行，1并行执行
filterCallBackName --过滤执行函数名。
*/
int API_EventConfigAdd(const char* eventName, const char* callBackName, int executionMode, const char* filterCallBackName)
{
	cJSON *eventCfg;
	cJSON *eventCfgElement;
	char eventCfgName[200];
	char* funName;

	if(callBackName == NULL)
	{
		return 0;
	}

	snprintf(eventCfgName, 200, "CB_%s", eventName);

    if(eventRun.eventCfg == NULL)
	{
		eventRun.eventCfg = cJSON_CreateObject();
	}

	eventCfg = cJSON_GetObjectItemCaseSensitive(eventRun.eventCfg, eventCfgName);
	if(eventCfg == NULL)
	{
		eventCfg = cJSON_AddArrayToObject(eventRun.eventCfg, eventCfgName);
	}

	cJSON_ArrayForEach(eventCfgElement, eventCfg)
	{
		funName = cJSON_GetStringValue(cJSON_GetObjectItemCaseSensitive(eventCfgElement, EVENT_KEY_Function));
		if(!strcmp(callBackName, funName))
		{
			//已经存在，不必重复添加
			return 0;
		}
	}

	eventCfgElement = cJSON_CreateObject();
	cJSON_AddItemToObject(eventCfgElement, EVENT_KEY_Function, cJSON_CreateString(callBackName));
	cJSON_AddItemToObject(eventCfgElement, EVENT_KEY_ExecutionMode, cJSON_CreateNumber(executionMode));

	//过滤回调函数
	if(filterCallBackName)
	{
		cJSON_AddItemToObject(eventCfgElement, EVENT_KEY_ExecutionFilter, cJSON_CreateString(filterCallBackName));
	}

	cJSON_AddItemToArray(eventCfg, eventCfgElement);

	return 1;
}

//代码中删除事件回调函数
int API_EventConfigDelete(const char* eventName, const char* callBackName)
{
	cJSON *eventCfg;
	cJSON *eventCfgElement;
	char eventCfgName[200];
	char* funName;
	int arraySize;

	if(callBackName == NULL)
	{
		return 0;
	}

	snprintf(eventCfgName, 200, "CB_%s", eventName);
	eventCfg = cJSON_GetObjectItemCaseSensitive(eventRun.eventCfg, eventCfgName);
	if(eventCfg == NULL)
	{
		return 0;
	}

	arraySize = cJSON_GetArraySize(eventCfg);
	while(arraySize)
	{
		arraySize--;
		eventCfgElement = cJSON_GetArrayItem(eventCfg, arraySize);
		funName = cJSON_GetStringValue(cJSON_GetObjectItemCaseSensitive(eventCfgElement, EVENT_KEY_Function));
		if(!strcmp(callBackName, funName))
		{
			//已经存在，则删除
			cJSON_DeleteItemFromArray(eventCfg, arraySize);
			return 1;
		}
	}

	return 0;
}




int API_Event(const char* jsonString)
{
	int ret = 0;
	
	if(jsonString)
	{
		ret = !push_vdp_common_queue(&eventRun.msgQ,  jsonString, strlen(jsonString)+1);
	}

    return ret;
}

int API_Event_Json(cJSON* jsonEvent)
{
	int ret = 0;

	char* eventStr = cJSON_PrintUnformatted(jsonEvent);
	if(eventStr)
	{
		ret = API_Event(eventStr);
		free(eventStr);
	}

    return ret;
}


int API_Event_By_Name(const char* eventName)
{
	int ret = 0;
	cJSON *event = cJSON_CreateObject();
	cJSON_AddStringToObject(event, EVENT_KEY_EventName, eventName);
	ret = API_Event_Json(event);
	cJSON_Delete(event);

	return ret;
}

int API_Event_NameAndMsg(const char* name, const char* format, ...)
{
	int ret = 0;
	char output[400] = {0};
	if(format)
	{
		va_list valist;
		va_start(valist, format);
		ret = vsnprintf(output, 400, format, valist);
		va_end(valist);
	}

	cJSON *event = cJSON_CreateObject();
	cJSON_AddStringToObject(event, EVENT_KEY_EventName, name);
	cJSON_AddStringToObject(event, EVENT_KEY_Message, output);
	ret = API_Event_Json(event);
	cJSON_Delete(event);

	return ret;
}

int API_Event_UpdateReport(const char* state, const char* msg)
{
	int ret = 0;
	cJSON *event = cJSON_CreateObject();
	cJSON_AddStringToObject(event, EVENT_KEY_EventName, EventUpdateReport);
	cJSON_AddStringToObject(event, "State", state);
	cJSON_AddStringToObject(event, EVENT_KEY_Message, msg);
	ret = API_Event_Json(event);
	cJSON_Delete(event);

	if(msg)
	{
		API_Event_NameAndMsg(Event_XD_RSP_Message, "%s:%s", state, msg);
	}
	else
	{
		API_Event_NameAndMsg(Event_XD_RSP_Message, "%s", state);
	}

	return ret;
}

int API_Event_UpdateStart(const char* serverIp, const char* code, int checkTime, const char* source)
{
	int ret = 0;
	cJSON *event = cJSON_CreateObject();
	cJSON_AddStringToObject(event, EVENT_KEY_EventName, EventUpdateStart);
	cJSON_AddStringToObject(event, "Server", serverIp);
	cJSON_AddStringToObject(event, "Code", code);
	cJSON_AddNumberToObject(event, "CheckTime", checkTime);
	cJSON_AddStringToObject(event, "Source", source);
	ret = API_Event_Json(event);
	cJSON_Delete(event);

	return ret;
}

int API_Event_Unlock(char* lockNbr, char* source)
{
	int ret;

	cJSON* event = cJSON_CreateObject();
	cJSON_AddStringToObject(event, IX2V_EventName, EventLockReq);
	cJSON_AddStringToObject(event, IX2V_LOCK_NBR, lockNbr);
	cJSON_AddTrueToObject(event, "LOCK_PROCESS");
	cJSON_AddNumberToObject(event, "TIME",0);
	cJSON_AddStringToObject(event, IX2V_SOURCE, source);
	ret = API_Event_Json(event);
	cJSON_Delete(event);

	return ret;
}

int API_Event_Unlock2(char* lockNbr, int state, char* source)
{
	int ret;

	cJSON* event = cJSON_CreateObject();
	cJSON_AddStringToObject(event, IX2V_EventName, EventLockReq);
	cJSON_AddStringToObject(event, IX2V_LOCK_NBR, lockNbr);
	if(state)
	{
		cJSON_AddTrueToObject(event, "LOCK_PROCESS");
	}
	else
	{
		cJSON_AddFalseToObject(event, "LOCK_PROCESS");
	}

	cJSON_AddNumberToObject(event, "TIME",0);
	cJSON_AddStringToObject(event, IX2V_SOURCE, source);
	ret = API_Event_Json(event);
	cJSON_Delete(event);

	return ret;
}

int API_Event_UnlockRequest(char* ipAddr, char* lockNbr, char* source)
{
	int ret;

	cJSON* event = cJSON_CreateObject();
	cJSON_AddStringToObject(event, IX2V_EventName, EventUnlock_Request);
	cJSON_AddStringToObject(event, IX2V_IP_ADDR, ipAddr);
	cJSON_AddStringToObject(event, IX2V_LOCK_NBR, lockNbr);
	cJSON_AddStringToObject(event, IX2V_SOURCE, source);
	ret = API_Event_Json(event);
	cJSON_Delete(event);

	return ret;
}

int API_Event_IO_Setting(int callback, cJSON* old, cJSON* new)
{
	int ret;

	cJSON* event = cJSON_CreateObject();
	cJSON_AddStringToObject(event, IX2V_EventName, EventIO_Setting);
	cJSON_AddNumberToObject(event, "CALLBACK", callback);
	cJSON_AddItemToObject(event, "OLD_VALUE", cJSON_Duplicate(old, 1));
	cJSON_AddItemToObject(event, "NEW_VALUE", cJSON_Duplicate(new, 1));
	ret = API_Event_Json(event);
	cJSON_Delete(event);

	return ret;
}

int API_EventDebug(const char* eventName, const char* format, ...)
{
	int ret = 0;
	char output[400] = {0};
	if(format)
	{
		va_list valist;
		va_start(valist, format);
		ret = vsnprintf(output, 400, format, valist);
		va_end(valist);
	}

	cJSON *event = cJSON_CreateObject();
	cJSON_AddStringToObject(event, EVENT_KEY_EventName, eventName);
	cJSON_AddStringToObject(event, EVENT_KEY_Debug, output);
	ret = API_Event_Json(event);
	cJSON_Delete(event);

	return ret;
}

int API_Event_NameAndDebugAndMsg(const char* name, const char* debugInfo, const char* format, ...)
{
	int ret = 0;
	char output[400] = {0};
	if(format)
	{
		va_list valist;
		va_start(valist, format);
		ret = vsnprintf(output, 400, format, valist);
		va_end(valist);
	}

	cJSON *event = cJSON_CreateObject();
	cJSON_AddStringToObject(event, EVENT_KEY_EventName, name);
	cJSON_AddStringToObject(event, EVENT_KEY_Message, output);
	cJSON_AddStringToObject(event, EVENT_KEY_Debug, debugInfo);
	ret = API_Event_Json(event);
	cJSON_Delete(event);

	return ret;
}

int API_EventTransmit_Json(int ip, cJSON* jsonEvent)
{
	if(ip == GetMyIpByTargetIp(ip))
	{
		API_Event_Json(jsonEvent);
	}
	else
	{
		API_XD_EventJson(ip, jsonEvent);
	}
}
/*********************************************************************************************************
**  End Of File
*********************************************************************************************************/

