/**
  ******************************************************************************
  * @file    obj_EventCallback.c
  * @author  czb
  * @version V00.01.00
  * @date    2022.5.17
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 

#include <dirent.h>
#include <sys/stat.h>
#include "task_Event.h"
#include "obj_PublicInformation.h"
#include "task_Beeper.h"
#include "elog.h"
#include "define_string.h"
#include "obj_IoInterface.h"
#include "task_StackAI.h"
#include "obj_APCIService.h"
#include "obj_TouchKey.h"
#include "obj_MDS_Output.h"
#include "task_WiFiConnect.h"
#include "obj_lvgl_msg.h"
#include "obj_DT_CheckOnline.h"

static int debugState = 0;
static CONNECT_PHONE_RUN_S phoneConnect = {.lock = PTHREAD_MUTEX_INITIALIZER};

int GetDebugState(void)
{
	return debugState;
}

void ReportDebugState(void)
{
	if(debugState)
	{
		API_Stack_New_BRD_Without_ACK(APCI_DX_SET_ON);
	}
	else
	{
		API_Stack_New_BRD_Without_ACK(APCI_DX_SET_OFF);
	}
}

void DebugStateOnReport(void)
{
	if(debugState)
	{
		API_Stack_New_BRD_Without_ACK(APCI_DX_SET_ON);
	}
}

static int StopDisplaySipNetworkState(int timing)
{
	API_PublicInfo_Write_Bool(PB_CHECK_SIP_STATE, 0);
	API_Event_By_Name(EventTouchKeyDisplay);
	log_v("StopDisplaySipNetworkState------------------------");
	return 2;
}

static int StopDisplayXDState(int timing)
{
	API_PublicInfo_Write_Bool(PB_CHECK_XD_STATE, 0);
	API_Event_By_Name(EventTouchKeyDisplay);
	log_v("StopDisplayXDState------------------------");
	return 2;
}

static int StopDisplayPhoneConnectState(int timing)
{
	if(timing >= 5)
	{
		phoneConnect.state = 0;
		API_PublicInfo_Write_Int(PB_CONNECT_PHONE_STATE, phoneConnect.state);
		API_Event_By_Name(EventTouchKeyDisplay);
		log_v("StopDisplayPhoneConnectState------------------------");
		return 2;
	}

	return 0;
}

static int displayLanStateFlag = 0;
static int displayWlanStateFlag = 0;

static int StopDisplayLanState(int timing)
{
	if(timing >= 5)
	{
		if(displayLanStateFlag)
		{
			API_LED_ctrl("LED_1", "MSG_LED_OFF");
			displayLanStateFlag = 0;
		}
		return 2;
	}

	return 0;
}

static int StopDisplayWlanState(int timing)
{
	if(timing >= 5)
	{
		if(displayWlanStateFlag)
		{
			API_LED_ctrl("LED_2", "MSG_LED_OFF");
			displayWlanStateFlag = 0;
		}
		return 2;
	}

	return 0;
}
int Led_DisplayNetState(cJSON *cmd)
{
    //上电5分钟之内
    	char* pbEocMacState = API_PublicInfo_Read_String(PB_EOC_Mac_State);
	char* pbEocState = API_PublicInfo_Read_String(PB_EOC_state);
	if(pbEocMacState&&pbEocState)
	{
		if(!IfHasSn())
		{
			//API_LED_ctrl("LED_2", "MSG_LED_FLASH_FAST");
			//API_LED_ctrl("LED_1", "MSG_LED_FLASH_FAST");
			return;
		}
		if(strcmp(pbEocState,"unlink")!=0&& strcmp(pbEocMacState, "Ok")!=0)
		{
			//API_LED_ctrl("LED_2", "MSG_LED_FLASH_FAST");
			//API_LED_ctrl("LED_1", "MSG_LED_FLASH_SLOW");
			return;
		}
	}
	if(!API_PublicInfo_Read_Bool(PB_StartUp5MinutesLater))
	{
		char* lanState = API_PublicInfo_Read_String(PB_SUB_LAN_STATE);
		if(lanState && !strcmp(lanState, "Connected"))
		{
			displayLanStateFlag = 1;
			API_LED_ctrl("LED_1", "MSG_LED_ON");
			API_Add_TimingCheck(StopDisplayLanState, 1);
		}

		char* wlanState = API_PublicInfo_Read_String(PB_SUB_WLAN_STATE);
		if(wlanState && !strcmp(wlanState, "Connected"))
		{
			displayWlanStateFlag = 1;
			API_LED_ctrl("LED_2", "MSG_LED_ON");
			API_Add_TimingCheck(StopDisplayWlanState, 1);
		}
	}

}


typedef struct
{
	int flag;
	DT_CHECK_DATA_T offline;
	DT_CHECK_DATA_T online;
	cJSON* result;
}DT_CHECK_RUN_S;

static DT_CHECK_RUN_S dtCheck;

const char *get_time_string(void) {
    static char cur_system_time[24] = { 0 };
    time_t cur_t;
    struct tm cur_tm;

    time(&cur_t);
    localtime_r(&cur_t, &cur_tm);

    strftime(cur_system_time, sizeof(cur_system_time), "%Y-%m-%d %T", &cur_tm);

    return cur_system_time;
}

cJSON* GreateCheckImOnlineResultToJson(DT_CHECK_RUN_S check)
{
	int i, roomNbr;
	cJSON* deviceNbr;
	if(dtCheck.result)
	{
		cJSON_Delete(dtCheck.result);
	}

	dtCheck.result = cJSON_CreateObject();
	
	cJSON_AddStringToObject(dtCheck.result, "ON_LINE_SearchTime", get_time_string());
	cJSON_AddNumberToObject(dtCheck.result, "ON_LINE_CNT", check.online.cnt);
	cJSON_AddNumberToObject(dtCheck.result, "OFF_LINE_CNT", check.offline.cnt);
	deviceNbr = cJSON_AddArrayToObject(dtCheck.result, "ON_LINE_IM");
	for(i = 0; i < check.online.cnt; i++)
	{
		roomNbr = ((check.online.addr[i] == 0x80)? 32 : (check.online.addr[i] - 0x80)/4);
		cJSON_AddItemToArray(deviceNbr, cJSON_CreateNumber(roomNbr));
	}

	deviceNbr = cJSON_AddArrayToObject(dtCheck.result, "OFF_LINE_IM");
	for(i = 0; i < check.offline.cnt; i++)
	{
		roomNbr = ((check.offline.addr[i] == 0x80)? 32 : (check.offline.addr[i] - 0x80)/4);
		cJSON_AddItemToArray(deviceNbr, cJSON_CreateNumber(roomNbr));
	}
	

	return dtCheck.result;
}

void StatisticalCheckImOnlineResult(DT_CHECK_RESULT_T *check)
{
    if(dtCheck.flag)
	{
		dtCheck.flag++;
		if(!check->result)
		{
			dtCheck.offline.addr[dtCheck.offline.cnt++] = check->addr;
		}
		else
		{
			dtCheck.online.addr[dtCheck.online.cnt++] = check->addr;
		}
    }
}

void M4_DisplayDtImOnline(DT_CHECK_RESULT_T *check)
{
	if(check->result)
	{
		if(check->addr == 0x80)
		{
			API_M4_LED(32, EXT_LED_ON);
		}
		else
		{
			API_M4_LED((check->addr - 0x80)/4, EXT_LED_ON);
		}
	}
}

void XD_ReportDtImOnline(DT_CHECK_RESULT_T *check)
{
	int addr = ((check->addr == 0x80) ? 32 : ((check->addr - 0x80)/4));
	char*state = (check->result) ? "Online" : "Offline";
	char reportMsg[50];

	snprintf(reportMsg, 50, "IM [%02d] is %s", addr, state);

	API_Event_NameAndMsg(Event_XD_RSP_CHECK_IM_ONLINE, reportMsg);
}


static void Check_DT_IM_Online(void)
{
	int i, j;
	int roomBbr;
	DT_CHECK_DATA_T data;
	int secondCnt;

	//在忙
	if(dtCheck.flag)
	{
		return;
	}

	API_ALL_M4_LED(EXT_LED_OFF);

	for(data.cnt = 0, i = 0; i < 8; i++)
	{
		if(IsMDS_Online(i))
		{
			for(j = 0; j < 4; j++)
			{
				roomBbr = 4 * i + j + 1;
				roomBbr = ((roomBbr >= 32) ? 0 : roomBbr);
				data.addr[data.cnt++] = 0x80+4*roomBbr;
			}
		}
	}

	if(data.cnt)
	{
		dtCheck.online.cnt = 0;
		dtCheck.offline.cnt = 0;
		dtCheck.flag = 1;
		vtk_lvgl_lock();
	    lv_msg_send(MSG_DT_CHECK_ONLINE_REQ, &data);
		vtk_lvgl_unlock();
		while(dtCheck.flag <= data.cnt)
		{
			usleep(500*1000);
		}

		usleep(500*1000);
		//重新检测一次不在线的设备
		if(dtCheck.offline.cnt)
		{
			data = dtCheck.offline;
			dtCheck.offline.cnt = 0;
			dtCheck.flag = 1;
			vtk_lvgl_lock();
			lv_msg_send(MSG_DT_CHECK_ONLINE_REQ, &data);
			vtk_lvgl_unlock();
			while(dtCheck.flag <= secondCnt)
			{
				usleep(500*1000);
			}
		}
		API_PublicInfo_Write_Int(PB_OnlineMonitors, dtCheck.online.cnt);
		sleep(2);
	}
	API_ALL_M4_LED(EXT_LED_FLASH_FAST);

	sleep(2);
	API_ALL_M4_LED(EXT_LED_ON);
	dtCheck.flag = 0;
}

int Event_XD_CheckImOnlne(cJSON *cmd)
{
	int roomBbr;
	DT_CHECK_DATA_T data;
	int secondCnt;
	int start, end;

	//在忙
	if(dtCheck.flag)
	{
		return;
	}

	API_ALL_M4_LED(EXT_LED_OFF);

	start = atoi(GetEventItemString(cmd,  "START"));
	end = atoi(GetEventItemString(cmd,  "END"));

	dprintf("start=%d, end = %d\n", start, end);

	for(data.cnt = 0, roomBbr = start; data.cnt < 32 && roomBbr <= end; roomBbr++)
	{
		data.addr[data.cnt++] = 0x80 + 4*((roomBbr >= 32) ? 0 : roomBbr);
	}

	if(data.cnt)
	{
		dtCheck.online.cnt = 0;
		dtCheck.offline.cnt = 0;
		dtCheck.flag = 1;
		vtk_lvgl_lock();
	    lv_msg_send(MSG_DT_CHECK_ONLINE_REQ, &data);
		vtk_lvgl_unlock();
		while(dtCheck.flag <= data.cnt)
		{
			usleep(500*1000);
		}

		usleep(500*1000);
		//重新检测一次不在线的设备
		if(dtCheck.offline.cnt)
		{
			data = dtCheck.offline;
			dtCheck.offline.cnt = 0;
			dtCheck.flag = 1;
			vtk_lvgl_lock();
			lv_msg_send(MSG_DT_CHECK_ONLINE_REQ, &data);
			vtk_lvgl_unlock();
			while(dtCheck.flag <= data.cnt)
			{
				usleep(500*1000);
			}
		}

		API_PublicInfo_Write_Int(PB_OnlineMonitors, dtCheck.online.cnt);
		API_Event_NameAndMsg(Event_XD_RSP_CHECK_IM_ONLINE, "OnlineMonitors:%d", dtCheck.online.cnt);
		sleep(2);
	}

	API_ALL_M4_LED(EXT_LED_FLASH_FAST);

	sleep(2);
	API_ALL_M4_LED(EXT_LED_ON);
	dtCheck.flag = 0;
}

static void DebugStateProcess(int state)
{
	if(state)
	{
		//1、启动调试模式
		#if !defined(PID_IX850) && !defined(PID_IX611) && !defined(PID_IX622)&& !defined(PID_IX821)
		MenuScr2Test();
		#endif
		ReportDebugState();

		//2、查询分机在线状态（同821-VD）利用M4模块显示各分机
		Check_DT_IM_Online();

		//3、启动IX-Cloud-Proxy连接
		if(!IsXDRemoteConnect())
		{
			XDRemoteConnect(NULL);
		}
	}
	else
	{
		//退出调试模式
		#if !defined(PID_IX850) && !defined(PID_IX611) && !defined(PID_IX622)&& !defined(PID_IX821)
		video_turn_on_init();
		#endif
		ReportDebugState();
	}
	return 1;
}

void CB_SET_SipRouteRespond(char  *set_route)
{
	//LV_UNUSED(s);

    //if(lv_msg_get_id(m) == MSG_SETTING_SIP_ROUTE_RSP)
	{
		//char  *set_route = lv_msg_get_payload(m);
		if(set_route==NULL)
			return;

		if(set_route[0] == 0)
		{
			//wifi已经连接
			if(phoneConnect.state == 2)
			{
				phoneConnect.saveXdState = IsXDRemoteConnect();
				if(phoneConnect.saveXdState)
				{
					XDRemoteDisconnect();
				}
				
				if(XDRemoteConnect("{\"IXD_NT\":\"wlan0\"}"))
				{
					API_Para_Write_Int(IXD_AutoConnect, 1);
					phoneConnect.state = 3;
					API_PublicInfo_Write_Int(PB_CONNECT_PHONE_STATE, phoneConnect.state);
					API_Event_By_Name(EventTouchKeyDisplay);
					API_Add_TimingCheck(StopDisplayPhoneConnectState, 1);
				}
				else
				{
					//恢复原来的状态
					if(!phoneConnect.saveXdState)
					{
						XDRemoteDisconnect();
					}
				}
			}
		}
    }
}

static int StopPhoneConnect(int timing);
static int StopDisplayPhoneConnectState(int timing);

static void ConnectPhone_inform_callback(int inform_code,void *ext_para)
{
	pthread_mutex_lock(&phoneConnect.lock);
	switch(inform_code)
	{
		/*因为需要尝试2个ssid
		case WLAN_INFORM_CODE_DISCONNECT:
			if(phoneConnect.state)
			{
				//连接失败
				phoneConnect.state = 4;
				API_PublicInfo_Write_Int(PB_CONNECT_PHONE_STATE, phoneConnect.state);
				API_Event_By_Name(EventTouchKeyDisplay);
				API_Add_TimingCheck(StopDisplayPhoneConnectState, 5);
				API_Del_TimingCheck(StopPhoneConnect);
				API_Del_Wlan_Inform_Callback(ConnectPhone_inform_callback);
			}
			break;
		*/

		case WLAN_INFORM_CODE_CONNECTING:
			if(phoneConnect.state == 0 && ext_para)
			{
				if((phoneConnect.ssid1 && !strcmp(phoneConnect.ssid1, ext_para)) || 
				(phoneConnect.ssid2 && !strcmp(phoneConnect.ssid2, ext_para)))
				{
					phoneConnect.state = 1;
					API_PublicInfo_Write_Int(PB_CONNECT_PHONE_STATE, phoneConnect.state);
					API_Event_By_Name(EventTouchKeyDisplay);
				}
			}
			break;

		case WLAN_INFORM_CODE_CONNECTED:
			if(phoneConnect.state == 1 && ext_para)
			{
				if((phoneConnect.ssid1 && !strcmp(phoneConnect.ssid1, ext_para)) || 
				(phoneConnect.ssid2 && !strcmp(phoneConnect.ssid2, ext_para)))
				{
					API_Del_Wlan_Inform_Callback(ConnectPhone_inform_callback);
					//连接成功
					phoneConnect.state = 2;
					//vtk_lvgl_lock();
					//lv_msg_send(MSG_SETTING_SIP_ROUTE, "WLAN");
					CB_SET_SipRoute("WLAN");
					//vtk_lvgl_unlock();

					API_PublicInfo_Write_Int(PB_CONNECT_PHONE_STATE, phoneConnect.state);
					API_Event_By_Name(EventTouchKeyDisplay);
					API_Add_TimingCheck(StopDisplayPhoneConnectState, 5);
					API_Del_TimingCheck(StopPhoneConnect);
				}
			}
			break;	
	}

	pthread_mutex_unlock(&phoneConnect.lock);
}

static int StopPhoneConnect(int timing)
{
	if(timing >= 30)
	{
		//连接超时
		phoneConnect.state = 4;
		API_PublicInfo_Write_Int(PB_CONNECT_PHONE_STATE, phoneConnect.state);
		API_Event_By_Name(EventTouchKeyDisplay);
		API_Add_TimingCheck(StopDisplayPhoneConnectState, 5);
		API_Del_Wlan_Inform_Callback(ConnectPhone_inform_callback);

		if(phoneConnect.saveWifiSwitch)
		{
			API_Wlan_Open(NULL);
		}
		else
		{
			API_Wlan_Close();
		}

		log_v("StopPhoneConnect------------------------");
		return 2;
	}

	return 0;
}

static void StartConnectPhoneProcess(void)
{
	if(!API_PublicInfo_Read_Int(PB_CONNECT_PHONE_STATE))
	{
		cJSON* wifiPara = cJSON_CreateArray();

		char* wifiSw = API_PublicInfo_Read_String(PB_WLAN_SW);
		API_PublicInfo_Write_Int(PB_CONNECT_PHONE_STATE, 1);
		API_Event_By_Name(EventTouchKeyDisplay);
		usleep(100*1000);
		API_Add_TimingCheck(StopPhoneConnect, 30);

		pthread_mutex_lock(&phoneConnect.lock);
		phoneConnect.saveWifiSwitch = ((wifiSw == NULL || !strcmp(wifiSw, "OFF")) ? 0 : 1);
		phoneConnect.state = 0;
		phoneConnect.ssid1 = API_Para_Read_String2(Mobile_SSID_NAME1);
		phoneConnect.ssid2 = API_Para_Read_String2(Mobile_SSID_NAME2);
	
		if(phoneConnect.ssid2 != NULL && phoneConnect.ssid2[0] != 0)
		{
			cJSON* para2 = cJSON_CreateObject();
			cJSON_AddStringToObject(para2, "SSID_NAME", phoneConnect.ssid2);
			cJSON_AddStringToObject(para2, "SSID_PWD", API_Para_Read_String2(Mobile_SSID_PWD2));
			cJSON_AddItemToArray(wifiPara, para2);
		}

		if(phoneConnect.ssid1 != NULL && phoneConnect.ssid1[0] != 0)
		{
			cJSON* para1 = cJSON_CreateObject();
			cJSON_AddStringToObject(para1, "SSID_NAME", phoneConnect.ssid1);
			cJSON_AddStringToObject(para1, "SSID_PWD", API_Para_Read_String2(Mobile_SSID_PWD1));
			cJSON_AddItemToArray(wifiPara, para1);
		}

		API_Add_Wlan_Inform_Callback(ConnectPhone_inform_callback);
		API_Wlan_Close();
		MyPrintJson("wifiPara", wifiPara);
		API_Wlan_Open(wifiPara);
		cJSON_Delete(wifiPara);

		pthread_mutex_unlock(&phoneConnect.lock);
	}
}

static TOUCH_KEY_RUN_S keyA;
static TOUCH_KEY_RUN_S keyB;

static int TouchKey_A_Timer(int timing)
{
	if(timing >= 3)
	{
		switch(keyA.status)
		{
			case KEY_STATUS_PRESS_3S:
				BEEP_KEY();
				API_LED_ctrl("LED_1", "MSG_LED_OFF");
				API_LED_ctrl("LED_2", "MSG_LED_FLASH_SLOW");
				keyA.status = KEY_STATUS_PRESS_6S;
				bprintf("AKEY_STATUS_PRESS_6S\n");
				break;
			case KEY_STATUS_PRESS_6S:
				BEEP_KEY();
				API_LED_ctrl("LED_1", "MSG_LED_FLASH_SLOW");
				API_LED_ctrl("LED_2", "MSG_LED_FLASH_SLOW");
				keyA.status = KEY_STATUS_PRESS_9S;
				bprintf("AKEY_STATUS_PRESS_9S\n");
				break;
			case KEY_STATUS_PRESS_9S:
				BEEP_KEY();
				API_LED_ctrl("LED_1", "MSG_LED_OFF");
				API_LED_ctrl("LED_2", "MSG_LED_OFF");
				keyA.status = KEY_STATUS_PRESS_12S;
				bprintf("AKEY_STATUS_PRESS_12A\n");
				return 2;
		}
		return 1;
	}

	return 0;
}

static int TouchKey_B_Timer(int timing)
{
	if(timing >= 3)
	{
		switch(keyB.status)
		{
			case KEY_STATUS_PRESS_3S:
				BEEP_KEY();
				API_LED_ctrl("LED_1", "MSG_LED_OFF");
				API_LED_ctrl("LED_2", "MSG_LED_FLASH_SLOW");
				keyB.status = KEY_STATUS_PRESS_6S;
				bprintf("BKEY_STATUS_PRESS_6S\n");
				break;
			case KEY_STATUS_PRESS_6S:
				BEEP_KEY();
				API_LED_ctrl("LED_1", "MSG_LED_FLASH_SLOW");
				API_LED_ctrl("LED_2", "MSG_LED_FLASH_SLOW");
				keyB.status = KEY_STATUS_PRESS_9S;
				bprintf("BKEY_STATUS_PRESS_9S\n");
				break;
			case KEY_STATUS_PRESS_9S:
				BEEP_KEY();
				API_LED_ctrl("LED_1", "MSG_LED_OFF");
				API_LED_ctrl("LED_2", "MSG_LED_OFF");
				keyB.status = KEY_STATUS_PRESS_12S;
				bprintf("BKEY_STATUS_PRESS_12S\n");
				return 2;
		}
		return 1;
	}

	return 0;
}

int TouchKey_AB_ToEvent(key_id_t keyId, key_status_t keyStatus)
{
	if(keyId == KEY_A)
	{
		switch(keyStatus)
		{
			case KEY_STATUS_PRESS:
				BEEP_KEY();
				keyA.status = KEY_STATUS_PRESS;
				break;
			case KEY_STATUS_RELEASE:
				keyA.status = KEY_STATUS_RELEASE;
				//查询网络状态（显示应用）
				if(!API_PublicInfo_Read_Bool(PB_CHECK_SIP_STATE))
				{
					API_PublicInfo_Write_Bool(PB_CHECK_SIP_STATE, 1);
					API_Event_By_Name(EventTouchKeyDisplay);
					API_Add_TimingCheck(StopDisplaySipNetworkState, 5);
				}
				break;
			case KEY_STATUS_PRESS_3S:
				BEEP_KEY();
				API_LED_ctrl("LED_1", "MSG_LED_FLASH_SLOW");
				API_LED_ctrl("LED_2", "MSG_LED_OFF");
				keyA.status = KEY_STATUS_PRESS_3S;
				API_Add_TimingCheck(TouchKey_A_Timer, 1);
				break;
			case KEY_STATUS_LONG_PRESS_RELEASE:
				API_Del_TimingCheck(TouchKey_A_Timer);
				if(keyA.status == KEY_STATUS_PRESS_3S)
				{
					//启动WiFi连接
					#ifndef PID_IX821
					StartConnectPhoneProcess();
					#endif
				}
				else if(keyA.status == KEY_STATUS_PRESS_6S)
				{
					API_PublicInfo_Write_Int(PB_CardSetup_State, 3);
					API_Event_By_Name(EventManageCard);
					
				}
				else if(keyA.status == KEY_STATUS_PRESS_9S)
				{
					
				}
				break;
		}
	}
	else if(keyId == KEY_B)
	{
		switch(keyStatus)
		{
			case KEY_STATUS_PRESS:
				BEEP_KEY();
				keyB.status = KEY_STATUS_PRESS;
				break;
			case KEY_STATUS_RELEASE:
				keyB.status = KEY_STATUS_RELEASE;
				//查询XD连接情况
				if(!API_PublicInfo_Read_Bool(PB_CHECK_XD_STATE))
				{
					API_PublicInfo_Write_Bool(PB_CHECK_XD_STATE, 1);
					API_Event_By_Name(EventTouchKeyDisplay);
					API_Add_TimingCheck(StopDisplayXDState, 5);
				}
				break;
			case KEY_STATUS_PRESS_3S:
				BEEP_KEY();
				API_LED_ctrl("LED_1", "MSG_LED_FLASH_SLOW");
				API_LED_ctrl("LED_2", "MSG_LED_OFF");
				keyB.status = KEY_STATUS_PRESS_3S;
				API_Add_TimingCheck(TouchKey_B_Timer, 1);
				break;
			case KEY_STATUS_LONG_PRESS_RELEASE:
				API_Del_TimingCheck(TouchKey_B_Timer);
				if(keyB.status == KEY_STATUS_PRESS_3S)
				{
					#ifndef PID_IX821
					//1、启动IX-Cloud-Proxy连接
					if(!IsXDRemoteConnect())
					{
						API_Para_Write_Int(IXD_AutoConnect, 1);
						XDRemoteConnect(NULL);
					}

					//查询XD连接情况
					if(!API_PublicInfo_Read_Bool(PB_CHECK_XD_STATE))
					{
						API_PublicInfo_Write_Bool(PB_CHECK_XD_STATE, 1);
						API_Event_By_Name(EventTouchKeyDisplay);
						API_Add_TimingCheck(StopDisplayXDState, 5);
					}

					//ReportDebugState();

					//2、查询分机在线状态（同821-VD）利用M4模块显示各分机
					Check_DT_IM_Online();
					#endif
				}
				else if(keyB.status == KEY_STATUS_PRESS_6S)
				{
					//启动IPC自动配置
					#ifndef PID_IX821
					API_Event_By_Name(Event_IPC_AUTO_CONFIG);
					#endif
				}
				else if(keyB.status == KEY_STATUS_PRESS_9S)
				{
					if(!API_PublicInfo_Read_Bool(PB_StartUp5MinutesLater))
					{
						XDRemoteDisconnect();
						API_Event_UpdateStart(API_Para_Read_String2(FW_UpdateServer), API_Para_Read_String2(FW_UpdateCode), 1, "Key B");
					}
					else
					{
						API_LED_ctrl("LED_1", "MSG_LED_OFF");
						API_LED_ctrl("LED_2", "MSG_LED_OFF");
					}
				}
				break;
		}
	}



	return 1;
}

void DebugTouchpad(int x, int y)
{
	if(debugState)
	{
		uart_touchpad_io(x, y);
	}
}

//呼叫状态和启动过程中，AB键不起作用
int EventKeyABFilter(cJSON* event)
{
	char* systemState = API_PublicInfo_Read_String(PB_System_State);
	if(systemState)
	{
		if(strcmp(systemState, "Starting") && strcmp(systemState, "Calling"))
		{
			return 1;
		}
		else
		{
			return 0;
		}
	}

	return 1;
}


/*********************************************************************************************************
**  End Of File
*********************************************************************************************************/

