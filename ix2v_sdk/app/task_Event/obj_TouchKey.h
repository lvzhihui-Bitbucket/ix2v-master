/**
  ******************************************************************************
  * @file    obj_TouchKey.h
  * @author  czb
  * @version V00.01.00
  * @date    2022.5.17
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 
#ifndef _obj_TouchKey_H
#define _obj_TouchKey_H
//key_id:
typedef enum
{
	KEY_A = 0,
	KEY_B,
}key_id_t;

//key_status:
typedef enum
{
	KEY_STATUS_PRESS = 0,
	KEY_STATUS_RELEASE,  
	KEY_STATUS_PRESS_3S,
	KEY_STATUS_LONG_PRESS_RELEASE,
	KEY_STATUS_PRESS_6S,
	KEY_STATUS_PRESS_9S,
	KEY_STATUS_PRESS_12S,
}key_status_t;

typedef struct
{
		int status;
		int timing;
}TOUCH_KEY_RUN_S;

typedef struct
{
	int 	  	saveWifiSwitch;
	int 		saveXdState;
	int 		state;
	char* 		ssid1;
	char* 		ssid2;
	pthread_mutex_t	lock;
}CONNECT_PHONE_RUN_S;

void DebugTouchpad(int x, int y);
#endif



/*********************************************************************************************************
**  End Of File
*********************************************************************************************************/

