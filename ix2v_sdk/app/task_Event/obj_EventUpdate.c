﻿/**
  ******************************************************************************
  * @file    obj_EventCallback.c
  * @author  czb
  * @version V00.01.00
  * @date    2022.5.17
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 

#include <dirent.h>
#include <sys/stat.h>
#include "task_Event.h"
#include "obj_PublicInformation.h"
#include "elog.h"
#include "utility.h"
#include "define_string.h"
#include "define_file.h"
#include "obj_IoInterface.h"
#include "cJSON.h"
#include "obj_Ftp.h"   
#include "obj_KeypadLed.h"
#include "ota_FileProcess.h"
#include "obj_MyCtrlLed.h"
#include "task_Beeper.h"

#if	defined(PID_IX611)
	#define	DownloadStartInstructIng()					API_KeypadLedCtrl(GetKeyType() ? LED_MK_BL : LED_RED, KEYPAD_LED_FLASH_FAST)
	#define	DownloadStopInstructIng()					API_KeypadLedCtrl(GetKeyType() ? LED_MK_BL : LED_RED, KEYPAD_LED_OFF)
#elif	defined(PID_IX622)
	#define	DownloadStartInstructIng()					API_MyLedCtrl(MY_LED_NAMEPLATE, MY_LED_FLASH_FAST, 0)
	#define	DownloadStopInstructIng()					API_MyLedCtrl(MY_LED_NAMEPLATE, MY_LED_ON, 0)
#else
	#define	DownloadStartInstructIng()
	#define	DownloadStopInstructIng()
#endif

static OtaDlTxt_t dlTxtInfo;

//返回0失败，1成功。
int API_RemoteUpdateReport(char* ipString, char* msg)
{
	int ret = 0;
	int ip = inet_addr(ipString);
	if(ip != -1)
	{
		cJSON* event = cJSON_CreateObject();
		cJSON_AddStringToObject(event, EVENT_KEY_EventName, EventUpdateReportToRemote);
		cJSON_AddStringToObject(event, "MSG", msg);
		ret = API_XD_EventJson(ip, event);
		cJSON_Delete(event);
	}

	return ret;
}

static int DownloadFwCallback(int state, FTP_FILE_STATISTICS statistics, void* data)
{
	cJSON *cmd = (cJSON*)data;
	char* reportIp;
	char* server;
	char* dlFile;
	char msg[100];
	int rate;
	static int reportCnt = 0;
	int ip;
	int rebootFlag;

	switch (state)
	{
		case FTP_STATE_PARA_ERR:
		case FTP_STATE_INIT_ERR:
		case FTP_STATE_DOWN_ERR:
			API_RemoteUpdateReport(GetEventItemString(cmd, "REPORT_IP"), "DOWNLOAD_ERROR");
			DownloadStopInstructIng();
			cJSON_Delete(cmd);
			break;

		case FTP_STATE_DOWN_STOP:
			API_RemoteUpdateReport(GetEventItemString(cmd, "REPORT_IP"), "DOWNLOAD_STOP");
			DownloadStopInstructIng();
			cJSON_Delete(cmd);
			break;

		case FTP_STATE_DOWN_OK:
			reportIp = GetEventItemString(cmd, "REPORT_IP");
			API_RemoteUpdateReport(reportIp, "DOWNLOAD_OK");
			API_RemoteUpdateReport(reportIp, "INSTALLING");
			snprintf(msg, 100, "%s/%s", (GetDirFreeSizeInMByte(DISK_SDCARD) ? SDCARD_DOWNLOAD_PATH : NAND_DOWNLOAD_PATH), GetEventItemString(cmd, "DOWNLOAD_FILE"));
			if(InstallDownloadZip(dlTxtInfo.dlType, msg, &rebootFlag))
			{
				API_RemoteUpdateReport(reportIp, "INSTALL_OK");
				if(rebootFlag)
				{
					sleep(1);
					API_RemoteUpdateReport(reportIp, "WAITING_REBOOT");
					ip = inet_addr(reportIp);
					WriteRebootFlag(&ip, 1);
					HardwareRestart("TFTP Update");
				}
			}
			else
			{
				API_RemoteUpdateReport(reportIp, "INSTALL_FAIL");
				DeleteFileProcess(NULL, msg);
				DownloadStopInstructIng();
			}
			cJSON_Delete(cmd);
			break;

		case FTP_STATE_DOWN_ING:
			rate = statistics.now*100.0/statistics.total;
			if(reportCnt != rate%5)
			{
				reportCnt = rate%5;
				snprintf(msg, 100, "DOWNLOAD_ING %u%%", rate);
				API_RemoteUpdateReport(GetEventItemString(cmd, "REPORT_IP"), msg);
			}
			break;
	
		default:
			break;
	}

	return 0;
}

static int DownloadFwInfoCallback(int state, FTP_FILE_STATISTICS statistics, void* data)
{
	cJSON *cmd = (cJSON*)data;
	char* reportIp;
	char* server;
	char* dlFile;
	int checkRet;
	char fileName[100];
	int sdcardSize;

	switch (state)
	{
		case FTP_STATE_PARA_ERR:
		case FTP_STATE_INIT_ERR:
		case FTP_STATE_DOWN_ERR:
		case FTP_STATE_DOWN_STOP:
			API_RemoteUpdateReport(GetEventItemString(cmd, "REPORT_IP"), "CHECK_ERROR");
			cJSON_Delete(cmd);
			break;

		case FTP_STATE_DOWN_OK:
			reportIp = GetEventItemString(cmd, "REPORT_IP");
			dlFile = GetEventItemString(cmd, "DOWNLOAD_FILE");
			snprintf(fileName, 100, "%s/%s.txt", TEMP_Folder, dlFile);
			//返回-2）空间不足不允许更新, -1）时间相等不允许更新, 0）类型校验不通过，1）校验通过
			switch(CheckDownloadIsAllowed(fileName, 1, &dlTxtInfo))
			{
				case -2:
					API_RemoteUpdateReport(reportIp, "CHECK_NO_SPACE");
					break;

				case -1:
					API_RemoteUpdateReport(reportIp, "CHECK_UP_TO_DATE");
					break;
				
				case 0:
					API_RemoteUpdateReport(reportIp, "CHECK_ERROR");
					break;

				case 1:
					API_RemoteUpdateReport(reportIp, "CHECK_PASSED");
					break;
			}
			DeleteFileProcess(NULL, fileName);
			cJSON_Delete(cmd);
			break;

		case FTP_STATE_DOWN_ING:
			break;
	
		default:
			break;
	}

	return 0;
}

char* GetFileName(const char* path, char* fileName)
{
	fileName[0] = 0;
	if(path && strlen(path) < 200)
	{
		char* name, *pos1;
		char temp[200+1];

		strcpy(temp, path);
		for(pos1 = strtok(temp, "/"), name = pos1; pos1 = strtok(NULL,"/"); name = pos1);
		strcpy(fileName, name);
	}

	return fileName;
}

static char* GetDir(const char* path, char* dir)
{
	dir[0] = 0;
	if(path && strlen(path) < 200)
	{
		char temp[200+1];
		char* name, *pos1;
		int len;

		strcpy(temp, path);
		for(pos1 = strtok(temp, "/"), name = pos1; pos1 = strtok(NULL,"/"); name = pos1);
		len = name - temp - 1;
		memcpy(dir, path, len);
		dir[len] = 0;
	}
	
	return dir;
}

/****************************************************************************************
 {
	"EventName":"EventUpdateRequest",
	"OPERATE":"INSTALL/CHECK",
	"DOWNLOAD_TYPE":"FTP或者TFTP",
	"REPORT_IP":"192.168.243.101",
	"SERVER":"47.106.104.38或者192.168.243.101",
	"DOWNLOAD_FILE":"611-test或者/mnt/nand1-2/temp/611-test"
}
****************************************************************************************/
#define UPDATE_CODE_CLOUD_DIR 	"/home/userota/"

int EventUpdateProcess(cJSON *cmd)
{
	char* dlType = GetEventItemString(cmd, "DOWNLOAD_TYPE");
	char* reportIp = GetEventItemString(cmd, "REPORT_IP");
	char* server = GetEventItemString(cmd, "SERVER");
	char* dlFile = GetEventItemString(cmd, "DOWNLOAD_FILE");
	char* operate = GetEventItemString(cmd, "OPERATE");
	cJSON* cbData;
	char* targetDir;
	char fileName[500];
	int rebootFlag;

	MakeDir(TEMP_Folder);

	if(!strcmp(dlType, "FTP"))
	{
		if(!strcmp(operate, "CHECK"))
		{
			cbData = cJSON_Duplicate(cmd, 1);
			snprintf(fileName, 500, "%s/%s.txt", UPDATE_CODE_CLOUD_DIR, dlFile);
			if(FTP_StartDownload(server, fileName, TEMP_Folder, cbData,  DownloadFwInfoCallback))
			{
				dprintf("FTP_StartDownload success.\n");
			}
			else
			{
				dprintf("FTP_StartDownload error.\n");
				cJSON_Delete(cbData);
				API_RemoteUpdateReport(reportIp, "CHECK_ERROR");
			}
		}
		else if(!strcmp(operate, "INSTALL"))
		{
			cbData = cJSON_Duplicate(cmd, 1);
			snprintf(fileName, 500, "%s/%s", UPDATE_CODE_CLOUD_DIR, dlFile);
			if(FTP_StartDownload(server, fileName, (GetDirFreeSizeInMByte(DISK_SDCARD) ? SDCARD_DOWNLOAD_PATH : NAND_DOWNLOAD_PATH), cbData,  DownloadFwCallback))
			{
				dprintf("FTP_StartDownload success.\n");
				API_RemoteUpdateReport(reportIp, "DOWNLOAD_START");
				DownloadStartInstructIng();
			}
			else
			{
				API_RemoteUpdateReport(reportIp, "DOWNLOAD_ERROR");
				cJSON_Delete(cbData);
			}
		}
	}
	else if(!strcmp(dlType, "TFTP"))
	{
		char srcDir[200];
		int ip = inet_addr(server);
		GetFileName(dlFile, fileName);
		GetDir(dlFile, srcDir);

		if(!strcmp(operate, "CHECK"))
		{
			if(!Api_TftpReadFileFromDevice(ip, srcDir, fileName, TEMP_Folder, fileName))
			{
				snprintf(srcDir, 200, "%s/%s", TEMP_Folder, fileName);
				//返回-2）空间不足不允许更新, -1）时间相等不允许更新, 0）类型校验不通过，1）校验通过
				switch(CheckDownloadIsAllowed(srcDir, 1, &dlTxtInfo))
				{
					case -2:
						API_RemoteUpdateReport(reportIp, "CHECK_NO_SPACE");
						break;

					case -1:
						API_RemoteUpdateReport(reportIp, "CHECK_UP_TO_DATE");
						break;
					
					case 0:
						API_RemoteUpdateReport(reportIp, "CHECK_ERROR");
						break;

					case 1:
						API_RemoteUpdateReport(reportIp, "CHECK_PASSED");
						break;
				}
				DeleteFileProcess(NULL, srcDir);
			}
			else
			{
				API_RemoteUpdateReport(reportIp, "CHECK_ERROR");
			}
		}
		else if(!strcmp(operate, "INSTALL"))
		{
			DownloadStartInstructIng();
			API_RemoteUpdateReport(reportIp, "DOWNLOAD_START");
			if(!Api_TftpReadFileFromDevice(ip, srcDir, fileName, (GetDirFreeSizeInMByte(DISK_SDCARD) ? SDCARD_DOWNLOAD_PATH : NAND_DOWNLOAD_PATH), fileName))
			{
				API_RemoteUpdateReport(reportIp, "DOWNLOAD_OK");
				API_RemoteUpdateReport(reportIp, "INSTALLING");
				snprintf(srcDir, 200, "%s/%s", (GetDirFreeSizeInMByte(DISK_SDCARD) ? SDCARD_DOWNLOAD_PATH : NAND_DOWNLOAD_PATH), fileName);
				if(InstallDownloadZip(dlTxtInfo.dlType, srcDir, &rebootFlag))
				{
					API_RemoteUpdateReport(reportIp, "INSTALL_OK");
					if(rebootFlag)
					{
						sleep(1);
						API_RemoteUpdateReport(reportIp, "WAITING_REBOOT");
						ip = inet_addr(reportIp);
						WriteRebootFlag(&ip, 1);
						HardwareRestart("TFTP Update");
					}
				}
				else
				{
					API_RemoteUpdateReport(reportIp, "INSTALL_FAIL");
					DeleteFileProcess(NULL, srcDir);
				}
			}
			else
			{
				API_RemoteUpdateReport(reportIp, "DOWNLOAD_ERROR");
			}
			DownloadStopInstructIng();
		}
	}

	return 1;
}

static void UpdateTip(const char* string, char* data)
{
	#if	defined(PID_IX850)
	static void * updateTip = NULL;
	if(updateTip)
	{
		API_TIPS_Close_Ext(updateTip);
		updateTip = NULL;
	}
	updateTip = API_TIPS_Ext_Prefix(string, data);
	#endif
	DX821_ReportString(string);
}

static int DownloadFw2Callback(int state, FTP_FILE_STATISTICS statistics, void* data)
{
	cJSON *cbData = (cJSON*)data;
	char* server;
	char* code;
	char msg[100];
	char tempMsg[100];
	int rate;
	static int reportCnt = 0;
	int rebootFlag;

	switch (state)
	{
		case FTP_STATE_PARA_ERR:
		case FTP_STATE_INIT_ERR:
		case FTP_STATE_DOWN_ERR:
			code = GetEventItemString(cbData, "Code");
			DownloadStopInstructIng();
			//snprintf(tempMsg, 100, "%s download error.", code);
			UpdateTip("download error", code);
			cJSON_Delete(cbData);
			break;

		case FTP_STATE_DOWN_STOP:
			DownloadStopInstructIng();
			//snprintf(tempMsg, 100, "Download %s stop.", GetEventItemString(cbData, "Code"));
			UpdateTip("download stop", GetEventItemString(cbData, "Code"));
			cJSON_Delete(cbData);
			break;

		case FTP_STATE_DOWN_OK:
			code = GetEventItemString(cbData, "Code");
			//snprintf(tempMsg, 100, "%s download OK.", code);
			UpdateTip("download ok", code);
			snprintf(msg, 100, "%s/%s", (GetDirFreeSizeInMByte(DISK_SDCARD) ? SDCARD_DOWNLOAD_PATH : NAND_DOWNLOAD_PATH), code);
			if(InstallDownloadZip(GetEventItemInt(cbData, "DownloadType"), msg, &rebootFlag))
			{
				//snprintf(tempMsg, 100, "Install %s OK.", code);
				UpdateTip("install ok", code);
				if(rebootFlag)
				{
					sleep(1);
					snprintf(tempMsg, 100, "Waiting reboot.");
					UpdateTip(tempMsg, NULL);
					HardwareRestart("FTP Update");
				}
			}
			else
			{
				//snprintf(tempMsg, 100, "Install %s fail.", code);
				UpdateTip("install fail", code);
				DeleteFileProcess(NULL, msg);
				DownloadStopInstructIng();
			}
			cJSON_Delete(cbData);
			break;

		case FTP_STATE_DOWN_ING:
			rate = statistics.now*100.0/statistics.total;
			if(reportCnt != rate%5)
			{
				reportCnt = rate%5;
				code = GetEventItemString(cbData, "Code");
				snprintf(tempMsg, 100, "%s %u%%", code, rate);
				UpdateTip("downloading", tempMsg);
			}
			break;
	
		default:
			break;
	}

	return 0;
}

static int DownloadFwInfo2Callback(int state, FTP_FILE_STATISTICS statistics, void* data)
{
	cJSON *cbData = (cJSON*)data;
	char* server;
	char* code;
	char* checkTime;
	char fileName[500];
	char fileName2[500];
	char tempMsg[100];
	OtaDlTxt_t dlTxt;
	switch (state)
	{
		case FTP_STATE_PARA_ERR:
		case FTP_STATE_INIT_ERR:
		case FTP_STATE_DOWN_ERR:
			code = GetEventItemString(cbData, "Code");
			//snprintf(tempMsg, 100, "%s.txt download error.", code);
			UpdateTip("download error", code);
		case FTP_STATE_DOWN_STOP:
			cJSON_Delete(cbData);
			break;

		case FTP_STATE_DOWN_OK:
			server = GetEventItemString(cbData, "Server");
			code = GetEventItemString(cbData, "Code");
			checkTime = GetEventItemString(cbData, "CheckTime");
			snprintf(fileName2, 500, "%s/%s.txt", TEMP_Folder, code);
			//返回-1）时间相等不允许更新, 0）类型校验不通过，1）校验通过
			switch(CheckDownloadIsAllowed(fileName2, atoi(checkTime), &dlTxt))
			{
				case -2:
					snprintf(tempMsg, 100, "Check no space.");
					UpdateTip(tempMsg, NULL);
					cJSON_Delete(cbData);
					break;

				case -1:
					snprintf(tempMsg, 100, "Check up to date.");
					UpdateTip(tempMsg, NULL);
					cJSON_Delete(cbData);
					break;
				
				case 0:
					snprintf(tempMsg, 100, "Check error.");
					UpdateTip(tempMsg, NULL);
					cJSON_Delete(cbData);
					break;

				case 1:
					snprintf(tempMsg, 100, "Check passed.");
					UpdateTip(tempMsg, NULL);
					cJSON_AddNumberToObject(cbData, "DownloadType", dlTxt.dlType);
					snprintf(fileName, 500, "%s/%s", UPDATE_CODE_CLOUD_DIR, code);
					if(FTP_StartDownload(server, fileName, (GetDirFreeSizeInMByte(DISK_SDCARD) ? SDCARD_DOWNLOAD_PATH : NAND_DOWNLOAD_PATH), cbData,  DownloadFw2Callback))
					{
						dprintf("FTP_StartDownload %s success.", code);
						usleep(500*1000);
						DownloadStartInstructIng();
					}
					else
					{
						snprintf(tempMsg, 100, "%s download error.", code);
						UpdateTip("download error", code);
						cJSON_Delete(cbData);
					}
					break;
			}
			DeleteFileProcess(NULL, fileName2);
			break;

		case FTP_STATE_DOWN_ING:
			break;
	
		default:
			break;
	}

	return 0;
}


int Event_FwUpdateStart(cJSON *cmd)
{
	char* server = GetEventItemString(cmd, "Server");
	char* code = GetEventItemString(cmd, "Code");
	char tempMsg[100];
	if(!server[0])
	{
		server = API_Para_Read_String2(FW_UpdateServer);
		cJSON_AddStringToObject(cmd, "Server", server);
	}

	if(!code[0])
	{
		code = API_Para_Read_String2(FW_UpdateCode);
		cJSON_AddStringToObject(cmd, "Code", code);
	}
	
	if(server && code && server[0] && code[0])
	{
		char fileName[500];
		if(!strcmp(server, "SD_Card"))
		{
			OtaDlTxt_t dlTxt;
			int rebootFlag;
			snprintf(fileName, 500, "%s/%s.txt", SdUpgradePath, code);
			//返回-1）时间相等不允许更新, 0）类型校验不通过，1）校验通过
			switch(CheckDownloadIsAllowed(fileName, atoi(GetEventItemString(cmd, "CheckTime")), &dlTxt))
			{
				case -2:
					UpdateTip("Check no space.", NULL);
					break;

				case -1:
					UpdateTip("Check up to date.", NULL);
					break;
				
				case 0:
					UpdateTip("Check error.", NULL);
					break;

				case 1:
					UpdateTip("Check passed.", NULL);
					snprintf(fileName, 500, "%s/%s", SdUpgradePath, code);
					if(InstallDownloadZip(dlTxt.dlType, fileName, &rebootFlag))
					{
						UpdateTip("install ok", code);
						if(rebootFlag)
						{
							#ifdef PID_IX821
							if(strcmp(GetEventItemString(cmd,"Source"),"DIP")==0)
							{
								BEEP_SET_ENTRY();
								sleep(1);
							}
							#endif
							sleep(1);
							UpdateTip("Waiting reboot.", NULL);
							HardwareRestart("SD Card Update");
						}
					}
					else
					{
						UpdateTip("install fail", code);
					}
					break;
			}
		}
		else
		{
			cJSON* cbData = cJSON_Duplicate(cmd, 1);
			MakeDir(TEMP_Folder);
			API_DeleteTempFile();
			API_DeletePhoto();		

			snprintf(fileName, 500, "%s/%s.txt", UPDATE_CODE_CLOUD_DIR, code);
			if(FTP_StartDownload(server, fileName, TEMP_Folder, cbData,  DownloadFwInfo2Callback))
			{
				snprintf(tempMsg, 100, "Check %s.", code);
			}
			else
			{
				cJSON_Delete(cbData);
				snprintf(tempMsg, 100, "%s.txt download error.", code);
			}
			UpdateTip(tempMsg, NULL);
		}
	}

	return 1;
}


/*********************************************************************************************************
**  End Of File
*********************************************************************************************************/

