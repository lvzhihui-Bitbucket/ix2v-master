﻿/**
  ******************************************************************************
  * @file    obj_EventCallback.c
  * @author  czb
  * @version V00.01.00
  * @date    2022.5.17
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 

#include <dirent.h>
#include <sys/stat.h>
#include "task_Event.h"
#include "obj_PublicInformation.h"
#include "task_Beeper.h"
#include "elog.h"
#include "define_string.h"
#include "define_file.h"
#include "obj_IoInterface.h"
#include "obj_TouchKey.h"
#include "obj_UnitSR.h"
#include "cJSON.h"
#include "obj_gpio.h"
#include "obj_MDS_Output.h"
#include "obj_KeypadLed.h"
#include "task_CallServer.h"
#include "obj_TableSurver.h"
#include "obj_DT_CheckOnline.h"
#include "obj_lvgl_msg.h"
#include "obj_NDM.h"
#include "obj_MyCtrlLed.h"

int Event_LedCtrl(cJSON *cmd)
{
#ifndef PID_IX850
	#ifdef PID_IX821
	char* pbEocMacState = API_PublicInfo_Read_String(PB_EOC_Mac_State);
	char* pbEocState = API_PublicInfo_Read_String(PB_EOC_state);
	if(pbEocMacState)
	{
		if(!IfHasSn())
		{
			API_LED_ctrl("LED_2", "MSG_LED_FLASH_FAST");
			API_LED_ctrl("LED_1", "MSG_LED_FLASH_FAST");
			return;
		}
		if(strcmp(pbEocState,"unlink")!=0&& strcmp(pbEocMacState, "Ok")!=0)
		{
			API_LED_ctrl("LED_2", "MSG_LED_FLASH_FAST");
			API_LED_ctrl("LED_1", "MSG_LED_FLASH_SLOW");
			return;
		}
	}
	#endif
	int wifiConnectState = API_PublicInfo_Read_Int(PB_CONNECT_PHONE_STATE);
	int ipcAutoState = API_PublicInfo_Read_Int(PB_IPC_AUTO_CONFIG);
	int cardState = API_PublicInfo_Read_Int(PB_CardSetup_State);
	//LED_Aćç¤ş
	//ćĽčŻ˘XDčżćĽćĺľççśćä¸
	if(API_PublicInfo_Read_Bool(PB_CHECK_XD_STATE))
	{
		char* XD_State = API_PublicInfo_Read_String(PB_XD_STATE);
		if(XD_State)
		{
			if(!strcmp(XD_State, DM_KEY_Idle))
			{
				API_LED_ctrl("LED_1", "MSG_LED_OFF");
				API_LED_ctrl("LED_2", "MSG_LED_OFF");
			}
			else if(!strcmp(XD_State, DM_KEY_Remote))
			{
				API_LED_ctrl("LED_1", "MSG_LED_OFF");
				API_LED_ctrl("LED_2", "MSG_LED_ON");
			}
			else if(!strcmp(XD_State, DM_KEY_Local))
			{
				API_LED_ctrl("LED_1", "MSG_LED_ON");
				API_LED_ctrl("LED_2", "MSG_LED_OFF");
			}
		}
	}
	//čżćĽWiFiççśćä¸
	else if(wifiConnectState)
	{
		switch (wifiConnectState)
		{
			//čżćĽä¸­
			case 1:
				API_LED_ctrl("LED_1", "MSG_LED_FLASH_FAST");
				API_LED_ctrl("LED_2", "MSG_LED_OFF");
				break;

			//WiFičżćĽćĺ
			case 2:
				API_LED_ctrl("LED_1", "MSG_LED_ON");
				API_LED_ctrl("LED_2", "MSG_LED_OFF");
				break;

			//äşčżćĽćĺ
			case 3:
				API_LED_ctrl("LED_1", "MSG_LED_ON");
				API_LED_ctrl("LED_2", "MSG_LED_ON");
				break;

			//čżćĽĺ¤ąč´Ľ
			case 4:
				API_LED_ctrl("LED_1", "MSG_LED_OFF");
				API_LED_ctrl("LED_2", "MSG_LED_OFF");
				break;
		}

	}
	else if(ipcAutoState)
	{
		printf("Event_LedCtrl ipcAutoState=%d \n", ipcAutoState);
		//ć­Łĺ¨ćç´˘IPC
		if(ipcAutoState == 1)
		{
			API_LED_ctrl("LED_2", "MSG_LED_FLASH_FAST");				
		}
		//ĺĺćďźććˇťĺ 
		else if(ipcAutoState == 2)
		{
			API_LED_ctrl("LED_1", "MSG_LED_ON");
			API_LED_ctrl("LED_2", "MSG_LED_ON");
		}
		//ĺĺćďźć ćˇťĺ 
		else if(ipcAutoState == 3)
		{
			API_LED_ctrl("LED_1", "MSG_LED_ON");
			API_LED_ctrl("LED_2", "MSG_LED_OFF");
		}
		//ĺĺć ďźććˇťĺ 
		else if(ipcAutoState == 4)
		{
			API_LED_ctrl("LED_1", "MSG_LED_OFF");
			API_LED_ctrl("LED_2", "MSG_LED_ON");
		}
		//ĺĺć ďźć ćˇťĺ 
		else if(ipcAutoState == 5)
		{
			API_LED_ctrl("LED_1", "MSG_LED_OFF");
			API_LED_ctrl("LED_2", "MSG_LED_OFF");
		}
	}
	//ćĽčŻ˘sipčżćĽćĺľççśćä¸
	else if(API_PublicInfo_Read_Bool(PB_CHECK_SIP_STATE))
	{
		char* sipNetwork = API_PublicInfo_Read_String(PB_SIP_CONNECT_NETWORK);
		if(sipNetwork)
		{
			if(!strcmp(sipNetwork, "4G"))
			{
				API_LED_ctrl("LED_1", "MSG_LED_ON");
				API_LED_ctrl("LED_2", "MSG_LED_ON");
			}
			else if(!strcmp(sipNetwork, "WLAN"))
			{
				API_LED_ctrl("LED_1", "MSG_LED_OFF");
				API_LED_ctrl("LED_2", "MSG_LED_ON");
			}
			else if(!strcmp(sipNetwork, "LAN"))
			{
				API_LED_ctrl("LED_1", "MSG_LED_ON");
				API_LED_ctrl("LED_2", "MSG_LED_OFF");
			}
			else
			{
				API_LED_ctrl("LED_1", "MSG_LED_OFF");
				API_LED_ctrl("LED_2", "MSG_LED_OFF");
			}
		}
	}
	else if(cardState)
	{
		if(cardState == 1)
		{
			API_LED_ctrl("LED_1", "MSG_LED_ON");
			API_LED_ctrl("LED_2", "MSG_LED_OFF");
		}
		else if(cardState == 2)
		{
			API_LED_ctrl("LED_1", "MSG_LED_OFF");
			API_LED_ctrl("LED_2", "MSG_LED_ON");
		}
		else if(cardState == 3)
		{
			API_LED_ctrl("LED_1", "MSG_LED_FLASH_FAST");
			API_LED_ctrl("LED_2", "MSG_LED_OFF");	
		}
	}
	//ćç¤şĺźĺŤçśćĺĺźéçść
	else
	{
		//LED_Bćç¤ş
		//ä¸ĺ¨ćĽčŻ˘çśćä¸ďźĺ˝éçŽĄ
		#if (!defined(PID_IX611)) && (!defined(PID_IX622))
		if(API_PublicInfo_Read_Bool(PB_LOCK1_STATE) || API_PublicInfo_Read_Bool(PB_LOCK2_STATE))
		{
			API_LED_ctrl("LED_2", "MSG_LED_ON");
		}
		else
		{
			#ifdef PID_IX821
			if(ifExitKeyLongPress()==0)
			#endif
				API_LED_ctrl("LED_2", "MSG_LED_OFF");
		}
		#endif
		//LED_Aćç¤ş
		char* callServerState = API_PublicInfo_Read_String(PB_CALL_SER_STATE);
		if(callServerState)
		{
			//ĺžćşçśćä¸
			if(!strcmp(callServerState, "Wait"))
			{
			#if 0
				//ćťçşżĺż
				if(API_PublicInfo_Read_Bool(PB_DT_LINK_STATE))
				{
					API_LED_ctrl("LED_1", "MSG_LED_ON");
				}
				//ćťçşżçŠşé˛
				else
				{
					API_LED_ctrl("LED_1", "MSG_LED_OFF");
				}
			#else
				API_LED_ctrl("LED_1", "MSG_LED_OFF");
			#endif
			}
			//ćŻéçśćä¸
			else if(!strcmp(callServerState, "Ring"))
			{
				//ćsipĺĺ 
				if(API_PublicInfo_Read_Bool(PB_CALL_SIP))
				{
					API_LED_ctrl("LED_1", "MSG_LED_FLASH_FAST");
				}
				else
				{
					API_LED_ctrl("LED_1", "MSG_LED_FLASH_SLOW");
				}
			}
			//éčŻçśćä¸
			else if(!strcmp(callServerState, "Ack") || !strcmp(callServerState, "RemoteAck"))
			{
				API_LED_ctrl("LED_1", "MSG_LED_ON");
			}
		}
	
	}

#endif
	return 1;
}

#if defined(PID_IX611)
int Event_IX611_CallLedCtrl(cJSON *cmd)
{
	char* callServerState = API_PublicInfo_Read_String(PB_CALL_SER_STATE);
	char* pbEocMacState = API_PublicInfo_Read_String(PB_EOC_Mac_State);
	char* pbEocState = API_PublicInfo_Read_String(PB_EOC_state);
	char* eventname=GetEventItemString(cmd, EVENT_KEY_EventName);
	if(pbEocMacState)
	{
		int selectLed = GetKeyType() ? LED_MK_BL : LED_BLUE;
		if(!IfHasSn())
		{
			API_KeypadLedCtrl(selectLed, KEYPAD_LED_FLASH_FAST);//API_MyLedCtrl(MY_LED_TALK, MY_LED_FLASH_FAST, 0);
			return;
		}
		if(strcmp(pbEocState,"unlink")!=0&& strcmp(pbEocMacState, "Ok")!=0)
		{
			API_KeypadLedCtrl(selectLed, KEYPAD_LED_FLASH_SLOW);//API_MyLedCtrl(MY_LED_TALK, MY_LED_FLASH_SLOW, 0);
			return;
		}
	}
	if(API_Para_Read_Int("IO_SENSOR_TYPE")==3)
	{
		if(strcmp(eventname,EventSnErrDisp)==0)
		{
			int selectLed = GetKeyType() ? LED_MK_BL : LED_BLUE;
			if(GetKeyPadBackLightState())
			{
				API_KeypadLedCtrl(selectLed, KEYPAD_LED_ON);
			}
			else
			{
				API_KeypadLedCtrl(selectLed, KEYPAD_LED_OFF);
			}
		}
		return;
	}
	
	if(callServerState)
	{
		int selectLed = GetKeyType() ? LED_MK_BL : LED_BLUE;
		//ĺžćşçśćä¸
		if(!strcmp(callServerState, "Wait"))
		{
			if(GetKeyPadBackLightState())
			{
				API_KeypadLedCtrl(selectLed, KEYPAD_LED_ON);
			}
			else
			{
				API_KeypadLedCtrl(selectLed, KEYPAD_LED_OFF);
			}
		}
		//ćŻéçśćä¸
		else if(!strcmp(callServerState, "Ring"))
		{
			API_KeypadLedCtrl(selectLed, KEYPAD_LED_FLASH_FAST);
		}
		//éčŻçśćä¸
		else if(!strcmp(callServerState, "Ack") || !strcmp(callServerState, "RemoteAck"))
		{
			API_KeypadLedCtrl(selectLed, KEYPAD_LED_FLASH_SLOW);
		}
	}

	return 1;
}
void DC7610_BackLightCtrl(int sw)
{
	char* pbEocMacState = API_PublicInfo_Read_String(PB_EOC_Mac_State);
	char* pbEocState = API_PublicInfo_Read_String(PB_EOC_state);
	if(pbEocMacState)
	{
		int selectLed = GetKeyType() ? LED_MK_BL : LED_BLUE;
		if(!IfHasSn())
		{
			API_KeypadLedCtrl(selectLed, KEYPAD_LED_FLASH_FAST);//API_MyLedCtrl(MY_LED_TALK, MY_LED_FLASH_FAST, 0);
			return;
		}
		if(strcmp(pbEocState,"unlink")!=0&& strcmp(pbEocMacState, "Ok")!=0)
		{
			API_KeypadLedCtrl(selectLed, KEYPAD_LED_FLASH_SLOW);//API_MyLedCtrl(MY_LED_TALK, MY_LED_FLASH_SLOW, 0);
			return;
		}
	}
	int selectLed = GetKeyType() ? LED_MK_BL : LED_PURPLE;
	if(sw)
	{
		API_KeypadLedCtrl(selectLed, KEYPAD_LED_ON);
	}
	else
	{
		if(GetKeyPadBackLightState())
		{
			selectLed = GetKeyType() ? LED_MK_BL : LED_BLUE;
			API_KeypadLedCtrl(selectLed, KEYPAD_LED_ON);
		}
		else
		{
			API_KeypadLedCtrl(selectLed, KEYPAD_LED_OFF);
		}
	}
}
#endif

#if defined(PID_IX622)
int Event_IX622_CallLedCtrl(cJSON *cmd)
{
	int selectLed;
	char* pbEocMacState = API_PublicInfo_Read_String(PB_EOC_Mac_State);
	char* pbEocState = API_PublicInfo_Read_String(PB_EOC_state);
	if(pbEocMacState)
	{
		if(!IfHasSn())
		{
			API_MyLedCtrl(MY_LED_TALK, MY_LED_FLASH_FAST, 0);
			return;
		}
		if(strcmp(pbEocState,"unlink")!=0&& strcmp(pbEocMacState, "Ok")!=0)
		{
			API_MyLedCtrl(MY_LED_TALK, MY_LED_FLASH_SLOW, 0);
			return;
		}
	}
		
	char* callServerState = API_PublicInfo_Read_String(PB_CALL_SER_STATE);
	if(callServerState)
	{
		//ĺžćşçśćä¸
		if(!strcmp(callServerState, "Wait"))
		{
			API_MyLedCtrl(MY_LED_TALK, MY_LED_OFF, 0);
		}
		//ćŻéçśćä¸
		else if(!strcmp(callServerState, "Ring"))
		{
			API_MyLedCtrl(MY_LED_TALK, MY_LED_FLASH_FAST, 0);
		}
		//éčŻçśćä¸
		else if(!strcmp(callServerState, "Ack") || !strcmp(callServerState, "RemoteAck"))
		{
			API_MyLedCtrl(MY_LED_TALK, MY_LED_FLASH_SLOW, 0);
		}
	}
	return 1;
}

int Event_IX622_CardLedCtrl(cJSON *cmd)
{
	int cardState = API_PublicInfo_Read_Int(PB_CardSetup_State);
	if(cardState == 1)
	{
		API_MyLedCtrl(MY_LED_LOCK, MY_LED_ON, 0);
		API_MyLedCtrl(MY_LED_TALK, MY_LED_OFF, 0);
	}
	else if(cardState == 2)
	{
		API_MyLedCtrl(MY_LED_LOCK, MY_LED_OFF, 0);
		API_MyLedCtrl(MY_LED_TALK, MY_LED_ON, 0);
	}
	else
	{
		API_MyLedCtrl(MY_LED_LOCK, MY_LED_OFF, 0);
		API_MyLedCtrl(MY_LED_TALK, MY_LED_OFF, 0);
	}
}
#endif

static int RestoreFactorySetting(int timing)
{
	if(timing >= 20)
	{
		BEEP_CONFIRM();
		log_i("RestoreFactorySetting------------------------");
		return 2;
	}

	return 0;
}

const char* GetEventItemString(cJSON* event,  const char* itemName)
{
	char* item = cJSON_GetStringValue(cJSON_GetObjectItemCaseSensitive(event, itemName));
	item = (item != NULL ? item : "");

	return item;
}

int GetEventItemInt(cJSON* event,  const char* itemName)
{
	int ret;
	cJSON* item = cJSON_GetObjectItemCaseSensitive(event, itemName);
	if(cJSON_IsNumber(item))
	{
		ret = item->valueint;
	}
	else if(cJSON_IsString(item))
	{
		ret = atoi(item->valuestring);
	}
	else if(cJSON_IsTrue(item))
	{
		ret = 1;
	}
	else
	{
		ret = 0;
	}

	return ret;
}

int Event_KeyAB(cJSON *cmd)
{
	key_id_t keyId;
	key_status_t keyStatus;

    char* eventName = cJSON_GetStringValue(cJSON_GetObjectItemCaseSensitive(cmd, EVENT_KEY_EventName));
	char* status = cJSON_GetStringValue(cJSON_GetObjectItemCaseSensitive(cmd, "KEY_STATUS"));

	if(status == NULL || eventName == NULL)
	{
		return 0;
	}

	if(!strcmp(eventName, "EventKEY_A"))
	{
		keyId = KEY_A;
	}
	else if(!strcmp(eventName, "EventKEY_B"))
	{
		keyId = KEY_B;
	}
	else
	{
		return 0;
	}

	if(!strcmp(status, "KEY_STATUS_PRESS"))
	{
		keyStatus = KEY_STATUS_PRESS;
	}
	else if(!strcmp(status, "KEY_STATUS_RELEASE"))
	{
		keyStatus = KEY_STATUS_RELEASE;
	}
	else if(!strcmp(status, "KEY_STATUS_PRESS_3S"))
	{
		keyStatus = KEY_STATUS_PRESS_3S;
	}
	else if(!strcmp(status, "KEY_STATUS_LONG_PRESS_RELEASE"))
	{
		keyStatus = KEY_STATUS_LONG_PRESS_RELEASE;
	}
	else
	{
		return 0;
	}

	return TouchKey_AB_ToEvent(keyId, keyStatus);
}

int Event_CallBeeper(cJSON *cmd)
{
    char* eventName = cJSON_GetStringValue(cJSON_GetObjectItemCaseSensitive(cmd, EVENT_KEY_EventName));

    if(eventName && !strcmp(eventName, EventCall))
    {
        char* callEvent = cJSON_GetStringValue(cJSON_GetObjectItemCaseSensitive(cmd, "CALL_EVENT"));
        if(callEvent)
        {
            if(!strcmp(callEvent, "Calling_Busy"))
            {
                BEEP_LINK_ERROR();
            }
            else if(!strcmp(callEvent, "Calling_Error"))
            {
                BEEP_DIAL_ERROR();
            }
        }
    }

	return 1;
}

void CallVoice(const char* voiceEvent)
{
	if(voiceEvent == NULL)
	{
		return;
	}
	if(judge_alsa_state_is_run())
		return;
	log_d("----------CallVoice:%s", voiceEvent);
	cJSON* voicePara = cJSON_CreateObject();
	cJSON_AddStringToObject(voicePara, "VoiceEvent", voiceEvent);
	VoiceParse(voicePara);
	cJSON_Delete(voicePara);
}

int Event_CallVoice(cJSON *cmd)
{
    char* eventName = cJSON_GetStringValue(cJSON_GetObjectItemCaseSensitive(cmd, EVENT_KEY_EventName));
    char* callserverState = cJSON_GetStringValue(API_PublicInfo_Read(PB_CALL_SER_STATE));
    int Call_Part = API_PublicInfo_Read_Bool(PB_CALL_PART);
    //char* Call_Tar_DtAddr = cJSON_GetStringValue(API_PublicInfo_Read(PB_CALL_TAR_DTADDR));
    
    if(callserverState == NULL)
    {
        return 0;
    }


    //dprintf("callserverState = %s, Call_Part = %d, Call_Tar_DtAddr = %s\n", callserverState, Call_Part, Call_Tar_DtAddr);

    if(eventName && !strcmp(eventName, EventCall))
    {
        char* callEvent = cJSON_GetStringValue(cJSON_GetObjectItemCaseSensitive(cmd, "CALL_EVENT"));
        if(callEvent)
        {
            if(!strcmp(callEvent, "Calling_Start"))
            {
                if(!strcmp(callserverState, "Wait"))
                {
                    
                }
            }
            else if(!strcmp(callEvent, "Calling_Busy"))
            {
				CallVoice("Event_SYS_BUSY");
            }
            else if(!strcmp(callEvent, "Calling_Error"))
            {
				CallVoice("Event_CALL_FAIL_DND");
            }
            else if(!strcmp(callEvent, "Calling_Cancel"))
            {
                //if(strcmp(callserverState, "Wait"))
                {
					//CallVoice("Event_CALL_CLOSE");
                }
            }
		else if(!strcmp(callEvent, "Calling_Close"))
            {
                //if(strcmp(callserverState, "Wait"))
                {
				CallVoice("Event_CALL_CLOSE");
                }
            }
		else if(!strcmp(callEvent,"Calling_Succ"))
		{
			CallVoice("Event_CALL_SUCC");
		}
        }
    }
    else if(callserverState)
    {
        if(!strcmp(callserverState, "Wait"))
        {
			//CallVoice("Event_CALL_CLOSE");
        }
        else if(Call_Part && (!strcmp(callserverState, "Ring")))
        {
			//CallVoice("Event_CALL_SUCC");
        }
        else if(Call_Part && (!strcmp(callserverState, "Ack") || !strcmp(callserverState, "RemoteAck")))
        {
        		
        		if(API_PublicInfo_Read_Bool(PB_CALL_SIP)==1||API_PublicInfo_Read(PB_CALL_IPCALLER)!=NULL||API_PublicInfo_Read(PB_CALL_IPBECALLED)!=NULL)
        			BEEP_KEY();//BEEP_CONFIRM();
       		else
				CallVoice("Event_CALL_TALK");
        }
    }

	return 1;
}

int Event_CallRingCallback(cJSON *cmd)
{
    char* eventName = GetEventItemString(cmd, EVENT_KEY_EventName);
	cJSON* ipBeCall = API_PublicInfo_Read(PB_CALL_IPBECALLED);
	cJSON* ipCall = API_PublicInfo_Read(PB_CALL_IPCALLER);
	cJSON* callSource = API_PublicInfo_Read(PB_CALL_SOURCE);
	cJSON* callTarDev = cJSON_GetArrayItem( API_PublicInfo_Read(PB_CALL_TAR_IxDev), 0);
    int Call_Part = API_PublicInfo_Read_Bool(PB_CALL_PART);
    char* callserverState = cJSON_GetStringValue(API_PublicInfo_Read(PB_CALL_SER_STATE));
  
	if(callserverState && !strcmp(callserverState, "Wait"))
	{
		API_RingStop();
		return 1;
	}
	//ĺźĺŤçŹćśçśćäşäťś
    if(!strcmp(eventName, EventCall))
    {
        char* callEvent = GetEventItemString(cmd, "CALL_EVENT");
		if(!strcmp(callEvent, "Calling_Start"))
		{

		}
		else if(!strcmp(callEvent, "Calling_Busy"))
		{

		}
		else if(!strcmp(callEvent, "Calling_Succ"))
		{

		}
		else if(!strcmp(callEvent, "Calling_Error"))
		{

		}	
		else if(!strcmp(callEvent, "Calling_Cancel"))
		{

		}
    }
	//ĺźĺŤçśćäşäťś
	else if(!strcmp(eventName, EventCallState))
	{
		char* becallState = GetEventItemString(ipBeCall, PB_CALL_IPBECALLED_STATE);
		char* becallType = GetEventItemString(ipBeCall, PB_CALL_IPBECALLED_TYPE);
		char* ipCallState = GetEventItemString(ipCall, PB_CALL_IPCALLER_STATE);
		char* ipCallType = GetEventItemString(ipCall, PB_CALL_IPCALLER_TYPE);
		//ĺćşč˘Ťĺź
		if(!strcmp(becallType, "BeIxMainCall") || !strcmp(becallType, "BeIxIntercomCall") || !strcmp(becallType, "BeIxInnerCall"))
		{
			if(!strcmp(becallState, "Ring"))
			{
				API_RingPlay2("RING_CALL", GetEventItemString(callSource, PB_CALL_SRC_IxDevNum));
			}
			else
			{
				API_RingStop();
			}
		}
		//ĺćşä¸ťĺź
		else if(!strcmp(ipCallType, "IxIntercomCall") || !strcmp(ipCallType, "IxInnerCall"))
		{
			if(!strcmp(ipCallState, "Invite"))
			{
				API_RingPlay2("RING_CALL", GetEventItemString(callTarDev, PB_CALL_TAR_IxDevNum));
			}
			else
			{
				API_RingStop();
			}
		}
	}
 
	return 1;
}

int Event_LockVoice(cJSON *cmd)
{
    char* name = cJSON_GetStringValue(cJSON_GetObjectItemCaseSensitive(cmd, "name"));
    if(name && cJSON_IsTrue(cJSON_GetObjectItemCaseSensitive(cmd, "ctrl")))
    {
        if(!strcmp(name, "RL1") || !strcmp(name, "RL2"))
        {
			CallVoice("Event_UNLOCK");
        }
    }
	return 1;
}

static int IpcAutoConfigOver(int timing)
{
	log_v("IpcAutoConfigOver time=%d\n", timing);
	API_PublicInfo_Write_Int(PB_IPC_AUTO_CONFIG, 0);
	API_Event_By_Name(EventTouchKeyDisplay);
	log_v("IpcAutoConfigOver !!!");

	return 2;
}

int Event_IpcConfig(cJSON *cmd)
{
    int ret;
	char* user;
	char* pwd;
	cJSON* io_para = NULL;

	if(!API_PublicInfo_Read_Int(PB_IPC_AUTO_CONFIG))
	{
		API_PublicInfo_Write_Int(PB_IPC_AUTO_CONFIG, 1);
		API_Event_By_Name(EventTouchKeyDisplay);

		user = cJSON_GetStringValue(cJSON_GetObjectItemCaseSensitive(cmd, "LOGIN_NAME"));
		pwd = cJSON_GetStringValue(cJSON_GetObjectItemCaseSensitive(cmd, "LOGIN_PWD"));
		if(user && pwd)
		{
			cJSON* userAndPwd = cJSON_CreateObject();
			io_para = cJSON_CreateArray();
			cJSON_AddItemToArray(io_para, userAndPwd);
			cJSON_AddStringToObject(userAndPwd, "IPC_USR_NAME", user);
			cJSON_AddStringToObject(userAndPwd, "IPC_USR_PWD", pwd);
		}
		else
		{
			io_para = cJSON_Duplicate(API_Para_Read_Public("IPC_USR_PARA"), 1);
		}
		ret = IpcAutoConfig2(io_para);
		cJSON_Delete(io_para);

		IpcAutoToBuPara();

		API_PublicInfo_Write_Int(PB_IPC_AUTO_CONFIG, ret);
		API_Event_By_Name(EventTouchKeyDisplay);
		usleep(5000*1000);
		API_Add_TimingCheck(IpcAutoConfigOver, 5);
	}

	return 1;
}

int EventCallbackLanConfig(cJSON *cmd)
{
    int result = 0;
    cJSON *set_para =  cJSON_CreateObject();
	cJSON_AddStringToObject(set_para, Lan_policy, API_Para_Read_String2(Lan_policy));

	result = API_Set_Lan_Config(set_para);
	cJSON_Delete(set_para);

	char* eventStr;
	cJSON *event = cJSON_CreateObject();
	cJSON_AddStringToObject(event, EVENT_KEY_EventName, Event_XD_RSP_LAN_RECONFIG);
	cJSON_AddItemToObject(event, DM_KEY_EventID, cJSON_Duplicate(cJSON_GetObjectItemCaseSensitive(cmd, DM_KEY_EventID), 1));
	cJSON_AddStringToObject(event, DM_KEY_Result, result == 0 ? RESULT_SUCC : RESULT_ERR06);

	eventStr=cJSON_PrintUnformatted(event);
	if(eventStr)
	{
		API_Event(eventStr);
		free(eventStr);
	}
	cJSON_Delete(event);
	return 1;
}

int EventCallbackXD_Report(cJSON *cmd)
{
	DX821_ReportString(GetEventItemString(cmd, "Message"));
	return 1;
}

//äşç§ć˘ĺ¤DRďźM4ĺžćşćžç¤ş
static int ResetDR_M4_Standby(int timing)
{
	if(timing >= 5)
	{
		//ćťçşżçŠşé˛
		if(!API_PublicInfo_Read_Bool(PB_DT_LINK_STATE))
		{
			//dprintf("111111111111111 5s API_SR_Deprived\n");
			API_SR_Deprived();
			API_MDS_Standby();
		}
		return 2;
	}

	return 0;
}

int EventFiveSecondsResetDisplay(cJSON *cmd)
{
	static int use_flag=0;
	if(API_PublicInfo_Read_Bool(PB_DT_LINK_STATE))
		return 1;
	if(use_flag)
		return 1;
	use_flag=1;
	int cnt=0;
	while(++cnt<55)
	{
		if(Get_SR_Request()||API_PublicInfo_Read_Bool(PB_DT_LINK_STATE))
		{
			use_flag=0;
			return 1;
		}
		usleep(100*1000);
	}
	if(Get_SR_Request()==0&&!API_PublicInfo_Read_Bool(PB_DT_LINK_STATE))
	{
		//dprintf("111111111111111 5s API_SR_Deprived\n");
		API_SR_Deprived();
		//API_MDS_Standby();
	}
	use_flag=0;
	return 1;
}

int EventConnetWiFi(cJSON *cmd)
{
    cJSON *ssid_arry =  cJSON_CreateObject();
	char* wifiSw = API_PublicInfo_Read_String(PB_WLAN_SW);
	char* ssid = cJSON_GetStringValue(cJSON_GetObjectItemCaseSensitive(cmd, "SSID"));
	char* pwd = cJSON_GetStringValue(cJSON_GetObjectItemCaseSensitive(cmd, "PWD"));
	char msg[100];

	if(ssid && ssid[0] && pwd && pwd[0])
	{
		cJSON_AddStringToObject(ssid_arry, "SSID_NAME", ssid);
		cJSON_AddStringToObject(ssid_arry, "SSID_PWD", pwd);

		if(wifiSw != NULL && !strcmp(wifiSw, "OFF"))
		{
			API_Wlan_Open(NULL);
			usleep(100*1000);
		}
		API_Wlan_ConnectSsid(ssid_arry, 1);

		snprintf(msg, 50, "Connecting SSID:%s", cJSON_GetStringValue(cJSON_GetObjectItemCaseSensitive(cmd, "SSID")));
		API_Event_NameAndMsg(Event_XD_RSP_ADD_WIFI, msg);
	}
	else
	{
		API_Event_NameAndMsg(Event_XD_RSP_ADD_WIFI, "Please fill in ssid and password");
	}

	return 1;
}

int CheckUnlockPassword(cJSON *cmd)
{
	char* pwd = GetEventItemString(cmd,  "PWD");
	
	if(pwd && pwd[0])
	{
		switch (UnlockPasswordVerify(pwd, strlen(pwd)))
		{
			case 0:
				API_Event_NameAndMsg(Event_XD_RSP_Message, "Password error");
				break;
			case 1:
			case 3:
				API_Event_Unlock("RL1", "Event");
				API_Event_NameAndMsg(Event_XD_RSP_Message, "Unlock 1");
				break;
			case 2:
				API_Event_Unlock("RL2", "Event");
				API_Event_NameAndMsg(Event_XD_RSP_Message, "Unlock 2");
				break;
		}
	}
	else
	{
		API_Event_NameAndMsg(Event_XD_RSP_Message, "Please input password");
	}

	return 1;
}

int Event_SipManage(cJSON* cmd)
{
	char* action = GetEventItemString(cmd,  "ACTION");
	if(action)
	{
		if(!strcmp(action,"use_default"))
		{
			if(sip_master_use_default()<0)
			{
				API_Event_NameAndMsg(Event_XD_RSP_Message, "Sip use default fail");
			}
			else
			{
				API_Event_NameAndMsg(Event_XD_RSP_Message, "Sip use default ok");
			}
		}
		else if(!strcmp(action,"re_reg"))
		{
			sip_master_rereg();
			API_Event_NameAndMsg(Event_XD_RSP_Message, "Sip re register ok");
		}
	}
	return 1;
}

static int GetIpByCmd(cJSON* cmd)
{
	int ip = inet_addr(GetEventItemString(cmd,  IX2V_IP_ADDR));
	char* roomNbr = GetEventItemString(cmd,  IX2V_IX_ADDR);

	if(ip == -1)
	{
		char* myIxAddr = API_PublicInfo_Read_String(PB_IX_ADDR);
		char* ixNet = API_Para_Read_String2(IX_NT);

		if(myIxAddr && !strcmp(roomNbr, myIxAddr))
		{
			ip = GetLocalIpByDevice(ixNet);
		}
		else
		{
			cJSON* devTb = cJSON_CreateArray();
			IXS_GetByNBR(ixNet, NULL, roomNbr, NULL, devTb);
			if(cJSON_GetArraySize(devTb))
			{
				ip = inet_addr(GetEventItemString(cJSON_GetArrayItem(devTb, 0),  IX2V_IP_ADDR));
			}
			cJSON_Delete(devTb);
		}
	}

	return ip;
}

int Event_UnlockRequestCallback(cJSON *cmd)
{
	int ip = GetIpByCmd(cmd);	
	char* myEthIp = API_PublicInfo_Read_String(PB_LAN_IP);
	char* myWlanIp = API_PublicInfo_Read_String(PB_WLAN_IP);
	char* lockNbr = GetEventItemString(cmd,  IX2V_LOCK_NBR);

	if(ip == -1)
	{
		return 0;
	}
	else if(ip == inet_addr(myEthIp) || ip == inet_addr(myWlanIp))
	{
		API_Event_Unlock(lockNbr, GetEventItemString(cmd,  IX2V_SOURCE));		
	}
	else
	{
		int lockId = 0;
		if(lockNbr && !strcmp(lockNbr, "RL2"))
		{
			lockId = 1;
		}
		Send_UnlockCmd_ByIp(ip, lockId);
	}

	return 1;
}

int Event_IX_HomeCall(cJSON *cmd)
{
	char* callServerState = API_PublicInfo_Read_String(PB_CALL_SER_STATE);
	int callKey = GetJsonObjectItemInt(cmd,  PB_IX_CALL_KEY);
	if(callServerState && !strcmp(callServerState, "Ring"))
	{
		if(callKey == API_PublicInfo_Read_Int(PB_IX_CALL_KEY))
		{
			API_CallServer_Redail();
		}
		else
		{
			API_CallServer_LocalBye();
		}
	}
	else if(callServerState && !strcmp(callServerState, "Wait"))
	{
		char input[11];
	
		snprintf(input, 11, "%s", GetEventItemString(cmd,  IX2V_IX_ADDR));
		switch(atoi(input))
		{
			case 2:
				API_Para_Read_String(HOME_KEY_IX_CALL_NBR2, input);
				break;
			case 3:
				API_Para_Read_String(HOME_KEY_IX_CALL_NBR3, input);
				break;
			case 4:
				API_Para_Read_String(HOME_KEY_IX_CALL_NBR4, input);
				break;
			default:
				API_Para_Read_String(HOME_KEY_IX_CALL_NBR, input);
				break;
		}
		if(input[0] == 0)
		{
			if(memcmp(GetSysVerInfo_BdRmMs()+4,"0000",4)==0)
			{
				snprintf(input, 11, "%s", "1");
			}
			else
			{
				memcpy(input, GetSysVerInfo_BdRmMs(), 8);
				input[8] = 0;
			}
		}

		dprintf("input = %s, callKey = %d\n", input, callKey);
	
		API_Event_IXCallAction("IxMainCall", input, callKey);
	}

	dprintf("callServerState = %s, callKey = %d\n", callServerState ? callServerState : "NULL", callKey);

	return 1;
}

/*****************************************************************
 * 
 * cmd:
 * {
 * 		"EventName":"EventReboot",
 * 		"SOURCE":"IX471S",
 * 		"IX_ADDR":"192.168.243.101"
 * }
******************************************************************/

int EventRebootProcess(cJSON *cmd)
{
    char* ipString = GetEventItemString(cmd, IX2V_IX_ADDR);
    char* source = GetEventItemString(cmd, IX2V_SOURCE);
	if(ipString[0] != 0)
	{
		int ip = inet_addr(ipString);
		API_Ring2(ip, "Wait for restart.", API_Para_Read_Int(Ring2ReportTime));
		WriteRebootFlag(&ip, 1);
	}

	HardwareRestart(source);

	return 1;
}

/*****************************************************************
 * 
 * cmd:
 * {
 * 		"EventName":"EventFactoryDefault",
 * 		"SOURCE":"IX471S",
 * 		"IX_ADDR":"192.168.243.101"
 * }
******************************************************************/

int EventFactoryDefaultProcess(cJSON *cmd)
{
    char* ipString = GetEventItemString(cmd, IX2V_IX_ADDR);
    char* source = GetEventItemString(cmd, IX2V_SOURCE);
	if(ipString&&ipString[0] != 0)
	{
		int ip = inet_addr(ipString);
		API_Ring2(ip, "Wait for restore and restart.", API_Para_Read_Int(Ring2ReportTime));
		WriteRebootFlag(&ip, 1);
	}

	FactoryDefaultProcess();

	return 1;
}

int RebootApp_Process(cJSON *cmd)
{
	usleep(3800*1000);
	#if defined(PID_IX850)	
	system("/mnt/nand1-1/App/rebootApp.sh lvgl-IX850");	
	#endif
	return 1;
}

int EventUpdateProcess(cJSON *cmd);

//网络连接成功回调函数
int NetworkConnectedReportCallback(cJSON *cmd)
{
	int netConnectedFlag = 0;

	char* lanState = API_PublicInfo_Read_String(PB_SUB_LAN_STATE);
	char* wlanState = API_PublicInfo_Read_String(PB_SUB_WLAN_STATE);
	if(lanState && !strcmp(lanState, "Connected"))
	{
		API_Event_NameAndMsg(EventNetworkedReadiness, "eth0");
		#ifdef PID_IX850
		API_SettingValueFresh(NULL);
		API_SettingNormalTip(NULL);//API_SettingSuccTip(NULL);
		#endif
	}
	
	if(wlanState && !strcmp(wlanState, "Connected"))
	{
		API_Event_NameAndMsg(EventNetworkedReadiness, "wlan0");
	}
}

//开锁密码汇报
int UnlockCodeReportCallback(cJSON *cmd)
{
	if(API_Para_Read_Int(UnlockCodeReportEnable))
	{
		char ixAddr[11];
		char rec_file[200];
		cJSON *event_record=NULL;
		char* ipString;
		cJSON* elemet;
		char* msg = GetEventItemString(cmd, "Message");
		
		if(memcmp(GetSysVerInfo_BdRmMs()+4,"0000",4)==0)
			return;
		
		strcpy(ixAddr, GetSysVerInfo_BdRmMs());
		strcpy(ixAddr+8, "01");
		#ifndef DH_LINK 
		if(API_ReqOneRecFile(rec_file)==0)
		{
			event_record=CreateOneEventRecord(time(NULL),msg,NULL, NULL, rec_file);
			if(event_record)
				AddOneEventRecordToLocal(event_record);	
		}
		#else
		event_record=NULL;
		#endif
		cJSON* devTb = cJSON_CreateArray();
		IXS_GetByNBR(NULL, NULL, ixAddr, NULL, devTb);
		
		
		cJSON_ArrayForEach(elemet, devTb)
		{
			ipString = cJSON_GetStringValue(cJSON_GetObjectItemCaseSensitive(elemet, IX2V_IP_ADDR));
			dprintf("ipString = %s\n", ipString);
			if(ipString)
			{
				int ip_int=inet_addr(ipString);
				API_Ring2(ip_int, msg, API_Para_Read_Int(Ring2ReportTime));
				if(event_record)
					AddOneEventRecordToRemote(ip_int,event_record);
			}
			
		}
		cJSON_Delete(devTb);
		cJSON_Delete(event_record);
	}
}

//远程事件记录
int EventTLogCallback(cJSON *cmd)
{
	cJSON* record = cJSON_Duplicate(cJSON_GetObjectItemCaseSensitive(cmd, IX2V_EventData), 1);
	if(record)
	{
		char temp[100];
		cJSON_AddStringToObject(record, "MY_TIME", GetCurrentTime(temp, 100, "%Y-%m-%d %H:%M:%S"));
		API_TB_AddByName(TB_NAME_LOG_TB, record);
		cJSON_Delete(record);		
	}
	return 1;
}

//IXG签到
int EventIXGCheckInCallback(cJSON *cmd)
{
	char* ipString = GetEventItemString(cmd, IX2V_IP_ADDR);
	IXG_RecordAddGW_ID(cmd);
	if(!API_TB_AddByName(TB_NAME_IXG_LIST, cmd))
	{
		cJSON *Where = cJSON_CreateObject();
		cJSON_AddItemToObject(Where, IX2V_MFG_SN, cJSON_Duplicate(cJSON_GetObjectItemCaseSensitive(cmd, IX2V_MFG_SN), 1));
		API_TB_UpdateByName(TB_NAME_IXG_LIST, Where, cmd);
		cJSON_Delete(Where);
	}
	API_UpdateIxgDtImList(inet_addr(ipString));

	return 1;
}


//等待检测超时，单位秒
static cJSON* GetDtImResultTimeout(int timeout)
{
	int timeCnt = (timeout*(1000/500));
	cJSON* ret = NULL;
	while(API_PublicInfo_Read_Bool(PB_InSearching)&&!API_PublicInfo_Read_Bool(PB_DT_LINK_STATE) && timeCnt)
	{
		usleep(500*1000);
		timeCnt--;
	}

	if(!API_PublicInfo_Read_Bool(PB_InSearching)&&timeCnt)
	{
		ret = cJSON_Duplicate(API_PublicInfo_Read(PB_LastSearchResult), 1) ;
	}
	return ret;
}


int IXGUpdateDtImCallback(cJSON *cmd)
{
	cJSON* dtIm = cJSON_GetObjectItemCaseSensitive(cmd, IX2V_DT_IM);
	char* ipString = GetEventItemString(cmd, IX2V_IP_ADDR);
	cJSON* checkResult = NULL;
	
	cJSON* report = cJSON_CreateObject();
	cJSON_AddStringToObject(report, IX2V_EventName, EventReportDT_IM_List);
	cJSON_AddNumberToObject(report, IX2V_IXG_ID, GetIXGDIP());
	cJSON_AddStringToObject(report, IX2V_IP_ADDR, GetSysVerInfo_IP_by_device(API_Para_Read_String2(IX_NT)));
	cJSON_AddStringToObject(report, IX2V_IX_ADDR, GetSysVerInfo_BdRmMs());
	cJSON_AddStringToObject(report, IX2V_MFG_SN, GetSysVerInfo_Sn());
	cJSON_AddStringToObject(report, IX2V_IX_TYPE, DeviceTypeToString(GetSysVerInfo_MyDeviceType()));
	cJSON_AddStringToObject(report, IX2V_UpTime, API_PublicInfo_Read_String(PB_UP_TIME));
	cJSON_AddStringToObject(report, IX2V_Platform, IX2V);
	if(API_DT_IM_Search(dtIm))
	{
		checkResult = GetDtImResultTimeout(50);

		if(checkResult)
		{
			int resultSize;
			if((resultSize = cJSON_GetArraySize(checkResult)) > 5)
			{
				cJSON* rusult = cJSON_AddObjectToObject(report, IX2V_Result);
				cJSON* element;
				for( ; resultSize > 5; resultSize--)
				{
					element = cJSON_DetachItemFromArray(checkResult, resultSize-1);
					cJSON_AddItemToObject(rusult, element->string, element);
				}
				API_XD_EventJson(inet_addr(ipString), report);
				cJSON_DeleteItemFromObjectCaseSensitive(report, IX2V_Result);
			}
			cJSON_AddItemToObject(report, IX2V_Result, checkResult);
		}
		else
		{
			cJSON_AddStringToObject(report, IX2V_Result, IX2V_ERROR);
		}
	}
	else
	{
		cJSON_AddStringToObject(report, IX2V_Result, IX2V_ERROR);
	}

	API_XD_EventJson(inet_addr(ipString), report);
	cJSON_Delete(report);

	return 1;
}

//查找数组元素，如有则删除
static int MyJSON_DeleteArrayItem(cJSON *array, const cJSON *item)
{
	int ret;
	int arraySize = cJSON_GetArraySize(array);
	cJSON* arrayItem;
	for(ret = 0; arraySize > 0; arraySize--)
	{
		arrayItem = cJSON_GetArrayItem(array, arraySize-1);
		if(cJSON_Compare(arrayItem, item, 1))
		{
			cJSON_DeleteItemFromArray(array, arraySize-1);
			ret++;
		}
	}

	return ret;
}

//查找数组元素，如无则增加
int MyJSON_AddArrayItem(cJSON *array, const cJSON *item)
{
	int ret;
	int arraySize = cJSON_GetArraySize(array);
	for(ret = 0; arraySize > 0; arraySize--)
	{
		if(cJSON_Compare(cJSON_GetArrayItem(array, arraySize-1), item, 1))
		{
			break;
		}
	}

	if(arraySize == 0)
	{
		cJSON_AddItemToArray(array, cJSON_Duplicate(item, 1));
	}

	return ret;
}


int IXGDtImReportCallback(cJSON *cmd)
{
	int ixgId = GetEventItemInt(cmd, IX2V_IXG_ID);
	if(ixgId)
	{
		int recordExistFlag;
		int bduId;
		char temp[8];
		cJSON* result = cJSON_GetObjectItemCaseSensitive(cmd, IX2V_Result);
		cJSON* dtIm;
		cJSON* online;
		cJSON* offline;
		cJSON* element;
		cJSON* bduIdJson;

		cJSON* Where = cJSON_CreateObject();
		cJSON* View = cJSON_CreateArray();
		cJSON* record = cJSON_CreateObject();
		cJSON_AddNumberToObject(Where, IX2V_IXG_ID, ixgId);

		if(API_TB_SelectBySortByName(TB_NAME_IXG_LIST, View, Where, 0))
		{
			recordExistFlag = 1;
			dtIm = cJSON_Duplicate(cJSON_GetObjectItemCaseSensitive(cJSON_GetArrayItem(View, 0), IX2V_DT_IM), 1);
		}
		else
		{
			recordExistFlag = 0;
			dtIm = cJSON_CreateArray();
		}

		cJSON_AddItemToObject(record, IX2V_IXG_ID, cJSON_Duplicate(cJSON_GetObjectItemCaseSensitive(cmd, IX2V_IXG_ID), 1));
		cJSON_AddItemToObject(record, IX2V_IP_ADDR, cJSON_Duplicate(cJSON_GetObjectItemCaseSensitive(cmd, IX2V_IP_ADDR), 1));
		cJSON_AddItemToObject(record, IX2V_IX_ADDR, cJSON_Duplicate(cJSON_GetObjectItemCaseSensitive(cmd, IX2V_IX_ADDR), 1));
		cJSON_AddItemToObject(record, IX2V_MFG_SN, cJSON_Duplicate(cJSON_GetObjectItemCaseSensitive(cmd, IX2V_MFG_SN), 1));
		cJSON_AddItemToObject(record, IX2V_IX_TYPE, cJSON_Duplicate(cJSON_GetObjectItemCaseSensitive(cmd, IX2V_IX_TYPE), 1));
		cJSON_AddItemToObject(record, IX2V_UpTime, cJSON_Duplicate(cJSON_GetObjectItemCaseSensitive(cmd, IX2V_UpTime), 1));
		cJSON_AddItemToObject(record, IX2V_Platform, cJSON_Duplicate(cJSON_GetObjectItemCaseSensitive(cmd, IX2V_Platform), 1));

		for(bduId = 0; bduId <= 8; bduId++)
		{
			snprintf(temp, 8, "BDU_%d", bduId);
			bduIdJson = cJSON_GetObjectItemCaseSensitive(result, bduId == 0 ? IX2V_DT_IM : temp);
			online = cJSON_GetObjectItemCaseSensitive(bduIdJson, IX2V_ONLINE);
			offline = cJSON_GetObjectItemCaseSensitive(bduIdJson, IX2V_OFFLINE);

			cJSON_ArrayForEach(element, offline)
			{
				if(bduId)
				{
					cJSON_SetNumberValue(element, element->valueint + bduId*100);
				}
				MyJSON_DeleteArrayItem(dtIm, element);
			}

			cJSON_ArrayForEach(element, online)
			{
				if(bduId)
				{
					cJSON_SetNumberValue(element, element->valueint + bduId*100);
				}
				MyJSON_AddArrayItem(dtIm, element);
			}
		}

		cJSON_AddItemToObject(record, IX2V_DT_IM, dtIm);
		IXG_RecordAddGW_ID(record);
		if(recordExistFlag)
		{
			API_TB_UpdateByName(TB_NAME_IXG_LIST, Where, record);
		}
		else
		{
			API_TB_AddByName(TB_NAME_IXG_LIST, record);
		}
		cJSON_Delete(Where);
		cJSON_Delete(View);
		cJSON_Delete(record);

		#if	defined(PID_IX850)
		API_DxgReportDtIm(ixgId);
		#endif
	}
	return 1;
}

//启动完成回调函数
static int StartCompleteCallback(cJSON *cmd)
{
	//#if defined(PID_IX622) || defined(PID_IX611) || defined(PID_IX850)
	#if defined(PID_IX611) || defined(PID_IX850)
	CmrDevManager_init();
	#endif
	API_Check_CERT();
	NDM_Init();
	InitDataSetList();
	API_Event_By_Name(Event_MAT_Update);

	#if defined(PID_IX622)
	MyLedCtrlInit();
	sleep(1);
	API_MyLedCtrl(MY_LED_CALL1, MY_LED_OFF, 0);
	API_MyLedCtrl(MY_LED_CALL2, MY_LED_OFF, 0);
	API_MyLedCtrl(MY_LED_CALL3, MY_LED_OFF, 0);
	API_MyLedCtrl(MY_LED_CALL4, MY_LED_OFF, 0);
	API_MyLedCtrl(MY_LED_NAMEPLATE, MY_LED_OFF, 0);
	if(!strcmp(GetSysVerInfo_BdRm(), "00990000"))
	{
		int addr = atoi((char*)GetSysVerInfo_Ms());
		if(addr)
		{
			API_MyLedCtrl(MY_LED_TALK, MY_LED_FLASH_FAST, addr);
		}
	}
	#endif
	return 1;
}

//网络就绪回调函数
static int NetworkedReadinessCallback(cJSON *cmd)
{
	char* netWork = GetEventItemString(cmd, EVENT_KEY_Message);
	char* ixNet = API_Para_Read_String2(IX_NT);

	//IX设备所在的网络就绪
	if(ixNet && !strcmp(netWork, ixNet))
	{ 
		//#ifdef PID_IX611
		//CmrDevManager_init();
		//#endif
		AutoTest_Init();
	}

	#if defined(PID_IX622)
	IX622_StartCompleted();
	#endif
	
	#if defined(PID_IX821)
	IX821_StartCompleted();
	#endif
	//不管哪个网络就绪
	RebootReport(netWork);
	
	XD2_Init();

	return 1;
}
#if	defined(PID_IX850)
int EventUpdateReportToRemoteCallback(cJSON *cmd)
{
	char* msg = cJSON_GetStringValue(cJSON_GetObjectItemCaseSensitive(cmd, "MSG"));
	//vtk_lvgl_lock();
	DxgUpdate_Report(msg);
	//vtk_lvgl_unlock();
	return 1;
}
#endif

//证书配置回调函数
static int CertConfigCallback(cJSON *cmd)
{
	char* opcode = GetEventItemString(cmd, "OPCODE");

	if(!strcmp(opcode, "CreateDcfg"))
	{
		//API_Create_DCFG_SELF("CERT_ETH0", SysType, NULL);
		API_Create_DCFG_SELF(SysType, NULL);
	}
	else if(!strcmp(opcode, "CreateCert"))
	{
		char* dcfgName = GetEventItemString(cmd, "DCFG_NAME");
		dcfgName = (dcfgName[0] == 0 ? CERT_DCFG_SELF_NAME : dcfgName);
		API_Create_CERT(dcfgName);
	}
	else if(!strcmp(opcode, "CheckGoLiveResult"))
	{
		API_CheckGoLiveResult();
	}
	else if(!strcmp(opcode, "GoLive"))
	{
		API_Go_Live();
	}
	else if(!strcmp(opcode, "DeleteCert"))
	{
		API_Delete_CERT();
	}
	else if(!strcmp(opcode, "CheckCert"))
	{
		int time = GetEventItemInt(cmd, "TIME");
		if(time)
		{
			API_Para_Write_Int(CERT_GoLive_Timer, time);
		}
		API_Check_CERT();
	}
	else if(!strcmp(opcode, "CreateCertSelf"))
	{
		//API_Create_DCFG_SELF("CERT_ETH0", SysType, NULL);
		API_Create_DCFG_SELF(SysType, NULL);
		API_Create_CERT(CERT_DCFG_SELF_NAME);
		API_Go_Live();
	}
	
	return 1;
}

//转发事件回调函数
static int TransmitEventCallback(cJSON *cmd)
{
	cJSON*  event = cJSON_GetObjectItemCaseSensitive(cmd, "Event");
	if(event)
	{
		char* targetIp = GetEventItemString(cmd, "T_TARGET_IP");
		int number = GetEventItemInt(cmd, "ORDER_NUMBER");
		cJSON_AddNumberToObject(event, "ORDER_NUMBER", number);
		if(targetIp[0])
		{
			int ip = inet_addr(targetIp);
			cJSON_AddStringToObject(event, "T_SOURCE_IP", GetSysVerInfo_IP_by_device(GetNetDeviceNameByTargetIp(ip)));
			API_EventTransmit_Json(ip, event);
		}
		else 
		{
			API_Event_Json(event);
		}
	}
	return 1;
}

//转发事件回调函数
static int EventXD_SaveUserCard_Callback(cJSON *cmd)
{
	cJSON* card = cJSON_CreateObject();
	cJSON_AddStringToObject(card, "CARD_NUM", GetJsonObjectItemString(cmd,  "CARD_NUM"));
	cJSON_AddNumberToObject(card, "ROOM", atoi(GetJsonObjectItemString(cmd,  "ROOM")));
	cJSON_AddStringToObject(card, "DATE", GetJsonObjectItemString(cmd,  "DATE"));
	cJSON_AddStringToObject(card, "NAME", GetJsonObjectItemString(cmd,  "NAME"));

	if(Judge_Card_Exist(card)>0)
	{
		API_Event_NameAndMsg(Event_XD_RSP_Message, "Card number is aleady exist!");
	}
	else
	{
		if(update_one_carddata_to_tb(card))
		{
			API_Event_NameAndMsg(Event_XD_RSP_Message, "Add card success");
		}
		else
		{
			API_Event_NameAndMsg(Event_XD_RSP_Message, "Add card error");
		}
	}
	cJSON_Delete(card);
	return 1;
}

int EventBackup_Process(cJSON *cmd)
{
	char cmd_line[200];
	cJSON *tb_list=cJSON_GetObjectItemCaseSensitive(cmd,"TB_LIST");
	 char *file_name =GetEventItemString(cmd,"BAK_FILE");
	 char temp_file_path[200];
	 snprintf(temp_file_path,200,"%s%s",TEMP_Folder,"TbBak");
	 create_multi_dir(temp_file_path);
	if(tb_list&&ResTbBackup(tb_list,temp_file_path)==0)
	{
		snprintf(cmd_line, 200, "tar -cf %s -C %s %s", file_name,TEMP_Folder, "TbBak");
		system(cmd_line);
		snprintf(cmd_line, 200, "rm -r %s", temp_file_path);
		system(cmd_line);
		if(strstr(file_name,SDCARD_Backup_Path))
		{
			snprintf(cmd_line, 200, "cp %s %sBAK.TB.tar", file_name,SDCARD_Backup_Path);
			system(cmd_line);
		}
		sync();
		BEEP_CONFIRM();
		
	}
}
void Api_Backup(cJSON *backup_list,char *back_file)
{
	cJSON *event = cJSON_CreateObject();
	cJSON_AddStringToObject(event, EVENT_KEY_EventName, EventBackupReq);
	cJSON_AddItemToObject(event, "TB_LIST", cJSON_Duplicate(backup_list,1));
	cJSON_AddStringToObject(event, "BAK_FILE", back_file);
	API_Event_Json(event);
	cJSON_Delete(event);

}
int EventRestore_Process(cJSON *cmd)
{
	char cmd_line[200];
	//cJSON *tb_list=cJSON_GetObjectItemCaseSensitive(cmd,"TB_LIST");
	 char *file_name =GetEventItemString(cmd,"BAK_FILE");
	 char temp_file_path[200];
	 //create_multi_dir(Backup_ResTb_Folder);
	if( access(file_name, F_OK ) == 0 )
	{
		snprintf(temp_file_path,200,"%s%s",TEMP_Folder,"TbBak");
		snprintf(cmd_line, 200, "rm -r %s", temp_file_path);
		system(cmd_line);
		sync();
		create_multi_dir(TEMP_Folder);
		snprintf(cmd_line, 200, "tar -xf %s -C %s", file_name,TEMP_Folder);
		system(cmd_line);
		sync();
		
		if( access(temp_file_path, F_OK ) == 0 )
		{
			if(ResTbRestore(temp_file_path)>0)
				BEEP_CONFIRM();
		}
		
	}
}
void Api_Restore(char *back_file)
{
	cJSON *event = cJSON_CreateObject();
	cJSON_AddStringToObject(event, EVENT_KEY_EventName, EventRestoreReq);
	
	cJSON_AddStringToObject(event, "BAK_FILE", back_file);
	API_Event_Json(event);
	cJSON_Delete(event);

}
int Event_LockReq(cJSON *cmd);
int DR_DisplayEventLock(cJSON *cmd);
int Event_VoicePlay(cJSON *cmd);
int DR_DisplayEventCall(cJSON *cmd);
int M4_DisplayEventCall(cJSON *cmd);
int EventCallbackCallAction(cJSON *cmd);

int Event_FwUpdateReportToApp(cJSON *cmd);
int Event_FwUpdateReportKeyB(cJSON *cmd);
int Event_FwUpdateStart(cJSON *cmd);
int Event_FwUpdateCancel(cJSON *cmd);

int EventCallActionFilter(cJSON *cmd);
int EventKeyABFilter(cJSON* event);
int EventCardSwip(cJSON *cmd);
int EventCardSetupState(cJSON *cmd);
int EventCardSetupRoom(cJSON *cmd);
int EventCardSetupInfo(cJSON *cmd);
int EventManageCardSet(cJSON *cmd);
int Led_DisplayNetState(cJSON *cmd);
#if	defined(PID_IX850)
int Menu_DisplayEventCall(cJSON *cmd);
#endif
int EventIXProgSetup(cJSON *cmd);
int Event_XD_CheckImOnlne(cJSON *cmd);
int EventBECall_Process(cJSON *cmd);
int EventBECallRetrig_Process(cJSON *cmd);
int EventBECallClose_Process(cJSON *cmd);
int EventBECallBye_Process(cJSON *cmd);
int EventBECallRing_Process(cJSON *cmd);
int EventBECallTalk_Process(cJSON *cmd);
int EventBECallDivert_Process(cJSON *cmd);
int EventBECallAnswer_Process(cJSON *cmd);

int IXS_ProxyReqUpdateTb_CallBack(cJSON *event);
int IXS_UpdateData_CallBack(cJSON* cmd);
int IXS_GetRemoteTb_CallBack(cJSON *event);

int EventMon_Process(cJSON *cmd);
int ProgReport_CallBack(cJSON *event);

int WifiMute_Process(cJSON *cmd);
int IXS_ProxyNetConnectedCallback(cJSON *cmd);
int GetNamelistCallback(cJSON *cmd);
int MenuEventLock(cJSON *cmd);

//#if	defined(PID_IX850)
int Event_DsCallRecord(cJSON *cmd);
int RecManager_FileReq(cJSON *cmd);
int EventRecFileRsp_CallRecord(cJSON *cmd);
//#endif

int EventPlayback_Process(cJSON *cmd);
int CallRecordCB(cJSON *cmd);
int DivertStateCallback(cJSON *cmd);
int IxDsDivertStateSetCallback(cJSON *cmd);
int IXGDivertStateSetCallback(cJSON *cmd);
int IXGAutoRecFileCache(cJSON*cmd);
int IXGMemoRecEvent_Process(cJSON *cmd);
int IXGMissCallEvent_Process(cJSON *cmd);
int EventIO_SettingCallback(cJSON* cmd);
int AutoTest_Callback(cJSON *cmd);

int DFRM_KeyEvent_Callback(cJSON *cmd);

int NDM_S_CtrlCallback(cJSON *cmd);
int NDM_C_CtrlCallback(cJSON *cmd);
int NDM_C_ResponeCallback(cJSON *cmd);
int NDM_S_CheckEndCallback(cJSON *cmd);
int EventCardIdReportCallback(cJSON *cmd);

int Event_SipManage_Callback(cJSON *cmd);

int Event_VideoSerError_Callback(cJSON *cmd);
int CertManageCallback(cJSON *cmd);

int EventIperfTestStartCallback(cJSON *cmd);
int EventIperfTestReportCallback(cJSON *cmd);
int IXS_ListUpdateForDataset(cJSON *cmd);
int RES_ListUpdateForDataset(cJSON *cmd);
int EventIperfTestReportToDxgconfig(cJSON *cmd);
int EventDXGListRefreshCallback(cJSON *cmd);
int EventDXGListReportCallback(cJSON *cmd);
int EventMenuResCheckOnlineCallback(cJSON* cmd);
int EventMenuIpcManageCallback(cJSON* cmd);
int SatHybridProgListChangeProcess(cJSON *cmd);
int SatHybridProgListUnChangeProcess(cJSON *cmd);
int EventMenuSatHybridProgCallback(cJSON *cmd);
int SatHybridCheckListChangeProcess(cJSON *cmd);
int EventMenuSatHybridReportCallback(cJSON *cmd);
int EventDH_HybridProgOnlineCallback(cJSON* cmd);
int EventDH_HybridProgOnlineReportCallback(cJSON* cmd);
int IM_ResgistCallBack(cJSON* cmd);
int MenuUpdateDhImProcess(cJSON *cmd);
int EventMenuAptMatReportCallback(cJSON *cmd);
int AptMatCheckListChangeProcess(cJSON *cmd);
int MenuSettingProcess_Callback(cJSON *cmd);
int ReqSNFromServer_Callback(cJSON *cmd);
int SetEocMacCallback(cJSON *cmd);
int menu_sn_eoc_err_disp(cJSON *cmd);
int UpdateResFromSD_Process(cJSON *cmd);
int M4LEDDisplayBySort_Process(cJSON *cmd);
int M4LEDDisplayImOnline_Process(cJSON *cmd);
int Cmr7610Config_Callback(cJSON *cmd);
int EventMenuVmManageCallback(cJSON *cmd);


void EventCallbackFunctionRegister(void)
{
	API_EventCallbackFunctionAdd("StartCompleteCallback", StartCompleteCallback, NULL);
	API_EventCallbackFunctionAdd("NetworkedReadinessCallback", NetworkedReadinessCallback, NULL);

	API_EventCallbackFunctionAdd("Event_LedCtrl", Event_LedCtrl, NULL);
	API_EventCallbackFunctionAdd("DR_DisplayEventLock", DR_DisplayEventLock, NULL);
	API_EventCallbackFunctionAdd("DR_DisplayEventCall", DR_DisplayEventCall, NULL);
	API_EventCallbackFunctionAdd("M4_DisplayEventCall", M4_DisplayEventCall, NULL);
	API_EventCallbackFunctionAdd("Event_LockReq", Event_LockReq, NULL);
	API_EventCallbackFunctionAdd("Event_VoicePlay", Event_VoicePlay, NULL);
	API_EventCallbackFunctionAdd("Event_KeyAB", Event_KeyAB, NULL);
	API_EventCallbackFunctionAdd("Event_CallBeeper", Event_CallBeeper, NULL);
	API_EventCallbackFunctionAdd("Event_CallVoice", Event_CallVoice, NULL);
	API_EventCallbackFunctionAdd("Event_LockVoice", Event_LockVoice, NULL);
	API_EventCallbackFunctionAdd("Event_IpcConfig", Event_IpcConfig, NULL);
	API_EventCallbackFunctionAdd("EventCallbackLanConfig", EventCallbackLanConfig, NULL);
	API_EventCallbackFunctionAdd("EventCallbackXD_Report", EventCallbackXD_Report, NULL);	
	API_EventCallbackFunctionAdd("EventFiveSecondsResetDisplay", EventFiveSecondsResetDisplay, NULL);		
	API_EventCallbackFunctionAdd("EventConnetWiFi", EventConnetWiFi, NULL);
	API_EventCallbackFunctionAdd("EventCallbackCallAction", EventCallbackCallAction, NULL);		
	API_EventCallbackFunctionAdd("Event_FwUpdateReportToApp", Event_FwUpdateReportToApp, NULL);
	API_EventCallbackFunctionAdd("Event_FwUpdateReportKeyB", Event_FwUpdateReportKeyB, NULL);
	API_EventCallbackFunctionAdd("Event_FwUpdateStart", Event_FwUpdateStart, NULL);
	API_EventCallbackFunctionAdd("Event_FwUpdateCancel", Event_FwUpdateCancel, NULL);
	
	API_EventFilterCallbackFunctionAdd("EventCallActionFilter", EventCallActionFilter, NULL);		
	API_EventFilterCallbackFunctionAdd("EventKeyABFilter", EventKeyABFilter, NULL);		
	API_EventFilterCallbackFunctionAdd("EventCardSwip", EventCardSwip, NULL);		
	API_EventFilterCallbackFunctionAdd("EventCardSetupState", EventCardSetupState, NULL);		
	API_EventFilterCallbackFunctionAdd("EventCardSetupRoom", EventCardSetupRoom, NULL);		
	API_EventFilterCallbackFunctionAdd("EventCardSetupInfo", EventCardSetupInfo, NULL);		
	API_EventFilterCallbackFunctionAdd("EventManageCardSetup", EventManageCardSet, NULL);		

	API_EventCallbackFunctionAdd("Led_DisplayNetState", Led_DisplayNetState, NULL);
	API_EventCallbackFunctionAdd("IXS_ProxyNetConnectedCallback", IXS_ProxyNetConnectedCallback, NULL);
	API_EventCallbackFunctionAdd("NetworkConnectedReportCallback", NetworkConnectedReportCallback, NULL);	
	API_EventCallbackFunctionAdd("EventUpdateProcess", EventUpdateProcess, NULL);
	API_EventCallbackFunctionAdd("UnlockCodeReportCallback", UnlockCodeReportCallback, NULL);
	API_EventCallbackFunctionAdd("EventIO_SettingCallback", EventIO_SettingCallback, NULL);
	API_EventCallbackFunctionAdd("EventTLogCallback", EventTLogCallback, NULL);
	API_EventCallbackFunctionAdd("NDM_S_CtrlCallback", NDM_S_CtrlCallback, NULL);
	API_EventCallbackFunctionAdd("NDM_C_CtrlCallback", NDM_C_CtrlCallback, NULL);
	API_EventCallbackFunctionAdd("NDM_C_ResponeCallback", NDM_C_ResponeCallback, NULL);
	API_EventFilterCallbackFunctionAdd("EventCardIdReportCallback", EventCardIdReportCallback, NULL);		

	#if	defined(PID_IX850)
	API_EventCallbackFunctionAdd("Menu_DisplayEventCall", Menu_DisplayEventCall, NULL);
	API_EventCallbackFunctionAdd("MenuEventLock", MenuEventLock, NULL);
	API_EventFilterCallbackFunctionAdd("EventMenuResCheckOnlineCallback", EventMenuResCheckOnlineCallback, NULL);		
	#endif
	API_EventCallbackFunctionAdd("EventIXProgSetup", EventIXProgSetup, NULL);
	API_EventCallbackFunctionAdd("Event_XD_CheckImOnlne", Event_XD_CheckImOnlne, NULL);
	API_EventCallbackFunctionAdd("CheckUnlockPassword", CheckUnlockPassword, NULL);
	API_EventCallbackFunctionAdd("Event_SipManage", Event_SipManage, NULL);	
	API_EventCallbackFunctionAdd("Event_IX_HomeCall", Event_IX_HomeCall, NULL);	
	
	API_EventCallbackFunctionAdd("EventBECall_Process", EventBECall_Process, NULL);
	API_EventCallbackFunctionAdd("EventBECallRetrig_Process", EventBECallRetrig_Process, NULL);
	API_EventCallbackFunctionAdd("EventBECallClose_Process", EventBECallClose_Process, NULL);
	API_EventCallbackFunctionAdd("EventBECallBye_Process", EventBECallBye_Process, NULL);
	API_EventCallbackFunctionAdd("EventBECallRing_Process", EventBECallRing_Process, NULL);
	API_EventCallbackFunctionAdd("EventBECallTalk_Process", EventBECallTalk_Process, NULL);
	API_EventCallbackFunctionAdd("EventBECallDivert_Process", EventBECallDivert_Process, NULL);
	API_EventCallbackFunctionAdd("EventBECallAnswer_Process", EventBECallAnswer_Process, NULL);

	API_EventCallbackFunctionAdd("IXS_ProxyReqUpdateTb_CallBack", IXS_ProxyReqUpdateTb_CallBack, NULL);	
	API_EventCallbackFunctionAdd("IXS_GetRemoteTb_CallBack", IXS_GetRemoteTb_CallBack, NULL);	
	API_EventCallbackFunctionAdd("IXS_UpdateData_CallBack", IXS_UpdateData_CallBack, NULL);
	API_EventCallbackFunctionAdd("EventMon_Process", EventMon_Process, NULL);
	API_EventCallbackFunctionAdd("Event_UnlockRequestCallback", Event_UnlockRequestCallback, NULL);
	API_EventCallbackFunctionAdd("EventRebootProcess", EventRebootProcess, NULL);
	API_EventCallbackFunctionAdd("EventFactoryDefaultProcess", EventFactoryDefaultProcess, NULL);
	
	#if defined(PID_IX611)
	API_EventCallbackFunctionAdd("Event_IX611_CallLedCtrl", Event_IX611_CallLedCtrl, NULL);
	API_EventCallbackFunctionAdd("Event_DsCallRecord", Event_DsCallRecord, NULL);
	#endif

	#if defined(PID_IX622)
	API_EventCallbackFunctionAdd("Event_IX622_CallLedCtrl", Event_IX622_CallLedCtrl, NULL);
	API_EventCallbackFunctionAdd("Event_IX622_CardLedCtrl", Event_IX622_CardLedCtrl, NULL);
	API_EventCallbackFunctionAdd("Event_DsCallRecord", Event_DsCallRecord, NULL);
	API_EventCallbackFunctionAdd("EventRecFileRsp_CallRecord", EventRecFileRsp_CallRecord, NULL);
	#endif
	#if defined(PID_IX821)
	API_EventCallbackFunctionAdd("Event_DsCallRecord", Event_DsCallRecord, NULL);
	API_EventCallbackFunctionAdd("EventRecFileRsp_CallRecord", EventRecFileRsp_CallRecord, NULL);
	API_EventCallbackFunctionAdd("M4LEDDisplayBySort_Process", M4LEDDisplayBySort_Process, NULL);
	API_EventCallbackFunctionAdd("M4LEDDisplayImOnline_Process", M4LEDDisplayImOnline_Process, NULL);
	#endif
	#if	defined(PID_IX850)
	API_EventCallbackFunctionAdd("EventPlayback_Process", EventPlayback_Process, NULL);
	API_EventCallbackFunctionAdd("ProgReport_CallBack", ProgReport_CallBack, NULL);
	API_EventCallbackFunctionAdd("GetNamelistCallback", GetNamelistCallback, NULL);
	API_EventCallbackFunctionAdd("Event_DsCallRecord", Event_DsCallRecord, NULL);
	API_EventCallbackFunctionAdd("EventRecFileRsp_CallRecord", EventRecFileRsp_CallRecord, NULL);
	API_EventCallbackFunctionAdd("IxDsDivertStateSetCallback", IxDsDivertStateSetCallback, NULL);
	API_EventCallbackFunctionAdd("EventIXGCheckInCallback", EventIXGCheckInCallback, NULL);	
	API_EventCallbackFunctionAdd("IXGDtImReportCallback", IXGDtImReportCallback, NULL);
	API_EventCallbackFunctionAdd("EventUpdateReportToRemoteCallback", EventUpdateReportToRemoteCallback, NULL);
	API_EventCallbackFunctionAdd("Event_SipManage_Callback", Event_SipManage_Callback, NULL);
	API_EventCallbackFunctionAdd("MenuSettingProcess_Callback", MenuSettingProcess_Callback, NULL);
	#endif
	
	API_EventCallbackFunctionAdd("RecManager_FileReq", RecManager_FileReq, NULL);
	API_EventCallbackFunctionAdd("RebootApp_Process", RebootApp_Process, NULL);
	API_EventCallbackFunctionAdd("AutoTest_Callback", AutoTest_Callback, NULL);
	API_EventCallbackFunctionAdd("CertConfigCallback", CertConfigCallback, NULL);
	API_EventCallbackFunctionAdd("TransmitEventCallback", TransmitEventCallback, NULL);
	API_EventCallbackFunctionAdd("Event_VideoSerError_Callback", Event_VideoSerError_Callback, NULL);
	API_EventCallbackFunctionAdd("EventIperfTestStartCallback", EventIperfTestStartCallback, NULL);	
	API_EventCallbackFunctionAdd("EventIperfTestReportCallback", EventIperfTestReportCallback, NULL);	
	API_EventCallbackFunctionAdd("IXS_ListUpdateForDataset", IXS_ListUpdateForDataset, NULL);
	API_EventCallbackFunctionAdd("RES_ListUpdateForDataset", RES_ListUpdateForDataset, NULL);
	#ifdef PID_IX850
	API_EventCallbackFunctionAdd("SatHybridProgListChangeProcess", SatHybridProgListChangeProcess, NULL);
	API_EventCallbackFunctionAdd("SatHybridProgListUnChangeProcess", SatHybridProgListUnChangeProcess, NULL);
	API_EventCallbackFunctionAdd("SatHybridCheckListChangeProcess", SatHybridCheckListChangeProcess, NULL);
	API_EventCallbackFunctionAdd("EventMenuSatHybridProgCallback", EventMenuSatHybridProgCallback, NULL);
	API_EventCallbackFunctionAdd("EventMenuSatHybridReportCallback", EventMenuSatHybridReportCallback, NULL);
	API_EventCallbackFunctionAdd("EventDH_HybridProgOnlineCallback", EventDH_HybridProgOnlineCallback, NULL);
	API_EventCallbackFunctionAdd("EventDH_HybridProgOnlineReportCallback", EventDH_HybridProgOnlineReportCallback, NULL);
	API_EventCallbackFunctionAdd("MenuUpdateDhImProcess", MenuUpdateDhImProcess, NULL);
	API_EventCallbackFunctionAdd("EventMenuAptMatReportCallback", EventMenuAptMatReportCallback, NULL);
	API_EventCallbackFunctionAdd("AptMatCheckListChangeProcess", AptMatCheckListChangeProcess, NULL);
	API_EventCallbackFunctionAdd("menu_sn_eoc_err_disp", menu_sn_eoc_err_disp, NULL);
	#endif
	API_EventCallbackFunctionAdd("IM_ResgistCallBack", IM_ResgistCallBack, NULL);
	API_EventCallbackFunctionAdd("ReqSNCb", ReqSNFromServer_Callback, NULL);
	API_EventCallbackFunctionAdd("SetEocMacCallback", SetEocMacCallback, NULL);
	API_EventCallbackFunctionAdd("UpdateResFromSD_Process", UpdateResFromSD_Process, NULL);
	API_EventCallbackFunctionAdd("Cmr7610Config_Callback", Cmr7610Config_Callback, NULL);
	API_EventCallbackFunctionAdd("EventBackup_Process", EventBackup_Process, NULL);
	API_EventCallbackFunctionAdd("EventRestore_Process", EventRestore_Process, NULL);
	
	
	//äťŁç ä¸­éç˝Žĺč°ĺ˝ć°

	API_EventConfigAdd(EventStartComplete, "StartCompleteCallback", 1, NULL);
	API_EventConfigAdd(EventNetworkedReadiness, "NetworkedReadinessCallback", 1, NULL);	

	API_EventConfigAdd(EventLockReq, "Event_LockReq", 1, NULL);
	API_EventConfigAdd(EventLinkState, "Event_LedCtrl", 1, NULL);	
	API_EventConfigAdd(EventLockAction, "Event_LockVoice", 1, NULL);	
	//API_EventConfigAdd(EventLockAction, "DR_DisplayEventLock", 1, NULL);
	API_EventConfigAdd(EventLockAction, "Event_LedCtrl", 1, NULL);
	API_EventConfigAdd(EventIX_HomeCall, "Event_IX_HomeCall", 1, NULL);

	#if	defined(PID_IX850)
	API_EventConfigAdd(EventLockAction, "MenuEventLock", 1, NULL);
	API_EventConfigAdd(EventCallState, "Event_CallVoice", 1, NULL);
	API_EventConfigAdd(EventCallState, "Event_DsCallRecord", 1, NULL);
	API_EventConfigAdd(EventCall, "Event_DsCallRecord", 1, NULL);
	#elif defined(PID_IX611)
	API_EventConfigAdd(EventCallState, "Event_CallVoice", 1, NULL);
	API_EventConfigAdd(EventCallState, "Event_IX611_CallLedCtrl", 1, NULL);	
	API_EventConfigAdd(EventSnErrDisp,"Event_IX611_CallLedCtrl", 1, NULL);
		#ifdef DH_LINK
		API_EventConfigAdd(EventCallState, "Event_DsCallRecord", 1, NULL);
		API_EventConfigAdd(EventCall, "Event_DsCallRecord", 1, NULL);
		#endif
	#endif

	#if defined(PID_IX622)
	API_EventConfigAdd(EventCallState, "Event_CallVoice", 1, NULL);
	API_EventConfigAdd(EventCallState, "Event_IX622_CallLedCtrl", 1, NULL);	
	API_EventConfigAdd(EventCardSetupLed, "Event_IX622_CardLedCtrl", 1, NULL);

	API_EventConfigAdd(EventCallState, "Event_DsCallRecord", 1, NULL);
	API_EventConfigAdd(EventCall, "Event_DsCallRecord", 1, NULL);
	API_EventConfigAdd(EventRecFileRsp, "EventRecFileRsp_CallRecord", 1, NULL);
	API_EventConfigAdd(EventSnErrDisp,"Event_IX622_CallLedCtrl", 1, NULL);
	#endif

	#if	defined(PID_IX850)
	API_EventConfigAdd(EventCallState, "Menu_DisplayEventCall", 1, NULL);
	API_EventConfigAdd(EventCall, "Menu_DisplayEventCall", 1, NULL);
	#endif
	#if	defined(PID_IX821)
	API_EventConfigAdd(EventCallState, "Event_DsCallRecord", 1, NULL);
	API_EventConfigAdd(EventCall, "Event_DsCallRecord", 1, NULL);
	API_EventConfigAdd(EventCallState, "DR_DisplayEventCall", 1, NULL);
	API_EventConfigAdd(EventCall, "DR_DisplayEventCall", 1, NULL);
	API_EventConfigAdd(EventCallState, "M4_DisplayEventCall", 1, NULL);
	API_EventConfigAdd(EventCall, "M4_DisplayEventCall", 1, NULL);
	API_EventConfigAdd(EventLockAction, "DR_DisplayEventLock", 1, NULL);
	API_EventConfigAdd(EventSnErrDisp,"Event_LedCtrl", 1, NULL);
	API_EventConfigAdd(EventSnErrDisp,"Led_DisplayNetState", 1, NULL);
	API_EventConfigAdd(EventCall, "Event_LedCtrl", 1, NULL);
	API_EventConfigAdd(EventCallState, "Event_LedCtrl", 1, NULL);
	API_EventConfigAdd(EventM4LEDDisplayBySort, "M4LEDDisplayBySort_Process", 1, NULL);
	API_EventConfigAdd(EventM4LEDDisplayImOnline, "M4LEDDisplayImOnline_Process", 1, NULL);
	#endif

	API_EventConfigAdd(EventCall, "Event_CallBeeper", 1, NULL);
	API_EventConfigAdd(EventCall, "Event_CallVoice", 1, NULL);
	API_EventConfigAdd(EventKey_A, "Event_KeyAB", 1, "EventKeyABFilter");
	API_EventConfigAdd(EventKey_B, "Event_KeyAB", 1, "EventKeyABFilter");
	API_EventConfigAdd(Event_IPC_AUTO_CONFIG, "Event_IpcConfig", 1, NULL);
	API_EventConfigAdd(EventTouchKeyDisplay, "Event_LedCtrl", 1, NULL);
	API_EventConfigAdd(Event_XD_REQ_LAN_RECONFIG, "EventCallbackLanConfig", 1, NULL);
	API_EventConfigAdd(EventLinkState, "EventFiveSecondsResetDisplay", 1, NULL);	
	API_EventConfigAdd(Event_XD_REQ_ADD_WIFI, "EventConnetWiFi", 1, NULL);
	API_EventConfigAdd(EventCallAction, "EventCallbackCallAction", 1, NULL);

	API_EventConfigAdd(EventUpdateReport, "Event_FwUpdateReportToApp", 1, NULL);
	API_EventConfigAdd(EventUpdateReport, "Event_FwUpdateReportKeyB", 1, NULL);
	API_EventConfigAdd(EventUpdateStart, "Event_FwUpdateStart", 1, NULL);
	API_EventConfigAdd(EventUpdateCancel, "Event_FwUpdateCancel", 1, NULL);
	API_EventConfigAdd(EventCard, "EventCardSwip", 1, NULL);
	API_EventConfigAdd(EventCardState, "EventCardSetupState", 1, NULL);
	API_EventConfigAdd(EventCardRoom, "EventCardSetupRoom", 1, NULL);
	API_EventConfigAdd(EventManageCard, "EventManageCardSetup", 1, NULL);
	API_EventConfigAdd(EventNetLinkState, "Led_DisplayNetState", 1, NULL);
	API_EventConfigAdd(EventNetLinkState, "IXS_ProxyNetConnectedCallback", 1, NULL);
	API_EventConfigAdd(EventNetLinkState, "NetworkConnectedReportCallback", 1, NULL);
	API_EventConfigAdd(EventCardReport, "EventCardSetupInfo", 1, NULL);
	
	API_EventConfigAdd(EventIXProg, "EventIXProgSetup", 1, NULL);
	API_EventConfigAdd(EventIXProgCheck, "EventIXProgSetup", 1, NULL);
	API_EventConfigAdd(EventIXSearch, "EventIXProgSetup", 1, NULL);
	API_EventConfigAdd(Event_XD_REQ_CHECK_IM_ONLINE, "Event_XD_CheckImOnlne", 1, NULL);
	API_EventConfigAdd(Event_XD_REQ_Check_Password, "CheckUnlockPassword", 1, NULL);

	API_EventConfigAdd(Event_XD_RSP_LAN_RECONFIG, "EventCallbackXD_Report", 1, NULL);
	API_EventConfigAdd(Event_XD_RSP_CHECK_IM_ONLINE, "EventCallbackXD_Report", 1, NULL);
	API_EventConfigAdd(Event_XD_RSP_ADD_WIFI, "EventCallbackXD_Report", 1, NULL);
	API_EventConfigAdd(Event_XD_RSP_Card_Management, "EventCallbackXD_Report", 1, NULL);
	API_EventConfigAdd(Event_XD_RSP_Message, "EventCallbackXD_Report", 1, NULL);	
	API_EventConfigAdd(Event_XD_REQ_SIP_Management, "Event_SipManage", 1, NULL);
	
	API_EventConfigAdd(EventBECall, "EventBECall_Process", 1, NULL);
	API_EventConfigAdd(EventBECallRetrig, "EventBECallRetrig_Process", 1, NULL);
	API_EventConfigAdd(EventBECallClose, "EventBECallClose_Process", 1, NULL);
	API_EventConfigAdd(EventBECallBye, "EventBECallBye_Process", 1, NULL);
	API_EventConfigAdd(EventBECallRing, "EventBECallRing_Process", 1, NULL);
	API_EventConfigAdd(EventBECallTalk, "EventBECallTalk_Process", 1, NULL);
	API_EventConfigAdd(EventBECallDivert, "EventBECallDivert_Process", 1, NULL);
	API_EventConfigAdd(EventBECallAnswer, "EventBECallAnswer_Process", 1, NULL);	
	API_EventConfigAdd(Event_IXS_ReqUpdateTb, "IXS_ProxyReqUpdateTb_CallBack", 1, NULL);
	API_EventConfigAdd(Event_IXS_GetRemoteTb, "IXS_GetRemoteTb_CallBack", 1, NULL);

	API_EventConfigAdd(EventMonOpen, "EventMon_Process", 1, NULL);
	API_EventConfigAdd(EventMonClose, "EventMon_Process", 1, NULL);
	API_EventConfigAdd(EventMonState, "EventMon_Process", 1, NULL);
	API_EventConfigAdd(EventGetMonList, "EventMon_Process", 1, NULL);
	API_EventConfigAdd(EventUnlock_Request, "Event_UnlockRequestCallback", 1, NULL);
	API_EventConfigAdd(EventReboot, "EventRebootProcess", 1, NULL);
	API_EventConfigAdd(EventFactoryDefault, "EventFactoryDefaultProcess", 1, NULL);
	API_EventConfigAdd(EventUpdateRequest, "EventUpdateProcess", 1, NULL);
	API_EventConfigAdd(EventUnlockCodeReport, "UnlockCodeReportCallback", 1, NULL);
	API_EventConfigAdd(EventIO_Setting, "EventIO_SettingCallback", 1, NULL);
	API_EventConfigAdd(EventRemoteElog, "EventTLogCallback", 1, NULL);
	API_EventConfigAdd(EventCardIdReport, "EventCardIdReportCallback", 1, NULL);

	#if defined(PID_IX850)
	API_EventConfigAdd(EventPlayback, "EventPlayback_Process", 1, NULL);
	API_EventConfigAdd(EventGetNamelist, "GetNamelistCallback", 1, NULL);
	API_EventConfigAdd(Event_DatasetUpdate, "GetNamelistCallback", 1, NULL);
	API_EventConfigAdd(EventRecFileRsp, "EventRecFileRsp_CallRecord", 1, NULL);
	API_EventConfigAdd(EventSetDivertState, "IxDsDivertStateSetCallback", 1, NULL);
	//API_EventConfigAdd(EventIXGCheckIn, "EventIXGCheckInCallback", 0, NULL);
	API_EventConfigAdd(EventReportDT_IM_List, "IXGDtImReportCallback", 1, NULL);
	API_EventConfigAdd(EventUpdateReportToRemote, "EventUpdateReportToRemoteCallback", 1, NULL);
	API_EventConfigAdd(EventSipManage, "Event_SipManage_Callback", 0, NULL);
	API_EventConfigAdd(EventMenuSettingProcess, "MenuSettingProcess_Callback", 1, NULL);
	#endif
	
	API_EventConfigAdd(EventRecFileReq, "RecManager_FileReq", 1, NULL);


	API_EventConfigAdd(EventRebootApp, "RebootApp_Process", 1, NULL);
	API_EventConfigAdd(EventAutoTest, "AutoTest_Callback", 1, NULL);
	API_EventConfigAdd(EventCertConfig, "CertConfigCallback", 1, NULL);
	API_EventConfigAdd(EventTransmission, "TransmitEventCallback", 1, NULL);
	API_EventConfigAdd(EventNDM_S_Ctrl, "NDM_S_CtrlCallback", 0, NULL);
	API_EventConfigAdd(EventNDM_C_Ctrl, "NDM_C_CtrlCallback", 0, NULL);
	API_EventConfigAdd(EventNDM_C_CtrlResponse, "NDM_C_ResponeCallback", 0, NULL);

	#if	defined(PID_IX850)
	API_EventCallbackFunctionAdd("NDM_S_CheckEndCallback", NDM_S_CheckEndCallback, NULL);
	API_EventConfigAdd(EventNDM_S_CheckEnd, "NDM_S_CheckEndCallback", 1, NULL);

	API_EventCallbackFunctionAdd("EventXD_SaveUserCard_Callback", EventXD_SaveUserCard_Callback, NULL);	
	API_EventConfigAdd(Event_XD_SaveUserCard, "EventXD_SaveUserCard_Callback", 1, NULL);

	API_EventCallbackFunctionAdd("CertManageCallback", CertManageCallback, NULL);
	API_EventConfigAdd(EventCertManage, "CertManageCallback", 1, NULL);
	API_EventCallbackFunctionAdd("EventIperfTestReportToDxgconfig", EventIperfTestReportToDxgconfig, NULL);
	API_EventConfigAdd(Event_IPERF_TEST_REPORT, "EventIperfTestReportToDxgconfig", 1, NULL);
	API_EventCallbackFunctionAdd("EventDXGListRefreshCallback", EventDXGListRefreshCallback, NULL);
	API_EventConfigAdd(EventDXGListRefresh, "EventDXGListRefreshCallback", 1, NULL);
	API_EventCallbackFunctionAdd("EventDXGListReportCallback", EventDXGListReportCallback, NULL);
	API_EventConfigAdd(Event_IXGListUpdate, "EventDXGListReportCallback", 1, NULL);
	API_EventConfigAdd(EventMenuResCheckOnline, "EventMenuResCheckOnlineCallback", 1, NULL);
	API_EventCallbackFunctionAdd("EventMenuIpcManageCallback", EventMenuIpcManageCallback, NULL);
	API_EventConfigAdd(EventMenuIpcManage, "EventMenuIpcManageCallback", 1, NULL);
	API_EventConfigAdd(Event_IXS_ReportTbChanged, "SatHybridProgListChangeProcess", 1, NULL);
	API_EventConfigAdd(Event_IXS_ReportTbChanged, "SatHybridCheckListChangeProcess", 1, NULL);
	API_EventConfigAdd(Event_IXS_ReportTbUnchanged, "SatHybridProgListUnChangeProcess", 1, NULL);
	API_EventConfigAdd(EventMenuSatHybridProg, "EventMenuSatHybridProgCallback", 1, NULL);
	API_EventConfigAdd(EventMenuSatHybridReport, "EventMenuSatHybridReportCallback", 1, NULL);
	API_EventConfigAdd(EventDH_HybridProgOnline, "EventDH_HybridProgOnlineCallback", 1, NULL);
	API_EventConfigAdd(EventDH_HybridProgOnlineReport, "EventDH_HybridProgOnlineReportCallback", 1, NULL);
	API_EventConfigAdd(Event_IXS_ReportTbChanged, "IXS_ListUpdateForDataset", 1, NULL);
	API_EventConfigAdd(Event_MAT_Update, "RES_ListUpdateForDataset", 1, NULL);
	API_EventConfigAdd(Event_IXS_ReportTbChanged, "MenuUpdateDhImProcess", 1, NULL);
	API_EventConfigAdd(Event_IXS_ReportTbUnchanged, "MenuUpdateDhImProcess", 1, NULL);
	API_EventConfigAdd(EventMenuUpdateDhIM, "MenuUpdateDhImProcess", 1, NULL);
	API_EventConfigAdd(EventMenuAptMatReport, "EventMenuAptMatReportCallback", 1, NULL);
	API_EventConfigAdd(Event_IXS_ReportTbChanged, "AptMatCheckListChangeProcess", 1, NULL);
	API_EventConfigAdd(Event_IXS_ReportTbUnchanged, "AptMatCheckListChangeProcess", 1, NULL);
	API_EventConfigAdd(EventIM_PROG_REPORT, "ProgReport_CallBack", 1, NULL);
	API_EventConfigAdd(EventSnErrDisp,"menu_sn_eoc_err_disp", 1, NULL);
	API_EventCallbackFunctionAdd("EventMenuVmManageCallback", EventMenuVmManageCallback, NULL);
	API_EventConfigAdd(EventVmManage,"EventMenuVmManageCallback", 1, NULL);
	#endif

	API_EventConfigAdd(Event_VideoSerError, "Event_VideoSerError_Callback", 0, NULL);
	API_EventConfigAdd(Event_IPERF_TEST_START, "EventIperfTestStartCallback", 1, NULL);
	API_EventConfigAdd(Event_IPERF_TEST_REPORT, "EventIperfTestReportCallback", 1, NULL);
	API_EventConfigAdd(EventIM_PROG_REPORT, "IXS_UpdateData_CallBack", 1, NULL);
	API_EventConfigAdd(EventIM_Regist, "IM_ResgistCallBack", 1, NULL);
	
	API_EventConfigAdd(EventReqSNFromServer,"ReqSNCb", 0, NULL);
	API_EventConfigAdd(EventSetEocMacBySN,"SetEocMacCallback", 0, NULL);
	API_EventConfigAdd(EventUpdateResFromSD,"UpdateResFromSD_Process", 0, NULL);
	API_EventConfigAdd(EventCmr7610Config,"Cmr7610Config_Callback", 0, NULL);
	API_EventConfigAdd(EventBackupReq,"EventBackup_Process", 0, NULL);
	API_EventConfigAdd(EventRestoreReq,"EventRestore_Process", 1, NULL);
	
}

/*********************************************************************************************************
**  End Of File
*********************************************************************************************************/

