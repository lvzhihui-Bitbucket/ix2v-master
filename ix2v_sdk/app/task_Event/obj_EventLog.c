﻿/**
  ******************************************************************************
  * @file    obj_EventCallback.c
  * @author  czb
  * @version V00.01.00
  * @date    2022.5.17
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 

#include <dirent.h>
#include <sys/stat.h>
#include "task_Event.h"
#include "obj_PublicInformation.h"
#include "elog.h"
#include "utility.h"
#include "define_string.h"
#include "define_file.h"
#include "obj_IoInterface.h"
#include "cJSON.h"
#include "obj_NDM.h"
#include "obj_TableSurver.h"

#define IX2V_Tlog

static int tlogIp = 0;
static int tlogSuccessCnt = 0;
static int tlogFailedCnt = 0;
static int tlogConsecutiveFailedCnt = 0;

int TLogStart(void)
{
	int ret = 0;
	char temp[100];
	char* ipString = API_Para_Read_String2(TLogIP_ADDR);
	if(ipString)
	{
		tlogIp = inet_addr(ipString);
		if(tlogIp != INADDR_NONE)
		{
			tlogSuccessCnt = 0;
			tlogFailedCnt = 0;
			tlogConsecutiveFailedCnt = 0;

			API_PublicInfo_Write_String(PB_TLogState, "ON");
			API_PublicInfo_Write_String(PB_TLogIP_ADDR, ipString);
			API_PublicInfo_Write_String(PB_TLogStartTime, GetCurrentTime(temp, 100, "%Y-%m-%d %H:%M:%S"));
			API_PublicInfo_Write(PB_TLogEvent, API_Para_Read_Public(TLogEventCfg));
			API_PublicInfo_Write_Int(PB_TLogCountOfFailures, tlogFailedCnt);
			API_PublicInfo_Write_Int(PB_TLogSuccessTimes, tlogSuccessCnt);
			ret = 1;
		}
		else
		{
			tlogIp = 0;
		}
	}

	return ret;
}

int TLogStop(void)
{
	char temp[100];
	tlogIp = 0;
	API_PublicInfo_Write_String(PB_TLogState, "OFF");
	API_PublicInfo_Write_String(PB_TLogStopTime, GetCurrentTime(temp, 100, "%Y-%m-%d %H:%M:%S"));
	return 1;
}

static void TLogWriteRecord(int ip, cJSON* record)
{
	if(API_XD_EventJson(ip, record) == 0)
	{
		//发送记录失败
		tlogConsecutiveFailedCnt++;
		tlogFailedCnt++;
		API_PublicInfo_Write_Int(PB_TLogCountOfFailures, tlogFailedCnt);
	}
	else
	{
		tlogSuccessCnt++;
		tlogConsecutiveFailedCnt = 0;
		API_PublicInfo_Write_Int(PB_TLogSuccessTimes, tlogSuccessCnt);
	}
	if(tlogConsecutiveFailedCnt >= 20)
	{
		TLogStop();
	}
}

int TLogCheck(int targetIp)
{
	int ret = 0;

	if(API_Para_Read_Int(TLogEnable))
	{
		if(!tlogIp)
		{
			API_Para_Write_String(TLogIP_ADDR, my_inet_ntoa2(targetIp));
			TLogStart();
		}
		ret = 1;
	}
	else
	{
		if(tlogIp)
		{
			TLogStop();
		}
	}
	return ret;
}

int EventLog(cJSON* event)
{
	if(tlogIp)
	{
		int typeEventSize;
		int typeEventCnt;
		cJSON* typeElement;
		cJSON* eventElement;
		cJSON* eventLogCfg = API_Para_Read_Public(TLogEventCfg);
		char* eventName = GetEventItemString(event,  EVENT_KEY_EventName);
		char* cfgEventType;
		char* cfgEventName;

		if(cJSON_IsArray(eventLogCfg))
		{
			cJSON_ArrayForEach(typeElement, eventLogCfg)
			{
				if(cJSON_IsArray(typeElement))
				{
					typeEventSize = cJSON_GetArraySize(typeElement);
					if(typeEventSize > 1)
					{
						cfgEventType = cJSON_GetStringValue(cJSON_GetArrayItem(typeElement, 0));
						if(cfgEventType)
						{
							for(typeEventCnt = 1; typeEventCnt < typeEventSize; typeEventCnt++)
							{
								cfgEventName = cJSON_GetStringValue(cJSON_GetArrayItem(typeElement, typeEventCnt));
								if(cfgEventName && !strcmp(eventName, cfgEventName))
								{
									char temp[100];
									char* tempString;
									cJSON* remoteEvent = cJSON_CreateObject();
									cJSON_AddStringToObject(remoteEvent, IX2V_EventName, EventRemoteElog);
									cJSON* eventLogRecord = cJSON_AddObjectToObject(remoteEvent, IX2V_EventData);

									cJSON_AddStringToObject(eventLogRecord, IX2V_TIME, GetCurrentTime(temp, 100, "%Y-%m-%d %H:%M:%S"));
									cJSON_AddStringToObject(eventLogRecord, IX2V_MFG_SN, API_PublicInfo_Read_String(PB_MFG_SN));
									cJSON_AddStringToObject(eventLogRecord, IX2V_IX_ADDR, API_PublicInfo_Read_String(PB_IX_ADDR));
									cJSON_AddStringToObject(eventLogRecord, IX2V_TYPE, cfgEventType);
									cJSON_AddStringToObject(eventLogRecord, IX2V_EventName, cfgEventName);

									cJSON* eventData = cJSON_CreateObject();
									cJSON_ArrayForEach(eventElement, event)
									{
										if(eventElement->string && strcmp(eventElement->string, IX2V_EventName))
										{
											cJSON_AddItemToObject(eventData, eventElement->string, cJSON_Duplicate(eventElement, 1));
										}
									}
									tempString = cJSON_PrintUnformatted(eventData);
									cJSON_Delete(eventData);

									cJSON_AddStringToObject(eventLogRecord, IX2V_EventData, tempString);
									if(tempString)
									{
										free(tempString);
									}

									TLogWriteRecord(tlogIp, remoteEvent);

									cJSON_Delete(remoteEvent);
									return 1;
								}
							}
						}
					}
				}
			}
		}
	}
	return 0;
}
int ShellCmd_Tlog(cJSON *cmd)
{
	char* ctrlStr = GetShellCmdInputString(cmd, 1);

	if(!strcmp(ctrlStr, "start"))
	{
		char* ipString = GetShellCmdInputString(cmd, 2);
		if(ipString[0])
		{
			API_Para_Write_String(TLogIP_ADDR, ipString);
		}

		if(TLogStart())
		{
			ShellCmdPrintf("Tlog start success!!!\n");
		}
		else
		{
			ShellCmdPrintf("Tlog start error!!!\n");
		}
	}
	else if(!strcmp(ctrlStr, "stop"))
	{
		if(TLogStop())
		{
			ShellCmdPrintf("Tlog stop success!!!\n");
		}
		else
		{
			ShellCmdPrintf("Tlog stop error!!!\n");
		}
	}
	else if(!strcmp(ctrlStr, "test"))
	{
		char* eventName = GetShellCmdInputString(cmd, 2);
		API_Event_By_Name(eventName);
		ShellCmdPrintf("Tlog test event:%s\n", eventName);
	}
	else
	{
		ShellCmdPrintf("Tlog input error!!!\n");
	}
}

/*********************************************************************************************************
**  End Of File
*********************************************************************************************************/

