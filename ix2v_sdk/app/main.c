
#include "main.h"
#include "gui_lib.h"

#include "ak_common.h"
#include "ak_log.h" 
#include "ak_vdec.h"
#include "ak_vo.h"
#include "ak_thread.h"
#include "ak_mem.h"
#include "ak_tde.h"
#include "ota_client_ifc.h"

#include "mul_timer.h"
#include "one_akvi_task.h"
#include "task_Event.h"
#include "define_file.h"

#if defined(PID_IX850)
#include "radar_uart.h"
#endif

#if 0

int main( int argc, char* argv[] ) 
{
	char paraId[100];
	char value[100];

	    sscanf("<ID=12a3B4>", "<ID=%[^,>]%*[,>]VALUE=%[^>]", paraId, value);
    printf("paraId=%s, value=%s\n", paraId, value);
	//my_elog_init();
	//IoTest(argc, argv);
	return 0;
}

#else

void tft_power_on(int on);

int main( int argc, char* argv[] ) 
{
	thread_log_init();
	thread_log_add("main");
	API_DeleteTempFile();
	API_MakeDir();
	// initial SIGALRM
	sigset_t sigmask;
	sigemptyset(&sigmask);
	sigaddset(&sigmask,SIGALRM);
	sigprocmask(SIG_BLOCK,&sigmask,NULL);

	// initial ak sdk
    sdk_run_config config;
    config.mem_trace_flag = SDK_RUN_NORMAL;
	config.isp_tool_server_flag = 0;
    ak_sdk_init( &config );
	//ak_ats_start(8012);
    ak_print_normal_ex(MODULE_ID_VDEC, "vdec lib version: %s\n\n", ak_vdec_get_version());
	// initial gui
	#if defined( LVGL_SUPPORT_TDE )
	if( ak_tde_open() != ERROR_TYPE_NO_ERROR )
	{
		ak_print_error_ex(MODULE_ID_APP, "ak tde open error.\n");
        	return -1;
    }
	#endif
	
	//vtk_display_initial();
	//api_start_akvi_task();
#if defined(PID_IX850)
	init_radar_uart();
#endif

#if defined(PID_IX622)||defined(PID_IX821)
	init_card_uart();
#endif
#if defined(PID_IX622)||defined(PID_IX850)||defined(PID_IX611)||defined(PID_IX821)
	init_cmr7610_uart();
#endif

	start_services();
	
	init_all_timer();

#if defined(PID_IX850)||defined(PID_IX821)
	Init_vdp_uart();
	vtk_TaskInit_StackAI(0);
#endif

	//init_all_timer();	
	InitIOServer();
	GetPhMFGSnInit();
	vtk_TaskInit_menu();
	sleep(3);
	my_elog_init();
	
	TimingCheckInit();
	EOC_WorkModeInit();
	TableSurverInit();
	

	init_c5_ipc_instance();

	init_udp_io_server_instance();

	init_device_update_instance();

	
	IXS_ProxyInit();
	
	vtk_TaskInit_Ota_client_ifc();
	
	vtk_TaskInit_sundry();	

	vtk_TaskInit_survey(0);

	vtk_taskinit_audio();

	init_vdp_callserver_task();

	init_video_cs_service();
	vtk_TaskInit_vbu();
	
	vtk_TaskInit_LanManager();
	vtk_TaskInit_WiFi();
	vtk_XD_ConfigInit();
	vdp_XD2_task_init();
	
	vtk_TaskInit_Event();

	init_linphone_if_service();
	vtk_TaskInit_VtkMediaTrans(0);

#if defined(PID_IX850)||defined(PID_IX821)
	Stm8HeartBeatServiceInit();	
#else
	char buff[2] = {0};
	ReceiveStm8lSwVerFromStm8l(buff);
#endif	

	// waiting
	task_ShellInit();
	//vtk_TaskInit_menu();

#if defined(PID_IX611) || defined(PID_IX850)||defined(PID_IX821)
	KeyPad_Manage_Init(1);
#endif
#if defined(PID_IX821)
	vtk_TaskInit_MDS();
#endif
#if defined(PID_IX611) || defined(PID_IX622)|| defined(PID_IX850)||defined(PID_IX821)
	ThreadHeartbeat_Init();
#endif

	memSpaceManager(2);

	start_services_ok();
	auto_reboot_init(1);
	while(1)
	{
		ShellCmdUartInputProcess();
	}
	vtk_display_deinitial();
	return 0;
}
#endif
