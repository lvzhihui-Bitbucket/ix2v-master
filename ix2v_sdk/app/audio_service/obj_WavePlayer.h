

#ifndef _OBJ_WAVEPLAYER_h
#define _OBJ_WAVEPLAYER_h

#include"waveheader.h"

void api_wave_volume_set( int vol );
int api_wave_volume_get( void );
void api_wave_file_set( char *fileName );

int api_WavePlay( void );
int api_WaveStop( void );

int ms_read_wav_header_from_fd( wave_header_t *header, int fd );

#endif


