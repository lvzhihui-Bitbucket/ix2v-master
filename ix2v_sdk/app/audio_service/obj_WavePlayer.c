
#include "waveheader.h"
#include "alsa.h"
#include "obj_PlayList.h"
#include "RTOS.h"
#include "OSQ.h"

typedef enum
{
	AUDIO_WAVE_IDLE,
	AUDIO_WAVE_PRE,
	AUDIO_WAVE_RUN,
	AUDIO_WAVE_STOP,
	AUDIO_WAVE_STOPED,
}
AUDIO_WAVE_STATE_t;

typedef struct
{
	int fd;
	int rate;
	int nchannels;
	int hsize;
	int samplesize;
	uint16 bits;	
	BOOL is_raw;
} WAVE_DATA;

WAVE_DATA wave_data;


pthread_t task_Wave_dec;
pthread_attr_t attr_Wave_dec;
int ringVolume = 0;
char TuneFileName[100];
AUDIO_WAVE_STATE_t  AudioWaveState = AUDIO_WAVE_IDLE;
pthread_mutex_t WavePlayerApi_Lock = PTHREAD_MUTEX_INITIALIZER;	

void SetAudioWaveState(AUDIO_WAVE_STATE_t state)
{
	pthread_mutex_lock(&WavePlayerApi_Lock);
	AudioWaveState = state;
	pthread_mutex_unlock(&WavePlayerApi_Lock);
}

AUDIO_WAVE_STATE_t GetAudioWaveState(void)
{
	return AudioWaveState;
}

int WAVE_STEP_TAB[10]=
{
	0,-20,-15,-10,-5,1,5,10,15,20
};
void api_wave_volume_set( int vol )
{
	if(vol > 9)	
		vol = 9;
	ringVolume = WAVE_STEP_TAB[vol];
}

int api_wave_volume_get( void )
{
	return ringVolume;
}
void api_wave_file_set( char *fileName )
{
	strcpy(TuneFileName, fileName);
}


int ms_read_wav_header_from_fd( wave_header_t *header, int fd )
{
	int count;
	int skip;
	int hsize = 0;
	riff_t *riff_chunk 			=& header->riff_chunk;
	format_t *format_chunk	=& header->format_chunk;
	data_t *data_chunk		=& header->data_chunk;
	
	unsigned long len = 0;
	
	len = read( fd, (char*)riff_chunk, sizeof(riff_t) );
	
	if( len != sizeof(riff_t) )
	{
		goto not_a_wav;
	}
	
	if( 0 != strncmp(riff_chunk->riff, "RIFF", 4) || 0 != strncmp(riff_chunk->wave, "WAVE", 4) )
	{
		goto not_a_wav;
	}
	
	len = read( fd, (char*)format_chunk, sizeof(format_t) );
	
	if( len != sizeof(format_t) )
	{
		ms_warning("Wrong wav header: cannot read file");
		goto not_a_wav;
	}
	
	if( (skip = le_uint32(format_chunk->len) - 0x10) > 0 )
	{
		lseek( fd, skip, SEEK_CUR );
	}
	
	hsize = sizeof(wave_header_t) - 0x10 + le_uint32(format_chunk->len);
	
	count = 0;
	do{
		len = read( fd, data_chunk, sizeof(data_t) );

		if( len != sizeof(data_t) )
		{
			ms_warning("Wrong wav header: cannot read file\n");
			goto not_a_wav;
		}

		if( strncmp(data_chunk->data, "data", 4) != 0 )
		{
			ms_warning("skipping chunk=%s len=%i\n", data_chunk->data, data_chunk->len);
			lseek( fd, le_uint32(data_chunk->len), SEEK_CUR );
			count++;
			hsize += len + le_uint32(data_chunk->len);
		}
		else
		{
			hsize += len;
			break;
		}
	}while( count < 30 );

	return hsize;
	
	not_a_wav:
		/*rewind*/
		lseek( fd, 0, SEEK_SET );
		return -1;
}

static int read_wav_header( WAVE_DATA* data )
{
	wave_header_t header;
	format_t *format_chunk =& header.format_chunk;
	int ret = ms_read_wav_header_from_fd( &header, data->fd );
	
	if( ret == -1 ) 
		goto not_a_wav;
	
	data->rate		= le_uint32( format_chunk->rate );
	data->nchannels	= le_uint16( format_chunk->channel );
	
	if( data->nchannels == 0 ) 
		goto not_a_wav;
	
	data->bits = header.format_chunk.bitpspl;	//cao_20170405
	
	data->samplesize	= le_uint16( format_chunk->blockalign ) / data->nchannels;
	data->hsize		= ret;
	
	#ifdef WORDS_BIGENDIAN
	if( le_uint16(format_chunk->blockalign) == le_uint16(format_chunk->channel) * 2 )
		data->swap=TRUE;
	#endif
	
	data->is_raw = FALSE;
	
	return 0;

	not_a_wav:
	{
		/*rewind*/
		lseek( data->fd, 0, SEEK_SET );
		data->hsize 	= 0;
		data->is_raw = TRUE;
		return -1;
	}
}


static int player_open( WAVE_DATA* data, void *arg )
{
	const char *file = (const char*)arg;
	
	if( (data->fd = open(file,O_RDONLY|O_BINARY))==-1 )
	{
		ms_warning("Failed to open %s\n",file);
		return -1;
	}
	
	if( read_wav_header( data ) != 0 && strstr(file,".wav") )
	{
		ms_warning("File %s has .wav extension but wav header could be found.\n",file);
	}
	
	//ms_message("%s opened: rate=%i,channel=%i\n",file,d->rate,d->nchannels);
	printf("%s opened: rate=%i,channel=%i,d->samplesize=%i\n",file, data->rate, data->nchannels, data->samplesize);
	return 0;
}


static void AudioWaveCleanup( void *arg )	
{	
	alsa_ring_uninit( NULL );
	if(wave_data.fd != 0)				
	{
		close(wave_data.fd);
		wave_data.fd = 0;
	}
	//SetAudioWaveState(AUDIO_WAVE_IDLE);
	SetAudioWaveState(AUDIO_WAVE_STOPED);
	printf( " >>>>>>>>>AUDIO_WAVE_IDLE<<<<<<<<<< \n ");
}

static void  vtk_TaskProcessEvent_Wave( void )
{
	int hand_id;
	int size	= 0;
	int err 	= 0;
	char buff[4096];	

	wave_data.fd = 0;

	//PlayList_GetFileName( buff, TuneID );

	pthread_cleanup_push( AudioWaveCleanup, NULL );
	
	if( player_open( &wave_data, TuneFileName ) == -1 )
	{
		goto ring_thread_exit;
	}
	hand_id = alsa_ring_init( wave_data.rate, wave_data.nchannels, ringVolume );
	if( hand_id == -1 )
	{
		goto ring_thread_exit;
	}

	SetAudioWaveState(AUDIO_WAVE_RUN);
	
	while( AudioWaveState == AUDIO_WAVE_RUN)
	{
		memset(buff, 0x00, sizeof(buff));
		size = read( wave_data.fd, buff, sizeof(buff) );
		if( size == 0 )
		{
			//ak_ao_wait_play_finish(hand_id);
            ak_print_normal(MODULE_ID_AO, "\n\t read to the end of file\n");
			break;
		}
		usleep(10);
		err = alsa_ring_write( hand_id, (unsigned char*)buff, size);
		//printf("=========alsa_ring_write ret = %d====================\n", err);
		if( err < 0)
			break;	
	}
ring_thread_exit:
	pthread_cleanup_pop( 1 );	
}

int api_WavePlay( void )
{
	int ret;
	if( AudioWaveState == AUDIO_WAVE_IDLE )
	{
		SetAudioWaveState(AUDIO_WAVE_PRE);
		pthread_attr_init( &attr_Wave_dec );
		if( pthread_create(&task_Wave_dec, &attr_Wave_dec, (void*)vtk_TaskProcessEvent_Wave, NULL ) != 0 )
		{
			printf( "Create task_Audio_dec pthread error! \n" );
		}
		ret = 0;
	}
	else 
	{
		printf( "api_WavePlay State!= AUDIO_WAVE_IDLE\n" );
		ret = -1;
	}
	return ret;
}

int api_WaveStop( void )
{
	int ret;
	if( AudioWaveState == AUDIO_WAVE_IDLE )
	{
		ret = 1;
	}
	else if( AudioWaveState == AUDIO_WAVE_STOPED)
	{
		pthread_join( task_Wave_dec, NULL );
		SetAudioWaveState(AUDIO_WAVE_IDLE);
		ret = 0;	
	}
	else
	{	
		int cnt = 0;
		while( AudioWaveState != AUDIO_WAVE_RUN )
		{
			usleep(100*1000);
			if( ++cnt >= 50 )
				break;
		}
		SetAudioWaveState(AUDIO_WAVE_STOP);
		pthread_join( task_Wave_dec, NULL );
		SetAudioWaveState(AUDIO_WAVE_IDLE);
		ret = 0;		
	}
	return ret;
}
