#include "RTOS.h"
#include "cJSON.h"
#include "define_file.h"
#include "obj_Voice.h"
#include "task_Audio.h"
#include "obj_IoInterface.h"
#include "obj_WavePlayControl.h"
#include "elog.h"
#include "task_Beeper.h"

void CheckHaveCustomerizedSound()
{
#if 0
    if(IsFileExist(CustomerizedSoundDir))
    {
        cJSON *fileList = cJSON_CreateArray();
        GetFileAndDirList(CustomerizedSoundDir, fileList, 0);
        if(cJSON_GetArraySize(fileList))
        {
            API_Para_Write_String(VOICE_LANGUAGE_SELECT_DIR, CustomerizedSoundDir);
        }
        else
        {
            API_Para_Restore_Default(VOICE_LANGUAGE_SELECT_DIR);
        }
    }
    else
    {
        API_Para_Restore_Default(VOICE_LANGUAGE_SELECT_DIR);
    }
#else
	char temp[200];
	if(API_Para_Read_String(VOICE_LANGUAGE_SELECT_DIR, temp)==1)
	{
		if(strstr(temp,"/")==NULL)
		{	
			char temp1[200];
			strcpy(temp1,temp);
			sprintf(temp,"%s%s",CustomerizedPromtDir,temp1);
		}
		
		if(IsFileExist(temp))
		{
			 cJSON *fileList = cJSON_CreateArray();
			 GetFileAndDirList(temp, fileList, 0);
		        if(cJSON_GetArraySize(fileList))
		        {
		            ;//API_Para_Write_String(VOICE_LANGUAGE_SELECT_DIR, CustomerizedSoundDir);
		        }
		        else
		        {
		            API_Para_Restore_Default(VOICE_LANGUAGE_SELECT_DIR);
		        }
			//API_Para_Restore_Default(VOICE_LANGUAGE_SELECT_DIR);
			cJSON_Delete(fileList);
		}
		else
		{
	       	API_Para_Restore_Default(VOICE_LANGUAGE_SELECT_DIR);
	 	}
	}
	else
	{
	       API_Para_Restore_Default(VOICE_LANGUAGE_SELECT_DIR);
	 }
#endif
}

void VoiceParse(cJSON* json)
{
    cJSON *root = NULL;
	char eventType[50];	
    char file[100];
	char buff[200];	
	FILE *pFile;
    int volume = API_Para_Read_Int(VOICE_VOLUME_IOID);
    if(volume > 0)
    {
        GetJsonDataPro(json, "VoiceEvent", eventType);
        API_Para_Read_String(eventType, file);
	char dir_buff[120]={0};
	 API_Para_Read_String(VOICE_LANGUAGE_SELECT_DIR,dir_buff);	
	if(strstr(dir_buff,"/")==NULL)
	{	
		char temp1[200];
		strcpy(temp1,dir_buff);
		sprintf(dir_buff,"%s%s",CustomerizedPromtDir,temp1);
	}	
        snprintf(buff, 200, "%s/%s", dir_buff, file);
        if( (pFile = fopen(buff, "rb"))== NULL )
        {
        	//char *path=API_Para_Read_String2(VOICE_LANGUAGE_SELECT_DIR);
        	DIR *dir = opendir(dir_buff);  
		if(dir != NULL)
		{
			if(strstr(dir_buff,"music")!=NULL&&(strstr(file,"Cfail")!=NULL||strstr(file,"Sbusy")!=NULL))
				BEEP_ERROR();
			else if(strstr(dir_buff,"music")!=NULL&&(strstr(file,"Unlock")!=NULL))
				BEEP_CONFIRM();
			closedir(dir);
			return;
		}
            snprintf(buff, 100, "%s/%s", RING_Folder, file);
        }
        else
        {
            fclose(pFile);
        }
        root = cJSON_CreateObject();
        cJSON_AddStringToObject(root, "RingFile", buff);
        cJSON_AddNumberToObject(root, "RingVol", volume);
        cJSON_AddNumberToObject(root, "RingCount", 1);
        
        //WaveCtrl_Start(root);
        API_RingTunePlay(root);
        cJSON_Delete(root);
    }
}

/*
json参数：
 "VoiceEvent":"Event_CALL_SUCC"
 */
int API_VoicePlay(cJSON* json)
{
    VoiceParse(json);
    #if 0
    AUDIO_JSON_STRUCT* msgVoice;
    int msgLen;
    char *string=NULL;
    string=cJSON_Print(json);
	if(string==NULL)
		return -1;

    msgVoice=(AUDIO_JSON_STRUCT *)malloc(sizeof(VDP_MSG_HEAD)+strlen(string)+1);

	msgVoice->head.msg_source_id    = 0;
	msgVoice->head.msg_target_id    = MSG_ID_Audio;
	msgVoice->head.msg_type 	    = MSG_TYPE_VOICE;
	msgVoice->head.msg_sub_type 	= 0;
    strcpy(msgVoice->json_str, string);

    msgLen = sizeof(VDP_MSG_HEAD)+strlen(string)+1;
	push_vdp_common_queue(&vdp_audio_mesg_queue, (char*)msgVoice, msgLen);
    free(string);
    free(msgVoice);
    #endif
    return 0;
}

int Event_VoicePlay(cJSON *cmd)
{
	VoiceParse(cmd);

	return 1;
}
#if defined(PID_IX611) || defined(PID_IX622) ||defined(PID_IX821)		//#ifdef PID_IX611
#define VOICE_LANGUAGE_MAX_CNT	20
typedef struct
{
	int		cnt;	
	char	name[VOICE_LANGUAGE_MAX_CNT][100];
	char	displayName[VOICE_LANGUAGE_MAX_CNT][30];
} VOICE_LANGUAGE_LIST_T;

VOICE_LANGUAGE_LIST_T voiceDir={0};


void VoiceLanguageListInit(void)
{
	voiceDir.cnt = 0;
	
	GetVoiceDirName("/mnt/sdcard/Customerized/prompt/", &voiceDir);
	if(voiceDir.cnt == 0)
	{
		GetVoiceDirName("/mnt/nand1-2/Customerized/prompt/", &voiceDir);

		if(voiceDir.cnt == 0)
		{
			GetVoiceDirName("/mnt/nand1-2/rings/", &voiceDir);
		}
	}

}


int GetVoiceDirName(const char * dir, VOICE_LANGUAGE_LIST_T *pVoiceDir)
{
	char cmd[100];
	char linestr[100];
	int ret = 0;
	char *backupDir;
	char *p;
	int i;
	
	snprintf(cmd, 100, "ls -l %s | grep ^d | awk '{print $9}'", dir);
	
	FILE *pf = popen(cmd,"r");
	if(pf == NULL)
	{
		return -1;
	}

	while(fgets(linestr,100,pf) != NULL)
	{
		for(i = 0; linestr[i] != 0 && i < 100; i++)
		{
			if(linestr[i] == '\r' || linestr[i] == '\n')
			{
				linestr[i] = 0;
			}
		}
		
		if(strlen(linestr) == 0)
		{
			continue;
		}
		
		snprintf(pVoiceDir->name[pVoiceDir->cnt++], 100, "%s%s", dir, linestr);
		
		snprintf(pVoiceDir->displayName[pVoiceDir->cnt-1], 30, "%s", linestr);
		
		if(pVoiceDir->cnt > 0)
		{
			printf("name[%d] = %s\n", pVoiceDir->cnt-1, pVoiceDir->name[pVoiceDir->cnt-1]);
		}

		if(pVoiceDir->cnt >= VOICE_LANGUAGE_MAX_CNT)
		{
			break;
		}
	}
	
	pclose(pf);
	
	return ret;
}

int GetVoiceLanguageListCnt(void)
{
	return voiceDir.cnt;
}
int GetVoiceLanguageInfo(int index,char *path,char *disp_name)
{
	if(index<voiceDir.cnt)
	{
		if(path)
			strcpy(path,voiceDir.name[index]);
		if(disp_name)
			strcpy(disp_name,voiceDir.displayName[index]);
		return 0;
	}
	return -1;
}

#endif