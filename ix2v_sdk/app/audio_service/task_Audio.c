
#include "cJSON.h"
#include "task_Audio.h"
#include "obj_Ring.h"
#include "define_file.h"
#include "obj_WavePlayControl.h"

Loop_vdp_common_buffer	vdp_audio_mesg_queue;
Loop_vdp_common_buffer	vdp_audio_sync_queue;
vdp_task_t				task_audio;

void vdp_audio_mesg_data_process(char* msg_data, int len);
void* vdp_audio_task( void* arg );


void vtk_taskinit_audio(int priority)
{
	init_vdp_common_queue(&vdp_audio_mesg_queue, 1000, (msg_process)vdp_audio_mesg_data_process, &task_audio);
	init_vdp_common_queue(&vdp_audio_sync_queue, 100, NULL, &task_audio);
	init_vdp_common_task(&task_audio, MSG_ID_Audio, vdp_audio_task, &vdp_audio_mesg_queue, &vdp_audio_sync_queue);

	WaveCtrl_Init();
	task_audio.task_StartCompleted = 1;
	printf("======vtk_taskinit_audio ok===========\n");	
}

void exit_vdp_audio_task(void)
{
	exit_vdp_common_queue(&vdp_audio_mesg_queue);
	exit_vdp_common_queue(&vdp_audio_sync_queue);
	exit_vdp_common_task(&task_audio);	
}

void* vdp_audio_task( void* arg )
{
	vdp_task_t*	 ptask 		= (vdp_task_t*)arg;
	p_vdp_common_buffer pdb 	= 0;
	int	size;
	thread_log_add("%s",__func__);
	while( ptask->task_run_flag )
	{
		size = pop_vdp_common_queue( ptask->p_msg_buf, &pdb, VDP_QUEUE_POLLING_TIME);
		if( size > 0 )
		{
			(*ptask->p_msg_buf->process)(pdb,size);
			purge_vdp_common_queue( ptask->p_msg_buf );
		}
	}
	return 0;
}

void vdp_audio_mesg_data_process(char* msg_data,int len)
{
	VDP_MSG_HEAD* pMsgaudio = (VDP_MSG_HEAD*)msg_data;
	WAVEPLAY_STRUCT*  pMsgWave	= (WAVEPLAY_STRUCT*)msg_data;
	AUDIO_JSON_STRUCT*  pMsg	= (AUDIO_JSON_STRUCT*)msg_data;
	cJSON *root=NULL;

	switch (pMsgaudio->msg_type)
	{
		case MSG_TYPE_RING:
			if(pMsgaudio->msg_sub_type == RING_PLAY)
			{
				printf("RingParse:%s\n",pMsg->json_str);
				root=cJSON_Parse(pMsg->json_str);
				if(root!=NULL)
				{
					printf("RingParse:22222222222\n");
					RingParse(root);
					cJSON_Delete(root);
				}	
				else
				{
					dprintf("Json error!!! : %s\n", pMsg->json_str);
				}			
			}
			else if(pMsgaudio->msg_sub_type == RING_SET_TUNE)
			{
				root=cJSON_Parse(pMsg->json_str);
				if(root!=NULL)
				{
					WaveCtrl_Start(root);
					cJSON_Delete(root);
				}	
			}
			else if(pMsgaudio->msg_sub_type == RING_STOP)
			{
				WaveCtrl_Stop();
			}
			else if(pMsgaudio->msg_sub_type == RING_SET_VOLUME)
			{
				RingVolSet(pMsgWave->cmd);
			}
			break;
		case MSG_TYPE_VOICE:
			root=cJSON_Parse(pMsg->json_str);
			if(root!=NULL)
			{
				VoiceParse(root);
				cJSON_Delete(root);
			}	
			break;
		case MSG_TYPE_WAVE:
			if(WaveGetState() == WAVE_STATE_PLAY)
			{
				if(pMsgWave->cmd == WAVE_TIME_CYCLE)
				{
					Wave_Time_Cycle();
				}
				else if(pMsgWave->cmd == WAVE_CNT_CYCLE)
				{
					Wave_Count_Cycle();
				}
			}
			
			break;
	}
}




#if 1
typedef enum
{
	RING_DS,
	RING_CDS,
	RING_OS,
	RING_INTERCOM,
	RING_INNERCALL,
	RING_MSG,
	RING_DOORBELL,
	RING_ALARM,
    VOICE_CALL_FAIL_INEXISTENT,	
    VOICE_CALL_FAIL_DND,		
    VOICE_CALL_SUCC,			
    VOICE_CALL_TALK,			
    VOICE_CALL_CLOSE,			
    VOICE_SYS_BUSY,				
    VOICE_UNLOCK_COMMAND,		
    VOICE_UNLOCK_BUTTON,		
    VOICE_VOLUME_ADJUST,		
}Voice_Ring_Type;
typedef struct
{
    Voice_Ring_Type    type;				// 类型
	const char*		   io_id;				// 参数编码
	const char*		   addr;				// DeviceAddr
}AUDIO_TEST_RUN;

const AUDIO_TEST_RUN TAB_AUDIO_TEST[] = 
{
	{RING_DS,		"RING_CALL",			"0099000001"  },	
	{RING_CDS,		"RING_CALL",			"0000000001"  },	
	{RING_OS,		"RING_CALL",			"0099000051"  },	
	{RING_INTERCOM,	"RING_CALL",			"0099000201"  },	
 	{RING_INNERCALL,"RING_CALL",			"0099000101"  },	
	{RING_MSG,		"RING_MSG",				NULL  },	
	{RING_DOORBELL,	"RING_DOORBELL",		NULL  },	
	{RING_ALARM,	"RING_ALARM",			NULL  },	
	{VOICE_CALL_FAIL_INEXISTENT,	"Event_CALL_FAIL_INEXISTENT",			NULL  },	
	{VOICE_CALL_FAIL_DND,			"Event_CALL_FAIL_DND",					NULL  },	
	{VOICE_CALL_SUCC,				"Event_CALL_SUCC",						NULL  },	
	{VOICE_CALL_TALK,				"Event_CALL_TALK",					NULL  },	
	{VOICE_CALL_CLOSE,				"Event_CALL_CLOSE",					NULL  },	
	{VOICE_SYS_BUSY,				"Event_SYS_BUSY",					NULL  },	
	{VOICE_UNLOCK_COMMAND,			"Event_UNLOCK",						NULL  },	
	{VOICE_UNLOCK_BUTTON,			"Event_UNLOCK",						NULL  },	
	{VOICE_VOLUME_ADJUST,			"Event_VOLUME_ADJUST",				NULL  },	
};
const uint8 TAB_AUDIO_TEST_NUM = sizeof(TAB_AUDIO_TEST)/sizeof(TAB_AUDIO_TEST[0]);
OS_TIMER timer_audiotest;
Voice_Ring_Type typeTest;

void AudioTestProcess(int type)
{
    cJSON *root = NULL;
	root = cJSON_CreateObject();
	if(type <= RING_ALARM)
	{
		cJSON_AddStringToObject(root, "RingType", TAB_AUDIO_TEST[type].io_id);
		cJSON_AddStringToObject(root, "Call_DeviceAddr", TAB_AUDIO_TEST[type].addr);
		RingParse(root);		 
	}	
	else if(type <= VOICE_VOLUME_ADJUST)
	{
		cJSON_AddStringToObject(root, "VoiceEvent", TAB_AUDIO_TEST[type].io_id);
		VoiceParse(root);
	}		
	cJSON_Delete(root);
		
}
void Callback_AudioTest(void)
{
	if(WaveGetState() == WAVE_STATE_STOP)
	{
		typeTest++;
		if(typeTest > VOICE_VOLUME_ADJUST)
		{
			OS_StopTimer( &timer_audiotest);
			return;
		}		
		AudioTestProcess(typeTest);
	}
	OS_RetriggerTimer( &timer_audiotest );
}
#endif

void test_audio_start(int type)
{
	printf("======test_audio_start ===========\n");	
	#if 1
	if(type == -1)
	{
		OS_CreateTimer(&timer_audiotest, Callback_AudioTest, 1000/25);
		typeTest = RING_DS;
		AudioTestProcess(typeTest);
		OS_RetriggerTimer( &timer_audiotest );
	}
	else
	{
		AudioTestProcess(type);
	}
    
	#endif
}