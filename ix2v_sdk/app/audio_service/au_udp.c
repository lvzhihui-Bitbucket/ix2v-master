#include <stdio.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/timeb.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <sys/ioctl.h>
#include <sys/ipc.h>
#include <sys/socket.h>
#include <sys/resource.h>
#include <sys/time.h>
#include <sys/types.h>
#include <errno.h>
#include <linux/fb.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/un.h>
#include <signal.h>
#include <dirent.h>
#include <ortp/ortp.h>

#include "au_udp.h"
#include "elog_forcall.h"
#include "vtk_udp_stack_class.h"

#include "cJSON.h"

#define AU_UDP_OBJECT_IDLE         0
#define AU_UDP_OBJECT_START        1
#define AU_UDP_OBJECT_RUN          2   
#define AU_UDP_OBJECT_STOP         3   

#pragma pack(2)

typedef struct
{
	char 		    magic[6];
	//
	unsigned short DataType;          //数据类型
	unsigned short FrameNo;           //帧序�?
	unsigned int   Timestamp;         //时间�?
	unsigned int   Framelen;          //帧数据长�?
	unsigned short TotalPackage;      //总包�?
	unsigned short CurrPackage;       //当前包数
	unsigned short Datalen;           //数据长度
	unsigned short PackLen;           //数据包大�?
} UDP_AUDIO_HEAD;

#pragma pack()

#define UDP_AUDIO_PACK_HEAD_LEN			sizeof(UDP_AUDIO_HEAD)
#define UDP_AUDIO_PACK_TOTAL_LEN		(UDP_AUDIO_PACK_HEAD_LEN+UDP_AUDIO_G711_DATA_LEN)

typedef struct
{
    int 	        state;      // running state
    queue_t 		i_buff;     // playback input buffer
    int			    i_size;     // playback input buffer size
    pthread_mutex_t i_lock;     // playback input buffer lock
    pthread_t	    tid;        // capture thread

	int             s_sock;
	int             s_ip;
	int             s_port;
    struct sockaddr_in s_addr;

	int             r_sock;
	int             r_ip;
	int             r_port;
    struct sockaddr_in r_addr;
	int 		mul_flag;

    UDP_AUDIO_HEAD  au_head;

    cb_io           input;
    cb_io           output;

    unsigned char*  precbuf;
    int             poffset;    
}AU_UDP_OBJECT;

AU_UDP_OBJECT  one_au_udp_ins={0};

static int create_one_udp_pack( const unsigned char* psrc, int len, unsigned char* ptar )
{
    one_au_udp_ins.au_head.CurrPackage  = 1;
    one_au_udp_ins.au_head.Datalen      = len;
    one_au_udp_ins.au_head.DataType     = 1;
    one_au_udp_ins.au_head.Framelen     = len;
    one_au_udp_ins.au_head.PackLen      = 1200;
    one_au_udp_ins.au_head.TotalPackage = 1;
    one_au_udp_ins.au_head.Timestamp    = 0; //timeGetTime();

    // copy pack head
    memcpy( ptar,(unsigned char*)&one_au_udp_ins.au_head, UDP_AUDIO_PACK_HEAD_LEN);
    one_au_udp_ins.au_head.FrameNo++;
    if( one_au_udp_ins.au_head.FrameNo >= 65535 ) one_au_udp_ins.au_head.FrameNo = 1;
    memcpy( ptar+UDP_AUDIO_PACK_HEAD_LEN, psrc, len);
    return UDP_AUDIO_PACK_HEAD_LEN+len;
}

int au_udp_input( const unsigned char* pbuff, int len )
{
    int pack_len;
    int i,total_len,total_blk,total_rem;
	unsigned char pcmbuf[UDP_AUDIO_PACK_TOTAL_LEN];

    if( one_au_udp_ins.precbuf == NULL )
    {
        one_au_udp_ins.precbuf = malloc((len+UDP_AUDIO_G711_DATA_LEN)*2);
        one_au_udp_ins.poffset = 0;
    }
    memcpy(one_au_udp_ins.precbuf+one_au_udp_ins.poffset, pbuff, len);

    total_len = one_au_udp_ins.poffset + len;
    total_blk = total_len/UDP_AUDIO_G711_DATA_LEN;
    total_rem = total_len%UDP_AUDIO_G711_DATA_LEN;

	if( one_au_udp_ins.state == AU_UDP_OBJECT_RUN )
	{
        //printf("au_udp_send pack, total_len=%d, total_blk=%d, total_rem=%d\n", total_len,total_blk,total_rem);
        for( i = 0; i < total_blk; i++ )
        {
            pack_len = create_one_udp_pack(one_au_udp_ins.precbuf+i*UDP_AUDIO_G711_DATA_LEN, UDP_AUDIO_G711_DATA_LEN, pcmbuf);
            // send package
            //if(one_au_udp_ins.mul_flag!=0&&one_au_udp_ins.s_addr.sin_addr.s_addr!=0)
            {
	            if( sendto( one_au_udp_ins.s_sock, pcmbuf, pack_len, 0, &one_au_udp_ins.s_addr, sizeof(one_au_udp_ins.s_addr) ) == -1 )
	            {
	                printf("can not send data from socket! errno:%d,means:%s\n",errno,strerror(errno));
	            }
            }
		    listen_subscriber_list_send(pcmbuf, pack_len);
		

			//PrintCurrentTime(pack_len);
        }
        if( total_rem )
        {
            memcpy(one_au_udp_ins.precbuf, one_au_udp_ins.precbuf+i*UDP_AUDIO_G711_DATA_LEN, total_rem );
        }
        one_au_udp_ins.poffset = total_rem;
        return 0;
    }
    else
        return -1;
}

static void* au_udp_process_clean( void* arg )
{	
	printf("%s\n",__FUNCTION__);

}

static void* au_udp_processing( void* arg )
{
    mblk_t *im;	
    AU_UDP_OBJECT* pins;
    pins = (AU_UDP_OBJECT*)arg;

	unsigned int addr_len = sizeof( pins->r_addr );

	unsigned char udpbuffer[UDP_AUDIO_G711_DATA_LEN*10];
	int data_size;
	int	i,total_block;

	pthread_cleanup_push( au_udp_process_clean, arg );

    rtp_process_set_high_prio();

    pins->state = AU_UDP_OBJECT_RUN;
	
	printf( "au_udp_processing\n");
	log_d("au udp thread start");
	//while( pins->state == AU_UDP_OBJECT_RUN )
	while( 1 )
	{
		data_size = recvfrom( pins->r_sock, udpbuffer, sizeof(udpbuffer), 0, (struct sockaddr*)&pins->r_addr, &addr_len );
		//printf("...recvfrom[%d]...\n",data_size);
        if( data_size > 0 )
        {
            total_block = data_size/UDP_AUDIO_PACK_TOTAL_LEN;
            for( i = 0; i < total_block; i++ )
            {
                if( pins->output != NULL )
                {
                    (*pins->output)(udpbuffer+i*UDP_AUDIO_PACK_TOTAL_LEN+UDP_AUDIO_PACK_HEAD_LEN,UDP_AUDIO_G711_DATA_LEN);
                }
                #if 0
                im = allocb(UDP_AUDIO_G711_DATA_LEN, 0);
                memcpy( im->b_rptr, udpbuffer+i*UDP_AUDIO_PACK_TOTAL_LEN+UDP_AUDIO_PACK_HEAD_LEN, UDP_AUDIO_G711_DATA_LEN );
                im->b_wptr += UDP_AUDIO_G711_DATA_LEN;
                pthread_mutex_lock( &pins->i_lock );
                putq(&pins->i_buff,im);      
                pins->i_size++;
                pthread_mutex_unlock( &pins->i_lock );
                #endif
            }
        }
        //usleep(1000);
 	}
	pthread_cleanup_pop( 0 );
}

/********************************************************************************
@function:
	au_udp_init:    audio udp transfer initial before starting
@parameters:
	s_ip:	        send ip address
	s_port:	        send ip port
	r_ip:	        recv ip address
	r_port:	        recv ip port
@return:
	0/ok, -1/error
********************************************************************************/
int au_udp_init( unsigned int s_ip, unsigned short s_port, unsigned int r_ip, unsigned short r_port,int mul_flag)
{
    one_au_udp_ins.state    = AU_UDP_OBJECT_IDLE;

    one_au_udp_ins.s_ip     = s_ip;
    one_au_udp_ins.s_port   = s_port;
    one_au_udp_ins.r_ip     = r_ip;
    one_au_udp_ins.r_port   = r_port;
	one_au_udp_ins.mul_flag = mul_flag;
    printf("au_udp_start: s_ip[%08x:%d],r_ip[%08x:%d]\n",s_ip,s_port,r_ip,r_port);

    // initial send socket
    //if( one_au_udp_ins.s_ip != 0 && one_au_udp_ins.s_port != 0 )
	if( one_au_udp_ins.s_port != 0 )	
    {
        if( ( one_au_udp_ins.s_sock = socket( PF_INET, SOCK_DGRAM, 0 ) ) == -1 )
        {
            printf("creating socket failed!");
            return -1;
        }
    }
    bzero( &one_au_udp_ins.s_addr, sizeof(one_au_udp_ins.s_addr) );
    one_au_udp_ins.s_addr.sin_family 		= AF_INET;
    one_au_udp_ins.s_addr.sin_addr.s_addr	= one_au_udp_ins.s_ip;
    one_au_udp_ins.s_addr.sin_port		    = one_au_udp_ins.s_port;

    // initial recv socket
    if( ( one_au_udp_ins.r_sock = socket( PF_INET, SOCK_DGRAM, 0 ) ) == -1 )
    {
        printf("creating socket failed!");
        close(one_au_udp_ins.s_sock);
        return -1;
    }
    bzero( &one_au_udp_ins.r_addr, sizeof(one_au_udp_ins.r_addr) );
    one_au_udp_ins.r_addr.sin_family 		= AF_INET;
    one_au_udp_ins.r_addr.sin_addr.s_addr	= htonl(INADDR_ANY);
    one_au_udp_ins.r_addr.sin_port		    = one_au_udp_ins.s_port;		
    if( bind(one_au_udp_ins.r_sock, (struct sockaddr*)&one_au_udp_ins.r_addr, sizeof(one_au_udp_ins.r_addr)) != 0 )
    {
        printf("bind socket failed!");
        close(one_au_udp_ins.s_sock);
        close(one_au_udp_ins.r_sock);
        return -1;
    }
	if( one_au_udp_ins.r_sock )	
		join_multicast_group2(one_au_udp_ins.r_sock==1?NET_ETH0:NET_WLAN0,one_au_udp_ins.r_sock, one_au_udp_ins.s_ip);
    printf( "bind one_au_udp_ins r_sock prot=%d\n",one_au_udp_ins.r_addr.sin_port);

    // initial package head
    one_au_udp_ins.au_head.DataType	    = 0x0001;
    one_au_udp_ins.au_head.FrameNo		= 0x0001;
    memcpy( one_au_udp_ins.au_head.magic, "SWITCH", 6 );

    // initial buffer
    qinit(&one_au_udp_ins.i_buff);
    one_au_udp_ins.i_size = 0;
    pthread_mutex_init( &one_au_udp_ins.i_lock, 0);

    one_au_udp_ins.input    = au_udp_input;
    printf(">>>>>>>>>>>>>>>>>>>>>>>>au_udp_init,input[%08x]>>>>>>>>>>>>>>>>>>\n",one_au_udp_ins.input);

    one_au_udp_ins.output   = NULL;

    one_au_udp_ins.precbuf  = NULL;
    return 0;
}

/********************************************************************************
@function:
	au_udp_start:   start audio udp transfer
@parameters:
    none
@return:
	0/ok, -1/error
********************************************************************************/
int au_udp_start( void )
{
	if( one_au_udp_ins.state == AU_UDP_OBJECT_IDLE )
    {
        one_au_udp_ins.state    = AU_UDP_OBJECT_START;

        pthread_create(&one_au_udp_ins.tid, NULL, au_udp_processing, (void*)&one_au_udp_ins);

        return 0;
    }
    else
	    return -1;
}

/********************************************************************************
@function:
	au_udp_stop:    stop audio udp transfer
@parameters:
    none
@return:
	0/ok, -1/error
********************************************************************************/
int au_udp_stop( void )
{
    if( one_au_udp_ins.state == AU_UDP_OBJECT_IDLE )
        return -1;

    int wait_cnt = 0;
    while( one_au_udp_ins.state != AU_UDP_OBJECT_RUN )
    {
        usleep(100*1000);
        if( ++wait_cnt > 50 )
        {
            one_au_udp_ins.state = AU_UDP_OBJECT_IDLE;
            return -1;
        }
    }
    one_au_udp_ins.state   = AU_UDP_OBJECT_STOP;
	usleep(100*1000);

    printf("%s start\n",__FUNCTION__);
    pthread_cancel( one_au_udp_ins.tid ) ;
    pthread_join( one_au_udp_ins.tid, NULL );
    printf("%s end\n",__FUNCTION__);
    
    flushq(&one_au_udp_ins.i_buff,0);

    close(one_au_udp_ins.s_sock);
    close(one_au_udp_ins.r_sock);

    if( one_au_udp_ins.precbuf != NULL )
    {
        free(one_au_udp_ins.precbuf);
        one_au_udp_ins.precbuf = NULL;
    }

    one_au_udp_ins.state   = AU_UDP_OBJECT_IDLE;
	log_d("au udp thread stop");
    return 0;
}

/********************************************************************************
@function:
	au_udp_set_output:  set audio udp output callback pointer
@parameters:
    output:     output callback pointer
@return:
	0/ok, -1/error
********************************************************************************/
int au_udp_set_output( cb_io output )
{
    one_au_udp_ins.output = output;
    printf(">>>>>>>>>>>>>>>>>>>>>>>>au_udp_set_output[%08x]>>>>>>>>>>>>>>>>>>\n",one_au_udp_ins.output);
    return 0;
}

/********************************************************************************
@function:
	au_udp_get_input:   get audio udp input callback pointer
@parameters:
    none
@return:
	cb_io type callback pointer
********************************************************************************/
cb_io au_udp_get_input( void )
{
    return one_au_udp_ins.input;
}

/********************************************************************************
@function:
	au_udp_pop: fetch udp received packet in buffer
@parameters:
    pbuff:      output buffer pointer
    limit:      output buffer size
@return:
	-1/error, x/ data length
********************************************************************************/
int au_udp_pop( unsigned char* pbuff, int limit )
{
    int len=0;
    mblk_t *im;
	if( one_au_udp_ins.state == AU_UDP_OBJECT_RUN )
	{
        pthread_mutex_lock( &one_au_udp_ins.i_lock );
        if( (im = getq(&one_au_udp_ins.i_buff)) != NULL ) 
        {
            pthread_mutex_unlock( &one_au_udp_ins.i_lock );
            len = im->b_wptr-im->b_rptr;
            len = (len<limit)?len:limit;
            memcpy(pbuff,im->b_rptr,len);
			freemsg (im);
            one_au_udp_ins.i_size--;
        }
        else
            pthread_mutex_unlock( &one_au_udp_ins.i_lock );
        return len;
    }
    else
        return -1;
}

cJSON *get_talk_udp_state(void)
{
	cJSON *state;
	char buff[100];
	state=cJSON_CreateObject();
	if(state!=NULL)
	{
		if(one_au_udp_ins.state==AU_UDP_OBJECT_RUN)
		{
			cJSON_AddStringToObject(state,"STATE","RUN");
			my_inet_ntoa(one_au_udp_ins.s_addr.sin_addr.s_addr,buff);
			sprintf(buff+strlen(buff),":%d",ntohs(one_au_udp_ins.s_addr.sin_port) );
			cJSON_AddStringToObject(state,"ADDR",buff);
		}
		else if(one_au_udp_ins.state==AU_UDP_OBJECT_IDLE)
		{
			cJSON_AddStringToObject(state,"STATE","IDLE");
		}
		else if(one_au_udp_ins.state==AU_UDP_OBJECT_START)
		{
			cJSON_AddStringToObject(state,"STATE","START");
		}
		else if(one_au_udp_ins.state==AU_UDP_OBJECT_STOP)
		{
			cJSON_AddStringToObject(state,"STATE","STOP");
		}
		else
		{
			cJSON_AddStringToObject(state,"STATE","UNKOWN");
		}
		
	}
	return state;
}
	
