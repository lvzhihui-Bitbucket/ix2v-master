

#ifndef _OBJ_RING_H_
#define _OBJ_RING_H_

#include "cJSON.h"

#define RING_STOP           0
#define RING_PLAY           1
#define RING_SET_TUNE       2
#define RING_SET_VOLUME     3

int API_RingPlay(cJSON* json);
int API_RingPlay2(const char* ringType, const char* ixAddr);
int API_RingTunePlay(cJSON* json);
int API_RingVolPlay(unsigned char iVol);
int API_RingStop(void);

#endif


