
#ifndef TASK_AUDIO_H
#define TASK_AUDIO_H

#include "task_survey.h"

#define AUDIO_JSON_DATA_LEN_MAX				(300)

#define MSG_TYPE_TALK  		    0
#define MSG_TYPE_RING     		1
#define MSG_TYPE_VOICE   		2
#define MSG_TYPE_WAVE     		3

#define WAVE_TIME_CYCLE         0
#define WAVE_CNT_CYCLE          1

typedef struct
{
    VDP_MSG_HEAD head;
	uint8 cmd;
} WAVEPLAY_STRUCT ;

typedef struct
{
    VDP_MSG_HEAD 	head;
	int	  			json_len;
	char            json_str[];
} AUDIO_JSON_STRUCT ;

extern Loop_vdp_common_buffer	vdp_audio_mesg_queue;
extern Loop_vdp_common_buffer	vdp_audio_sync_queue;

void vtk_TaskInit_Audio(int priority);



#endif
