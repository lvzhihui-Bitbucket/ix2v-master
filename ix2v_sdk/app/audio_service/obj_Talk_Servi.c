#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "obj_Talk_Servi.h"
#include "IP_Audio_Control.h"
#include "elog_forcall.h"
#include "obj_IoInterface.h"

TalkService_JSON talkServiceIns;
#if 0
int Get_TalkService_Mic_Vol(void)
{
	return talkServiceIns.mic_vol;
}
int Get_TalkService_Mic_Gain(void)
{
	return talkServiceIns.mic_gain;
}
int Get_TalkService_Spk_Vol(void)
{
	return talkServiceIns.spk_vol;
}
int Get_TalkService_Spk_Gain(void)
{
	return talkServiceIns.spk_gain;
}

int Get_TalkService_Mic_Vol_app(void)
{
	return talkServiceIns.mic_vol_app;
}
int Get_TalkService_Mic_Gain_app(void)
{
	return talkServiceIns.mic_gain_app;
}
int Get_TalkService_Spk_Vol_app(void)
{
	return talkServiceIns.spk_vol_app;
}
int Get_TalkService_Spk_Gain_app(void)
{
	return talkServiceIns.spk_gain_app;
}
#endif
int Get_TalkService_Mic_Vol_phy(void)
{
	return talkServiceIns.mic_vol_phy;
}
int Get_TalkService_Mic_Gain_phy(void)
{
	return talkServiceIns.mic_gain_phy;
}
int Get_TalkService_Spk_Vol_phy(void)
{
	return talkServiceIns.spk_vol_phy;
}
int Get_TalkService_Spk_Gain_phy(void)
{
	return talkServiceIns.spk_gain_phy;
}

int API_TalkService_on(cJSON* json)
{
	char temp[100];
	char buff[50];
	char *ptr;
	char *io_para_pre=NULL;
	cJSON *io_vol_para;
	int phone_vol_para=0;
	GetJsonDataPro(json, TalkType, temp);
	if(strcmp(temp,TalkType_LocalAndDt)==0)
	{
		talkServiceIns.talktype=LocalAndDt;
		return 0;
	}
	else if(strcmp(temp,TalkType_LocalAndUdp)==0)
	{
		talkServiceIns.talktype=LocalAndUdp;
	}
	else if(strcmp(temp,TalkType_LocalAndRtp)==0)
	{
		talkServiceIns.talktype=LocalAndRtp;
	}
	else if(strcmp(temp,TalkType_DtAndUdp)==0)
	{
		talkServiceIns.talktype=DtAndUdp;
	}
	else if(strcmp(temp,TalkType_DtAndRtp)==0)
	{
		talkServiceIns.talktype=DtAndRtp;
	}
	else if(strcmp(temp,TalkType_UdpAndRtp)==0)
	{
		talkServiceIns.talktype=UdpAndRtp;
	}
	else
	{
		log_d("Unkown Talk Type");
		return -1;
	}
	if(GetJsonDataPro(json, TalkVolPara, temp))
	{
		if(strcmp(temp,AU_PHONE)==0)
			phone_vol_para=1;
	}
	
	io_vol_para=NULL;
	if(talkServiceIns.talktype==LocalAndRtp||phone_vol_para)
	{
		io_vol_para=API_Para_Read_Public(AU_PHONE);
		io_para_pre=AU_PHONE;
	}
		
	if(io_vol_para==NULL)
	{
		if(API_Para_Read_Int(AU_VOL_SPEC))
		{
			io_vol_para=API_Para_Read_Public(AU_SPEC);
			io_para_pre=AU_SPEC;
		}
		else
		{

			int sel=API_Para_Read_Int(AU_VOL_NORM);
			io_para_pre=NULL;
			API_Para_Read_Int(AU_VOL_NORM);
			io_vol_para=API_Para_Read_Public(AU_NORM);	
			if(sel>=cJSON_GetArraySize(io_vol_para))
				sel=cJSON_GetArraySize(io_vol_para)-1;
			io_vol_para=cJSON_GetArrayItem(io_vol_para,sel);
		}
	}
	if(GetJsonDataPro(json, MIC_VOLUME_PHY, &talkServiceIns.mic_vol_phy)==0)
	{
		if(io_para_pre!=NULL)
			sprintf(temp,"%s.%s",io_para_pre,MIC_VOLUME_PHY);
		else
			sprintf(temp,"%s",MIC_VOLUME_PHY);
		if(GetJsonDataPro(io_vol_para, temp, &talkServiceIns.mic_vol_phy)==0)
			talkServiceIns.mic_vol_phy=1;	
	}
	else
	{
		if(talkServiceIns.mic_vol_phy<-90||talkServiceIns.mic_vol_phy>20)
			talkServiceIns.mic_vol_phy=-999;
	}
	
	if(GetJsonDataPro(json, MIC_GAIN_PHY, &talkServiceIns.mic_gain_phy)==0)
	{
		if(io_para_pre!=NULL)
			sprintf(temp,"%s.%s",io_para_pre,MIC_GAIN_PHY);
		else
			sprintf(temp,"%s",MIC_GAIN_PHY);
		if(GetJsonDataPro(io_vol_para, temp, &talkServiceIns.mic_gain_phy)==0)
			talkServiceIns.mic_gain_phy=0;	
	}
	else
	{
		if(talkServiceIns.mic_gain_phy<0||talkServiceIns.mic_gain_phy>8)
			talkServiceIns.mic_gain_phy=-999;
	}
	if(GetJsonDataPro(json, SPK_VOLUME_PHY, &talkServiceIns.spk_vol_phy)==0)
	{
		if(io_para_pre!=NULL)
			sprintf(temp,"%s.%s",io_para_pre,SPK_VOLUME_PHY);
		else
			sprintf(temp,"%s",SPK_VOLUME_PHY);
		if(GetJsonDataPro(io_vol_para, temp, &talkServiceIns.spk_vol_phy)==0)
			talkServiceIns.spk_vol_phy=2;	
	}
	else
	{
		if(talkServiceIns.spk_vol_phy<-90||talkServiceIns.spk_vol_phy>20)
			talkServiceIns.spk_vol_phy=-999;
	}
	if(GetJsonDataPro(json, SPK_GAIN_PHY, &talkServiceIns.spk_gain_phy)==0)
	{
		if(io_para_pre!=NULL)
			sprintf(temp,"%s.%s",io_para_pre,SPK_GAIN_PHY);
		else
			sprintf(temp,"%s",SPK_GAIN_PHY);
		if(GetJsonDataPro(io_vol_para, temp, &talkServiceIns.spk_gain_phy)==0)
			talkServiceIns.spk_gain_phy=0;	
	}
	else
	{
		if(talkServiceIns.spk_gain_phy<0||talkServiceIns.spk_gain_phy>6)
			talkServiceIns.spk_gain_phy=-999;
	}
	
	
	GetJsonDataPro(json, TargetIP, temp);
	if((ptr=strstr(temp,":"))==NULL)
	{
		talkServiceIns.target_ip=inet_addr(temp);	
		talkServiceIns.target_port=0;
	}
	else
	{
		memcpy(buff,temp,ptr-temp);
		buff[ptr-temp]=0;
		talkServiceIns.target_ip=inet_addr(buff);
		talkServiceIns.target_port=atoi(ptr+1);
	}
	
	GetJsonDataPro(json, SourceIP, temp);
	if((ptr=strstr(temp,":"))==NULL)
	{
		talkServiceIns.source_ip=inet_addr(temp);	
		talkServiceIns.source_port=0;
	}
	else
	{
		memcpy(buff,temp,ptr-temp);
		buff[ptr-temp]=0;
		talkServiceIns.source_ip=inet_addr(buff);
		talkServiceIns.source_port=atoi(ptr+1);
	}
	GetJsonDataPro(json, RtpIP, temp);
	if((ptr=strstr(temp,":"))==NULL)
	{
		//talkServiceIns.source_ip=inet_addr(temp);	
		//talkServiceIns.source_port=0;
	}
	else
	{
		memcpy(buff,temp,ptr-temp);
		buff[ptr-temp]=0;
		// lzh_20220817_s
		//au_service_set_rtp(buff,atoi(ptr+1));
		// lzh_20220817_e
		//talkServiceIns.source_ip=inet_addr(buff);
		//talkServiceIns.source_port=atoi(ptr+1);
	}
	
	

	API_talk_on_by_type(talkServiceIns.talktype, talkServiceIns.target_ip, AUDIO_CLIENT_UNICAST_PORT, AUDIO_SERVER_UNICAST_PORT);

	return 0;

}

int API_TalkService_off(void)
{
	API_talk_off();
}

void TalkServiceTest(int ip)
{
	cJSON *root = NULL;
	root = cJSON_CreateObject();

	cJSON_AddNumberToObject(root, TalkType, 0);
	cJSON_AddNumberToObject(root, TargetIP, ip);

	API_TalkService_on(root);
	cJSON_Delete(root);

}
cJSON *GetTalkAllState(void)
{
	static cJSON *all_state=NULL;
	cJSON *sub_state;
	int i;
	cJSON *one_ins;
	
	char buff[50];
	
	if(all_state!=NULL)
		cJSON_Delete(all_state);
	
	all_state=cJSON_CreateObject();
	if(all_state==NULL)
		return NULL;
	//one_ins=GetSystemStat();
	//if(one_ins!=NULL)
	//	cJSON_AddItemToObject(all_state,"SystemStat",one_ins);

	
	if(get_au_service_state()==0)
	{
		cJSON_AddStringToObject(all_state,"SERVICE","IDLE");
	}
	else
	{
		if(talkServiceIns.talktype==LocalAndDt)
			cJSON_AddStringToObject(all_state,"SERVICE",TalkType_LocalAndDt);
		else if(talkServiceIns.talktype==DtAndRtp)
			cJSON_AddStringToObject(all_state,"SERVICE",TalkType_DtAndRtp);
		else if(talkServiceIns.talktype==LocalAndUdp)
			cJSON_AddStringToObject(all_state,"SERVICE",TalkType_LocalAndUdp);
		else if(talkServiceIns.talktype==LocalAndRtp)
			cJSON_AddStringToObject(all_state,"SERVICE",TalkType_LocalAndRtp);
		else if(talkServiceIns.talktype==TalkType_UdpAndRtp)
			cJSON_AddStringToObject(all_state,"SERVICE",TalkType_UdpAndRtp);
		else
			cJSON_AddStringToObject(all_state,"SERVICE","UNKOWN");
	}
	sub_state=get_alsa_state();
	if(sub_state!=NULL)
		cJSON_AddItemToObject(all_state,"ALSA",sub_state);
	sub_state=get_talk_udp_state();
	if(sub_state!=NULL)
		cJSON_AddItemToObject(all_state,"UDP",sub_state);
	sub_state=get_talk_rtp_state();
	if(sub_state!=NULL)
		cJSON_AddItemToObject(all_state,"RTP",sub_state);
	return all_state;
	
}
void au_adj_process(cJSON *adj_para)
{
	char buff[200];
	if(judge_alsa_state_is_run()==0)
	{
		ShellCmdPrintf("alsa is not run");
		return;
	}
	if(adj_para!=NULL)
	{
		if(GetJsonDataPro(adj_para, MIC_VOLUME_PHY, &talkServiceIns.mic_vol_phy))
		{
			if(talkServiceIns.mic_vol_phy<-90||talkServiceIns.mic_vol_phy>20)
			{
				sprintf(buff,"MIC_VOLUME_PHY Must be:-90~20");
				ShellCmdPrintf(buff);
			}
			else
			{
				set_mic_vol(talkServiceIns.mic_vol_phy);
				sprintf(buff,"set MIC_VOLUME_PHY:%d",talkServiceIns.mic_vol_phy);
				ShellCmdPrintf(buff);
			}
		}
		
		
		if(GetJsonDataPro(adj_para, MIC_GAIN_PHY, &talkServiceIns.mic_gain_phy))
		{
			if(talkServiceIns.mic_gain_phy<0||talkServiceIns.mic_gain_phy>8)
			{
				sprintf(buff,"MIC_GAIN_PHY Must be:0~8");
				ShellCmdPrintf(buff);
			}
			else
			{
				set_mic_gain(talkServiceIns.mic_gain_phy);
				sprintf(buff,"set MIC_GAIN_PHY:%d",talkServiceIns.mic_gain_phy);
				ShellCmdPrintf(buff);
			}
		}
		
		if(GetJsonDataPro(adj_para, SPK_VOLUME_PHY, &talkServiceIns.spk_vol_phy))
		{
			if(talkServiceIns.spk_vol_phy<-90||talkServiceIns.spk_vol_phy>20)
			{
				sprintf(buff,"SPK_VOLUME_PHY Must be:-90~20");
				ShellCmdPrintf(buff);
			}
			else
			{
				set_spk_vol(talkServiceIns.spk_vol_phy);
				sprintf(buff,"set SPK_VOLUME_PHY:%d",talkServiceIns.spk_vol_phy);
				ShellCmdPrintf(buff);
			}
		}
		
		if(GetJsonDataPro(adj_para, SPK_GAIN_PHY, &talkServiceIns.spk_gain_phy))
		{
			if(talkServiceIns.spk_gain_phy<0||talkServiceIns.spk_gain_phy>6)
			{
				sprintf(buff,"SPK_GAIN_PHY Must be:0~6");
				ShellCmdPrintf(buff);
			}
			else
			{
				set_spk_gain(talkServiceIns.spk_gain_phy);
				sprintf(buff,"set SPK_GAIN_PHY:%d",talkServiceIns.spk_gain_phy);
				ShellCmdPrintf(buff);
			}
		}
		
	}
	sprintf(buff,"cur au_para:\nMIC_VOLUME_PHY:%d\nMIC_GAIN_PHY:%d\nSPK_VOLUME_PHY:%d\nSPK_GAIN_PHY:%d\n",get_mic_vol(),get_mic_gain(),get_spk_vol(),get_spk_gain());
	ShellCmdPrintf(buff);
}
extern int en_au_out;
extern int au_in_delay;
int ShellCmd_Talk(cJSON *cmd)
{
	char *para=GetShellCmdInputString(cmd, 1);
	cJSON *jpara;
	
	if(para==NULL)
	{
		ShellCmdPrintf("hava no para");
		return 1;
	}
	if(strcmp(para,"state")==0)
	{	
		jpara=GetTalkAllState();
		char* temp;
		temp=cJSON_Print(jpara);
		if(temp==NULL)
		{
			ShellCmdPrintf("Get State File");
			return 1;
		}
		ShellCmdPrintf(temp);
		free(temp);
	}
	else if(strcmp(para,"on")==0)
	{
		para=GetShellCmdInputString(cmd, 2);
		
		if(para==NULL)
		{
			ShellCmdPrintf("hava no para");
			return 1;
		}
		jpara=cJSON_Parse(para);
		if(jpara!=NULL)
		{
			API_TalkService_on(jpara);
			ShellCmdPrintf("Talk on");
			cJSON_Delete(jpara);
		}
		else
		{
			ShellCmdPrintf("json para is err");
		}
	}
	else if(strcmp(para,"off")==0)
	{
	
		API_TalkService_off();
		ShellCmdPrintf("Talk off");
	}
	else if(strcmp(para,"adj")==0)
	{
		para=GetShellCmdInputString(cmd, 2);
		if(para==NULL)
		{
			au_adj_process(NULL);
			return 1;
		}
		jpara=cJSON_Parse(para);
		if(jpara!=NULL)
		{
			au_adj_process(jpara);
			cJSON_Delete(jpara);
		}
		else
		{
			ShellCmdPrintf("json para is err");
		}
	}
	else if(strcmp(para,"udp_link")==0)
	{
		int target_ip,target_port;
		
		char buff[50];
		char *ptr;
		para=GetShellCmdInputString(cmd, 2);
		if(para==NULL)
		{
			return 1;
		}
		
		
		if((ptr=strstr(para,":"))==NULL)
		{
			target_ip=inet_addr(para);	
			target_port=0;
		}
		else
		{
			memcpy(buff,para,ptr-para);
			buff[ptr-para]=0;
			target_ip=inet_addr(buff);
			target_port=atoi(ptr+1);
		}
		if(Send_ListenCtrlSubscriber_Req(target_ip,300,0,&target_port,&target_port)==0)
		{
			Set_listenTalk_ServerIp(target_ip);
			if(Send_ListenCtrlTalk_Req(Get_listenTalk_ServerIp())==0)
			{
				ShellCmdPrintf("udp_link ok");
			}
			else
			{
				ShellCmdPrintf("udp_link fail2");
			}
		}
		else
		{
			ShellCmdPrintf("udp_link fail1");
		}
	}
	else if(strcmp(para,"udp_unlink")==0)
	{
		if(Get_listenTalk_ServerIp()!=0)
		{
			if(Send_ListenCtrlDesubscriber_Req(Get_listenTalk_ServerIp())==0)
			{
				ShellCmdPrintf("udp_unlink ok");
				Set_listenTalk_ServerIp(0);
			}
			else
			{
				ShellCmdPrintf("udp_unlink fail");
			}
				
		}
		else
		{
			ShellCmdPrintf("udp have no link");
		}
	}
	else if(strcmp(para,"enter_dt")==0)
	{
		int spk_vol,mic_vol;
		para=GetShellCmdInputString(cmd, 2);
		if(para==NULL)
		{
			ShellCmdPrintf("have no spk para");
			return 1;
		}
		spk_vol=atoi(para);
		para=GetShellCmdInputString(cmd, 3);
		if(para==NULL)
		{
			ShellCmdPrintf("have no mic para");
			return 1;
		}
		mic_vol=atoi(para);
		OpenDtTalkWithPara(spk_vol,mic_vol);
	}
	else if(strcmp(para,"exit_dt")==0)
	{
		CloseDtTalk();
	}
	else if(strcmp(para,"pwm")==0)
	{
		char data[3];
		para=GetShellCmdInputString(cmd, 2);
		if(para==NULL)
		{
			ShellCmdPrintf("have no para1");
			return 1;
		}
		data[0]=atoi(para);
		para=GetShellCmdInputString(cmd, 3);
		if(para==NULL)
		{
			ShellCmdPrintf("have no para2");
			return 1;
		}
		data[1]=atoi(para);
		para=GetShellCmdInputString(cmd, 4);
		if(para==NULL)
		{
			ShellCmdPrintf("have no para3");
			return 1;
		}
		data[2]=atoi(para);
		api_uart_send_pack_and_wait_rsp(112, data, 3, NULL, NULL, 0);
	}
	#if 0
	else if(strcmp(para,"ai_delay")==0)
	{
		para=GetShellCmdInputString(cmd, 2);
		if(para==NULL)
		{
			ShellCmdPrintf("have no para1");
			return 1;
		}
		au_in_delay= atoi(para);
		ShellCmdPrintf("set au_in_delay=%d",au_in_delay);
	}
	else if(strcmp(para,"ao_en")==0)
	{
		para=GetShellCmdInputString(cmd, 2);
		if(para==NULL)
		{
			ShellCmdPrintf("have no para1");
			return 1;
		}
		en_au_out= atoi(para);
		ShellCmdPrintf("set en_au_out=%d",en_au_out);
	}
	#endif
	else
	{
		ShellCmdPrintf("Unkown para");
		
	}
	return 1;
}
