
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <alsa/asoundlib.h>
#include <ortp/ortp.h>

#include "ak_ai.h"
#include "ak_ao.h"
#include "ak_common.h"
#include "ak_log.h"
#include "ak_mem.h"
#include "RTOS.h"

#include "alsa.h"
#include "g711core.h"
#include "g711common.h"

#include "au_alsa.h"
#include "task_Power.h"

#include "obj_Talk_Servi.h"

#define AU_CAPTURE_BUF_CNT_UDP      20
#define AU_PLAYBACK_BUF_CNT_UDP     20

#define AU_CAPTURE_BUF_CNT_RTP      5
#define AU_PLAYBACK_BUF_CNT_RTP     5

#define AU_ALSA_OBJECT_IDLE         0
#define AU_ALSA_OBJECT_START        1
#define AU_ALSA_OBJECT_RUN          2   
#define AU_ALSA_OBJECT_STOP         3   


// LOCAL
#define AU_LOCAL_PACK_SIZE          128
#define AU_CAPTURE_BUF_CNT_LOCAL    4
#define AU_PLAYBACK_BUF_CNT_LOCAL   4

typedef struct
{
    int 	        state;      // running state
    queue_t 		i_buff;     // playback input buffer
    int			    i_size;     // playback input buffer size
    pthread_mutex_t i_lock;     // playback input buffer lock
    queue_t 		o_buff;     // capture output buffer
    int			    o_size;     // capture output buffer size 
    pthread_mutex_t o_lock;     // capture output buffer lock
    pthread_t	    tid;        // capture thread

    int             samples;    // sample length
    int             framelen;   // frame length
	int             rate;       // sample rate 8000
	int             channels;   // channel
    int             au_capture_buf_cnt;
    int             au_playback_buf_cnt;

    int             mic_vol;
    int             mic_gain;
    int             spk_vol;
    int             spk_gain;

    unsigned char*  enc_frame_buffer;
    unsigned char*  dec_frame_buffer;
	unsigned char*  capture_buffer;
    unsigned char*  playback_buffer;
    int             pcmbuf_cnt;

    cb_io           input;
    cb_io           output;

    AU_IO_TYPE      au_type;
    // test
    int             playback_tick_2_percent;
    // test

}AU_ALSA_OBJECT;
#if 0
static int DX_APP_MIC_VOLUME_STEP_TAB[31]=
{
	//  0 -  10
	-40, -38,-36,-34,-32,-30,-28,-26,-24,-22,-20,
	// 11 - 20
	-18,-16,-14,-12,-10,-8,-6,-4,-2, 0,
	// 21 - 30
	2,4,6,8,10,12,14,16,18,20
};

static int DX_APP_MIC_GAIN_STEP_TAB[9]=
{
	//  0 -  8
	0,1,2,3,4,5,6,7,8
};

int IX_IX_MIC_STEP_TAB[41]=
{
	-20,-19,-18,-17,-16,-15,-14,-13,-12,-11,-10,-9,-8,-7,-6,-5,-4,-3,-2,-1,0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20
};

static int IX_APP_MIC_VOLUME_STEP_TAB[31]=
{
	//  0 -  10
	-40, -38,-36,-34,-32,-30,-28,-26,-24,-22,-20,
	// 11 - 20
	-18,-16,-14,-12,-10,-8,-6,-4,-2, 0,
	// 21 - 30
	2,4,6,8,10,12,14,16,18,20
};

static int IX_APP_MIC_GAIN_STEP_TAB[9]=
{
	//  0 -  8
	0,1,2,3,4,5,6,7,8
};
#endif
int LoadAudioCaptureVol( AU_IO_TYPE au_type )
{
	if(Get_TalkService_Mic_Vol_phy()>-999)
		return Get_TalkService_Mic_Vol_phy();
	
	return 0;
}
int LoadAudioCaptureGain( AU_IO_TYPE au_type )
{
	if(Get_TalkService_Mic_Gain_phy()>-999)
		return Get_TalkService_Mic_Gain_phy();
	
	
        return 0;
}
/*-----------------------------------------------------------------------*/
#if 0
static int DX_APP_SPK_VOLUME_STEP_TAB[31]=
{
	//  0 -  10
	-40, -38,-36,-34,-32,-30,-28,-26,-24,-22,-20,
	// 11 - 20
	-18,-16,-14,-12,-10,-8,-6,-4,-2, 0,
	// 21 - 30
	2,4,6,8,10,12,14,16,18,20
};

static int DX_APP_SPK_GAIN_STEP_TAB[7]=
{
	//  0 -  6
	0,1,2,3,4,5,6
};
static int IX_APP_SPK_VOLUME_STEP_TAB[31]=
{
	//  0 -  10
	-40, -38,-36,-34,-32,-30,-28,-26,-24,-22,-20,
	// 11 - 20
	-18,-16,-14,-12,-10,-8,-6,-4,-2, 0,
	// 21 - 30
	2,4,6,8,10,12,14,16,18,20
};
static int IX_APP_SPK_GAIN_STEP_TAB[7]=
{
	//  0 -  6
	0,1,2,3,4,5,6
};
#endif
int LoadAudioPlaybackVol( AU_IO_TYPE au_type )
{
	if(Get_TalkService_Spk_Vol_phy()>-999)
		return Get_TalkService_Spk_Vol_phy();
        return 0;
}

int LoadAudioPlaybackGain( AU_IO_TYPE au_type )
{
	if(Get_TalkService_Spk_Gain_phy()>-999)
		return Get_TalkService_Spk_Gain_phy();
        return 0;
}
/*-----------------------------------------------------------------------*/

AU_ALSA_OBJECT  one_au_alsa_ins={0};
//mblk_t *im=NULL;
static int au_alsa_input( const unsigned char* pbuff, int len )
{
    int frame_len;
    mblk_t *im=NULL;
	if( one_au_alsa_ins.state == AU_ALSA_OBJECT_RUN )
	{
        if( len != one_au_alsa_ins.framelen )
        {
            printf("frame length is NOT matched!\n");
            return -1;
        }
        G711Decoder2((short*)one_au_alsa_ins.dec_frame_buffer, (unsigned char*)pbuff, len, USE_G711_LAW); 
        memcpy( one_au_alsa_ins.playback_buffer + one_au_alsa_ins.pcmbuf_cnt*one_au_alsa_ins.samples, one_au_alsa_ins.dec_frame_buffer, one_au_alsa_ins.samples );
        if( ++one_au_alsa_ins.pcmbuf_cnt >= one_au_alsa_ins.au_playback_buf_cnt )
        {
            one_au_alsa_ins.pcmbuf_cnt = 0;
            im = allocb(one_au_alsa_ins.au_playback_buf_cnt*one_au_alsa_ins.samples, 0);
            memcpy( im->b_rptr, one_au_alsa_ins.playback_buffer, one_au_alsa_ins.au_playback_buf_cnt*one_au_alsa_ins.samples );
            im->b_wptr += (one_au_alsa_ins.au_playback_buf_cnt*one_au_alsa_ins.samples);
            pthread_mutex_lock( &one_au_alsa_ins.i_lock );
            putq(&one_au_alsa_ins.i_buff,im);
            one_au_alsa_ins.i_size++;
            pthread_mutex_unlock( &one_au_alsa_ins.i_lock );
            //printf("au_alsa_input:buf cnt[%d]\n",one_au_alsa_ins.i_size);
        }
        return 0;
    }
    else
        return -1;
}
extern int ai_handle_id;
extern int ao_handle_id;
static void* au_alsa_process_clean( void* arg )
{	
	printf("%s\n",__FUNCTION__);
    AU_ALSA_OBJECT* pins = (AU_ALSA_OBJECT*)arg;

	if( pins->enc_frame_buffer != NULL )
    {
        free(pins->enc_frame_buffer);
        pins->enc_frame_buffer = NULL;
    }
    if( pins->dec_frame_buffer != NULL )
    {
        free(pins->dec_frame_buffer);
        pins->dec_frame_buffer = NULL;
    }
    if( pins->capture_buffer != NULL )
    {
        free(pins->capture_buffer);
        pins->capture_buffer = NULL;
    }
    if( pins->playback_buffer != NULL )
    {
        free(pins->playback_buffer);
        pins->playback_buffer = NULL;
    }
	if(ai_handle_id!=-1)
	{
		alsa_read_uninit( NULL );
		ai_handle_id=-1;
	}
	if(ao_handle_id!=-1)
	{
		alsa_write_uninit( NULL );	
		#if defined(ALSA_W_SHARE)
		#else
		ao_handle_id = -1;
		#endif
	}
}

static void* au_alsa_processing( void* arg )
{
    AU_ALSA_OBJECT* pins = (AU_ALSA_OBJECT*)arg;
    mblk_t *im;
	int	total_block;
	int i,remain,packlen;

    printf("------------au_alsa_processing----------\n");

    pins->pcmbuf_cnt = 0;
    // test
    pins->playback_tick_2_percent = 0;
    // test

	pins->enc_frame_buffer  = malloc(pins->samples);
    pins->dec_frame_buffer  = malloc(pins->samples);
	pins->capture_buffer    = malloc(pins->samples*pins->au_capture_buf_cnt);
    pins->playback_buffer   = malloc(pins->samples*pins->au_playback_buf_cnt);

	pthread_cleanup_push( au_alsa_process_clean, arg );		
	#if	defined(ALSA_W_SHARE)
	
	
		alsa_read_init( pins->samples*pins->au_capture_buf_cnt, pins->rate, pins->channels, pins->mic_vol, pins->mic_gain );
		if(ai_handle_id == -1)
			goto au_alsa_processing_end;
	
		
	
	alsa_write_init( pins->samples*pins->au_playback_buf_cnt, pins->rate, pins->channels, pins->spk_vol, pins->spk_gain );
	if(ao_handle_id == -1)
		goto au_alsa_processing_end;
	
	#else
	
	alsa_read_init( pins->samples*pins->au_capture_buf_cnt, pins->rate, pins->channels, pins->mic_vol, pins->mic_gain );
	if(ai_handle_id == -1)
		goto au_alsa_processing_end;
	alsa_write_init( pins->samples*pins->au_playback_buf_cnt, pins->rate, pins->channels, pins->spk_vol, pins->spk_gain );
	if(ao_handle_id == -1)
		goto au_alsa_processing_end;
	#endif
    pins = (AU_ALSA_OBJECT*)arg;
    pins->state = AU_ALSA_OBJECT_RUN;

	printf( "au_alsa_processing\n");

    rtp_process_set_high_prio();

    remain = 0;
	while( pins->state==AU_ALSA_OBJECT_RUN )
	{
		// playback
        pthread_mutex_lock( &pins->i_lock );
        if( (im = getq(&pins->i_buff)) != NULL ) 
        {
            pthread_mutex_unlock( &pins->i_lock );
            alsa_write( NULL, im->b_rptr, im->b_wptr-im->b_rptr );
            pins->i_size--;
           // printf("==============app write len=%d, buf cnt[%d]=============\n",im->b_wptr-im->b_rptr,pins->i_size);
			freemsg (im);
        }
        else
        {
            pthread_mutex_unlock( &pins->i_lock );
        }
 		// capture
		if( ( i = alsa_read( NULL, pins->capture_buffer+remain, pins->samples ) ) >= 0 ) 
		{
			if( pins->au_type == LocalAndDt)
			{
			    alsa_write( NULL, pins->capture_buffer+remain, i );     
			}
			else
			{
			    total_block = i/pins->samples;
			    remain		= i%pins->samples;
			    //printf( "play with send multi block[%d][%d][%d]\n",i,total_block,remain);
			    for( i = 0; i < total_block; i++ )
			    {
			        G711Encoder2((short*)(pins->capture_buffer+i*pins->samples), pins->enc_frame_buffer, pins->framelen, USE_G711_LAW);	
				#if 0
				int cnt;
				for(cnt=0;cnt<pins->framelen;cnt++)
					printf("%02x "	,pins->enc_frame_buffer[cnt]);
				printf("\n");
				#endif
			        if( pins->output != NULL )
			        {
			            (*pins->output)(pins->enc_frame_buffer, pins->framelen);
			        }
			    }
			    memcpy(pins->capture_buffer,pins->capture_buffer+total_block*pins->samples,remain);
			}
		}
 		usleep(1000); 		
	}
au_alsa_processing_end:	
	pthread_cleanup_pop( 1 );
}

/********************************************************************************
@function:
	au_alsa_init:   alsa service initial
@parameters:
    none
@return:
	-1/error, x/ data length
********************************************************************************/
int au_alsa_init( AU_IO_TYPE au_type )
{
	one_au_alsa_ins.state       = AU_ALSA_OBJECT_IDLE;

    qinit(&one_au_alsa_ins.i_buff);
    one_au_alsa_ins.i_size = 0;
    pthread_mutex_init( &one_au_alsa_ins.i_lock, 0);
    
    qinit(&one_au_alsa_ins.o_buff);
    one_au_alsa_ins.o_size = 0;
    pthread_mutex_init( &one_au_alsa_ins.o_lock, 0);

    one_au_alsa_ins.channels    = 1;
    one_au_alsa_ins.rate        = 8000;

    if( au_type == LocalAndRtp||au_type == DtAndRtp)
    {
        one_au_alsa_ins.samples             = AU_RTP_PACK_SIZE*2;
        one_au_alsa_ins.framelen            = AU_RTP_PACK_SIZE;
        one_au_alsa_ins.au_capture_buf_cnt  = AU_CAPTURE_BUF_CNT_RTP;
        one_au_alsa_ins.au_playback_buf_cnt = AU_PLAYBACK_BUF_CNT_RTP;
    }
    else if( au_type == LocalAndDt)
    {
        one_au_alsa_ins.samples             = AU_LOCAL_PACK_SIZE*2;
        one_au_alsa_ins.framelen            = AU_LOCAL_PACK_SIZE;
        one_au_alsa_ins.au_capture_buf_cnt  = AU_CAPTURE_BUF_CNT_LOCAL;
        one_au_alsa_ins.au_playback_buf_cnt = AU_PLAYBACK_BUF_CNT_LOCAL;
    }
    else
    {
        one_au_alsa_ins.samples             = UDP_AUDIO_G711_DATA_LEN*2;
        one_au_alsa_ins.framelen            = UDP_AUDIO_G711_DATA_LEN;
        one_au_alsa_ins.au_capture_buf_cnt  = AU_CAPTURE_BUF_CNT_UDP;
        one_au_alsa_ins.au_playback_buf_cnt = AU_PLAYBACK_BUF_CNT_UDP;
    }

	one_au_alsa_ins.enc_frame_buffer  = NULL;
    one_au_alsa_ins.dec_frame_buffer  = NULL;
	one_au_alsa_ins.capture_buffer    = NULL;
    one_au_alsa_ins.playback_buffer   = NULL;

    one_au_alsa_ins.mic_vol     = LoadAudioCaptureVol(au_type);
    one_au_alsa_ins.mic_gain    = LoadAudioCaptureGain(au_type);
    one_au_alsa_ins.spk_vol     = LoadAudioPlaybackVol(au_type);
    one_au_alsa_ins.spk_gain    = LoadAudioPlaybackGain(au_type);

    one_au_alsa_ins.input       = au_alsa_input;
    one_au_alsa_ins.output      = NULL;
    return 0;
}

/********************************************************************************
@function:
	au_alsa_start:   alsa service start
@parameters:
    sync_write - 同步播放
@return:
	-1/error, x/ data length
********************************************************************************/
int au_alsa_start( AU_IO_TYPE au_type )
{
	if( one_au_alsa_ins.state == AU_ALSA_OBJECT_IDLE )
    {
        one_au_alsa_ins.state        = AU_ALSA_OBJECT_START;
        one_au_alsa_ins.au_type      = au_type;

        printf("------------au_alsa_start----------\n");
	    //API_POWER_TALK_ON();	
        pthread_create(&one_au_alsa_ins.tid, NULL, au_alsa_processing, (void*)&one_au_alsa_ins);
        return 0;
    }
    else
	    return -1;
}

/********************************************************************************
@function:
	au_alsa_stop:   alsa service stop
@parameters:
    none
@return:
	-1/error, x/ data length
********************************************************************************/
int au_alsa_stop( void )
{
    if( one_au_alsa_ins.state == AU_ALSA_OBJECT_IDLE )
        return -1;
    int wait_cnt = 0;
    while( one_au_alsa_ins.state != AU_ALSA_OBJECT_RUN )
    {
        usleep(100*1000);
        if( ++wait_cnt > 50 )
        {
            one_au_alsa_ins.state = AU_ALSA_OBJECT_IDLE;
            return -1;
        }
    }
    one_au_alsa_ins.state = AU_ALSA_OBJECT_STOP;
	usleep(100*1000);
    //printf("%s start\n",__FUNCTION__);
    //pthread_cancel( one_au_alsa_ins.tid ) ;
    pthread_join( one_au_alsa_ins.tid, NULL );
    //printf("1111111111111%s end %d\n ",__FUNCTION__,FLUSHALL);
	
    flushq(&one_au_alsa_ins.i_buff,0);
    flushq(&one_au_alsa_ins.o_buff,0);
	//usleep(1000*1000);
    one_au_alsa_ins.state = AU_ALSA_OBJECT_IDLE;
	//API_POWER_TALK_OFF();
	//freemsg (im);
    return 0;
}

/********************************************************************************
@function:
	au_alsa_set_output: set alsa output callback pointer
@parameters:
    output:     output callback pointer
@return:
	0/ok, -1/error
********************************************************************************/
int au_alsa_set_output( cb_io output )
{
	one_au_alsa_ins.output = output;
    return 0;
}

/********************************************************************************
@function:
	au_alsa_get_input: get alsa input callback pointer
@parameters:
    none
@return:
	cb_io type callback pointer
********************************************************************************/
cb_io au_alsa_get_input( void )
{
	return one_au_alsa_ins.input;
}

/********************************************************************************
@function:
	au_alsa_pop:    fetch alsa capture packet in buffer
@parameters:
    pbuff:      output buffer pointer
    limit:      output buffer size
@return:
	-1/error, x/ data length
********************************************************************************/
int au_alsa_pop( unsigned char* pbuff, int limit )
{
    int len=0;
    mblk_t *im;
	if( one_au_alsa_ins.state == AU_ALSA_OBJECT_RUN )
	{
        pthread_mutex_lock( &one_au_alsa_ins.o_lock );
        if( (im = getq(&one_au_alsa_ins.o_buff)) != NULL ) 
        {
            pthread_mutex_unlock( &one_au_alsa_ins.o_lock );
            len = im->b_wptr-im->b_rptr;
            len = (len<limit)?len:limit;
            memcpy(pbuff,im->b_rptr,len);
            freemsg (im);
            one_au_alsa_ins.o_size--;
        }
        else
            pthread_mutex_unlock( &one_au_alsa_ins.o_lock );
        return len;
    }
    else
        return -1;
}
cJSON *get_alsa_state(void)
{
	cJSON *state;
	state=cJSON_CreateObject();
	if(state!=NULL)
	{
		if(one_au_alsa_ins.state==AU_ALSA_OBJECT_RUN)
		{
			cJSON_AddStringToObject(state,"STATE","RUN");
			cJSON_AddNumberToObject(state,"MIC_VOL",one_au_alsa_ins.mic_vol);
			cJSON_AddNumberToObject(state,"MIC_GAIN",one_au_alsa_ins.mic_gain);
			cJSON_AddNumberToObject(state,"SPK_VOL",one_au_alsa_ins.spk_vol);
			cJSON_AddNumberToObject(state,"SPK_GAIN",one_au_alsa_ins.spk_gain);
		}
		else if(one_au_alsa_ins.state==AU_ALSA_OBJECT_IDLE)
			cJSON_AddStringToObject(state,"STATE","IDLE");
		else if(one_au_alsa_ins.state==AU_ALSA_OBJECT_START)
			cJSON_AddStringToObject(state,"STATE","START");
		else if(one_au_alsa_ins.state==AU_ALSA_OBJECT_STOP)
			cJSON_AddStringToObject(state,"STATE","STOP");
		else
			cJSON_AddStringToObject(state,"STATE","UNKOWN");
	}
	return state;
}

int judge_alsa_state_is_run(void)
{
	if(one_au_alsa_ins.state==AU_ALSA_OBJECT_RUN)
		return 1;
	return 0;
}