

#ifndef _OBJ_TALK_SERVI_H_
#define _OBJ_TALK_SERVI_H_

#include "cJSON.h"

#define TalkType			"TalkType"
#define TalkVolPara		"TalkVolPara"
#define TargetIP			"TargetIP"
#define SourceIP			"SourceIP"
#define RtpIP			"RtpIP"


#define MIC_VOLUME			"MIC_VOLUME"
#define MIC_GAIN			"MIC_GAIN"
#define SPK_VOLUME			"SPK_VOLUME"
#define SPK_GAIN			"SPK_GAIN"

#define MIC_VOLUME_PHY			"MIC_VOLUME_PHY"
#define MIC_GAIN_PHY			"MIC_GAIN_PHY"
#define SPK_VOLUME_PHY			"SPK_VOLUME_PHY"
#define SPK_GAIN_PHY			"SPK_GAIN_PHY"

#define MIC_VOLUME_APP		"MIC_VOLUME_APP"
#define MIC_GAIN_APP		"MIC_GAIN_APP"
#define SPK_VOLUME_APP		"SPK_VOLUME_APP"
#define SPK_GAIN_APP		"SPK_GAIN_APP"

#define TalkType_LocalAndDt		"LocalAndDt"
#define TalkType_LocalAndUdp	"LocalAndUdp"
#define TalkType_LocalAndRtp	"LocalAndRtp"
#define TalkType_DtAndUdp		"DtAndUdp"
#define TalkType_DtAndRtp		"DtAndRtp"
#define TalkType_UdpAndRtp		"UdpAndRtp"

// audio客户端传输端口号
#define AUDIO_CLIENT_UNICAST_PORT		25003
// audio服务器端传输端口
#define AUDIO_SERVER_UNICAST_PORT		25003

typedef struct 
{
	int 		talktype;
	int			source_ip;
	int 		target_ip;
	int			source_port;
	int			target_port;	
    //int         mic_vol;
   // int         mic_gain;
   // int         spk_vol;
  //  int         spk_gain;
	int         mic_vol_phy;
    int         mic_gain_phy;
    int         spk_vol_phy;
    int         spk_gain_phy;
    //int         mic_vol_app;
   // int         mic_gain_app;
   // int         spk_vol_app;
   // int         spk_gain_app;
} TalkService_JSON;

int API_TalkService_on(cJSON* json);
int API_TalkService_off(void);

int Get_TalkService_Mic_Vol(void);
int Get_TalkService_Mic_Gain(void);
int Get_TalkService_Spk_Vol(void);
int Get_TalkService_Spk_Gain(void);

int Get_TalkService_Mic_Vol_app(void);
int Get_TalkService_Mic_Gain_app(void);
int Get_TalkService_Spk_Vol_app(void);
int Get_TalkService_Spk_Gain_app(void);


#endif

