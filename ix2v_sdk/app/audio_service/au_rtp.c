
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <ortp/ortp.h>

#include "rtp_utility.h"
#include "au_rtp.h"

#include "cJSON.h"

#define AU_RTP_OBJECT_IDLE         0
#define AU_RTP_OBJECT_START        1
#define AU_RTP_OBJECT_RUN          2
#define AU_RTP_OBJECT_STOP          3

typedef struct
{
    int 	        state;      // running state
    queue_t 		i_buff;     // playback input buffer
    int			    i_size;     // playback input buffer size
    pthread_mutex_t i_lock;     // playback input buffer lock

    char            ipstr[20]; 
    short           port;
    ONE_RTP_SESSION* rtp_sess;

    cb_io           input;
    cb_io           output;

    int             samples;
    unsigned char*  precbuf;
    int             poffest;
}AU_RTP_OBJECT;

AU_RTP_OBJECT  one_au_rtp_ins={0};

static int au_rtp_input( const unsigned char* pbuff, int len )
{
    int total_len, tail_cnt, copy_cnt;
	//printf("0000000000000au_rtp_input %d\n",len);
	if( one_au_rtp_ins.state == AU_RTP_OBJECT_RUN )
	{
        total_len = one_au_rtp_ins.poffest + len;
        if( total_len < one_au_rtp_ins.samples )
        {            
            memcpy( one_au_rtp_ins.precbuf + one_au_rtp_ins.poffest, pbuff, len);
            one_au_rtp_ins.poffest += len;
        }
        else
        {
            tail_cnt = total_len - one_au_rtp_ins.samples;     // tail part
            copy_cnt = len - tail_cnt;                         // copy part
            memcpy( one_au_rtp_ins.precbuf + one_au_rtp_ins.poffest, pbuff, copy_cnt);  // copy to full
            // send frame
           // printf("1111111111au_rtp_input %d\n",one_au_rtp_ins.samples);
            send_one_rtp_pack(one_au_rtp_ins.rtp_sess,one_au_rtp_ins.precbuf,one_au_rtp_ins.samples,0);
            // store tail data
            if( tail_cnt ) memcpy( one_au_rtp_ins.precbuf, pbuff+copy_cnt, tail_cnt );
            one_au_rtp_ins.poffest = tail_cnt;
        }
        return 0;
    }
    else
        return -1;
}

void rtp_recv_data_process(mblk_t *im, int timestamp)
{
    int len;
    if( one_au_rtp_ins.state == AU_RTP_OBJECT_RUN )
    {
        if( one_au_rtp_ins.output != NULL )
        {
            len = im->b_wptr-im->b_rptr;
            (*one_au_rtp_ins.output)(im->b_rptr,len);
		printf("rtp_recv_data_process:%d\n",len);
        }
        #if 0
        pthread_mutex_lock( &one_au_rtp_ins.i_lock );
        putq(&one_au_rtp_ins.i_buff,im);
        one_au_rtp_ins.i_size++;
        pthread_mutex_unlock( &one_au_rtp_ins.i_lock );
        #endif
    }
}

/********************************************************************************
@function:
	audio rtp transfer initial before starting
@parameters:
	ip:	        server ip address
	port:	    server port
@return:
	0/ok, -1/error
********************************************************************************/
int au_rtp_init( char* ip, short port )
{
    one_au_rtp_ins.state = AU_RTP_OBJECT_IDLE;

    qinit(&one_au_rtp_ins.i_buff);
    one_au_rtp_ins.i_size = 0;
    pthread_mutex_init( &one_au_rtp_ins.i_lock, 0);

    memset( one_au_rtp_ins.ipstr, 0, 20 );
    strcpy( one_au_rtp_ins.ipstr, ip);
    one_au_rtp_ins.port = port;

    one_au_rtp_ins.input = au_rtp_input;
    printf(">>>>>>>>>>>>>>>>>>>>>>>>au_rtp_init,input[%08x]>>>>>>>>>>>>>>>>>>\n",one_au_rtp_ins.input);
    
    one_au_rtp_ins.output = NULL;

    one_au_rtp_ins.samples = AU_RTP_PACK_SIZE;
    one_au_rtp_ins.precbuf = malloc(AU_RTP_PACK_SIZE);
    one_au_rtp_ins.poffest = 0;
    return 0;
}

/********************************************************************************
@function:
	start audio rtp transfer
@parameters:
    none
@return:
	0/ok, -1/error
********************************************************************************/
int au_rtp_start( void )
{
	if( one_au_rtp_ins.state == AU_RTP_OBJECT_IDLE )    
    {
	    printf( "au_rtp_processing\n");

        one_au_rtp_ins.rtp_sess = start_one_rtp_process(1,one_au_rtp_ins.ipstr,one_au_rtp_ins.port,payload_type_pcmu8000,8,rtp_recv_data_process);
        if( one_au_rtp_ins.rtp_sess == NULL)
        {
            return -1;
        }
        else
        {
            one_au_rtp_ins.state = AU_RTP_OBJECT_RUN;
            return 0;
        }
    }
}

/********************************************************************************
@function:
	stop audio rtp transfer
@parameters:
    none
@return:
	0/ok, -1/error
********************************************************************************/
int au_rtp_stop( void )
{
    if( one_au_rtp_ins.state == AU_RTP_OBJECT_IDLE )
        return -1;
	one_au_rtp_ins.state=AU_RTP_OBJECT_STOP;
	usleep(100*1000);
	if( one_au_rtp_ins.rtp_sess )
    {
		stop_one_rtp_process(one_au_rtp_ins.rtp_sess);
        one_au_rtp_ins.rtp_sess = NULL;
    }

    flushq(&one_au_rtp_ins.i_buff,0);

    if( one_au_rtp_ins.precbuf != NULL )
    {
        free(one_au_rtp_ins.precbuf);
        one_au_rtp_ins.precbuf = NULL;
    }

    one_au_rtp_ins.state = AU_RTP_OBJECT_IDLE;
    return 0;
}

/********************************************************************************
@function:
	set audio rtp output callback pointer
@parameters:
    output:     output callback pointer
@return:
	0/ok, -1/error
********************************************************************************/
int au_rtp_set_output( cb_io output )
{
    one_au_rtp_ins.output = output;
    printf(">>>>>>>>>>>>>>>>>>>>>>>>au_rtp_set_output[%08x]>>>>>>>>>>>>>>>>>>\n",one_au_rtp_ins.output);
    return 0;
}

/********************************************************************************
@function:
	get audio rtp output callback pointer
@parameters:
    none
@return:
	cb_io type callback pointer
********************************************************************************/
cb_io au_rtp_get_output( void )
{
    return one_au_rtp_ins.output;
}

/********************************************************************************
@function:
	get audio rtp input callback pointer
@parameters:
    none
@return:
	cb_io type callback pointer
********************************************************************************/
cb_io au_rtp_get_input( void )
{
    return one_au_rtp_ins.input;
}

/********************************************************************************
@function:
	fetch rtp received packet in buffer
@parameters:
    pbuff:      output buffer pointer
    limit:      output buffer size
@return:
	-1/error, x/ data length
********************************************************************************/
int au_rtp_pop( unsigned char* pbuff, int limit )
{
    int len=0;
    mblk_t *im;
	if( one_au_rtp_ins.state == AU_RTP_OBJECT_RUN )
	{        
        pthread_mutex_lock( &one_au_rtp_ins.i_lock );
        if( (im = getq(&one_au_rtp_ins.i_buff)) != NULL ) 
        {
            pthread_mutex_unlock( &one_au_rtp_ins.i_lock );
            len = im->b_wptr-im->b_rptr;
            len = (len<limit)?len:limit;
            memcpy(pbuff,im->b_rptr,len);
			freemsg (im);
            one_au_rtp_ins.i_size--;
        }
        else
            pthread_mutex_unlock( &one_au_rtp_ins.i_lock );
        return len;
    }
    else
        return -1;
}
cJSON *get_talk_rtp_state(void)
{
	cJSON *state;
	char buff[100];
	state=cJSON_CreateObject();
	if(state!=NULL)
	{
		if(one_au_rtp_ins.state==AU_RTP_OBJECT_RUN)
		{
			cJSON_AddStringToObject(state,"STATE","RUN");
			sprintf(buff,"%s:%d",one_au_rtp_ins.ipstr,one_au_rtp_ins.port);
			cJSON_AddStringToObject(state,"ADDR",buff);
		}
		else if(one_au_rtp_ins.state==AU_RTP_OBJECT_IDLE)
		{
			cJSON_AddStringToObject(state,"STATE","IDLE");
		}
		else if(one_au_rtp_ins.state==AU_RTP_OBJECT_START)
		{
			cJSON_AddStringToObject(state,"STATE","START");
		}
		else if(one_au_rtp_ins.state==AU_RTP_OBJECT_STOP)
		{
			cJSON_AddStringToObject(state,"STATE","STOP");
		}
		else
		{
			cJSON_AddStringToObject(state,"STATE","UNKOWN");
		}
		
	}
	return state;
}