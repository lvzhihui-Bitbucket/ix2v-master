
#ifndef _AU_ALSA_H_
#define _AU_ALSA_H_

#define G711_A_LAW	0
#define G711_U_LAW	1
#define USE_G711_LAW    G711_U_LAW

#include "au_service.h"

/********************************************************************************
@function:
	au_alsa_init:   alsa service initial
@parameters:
    none
@return:
	-1/error, x/ data length
********************************************************************************/
int au_alsa_init( AU_IO_TYPE au_type );

/********************************************************************************
@function:
	au_alsa_start:   alsa service start
@parameters:
    sync_write - 同步播放
@return:
	-1/error, x/ data length
********************************************************************************/
int au_alsa_start( AU_IO_TYPE au_type );

/********************************************************************************
@function:
	au_alsa_stop:   alsa service stop
@parameters:
    none
@return:
	-1/error, x/ data length
********************************************************************************/
int au_alsa_stop( void );

/********************************************************************************
@function:
	au_alsa_set_output: set alsa output callback pointer
@parameters:
    output:     output callback pointer
@return:
	0/ok, -1/error
********************************************************************************/
int au_alsa_set_output( cb_io output );

/********************************************************************************
@function:
	au_alsa_get_input: get alsa input callback pointer
@parameters:
    none
@return:
	cb_io type callback pointer
********************************************************************************/
cb_io au_alsa_get_input( void );

/********************************************************************************
@function:
	au_alsa_pop:    fetch alsa capture packet in buffer
@parameters:
    pbuff:      output buffer pointer
    limit:      output buffer size
@return:
	-1/error, x/ data length
********************************************************************************/
int au_alsa_pop( unsigned char* pbuff, int limit );

#endif
