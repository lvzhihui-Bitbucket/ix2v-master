
#ifndef __OBJ_WAVEPLAYCONTROL_H
#define __OBJ_WAVEPLAYCONTROL_H

#define WAVE_STATE_STOP   0
#define WAVE_STATE_PLAY   1
#define PRIORITY_COUNT_LIMIT     0
#define PRIORITY_TIME_LIMIT      1

// Define Object Property-------------------------------------------------------
typedef struct
{
    unsigned char WaveState;                // the whole ring state,ring or stop
    unsigned char WaveCycleState;           // current cycle state, play or stop
    unsigned char WaveCount;                // how many cycles ring counter
    int           WaveTime;                // the whole ring running time ,1ms/unit
    int           WaveTimeLimit;            // the whole ring time limit of the scene tune, 1s/unit
    int           WaveCountLimit;           // the whole ring times limit of the scene tune
    int           WaveCycleTimeLimit;       // 曲子每次播放的时长。one cycle time limit, 1ms/unit
    int           WaveCycleGap;             // 间隔intermission of one cycle and the next cycle, 1ms/uint
    int           waveVolume;	            // 音量
} WaveCtrl_TypeDef;

// Define Object Function - Public----------------------------------------------
void WaveCtrl_Init(void);

void WaveCtrl_Start(cJSON* json);
void WaveCtrl_Stop(void);

unsigned char WaveGetState(void);
void  WaveSetState(unsigned char state);

#endif

