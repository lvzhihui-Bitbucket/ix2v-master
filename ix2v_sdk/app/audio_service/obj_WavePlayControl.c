#include "OSTIME.h"
#include "cJSON.h"
#include "obj_WavePlayControl.h"
#include "task_Audio.h"
#include "task_Hal.h"


#define CYCLE_STATE_STOP   0
#define CYCLE_STATE_PLAY   1


static WaveCtrl_TypeDef waveCtrl;
static OS_TIMER timer_CountLimit;  //用于按次数播放铃声
static OS_TIMER timer_TimeLimit;  //用于按时长播放铃声


static void Timer_CountLimit_Callback(void)
{
	WAVEPLAY_STRUCT msgWave;
	msgWave.head.msg_source_id 	= 0;
	msgWave.head.msg_target_id 	= MSG_ID_Audio;
	msgWave.head.msg_type 		= MSG_TYPE_WAVE;
	msgWave.head.msg_sub_type 	= 0;
	msgWave.cmd					= WAVE_CNT_CYCLE;

	// 压入本地队列
	push_vdp_common_queue(&vdp_audio_mesg_queue, (char*)&msgWave, sizeof(WAVEPLAY_STRUCT));

}

static void Timer_TimeLimit_Callback(void)
{
	WAVEPLAY_STRUCT msgWave;

	msgWave.head.msg_source_id 	= 0;
	msgWave.head.msg_target_id 	= MSG_ID_Audio;
	msgWave.head.msg_type 		= MSG_TYPE_WAVE;
	msgWave.head.msg_sub_type 	= 0;
	msgWave.cmd					= WAVE_TIME_CYCLE;

	// 压入本地队列
	push_vdp_common_queue(&vdp_audio_mesg_queue, (char*)&msgWave, sizeof(WAVEPLAY_STRUCT));
}

void Wave_Count_Cycle(void)
{
	if (waveCtrl.WaveCount >= waveCtrl.WaveCountLimit)
	{
		WaveCtrl_Stop();
		return;
	}

	if (waveCtrl.WaveCycleState == CYCLE_STATE_PLAY)//进入Timer ISR之前处于"CYCLE_STATE_PLAY"状态
	{
		waveCtrl.WaveCycleState = CYCLE_STATE_STOP;
		api_WaveStop();
		OS_SetTimerPeriod(&timer_CountLimit, waveCtrl.WaveCycleGap/25);		//czn_20170113
		OS_RetriggerTimer(&timer_CountLimit);
	}
	else //进入Timer ISR之前处于"CYCLE_STATE_STOP"状态
	{
		waveCtrl.WaveCycleState = CYCLE_STATE_PLAY;
		api_WavePlay();
		waveCtrl.WaveCount++;
		OS_SetTimerPeriod(&timer_CountLimit, waveCtrl.WaveCycleTimeLimit/25);	//czn_20170113
		OS_RetriggerTimer(&timer_CountLimit);
	}
}

void Wave_Time_Cycle(void)
{
	if (waveCtrl.WaveTime >= (1000 * waveCtrl.WaveTimeLimit))	
	{
		WaveCtrl_Stop();
		return;
	}

	if (waveCtrl.WaveCycleState == CYCLE_STATE_PLAY)//进入Timer ISR之前处于"CYCLE_STATE_PLAY"状态
	{
		waveCtrl.WaveCycleState = CYCLE_STATE_STOP;
		api_WaveStop();
		waveCtrl.WaveTime += waveCtrl.WaveCycleGap;
		OS_SetTimerPeriod(&timer_TimeLimit, waveCtrl.WaveCycleGap/25);
		OS_RetriggerTimer(&timer_TimeLimit);
	}
	else //进入Timer ISR之前处于"CYCLE_STATE_STOP"状态
	{
		waveCtrl.WaveCycleState = CYCLE_STATE_PLAY;
		api_WavePlay();
		waveCtrl.WaveTime += waveCtrl.WaveCycleTimeLimit;
		OS_SetTimerPeriod(&timer_TimeLimit, waveCtrl.WaveCycleTimeLimit/25);
		OS_RetriggerTimer(&timer_TimeLimit);
	}

}


void WaveCtrl_Init(void)
{
	//创建软定时(未启动)
	OS_CreateTimer(&timer_CountLimit, Timer_CountLimit_Callback, 1);
	OS_CreateTimer(&timer_TimeLimit, Timer_TimeLimit_Callback, 1);
}


void WaveCtrl_Start(cJSON* json)
{	
	char strFile[100];	
	uint32 ringTime;
	if(WaveGetState() == WAVE_STATE_PLAY)
    {
        WaveCtrl_Stop();
    }
	memset(&waveCtrl, 0, sizeof(WaveCtrl_TypeDef));
	GetJsonDataPro(json, "RingFile", strFile);
	GetJsonDataPro(json, "RingVol", &waveCtrl.waveVolume);
	GetJsonDataPro(json, "RingTime", &waveCtrl.WaveTimeLimit);
	GetJsonDataPro(json, "RingGap", &waveCtrl.WaveCycleGap);
	GetJsonDataPro(json, "RingCount", &waveCtrl.WaveCountLimit);
	ShellCmdPrintf("WaveCtrl_Start file=%s cnt=%d vol=%d time=%d gap=%d\n", strFile, waveCtrl.WaveCountLimit, waveCtrl.waveVolume, waveCtrl.WaveTimeLimit, waveCtrl.WaveCycleGap);
	// start ring
	WaveSetState(WAVE_STATE_PLAY);	//播放标志
	waveCtrl.WaveCycleState = CYCLE_STATE_PLAY;
	waveCtrl.WaveCycleGap = waveCtrl.WaveCycleGap * 1000;
	api_wave_volume_set(waveCtrl.waveVolume);
	api_wave_file_set(strFile);
	//AMPMUTE_SET();
	AMP_RING_OPEN();
	api_WavePlay();     //启动播放
	PlayList_GetUsingLength_ms_DS(strFile, &waveCtrl.WaveCycleTimeLimit);

	if (waveCtrl.WaveTimeLimit)
	{
		ringTime = waveCtrl.WaveTimeLimit*1000;
		if(ringTime <= waveCtrl.WaveCycleTimeLimit)
		{
			ringTime = waveCtrl.WaveCycleTimeLimit;
		}
		else
		{
			uint32 remainder;
			remainder = (ringTime - waveCtrl.WaveCycleTimeLimit)%(waveCtrl.WaveCycleTimeLimit + waveCtrl.WaveCycleGap);
			if(remainder <= waveCtrl.WaveCycleGap)
			{
				ringTime = ringTime - remainder;
			}
			else
			{
				ringTime = ringTime + (waveCtrl.WaveCycleTimeLimit + waveCtrl.WaveCycleGap - remainder);
			}
		}
		waveCtrl.WaveTimeLimit = ringTime/1000 + (ringTime%1000 ? 1 : 0);
		waveCtrl.WaveTime = waveCtrl.WaveCycleTimeLimit;
		OS_SetTimerPeriod(&timer_TimeLimit, waveCtrl.WaveCycleTimeLimit/25);
		OS_RetriggerTimer(&timer_TimeLimit);//启动软定?
	}
	else
	{
		waveCtrl.WaveCount = 1;
		OS_SetTimerPeriod(&timer_CountLimit, (waveCtrl.WaveCycleTimeLimit+200)/25);
		OS_RetriggerTimer(&timer_CountLimit);//启动软定时
	}
}


void WaveCtrl_Stop(void)
{
	ShellCmdPrintf("WaveCtrl_Stop !!!\n");
	OS_StopTimer(&timer_CountLimit);
	OS_StopTimer(&timer_TimeLimit);
	api_WaveStop();
	//AMPMUTE_RESET();
	AMP_RING_CLOSE();

	WaveSetState(WAVE_STATE_STOP);
	waveCtrl.WaveCycleState   = CYCLE_STATE_STOP;
	waveCtrl.WaveCount        = 0;
	waveCtrl.WaveTime         = 0;
}

unsigned char WaveGetState(void)
{
	return waveCtrl.WaveState;
}

void WaveSetState(unsigned char state)
{
	waveCtrl.WaveState = state;
}
