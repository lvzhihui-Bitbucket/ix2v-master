#include "cJSON.h"
#include "define_file.h"
#include "task_Audio.h"
#include "obj_Ring.h"
#include "obj_WavePlayControl.h"
#include "obj_GetDeviceTypeAndArea.h"
#include "elog.h"

char tempScene[50];	
char* Get_CallRingScene_ByAddr(char* addr)
{
	char* result = NULL;
	DeviceTypeAndArea_T type_area;
	type_area = GetDeviceTypeAndAreaByNumber(addr);
	if( type_area.type == TYPE_DS )
	{
		if( type_area.area == CommonArea )
		{
			snprintf(tempScene, 50, "CDS_TUNE_SELECT");
		}
		else
		{
			snprintf(tempScene, 50, "DS_TUNE_SELECT");
		}
	}
	if( type_area.type == TYPE_OS )
	{
		snprintf(tempScene, 50, "OS_TUNE_SELECT");
	}
	else if( type_area.type == TYPE_IM || type_area.type == TYPE_GL )
	{
		if( type_area.type == TYPE_IM && type_area.area == SameRoom )
		{
			snprintf(tempScene, 50, "INNERCALL_TUNE_SELECT");
		}
		else
		{
			snprintf(tempScene, 50, "INTERCOM_TUNE_SELECT");
		}
	}
	else
	{
		snprintf(tempScene, 50, "DS_TUNE_SELECT");
	}
	result = tempScene;
	printf("Get_CallRingScene_ByAddr %s ret=%s\n", addr, result);
	return result;
}

void RingParse(cJSON* json)
{
	cJSON *root = NULL;
    cJSON * retJson = NULL;
    cJSON* childJson = NULL;
	char ringType[50];	
	char deviceAddr[50];	
	char buff[100];	
    char name[100]={0};
	int vol=0; 
	int time=0; 
	int count=0;    

	GetJsonDataPro(json, "RingType", ringType);
	GetJsonDataPro(json, "Call_DeviceAddr", deviceAddr);
    
    retJson = API_Para_Read_Public(ringType);//解析RingType固定参数
	sprintf(name, "%s_Vol", ringType);
	GetJsonDataPro(retJson, name, &vol);
	sprintf(name, "%s_Time", ringType);
	GetJsonDataPro(retJson, name, &time);
	sprintf(name, "%s_Count", ringType);
	GetJsonDataPro(retJson, name, &count);
	printf("RingParse start vol=%d, time=%d, count=%d\n", vol, time, count);
	name[0] = 0;
	if(strcmp(ringType, "RING_DOORBELL") == 0)
	{
		API_Para_Read_String("DOORBELL_TUNE_SELECT", name);
	}
	else if(strcmp(ringType, "RING_MSG") == 0)
	{
		API_Para_Read_String("MSG_TUNE_SELECT", name);
	}
	else if(strcmp(ringType, "RING_ALARM") == 0)
	{
		API_Para_Read_String("ALARM_TUNE_SELECT", name);
		printf("RING_ALARM tune =%s\n", name);
	}
	else
	{
		retJson = API_Para_Read_Public("RingTuneByCallerID");//解析个性化铃声By地址
		if(retJson != NULL && deviceAddr != NULL)
		{
			cJSON_ArrayForEach(childJson, retJson)
			{
				printf("para RingTuneByCallerID addr=%s\n", deviceAddr);
				cJSON *value = cJSON_GetObjectItemCaseSensitive(childJson, deviceAddr);
				if(value != NULL)
				{
					printf("Checking addr %s %s\n", value->valuestring, value->string);
					break;
				}
			}
		}
		printf("RingFile1 =%s\n", name);
		if(name[0] == 0)//未解析到个性化铃声，取RingSceneTuneDefine参数
		{
			API_Para_Read_String(Get_CallRingScene_ByAddr(deviceAddr), name);
			printf("RingFile2 =%s\n", name);
		}
	}
	
    snprintf(buff, 100, "%s%s", RING_Folder, name);
    root = cJSON_CreateObject();	
    cJSON_AddStringToObject(root, "RingFile", buff);
    cJSON_AddNumberToObject(root, "RingVol", vol);
    cJSON_AddNumberToObject(root, "RingTime", time);
    cJSON_AddNumberToObject(root, "RingCount", count);
    cJSON_AddNumberToObject(root, "RingGap", API_Para_Read_Int("RingGap"));
 	
	WaveCtrl_Start(root);
	cJSON_Delete(root);
}

void RingVolSet(unsigned char iVol)
{
	char buff[100];	
    cJSON *root = NULL;

    root = cJSON_CreateObject();	
    snprintf(buff, 100, "%s%s", RING_Folder, "Popular.wav");
    cJSON_AddStringToObject(root, "RingFile", buff);
    cJSON_AddNumberToObject(root, "RingVol", iVol);
    cJSON_AddNumberToObject(root, "RingCount", 1);
 	
	WaveCtrl_Start(root); 
	cJSON_Delete(root);
}

int API_RingTunePlay2(char* tune)
{
	char buff[100];	
    cJSON *root = NULL;

    root = cJSON_CreateObject();	
    snprintf(buff, 100, "%s%s", RING_Folder, tune);
    cJSON_AddStringToObject(root, "RingFile", buff);
    cJSON_AddNumberToObject(root, "RingVol", 5);
    cJSON_AddNumberToObject(root, "RingCount", 1);
 	
	WaveCtrl_Start(root); 
	cJSON_Delete(root);
}


int API_RingVolPlay(unsigned char iVol)
{
	RingVolSet(iVol);
	#if 0
    WAVEPLAY_STRUCT msgRing;

	msgRing.head.msg_source_id  = 0;
	msgRing.head.msg_target_id  = MSG_ID_Audio;
	msgRing.head.msg_type 		= MSG_TYPE_RING;
	msgRing.head.msg_sub_type 	= RING_SET_VOLUME;
	msgRing.cmd				    = iVol;

	// 压入本地队列
	push_vdp_common_queue(&vdp_audio_mesg_queue, (char*)&msgRing, sizeof(WAVEPLAY_STRUCT));
	#endif
	return 0;
}

int API_RingStop(void)
{
	//WaveCtrl_Stop();
	#if 1
    WAVEPLAY_STRUCT msgRing;

	msgRing.head.msg_source_id  = 0;
	msgRing.head.msg_target_id  = MSG_ID_Audio;
	msgRing.head.msg_type 		= MSG_TYPE_RING;
	msgRing.head.msg_sub_type 	= RING_STOP;

	// 压入本地队列
	push_vdp_common_queue(&vdp_audio_mesg_queue, (char*)&msgRing, sizeof(WAVEPLAY_STRUCT));
	#endif
	return 0;
}

/*
json参数：
 "RingType": "RING_CALL"
 "Call_DeviceAddr": ""
 */
int API_RingPlay(cJSON* json)
{
	RingParse(json);
	#if 0
	AUDIO_JSON_STRUCT* msgRing;
    int msgLen;
    char *string=NULL;
    string=cJSON_Print(json);
	if(string==NULL)
		return -1;
    msgRing=(AUDIO_JSON_STRUCT *)malloc(sizeof(VDP_MSG_HEAD)+strlen(string)+2);
	if(msgRing==NULL)
		return -1;
	msgRing->head.msg_source_id    	= 0;
	msgRing->head.msg_target_id    	= MSG_ID_Audio;
	msgRing->head.msg_type 	    	= MSG_TYPE_RING;
	msgRing->head.msg_sub_type 		= RING_PLAY;
    strcpy(msgRing->json_str, string);
    msgLen = sizeof(VDP_MSG_HEAD)+strlen(string)+2;
	printf("API_RingPlay string2=%s len=%d\n", msgRing->json_str, msgLen);
	push_vdp_common_queue(&vdp_audio_mesg_queue, (char*)msgRing, msgLen);
    free(string);
    free(msgRing);
	#endif
	return 0;
}

/*
json参数：
 "RingType": "RING_CALL"
 "Call_DeviceAddr": ""
 */

int API_RingPlay2(const char* ringType, const char* ixAddr)
{
	int ret;
	cJSON* json = cJSON_CreateObject();
	cJSON_AddStringToObject(json, "RingType", ringType);
	cJSON_AddStringToObject(json, "Call_DeviceAddr", ixAddr);
	ret = API_RingPlay(json);
	cJSON_Delete(json);
	return ret;
}

/*
json参数：
 "RingFile": ""
 "RingVol": ""
 "RingTime": ""
 ...
 */
int API_RingTunePlay(cJSON* json)
{
	//WaveCtrl_Start(json);
	#if 1
	AUDIO_JSON_STRUCT* msgRing;
    int msgLen;
    char *string=NULL;
    string=cJSON_Print(json);
	if(string==NULL)
		return -1;

    msgRing=(AUDIO_JSON_STRUCT *)malloc(sizeof(VDP_MSG_HEAD)+strlen(string)+5);

	msgRing->head.msg_source_id    	= 0;
	msgRing->head.msg_target_id    	= MSG_ID_Audio;
	msgRing->head.msg_type 	    	= MSG_TYPE_RING;
	msgRing->head.msg_sub_type 		= RING_SET_TUNE;
    strcpy(msgRing->json_str, string);

    msgLen = sizeof(VDP_MSG_HEAD)+strlen(string)+5;
	push_vdp_common_queue(&vdp_audio_mesg_queue, (char*)msgRing, msgLen);
    free(string);
    free(msgRing);
	#endif
	return 0;
}

void API_RingPlay_test(void)
{
    cJSON* test = cJSON_CreateObject();
    cJSON_AddStringToObject(test, "RingType", "RING_CALL");
    cJSON_AddNumberToObject(test, "Call_DeviceAddr", 1);
	RingParse(test);
	cJSON_Delete(test);
}
