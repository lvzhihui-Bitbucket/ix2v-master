/**
  ******************************************************************************
  * @file    task_IxProxy.c
  * @author  lvzhihui
  * @version V1.0.0
  * @date    2016.04.15
  * @brief   This file contains the functions of task_ix_proxy
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2012 V-Tec</center></h2>
  ******************************************************************************
  */
  
#include <sys/types.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <error.h>
#include <signal.h>
#include <pthread.h>
#include <semaphore.h>
#include <sched.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <errno.h>
#include <dirent.h>

#include "one_tiny_task.h"
#include "task_IxProxy.h"
#include "tcp_server_process.h"
#include "obj_CmdGetAbout.h"
#include "obj_CmdReboot.h"
#include "obj_CmdParaRW.h"
#include "obj_CmdGetInfo.h"
#include "obj_CmdRing.h"
#include "obj_CmdProg.h"
#include "obj_CmdR8012.h"
#include "obj_CmdProgIpAddr.h"
#include "obj_CmdResDownload.h"
#include "obj_CmdGetVersion.h"
#include "obj_CmdGetMergeInfo.h"
#include "obj_CmdSettingFromR8001.h"
#include "obj_CmdBackup.h"
#include "obj_CmdRestore.h"
#include "define_string.h"
#include "obj_PublicInformation.h"
#include "elog_forcall.h"

Loop_vdp_common_buffer	vdp_ix_proxy_mesg_queue;
Loop_vdp_common_buffer	vdp_ix_proxy_sync_queue;
vdp_task_t				task_ix_proxy = {.task_run_flag = 0};
static IxProxyData_T 	myProxyData;
static IxProxyRUN_T 	ixProxyRun = {.taskLock = PTHREAD_MUTEX_INITIALIZER, .lock = PTHREAD_MUTEX_INITIALIZER, .state = IX_PROXY_IDLE, .connect.info = NULL};

void SetIxProxyConnectState(CONNECT_STATE state, char* network, cJSON* info)
{
	char* stateStr;

	ixProxyRun.connect.state = state;
	strcpy(ixProxyRun.connect.network, network ? network : "");
	cJSON_Delete(ixProxyRun.connect.info);
	ixProxyRun.connect.info = cJSON_Duplicate(info, 1);

	switch(state)
	{
		case Idle:
			stateStr = DM_KEY_Idle;
			break;
		case Local:
			stateStr = DM_KEY_Local;
			break;
		case Remote:
			stateStr = DM_KEY_Remote;
			break;
		default:
			stateStr = NULL;
			break;
	}
	API_PublicInfo_Write_String(PB_XD_STATE, stateStr);

	if(state == Idle && GetIxProxyRebootFlag())
	{
		HardwareRestart("XD Proxy disconnect");
	}
}

const cJSON* GetIxProxyConnectState(void)
{
	static cJSON* proxyConnectState = NULL;
	char* stateStr;
	pthread_mutex_lock(&ixProxyRun.lock);
	IxProxyConnect connect = ixProxyRun.connect;
	pthread_mutex_unlock(&ixProxyRun.lock);

	if(proxyConnectState)
	{
		cJSON_Delete(proxyConnectState);
	}

	proxyConnectState = cJSON_CreateObject();
	switch(connect.state)
	{
		case Idle:
			stateStr = DM_KEY_Idle;
			break;
		case Local:
			stateStr = DM_KEY_Local;
			break;
		case Remote:
			stateStr = DM_KEY_Remote;
			break;
	}
	cJSON_AddStringToObject(proxyConnectState, DM_KEY_ProxyState, stateStr);
	cJSON_AddStringToObject(proxyConnectState, DM_KEY_ProxyNetwork, connect.network);
	cJSON_AddItemToObject(proxyConnectState, DM_KEY_ProxyInfo, cJSON_Duplicate(connect.info, 1));

	return proxyConnectState;
}

void SetIxProxyState(IX_PROXY_STATE state)
{
	pthread_mutex_lock(&ixProxyRun.lock);
	ixProxyRun.state = state;
	pthread_mutex_unlock(&ixProxyRun.lock);
}

void SetIxProxyRebootFlag(int flag)
{
	pthread_mutex_lock(&ixProxyRun.lock);
	ixProxyRun.rebootFlag = flag;
	pthread_mutex_unlock(&ixProxyRun.lock);
}
int GetIxProxyRebootFlag(void)
{
	return ixProxyRun.rebootFlag;
}

IX_PROXY_STATE GetIxProxyState(void)
{
	IX_PROXY_STATE state;
	pthread_mutex_lock(&ixProxyRun.lock);
	state = ixProxyRun.state;
	pthread_mutex_unlock(&ixProxyRun.lock);
	return state;
}

void SetIxProxyRSP_ID(int RSP_ID)
{
	ixProxyRun.RSP_ID = RSP_ID;
}

int IncreaseIxProxyRSP_ID(void)
{
	return ixProxyRun.RSP_ID++;
}

int GetIxProxyRSP_ID(void)
{
	return ixProxyRun.RSP_ID;
}

void ProxyTask_pthread_mutex_lock(void)
{
	pthread_mutex_lock(&ixProxyRun.taskLock);
}

void ProxyTask_pthread_mutex_unlock(void)
{
	pthread_mutex_unlock(&ixProxyRun.taskLock);
}

void start_vdp_ix_proxy_task(void)
{
	ProxyTask_pthread_mutex_lock();
	if(!task_ix_proxy.task_run_flag)
	{
		init_vdp_common_queue(&vdp_ix_proxy_mesg_queue, 5000, vdp_ix_proxy_mesg_data_process, &task_ix_proxy);
		init_vdp_common_queue(&vdp_ix_proxy_sync_queue, 2000, NULL, &task_ix_proxy);
		init_vdp_common_task(&task_ix_proxy, MSG_ID_IX_PROXY, vdp_public_task, &vdp_ix_proxy_mesg_queue, &vdp_ix_proxy_sync_queue);
		
		SetIxProxyState(IX_PROXY_IDLE);
		MakeIxDeviceFileDir();
		task_ix_proxy.task_StartCompleted = 1;
	}
	ProxyTask_pthread_mutex_unlock();
}

void exit_vdp_ix_proxy_task(void)
{
	ProxyTask_pthread_mutex_lock();
	if(task_ix_proxy.task_run_flag)
	{
		exit_vdp_common_queue(&vdp_ix_proxy_mesg_queue);
		exit_vdp_common_queue(&vdp_ix_proxy_sync_queue);
		exit_vdp_common_task(&task_ix_proxy);	
	}
	ProxyTask_pthread_mutex_unlock();
}


IxProxyData_T GetMyProxyData(void)
{
	snprintf(myProxyData.CacheNbr, IX_BUILDER_NAME_LEN, "%d", GetR8012CacheTableNbr());
	strncpy(myProxyData.CacheTime, GetR8012CacheTableTime(), IX_BUILDER_NAME_LEN);

	return myProxyData;
}

int IsMyProxyLinked(void)
{
	return ixProxyRun.connect.state;
}

const char*  GetMyProxyLinked(void)
{
	return myProxyData.linked;
}

void SetMyProxyUnlinked(void)
{
	SetMyProxyData(NULL, NULL, "0", NULL);
	SetIxProxyConnectState(Idle, NULL, NULL);
}

const char* GetMyProxy_IP(void)
{
	return myProxyData.ip_addr;
}

void SetMyProxyData(char* version, char* priority, char* linked, char* netCard)
{
	char* deviceType;
	char* pIpChar;
	
	if(version)
	{
		strncpy(myProxyData.version, version, IX_BUILDER_NAME_LEN);
	}
	
	if(priority)
	{
		strncpy(myProxyData.priority, priority, IX_BUILDER_NAME_LEN);
	}
	
	if(linked)
	{
		strncpy(myProxyData.linked, linked, IX_BUILDER_NAME_LEN*2);
	}

	if((pIpChar = strchr(myProxyData.linked, '@')) != NULL)
	{
		strncpy(myProxyData.ip_addr, GetSysVerInfo_IP_by_device(netCard), IX_BUILDER_NAME_LEN);
	}
	else
	{
		myProxyData.ip_addr[0] = 0;
	}
	
	strncpy(myProxyData.device_addr, GetSysVerInfo_BdRmMs(), IX_BUILDER_NAME_LEN);
	deviceType = DeviceTypeToString(GetSysVerInfo_MyDeviceType());
	strncpy(myProxyData.deviceType, (deviceType == NULL) ? "" : deviceType, IX_BUILDER_NAME_LEN);

	strncpy(myProxyData.MFG_SN, GetSysVerInfo_Sn(), IX_BUILDER_NAME_LEN);
	strncpy(myProxyData.name, GetSysVerInfo_name(), IX_BUILDER_NAME_LEN);
	strncpy(myProxyData.name2, GetSysVerInfo_name(), IX_BUILDER_NAME_LEN*2);

	if(linked != NULL && !strcmp(linked, "0") && GetIxProxyRebootFlag())
	{
		HardwareRestart("XD Proxy disconnect");
	}
}

static void vdp_ix_proxy_mesg_data_process(char* msg_data,int len)
{
	IX_PROXY_MSG* pMsg = (IX_PROXY_MSG*)msg_data;
	IX_PROXY_TCP_HEAD* phead = (IX_PROXY_TCP_HEAD*)pMsg->data;
	char* cmdData = (char*)(pMsg->data + sizeof(IX_PROXY_TCP_HEAD));
	int dataLen = pMsg->dataLen;

	log_d("businessCode = %d, cmd_id = %d, phead->OP = 0x%04X, cmdData=%s\n", ixProxyRun.businessCode, ixProxyRun.cmd_id, ntohs(phead->OP), cmdData);

	SetIxProxyState(IX_PROXY_BUSY);

	ixProxyRun.businessCode = pMsg->businessCode;
	ixProxyRun.cmd_id = pMsg->cmd_id;
	

	switch (ntohs(phead->OP))
	{
		case REQ_DM_REBOOT:
			IxProxyRebootProcess(pMsg->data, dataLen);
			break;

		case REQ_DM_ABOUT:
			IxProxyGetAboutProcess(pMsg->data, dataLen);
			break;
		case REQ_DM_INFO:
			IxProxyGetInfoProcess(pMsg->data, dataLen);
			break;
		case REQ_DM_INFO2:
			IxProxyGetInfo2Process(pMsg->data, dataLen);
			break;
		case REQ_DM_R8012:
			IxProxyGetR8012Process(pMsg->data, dataLen);
			break;
		case REQ_DM_PARA_READ:
			IxProxyParameterReadProcess(pMsg->data, dataLen);
			break;
		case REQ_DM_PARA_WRITE:
			IxProxyParameterWriteProcess(pMsg->data, dataLen);
			break;
		case REQ_DM_PARA_DEFAULT:
			IxProxyParameterDefaultProcess(pMsg->data, dataLen);
			break;
		case REQ_IP_CHECK:
			//IX2V test IxProxyIpCheckProcess(pMsg->data, dataLen);
			break;

		case REQ_DM_RING:
			IxProxyRingProcess(pMsg->data, dataLen);
			break;
			
		case REQ_DM_PROG_CALL_NBR:
			IxProxyProgProcess(pMsg->data, dataLen);
			break;
			
		case REQ_DM_R8012_SCAN_NEW:
			IxProxyR8012ScanNewProcess(pMsg->data, dataLen);
			break;
		case REQ_DM_R8012_GET_CACHE:
			IxProxyR8012GetCacheTableProcess(pMsg->data, dataLen);
			break;
		case REQ_DM_R8012_CHECK:
			IxProxyR8012CheckProcess(pMsg->data, dataLen);
			break;
		case REQ_DM_GET_INFO_BY_MFG_SN:
			IxProxyGetInfoBySnProcess(pMsg->data, dataLen);
			break;
		case REQ_DM_PROG_IP_ADDR:
			IxProxyProgIpAddrProcess(pMsg->data, dataLen);
			break;
		case REQ_DM_RES_DOWNLOAD:
			IxProxyPCDownloadResProcess(pMsg->data, dataLen);
			break;
		case REQ_DM_GET_VERSION:
			IxProxyGetVersionProcess(pMsg->data, dataLen);
			break;
		case REQ_DM_GET_MERGE_INFO:
			IxProxyGetMergeInfoProcess(pMsg->data, dataLen);
			break;
		
		case REQ_DM_SETTING_FROM_R8001:
			IxProxySettingFromR8001Process(pMsg->data, dataLen);
			break;
		
		case REQ_DM_BACKUP:
			IxProxyBackupProcess(pMsg->data, dataLen);
			break;
		
		case REQ_DM_RESTORE:
			IxProxyRestoreProcess(pMsg->data, dataLen);
			break;

		case REQ_DM_FILE:
			IxProxyFileManagementProcess(pMsg->data, dataLen);
			break;

		case REQ_SHELL:
			IxProxyShellProcess(pMsg->data, dataLen);
			break;
			
		case REQ_EVENT_APP_TO_DEV:
			IxProxyEventProcess(pMsg->data, dataLen);
			break;
		case REQ_SEARCH_DEVICE:
			IxProxySearchDevices(pMsg->data, dataLen);
			break;

		case REQ_GET_PB_IO:
			IxProxyGetDeviceInfo(pMsg->data, dataLen);
			break;

		case REQ_READ_WRITE_IO:
			IXD_RwIoProcess(pMsg->data, dataLen);
			break;
		default:
			break;
	}
	
	SetIxProxyState(IX_PROXY_IDLE);
}

void API_IxProxy(short businessCode, short cmd_id, char* pData, int len)
{
	IX_PROXY_MSG msg;
	int msgLen;
	
	IX_PROXY_TCP_HEAD* phead = (IX_PROXY_TCP_HEAD*)pData;
	
	msg.cmd_id = cmd_id;
	msg.businessCode = businessCode;
	msg.dataLen = len;
	memcpy(msg.data, pData, len);

	msgLen = sizeof(msg) - CLIENT_CONNECT_BUF_MAX + msg.dataLen;
	
	if(ntohs(phead->Session_ID) == 0)
	{
		SetIxProxyState(IX_PROXY_IDLE);
		return;
	}

	// 压入本地队列
	push_vdp_common_queue(&vdp_ix_proxy_mesg_queue, (char*)&msg, msgLen);
}


void IxProxyRespone(unsigned short OP,unsigned short Session_ID, unsigned short RSP_ID, void* pCmdData)
{
	char* rspJson = NULL;
	IX_PROXY_TCP_HEAD head;
	
	head.OP = OP;

	if(!(head.OP & 0x8000))
	{
		head.OP |= 0x8000;
	}
	
	head.Session_ID = Session_ID;
	head.RSP_ID = RSP_ID;

	if(pCmdData != NULL)
	{
		switch(head.OP)
		{
			case RSP_DM_REBOOT:
				rspJson = CreateRebootRspJsonData((REBOOT_RSP_DATA_T*)pCmdData);
				break;
				
			case RSP_DM_ABOUT:
				rspJson = CreateGetAboutRspJsonData((GET_ABOUT_RSP_DATA_T*)pCmdData);
				break;
				
			case RSP_DM_INFO:
				rspJson = CreateGetInfoRspJsonData((DeviceInfo*)pCmdData);
				break;
				
			case RSP_DM_INFO2:
				rspJson = CreateGetInfo2RspJsonData((DeviceInfo2*)pCmdData);
				break;
				
			case RSP_DM_R8012:
			case RSP_DM_R8012_SCAN_NEW:
				rspJson = CreateGetR8012RspJsonData((R8012_Device*)pCmdData);
				break;
				
			case RSP_DM_PARA_READ:
			case RSP_DM_PARA_WRITE:
				rspJson = CreateParameterRspJsonData((PARAMETER_RSP_DATA_T*)pCmdData);
				break;
				
			case RSP_DM_PARA_DEFAULT:
				rspJson = CreateParameterDefaultRspJsonData((PARAMETER_RSP_DATA_T*)pCmdData);
				break;
				
			case RSP_IP_CHECK:
				//IX2V test rspJson = CreateIpCheckRspJsonData((IP_CHECK_RSP_DATA_T*)pCmdData);
				break;
			case RSP_DM_RING:
				rspJson = CreateRingRspJsonData((RING_RSP_DATA_T*)pCmdData);
				break;
				
			case RSP_DM_PROG_CALL_NBR:
				rspJson = CreateProgRspJsonData((PROG_RSP_DATA_T*)pCmdData);
				break;

			#if (!USE_SEARCH_TABLE)
			case RSP_DM_R8012_GET_CACHE:
				rspJson = CreateR8012GetCacheTableRspJsonData((R8012_DeviceTable*)pCmdData);
				break;
			#endif

			case RSP_DM_R8012_CHECK:
				rspJson = CreateR8012CheckRspJsonData((CHECK_R8012_RSP_DATA_T*)pCmdData);
				break;
				
			case RSP_DM_GET_INFO_BY_MFG_SN:
				rspJson = CreateGetInfoBySnRspJsonData((DeviceInfo3*)pCmdData);
				break;
				
			case RSP_DM_PROG_IP_ADDR:
				rspJson = CreateProgIpAddrRspJsonData((PROG_IP_ADDR_RSP_DATA_T*)pCmdData);
				break;
			case RSP_DM_GET_VERSION:
				rspJson = CreateGetVersionRspJsonData((VERSION_RSP_T*)pCmdData);
				break;
			case RSP_DM_GET_MERGE_INFO:
				rspJson = CreateGetMergeInfoRspJsonData((MERGE_INFO_RSP_DATA*)pCmdData);
				break;	
			
			case RSP_DM_SETTING_FROM_R8001:
				rspJson = CreateSettingFromR8001RspJsonData((SETTING_FROM_R8001_RSP_DATA*)pCmdData);
				break;
			case RSP_DM_BACKUP:
				rspJson = CreateBackupRspJsonData((CMD_BACKUP_ONE_RSP_T*)pCmdData);
				break;
			case RSP_DM_RESTORE:
				rspJson = CreateRestoreRspJsonData((CMD_RESTORE_ONE_RSP_T*)pCmdData);
				break;
				
			case RSP_SHELL:
			case RSP_EVENT_APP_TO_DEV:
				rspJson = cJSON_Print((cJSON*)pCmdData);
				break;

			#if USE_SEARCH_TABLE
			case RSP_DM_R8012_GET_CACHE:
			#endif
			case RSP_SEARCH_DEVICE:
			case RSP_GET_PB_IO:
				rspJson = cJSON_PrintUnformatted((cJSON*)pCmdData);
				break;
				
			default:
				rspJson = pCmdData;
				break;
		}
	}
	
	if(rspJson != NULL)
	{
		ResponseClient(head, ixProxyRun.cmd_id, ixProxyRun.businessCode, rspJson, strlen(rspJson)+1);
		free(rspJson);
	}
	else
	{
		ResponseClient(head, ixProxyRun.cmd_id, ixProxyRun.businessCode, NULL, 0);
	}
}

void IxProxyResponeNew(unsigned short OP,unsigned short Session_ID, unsigned short RSP_ID, char* jsonString, int jsonStringLen)
{
	IX_PROXY_TCP_HEAD head;
	int sendLen, packCnt, i;
	char sendData[IX_DEVICE_JSON_DATA_LEN_MAX+3];

	head.OP = OP;

	if(!(head.OP & 0x8000))
	{
		head.OP |= 0x8000;
	}
	
	head.Session_ID = Session_ID;
	head.RSP_ID = RSP_ID;
	if(jsonString != NULL)
	{
		packCnt = jsonStringLen/IX_DEVICE_JSON_DATA_LEN_MAX+1;
		for(i = 0; i < packCnt; i++)
		{
			sendLen = (jsonStringLen - IX_DEVICE_JSON_DATA_LEN_MAX*i > IX_DEVICE_JSON_DATA_LEN_MAX ? IX_DEVICE_JSON_DATA_LEN_MAX : jsonStringLen - IX_DEVICE_JSON_DATA_LEN_MAX*i);
			sendData[0] = i+1;
			sendData[1] = packCnt;
			memcpy(sendData+2, jsonString+IX_DEVICE_JSON_DATA_LEN_MAX*i, sendLen);
			sendData[sendLen+2] = 0;
			//dprintf("op=0x%04x, %d/%d, data=%s\n\n", head.OP, sendData[0], sendData[1], sendData+2);
			CMD_RSP_DELAY();
			ResponseClient(head, ixProxyRun.cmd_id, ixProxyRun.businessCode, sendData, sendLen+2);
		}
	}
	else
	{
		CMD_RSP_DELAY();
		ResponseClient(head, ixProxyRun.cmd_id, ixProxyRun.businessCode, NULL, 0);
	}
}


void MakeIxDeviceFileDir(void)
{
	DIR *dir;
	char cmd_buff[200];
	
	dir = opendir(IxDeviceFileDir);

	if(dir == NULL)
	{
		MakeDir(IxDeviceFileDir);
	}
	else
	{
		closedir(dir);
	}
	
	dir = opendir(IxDeviceTempFileDir);
	
	if(dir == NULL)
	{
		MakeDir(IxDeviceTempFileDir);
	}
	else
	{
		closedir(dir);
	}
	
	snprintf(cmd_buff, 200, "rm -r %s/*", IxDeviceTempFileDir);
	system(cmd_buff);
	sync();
}

char* GetErrorCodeByResult(int result)
{
	char* retCode;

	switch(result)
	{
		case 0:
			retCode = RESULT_SUCC;
			break;

		case -1:
			retCode = RESULT_ERR01;
			break;
		case -2:
			retCode = RESULT_ERR02;
			break;
		case -3:
			retCode = RESULT_ERR03;
			break;
		case -4:
			retCode = RESULT_ERR04;
			break;
		case -5:
			retCode = RESULT_ERR05;
			break;
		case -6:
			retCode = RESULT_ERR06;
			break;
		case -7:
			retCode = RESULT_ERR07;
			break;
		case -10:
			retCode = RESULT_ERR10;
			break;

		default:
			retCode = RESULT_ERR07;
			break;
	}

	return retCode;
}
