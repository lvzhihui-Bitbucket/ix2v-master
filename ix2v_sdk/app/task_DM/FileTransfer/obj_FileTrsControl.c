#include <sys/types.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <error.h>
#include <signal.h>
#include <pthread.h>
#include <semaphore.h>
#include <sched.h>
#include <dirent.h>
#include "OSTIME.h"

#include "obj_FileTrsCmd.h"
#include "obj_FileTrsControl.h"
#include "task_IxProxy.h"
#include "ota_FileProcess.h"


void FileTrsResFileSave(int rid);

static FileTrsCtrl_Run_Stru FileTrsCtrl_Run;

int GetIXDeviceTrsFileLen(char* filepath)
{
	int ret;
	
	FILE *pf = fopen(filepath,"rb");
	
	if(pf == NULL)
	{
		ret = -1;
	}
	else
	{
		fseek(pf, 0, SEEK_END);
		ret = ftell(pf);
		fclose(pf);
	}

	return ret;
}

int ResUpdateProcess(int ResId, char* dir, char* fileName, int * pRebootFlag)
{
	int ret = 0;
	
	ret = UDPDownloadFwUpdate(dir, fileName, pRebootFlag);
	
	//dprintf("ResId=%d, ret=%d, dir = %s, fileName=%s\n", ResId, ret, dir, fileName);
	//ret = ResReloadById(ResId);
	return ret;
}

int UDPDownloadFwUpdate(char *filePath, char *fileName, int* pRebootFlag)
{
	char cmd[200];
	char *downloadPath;
	int ret;
	char zipFileName[50];
	char txtFileName[50];
	char tempFileName[50];
	char* pReadBuf[2];
	OtaDlTxt_t txtInfo;

	pReadBuf[0] = txtFileName;
	pReadBuf[1] = zipFileName;

	snprintf(cmd, 200, "unzip -l %s/%s | awk -F ' ' '{print $NF}' | awk 'NR==4 || NR==5'", filePath, fileName);
	
	if(PopenProcess(cmd, pReadBuf, 50, 2) != 2)
	{
		ret = -2;
	}
	else
	{
		downloadPath = (Judge_SdCardLink() ? SDCARD_DOWNLOAD_PATH : NAND_DOWNLOAD_PATH);
		MakeDir(downloadPath);
		
		snprintf(cmd, 200, "unzip -o %s/%s -d %s", filePath, fileName, downloadPath);
		system(cmd);

		if(strcmp(txtFileName + strlen(txtFileName) - strlen(".txt"), ".txt"))
		{
			strcpy(tempFileName, zipFileName);
			strcpy(zipFileName, txtFileName);
			strcpy(txtFileName, tempFileName);
		}

		snprintf(cmd, 200, "%s/%s", downloadPath, txtFileName);

		if(CheckDownloadIsAllowed(cmd, 0, &txtInfo))
		{
			snprintf(cmd, 200, "%s/%s", downloadPath, zipFileName);
			ret = InstallDownloadZip(txtInfo.dlType, cmd, pRebootFlag);
		}
		else
		{
			ret = -1;
		}
		DeleteFileProcess(NULL, cmd);
	}
	
	DeleteFileProcess(filePath, fileName);

	return ret;
}

int GetFileMd5_Check(char *file_path,unsigned char *file_md5)
{
	if(FileMd5_Calculate(file_path,file_md5) != 0)
	{
		return -1;
	}
	
	return 0;
}

int FileMd5_Check(char *file_path,unsigned char *file_md5)
{
	unsigned char file_md5_calculate[16];
	
	if(FileMd5_Calculate(file_path,file_md5_calculate) != 0)
	{
		dprintf("FileMd5_Calculate fail\n");
		return -1;
	}
	int i;
	if(memcmp(file_md5,file_md5_calculate,16) != 0)
	{
		dprintf("FileMd5_cmp fail:rxmd5:");
		for(i = 0; i< 16 ;i++)
		{
			printf("%02x",file_md5[i]);
		}
		dprintf("calmd5:");
		for(i = 0; i< 16 ;i++)
		{
			printf("%02x",file_md5_calculate[i]);
		}
		printf("\n");
		return -1;
	}
	
	return 0;
}

void FileTrsServer_Init(void)
{
	char cmd_line[250]={0};
	FileTrsCtrl_Run.wr_task_cnt		= 0;
	FileTrsCtrl_Run.wr_task		= NULL;
	FileTrsCtrl_Run.rd_task_cnt		= 0;
	FileTrsCtrl_Run.rd_task			= NULL;
	pthread_mutex_init( &FileTrsCtrl_Run.lock, 0);
	OS_CreateTimer(&FileTrsCtrl_Run.wait_timer, FileTrsCtrl_Timer_Callback, FileTrsCtrl_Timer_Gap*1000/25);
	OS_RetriggerTimer(&FileTrsCtrl_Run.wait_timer);

	FileSync_Init();
}

int FileTrsCtrl_SendReq(int cmd_id, int server_ip,int id,int rid, char* saveDir, char* tempDir, char *file_name, char *file_md5, int file_len, int pack_len)
{
	unsigned char result;

	int i;
	FileTrs_TaskCtrl_Stru 	*ptrs_task;
	
	pthread_mutex_lock( &FileTrsCtrl_Run.lock);

	for(i = 0,ptrs_task = FileTrsCtrl_Run.wr_task;i < FileTrsCtrl_Run.wr_task_cnt && ptrs_task != NULL; i++)
	{ 
		if(ptrs_task->rid == rid)
		{
			if(ptrs_task->server_ip != server_ip)
			{
				result = 1;
				goto FileTrsCtrl_SendReq_End;
			}
			if(memcmp(file_md5,ptrs_task->file_md5,16) == 0)
			{
				ptrs_task->id = id;
				result = 0;
				goto FileTrsCtrl_SendReq_End;
			}
			
			FileTrsCtrl_DestroySendTask(rid,1);
			
			FileSync_Write_Release(rid);
			break;
		}
		ptrs_task = ptrs_task->next;
	}
	
	if(FileSync_Write(rid) != 0)
	{
		result = 1;
		goto FileTrsCtrl_SendReq_End;
	}

	if(FileTrsCtrl_CreateNewSendTask(server_ip,id,rid,saveDir, tempDir,file_name,file_md5,file_len,pack_len) == 0)
	{
		result = 0;
	}
	else
	{
		FileSync_Write_Release(rid);
		result = 1;
	}
FileTrsCtrl_SendReq_End:	
	pthread_mutex_unlock( &FileTrsCtrl_Run.lock);
	
	FileTrs_SendReqRsp(cmd_id, server_ip,id,rid,result,pack_len);
	return result;
}



int FileTrsCtrl_SendOnePack(int cmd_id, int server_ip,int id,int rid,int pack_no,int pack_len,unsigned char *data, int* packetNum)
{
	int i;
	int result = -1;
	FileTrs_TaskCtrl_Stru 	*ptrs_task;
	
	pthread_mutex_lock( &FileTrsCtrl_Run.lock);
	
	for(i = 0,ptrs_task = FileTrsCtrl_Run.wr_task;i < FileTrsCtrl_Run.wr_task_cnt && ptrs_task != NULL; i++)
	{
		if(ptrs_task->rid == rid)
		{
			ptrs_task->id = id;
			ptrs_task->timer = 0;
			result = FileTrsDataOpt_WriteOnePack(cmd_id, ptrs_task->data_opt_obj,pack_no,pack_len,data, packetNum);
			break;
		}
		ptrs_task = ptrs_task->next;
	}
	
	pthread_mutex_unlock( &FileTrsCtrl_Run.lock);
	
	return result;
}


int FileTrsCtrl_SendEnd(int cmd_id, int server_ip,int id,int rid, char* dir, char* fileName)
{
	int i;
	FileTrs_TaskCtrl_Stru 	*ptrs_task,trs_task_temp;
	unsigned char 		result = 0;
	char filepath[IX_DEVICE_FILE_LEN+1]={0};
	char prefilepath[IX_DEVICE_FILE_LEN+1]={0};
	char cmd_line[250]={0};
	
	pthread_mutex_lock( &FileTrsCtrl_Run.lock);
	
	for(i = 0,ptrs_task = FileTrsCtrl_Run.wr_task;i < FileTrsCtrl_Run.wr_task_cnt && ptrs_task != NULL; i++)
	{
		if(ptrs_task->rid == rid && ptrs_task->server_ip == server_ip)
		{
			ptrs_task->id = id;
			memcpy(&trs_task_temp,ptrs_task,sizeof(FileTrs_TaskCtrl_Stru));
			
			if(FileTrsCtrl_DestroySendTask(rid,0) != 0)
			{
				result = 1;
				DeleteFileProcess(trs_task_temp.file_dir, trs_task_temp.file_name);
				break;
			}
			FileSync_Write_Release(rid);
			
			snprintf(filepath,IX_DEVICE_FILE_LEN,"%s%s",trs_task_temp.file_dir,trs_task_temp.file_name);
			if(FileMd5_Check(filepath,trs_task_temp.file_md5) != 0)
			{
				bprintf("FileMd5_Check Fail\n");
				DeleteFileProcess(trs_task_temp.file_dir, trs_task_temp.file_name);
				result = 2;
				break;
			}

			//本地写文件
			if(cmd_id == -1)
			{
				sprintf(cmd_line,"mv %s/%s %s/%s", trs_task_temp.file_dir, trs_task_temp.file_name, trs_task_temp.saveDir, trs_task_temp.file_name);
				system(cmd_line);
				sync();
			}
			//远程写文件
			else
			{
				if(server_ip == inet_addr(GetSysVerInfo_IP_by_device(GetNetDeviceNameByTargetIp(server_ip))))
				{
					sprintf(cmd_line,"mv %s/%s %s/%s", trs_task_temp.file_dir, trs_task_temp.file_name, trs_task_temp.saveDir, trs_task_temp.file_name);
					system(cmd_line);
					sync();
				}
				else
				{
					result = Api_TftpSendFileToDevice(server_ip, trs_task_temp.file_dir, trs_task_temp.file_name, trs_task_temp.saveDir, trs_task_temp.file_name);
					DeleteFileProcess(trs_task_temp.file_dir, trs_task_temp.file_name);
				}
			}

			if(dir != NULL)
			{
				strcpy(dir, trs_task_temp.file_dir);
			}

			if(fileName != NULL)
			{
				strcpy(fileName, trs_task_temp.file_name);
			}
			break;
		}
		ptrs_task = ptrs_task->next;
		
	}
	
	pthread_mutex_unlock( &FileTrsCtrl_Run.lock);
	
	FileTrs_SendEndRsp(cmd_id, server_ip,id,rid,result);

	return  result;
}


int FileTrsCtrl_SendCancel(int cmd_id, int server_ip,int id,int rid)
{
	int i;
	FileTrs_TaskCtrl_Stru 	*ptrs_task;
	unsigned char 		result = 0;
	pthread_mutex_lock( &FileTrsCtrl_Run.lock);
	
	for(i = 0,ptrs_task = FileTrsCtrl_Run.wr_task;i < FileTrsCtrl_Run.wr_task_cnt && ptrs_task != NULL; i++)
	{
		if(ptrs_task->rid == rid)
		{
			ptrs_task->id = id;
			if(FileTrsCtrl_DestroySendTask(rid,1) != 0)
			{
				result = 1;
			}
			FileSync_Write_Release(rid);		//czn_20170224
			break;
		}
		ptrs_task = ptrs_task->next;
		
	}
	
	pthread_mutex_unlock( &FileTrsCtrl_Run.lock);
	
	FileTrs_SendCancelRsp(cmd_id, server_ip,id,rid,result);
	return result;
}

int FileTrsCtrl_ReadReq(int cmd_id, int server_ip,int id,int rid, char* dir, char* tempDir, char* fileName)
{
	unsigned char result = 0,file_md5[16];
	int file_len = 0, pack_len = 0;
	char filepath[IX_DEVICE_FILE_LEN+1]={0},cmd_line[250]={0};

	int i;
	FileTrs_TaskCtrl_Stru 	*ptrs_task;
	
	pthread_mutex_lock( &FileTrsCtrl_Run.lock);


	for(i = 0,ptrs_task = FileTrsCtrl_Run.rd_task;i < FileTrsCtrl_Run.rd_task_cnt && ptrs_task != NULL; i++)
	{ 
		if(ptrs_task->rid == rid)
		{
			if(ptrs_task->server_ip != server_ip)
			{
				result = 1;
				goto FileTrsCtrl_ReadReqRsp;
			}
			result = 0;
			file_len = ptrs_task->data_opt_obj->file_len;
			pack_len = ptrs_task->data_opt_obj->pack_len;
			memcpy(file_md5,ptrs_task->file_md5,16);
			goto FileTrsCtrl_ReadReqRsp;
		}
		ptrs_task = ptrs_task->next;
	}


	if(FileSync_Read(rid) != 0)
	{
		result = 1;
		goto FileTrsCtrl_ReadReqRsp;
	}

	DeleteFileProcess(tempDir, fileName);
	snprintf(filepath, IX_DEVICE_FILE_LEN, "%s/%s", tempDir, fileName);

	//本地读取
	if(cmd_id == -1)
	{
		snprintf(cmd_line, 250, "cp %s/%s %s", dir, fileName, filepath);
		result = system(cmd_line);
	}
	//远程读取
	else
	{
		if(server_ip == inet_addr(GetSysVerInfo_IP_by_device(GetNetDeviceNameByTargetIp(server_ip))))
		{
			snprintf(cmd_line, 250, "cp %s/%s %s", dir, fileName, filepath);
			result = system(cmd_line);
		}
		else
		{
			result = Api_TftpReadFileFromDevice(server_ip, dir, fileName, tempDir, fileName);
			dprintf("result=%d, rid=%d, src=%s%s, tar=%s%s\n", result, rid, dir, fileName, tempDir, fileName);
		}
	}
	if(result != 0)
	{
		result = 1;
		FileSync_Read_Release(rid);
		dprintf("Copy file error!!!\n");
		goto FileTrsCtrl_ReadReqRsp;
	}

	file_len = GetIXDeviceTrsFileLen(filepath);
	if(file_len < 0)
	{
		result = 1;
		remove(filepath);
		FileSync_Read_Release(rid);
		dprintf("File error!!!, filepath=%s\n", filepath);
		goto FileTrsCtrl_ReadReqRsp;
	}

	if(GetFileMd5_Check(filepath, file_md5) != 0)
	{
		result = 1;
		remove(filepath);
		FileSync_Read_Release(rid);
		dprintf("GetFileMd5_Check error!!!\n");
		goto FileTrsCtrl_ReadReqRsp;
	}

	pack_len = FILE_TRS_DEFAULT_PACK_LEN;

	if(FileTrsCtrl_CreateNewReadTask(server_ip, id, rid, tempDir, fileName,file_md5,file_len,pack_len) != 0)
	{
		result = 1;
		remove(filepath);
		FileSync_Read_Release(rid);
		dprintf("FileTrsCtrl_CreateNewReadTask error!!!\n");
		goto FileTrsCtrl_ReadReqRsp;
	}
	
FileTrsCtrl_ReadReqRsp:

	pthread_mutex_unlock( &FileTrsCtrl_Run.lock);
	FileTrs_ReadRsp(cmd_id, server_ip,id,rid,result,file_len,pack_len,file_md5,fileName);

	return 0;
}

int FileTrsCtrl_ReadOnePack(int cmd_id, int server_ip,int id,int rid,int pack_no)
{
	int i;
	FileTrs_TaskCtrl_Stru 	*ptrs_task;
	pthread_mutex_lock( &FileTrsCtrl_Run.lock);
	
	for(i = 0,ptrs_task = FileTrsCtrl_Run.rd_task;i < FileTrsCtrl_Run.rd_task_cnt && ptrs_task != NULL; i++)
	{
		if(ptrs_task->rid == rid)
		{
			ptrs_task->id = id;
			FileTrsDataOpt_ReadOnePack(cmd_id, ptrs_task->data_opt_obj,pack_no);
			ptrs_task->timer = 0;
			break;
		}
		ptrs_task = ptrs_task->next;
	}
	
	pthread_mutex_unlock( &FileTrsCtrl_Run.lock);
	
	return 0;
}
int FileTrsCtrl_ReadEnd(int cmd_id, int server_ip,int id,int rid)
{
	int i;
	FileTrs_TaskCtrl_Stru 	*ptrs_task;
	pthread_mutex_lock( &FileTrsCtrl_Run.lock);
	
	for(i = 0,ptrs_task = FileTrsCtrl_Run.rd_task;i < FileTrsCtrl_Run.rd_task_cnt && ptrs_task != NULL; i++)
	{
		if(ptrs_task->rid == rid)
		{
			ptrs_task->id = id;
			FileTrsCtrl_DestroyReadTask(rid,1);
			FileSync_Read_Release(rid);
			break;
		}
		ptrs_task = ptrs_task->next;
	}
	
	pthread_mutex_unlock( &FileTrsCtrl_Run.lock);
	
	FileTrs_ReadEndRsp(cmd_id, server_ip,id,rid);
	return 0;
}

int FileTrsCtrl_ReadCancel(int cmd_id, int server_ip,int id,int rid)
{
	int i;
	FileTrs_TaskCtrl_Stru 	*ptrs_task;
	unsigned char 		result = 0;
	pthread_mutex_lock( &FileTrsCtrl_Run.lock);
	
	for(i = 0,ptrs_task = FileTrsCtrl_Run.rd_task;i < FileTrsCtrl_Run.rd_task_cnt && ptrs_task != NULL; i++)
	{
		if(ptrs_task->rid == rid)
		{
			ptrs_task->id = id;
			if(FileTrsCtrl_DestroyReadTask(rid,1) != 0)
			{
				result = 1;
			}
			FileSync_Read_Release(rid);
			break;
		}
		ptrs_task = ptrs_task->next;
		
	}
	
	pthread_mutex_unlock( &FileTrsCtrl_Run.lock);
	
	FileTrs_ReadCancelRsp(cmd_id, server_ip,id,rid,result);
	return 0;
}

int FileTrsCtrl_CreateNewSendTask(int server_ip,int id,int rid,char* saveDir, char* tempDir, char *file_name,unsigned char *file_md5,int file_len,int pack_len)
{
	int i;
	FileTrs_TaskCtrl_Stru 	*ptrs_task;
	FileDataOptObj_Stru		*pdata_opt;
	
	for(i = 0,ptrs_task = FileTrsCtrl_Run.wr_task;i < FileTrsCtrl_Run.wr_task_cnt && ptrs_task != NULL; i++)
	{ 
		if(ptrs_task->rid == rid)
		{
			if(ptrs_task->server_ip != server_ip)
			{
				return -1;
			}
			if(memcmp(file_md5,ptrs_task->file_md5,16) == 0)
			{
				ptrs_task->id = id;
				return 0;
			}
			
			FileTrsCtrl_DestroySendTask(rid,1);
			break;
		}
		ptrs_task = ptrs_task->next;
	}

	if(FileTrsCtrl_Run.wr_task_cnt >= MAX_RSWRTASK_CNT)
	{
		return -1;
	}
	
	pdata_opt = FileTrsDataOpt_CreateNewWriteObj(tempDir, file_name,file_len,pack_len);
	
	if(pdata_opt == NULL)
	{
		return -1;
	}
	
	ptrs_task = (FileTrs_TaskCtrl_Stru 	*)malloc(sizeof(FileTrs_TaskCtrl_Stru));
	
	if(ptrs_task == NULL)
	{
		FileTrsDataOpt_DestroyOptObj(pdata_opt);
		return -1;
	}
	ptrs_task->rid 		= rid;
	ptrs_task->server_ip 	= server_ip;
	ptrs_task->id			= id;
	memcpy(ptrs_task->file_md5,file_md5,16);
	snprintf(ptrs_task->saveDir, MAX_FILE_TRS_DIR_LEN, "%s", saveDir);
	snprintf(ptrs_task->file_dir, MAX_FILE_TRS_DIR_LEN, "%s", tempDir);
	snprintf(ptrs_task->file_name, MAX_FILE_TRS_FILENAME_LEN, "%s", file_name);
	ptrs_task->data_opt_obj = pdata_opt;
	ptrs_task->timer = 0;

	FileTrsCtrl_Run.wr_task_cnt ++;
	ptrs_task->next = FileTrsCtrl_Run.wr_task;
	FileTrsCtrl_Run.wr_task = ptrs_task;

	bprintf("greatobj md5:");
	for(i= 0; i<16;i++)
	{
		printf("%02x",ptrs_task->file_md5[i]);
	}
	printf("\n");

	return 0;
}

int FileTrsCtrl_CreateNewReadTask(int server_ip,int id,int rid,char* dir, char *file_name,unsigned char *file_md5,int file_len,int pack_len)
{
	int i;
	FileTrs_TaskCtrl_Stru 	*ptrs_task;
	FileDataOptObj_Stru		*pdata_opt;
	
	for(i = 0,ptrs_task = FileTrsCtrl_Run.rd_task;i < FileTrsCtrl_Run.rd_task_cnt && ptrs_task != NULL; i++)
	{ 
		if(ptrs_task->rid == rid)
		{
			if(ptrs_task->server_ip != server_ip)
			{
				return -1;
			}
			ptrs_task->id = id;
			return 0;
		}
		ptrs_task = ptrs_task->next;
	}

	if(FileTrsCtrl_Run.rd_task_cnt >= MAX_RSRDTASK_CNT)
	{
		return -1;
	}
	
	pdata_opt = FileTrsDataOpt_CreateNewReadObj(dir, file_name,file_len,pack_len);
	
	if(pdata_opt == NULL)
	{
		return -1;
	}
	
	ptrs_task = (FileTrs_TaskCtrl_Stru 	*)malloc(sizeof(FileTrs_TaskCtrl_Stru));
	
	if(ptrs_task == NULL)
	{
		FileTrsDataOpt_DestroyOptObj(pdata_opt);
		return -1;
	}
	ptrs_task->rid 			= rid;
	ptrs_task->server_ip 	= server_ip;
	ptrs_task->id			= id;
	memcpy(ptrs_task->file_md5,file_md5,16);
	snprintf(ptrs_task->file_dir, MAX_FILE_TRS_DIR_LEN, "%s", dir);
	snprintf(ptrs_task->file_name, MAX_FILE_TRS_FILENAME_LEN, "%s", file_name);
	ptrs_task->data_opt_obj = pdata_opt;
	ptrs_task->timer = 0;

	FileTrsCtrl_Run.rd_task_cnt++;
	ptrs_task->next = FileTrsCtrl_Run.rd_task;
	FileTrsCtrl_Run.rd_task= ptrs_task;

	return 0;
}

int FileTrsCtrl_DestroySendTask(int rid,int rmfile_flag)
{
	int i;
	FileTrs_TaskCtrl_Stru 	*ptrs_task,*pretrs_task;
	char filepath[IX_DEVICE_FILE_LEN+1];
	
	for(i = 0,ptrs_task = FileTrsCtrl_Run.wr_task;i < FileTrsCtrl_Run.wr_task_cnt&& ptrs_task != NULL; i++)
	{
		if(ptrs_task->rid ==rid)
		{
			if(i == 0)
			{
				FileTrsCtrl_Run.wr_task = ptrs_task->next;
			}
			else
			{
				pretrs_task->next = ptrs_task->next;
			}
			FileTrsDataOpt_DestroyOptObj(ptrs_task->data_opt_obj);
			if(rmfile_flag)
			{
				DeleteFileProcess(ptrs_task->file_dir, ptrs_task->file_name);
			}
			
			free(ptrs_task);
			
			if(--FileTrsCtrl_Run.wr_task_cnt== 0)
			{
				FileTrsCtrl_Run.wr_task = NULL;
			}
			return 0;
		}
		pretrs_task = ptrs_task;
		ptrs_task = ptrs_task->next;
	}
	return 0;
}

int FileTrsCtrl_DestroyReadTask(int rid,int rmfile_flag)
{
	int i;
	FileTrs_TaskCtrl_Stru 	*ptrs_task,*pretrs_task;
	char filepath[IX_DEVICE_FILE_LEN+1];
	
	for(i = 0,ptrs_task = FileTrsCtrl_Run.rd_task;i < FileTrsCtrl_Run.rd_task_cnt && ptrs_task != NULL; i++)
	{
		if(ptrs_task->rid ==rid)
		{
			if(i == 0)
			{
				FileTrsCtrl_Run.rd_task = ptrs_task->next;
			}
			else
			{
				pretrs_task->next = ptrs_task->next;
			}
			FileTrsDataOpt_DestroyOptObj(ptrs_task->data_opt_obj);

			if(rmfile_flag)
			{
				DeleteFileProcess(ptrs_task->file_dir, ptrs_task->file_name);
			}
			
			free(ptrs_task);
			
			if(--FileTrsCtrl_Run.rd_task_cnt == 0)
			{
				FileTrsCtrl_Run.rd_task = NULL;
			}
			return 0;
		}
		pretrs_task = ptrs_task;
		ptrs_task = ptrs_task->next;
	}

	return 0;
}


int FileTrsCtrl_GetTrsMsg_ByDataOptObj(FileDataOptObj_Stru *data_opt_obj, int *server_ip,int *id,int *rid)
{
	int i;
	FileTrs_TaskCtrl_Stru 	*ptrs_task;
	
	for(i = 0,ptrs_task = FileTrsCtrl_Run.wr_task;i < FileTrsCtrl_Run.wr_task_cnt && ptrs_task != NULL; i++)
	{
		if(ptrs_task->data_opt_obj == data_opt_obj)
		{
			if(server_ip != NULL)
			{
				*server_ip = ptrs_task->server_ip;
			}
			if(id != NULL)
			{
				*id = ptrs_task->id;
			}
			if(rid != NULL)
			{
				*rid = ptrs_task->rid;
			}
			return 0;
		}
		ptrs_task = ptrs_task->next;
	}

	for(i = 0,ptrs_task = FileTrsCtrl_Run.rd_task;i < FileTrsCtrl_Run.rd_task_cnt && ptrs_task != NULL; i++)
	{
		if(ptrs_task->data_opt_obj == data_opt_obj)
		{
			if(server_ip != NULL)
			{
				*server_ip = ptrs_task->server_ip;
			}
			if(id != NULL)
			{
				*id = ptrs_task->id;
			}
			if(rid != NULL)
			{
				*rid = ptrs_task->rid;
			}
			return 0;
		}
		ptrs_task = ptrs_task->next;
	}

	return -1;
}


void FileTrsCtrl_Timer_Callback(void)
{
	int i;
	FileTrs_TaskCtrl_Stru 	*ptrs_task;
	
	pthread_mutex_lock( &FileTrsCtrl_Run.lock);
	
	for(i = 0,ptrs_task = FileTrsCtrl_Run.wr_task;i < FileTrsCtrl_Run.wr_task_cnt && ptrs_task != NULL; i++)
	{
		if(++ptrs_task->timer >= (FileTrsCtrl_WaitTime_Max/FileTrsCtrl_Timer_Gap))
		{
			FileTrsCtrl_DestroySendTask(ptrs_task->rid,1);
			FileSync_Write_Release(ptrs_task->rid);
		}
		ptrs_task = ptrs_task->next;
	}

	for(i = 0,ptrs_task = FileTrsCtrl_Run.rd_task;i < FileTrsCtrl_Run.rd_task_cnt && ptrs_task != NULL; i++)
	{
		if(++ptrs_task->timer >= (FileTrsCtrl_WaitTime_Max/FileTrsCtrl_Timer_Gap))
		{
			FileTrsCtrl_DestroyReadTask(ptrs_task->rid,1);
			FileSync_Read_Release(ptrs_task->rid);
		}
		ptrs_task = ptrs_task->next;
	}
	
	pthread_mutex_unlock( &FileTrsCtrl_Run.lock);	

	OS_RetriggerTimer(&FileTrsCtrl_Run.wait_timer);
}

FileSync_Run_Stru FileSync_Run;

int FileSync_Init(void)
{
	pthread_mutex_init( &FileSync_Run.lock, 0);
	FileSync_Run.entry_cnt = 0;
	FileSync_Run.sync_entry = NULL;

	return 0;
}

int FileSync_Read(int rid)
{
	int i;
	FileSync_Entry_Stru		*psync_entry;
	
	pthread_mutex_lock(&FileSync_Run.lock);
	
	for(i = 0,psync_entry = FileSync_Run.sync_entry;i<FileSync_Run.entry_cnt && psync_entry != NULL;i++)
	{
		if(psync_entry->rid == rid)
		{
			if(psync_entry->sync_mode == 0xffff)	//being write
			{
				pthread_mutex_unlock(&FileSync_Run.lock);
				return -1;
			}
			psync_entry->sync_mode ++;

			pthread_mutex_unlock(&FileSync_Run.lock);
			return 0;
		}
		psync_entry = psync_entry->next;
	}

	psync_entry = malloc(sizeof(FileSync_Entry_Stru));
	if(psync_entry == NULL)
	{
		pthread_mutex_unlock(&FileSync_Run.lock);
		return -1;
	}

	
	psync_entry->rid	 = rid;
	psync_entry->sync_mode = 1;

	psync_entry->next = FileSync_Run.sync_entry;
	FileSync_Run.sync_entry = psync_entry;
	
	FileSync_Run.entry_cnt ++;
	
	pthread_mutex_unlock(&FileSync_Run.lock);
	return 0;
}

int FileSync_Write(int rid)
{
	int i;
	FileSync_Entry_Stru		*psync_entry;
	
	pthread_mutex_lock(&FileSync_Run.lock);
	
	for(i = 0,psync_entry = FileSync_Run.sync_entry;i<FileSync_Run.entry_cnt && psync_entry != NULL;i++)
	{
		if(psync_entry->rid == rid)
		{
			pthread_mutex_unlock(&FileSync_Run.lock);
			return -1;
		}
		psync_entry = psync_entry->next;
	}

	psync_entry = malloc(sizeof(FileSync_Entry_Stru));
	if(psync_entry == NULL)
	{
		pthread_mutex_unlock(&FileSync_Run.lock);
		return -1;
	}

	
	psync_entry->rid	 = rid;
	psync_entry->sync_mode = 0xffff;

	psync_entry->next = FileSync_Run.sync_entry;
	FileSync_Run.sync_entry = psync_entry;
	
	FileSync_Run.entry_cnt ++;
	
	pthread_mutex_unlock(&FileSync_Run.lock);
	return 0;
}

int FileSync_Read_Release(int rid)
{
	int i;
	FileSync_Entry_Stru		*psync_entry,*presyns_entry;
	
	pthread_mutex_lock(&FileSync_Run.lock);
	
	for(i = 0,psync_entry = FileSync_Run.sync_entry;i<FileSync_Run.entry_cnt && psync_entry != NULL;i++)
	{
		if(psync_entry->rid == rid)
		{
			if(psync_entry->sync_mode == 0xffff)	//being write
			{
				pthread_mutex_unlock(&FileSync_Run.lock);
				return -1;
			}
			
			if(-- psync_entry->sync_mode == 0)
			{
				if(i == 0)
				{
					FileSync_Run.sync_entry = NULL;
					free(psync_entry);
				}
				else
				{
					presyns_entry->next = psync_entry->next;
					free(psync_entry);
				}
			}
			FileSync_Run.entry_cnt--;
			pthread_mutex_unlock(&FileSync_Run.lock);
			return 0;
		}
		
		presyns_entry = psync_entry;
		psync_entry = psync_entry->next;
	}

	pthread_mutex_unlock(&FileSync_Run.lock);
	return -1;
}

int FileSync_Write_Release(int rid)
{
	int i;
	FileSync_Entry_Stru		*psync_entry,*presyns_entry;
	
	pthread_mutex_lock(&FileSync_Run.lock);
	
	for(i = 0,psync_entry = FileSync_Run.sync_entry;i<FileSync_Run.entry_cnt && psync_entry != NULL;i++)
	{
		if(psync_entry->rid == rid)
		{
			if(psync_entry->sync_mode != 0xffff)	//being write
			{
				pthread_mutex_unlock(&FileSync_Run.lock);
				return -1;
			}
			
			if(i == 0)
			{
				FileSync_Run.sync_entry = NULL;
				free(psync_entry);
			}
			else
			{
				presyns_entry->next = psync_entry->next;
				free(psync_entry);
			}
			
			pthread_mutex_unlock(&FileSync_Run.lock);
			return 0;
		}
		
		presyns_entry = psync_entry;
		psync_entry = psync_entry->next;
		FileSync_Run.entry_cnt--;
	}

	pthread_mutex_unlock(&FileSync_Run.lock);
	return -1;
}

void FileTrsResFileSave(int rid)
{
	//IX2V test SaveResFileFromActFile(rid,GetSysVerInfo_BdRmMs(),2);
}
