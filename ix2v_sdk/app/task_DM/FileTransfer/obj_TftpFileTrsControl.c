#include <sys/types.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <error.h>
#include <signal.h>
#include <pthread.h>
#include <semaphore.h>
#include <sched.h>
#include <dirent.h>
#include "OSTIME.h"

#include "cJSON.h"
#include "vtk_udp_stack_device_update.h"
#include "obj_PublicUnicastCmd.h"
#include "task_IxProxy.h"

#define TFTP_SERVER_TRS_START	"start "
#define TFTP_SERVER_TRS_END		"end "

typedef struct
{
	int 				state;			// 0 = idle,1 = busy
	int 				start_time;			// 0 = idle,1 = busy
	int					trs_result;
	pthread_mutex_t		lock;
	pthread_t 			thread;
	pthread_attr_t 		attr_thread;
	char 				sendfileDir[200];
}TFTP_S_Run_Stru;

typedef struct
{
	int 				state;			// 0 = idle,1 = busy
	int 				serverIp;
	pthread_mutex_t		lock;
	pthread_t 			thread;
	pthread_attr_t 		attr_thread;
	char 				srcFile[50];
	char 				tarDir[200];
	char 				tarFile[50];
}TFTP_C_Run_Stru;

#define MAX_TFTP_TRS_TIME		300

static TFTP_S_Run_Stru tftp_S_Run = {.state = 0, .lock = PTHREAD_MUTEX_INITIALIZER};
static TFTP_C_Run_Stru tftp_C_Run = {.state = 0, .lock = PTHREAD_MUTEX_INITIALIZER};

static void ThreadTftpServer(void);
static void ThreadTftpCleint(void);

int TftpServerStart(char* sendfileDir,int detached)
{
	int state;

	pthread_mutex_lock( &tftp_S_Run.lock);
	state = tftp_S_Run.state;
	pthread_mutex_unlock( &tftp_S_Run.lock);

	if(state)
	{
		if(abs(time(NULL)-tftp_S_Run.start_time)>MAX_TFTP_TRS_TIME)
		{
			tftp_S_Run.state = 0;
			sleep(1);
		}
		return 0;
	}

	pthread_mutex_lock( &tftp_S_Run.lock);
	tftp_S_Run.state = 1;
	tftp_S_Run.start_time=time(NULL);
	pthread_mutex_unlock( &tftp_S_Run.lock);

	snprintf(tftp_S_Run.sendfileDir, 200, "%s", sendfileDir);

	pthread_attr_init(&tftp_S_Run.attr_thread);
	if(detached)
		pthread_attr_setdetachstate(&tftp_S_Run.attr_thread,PTHREAD_CREATE_DETACHED);
	if( pthread_create(&tftp_S_Run.thread,&tftp_S_Run.attr_thread,(void*)ThreadTftpServer, NULL) != 0 )
	{
		tftp_S_Run.state = 0;
		return -1;
	}
	
	return 0;
}


int WaitTftpServerExit(void)
{
	pthread_join(tftp_S_Run.thread,NULL);

	return 0;
}

static void ThreadTftpServer(void)
{
	FILE* pf   = NULL;
	char cmd_str[200]={'\0'};
	int pid;
	fd_set fds;
	struct timeval tv={1,0};
	int ret,fd;

	snprintf(cmd_str,200,"/usr/bin/udpsvd -vE 0.0.0.0 69 tftpd -c %s 2>&1 &", tftp_S_Run.sendfileDir);
	if( (pf = popen( cmd_str, "r" )) == NULL )
	{
		printf( "start_tftpd_download error:%s\n",strerror(errno) );
		tftp_S_Run.trs_result = -1;
		goto Tftp_Server_Thread_Exit;
	}

	fd = fileno(pf);
	while(1)
	{
		FD_ZERO(&fds);
		FD_SET(fd,&fds);
			
		tv.tv_sec = 5;
		tv.tv_usec = 0;	
		ret = select( fd + 1, &fds, NULL, NULL, &tv );
		if(ret == 0 ||ret == -1)
		{
			tftp_S_Run.trs_result = 0;
			dprintf("Thread_Tftp_Server start error ret=%d\n",ret);
			goto Tftp_Server_Thread_Exit;
		}
		fgets(cmd_str,200,pf);
		
		//dprintf("pipe print:%s\n",cmd_str);
			
		if(strstr(cmd_str,TFTP_SERVER_TRS_START) != NULL)
		{
			break;
		}
	}

	while(tftp_S_Run.state)
	{
		FD_ZERO(&fds);
		FD_SET(fd,&fds);
			
		tv.tv_sec = 180;
		tv.tv_usec = 0;	

		ret = select( fd + 1, &fds, NULL, NULL, &tv );
		if(ret == 0 ||ret == -1)
		{
			dprintf("Thread_Tftp_Server download error ret= %d\n",ret);
			tftp_S_Run.trs_result = -1;
			goto Tftp_Server_Thread_Exit;
		}

		fgets(cmd_str,200,pf);
		
		//dprintf("pipe print:%s\n",cmd_str);
			
		if(strstr(cmd_str,TFTP_SERVER_TRS_END) != NULL)
		{
			tftp_S_Run.trs_result = 0;
			break;
		}
	}

Tftp_Server_Thread_Exit:
	pid = getpid_bycmdline("/usr/bin/udpsvd -vE 0.0.0.0 69");//
	if(pid > 0)
	{
		//printf("kill the process pid =%d\n",pid);
		kill(pid,SIGKILL);
		pclose(pf);
	}
	//dprintf("start_tftpd_download time:%d-- over\n",time(NULL));
	
	pthread_mutex_lock( &tftp_S_Run.lock);
	tftp_S_Run.state = 0;
	pthread_mutex_unlock( &tftp_S_Run.lock);
}

static int TftpCleintDownloadFile(int ip, const char* srcFile, const char* tarDir, const char* tarFile)
{
	FILE* pf   = NULL;
	char cmd_str[500];
	char dateTime[100];

	pthread_mutex_lock( &tftp_C_Run.lock);
	tftp_C_Run.state = 1;
	pthread_mutex_unlock( &tftp_C_Run.lock);
	
	snprintf(cmd_str, 500,"tftp -g -r %s -l %s/%s -b 65464 %s", srcFile, tarDir, tarFile, my_inet_ntoa2(ip));
	//dprintf("start_tftp_download -- %s, %s\n",cmd_str, GetCurrentTime(dateTime, 100, "%Y-%m-%d %H:%M:%S"));

	if( (pf = popen( cmd_str, "r" )) == NULL )
	{
		dprintf( "start_tftp_download error:%s\n", strerror(errno) );
		pthread_mutex_lock( &tftp_C_Run.lock);
		tftp_C_Run.state = 0;
		pthread_mutex_unlock( &tftp_C_Run.lock);
		return - 1;
	}
	pclose( pf );
	//dprintf("start_tftp_download --  %s over\n", GetCurrentTime(dateTime, 100, "%Y-%m-%d %H:%M:%S"));

	pthread_mutex_lock( &tftp_C_Run.lock);
	tftp_C_Run.state = 0;
	pthread_mutex_unlock( &tftp_C_Run.lock);

	return 0;
}

int StartTftpCleint(int synchronous, int ip, const char* srcFile, const char* tarDir, const char* tarFile)
{
	int state;

	pthread_mutex_lock( &tftp_C_Run.lock);
	state = tftp_C_Run.state;
	pthread_mutex_unlock( &tftp_C_Run.lock);

	if(state)
	{
		return 0;
	}

	if(synchronous)
	{
		TftpCleintDownloadFile(ip, srcFile, tarDir, tarFile);
	}
	else
	{
		tftp_C_Run.serverIp = ip;
		strcpy(tftp_C_Run.srcFile, srcFile);
		strcpy(tftp_C_Run.tarDir, tarDir);
		strcpy(tftp_C_Run.tarFile, tarFile);

		pthread_attr_init(&tftp_C_Run.attr_thread);

		pthread_attr_setdetachstate( &tftp_C_Run.attr_thread, PTHREAD_CREATE_DETACHED );
		if( pthread_create(&tftp_C_Run.thread,&tftp_C_Run.attr_thread,(void*)ThreadTftpCleint, NULL) != 0 )
		{
			return -1;
		}
	}
	
	
	return 0;
}

static void ThreadTftpCleint(void)
{
	TftpCleintDownloadFile(tftp_C_Run.serverIp, tftp_C_Run.srcFile, tftp_C_Run.tarDir, tftp_C_Run.tarFile);
}


int TftpReadFileReqProcess(int ip, PublicUnicastCmdData dataIn, PublicUnicastCmdData* dataOut)
{
	char *srcDir = NULL;
	char *srcFile = NULL;
	char *tarDir = NULL;
	char *tarFile = NULL;

	cJSON *reqJson;
	cJSON *rspJson;
	int ret = 0;
	char path[500];

	reqJson = cJSON_Parse(dataIn.data);
	if(reqJson == NULL)
	{
		ret -3;
	}
	else
	{
		srcDir = cJSON_GetStringValue(cJSON_GetObjectItem(reqJson, DM_KEY_SRC_DIR));
		srcFile = cJSON_GetStringValue(cJSON_GetObjectItem(reqJson, DM_KEY_SRC_FILE));
		tarDir = cJSON_GetStringValue(cJSON_GetObjectItem(reqJson, DM_KEY_TAR_DIR));
		tarFile = cJSON_GetStringValue(cJSON_GetObjectItem(reqJson, DM_KEY_TAR_FILE));

		snprintf(path, 500, "%s/%s", srcDir, srcFile);
		dprintf("path=%s\n", path);

		if(!IsFileExist(path))
		{
			dprintf("File not exist!!! path=%s\n", path);
			ret = -1;
		}
		else
		{
			if(TftpServerStart(srcDir,1) == 0)
			{
				ret = 0;
			}
			else
			{
				ret = -2;
			}
		}
		cJSON_Delete(reqJson);
	}


	if(dataOut != NULL)
	{
		rspJson = cJSON_CreateObject();
		cJSON_AddStringToObject(rspJson, DM_KEY_SRC_DIR, srcDir);
		cJSON_AddStringToObject(rspJson, DM_KEY_SRC_FILE, srcFile);
		cJSON_AddStringToObject(rspJson, DM_KEY_TAR_DIR, tarDir);
		cJSON_AddStringToObject(rspJson, DM_KEY_TAR_FILE, tarFile);
		cJSON_AddNumberToObject(rspJson, DM_KEY_Result, ret);
		(*dataOut).data = cJSON_Print(rspJson);
		(*dataOut).len = strlen((*dataOut).data) + 1;
		cJSON_Delete(rspJson);
	}
	
	//dprintf("TftpReadFileReqProcess ret=%d\n", ret);

	return ret;
}

int FileVerifyReqProcess(int ip, PublicUnicastCmdData dataIn, PublicUnicastCmdData* dataOut)
{
	char *path = NULL;
	char *checkCode = NULL;
	char md5String[33];

	cJSON *reqJson;
	cJSON *rspJson;
	int ret = 0;

	reqJson = cJSON_Parse(dataIn.data);
	if(reqJson == NULL)
	{
		ret -3;
	}
	else
	{
		path = cJSON_GetStringValue(cJSON_GetObjectItem(reqJson, DM_KEY_Path));
		checkCode = cJSON_GetStringValue(cJSON_GetObjectItem(reqJson, DM_KEY_CODE));

		if(FileMd5_CalculateString(path, md5String) != 0)
		{
			dprintf("File not exist!!! path=%s\n", path);
			ret = -1;
		}
		else
		{
			if(!strcmp(checkCode, md5String))
			{
				ret = 0;
			}
			else
			{
				ret = -2;
			}
		}
		cJSON_Delete(reqJson);
	}


	if(dataOut != NULL)
	{
		rspJson = cJSON_CreateObject();
		cJSON_AddStringToObject(rspJson, DM_KEY_Path, path);
		cJSON_AddStringToObject(rspJson, DM_KEY_CODE, md5String);
		cJSON_AddNumberToObject(rspJson, DM_KEY_Result, ret);
		(*dataOut).data = cJSON_Print(rspJson);
		(*dataOut).len = strlen((*dataOut).data) + 1;
		cJSON_Delete(rspJson);
	}
	
	//dprintf("FileVerifyReqProcess ret=%d\n", ret);

	return ret;
}



int TftpWriteFileReqProcess(int ip, PublicUnicastCmdData dataIn, PublicUnicastCmdData* dataOut)
{
	char *srcDir = NULL;
	char *srcFile = NULL;
	char *tarDir = NULL;
	char *tarFile = NULL;
	char *retData = NULL;
	char *ipStr;
	char path[500];

	cJSON *reqJson;
	cJSON *rspJson;
	int ret = 0;

	//dprintf("dataIn.data =%s\n", dataIn.data);

	reqJson = cJSON_Parse(dataIn.data);
	if(reqJson == NULL)
	{
		ret -3;
	}
	else
	{
		srcDir = cJSON_GetStringValue(cJSON_GetObjectItem(reqJson, DM_KEY_SRC_DIR));
		srcFile = cJSON_GetStringValue(cJSON_GetObjectItem(reqJson, DM_KEY_SRC_FILE));
		tarDir = cJSON_GetStringValue(cJSON_GetObjectItem(reqJson, DM_KEY_TAR_DIR));
		tarFile = cJSON_GetStringValue(cJSON_GetObjectItem(reqJson, DM_KEY_TAR_FILE));
		ipStr = cJSON_GetStringValue(cJSON_GetObjectItem(reqJson, DM_KEY_IP));

		if(!IsFileExist(tarDir))
		{
			dprintf("File not exist!!! path=%s\n", tarDir);
			ret = -1;
		}
		else
		{
			ret = StartTftpCleint(0, inet_addr(ipStr), srcFile, tarDir, tarFile);
			if(ret != 0)
			{
				ret = -2;
			}
		}
		cJSON_Delete(reqJson);
	}
	
	if(dataOut != NULL)
	{
		rspJson = cJSON_CreateObject();
		cJSON_AddStringToObject(rspJson, DM_KEY_SRC_DIR, srcDir);
		cJSON_AddStringToObject(rspJson, DM_KEY_SRC_FILE, srcFile);
		cJSON_AddStringToObject(rspJson, DM_KEY_TAR_DIR, tarDir);
		cJSON_AddStringToObject(rspJson, DM_KEY_TAR_FILE, tarFile);
		cJSON_AddNumberToObject(rspJson, DM_KEY_Result, ret);
		(*dataOut).data = cJSON_Print(rspJson);
		cJSON_Delete(rspJson);
		(*dataOut).len = strlen((*dataOut).data) + 1;
	}
	
	//dprintf("TftpWriteFileReqProcess ret=%d\n", ret);
	return ret;
}

int Api_TftpReadFileFromDevice(int ip, char* srcDir, char* srcFile, char* tarDir, char* tarFile)
{
	PublicUnicastCmdData dataIn;
	cJSON *reqJson = NULL;
	char* reqData = NULL;
	char* retData = NULL;
	int retLen;
	int ret;
	char md5String[33];
	char path[500];
	
	reqJson = cJSON_CreateObject();

	cJSON_AddStringToObject(reqJson, DM_KEY_SRC_DIR, srcDir);
	cJSON_AddStringToObject(reqJson, DM_KEY_SRC_FILE, srcFile);
	cJSON_AddStringToObject(reqJson, DM_KEY_TAR_DIR, tarDir);
	cJSON_AddStringToObject(reqJson, DM_KEY_TAR_FILE, tarFile);
	reqData = cJSON_Print(reqJson);

	cJSON_Delete(reqJson);

	dataIn.len = strlen(reqData) + 1;
	dataIn.data = reqData;
	ret = PublicUnicastCmdRequst(ip, NULL, IxProxyWaitUdpTime, TFTP_READ_FILE_REQ, dataIn, NULL);
	free(reqData);
	if(ret == 0)
	{
		ret = StartTftpCleint(1, ip, srcFile, tarDir, tarFile);
		if(ret != 0)
		{
			ret = -2;
		}
		else
		{
			snprintf(path, 500, "%s/%s", tarDir, tarFile);

			if(FileMd5_CalculateString(path, md5String) != 0)
			{
				dprintf("File not exist!!! path=%s\n", path);
				ret = -3;
			}
			else
			{
				snprintf(path, 500, "%s/%s", srcDir, srcFile);
				reqJson = cJSON_CreateObject();
				cJSON_AddStringToObject(reqJson, DM_KEY_Path, path);
				cJSON_AddStringToObject(reqJson, DM_KEY_CODE, md5String);
				reqData = cJSON_Print(reqJson);

				cJSON_Delete(reqJson);

				dataIn.len = strlen(reqData) + 1;
				dataIn.data = reqData;
				ret = PublicUnicastCmdRequst(ip, NULL, IxProxyWaitUdpTime, TFTP_CHECK_FILE_REQ, dataIn, NULL);
				free(reqData);
				if(ret != 0)
				{
					dprintf("File %s check error\n", path);
					ret = -4;
				}
			}
		}
	}
	else
	{
		dprintf("Tftp read request not respone\n");
		ret = -1;
	}


	//dprintf("Read [%s]%s/%s to %s/%s, ret=%d\n",  my_inet_ntoa2(ip), srcDir, srcFile, tarDir, tarFile, ret);
	return ret;	
}

int Api_TftpSendFileToDevice(int ip, char* srcDir, char* srcFile, char* tarDir, char* tarFile)
{
	PublicUnicastCmdData dataIn, dataOut;
	cJSON *reqJson = NULL;
	char* reqData = NULL;
	int ret;
	char path[500];
	char md5String[33];
	
	if(TftpServerStart(srcDir,0) == 0)
	{
		reqJson = cJSON_CreateObject();

		cJSON_AddStringToObject(reqJson, DM_KEY_SRC_DIR, srcDir);
		cJSON_AddStringToObject(reqJson, DM_KEY_SRC_FILE, srcFile);
		cJSON_AddStringToObject(reqJson, DM_KEY_TAR_DIR, tarDir);
		cJSON_AddStringToObject(reqJson, DM_KEY_TAR_FILE, tarFile);
		cJSON_AddStringToObject(reqJson, DM_KEY_IP, GetSysVerInfo_IP_by_device(GetNetDeviceNameByTargetIp(ip)));
		reqData = cJSON_Print(reqJson);

		cJSON_Delete(reqJson);
		dataIn.data = reqData;
		dataIn.len = strlen(reqData) + 1;
		ret = PublicUnicastCmdRequst(ip, NULL, IxProxyWaitUdpTime, TFTP_WRITE_FILE_REQ, dataIn, NULL);
		free(reqData);

		if(ret == 0)
		{
			WaitTftpServerExit();

			snprintf(path, 500, "%s/%s", srcDir, srcFile);
			ret = FileMd5_CalculateString(path, md5String);

			if(ret != 0)
			{
				dprintf("File not exist!!! path=%s\n", path);
				ret = -3;
			}
			else
			{
				snprintf(path, 500, "%s/%s", tarDir, tarFile);
				reqJson = cJSON_CreateObject();
				cJSON_AddStringToObject(reqJson, DM_KEY_Path, path);
				cJSON_AddStringToObject(reqJson, DM_KEY_CODE, md5String);
				reqData = cJSON_Print(reqJson);
				cJSON_Delete(reqJson);
				dataIn.data = reqData;
				dataIn.len = strlen(reqData) + 1;
				ret = PublicUnicastCmdRequst(ip, NULL, IxProxyWaitUdpTime, TFTP_CHECK_FILE_REQ, dataIn, NULL);
				free(reqData);

				if(ret != 0)
				{
					dprintf("File %s check error\n", path);
					ret = -4;
				}
			}
		}
		else
		{
			ret = -1;
			dprintf("Tftp Send request not respone\n");
		}
	}
	else
	{
		ret = -2;
	}

	//dprintf("Send %s/%s to [%s]%s/%s, ret=%d\n",  srcDir, srcFile, my_inet_ntoa2(ip), tarDir, tarFile, ret);
	return ret;
}
