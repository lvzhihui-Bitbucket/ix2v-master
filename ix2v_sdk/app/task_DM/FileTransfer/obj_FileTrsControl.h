#ifndef _obj_FileTrsControl_h
#define _obj_FileTrsControl_h

#include "utility.h"
#include "obj_FileTrsDataOpt.h"
#include "define_file.h"

#define	MAX_RSWRTASK_CNT				1
#define	MAX_RSRDTASK_CNT				1

#define MAX_FILE_TRS_DIR_LEN			100
#define MAX_FILE_TRS_FILENAME_LEN		44
#define	FILE_TRS_DEFAULT_PACK_LEN		1024
#define FileTrsCtrl_Timer_Gap			5	
#define FileTrsCtrl_WaitTime_Max		30	

#define	IX_DEVICE_FILE_LEN			150

typedef struct _FileTrs_TaskCtrl_Stru	
{
	struct _FileTrs_TaskCtrl_Stru		*next;
	unsigned short				rid;
	int							server_ip;
	int							id;
	unsigned char					file_md5[16];
	char							file_dir[MAX_FILE_TRS_DIR_LEN + 1];
	char							saveDir[MAX_FILE_TRS_DIR_LEN + 1];
	char							file_name[MAX_FILE_TRS_FILENAME_LEN + 1];
	FileDataOptObj_Stru				*data_opt_obj;
	int							timer;
}FileTrs_TaskCtrl_Stru;

typedef struct
{
	pthread_mutex_t		lock;
	OS_TIMER 			wait_timer;
	int					wr_task_cnt;
	FileTrs_TaskCtrl_Stru	*wr_task;
	int					rd_task_cnt;
	FileTrs_TaskCtrl_Stru	*rd_task;
}FileTrsCtrl_Run_Stru;

typedef struct _FileSync_Entry_Stru
{
	struct _FileSync_Entry_Stru *next;
	unsigned short			rid;
	unsigned short			sync_mode;
}FileSync_Entry_Stru;

typedef struct
{
	pthread_mutex_t		lock;
	int					entry_cnt;
	FileSync_Entry_Stru		*sync_entry;
}FileSync_Run_Stru;

void FileTrsServer_Init(void);
void FileTrsCtrl_Timer_Callback(void);
int FileTrsCtrl_SendReq(int cmd_id, int server_ip,int id,int rid, char* saveDir, char* tempDir, char *file_name, char *file_md5, int file_len, int pack_len);
int FileTrsCtrl_SendOnePack(int cmd_id, int server_ip,int id,int rid,int pack_no,int pack_len,unsigned char *data, int* packetNum);
int FileTrsCtrl_SendEnd(int cmd_id, int server_ip,int id,int rid, char* dir, char* fileName);
int FileTrsCtrl_SendCancel(int cmd_id, int server_ip,int id,int rid);
int FileTrsCtrl_ReadReq(int cmd_id, int server_ip,int id,int rid, char* dir, char* tempDir, char* fileName);
int FileTrsCtrl_ReadOnePack(int cmd_id, int server_ip,int id,int rid,int pack_no);
int FileTrsCtrl_ReadEnd(int cmd_id, int server_ip,int id,int rid);
int FileTrsCtrl_ReadCancel(int cmd_id, int server_ip,int id,int rid);

int FileTrsCtrl_CreateNewSendTask(int server_ip,int id,int rid,char* saveDir, char* tempDir, char *file_name,unsigned char *file_md5,int file_len,int pack_len);
int FileTrsCtrl_CreateNewReadTask(int server_ip,int id,int rid,char *dir,char *file_name,unsigned char *file_md5,int file_len,int pack_len);
int FileTrsCtrl_DestroySendTask(int rid,int rmfile_flag);
int FileTrsCtrl_DestroyReadTask(int rid,int rmfile_flag);
int FileTrsCtrl_GetTrsMsg_ByDataOptObj(FileDataOptObj_Stru *data_opt_obj, int *server_ip,int *id,int *rid);

int FileSync_Init(void);
int FileSync_Read(int rid);
int FileSync_Read_Release(int rid);
int FileSync_Write(int rid);
int FileSync_Write_Release(int rid);

#endif
