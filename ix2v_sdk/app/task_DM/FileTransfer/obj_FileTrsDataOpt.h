#ifndef _obj_FileTrsDataOpt_h
#define _obj_FileTrsDataOpt_h

typedef struct
{
	FILE *pfile;
	int	file_len;
	int	pack_len;
	int	pack_no;
	int	cur_pack_no;
}FileDataOptObj_Stru;

FileDataOptObj_Stru* FileTrsDataOpt_CreateNewWriteObj(char* dir, char *file_name,int file_len,int pack_len);
FileDataOptObj_Stru* FileTrsDataOpt_CreateNewReadObj(char* dir, char *file_name,int file_len,int pack_len);
int FileTrsDataOpt_DestroyOptObj(FileDataOptObj_Stru *data_opt);
int FileTrsDataOpt_WriteOnePack(int cmd_id, FileDataOptObj_Stru *data_opt,int pack_no,int pack_len,unsigned char *data, int* packetNum);
int FileTrsDataOpt_ReadOnePack(int cmd_id, FileDataOptObj_Stru *data_opt,int pack_no);

#endif