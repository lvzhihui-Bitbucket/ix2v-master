#ifndef _obj_FileTrsCmd_h
#define _obj_FileTrsCmd_h

#include "obj_FileTrsControl.h"
#include "vtk_udp_stack_class.h"

#define RSTRS_CMD_SENDREQ				0x3001
#define RSTRS_CMD_SENDONEPACK			0x3002
#define RSTRS_CMD_SENDEND				0x3003
#define RSTRS_CMD_SENDCANCEL			0x3004
#define RSTRS_CMD_READREQ				0x3101
#define RSTRS_CMD_READONEPACK			0x3102
#define RSTRS_CMD_READEND				0x3103
#define RSTRS_CMD_READCANCEL			0x3104
#define RSTRS_CMD_FWUPDATEREQ			0x3011
#define RSTRS_CMD_GETFWVER				0x3012
#define RSTRS_CMD_FWUPDATEDONE			0x3013		//czn_20170216
#define RSTRS_CMD_FILECHKREQ			0x3105		//czn_20170206

#define FileReadFileNameLenMax			44

#pragma pack(1)

typedef struct
{
	//send head
	baony_head	head;
	//send target
	target_head	target;
	//send data
	char		dat[IP8210_BUFFER_MAX]; 
} FileTrsCmd_PKT_S;

typedef struct 
{
	//unsigned char cmd;
	unsigned char resid[2];
}FileTrsCmd_Head_Stru;

typedef struct 
{
	//unsigned char cmd;
	unsigned char resid[2];
}RsTrsCmd_Head_Stru;

typedef struct 
{
	FileTrsCmd_Head_Stru 	cmd_head;
	unsigned char 			file_length[4];
	unsigned char			pack_length[2];
	unsigned char			file_md5[16];		//czn_20160922
	unsigned char			file_name[FileReadFileNameLenMax];		//czn_20160919
}FileTrsCmd_SendReq_Stru;

typedef struct 
{
	FileTrsCmd_Head_Stru 	cmd_head;
	unsigned char 			file_length[4];
	unsigned char			pack_length[2];
	unsigned char			file_md5[16];
	char					jsonData[1];
}FileTrsCmd_SendReq_Stru2;

typedef struct 
{
	FileTrsCmd_Head_Stru 	cmd_head;
	unsigned char 			pack_no[2];
	unsigned char			pack_length[2];
	unsigned char			data[1];
}FileTrsCmd_SendOnePack_Stru;

typedef struct 
{
	FileTrsCmd_Head_Stru 	cmd_head;
}FileTrsCmd_SendEnd_Stru;

typedef struct 
{
	FileTrsCmd_Head_Stru 	cmd_head;
}FileTrsCmd_SendCancel_Stru;

typedef struct 
{
	FileTrsCmd_Head_Stru 	cmd_head;
	unsigned char			result;
}FileTrsCmd_SendRsp_Stru;
//czn_20160919_s
typedef struct 
{
	FileTrsCmd_Head_Stru 	cmd_head;
	unsigned char			result;
	unsigned char			pack_len[2];
}FileTrsCmd_SendReqRsp_Stru;
//czn_20160919_e
typedef struct 
{
	FileTrsCmd_Head_Stru 	cmd_head;
}FileTrsCmd_ReadReq_Stru;

typedef struct 
{
	FileTrsCmd_Head_Stru 	cmd_head;
	char					jsonData[1];
}FileTrsCmd_ReadReq_Stru2;


typedef struct 
{
	FileTrsCmd_Head_Stru 	cmd_head;
	unsigned char			agree;
	unsigned char 			file_length[4];
	unsigned char			pack_length[2];
	unsigned char			file_md5[16];		//czn_20160922
	unsigned char			file_name[FileReadFileNameLenMax];		//czn_20160922
}FileTrsCmd_ReadRsp_Stru;

typedef struct 
{
	FileTrsCmd_Head_Stru 	cmd_head;
	unsigned char 			pack_no[2];
}FileTrsCmd_ReadOnePackReq_Stru;

typedef struct 
{
	FileTrsCmd_Head_Stru 	cmd_head;
	unsigned char			result;			//czn_20160922
	unsigned char 			pack_no[2];
	unsigned char			pack_length[2];
	unsigned char			data[1];
}FileTrsCmd_ReadOnePackRsp_Stru;

typedef struct 
{
	FileTrsCmd_Head_Stru 	cmd_head;
	unsigned char 			result;
}FileTrsCmd_ReadEndNotice_Stru;

typedef struct 
{
	FileTrsCmd_Head_Stru 	cmd_head;
}FileTrsCmd_ReadEndRsp_Stru;

typedef struct 
{
	FileTrsCmd_Head_Stru 	cmd_head;
}FileTrsCmd_ReadCancelReq_Stru;

typedef struct 
{
	FileTrsCmd_Head_Stru 	cmd_head;
	unsigned char 		result;
}FileTrsCmd_ReadCancelRsp_Stru;

typedef struct
{
	FileTrsCmd_Head_Stru 	cmd_head;
	unsigned char			file_length[4];
	unsigned char			file_md5[16];
}FileTrsCmd_FwUpdateReq;

typedef struct
{
	FileTrsCmd_Head_Stru 	cmd_head;
	unsigned char			result;
}FileTrsCmd_FwUpdateRsp;

typedef struct
{
	FileTrsCmd_Head_Stru 	cmd_head;
	unsigned char			result;
	unsigned char			ver[18];
	unsigned char			file_length[4];
	unsigned char			file_md5[16];
}FileTrsCmd_GetFwVerRsp_Stru;
//czn_20170206_s
typedef struct
{
	unsigned char			req_num;
	unsigned char			resid[];
}FileTrsCmd_FileChkReq_Stru;

typedef struct
{
	unsigned char			resid[2];
	unsigned char			result;
	unsigned char			file_md5[16];
	unsigned char			upgraded_time[4];
}FileTrsCmd_FileChkRspResult_Stru;

typedef struct
{
	unsigned char			rsp_num;
	unsigned char			rsp_data[];
}FileTrsCmd_FileChkRsp_Stru;
//czn_20170206_e

#pragma pack()

int RemoteRsTrsCmd_Process(int cmd_id, char* pbuf, int len);

int FileTrs_SendRsp_Common(int cmd_id, int server_ip, int id,int rspcmd,int rid,unsigned char result);
	#define FileTrs_SendOnePackRsp(cmd_id, server_ip,id,rid,result)		FileTrs_SendRsp_Common(cmd_id, server_ip,id,RSTRS_CMD_SENDONEPACK|0x80,rid,result)
	#define FileTrs_SendEndRsp(cmd_id, server_ip,id,rid,result)			FileTrs_SendRsp_Common(cmd_id, server_ip,id,RSTRS_CMD_SENDEND|0x80,rid,result)
	#define FileTrs_SendCancelRsp(cmd_id, server_ip,id,rid,result)		FileTrs_SendRsp_Common(cmd_id, server_ip,id,RSTRS_CMD_SENDCANCEL|0x80,rid,result)
int FileTrs_SendReqRsp(int cmd_id, int server_ip, int id,int rid,unsigned char result,int pack_len);//czn_20160919

int FileTrs_ReadRsp(int cmd_id, int server_ip,int id,int rid,unsigned char agree,int file_length,int pack_length,unsigned char *md5_chk,char *file_name);//czn_20160922
int FileTrs_ReadOnePackRsp(int cmd_id, int server_ip,int id,int rid,unsigned char result,int pack_no,int pack_length,unsigned char *data);	//czn_20160922
int FileTrs_ReadEndRsp(int cmd_id, int server_ip,int id,int rid);	
int FileTrs_ReadCancelRsp(int cmd_id, int server_ip,int id,int rid,unsigned  char result);	

int FileTrs_FwUpdateRsp(int cmd_id, int server_ip, int id,int rid,unsigned char result);
int FileTrs_GetFwVerRsp(int cmd_id, int server_ip, int id,int rid,unsigned char result,int file_length,char *fw_ver,unsigned char *file_md5);

#endif
