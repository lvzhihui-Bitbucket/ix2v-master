/**
  ******************************************************************************
  * @file    task_IxProxy.h
  * @author  zeng
  * @version V1.0.0
  * @date    2012.08.15
  * @brief   This file contains the headers of the power task_3.
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2012 V-Tec</center></h2>
  ******************************************************************************
  */
#ifndef TASK_IxProxy_H
#define TASK_IxProxy_H

#include "tcp_server_process.h"
#include "obj_GetIpByNumber.h"
#include "obj_DeviceManage.h"
#include "obj_GetAboutByIp.h"
#include "obj_GetInfoByIp.h"
#include "obj_SearchIpByFilter.h"
#include "obj_SYS_VER_INFO.h"
#include "define_file.h"
#include "define_string.h"
#include "cJSON.h"

#define FILE_MANAGE_RID					8888

#define IxDeviceMaxNum					10

#define IxProxyWaitUdpTime				3
#define USE_SEARCH_TABLE				1


#define CMD_RSP_DELAY()							usleep(1*1000)
#define LOCAL_CMD_RSP_DELAY()					usleep(10*1000)


#define REQ_HEARTBEAT							0x0201			// ��������
#define RSP_HEARTBEAT							0x8201			// �����ظ�

#define REQ_LOGIN								0x0202			// ��¼����
#define RSP_LOGIN								0x8202			// ��¼�ظ�

#define REQ_DM_REBOOT							0x0203			// ��������
#define RSP_DM_REBOOT							0x8203			// �����ظ�

#define REQ_DM_ABOUT							0x0204			// Get about����
#define RSP_DM_ABOUT							0x8204			// Get about�ظ�

#define REQ_DM_INFO								0x0205			// Get Info����
#define RSP_DM_INFO								0x8205			// Get Info�ظ�

#define REQ_DM_R8012							0x0206			// 8012ҵ������
#define RSP_DM_R8012							0x8206			// 8012ҵ��ظ�

#define REQ_DM_PARA_READ						0x0207			// ����������
#define RSP_DM_PARA_READ						0x8207			// �������ظ�

#define REQ_DM_PARA_WRITE						0x0208			// д��������
#define RSP_DM_PARA_WRITE						0x8208			// д�����ظ�

#define REQ_DM_PARA_DEFAULT						0x0209			// �ָ����в�������
#define RSP_DM_PARA_DEFAULT						0x8209			// �ָ����в����ظ�

#define REQ_IP_CHECK							0x020A			// ���IP����
#define RSP_IP_CHECK							0x820A			// ���IP�ظ�

#define REQ_DM_INFO2							0x020B			// Get Info2����
#define RSP_DM_INFO2							0x820B			// Get Info2�ظ�

#define REQ_DM_RING								0x020C			// RING����
#define RSP_DM_RING								0x820C			// RING�ظ�

#define REQ_DM_PROG_CALL_NBR					0x020D			// PROG_CALL_NBR����
#define RSP_DM_PROG_CALL_NBR					0x820D			// PROG_CALL_NBR�ظ�

#define REQ_DM_R8012_SCAN_NEW					0x020E			// 8012ҵ���������豸����
#define RSP_DM_R8012_SCAN_NEW					0x820E			// 8012ҵ���������豸�ظ�

#define REQ_DM_R8012_GET_CACHE					0x020F			// 8012 Get Cache����
#define RSP_DM_R8012_GET_CACHE					0x820F			// 8012 Get Cache�ظ�

#define REQ_DM_R8012_CHECK						0x0210			// 8012 CHECK����
#define RSP_DM_R8012_CHECK						0x8210			// 8012 CHECK�ظ�

#define REQ_DM_GET_INFO_BY_MFG_SN				0x0211			// Get Info by MFG_SN����
#define RSP_DM_GET_INFO_BY_MFG_SN				0x8211			// Get Info by MFG_SN�ظ�

#define REQ_DM_PROG_IP_ADDR						0x0212			// PROG_IP_ADDR����
#define RSP_DM_PROG_IP_ADDR						0x8212			// PROG_IP_ADDR�ظ�

#define REQ_DM_RES_DOWNLOAD						0x0213			// RES_DOWNLOAD����
#define RSP_DM_RES_DOWNLOAD						0x8213			// RES_DOWNLOAD�ظ�

#define REQ_DM_GET_VERSION						0x0214			// GET_VERSION����
#define RSP_DM_GET_VERSION						0x8214			// GET_VERSION�ظ�

#define REQ_DM_GET_MERGE_INFO					0x0215			// Get �ϲ� Info
#define RSP_DM_GET_MERGE_INFO					0x8215


#define REQ_DM_SETTING_FROM_R8001				0x0216			// ��R8001����ȡ��������
#define RSP_DM_SETTING_FROM_R8001				0x8216

#define REQ_DM_BACKUP							0x0217			// ��������
#define RSP_DM_BACKUP							0x8217

#define REQ_DM_RESTORE							0x0218			// �ָ�
#define RSP_DM_RESTORE							0x8218

#define REQ_DM_FILE								0x0219			// �ļ�����
#define RSP_DM_FILE								0x8219

#define REQ_SHELL								0x021A			// shell
#define RSP_SHELL								0x821A

#define REQ_SEARCH_DEVICE						0x021B			// 搜索设备请求
#define RSP_SEARCH_DEVICE						0x821B			// 

#define REQ_GET_PB_IO							0x021C			// 获取设备信息
#define RSP_GET_PB_IO							0x821C			// 

#define REQ_READ_WRITE_IO						0x021D			// 读写IO参数
#define RSP_READ_WRITE_IO						0x821D			// 

#define REQ_EVENT_APP_TO_DEV					0x021E			// APP向设备发送Event msg
#define RSP_EVENT_APP_TO_DEV					0x821E			// 

#define IX_TRANS_REQ							0x0301			// IXת������
#define IX_TRANS_RSP							0x8301			// IXת���ظ�


typedef enum
{
	 IX_PROXY_IDLE = 0,
	 IX_PROXY_BUSY,
}IX_PROXY_STATE;

typedef struct
{
	unsigned short OP;
	unsigned short Session_ID;
	unsigned short RSP_ID;
	unsigned short Length;
	unsigned short CheckSUM;
}IX_PROXY_TCP_HEAD;

typedef struct
{
	unsigned short OP;
	unsigned short Session_ID;
	unsigned short RSP_ID;
	unsigned short Length;
	unsigned short CheckSUM;
	unsigned short SUB_OP;
}IX_PROXY_TRANSMIT_HEAD;


typedef struct
{
	char version[IX_BUILDER_NAME_LEN];
	char priority[IX_BUILDER_NAME_LEN];
	char linked[IX_BUILDER_NAME_LEN*2];
	char ip_addr[IX_BUILDER_NAME_LEN];
	char device_addr[IX_BUILDER_NAME_LEN];
	char MFG_SN[IX_BUILDER_NAME_LEN];
	char deviceType[IX_BUILDER_NAME_LEN];
	char name[IX_BUILDER_NAME_LEN];
	char name2[IX_BUILDER_NAME_LEN*2];
	char CacheTime[IX_BUILDER_NAME_LEN];
	char CacheNbr[IX_BUILDER_NAME_LEN];
	char netCard[IX_BUILDER_NAME_LEN];
}IxProxyData_T;

typedef enum
{
	 Idle = 0,
	 Local,
	 Remote,
}CONNECT_STATE;

typedef struct
{
	CONNECT_STATE 	state;
	char 			network[10];
	cJSON* 			info;
}IxProxyConnect;

typedef struct
{
	short cmd_id;
	short businessCode;
	short dataLen;
	char  data[CLIENT_CONNECT_BUF_MAX];
}IX_PROXY_MSG;

typedef struct
{
	pthread_mutex_t 		taskLock;
	pthread_mutex_t 		lock;
	IX_PROXY_STATE  		state;
	IxProxyConnect			connect;
	int 					RSP_ID;
	int 					rebootFlag;
	short 					cmd_id;
	short 					businessCode;
}IxProxyRUN_T;


/*******************************************************************************
                         Define task event flag
*******************************************************************************/

/*******************************************************************************
                  Define task vars, structures and macro
*******************************************************************************/

void vtk_TaskInit_IxProxy(void);

static void vdp_ix_proxy_mesg_data_process(char* msg_data, int len);

IxProxyData_T GetMyProxyData(void);


#endif
