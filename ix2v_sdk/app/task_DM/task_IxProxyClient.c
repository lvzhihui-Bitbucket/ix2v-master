/**
  ******************************************************************************
  * @file    task_IxProxy.c
  * @author  lvzhihui
  * @version V1.0.0
  * @date    2016.04.15
  * @brief   This file contains the functions of task_ix_proxy
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2012 V-Tec</center></h2>
  ******************************************************************************
  */
#include "one_tiny_task.h"
#include "task_IxProxyClient.h"
#include "tcp_server_process.h"
#include "task_IxProxy.h"
#include "elog.h"

Loop_vdp_common_buffer	vdp_ix_proxy_client_mesg_queue;
Loop_vdp_common_buffer	vdp_ix_proxy_client_sync_queue;
vdp_task_t				task_ix_proxy_client = {.task_run_flag = 0};

void start_vdp_ix_proxy_client_task(void)
{
	ProxyTask_pthread_mutex_lock();
	if(!task_ix_proxy_client.task_run_flag)
	{
		init_vdp_common_queue(&vdp_ix_proxy_client_mesg_queue, 5000, vdp_ix_proxy_client_mesg_data_process, &task_ix_proxy_client);
		init_vdp_common_queue(&vdp_ix_proxy_client_sync_queue, 100, NULL, &task_ix_proxy_client);
		init_vdp_common_task(&task_ix_proxy_client, MSG_ID_IX_PROXY_CLIENT, vdp_public_task, &vdp_ix_proxy_client_mesg_queue, &vdp_ix_proxy_client_sync_queue);
		task_ix_proxy_client.task_StartCompleted = 1;
	}
	ProxyTask_pthread_mutex_unlock();
}

void exit_vdp_ix_proxy_client_task(void)
{
	ProxyTask_pthread_mutex_lock();
	if(task_ix_proxy_client.task_run_flag)
	{
		exit_vdp_common_queue(&vdp_ix_proxy_client_mesg_queue);
		exit_vdp_common_queue(&vdp_ix_proxy_client_sync_queue);
		exit_vdp_common_task(&task_ix_proxy_client);	
	}
	ProxyTask_pthread_mutex_unlock();
}


int IxProxyTransmit(unsigned short SUB_OP, void* data)
{
	char* rspJson = NULL;
	IX_PROXY_TRANSMIT_HEAD head;
	
	head.OP = 0x0301;
	
	head.Session_ID = MyRand();
	head.RSP_ID = 0;
	head.SUB_OP = SUB_OP;

	if(data != NULL)
	{
		return TransmitClient(head, data, strlen(data)+1);
	}
	else
	{
		return TransmitClient(head, NULL, 0);
	}
}


static void vdp_ix_proxy_client_mesg_data_process(char* msg_data,int len)
{
	IX_PROXY_TRANSMIT_HEAD* phead = (IX_PROXY_TRANSMIT_HEAD*)msg_data;
	unsigned short OP = ntohs(phead->OP);
	
	//dprintf("vdp_ix_proxy_client_mesg_data_process OP = 0x%04x, SUB_OP=0x%04x.\n", OP, phead->SUB_OP);

	switch(OP)
	{
		case IX_TRANS_REQ:
			IxProxyTransmit(phead->SUB_OP, (void*)(msg_data + sizeof(IX_PROXY_TRANSMIT_HEAD)));
			break;
		case IX_TRANS_RSP:
			
			break;

		default:
			break;
	}
}

void API_IxProxyClient(char* pData, int len)
{
	// 压入本地队列
	push_vdp_common_queue(&vdp_ix_proxy_client_mesg_queue, (char*)pData, len);
}

int API_IxProxyClientTransmit(unsigned short SUB_OP, void* data)
{
	#include "obj_IX_Report.h"
	IX_PROXY_TRANSMIT_HEAD head;
	int headLen, dataLen, dataIndex, sendLen, stringLen;
	char sendData[sizeof(IX_PROXY_TRANSMIT_HEAD)+IX_REPORT_DATA_LEN+1];
	
	if(data != NULL && (dataLen = strlen(data)))
	{
		head.OP = htons(IX_TRANS_REQ);
		head.SUB_OP = SUB_OP;
		headLen  = sizeof(head);
		memcpy(sendData, &head, headLen);

		for (dataIndex = 0; dataIndex < dataLen; dataIndex += stringLen)
		{
			stringLen = ((dataLen - dataIndex >= IX_REPORT_DATA_LEN) ? IX_REPORT_DATA_LEN : dataLen - dataIndex);
			memcpy(sendData + headLen, data+dataIndex, stringLen);
			sendLen = headLen + stringLen;
			sendData[sendLen++] = 0;
			API_IxProxyClient(sendData, sendLen);
			usleep(50*1000);
		}
	}

	return 0;
}


