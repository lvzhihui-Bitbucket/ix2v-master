
#include <stdio.h>
#include <strings.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <sys/time.h>
#include <stdlib.h>
#include <memory.h>
#include <arpa/inet.h>
#include <arpa/nameser.h>
#include <netinet/in.h>
#include <net/if.h>
#include <netdb.h>
#include <poll.h>
#include <pthread.h>
#include <error.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <resolv.h>
#include "utility.h"

#include "vtk_udp_stack_class.h"
#include "one_tiny_task.h"
#include "tcp_Client_process.h"
#include "tcp_server_process.h"
#include "obj_IoInterface.h"
#include "task_IxProxy.h"
#include "elog.h"
#include "obj_PublicInformation.h"

static IxBuilderRemoteRun ixDeviceRemoteRun = {.lock = PTHREAD_MUTEX_INITIALIZER, .beatHeartTimeing = 0, .client = NULL, .cmd_id = 0, .autoConnectEnable = 0};

//设置套接字为非阻塞, blockEnable：0 --非阻塞，1 --阻塞
//return：0 --设置失败，1 -- 设置成功
static int MyFcntl(int fd, int blockEnable)
{
	int flags = fcntl(fd, F_GETFL, 0);
	if (flags < 0) 
	{
		eprintf("Get flags error:%s\n");
		return 0;
	}
	if(blockEnable)
	{
		flags &= (~O_NONBLOCK);
	}
	else
	{
		flags |= O_NONBLOCK;
	}

	if (fcntl(fd, F_SETFL, flags) < 0) 
	{
		eprintf("Set flags error:%s\n");
		return 0;
	}

	return 1;
}

int tcp_client_connect_with_timeout(char* net_dev_name, char* ip_str, int port, int t_timeout, int* psockfd )
{
	int fd = 0;
	int m_loop;
	struct sockaddr_in	addr;
	struct sockaddr_in	sin;
	fd_set fdr, fdw;
	struct ifreq ifr;
	
	struct timeval timeout;
	int err = 0;
	int errlen = sizeof(err);
	
	bzero(&addr, sizeof(addr));
	addr.sin_family = AF_INET;
	addr.sin_port = htons(port);
	inet_pton(AF_INET, ip_str, &addr.sin_addr);

	bzero(&sin, sizeof(sin));	
	sin.sin_family = AF_INET;
	sin.sin_addr.s_addr = INADDR_ANY;
	sin.sin_port = htons(port);
	

	fd = socket(AF_INET,SOCK_STREAM,0);
	
	if (fd < 0) 
	{
		eprintf("create socket failed\n");
		return -1;
	}

	//如果要已经处于连接状态的soket在调用closesocket后强制关闭，不经历TIME_WAIT的过程
	struct linger soLinger;
	soLinger.l_onoff = 1;
	soLinger.l_linger = 0;
	if(setsockopt(fd, SOL_SOCKET, SO_LINGER, &soLinger, sizeof(soLinger)) == -1)
	{
		eprintf("sockfd =%d, Setsockopt SOL_SOCKET::SO_LINGER Failure!\n", fd);
		goto connect_error;
	}

	//如果在已经处于 ESTABLISHED状态下的socket(一般由端口号和标志符区分）调用closesocket（一般不会立即关闭而经历TIME_WAIT的过程）后想继续重用该socket
	m_loop = 1;
	if( setsockopt(fd, SOL_SOCKET, SO_REUSEADDR, &m_loop, sizeof(m_loop)) == -1 )
	{
		eprintf("sockfd =%d, Setsockopt SOL_SOCKET::SO_REUSEADDR Failure!\n", fd);
		goto connect_error;
	}

	if(net_dev_name != NULL)
	{
		memset(ifr.ifr_name, 0, IFNAMSIZ);
		sprintf(ifr.ifr_name, "%s", net_dev_name);
		if(setsockopt(fd, SOL_SOCKET, SO_BINDTODEVICE, (char *)&ifr, sizeof(struct ifreq)))
		{
			eprintf("Setsockopt SOL_SOCKET::SO_BINDTODEVICE Failure!\n");
			goto connect_error;
		}

		if(bind(fd, (struct sockaddr *) &sin, sizeof(sin)) == -1)
		{
			eprintf("fail to bind\n");
			goto connect_error;
		}
	}

	/* 通过选项SO_RCVTIMEO和SO_SNDTIMEO设置的超时时间的类型时timeval, 和select系统调用的超时参数类型相同 */
    timeout.tv_sec = t_timeout;
    timeout.tv_usec = 0;
    if (setsockopt(fd, SOL_SOCKET, SO_SNDTIMEO, &timeout, sizeof(timeout)) == -1) 
	{
		goto connect_error;
    }

	int rc = connect(fd, (struct sockaddr*)&addr, sizeof(addr));
	if (rc != 0) 
	{
		if (errno == EINPROGRESS) 
		{
			printf("Doing connection.\n");
			/*正在处理连接*/
			FD_ZERO(&fdr);
			FD_ZERO(&fdw);
			FD_SET(fd, &fdr);
			FD_SET(fd, &fdw);
			timeout.tv_sec = t_timeout;
			timeout.tv_usec = 0;
			rc = select(fd + 1, &fdr, &fdw, NULL, &timeout);
			printf("rc is: %d\n", rc);
			/*select调用失败*/
			if (rc < 0) 
			{
				fprintf(stderr, "connect error:%s\n", strerror(errno));
				goto connect_error;
			}
		
			/*连接超时*/
			if (rc == 0) 
			{
				fprintf(stderr, "Connect timeout.\n");
				goto connect_error;
			}
			/*[1] 当连接成功建立时，描述符变成可写,rc=1*/
			if (rc == 1 && FD_ISSET(fd, &fdw)) 
			{
				goto connect_success;
			}
			/*[2] 当连接建立遇到错误时，描述符变为即可读，也可写，rc=2 遇到这种情况，可调用getsockopt函数*/
			if (rc == 2) 
			{
				if (getsockopt(fd, SOL_SOCKET, SO_ERROR, &err, &errlen) == -1) 
				{
					fprintf(stderr, "getsockopt(SO_ERROR): %s", strerror(errno));
					goto connect_error;
				}
			}
		}
		goto connect_error;
	} 

	connect_success:
	MyFcntl(fd, 1);

	if(psockfd != NULL)
	{
		*psockfd = fd;
	}
	return 0;

	connect_error:

	if(fd >= 0)
	{
		close(fd);
	}
	fprintf(stderr, "connect failed, error:%s.\n", strerror(errno));
	return -1;
}


int Api_tcp_client_connect_to_server(char* net_dev_name, char *ip_str, int port, char* id ,char* pwd)
{
	int retry_cnt;
	int  err, num;
	int fd = 0;
	ProxyRemoteOnlineRsp recv_pkt;
	int recv_len;
	
	memset(&recv_pkt,0,sizeof(ProxyRemoteOnlineRsp));
	

	for(retry_cnt = 2, err = -1; retry_cnt > 0 && err != 0; retry_cnt--)
	{
		err = tcp_client_connect_with_timeout(net_dev_name, ip_str, port, 3, &fd);
	}
	
	if(err == 0)
	{
		// send
		if(TCP_ClientLoginOrLogoutReq(fd, 0, 1, id, pwd, 3)>0)
		{
			// recv
			num = Api_tcp_client_recv(fd, (char*)&recv_pkt, sizeof(recv_pkt), 3);
			if (num > 0) // success
			{
				if(recv_pkt.head.cmd_type == IX_DEVICE_REMOTE_PROXY_ONLINE_RSP && recv_pkt.online == 1 && recv_pkt.result == 0)
				{
					return fd;
				}
				else
				{
					eprintf("Login error: respone error recv_pkt.head.cmd_type=0x%04x, recv_pkt.online=%d, recv_pkt.result=%d\n", recv_pkt.head.cmd_type, recv_pkt.online, recv_pkt.result);
				}
			}
			else
			{
				eprintf("Login error: wait respone timeout [server:%s, port:%d net_dev_name:%s]\n", ip_str, port, net_dev_name);
			}
		}
		else
		{
			eprintf("Login error: send request timeout [server:%s, port:%d net_dev_name:%s]\n", ip_str, port, net_dev_name);
		}
	}
	else
	{
		eprintf("TCP connect error [server:%s, port:%d, net_dev_name:%s, fd=%d]\n", ip_str, port, net_dev_name, fd);
	}

	if(fd > 0)
	{
		close(fd);
		fd = 0;
	}
	
	return fd;
}

int Api_tcp_client_disconnect(int* fd)
{
	if(*fd != 0)
	{
		close(*fd);
		*fd=0;
	}
}

int Api_tcp_client_send(int fd, const char *dat, int dat_len, int timeout)
{	
	int ret = -1;
	if(timeout)
	{
		struct timeval tv;
		tv.tv_sec = timeout;
		tv.tv_usec = 0;

		if(setsockopt(fd, SOL_SOCKET, SO_SNDTIMEO, &tv, sizeof(tv)) < 0)
		{
	        dprintf("setsockopt SOL_SOCKET SO_SNDTIMEO error:%s\n", strerror(errno));
			return ret;
		}
		ret = send(fd, dat, dat_len, 0);
	}
	else
	{
		ret = send(fd, dat, dat_len, MSG_DONTWAIT);
	}
	
	return ret;
}

int Api_tcp_client_recv(int fd, char *dat, int dat_len, int timeout)
{	
	int ret = -1;
	if(timeout)
	{
		struct timeval tv;
		tv.tv_sec = timeout;
		tv.tv_usec = 0;

		if(setsockopt(fd, SOL_SOCKET, SO_RCVTIMEO, &tv, sizeof(tv)) < 0)
		{
	        dprintf("setsockopt SOL_SOCKET SO_RCVTIMEO error:%s\n", strerror(errno));
			return ret;
		}
		ret = recv(fd, dat, dat_len, 0);
	}
	else
	{
		ret = recv(fd, dat, dat_len, MSG_DONTWAIT);
	}

	return ret;
}

int TCP_ClientBeatHeart(int fd, short cmdId, int id)
{
	ProxyRemoteBeatHeart beatHeart;

	memcpy(beatHeart.head.head, IX_DEVICE_REMOTE_CMD_HEAD, 4);
	beatHeart.head.cmd_type = IX_DEVICE_REMOTE_NORMAL_BEAT_HEART_REQ;
	beatHeart.head.cmd_id = cmdId;
	beatHeart.head.dev_type = 0;
	beatHeart.head.data_len = sizeof(beatHeart) - sizeof(beatHeart.head);
	beatHeart.ID = id;
	beatHeart.RESERVE = 0x0000;
	
	log_d("TCP_ClientBeatHeart, cmd_id =%d, len=%d\n", beatHeart.head.cmd_id, sizeof(beatHeart));

	return Api_tcp_client_send(fd, (char*)&beatHeart, sizeof(beatHeart), 0);
}

int TCP_ClientLoginOrLogoutReq(int fd, short cmdId, short online, char* id, char* pwd, int timeout)
{
	ProxyRemoteOnlineReq send_pkt;
	memcpy(send_pkt.head.head, IX_DEVICE_REMOTE_CMD_HEAD, 4);
	send_pkt.head.cmd_type = IX_DEVICE_REMOTE_PROXY_ONLINE_REQ;
	send_pkt.head.cmd_id = 0;
	send_pkt.head.dev_type = 0;
	send_pkt.head.data_len = sizeof(send_pkt) - sizeof(ProxyRemoteHead);
	send_pkt.online = 1;
	send_pkt.reserve = 0;
	strncpy(send_pkt.proxyId, id, 32);
	strncpy(send_pkt.proxyPwd, pwd, 16);

	return Api_tcp_client_send(fd, (char*)&send_pkt, sizeof(send_pkt), timeout);
}

int ProxyRemoteRspData(int fd, short cmd_id, short businessCode, char* data, int len)
{
	ProxyRemoteTransmitRsp sendPacket;
	int headLen;
	char sendData[IX_DEVICE_REMOTE_TCP_BUFF_MAX];

	memcpy(sendPacket.head.head, IX_DEVICE_REMOTE_CMD_HEAD, 4);
	sendPacket.head.cmd_type = IX_DEVICE_REMOTE_NORMAL_TRANSFER_DAT;
	sendPacket.head.cmd_id = cmd_id;
	sendPacket.head.dev_type = 0;
	sendPacket.head.data_len = sizeof(businessCode) + len;
	sendPacket.businessCode = businessCode;
	headLen = sizeof(sendPacket);

	
	memcpy(sendData, &sendPacket, headLen);
	memcpy(sendData + headLen, data, len);
	
	return Api_tcp_client_send(fd, sendData, headLen + len, 0);
}

int ProxyRemoteTransferData(int fd, short businessCode, char* data, int len)
{
	ProxyRemoteTransmitRsp sendPacket;
	int headLen;
	char sendData[IX_DEVICE_REMOTE_TCP_BUFF_MAX];

	memcpy(sendPacket.head.head, IX_DEVICE_REMOTE_CMD_HEAD, 4);
	sendPacket.head.cmd_type = IX_DEVICE_REMOTE_NORMAL_TRANSFER_DAT;
	sendPacket.head.cmd_id = ixDeviceRemoteRun.cmd_id++;
	sendPacket.head.dev_type = 0;
	sendPacket.head.data_len = sizeof(businessCode) + len;
	sendPacket.businessCode = businessCode;
	headLen = sizeof(sendPacket);

	
	memcpy(sendData, &sendPacket, headLen);
	memcpy(sendData + headLen, data, len);
		
	return Api_tcp_client_send(fd, sendData, headLen + len, 0);
}

static void ProxyRemoteCmdProcess(ProxyRemoteHead packHead, char* data, int dat_len)
{
	short businessCode;
	
	businessCode = (data[1] << 8) + data[0];
	
	switch(packHead.cmd_type)
	{
		case IX_DEVICE_REMOTE_NORMAL_TRANSFER_DAT:
			IxProxyRecvDataProcess(businessCode, packHead.cmd_id, data+2, dat_len-2);
			break;
		case IX_DEVICE_REMOTE_PROXY_ONLINE_RSP:
			break;
		case IX_DEVICE_REMOTE_NORMAL_BEAT_HEART_RSP:
			ixDeviceRemoteRun.beatHeartTimeing = 0;
			break;
	}
}

static void client_socket_task_process( void* arg )
{
	p_client_tcp_ins ptr_ins = (p_client_tcp_ins*)arg;
	int recv_len, num, timeCnt, id;
	ProxyRemoteHead packHead;
	char recv_data[IX_DEVICE_REMOTE_TCP_BUFF_MAX];	
	timeCnt = 0;
	id = 0;

	dprintf("---------------------client_socket_task_process init...\n");
	//client_socket_task_init
	ixDeviceRemoteRun.cmd_id = 0;
	ixDeviceRemoteRun.beatHeartTimeing = 0;

	snprintf(recv_data, IX_DEVICE_REMOTE_TCP_BUFF_MAX, "%s@%s", ptr_ins->id, ptr_ins->ip); 
	SetMyProxyData(NULL, NULL, recv_data, ptr_ins->net_dev_name);
	cJSON* info = cJSON_CreateString(recv_data);
	SetIxProxyConnectState(Remote, ptr_ins->net_dev_name, info);
	cJSON_Delete(info);

	dprintf("---------------------client_socket_task_process start...\n");
	ptr_ins->taskRunFlag = 1;

	while(ptr_ins->taskRunFlag)
	{		
		recv_len = sizeof(ProxyRemoteHead);
		
		num = recv(ptr_ins->fd, (char*)&packHead, recv_len, MSG_DONTWAIT);
		if(num == recv_len && !memcmp(packHead.head, "VTEK", 4) && packHead.data_len > 0)
		{
			recv_len = packHead.data_len;
			num = recv(ptr_ins->fd, recv_data, recv_len, MSG_DONTWAIT);
			
			//dprintf("num=%d, cmd_type=0X%04x, cmd_id=%d, dev_type=%d, data_len=%d\n", num, packHead.cmd_type, packHead.cmd_id, packHead.dev_type, packHead.data_len);
			
			if(num != recv_len)
			{
				//数据包有错，清空接收缓存的数据
				recv_len = IX_DEVICE_REMOTE_TCP_BUFF_MAX;
				while(recv(ptr_ins->fd, recv_data, recv_len, MSG_DONTWAIT) > 0);
			}
			else 
			{
				//数据包处理
				ProxyRemoteCmdProcess(packHead, recv_data, recv_len);
			}
		}
		//TCP服务器已经断开
		else if(num == 0)
		{
			dprintf("TCP disconnect..........\n");
			break;
		}
		
		usleep(IX_DEVICE_REMOTE_TASK_CYCLE_TIME_MS*1000);


		if(++timeCnt >= IX_DEVICE_REMOTE_SEND_BEAT_HEART_TIME_S*1000/IX_DEVICE_REMOTE_TASK_CYCLE_TIME_MS)
		{
			timeCnt = 0;
			TCP_ClientBeatHeart(ptr_ins->fd, ixDeviceRemoteRun.cmd_id++, ++id);
		}
		
		if(++ixDeviceRemoteRun.beatHeartTimeing >= IX_DEVICE_REMOTE_SEND_RECV_HEART_TIME_S*1000/IX_DEVICE_REMOTE_TASK_CYCLE_TIME_MS)
		{
			dprintf("TCP server no respond\n");
			break;
		}
	}

	//client_socket_task_exit
	TCP_ClientLoginOrLogoutReq(ptr_ins->fd, ixDeviceRemoteRun.cmd_id++, 0, ptr_ins->id, ptr_ins->pwd, 1);
	Api_tcp_client_disconnect(&ptr_ins->fd);
	free( ptr_ins );
	ptr_ins = NULL;
	ixDeviceRemoteRun.client = NULL;
	SetMyProxyUnlinked();
	dprintf("------------------client_socket_task_process end...\n");
	pthread_exit(NULL);	
}

static p_client_tcp_ins ProxyConnectToServer(char* net_dev_name, char *ip_str, int port, char* id, char* pwd)
{	
	p_client_tcp_ins	ptr_ins;
	ptr_ins = (p_client_tcp_ins)malloc(sizeof(client_tcp_ins));
	struct ifreq ifr;
	char tempData[IX_BUILDER_NAME_LEN*2];

	if( ptr_ins == NULL )
		return NULL;
	
	if(IsMyProxyLinked())
		return NULL;
		
	ptr_ins->port = port;	
	snprintf(ptr_ins->net_dev_name, 16, "%s", net_dev_name);
	snprintf(ptr_ins->ip, 16, "%s", ip_str);
	snprintf(ptr_ins->id, 32, "%s", id);
	snprintf(ptr_ins->pwd, 16, "%s", pwd);
	ptr_ins->taskRunFlag = 0;

	ptr_ins->fd = Api_tcp_client_connect_to_server(net_dev_name, ptr_ins->ip, ptr_ins->port, ptr_ins->id, ptr_ins->pwd);
	if(ptr_ins->fd > 0)
	{
		if(IsMyProxyLinked() == Idle)
		{
			if(pthread_create(&ptr_ins->pid, NULL,(void*)client_socket_task_process, ptr_ins) == 0 )
			{
				return ptr_ins;
			}
		}
		else if(IsMyProxyLinked() == Local)
		{
			TCP_ClientLoginOrLogoutReq(ptr_ins->fd, ixDeviceRemoteRun.cmd_id++, 0, ptr_ins->id, ptr_ins->pwd, 1);
		}
	}
	else
	{
		eprintf("Connect  error:%s [ip=%s, port=%d] [%s, %s]\n", net_dev_name, ptr_ins->ip, ptr_ins->port, ptr_ins->id, ptr_ins->pwd);		
	}

	if(ptr_ins->fd > 0)
	{
		close(ptr_ins->fd);
	}

	if( ptr_ins != NULL )
	{
		free( ptr_ins );
		ptr_ins = NULL;
	}

	return NULL;		
}



static int ProxyDisconnectToServer(p_client_tcp_ins ptr_ins)
{
	if( ptr_ins == NULL )
		return -1;
	int pid = ptr_ins->pid;
	
	// 等待线程结束
	if(ptr_ins->taskRunFlag)
	{
		ptr_ins->taskRunFlag = 0;
		if(pid != pthread_self())
		{
			pthread_join(pid, NULL);
		}
	}
	
	return 0;
}

p_client_tcp_ins GetIxProxyRemoteTcpFd(void)
{
	p_client_tcp_ins 	client;
	
	pthread_mutex_lock(&ixDeviceRemoteRun.lock);
	client = ixDeviceRemoteRun.client;
	pthread_mutex_unlock(&ixDeviceRemoteRun.lock);
	
	return client;
}


int XDRemoteConnect(const char* jsonPara)
{
	char netWork[10];
	char server[20];
	char account[100];
	char pwd[100];
	int ret = 0;
	
	if(IsMyProxyLinked())
	{
		return NULL;
	}

	pthread_mutex_lock(&ixDeviceRemoteRun.lock);
	ixDeviceRemoteRun.autoConnectEnable = 1;
	cJSON* para = cJSON_Parse(jsonPara);

	if(!GetJsonDataPro(para, "IXD_NT", netWork))
	{
		char* sipNetwork = API_PublicInfo_Read_String(PB_SIP_CONNECT_NETWORK);
		if(sipNetwork)
		{
			if(!strcmp(sipNetwork, "4G"))
			{
				strcpy(netWork, NET_USB0);
			}
			else if(!strcmp(sipNetwork, "WLAN"))
			{
				strcpy(netWork, NET_WLAN0);
			}
			else if(!strcmp(sipNetwork, "LAN"))
			{
				strcpy(netWork, NET_ETH0);
			}
		}
	}
	
	if(!GetJsonDataPro(para, IXD_CloudServer, server))
	{
		API_Para_Read_String(IXD_CloudServer, server);
	}

	if(!GetJsonDataPro(para, PRJ_ID, account))
	{
		API_Para_Read_String(PRJ_ID, account);
	}

	if(!GetJsonDataPro(para, PRJ_PWD, pwd))
	{
		API_Para_Read_String(PRJ_PWD, pwd);
	}
	cJSON_Delete(para);

	if(ixDeviceRemoteRun.client == NULL && RegexCheck("^(wlan0)|(eth0)|(p2p0)$", netWork))
	{
		ixDeviceRemoteRun.client = ProxyConnectToServer(netWork, server, IX_DEVICE_REMOTE_TCP_PORT, account, pwd);
		if(ixDeviceRemoteRun.client)
		{					 
			log_d("XD connect to %s by network %s (account: %s, pwd:%s, fd:%d) success.", server, netWork, account, pwd, ixDeviceRemoteRun.client->fd);
		 	ret = 1;
		}
		else
		{
			log_e("XD connect to %s by network %s (account: %s, pwd:%s, fd:%d) error.", server, netWork, account, pwd);
		}
	}
	pthread_mutex_unlock(&ixDeviceRemoteRun.lock);
	
	return ret;
}


void XDRemoteDisconnect(void)
{
	pthread_mutex_lock(&ixDeviceRemoteRun.lock);
	if(ixDeviceRemoteRun.client != NULL)
	{
		ProxyDisconnectToServer(ixDeviceRemoteRun.client);
		ixDeviceRemoteRun.client = NULL;
		log_d("XD disconnect.");
	}
	ixDeviceRemoteRun.autoConnectEnable = 0;
	pthread_mutex_unlock(&ixDeviceRemoteRun.lock);
}

int IsXDRemoteConnect(void)
{
	int ret;
	pthread_mutex_lock(&ixDeviceRemoteRun.lock);
	ret = ((ixDeviceRemoteRun.client != NULL) ? 1 : 0);
	pthread_mutex_unlock(&ixDeviceRemoteRun.lock);
	return ret;
}

void IxBuilderTcpClientSwitchLoad(void)
{
	ixDeviceRemoteRun.autoConnectEnable = (API_Para_Read_Int(IXD_AutoLogin) || API_Para_Read_Int(IXD_AutoConnect));
	if(ixDeviceRemoteRun.autoConnectEnable)
	{
		XDRemoteConnect(NULL);
	}
	else
	{
		XDRemoteDisconnect();
	}
}

void IxBuilderTcpClientConnectPolling(void)
{
	if(ixDeviceRemoteRun.autoConnectEnable)
	{
		XDRemoteConnect(NULL);
	}
}

