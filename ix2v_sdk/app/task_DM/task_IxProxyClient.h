/**
  ******************************************************************************
  * @file    task_IxProxy.h
  * @author  zeng
  * @version V1.0.0
  * @date    2012.08.15
  * @brief   This file contains the headers of the power task_3.
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2012 V-Tec</center></h2>
  ******************************************************************************
  */
#ifndef TASK_IxProxyClient_H
#define TASK_IxProxyClient_H

#include "task_survey.h"
#include "tcp_server_process.h"
#include "obj_GetIpByNumber.h"
#include "obj_DeviceManage.h"
#include "obj_GetAboutByIp.h"
#include "obj_GetInfoByIp.h"
#include "obj_SearchIpByFilter.h"
#include "obj_SYS_VER_INFO.h"

#define XD_APP_REPORT_TYEP_SHELL		0x0405			//shell
#define XD_APP_REPORT_TYEP_Event		0x0406			//事件汇报请求
#define XD_APP_REPORT_TYEP_FTP		  0x0407			//FTP


/*******************************************************************************
                         Define task event flag
*******************************************************************************/

/*******************************************************************************
                  Define task vars, structures and macro
*******************************************************************************/

void vtk_TaskInit_IxProxy_client(void);

static void vdp_ix_proxy_client_mesg_data_process(char* msg_data, int len);

#endif
