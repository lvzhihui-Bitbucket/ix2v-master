/**
  ******************************************************************************
  * @file    obj_PublicUdpCmd.h
  * @author  cao
  * @version V00.01.00
  * @date    2016.5.31
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 

#ifndef _obj_PublicUdpCmd_H
#define _obj_PublicUdpCmd_H

#include "task_IxProxy.h"
#include "obj_CmdResDownload.h"
#include "obj_CmdBackup.h"
#include "obj_CmdRestore.h"


// Define Object Property-------------------------------------------------------
typedef int (*LocalProcess)(void*);
typedef char *(*CreateJsonData)(void*);


// Define Object Function - Public----------------------------------------------
int PublicUdpCmdReqProcess(int ip, int timeOut, unsigned short cmd, LocalProcess loaclProcess, CreateJsonData createJson, void* data);

char* CreateUdpBackupReqJsonData(void* data);
static int ParseUdpBackupReqJsonData(const char* json, CMD_BACKUP_ONE_REQ_T* pData);

// Define Object Function - Private---------------------------------------------



#endif


