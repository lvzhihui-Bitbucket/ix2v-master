/**
  ******************************************************************************
  * @file    obj_PublicMulticastCmd.h
  * @author  cao
  * @version V00.01.00
  * @date    2022.3.28
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 

#ifndef _obj_PublicMulticastCmd_H
#define _obj_PublicMulticastCmd_H

#include "cJSON.h"

// Define Object Property-------------------------------------------------------
#define MULTICAST_DATA_LEN		    1000

typedef struct
{
    int search1Timeout;			//如果发出search1指令之后，没有设备回应，最长等待时间
    int	search1IntervalTime;	//接收到一个设备之后，超过这段时间没有其他设备应答，当作后面没有设备回来了
    int	search2Enable;			//搜索1结束之后，是否需要发送搜索2
    int	search2Timeout;			//如果发出search2指令之后，没有设备回应，最长等待时间
    int	search2IntervalTime;	//接收到一个设备之后，超过这段时间没有其他设备应答，当作后面没有设备回来了
}MulticastCfg;

typedef struct
{
	int					len;			//数据长度
	char*				data;			//数据内容指针
}MulticastCmdData;

typedef struct
{
    cJSON* packetAddr;				//应答包内存地址数组					
}MulticastCmdOutData;

typedef struct
{
    int				rand;											//随机数
    int				sourceIp;										//源ip
    int				recDevCnt;										//已经搜索到的回复设备
    int				dataLen;										//后面的数据长度
    char			data[MULTICAST_DATA_LEN];
} MulticastCmdReq;

typedef struct
{
    int						rand;				//随机数
    int						sourceIp;			//源ip
	char    				MFG_SN[13];			//应答设备的序列号，为了去重复
    int						result;				//result
    int 					dataLen;			//后面的数据长度
    char					data[MULTICAST_DATA_LEN];
} MulticastCmdRsp;

typedef int (*MulticastCmdProcess)(int sourceIp, MulticastCmdData dataIn, MulticastCmdData* dataOut);
typedef struct
{
	char    			MFG_SN[13];			//应答设备的序列号，为了去重复
    int					sourceIp;			//源ip
    int					result;
	int					dataLen;			//数据长度
	char*				data;				//接收应答数据
}MulticastPacket;

typedef struct
{
    int					                waitFlag;			//等待回应标记
    int					                rand;				//随机数
	int					                searchAllTimeCnt;	//搜索总时间计时
	int					                searchTimeCnt;		//搜索计时
	int					                searchRspFlag;		//是否有设备回应标记
	int					                searchMaxDevice;	//搜索最大设备数
    MulticastCmdProcess			        process;			//处理函数
    MulticastCfg                        cfg;               //搜索配置
    pthread_mutex_t 	                lock;
    cJSON*	                            packet;				//应答包					
}MulticastCmdRun;

typedef struct
{
    int					                reqCmd;			  //请求指令字
    int					                rspCmd;				//应答指令字
    MulticastCmdProcess			        process;			//处理函数
}MulticastCmdtoProcess;

typedef struct
{
    pthread_mutex_t 	lock;
    cJSON*				table;
}MyMulticastTable;

typedef struct
{
	int					searchResult;			//搜索结果

	int					searchNetMode;			//搜索使用哪个网卡

	int					search1Timeout;			//如果发出search1指令之后，没有设备回应，最长等待时间
	int					search1IntervalTime;	//接收到一个设备之后，超过这段时间没有其他设备应答，当作后面没有设备回来了
	int					search2Enable;			//搜索1结束之后，是否需要发送搜索2
	int					search2Timeout;			//如果发出search2指令之后，没有设备回应，最长等待时间
	int					search2IntervalTime;	//接收到一个设备之后，超过这段时间没有其他设备应答，当作后面没有设备回来了

	int					searchAllTime;			//搜索总时间计时
	
	int					search1Time;			//搜索1花费时间
	int					search1DevCnt;			//搜索1获取设备数量
	int					search1FirstDevTime;	//搜索1第一个设备回来时间
	
	int					search2Time;			//搜索2花费时间
	int					search2DevCnt;			//搜索2获取设备数量
	int					search2FirstDevTime;	//搜索2第一个设备回来时间
	int					search2SendCnt;			//搜索2发送次数
} MulticastSearchResult;


// Define Object Function - Public----------------------------------------------
MulticastSearchResult MulticastCmdRequst(int getDeviceCnt, int timeOut, unsigned short cmd, MulticastCfg config, MulticastCmdData dataIn, MulticastCmdOutData* dataOut);
void ReceiveMulticastCmdRsp(unsigned short cmd, MulticastCmdRsp* rspData);
int ReceiveMulticastCmdReq(char* netDeviceName, unsigned short cmd, MulticastCmdReq* reqData);
void FreeMulticastOutData(MulticastCmdOutData dataOut);
MulticastCfg GetMulticastPara(const cJSON* config);

// Define Object Function - Private---------------------------------------------



#endif


