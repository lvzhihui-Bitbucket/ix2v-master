/**
  ******************************************************************************
  * @file    obj_SetDivertState.c
  * @author  czb
  * @version V00.01.00
  * @date    2023.3.13
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 
  
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <memory.h>
#include <string.h>
#include "cJSON.h"
#include "obj_SetDivertState.h"
#include "obj_PublicUnicastCmd.h"
#include "vtk_udp_stack_device_update.h"
#include "task_Event.h"
#include "define_string.h"
#include "obj_IoInterface.h"

int SetDivertStateProcess(int ip, PublicUnicastCmdData dataIn, PublicUnicastCmdData* dataOut)
{
	int ret = -1;
	cJSON *tbProcess = cJSON_Parse(dataIn.data);
	if(tbProcess)
	{
		if(dataOut != NULL)
		{
			(*dataOut).data = cJSON_PrintUnformatted(tbProcess);
			(*dataOut).len = strlen((*dataOut).data) + 1;
		}

		cJSON_AddStringToObject(tbProcess, EVENT_KEY_EventName, EventSetDivertState);
		API_Event_Json(tbProcess);
		cJSON_Delete(tbProcess);
		ret = 0;
	}

	return ret;
}


/*
	source: 自己的房号
	target：发送的目标房号
	setDev：设置的房号
	onOffCheck：设置状态OFF-关闭，ON-开启，CHECK-查询
	message:信息说明
*/
int API_SetDivertState(char* source, char* target, char* setDev, char* onOffCheck, char* message)
{
	cJSON* sendJson = cJSON_CreateObject();
	cJSON* ipTb = cJSON_CreateArray();
	PublicUnicastCmdData dataIn;
	int retInt = IXS_GetByNBR(NULL, NULL, target, target, ipTb);
	if(retInt > 0)
	{
		cJSON_AddStringToObject(sendJson, IX2V_SOURCE, source);
		cJSON_AddStringToObject(sendJson, IX2V_TARGET, setDev);
		cJSON_AddStringToObject(sendJson, IX2V_STATE, onOffCheck);
		cJSON_AddStringToObject(sendJson, IX2V_MSG, message);
		dataIn.data = cJSON_PrintUnformatted(sendJson);
		cJSON_Delete(sendJson);

		if(dataIn.data)
		{
			dataIn.len = strlen(dataIn.data) + 1;
		}
		else
		{
			dataIn.len = 0;
		}

		cJSON* element;
		cJSON_ArrayForEach(element, ipTb)
		{
			char* ipString = GetEventItemString(element, IX2V_IP_ADDR);
			PublicUnicastCmdRequst(inet_addr(ipString), NULL, BusinessWaitUdpTime, SET_DIVERT_STATE_REQ, dataIn, NULL);
		}

		if(dataIn.data)
		{
			free(dataIn.data);
		}
	}
	cJSON_Delete(ipTb);

	return retInt;
}



int GetQR_CodeProcess(int ip, PublicUnicastCmdData dataIn, PublicUnicastCmdData* dataOut)
{
	int ret = -1;
	cJSON *tbProcess = cJSON_Parse(dataIn.data);
	if(tbProcess)
	{
		if(dataOut != NULL)
		{
			char sip_acc[100];
			char sip_pwd[100];
			char* im_addr = GetEventItemString(tbProcess, IX2V_TARGET);

			if(API_Para_Read_Int(SIP_ENABLE)==1&&get_sip_ix_im_acc_from_tb2(im_addr, sip_acc, sip_pwd) == 0)
			{
				(*dataOut).data = create_sip_phone_qrcode(sip_acc,sip_pwd);
				(*dataOut).len = strlen((*dataOut).data) + 1;
			}
			else
			{
				(*dataOut).data = NULL;
				(*dataOut).len = 0;
			}
		}
		cJSON_Delete(tbProcess);
		ret = 0;
	}

	return ret;
}


/*
	source: 自己的房号
	target：发送的目标房号
	getDev：获取二维码的房号
	返回二维码字符串：如无返回NULL；如非NULL,使用完后需要调用free()释放。
*/
char* API_GetQR_Code(char* source, char* target, char* getDev)
{
	char* ret = NULL;
	cJSON* sendJson = cJSON_CreateObject();
	cJSON* ipTb = cJSON_CreateArray();
	PublicUnicastCmdData dataIn;
	PublicUnicastCmdData dataOut;
	int retInt = IXS_GetByNBR(NULL, NULL, target, target, ipTb);
	if(retInt > 0)
	{
		cJSON_AddStringToObject(sendJson, IX2V_SOURCE, source);
		cJSON_AddStringToObject(sendJson, IX2V_TARGET, getDev);
		dataIn.data = cJSON_PrintUnformatted(sendJson);
		cJSON_Delete(sendJson);

		if(dataIn.data)
		{
			dataIn.len = strlen(dataIn.data) + 1;
		}
		else
		{
			dataIn.len = 0;
		}

		cJSON* element = cJSON_GetArrayItem(ipTb, 0);
		char* ipString = GetEventItemString(element, IX2V_IP_ADDR);

		if(PublicUnicastCmdRequst(inet_addr(ipString), NULL, BusinessWaitUdpTime, GET_QR_CODE_REQ, dataIn, &dataOut) == 0)
		{
			if(dataOut.len==0)
				ret=NULL;
			else
				ret = dataOut.data;
		}

		if(dataIn.data)
		{
			free(dataIn.data);
		}
	}
	cJSON_Delete(ipTb);

	return ret;
}

/*********************************************************************************************************
**  End Of File
*********************************************************************************************************/

