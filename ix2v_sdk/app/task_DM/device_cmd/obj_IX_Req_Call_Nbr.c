/**
  ******************************************************************************
  * @file    obj_CmdReboot.c
  * @author  czb
  * @version V00.01.00
  * @date    2016.5.31
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 
  
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <memory.h>
#include "obj_IX_Req_Call_Nbr.h"
#include "task_IxProxy.h"
#include "tcp_server_process.h"
#include "cJSON.h"
#include "vtk_udp_stack_device_update.h"


IxReqCallNbrRun ixReqCallNbrRun;

char* CreateIxReqCallNbrJsonData(IX_REQ_CALL_NBR_DATA_T* data)
{
    cJSON *root = NULL;
	char *string = NULL;

    root = cJSON_CreateObject();

	cJSON_AddStringToObject(root, "IP_Addr", data->IP_Addr);
	cJSON_AddStringToObject(root, "MFG_SN", data->MFG_SN);
	cJSON_AddStringToObject(root, "DeviceNbr", data->DeviceNbr);

	cJSON_AddStringToObject(root, "Local", data->Local);
	cJSON_AddStringToObject(root, "Global", data->Global);
	cJSON_AddStringToObject(root, "name1", data->name1);
	cJSON_AddStringToObject(root, "name2_utf8", data->name2_utf8);

	string = cJSON_Print(root);

	cJSON_Delete(root);

	return string;
}


int IxReqCallNbrProcess(int ip, int timeOut)
{
	IX_REQ_CALL_NBR_DATA_T cmdDataObj;
	char* dataJson;
	
	IxReqCallNbrReq sendData;
	int i, timeCnt, ret, len;
	

	strcpy(cmdDataObj.DeviceNbr, GetSysVerInfo_BdRmMs());
	strcpy(cmdDataObj.IP_Addr, GetSysVerInfo_IP_by_device(GetNetDeviceNameByTargetIp(ip)));
	strcpy(cmdDataObj.MFG_SN, GetSysVerInfo_Sn());
	
	strcpy(cmdDataObj.Local, GetSysVerInfo_LocalNum());
	strcpy(cmdDataObj.Global, GetSysVerInfo_GlobalNum());
	strcpy(cmdDataObj.name1, GetSysVerInfo_name());
	cmdDataObj.name2_utf8[0] = 0;

	dataJson = CreateIxReqCallNbrJsonData(&cmdDataObj);

	
	sendData.rand = MyRand() + MyRand2(ip);
	sendData.sourceIp = inet_addr(GetSysVerInfo_IP_by_device(GetNetDeviceNameByTargetIp(ip)));
	
	len = sizeof(sendData) - 500;
	
	if(dataJson != NULL)
	{
		strncpy(sendData.data, dataJson, 500);
		len += (strlen(dataJson)+1);
		
		free(dataJson);
	}

	if(sendData.sourceIp == ip)
	{
		return ReceiveIxReqCallNbrCmdReq(&sendData);
	}

	ixReqCallNbrRun.waitFlag = 1;
	ixReqCallNbrRun.result = -1;
	ixReqCallNbrRun.rand = sendData.rand;
	
	api_udp_device_update_send_data(ip, htons(IX_REQ_Prog_Call_Nbr_REQ), (char*)&sendData, len);
	
	timeCnt = timeOut*1000/UDP_WAIT_RSP_TIME;
	while(1)
	{
		usleep(UDP_WAIT_RSP_TIME*1000);
		
		if(ixReqCallNbrRun.waitFlag == 0)
		{
			ret = ixReqCallNbrRun.result;
			break;
		}

		if(--timeCnt == 0)
		{
			ret = -1;
			break;
		}
	}

	ixReqCallNbrRun.waitFlag = 0;

	return ret;
}

//接收到IxReqCallNbr命令应答指令
void ReceiveIxReqCallNbrCmdRsp(IxReqCallNbrRsp* rspData)
{
	if(ixReqCallNbrRun.waitFlag == 1)
	{
		if(rspData->rand == ixReqCallNbrRun.rand)
		{
			ixReqCallNbrRun.sourceIp = rspData->sourceIp;
			ixReqCallNbrRun.result = rspData->result;
			ixReqCallNbrRun.waitFlag = 0;
		}
	}
}

//接收到IxReqCallNbr命令请求指令
int ReceiveIxReqCallNbrCmdReq(IxReqCallNbrReq* rspData)
{
	
	IxReqCallNbrRsp	sendData;
	IxProxyData_T proxyData;
	
	sendData.rand = rspData->rand;
	sendData.sourceIp = inet_addr(GetSysVerInfo_IP_by_device(GetNetDeviceNameByTargetIp(rspData->sourceIp)));

	sendData.result = TransmitCmdToPC(IX_REQ_Prog_Call_Nbr_REQ, rspData->data);

	api_udp_device_update_send_data(rspData->sourceIp, htons(IX_REQ_Prog_Call_Nbr_RSP), (char*)&sendData, sizeof(sendData) );
	return 0;
}

/*********************************************************************************************************
**  End Of File
*********************************************************************************************************/

