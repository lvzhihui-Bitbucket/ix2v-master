/**
  ******************************************************************************
  * @file    obj_SearchIpByFilter.c
  * @author  czb
  * @version V00.01.00
  * @date    2016.5.31
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 
  
#include <stdio.h>
#include <stdlib.h>
#include "obj_SearchDevice.h"
#include "obj_PublicMulticastCmd.h"
#include "obj_PublicUnicastCmd.h"
#include "vtk_udp_stack_device_update.h"

cJSON* API_IXS_Search(const cJSON* cfg, const cJSON* where, const cJSON* view, int state)
{
	MulticastSearchResult result;
	MulticastCfg config = GetMulticastPara(cfg);
	MulticastCmdOutData dataOut;
	MulticastCmdData dataIn;
	MulticastPacket* packet;
	cJSON* ret = cJSON_CreateObject();
	cJSON* table;
	cJSON* element;

	dataIn.data = cJSON_Print(where);
	if(dataIn.data)
	{
		dataIn.len = strlen(dataIn.data) + 1;
	}
	else
	{
		dataIn.len = 0;
	}

	result = MulticastCmdRequst(0, 5, SEARCH_SERVICE_REQ, config, dataIn, &dataOut);

	if(state)
	{
		cJSON_AddNumberToObject(ret, "Result", result.searchResult);
		cJSON_AddNumberToObject(ret, "NetMode", result.searchNetMode);
		cJSON_AddNumberToObject(ret, "1Timeout", result.search1Timeout);
		cJSON_AddNumberToObject(ret, "1IntervalTime", result.search1IntervalTime);
		cJSON_AddNumberToObject(ret, "2Enable", result.search2Enable);
		cJSON_AddNumberToObject(ret, "2Timeout", result.search2Timeout);
		cJSON_AddNumberToObject(ret, "2IntervalTime", result.search2IntervalTime);
		cJSON_AddNumberToObject(ret, "AllTime", result.searchAllTime);

		cJSON_AddNumberToObject(ret, "1Time", result.search1Time);
		cJSON_AddNumberToObject(ret, "1DevCnt", result.search1DevCnt);
		cJSON_AddNumberToObject(ret, "1FirstDevTime", result.search1FirstDevTime);

		cJSON_AddNumberToObject(ret, "2Time", result.search2Time);
		cJSON_AddNumberToObject(ret, "2DevCnt", result.search2DevCnt);
		cJSON_AddNumberToObject(ret, "2FirstDevTime", result.search2FirstDevTime);

		cJSON_AddNumberToObject(ret, "2SendCnt", result.search2SendCnt);
	}

	table = cJSON_CreateArray();
	cJSON_AddItemToObject(ret, "table", table);
	
	cJSON_ArrayForEach(element, dataOut.packetAddr)
	{
		packet = element->valueint;
		if(packet)
		{
			cJSON_AddItemToArray(table, API_GetPbIo(packet->sourceIp, packet->MFG_SN, view));
		}
	}

	FreeMulticastOutData(dataOut);

	return ret;
}
