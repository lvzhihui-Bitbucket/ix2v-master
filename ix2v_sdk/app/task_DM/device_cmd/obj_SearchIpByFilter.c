/**
  ******************************************************************************
  * @file    obj_SearchIpByFilter.c
  * @author  czb
  * @version V00.01.00
  * @date    2016.5.31
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 
  
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <memory.h>
#include "cJSON.h"
#include "utility.h"
#include "obj_SearchIpByFilter.h"
#include "obj_SYS_VER_INFO.h"
#include "vtk_udp_stack_device_update.h"
#include "obj_CmdR8012.h"
#include "obj_IoInterface.h"


/*
{
	"S1_Timeout":300,
	"S1_IntervalTime":20,
	"S2_Enable":20,
	"S2_Timeout":200,
	"S2_IntervalTime":20
}
*/

static SearchIpRun searchIpRun = {PTHREAD_MUTEX_INITIALIZER, 0, 0, 0, 0, 0, 300, 20, 1, 200, 20, NULL};
static SearchPara searchPara;

void LoadSearchTimeoutParameter(void)
{
	searchPara.lanSearch1Timeout = API_Para_Read_Int(Lan_S1_Timeout);
	searchPara.lanSearch1IntervalTime = API_Para_Read_Int(Lan_S1_IntervalTime);
	searchPara.lanSearch2Enable = API_Para_Read_Int(Lan_S2_Enable);
	searchPara.lanSearch2Timeout = API_Para_Read_Int(Lan_S2_Timeout);
	searchPara.lanSearch2IntervalTime = API_Para_Read_Int(Lan_S2_IntervalTime);

	searchPara.wlanSearch1Timeout = API_Para_Read_Int(Wlan_S1_Timeout);
	searchPara.wlanSearch1IntervalTime = API_Para_Read_Int(Wlan_S1_IntervalTime);
	searchPara.wlanSearch2Enable = API_Para_Read_Int(Wlan_S2_Enable);
	searchPara.wlanSearch2Timeout = API_Para_Read_Int(Wlan_S2_Timeout);
	searchPara.wlanSearch2IntervalTime = API_Para_Read_Int(Wlan_S2_IntervalTime);
}

int GetSearch1Timeout(void)
{
	if(GetNetMode() == NM_MODE_LAN)
	{
		return searchPara.lanSearch1Timeout;
	}
	else
	{
		return searchPara.wlanSearch1Timeout;
	}
}

int GetSearch1IntervalTime(void)
{
	if(GetNetMode() == NM_MODE_LAN)
	{
		return searchPara.lanSearch1IntervalTime;
	}
	else
	{
		return searchPara.wlanSearch1IntervalTime;
	}
}

static void GetSearchParameter(int netMode)
{
	if(netMode == NM_MODE_LAN)
	{
		searchIpRun.search1Timeout = searchPara.lanSearch1Timeout;
		searchIpRun.search1IntervalTime = searchPara.lanSearch1IntervalTime;
		searchIpRun.search2Enable = searchPara.lanSearch2Enable;
		searchIpRun.search2Timeout = searchPara.lanSearch2Timeout;
		searchIpRun.search2IntervalTime = searchPara.lanSearch2IntervalTime;
		searchIpRun.searchMaxDevice = MAX_DEVICE;
	}
	else
	{
		searchIpRun.search1Timeout = searchPara.wlanSearch1Timeout;
		searchIpRun.search1IntervalTime = searchPara.wlanSearch1IntervalTime;
		searchIpRun.search2Enable = searchPara.wlanSearch2Enable;
		searchIpRun.search2Timeout = searchPara.wlanSearch2Timeout;
		searchIpRun.search2IntervalTime = searchPara.wlanSearch2IntervalTime;
		searchIpRun.searchMaxDevice = WLAN_MAX_DEVICE;
	}
}

//搜索结果按开机时间排序
void SearchResultSorting(SearchIpRspData* pSearchData)
{
	int j, k;	   
	
	if(pSearchData->deviceCnt != 0)
	{
		for (j = 0; j < pSearchData->deviceCnt - 1; j++)     //外层循环控制趟数，总趟数为deviceCnt-1  
		{
			for (k = 0; k < pSearchData->deviceCnt - 1 - j; k++)  	//内层循环为当前j趟数 所需要比较的次数  
			{
				if (pSearchData->data[k].systemBootTime > pSearchData->data[k+1].systemBootTime)
				{
					SearchOneDeviceData tempData;

					tempData = pSearchData->data[k+1];
					pSearchData->data[k+1] = pSearchData->data[k];
					pSearchData->data[k] = tempData;
				}
			}
		}
	}
}

//发送By number搜索指令
// paras:
// bd : bd number
// type: 搜索类型
// time : 超时时间 单位S
// getDeviceCnt : 查找设备数量限制
// data : 查找结果
// return:
//  -1:系统忙, -2:传递进来参数错误, -3:没有查找到, 0:查找完成
int API_SearchIpByFilter(const char* bd, Type_e type, int time, int getDeviceCnt, SearchIpRspData* data)
{
	SearchIpReq sendData;
	SearchIpReq2 sendData2;
	int i, j;
	int waitFlag, searchEnd;
	
	pthread_mutex_lock(&searchIpRun.lock);
	waitFlag = searchIpRun.waitFlag;
	pthread_mutex_unlock(&searchIpRun.lock);
	
	if(waitFlag)
	{
		return -1;
	}
	
	pthread_mutex_lock(&searchIpRun.lock);
	searchIpRun.waitFlag = 1;
	waitFlag = searchIpRun.waitFlag;
	GetSearchParameter(GetNetMode());
	searchIpRun.rand = MyRand();
	searchIpRun.searchAllTimeCnt = 0;
	searchIpRun.searchTimeCnt = 0;
	searchIpRun.searchRspFlag = 0;	
	searchIpRun.data = data;
	searchIpRun.data->deviceCnt = 0;
	pthread_mutex_unlock(&searchIpRun.lock);

	if(bd != NULL)
	{	
		//9999 为全范围搜索
		strcpy(sendData.BD, !strcmp(bd, "9999") ? "" : bd);
	}
	sendData.type = type;
	sendData.rand = searchIpRun.rand;
	
	api_udp_device_update_send_data2(inet_addr(DEVICE_SEARCH_MULTICAST_ADDR), htons(SEARCH_IP_BY_FILTER_REQ), (char*)&sendData, sizeof(sendData) );
				
	for(searchEnd = 0; waitFlag && !searchEnd; )
	{
		usleep(UDP_SEARCH_TIME_UNIT*1000);
		
		pthread_mutex_lock(&searchIpRun.lock);

		//搜索设备数量够了，搜索结束
		if(getDeviceCnt > 0 && searchIpRun.data->deviceCnt >= getDeviceCnt)
		{
			searchIpRun.waitFlag = 0;
		}

		//搜索总时间超时
		searchIpRun.searchAllTimeCnt++;
		if(searchIpRun.searchAllTimeCnt >= time*1000/UDP_SEARCH_TIME_UNIT)
		{
			searchIpRun.waitFlag = 0;
		}

		searchIpRun.searchTimeCnt++;
		if(searchIpRun.searchRspFlag)
		{
			//搜索1有应答，但是规定时间内没有设备增加，搜索1结束
			if(searchIpRun.searchTimeCnt >= searchIpRun.search1IntervalTime/UDP_SEARCH_TIME_UNIT)
			{
				searchEnd = 1;
			}
		}
		else
		{
			//搜索1没有应答，搜索1超时结束
			if(searchIpRun.searchTimeCnt >= searchIpRun.search1Timeout/UDP_SEARCH_TIME_UNIT)
			{
				searchEnd = 1;
			}
		}
		waitFlag = searchIpRun.waitFlag;
		pthread_mutex_unlock(&searchIpRun.lock);
	}
	
	while(waitFlag && searchIpRun.search2Enable)
	{
		pthread_mutex_lock(&searchIpRun.lock);		
		searchIpRun.searchRspFlag = 0;
		searchIpRun.searchTimeCnt = 0;
		pthread_mutex_unlock(&searchIpRun.lock);
		
		memset(&sendData2, 0, sizeof(sendData2));
		sendData2.type = type;
		sendData2.rand = searchIpRun.rand;
		sendData2.sourceIp = sendData.sourceIp;
		
		if(bd != NULL)
		{	
			//9999 为全范围搜索
			strcpy(sendData2.BD, !strcmp(bd, "9999") ? "" : bd);
		}

		for(j = 0; j < searchIpRun.data->deviceCnt; j++)
		{
			sendData2.ip[j][0] = (unsigned char)((searchIpRun.data->data[j].Ip>>24) & 0xff);
			sendData2.ip[j][1] = (unsigned char)((searchIpRun.data->data[j].Ip>>16) & 0xff);
		}
	
		api_udp_device_update_send_data2(inet_addr(DEVICE_SEARCH_MULTICAST_ADDR), htons(SEARCH_IP_BY_FILTER2_REQ), (char*)&sendData2, sizeof(sendData2) - 2*(MAX_DEVICE - j - 1));
	
		for(searchEnd = 0; waitFlag && !searchEnd; )
		{
			usleep(UDP_SEARCH_TIME_UNIT*1000);
			
			pthread_mutex_lock(&searchIpRun.lock);
		
			//搜索设备数量够了，搜索结束
			if(getDeviceCnt > 0 && searchIpRun.data->deviceCnt >= getDeviceCnt)
			{
				searchIpRun.waitFlag = 0;
			}
			
			//搜索总时间超时
			searchIpRun.searchAllTimeCnt++;
			if(searchIpRun.searchAllTimeCnt >= time*1000/UDP_SEARCH_TIME_UNIT)
			{
				searchIpRun.waitFlag = 0;
			}

			searchIpRun.searchTimeCnt++;		
			if(searchIpRun.searchRspFlag)
			{
				if(searchIpRun.searchTimeCnt >= searchIpRun.search2IntervalTime/UDP_SEARCH_TIME_UNIT)
				{
					searchEnd = 1;
				}
			}
			else
			{
				if(searchIpRun.searchTimeCnt >= searchIpRun.search2Timeout/UDP_SEARCH_TIME_UNIT)
				{
					searchIpRun.waitFlag = 0;
				}
			}
			waitFlag = searchIpRun.waitFlag;
			pthread_mutex_unlock(&searchIpRun.lock);
		}
	}

	return (searchIpRun.data->deviceCnt != 0 ? 0 : -2);
}

//测试搜索功能
int API_SearchIpByFilterTest(const char* bd, Type_e type, int time, int getDeviceCnt, SearchIpRspData* data, SearchResult* pResult)
{
	SearchIpReq sendData;
	SearchIpReq2 sendData2;
	int i, j;
	int waitFlag, searchEnd;
	
	pthread_mutex_lock(&searchIpRun.lock);
	waitFlag = searchIpRun.waitFlag;
	pthread_mutex_unlock(&searchIpRun.lock);
	
	if(waitFlag)
	{
		pResult->searchResult = -1;
		return -1;
	}
	
	pthread_mutex_lock(&searchIpRun.lock);
	searchIpRun.waitFlag = 1;
	waitFlag = searchIpRun.waitFlag;
	GetSearchParameter(GetNetMode());
	searchIpRun.rand = MyRand();
	searchIpRun.searchAllTimeCnt = 0;
	searchIpRun.searchTimeCnt = 0;
	searchIpRun.searchRspFlag = 0;	
	searchIpRun.data = data;
	searchIpRun.data->deviceCnt = 0;
	pthread_mutex_unlock(&searchIpRun.lock);

	pResult->searchNetMode = GetNetMode();
	
	pResult->search1Timeout = searchIpRun.search1Timeout;
	pResult->search1IntervalTime = searchIpRun.search1IntervalTime;
	pResult->search2Enable = searchIpRun.search2Enable;
	pResult->search2Timeout = searchIpRun.search2Timeout;
	pResult->search2IntervalTime = searchIpRun.search2IntervalTime;
	pResult->search1FirstDevTime = 0;
	pResult->search2FirstDevTime = 0;
	pResult->search2SendCnt = 0;

	if(bd != NULL)
	{	
		//9999 为全范围搜索
		strcpy(sendData.BD, !strcmp(bd, "9999") ? "" : bd);
	}
	sendData.type = type;
	sendData.rand = searchIpRun.rand;
	
	api_udp_device_update_send_data2(inet_addr(DEVICE_SEARCH_MULTICAST_ADDR), htons(SEARCH_IP_BY_FILTER_REQ), (char*)&sendData, sizeof(sendData) );
				
	for(searchEnd = 0; waitFlag && !searchEnd; )
	{
		usleep(UDP_SEARCH_TIME_UNIT*1000);
		
		pthread_mutex_lock(&searchIpRun.lock);

		//搜索设备数量够了，搜索结束
		if(getDeviceCnt > 0 && searchIpRun.data->deviceCnt >= getDeviceCnt)
		{
			searchIpRun.waitFlag = 0;
		}

		//搜索总时间超时
		searchIpRun.searchAllTimeCnt++;
		if(searchIpRun.searchAllTimeCnt >= time*1000/UDP_SEARCH_TIME_UNIT)
		{
			searchIpRun.waitFlag = 0;
		}

		searchIpRun.searchTimeCnt++;
		if(searchIpRun.searchRspFlag)
		{
			if(!pResult->search1FirstDevTime)
			{
				pResult->search1FirstDevTime = searchIpRun.searchAllTimeCnt;
			}

			//搜索1有应答，但是规定时间内没有设备增加，搜索1结束
			if(searchIpRun.searchTimeCnt >= searchIpRun.search1IntervalTime/UDP_SEARCH_TIME_UNIT)
			{
				searchEnd = 1;
			}
		}
		else
		{
			//搜索1没有应答，搜索1超时结束
			if(searchIpRun.searchTimeCnt >= searchIpRun.search1Timeout/UDP_SEARCH_TIME_UNIT)
			{
				searchEnd = 1;
			}
		}
		waitFlag = searchIpRun.waitFlag;
		pthread_mutex_unlock(&searchIpRun.lock);
	}
	
	pResult->search1Time = searchIpRun.searchAllTimeCnt;
	pResult->search1DevCnt = searchIpRun.data->deviceCnt;

	while(waitFlag && searchIpRun.search2Enable)
	{
		pthread_mutex_lock(&searchIpRun.lock);		
		searchIpRun.searchRspFlag = 0;
		searchIpRun.searchTimeCnt = 0;
		pthread_mutex_unlock(&searchIpRun.lock);
		
		memset(&sendData2, 0, sizeof(sendData2));
		sendData2.type = type;
		sendData2.rand = searchIpRun.rand;
		sendData2.sourceIp = sendData.sourceIp;
		
		if(bd != NULL)
		{	
			//9999 为全范围搜索
			strcpy(sendData2.BD, !strcmp(bd, "9999") ? "" : bd);
		}

		for(j = 0; j < searchIpRun.data->deviceCnt; j++)
		{
			sendData2.ip[j][0] = (unsigned char)((searchIpRun.data->data[j].Ip>>24) & 0xff);
			sendData2.ip[j][1] = (unsigned char)((searchIpRun.data->data[j].Ip>>16) & 0xff);
		}
		
		pResult->search2SendCnt++;
		api_udp_device_update_send_data2(inet_addr(DEVICE_SEARCH_MULTICAST_ADDR), htons(SEARCH_IP_BY_FILTER2_REQ), (char*)&sendData2, sizeof(sendData2) - 2*(MAX_DEVICE - j - 1));
	
		for(searchEnd = 0; waitFlag && !searchEnd; )
		{
			usleep(UDP_SEARCH_TIME_UNIT*1000);
			
			pthread_mutex_lock(&searchIpRun.lock);
		
			//搜索设备数量够了，搜索结束
			if(getDeviceCnt > 0 && searchIpRun.data->deviceCnt >= getDeviceCnt)
			{
				searchIpRun.waitFlag = 0;
			}
			
			//搜索总时间超时
			searchIpRun.searchAllTimeCnt++;
			if(searchIpRun.searchAllTimeCnt >= time*1000/UDP_SEARCH_TIME_UNIT)
			{
				searchIpRun.waitFlag = 0;
			}

			searchIpRun.searchTimeCnt++;		
			if(searchIpRun.searchRspFlag)
			{
				if(!pResult->search2FirstDevTime)
				{
					pResult->search2FirstDevTime = searchIpRun.searchAllTimeCnt - pResult->search1Time;
				}

				if(searchIpRun.searchTimeCnt >= searchIpRun.search2IntervalTime/UDP_SEARCH_TIME_UNIT)
				{
					searchEnd = 1;
				}
			}
			else
			{
				if(searchIpRun.searchTimeCnt >= searchIpRun.search2Timeout/UDP_SEARCH_TIME_UNIT)
				{
					searchIpRun.waitFlag = 0;
				}
			}
			waitFlag = searchIpRun.waitFlag;
			pthread_mutex_unlock(&searchIpRun.lock);
		}
	}

	pResult->search2Time = searchIpRun.searchAllTimeCnt - pResult->search1Time;
	pResult->search2DevCnt = searchIpRun.data->deviceCnt - pResult->search1DevCnt;
	
	pResult->searchAllTime = searchIpRun.searchAllTimeCnt;
	pResult->searchResult = (searchIpRun.data->deviceCnt != 0 ? 0 : -2);

	pResult->searchAllTime = pResult->searchAllTime*UDP_SEARCH_TIME_UNIT;
	pResult->search1Time = pResult->search1Time*UDP_SEARCH_TIME_UNIT;
	pResult->search2Time = pResult->search2Time*UDP_SEARCH_TIME_UNIT;
	pResult->search1FirstDevTime = pResult->search1FirstDevTime*UDP_SEARCH_TIME_UNIT;
	pResult->search2FirstDevTime = pResult->search2FirstDevTime*UDP_SEARCH_TIME_UNIT;

	return pResult->searchResult;
}


//发送By number搜索指令
// paras:
// pR8012Table:已有的8012表
// bd : bd number
// type: 搜索类型
// time : 超时时间 单位mS
// getDeviceCnt : 查找设备数量限制
// data : 查找结果
// return:
//  -1:系统忙, -2:传递进来参数错误, -3:没有查找到, 0:查找完成
int API_SearchNewDeviceIpByFilter(R8012_DeviceTable* pR8012Table, const char* bd, Type_e type, int time, int getDeviceCnt, SearchIpRspData* data)
{
	SearchIpReq2 sendData2;
	int ip, deviceCnt, i;
	int waitFlag, searchEnd;
	
	pthread_mutex_lock(&searchIpRun.lock);
	waitFlag = searchIpRun.waitFlag;
	pthread_mutex_unlock(&searchIpRun.lock);
	if(waitFlag)
	{
		return -1;
	}
	
	pthread_mutex_lock(&searchIpRun.lock);
	searchIpRun.waitFlag = 1;
	waitFlag = searchIpRun.waitFlag;
	GetSearchParameter(GetNetMode());
	searchIpRun.rand = sendData2.rand;
	searchIpRun.searchTimeCnt = 0;
	searchIpRun.searchRspFlag = 0;	
	searchIpRun.data = data;
	searchIpRun.data->deviceCnt = 0;
	pthread_mutex_unlock(&searchIpRun.lock);

	memset(&sendData2, 0, sizeof(sendData2));
	sendData2.type = type;
	sendData2.rand = MyRand();
	
	if(bd != NULL)
	{	
		//9999 为全范围搜索
		strcpy(sendData2.BD, !strcmp(bd, "9999") ? "" : bd);
	}

	for(i = 0, deviceCnt = 0; deviceCnt < pR8012Table->deviceCnt && i < searchIpRun.searchMaxDevice; i++)
	{
		if(pR8012Table->device[i] != NULL)
		{	
			ip = inet_addr(pR8012Table->device[i]->IP_ADDR);
			sendData2.ip[deviceCnt][0] = (unsigned char)((ip>>24) & 0xff);
			sendData2.ip[deviceCnt][1] = (unsigned char)((ip>>16) & 0xff);
			deviceCnt++;
		}
	}
	
	api_udp_device_update_send_data2(inet_addr(DEVICE_SEARCH_MULTICAST_ADDR), htons(SEARCH_IP_BY_FILTER2_REQ), (char*)&sendData2, sizeof(sendData2) - 2*(MAX_DEVICE - deviceCnt - 1));

	while(waitFlag)
	{
		usleep(UDP_SEARCH_TIME_UNIT*1000);
		
		pthread_mutex_lock(&searchIpRun.lock);
		
		//搜索设备数量够了，搜索结束
		if(getDeviceCnt > 0 && searchIpRun.data->deviceCnt >= getDeviceCnt)
		{
			searchIpRun.waitFlag = 0;
		}

		//搜索总时间超时
		searchIpRun.searchAllTimeCnt++;
		if(searchIpRun.searchAllTimeCnt >= time*1000/UDP_SEARCH_TIME_UNIT)
		{
			searchIpRun.waitFlag = 0;
		}

		searchIpRun.searchTimeCnt++;
		if(searchIpRun.searchRspFlag)
		{
			//搜索1有应答，但是规定时间内没有设备增加，搜索1结束
			if(searchIpRun.searchTimeCnt >= searchIpRun.search1IntervalTime/UDP_SEARCH_TIME_UNIT)
			{
				searchIpRun.waitFlag = 0;
			}
		}
		else
		{
			//搜索1没有应答，搜索1超时结束
			if(searchIpRun.searchTimeCnt >= searchIpRun.search1Timeout/UDP_SEARCH_TIME_UNIT)
			{
				searchIpRun.waitFlag = 0;
			}
		}
		waitFlag = searchIpRun.waitFlag;
		pthread_mutex_unlock(&searchIpRun.lock);
	}

	return (searchIpRun.data->deviceCnt != 0 ? 0 : -2);
}

//接收到搜索应答指令
void ReceiveSearchIpByFilterRsp(SearchIpRsp* rspData)
{
	int j, flag;

	pthread_mutex_lock(&searchIpRun.lock);
	if(searchIpRun.waitFlag)
	{
		if(rspData->rand == searchIpRun.rand)
		{
			for(j = 0; j < searchIpRun.data->deviceCnt; j++)
			{
				if(!strcmp(searchIpRun.data->data[j].MFG_SN, rspData->data.MFG_SN))
				{
					//dprintf("rspData->data.MFG_SN = %s\n", rspData->data.MFG_SN);
					break;
				}
			}

			if(j == searchIpRun.data->deviceCnt)
			{
				searchIpRun.searchRspFlag = 1;
				searchIpRun.searchTimeCnt = 0;

				//搜素设备数量还没达到上限
				if(searchIpRun.data->deviceCnt < searchIpRun.searchMaxDevice)
				{
					searchIpRun.data->data[searchIpRun.data->deviceCnt] = rspData->data;
					searchIpRun.data->data[searchIpRun.data->deviceCnt].ifInRegisterTable = 0;
					searchIpRun.data->deviceCnt++;
				}
				else
				{
					searchIpRun.waitFlag = 0;
				}
			}
		}
	}
	
	pthread_mutex_unlock(&searchIpRun.lock);
}


//接收到SearchIpByFilter指令
void ReceiveSearchIpByFilterReq(int target_ip, SearchIpReq* pReadReq)
{
	int check = 0;

	if(strlen(pReadReq->BD) != 0)
	{
		//BD number 不为0并且不匹配，则不回应
		if(strcmp(GetSysVerInfo_bd(), pReadReq->BD))
		{
			return;
		}
	}

	if(pReadReq->type == 0 || pReadReq->type == GetSysVerInfo_MyDeviceType())
	{
		check = 1;
	}
	
	if(check)
	{
		SearchIpRsp rspData;
		
		rspData.rand = pReadReq->rand;
		rspData.data.Ip = inet_addr(GetSysVerInfo_IP_by_device(GetNetDeviceNameByTargetIp(target_ip)));
		rspData.data.deviceType = GetSysVerInfo_MyDeviceType();
		rspData.data.systemBootTime = GetSystemBootTime();
		strcpy(rspData.data.BD_RM_MS, GetSysVerInfo_BdRmMs());
		strcpy(rspData.data.MFG_SN, GetSysVerInfo_Sn());
		strcpy(rspData.data.name, GetSysVerInfo_name());
		strcpy(rspData.data.platform, IX2V);
		
		usleep((MyRand()%20) * 5 * 1000);
		api_udp_device_update_send_data(target_ip, htons(SEARCH_IP_BY_FILTER_RSP), (char*)&rspData, sizeof(rspData) );
	}

}

//接收到SearchIpByFilter指令
void ReceiveSearchIpByFilter2Req(int target_ip, SearchIpReq2* pReadReq)
{
	int check = 0;
	int i, myIp;
	char* netDeviceName;
	int maxDevice;

	netDeviceName = GetNetDeviceNameByTargetIp(target_ip);
	maxDevice = (!strcmp(netDeviceName, NET_WLAN0) ? WLAN_MAX_DEVICE : MAX_DEVICE);

	myIp = inet_addr(GetSysVerInfo_IP_by_device(netDeviceName));


	if(strlen(pReadReq->BD) != 0)
	{
		//BD number 不为0并且不匹配，则不回应
		if(strcmp(GetSysVerInfo_bd(), pReadReq->BD))
		{
			return;
		}
	}
	

	for(i = 0; pReadReq->ip[i][0] != 0 && pReadReq->ip[i][1] != 0 && i < maxDevice; i++)
	{
		if(((myIp>>24) & 0xff) == pReadReq->ip[i][0] && ((myIp>>16) & 0xff) == pReadReq->ip[i][1])
		{
			//dprintf("pReadReq->ip[i][0] = %d, pReadReq->ip[i][1] = %d\n", pReadReq->ip[i][0], pReadReq->ip[i][1]);
			return;
		}
	}

	if(pReadReq->type == 0 || pReadReq->type == GetSysVerInfo_MyDeviceType())
	{
		check = 1;
	}
	if(check)
	{
		SearchIpRsp rspData;
		
		rspData.rand = pReadReq->rand;
		rspData.data.Ip = inet_addr(GetSysVerInfo_IP_by_device(netDeviceName));
		rspData.data.deviceType = GetSysVerInfo_MyDeviceType();
		rspData.data.systemBootTime = GetSystemBootTime();
		strcpy(rspData.data.BD_RM_MS, GetSysVerInfo_BdRmMs());
		strcpy(rspData.data.MFG_SN, GetSysVerInfo_Sn());
		strcpy(rspData.data.name, GetSysVerInfo_name());
		strcpy(rspData.data.platform, IX2V);
		
		usleep((MyRand()%20) * 5 * 1000);
		api_udp_device_update_send_data(target_ip, htons(SEARCH_IP_BY_FILTER_RSP), (char*)&rspData, sizeof(rspData) );
	}

}


#define SearchDeviceTime	100		//最长搜索时间100秒
#define SearchDeviceCnt		0		//最多搜索360个
const char* DeviceTypeTable[] = {"ALL", "IM", "OS", "DS", "GL", "VM", "IM_BAK", "DT_IM", "AC", "IXG"};
static int DeviceTypeNum = (sizeof(DeviceTypeTable)/sizeof(DeviceTypeTable[0]));

static Type_e StringToDeviceType(const char* string)
{
	Type_e ret = TYPE_ALL;

	if(string)
	{

		for(int i = 0; i < DeviceTypeNum; i++)
		{
			if(!strcmp(string, DeviceTypeTable[i]))
			{
				ret = i;
				break;
			}
		}
	}

	return ret;
}

static void LoadSearchParameter(const char* net, cJSON* searchCtrl)
{
	int intValue;

	if(net && !strcmp(net, "wlan0"))
	{
		GetJsonDataOrIoPro(searchCtrl, Wlan_S1_Timeout, &searchIpRun.search1Timeout);
		GetJsonDataOrIoPro(searchCtrl, Wlan_S1_IntervalTime, &searchIpRun.search1IntervalTime);
		GetJsonDataOrIoPro(searchCtrl, Wlan_S2_Enable, &searchIpRun.search2Enable);
		GetJsonDataOrIoPro(searchCtrl, Wlan_S2_Timeout, &searchIpRun.search2Timeout);
		GetJsonDataOrIoPro(searchCtrl, Wlan_S2_IntervalTime, &searchIpRun.search2IntervalTime);
		GetJsonDataOrIoPro(searchCtrl, Wlan_S2_MinCnt, &searchIpRun.search2TimeoutMaxCnt);		
		searchIpRun.searchMaxDevice = WLAN_MAX_DEVICE;
	}
	else
	{
		GetJsonDataOrIoPro(searchCtrl, Lan_S1_Timeout, &searchIpRun.search1Timeout);
		GetJsonDataOrIoPro(searchCtrl, Lan_S1_IntervalTime, &searchIpRun.search1IntervalTime);
		GetJsonDataOrIoPro(searchCtrl, Lan_S2_Enable, &searchIpRun.search2Enable);
		GetJsonDataOrIoPro(searchCtrl, Lan_S2_Timeout, &searchIpRun.search2Timeout);
		GetJsonDataOrIoPro(searchCtrl, Lan_S2_IntervalTime, &searchIpRun.search2IntervalTime);
		GetJsonDataOrIoPro(searchCtrl, Lan_S2_MinCnt, &searchIpRun.search2TimeoutMaxCnt);
		searchIpRun.searchMaxDevice = MAX_DEVICE;
	}
}

static void SaveSearchResult(const char* net, const char* BD_Nbr, const char* deviceType, SearchResult result)
{
	char* resultRecordEnable = API_Para_Read_String2(Search_Record);

	if(resultRecordEnable && strcmp(resultRecordEnable, "Ignore"))
	{
		if((!strcmp(resultRecordEnable, "New")) || (!strcmp(resultRecordEnable, "AddUp")))
		{
			cJSON* recordTable;
			cJSON* resultRecord = cJSON_CreateObject();

			cJSON_AddStringToObject(resultRecord, "Time", get_time_string());
			cJSON_AddStringToObject(resultRecord, "Net", ((net != NULL) && (!strcmp(net, "wlan0"))) ? "wlan0" : "eth0");
			cJSON_AddStringToObject(resultRecord, "BD_Nbr", (BD_Nbr != NULL) ? BD_Nbr : "");
			cJSON_AddStringToObject(resultRecord, "Type", (deviceType != NULL) ? deviceType : "");

			cJSON_AddNumberToObject(resultRecord, "SearchAllTime", result.searchAllTime);
			cJSON_AddNumberToObject(resultRecord, "Search1Time", result.search1Time);
			cJSON_AddNumberToObject(resultRecord, "Search2Time", result.search2Time);

			cJSON_AddNumberToObject(resultRecord, "SearchAllCnt", result.search1DevCnt+result.search2DevCnt);
			cJSON_AddNumberToObject(resultRecord, "Search1Cnt", result.search1DevCnt);
			cJSON_AddNumberToObject(resultRecord, "Search2Cnt", result.search2DevCnt);

			cJSON_AddNumberToObject(resultRecord, "search1FirstDevTime", result.search1FirstDevTime);
			cJSON_AddNumberToObject(resultRecord, "search2FirstDevTime", result.search2FirstDevTime);

			cJSON_AddNumberToObject(resultRecord, "search2SendCnt", result.search2SendCnt);

			if(!strcmp(resultRecordEnable, "New"))
			{
				recordTable = cJSON_CreateArray();
			}
			else
			{
				recordTable = GetJsonFromFile(SearchResult_FILE);
				if(cJSON_IsArray(recordTable))
				{
					while (cJSON_GetArraySize(recordTable) >= 100)
					{
						cJSON_DeleteItemFromArray(recordTable, 0);
					}
				}
				else
				{
					cJSON_Delete(recordTable);
					recordTable = cJSON_CreateArray();
				}
			}

			cJSON_AddItemToArray(recordTable, resultRecord);


			SetJsonToFile(SearchResult_FILE, recordTable);

			cJSON_Delete(recordTable);
		}
	}
}

static void SaveDeviceTable(const cJSON* resultTable, SearchIpRspData rspData)
{
	if(cJSON_IsArray(resultTable))
	{
		for (size_t i = 0; i < rspData.deviceCnt; i++)
		{
			cJSON* record = cJSON_CreateObject();
			cJSON_AddItemToArray(resultTable, record);
			cJSON_AddStringToObject(record, IX2V_IP_ADDR, my_inet_ntoa2(rspData.data[i].Ip));
			cJSON_AddStringToObject(record, IX2V_IX_ADDR, rspData.data[i].BD_RM_MS);
			cJSON_AddStringToObject(record, IX2V_MFG_SN, rspData.data[i].MFG_SN);
			cJSON_AddStringToObject(record, IX2V_IX_TYPE, DeviceTypeToString(rspData.data[i].deviceType));
			cJSON_AddStringToObject(record, IX2V_UpTime, TimeToString(rspData.data[i].systemBootTime));
			cJSON_AddStringToObject(record, IX2V_Platform, !strcmp(rspData.data[i].platform, IX2V) ? IX2V : "IX1/2");
		}
	}
}

int SortJsonTable(const cJSON* table, const char* key, int order)
{
	int recordCnt;
	int i, j, ifExchange;
	cJSON* record1;
	cJSON* record2;

	if(!cJSON_IsArray(table) || key == NULL)
	{
		return 0;
	}

	recordCnt = cJSON_GetArraySize(table);
    for (i=0; i < recordCnt-1; i++) /* 外循环为排序趟数，recordCnt个数进行recordCnt-1趟 */
	{
		/* 内循环为每趟比较的次数，第i趟比较recordCnt-i次 */
        for (j = 0; j < recordCnt-1-i; j++) 
		{
			record1 = cJSON_GetObjectItemCaseSensitive(cJSON_GetArrayItem(table, j), key);
			record2 = cJSON_GetObjectItemCaseSensitive(cJSON_GetArrayItem(table, j+1), key);
			if(record1->type !=  record1->type)
			{
				return 0;
			}
			else
			{
				ifExchange = 0;
				if(cJSON_IsNumber(record1))
				{
					//降序 || 升序，交换位置
					if(((order) && (record1->valuedouble < record2->valuedouble)) || 
						((!order) && (record1->valuedouble > record2->valuedouble)))
					{
						ifExchange = 1;
					}
				}
				else if (cJSON_IsString(record1))
				{
					//降序 || 升序，交换位置
					if(((order) && (strcmp(record1->valuestring, record2->valuestring) < 0)) || 
					   ((!order) && (strcmp(record1->valuestring, record2->valuestring) > 0)))
					{
						ifExchange = 1;
					}
				}
				else
				{
					return 0;
				}

				if(ifExchange)
				{
					record1 = cJSON_DetachItemFromArray(table, j);
					cJSON_InsertItemInArray(table, j+1, record1);
				}
			}
		}
    }

	return 1;
}

static void SendSearchCmd(const char* net, int cmd, char* data, int dataLen)
{
	int mask, ipStart, ipEnd, myIp;
	int sendErrorCnt;

	if(net && !strcmp(net, NET_WLAN0))
	{
		mask = ntohl(GetLocalMaskAddrByDevice(NET_WLAN0));
		myIp = ntohl(GetLocalIpByDevice(NET_WLAN0));
		ipStart = mask & myIp;
		ipEnd = ipStart + (~mask);
		sendErrorCnt = 0;

		while(ipStart < ipEnd && sendErrorCnt < 200)
		{
			if(myIp == ipStart)
			{
				ipStart++;
				continue;
			}

			if(api_udp_device_update_send_data_by_device(NET_WLAN0, htonl(ipStart), htons(cmd), data, dataLen))
			{
				usleep(100*1000);
				sendErrorCnt++;
				dprintf("sendErrorCnt = %d, ipStart = %s\n", sendErrorCnt, my_inet_ntoa2(htonl(ipStart)));
			}
			else
			{
				ipStart++;
			}
		}
	}
	else
	{
		api_udp_device_update_send_data_by_device(NET_ETH0, inet_addr(DEVICE_SEARCH_MULTICAST_ADDR), htons(cmd), data, dataLen);
	}
}
/*
	net: wlan0/eth0
	searchCtrl:控制搜索参数
	BD_Nbr:	9999搜索全部，其他搜索栋号
	deviceType：IM/DS/AC等
	resultTable:搜索结果
*/
int IXS_SearchJsonDevTb(const char* net, cJSON* searchCtrl, const char* BD_Nbr, const char* deviceType, const cJSON* resultTable)
{
	SearchIpReq sendData;
	SearchIpReq2 sendData2;
	SearchIpRspData rspData;
	SearchResult result;

	int i, j;
	int waitFlag, searchEnd, search2TimeoutCnt;
	
	pthread_mutex_lock(&searchIpRun.lock);
	waitFlag = searchIpRun.waitFlag;
	pthread_mutex_unlock(&searchIpRun.lock);
	
	if(waitFlag)
	{
		result.searchResult = -1;
		return -1;
	}
	
	pthread_mutex_lock(&searchIpRun.lock);
	searchIpRun.waitFlag = 1;
	waitFlag = searchIpRun.waitFlag;
	LoadSearchParameter(net, searchCtrl);
	searchIpRun.rand = MyRand();
	searchIpRun.searchAllTimeCnt = 0;
	searchIpRun.searchTimeCnt = 0;
	searchIpRun.searchRspFlag = 0;	
	searchIpRun.data = &rspData;
	searchIpRun.data->deviceCnt = 0;
	pthread_mutex_unlock(&searchIpRun.lock);

	result.search1Timeout = searchIpRun.search1Timeout;
	result.search1IntervalTime = searchIpRun.search1IntervalTime;
	result.search2Enable = searchIpRun.search2Enable;
	result.search2Timeout = searchIpRun.search2Timeout;
	result.search2IntervalTime = searchIpRun.search2IntervalTime;
	result.search1FirstDevTime = 0;
	result.search2FirstDevTime = 0;
	result.search2SendCnt = 0;

	//9999 为全范围搜索
	snprintf(sendData.BD, 5, "%s", ((BD_Nbr == NULL) || (!strcmp(BD_Nbr, "9999"))) ? "" : BD_Nbr);
	sendData.type = StringToDeviceType(deviceType);
	sendData.rand = searchIpRun.rand;
	
	SendSearchCmd(net, SEARCH_IP_BY_FILTER_REQ, (char*)&sendData, sizeof(sendData));
				
	for(searchEnd = 0; waitFlag && !searchEnd; )
	{
		usleep(UDP_SEARCH_TIME_UNIT*1000);
		
		pthread_mutex_lock(&searchIpRun.lock);

		//搜索设备数量够了，搜索结束
		if(SearchDeviceCnt > 0 && searchIpRun.data->deviceCnt >= SearchDeviceCnt)
		{
			searchIpRun.waitFlag = 0;
		}

		//搜索总时间超时
		searchIpRun.searchAllTimeCnt++;
		if(searchIpRun.searchAllTimeCnt >= SearchDeviceTime*1000/UDP_SEARCH_TIME_UNIT)
		{
			searchIpRun.waitFlag = 0;
		}

		searchIpRun.searchTimeCnt++;
		if(searchIpRun.searchRspFlag)
		{
			if(!result.search1FirstDevTime)
			{
				result.search1FirstDevTime = searchIpRun.searchAllTimeCnt;
			}

			//搜索1有应答，但是规定时间内没有设备增加，搜索1结束
			if(searchIpRun.searchTimeCnt >= searchIpRun.search1IntervalTime/UDP_SEARCH_TIME_UNIT)
			{
				searchEnd = 1;
			}
		}
		else
		{
			//搜索1没有应答，搜索1超时结束
			if(searchIpRun.searchTimeCnt >= searchIpRun.search1Timeout/UDP_SEARCH_TIME_UNIT)
			{
				searchEnd = 1;
			}
		}
		waitFlag = searchIpRun.waitFlag;
		pthread_mutex_unlock(&searchIpRun.lock);
	}

	result.search1Time = searchIpRun.searchAllTimeCnt;
	result.search1DevCnt = searchIpRun.data->deviceCnt;
	search2TimeoutCnt = 0;

	while(waitFlag && searchIpRun.search2Enable)
	{
		pthread_mutex_lock(&searchIpRun.lock);		
		searchIpRun.searchRspFlag = 0;
		searchIpRun.searchTimeCnt = 0;
		pthread_mutex_unlock(&searchIpRun.lock);
		
		memset(&sendData2, 0, sizeof(sendData2));
		strcpy(sendData2.BD, sendData.BD);
		sendData2.type = sendData.type;
		sendData2.rand = searchIpRun.rand;
		sendData2.sourceIp = sendData.sourceIp;
		
		for(j = 0; j < searchIpRun.data->deviceCnt; j++)
		{
			sendData2.ip[j][0] = (unsigned char)((searchIpRun.data->data[j].Ip>>24) & 0xff);
			sendData2.ip[j][1] = (unsigned char)((searchIpRun.data->data[j].Ip>>16) & 0xff);
		}

		result.search2SendCnt++;
		SendSearchCmd(net, SEARCH_IP_BY_FILTER2_REQ, (char*)&sendData2, sizeof(sendData2) - 2*(MAX_DEVICE - searchIpRun.data->deviceCnt));
	
		for(searchEnd = 0; waitFlag && !searchEnd; )
		{
			usleep(UDP_SEARCH_TIME_UNIT*1000);
			
			pthread_mutex_lock(&searchIpRun.lock);
		
			//搜索设备数量够了，搜索结束
			if(SearchDeviceCnt > 0 && searchIpRun.data->deviceCnt >= SearchDeviceCnt)
			{
				searchIpRun.waitFlag = 0;
			}
			
			//搜索总时间超时
			searchIpRun.searchAllTimeCnt++;
			if(searchIpRun.searchAllTimeCnt >= SearchDeviceTime*1000/UDP_SEARCH_TIME_UNIT)
			{
				searchIpRun.waitFlag = 0;
			}

			searchIpRun.searchTimeCnt++;		
			if(searchIpRun.searchRspFlag)
			{
				if(!result.search2FirstDevTime)
				{
					result.search2FirstDevTime = searchIpRun.searchAllTimeCnt - result.search1Time;
				}

				if(searchIpRun.searchTimeCnt >= searchIpRun.search2IntervalTime/UDP_SEARCH_TIME_UNIT)
				{
					searchEnd = 1;
				}
			}
			else
			{
				if(searchIpRun.searchTimeCnt >= searchIpRun.search2Timeout/UDP_SEARCH_TIME_UNIT)
				{
					if(++search2TimeoutCnt >= searchIpRun.search2TimeoutMaxCnt)
					{
						searchIpRun.waitFlag = 0;
					}
					else
					{
						searchEnd = 1;
					}
				}
			}
			waitFlag = searchIpRun.waitFlag;
			pthread_mutex_unlock(&searchIpRun.lock);
		}
	}
	
	result.search2Time = searchIpRun.searchAllTimeCnt - result.search1Time;
	result.search2DevCnt = searchIpRun.data->deviceCnt - result.search1DevCnt;
	result.searchAllTime = searchIpRun.searchAllTimeCnt;
	result.searchAllTime = result.searchAllTime*UDP_SEARCH_TIME_UNIT;
	result.search1Time = result.search1Time*UDP_SEARCH_TIME_UNIT;
	result.search2Time = result.search2Time*UDP_SEARCH_TIME_UNIT;
	result.search1FirstDevTime = result.search1FirstDevTime*UDP_SEARCH_TIME_UNIT;
	result.search2FirstDevTime = result.search2FirstDevTime*UDP_SEARCH_TIME_UNIT;

	//搜索结果统计写到文件
	SaveSearchResult(net, BD_Nbr, deviceType, result);

	//将搜索到的设备信息保存到tb
	SaveDeviceTable(resultTable, rspData);
	
	return rspData.deviceCnt;
}

int IXS_SearchIsBusy(void)
{
	int waitFlag;

	pthread_mutex_lock(&searchIpRun.lock);
	waitFlag = searchIpRun.waitFlag;
	pthread_mutex_unlock(&searchIpRun.lock);

	return waitFlag;
}


