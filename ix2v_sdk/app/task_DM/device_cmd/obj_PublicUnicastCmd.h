/**
  ******************************************************************************
  * @file    obj_PublicUnicastCmd.h
  * @author  cao
  * @version V00.01.00
  * @date    2022.3.28
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 

#ifndef _obj_PublicUnicastCmd_H
#define _obj_PublicUnicastCmd_H

#include "cJSON.h"

// Define Object Property-------------------------------------------------------
#define PUBLIC_UNICAST_DATA_LEN		960

typedef struct
{
	int					len;			//数据长度
	char*				data;			//数据内容指针
}PublicUnicastCmdData;

typedef struct
{
    int				rand;											//随机数
    int				sourceIp;										//源ip
    char            MFG_SN[13];                                     //用以校验的序列号，避免IP改变后获取错误数据
    int				dataLen;										//后面的数据长度
    char			data[PUBLIC_UNICAST_DATA_LEN];
} PublicUnicastCmdReq;

typedef struct
{
    int						rand;				  //随机数
    int						sourceIp;			//源ip
    int						packetNo;			//应答包序号
    int						packetTotal;	//应答包总数
    int						result;				//result
    int 					dataLen;			//后面的数据长度
    char					data[PUBLIC_UNICAST_DATA_LEN];
} PublicUnicastCmdRsp;

typedef int (*PublicUnicastCmdProcess)(int, PublicUnicastCmdData, PublicUnicastCmdData*);
typedef struct
{
	int					no;					//应答包序号
	int					dataLen;			//数据长度
	char*				data;				//接收应答数据
}PublicUnicastPacket;

typedef struct
{
    int					                waitFlag;			//等待回应标记
    int					                rand;				  //随机数
    int					                targetIp;			//目标ip
    int					                result;
    PublicUnicastCmdProcess			process;			//处理函数
    int					                packetCnt;		//应答总数
    PublicUnicastPacket*	      packet				//应答包					
}PublicUnicastCmdRun;

typedef struct
{
    int					                reqCmd;			  //请求指令字
    int					                rspCmd;				//应答指令字
    PublicUnicastCmdProcess			process;			//处理函数
}PublicUnicastCmdtoProcess;

typedef struct
{
    pthread_mutex_t 	lock;
    cJSON*				table;
}MyPublicUnicastTable;


#define UNICAST_CMD_RSP_DELAY()					usleep(10*1000)


// Define Object Function - Public----------------------------------------------
int PublicUnicastCmdRequst(int ip, const char* mfg_sn, int timeOut, unsigned short cmd, PublicUnicastCmdData dataIn, PublicUnicastCmdData* dataOut);
void ReceivePublicUnicastCmdRsp(unsigned short cmd, PublicUnicastCmdRsp* rspData);
int ReceivePublicUnicastCmdReq(char* netDeviceName, unsigned short cmd, PublicUnicastCmdReq* reqData, int len);
void FreeUnicastOutData(PublicUnicastCmdData* data);


cJSON* API_GetPbIo(int ip, const char *sn, const cJSON *view);
int API_IXS_ProxyUpdateDevTbReq(int ip);

//返回：0 --失败；1 --成功
int API_XD_EventJson(int ip, cJSON* event);

/*
	value格式：["IX_ADDR", "IP_ADDR", "IX_NAME"]

	返回值格式：{
					"READ":{
						"IX_ADDR":"0099000101",
						"IP_ADDR":"192.168.243.101",
						"IX_NAME":"IX611"
					},

					"Result":"Succ"
	}
*/
cJSON* API_ReadRemoteIo(int ip, const cJSON *value);

/*
	value格式：{
						"IX_ADDR":"0099000101",
						"IP_ADDR":"192.168.243.101",
						"IX_NAME":"IX611"
	}

	返回值格式：{
					"WRITE":{
						"IX_ADDR":"0099000101",
						"IP_ADDR":"192.168.243.101",
						"IX_NAME":"IX611"
					},
					
					"Result":"Succ"
	}
*/
cJSON* API_WriteRemoteIo(int ip, const cJSON *value);

//返回：0 --失败；1 --成功
int API_Ring2(int ip, char* msg, int time);

cJSON* API_GetRemotePb(int ip, const char* pbName);

// Define Object Function - Private---------------------------------------------



#endif


