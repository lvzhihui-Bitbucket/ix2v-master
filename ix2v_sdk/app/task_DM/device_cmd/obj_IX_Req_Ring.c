/**
  ******************************************************************************
  * @file    obj_IX_Req_Ring.c
  * @author  czb
  * @version V00.01.00
  * @date    2016.5.31
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 
  
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <memory.h>
#include "obj_IX_Req_Ring.h"
#include "obj_IoInterface.h"
#include "vtk_udp_stack_device_update.h"
#include "obj_PublicInformation.h"
#include "task_Beeper.h"


IxReqRingRun ixReqRingRun;

static int InternetTimeAutoUpdate(void)
{
	char timeServer[30];
		
	//IX2V test if(API_Para_Read_Int(TimeAutoUpdateEnable))
	{
		//IX2V test API_Para_Read_String(TIME_SERVER, timeServer);
		//IX2V test if(sync_time_from_sntp_server(inet_addr(timeServer), API_Para_Read_Int(TimeZone)) != -1)
		{
			//dprintf("11111111111111111111\n");
		}
		//IX2V test else
		{
			//dprintf("22222222222222222222\n");
		}
	}		
}

/*
821-DX的响应情况

1） 待机时，A/B灯都亮 3S

2） 无论是否待机，均发出持续3S的Beeper提示短音

3） 如有DR模块，在LED屏显示Message内容
*/
int CmdRingReq(const char* data)
{
	dprintf("Ring:%s\n", data);
	BEEP_T8();

#ifdef PID_IX850
	API_TIPS_Ext(data);
#else
	char* systemState = API_PublicInfo_Read_String(PB_System_State);
	if(systemState && !strcmp(systemState, "Standby"))
	{
		API_LED_ctrl2("LED_1", "MSG_LED_ON", 3);
		API_LED_ctrl2("LED_2", "MSG_LED_ON", 3);
		DR_DisplayMessage(data);
	}
#endif

	return 0;
}


int IxReqRingProcess(int ip, int timeOut, const char* message, IxReqRingRsp* pRsp)
{
	
	IxReqRingReq sendData = {0};
	int i, timeCnt, ret, len;
	
	sendData.rand = MyRand() + MyRand2(ip);
	sendData.sourceIp = inet_addr(GetSysVerInfo_IP_by_device(GetNetDeviceNameByTargetIp(ip)));
	len = sizeof(sendData) - RING_DATA_LEN;

	if(message != NULL)
	{
		strncpy(sendData.data, message, RING_DATA_LEN);
		len += strlen(message);
	}
	
	memset(pRsp, 0, sizeof(IxReqRingRsp));
	
	
	if(sendData.sourceIp == ip)
	{
		strcpy(pRsp->DeviceNbr, GetSysVerInfo_BdRmMs());
		strcpy(pRsp->IP_Addr, GetSysVerInfo_IP_by_device(GetNetDeviceNameByTargetIp(ip)));
		strcpy(pRsp->MFG_SN, GetSysVerInfo_Sn());
		return CmdRingReq(message);
	}

	ixReqRingRun.waitFlag = 1;
	ixReqRingRun.rsp.result = -1;
	ixReqRingRun.rsp.rand = sendData.rand;
	
	api_udp_device_update_send_data(ip, htons(IX_REQ_Ring_REQ), (char*)&sendData, len);
	
	timeCnt = timeOut*1000/UDP_WAIT_RSP_TIME;
	while(1)
	{
		usleep(UDP_WAIT_RSP_TIME*1000);
		
		if(ixReqRingRun.waitFlag == 0)
		{
			ret = ixReqRingRun.rsp.result;
			*pRsp = ixReqRingRun.rsp; 
			break;
		}

		if(--timeCnt == 0)
		{
			ixReqRingRun.waitFlag = 0;
			ret = -1;
			break;
		}
	}

	return ret;
}

//接收到IxReqRing命令应答指令
void ReceiveIxReqRingCmdRsp(IxReqRingRsp* rspData)
{
	if(ixReqRingRun.waitFlag == 1)
	{
		if(rspData->rand == ixReqRingRun.rsp.rand)
		{
			ixReqRingRun.rsp = *rspData;
			ixReqRingRun.waitFlag = 0;
		}
	}
}

//接收到IxReqRing命令请求指令
int ReceiveIxReqRingCmdReq(IxReqRingReq* reqData)
{
	
	IxReqRingRsp	sendData;
	sendData.rand = reqData->rand;
	sendData.sourceIp = inet_addr(GetSysVerInfo_IP_by_device(GetNetDeviceNameByTargetIp(reqData->sourceIp)));
	sendData.result = 0;
	strcpy(sendData.DeviceNbr, GetSysVerInfo_BdRmMs());
	strcpy(sendData.IP_Addr, GetSysVerInfo_IP_by_device(GetNetDeviceNameByTargetIp(reqData->sourceIp)));
	strcpy(sendData.MFG_SN, GetSysVerInfo_Sn());

	api_udp_device_update_send_data(reqData->sourceIp, htons(IX_REQ_Ring_RSP), (char*)&sendData, sizeof(sendData) );

	CmdRingReq(reqData->data);
	
	return 0;
}

int API_IxReqRing(int ip, const char* message)
{
	IxReqRingRsp rsp;
	
	return IxReqRingProcess(ip, 1, message, &rsp);
}

/*********************************************************************************************************
**  End Of File
*********************************************************************************************************/

