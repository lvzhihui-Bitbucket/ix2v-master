/**
  ******************************************************************************
  * @file    obj_RemoteUpgrade.h
  * @author  cao
  * @version V00.01.00
  * @date    2016.5.31
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 
	  //cao_20170304  整个文件

#ifndef _obj_RemoteUpgrade_H
#define _obj_RemoteUpgrade_H

#define RemoteUpgradeCMD_DATA_LEN		500

#define UpgradeCMD_OP_CODE_GET_STATE		0
#define UpgradeCMD_OP_CODE_SET_INFO			1
#define UpgradeCMD_OP_CODE_OPERATION		2
#define UpgradeCMD_OP_CODE_CANCLE			3
#define UpgradeCMD_OP_CODE_START			4

#define	FW_INFO_SERVER_DISP_ID			0
#define	FW_INFO_SERVER_SELECT_ID		1
#define	FW_INFO_UPGRADE_CODE_ID			2

#define	FW_INFO_LINE2_DISP_ID			3
#define	FW_INFO_LINE3_DISP_ID			4
#define	FW_INFO_LINE4_DISP_ID			5
#define	FW_INFO_UPGRADE_REBOOT_TIME_ID	6


// Define Object Property-------------------------------------------------------
#pragma pack(1)  //指定按1字节对齐

//指令包结构
typedef struct
{
	int		OP_CODE;								//操作码
	int 	state;									//状态
	char	data[RemoteUpgradeCMD_DATA_LEN];		//数据
} RemoteUpgrade_T;

typedef struct
{
	int				rand;				//随机数
	int				sourceIp;			//源ip
	RemoteUpgrade_T data;				//指令数据
} RemoteUpgradeRsp;

typedef struct
{
	int				rand;				//随机数
	int				sourceIp;			//源ip
	RemoteUpgrade_T data;				//指令数据
} RemoteUpgradeReq;

typedef struct
{
	int						waitFlag;			//等待回应标记
	int						rand;				//随机数
	RemoteUpgrade_T 		data;				//指令数据
} RemoteUpgradeRun;


#pragma pack()



// Define Object Function - Public----------------------------------------------


// Define Object Function - Private---------------------------------------------



#endif


