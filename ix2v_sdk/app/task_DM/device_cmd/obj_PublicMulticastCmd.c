/**
  ******************************************************************************
  * @file    obj_PublicMulticastCmd.c
  * @author  czb
  * @version V00.01.00
  * @date    2022.3.28
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 
  
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <memory.h>
#include "cJSON.h"
#include "elog_forcall.h"
#include "vtk_udp_stack_device_update.h"
#include "obj_PublicInformation.h"
#include "obj_PublicMulticastCmd.h"
#include "obj_IoInterface.h"
#include "define_string.h"

int SearchServiceCmdProcess(int sourceIp, MulticastCmdData dataIn, MulticastCmdData* dataOut);

static const MulticastCmdtoProcess MulticastCmdTable[] = {
	{SEARCH_SERVICE_REQ,	SEARCH_SERVICE_RSP, 	SearchServiceCmdProcess},
	
};

static const int MulticastCmdTableNum = sizeof(MulticastCmdTable)/sizeof(MulticastCmdTable[0]);
static MyMulticastTable myMulticastTable = {.lock = PTHREAD_MUTEX_INITIALIZER, .table = NULL};

MulticastCfg GetMulticastPara(const cJSON* config)
{
	MulticastCfg cfg;
	cJSON* value;

	if(GetNetMode())
	{
		GetJsonDataOrIoPro(config, Wlan_S1_Timeout, &cfg.search1Timeout);
		GetJsonDataOrIoPro(config, Wlan_S1_IntervalTime, &cfg.search1IntervalTime);
		GetJsonDataOrIoPro(config, Wlan_S2_Enable, &cfg.search2Enable);
		GetJsonDataOrIoPro(config, Wlan_S2_Timeout, &cfg.search2Timeout);
		GetJsonDataOrIoPro(config, Wlan_S2_IntervalTime, &cfg.search2IntervalTime);
	}
	else 
	{
		GetJsonDataOrIoPro(config, Lan_S1_Timeout, &cfg.search1Timeout);
		GetJsonDataOrIoPro(config, Lan_S1_IntervalTime, &cfg.search1IntervalTime);
		GetJsonDataOrIoPro(config, Lan_S2_Enable, &cfg.search2Enable);
		GetJsonDataOrIoPro(config, Lan_S2_Timeout, &cfg.search2Timeout);
		GetJsonDataOrIoPro(config, Lan_S2_IntervalTime, &cfg.search2IntervalTime);
	}
	return cfg;
}

static MulticastCmdRun* CreateMulticastCmdRun(MulticastCfg config)
{
	int rand;
	cJSON* element = NULL;
	MulticastCmdRun* multicastCmdRun = NULL;

	pthread_mutex_lock(&myMulticastTable.lock);

	if(myMulticastTable.table == NULL)
	{
		myMulticastTable.table = cJSON_CreateArray();
	}

	do
	{
		rand = MyRand();
		cJSON_ArrayForEach(element, myMulticastTable.table)
		{
			multicastCmdRun = (MulticastCmdRun*)element->valueint;
			if(multicastCmdRun->rand == rand)
			{
				break;
			}
		}
	}
	while(element);

	multicastCmdRun = malloc(sizeof(MulticastCmdRun));
	element = cJSON_CreateNumber((int)multicastCmdRun);
	cJSON_AddItemToArray(myMulticastTable.table, element);

	multicastCmdRun->searchAllTimeCnt = 0;
	multicastCmdRun->searchTimeCnt = 0;
	multicastCmdRun->searchRspFlag = 0;

	multicastCmdRun->rand = rand;
	multicastCmdRun->waitFlag = 1;
	multicastCmdRun->packet = cJSON_CreateArray();
	multicastCmdRun->cfg = config;
	pthread_mutex_init(&multicastCmdRun->lock, 0);
	pthread_mutex_unlock(&myMulticastTable.lock);
	
	return multicastCmdRun;
}

static void FreeMulticastCmdRun(MulticastCmdRun* multicastCmdRun)
{
	cJSON* element = NULL;

	pthread_mutex_lock(&myMulticastTable.lock);
	if(multicastCmdRun)
	{
		free(multicastCmdRun);
	}

	cJSON_ArrayForEach(element, myMulticastTable.table)
	{
		if(((int)multicastCmdRun) == element->valueint)
		{
			cJSON_DetachItemViaPointer(myMulticastTable.table, element);
			cJSON_Delete(element);
			break;
		}
	}
	pthread_mutex_unlock(&myMulticastTable.lock);
}

static MulticastCmdProcess GetMulticastCmdProcess(unsigned short cmd)
{
	int i;
	MulticastCmdProcess			process;

	pthread_mutex_lock(&myMulticastTable.lock);
	for(i = 0; i < MulticastCmdTableNum; i++)
	{
		if(cmd == MulticastCmdTable[i].reqCmd || cmd == MulticastCmdTable[i].rspCmd)
		{
			process = MulticastCmdTable[i].process;
			break;
		}
	}
	pthread_mutex_unlock(&myMulticastTable.lock);

	return process;
}

static unsigned short GetMulticastRspCmd(unsigned short cmd)
{
	int i;
	unsigned short rspCmd = 0;

	pthread_mutex_lock(&myMulticastTable.lock);
	for(i = 0; i < MulticastCmdTableNum; i++)
	{
		if(cmd == MulticastCmdTable[i].reqCmd)
		{
			rspCmd = MulticastCmdTable[i].rspCmd;
			break;
		}
	}
	pthread_mutex_unlock(&myMulticastTable.lock);

	return rspCmd;
}


MulticastSearchResult MulticastCmdRequst(int getDeviceCnt, int timeOut, unsigned short cmd, MulticastCfg config, MulticastCmdData dataIn, MulticastCmdOutData* dataOut)
{
	MulticastCmdRun* multicastCmdRun;
	MulticastCmdReq sendData;
	MulticastCmdProcess process = GetMulticastCmdProcess(cmd);
	MulticastCmdData myDataOut;
	cJSON * element;
	cJSON * onePacket;
	MulticastPacket* packet;
	int waitFlag, searchEnd;
	short *checkIp;
	MulticastSearchResult result;

	if(process == NULL)
	{
		result.searchResult = -1;
		log_e("MulticastCmdRequst process is NULL");
		return result;
	}

	sendData.dataLen = dataIn.len;
	if(sendData.dataLen > MULTICAST_DATA_LEN)
	{
		result.searchResult = -2;
		log_e("dataIn.len is too long (%d).", dataIn.len);
		return result;
	}

	if(dataIn.data == NULL || sendData.dataLen == 0)
	{
		sendData.data[0] = 0;
	}
	else
	{
		memcpy(sendData.data, dataIn.data, sendData.dataLen);
	}

	multicastCmdRun = CreateMulticastCmdRun(config);

	result.searchNetMode = GetNetMode();
	
	result.search1Timeout = multicastCmdRun->cfg.search1Timeout;
	result.search1IntervalTime = multicastCmdRun->cfg.search1IntervalTime;
	result.search2Enable = multicastCmdRun->cfg.search2Enable;
	result.search2Timeout = multicastCmdRun->cfg.search2Timeout;
	result.search2IntervalTime = multicastCmdRun->cfg.search2IntervalTime;
	result.search1FirstDevTime = 0;
	result.search2FirstDevTime = 0;
	result.search2SendCnt = 0;

	sendData.recDevCnt = 0;
	sendData.rand = multicastCmdRun->rand;

	if(process(0, dataIn, &myDataOut) == 0)
	{
		pthread_mutex_lock(&multicastCmdRun->lock);
		packet = malloc(sizeof(MulticastPacket));
		cJSON_AddItemToArray(multicastCmdRun->packet, cJSON_CreateNumber((int)packet));

		packet->sourceIp = GetIXNetIp();
		packet->result = 0;
		strcpy(packet->MFG_SN, GetSysVerInfo_Sn());
		packet->dataLen = myDataOut.len;
		packet->data = myDataOut.data ;
		pthread_mutex_unlock(&multicastCmdRun->lock);
	}


	api_udp_device_update_send_data2(inet_addr(DEVICE_SEARCH_MULTICAST_ADDR), htons(cmd), (char*)&sendData, sizeof(sendData) - MULTICAST_DATA_LEN + sendData.dataLen);

	for(searchEnd = 0, waitFlag = multicastCmdRun->waitFlag; waitFlag && !searchEnd; )
	{
		usleep(UDP_WAIT_RSP_TIME*1000);
		
		pthread_mutex_lock(&multicastCmdRun->lock);

		//搜索设备数量够了，搜索结束
		if(getDeviceCnt > 0 && cJSON_GetArraySize(multicastCmdRun->packet) >= getDeviceCnt)
		{
			multicastCmdRun->waitFlag = 0;
		}

		//搜索总时间超时
		multicastCmdRun->searchAllTimeCnt++;
		if(multicastCmdRun->searchAllTimeCnt >= timeOut*1000/UDP_WAIT_RSP_TIME)
		{
			multicastCmdRun->waitFlag = 0;
		}

		multicastCmdRun->searchTimeCnt++;
		if(multicastCmdRun->searchRspFlag)
		{
			if(!result.search1FirstDevTime)
			{
				result.search1FirstDevTime = multicastCmdRun->searchAllTimeCnt;
			}

			//搜索1有应答，但是规定时间内没有设备增加，搜索1结束
			if(multicastCmdRun->searchTimeCnt >= multicastCmdRun->cfg.search1IntervalTime/UDP_WAIT_RSP_TIME)
			{
				searchEnd = 1;
			}
		}
		else
		{
			//搜索1没有应答，搜索1超时结束
			if(multicastCmdRun->searchTimeCnt >= multicastCmdRun->cfg.search1Timeout/UDP_WAIT_RSP_TIME)
			{
				searchEnd = 1;
			}
		}
		waitFlag = multicastCmdRun->waitFlag;
		pthread_mutex_unlock(&multicastCmdRun->lock);
	}

	result.search1Time = multicastCmdRun->searchAllTimeCnt;
	result.search1DevCnt = cJSON_GetArraySize(multicastCmdRun->packet);

	while(waitFlag && multicastCmdRun->cfg.search2Enable)
	{
		pthread_mutex_lock(&multicastCmdRun->lock);		
		multicastCmdRun->searchRspFlag = 0;
		multicastCmdRun->searchTimeCnt = 0;
		
		//装载第二次指令数据
		checkIp = sendData.data;
		cJSON_ArrayForEach(onePacket, multicastCmdRun->packet)
		{
			packet = (MulticastPacket*)onePacket->valueint;
			if(packet)
			{
				if(sendData.dataLen + sizeof(short) <= MULTICAST_DATA_LEN)
				{
					sendData.recDevCnt++;
					sendData.dataLen += sizeof(short);
					*checkIp = (packet->sourceIp>>16);
					checkIp++;
				}
			}
		}
		memcpy((char*)checkIp, dataIn.data, dataIn.len);

		pthread_mutex_unlock(&multicastCmdRun->lock);
	
		result.search2SendCnt++;
		api_udp_device_update_send_data2(inet_addr(DEVICE_SEARCH_MULTICAST_ADDR), htons(cmd), (char*)&sendData, sizeof(sendData) - MULTICAST_DATA_LEN + sendData.dataLen);
	
		for(searchEnd = 0; waitFlag && !searchEnd; )
		{
			usleep(UDP_WAIT_RSP_TIME*1000);
			
			pthread_mutex_lock(&multicastCmdRun->lock);
		
			//搜索设备数量够了，搜索结束
			if(getDeviceCnt > 0 && cJSON_GetArraySize(multicastCmdRun->packet) >= getDeviceCnt)
			{
				multicastCmdRun->waitFlag = 0;
			}
			
			//搜索总时间超时
			multicastCmdRun->searchAllTimeCnt++;
			if(multicastCmdRun->searchAllTimeCnt >= timeOut*1000/UDP_WAIT_RSP_TIME)
			{
				multicastCmdRun->waitFlag = 0;
			}

			multicastCmdRun->searchTimeCnt++;		
			if(multicastCmdRun->searchRspFlag)
			{
				if(!result.search2FirstDevTime)
				{
					result.search2FirstDevTime = multicastCmdRun->searchAllTimeCnt - result.search1Time;
				}

				if(multicastCmdRun->searchTimeCnt >= multicastCmdRun->cfg.search2IntervalTime/UDP_WAIT_RSP_TIME)
				{
					searchEnd = 1;
				}
			}
			else
			{
				if(multicastCmdRun->searchTimeCnt >= multicastCmdRun->cfg.search2Timeout/UDP_WAIT_RSP_TIME)
				{
					multicastCmdRun->waitFlag = 0;
				}
			}
			waitFlag = multicastCmdRun->waitFlag;
			pthread_mutex_unlock(&multicastCmdRun->lock);
		}
	}

	if(dataOut)
	{
		(*dataOut).packetAddr = multicastCmdRun->packet;
	}

	result.search2Time = multicastCmdRun->searchAllTimeCnt - result.search1Time;
	result.search2DevCnt = cJSON_GetArraySize(multicastCmdRun->packet) - result.search1DevCnt;
	
	result.searchAllTime = multicastCmdRun->searchAllTimeCnt;
	result.searchResult = 0;
	result.searchAllTime = result.searchAllTime*UDP_WAIT_RSP_TIME;
	result.search1Time = result.search1Time*UDP_WAIT_RSP_TIME;
	result.search2Time = result.search2Time*UDP_WAIT_RSP_TIME;
	result.search1FirstDevTime = result.search1FirstDevTime*UDP_WAIT_RSP_TIME;
	result.search2FirstDevTime = result.search2FirstDevTime*UDP_WAIT_RSP_TIME;

	FreeMulticastCmdRun(multicastCmdRun);

	return result;
}

//接收到Multicast命令应答指令
void ReceiveMulticastCmdRsp(unsigned short cmd, MulticastCmdRsp* rspData)
{
	MulticastCmdRun* multicastCmdRun;
	MulticastPacket* packet;
	cJSON* element;
	cJSON* onePacket;

	pthread_mutex_lock(&myMulticastTable.lock);
	cJSON_ArrayForEach(element, myMulticastTable.table)
	{
		multicastCmdRun = (MulticastCmdRun*)element->valueint;

		pthread_mutex_lock(&multicastCmdRun->lock);
		if(multicastCmdRun->waitFlag && multicastCmdRun->rand == rspData->rand)
		{
			cJSON_ArrayForEach(onePacket, multicastCmdRun->packet)
			{
				packet = (MulticastPacket*)onePacket->valueint;
				if(!strcmp(packet->MFG_SN, rspData->MFG_SN))
				{
					break;
				}
			}

			if(onePacket == NULL)
			{
				multicastCmdRun->searchRspFlag = 1;
				multicastCmdRun->searchTimeCnt = 0;

				packet = malloc(sizeof(MulticastPacket));
				cJSON_AddItemToArray(multicastCmdRun->packet, cJSON_CreateNumber((int)packet));

				packet->dataLen = rspData->dataLen;
				packet->sourceIp = rspData->sourceIp;
				packet->result = rspData->result;
				strcpy(packet->MFG_SN, rspData->MFG_SN);
				if(packet->dataLen)
				{
					packet->data = malloc(packet->dataLen);
					memcpy(packet->data, rspData->data, packet->dataLen);
				}
				else
				{
					packet->data = NULL;
				}
			}
		}
		pthread_mutex_unlock(&multicastCmdRun->lock);

	}
	pthread_mutex_unlock(&myMulticastTable.lock);
}

//接收到Multicast命令请求指令
int ReceiveMulticastCmdReq(char* netDeviceName, unsigned short cmd, MulticastCmdReq* reqData)
{
	MulticastCmdRsp	sendData;
	MulticastCmdProcess process = GetMulticastCmdProcess(cmd);
	MulticastCmdData dataIn;
	MulticastCmdData dataOut;
	int i, myIp, checkIp;
	short *ipTable;
	
	if(process == NULL)
	{
		log_e("ReceiveMulticastCmdReq process is NULL");
		return -1;
	}

	myIp = GetLocalIpByDevice(netDeviceName);
	ipTable = reqData->data;
	checkIp = myIp>>16;

	for(i = 0;  i < reqData->recDevCnt; i++)
	{
		if(checkIp == ipTable[i])
		{
			return -2;
		}
	}

	dataIn.data = reqData->data + sizeof(short) * reqData->recDevCnt;
	dataIn.len = reqData->dataLen - sizeof(short) * reqData->recDevCnt;

	sendData.result = process(reqData->sourceIp, dataIn, &dataOut);
	if(sendData.result != 0)
	{
		//log_i("ReceiveMulticastCmdReq process error");
		return -3;
	}

	sendData.rand = reqData->rand;
	strcpy(sendData.MFG_SN, API_PublicInfo_Read_String(PB_MFG_SN));
	sendData.sourceIp = myIp;
	sendData.dataLen = dataOut.len;
	if(dataOut.data && dataOut.len)
	{
		memcpy(sendData.data, dataOut.data, sendData.dataLen);
		free(dataOut.data);
	}
	usleep((MyRand2(myIp)%20) * 1000);	//延时20ms以内的随机数
	api_udp_device_update_send_data_by_device(netDeviceName, reqData->sourceIp, htons(GetMulticastRspCmd(cmd)), (char*)&sendData, sizeof(sendData) - MULTICAST_DATA_LEN + sendData.dataLen);
	return 0;
}

void FreeMulticastOutData(MulticastCmdOutData dataOut)
{
	cJSON* onePacket = NULL;
	MulticastPacket* packet;

	cJSON_ArrayForEach(onePacket, dataOut.packetAddr)
	{
		packet = (MulticastPacket*)onePacket->valueint;
		if(packet)
		{
			if(packet->data)
			{
				free(packet->data);
			}
			free(packet);
		}
	}

	cJSON_Delete(dataOut.packetAddr);
}


int SearchServiceCmdProcess(int sourceIp, MulticastCmdData dataIn, MulticastCmdData* dataOut)
{
	cJSON* where = cJSON_Parse(dataIn.data);
	cJSON* element;
	cJSON* ioElement;
	cJSON* pbValue;
	cJSON* retValue;
	int checkFlag = 1;

	cJSON_ArrayForEach(element, where)
	{
		if(element->string != NULL)
		{
			if(!strcmp(DM_KEY_BY_NBR, element->string))
			{
				
			}
			else if(!strcmp(DM_KEY_BY_INPUT, element->string))
			{

			}
			else if(!strcmp(DM_KEY_IO, element->string))
			{
				cJSON_ArrayForEach(ioElement, element)
				{
					if(ioElement->string != NULL && (pbValue = API_Para_Read_Public(ioElement->string)))
					{
						if(!cJSON_Compare(ioElement, pbValue, 1))
						{
							checkFlag = 0;
							return -1;
						}
					}
				}
			}
			else
			{
				pbValue = API_PublicInfo_Read(element->string);
				if(pbValue != NULL && !cJSON_Compare(element, pbValue, 1))
				{
					checkFlag = 0;
					return -1;
				}
			}
		}
	}

	if(checkFlag)
	{
		retValue = cJSON_CreateObject();
		cJSON_AddItemToObject(retValue, PB_MFG_SN, cJSON_Duplicate(API_PublicInfo_Read(PB_MFG_SN), 1));
		cJSON_AddItemToObject(retValue, PB_IX_ADDR, cJSON_Duplicate(API_PublicInfo_Read(PB_IX_ADDR), 1));
		if(dataOut)
		{
			(*dataOut).data = cJSON_PrintUnformatted(retValue);
			(*dataOut).len = strlen((*dataOut).data) + 1;
		}
		cJSON_Delete(retValue);
	}

	return 0;
}





/*********************************************************************************************************
**  End Of File
*********************************************************************************************************/

