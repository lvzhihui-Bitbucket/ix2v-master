/**
  ******************************************************************************
  * @file    obj_SetTimeServer.c
  * @author  czb
  * @version V00.01.00
  * @date    2016.5.31
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 
  
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <memory.h>
#include "obj_SetTimeServer.h"
#include "cJSON.h"
#include "vtk_udp_stack_device_update.h"
#include "obj_IoInterface.h"



static SetTimeServerRun setTimeServerRun;

static char* CreateSetTimeServerJsonData(char* serverIp, int timeZone, char* y, char* mon, char* d, char* h, char* min, char* s)
{
	int i;
	
    cJSON *string;
    cJSON *root = NULL;

    root = cJSON_CreateObject();
		
	cJSON_AddStringToObject(root, "serverIp", serverIp);	
	cJSON_AddNumberToObject(root, "timeZone", timeZone);	
	cJSON_AddStringToObject(root, "year", y);	
	cJSON_AddStringToObject(root, "month", mon);	
	cJSON_AddStringToObject(root, "day", d);	
	cJSON_AddStringToObject(root, "hour", h);	
	cJSON_AddStringToObject(root, "minute", min);	
	cJSON_AddStringToObject(root, "second", s);	

	string = cJSON_Print(root);

	cJSON_Delete(root);

	printf("CreateSetTimeServerJsonData = %s\n", string);
	
	return string;
}

static int ParseSetTimeServerJsonData(const char* json, char* serverIp, int* timeZone, char* y, char* mon, char* d, char* h, char* min, char* s)
{
	int status = 0;

	/* 创建一个用于解析的 cJSON 结构 */
	cJSON *root = cJSON_Parse(json);
	if (root == NULL)
	{
		dprintf("Json error!!! : %s\n", json);
		goto end;
	}
	
	GetJsonDataPro(root, "serverIp", serverIp);
	GetJsonDataPro(root, "timeZone", timeZone);
	GetJsonDataPro(root, "year", y);	
	GetJsonDataPro(root, "month", mon);	
	GetJsonDataPro(root, "day", d);	
	GetJsonDataPro(root, "hour", h);	
	GetJsonDataPro(root, "minute", min);	
	GetJsonDataPro(root, "second", s);	
	
	status = 1;

	
	end:
	
	cJSON_Delete(root);
	
	return status;
}


static void	GetDateAndTimeString(int* timeZone, char* year, char* mon, char* day, char* hour, char* min, char* second)	
{
	time_t t;
	struct tm *tblock;	
	char temp[10];
	
	//IX2V test *timeZone = API_Para_Read_Int(TimeZone);
	
	set_timezone((*timeZone) - 12);

	t = time(NULL); 
	tblock=localtime(&t);

	// init year
	snprintf(year,5,"20%02d", tblock->tm_year-100);	
	// init month
	snprintf(mon,3,"%02d", tblock->tm_mon+1);	
	// init day
	snprintf(day,3,"%02d", tblock->tm_mday);	
	// init hour
	snprintf(hour,3,"%02d", tblock->tm_hour);	
	// init minute
	snprintf(min,3,"%02d", tblock->tm_min);	
	// init minute
	snprintf(second,3,"%02d", tblock->tm_sec);	
}

static int RtcSet_ParaCheckDateAndTime(int timeZone, char* year, char* mon, char* day, char* hour, char* min, char* second)
{
	int para_len,para_value;

	if(timeZone < 0 || timeZone > 24)
	{
		return -1;
	}
	
	para_len = strlen(year);
	if(para_len != 4)
	{
		return -1;
	}
	para_value = atol(year);
	if(para_value > 2037 || para_value < 2005)
	{
		return -1;
	}

	para_len = strlen(mon);
	if(para_len != 1 && para_len != 2)
	{
		return -1;
	}
	para_value = atol(mon);
	if(para_value > 12)
	{
		return -1;
	}

	para_len = strlen(day);
	if(para_len != 1 && para_len != 2)
	{
		return -1;
	}
	para_value = atol(day);
	if(para_value > 31)
	{
		return -1;
	}

	
	para_len = strlen(hour);
	if(para_len != 1 && para_len != 2)
	{
		return -1;
	}
	para_value = atol(hour);
	if(para_value > 23)
	{
		return -1;
	}

	para_len = strlen(min);
	if(para_len != 1 && para_len != 2)
	{
		return -1;
	}
	para_value = atol(min);
	if(para_value > 59)
	{
		return -1;
	}

	para_len = strlen(second);
	if(para_len != 1 && para_len != 2)
	{
		return -1;
	}
	para_value = atol(second);
	if(para_value > 59)
	{
		return -1;
	}

	return 0;
}

static int SetDateAndTime(int timeZone, char* year, char* mon, char* day, char* hour, char* min, char* second)
{
	FILE* fd   = NULL;
	char buf[100] = {0};

	if(RtcSet_ParaCheckDateAndTime(timeZone, year,mon,day,hour,min,second) < 0)
	{
		return -1;
	}
	
	//IX2V test API_Para_Write_Int(TimeZone, timeZone);

	set_timezone(timeZone - 12);
	
	snprintf( buf, sizeof(buf),  "%s\"%s-%s-%s %s:%s:%s\"\n", "date -s ", year,mon,day,hour,min,second);

	dprintf( "%s\n",buf);
	
	if( (fd = popen( buf, "r" )) == NULL )
	{
		dprintf( "set_system_date_time error:%s\n",strerror(errno) );
		return -1;
	}		
	pclose( fd );

	if( (fd = popen( "hwclock -w", "r" )) == NULL )
	{
		dprintf( "set_system_date_time error:%s\n",strerror(errno) );
		return -1;
	}
	pclose( fd );
	
	dprintf( "set_system_date_time ok!\n" );

	//cao_201081020_s
	//IX2V test SetTimeUpdateFlag(1);
	//IX2V test SaveDateAndTimeToFile();
	//cao_201081020_e
	return 0;
}


static int SetTimeServerDateAndTime(char* serverIp, int timeZone, char* year, char* mon, char* day, char* hour, char* min, char* second)	
{
	FILE* fd   = NULL;
	char buf[100] = {0};

	//IX2V test API_Para_Write_String(TIME_SERVER, serverIp);
	
	//IX2V test if(API_Para_Read_Int(TimeAutoUpdateEnable))
	{
		return SetDateAndTime(timeZone, year,mon,day,hour,min,second);
	}
	//IX2V test else
	{
		return -1;
	}
}

static int SetMyTimeServer(char* serverIp)
{
	//IX2V test return API_Para_Write_String(TIME_SERVER, serverIp);
}

int SetTimeServerRequest(int ip, int timeOut, const char* serverIp)
{
	
	SetTimeServer_T sendData = {0};
	char* data;
	int i, timeCnt, ret, len, timeZone;
	char y[5], mon[3], d[3], h[3], min[3], s[3];
	
	sendData.rand = MyRand();
	sendData.sourceIp = inet_addr(GetSysVerInfo_IP());
	
	GetDateAndTimeString(&timeZone, y, mon, d, h, min, s);
	data = CreateSetTimeServerJsonData(serverIp, timeZone, y, mon, d, h, min, s);

	if(data != NULL)
	{
		len = strlen(data) > SetTimeServer_DATA_LEN ? SetTimeServer_DATA_LEN : strlen(data);
		
		sendData.dataLen = len + 1;
		memcpy(sendData.data, data, sendData.dataLen);
		free(data);
	}
	else
	{
		return -1;
	}
	
	len += (sizeof(sendData) - SetTimeServer_DATA_LEN);

	if(sendData.sourceIp == inet_addr(serverIp))	//本机作为服务器
	{
		api_udp_device_update_send_data(inet_addr(DEVICE_SEARCH_MULTICAST_ADDR), htons(SetTimeServer_RSP), (char*)&sendData, len);
		return 0;
	}
	//其他设备作为服务器
	else
	{
		setTimeServerRun.waitFlag = 1;
		setTimeServerRun.rsp.rand = sendData.rand;
		
		api_udp_device_update_send_data(ip, htons(SetTimeServer_REQ), (char*)&sendData, len);
		
		timeCnt = timeOut*1000/UDP_WAIT_RSP_TIME;
		while(1)
		{
			usleep(UDP_WAIT_RSP_TIME*1000);
			
			if(setTimeServerRun.waitFlag == 0)
			{
				ret = 0;
				break;
			}
		
			if(--timeCnt == 0)
			{
				setTimeServerRun.waitFlag = 0;
				ret = -1;
				break;
			}
		}
		
		return ret;
	}
}

//接收到SetTimeServer命令应答指令
void ReceiveSetTimeServerCmdRsp(SetTimeServer_T* rspData)
{
	char serverIp[20], y[5], mon[3], d[3], h[3], min[3], s[3];
	int timeZone;
		
	if(setTimeServerRun.waitFlag == 1)
	{
		if(rspData->rand == setTimeServerRun.rsp.rand)
		{
			setTimeServerRun.rsp = *rspData;
			setTimeServerRun.waitFlag = 0;
		}
	}
	

	ParseSetTimeServerJsonData(rspData->data, serverIp, &timeZone, y, mon, d, h, min, s);
	SetTimeServerDateAndTime(serverIp, timeZone, y, mon, d, h, min, s);
}

//接收到SetTimeServer命令请求指令
int ReceiveSetTimeServerCmdReq(SetTimeServer_T* reqData)
{
	SetTimeServer_T	sendData;
	int len;
	char* data;
	char serverIp[20], y[5], mon[3], d[3], h[3], min[3], s[3];
	int timeZone;
	
	sendData.rand = reqData->rand;
	sendData.sourceIp = inet_addr(GetSysVerInfo_IP());
	
	ParseSetTimeServerJsonData(reqData->data, serverIp, &timeZone, y, mon, d, h, min, s);

	if(inet_addr(serverIp) == sendData.sourceIp)
	{
		GetDateAndTimeString(&timeZone, y, mon, d, h, min, s);
			
		data = CreateSetTimeServerJsonData(serverIp, timeZone, y, mon, d, h, min, s);
		SetMyTimeServer(serverIp);
		
		if(data != NULL)
		{
			len = strlen(data) > SetTimeServer_DATA_LEN ? SetTimeServer_DATA_LEN : strlen(data);
			
			sendData.dataLen = len +1;
			memcpy(sendData.data, data, sendData.dataLen);
			free(data);
			len += (sizeof(sendData) - SetTimeServer_DATA_LEN);
			
			api_udp_device_update_send_data(inet_addr(DEVICE_SEARCH_MULTICAST_ADDR), htons(SetTimeServer_RSP), (char*)&sendData, len);
			return 0;
		}
	}
	return -1;
}

/*********************************************************************************************************
**  End Of File
*********************************************************************************************************/

