/**
  ******************************************************************************
  * @file    obj_SetTimeServer.h
  * @author  cao
  * @version V00.01.00
  * @date    2016.5.31
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 
	  //cao_20170304  整个文件

#ifndef _obj_SetTimeServer_H
#define _obj_SetTimeServer_H

// Define Object Property-------------------------------------------------------

#define SetTimeServer_DATA_LEN		500

typedef struct
{
	int				rand;					//随机数
	int				sourceIp;				//源ip
	int				dataLen;				//数据长度
	char			data[SetTimeServer_DATA_LEN+1];
} SetTimeServer_T;

typedef struct
{
	int					waitFlag;			//等待回应标记
	SetTimeServer_T	rsp;
}SetTimeServerRun;


// Define Object Function - Public----------------------------------------------

// Define Object Function - Private---------------------------------------------



#endif


