/**
  ******************************************************************************
  * @file    obj_SetDivertState.h
  * @author  cao
  * @version V00.01.00
  * @date    2023.3.13
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 

#ifndef obj_SetDivertState
#define obj_SetDivertState



// Define Object Property-------------------------------------------------------



// Define Object Function - Public----------------------------------------------

// Define Object Function - Private---------------------------------------------



#endif


