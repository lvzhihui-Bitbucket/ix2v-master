/**
  ******************************************************************************
  * @file    obj_GetIpByNumber.h
  * @author  cao
  * @version V00.01.00
  * @date    2016.5.31
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 
	  //cao_20170304  �����ļ�

#ifndef _obj_GetIpByNumber_H
#define _obj_GetIpByNumber_H

// Define Object Property-------------------------------------------------------
#include "obj_PublicCmdDefine.h"
#include "obj_GetDeviceTypeAndArea.h"

#pragma pack(1)  //ָ����1�ֽڶ���

typedef struct
{
	int		cnt;						//��Ӧ����
	int		Ip[SEARCH_NUMBER_MAX_DEVICE];				//ip
	Type_e	deviceType[SEARCH_NUMBER_MAX_DEVICE];		//�豸����
	char	BD_RM_MS[SEARCH_NUMBER_MAX_DEVICE][11];	//BD_RM_MS
} GetIpRspData;

typedef struct
{
	int					waitFlag;			//�ȴ���Ӧ���
	int					rand;				//�����
	int					searchAllTimeCnt;	//������ʱ���ʱ
	int					searchTimeCnt;		//������ʱ
	int					searchRspFlag;		//�Ƿ����豸��Ӧ���
	int					searchTimeout;		//�������searchָ��֮��û���豸��Ӧ����ȴ�ʱ��
	int					searchIntervalTime;	//���յ�һ���豸֮�󣬳������ʱ��û�������豸Ӧ�𣬵�������û���豸������
	GetIpRspData* 		data;
} GetIpRsp;

// GetIpBy Number ָ����ṹ
typedef struct
{
	int		rand;				//�����
	int		sourceIp;			//Դip
	char	BD_RM_MS[10+1];		//BD_RM_MS
	char	input[10+1];		//global����local
	char	my_BD[4+1];			//��������
} GetIpByNumberReq;

typedef struct
{
	int		rand;				//�����
	int		sourceIp;			//Դip
	int		deviceType;			//�豸����
	char	BD_RM_MS[10+1];		//BD_RM_MS
} GetIpByNumberRsp;


#pragma pack()



// Define Object Function - Public----------------------------------------------

//����By number����ָ��
// paras:
// bd_rm_ms: bd_rm_ms
// input : input
// bd : bd number
// time : ��ʱʱ�� ��λS
// getDeviceCnt : �����豸��������
// data : ���ҽ��
// return:
//  -1:ϵͳæ, -2:���ݽ�����������, -3:û�в��ҵ�, 0:�������
int API_GetIpNumberFromNet(const char* bdRmMs, const char* input, const char* bd, int time, int getDeviceCnt, GetIpRspData* data);

//���յ�By NUMBER����ָ��
void ReceiveGetIpByNumberFromNetReq(int target_ip, GetIpByNumberReq* pReadReq);

//���յ�����Ӧ��ָ��
void ReceiveGetIpByNumberFromNetRsp(int target_ip, GetIpByNumberRsp* rspData);


// Define Object Function - Private---------------------------------------------

//��������Ӧ��ָ��
void GetIpByBdRmMsFromNetRsp(int targetIp, int rand, int myIp, Type_e myDevType, char* myBD_RM_MS);


#endif


