/**
  ******************************************************************************
  * @file    obj_GetAboutByIp.c
  * @author  czb
  * @version V00.01.00
  * @date    2016.5.31
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 
  
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <memory.h>
#include "obj_UnlockReport.h"
#include "obj_SYS_VER_INFO.h"
#include "vtk_udp_stack_device_update.h"

//发送开锁报告指令
int API_UnlockReport(int ip, int lock, int state)
{
	UnlockReport_T sendData;
	
	memset(&sendData, 0, sizeof(sendData));

	sendData.rand = MyRand() + MyRand2(ip);
	sendData.sourceIp = inet_addr(GetSysVerInfo_IP_by_device(GetNetDeviceNameByTargetIp(ip)));
	sendData.lock = lock;
	sendData.state = state;

	api_udp_device_update_send_data(ip, htons(CMD_UNLOCK_STATE_REPORT), (char*)&sendData, sizeof(UnlockReport_T));

	return 0;
}

//接收到开锁报告指令
void ReceiveUnlockReport(UnlockReport_T* rspData)
{
	if(rspData->state == UNLOCK_SUCESSFULL)
	{
		//IX2V test API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_UNLOCK_SUCESSFULL);
	}
	else
	{
		//IX2V test API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_UNLOCK_UNSUCESSFULL);
	}
}


/*********************************************************************************************************
**  End Of File
*********************************************************************************************************/

