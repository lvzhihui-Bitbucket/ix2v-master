/**
  ******************************************************************************
  * @file    obj_SearchIpByFilter.c
  * @author  czb
  * @version V00.01.00
  * @date    2016.5.31
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 
  
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <memory.h>
#include "obj_SearchIxProxy.h"
#include "obj_SYS_VER_INFO.h"
#include "vtk_udp_stack_device_update.h"
#include "cJSON.h"
#include "task_IxProxy.h"

#define UDP_SEARCH_IX_PROXY_WAIT_RSP_TIME						200				//等待应答时间 ms

static SearchIxProxyRun searchIxProxyRun = {.lock = PTHREAD_MUTEX_INITIALIZER, .waitFlag = 0};

static int ParseSearchIxProxyObject(const char* json, IxProxyData_T* pObj)
{
    int status = 0;

	memset(pObj, 0, sizeof(IxProxyData_T));

    /* 创建一个用于解析的 cJSON 结构 */
    cJSON *proxyObject = cJSON_Parse(json);
    if (proxyObject == NULL)
    {
		dprintf("Json error!!! : %s\n", json);
        goto end;
    }

	/* 获取名为“version”的值 */
    GetJsonDataPro(proxyObject, "Version", pObj->version);

	/* 获取名为“priority”的值 */
    GetJsonDataPro(proxyObject, "Priority", pObj->priority);

	/* 获取名为“linked”的值 */
    GetJsonDataPro(proxyObject, "Linked", pObj->linked);

	/* 获取名为“ip_addr”的值 */
    GetJsonDataPro(proxyObject, "IP-Addr", pObj->ip_addr);

	/* 获取名为“device_addr”的值 */
    GetJsonDataPro(proxyObject, "Device-Addr", pObj->device_addr);

	/* 获取名为“MFG_SN”的值 */
    GetJsonDataPro(proxyObject, "MFG_SN", pObj->MFG_SN);

	/* 获取名为“deviceType”的值 */
    GetJsonDataPro(proxyObject, "DeviceType", pObj->deviceType);

	/* 获取名为“name”的值 */
    GetJsonDataPro(proxyObject, "Name", pObj->name);

	/* 获取名为“name2”的值 */
    GetJsonDataPro(proxyObject, "Name2", pObj->name2);

    status = 1;

end:
	
    cJSON_Delete(proxyObject);
	
    return status;
}

static char* CreateSearchIxProxyObject(int target_ip, IxProxyData_T obj)
{
    cJSON *root = NULL;
	char *string = NULL;


    root = cJSON_CreateObject();
    cJSON_AddStringToObject(root, "Version", obj.version);
    cJSON_AddStringToObject(root, "Priority", obj.priority);
    cJSON_AddStringToObject(root, "Linked", obj.linked);
	
    cJSON_AddStringToObject(root, "IP-Addr", GetSysVerInfo_IP_by_device(GetNetDeviceNameByTargetIp(target_ip)));
    cJSON_AddStringToObject(root, "Device-Addr", obj.device_addr);
    cJSON_AddStringToObject(root, "MFG_SN", obj.MFG_SN);

    cJSON_AddStringToObject(root, "DeviceType", obj.deviceType);
    cJSON_AddStringToObject(root, "Name", obj.name);
    cJSON_AddStringToObject(root, "Name2", obj.name2);
	
    cJSON_AddStringToObject(root, "CacheTime", obj.CacheTime);
    cJSON_AddStringToObject(root, "CacheNbr", obj.CacheNbr);

	string = cJSON_Print(root);

	cJSON_Delete(root);

	return string;
}


//发送SearchIxProxy指令
// paras:
// options : 保留
// getDeviceCnt : 查找设备数量限制
// time:等待时间
// return:
//  -1:系统忙, -2:传递进来参数错误, -3:没有查找到, 0:查找完成
int API_SearchIxProxyLinked(char* options, int getDeviceCnt, SearchIxProxyRspData* data, int time)
{
	SearchIxProxyReq sendData;
	IxProxyData_T proxyData;
	int timeCnt;
	if(data == NULL)
	{
		return -2;
	}
	data->deviceCnt = 0;

	pthread_mutex_lock(&searchIxProxyRun.lock);
	if(searchIxProxyRun.waitFlag)
	{
		pthread_mutex_unlock(&searchIxProxyRun.lock);
		return -1;
	}
	pthread_mutex_unlock(&searchIxProxyRun.lock);

	memset(&sendData, 0, sizeof(sendData));

	sendData.rand = MyRand();

	pthread_mutex_lock(&searchIxProxyRun.lock);
	searchIxProxyRun.waitFlag = 1;
	searchIxProxyRun.rand = sendData.rand;
	searchIxProxyRun.getDeviceCnt = getDeviceCnt;
	searchIxProxyRun.data.deviceCnt = 0;
		
	//本机代理已经连接
	proxyData = GetMyProxyData();
	if(strcmp(proxyData.linked, "0"))
	{
		data->data[0] = GetMyProxyData();
		searchIxProxyRun.data.data[0] = GetMyProxyData();
		
		data->deviceCnt++;
		searchIxProxyRun.data.deviceCnt++;

		//如果只搜索一个，已经搜索完成。
		if(searchIxProxyRun.data.deviceCnt == getDeviceCnt)
		{
			searchIxProxyRun.waitFlag = 0;
			pthread_mutex_unlock(&searchIxProxyRun.lock);
			return 0;
		}
	}
	
	pthread_mutex_unlock(&searchIxProxyRun.lock);

	api_udp_device_update_send_data(inet_addr(DEVICE_SEARCH_MULTICAST_ADDR), htons(SEARCH_IX_PROXY_LINKED_REQ), &sendData, sizeof(sendData.rand)+sizeof(sendData.sourceIp));
			
	timeCnt = time*1000/UDP_SEARCH_IX_PROXY_WAIT_RSP_TIME;

	while(searchIxProxyRun.waitFlag)
	{
		usleep(UDP_SEARCH_IX_PROXY_WAIT_RSP_TIME*1000);
		
		if(--timeCnt <= 0)
		{
			break;
		}
		
		pthread_mutex_lock(&searchIxProxyRun.lock);

		if(getDeviceCnt > 0 && (searchIxProxyRun.data.deviceCnt >= getDeviceCnt || searchIxProxyRun.data.deviceCnt >= 10))
		{
			pthread_mutex_unlock(&searchIxProxyRun.lock);
			break;
		}
		pthread_mutex_unlock(&searchIxProxyRun.lock);
	}


	pthread_mutex_lock(&searchIxProxyRun.lock);
	if(searchIxProxyRun.data.deviceCnt != 0)
	{
		int j, k;      
		
		for (j = 0; j < searchIxProxyRun.data.deviceCnt - 1; j++)     //外层循环控制趟数，总趟数为deviceCnt-1  
		{
			for (k = 0; k < searchIxProxyRun.data.deviceCnt - 1 - j; k++)  	//内层循环为当前j趟数 所需要比较的次数  
			{
				if (atoi(searchIxProxyRun.data.data[k].priority) > atoi(searchIxProxyRun.data.data[k+1].priority))
				{
					IxProxyData_T tempData;

					tempData = searchIxProxyRun.data.data[k+1];
					searchIxProxyRun.data.data[k+1] = searchIxProxyRun.data.data[k];
					searchIxProxyRun.data.data[k] = tempData;
				}
			}
		}
		memcpy(data, &searchIxProxyRun.data, sizeof(SearchIxProxyRspData));
		searchIxProxyRun.waitFlag = 0;
		pthread_mutex_unlock(&searchIxProxyRun.lock);
		return 0;
	}
	else
	{
		searchIxProxyRun.waitFlag = 0;
		pthread_mutex_unlock(&searchIxProxyRun.lock);
		return -3;
	}

}

//接收到搜索应答指令
void ReceiveIxProxyLinkedRsp(SearchIxProxyRsp* rspData)
{
	int i;
	IxProxyData_T data;
	pthread_mutex_lock(&searchIxProxyRun.lock);
	
	if(searchIxProxyRun.waitFlag)
	{
		if(rspData->rand == searchIxProxyRun.rand)
		{
			ParseSearchIxProxyObject(rspData->data, &data);
			for(i = 0; i < searchIxProxyRun.data.deviceCnt; i++)
			{
				if(!strcmp(searchIxProxyRun.data.data[i].MFG_SN, data.MFG_SN))
				{
					//dprintf("rspData->data.MFG_SN = %s\n", rspData->data.MFG_SN);
					break;
				}
			}

			if(i == searchIxProxyRun.data.deviceCnt)
			{
				//搜素设备数量还没达到上限
				if(searchIxProxyRun.data.deviceCnt < 10)
				{
					searchIxProxyRun.data.data[searchIxProxyRun.data.deviceCnt] = data;
					searchIxProxyRun.data.deviceCnt++;
				}
			}
		}
	}
	
	pthread_mutex_unlock(&searchIxProxyRun.lock);
}

//接收到SearchIxProxyLinked指令
void ReceiveSearchIxProxyLinkedReq(int target_ip, SearchIxProxyReq* pReadReq)
{
	SearchIxProxyRsp rspData;
	IxProxyData_T proxyData;
	char *jsonString = NULL;

	//没有连接不回应
	proxyData = GetMyProxyData();
	if(!strcmp(proxyData.linked, "0"))
	{
		return;
	}

	rspData.rand = pReadReq->rand;
	rspData.sourceIp = inet_addr(GetSysVerInfo_IP_by_device(GetNetDeviceNameByTargetIp(target_ip)));

	rspData.sourceIp = htonl(rspData.sourceIp);

	jsonString = CreateSearchIxProxyObject(target_ip, GetMyProxyData());
	if(jsonString != NULL)
	{
		strcpy(rspData.data, jsonString);
		free(jsonString);
	}
	else
	{
		rspData.data[0] = 0;
	}

	api_udp_device_update_send_data(target_ip, htons(SEARCH_IX_PROXY_LINKED_RSP), &rspData, sizeof(rspData) - 500 + strlen(rspData.data) + 1);
}

//接收到SearchIxProxyFilter指令
void ReceiveSearchIxProxyReq(int target_ip, SearchIxProxyReq* pReadReq, int len)
{
	int check = 0;
	SearchIxProxyRsp rspData;
	char *jsonString = NULL;

	if(len > 8)
	{
		cJSON *proxyObject = cJSON_Parse(pReadReq->data);
		if(proxyObject != NULL)
		{
			check = strcmp(cJSON_GetStringValue(cJSON_GetObjectItemCaseSensitive(proxyObject, DM_KEY_MFG_SN)), GetSysVerInfo_Sn());
			cJSON_Delete(proxyObject);
			if(check != 0)
			{
				return;
			}
		}
	}

	rspData.rand = pReadReq->rand;
	rspData.sourceIp = inet_addr(GetSysVerInfo_IP_by_device(GetNetDeviceNameByTargetIp(target_ip)));

	rspData.sourceIp = htonl(rspData.sourceIp);

	jsonString = CreateSearchIxProxyObject(target_ip, GetMyProxyData());
	if(jsonString != NULL)
	{
		snprintf(rspData.data, 500, "%s", jsonString);
		usleep(10*1000);
		free(jsonString);
	}
	else
	{
		rspData.data[0] = 0;
	}

	api_udp_device_update_send_data(target_ip, htons(SEARCH_IX_PROXY_RSP), &rspData, sizeof(rspData) - 500 + strlen(rspData.data) + 1);
}




