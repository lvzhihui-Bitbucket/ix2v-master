/**
  ******************************************************************************
  * @file    obj_GetIpByInput.c
  * @author  czb
  * @version V00.01.00
  * @date    2016.5.31
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 
  
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <memory.h>
#include "obj_GetIpByNumber.h"
#include "obj_GetIpByInput.h"

//得到一个指定字符串的房号：
// paras：
// input_str: 输入字符串
// bd_rm_ms: 输出房号
// -1/不能拆解, 0/拆解
int GetBdRmMsByInput( const char* input_str,  char* bd_rm_ms)
{
	int inputLen;
	int i;
	int ret;

	inputLen = strlen(input_str);

	//输入中有字母则不能拆解
	for(i = 0; i<inputLen; i++)
	{
		if(input_str[i] < '0' || input_str[i] > '9')
		{
			return -1;
		}
	}
	
	strcpy(bd_rm_ms, "0000000000");

	//10位数字，按4-4-2拆分10位房号
	if(inputLen == 10)
	{
		strcpy(bd_rm_ms, input_str);
	}
	//9位数字，前补0为10位数字，按4-4-2拆分10位房号
	else if(inputLen == 9)
	{
		strcpy(bd_rm_ms+1, input_str);
	}
	//8位数字，则为8位的BD&RM_Nbr
	else if(inputLen == 8)
	{
		memcpy(bd_rm_ms, input_str, 8);
	}
	//5~7位数字，前补0，得到8位的BD&RM_Nbr
	else if(inputLen >= 5 && inputLen <= 7)
	{
		memcpy(bd_rm_ms+(8-inputLen), input_str, inputLen);
	}
	//1~4位的数字，认为是本单元呼叫，自行添加BD_Nbr.
	else if(inputLen >= 1 && inputLen <= 4)
	{
		memcpy(bd_rm_ms, GetSysVerInfo_bd(), 4);
		memcpy(bd_rm_ms+(8-inputLen), input_str, inputLen);
	}
	else
	{
		return -1;
	}

	return 0;
}
//通过输入得到IP
// paras：
// input_str: 输入字符串
// record: IP和房号
// 0/得不到IP, >0/得到多个房号IP 
int API_GetIpByInput(const char* input, IpCacheRecord_T record[])
{
	int i, j;
	GetIpRspData data = {0};
	DeviceTypeAndArea_T device;
	char bd_rm_ms[11] = {0};
	
	GetBdRmMsByInput(input, bd_rm_ms);

	API_GetIpNumberFromNet(bd_rm_ms, input, GetSysVerInfo_bd(), 2, 0, &data);
	//printf("11111111111111111 data.cnt = %d\n", data.cnt);
	for(i = 0, j = 0; i < data.cnt; i++)
	{
		//IX2V test if(api_nm_if_judge_include_dev(data.BD_RM_MS[i], data.Ip[i])==0)
		{
			 //continue;
		}
		#if 0
		//有呼叫控制表
		if(hasCallCtrlTable())
		{
			//IX2V test if(!Find_bd_rm_ms_in_call_control_table(data.BD_RM_MS[i]))
			{
				continue;
			}
		}
		//无呼叫控制表
		else
		#endif
		{
			//printf("22222222222222 data.BD_RM_MS[i] = %s\n", data.BD_RM_MS[i]);
			device = GetDeviceTypeAndAreaByNumber(data.BD_RM_MS[i]);
			//printf("33333333333 device.type = %d, device.area = %d\n", device.type, device.area);
			
			//本机是小区管理机或者单栋管理机
			if(!memcmp(GetSysVerInfo_BdRmMs(), "00000000", 8) || !strcmp(GetSysVerInfo_rm(), "0000"))
			{
				if(device.type == TYPE_GL || device.type == TYPE_IM)
				{
					//printf("44444444444 device.type = %d, device.area = %d\n", device.type, device.area);					
				}
				else
				{
					//printf("5555555555555 device.type = %d, device.area = %d\n", device.type, device.area);					
					continue;
				}
			}
			//本机是普通分机
			else
			{
				if((device.area == CommonArea && device.type == TYPE_GL) ||
					(device.area == SameBuilding && device.type == TYPE_GL) ||
					(device.area == SameBuilding && device.type == TYPE_IM))
				{
					//printf("666666666666666 device.type = %d, device.area = %d\n", device.type, device.area);					
				}
				else
				{
					//printf("777777777777777 device.type = %d, device.area = %d\n", device.type, device.area);					
					continue;
				}
			}
		}

		record[j].ip = data.Ip[i];
		strcpy(record[j++].BD_RM_MS, data.BD_RM_MS[i]);
	}
	//printf("999999999999 j = %d\n", j); 				

	return j;
}


/*********************************************************************************************************
**  End Of File
*********************************************************************************************************/

