/**
  ******************************************************************************
  * @file    obj_GetInfoByIp.c
  * @author  czb
  * @version V00.01.00
  * @date    2016.5.31
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 
  
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "cJSON.h"
#include <memory.h>
#include "obj_GetMergeInfoByIp.h"
#include "obj_SYS_VER_INFO.h"
#include "vtk_udp_stack_device_update.h"
#include "obj_PublicInformation.h"

static GetMergeInfoRun getMergeInfo[CLIENT_MAX_NUM];

//发送获取信息指令
int API_GetMergeInfoByIp(int ip, int time, char* info)
{
	GetMergeInfoByIpReq sendData;
	int i, cnt;

	memset(&sendData, 0, sizeof(sendData));

	sendData.rand = MyRand() + MyRand2(ip);
	sendData.sourceIp = inet_addr(GetSysVerInfo_IP_by_device(GetNetDeviceNameByTargetIp(ip)));
	
	for(i = 0; i < CLIENT_MAX_NUM && getMergeInfo[i].waitFlag; i++);

	if(i >= CLIENT_MAX_NUM)
	{
		return -1;
	}
	
	getMergeInfo[i].waitFlag = 1;

	getMergeInfo[i].rand = sendData.rand;

	api_udp_device_update_send_data(ip, htons(GET_MERGE_INFO_BY_IP_REQ), (char*)&sendData, sizeof(sendData) );

	cnt = time*1000/UDP_WAIT_RSP_TIME;
	while(getMergeInfo[i].waitFlag)
	{
		usleep(UDP_WAIT_RSP_TIME*1000);
		if(--cnt == 0)
		{
			getMergeInfo[i].waitFlag = 0;
			return -3;
		}

		//每隔400ms重发一次
		if(!(cnt%(UDP_Resend_TIME/UDP_WAIT_RSP_TIME)))
		{
			api_udp_device_update_send_data(ip, htons(GET_MERGE_INFO_BY_IP_REQ), (char*)&sendData, sizeof(sendData));
		}
	}

	strcpy(info, getMergeInfo[i].data);
	getMergeInfo[i].waitFlag = 0;

	return 0;
}

//接收到获取信息应答指令
void ReceiveGetMergeInfoRsp(GetMergeInfoByIpRsp* rspData)
{
	int i;

	for(i = 0; i < CLIENT_MAX_NUM; i++)
	{
		if(getMergeInfo[i].waitFlag)
		{
			if(rspData->rand == getMergeInfo[i].rand)
			{
				strcpy(getMergeInfo[i].data, rspData->data);
				getMergeInfo[i].waitFlag = 0;
				break;
			}
		}
	}
}

//接收到Get Merge info请求指令
void ReceiveGetMergeInfoReq(int target_ip, GetMergeInfoByIpReq* pReadReq)
{
	char aboutData[1000];
	char* jsonString;

	GetMergeInfoByIpRsp rspData = {0};
	MERGE_INFO_RSP_DATA data = {0};
	
	rspData.rand = pReadReq->rand;

	//读取本机info
	GetMyInfo(target_ip, &data.info);
	
	//读取本机about
	GetMyAboutString(target_ip, aboutData);
	GetALLAbout(aboutData, &data.about);
	
	//读取本机version
	GetFwVersionAndVerify(inet_addr(GetSysVerInfo_IP_by_device(GetNetDeviceNameByTargetIp(target_ip))), 0, &data.version);

	jsonString = CreateGetMergeInfoRspJsonData(&data);
	if(jsonString != NULL)
	{
		snprintf(rspData.data, 1000, "%s", jsonString);
		free(jsonString);
	}

	api_udp_device_update_send_data(target_ip, htons(GET_MERGE_INFO_BY_IP_RSP), (char*)&rspData, sizeof(rspData)- 1000 + strlen(rspData.data) + 1);
}

cJSON* GetMyJsonInfo(char* net)
{
	cJSON* myJsoninfo = cJSON_CreateObject();
	char* ipString;
	char fwVer[100];

	if((net != NULL) && (!strcmp(net, NET_WLAN0)))
	{
		ipString = API_PublicInfo_Read_String(PB_WLAN_IP);
	}
	else
	{
		ipString = API_PublicInfo_Read_String(PB_LAN_IP);
	}
	snprintf(fwVer, 100, "%s-%s", API_PublicInfo_Read_String(PB_FW_VER), API_PublicInfo_Read_String(PB_MCU_VER));

	cJSON_AddStringToObject(myJsoninfo, IX2V_Platform, IX2V);
	cJSON_AddStringToObject(myJsoninfo, IX2V_MFG_SN, API_PublicInfo_Read_String(PB_MFG_SN));
	cJSON_AddStringToObject(myJsoninfo, IX2V_IP_ADDR, ipString);
	cJSON_AddStringToObject(myJsoninfo, IX2V_IX_ADDR, API_PublicInfo_Read_String(PB_IX_ADDR));
	cJSON_AddStringToObject(myJsoninfo, IX2V_G_NBR, API_PublicInfo_Read_String(PB_G_NBR));
	cJSON_AddStringToObject(myJsoninfo, IX2V_L_NBR, API_PublicInfo_Read_String(PB_L_NBR));
	cJSON_AddStringToObject(myJsoninfo, IX2V_IX_NAME, API_PublicInfo_Read_String(PB_IX_NAME));					
	cJSON_AddStringToObject(myJsoninfo, IX2V_IX_TYPE, DeviceTypeToString(GetSysVerInfo_MyDeviceType()));
	cJSON_AddStringToObject(myJsoninfo, IX2V_IX_Model, API_PublicInfo_Read_String(PB_DEV_NAME));
	cJSON_AddStringToObject(myJsoninfo, IX2V_DevModel, API_PublicInfo_Read_String(PB_DEV_MODEL));
	cJSON_AddStringToObject(myJsoninfo, IX2V_FW_VER, fwVer);
	cJSON_AddStringToObject(myJsoninfo, IX2V_HW_VER, API_PublicInfo_Read_String(PB_HD_VER));
	cJSON_AddStringToObject(myJsoninfo, IX2V_UpTime, API_PublicInfo_Read_String(PB_UP_TIME));

	return myJsoninfo;
}

/*********************************************************************************************************
**  End Of File
*********************************************************************************************************/

