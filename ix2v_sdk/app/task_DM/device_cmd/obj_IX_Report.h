/**
  ******************************************************************************
  * @file    obj_IX_Report.h
  * @author  cao
  * @version V00.01.00
  * @date    2016.5.31
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 
	  //cao_20170304  �����ļ�

#ifndef _obj_IX_Report_H
#define _obj_IX_Report_H

// Define Object Property-------------------------------------------------------
#define IX_REPORT_DATA_LEN						950

#define REQUEST_PROG_CALL_NBR_REQ				0xA02B			// �༭��������
#define REQUEST_PROG_CALL_NBR_RSP				0xA02C			// �༭����Ӧ��

#define REQUEST_TIP_REQ							0xA02D			// TIP ����
#define REQUEST_TIP_RSP							0xA02E			// TIP ����Ӧ��

#define REPORT_IP_ADDR_REQ						0x0401			// IP��ַ��������
#define REPORT_IP_ADDR_RSP						0x8401			// IP��ַ����Ӧ��

#define REPORT_REBOOT_REQ						0x0402			// ������������
#define REPORT_REBOOT_RSP						0x8402			// ��������Ӧ��

#define REPORT_REQ_RES_DOWNLOAD					0x0403			// RES_DOWNLOAD����
#define REPORT_RSP_RES_DOWNLOAD					0x8403			// RES_DOWNLOADӦ��

#define REQUEST_RES_Download_REQ				0x0404			// ��������
#define REQUEST_RES_Download_RSP				0x8404			//

#define REPORT_SHELL_REQ						0x0405			//shell��ӡ����
#define REPORT_SHELL_RSP						0x8405			//

#define REPORT_Event_REQ						0x0406			//事件汇报请求
#define REPORT_Event_RSP						0x8406			//

#define GetNotNullString(str)					((str == NULL) ? "" : str)

#define OTHER_PARA_DATA_LEN			50

typedef struct 
{
	char 		deviceType[OTHER_PARA_DATA_LEN+1];
	char 		codeInfo[OTHER_PARA_DATA_LEN+1];
	char 		appCode[OTHER_PARA_DATA_LEN+1];
	char 		appVer[OTHER_PARA_DATA_LEN+1];
	int 		codeSize;
} FW_VER_VERIFY_T;

typedef struct
{
	char	MFG_SN[12+1];			//MFG_SN
	char	BD_RM_MS[10+1];			//BD_RM_MS
	char	Local[6+1];				//Local
	char	Global[10+1];			//Global
	char	IP_STATIC[7];			//IP_STATIC
	char	IP_ADDR[16];			//IP_ADDR
	char	IP_MASK[16];			//IP_MASK
	char	IP_GATEWAY[16];			//IP_GATEWAY
	int		deviceType;
	char	name1[21];
	char	name2_utf8[41];
	int		bootTime;
	char	Ver_Devicetype[10+1];
} R8012_Device;

typedef struct
{
	R8012_Device	r8012Device;			
}REPORT_IP_ADDR_DATA_T;

typedef struct
{
	R8012_Device	r8012Device;
	FW_VER_VERIFY_T FwVersion;
}REPORT_REBOOT_DATA_T;

typedef struct
{
	R8012_Device	r8012Device;			
}REPORT_REQ_CALL_NBR_DATA_T;

typedef struct
{
	R8012_Device	r8012Device;			
}REPORT_TIP_REQ_DATA_T;

typedef struct
{
	char			TYPE[20+1];				//��������TCP or UDP
	char			STATE[20+1];			//״̬
	char			RATES[20+1];			//����
	char			RES_NBR[20+1];			//��Դ��
	char			IP[20+1];				//������IP
	char			CODE[20+1];				//������			
}RES_DOWNLOAD_T;

typedef struct
{
	R8012_Device	r8012Device;	
	char			res_nbr[6+1];
}DOWNLOAD_REQ_T;


typedef struct
{
	int				rand;				//�����
	int				sourceIp;			//Դip
	unsigned short	reportType;			//��������
	char			data[IX_REPORT_DATA_LEN+1];			//information
} IxReportReq;

typedef struct
{
	int						rand;				//�����
	int						sourceIp;			//Դip
	int						result;				//result
} IxReportRsp;

typedef struct
{
	int				waitFlag;			//�ȴ���Ӧ���
	int				rand;				//�����
	int				sourceIp;			//Դip
	int				result;
}IxReportRun;


// Define Object Function - Public----------------------------------------------
void IxReport(int ip, unsigned short reportType);

void IxReportResDownload(int* reportIp, int reportCnt, char* TYPE, char* STATE, char* RATES, char* RES_NBR, char* IP, char* CODE);


// Define Object Function - Private---------------------------------------------



#endif


