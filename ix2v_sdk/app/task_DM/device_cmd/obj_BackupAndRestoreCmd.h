/**
  ******************************************************************************
  * @file    obj_BackupAndRestoreCmd.h
  * @author  cao
  * @version V00.01.00
  * @date    2016.5.31
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 
	  //cao_20170304  整个文件

#ifndef _obj_BackupAndRestoreCmd_H
#define _obj_BackupAndRestoreCmd_H

// Define Object Property-------------------------------------------------------
#define OPERATION_RESTORE		0
#define OPERATION_BACKUP		1

#pragma pack(1)  //指定按1字节对齐

typedef struct
{
	int				waitFlag;			//等待回应标记
	int				rand;				//随机数
	int				sourceIp;			//源ip
	int				result;
}BackupAndRestoreCmdRun;

// GetIpBy Number 指令包结构
typedef struct
{
	int		rand;				//随机数
	int		sourceIp;			//源ip
	int		operation;			//备份或者恢复
	int		range;				//备份或者恢复范围
	int 	backupPacket;		//选择哪个备份包
} BackupAndRestoreCmdReq;

typedef struct
{
	int		rand;				//随机数
	int		sourceIp;			//源ip
	int		operation;			//备份或者恢复
	int		range;				//备份或者恢复范围
	int 	backupPacket;		//选择哪个备份包
	int		result;				//结果
}BackupAndRestoreCmdRsp;

#pragma pack()



// Define Object Function - Public----------------------------------------------
int API_BackupAndRestoreCmd(int ip, int operation, int packet, int range, int timeOut);

//接收到BackupAndRestore命令处理应答指令
void ReceiveBackupAndRestoreCmdRsp(BackupAndRestoreCmdRsp* rspData);

//接收到BackupAndRestore命令处理请求指令
void ReceiveBackupAndRestoreCmdReq(int target_ip, BackupAndRestoreCmdReq* pReadReq);


// Define Object Function - Private---------------------------------------------


#endif


