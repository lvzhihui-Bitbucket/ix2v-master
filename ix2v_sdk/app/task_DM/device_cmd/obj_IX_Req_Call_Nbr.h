/**
  ******************************************************************************
  * @file    obj_CmdGetAbout.h
  * @author  cao
  * @version V00.01.00
  * @date    2016.5.31
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 
	  //cao_20170304  整个文件

#ifndef _obj_IX_Req_Call_Nbr_H
#define _obj_IX_Req_Call_Nbr_H

// Define Object Property-------------------------------------------------------
#include "task_IxProxy.h"
#include "obj_CmdReboot.h"


typedef struct
{
	char	DeviceNbr[10+1];		//房号
	char	MFG_SN[12+1];			//MFG_SN
	char	IP_Addr[15+1];			//IP地址

	char	Local[6+1];				//Local
	char	Global[10+1];			//Global
	char	name1[21];
	char	name2_utf8[41];
}IX_REQ_CALL_NBR_DATA_T;

typedef struct
{
	int				rand;				//随机数
	int				sourceIp;			//源ip
	char			data[500];				//information
} IxReqCallNbrReq;

typedef struct
{
	int						rand;				//随机数
	int						sourceIp;			//源ip
	int						result;				//result
} IxReqCallNbrRsp;

typedef struct
{
	int				waitFlag;			//等待回应标记
	int				rand;				//随机数
	int				sourceIp;			//源ip
	int				result;
}IxReqCallNbrRun;


// Define Object Function - Public----------------------------------------------
char* CreateIxReqCallNbrJsonData(IX_REQ_CALL_NBR_DATA_T* data);


// Define Object Function - Private---------------------------------------------



#endif


