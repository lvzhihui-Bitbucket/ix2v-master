/**
  ******************************************************************************
  * @file    obj_GetIpByInput.h
  * @author  cao
  * @version V00.01.00
  * @date    2016.5.31
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 
	  //cao_20170304  整个文件

#ifndef _obj_GetIpByInput_H
#define _obj_GetIpByInput_H


// Define Object Property-------------------------------------------------------
typedef struct
{
	char	BD_RM_MS[11];	//BD_RM_MS
	int 	ip;				//ip地址
} IpCacheRecord_T;



// Define Object Function - Public----------------------------------------------
//通过输入得到IP
// paras：
// input_str: 输入字符串
// record: IP和房号
// 0/得不到IP, >0/得到多个房号IP 
int API_GetIpByInput(const char* input, IpCacheRecord_T record[]);


#endif


