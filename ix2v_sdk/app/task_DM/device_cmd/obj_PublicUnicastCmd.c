/**
  ******************************************************************************
  * @file    obj_PublicUnicastCmd.c
  * @author  czb
  * @version V00.01.00
  * @date    2022.3.28
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 
  
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <memory.h>
#include "cJSON.h"
#include "vtk_udp_stack_device_update.h"
#include "obj_PublicUnicastCmd.h"
#include "obj_PublicInformation.h"
#include "elog.h"

int ShellCmdProcess(int ip, PublicUnicastCmdData dataIn, PublicUnicastCmdData* dataOut);
int EventCmdProcess(int ip, PublicUnicastCmdData dataIn, PublicUnicastCmdData* dataOut);
int FileManagementProcess(int ip, PublicUnicastCmdData dataIn, PublicUnicastCmdData* dataOut);
int TftpWriteFileReqProcess(int ip, PublicUnicastCmdData dataIn, PublicUnicastCmdData* dataOut);
int TftpReadFileReqProcess(int ip, PublicUnicastCmdData dataIn, PublicUnicastCmdData* dataOut);;
int FileVerifyReqProcess(int ip, PublicUnicastCmdData dataIn, PublicUnicastCmdData* dataOut);
static int GetPbByIpProcess(int ip, PublicUnicastCmdData dataIn, PublicUnicastCmdData* dataOut);
static int ReadWriteIoProcess(int ip, PublicUnicastCmdData dataIn, PublicUnicastCmdData* dataOut);
int IXS_ProxyUpdateDevTbRsp(int ip, PublicUnicastCmdData dataIn, PublicUnicastCmdData* dataOut);
int RemoteTableProcess(int ip, PublicUnicastCmdData dataIn, PublicUnicastCmdData* dataOut);
int SetDivertStateProcess(int ip, PublicUnicastCmdData dataIn, PublicUnicastCmdData* dataOut);
int GetQR_CodeProcess(int ip, PublicUnicastCmdData dataIn, PublicUnicastCmdData* dataOut);
static int Ring2Process(int ip, PublicUnicastCmdData dataIn, PublicUnicastCmdData* dataOut);

static const PublicUnicastCmdtoProcess UnicastCmdTable[] = {
	{EVENT_REQ, 						EVENT_RSP, 					EventCmdProcess},
	{SHELL_REQ, 						SHELL_RSP, 					ShellCmdProcess},
	{FileManagement_REQ, 				FileManagement_RSP, 		FileManagementProcess},
	{TFTP_WRITE_FILE_REQ, 				TFTP_WRITE_FILE_RSP, 		TftpWriteFileReqProcess},
	{TFTP_READ_FILE_REQ, 				TFTP_READ_FILE_RSP, 		TftpReadFileReqProcess},
	{TFTP_CHECK_FILE_REQ, 				TFTP_CHECK_FILE_RSP, 		FileVerifyReqProcess},
	{GET_PB_BY_IP_REQ, 					GET_PB_BY_IP_RSP, 			GetPbByIpProcess},
	{GET_RW_IO_BY_IP_REQ, 				GET_RW_IO_BY_IP_RSP, 		ReadWriteIoProcess},
	{UPDATE_DEV_TABLE_REQ, 				UPDATE_DEV_TABLE_RSP, 		IXS_ProxyUpdateDevTbRsp},
	{TABLE_PROCESS_REQ,					TABLE_PROCESS_RSP,			RemoteTableProcess},
	{SET_DIVERT_STATE_REQ,				SET_DIVERT_STATE_RSP,		SetDivertStateProcess},
	{GET_QR_CODE_REQ,					GET_QR_CODE_RSP,			GetQR_CodeProcess},
	{IX_Ring2_REQ,						IX_Ring2_RSP,				Ring2Process},
	
	};

static const int UnicastCmdTableNum = sizeof(UnicastCmdTable)/sizeof(UnicastCmdTable[0]);
static MyPublicUnicastTable myPublicUnicastTable = {.lock = PTHREAD_MUTEX_INITIALIZER, .table = NULL};

static PublicUnicastCmdRun* CreatePublicUnicastCmdRun(int targetIp)
{
	int rand;
	cJSON* element = NULL;
	PublicUnicastCmdRun* publicUnicastCmdRun = NULL;

	pthread_mutex_lock(&myPublicUnicastTable.lock);

	if(myPublicUnicastTable.table == NULL)
	{
		myPublicUnicastTable.table = cJSON_CreateArray();
	}

	do
	{
		rand = MyRand() ;//+ MyRand2(targetIp);
		cJSON_ArrayForEach(element, myPublicUnicastTable.table)
		{
			publicUnicastCmdRun = (PublicUnicastCmdRun*)element->valueint;
			if(publicUnicastCmdRun->rand == rand)
			{
				printf("!!!!!!!!!!!:%d\n",rand);
				break;
			}
		}
	} while(element);

	publicUnicastCmdRun = malloc(sizeof(PublicUnicastCmdRun));
	element = cJSON_CreateNumber((int)publicUnicastCmdRun);
	cJSON_AddItemToArray(myPublicUnicastTable.table, element);

	publicUnicastCmdRun->rand = rand;
	publicUnicastCmdRun->waitFlag = 1;
	publicUnicastCmdRun->result = -1;
	publicUnicastCmdRun->targetIp = targetIp;
	publicUnicastCmdRun->packetCnt = 0;
	publicUnicastCmdRun->packet = NULL;

	pthread_mutex_unlock(&myPublicUnicastTable.lock);

	return publicUnicastCmdRun;
}

static void FreePublicUnicastCmdRun(PublicUnicastCmdRun* publicUnicastCmdRun)
{
	cJSON* element = NULL;

	pthread_mutex_lock(&myPublicUnicastTable.lock);
	publicUnicastCmdRun->waitFlag = 0;
	int j;
	for(j = 0; j < publicUnicastCmdRun->packetCnt; j++)
	{
		if(publicUnicastCmdRun->packet[j].data != NULL)
		{
			free(publicUnicastCmdRun->packet[j].data);
			publicUnicastCmdRun->packet[j].data = NULL;
		}
	}

	if(publicUnicastCmdRun->packet != NULL)
	{
		free(publicUnicastCmdRun->packet);
		publicUnicastCmdRun->packet = NULL;
	}

	if(publicUnicastCmdRun)
	{
		free(publicUnicastCmdRun);
	}

	for(j = cJSON_GetArraySize(myPublicUnicastTable.table); j > 0; j--)
	{
		element = cJSON_GetArrayItem(myPublicUnicastTable.table, j);
		if(element)
		{
			if(((int)publicUnicastCmdRun) == element->valueint)
			{
				cJSON_DeleteItemFromArray(myPublicUnicastTable.table, j);
				break;
			}
		}
	}
	pthread_mutex_unlock(&myPublicUnicastTable.lock);
}

static PublicUnicastCmdProcess GetPublicUnicastCmdProcess(unsigned short cmd)
{
	int i;
	PublicUnicastCmdProcess			process;

	pthread_mutex_lock(&myPublicUnicastTable.lock);
	for(i = 0; i < UnicastCmdTableNum; i++)
	{
		if(cmd == UnicastCmdTable[i].reqCmd || cmd == UnicastCmdTable[i].rspCmd)
		{
			process = UnicastCmdTable[i].process;
			break;
		}
	}
	pthread_mutex_unlock(&myPublicUnicastTable.lock);

	return process;
}

static unsigned short GetPublicUnicastRspCmd(unsigned short cmd)
{
	int i;
	unsigned short rspCmd = 0;

	pthread_mutex_lock(&myPublicUnicastTable.lock);
	for(i = 0; i < UnicastCmdTableNum; i++)
	{
		if(cmd == UnicastCmdTable[i].reqCmd)
		{
			rspCmd = UnicastCmdTable[i].rspCmd;
			break;
		}
	}
	pthread_mutex_unlock(&myPublicUnicastTable.lock);

	return rspCmd;
}

static int VerifyMFG_SN(const char* mfg_sn, const char* mfg_sn2)
{
	if(mfg_sn == NULL || mfg_sn2 == NULL)
	{
		return 1;
	}
	else if(mfg_sn[0] == 0 || mfg_sn2[0] == 0)
	{
		return 1;
	}
	else if(!strcmp(mfg_sn, mfg_sn2))
	{
		return 1;
	}

	return 0;
}

int PublicUnicastCmdRequst(int ip, const char* mfg_sn, int timeOut, unsigned short cmd, PublicUnicastCmdData dataIn, PublicUnicastCmdData* dataOut)
{
	PublicUnicastCmdRun* publicUnicastCmdRun;
	PublicUnicastCmdReq sendData;
	PublicUnicastCmdProcess process = GetPublicUnicastCmdProcess(cmd);
	cJSON * element;

	int rand, i, j, timeCnt, ret, myIp, dataLen;
	char* tempData = NULL;

	ret = -1;

	sendData.dataLen = dataIn.len;
	if(sendData.dataLen > PUBLIC_UNICAST_DATA_LEN)
	{
		return -2;
	}

	if(dataIn.data == NULL || sendData.dataLen == 0)
	{
		sendData.data[0] = 0;
	}
	else
	{
		memcpy(sendData.data, dataIn.data, sendData.dataLen);
	}

	myIp = inet_addr(GetSysVerInfo_IP_by_device(GetNetDeviceNameByTargetIp(ip)));
	if(myIp == ip)
	{
		if(VerifyMFG_SN(mfg_sn, API_PublicInfo_Read_String(PB_MFG_SN)))
		{
			if(process)
			{
				ret = process(ip, dataIn, dataOut);
			}
			else
			{
				ret = -4;
			}
		}
		else
		{
			ret = -3;
		}
	}
	else
	{		
		publicUnicastCmdRun = CreatePublicUnicastCmdRun(ip);

		sendData.rand = publicUnicastCmdRun->rand;
		sendData.sourceIp = myIp;
		strncpy(sendData.MFG_SN, (mfg_sn == NULL ? "" : mfg_sn), 13);

		api_udp_device_update_send_data(ip, htons(cmd), (char*)&sendData, sizeof(sendData) - PUBLIC_UNICAST_DATA_LEN + sendData.dataLen);

		timeCnt = timeOut*1000/UDP_WAIT_RSP_TIME;
		while(timeCnt)
		{
			usleep(UDP_WAIT_RSP_TIME*1000);

			pthread_mutex_lock(&myPublicUnicastTable.lock);
			if(publicUnicastCmdRun->waitFlag == 0)
			{
				ret = publicUnicastCmdRun->result;
				if(ret == 0)
				{
					for(i = 0, dataLen = 0; i < publicUnicastCmdRun->packetCnt; i++)
					{
						dataLen += publicUnicastCmdRun->packet[i].dataLen;
					}

					if(dataOut != NULL)
					{
						if(dataLen)
						{
							tempData = malloc(dataLen);
						}

						if(tempData != NULL)
						{
							for(i = 0, dataLen = 0; i < publicUnicastCmdRun->packetCnt; i++)
							{
								for(j = 0; j < publicUnicastCmdRun->packetCnt; j++)
								{
									if(i+1 == publicUnicastCmdRun->packet[j].no)
									{
										memcpy(tempData + dataLen, publicUnicastCmdRun->packet[j].data, publicUnicastCmdRun->packet[j].dataLen);
										dataLen += publicUnicastCmdRun->packet[j].dataLen;
										break;
									}
								}
							}
						}

						(*dataOut).data = tempData;
						(*dataOut).len = dataLen;
					}
				}
			}
			int waitFlag = publicUnicastCmdRun->waitFlag;
			pthread_mutex_unlock(&myPublicUnicastTable.lock);
			if(waitFlag)
			{
				if(--timeCnt == 0)
				{
					ret = -1;
				}
			}
			else
			{
				timeCnt = 0;
			}
		}

		FreePublicUnicastCmdRun(publicUnicastCmdRun);
	}

	return ret;
}

//接收到PublicUnicast命令应答指令
void ReceivePublicUnicastCmdRsp(unsigned short cmd, PublicUnicastCmdRsp* rspData)
{
	PublicUnicastCmdRun* publicUnicastCmdRun;
	cJSON* element;
	int i;

	pthread_mutex_lock(&myPublicUnicastTable.lock);
	cJSON_ArrayForEach(element, myPublicUnicastTable.table)
	{
		publicUnicastCmdRun = (PublicUnicastCmdRun*)element->valueint;
		if(publicUnicastCmdRun->waitFlag && publicUnicastCmdRun->rand == rspData->rand && rspData->sourceIp == publicUnicastCmdRun->targetIp)
		{
			publicUnicastCmdRun->result = rspData->result;

			if(publicUnicastCmdRun->packet == NULL)
			{
				publicUnicastCmdRun->packet = malloc(rspData->packetTotal * sizeof(PublicUnicastPacket));
			}

			//log_d("packetTotal=%d, packetNo=%d,dataLen=%d\n", rspData->packetTotal, rspData->packetNo, rspData->dataLen);

			if(publicUnicastCmdRun->packet != NULL)
			{
				for(i = 0; i < publicUnicastCmdRun->packetCnt; i++)
				{
					if(rspData->packetNo == publicUnicastCmdRun->packet[i].no)
					{
						break;
					}
				}

				if(i == publicUnicastCmdRun->packetCnt)
				{
					publicUnicastCmdRun->packet[publicUnicastCmdRun->packetCnt].no = rspData->packetNo;
					publicUnicastCmdRun->packet[publicUnicastCmdRun->packetCnt].dataLen = rspData->dataLen;
					if(rspData->dataLen)
					{
						publicUnicastCmdRun->packet[publicUnicastCmdRun->packetCnt].data = malloc(rspData->dataLen);
						if(publicUnicastCmdRun->packet[publicUnicastCmdRun->packetCnt].data != NULL)
						{
							memcpy(publicUnicastCmdRun->packet[publicUnicastCmdRun->packetCnt].data, rspData->data, rspData->dataLen);
						}
					}
					else
					{
						publicUnicastCmdRun->packet[publicUnicastCmdRun->packetCnt].data = NULL;
					}

					publicUnicastCmdRun->packetCnt++;
				}

				if(publicUnicastCmdRun->packetCnt == rspData->packetTotal)
				{
					publicUnicastCmdRun->waitFlag = 0;
				}
			}

		}
	}
	pthread_mutex_unlock(&myPublicUnicastTable.lock);
}

//接收到PublicUnicast命令请求指令
int ReceivePublicUnicastCmdReq(char* netDeviceName, unsigned short cmd, PublicUnicastCmdReq* reqData, int len)
{
	PublicUnicastCmdRsp	sendData;
	PublicUnicastCmdProcess process = GetPublicUnicastCmdProcess(cmd);
	PublicUnicastCmdData dataIn;
	PublicUnicastCmdData dataOut = {.data = NULL, .len = 0};
	PublicMulRspUdpCmdReq* reqData2 = (PublicMulRspUdpCmdReq*)reqData;
	int i, len1, len2;
	len1 = sizeof(PublicUnicastCmdReq) - PUBLIC_UNICAST_DATA_LEN + reqData->dataLen;
	len2 = sizeof(PublicMulRspUdpCmdReq) - PUBLIC_MUL_RSP_UDP_CMD_REQ_DATA_LEN + reqData2->dataLen;

	log_d("len=%d, len1=%d,len2=%d\n", len, len1, len2);

	if(len == len1)
	{
		if(!VerifyMFG_SN(reqData->MFG_SN, API_PublicInfo_Read_String(PB_MFG_SN)))
		{
			return -2;
		}

		dataIn.data = reqData->data;
		dataIn.len = reqData->dataLen;
	}
	else if(len == len2)
	{
		dataIn.data = reqData2->data;
		dataIn.len = reqData2->dataLen;
	}
	else
	{
		return -3;
	}

	if(process)
	{
		sendData.result = process(reqData->sourceIp, dataIn, &dataOut);
	}
	else
	{
		sendData.result = -1;
	}

	sendData.rand = reqData->rand;
	sendData.sourceIp = inet_addr(GetSysVerInfo_IP_by_device(netDeviceName));
	if(dataOut.len == 0 || dataOut.data == NULL)
	{
		sendData.packetTotal = 1;
		sendData.packetNo=1;
		sendData.dataLen=0;
		api_udp_device_update_send_data_by_device(netDeviceName, reqData->sourceIp, htons(GetPublicUnicastRspCmd(cmd)), (char*)&sendData, sizeof(sendData) - PUBLIC_UNICAST_DATA_LEN);
	}
	else
	{
		sendData.packetTotal = dataOut.len/PUBLIC_UNICAST_DATA_LEN + ((dataOut.len % PUBLIC_UNICAST_DATA_LEN) ? 1 : 0);
		for(i = 0; i < sendData.packetTotal; i++)
		{
			sendData.dataLen = (dataOut.len - PUBLIC_UNICAST_DATA_LEN*i > PUBLIC_UNICAST_DATA_LEN ? PUBLIC_UNICAST_DATA_LEN : dataOut.len - PUBLIC_UNICAST_DATA_LEN*i);
			sendData.packetNo = i+1;
			memcpy(sendData.data, dataOut.data + PUBLIC_UNICAST_DATA_LEN*i, sendData.dataLen);
			UNICAST_CMD_RSP_DELAY();
			log_d("packetTotal=%d, packetNo=%d,dataLen=%d\n", sendData.packetTotal, sendData.packetNo, sendData.dataLen);
			api_udp_device_update_send_data_by_device(netDeviceName, reqData->sourceIp, htons(GetPublicUnicastRspCmd(cmd)), (char*)&sendData, sizeof(sendData) - PUBLIC_UNICAST_DATA_LEN + sendData.dataLen);
		}
	}

	if(dataOut.data != NULL)
	{
		free(dataOut.data);
		dataOut.data = NULL;
	}

	return 0;
}

void FreeUnicastOutData(PublicUnicastCmdData* data)
{
	if(data)
	{
		if(data->data)
		{
			free(data->data);
			data->data = NULL;
		}
		data->len = 0;
	}
}

static int GetPbByIpProcess(int ip, PublicUnicastCmdData dataIn, PublicUnicastCmdData* dataOut)
{
	cJSON *view = cJSON_Parse(dataIn.data);;
	cJSON *retValue  = cJSON_CreateObject();
	cJSON *element;

	if(view)
	{
		cJSON_ArrayForEach(element, view)
		{
			if(element->string)
			{
				if(element->valuestring != NULL && !strcmp(element->valuestring, "IO"))
				{
					cJSON_AddItemReferenceToObject(retValue, element->string, API_Para_Read_Public(element->string));
				}
				else
				{
					cJSON_AddItemReferenceToObject(retValue, element->string, API_PublicInfo_Read(element->string));
				}
			}
		}
	}

	if(dataOut != NULL)
	{
		(*dataOut).data = cJSON_PrintUnformatted(retValue);
		(*dataOut).len = strlen((*dataOut).data) + 1;
	}
	cJSON_Delete(retValue);
	cJSON_Delete(view);

	return 0;
}

static int Ring2Process(int ip, PublicUnicastCmdData dataIn, PublicUnicastCmdData* dataOut)
{
	cJSON *ringData = cJSON_Parse(dataIn.data);
	cJSON * msg = cJSON_GetObjectItemCaseSensitive(ringData, IX2V_MSG);
	cJSON * time = cJSON_GetObjectItemCaseSensitive(ringData, IX2V_TIME);

	if(cJSON_IsString(msg) && cJSON_IsNumber(time))
	{
		//API_VideoMenu_Ring2(msg->valuestring, msg->valueint);
		#ifdef PID_IX850
		//if(strstr(msg->valuestring,"restart completed")!=NULL)
		{
			vtk_lvgl_lock(); 
			API_TIPS_Ext(msg->valuestring);
			vtk_lvgl_unlock(); 
		}
		#endif
	}
	cJSON_Delete(ringData);

	return 0;
}

static int ReadWriteIoProcess(int ip, PublicUnicastCmdData dataIn, PublicUnicastCmdData* dataOut)
{
	cJSON *rwData = cJSON_Parse(dataIn.data);
	cJSON *retValue  = cJSON_Duplicate(rwData, 1);
	cJSON *paraValue, *para, *element;
	int writeResult = 1;

	//dprintf("dataIn.data=%s\n", dataIn.data);

	if(para = cJSON_GetObjectItem(rwData, DM_KEY_READ))
	{
		cJSON_DeleteItemFromObject(retValue, DM_KEY_READ);
		paraValue = cJSON_AddObjectToObject(retValue, DM_KEY_READ);
	}
	else if(para = cJSON_GetObjectItem(rwData, DM_KEY_WRITE))
	{
		writeResult = API_Para_Write_Public(para);
		cJSON_DeleteItemFromObject(retValue, DM_KEY_WRITE);
		paraValue = cJSON_AddObjectToObject(retValue, DM_KEY_WRITE);
	}

	if(para)
	{
		cJSON_AddStringToObject(retValue, IX2V_Result, (writeResult ? RESULT_SUCC : RESULT_ERR10));
	
		cJSON_ArrayForEach(element, para)
		{
			if(element->string)
			{
				cJSON_AddItemReferenceToObject(paraValue, element->string, API_Para_Read_Public(element->string));
			}
		}
	}


	if(dataOut != NULL)
	{
		(*dataOut).data = cJSON_PrintUnformatted(retValue);
		(*dataOut).len = strlen((*dataOut).data) + 1;
	}
	cJSON_Delete(retValue);
	cJSON_Delete(rwData);

	return 0;
}

static int ReadWrite_DT_Information(int ip, PublicUnicastCmdData dataIn, PublicUnicastCmdData* dataOut)
{
	cJSON *rwData = cJSON_Parse(dataIn.data);
	cJSON *retValue  = cJSON_Duplicate(rwData, 1);
	cJSON *paraValue, *para, *element;
	char* hexString = GetEventItemString(rwData, IX2V_Data);
	int hexStrLen, i;
	char hex[200], ch0, ch1;

	if(hexString && !((hexStrLen = strlen(hexString))%2))
	{
		for(i = 0; i < hexStrLen/2 && i < 200; i++)
		{
			ch0 = CharToHex(hexString[2*i]);
			ch1 = CharToHex(hexString[2*i+1]);
			hex[i] = (ch0 << 4 | ch1);
		}


		if(para = cJSON_GetObjectItem(rwData, DM_KEY_READ))
		{

		}
		else if(para = cJSON_GetObjectItem(rwData, DM_KEY_WRITE))
		{

		}

		cJSON_AddStringToObject(retValue, IX2V_Result, RESULT_SUCC);
	}



	if(dataOut != NULL)
	{
		(*dataOut).data = cJSON_PrintUnformatted(retValue);
		(*dataOut).len = strlen((*dataOut).data) + 1;
	}
	cJSON_Delete(retValue);
	cJSON_Delete(rwData);

	return 0;
}


cJSON* API_GetPbIo(int ip, const char *sn, const cJSON *view)
{
	cJSON* retJson = NULL;
	PublicUnicastCmdData dataIn = {0};
	PublicUnicastCmdData dataOut = {0};

	dataIn.data = cJSON_PrintUnformatted(view);
	if(dataIn.data)
	{
		dataIn.len = strlen(dataIn.data) + 1;
	}
	else
	{
		dataIn.len = 0;
	}

	if(PublicUnicastCmdRequst(ip, sn, BusinessWaitUdpTime, GET_PB_BY_IP_REQ, dataIn, &dataOut) == 0)
	{
		retJson = cJSON_Parse(dataOut.data);
	}
	FreeUnicastOutData(&dataOut);
	FreeUnicastOutData(&dataIn);

	return retJson;
}

cJSON* API_GetRemotePb(int ip, const char* pbName)
{
	cJSON* retJson = NULL;
	if(pbName)
	{
		cJSON *view = cJSON_CreateObject();
		cJSON_AddStringToObject(view, pbName, "PB");
		cJSON *getJson = API_GetPbIo(ip, NULL, view);
		if(!getJson)
		{
			getJson = API_GetPbIo(ip, NULL, view);
		}
		retJson = cJSON_Duplicate(cJSON_GetObjectItemCaseSensitive(getJson, pbName), 1);
		cJSON_Delete(view);
		cJSON_Delete(getJson);
	}
	return retJson;
}

//返回0--不在线，-1--不是字符串，1--正确。
int API_GetRemotePbString(int ip, const char* pbName, char* pbResult)
{
	int ret = 0;
	cJSON* retJson = NULL;
	cJSON* result = NULL;
	PublicUnicastCmdData dataIn = {0};
	PublicUnicastCmdData dataOut = {0};

	cJSON *view = cJSON_CreateObject();
	cJSON_AddStringToObject(view, pbName, "PB");
	dataIn.data = cJSON_PrintUnformatted(view);
	cJSON_Delete(view);

	if(dataIn.data)
	{
		dataIn.len = strlen(dataIn.data) + 1;
	}
	else
	{
		dataIn.len = 0;
	}

	if(PublicUnicastCmdRequst(ip, NULL, BusinessWaitUdpTime, GET_PB_BY_IP_REQ, dataIn, &dataOut) == 0)
	{
		retJson = cJSON_Parse(dataOut.data);
		result = cJSON_GetObjectItemCaseSensitive(retJson, pbName);
		if(cJSON_IsString(result))
		{
			ret = 1;
			if(pbResult)
			{
				strcpy(pbResult, result->valuestring);
			}
		}
		else
		{
			ret = -1;
		}
		cJSON_Delete(retJson);
	}
	FreeUnicastOutData(&dataOut);
	FreeUnicastOutData(&dataIn);

	return ret;
}

int API_IXS_ProxyUpdateDevTbReq(int ip)
{
	PublicUnicastCmdData dataIn;
	int ret = 0;

	dataIn.len = 0;

	if(PublicUnicastCmdRequst(ip, NULL, BusinessWaitUdpTime, UPDATE_DEV_TABLE_REQ, dataIn, NULL) == 0)
	{
		ret = 1;
	}

	return ret;
}

//返回：0 --失败；1 --成功
int API_XD_EventJson(int ip, cJSON* event)
{
	PublicUnicastCmdData dataIn;
	int ret = 0;
	cJSON* sendJson = cJSON_CreateObject();
	cJSON_AddItemToObject(sendJson, IX2V_Event, cJSON_Duplicate(event, 1));
	dataIn.data = cJSON_PrintUnformatted(sendJson);
	cJSON_Delete(sendJson);
	dataIn.len = strlen(dataIn.data) + 1;

	if(PublicUnicastCmdRequst(ip, NULL, BusinessWaitUdpTime, EVENT_REQ, dataIn, NULL) == 0)
	{
		ret = 1;
	}
	else
	{
		if(PublicUnicastCmdRequst(ip, NULL, BusinessWaitUdpTime, EVENT_REQ, dataIn, NULL) == 0)
		{
			ret = 1;
		}
	}

	if(dataIn.data)
	{
		free(dataIn.data);
	}

	return ret;
}

int API_XD_EventJson2(int ip, cJSON* event)
{
	PublicUnicastCmdData dataIn;
	int ret = 0;
	cJSON* sendJson = cJSON_CreateObject();
	cJSON_AddItemToObject(sendJson, "Event", cJSON_Duplicate(event, 1));
	dataIn.data = cJSON_PrintUnformatted(sendJson);
	cJSON_Delete(sendJson);
	dataIn.len = strlen(dataIn.data) + 1;

	if(PublicUnicastCmdRequst(ip, NULL, 0, EVENT_REQ, dataIn, NULL) == 0)
	{
		ret = 1;
	}

	if(dataIn.data)
	{
		free(dataIn.data);
	}

	return ret;
}


/*
	value格式：["IX_ADDR", "IP_ADDR", "IX_NAME"]

	返回值格式：{
					"READ":{
						"IX_ADDR":"0099000101",
						"IP_ADDR":"192.168.243.101",
						"IX_NAME":"IX611"
					},

					"Result":"Succ"
	}
*/
cJSON* API_ReadRemoteIo(int ip, const cJSON *value)
{
	cJSON* retJson = NULL;
	PublicUnicastCmdData dataIn = {0};
	PublicUnicastCmdData dataOut = {0};
	cJSON* element = NULL;

	cJSON* view = cJSON_CreateObject();
	cJSON* read = cJSON_AddObjectToObject(view, DM_KEY_READ);

	cJSON_ArrayForEach(element, value)
	{
		if(cJSON_IsString(element))
		{
			cJSON_AddFalseToObject(read, element->valuestring);
		}
	}

	dataIn.data = cJSON_PrintUnformatted(view);
	cJSON_Delete(view);

	if(dataIn.data)
	{
		dataIn.len = strlen(dataIn.data) + 1;
	}
	else
	{
		dataIn.len = 0;
	}

	if(PublicUnicastCmdRequst(ip, NULL, BusinessWaitUdpTime, GET_RW_IO_BY_IP_REQ, dataIn, &dataOut) == 0)
	{
		retJson = cJSON_Parse(dataOut.data);
	}
	else if(PublicUnicastCmdRequst(ip, NULL, BusinessWaitUdpTime, GET_RW_IO_BY_IP_REQ, dataIn, &dataOut) == 0)
	{
		retJson = cJSON_Parse(dataOut.data);
	}

	FreeUnicastOutData(&dataOut);
	FreeUnicastOutData(&dataIn);

	return retJson;
}

/*
	value格式：{
						"IX_ADDR":"0099000101",
						"IP_ADDR":"192.168.243.101",
						"IX_NAME":"IX611"
	}

	返回值格式：{
					"WRITE":{
						"IX_ADDR":"0099000101",
						"IP_ADDR":"192.168.243.101",
						"IX_NAME":"IX611"
					},
					
					"Result":"Succ"
	}
*/

cJSON* API_WriteRemoteIo(int ip, const cJSON *value)
{
	cJSON* retJson = NULL;
	PublicUnicastCmdData dataIn = {0};
	PublicUnicastCmdData dataOut = {0};
	cJSON* element = NULL;

	cJSON* view = cJSON_CreateObject();
	cJSON_AddItemReferenceToObject(view, DM_KEY_WRITE, value);

	dataIn.data = cJSON_PrintUnformatted(view);
	cJSON_Delete(view);

	if(dataIn.data)
	{
		dataIn.len = strlen(dataIn.data) + 1;
	}
	else
	{
		dataIn.len = 0;
	}

	if(PublicUnicastCmdRequst(ip, NULL, BusinessWaitUdpTime, GET_RW_IO_BY_IP_REQ, dataIn, &dataOut) == 0)
	{
		retJson = cJSON_Parse(dataOut.data);
		
	}
	FreeUnicastOutData(&dataOut);
	FreeUnicastOutData(&dataIn);

	return retJson;
}

//返回：0 --失败；1 --成功
int API_Ring2(int ip, char* msg, int time)
{
	PublicUnicastCmdData dataIn;
	int ret = 0;
	cJSON* sendJson = cJSON_CreateObject();
	cJSON_AddStringToObject(sendJson, IX2V_MSG, msg);
	cJSON_AddNumberToObject(sendJson, IX2V_TIME, time);
	dataIn.data = cJSON_PrintUnformatted(sendJson);
	cJSON_Delete(sendJson);
	dataIn.len = strlen(dataIn.data) + 1;

	if(PublicUnicastCmdRequst(ip, NULL, BusinessWaitUdpTime, IX_Ring2_REQ, dataIn, NULL) == 0)
	{
		ret = 1;
	}

	if(dataIn.data)
	{
		free(dataIn.data);
	}

	return ret;
}

/*********************************************************************************************************
**  End Of File
*********************************************************************************************************/

