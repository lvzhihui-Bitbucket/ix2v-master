/**
  ******************************************************************************
  * @file    obj_SearchDevice.h
  * @author  cao
  * @version V00.01.00
  * @date    2016.5.31
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 
	  //cao_20170304  �����ļ�

#ifndef _obj_SearchDevice_H
#define _obj_SearchDevice_H

#include <stdio.h>
#include <pthread.h>
#include "cJSON.h"

// Define Object Property-------------------------------------------------------

typedef void (*SearchCallback)(cJSON*);

typedef struct
{
    pthread_t tid;
	  cJSON* cfg;
	  cJSON* where;
	  cJSON* view;
	  int state;
    SearchCallback callback;
}Search_T;



// Define Object Function - Public----------------------------------------------



// Define Object Function - Private---------------------------------------------


#endif


