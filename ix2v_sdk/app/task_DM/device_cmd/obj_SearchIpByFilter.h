/**
  ******************************************************************************
  * @file    obj_SearchIpByFilter.h
  * @author  cao
  * @version V00.01.00
  * @date    2016.5.31
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 
	  //cao_20170304  �����ļ�

#ifndef _obj_SearchIpByFilter_H
#define _obj_SearchIpByFilter_H

#include <stdio.h>
#include <sys/sysinfo.h>
#include <time.h>
#include "cJSON.h"
#include "obj_PublicCmdDefine.h"
#include "obj_GetDeviceTypeAndArea.h"

// Define Object Property-------------------------------------------------------

#define UDP_SEARCH_TIME_UNIT					10				//�ȴ�Ӧ��ʱ�� ms

#pragma pack(1)  //ָ����1�ֽڶ���

typedef struct
{
	int		Ip;					//ip
	Type_e	deviceType;			//�豸����
	time_t 	systemBootTime;		//����ʱ��
	char	BD_RM_MS[10+1];		//BD_RM_MS
	char	MFG_SN[12+1];		//MFG_SN
	int		ifInRegisterTable;
	char	name[20+1];
	char	platform[5+1];
} SearchOneDeviceData;

typedef struct
{
	int						deviceCnt;				//��Ӧ����
	SearchOneDeviceData		data[MAX_DEVICE];		//�豸��Ϣ
} SearchIpRspData;

typedef struct
{
	pthread_mutex_t 	lock;
	int					waitFlag;			//�ȴ���Ӧ���
	int					rand;				//�����
	int					searchAllTimeCnt;	//������ʱ���ʱ
	int					searchTimeCnt;		//������ʱ
	int					searchRspFlag;		//�Ƿ����豸��Ӧ���
	int					search1Timeout;			//�������search1ָ��֮��û���豸��Ӧ����ȴ�ʱ��
	int					search1IntervalTime;	//���յ�һ���豸֮�󣬳������ʱ��û�������豸Ӧ�𣬵�������û���豸������
	int					search2Enable;			//����1����֮���Ƿ���Ҫ��������2
	int					search2Timeout;			//�������search2ָ��֮��û���豸��Ӧ����ȴ�ʱ��
	int					search2TimeoutMaxCnt;	//Search2最大超时次数
	int					search2IntervalTime;	//���յ�һ���豸֮�󣬳������ʱ��û�������豸Ӧ�𣬵�������û���豸������
	int					searchMaxDevice;		//��������豸��
	SearchIpRspData* 	data;
} SearchIpRun;

typedef struct
{
	int					lanSearch1Timeout;			//�������search1ָ��֮��û���豸��Ӧ����ȴ�ʱ��
	int					lanSearch1IntervalTime;		//���յ�һ���豸֮�󣬳������ʱ��û�������豸Ӧ�𣬵�������û���豸������
	int					lanSearch2Enable;			//����1����֮���Ƿ���Ҫ��������2
	int					lanSearch2Timeout;			//�������search2ָ��֮��û���豸��Ӧ����ȴ�ʱ��
	int					lanSearch2IntervalTime;		//���յ�һ���豸֮�󣬳������ʱ��û�������豸Ӧ�𣬵�������û���豸������
	int					wlanSearch1Timeout;			//�������search1ָ��֮��û���豸��Ӧ����ȴ�ʱ��
	int					wlanSearch1IntervalTime;	//���յ�һ���豸֮�󣬳������ʱ��û�������豸Ӧ�𣬵�������û���豸������
	int					wlanSearch2Enable;			//����1����֮���Ƿ���Ҫ��������2
	int					wlanSearch2Timeout;			//�������search2ָ��֮��û���豸��Ӧ����ȴ�ʱ��
	int					wlanSearch2IntervalTime;	//���յ�һ���豸֮�󣬳������ʱ��û�������豸Ӧ�𣬵�������û���豸������
} SearchPara;

// GetIpBy Number ָ����ṹ
typedef struct
{
	int				rand;				//�����
	int				sourceIp;			//Դip
	Type_e			type;				//��������
	char			BD[4+1];			//BD_Number
} SearchIpReq;

typedef struct
{
	int						rand;				//�����
	SearchOneDeviceData 	data;				//�豸��Ϣ
} SearchIpRsp;

typedef struct
{
	int				rand;				//�����
	int				sourceIp;			//Դip
	Type_e			type;				//��������
	char			BD[4+1];			//BD_Number
	unsigned char	ip[MAX_DEVICE][2];	//�Ѿ���Ӧ��IP
} SearchIpReq2;

typedef struct
{
	int					searchResult;			//�������

	int					searchNetMode;			//����ʹ���ĸ�����

	int					search1Timeout;			//�������search1ָ��֮��û���豸��Ӧ����ȴ�ʱ��
	int					search1IntervalTime;	//���յ�һ���豸֮�󣬳������ʱ��û�������豸Ӧ�𣬵�������û���豸������
	int					search2Enable;			//����1����֮���Ƿ���Ҫ��������2
	int					search2Timeout;			//�������search2ָ��֮��û���豸��Ӧ����ȴ�ʱ��
	int					search2IntervalTime;	//���յ�һ���豸֮�󣬳������ʱ��û�������豸Ӧ�𣬵�������û���豸������

	int					searchAllTime;			//������ʱ���ʱ
	
	int					search1Time;			//����1����ʱ��
	int					search1DevCnt;			//����1��ȡ�豸����
	int					search1FirstDevTime;	//����1��һ���豸����ʱ��
	
	int					search2Time;			//����2����ʱ��
	int					search2DevCnt;			//����2��ȡ�豸����
	int					search2FirstDevTime;	//����2��һ���豸����ʱ��
	int					search2SendCnt;			//����2���ʹ���
} SearchResult;

#pragma pack()

#define SearchDeviceRecommendedWaitingTime			5				//�����豸����ȴ�ʱ�䵥λ��


// Define Object Function - Public----------------------------------------------

//����By number����ָ��
// paras:
// bd_rm_ms: bd_rm_ms
// input : input
// bd : bd number
// time : ��ʱʱ�� ��λmS
// getDeviceCnt : �����豸��������
// data : ���ҽ��
// return:
//  -1:ϵͳæ, -2:���ݽ�����������, -3:û�в��ҵ�, 0:�������
int API_SearchIpByFilter(const char* bd, Type_e type, int time, int getDeviceCnt, SearchIpRspData* data);

//���յ�����Ӧ��ָ��
void ReceiveSearchIpByFilterRsp(SearchIpRsp* rspData);

//���յ�SearchIpByFilterָ��
void ReceiveSearchIpByFilterReq(int target_ip, SearchIpReq* pReadReq);


// Define Object Function - Private---------------------------------------------
int SortJsonTable(const cJSON* table, const char* key, int order);

int IXS_Search(const char* net, cJSON* searchCtrl, const char* BD_Nbr, const char* deviceType, const cJSON* resultTable);
int IXS_SearchIsBusy(void);

/*
	net: wlan0/eth0
	searchCtrl:控制搜索参数
	BD_Nbr:	9999搜索全部，其他搜索栋号
	deviceType：IM/DS/AC等
	resultTable:搜索结果
*/
int IXS_SearchJsonDevTb(const char* net, cJSON* searchCtrl, const char* BD_Nbr, const char* deviceType, const cJSON* resultTable);

#endif


