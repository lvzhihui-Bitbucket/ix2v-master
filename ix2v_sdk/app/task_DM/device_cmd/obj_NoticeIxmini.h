/**
  ******************************************************************************
  * @file    obj_NoticeIxmini.h
  * @author  cao
  * @version V00.01.00
  * @date    2016.5.31
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 
	  //cao_20170304  整个文件

#ifndef _obj_NoticeIxmini_H
#define _obj_NoticeIxmini_H

// Define Object Property-------------------------------------------------------
#define NoticeIxminiUnlockMaxNum	10

#define ALL_FLOOR		0
#define PUBLIC_FLOOR	1
#define RPIVATE_FLOOR	2
#define CALLLIFT_FLOOR	3

typedef struct
{
	int		publicCnt;
	int		publicTime;
	int		publicRlcTime;
	char	publicFloor[NoticeIxminiUnlockMaxNum][2+1];
	int		privateCnt;
	int		privateTime;
	int		privateRlcTime;
	char	privateFloor[NoticeIxminiUnlockMaxNum][2+1];
	int		callliften;
	int		callliftCnt;
	int		callliftTime;
	int		callliftRlcTime;
	char		callliftFloor[NoticeIxminiUnlockMaxNum][2+1];
}LiftFloorCfg_T;

typedef struct
{
	char	floor[2+1];
	int		time;			//开锁时间
	int		rlcTime;		//rlc时间
}IxminiUnlockPara;

typedef struct
{
	int					cnt;
	IxminiUnlockPara	device[NoticeIxminiUnlockMaxNum];
}NoticeIxminiData;

#pragma pack(1)  //指定按1字节对齐
// GetIpBy Number 指令包结构
typedef struct
{
	int		rand;				//随机数
	int		sourceIp;			//源ip
	char	data[500+1];
}NoticeIxminiReq;

typedef struct
{
	int			rand;		//随机数
	int			result;
}NoticeIxminiRsp;

#pragma pack()



// Define Object Function - Public----------------------------------------------



// Define Object Function - Private---------------------------------------------



#endif


