/**
  ******************************************************************************
  * @file    obj_UnlockReport.h
  * @author  cao
  * @version V00.01.00
  * @date    2016.5.31
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 
	  //cao_20170304  整个文件

#ifndef _obj_UnlockReport_H
#define _obj_UnlockReport_H

// Define Object Property-------------------------------------------------------
#pragma pack(1)  //指定按1字节对齐

typedef struct
{
	int		rand;				//随机数
	int		sourceIp;			//源ip
	int		lock;				//锁
	int		state;				//状态
}UnlockReport_T;


#pragma pack()

#define LOCK_1				0
#define LOCK_2				1

#define UNLOCK_SUCESSFULL				0
#define UNLOCK_UNSUCESSFULL				1


// Define Object Function - Public----------------------------------------------
int API_UnlockReport(int ip, int lock, int state);
void ReceiveUnlockReport(UnlockReport_T* rspData);


// Define Object Function - Private---------------------------------------------



#endif


