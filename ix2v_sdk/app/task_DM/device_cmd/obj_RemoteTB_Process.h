/**
  ******************************************************************************
  * @file    obj_RemoteTB_Process.h
  * @author  cao
  * @version V00.01.00
  * @date    2023.3.13
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 

#ifndef obj_RemoteTB_Process
#define obj_RemoteTB_Process



// Define Object Property-------------------------------------------------------



// Define Object Function - Public----------------------------------------------

//远程读取表数量
int API_RemoteTableCount(int ip, char* TB_name, cJSON* where);

//远程删除表
int API_RemoteTableDelete(int ip, char* TB_name, cJSON* where);

//远程增加表记录
int API_RemoteTableAdd(int ip, char* TB_name, cJSON* record);

//远程更新表记录
int API_RemoteTableUpdate(int ip, char* TB_name, cJSON* where, cJSON* record);

//远程读表记录
int API_RemoteTableSelect(int ip, char* TB_name, cJSON* view, cJSON* where, int sort);

//远程读取表KEY值
cJSON* API_RemoteTableKey(int ip, char* TB_name);

//远程读取表新记录
cJSON* API_RemoteTableNewRecord(int ip, char* TB_name);

// Define Object Function - Private---------------------------------------------



#endif


