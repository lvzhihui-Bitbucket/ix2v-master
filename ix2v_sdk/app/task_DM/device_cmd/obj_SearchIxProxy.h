/**
  ******************************************************************************
  * @file    obj_SearchIxProxy.h
  * @author  cao
  * @version V00.01.00
  * @date    2016.5.31
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 
	  //cao_20170304  整个文件

#ifndef _obj_SearchIxProxy_H
#define _obj_SearchIxProxy_H

#include <stdio.h>
#include <sys/sysinfo.h>
#include <time.h>
#include "obj_PublicCmdDefine.h"
#include "obj_GetDeviceTypeAndArea.h"

#define IX_BUILDER_NAME_LEN						33

// Define Object Property-------------------------------------------------------
#include "task_IxProxy.h"

#pragma pack(1)  //指定按1字节对齐

typedef struct
{
	int					deviceCnt;		//回应计数
	IxProxyData_T		data[10];		//设备信息
} SearchIxProxyRspData;

typedef struct
{
	pthread_mutex_t 		lock;
	int						waitFlag;			//等待回应标记
	int						rand;				//随机数
	int						getDeviceCnt;		//等待接收几个
	SearchIxProxyRspData 	data;
} SearchIxProxyRun;

typedef struct
{
	int				rand;				//随机数
	int				sourceIp;			//源ip
	char 			data[500];
} SearchIxProxyReq;

typedef struct
{
	int					rand;				//随机数
	int					sourceIp;			//源ip
	char  				data[500];			//设备信息
} SearchIxProxyRsp;

#pragma pack()



// Define Object Function - Public----------------------------------------------



// Define Object Function - Private---------------------------------------------


#endif


