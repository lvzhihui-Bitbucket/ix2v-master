/**
  ******************************************************************************
  * @file    obj_NoticeIxmini.c
  * @author  czb
  * @version V00.01.00
  * @date    2016.5.31
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 
  
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <memory.h>
#include "obj_NoticeIxmini.h"
#include "obj_SYS_VER_INFO.h"
#include "vtk_udp_stack_device_update.h"
#include "cJSON.h"

#include "obj_GetIpByNumber.h"
#include "obj_IoInterface.h"


char* CreateLiftFloorCfgJsonData(LiftFloorCfg_T liftFloorCfg)
{
	char *pFloor[NoticeIxminiUnlockMaxNum];
	int i;
	
    cJSON *root = NULL;
	char *string = NULL;
	
	const cJSON *public = NULL;
	const cJSON *private = NULL;
	cJSON *calllift=NULL;
	cJSON *callliftFloor=NULL;
	const cJSON *time = NULL;
	const cJSON *rlcTime = NULL;
	const cJSON *publicFloor = NULL;
	const cJSON *privateFloor = NULL;
	const cJSON *floor = NULL;
	
    root = cJSON_CreateObject();
	
	
	public = cJSON_CreateObject();
	private = cJSON_CreateObject();
	calllift=cJSON_CreateObject();
	
	cJSON_AddItemToObject(root, "public", public);	
	cJSON_AddItemToObject(root, "private", private);
	cJSON_AddItemToObject(root,"call lift",calllift);

	cJSON_AddNumberToObject(public, "time", liftFloorCfg.publicTime);
	cJSON_AddNumberToObject(public, "rlcTime", liftFloorCfg.publicRlcTime);

	cJSON_AddNumberToObject(private, "time", liftFloorCfg.privateTime);
	cJSON_AddNumberToObject(private, "rlcTime", liftFloorCfg.privateRlcTime);

	cJSON_AddNumberToObject(calllift, "en", liftFloorCfg.callliften);
	cJSON_AddNumberToObject(calllift, "time", liftFloorCfg.callliftTime);
	cJSON_AddNumberToObject(calllift, "rlcTime", liftFloorCfg.callliftRlcTime);

	for(i = 0; i < liftFloorCfg.publicCnt; i++)
	{
		pFloor[i] = liftFloorCfg.publicFloor[i];
	}
	publicFloor = cJSON_CreateStringArray(pFloor, liftFloorCfg.publicCnt);
	
	for(i = 0; i < liftFloorCfg.privateCnt; i++)
	{
		pFloor[i] = liftFloorCfg.privateFloor[i];
	}
	privateFloor = cJSON_CreateStringArray(pFloor, liftFloorCfg.privateCnt);

	if(liftFloorCfg.callliftCnt>0)
	{
		for(i = 0; i < liftFloorCfg.callliftCnt; i++)
		{
			pFloor[i] = liftFloorCfg.callliftFloor[i];
		}
		callliftFloor= cJSON_CreateStringArray(pFloor, liftFloorCfg.callliftCnt);
	}
	cJSON_AddItemToObject(private, "floor", privateFloor);
	cJSON_AddItemToObject(public, "floor", publicFloor);
	if(liftFloorCfg.callliftCnt>0)
		cJSON_AddItemToObject(calllift, "floor", callliftFloor);

	string = cJSON_Print(root);

	cJSON_Delete(root);

	printf("CreateLiftFloorCfgJsonData = %s\n", string);
	
	return string;
}

int ParseLiftFloorCfgJsonData(const char* json, LiftFloorCfg_T* liftFloorCfg)
{
	int status = 0;
	int i;
	
	const cJSON *public = NULL;
	const cJSON *private = NULL;
		  cJSON *calllift = NULL;
		  cJSON *callliftFloor=NULL;
	const cJSON *time = NULL;
	const cJSON *rlcTime = NULL;
	const cJSON *publicFloor = NULL;
	const cJSON *privateFloor = NULL;
	const cJSON *floor = NULL;
	
	
	memset(liftFloorCfg, 0, sizeof(LiftFloorCfg_T));

	/* 创建一个用于解析的 cJSON 结构 */
	cJSON *root = cJSON_Parse(json);
	if (root == NULL)
	{
		dprintf("Json error!!! : %s\n", json);
		goto end;
	}

	public = cJSON_GetObjectItem( root, "public");
		
	GetJsonDataPro(public, "time", &liftFloorCfg->publicTime);
	GetJsonDataPro(public, "rlcTime", &liftFloorCfg->publicRlcTime);
	publicFloor = cJSON_GetObjectItem( public, "floor");
	
	if(cJSON_IsArray(publicFloor))
	{
		liftFloorCfg->publicCnt = cJSON_GetArraySize(publicFloor);
		
		for(i = 0; i < liftFloorCfg->publicCnt; i++)
		{
			floor = cJSON_GetArrayItem(publicFloor, i);
		
			if (cJSON_IsString(floor) && (floor->valuestring != NULL))
			{
				strncpy(liftFloorCfg->publicFloor[i] , floor->valuestring, 2);
				liftFloorCfg->publicFloor[i][2] = 0;
			}
			else
			{
				liftFloorCfg->publicFloor[i][0] = 0;
			}
		}
	}

	private = cJSON_GetObjectItem( root, "private");
	
	GetJsonDataPro(private, "time", &liftFloorCfg->privateTime);
	GetJsonDataPro(private, "rlcTime", &liftFloorCfg->privateRlcTime);
	privateFloor = cJSON_GetObjectItem( private, "floor");
	
	if(cJSON_IsArray(privateFloor))
	{
		liftFloorCfg->privateCnt = cJSON_GetArraySize(privateFloor);
		
		for(i = 0; i < liftFloorCfg->privateCnt; i++)
		{
			floor = cJSON_GetArrayItem(privateFloor, i);
		
			if (cJSON_IsString(floor) && (floor->valuestring != NULL))
			{
				strncpy(liftFloorCfg->privateFloor[i] , floor->valuestring, 2);
				liftFloorCfg->privateFloor[i][2] = 0;
			}
			else
			{
				liftFloorCfg->privateFloor[i][0] = 0;
			}
		}
	}
	calllift = cJSON_GetObjectItem( root, "call lift");
	if(calllift!=NULL)
	{
		GetJsonDataPro(calllift, "en", &liftFloorCfg->callliften);
		GetJsonDataPro(calllift, "time", &liftFloorCfg->callliftTime);
		GetJsonDataPro(calllift, "rlcTime", &liftFloorCfg->callliftRlcTime);
		callliftFloor= cJSON_GetObjectItem( calllift, "floor");
		if(callliftFloor!=NULL)
		{
			liftFloorCfg->callliftCnt= cJSON_GetArraySize(callliftFloor);
		
			for(i = 0; i < liftFloorCfg->callliftCnt; i++)
			{
				floor = cJSON_GetArrayItem(callliftFloor, i);
			
				if (cJSON_IsString(floor) && (floor->valuestring != NULL))
				{
					strncpy(liftFloorCfg->callliftFloor[i] , floor->valuestring, 2);
					liftFloorCfg->callliftFloor[i][2] = 0;
				}
				else
				{
					liftFloorCfg->callliftFloor[i][0] = 0;
				}
			}
		}
		else
		{
			liftFloorCfg->callliftCnt=0;
		}
	}
	else
	{
		liftFloorCfg->callliften=0;
		liftFloorCfg->callliftCnt=0;
	}
	
	status = 1;

	
	end:
	
	cJSON_Delete(root);
	
	return status;
}

static int ParseLiftFloorCfgJsonData2(const char* json, int floorType, NoticeIxminiData* pData)
{
	int status = 0;
	int i,j;
	
	const cJSON *public = NULL;
	const cJSON *private = NULL;
	cJSON *calllift = NULL;
	cJSON *callliftFloor=NULL;
	const cJSON *time = NULL;
	const cJSON *rlcTime = NULL;
	const cJSON *publicFloor = NULL;
	const cJSON *privateFloor = NULL;
	const cJSON *floor = NULL;
	
	LiftFloorCfg_T liftFloorCfg;
	
	memset(pData, 0, sizeof(NoticeIxminiData));
	memset(&liftFloorCfg, 0, sizeof(liftFloorCfg));

	/* 创建一个用于解析的 cJSON 结构 */
	cJSON *root = cJSON_Parse(json);
	if (root == NULL)
	{
		dprintf("Json error!!! : %s\n", json);
		goto end;
	}

	public = cJSON_GetObjectItem( root, "public");
	
	GetJsonDataPro(public, "time", &liftFloorCfg.publicTime);
	GetJsonDataPro(public, "rlcTime", &liftFloorCfg.publicRlcTime);
	publicFloor = cJSON_GetObjectItem( public, "floor");
	
	if(cJSON_IsArray(publicFloor))
	{
		liftFloorCfg.publicCnt = cJSON_GetArraySize(publicFloor);
		
		for(i = 0; i < liftFloorCfg.publicCnt; i++)
		{
			floor = cJSON_GetArrayItem(publicFloor, i);
		
			if (cJSON_IsString(floor) && (floor->valuestring != NULL))
			{
				strncpy(liftFloorCfg.publicFloor[i] , floor->valuestring, 2);
				liftFloorCfg.publicFloor[i][2] = 0;
			}
			else
			{
				liftFloorCfg.privateFloor[i][0] = 0;
			}
		}
	}

	private = cJSON_GetObjectItem( root, "private");
	
	GetJsonDataPro(private, "time", &liftFloorCfg.privateTime);
	GetJsonDataPro(private, "rlcTime", &liftFloorCfg.privateRlcTime);
	privateFloor = cJSON_GetObjectItem( private, "floor");
	
	if(cJSON_IsArray(privateFloor))
	{
		liftFloorCfg.privateCnt = cJSON_GetArraySize(privateFloor);
		
		for(i = 0; i < liftFloorCfg.privateCnt; i++)
		{
			floor = cJSON_GetArrayItem(privateFloor, i);
		
			if (cJSON_IsString(floor) && (floor->valuestring != NULL))
			{
				strncpy(liftFloorCfg.privateFloor[i] , floor->valuestring, 2);
				liftFloorCfg.privateFloor[i][2] = 0;
			}
			else
			{
				liftFloorCfg.privateFloor[i][0] = 0;
			}
		}
	}
	calllift = cJSON_GetObjectItem( root, "call lift");
	if(calllift!=NULL)
	{
		GetJsonDataPro(calllift, "en", &liftFloorCfg.callliften);
		GetJsonDataPro(calllift, "time", &liftFloorCfg.callliftTime);
		GetJsonDataPro(calllift, "rlcTime", &liftFloorCfg.callliftRlcTime);
		callliftFloor= cJSON_GetObjectItem( calllift, "floor");
		if(callliftFloor!=NULL)
		{
			liftFloorCfg.callliftCnt= cJSON_GetArraySize(callliftFloor);
		
			for(i = 0; i < liftFloorCfg.callliftCnt; i++)
			{
				floor = cJSON_GetArrayItem(callliftFloor, i);
			
				if (cJSON_IsString(floor) && (floor->valuestring != NULL))
				{
					strncpy(liftFloorCfg.callliftFloor[i] , floor->valuestring, 2);
					liftFloorCfg.callliftFloor[i][2] = 0;
				}
				else
				{
					liftFloorCfg.callliftFloor[i][0] = 0;
				}
			}
		}
		else
		{
			liftFloorCfg.callliftCnt=0;
		}
	}
	else
	{
		liftFloorCfg.callliften=0;
		liftFloorCfg.callliftCnt=0;
	}
	status = 1;

	if(floorType == ALL_FLOOR)
	{
		pData->cnt = liftFloorCfg.privateCnt + liftFloorCfg.publicCnt+ liftFloorCfg.callliftCnt;
		for(i = 0,j=0; i < liftFloorCfg.publicCnt; i++)
		{
			pData->device[j].time = liftFloorCfg.publicTime;
			pData->device[j].rlcTime = liftFloorCfg.publicRlcTime;
			strcpy(pData->device[j].floor,	liftFloorCfg.publicFloor[i]);
			j++;
		}
		
		for(i = 0; i < liftFloorCfg.privateCnt; i++)
		{
			pData->device[j].time = liftFloorCfg.privateTime;
			pData->device[j].rlcTime = liftFloorCfg.privateRlcTime;
			strcpy(pData->device[j].floor,	liftFloorCfg.privateFloor[i]);
			j++;
		}
		for( i=0; i < liftFloorCfg.callliftCnt; i++)
		{
			pData->device[j].time = liftFloorCfg.callliftTime;
			pData->device[j].rlcTime = liftFloorCfg.callliftRlcTime;
			strcpy(pData->device[j].floor,	liftFloorCfg.callliftFloor[i]);
			j++;
		}
	}
	else if(floorType == PUBLIC_FLOOR)
	{
		pData->cnt = liftFloorCfg.publicCnt;
		for(i = 0; i < liftFloorCfg.publicCnt; i++)
		{
			pData->device[i].time = liftFloorCfg.publicTime;
			pData->device[i].rlcTime = liftFloorCfg.publicRlcTime;
			strcpy(pData->device[i].floor,	liftFloorCfg.publicFloor[i]);
		}
	}
	else if(floorType == RPIVATE_FLOOR)
	{
		pData->cnt = liftFloorCfg.privateCnt;
		for(i = 0; i < pData->cnt; i++)
		{
			pData->device[i].time = liftFloorCfg.privateTime;
			pData->device[i].rlcTime = liftFloorCfg.privateRlcTime;
			strcpy(pData->device[i].floor,	liftFloorCfg.privateFloor[i]);
		}
	}
	else if(floorType == CALLLIFT_FLOOR)
	{
		pData->cnt = liftFloorCfg.callliftCnt;
		for(i = 0; i < pData->cnt; i++)
		{
			pData->device[i].time = liftFloorCfg.callliftTime;
			pData->device[i].rlcTime = liftFloorCfg.callliftRlcTime;
			strcpy(pData->device[i].floor,	liftFloorCfg.callliftFloor[i]);
		}
	}
	else
	{
		pData->cnt = 0;
	}

	
	end:
	
	cJSON_Delete(root);
	
	return status;
}


int GetFloorNbr(int ip, char*deviceNbr, int floorType, NoticeIxminiData* floorNbr)
{
    int ret = 0;
	int enable = 0;
	char tempChar[500];
	
	if(ip == inet_addr(GetSysVerInfo_IP()))
	{
		//IX2V test enable = API_Para_Read_Int(LIFT_CTRL_ENABLE);
		if(enable)
		{
			//IX2V test ret = API_Para_Read_String(DEVICE_FLOOR, tempChar);
			if(ret == 0 && tempChar[0] != 0)
			{
				ParseLiftFloorCfgJsonData2(tempChar, floorType, floorNbr);
				/*
				dprintf("floorNbr: cnt=%d\n", floorNbr->cnt);
				for(ret = 0; ret < floorNbr->cnt; ret++)
				{
					dprintf("[%s, %d, %d]\n", floorNbr->device[ret].floor, floorNbr->device[ret].time, floorNbr->device[ret].rlcTime);
				}
				*/
			}
			else
			{
				floorNbr->cnt = 1;
				floorNbr->device[0].time = 0;
				floorNbr->device[0].rlcTime = 0;
				strncpy(floorNbr->device[0].floor, deviceNbr+4, 2);
			}
		}
	}
	else
	{
		//IX2V test if(API_Para_ReadReomteByID(ip, LIFT_CTRL_ENABLE, tempChar))
		{
			enable = atoi(tempChar);
			if(enable)
			{
				//IX2V test if(API_Para_ReadReomteByID(ip, DEVICE_FLOOR, tempChar) && tempChar[0] != 0)
				{
					ParseLiftFloorCfgJsonData2(tempChar, floorType, floorNbr);
				}
				//IX2V test else
				{
					floorNbr->cnt = 1;
					floorNbr->device[0].time = 0;
					floorNbr->device[0].rlcTime = 0;
					strncpy(floorNbr->device[0].floor, deviceNbr+4, 2);
				}
			}
		}
	}
	
	return enable;
}

static int ParseNoticeIxminiJsonData(const char* json, NoticeIxminiData* pData)
{
	int status = 0;
	
	const cJSON *devices = NULL;
	const cJSON *device = NULL;

	memset(pData, 0, sizeof(NoticeIxminiData));

	/* 创建一个用于解析的 cJSON 结构 */
	cJSON *root = cJSON_Parse(json);
	if (root == NULL)
	{
		dprintf("Json error!!! : %s\n", json);
		goto end;
	}

	devices = cJSON_GetObjectItem( root, "devices");
	
	if(cJSON_IsArray(devices))
	{
		int i;
		pData->cnt = cJSON_GetArraySize(devices);
		for(i = 0; i < pData->cnt; i++)
		{
			device = cJSON_GetArrayItem(devices, i);
			
			GetJsonDataPro(device, "floor", pData->device[i].floor);
			GetJsonDataPro(device, "time", &pData->device[i].time);
			GetJsonDataPro(device, "rlcTime", &pData->device[i].rlcTime);
		}
	}

	status = 1;

	end:
	
	cJSON_Delete(root);
	
	return status;
}

static char* CreateNoticeIxminiJsonData(NoticeIxminiData* data)
{
    cJSON *root = NULL;
	cJSON *devices = NULL;
	cJSON *device = NULL;
	char *string = NULL;
	int i, cnt;

    root = cJSON_CreateObject();
	
	cJSON_AddItemToObject(root, "devices", devices = cJSON_CreateArray());	

    for (i = 0, cnt = 0; i < NoticeIxminiUnlockMaxNum && cnt < data->cnt; i++, cnt++)
    {
		device = cJSON_CreateObject();
		
		cJSON_AddStringToObject(device, "floor", data->device[i].floor);
		cJSON_AddNumberToObject(device, "time", data->device[i].time);
		cJSON_AddNumberToObject(device, "rlcTime", data->device[i].rlcTime);
		
		cJSON_AddItemToArray(devices, device);
    }

	string = cJSON_Print(root);

	cJSON_Delete(root);
	
	return string;
}


void ReportIxminiUnlockProcess2(int ip, char* deviceNbr)
{
	NoticeIxminiData noticeIxminidata;
	GetIpRspData data;
	int i = 0;
	
	if(ip == 0)
	{
		memset(&data, 0, sizeof(data));
		if(API_GetIpNumberFromNet(deviceNbr, NULL, NULL, 2, 1, &data) == 0)
		{
			if(data.cnt > 0)
			{
				ip = data.Ip[0];
			}
		}
	}
	
	if(ip == 0)
	{
		return;
	}
		
	if(GetFloorNbr(ip, deviceNbr, RPIVATE_FLOOR, &noticeIxminidata))
	{
		GetIpRspData data = {0};
		char bd_rm_ms[11];

		snprintf(bd_rm_ms, 11, "%s920100", GetSysVerInfo_bd());

		API_GetIpNumberFromNet(bd_rm_ms, NULL, GetSysVerInfo_bd(), 2, 0, &data);
		for(i = 0; i < data.cnt; i++)
		{
			//printf("ReportIxminiUnlockProcess2 3333333333333 data.cnt=%d, data.Ip[%d]=%d\n", data.cnt, i, data.Ip[i]);
			API_NoticeIxmini(data.Ip[i], noticeIxminidata);
		}
	}
}

void ImReportIxminiUnlock(void)
{
	ReportIxminiUnlockProcess2(inet_addr(GetSysVerInfo_IP()), GetSysVerInfo_BdRmMs());
}


//发送电梯控制请求
int API_NoticeIxmini(int ip, NoticeIxminiData data)
{
	NoticeIxminiReq sendData;
	char *tempChar;
	int len;

	memset(&sendData, 0, sizeof(sendData));

	sendData.rand = MyRand() + MyRand2(ip);
	sendData.sourceIp = inet_addr(GetSysVerInfo_IP());
	
	tempChar = CreateNoticeIxminiJsonData(&data);
	
	if(tempChar != NULL)
	{
		strncpy(sendData.data, tempChar, 500);
		free(tempChar);
	}
	
	len = sizeof(sendData) - 500 + strlen(sendData.data);
	
	api_udp_device_update_send_data(ip, htons(NOTICE_IXMINI_REQ), (char*)&sendData, len);

	return 0;
}

//发送电梯控制应答
int API_NoticeIxminiRsp(int ip, int rand, int result)
{
	NoticeIxminiRsp sendData;
	char *tempChar;
	int len;

	memset(&sendData, 0, sizeof(sendData));

	sendData.rand = rand;
	sendData.result = result;
	len = sizeof(sendData);
	
	api_udp_device_update_send_data(ip, htons(NOTICE_IXMINI_RSP), (char*)&sendData, len);

	return 0;
}

//接收到获取信息应答指令
void ReceiveNoticeIxminiRsp(NoticeIxminiRsp* rspData)
{
	
}


//接收到InformUnlock
void ReceiveNoticeIxminiReq(NoticeIxminiReq* pReadReq)
{

}

int GetCallliftEnable(void)
{
	char tempChar[500];
	int ret;
	int rev=0;
	;
	//IX2V test if(API_Para_ReadByID_String(DEVICE_FLOOR, tempChar) && tempChar[0])
	{
		cJSON *root = cJSON_Parse(tempChar);
		if (root != NULL)
		{
			cJSON *calllift = cJSON_GetObjectItem( root, "call lift");
			if(calllift!=NULL)
			{
				GetJsonDataPro(calllift, "en", &rev);
			}
			cJSON_Delete(root);
		}
	}
	return rev;
}
int CallLiftProcess(void)
{
	int i;
	NoticeIxminiData noticeIxminidata;
	int ip = inet_addr(GetSysVerInfo_IP());
	int rev=-1;
	if(GetFloorNbr(ip, GetSysVerInfo_BdRmMs(), CALLLIFT_FLOOR, &noticeIxminidata))
	{
		GetIpRspData data = {0};
		char bd_rm_ms[11];

		snprintf(bd_rm_ms, 11, "%s920100", GetSysVerInfo_bd());

		API_GetIpNumberFromNet(bd_rm_ms, NULL, GetSysVerInfo_bd(), 2, 0, &data);
		for(i = 0; i < data.cnt; i++)
		{
			printf("ReportIxminiUnlockProcess2 3333333333333 data.cnt=%d, data.Ip[%d]=%d\n", data.cnt, i, data.Ip[i]);
			API_NoticeIxmini(data.Ip[i], noticeIxminidata);
			rev=0;
		}
	}
	return rev;
}
/*********************************************************************************************************
**  End Of File
*********************************************************************************************************/

