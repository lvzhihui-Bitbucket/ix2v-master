/**
  ******************************************************************************
  * @file    obj_CmdResDownload.h
  * @author  cao
  * @version V00.01.00
  * @date    2016.5.31
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 
	  //cao_20170304  整个文件

#ifndef _obj_CmdResDownload_H
#define _obj_CmdResDownload_H

// Define Object Property-------------------------------------------------------


typedef struct
{
	char	IP_Addr[15+1];			//IP地址
	char	DeviceNbr[10+1];		//房号
	char	MFG_SN[12+1];			//MFG_SN

	char	OPERATE[20+1];			//操作
	char	SERVER[20+1];			//下载服务器
	char	CODE[6+1];				//下载码
	int 	reportIp;
}RES_DOWNLOAD_REQ_DATA_T;

typedef struct
{
	char	DeviceNbr[10+1];		//房号
	char	IP_Addr[15+1];			//IP地址
	char	MFG_SN[12+1];			//MFG_SN
	char	OPERATE[20+1];			//操作
	char	Result[5+1];			//发送结果
}RES_DOWNLOAD_RSP_DATA_T;


// Define Object Function - Public----------------------------------------------



// Define Object Function - Private---------------------------------------------



#endif


