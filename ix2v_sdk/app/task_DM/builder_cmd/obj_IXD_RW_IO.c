/**
  ******************************************************************************
  * @file    obj_CmdShell.c
  * @author  czb
  * @version V00.01.00
  * @date    2016.5.31
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 
  
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <memory.h>
#include <dirent.h>
#include <string.h>
#include <sys/stat.h>
#include "task_IxProxy.h"
#include "tcp_server_process.h"
#include "cJSON.h"
#include "vtk_udp_stack_device_update.h"
#include "obj_PublicUnicastCmd.h"

void IXD_RwIoProcess(char* pData, int len)
{
	IX_PROXY_TCP_HEAD* phead = (IX_PROXY_TCP_HEAD*)pData;
	char* cmdData = (char*)(pData + sizeof(IX_PROXY_TCP_HEAD));	
	PublicUnicastCmdData dataIn;
	PublicUnicastCmdData dataOut;
	
	cJSON *reqJson = NULL;
	char *strIp, *strSn;
	int result;

	SetIxProxyRSP_ID(1);
	if(GetIxProxyState() == IX_PROXY_BUSY)
	{
	    reqJson = cJSON_Parse(cmdData);

		strIp = cJSON_GetStringValue(cJSON_GetObjectItem(reqJson, DM_KEY_IP_Addr));
		strSn = cJSON_GetStringValue(cJSON_GetObjectItem(reqJson, IX2V_MFG_SN));
		dataIn.data = cmdData;
		dataIn.len = strlen(cmdData) + 1;
		dataOut.data = NULL;
		dataOut.len = 0;

		result = PublicUnicastCmdRequst(inet_addr(strIp), strSn, IxProxyWaitUdpTime, GET_RW_IO_BY_IP_REQ, dataIn, &dataOut);
		if(dataOut.data == NULL)
		{
			cJSON *rspJson = cJSON_Duplicate(reqJson, 1);
			cJSON_AddStringToObject(reqJson, IX2V_Result, (result ? RESULT_ERR02 : RESULT_ERR10));
			dataOut.data = cJSON_PrintUnformatted(rspJson);
			cJSON_Delete(rspJson);
		}
		cJSON_Delete(reqJson);

		IxProxyRespone(RSP_READ_WRITE_IO, ntohs(phead->Session_ID), IncreaseIxProxyRSP_ID(), dataOut.data);
		dataOut.data = NULL;
	}
}

/*********************************************************************************************************
**  End Of File
*********************************************************************************************************/

