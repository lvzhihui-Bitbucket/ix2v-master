/**
  ******************************************************************************
  * @file    obj_CmdGetVersion.h
  * @author  cao
  * @version V00.01.00
  * @date    2016.5.31
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 
	  //cao_20170304  整个文件

#ifndef _obj_CmdGetVersion_H
#define _obj_CmdGetVersion_H

// Define Object Property-------------------------------------------------------
#include "task_IxProxy.h"
#include "obj_IX_Report.h"

typedef struct
{
	int  cnt;
	char MFG_SN[256][12+1];			//IP地址
}VERSION_REQ_T;

typedef struct
{
	char			Result[5+1];		//BD_RM_MS
	char			MFG_SN[12+1];		//MFG_SN
	char			BD_RM_MS[10+1];		//BD_RM_MS
	char			IP_Addr[15+1];		//IP_Addr
	FW_VER_VERIFY_T ver;
}VERSION_RSP_T;


// Define Object Function - Public----------------------------------------------
char* CreateGetVersionRspJsonData(VERSION_RSP_T* data);


// Define Object Function - Private---------------------------------------------



#endif


