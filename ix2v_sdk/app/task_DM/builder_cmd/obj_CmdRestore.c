/**
  ******************************************************************************
  * @file    obj_CmdRestore.c
  * @author  czb
  * @version V00.01.00
  * @date    2016.5.31
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 
  
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <memory.h>
#include "cJSON.h"
#include "obj_CmdRestore.h"
#include "task_IxProxy.h"
#include "tcp_server_process.h"
#include "vtk_udp_stack_device_update.h"
#include "obj_PublicUdpCmd.h"

#define IxProxyRestoreWaitTime			15

static int ParseRestoreReqJsonData(const char* json, CMD_RESTORE_MUL_REQ_T* pData)
{
    int status = 0;
	int i;
	cJSON *pSub;
		
	memset(pData, 0, sizeof(CMD_RESTORE_MUL_REQ_T));

    /* 创建一个用于解析的 cJSON 结构 */
    cJSON *root = cJSON_Parse(json);
    if (root == NULL)
    {
        dprintf("---------------------------------json Error.\n%s\n", json);
        status = 0;
        goto end;
    }
	
	pData->cnt = cJSON_GetArraySize (root);
	
	for(i = 0 ; i < pData->cnt; i++)
	{
		pSub = cJSON_GetArrayItem(root, i);
		GetJsonDataPro(pSub, DM_KEY_DeviceNbr, pData->data[i].DeviceNbr);
		GetJsonDataPro(pSub, DM_KEY_IP_Addr, pData->data[i].IP_Addr);
		GetJsonDataPro(pSub, DM_KEY_MFG_SN, pData->data[i].MFG_SN);
		GetJsonDataPro(pSub, DM_KEY_BAK_SELECT, &pData->data[i].bakSelect);
	}

    status = 1;

end:
	
    cJSON_Delete(root);
	
    return status;
}

char* CreateRestoreRspJsonData(CMD_RESTORE_ONE_RSP_T* pData)
{
    cJSON *root = NULL;
	char *string = NULL;


    root = cJSON_CreateObject();
	
	cJSON_AddStringToObject(root, DM_KEY_DeviceNbr, pData->DeviceNbr);
	cJSON_AddStringToObject(root, DM_KEY_IP_Addr, pData->IP_Addr);
	cJSON_AddStringToObject(root, DM_KEY_MFG_SN, pData->MFG_SN);
	cJSON_AddNumberToObject(root, DM_KEY_BAK_SELECT, pData->bakSelect);
	cJSON_AddStringToObject(root, DM_KEY_Result, pData->Result);

	string = cJSON_Print(root);

	cJSON_Delete(root);

	return string;
}

static int LocalRestoreProcess(void* data)
{
	CMD_RESTORE_ONE_REQ_T *req = (CMD_RESTORE_ONE_REQ_T *)data;
	
	//IX2V test return RestoreBak(req->bakSelect);
}

static char* CreateRestoreUdpReqJsonData(void* data)
{
	CMD_RESTORE_ONE_REQ_T *req = (CMD_RESTORE_ONE_REQ_T *)data;
    cJSON *root = NULL;
	char *string = NULL;

    root = cJSON_CreateObject();
	
	cJSON_AddNumberToObject(root, DM_KEY_BAK_SELECT, req->bakSelect);

	string = cJSON_Print(root);

	cJSON_Delete(root);

	return string;
}


void IxProxyRestoreProcess(char* pData, int len)
{
	IX_PROXY_TCP_HEAD* phead = (IX_PROXY_TCP_HEAD*)pData;
	char* cmdData = (char*)(pData + sizeof(IX_PROXY_TCP_HEAD));	
	CMD_RESTORE_ONE_REQ_T reqObj;
	CMD_RESTORE_ONE_RSP_T rspObj;
	
	CMD_RESTORE_MUL_REQ_T req;
	DeviceInfo deviceInfo;
	
	int i, j, ipAddr;
	
	SetIxProxyRSP_ID(1);

	ParseRestoreReqJsonData(cmdData, &req);

	for(j = 0; j < req.cnt; j++)
	{
		reqObj = req.data[j];
		
		//By IP_Addr
		if(reqObj.IP_Addr[0] != 0)
		{
			ipAddr = inet_addr(reqObj.IP_Addr);

			if(PublicUdpCmdReqProcess(ipAddr, IxProxyRestoreWaitTime, Restore_REQ, LocalRestoreProcess, CreateRestoreUdpReqJsonData, &reqObj) == 0)
			{
				if(ipAddr == inet_addr(GetMyProxy_IP()))
				{
					strcpy(rspObj.DeviceNbr, GetSysVerInfo_BdRmMs());
					strcpy(rspObj.IP_Addr, GetMyProxy_IP());
					strcpy(rspObj.MFG_SN, GetSysVerInfo_Sn());
				}
				else
				{
					API_GetInfoByIp(ipAddr, IxProxyWaitUdpTime, &deviceInfo);
					strcpy(rspObj.DeviceNbr, deviceInfo.BD_RM_MS);
					strcpy(rspObj.IP_Addr, deviceInfo.IP_ADDR);
					strcpy(rspObj.MFG_SN, deviceInfo.MFG_SN);
				}
				rspObj.bakSelect = reqObj.bakSelect;
				strncpy(rspObj.Result, RESULT_SUCC, 6);
			}
			else
			{
				memset(&rspObj, 0, sizeof(rspObj));
				rspObj.bakSelect = reqObj.bakSelect;
				strncpy(rspObj.Result, RESULT_ERR02, 6);
			}

			IxProxyRespone(RSP_DM_RESTORE, ntohs(phead->Session_ID), IncreaseIxProxyRSP_ID(), (void*)&rspObj);
		}
		//By Device_Addr：可能有很多重复的
		else if(reqObj.DeviceNbr[0] != 0)
		{
			GetIpRspData data = {0};

			//本机
			if(!strcmp(reqObj.DeviceNbr, GetSysVerInfo_BdRmMs()))
			{
				if(PublicUdpCmdReqProcess(inet_addr(GetMyProxy_IP()), IxProxyRestoreWaitTime, Restore_REQ, LocalRestoreProcess, CreateRestoreUdpReqJsonData, &reqObj) == 0)
				{
					strncpy(rspObj.Result, RESULT_SUCC, 6);
					strcpy(rspObj.DeviceNbr, GetSysVerInfo_BdRmMs());
					strcpy(rspObj.IP_Addr, GetMyProxy_IP());
					strcpy(rspObj.MFG_SN, GetSysVerInfo_Sn());
					rspObj.bakSelect = reqObj.bakSelect;
					
					IxProxyRespone(RSP_DM_RESTORE, ntohs(phead->Session_ID), IncreaseIxProxyRSP_ID(), (void*)&rspObj);
				}
			}
			
			//其他机器
			if(API_GetIpNumberFromNet(reqObj.DeviceNbr, NULL, NULL, IxProxyWaitUdpTime, MAX_DEVICE, &data) == 0)
			{
				for(i = 0; i < data.cnt && GetIxProxyState() == IX_PROXY_BUSY; i++)
				{
					if(PublicUdpCmdReqProcess(data.Ip[i], IxProxyRestoreWaitTime, Restore_REQ, LocalRestoreProcess, CreateRestoreUdpReqJsonData, &reqObj) == 0)
					{
						API_GetInfoByIp(data.Ip[i], IxProxyWaitUdpTime, &deviceInfo);

						strcpy(rspObj.DeviceNbr, deviceInfo.BD_RM_MS);
						strcpy(rspObj.IP_Addr, deviceInfo.IP_ADDR);
						strcpy(rspObj.MFG_SN, deviceInfo.MFG_SN);
						
						rspObj.bakSelect = reqObj.bakSelect;
						strncpy(rspObj.Result, RESULT_SUCC, 6);
						IxProxyRespone(RSP_DM_RESTORE, ntohs(phead->Session_ID), IncreaseIxProxyRSP_ID(), (void*)&rspObj);
					}
				}
			}
			else
			{
				memset(&rspObj, 0, sizeof(rspObj));
				rspObj.bakSelect = reqObj.bakSelect;
				strncpy(rspObj.Result, RESULT_ERR02, 6);
				IxProxyRespone(RSP_DM_RESTORE, ntohs(phead->Session_ID), IncreaseIxProxyRSP_ID(), (void*)&rspObj);
			}
		}

		//By MFG_SN
		else if(reqObj.MFG_SN[0] != 0)
		{
			//本机
			if(!strcmp(reqObj.MFG_SN, GetSysVerInfo_Sn()))
			{
				if(PublicUdpCmdReqProcess(inet_addr(GetMyProxy_IP()), IxProxyRestoreWaitTime, Restore_REQ, LocalRestoreProcess, CreateRestoreUdpReqJsonData, &reqObj) == 0)
				{
					strcpy(rspObj.DeviceNbr, GetSysVerInfo_BdRmMs());
					strcpy(rspObj.IP_Addr, GetMyProxy_IP());
					strcpy(rspObj.MFG_SN, GetSysVerInfo_Sn());

					rspObj.bakSelect = reqObj.bakSelect;
					strncpy(rspObj.Result, RESULT_SUCC, 6);
					IxProxyRespone(RSP_DM_RESTORE, ntohs(phead->Session_ID), IncreaseIxProxyRSP_ID(), (void*)&rspObj);
				}
			}
			//其他机器
			else
			{
				if(API_GetIpByMFG_SN(reqObj.MFG_SN, IxProxyWaitUdpTime, &ipAddr, NULL) == 0)
				{
					API_GetInfoByIp(ipAddr, IxProxyWaitUdpTime, &deviceInfo);

					strcpy(rspObj.DeviceNbr, deviceInfo.BD_RM_MS);
					strcpy(rspObj.IP_Addr, deviceInfo.IP_ADDR);
					strcpy(rspObj.MFG_SN, deviceInfo.MFG_SN);
					rspObj.bakSelect = reqObj.bakSelect;

					if(PublicUdpCmdReqProcess(ipAddr, IxProxyRestoreWaitTime, Restore_REQ, LocalRestoreProcess, CreateRestoreUdpReqJsonData, &reqObj) == 0)
					{
						strncpy(rspObj.Result, RESULT_SUCC, 6);
					}
					else
					{
						strncpy(rspObj.Result, RESULT_ERR02, 6);
					}
				}
				else
				{
					memset(&rspObj, 0, sizeof(rspObj));
					rspObj.bakSelect = reqObj.bakSelect;
					strncpy(rspObj.Result, RESULT_ERR02, 6);
				}
				IxProxyRespone(RSP_DM_RESTORE, ntohs(phead->Session_ID), IncreaseIxProxyRSP_ID(), (void*)&rspObj);
			}
		}
	}
	IxProxyRespone(RSP_DM_RESTORE, ntohs(phead->Session_ID), 0, NULL);
}


/*********************************************************************************************************
**  End Of File
*********************************************************************************************************/

