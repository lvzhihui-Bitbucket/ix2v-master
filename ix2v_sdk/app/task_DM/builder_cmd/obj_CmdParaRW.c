/**
  ******************************************************************************
  * @file    obj_CmdReboot.c
  * @author  czb
  * @version V00.01.00
  * @date    2016.5.31
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 
  
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <memory.h>
#include "obj_CmdGetAbout.h"
#include "task_IxProxy.h"
#include "tcp_server_process.h"
#include "cJSON.h"
#include "obj_CmdParaRW.h"


static int ParseParameterReqJsonData(const char* json, PARAMETER_REQ_DATA_T* pData)
{
    int status = 0;
	int iCnt, deviceNum;
	
    const cJSON *pSub = NULL;
    const cJSON *Parameter = NULL;

	memset(pData, 0, sizeof(PARAMETER_REQ_DATA_T));

    /* 创建一个用于解析的 cJSON 结构 */
    cJSON *root = cJSON_Parse(json);
    if (root == NULL)
    {
        dprintf("Json error!!!: %s\n", json);
        status = 0;
        goto end;
    }
	
	GetJsonDataPro(root, "DeviceNbr", pData->DeviceNbr);
	GetJsonDataPro(root, "IP_Addr", pData->IP_Addr);
	
	Parameter = cJSON_GetObjectItemCaseSensitive( root, "Parameter");
	if(Parameter != NULL)
	{
		pData->cnt = cJSON_GetArraySize ( Parameter );
		
		for( iCnt = 0 ; iCnt < pData->cnt; iCnt ++ )
		{
			pSub = cJSON_GetArrayItem(Parameter, iCnt);
			
			if(NULL == pSub ){ continue ; }
			
			GetJsonDataPro(pSub, "PARA_ID", pData->Parameter[iCnt].PARA_ID);
			GetJsonDataPro(pSub, "VALUE", pData->Parameter[iCnt].VALUE);
		}
	}

    status = 1;

end:
	
    cJSON_Delete(root);
	
    return status;
}

char* CreateParameterRspJsonData(PARAMETER_RSP_DATA_T* data)
{
    cJSON *root = NULL;
	char *string = NULL;

    root = cJSON_CreateObject();

	cJSON_AddStringToObject(root, "Result", data->Result);
	cJSON_AddStringToObject(root, "IP_Addr", data->IP_Addr);
	cJSON_AddStringToObject(root, "DeviceNbr", data->DeviceNbr);
	cJSON_AddStringToObject(root, "PARA_ID", data->PARA_ID);
	cJSON_AddStringToObject(root, "VALUE", data->VALUE);

	string = cJSON_Print(root);

	cJSON_Delete(root);

	return string;
}

char* CreateParameterDefaultRspJsonData(PARAMETER_RSP_DATA_T* data)
{
    cJSON *root = NULL;
	char *string = NULL;

    root = cJSON_CreateObject();

	cJSON_AddStringToObject(root, "Result", data->Result);
	cJSON_AddStringToObject(root, "IP_Addr", data->IP_Addr);
	cJSON_AddStringToObject(root, "DeviceNbr", data->DeviceNbr);

	string = cJSON_Print(root);

	cJSON_Delete(root);

	return string;
}

void IxProxyParameterReadProcess(char* pData, int len)
{
	IX_PROXY_TCP_HEAD* phead = (IX_PROXY_TCP_HEAD*)pData;
	char* cmdData = (char*)(pData + sizeof(IX_PROXY_TCP_HEAD));	
	PARAMETER_REQ_DATA_T cmdDataObj;
	PARAMETER_RSP_DATA_T rspData;
	int i;
	
	char sendData[CLIENT_CONNECT_BUF_MAX];

	SetIxProxyRSP_ID(1);
	
	ParseParameterReqJsonData(cmdData, &cmdDataObj);

	memset(&rspData, 0, sizeof(rspData));
	//读取本机参数
	if(inet_addr(cmdDataObj.IP_Addr) == inet_addr(GetMyProxy_IP()) )
	{
		//房号匹配
		if(!strcmp(cmdDataObj.DeviceNbr, GetSysVerInfo_BdRmMs()))
		{
			for(i = 0; i < cmdDataObj.cnt && GetIxProxyState() == IX_PROXY_BUSY; i++)
			{
				memset(&rspData, 0, sizeof(rspData));
				//读取本机参数正确
				if(API_OldPara_ReadByID(cmdDataObj.Parameter[i].PARA_ID, cmdDataObj.Parameter[i].VALUE))
				{
					strncpy(rspData.Result, RESULT_SUCC, 6);
					strncpy(rspData.PARA_ID, cmdDataObj.Parameter[i].PARA_ID, 5);
					strncpy(rspData.VALUE, cmdDataObj.Parameter[i].VALUE, IO_DATA_LENGTH+1);
				}
				//读取本机参数错误
				else
				{
					strncpy(rspData.Result, RESULT_ERR10, 6);
					strcpy(rspData.PARA_ID, cmdDataObj.Parameter[i].PARA_ID);
				}

				strcpy(rspData.IP_Addr, GetMyProxy_IP());
				strcpy(rspData.DeviceNbr, GetSysVerInfo_BdRmMs());
				
				IxProxyRespone(RSP_DM_PARA_READ, ntohs(phead->Session_ID), IncreaseIxProxyRSP_ID(), (void*)&rspData);		
			}
		}
		//房号不匹配
		else
		{
			strncpy(rspData.Result, RESULT_ERR01, 6);
			strcpy(rspData.IP_Addr, GetMyProxy_IP());
			strcpy(rspData.DeviceNbr, GetSysVerInfo_BdRmMs());
			IxProxyRespone(RSP_DM_PARA_READ, ntohs(phead->Session_ID), IncreaseIxProxyRSP_ID(), (void*)&rspData);
		}
	}
	//读取其他机器参数
	else
	{
		DeviceInfo info;
		memset(&info, 0, sizeof(info));
		
		//读取机器在线
		if(API_GetInfoByIp(inet_addr(cmdDataObj.IP_Addr), 1, &info) == 0)
		{
			//房号匹配
			if(!strcmp(cmdDataObj.DeviceNbr, info.BD_RM_MS))
			{
				for(i = 0; i < cmdDataObj.cnt && GetIxProxyState() == IX_PROXY_BUSY; i++)
				{
					memset(&rspData, 0, sizeof(rspData));
				
					//读取远程设备参数
					if(API_Para_ReadReomteByID(inet_addr(cmdDataObj.IP_Addr), cmdDataObj.Parameter[i].PARA_ID, cmdDataObj.Parameter[i].VALUE))
					{
						strncpy(rspData.Result, RESULT_SUCC, 6);
						strcpy(rspData.PARA_ID, cmdDataObj.Parameter[i].PARA_ID);
						strcpy(rspData.VALUE, cmdDataObj.Parameter[i].VALUE);
					}
					else
					{
						strncpy(rspData.Result, RESULT_ERR10, 6);
						strcpy(rspData.PARA_ID, cmdDataObj.Parameter[i].PARA_ID);
					}
					
					strcpy(rspData.IP_Addr, cmdDataObj.IP_Addr);
					strcpy(rspData.DeviceNbr, cmdDataObj.DeviceNbr);
					
					IxProxyRespone(RSP_DM_PARA_READ, ntohs(phead->Session_ID), IncreaseIxProxyRSP_ID(), (void*)&rspData);		
				}
			}
			//房号不匹配
			else
			{
				strncpy(rspData.Result, RESULT_ERR01, 6);
				strcpy(rspData.IP_Addr, cmdDataObj.IP_Addr);
				strcpy(rspData.DeviceNbr, info.BD_RM_MS);
				IxProxyRespone(RSP_DM_PARA_READ, ntohs(phead->Session_ID), IncreaseIxProxyRSP_ID(), (void*)&rspData);
			}
		}
		//读取机器不在线
		else
		{
			strncpy(rspData.Result, RESULT_ERR02, 6);
			strcpy(rspData.IP_Addr, cmdDataObj.IP_Addr);
			strcpy(rspData.DeviceNbr, cmdDataObj.DeviceNbr);
			IxProxyRespone(RSP_DM_PARA_READ, ntohs(phead->Session_ID), IncreaseIxProxyRSP_ID(), (void*)&rspData);
		}

	}

	CMD_RSP_DELAY();
	IxProxyRespone(RSP_DM_PARA_READ, ntohs(phead->Session_ID), 0, NULL);

}

void IxProxyParameterWriteProcess(char* pData, int len)
{
	IX_PROXY_TCP_HEAD* phead = (IX_PROXY_TCP_HEAD*)pData;
	char* cmdData = (char*)(pData + sizeof(IX_PROXY_TCP_HEAD));	
	PARAMETER_REQ_DATA_T cmdDataObj;
	PARAMETER_RSP_DATA_T rspData;
	int i;
	
	char tempValue[IO_DATA_LENGTH+1];

	SetIxProxyRSP_ID(1);
	
	ParseParameterReqJsonData(cmdData, &cmdDataObj);

	memset(&rspData, 0, sizeof(rspData));
	//写本机参数
	if(inet_addr(cmdDataObj.IP_Addr) == inet_addr(GetMyProxy_IP()) )
	{
		//房号匹配
		if(!strcmp(cmdDataObj.DeviceNbr, GetSysVerInfo_BdRmMs()))
		{
			for(i = 0; i < cmdDataObj.cnt && GetIxProxyState() == IX_PROXY_BUSY; i++)
			{
				memset(&rspData, 0, sizeof(rspData));

				//是否需要恢复出厂设置
				if(!strcmp(cmdDataObj.Parameter[i].VALUE, IO_WRITE_DEFAULT_VALUE))
				{
					if(API_Para_RestoreByID_Default(cmdDataObj.Parameter[i].PARA_ID))
					{
						API_OldPara_ReadByID(cmdDataObj.Parameter[i].PARA_ID, cmdDataObj.Parameter[i].VALUE);
						strncpy(rspData.Result, RESULT_SUCC, 6);
					}
					else
					{
						strncpy(rspData.Result, RESULT_ERR10, 6);
					}
				}
				else
				{
					//写本机参数正确
					if(API_OldPara_WriteByID(cmdDataObj.Parameter[i].PARA_ID, cmdDataObj.Parameter[i].VALUE))
					{
						strncpy(rspData.Result, RESULT_SUCC, 6);
					}
					//写本机参数错误
					else
					{
						strncpy(rspData.Result, RESULT_ERR10, 6);
					}
				}

				strncpy(rspData.PARA_ID, cmdDataObj.Parameter[i].PARA_ID, 5);
				strncpy(rspData.VALUE, cmdDataObj.Parameter[i].VALUE, IO_DATA_LENGTH+1);
				
				strcpy(rspData.IP_Addr, cmdDataObj.IP_Addr);
				strcpy(rspData.DeviceNbr, cmdDataObj.DeviceNbr);

				IxProxyRespone(RSP_DM_PARA_WRITE, ntohs(phead->Session_ID), IncreaseIxProxyRSP_ID(), (void*)&rspData);		
			}
		}
		//房号不匹配
		else
		{
			strncpy(rspData.Result, RESULT_ERR01, 6);
			strcpy(rspData.IP_Addr, GetMyProxy_IP());
			strcpy(rspData.DeviceNbr, GetSysVerInfo_BdRmMs());
			IxProxyRespone(RSP_DM_PARA_WRITE, ntohs(phead->Session_ID), IncreaseIxProxyRSP_ID(), (void*)&rspData);
		}
	}
	//写其他机器参数
	else
	{
		DeviceInfo info;
		memset(&info, 0, sizeof(info));
		
		//读取机器在线
		if(API_GetInfoByIp(inet_addr(cmdDataObj.IP_Addr), 1, &info) == 0)
		{
			//房号匹配
			if(!strcmp(cmdDataObj.DeviceNbr, info.BD_RM_MS))
			{
				for(i = 0; i < cmdDataObj.cnt && GetIxProxyState() == IX_PROXY_BUSY; i++)
				{
					memset(&rspData, 0, sizeof(rspData));

					//是否需要恢复出厂设置
					if(!strcmp(cmdDataObj.Parameter[i].VALUE, IO_WRITE_DEFAULT_VALUE))
					{
						if(API_Para_WriteDefaultReomteByID(inet_addr(cmdDataObj.IP_Addr), cmdDataObj.Parameter[i].PARA_ID, cmdDataObj.Parameter[i].VALUE))
						{
							strncpy(rspData.Result, RESULT_SUCC, 6);
						}
						else
						{
							strncpy(rspData.Result, RESULT_ERR10, 6);
						}
					}
					else
					{
						//写远程设备参数
						if(API_Para_WriteReomteByID(inet_addr(cmdDataObj.IP_Addr), cmdDataObj.Parameter[i].PARA_ID, cmdDataObj.Parameter[i].VALUE))
						{
							strncpy(rspData.Result, RESULT_SUCC, 6);
						}
						else
						{
							strncpy(rspData.Result, RESULT_ERR10, 6);
						}
					}

					strcpy(rspData.PARA_ID, cmdDataObj.Parameter[i].PARA_ID);
					strcpy(rspData.VALUE, cmdDataObj.Parameter[i].VALUE);

					strcpy(rspData.IP_Addr, cmdDataObj.IP_Addr);
					strcpy(rspData.DeviceNbr, cmdDataObj.DeviceNbr);

					IxProxyRespone(RSP_DM_PARA_WRITE, ntohs(phead->Session_ID), IncreaseIxProxyRSP_ID(), (void*)&rspData);		
				}
			}
			//房号不匹配
			else
			{
				strncpy(rspData.Result, RESULT_ERR01, 6);
				strcpy(rspData.IP_Addr, cmdDataObj.IP_Addr);
				strcpy(rspData.DeviceNbr, info.BD_RM_MS);
				IxProxyRespone(RSP_DM_PARA_WRITE, ntohs(phead->Session_ID), IncreaseIxProxyRSP_ID(), (void*)&rspData);
			}
		}
		//读取机器不在线
		else
		{
			strncpy(rspData.Result, RESULT_ERR02, 6);
			strcpy(rspData.IP_Addr, cmdDataObj.IP_Addr);
			strcpy(rspData.DeviceNbr, cmdDataObj.DeviceNbr);
			IxProxyRespone(RSP_DM_PARA_WRITE, ntohs(phead->Session_ID), IncreaseIxProxyRSP_ID(), (void*)&rspData);
		}

	}

	CMD_RSP_DELAY();
	IxProxyRespone(RSP_DM_PARA_WRITE, ntohs(phead->Session_ID), 0, NULL);
}

void IxProxyParameterDefaultProcess(char* pData, int len)
{
	IX_PROXY_TCP_HEAD* phead = (IX_PROXY_TCP_HEAD*)pData;
	char* cmdData = (char*)(pData + sizeof(IX_PROXY_TCP_HEAD));	
	PARAMETER_DEFAULT_REQ_DATA_T cmdDataObj;
	PARAMETER_RSP_DATA_T rspData;
	int i;
	int ret;
	
	SetIxProxyRSP_ID(1);
	
	ParseGetAboutReqJsonData(cmdData, &cmdDataObj);

	
	for(i = 0; i < cmdDataObj.cnt && GetIxProxyState() == IX_PROXY_BUSY; i++)
	{
		memset(&rspData, 0, sizeof(rspData));
		
		//本机
		if(inet_addr(cmdDataObj.device[i].IP_Addr) == inet_addr(GetMyProxy_IP()) )
		{
			//房号相等
			if(!strcmp(cmdDataObj.device[i].DeviceNbr, GetSysVerInfo_BdRmMs()))
			{
				//IX2V test ret = API_io_server_UDP_to_invite_factoryDefault(0xFFFFFFFF, 0xFFFF);
				if(ret == 0)
				{
					strncpy(rspData.Result, RESULT_SUCC, 6);
				}
				//IX2V test else
				{
					strncpy(rspData.Result, RESULT_ERR10, 6);
				}
				strcpy(rspData.IP_Addr, cmdDataObj.device[i].IP_Addr);
				strcpy(rspData.DeviceNbr, cmdDataObj.device[i].DeviceNbr);
			}
			//房号不匹配
			else
			{
				strncpy(rspData.Result, RESULT_ERR01, 6);
				strcpy(rspData.IP_Addr, cmdDataObj.device[i].IP_Addr);
				strcpy(rspData.DeviceNbr, GetSysVerInfo_BdRmMs());
			}
			IxProxyRespone(RSP_DM_PARA_DEFAULT, ntohs(phead->Session_ID), IncreaseIxProxyRSP_ID(), (void*)&rspData);
		}
		//其他机器
		else
		{
			DeviceInfo info;

			//读取机器在线
			if(API_GetInfoByIp(inet_addr(cmdDataObj.device[i].IP_Addr), 1, &info) == 0)
			{
				//房号相等
				if(!strcmp(info.BD_RM_MS, cmdDataObj.device[i].DeviceNbr))
				{
					//IX2V test ret = API_io_server_UDP_to_invite_factoryDefault(inet_addr(cmdDataObj.device[i].IP_Addr), 0xFFFF);
					if(ret == 0)
					{
						strncpy(rspData.Result, RESULT_SUCC, 6);
					}
					else
					{
						strncpy(rspData.Result, RESULT_ERR10, 6);
					}
					strcpy(rspData.IP_Addr, cmdDataObj.device[i].IP_Addr);
					strcpy(rspData.DeviceNbr, cmdDataObj.device[i].DeviceNbr);
				}
				//房号不相等
				else
				{
					strncpy(rspData.Result, RESULT_ERR01, 6);
					strcpy(rspData.IP_Addr, cmdDataObj.device[i].IP_Addr);
					strcpy(rspData.DeviceNbr, info.BD_RM_MS);
				}
			}
			else
			{
				strncpy(rspData.Result, RESULT_ERR02, 6);
				strcpy(rspData.IP_Addr, cmdDataObj.device[i].IP_Addr);
				strcpy(rspData.DeviceNbr, cmdDataObj.device[i].DeviceNbr);
			}
			
			IxProxyRespone(RSP_DM_PARA_DEFAULT, ntohs(phead->Session_ID), IncreaseIxProxyRSP_ID(), (void*)&rspData);		
		}
	}


	CMD_RSP_DELAY();
	IxProxyRespone(RSP_DM_PARA_WRITE, ntohs(phead->Session_ID), 0, NULL);

}

/*********************************************************************************************************
**  End Of File
*********************************************************************************************************/

