/**
  ******************************************************************************
  * @file    obj_CmdGetInfo.h
  * @author  cao
  * @version V00.01.00
  * @date    2016.5.31
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 
	  //cao_20170304  整个文件

#ifndef _obj_CmdGetInfo_H
#define _obj_CmdGetInfo_H

// Define Object Property-------------------------------------------------------
#include "task_IxProxy.h"

typedef struct
{
	char	Result[6];			//Result
	char	MFG_SN[12+1];		//MFG_SN
	char	BD_RM_MS[10+1];		//BD_RM_MS
	char	Local[6+1];			//Local
	char	Global[10+1];		//Global
	char	IP_STATIC[7];			//IP_STATIC
	char	IP_ADDR[16];			//IP_ADDR
	char	IP_MASK[16];			//IP_MASK
	char	IP_GATEWAY[16];			//IP_GATEWAY
	int		deviceType;
	char	name1[21];
	char	name2_utf8[41];
}DeviceInfo2;

typedef struct
{
	char	Result[6];			//Result
	char	MFG_SN[12+1];		//MFG_SN
	char	BD_RM_MS[10+1];		//BD_RM_MS
	char	Local[6+1];			//Local
	char	Global[10+1];		//Global
	char	IP_STATIC[7];			//IP_STATIC
	char	IP_ADDR[16];			//IP_ADDR
	char	IP_MASK[16];			//IP_MASK
	char	IP_GATEWAY[16];			//IP_GATEWAY
	int		deviceType;
	char	name1[21];
	char	name2_utf8[41];
	int		bootTime;
}DeviceInfo3;


typedef struct
{
	int		cnt;								//设备数量
	char	deviceNbr[DEVICE_MAX_NUM][10+1];	//房号
}GET_INFO_REQ_DATA_T;

typedef struct
{
	char	DeviceNbr[10+1];		//房号
	char	IP_Addr[15+1];			//IP地址
}GET_INFO_REQ2_DATA_T;

typedef struct
{
	int	deviceCnt;							//设备数量
	char MFG_SN[MAX_DEVICE][12+1];			//MFG_SN
}GET_INFO_REQ3_DATA_T;


// Define Object Function - Public----------------------------------------------
char* CreateGetInfoRspJsonData(DeviceInfo* data);


// Define Object Function - Private---------------------------------------------



#endif


