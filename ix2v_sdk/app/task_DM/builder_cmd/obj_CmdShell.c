/**
  ******************************************************************************
  * @file    obj_CmdShell.c
  * @author  czb
  * @version V00.01.00
  * @date    2016.5.31
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 
  
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <memory.h>
#include <dirent.h>
#include <string.h>
#include <sys/stat.h>
#include "task_IxProxy.h"
#include "tcp_server_process.h"
#include "cJSON.h"
#include "vtk_udp_stack_device_update.h"
#include "task_Shell.h"
#include "obj_PublicUnicastCmd.h"

int ShellCmdProcess(int ip, PublicUnicastCmdData dataIn, PublicUnicastCmdData* dataOut)
{
	char *strIp, *strSn, *strDeviceNbr, *strShellCmd;
	cJSON *reqJson;
	int ret = 0;

	reqJson = cJSON_Parse(dataIn.data);
	if(reqJson == NULL)
	{
		return -1;
	}
	strIp = cJSON_GetStringValue(cJSON_GetObjectItem(reqJson, DM_KEY_IP_Addr));
	strSn = cJSON_GetStringValue(cJSON_GetObjectItem(reqJson, DM_KEY_MFG_SN));
	strDeviceNbr = cJSON_GetStringValue(cJSON_GetObjectItem(reqJson, DM_KEY_DeviceNbr));
	strShellCmd = cJSON_GetStringValue(cJSON_GetObjectItem(reqJson, DM_KEY_SHELL_CMD));

	API_ShellCommand(ip, SourceIXD, strShellCmd);

	cJSON_Delete(reqJson);
	
	return ret;
}

void IxProxyShellProcess(char* pData, int len)
{
	IX_PROXY_TCP_HEAD* phead = (IX_PROXY_TCP_HEAD*)pData;
	char* cmdData = (char*)(pData + sizeof(IX_PROXY_TCP_HEAD));	
	PublicUnicastCmdData dataIn;
	
	cJSON *reqJson = NULL;
	cJSON *rspJson = NULL;
	char *strIp, *strSn, *strDeviceNbr;
	int ip;
	int result;

	SetIxProxyRSP_ID(1);
	if(GetIxProxyState() == IX_PROXY_BUSY)
	{
	    reqJson = cJSON_Parse(cmdData);
		rspJson = cJSON_Parse(cmdData);
		
		if(reqJson != NULL)
		{
			strIp = cJSON_GetStringValue(cJSON_GetObjectItem(reqJson, DM_KEY_IP_Addr));
			strSn = cJSON_GetStringValue(cJSON_GetObjectItem(reqJson, DM_KEY_MFG_SN));
			strDeviceNbr = cJSON_GetStringValue(cJSON_GetObjectItem(reqJson, DM_KEY_DeviceNbr));
			result = ProxyGetIpByPara(strIp, strDeviceNbr, strSn, &ip);
		}
		else
		{
			result = -5;
		}

		if(result < 0)
		{
			cJSON_AddStringToObject(rspJson, DM_KEY_Result, GetErrorCodeByResult(result));
		}
		else
		{
			dataIn.data = cmdData;
			dataIn.len = strlen(cmdData) + 1;
			if(PublicUnicastCmdRequst(ip, strSn, IxProxyWaitUdpTime, SHELL_REQ, dataIn, NULL) != 0)
			{
				cJSON_AddStringToObject(rspJson, DM_KEY_Result, RESULT_ERR06);
			}
			else
			{
				cJSON_AddStringToObject(rspJson, DM_KEY_Result, RESULT_SUCC);
			}
		}

		IxProxyRespone(RSP_SHELL, ntohs(phead->Session_ID), IncreaseIxProxyRSP_ID(), rspJson);
		
		cJSON_Delete(reqJson);
		cJSON_Delete(rspJson);
	}

}

/*********************************************************************************************************
**  End Of File
*********************************************************************************************************/

