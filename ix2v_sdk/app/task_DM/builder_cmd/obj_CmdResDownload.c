/**
  ******************************************************************************
  * @file    obj_CmdResDownload.c
  * @author  czb
  * @version V00.01.00
  * @date    2016.5.31
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 
  
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <memory.h>
#include "cJSON.h"
#include "utility.h"
#include "obj_CmdResDownload.h"
#include "task_IxProxy.h"
#include "tcp_server_process.h"
#include "obj_PublicUdpCmd.h"
#include "vtk_udp_stack_device_update.h"
#include "ota_client_ifc.h"
#include "obj_IoInterface.h"
#include "obj_PublicInformation.h"

int CodeDownloadProcess(const char* reqData)
{
	char *strIp, *strSn, *strDeviceNbr;
	OtaClientPara para;
	cJSON *reqJson;
	int ret = -1;
	int serverIpAddr;
	int server1IpAddr;
	int server2IpAddr;

	reqJson = cJSON_Parse(reqData);
	if(reqJson != NULL)
	{
		serverIpAddr = inet_addr(cJSON_GetStringValue(cJSON_GetObjectItem(reqJson, DM_KEY_server)));
		server1IpAddr = inet_addr("47.106.104.38");
		server2IpAddr = inet_addr("47.91.88.33");

		strcpy(para.server_ip, cJSON_GetStringValue(cJSON_GetObjectItem(reqJson, DM_KEY_server)));
		strcpy(para.file_code, cJSON_GetStringValue(cJSON_GetObjectItem(reqJson, DM_KEY_code)));
		para.file_type = 1;
		API_Para_Read_String(DEV_NAME, para.dev_type);
		strcpy(para.dev_ver, API_PublicInfo_Read_String(PB_FW_VER));
		strcpy(para.dev_id, API_PublicInfo_Read_String(PB_MFG_SN));
		para.reportIpCnt = 1;
		para.reportIp[0] = inet_addr(cJSON_GetStringValue(cJSON_GetObjectItem(reqJson, DM_KEY_ReportIp)));
		para.space = 0;

		/*
		if(serverIpAddr == server1IpAddr || serverIpAddr == server2IpAddr)
		{
			para.downloadType = OtaDlTypeFtp;
		}
		else
		{
			para.downloadType = OtaDlTypeTcp;
		}
		*/
	
		para.downloadType = OtaDlTypeFtp;
		
		if(!strcmp(cJSON_GetStringValue(cJSON_GetObjectItem(reqJson, DM_KEY_operate)), DM_KEY_CANCEL))
		{
			ret = API_OtaCancel();
		}
		else
		{
			ret = API_OtaDownload(1, para);
		}
	}

	return ret;
}

void IxProxyPCDownloadResProcess(char* pData, int len)
{
	IX_PROXY_TCP_HEAD* phead = (IX_PROXY_TCP_HEAD*)pData;
	char* cmdData = (char*)(pData + sizeof(IX_PROXY_TCP_HEAD));	

	cJSON *ixReqJson;
	cJSON *reqJson;
	cJSON *rspJson;
	char* rspData = NULL;
	int rspDataLen;
	char* reqData = NULL;

	char *strIp, *strSn, *strDeviceNbr;
	int ip;
	int result;
	
	reqJson = cJSON_Parse(cmdData);

	dprintf("cmdData: %s\n", cmdData);

	if(reqJson == NULL)
	{
		dprintf("cmdData Error: %s\n", cmdData);
		return;
	}

	SetIxProxyRSP_ID(1);

    rspJson = cJSON_CreateObject();
	cJSON_AddStringToObject(rspJson, DM_KEY_IP_Addr, cJSON_GetStringValue(cJSON_GetObjectItem(reqJson, DM_KEY_IP_Addr)));
	cJSON_AddStringToObject(rspJson, DM_KEY_MFG_SN, cJSON_GetStringValue(cJSON_GetObjectItem(reqJson, DM_KEY_MFG_SN)));
	cJSON_AddStringToObject(rspJson, DM_KEY_DeviceNbr, cJSON_GetStringValue(cJSON_GetObjectItem(reqJson, DM_KEY_DeviceNbr)));
	cJSON_AddStringToObject(rspJson, DM_KEY_OPERATE, cJSON_GetStringValue(cJSON_GetObjectItem(reqJson, DM_KEY_operate)));

	strIp = cJSON_GetStringValue(cJSON_GetObjectItem(reqJson, DM_KEY_IP_Addr));
	strSn = cJSON_GetStringValue(cJSON_GetObjectItem(reqJson, DM_KEY_MFG_SN));
	strDeviceNbr = cJSON_GetStringValue(cJSON_GetObjectItem(reqJson, DM_KEY_DeviceNbr));
	result = ProxyGetIpByPara(strIp, strDeviceNbr, strSn, &ip);

	if(result < 0)
	{
		cJSON_AddStringToObject(rspJson, DM_KEY_Result, GetErrorCodeByResult(result));
	}
	else
	{
		ixReqJson = cJSON_CreateObject();
		cJSON_AddStringToObject(ixReqJson, DM_KEY_server, cJSON_GetStringValue(cJSON_GetObjectItem(reqJson, DM_KEY_SERVER)));
		cJSON_AddStringToObject(ixReqJson, DM_KEY_code, cJSON_GetStringValue(cJSON_GetObjectItem(reqJson, DM_KEY_CODE)));
		cJSON_AddStringToObject(ixReqJson, DM_KEY_operate, cJSON_GetStringValue(cJSON_GetObjectItem(reqJson, DM_KEY_operate)));
		cJSON_AddStringToObject(ixReqJson, DM_KEY_ReportIp, GetSysVerInfo_IP_by_device(GetNetDeviceNameByTargetIp(ip)));
		reqData = cJSON_Print(ixReqJson);

		cJSON_Delete(ixReqJson);
		cJSON_Delete(reqJson);
	
		if(PublicUdpCmdReqProcess2(ip, IxProxyWaitUdpTime, IX_DEVICE_RES_DOWNLOAD_REQ, CodeDownloadProcess, reqData) != 0)
		{
			cJSON_AddStringToObject(rspJson, DM_KEY_Result, RESULT_ERR06);
		}
		else
		{
			cJSON_AddStringToObject(rspJson, DM_KEY_Result, RESULT_SUCC);
		}
		free(reqData);
	}

	rspData = cJSON_Print(rspJson);

	IxProxyRespone(RSP_DM_RES_DOWNLOAD, ntohs(phead->Session_ID), IncreaseIxProxyRSP_ID(), rspData);

	IxProxyRespone(RSP_DM_RES_DOWNLOAD, ntohs(phead->Session_ID), 0, NULL);

	cJSON_Delete(rspJson);
}


/*********************************************************************************************************
**  End Of File
*********************************************************************************************************/

