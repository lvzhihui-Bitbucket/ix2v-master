/**
  ******************************************************************************
  * @file    obj_CmdReboot.c
  * @author  czb
  * @version V00.01.00
  * @date    2016.5.31
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 
  
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <memory.h>
#include "obj_CmdGetAbout.h"
#include "task_IxProxy.h"
#include "tcp_server_process.h"
#include "cJSON.h"

int ParseGetAboutReqJsonData(const char* json, GET_ABOUT_REQ_DATA_T* pData)
{
    int status = 0;
	int iCnt, deviceNum;
	
    const cJSON *pSub = NULL;

	memset(pData, 0, sizeof(GET_ABOUT_REQ_DATA_T));

    /* 创建一个用于解析的 cJSON 结构 */
    cJSON *root = cJSON_Parse(json);
    if (root == NULL)
    {
		dprintf("Json error!!! : %s\n", json);
        goto end;
    }

	
	pData->cnt = cJSON_GetArraySize ( root );
 
	for( iCnt = 0 ; iCnt < pData->cnt; iCnt ++ )
	{
		pSub = cJSON_GetArrayItem(root, iCnt);
		
		if(NULL == pSub ){ continue ; }

		GetJsonDataPro(pSub, "DeviceNbr", pData->device[iCnt].DeviceNbr);
		GetJsonDataPro(pSub, "IP_Addr", pData->device[iCnt].IP_Addr);
	}

    status = 1;

end:
	
    cJSON_Delete(root);
	
    return status;
}

char* CreateGetAboutRspJsonData(GET_ABOUT_RSP_DATA_T* data)
{
    cJSON *root = NULL;
	char *string = NULL;

    root = cJSON_CreateObject();

	cJSON_AddStringToObject(root, "Result", data->Result);

	cJSON_AddStringToObject(root, "IP_Addr", data->IP_Addr);
	cJSON_AddStringToObject(root, "Device_Addr", data->Device_Addr);
	cJSON_AddStringToObject(root, "Name", data->Name);
	cJSON_AddStringToObject(root, "G_Nbr", data->G_Nbr);
	cJSON_AddStringToObject(root, "L_Nbr", data->L_Nbr);
	
	cJSON_AddStringToObject(root, "SW_Ver", data->SW_Ver);
	cJSON_AddStringToObject(root, "HW_Ver", data->HW_Ver);
	cJSON_AddStringToObject(root, "UpgradeTime", data->UpgradeTime);
	cJSON_AddStringToObject(root, "UpgradeCode", data->UpgradeCode);
	cJSON_AddStringToObject(root, "UpTime", data->UpTime);
	
	cJSON_AddStringToObject(root, "SerialNo", data->SerialNo);
	cJSON_AddStringToObject(root, "DeviceType", data->DeviceType);
	cJSON_AddStringToObject(root, "DeviceModel", data->DeviceModel);
	cJSON_AddStringToObject(root, "AreaCode", data->AreaCode);
	cJSON_AddStringToObject(root, "TransferState", data->TransferState);
	
	cJSON_AddStringToObject(root, "HW_Address", data->HW_Address);
	cJSON_AddStringToObject(root, "SubnetMask", data->SubnetMask);
	cJSON_AddStringToObject(root, "DefaultRoute", data->DefaultRoute);

	string = cJSON_Print(root);

	cJSON_Delete(root);

	return string;
}

static void GetAboutByAboutId(const char* aboutData, int aboutId, char* pValue)
{
	char tempChar[ABOUT_DATA_DOMAIN_MAX_LEN];
	char *pos1, *pos2;
	
	pValue[0] = 0;

	snprintf(tempChar, ABOUT_DATA_DOMAIN_MAX_LEN, "<ID=%03d,Value=", aboutId);
	pos1 = strstr(aboutData, tempChar);
	
	if(pos1 != NULL)
	{
		pos2 = strchr(pos1, '>');
		if(pos2 != NULL)
		{
			int len = pos2 - pos1 - strlen("<ID=001,Value=");

			if(len >= ABOUT_DATA_DOMAIN_MAX_LEN)
			{
				len = ABOUT_DATA_DOMAIN_MAX_LEN - 1;
			}
			
			memcpy(pValue, pos1 + strlen("<ID=001,Value="), len);
			pValue[len] = 0;
		}
	}

}

void GetALLAbout(const char* aboutData, GET_ABOUT_RSP_DATA_T* aboutRspObj)
{
	GetAboutByAboutId(aboutData, ABOUT_ID_IP_Address, aboutRspObj->IP_Addr);
	GetAboutByAboutId(aboutData, ABOUT_ID_BD_RM_MS_Nbr, aboutRspObj->Device_Addr);
	GetAboutByAboutId(aboutData, ABOUT_ID_Name, aboutRspObj->Name);
	GetAboutByAboutId(aboutData, ABOUT_ID_Global_Nbr, aboutRspObj->G_Nbr);
	GetAboutByAboutId(aboutData, ABOUT_ID_Local_Nbr, aboutRspObj->L_Nbr);
	
	GetAboutByAboutId(aboutData, ABOUT_ID_SW_Version, aboutRspObj->SW_Ver);
	GetAboutByAboutId(aboutData, ABOUT_ID_HW_Version, aboutRspObj->HW_Ver);
	GetAboutByAboutId(aboutData, ABOUT_ID_UpgradeTime, aboutRspObj->UpgradeTime);
	GetAboutByAboutId(aboutData, ABOUT_ID_UpgradeCode, aboutRspObj->UpgradeCode);
	GetAboutByAboutId(aboutData, ABOUT_ID_UpTime, aboutRspObj->UpTime);
	
	GetAboutByAboutId(aboutData, ABOUT_ID_SerialNo, aboutRspObj->SerialNo);
	GetAboutByAboutId(aboutData, ABOUT_ID_DeviceType, aboutRspObj->DeviceType);
	GetAboutByAboutId(aboutData, ABOUT_ID_DeviceModel, aboutRspObj->DeviceModel);
	GetAboutByAboutId(aboutData, ABOUT_ID_AreaCode, aboutRspObj->AreaCode);
	GetAboutByAboutId(aboutData, ABOUT_ID_TransferState, aboutRspObj->TransferState);
	
	GetAboutByAboutId(aboutData, ABOUT_ID_HW_Address, aboutRspObj->HW_Address);
	GetAboutByAboutId(aboutData, ABOUT_ID_SubnetMask, aboutRspObj->SubnetMask);
	GetAboutByAboutId(aboutData, ABOUT_ID_DefaultRoute, aboutRspObj->DefaultRoute);
}


void IxProxyGetAboutProcess(char* pData, int len)
{
	IX_PROXY_TCP_HEAD* phead = (IX_PROXY_TCP_HEAD*)pData;
	char* cmdData = (char*)(pData + sizeof(IX_PROXY_TCP_HEAD));	
	GET_ABOUT_REQ_DATA_T cmdDataObj;
	GET_ABOUT_RSP_DATA_T rspData;
	char aboutData[DATA_LEN];
	int i;
	
	char sendData[CLIENT_CONNECT_BUF_MAX];

	SetIxProxyRSP_ID(1);
	
	ParseGetAboutReqJsonData(cmdData, &cmdDataObj);

	for(i = 0; i < cmdDataObj.cnt && GetIxProxyState() == IX_PROXY_BUSY; i++)
	{
		memset(&rspData, 0, sizeof(rspData));
		
		//Get about本机
		if(inet_addr(cmdDataObj.device[i].IP_Addr) == inet_addr(GetMyProxy_IP()) )
		{
			//房号相等
			if(!strcmp(cmdDataObj.device[i].DeviceNbr, GetSysVerInfo_BdRmMs()))
			{
				GetMyAboutString(inet_addr(GetMyProxy_IP()), aboutData);
				GetALLAbout(aboutData, &rspData);
				strncpy(rspData.Result, RESULT_SUCC, 6);
			}
			//房号不匹配
			else
			{
				strncpy(rspData.Result, RESULT_ERR01, 6);
				strcpy(rspData.IP_Addr, cmdDataObj.device[i].IP_Addr);
				strcpy(rspData.Device_Addr, cmdDataObj.device[i].DeviceNbr);
			}
			IxProxyRespone(RSP_DM_ABOUT, ntohs(phead->Session_ID), IncreaseIxProxyRSP_ID(), (void*)&rspData);
			continue;
		}

		//Get about 其他机器
		if(API_GetAboutByIp(inet_addr(cmdDataObj.device[i].IP_Addr), 1, aboutData) == 0)
		{
			GetALLAbout(aboutData, &rspData);
			//房号相等
			if(!strcmp(rspData.Device_Addr, cmdDataObj.device[i].DeviceNbr))
			{
				strncpy(rspData.Result, RESULT_SUCC, 6);
			}
			//房号不相等
			else
			{
				strncpy(rspData.Result, RESULT_ERR01, 6);
			}
		}
		else
		{
			strncpy(rspData.Result, RESULT_ERR02, 6);
			strcpy(rspData.IP_Addr, cmdDataObj.device[i].IP_Addr);
			strcpy(rspData.Device_Addr, cmdDataObj.device[i].DeviceNbr);
		}
		
		IxProxyRespone(RSP_DM_ABOUT, ntohs(phead->Session_ID), IncreaseIxProxyRSP_ID(), (void*)&rspData);		
	}
	
	IxProxyRespone(RSP_DM_ABOUT, ntohs(phead->Session_ID), 0, NULL);

}


/*********************************************************************************************************
**  End Of File
*********************************************************************************************************/

