/**
  ******************************************************************************
  * @file    obj_CmdProg.c
  * @author  czb
  * @version V00.01.00
  * @date    2016.5.31
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 
  
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <memory.h>
#include "obj_CmdProg.h"
#include "task_IxProxy.h"
#include "tcp_server_process.h"
#include "cJSON.h"
#include "obj_IX_Req_Ring.h"
#include "obj_CmdR8012.h"


static int ParseProgReqJsonData(const char* json, PROG_REQ_DATA_T* pData)
{
    int status = 0;

	memset(pData, 0, sizeof(PROG_REQ_DATA_T));

    /* 创建一个用于解析的 cJSON 结构 */
    cJSON *root = cJSON_Parse(json);
    if (root == NULL)
    {
		dprintf("Json error!!! : %s\n", json);
        goto end;
    }

	
	GetJsonDataPro(root, "IP_Addr", pData->IP_Addr);
	GetJsonDataPro(root, "MFG_SN", pData->MFG_SN);
	
	GetJsonDataPro(root, "DeviceNbr", pData->DeviceNbr);
	GetJsonDataPro(root, "Local", pData->Local);
	GetJsonDataPro(root, "Global", pData->Global);
	GetJsonDataPro(root, "name1", pData->name1);
	GetJsonDataPro(root, "name2_utf8", pData->name2_utf8);
	
    status = 1;

end:
	
    cJSON_Delete(root);
	
    return status;
}

char* CreateProgRspJsonData(PROG_RSP_DATA_T* pData)
{
    cJSON *root = NULL;
	char *string = NULL;


    root = cJSON_CreateObject();
	
	cJSON_AddStringToObject(root, "Result", pData->Result);
	cJSON_AddStringToObject(root, "IP_Addr", pData->IP_Addr);
	cJSON_AddStringToObject(root, "MFG_SN", pData->MFG_SN);

	cJSON_AddStringToObject(root, "DeviceNbr", pData->DeviceNbr);
	cJSON_AddStringToObject(root, "Local", pData->Local);
	cJSON_AddStringToObject(root, "Global", pData->Global);
	cJSON_AddStringToObject(root, "name1", pData->name1);
	cJSON_AddStringToObject(root, "name2_utf8", pData->name2_utf8);

	string = cJSON_Print(root);

	cJSON_Delete(root);

	return string;
}


void IxProxyProgProcess(char* pData, int len)
{
	IX_PROXY_TCP_HEAD* phead = (IX_PROXY_TCP_HEAD*)pData;
	char* cmdData = (char*)(pData + sizeof(IX_PROXY_TCP_HEAD));	
	PROG_REQ_DATA_T reqObj;
	PROG_RSP_DATA_T rspObj = {0};
	
	R8012_Device r8012DeviceInfo;
	R8012_Device* p8012DeviceInfo;
	int bootTime;
	
	DeviceInfo deviceInfo = {0};
	int ipAddr;
	
	SetIxProxyRSP_ID(1);

	dprintf("%s\n", cmdData);
	ParseProgReqJsonData(cmdData, &reqObj);

	//By IP_Addr
	if(reqObj.IP_Addr[0] != 0)
	{
		ipAddr = inet_addr(reqObj.IP_Addr);
		if(ipAddr == inet_addr(GetMyProxy_IP()))
		{
			if(!strcmp(GetSysVerInfo_Sn(), reqObj.MFG_SN))
			{
				if(reqObj.DeviceNbr[0])
				{
					SetSysVerInfo_BdRmMs(reqObj.DeviceNbr);
				}

				if(reqObj.Local[0])
				{
					SetSysVerInfo_LocalNum(reqObj.Local);
				}
				if(reqObj.Global[0])
				{
					SetSysVerInfo_GlobalNum(reqObj.Global);
				}
				
				if(reqObj.name1[0])
				{
					SetSysVerInfo_name(reqObj.name1);
				}
				strcpy(rspObj.DeviceNbr, GetSysVerInfo_BdRmMs());
				strcpy(rspObj.MFG_SN, GetSysVerInfo_Sn());
				strcpy(rspObj.IP_Addr, GetMyProxy_IP());
				strcpy(rspObj.Local, GetSysVerInfo_LocalNum());
				strcpy(rspObj.Global, GetSysVerInfo_GlobalNum());
				strcpy(rspObj.name1, GetSysVerInfo_name());
				strncpy(rspObj.Result, RESULT_SUCC, 6);

				GetMyInfo(inet_addr(GetMyProxy_IP()), &deviceInfo);
				DeviceInfoToR8012DeviceInfo(deviceInfo, &r8012DeviceInfo, GetSystemBootTime());
				AddR8012Record(r8012DeviceInfo);
				
				CmdProgCallNbrReq();
			}
			else
			{
				strcpy(rspObj.MFG_SN, GetSysVerInfo_Sn());
				strncpy(rspObj.Result, RESULT_ERR02, 6);
			}
		}
		else
		{
			if(API_GetInfoByIp(ipAddr, IxProxyWaitUdpTime, &deviceInfo) == 0)
			{
				if(!strcmp(deviceInfo.MFG_SN, reqObj.MFG_SN))
				{
					if(reqObj.DeviceNbr[0])
					{
						strncpy(deviceInfo.BD_RM_MS, reqObj.DeviceNbr, 10+1);
					}

					if(reqObj.Local[0])
					{
						strncpy(deviceInfo.Local, reqObj.Local, 6+1);
					}
					if(reqObj.Global[0])
					{
						strncpy(deviceInfo.Global, reqObj.Global, 10+1);
					}
					
					if(reqObj.name1[0])
					{
						strncpy(deviceInfo.name1, reqObj.name1, 20+1);
					}
					
					if(reqObj.name2_utf8[0])
					{
						strncpy(deviceInfo.name2_utf8, reqObj.name2_utf8, 40+1);
					}

					if(API_ProgInfoByIp(ipAddr, 5, &deviceInfo) == 0)
					{
						strcpy(rspObj.DeviceNbr, deviceInfo.BD_RM_MS);
						strcpy(rspObj.MFG_SN, deviceInfo.MFG_SN);
						strcpy(rspObj.IP_Addr, deviceInfo.IP_ADDR);
						
						strcpy(rspObj.Local, deviceInfo.Local);
						strcpy(rspObj.Global, deviceInfo.Global);
						strcpy(rspObj.name1, deviceInfo.name1);
						strcpy(rspObj.name2_utf8, deviceInfo.name2_utf8);

						strncpy(rspObj.Result, RESULT_SUCC, 6);

						//修改cache表
						p8012DeviceInfo = GetR8012Record(deviceInfo.MFG_SN);
						if(p8012DeviceInfo != NULL)
						{
							strcpy(p8012DeviceInfo->BD_RM_MS, deviceInfo.BD_RM_MS);
							strcpy(p8012DeviceInfo->Local, deviceInfo.Local);
							strcpy(p8012DeviceInfo->Global, deviceInfo.Global);
							strcpy(p8012DeviceInfo->name1, deviceInfo.name1);
							strcpy(p8012DeviceInfo->name2_utf8, deviceInfo.name2_utf8);
						}
					}
					else
					{
						strncpy(rspObj.Result, RESULT_ERR02, 6);
					}
				}
				else
				{
					strcpy(rspObj.MFG_SN, deviceInfo.MFG_SN);
					strncpy(rspObj.Result, RESULT_ERR01, 6);
				}
			}
			else
			{
				strncpy(rspObj.Result, RESULT_ERR02, 6);
			}
		}
		IxProxyRespone(RSP_DM_PROG_CALL_NBR, ntohs(phead->Session_ID), IncreaseIxProxyRSP_ID(), (void*)&rspObj);
	}
}


/*********************************************************************************************************
**  End Of File
*********************************************************************************************************/

