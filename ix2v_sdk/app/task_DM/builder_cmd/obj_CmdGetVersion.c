/**
  ******************************************************************************
  * @file    obj_CmdGetVersion.c
  * @author  czb
  * @version V00.01.00
  * @date    2016.5.31
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 
  
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <memory.h>
#include "obj_CmdGetVersion.h"
#include "task_IxProxy.h"
#include "tcp_server_process.h"
#include "cJSON.h"
#include "obj_GetInfoByIp.h"
#include "obj_IoInterface.h"


static int ParseGetVersionReqJsonData(const char* json, VERSION_REQ_T* pData)
{
    int status = 0;
	int iCnt;
    const cJSON *device = NULL;
    const cJSON *pSub = NULL;
	

	memset(pData, 0, sizeof(VERSION_REQ_T));

    /* 创建一个用于解析的 cJSON 结构 */
    cJSON *root = cJSON_Parse(json);
    if (root == NULL)
    {
		dprintf("Json error!!! : %s\n", json);
        goto end;
    }

	pData->cnt = cJSON_GetArraySize ( root );

	if(pData->cnt > 256)
	{
		pData->cnt = 256;
	}
 
	for( iCnt = 0 ; iCnt < pData->cnt; iCnt ++ )
	{
		pSub = cJSON_GetArrayItem(root, iCnt);
		
		if(NULL == pSub ){ continue ; }
		
		if (cJSON_IsString(pSub) && (pSub->valuestring != NULL))
		{
			strncpy(pData->MFG_SN[iCnt], pSub->valuestring, 12);
			pData->MFG_SN[iCnt][12] = 0;
		}
	}

    status = 1;

end:
	
    cJSON_Delete(root);
	
    return status;
}

char* CreateGetVersionRspJsonData(VERSION_RSP_T* data)
{
    cJSON *root = NULL;
	char* string;
	
    root = cJSON_CreateObject();
	
    cJSON_AddStringToObject(root, "Result", data->Result);
    cJSON_AddStringToObject(root, "MFG_SN", data->MFG_SN);
    cJSON_AddStringToObject(root, "BD_RM_MS", data->BD_RM_MS);
    cJSON_AddStringToObject(root, "IP_Addr", data->IP_Addr);
    cJSON_AddStringToObject(root, "Device type", data->ver.deviceType);
    cJSON_AddStringToObject(root, "Code info", data->ver.codeInfo);
    cJSON_AddNumberToObject(root, "Code size", data->ver.codeSize);
    cJSON_AddStringToObject(root, "App Code", data->ver.appCode);
    cJSON_AddStringToObject(root, "App Ver", data->ver.appVer);

	
	string = cJSON_Print(root);
	if(string != NULL)
	{	
		//printf("CreateGetVersionRspJsonData=%s\n", string);
	}
	cJSON_Delete(root);

	return string;
}

void GetMyVerDevicetype(char* verDeviceType)
{
	char ioData[500];
	FW_VER_VERIFY_T version;
		
	if(API_Para_Read_String(FWVersionAndVerify, ioData) == 0)
	{
		ParseFwVerVerifyObject(ioData, &version);
		if(strlen(version.deviceType) > 10)
		{
			version.deviceType[10] = 0;
		}
		strcpy(verDeviceType, version.deviceType);
	}
	else
	{
		API_Para_Read_String(DEV_NAME, verDeviceType);
	}
}

int GetFwVersionAndVerify(int ipAddr, int deviceType, FW_VER_VERIFY_T* version)
{
	char ioData[500];
	
	if(ipAddr == inet_addr(GetSysVerInfo_IP_by_device(NET_ETH0))|| ipAddr == inet_addr(GetSysVerInfo_IP_by_device(NET_WLAN0)))
	{
		if(API_Para_Read_String(FWVersionAndVerify, ioData) == 0)
		{
			ParseFwVerVerifyObject(ioData, version);
		}
		else
		{
			API_Para_Read_String(DEV_NAME, version->deviceType);
		}
	}
	else
	{
		if(API_Para_Read_String(FWVersionAndVerify, ioData) == 0)
		{
			ParseFwVerVerifyObject(ioData, version);
		}
		else
		{
			switch(deviceType)
			{
				case TYPE_OS:
					strcpy(version->deviceType, "IX610");
					break;
				case TYPE_DS:
					strcpy(version->deviceType, "IX850");
					break;
				case TYPE_IM:
					strcpy(version->deviceType, "IX471");
					break;
			}
		}
	}
}
void IxProxyGetVersionProcess(char* pData, int len)
{
	IX_PROXY_TCP_HEAD* phead = (IX_PROXY_TCP_HEAD*)pData;
	char* cmdData = (char*)(pData + sizeof(IX_PROXY_TCP_HEAD));	
	VERSION_REQ_T cmdDataObj;
	VERSION_RSP_T rspData;
	
	int i, ipAddr;	
	char ioData[500];
	DeviceInfo info1;

	SetIxProxyRSP_ID(1);

	ParseGetVersionReqJsonData(cmdData, &cmdDataObj);

	for(i = 0; i < cmdDataObj.cnt && GetIxProxyState() == IX_PROXY_BUSY; i++)
	{
		memset(&rspData, 0, sizeof(rspData));
				
		//读取本机
		if(!strcmp(cmdDataObj.MFG_SN[i], GetSysVerInfo_Sn()))
		{
			strncpy(rspData.Result, RESULT_SUCC, 6);
			strcpy(rspData.BD_RM_MS, GetSysVerInfo_BdRmMs());
			strcpy(rspData.MFG_SN, GetSysVerInfo_Sn());
			strcpy(rspData.IP_Addr, GetMyProxy_IP());
			GetFwVersionAndVerify(inet_addr(GetMyProxy_IP()), 0, &rspData.ver);
		}
		//读取其他设备
		else
		{
			if(API_GetIpByMFG_SN(cmdDataObj.MFG_SN[i],  IxProxyWaitUdpTime, &ipAddr, NULL) == 0)
			{
				//读取设备信息成功
				if(API_GetInfoByIp(ipAddr, IxProxyWaitUdpTime, &info1) == 0)
				{
					strncpy(rspData.Result, RESULT_SUCC, 6);
					strncpy(rspData.BD_RM_MS, info1.BD_RM_MS,10);
					strncpy(rspData.MFG_SN, info1.MFG_SN,12);
					strncpy(rspData.IP_Addr, info1.IP_ADDR,15);
					GetFwVersionAndVerify(ipAddr, info1.deviceType, &rspData.ver);
				}
				//无此设备
				else
				{
					strncpy(rspData.Result, RESULT_ERR02, 6);
				}
			}
			//无此设备
			else
			{
				strncpy(rspData.Result, RESULT_ERR02, 6);
			}
		}
		IxProxyRespone(RSP_DM_GET_VERSION, ntohs(phead->Session_ID), IncreaseIxProxyRSP_ID(), (void*)&rspData);
	}

	IxProxyRespone(RSP_DM_GET_VERSION, ntohs(phead->Session_ID), 0, NULL);
}

/*********************************************************************************************************
**  End Of File
*********************************************************************************************************/

