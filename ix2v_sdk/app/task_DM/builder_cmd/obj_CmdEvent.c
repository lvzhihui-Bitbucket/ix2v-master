/**
  ******************************************************************************
  * @file    obj_CmdShell.c
  * @author  czb
  * @version V00.01.00
  * @date    2016.5.31
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 
  
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <memory.h>
#include <dirent.h>
#include <string.h>
#include <sys/stat.h>
#include "task_IxProxy.h"
#include "tcp_server_process.h"
#include "cJSON.h"
#include "vtk_udp_stack_device_update.h"
#include "task_Event.h"
#include "obj_PublicUnicastCmd.h"
#include "obj_IX_Report.h"

int EventCmdProcess(int ip, PublicUnicastCmdData dataIn, PublicUnicastCmdData* dataOut)
{
	char *strIp, *strSn, *strDeviceNbr;
	char *eventStr = NULL;
	cJSON *event;
	cJSON *reqJson;
	int ret = 0;

	reqJson = cJSON_Parse(dataIn.data);
	if(reqJson == NULL)
	{
		return -1;
	}

	strIp = GetJsonItemString(reqJson, DM_KEY_IP_Addr);
	strSn = GetJsonItemString(reqJson, DM_KEY_MFG_SN);
	strDeviceNbr = GetJsonItemString(reqJson, DM_KEY_DeviceNbr);
	event = cJSON_GetObjectItem(reqJson, IX2V_Event);
	cJSON_AddStringToObject(event, IX2V_EventSourceIP, strIp);
	API_Event_Json(event);
	cJSON_Delete(reqJson);
	
	return ret;
}


void IxProxyEventProcess(char* pData, int len)
{
	IX_PROXY_TCP_HEAD* phead = (IX_PROXY_TCP_HEAD*)pData;
	char* cmdData = (char*)(pData + sizeof(IX_PROXY_TCP_HEAD));	
	PublicUnicastCmdData dataIn;
	
	cJSON *reqJson = NULL;
	cJSON *rspJson = NULL;
	char *strIp, *strSn, *strDeviceNbr;
	int ip;
	int result;

	SetIxProxyRSP_ID(1);
	if(GetIxProxyState() == IX_PROXY_BUSY)
	{
	    reqJson = cJSON_Parse(cmdData);
		rspJson = cJSON_Parse(cmdData);
		
		if(reqJson != NULL)
		{
			strIp = cJSON_GetStringValue(cJSON_GetObjectItem(reqJson, DM_KEY_IP_Addr));
			strSn = cJSON_GetStringValue(cJSON_GetObjectItem(reqJson, DM_KEY_MFG_SN));
			strDeviceNbr = cJSON_GetStringValue(cJSON_GetObjectItem(reqJson, DM_KEY_DeviceNbr));
			result = ProxyGetIpByPara(strIp, strDeviceNbr, strSn, &ip);
		}
		else
		{
			result = -5;
		}

		if(result < 0)
		{
			cJSON_AddStringToObject(rspJson, IX2V_Result, GetErrorCodeByResult(result));
		}
		else
		{
			dataIn.data = cmdData;
			dataIn.len = strlen(cmdData) + 1;
			if(PublicUnicastCmdRequst(ip, strSn, IxProxyWaitUdpTime, EVENT_REQ, dataIn, NULL) != 0)
			{
				cJSON_AddStringToObject(rspJson, IX2V_Result, RESULT_ERR06);
			}
			else
			{
				cJSON_AddStringToObject(rspJson, IX2V_Result, RESULT_SUCC);
			}
		}

		IxProxyRespone(RSP_EVENT_APP_TO_DEV, ntohs(phead->Session_ID), IncreaseIxProxyRSP_ID(), rspJson);
		
		cJSON_Delete(reqJson);
		cJSON_Delete(rspJson);
	}

}

int IxReportEvent(int ip, cJSON* reportJson)
{
	cJSON* sendJson;
	char* sendString = NULL;
	int ret;
	sendJson = cJSON_CreateObject();
	cJSON_AddStringToObject(sendJson, DM_KEY_DeviceNbr, GetSysVerInfo_BdRmMs());
	cJSON_AddStringToObject(sendJson, DM_KEY_MFG_SN, GetSysVerInfo_Sn());
	cJSON_AddStringToObject(sendJson, DM_KEY_IP_Addr, GetSysVerInfo_IP_by_device(GetNetDeviceNameByTargetIp(ip)));
	cJSON_AddItemToObject(sendJson, DM_KEY_Event, cJSON_Duplicate(reportJson, 1));
	
	sendString = cJSON_PrintUnformatted(sendJson);
	cJSON_Delete(sendJson);

	if(sendString)
	{
		ret = IxReportProcess(ip, 1, REPORT_Event_REQ, sendString);
		free(sendString);
	}

	if(ret != 0)
	{
		dprintf("IxReportEvent Error!!!!!!!!!!!!!!!!!!!!\n");
	}

	return ret;
}

int DX821_ReportString(char* reportString)
{
	char* ipStr = GetMyProxy_IP();
	int ret = -1;

	if(ipStr[0] && reportString)
	{

		cJSON* sendJson;
		char* sendString = NULL;
		int ret;
		int ip = inet_addr(GetMyProxy_IP());

		sendJson = cJSON_CreateObject();
		cJSON_AddStringToObject(sendJson, DM_KEY_DeviceNbr, GetSysVerInfo_BdRmMs());
		cJSON_AddStringToObject(sendJson, DM_KEY_MFG_SN, GetSysVerInfo_Sn());
		cJSON_AddStringToObject(sendJson, DM_KEY_IP_Addr, GetSysVerInfo_IP_by_device(GetNetDeviceNameByTargetIp(ip)));
		cJSON_AddStringToObject(sendJson, DM_KEY_Event, reportString);
		sendString = cJSON_PrintUnformatted(sendJson);
		ret = IxReportProcess(ip, 1, REPORT_Event_REQ, sendString);
		cJSON_Delete(sendJson);
		if(ret)
		{
			dprintf("IxReportEvent Error! %s\n", sendString);
		}
		free(sendString);
	}

	return ret;
}

/*********************************************************************************************************
**  End Of File
*********************************************************************************************************/

