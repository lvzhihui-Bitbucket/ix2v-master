/**
  ******************************************************************************
  * @file    obj_CmdFileManagement.c
  * @author  czb
  * @version V00.01.00
  * @date    2016.5.31
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 
  
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <memory.h>
#include <dirent.h>
#include <string.h>
#include <sys/stat.h>
#include "obj_CmdFileManagement.h"
#include "task_IxProxy.h"
#include "tcp_server_process.h"
#include "cJSON.h"
#include "obj_SearchIpByFilter.h"
#include "vtk_udp_stack_device_update.h"
#include "obj_PublicUnicastCmd.h"
#include "elog.h"
#include "task_IxProxyClient.h"
#include "obj_Ftp.h"
#include "obj_IoInterface.h"

#define XD_ClOUD_SEVERT_ROOT				          "/home/userota/RECORDS_ROOT/"

//{"DeviceNbr":"","IP_Addr":"192.168.243.105","MFG_SN":"admin","Operation":"GetFList","Path":"\/mnt\/nand1-2"}
/*********************************************************************************************
 ***********************************文件上传指令处理*******************************************
 ********************************************************************************************/
static int XD_UploadFileCallback(int state, FTP_FILE_STATISTICS statistics, void* data)
{
	cJSON* rspJson = NULL;
	cJSON* jsonData = (cJSON*)data;
    int reportCnt = 0;

	if(state == FTP_STATE_UP_ING)
	{
		int rate = statistics.now*100.0/statistics.total;;
		if(reportCnt != rate%5)
		{
			char info[100];
			rspJson = cJSON_Duplicate(jsonData, 1);
			cJSON_AddStringToObject(rspJson, "RESULT", "UPLOADING");
			reportCnt = rate%5;
			snprintf(info, 100, "%u/%u(%u%%)", statistics.now, statistics.total, rate);
			cJSON_AddStringToObject(rspJson, "INFO", info);
		}
	}
	else if(state == FTP_STATE_UP_ERR)
	{
		rspJson = cJSON_Duplicate(jsonData, 1);
		cJSON_AddStringToObject(rspJson, "RESULT", "UPLOAD_ERROR");

		//结束之后，需要释放内存。
		cJSON_Delete(jsonData);
	}
	else if(state == FTP_STATE_UP_OK)
	{
		rspJson = cJSON_Duplicate(jsonData, 1);
		cJSON_AddStringToObject(rspJson, "RESULT", "UPLOAD_OK");

		//结束之后，需要释放内存。
		cJSON_Delete(jsonData);
	}
	else if(state == FTP_STATE_UP_STOP)
	{
		rspJson = cJSON_Duplicate(jsonData, 1);
		cJSON_AddStringToObject(rspJson, "RESULT", "UPLOAD_STOP");

		//结束之后，需要释放内存。
		cJSON_Delete(jsonData);
	}

	if(rspJson)
	{
		char* rspString = cJSON_PrintUnformatted(rspJson);
        API_IxProxyClientTransmit(XD_APP_REPORT_TYEP_FTP, rspString);
        printf(rspString);
		free(rspString);
		cJSON_Delete(rspJson);
	}

	return 0;
}

static int XD_DownloadFileCallback(int state, FTP_FILE_STATISTICS statistics, void* data)
{
	cJSON *jsonData = (cJSON*)data;
	cJSON* rspJson = NULL;
    int reportCnt = 0;
    int rate;

	switch (state)
	{
		case FTP_STATE_PARA_ERR:
		case FTP_STATE_INIT_ERR:
		case FTP_STATE_DOWN_ERR:
		case FTP_STATE_DOWN_STOP:
        	rspJson = cJSON_Duplicate(jsonData, 1);
		    cJSON_AddStringToObject(rspJson, "RESULT", "DOWNLOAD_ERROR");
            //结束之后，需要释放内存。
		    cJSON_Delete(jsonData);
			break;

		case FTP_STATE_DOWN_OK:
            rspJson = cJSON_Duplicate(jsonData, 1);
            cJSON_AddStringToObject(rspJson, "RESULT", "DOWNLOAD_OK");

            //结束之后，需要释放内存。
            cJSON_Delete(jsonData);
			break;

		case FTP_STATE_DOWN_ING:
            rate = statistics.now*100.0/statistics.total;;
            if(reportCnt != rate%5)
            {
                char info[100];
                rspJson = cJSON_Duplicate(jsonData, 1);
                cJSON_AddStringToObject(rspJson, "RESULT", "DOWNLOADING");
                reportCnt = rate%5;
                snprintf(info, 100, "%u/%u(%u%%)", statistics.now, statistics.total, rate);
                cJSON_AddStringToObject(rspJson, "INFO", info);
            }
			break;
	
		default:
			break;
	}

	if(rspJson)
	{
		char* rspString = cJSON_PrintUnformatted(rspJson);
        API_IxProxyClientTransmit(XD_APP_REPORT_TYEP_FTP, rspString);
        printf(rspString);
		free(rspString);
		cJSON_Delete(rspJson);
	}

	return 0;
}

int FileManagementProcess(int ip, PublicUnicastCmdData dataIn, PublicUnicastCmdData* dataOut)
{
	char *strIp, *strSn, *strDeviceNbr, *strOperation, *strPath, *newPath, *fileMode;
	cJSON *reqJson;
	cJSON *rspJson;
	int ret = 0;

	reqJson = cJSON_Parse(dataIn.data);
	rspJson = cJSON_Duplicate(reqJson, 1);
	if(reqJson == NULL)
	{
		return -1;
	}

	strIp = GetJsonItemString(reqJson, DM_KEY_IP_Addr);
	strSn = GetJsonItemString(reqJson, DM_KEY_MFG_SN);
	strDeviceNbr = GetJsonItemString(reqJson, DM_KEY_DeviceNbr);
	strOperation = GetJsonItemString(reqJson, DM_KEY_Operation);
	strPath = GetJsonItemString(reqJson, DM_KEY_Path);

	if(!strcmp(strOperation, DM_KEY_GetFileList))
	{
		ret = GetFileAndDirList(strPath, cJSON_AddArrayToObject(rspJson, DM_KEY_FileList), 2);
	}
	else if(!strcmp(strOperation, DM_KEY_DeleteFile))
	{
		ret = DeleteFileProcess(strPath, NULL);
	}
	else if(!strcmp(strOperation, DM_KEY_FileMove))
	{
		newPath = cJSON_GetStringValue(cJSON_GetObjectItem(reqJson, DM_KEY_NewPath));
		ret = MoveFile(strPath, newPath);
	}
	else if(!strcmp(strOperation, DM_KEY_FileCopy))
	{
		newPath = cJSON_GetStringValue(cJSON_GetObjectItem(reqJson, DM_KEY_NewPath));
		ret = CopyFile(strPath, newPath);
	}
	else if(!strcmp(strOperation, DM_KEY_MakeDir))
	{
		ret = MakeDir(strPath);
	}
	else if(!strcmp(strOperation, DM_KEY_GetFileInfo))
	{
		ret = GetFileAndDirInfo(strPath, cJSON_AddArrayToObject(rspJson, DM_KEY_FileInfo));
	}
	else if(!strcmp(strOperation, DM_KEY_FileChmod))
	{
		fileMode = cJSON_GetStringValue(cJSON_GetObjectItem(reqJson, DM_KEY_FileMode));
		ret = FileChmod(strPath, fileMode);
	}

	if(ret == 0)
	{
		cJSON_AddStringToObject(rspJson, DM_KEY_Result, RESULT_SUCC);
	}
	else
	{
		cJSON_AddStringToObject(rspJson, DM_KEY_Result, RESULT_ERR06);
	}


	if(dataOut != NULL)
	{
		(*dataOut).data = cJSON_PrintUnformatted(rspJson);
		(*dataOut).len = strlen((*dataOut).data) + 1;
	}

	cJSON_Delete(rspJson);
	cJSON_Delete(reqJson);

	return ret;
}

int ProxyGetIpByPara(const char* strIp, const char* strDeviceNbr, const char* strSn, int *getIp)
{
	int result = 0;
	int ip = 0;

	if(strIp != NULL && strIp[0] != 0)
	{
		//本机
		if(inet_addr(strIp) == inet_addr(GetMyProxy_IP()))
		{
			if(strDeviceNbr != NULL && strDeviceNbr[0] != 0)
			{
				if(strcmp(strDeviceNbr, GetSysVerInfo_BdRmMs()))
				{
					result = -1;
				}
			}

			if(strSn != NULL && strSn[0] != 0)
			{
				if(strcmp(strSn, GetSysVerInfo_Sn()))
				{
					result = -1;
				}
			}
		}

		ip = inet_addr(strIp);
	}
	else if(strDeviceNbr != NULL && strDeviceNbr[0] != 0)
	{
		GetIpRspData getIpRsp;
		API_GetIpNumberFromNet(strDeviceNbr, NULL, NULL, IxProxyWaitUdpTime, 0, &getIpRsp);
		if(getIpRsp.cnt == 0)
		{
			result = -2;
		}
		else if(getIpRsp.cnt == 1)
		{
			ip = getIpRsp.Ip[0];
			if(strSn != NULL && strSn[0] != 0)
			{
				DeviceInfo info;
				if(API_GetInfoByIp(ip, IxProxyWaitUdpTime, &info) == 0)
				{
					if(strcmp(strSn, info.MFG_SN))
					{
						result = -1;
					}
				}
				else
				{
					result = -3;
				}
			}
		}
		else
		{
			result = -3;
		}
	} 
	else if(strSn != NULL && strSn[0] != 0)
	{
		if(API_GetIpByMFG_SN(strSn,  IxProxyWaitUdpTime, &ip, NULL) != 0)
		{
			result = -2;
		}
	} 
	if(getIp != NULL)
	{
		*getIp = ip;
	}

	return result;
}

void IxProxyFileManagementProcess(char* pData, int len)
{
	IX_PROXY_TCP_HEAD* phead = (IX_PROXY_TCP_HEAD*)pData;
	char* cmdData = (char*)(pData + sizeof(IX_PROXY_TCP_HEAD));	
	PublicUnicastCmdData dataIn, dataOut;
	
	cJSON *reqJson;
	cJSON *rspJson;
	char* rspData = NULL;
	int rspDataLen;
	char* reqData = NULL;

	char *strIp, *strSn, *strDeviceNbr;
	int ip;
	int result;

	SetIxProxyRSP_ID(1);
	
	if(GetIxProxyState() == IX_PROXY_BUSY)
	{
	    reqJson = cJSON_Parse(cmdData);
		rspJson = cJSON_Parse(cmdData);

		if(reqJson != NULL)
		{
			strIp = cJSON_GetStringValue(cJSON_GetObjectItem(reqJson, DM_KEY_IP_Addr));
			strSn = cJSON_GetStringValue(cJSON_GetObjectItem(reqJson, DM_KEY_MFG_SN));
			strDeviceNbr = cJSON_GetStringValue(cJSON_GetObjectItem(reqJson, DM_KEY_DeviceNbr));
			result = ProxyGetIpByPara(strIp, strDeviceNbr, strSn, &ip);
		}
		else
		{
			result = -5;
		}

		if(result < 0)
		{
			cJSON_AddStringToObject(rspJson, DM_KEY_Result, GetErrorCodeByResult(result));
			rspData = cJSON_PrintUnformatted(rspJson);
			IxProxyResponeNew(RSP_DM_FILE, ntohs(phead->Session_ID), IncreaseIxProxyRSP_ID(), rspData, strlen(rspData)+1);
			free(rspData);
		}
		else
		{
			char* strOperation = GetJsonItemString(reqJson, DM_KEY_Operation);
			if((!strcmp(strOperation, DM_KEY_Download) || !strcmp(strOperation, DM_KEY_Upload) || !strcmp(strOperation, DM_KEY_DownloadCancel) || !strcmp(strOperation, DM_KEY_UploadCancel)) 
				&& (ip == inet_addr(GetSysVerInfo_IP_by_device(GetNetDeviceNameByTargetIp(ip)))))
			{
				if(!strcmp(strOperation, DM_KEY_Download))
				{
					char* server = GetJsonObjectItemString(reqJson, "SERVER");
					char* source = GetJsonObjectItemString(reqJson, "SOURCE");
					char* target = GetJsonObjectItemString(reqJson, "TARGET");
					char* type = GetJsonObjectItemString(reqJson, "TYPE");
					char sourceDir[500];
					if(!strcpy(type, "cloud"))
					{
						snprintf(sourceDir, 500, "%s/%s/%s", XD_ClOUD_SEVERT_ROOT, API_Para_Read_String2(PRJ_ID), source);
					}
					else
					{
						snprintf(sourceDir, 500, "%s", source);
					}
					cJSON* callbackData = cJSON_CreateObject();
					cJSON_AddStringToObject(callbackData, "SERVER", server);
					cJSON_AddStringToObject(callbackData, "SOURCE", source);
					cJSON_AddStringToObject(callbackData, "TARGET", target);
					cJSON_AddNumberToObject(callbackData, "SESSION_ID", ntohs(phead->Session_ID));

					if(!FTP_StartDownload(server, sourceDir, target, callbackData,  XD_DownloadFileCallback))
					{
						result = -6;
						cJSON_Delete(callbackData);
					}
				}
				else if(!strcmp(strOperation, DM_KEY_DownloadCancel))
				{
					char* source = GetJsonObjectItemString(reqJson, "SOURCE");
					char* target = GetJsonObjectItemString(reqJson, "TARGET");
					if(!FTP_StopDownload(source, target))
					{
						result = -6;
					}
				}
				else if(!strcmp(strOperation, DM_KEY_Upload))
				{
					char* server = GetJsonObjectItemString(reqJson, "SERVER");
					char* source = GetJsonObjectItemString(reqJson, "SOURCE");
					char* target = GetJsonObjectItemString(reqJson, "TARGET");
					char* type = GetJsonObjectItemString(reqJson, "TYPE");
					char targetDir[500];
					if(!strcpy(type, "cloud"))
					{
						snprintf(targetDir, 500, "%s/%s/%s", XD_ClOUD_SEVERT_ROOT, API_Para_Read_String2(PRJ_ID), target);
					}
					else
					{
						snprintf(targetDir, 500, "%s", target);
					}
					cJSON* callbackData = cJSON_CreateObject();
					cJSON_AddStringToObject(callbackData, "SERVER", server);
					cJSON_AddStringToObject(callbackData, "SOURCE", source);
					cJSON_AddStringToObject(callbackData, "TARGET", target);
					cJSON_AddNumberToObject(callbackData, "SESSION_ID", ntohs(phead->Session_ID));
					//启动成功
					if(!FTP_StartUpload(server, source, targetDir, callbackData,  XD_UploadFileCallback))
					{
						result = -6;
						cJSON_Delete(callbackData);
					}
				}
				else if(!strcmp(strOperation, DM_KEY_UploadCancel))
				{
					char* server = GetJsonObjectItemString(reqJson, "SERVER");
					char* source = GetJsonObjectItemString(reqJson, "SOURCE");
					char* target = GetJsonObjectItemString(reqJson, "TARGET");
					if(!FTP_StopUpload(server, source, target))
					{
						result = -6;
					}
				}

				cJSON_AddStringToObject(rspJson, DM_KEY_Result, result == 0 ? RESULT_SUCC : RESULT_ERR06);
				rspData = cJSON_PrintUnformatted(rspJson);
				IxProxyResponeNew(RSP_DM_FILE, ntohs(phead->Session_ID), IncreaseIxProxyRSP_ID(), rspData, strlen(rspData)+1);
				free(rspData);
			}
			else
			{
				reqData = cJSON_Print(reqJson);
				dataIn.data = reqData;
				dataIn.len = strlen(reqData)+1;
				result = PublicUnicastCmdRequst(ip, strSn, IxProxyWaitUdpTime, FileManagement_REQ, dataIn, &dataOut);
				if(result != 0)
				{
					cJSON_AddStringToObject(rspJson, DM_KEY_Result, RESULT_ERR06);
				}
				free(reqData);
				reqData = NULL;

				//成功
				if(result == 0)
				{
					if(dataOut.data != NULL)
					{
						IxProxyResponeNew(RSP_DM_FILE, ntohs(phead->Session_ID), IncreaseIxProxyRSP_ID(), dataOut.data, dataOut.len);
						free(dataOut.data);
						dataOut.data = NULL;
					}
				}
				//失败
				else
				{
					rspData = cJSON_PrintUnformatted(rspJson);
					log_e("result=%s, cmdData=%s\n", GetErrorCodeByResult(result), cmdData);
					if(rspData)
					{
						IxProxyResponeNew(RSP_DM_FILE, ntohs(phead->Session_ID), IncreaseIxProxyRSP_ID(), rspData, strlen(rspData)+1);
						free(rspData);
						rspData = NULL;
					}
				}
			}
		}

		IxProxyResponeNew(RSP_DM_FILE, ntohs(phead->Session_ID), 0, NULL, 0);

		cJSON_Delete(reqJson);
		cJSON_Delete(rspJson);
	}
}

/*********************************************************************************************************
**  End Of File
*********************************************************************************************************/

