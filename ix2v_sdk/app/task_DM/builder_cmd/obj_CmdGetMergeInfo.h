/**
  ******************************************************************************
  * @file    obj_CmdGetMergeInfo.h
  * @author  cao
  * @version V00.01.00
  * @date    2016.5.31
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 
	  //cao_20170304  整个文件

#ifndef _obj_CmdGetMergeInfo_H
#define _obj_CmdGetMergeInfo_H

// Define Object Property-------------------------------------------------------
#include "obj_CmdGetAbout.h"
#include "obj_CmdGetVersion.h"
#include "obj_GetInfoByIp.h"
#include "obj_IX_Report.h"

typedef struct
{
	char	DeviceNbr[10+1];		//房号
	char	IP_Addr[15+1];			//IP地址
	char 	MFG_SN[12+1];			//MFG_SN
}MERGE_INFO_REQ_DEVICE;

typedef struct
{
	char					Result[5+1];	
	DeviceInfo				info;
	GET_ABOUT_RSP_DATA_T 	about;
	FW_VER_VERIFY_T 		version;
}MERGE_INFO_RSP_DATA;

typedef struct
{
	int						cnt;							//设备数量
	MERGE_INFO_REQ_DEVICE	dev[DEVICE_MAX_NUM];			//设备
}GET_MERGE_INFO_REQ_DATA_T;

// Define Object Function - Public----------------------------------------------


// Define Object Function - Private---------------------------------------------
char* CreateGetMergeInfoRspJsonData(MERGE_INFO_RSP_DATA* data);



#endif


