/**
  ******************************************************************************
  * @file    obj_CmdProgIpAddr.h
  * @author  cao
  * @version V00.01.00
  * @date    2016.5.31
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 
	  //cao_20170304  整个文件

#ifndef _obj_CmdProgIpAddr_H
#define _obj_CmdProgIpAddr_H


// Define Object Property-------------------------------------------------------


typedef struct
{
	char	MFG_SN[12+1];			
	char	IP_ASSIGNED[15+1];		//IP策略
	char	IP_ADDR[15+1];			
	char	IP_MASK[15+1];			
	char	IP_GATEWAY[15+1];
}PROG_IP_ADDR_REQ_DATA_T;


typedef struct
{
	char	Result[5+1];			//修改结果
	char	MFG_SN[12+1];			//MFG_SN
}PROG_IP_ADDR_RSP_DATA_T;


// Define Object Function - Public----------------------------------------------

char* CreateProgIpAddrRspJsonData(PROG_IP_ADDR_RSP_DATA_T* pData);


// Define Object Function - Private---------------------------------------------



#endif


