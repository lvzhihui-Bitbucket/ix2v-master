
#include "iperf_udp_client.h"
#include "utility.h"

#define MAX_HOST_STR_LEN	30

typedef struct
{
	IPERF_BASE_INS_T		base;
	char					host[MAX_HOST_STR_LEN];
	short					port;
	int						period;
	int 					interval;
	int						msg_mode;
	msg_callback_func		process;
} IPERF_UDP_CLIENT_INS_T;

IPERF_UDP_CLIENT_INS_T one_iperf_udp_client_ins;

void* iperf_udp_client_callback(int type, char* pbuf, int len)
{
	char filter_buf[250];
	if( one_iperf_udp_client_ins.process != NULL )
	{
		printf("iperf_udp_client_callback,type[%d],len[%d]\n",type,len);
		iperf_message_anaylizer(one_iperf_udp_client_ins.period, pbuf, len, IPERF_MSG_UDP_SERVER_BAND_LOSE_PER, filter_buf, 250);
		(*one_iperf_udp_client_ins.process)(type, filter_buf, strlen(filter_buf));
	}
}

/*****************************************************************************************************************
@function:
	api_iperf_udp_client_start_without_apply
@parameters:
	server:	    server host			eg: vdpconnect.com, 47.91.88.33
	port:	    server port			eg: 6789
	period:		send time			eg: 3 (max 30)
	interval:	send interval		eg: 1 (default 1)
	msg_mode:	callback msg mode	eg: 0/bandwidth message per interval, 1/average bandwidth message in period
	msg_cb:	    message callback process
@return:
	0/ok, -1/error, 1/busy
******************************************************************************************************************/
int api_iperf_udp_client_start_without_apply( char* server, short port, int period, int interval, int msg_mode, msg_callback_func msg_cb )
{
	char iperf_cmd[200];
	char iperf_host[30];
    char iperf_port[10];
    char iperf_peri[10];
    char iperf_inte[10];
    // init
	memset(one_iperf_udp_client_ins.host,0,sizeof(one_iperf_udp_client_ins.host));
	strcpy(one_iperf_udp_client_ins.host,server);
	one_iperf_udp_client_ins.port 		= port;
	one_iperf_udp_client_ins.period		= period;
	one_iperf_udp_client_ins.interval	= interval;
	one_iperf_udp_client_ins.msg_mode	= msg_mode;
    one_iperf_udp_client_ins.process 	= msg_cb;

	snprintf(iperf_host,30,"-c %s ",one_iperf_udp_client_ins.host);
	snprintf(iperf_port,10,"-p %d ",one_iperf_udp_client_ins.port);
	snprintf(iperf_peri,10,"-t %d ",one_iperf_udp_client_ins.period);
	snprintf(iperf_inte,10,"-i %d ",one_iperf_udp_client_ins.interval);
	memset( iperf_cmd, 0, sizeof(iperf_cmd) );
	strcat( iperf_cmd, "iperf ");
	strcat( iperf_cmd, iperf_host );	// -c
	strcat( iperf_cmd, "-u -b 100M ");	// -u
	strcat( iperf_cmd, iperf_port );	// -p
	strcat( iperf_cmd, iperf_peri );	// -t
	strcat( iperf_cmd, iperf_inte );	// -i
	
	printf( "api_iperf_udp_client_start[%s]----\n",iperf_cmd);

    // start
	if( iperf_base_start(&one_iperf_udp_client_ins.base,iperf_cmd,iperf_udp_client_callback) == -1 )
	{
        printf( "api_iperf_client_start start fail !!!\n"); 
        return -1;
	}
    printf( "api_iperf_client_start start ok !!!\n"); 
    return 0;
}

/*****************************************************************************************************************
@function:
	api_iperf_udp_client_start
@parameters:
	server:	    server host			eg: ip addr(int type)
	port:	    server port			eg: 6789
	period:		send time			eg: 3 (max 30)
	interval:	send interval		eg: 1 (default 1)
	msg_mode:	callback msg mode	eg: 0/bandwidth message per interval, 1/average bandwidth message in period
	msg_cb:	    message callback process
@return:
	0/ok, -1/error, 1/busy
******************************************************************************************************************/
int api_iperf_udp_client_start( int server, short port, int period, int interval, int msg_mode, msg_callback_func msg_cb )
{
	return api_iperf_udp_client_start_without_apply(server,port, period, interval, msg_mode, msg_cb );
}

/*****************************************************************************************************************
@function:
	api_iperf_udp_client_stop
@parameters:
    none
@return:
	0/ok, -1/error
******************************************************************************************************************/
int api_iperf_udp_client_stop( void )
{
    return iperf_base_stop(&one_iperf_udp_client_ins.base);
}