
#include "utility.h"
#include "iperf_utility.h"
#include "obj_SYS_VER_INFO.h"
#include "vtk_udp_stack_c5_ipc_cmd.h"
#include "cJSON.h"

#define BUFFER_CNT_MAX	                4
#define BUFFER_LEN_MAX	                50

static int kill_task(char* task_name)
{
	int account, timeCnt;
	int pid;
	int states;

	for(account = 0, timeCnt = 0; timeCnt < 50; timeCnt++)
	{
		pid = getpid_bycmdline(task_name);
		if(pid > 0)
		{
			if(waitpid(pid, &states, WNOHANG) == 0)
			{
				if(kill(pid, kill(pid, SIGTERM)) == 0)
				{
					dprintf("waiting killed %d\n", pid);
					while(waitpid(pid, &states, 0) == -1 && errno == EINTR);
					account++;
					dprintf("kill %d\n", pid);
				}
			}
		}
		else if(pid == 0)
		{
			usleep(100*1000);
		}
		else if(pid < 0)
		{
			break;
		}
	}

	dprintf("total %d tasks be killed!\n", account);

	return account;
}

int iperf_wait_over( IPERF_BASE_INS_T* pins )
{
    // wait last over
	int wait_to = 5;

	while(pins->status == IPERF_BASE_STATUS_INIT)
	{
		usleep(1000*1000);
		if(--wait_to <= 0)
		{
			printf( "iperf_wait_over error !!!\n"); 
			return 0;
		}
	}

	if(pins->status == IPERF_BASE_STATUS_RUN)
	{
		pins->status = IPERF_BASE_STATUS_OVER;
	}

	if(pins->iperf_pid)
	{
		pthread_join(pins->iperf_pid, NULL);
		pins->iperf_pid = NULL;
	}
	printf( "iperf_wait_over success !!!\n"); 
	return 1;
}

static void callback_for_notify(IPERF_BASE_INS_T* pins, char* pbuf, int len )
{
    if( pins->process != NULL )
    {
        (*pins->process)(0,(void*)pbuf,len);
    }
}

#define IPERF_MAX_LINE_CHARS    1000
void* task_iperf_base_process(void* arg )
{
	IPERF_BASE_INS_T* pins = (IPERF_BASE_INS_T*)arg;
	char readBuf[IPERF_MAX_LINE_CHARS];
	char iperf_line[IPERF_MAX_LINE_CHARS];
	int pid;
	FILE* stream[2];
	int readLen;
	int read_cnt = 0;
	char tempString[IPERF_MAX_LINE_CHARS*2];
	int readIndex;
	int iperf_line_index = 0;

	printf( "task_iperf_base_process start[%s]----\n",pins->cmd);
	pins->status = IPERF_BASE_STATUS_RUN;

	cJSON * iperfJsonCmd = CmdLineToCjson(pins->cmd);

	pid = mypopen2("r", stream, iperfJsonCmd);
	cJSON_Delete(iperfJsonCmd);
	
	while(pid > 0 && pins->status==IPERF_BASE_STATUS_RUN)
	{
		readLen = myfread_oneline(stream, readBuf, IPERF_MAX_LINE_CHARS, 1);
		//printf("readBuf=%s, len=%d,pid=%d\n",readBuf, readLen, getpid_bycmdline("iperf -c"));
		if(readLen > 0)
		{
			if(iperf_line_index)
			{
				memcpy(tempString, iperf_line, iperf_line_index);
			}
			memcpy(tempString+iperf_line_index, readBuf, readLen);
			readLen += iperf_line_index;

			for(readIndex = 0, iperf_line_index = 0; readIndex < readLen; readIndex++)
			{
				iperf_line[iperf_line_index] = tempString[readIndex];
				if(iperf_line[iperf_line_index] == '\n')
				{
					iperf_line[iperf_line_index] = 0;
					callback_for_notify(pins, iperf_line, iperf_line_index+1);
					iperf_line_index = 0;
				}
				else
				{
					iperf_line_index++;
				}
			}
		}
		else if(readLen == -1 && strstr(pins->cmd, "-c") != NULL && read_cnt==0)
		{
			callback_for_notify(pins, "connect failed: Connection refused", strlen("connect failed: Connection refused") );
			break;
		}
		if(getpid_bycmdline("iperf -c")<=0)
			break;
		//usleep(5*1000);
		read_cnt++;
	}

	kill_task("iperf");
	mypclose(stream, &pid);
		sleep(2);
	  if( pins->process != NULL )
	  {
	        (*pins->process)(1,NULL,0);
	   }

    pins->status = IPERF_BASE_STATUS_IDLE;

	printf( "task_iperf_base_process stop[%s]----\n",pins->cmd);

    return NULL;
}

/*****************************************************************************************************************
@function:
	iperf_base_start
@parameters:
    pins:       one instance ptr
    cmd:        shell command
	msg_cb:	    message callback process

@return:
	0/ok, -1/error, 1/busy
******************************************************************************************************************/
int iperf_base_start(IPERF_BASE_INS_T* pins, char* cmd, msg_callback_func msg_cb )
{
	// wait idle
	iperf_wait_over(pins);

    // init
    pins->status    = IPERF_BASE_STATUS_INIT;
    memset( pins->cmd, 0, sizeof(pins->cmd));
    strcpy(pins->cmd,cmd);
    pins->process   = msg_cb;

    // start
	pthread_attr_init( &pins->iperf_attr );
	pthread_attr_setdetachstate(&pins->iperf_attr,PTHREAD_CREATE_DETACHED);
 	if( pthread_create(&pins->iperf_pid, &pins->iperf_attr, task_iperf_base_process, (void*)pins) )
	{
        pins->status = IPERF_BASE_STATUS_IDLE;
        printf( "api_iperf_client_start api_iperf_base_start fail !!!\n"); 
        return -1;
	}
    printf( "api_iperf_base_start start ok !!!\n"); 
    return 0;
}

/*****************************************************************************************************************
@function:
	iperf_base_stop
@parameters:
    pins:       one instance ptr
@return:
	0/ok, -1/error
******************************************************************************************************************/
int iperf_base_stop(IPERF_BASE_INS_T* pins)
{
    //kill_task("iperf");
	// wait idle
	iperf_wait_over(pins);
    printf( "api_iperf_base_stop start ok !!!\n"); 
    return 0;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

int request_iperf_client_start(int target_ip, int server, short port, int times, int interval)
{
	IPERF_UDP_REQ_T request;
	IPERF_UDP_RSP_T response = {0};
	unsigned int rlen = sizeof(IPERF_UDP_RSP_T);
	request.IpAddr 			= target_ip;
	request.subAddr 		= 0;
	request.iperf_server 	= server;
	request.iperf_port	 	= port;
	request.iperf_times		= times;
	request.iperf_interval	= interval;

	if(api_udp_c5_ipc_send_req(target_ip, CMD_IPERF_START_REQ, (char*)&request, sizeof(IPERF_UDP_REQ_T), (char*)&response, &rlen) == 0)
	{
		printf("!*!*!*!&!!!!!!ip = %08x saddr = %04x result = %d\n",response.IpAddr,response.subAddr,response.result);
		if(request.IpAddr ==  response.IpAddr  && response.result == 0)
		{
			return 0;
		}
		else
		{
			return 1;
		}
	}
	else
	{
		return 2;	//ipdevice offline 
	}
}

void response_iperf_client_start(void *pdata)
{	
	char ip_str[20];
	UDP_MSG_TYPE* pudp = (UDP_MSG_TYPE*)pdata;
	IPERF_UDP_REQ_T *pRequest = (IPERF_UDP_REQ_T*)pudp->pbuf;
	IPERF_UDP_RSP_T response;

	memcpy(&response, pRequest, sizeof(IPERF_UDP_RSP_T));		

	if(pRequest->IpAddr == GetLocalIpByDevice(GetNetDeviceNameByTargetIp(pRequest->IpAddr)))
	{
		// response
		response.result = 0;		
		api_udp_c5_ipc_send_rsp( pRequest->iperf_server, pudp->cmd|0x80, pudp->id, (char*)&response, sizeof(IPERF_UDP_RSP_T));
		usleep(300*100);
		// start iperf client
		memset( ip_str, 0, 20);	
		ConvertIpInt2IpStr( pRequest->iperf_server,ip_str );

		printf("api_iperf_udp_client_start,server[%s]\n",ip_str);

		api_iperf_udp_client_start(ip_str,pRequest->iperf_port,pRequest->iperf_times,pRequest->iperf_interval,0,NULL);
	}
	else
	{
		response.result = 1;		
		api_udp_c5_ipc_send_rsp( pRequest->iperf_server, pudp->cmd|0x80, pudp->id, (char*)&response, sizeof(IPERF_UDP_RSP_T));
	}
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
tcp_client:
  3] 29.0-30.0 sec  4.63 MBytes  38.8 Mbits/sec
[  3]  0.0-30.0 sec   150 MBytes  42.0 Mbits/sec
udp server:
[  3] 28.0-29.0 sec   128 KBytes  1.05 Mbits/sec   0.230 ms    0/   89 (0%)
[  3] 29.0-30.0 sec   128 KBytes  1.05 Mbits/sec   0.007 ms    0/   89 (0%)
[  3]  0.0-30.0 sec  3.75 MBytes  1.05 Mbits/sec   0.087 ms    0/ 2677 (0%)
*/
#define	IPERF_MSG_TCP_CLIENT_BANDWIDTH_PER		1		// Mbits/sec
#define	IPERF_MSG_TCP_CLIENT_BANDWIDTH_AVG		2		// Mbits/sec
#define	IPERF_MSG_UDP_SERVER_BANDWIDTH_PER		3		// Mbits/sec
#define	IPERF_MSG_UDP_SERVER_BANDWIDTH_AVG		4		// Mbits/sec
#define	IPERF_MSG_UDP_SERVER_LOSE_RATE_PER		5		// (0%)
#define	IPERF_MSG_UDP_SERVER_LOSE_RATE_AVG		6		// (0%)
#define	IPERF_MSG_UDP_SERVER_BAND_LOSE_PER		7		// Mbits/sec (0%)
#define	IPERF_MSG_UDP_SERVER_BAND_LOSE_AVG		8		// Mbits/sec (0%)

int iperf_message_get_band( char* pinbuf, char* poutbuf )
{
	int strlen;
	char *strpos1;
	char *strpos2;
	strpos1 = strstr(pinbuf,"ytes");
	strpos2 = strstr(pinbuf,"/sec");	
	strlen = strpos2 - strpos1 -2;
	if( strpos1 != NULL && strpos2 != NULL )
	{
		strncpy( poutbuf, strpos1+4, strlen);
		return strlen;
	}
	else
		return 0;
}

int iperf_message_get_time( char* pinbuf, char* poutbuf )
{
	int strlen;
	char *strpos1;
	char *strpos2;
	strpos1 = strstr(pinbuf,"]");
	strpos2 = strstr(pinbuf," sec");
	strlen = strpos2 - strpos1;
	if( strpos1 != NULL && strpos2 != NULL )
	{
		strncpy( poutbuf, strpos1+2, strlen);
		return strlen;
	}
	else
		return 0;
}

int iperf_message_get_lose( char* pinbuf, char* poutbuf )
{
	int strlen;
	char *strpos1;
	char *strpos2;
	if(strstr(pinbuf, "default"))
	{
		return 0;
	}
	
	strpos1 = strstr(pinbuf,"(");
	strpos2 = strstr(pinbuf,")");
	strlen = strpos2 - strpos1+1;
	if( strpos1 != NULL && strpos2 != NULL )
	{
		strncpy( poutbuf, strpos1, strlen);
		return strlen;
	}
	else
		return 0;
}

int iperf_message_get_connect_failed( char* pinbuf, char* poutbuf )
{
	char *strpos1;
	strpos1 = strstr(pinbuf,"connect failed");
	if( strpos1 != NULL )
	{
		strncpy( poutbuf, strpos1, strlen("connect failed"));
		return strlen("connect failed");
	}
	else
		return 0;
}

int iperf_message_anaylizer( int total, char* pinbuf, int in_len, int out_type, char* poutbuf, int out_len )
{
	char strtime[30];
	char strband[30];
	char strlose[30];

	memset( strtime, 0, 30);
	memset( strband, 0, 30);
	memset( strlose, 0, 30);
	memset( poutbuf, 0, out_len);

	if( out_type == IPERF_MSG_TCP_CLIENT_BANDWIDTH_PER || out_type == IPERF_MSG_TCP_CLIENT_BANDWIDTH_AVG )
	{
		if( iperf_message_get_connect_failed(pinbuf,strlose) != 0 )
		{
			strcat(poutbuf,strlose);
			//strcat(poutbuf,"               ");
			printf("connect status,outbuf[%s]\n",poutbuf);
		}
		else
		{
			iperf_message_get_time(pinbuf,strtime);
			iperf_message_get_band(pinbuf,strband);

			strcat(poutbuf,strtime);
			strcat(poutbuf,strband);
			//strcat(poutbuf,"   ");

			//if( strlen(poutbuf) < 10 ) strcat(poutbuf,"                 ");

			printf("time[%s],band[%s],outbuf[%s]\n",strtime,strband,poutbuf);
		}
	}
	else if( out_type == IPERF_MSG_UDP_SERVER_BAND_LOSE_PER || out_type == IPERF_MSG_UDP_SERVER_BAND_LOSE_AVG )
	{
			iperf_message_get_time(pinbuf,strtime);
			iperf_message_get_band(pinbuf,strband);
			iperf_message_get_lose(pinbuf,strlose);

			strcat(poutbuf,strtime);
			strcat(poutbuf,strband);
			strcat(poutbuf,strlose);
			//strcat(poutbuf,"   ");

			//if( strlen(poutbuf) < 10 ) strcat(poutbuf,"                 ");

			printf("time[%s],band[%s],lose[%s], outbuf[%s]\n",strtime,strband,strlose,poutbuf);
	}

	return 0;
}


