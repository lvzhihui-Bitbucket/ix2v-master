
#ifndef _IPERF_UDP_SERVER_H_
#define _IPERF_UDP_SERVER_H_

#include "iperf_utility.h"

/*****************************************************************************************************************
@function:
	api_iperf_udp_server_start
@parameters:
	target:		client target
	port:	    server port			eg: 6789
	period:		service period		eg: max 30
	interval:	send interval		eg: 1 (default 1)
	msg_mode:	message filter type
	msg_cb:	    message callback process    
@return:
	0/ok, -1/error, 1/busy
******************************************************************************************************************/
int api_iperf_udp_server_start( int target, short port, int period, int interval, int msg_mode, msg_callback_func msg_cb );

/*****************************************************************************************************************
@function:
	api_iperf_udp_server_stop
@parameters:
    none
@return:
	0/ok, -1/error
******************************************************************************************************************/
int api_iperf_udp_server_stop( void );

#endif
