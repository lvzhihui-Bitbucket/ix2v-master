
#include "iperf_udp_server.h"

#define MAX_HOST_STR_LEN	30

typedef struct
{
	IPERF_BASE_INS_T		base;
	short					port;
	int						period;
	int 					interval;
	int						msg_mode;
	msg_callback_func		process;
} IPERF_UDP_SERVER_INS_T;

IPERF_UDP_SERVER_INS_T one_iperf_udp_server_ins;

void* iperf_udp_server_callback(int type, char* pbuf, int len)
{
	char filter_buf[251];
	int bufLen = 250;
	if( one_iperf_udp_server_ins.process != NULL )
	{
		printf("iperf_udp_server_callback,type[%d],len[%d],pbuf =%s\n",type,len, pbuf);
		iperf_message_anaylizer(one_iperf_udp_server_ins.period, pbuf, len, IPERF_MSG_UDP_SERVER_BAND_LOSE_PER, filter_buf, bufLen);
		if((bufLen = strlen(filter_buf)) > 0)
		{
			(*one_iperf_udp_server_ins.process)(type, filter_buf, bufLen);
		}
	}
}

/*****************************************************************************************************************
@function:
	api_iperf_udp_server_start
@parameters:
	target:		client target
	port:	    server port			eg: 6789
	period:		service period		eg: max 30
	interval:	send interval		eg: 1 (default 1)
	msg_mode:	message filter type
	msg_cb:	    message callback process    
@return:
	0/ok, -1/error, 1/busy
******************************************************************************************************************/
int api_iperf_udp_server_start( int target, short port, int period, int interval, int msg_mode, msg_callback_func msg_cb )
{
	int result;
	int server;
	char ip_str[20];
	char iperf_server[30];
	char iperf_cmd[200];
    char iperf_port[20];
    char iperf_inte[20];

	// get server according to the client ip addr
	server = GetLocalIpByDevice(GetNetDeviceNameByTargetIp(target));
	memset( ip_str, 0, 20);	
	ConvertIpInt2IpStr( server,ip_str );

	printf("api_iperf_udp_server_start,server[%s]\n",ip_str);

	result = request_iperf_client_start( target, server, port, period, interval);
	if( result == 2 )		// offline
	{
		printf( "request_iperf_client_start OFFLINE!\n");
		return 2;
	}
	else if( result == 1 )	// error
	{
		printf( "request_iperf_client_start ERROR!\n");
		return 1;
	}

    // init
	one_iperf_udp_server_ins.port 		= port;
	one_iperf_udp_server_ins.period		= period;
	one_iperf_udp_server_ins.interval	= interval;
	one_iperf_udp_server_ins.msg_mode	= msg_mode;
    one_iperf_udp_server_ins.process 	= msg_cb;

	snprintf(iperf_port,20,"-p %d ",one_iperf_udp_server_ins.port);
	snprintf(iperf_inte,20,"-i %d ",one_iperf_udp_server_ins.interval);
	snprintf(iperf_server,30,"-B %s ",ip_str);

	memset( iperf_cmd, 0, sizeof(iperf_cmd) );
	strcat( iperf_cmd, "iperf -s -u " );
	strcat( iperf_cmd, iperf_port );	// -p
	strcat( iperf_cmd, iperf_inte );	// -i
	strcat( iperf_cmd, iperf_server );	// -B
	
	printf( "api_iperf_udp_server_start[%s]----\n",iperf_cmd);

    // start
	if( iperf_base_start(&one_iperf_udp_server_ins.base,iperf_cmd,iperf_udp_server_callback) == -1 )
	{
        printf( "api_iperf_udp_server_start start fail !!!\n"); 
        return -1;
	}
    printf( "api_iperf_udp_server_start ok !!!\n"); 
    return 0;
}

/*****************************************************************************************************************
@function:
	api_iperf_udp_server_stop
@parameters:
    none
@return:
	0/ok, -1/error
******************************************************************************************************************/
int api_iperf_udp_server_stop( void )
{
    return iperf_base_stop(&one_iperf_udp_server_ins.base);
}
