
#include "iperf_tcp_client.h"
#include "task_Shell.h"
#include "cJSON.h"

#define MAX_HOST_STR_LEN	30

typedef struct
{
	IPERF_BASE_INS_T		base;
	char					host[MAX_HOST_STR_LEN];
	short					port;
	int						period;
	int 					interval;
	int						msg_mode;
	msg_callback_func		process;
} IPERF_TCP_CLIENT_INS_T;

IPERF_TCP_CLIENT_INS_T one_iperf_tcp_client_ins;

void* iperf_tcp_client_callback(int type, char* pbuf, int len)
{
	char filter_buf[250]={0};
	if( one_iperf_tcp_client_ins.process != NULL )
	{
		printf("iperf_tcp_client_callback,type[%d],len[%d]\n",type,len);
		if(type==0)
			iperf_message_anaylizer(one_iperf_tcp_client_ins.period, pbuf, len, IPERF_MSG_TCP_CLIENT_BANDWIDTH_PER, filter_buf, 250);
		(*one_iperf_tcp_client_ins.process)(type, filter_buf, strlen(filter_buf));
	}
}

/*****************************************************************************************************************
@function:
	api_iperf_tcp_client_start
@parameters:
	server:	    server host			eg: vdpconnect.com, 47.91.88.33
	port:	    server port			eg: 6789
	period:		send time			eg: 3 (max 30)
	interval:	send interval		eg: 1 (default 1)
	msg_mode:	callback msg mode	eg: 0/bandwidth message per interval, 1/average bandwidth message in period
	msg_cb:	    message callback process
@return:
	0/ok, -1/error, 1/busy
******************************************************************************************************************/
int api_iperf_tcp_client_start( char* server, short port, int period, int interval, int msg_mode, msg_callback_func msg_cb )
{
	char iperf_cmd[200];
	char iperf_host[30];
    char iperf_port[10];
    char iperf_peri[10];
    char iperf_inte[10];
    // init
	memset(one_iperf_tcp_client_ins.host,0,sizeof(one_iperf_tcp_client_ins.host));
	strcpy(one_iperf_tcp_client_ins.host,server);
	one_iperf_tcp_client_ins.port 		= port;
	one_iperf_tcp_client_ins.period		= period;
	one_iperf_tcp_client_ins.interval	= interval;
	one_iperf_tcp_client_ins.msg_mode	= msg_mode;
    one_iperf_tcp_client_ins.process 	= msg_cb;

	snprintf(iperf_host,30,"-c %s ",one_iperf_tcp_client_ins.host);
	snprintf(iperf_port,10,"-p %d ",one_iperf_tcp_client_ins.port);
	snprintf(iperf_peri,10,"-t %d ",one_iperf_tcp_client_ins.period);
	snprintf(iperf_inte,10,"-i %d ",one_iperf_tcp_client_ins.interval);
	memset( iperf_cmd, 0, sizeof(iperf_cmd) );
	strcat( iperf_cmd, "iperf " );
	strcat( iperf_cmd, iperf_host );	// -c
	strcat( iperf_cmd, iperf_port );	// -p
	strcat( iperf_cmd, iperf_peri );	// -t
	strcat( iperf_cmd, iperf_inte );	// -i
	
	printf( "api_iperf_tcp_client_start[%s]----\n",iperf_cmd);

    // start
	if( iperf_base_start(&one_iperf_tcp_client_ins.base,iperf_cmd,iperf_tcp_client_callback) == -1 )
	{
        printf( "api_iperf_client_start start fail !!!\n"); 
        return -1;
	}
    printf( "api_iperf_client_start start ok !!!\n"); 
    return 0;
}

/*****************************************************************************************************************
@function:
	api_iperf_tcp_client_stop
@parameters:
    none
@return:
	0/ok, -1/error
******************************************************************************************************************/
int api_iperf_tcp_client_stop( void )
{
    return iperf_base_stop(&one_iperf_tcp_client_ins.base);
}

int api_iperf_client_start_json(cJSON *cmd,msg_callback_func msg_cb )
{
	char iperf_cmd[200];
	char iperf_host[30];
    char iperf_port[10];
    char iperf_peri[10];
    char iperf_inte[10];
	char buff[100];
	int temp;
	cJSON *cmd_str_array,*item;
	int i;
    // init
	memset(one_iperf_tcp_client_ins.host,0,sizeof(one_iperf_tcp_client_ins.host));
	#if 0
	strcpy(one_iperf_tcp_client_ins.host,server);
	one_iperf_tcp_client_ins.port 		= port;
	one_iperf_tcp_client_ins.period		= period;
	one_iperf_tcp_client_ins.interval	= interval;
	one_iperf_tcp_client_ins.msg_mode	= msg_mode;
	#endif
    one_iperf_tcp_client_ins.process 	= msg_cb;
	strcpy( iperf_cmd, "iperf " );
	
	if((cmd_str_array=cJSON_GetObjectItemCaseSensitive(cmd,"CMD_STR"))!=NULL)
	{
		#if 0
		char *str=cJSON_Print(cmd);
		ShellCmdPrintf(str);
		free(str);
		for(i=0;i<cJSON_GetArraySize(cmd_str_array);i++)
		{
			strcat(iperf_cmd," ");
			item=cJSON_GetArrayItem(cmd_str_array,i);
			char *str=cJSON_Print(item);
			ShellCmdPrintf(str);
			free(str);
		#endif
			strcat(iperf_cmd,cmd_str_array->valuestring);
		//}
	}
	else
	{
		
		
		if(GetJsonDataPro(cmd,"PROTOCOL",buff))
		{
			if(strcmp(buff,"udp")==0)
			{
				strcat(iperf_cmd,"-u ");
				strcat(iperf_cmd,"-b 100000000 ");
			}
			else
			{
				;
			}
		}
		else
		{
			;
		}

		if(GetJsonDataPro(cmd,"NET",buff))
		{
			if(strcmp(buff,"wlan")==0)
			{
				my_inet_ntoa(GetLocalIpByDevice("wlan0"), buff);
				snprintf(iperf_host,30,"-B %s ",buff);
				strcat(iperf_cmd,iperf_host);
			}
			else if(strcmp(buff,"lan")==0)
			{
				my_inet_ntoa(GetLocalIpByDevice("eth0"), buff);
				snprintf(iperf_host,30,"-B %s ",buff);
				strcat(iperf_cmd,iperf_host);
			}
		}
		else
		{
			;
		}
		
		
		if(GetJsonDataPro(cmd,"HOST",buff))
		{
			snprintf(iperf_host,30,"-c %s ",buff);
		}
		else
		{
			snprintf(iperf_host,30,"-c %s ","47.91.88.33");
		}
		strcat( iperf_cmd, iperf_host );

		if(GetJsonDataPro(cmd,"PORT",&temp))
		{
			snprintf(iperf_port,10,"-p %d ",temp);
		}
		else
		{
			snprintf(iperf_port,10,"-p %d ",6789);
		}
		strcat( iperf_cmd, iperf_port );	// -p

		if(GetJsonDataPro(cmd,"TIME",&temp))
		{
			snprintf(iperf_peri,10,"-t %d ",temp);
		}
		else
		{
			snprintf(iperf_peri,10,"-t %d ",10);
		}
		strcat( iperf_cmd, iperf_peri);	// -p

		if(GetJsonDataPro(cmd,"INTERVAL",&temp))
		{
			snprintf(iperf_inte,10,"-i %d ",temp);
		}
		else
		{
			snprintf(iperf_inte,10,"-i %d ",1);
		}
		strcat( iperf_cmd, iperf_inte);	// -p
	}

	
	printf( "api_iperf_tcp_client_start[%s]----\n",iperf_cmd);

    // start
	if( iperf_base_start(&one_iperf_tcp_client_ins.base,iperf_cmd,iperf_tcp_client_callback) == -1 )
	{
        printf( "api_iperf_client_start start fail !!!\n"); 
        return -1;
	}
    printf( "api_iperf_client_start start ok !!!\n"); 
    return 0;
}
void iperf_bandwidth_shell_print(int type, void* pbuf,int len )
{
	//printf("%s\n",pbuf);
	ShellCmdPrintf((char *)pbuf);	
}
int ShellCmd_Iperf(cJSON *cmd)
{
	char *para=GetShellCmdInputString(cmd, 1);
	cJSON *jpara;
	char cmd_line[200];
	
	
	if(para == NULL || !strcmp(para, ""))
	{
		ShellCmdPrintf("start test by defauft");
		api_iperf_client_start_json(NULL,iperf_bandwidth_shell_print);
		return 1;
	}
	else
	{
		if(strcmp(para,"stop")==0)
		{
			api_iperf_tcp_client_stop();
			ShellCmdPrintf("stop iperf");
			return 0;
		}
		else if(strcmp(para,"server")==0)
		{
			api_iperf_tcp_client_stop();
			jpara=cJSON_CreateObject();
			strcpy(cmd_line,"-s -p 6789 -i 1");
			
			cJSON_AddStringToObject(jpara,"CMD_STR",cmd_line);
			api_iperf_client_start_json(jpara,iperf_bandwidth_shell_print);
			ShellCmdPrintf("start test by server");
		}
		else if(strcmp(para,"set_route")==0)
		{
			//char gw[20] = {0};
			int intGw;
			para=GetShellCmdInputString(cmd, 2);
			if(para==NULL)
			{
				ShellCmdPrintf("have no interface");
				
				return 0;
			}

			#if 1
			if(strcmp(para,"LAN")==0)
			{
				if(API_Change_SipNetwork("LAN")==0)
					ShellCmdPrintf("set default route ok");
				else
					ShellCmdPrintf("set default route fail");
				#if 0
				DelDefaultGatewayByDevice("wlan0");
				DelDefaultGatewayByDevice("usb0");
				//intGw = GetLocalGatewayByDevice("eth0");
				//my_inet_ntoa(intGw,gw);
				if(SetDefaultGatewayByDevice("eth0",get_lan_gw_save())==0)
				{
					ShellCmdPrintf("set default route to eth0");
				}
				else
				{
					ShellCmdPrintf("set default route fail");
				}
				#endif
			}
			else if(strcmp(para,"WLAN")==0)
			{
				if(API_Change_SipNetwork("WLAN")==0)
					ShellCmdPrintf("set default route ok");
				else
					ShellCmdPrintf("set default route fail");
				#if 0
				DelDefaultGatewayByDevice("eth0");
				DelDefaultGatewayByDevice("usb0");
				//intGw = GetLocalGatewayByDevice("wlan0");
				//my_inet_ntoa(intGw,gw);
				if(SetDefaultGatewayByDevice("wlan0",get_wlan_gw_save())==0)
				{
					ShellCmdPrintf("set default route to wlan0");
				}
				else
				{
					ShellCmdPrintf("set default route fail");
				}
				#endif
			}
			else if(strcmp(para,"4G")==0)
			{
				if(API_Change_SipNetwork("4G")==0)
					ShellCmdPrintf("set default route ok");
				else
					ShellCmdPrintf("set default route fail");
			#if 0
				DelDefaultGatewayByDevice("eth0");
				DelDefaultGatewayByDevice("wlan0");
				//if(SetDefaultGatewayByDevice("usb0", my_inet_ntoa2(GetLocalGatewayByDevice("usb0")))==0)
				if(SetDefaultGatewayByDevice("usb0", "192.168.0.1")==0)
				{
					ShellCmdPrintf("set default route to usb0");
				}
				else
				{
					ShellCmdPrintf("set default route fail");
				}
				#endif
			}
			else
			{
				ShellCmdPrintf("unkown interface");
			}
			#endif
			return 0;
		}
		else if(strcmp(para,"test_tcp")==0)
		{
			para=GetShellCmdInputString(cmd, 2);
			if(para==NULL)
			{
				ShellCmdPrintf("have no interface");
				
				return 0;
			}
			if(check_connect_to_server(para,"47.91.88.33")==0)
				ShellCmdPrintf("tcp connect %s ok",para);
			else
				ShellCmdPrintf("tcp connect %s fail",para);

			return 0;
		}
		else if(para[0]!='{')
		{
			int i=2;
			cJSON *cmd_str;
			jpara=cJSON_CreateObject();
			strcpy(cmd_line,para);
			while((para=GetShellCmdInputString(cmd, i++)) && strcmp(para, ""))
			{
				strcat(cmd_line," ");
				strcat(cmd_line,para);
			}
			cJSON_AddStringToObject(jpara,"CMD_STR",cmd_line);
			api_iperf_client_start_json(jpara,iperf_bandwidth_shell_print);
			ShellCmdPrintf("start test by cmd line");
		}
		else
		{
			jpara=cJSON_Parse(para);
			if(jpara!=NULL)
			{
				api_iperf_client_start_json(jpara,iperf_bandwidth_shell_print);
				ShellCmdPrintf("start test by appoint-para");
			}
			else
			{
				
				ShellCmdPrintf("json para is err");
			}
		}
		cJSON_Delete(jpara);
	}
	
	return 0;
}

