#include <sys/types.h>

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <error.h>
#include <signal.h>
#include <pthread.h>
#include <semaphore.h>
#include "cJSON.h"

#include "uart.h"
#include "vdp_uart.h"
#include "utility.h"

#include "fw_uart.h"

#include "task_Hal.h"
#include "elog.h"
#include "obj_UartBusiness.h"
#include "task_MDS.h"

#include "remote_id_access.h"
#include "task_Beeper.h"

Serial_Task_Info 		ttyS0_sti;
static pthread_mutex_t	ttyS0_stiLock = PTHREAD_MUTEX_INITIALIZER;

unsigned char 	rcvByteCnt;		//�ֽڼ�����
unsigned char 	rcvData[100];	//��������֡����	

COM_RCV_FLAG 	phComRcvFlag = COM_RCV_NONE;		
COM_TRS_FLAG 	phComTrsFlag = COM_TRS_NONE;
BUS_ERR  		stateError;			//���ߴ���״̬

int exit_key_filter(cJSON *cmd);

// ֡��ʽ:
// [��ʼΪ: 0x02] [���ݳ���] [����1][����2][����N][�����][ֹͣλ:0x03]
// [���ݳ���] :  [����1]��[����N]��BYTE��
// [�����] :  [����1]��[����N]���ۼӺ�,  Ϊ1BYTE, ��Ȼ���?;

#define DT_UART_CMD_PACK_MIN_LEN				4			// ��С�������L��

#define DT_UART_CMD_PACK_HEAD_OFF				0			// ���^ƫ����
#define DT_UART_CMD_PACK_LEN_OFF				1			// ���L��ƫ����
#define DT_UART_CMD_PACK_CMD_OFF				2			// ������ƫ����
#define DT_UART_CMD_PACK_DAT_OFF				3			// ������ƫ����

#define	DT_UART_CMD_PACK_HEAD					0x02		// ���^
#define	DT_UART_CMD_PACK_TAIL					0x03		// ��β

#if 1
BUFFER_POOL HABufferPool;

//���ջ���ص�ָ��ǰ��?
unsigned short GetBufferLastPos(unsigned short iPos)
{
	if( iPos == 0 ) iPos = POOL_LENGTH-1; else iPos--;
	return iPos;
}
//���ջ���ص�ָ�����
unsigned short GetBufferNextPos(unsigned short iPos)
{
	iPos++; if( iPos >= POOL_LENGTH ) iPos = 0;
	return iPos;
}
//����������ݵĸ���?
unsigned short GetBufferLength(void)
{
	if( HABufferPool.ptr_i >= HABufferPool.ptr_o )
		return ( HABufferPool.ptr_i-HABufferPool.ptr_o);
	else
		return ( HABufferPool.ptr_i+POOL_LENGTH-HABufferPool.ptr_o);
}
// ���յ����ݽ��뻺���?
void PushIntoBufferPool(unsigned char data)
{
	HABufferPool.Buffer[HABufferPool.ptr_i] = data;					//���浱ǰ����
	HABufferPool.ptr_i = GetBufferNextPos(HABufferPool.ptr_i);		//����ָ����?
	if( HABufferPool.ptr_i == HABufferPool.ptr_o )					//��������������򸲸ǵ���ɵ�����
		HABufferPool.ptr_o = GetBufferNextPos(HABufferPool.ptr_o);
}

//�ӻ����ȡ����?
uint8 PollOutBufferPool(unsigned char *pdata)
{
	if( HABufferPool.ptr_i == HABufferPool.ptr_o )					//û�����ݿ�ȡ
	{
		return 0;
	}
	else
	{
		*pdata = HABufferPool.Buffer[HABufferPool.ptr_o];				//ȡ����ǰ����
		HABufferPool.ptr_o = GetBufferNextPos(HABufferPool.ptr_o);		//���ָ���һ
		return 1;
	}
}

#endif

#define RCV_RESET_TIMER		500	//ms
OnePacket packet;
OS_TIMER timerRcvReset;


unsigned char UartCheckSum( char* pbuf, int len )
{
	unsigned char checksum = 0;
	int i;
	for( i = 0; i < len; i++ )
	{
		checksum += pbuf[i];
	}
	return checksum;
}	

/*******************************************************************************************
 * @fn:		ttyS0_rcv_handle
 *
 * @brief:	���ڽ������ݰ�����
 *
 * @param:  	*data	- ������ָ��
 * @param:  	len		- ���ݰ�����
 *
 * @return: 	none
*******************************************************************************************/
void ttyS0_rcv_handle(char *data, int len)
{
	unsigned char cks;
	int i;
	int dataLen;
	
#if 0
	unsigned char* ptrDat;;
	ptrDat =(unsigned char*) data;
	dprintf("uart recv len = %d: ",len);
	for( i = 0; i < len; i++ )
	{
		printf(" %02x ",ptrDat[i]);	
	}
	printf("\n\r"); 
#endif	
	for(i = 0; i < len; i++)
	{
		PushIntoBufferPool(data[i]);
	}

	while( PollOutBufferPool(&packet.data[packet.len++]) )
	{
		OS_RetriggerTimer(&timerRcvReset);
		//��ͷ���?
		if(packet.data[0] != DT_UART_CMD_PACK_HEAD)
		{
			//��ͷ���Դ�ͷ��ʼ����
			packet.len = 0;
			dprintf("dt uart cmd pack recv err!\n");
			continue;
		}
		// get length
		if(packet.len <= DT_UART_CMD_PACK_LEN_OFF)
		{
			continue;
		}
		
		//���ݳ��ȼ��?
		dataLen = packet.data[DT_UART_CMD_PACK_LEN_OFF]+4;
		if(dataLen%4)
		{
			//���ݳ��Ȳ���4���������ݰ����?0x03��ֱ�����ȱ�4����
			dataLen = dataLen + (4 -dataLen%4);
		}
		//���ݰ���û������ɣ���������?
		if( dataLen >= PACKET_LENGTH )
		{
			packet.len = 0;
			dprintf("dt uart cmd pack recv too long!\n");
			continue;
		}
		
		//���ݰ���û������ɣ���������?
		if(packet.len < dataLen)
		{
			continue;
		}
		
		//�ж�У���?
		if(UartCheckSum( &packet.data[DT_UART_CMD_PACK_CMD_OFF], packet.data[DT_UART_CMD_PACK_LEN_OFF]) != packet.data[packet.data[DT_UART_CMD_PACK_LEN_OFF]+DT_UART_CMD_PACK_CMD_OFF])
		{
			//����Ͳ��Դ�ͷ��ʼ����?
			packet.len = 0;
			dprintf("dt uart cmd pack recv err!\n");
			continue;
		}
		//�жϰ�β
		for(i = packet.data[DT_UART_CMD_PACK_LEN_OFF]+DT_UART_CMD_PACK_CMD_OFF+1; i < dataLen; i++)
		{
			if(packet.data[i] != DT_UART_CMD_PACK_TAIL)
			{
				break;
			}
		}
		if(i < dataLen)
		{
			//��β���Դ�ͷ��ʼ����
			packet.len = 0;
			dprintf("dt uart cmd pack recv err!\n");
			continue;
		}
		api_uart_recv_callback(packet.data+DT_UART_CMD_PACK_CMD_OFF, packet.data[DT_UART_CMD_PACK_LEN_OFF]);
		packet.len = 0;
	}
	//û�����ݵ�ʱ��Ӷ���?1
	packet.len--;
}

int vdp_uart_send_data(char *data, int len)
{

#if defined(PID_IX850)||defined(PID_IX821)

#if 0
		int i;
		unsigned char* ptrDat;;
		ptrDat =(unsigned char*) data;
		dprintf("uart send len = %d: ",len);
		for( i = 0; i < len; i++ )
		{
			printf(" %02x ",ptrDat[i]);	
		}
		printf("\n\r"); 
#endif		
	pthread_mutex_lock(&ttyS0_stiLock);	
	//如果上次发送还没超过50ms，则等待50ms
	if(GetHBtimerCnt() == 0)
	{
		usleep(100*1000);
	}
	RefreshHearBeatTimer();

	push_serial_data(&ttyS0_sti, data, len);

	pthread_mutex_unlock(&ttyS0_stiLock);
	
#endif	
	return 0;
}

void RcvResetCallBack(void)
{
	packet.len = 0;
	OS_StopTimer(&timerRcvReset);
}

/*******************************************************************************************
 * @fn:		Init_vdp_uart
 *
 * @brief:	��ʼ��uart�˿�
 *
 * @param:  	none
 *
 * @return: 	0/ok, -1/err
*******************************************************************************************/
int Init_vdp_uart(void)
{
	if( !start_serial_task(&ttyS0_sti, "/dev/ttySAK2", 9600, 8, 'N', 1, &ttyS0_rcv_handle) )
	{
	 	printf("--------------------------ttyS1 open  Success!\n");
	}
	else
	{
		printf("----------------------------start_serial_task failure!\n");
	}	

	HABufferPool.ptr_i = 0;
	HABufferPool.ptr_o = 0;
	packet.len = 0;
	OS_CreateTimer(&timerRcvReset, RcvResetCallBack, RCV_RESET_TIMER/25);
	return 0;
}

/*******************************************************************************************
 * @fn:		close_vdp_uart
 *
 * @brief:	�رմ��ڼ�������?
 *
 * @param:  	none
 *
 * @return: 	0/ok, -1/err
*******************************************************************************************/
int close_vdp_uart(void)
{
	stop_serial_task(&ttyS0_sti);
	return 0;
}

void LogSendUartCmd(const char* prefix, char cmd, char* pbuf, unsigned int len)
{
	char* jsonString;
	cJSON* json;

	json = UartDataToJson(cmd, pbuf, len);
	jsonString = cJSON_PrintUnformatted(json);
	if(jsonString)
	{
		log_v("%s:%s", prefix, jsonString);
		free(jsonString);
	}
	cJSON_Delete(json);
}

int uart_send_pack(char cmd, char* pbuf, unsigned int len )
{
#if defined(PID_IX850)||defined(PID_IX821)
	unsigned char cks;
	char send_buf[PACKET_LENGTH];
	int ret;

	if(len > PACKET_LENGTH) len = PACKET_LENGTH;

	send_buf[DT_UART_CMD_PACK_HEAD_OFF] 		= DT_UART_CMD_PACK_HEAD;
	send_buf[DT_UART_CMD_PACK_LEN_OFF]			= len+1;
	send_buf[DT_UART_CMD_PACK_CMD_OFF]			= cmd;
	// ����checksum
	cks = cmd;
	if( len )
	{
		memcpy( send_buf+DT_UART_CMD_PACK_DAT_OFF, pbuf, len );	
		cks += UartCheckSum( pbuf, len );
	}
	send_buf[DT_UART_CMD_PACK_DAT_OFF+len] 		= cks;
	send_buf[DT_UART_CMD_PACK_DAT_OFF+len+1] 	= DT_UART_CMD_PACK_TAIL;
	ret = vdp_uart_send_data(send_buf,DT_UART_CMD_PACK_DAT_OFF+len+2);

	return ret;
#else
	return 0;
#endif	
}



// �������ݰ�������
int api_uart_send_pack( unsigned char cmd, char* pbuf, unsigned int len )
{
	char sendData[PACKET_LENGTH];

	if(len == 0 || pbuf == NULL)
	{
		len = 0;
	}
	else
	{
		memcpy(sendData, pbuf, len);
	}
	sendData[len++] = 0;

	LogSendUartCmd("send1", cmd, sendData, len);

	return uart_send_pack(cmd, sendData, len);
}



// zzb_20171127
void api_notify_global_reset(void)
{
	dprintf("----------------------------I request restart!!!!!!!\n");
	api_uart_send_pack(UART_TYPE_N2S_RESET, NULL, 0); 
}
static void Uint32ToString(uint32 temp, unsigned char* str)
{
	str[0] = temp/1000000000 + '0';
	str[1] = temp/100000000%10 + '0';
	str[2] = temp/10000000%10 + '0';
	str[3] = temp/1000000%10 + '0';
	str[4] = temp/100000%10 + '0';
	str[5] = temp/10000%10 + '0';
	str[6] = temp/1000%10 + '0';
	str[7] = temp/100%10 + '0';
	str[8] = temp/10%10 + '0';
	str[9] = temp%10 + '0';
	str[10] = 0;
	#if 0
	int cnt=0;
	int i;
	str[0] = temp/1000000000 + '0';
	if(str[0]!='0')
		cnt++;
	for(i=0;i<9;i++)
	{
		str[cnt] = temp/(10^(8-i))%10 + '0';
		if(cnt>0||str[cnt]!='0')
			cnt++;
	}
	#endif
}

// ���յ��������ݰ��Ļص����� - ���͵�survey����
int api_uart_recv_callback( char* pbuf, unsigned int len )
{
	unsigned short logic_addr;
	unsigned char slave_addr;
	char task_id = 0;
	char cmd;
	char cardNum[21];
	cJSON* json;

	static int myAddr_init = 0;	//czn_20170818
	static int ts_3s_flag = 0;	//czn_20190216

	if(len < 1)
	{
		return 0;
	}

	#if 0	
		int i;
		unsigned char* ptrDat;;
		ptrDat =(unsigned char*) pbuf;
		dprintf("api_uart_recv_callback len = %d: ",len);
		for( i = 0; i < len; i++ )
		{
			printf(" %02x ",ptrDat[i]);	
		}
		printf("\n\r"); 
		//PrintCurrentTime(101);
	#endif	

	cmd = pbuf[0];
	if(cmd > 100)
	{
		task_id = pbuf[len - 1];
	}

	LogSendUartCmd("recv", cmd, pbuf+1, len-1);

	switch (task_id)
	{
		//case TASK_TYPE_DTS_ADDR_ER:
		case TASK_TYPE_DTS_CHSUM_ER:
		case TASK_TYPE_DTS_HEAD_ER:
		case TASK_TYPE_MDS_CHSUM_ER:
		case TASK_TYPE_MDS_HEAD_ER:
			return 0;
	}

	switch(cmd)
	{
		// 1 = STM8���յ�����ָ��,��N329����; - ����ָ���?
		case UART_TYPE_S2N_REC_DT_CMD:


			COMM_RCV_FLAG_ENABLE(COM_RCV_DAT,BUS_ERR_NONE);
			rcvByteCnt = len-2;
			memcpy( rcvData, pbuf+1,rcvByteCnt );
			L2_LayerMonitorService();

			
			break;

		// 3 = STM8ִ�з���ָ��,  ���ط��ͽ����N329 - * ���ͽ��?
		case UART_TYPE_S2N_SND_RESULT:

			// lzh_20161207_s
			if( pbuf[1] == 0 )
				phComTrsFlag = COM_TRS_DAT;
			else if( pbuf[1] == 10 )
				phComTrsFlag = COM_TRS_DAT_HAVE_ACK;
			else
				phComTrsFlag = COM_TRS_ERR;	
			
			// lzh_20161207_e
			L2_LayerMonitorService();	

			break;

		case UART_TYPE_R_M_CMD_REP:
			API_MDS_RcvUart(MDS_INPUT, pbuf+1, len-1);
			break;

		case UART_TYPE_S_M_CMD_RSP:
			UartRecvSyncPacketProcess(task_id, pbuf, len-1);
			break;

		// 6 = STM8��N329����:	 DIP״̬(�ı�) - 5λDIP���ص�ֵ
		case UART_TYPE_S2N_DIP_STATUS:
/*
		
			logic_addr = ((pbuf[1]&0x10)>>4)|((pbuf[1]&0x08)>>2)|((pbuf[1]&0x04))|((pbuf[1]&0x02)<<2)|((pbuf[1]&0x01)<<4);
			myAddr = 0x80|(logic_addr<<2);

			if(myAddr_init == 0)
			{
				myAddr_init = 1;
			}
			else
			{
				key_msg.head.msg_target_id	= MSG_ID_Menu;
				key_msg.head.msg_source_id	= MSG_ID_hal;
				key_msg.head.msg_type		= HAL_TYPE_TOUCH_KEY;
				key_msg.head.msg_sub_type	= 0;
				key_msg.key.keyType 		= KEY_TYPE_DIP;
				key_msg.key.keyData 		= myAddr;
				gettimeofday(&key_msg.happen_time,NULL);
				API_add_hal_message_to_VideoMenu_queue( (char*)&key_msg,sizeof(HAL_MSG_TYPE) );
			}
*/
			
			break;
			
		// 9 = STM8��N329Ӧ��:	 DIP״̬  - 5λDIP���ص�ֵ
		case UART_TYPE_S2N_REPLY_DIP_STATUS:
/*
			logic_addr = ((pbuf[1]&0x10)>>4)|((pbuf[1]&0x08)>>2)|((pbuf[1]&0x04))|((pbuf[1]&0x02)<<2)|((pbuf[1]&0x01)<<4);
			
			API_Event_IoServer_InnerRead_All(MASTER_SLAVE_SET, (uint8*)&slave_addr);
			if(slave_addr >= 4)
				slave_addr = 0;
			
			myAddr = 0x80|(logic_addr<<2)|slave_addr;
*/

			break;
			
		//  7 = STM8��N329����:	 DoorBell״̬(�ı�) - 0=�ͷ�; 1=���� 
		// 11 = STM8��N329Ӧ��:   DoorBell״̬	- 0=�ͷ�; 1=���� 
		case UART_TYPE_S2N_REPLY_DOORBELL_STATUS:
		case UART_TYPE_S2N_DOORBELL_STATUS:
		#if 0
			key_msg.head.msg_target_id	= MSG_ID_survey;
			key_msg.head.msg_source_id	= MSG_ID_hal;
			key_msg.head.msg_type		= HAL_TYPE_TOUCH_KEY;
			key_msg.head.msg_sub_type	= 0;
			key_msg.key.keyType 		= KEY_TYPE_DB;
			key_msg.key.keyData 		= pbuf[1];			
			gettimeofday(&key_msg.happen_time,NULL);
		#endif
			// zfz_20190605
			//API_add_message_to_suvey_queue( (char*)&key_msg,sizeof(HAL_MSG_TYPE) );
			//IX2_TEST API_add_hal_message_to_VideoMenu_queue( (char*)&key_msg,sizeof(HAL_MSG_TYPE) );
			break;
		// lzh_20161207_s
		// 14 = STM8֪ͨ���յ�ACK
		case UART_TYPE_S2N_BUS_ACK_RCV:
/*
			phComRcvFlag = COM_RCV_ACK;
			L2_LayerMonitorService();
			break;
*/
		// lzh_20161207_e

		case UART_TYPE_S2N_TOUCHKEY_STATUS:     //lyx 20170721
			#if 0
			key_msg.head.msg_target_id	= MSG_ID_survey;
			key_msg.head.msg_source_id	= MSG_ID_hal;
			key_msg.head.msg_type		= HAL_TYPE_TOUCH_KEY;
			key_msg.head.msg_sub_type	= 0;
			key_msg.key.keyType		= KEY_TYPE_TS;	
			key_msg.key.keyData              = pbuf[2];
		
			//czn_20190216_s
			if(pbuf[1] == KEY_STATUS_PRESS)
			{
				ts_3s_flag = 0;
				key_msg.key.keyStatus = TOUCHPRESS;
			}
			else if(pbuf[1] == KEY_STATUS_RELEASE)
			{	
				if(ts_3s_flag == 1)
				{
					ts_3s_flag = 0;
					break;
				}
				key_msg.key.keyStatus = TOUCHCLICK;
			}
			else if(pbuf[1] == KEY_STATUS_3SECOND)
			{
				ts_3s_flag = 1;
				key_msg.key.keyStatus = TOUCH_3SECOND;
			}
			//czn_20190216_e
			dprintf(" Touchkey: keyData=%d,keyStatus=%d\n",pbuf[2], pbuf[1]);

			gettimeofday(&key_msg.happen_time,NULL);			//key_msg.happen_time		= clock();		//czn_20170111
			//bprintf("get clock = %d:%d\n",key_msg.happen_time.tv_sec,key_msg.happen_time.tv_usec);     
			///API_add_hal_message_to_VideoMenu_queue( (char*)&key_msg,sizeof(HAL_MSG_TYPE) ); 
			#endif
			break;

		case UART_TYPE_S2N_NOTIFY_RESET:
			log_i("MCU will reset in 2 seconds.");
			break;

		//cao_20180604
		case UART_TYPE_STM8_VERSION_ASK:
			ReceiveStm8lSwVerFromStm8l(pbuf);
			break;
				
		// lzh_20210727
		case UART_TYPE_FIRMWARE_S2M:
			api_fw_uart_upgrade_recv( pbuf+1, len-1 );
			break;
			
		case UART_TYPE_AMP_MUTE:
			//printf("$$$$$$$UART_TYPE_AMP_MUTE\n");
			API_StackRecvMute_On();
			break;	
		#ifdef PID_IX850
		case UART_TYPE_SWIP_ID_CARD:
		case UART_TYPE_SWIP_IC_CARD:
			if(len<=5)
				snprintf(cardNum, 20, "%d", (unsigned int)((pbuf[1]<<16) + (pbuf[2]<<8) + pbuf[3]));
			else
			{
				Uint32ToString((unsigned int)((pbuf[1]<<24) + (pbuf[2]<<16) + (pbuf[3]<<8)+pbuf[4]),cardNum);
				snprintf(cardNum, 20, "%u", (unsigned int)((pbuf[1]<<24) + (pbuf[2]<<16) + (pbuf[3]<<8)+pbuf[4]));
			}
			//Uint32ToString(IDCardData,cardNum);
			if((pbuf[1]==0xff||pbuf[1]==0)&&(pbuf[2]==0xff||pbuf[2]==0)&&(pbuf[3]==0xff||pbuf[3]==0))
				return;
			////Send_CardSwip(cardNum);
			//CardSwip_Process(cardNum);
			// lzh_20240827_s
			if( is_remote_device_enable() )
				thirdpart_remote_id_access(cardNum);
			else
				CardSwip_Process(cardNum);
			//printf("111111111:cardNum:%s\n",cardNum);
			break;
		//case UART_TYPE_INTPUT_EVT_KEY:
		//	break;
		#endif
		
		case UART_TYPE_INTPUT_EVT_KEY:
			//printf("UART_TYPE_INTPUT_EVT_KEY\n");
			json = UartDataToJson(cmd, pbuf+1, len-1);
			printf_json(json, "UART_TYPE_INTPUT_EVT_KEY");
			if(exit_key_filter(json))
			{
				cJSON_Delete(json);
				break;
			}
			cJSON_Delete(json);
			
		//	break;
		default:
			if(task_id > 0 && task_id < 200)
			{
				UartRecvSyncPacketProcess(task_id, pbuf, len-1);
			}
			else if(task_id == 213)
			{
				json = UartDataToJson(cmd, pbuf+1, len-1);
				UartRecvHalEventProcess(json);
				cJSON_Delete(json);
			}
			break;
	}
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*******************************************************************************************
 * @fn:		Ph_AckWrite
 *
 * @brief:	��������ACK�ź�
 *			
 * @param:  none
 *
 * @return: 1 �ɹ���0 ʧ��
 *******************************************************************************************/
//lzh_20130413
unsigned char Ph_AckWrite(void)
{
	// lzh_20161207_s
	//api_uart_send_pack(UART_TYPE_N2S_BUS_ACK_SND,NULL,0);
	api_uart_send_pack_and_wait_rsp(UART_TYPE_N2S_BUS_ACK_SND, NULL, 0, NULL, NULL, 0);
	//dprintf("---------Ph_AckWrite\n");
	// lzh_20161207_e
	return 1;
}

/*******************************************************************************************
 * @fn:		Ph_DataWrite
 *
 * @brief:	������������
 *			
 * @param:  pdat - ����ָ��
 * @param:  len - ���ݳ���
 * @param:  uNew - ��ָ����?
 *
 * @return: 1 �ɹ���0 ʧ��
 *******************************************************************************************/
//lzh_20130416
unsigned char Ph_DataWrite(unsigned char *pdat,unsigned char len,unsigned char uNew)
{
	unsigned char send_buf[200];
	send_buf[0] = 0x00;		// mac_type
	send_buf[1] = uNew;		// new/old type
	//send_buf[2] = 0;		// new/old type
	memcpy( send_buf+2, pdat, len );
	
	#if 1	
		int i;
		unsigned char* ptrDat;;
		ptrDat =(unsigned char*) pdat;
		printf("Ph_DataWrite len = %d: ",len);
		for( i = 0; i < len; i++ )
		{
			printf(" %02x ",ptrDat[i]);	
		}
		printf("\n\r"); 
	#endif
	//send_buf[2+len]=MSG_ID_StackAI;
	//api_uart_send_pack(UART_TYPE_N2S_SND_DT_CMD,send_buf,len+2);
	API_StackSendMute_On();
	api_uart_send_pack_and_wait_rsp(UART_TYPE_N2S_SND_DT_CMD, send_buf, len+2, NULL, NULL, 0);
		
	return 0;
}

/*******************************************************************************************
 * @fn:		Ph_PollingWriteResult
 *
 * @brief:	��ѯ��ǰ���ߣ��õ��������ݽ���?�ű�־������������ŵ�pdat�ĵ�ַ��
 *			
 * @param:  COM_TRS_FLAG flag - ���ͽ�����־
 *
 * @return: 1 �ɹ���0 ʧ��
 *******************************************************************************************/
unsigned char Ph_PollingWriteResult(COM_TRS_FLAG *flag)
{
#if 1
	if(phComTrsFlag==COM_TRS_DAT_HAVE_ACK)
		*flag=COM_TRS_DAT_HAVE_ACK;
	else
		*flag = COM_TRS_DAT;
	//��ȡ���ݺ�������?
#else
	*flag = COM_TRS_DAT;
	//��ȡ���ݺ�������?
#endif
	phComTrsFlag = COM_TRS_NONE;
	return 1;
}

/*******************************************************************************************
 * @fn:		Ph_PollingReadResult
 *
 * @brief:	��ѯ��ǰ���ߣ��õ�����ͨ�ű�־������������ŵ�pdat�ĵ�ַ��
 *			
 * @param:  pdat - ������ݵ�ָ��?
 * @param:	COM_RCV_FLAG flag - ����ͨ�ű�־
 *
 * @return: pdat�е���Ч���ݵĳ��ȣ����������򷵻�0
 *******************************************************************************************/
unsigned char Ph_PollingReadResult(unsigned char *pdat,COM_RCV_FLAG *flag)
{
	unsigned char i;

	if( phComRcvFlag == COM_RCV_DAT )
	{
		memcpy( pdat, rcvData, rcvByteCnt );
		i = rcvByteCnt;	
	}
	else
	{
		i = 0;
	}

	*flag = phComRcvFlag;
	//��ȡ���ݺ������??
	phComRcvFlag = COM_RCV_NONE;
	return i;	
}
#if defined(PID_IX850)
void tft_power_on(int on )
{
	int temp = on;
	api_uart_send_pack(UART_TYPE_N2S_NOTIFY_STATE, &temp, 1);
	api_uart_send_pack(UART_TYPE_N2S_NOTIFY_STATE, &temp, 1);
}
#endif

static Serial_Task_Info ttyUSB2 = {.sdev_fd = 0};

void ttyUSB2_rcv_handle(char *data, int len)
{
	if(data)
	{
		ShellCmdPrintf("%s", data);
	}
}

int Modules4G_AT_Send(char *data, int len)
{
	if(ttyUSB2.sdev_fd == 0)
	{
		start_serial_task(&ttyUSB2, "/dev/ttyUSB2", 115200, 8, 'N', 1, &ttyUSB2_rcv_handle);
	}
	
	if(ttyUSB2.sdev_fd)
	{
		push_serial_data(&ttyUSB2, data, len);
		push_serial_data(&ttyUSB2, "\r\n", 2);
	}
	return 1;
}
void uart_beep_ctrl(int beep_type)
{
	int temp;;
	switch ( beep_type )
	{
		case BEEP_TYPE_DI1:
			temp = BUZZER_DI;
			break;
		case BEEP_TYPE_DI2:
			temp = BUZZER_DIDI;
			break;
		case BEEP_TYPE_DI3:
			temp = BUZZER_DIDIDI;
			break;
		case BEEP_TYPE_DL1:
			temp = BUZZER_LONGDI;
			break;
		case BEEP_TYPE_DI4:
		case BEEP_TYPE_DI5:
		case BEEP_TYPE_DI8:
		case BEEP_TYPE_DL2:
		case BEEP_TYPE_DL1DI1:
		case BEEP_TYPE_DI1DL1:
		case BEEP_TYPE_PHONECALL:
		default:
			return;
	}
	api_uart_send_pack(UART_TYPE_BEEP_C, &temp, 1);
	//api_uart_send_pack(UART_TYPE_N2S_NOTIFY_STATE, &temp, 1);
}
static int exit_key_long_press=0;
static int exit_key_long_press_release=0;
static time_t exit_key_long_press_time=0;
static time_t exit_key_last_press_time=0;
int ExitKeyLongPressStatusRelease(int timing)
{
	if(exit_key_long_press_release)
	{
		if(abs(time(NULL)-exit_key_long_press_time)>15)
		{
			exit_key_long_press_release=0;
			API_LED_ctrl("LED_2", "MSG_LED_OFF");
			return 2;
		}
		return 1;
	}
	else
		return 2;
}

int exit_key_filter(cJSON *cmd) 
{
	int cur_time=time(NULL);
	if(strcmp(GetEventItemString(cmd,UART_KEY_STATUS),"KEY_STATUS_PRESS_3S")==0)
	{
		if(exit_key_long_press==0)
		{
			exit_key_long_press=1;
			BEEP_CONFIRM();
			exit_key_long_press_time=cur_time;
			if(exit_key_long_press_release)
			{
				exit_key_long_press_release=0;
				API_LED_ctrl("LED_2", "MSG_LED_OFF");
			}
			return 1;
		}
	}
	else
	{
		
		if(exit_key_long_press==1)
		{
			exit_key_long_press=0;
			exit_key_long_press_release=1;
			API_LED_ctrl("LED_2", "MSG_LED_FLASH_FAST");
			exit_key_long_press_time=cur_time;
			API_Add_TimingCheck(ExitKeyLongPressStatusRelease, 1);
		}

		if(strcmp(GetEventItemString(cmd,UART_KEY_STATUS),"KEY_STATUS_PRESS")==0)
		{
			if(exit_key_long_press_release)
			{
				exit_key_long_press_release=0;
				API_LED_ctrl("LED_2", "MSG_LED_OFF");
			}
			if(abs(cur_time-exit_key_last_press_time)<3)
				return 1;
			exit_key_last_press_time=cur_time;
		}
	}
	return 0;
}
int ifExitKeyLongPress(void)
{
	if(exit_key_long_press_release&&abs(time(NULL)-exit_key_long_press_time)<15)
	{
		return 1;
	}
	return 0;
}
