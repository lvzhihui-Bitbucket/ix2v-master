
#ifndef RADAR_UART_H
#define RADAR_UART_H

#define RADAR_POOL_LENGTH		(300)

typedef struct _RADAR_BUFFER_POOL_
{
	volatile unsigned short ptr_i;
	volatile unsigned short ptr_o;
	unsigned char Buffer[RADAR_POOL_LENGTH];
} RADAR_BUFFER_POOL;

#define RADAR_PACKET_LENGTH		(256)

typedef struct 
{
	unsigned short len;
	unsigned char data[RADAR_PACKET_LENGTH];
} OneRadarPacket;

#define	RADAR_UART_CMD_REQ_HEAD			0x58
#define	RADAR_UART_CMD_RSP_HEAD			0x59

#define RADAR_UART_CMD_PACK_HEAD_OFF	0			// head
#define RADAR_UART_CMD_PACK_CMD_OFF		1			// cmd
#define RADAR_UART_CMD_PACK_LEN_OFF		2			// len
#define RADAR_UART_CMD_PACK_DAT_OFF		3			// dat

#define RADAR_PACKET_MAX_LENGTH			255

// normal command
// 运动感应距离
/*
e.g., set distance level to 15
HEX is: 58 02 01 0F 6A 00
Resp: 59 02 01 00 5C 00 (success),
*/
#define RADAR_CMD_DISTANCE_LEVEL_SET	0x02		// set distance level, range: 0-15
/*
发送命令格式（HEX）：58 03 00 5B 00
回复帧: 59 03 01 0F 6C 00 // e.g. get distance level: 1
*/
#define RADAR_CMD_DISTANCE_LEVEL_GET	0x03		// get distance level, range: 0-15

// 亮灯时间
/*
e.g., set time to 1s
HEX is: 58 04 02 01 00 5F 00
Resp: 59 04 01 00 5E 00 (success), 
*/
#define RADAR_CMD_LAMP_ON_TIME_SET		0x04		// set lamp on time (s)
/*
发送命令格式（HEX）： 58 05 00 5D 00
回复帧: 59 05 02 01 00 61 00 // e.g. get lot: 0x0001 (1
*/
#define RADAR_CMD_LAMP_ON_TIME_GET		0x05		// get lamp on time (s)

// 光敏阈值
/*
e.g., set lux to 1000
HEX is: 58 06 02 E8 03 4B 01
Resp: 59 06 01 00 60 00 (success), , Other: fail
*/
#define RADAR_CMD_LUX_THRESHOLD_SET		0x06
/*
发送命令格式（HEX）： 58 07 00 5F 00
回复帧: 59 07 02 E8 03 4D 01 // e.g. get lux: 0x03E8
3.1.7 设置运动检测 delta 值
*/
#define RADAR_CMD_LUX_THRESHOLD_GET		0x07

// 运动检测deltra阈值
#define RADAR_CMD_DELTA_VALUE_SET		0xd3

/*
e.g., set delta to 300
HEX is: 58 d2 02 2c 01 59 01
Resp: 59 D2 01 00 2C 01
*/
#define RADAR_CMD_DELTA_VALUE_GET		0xd4

// 微动检测距离
/*
e.g., set distance level to 15
HEX is: 58 0E 01 0F 76 00
Resp: 59 0E 01 00 68 00
*/
#define RADAR_CMD_MOTION_DISTANCE_LEVEL_SET		0x0E

/*
发送命令格式（HEX）：58 0F 00 67 00
回复帧: 59 0F 01 0F 78 00 // e.g. get distance level: 15
*/
#define RADAR_CMD_MOTION_DISTANCE_LEVEL_GET		0x0F

// 开关灯
/*
发送命令格式（HEX）：58 0A 01 01 64 00 (打开灯)
58 0A 01 00 63 00 (关闭灯)
回复帧：59 0A 01 00 64 00 (success), Other: fail
*/
#define RADAR_CMD_LAMP_SWITCH_SET		0x0A

// 设置占空比
/*
参数：para1: value of duty (low byte), para2: value of duty (high byte). Unit 0.1%
发送命令格式（HEX）：58 0B 02 xx xx xx xx
回复帧: 参考回复帧格式
e.g., set duty to 50.0%
HEX is: 58 0B 02 F4 01 5A 0
*/
#define RADAR_CMD_LAMP_DUTY_SET			0x0B

// 打开/关闭雷达
/*
发送命令格式（HEX）：58 D1 01 01 2B 01 (打开雷达)
58 D1 01 00 2A 01 (关闭雷达)
回复帧：59 D1 01 00 2B 01 (success), Other: fail
*/
#define RADAR_CMD_OPEN_CLOSE_SET		0xD1
/*
发送命令格式（HEX）： 58 D0 00 28 01
回复帧：59 D0 01 01 2B 01 // e.g., radar is on
59 D0 00 01 2A 01 // e.g., radar is off
*/
#define RADAR_CMD_OPEN_CLOSE_GET		0xD0

// 是否保存雷达设置的开关
/*
发送命令格式（HEX）：58 08 01 01 62 00 (保存)
58 08 01 00 61 00 (不保存)
回复帧：59 D8 01 00 62 00 (success) Other: fail
Note：如果需要将参数保存在雷达模块中，建议发送命令后延迟 1 秒以上
*/
#define RADAR_CMD_SAVE_SWITCH_SET		0x08
/*
发送命令格式（HEX）：58 09 00 61 00
AHLK-LD016-5G 雷达模组
9 / 12
回复帧：59 09 01 01 64 00 // e.g., save is on
59 09 01 00 63 00 // e.g., save is off
*/
#define RADAR_CMD_SAVE_SWITCH_GET		0x09

// 系统复位
/*
控制帧格式定义：
boot_hci_ctrl_frm_t send_frm = {
0x58, // uint8 head
BOOT_HCI_SYS_RESET&0x1f, // uint8 cmd: 5
(BOOT_HCI_SYS_RESET>>5) &0x07, // uint8 cmd_grp: 3
0x1, // uint8 para_len
NULL, // uint8 *para
0x0, // uint16 check_code
};
回复帧: 数据格式如下：
e.g., sys_reset(0x1)
HEX is: 58 13 01 01 6d 00
*/
#define RADAR_CMD_SYSTEM_RESET			0x13

// debug command
// 寄存器读写
/*
参数：para1: 32-bits register address, para2: 32-bits data to write
返回：8-bits value, 0: success, other: fail. 发送命令格式（HEX）：58 00 08 xx xx xx xx xx xx xx xx xx xx
回复帧: 参考回复帧格式
e.g. reg_write(0x40003008, 0x0e810f3b)
HEX is: 58 00 08 08 30 00 40 3b 0f 81 0e b1 01
Resp: 59 00 01 00 5a 00
*/
#define RADAR_CMD_REGISTER_WRITE		0x00
/*
参数：para1: 32-bits register address
AHLK-LD016-5G 雷达模组
10 / 12
返回：32-bits register value
*/
#define RADAR_CMD_REGISTER_READ			0x01

// 内存读写
/*
e.g. memory_write(0x20001000, 0xaabbccdd)
HEX is: 58 10 08 00 10 00 20 dd cc bb aa ae 03
Resp: 59 10 01 00 6A 00
*/
#define RADAR_CMD_MEMORY_WRITE			0x10
/*
e.g. memery_read(0x20001000)
HEX is: 58 11 04 00 10 00 20 9d 00
Resp: 59 11 04 xx xx xx xx xx xx
*/
#define RADAR_CMD_MEMORY_READ			0x11
/*
e.g., memory_dump(0x20001000, 0x400)
HEX is: 58 12 06 00 10 00 20 00 04 a4 00
*/
#define RADAR_CMD_MEMORY_DUMP			0x12

// flash写
/*
e.g. flash_write(addr, size, buf) // addr=0x0a000000, size=8, buf[8] = {0x10, 0x11, 0x12, …}
HEX is: 58 14 0c 04 00 00 0a 10 11 12 13 14 15 16 17 2201
Resp: 59 14 01 00 6E 00 (OK), 59 14 01 01 6F 00 (Fail)
*/
#define RADAR_CMD_FLASH_WRITE			0x14

#pragma pack(1)  //指定按1字节对齐

typedef struct
{
	unsigned char	cmd;
	unsigned char 	len;
	unsigned char	*pbuf;
}RADAR_DATA_PACK;

typedef struct
{
	unsigned char	head;
	RADAR_DATA_PACK	data;
	unsigned char	cksl;
	unsigned char	cksh;
}RADAR_UART_PACK;

#pragma pack()


int init_radar_uart(void);
int close_radar_uart(void);

int api_radar_request_pack(char cmd, char* pbuf, unsigned int len,int wait_ack);

#endif


