#include <sys/types.h>

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <error.h>
#include <signal.h>
#include <pthread.h>
#include <semaphore.h>
#include "cJSON.h"

#include "uart.h"
#include "card_uart.h"
#include "utility.h"
#include "elog.h"
#include "task_Beeper.h"

static Serial_Task_Info 		tty_card;
static pthread_mutex_t	tty_card_stiLock = PTHREAD_MUTEX_INITIALIZER;

#define RCV_RESET_TIMER		500	//ms

#define CARD_UART_CMD_PACK_HEAD				0x02
#define CARD_UART_CMD_PACK_TAIL				0x03
#define CARD_UART_CMD_PACK_LEN_OFF			1
#define CARD_PACKET_MIN_LENGTH				3

static CARD_BUFFER_POOL HABufferPool={0};

static unsigned short GetBufferLastPos(unsigned short iPos)
{
	if( iPos == 0 ) iPos = CARD_POOL_LENGTH-1; else iPos--;
	return iPos;
}
static unsigned short GetBufferNextPos(unsigned short iPos)
{
	iPos++; if( iPos >= CARD_POOL_LENGTH ) iPos = 0;
	return iPos;
}
static unsigned short GetBufferLength(void)
{
	if( HABufferPool.ptr_i >= HABufferPool.ptr_o )
		return ( HABufferPool.ptr_i-HABufferPool.ptr_o);
	else
		return ( HABufferPool.ptr_i+CARD_POOL_LENGTH-HABufferPool.ptr_o);
}
static void PushIntoBufferPool(unsigned char data)
{
	HABufferPool.Buffer[HABufferPool.ptr_i] = data;
	HABufferPool.ptr_i = GetBufferNextPos(HABufferPool.ptr_i);
	if( HABufferPool.ptr_i == HABufferPool.ptr_o )
		HABufferPool.ptr_o = GetBufferNextPos(HABufferPool.ptr_o);
}
static uint8 PollOutBufferPool(unsigned char *pdata)
{
	if( HABufferPool.ptr_i == HABufferPool.ptr_o )
	{
		return 0;
	}
	else
	{
		*pdata = HABufferPool.Buffer[HABufferPool.ptr_o];
		HABufferPool.ptr_o = GetBufferNextPos(HABufferPool.ptr_o);
		return 1;
	}
}

static OS_TIMER timerRcvReset;
static CardOnePacket packet = {.len = 0};

unsigned char CardUartCheckSum( char* pbuf, int len )
{
	unsigned char checksum = 0;
	int i;
	for( i = 0; i < len; i++ )
	{
		checksum ^= pbuf[i];
	}
	return checksum;
}
#ifdef PID_IX821
static void ttyS1_card_rcv_handle(char *data, int len)
{
	unsigned short cks,cks_in;
	int i;
	int dataLen;
	struct timeval timeval_temp;
	int time_temp=0;
	static int time_save=0;
	
	
#if 1
	unsigned char* ptrDat;;
	ptrDat =(unsigned char*) data;
	printf("uart recv len = %d: ",len);
	for( i = 0; i < len; i++ )
	{
		printf(" %02x ",ptrDat[i]);	
	}
	printf("\n\r"); 
#endif	
#if 1
	for(i = 0; i < len; i++)
	{
		PushIntoBufferPool(data[i]);
	}
	
	while( PollOutBufferPool(&packet.data[packet.len]) )
	{
		OS_RetriggerTimer(&timerRcvReset);
		packet.len++;
		if(packet.len>=5)
		{
			if(packet.data[4]==CardUartCheckSum(packet.data, 4))
			{
				if(gettimeofday(&timeval_temp,NULL) == 0)
				{
					time_temp=timeval_temp.tv_sec*1000+timeval_temp.tv_usec/1000;
					if(abs(time_temp-time_save)>500)
						api_card_recv_callback(packet.data, packet.len);
					time_save = time_temp;		//czn_20170111
				}
				else
					api_card_recv_callback(packet.data, packet.len);
			}
			packet.len = 0;
		}
	}
#endif
}
#else
static void ttyS1_card_rcv_handle(char *data, int len)
{
	unsigned short cks,cks_in;
	int i;
	int dataLen;
	
#if 1
	unsigned char* ptrDat;;
	ptrDat =(unsigned char*) data;
	printf("uart recv len = %d: ",len);
	for( i = 0; i < len; i++ )
	{
		printf(" %02x ",ptrDat[i]);	
	}
	printf("\n\r"); 
#endif	
#if 1
	for(i = 0; i < len; i++)
	{
		PushIntoBufferPool(data[i]);
	}
	
	while( PollOutBufferPool(&packet.data[packet.len]) )
	{
		OS_RetriggerTimer(&timerRcvReset);

		packet.len++;

		if(packet.data[0] != CARD_UART_CMD_PACK_HEAD)
		{
			packet.len = 0;
			dprintf("card uart cmd pack recv err!\n");
			continue;
		}
		
		if(packet.len > CARD_UART_CMD_PACK_LEN_OFF)
		{
			dataLen = packet.data[CARD_UART_CMD_PACK_LEN_OFF];
		}
		else
		{
			dataLen = 0;
		}

		if(dataLen == packet.len)
		{
			if(packet.len > CARD_PACKET_MIN_LENGTH && packet.data[packet.len-1] == CARD_UART_CMD_PACK_TAIL && packet.data[packet.len-2] == CardUartCheckSum(packet.data+1, packet.len-3))
			{
				api_card_recv_callback(packet.data, packet.len);
			}
			else
			{
				dprintf("card uart cmd pack recv err!\n");
			}
			packet.len = 0;
			continue;
		}

		if(packet.len >= CARD_PACKET_MAX_LENGTH)
		{
			packet.len = 0;
			dprintf("card uart cmd pack recv too long!\n");
			continue;
		}
	}
#endif
}
#endif
static void RcvResetCallBack(void)
{
	packet.len = 0;
	OS_StopTimer(&timerRcvReset);
}

int init_card_uart(void)
{
	#ifdef PID_IX821
	if( !start_serial_task(&tty_card, "/dev/ttySAK0", 9600, 8, 'N', 1, &ttyS1_card_rcv_handle) )
	#else
	if( !start_serial_task(&tty_card, "/dev/ttySAK1", 9600, 8, 'N', 1, &ttyS1_card_rcv_handle) )
	#endif
	{
	 	printf("--------------------------card uart open  Success!\n");
	}
	else
	{
	 	printf("--------------------------card uart open  failuar!\n");
	}	
	HABufferPool.ptr_i = 0;
	HABufferPool.ptr_o = 0;
	packet.len = 0;
	OS_CreateTimer(&timerRcvReset, RcvResetCallBack, RCV_RESET_TIMER/25);
	return 0;
}

int close_card_uart(void)
{
	stop_serial_task(&tty_card);
	return 0;
}

/*
数据头 		长度 	卡片类型 	卡号数据 		BCC校验 					数据结尾
0x02 		0x09 	0x01 		SN0~SN3 (除数据头尾外的其它数据的异或运算) 	0x03
其中卡片类型有：
0x02 EM4100
0x01 MIFARE 1K
0x03 MIFARE 4K
0x10 HID 卡
0x11 T5567
0x20 二代证
0x21 ISO14443B
0x22 FELICA
0x30 15693 标签
0x50 CPU 卡
0x51 扇区信息
0xFF 键盘数据

例如：串口工具接收到的数据为 02 0A 02 2E 00 B6 D7 B5 F2 03 则
第一个字节 0x02 表示数据开始。
第二个字节 0x0A 表示整条数据长度为 10 个字节，包括数据开始和数据结束。
第三个字节 0x02 表示该卡片类型为 EM4100。
第四个字节到第八个字节(0x2E 0x00 0xB6 0xD7 0xB5)这 5 个字节 表示读取到的卡号，其中第四个字节 0x2E 为隐藏卡号。
第九个字节 0xF2 表示第二个字节到第八个字节的 BCC 校验。
第十个字节 0x03 表示数据结束
*/
#ifdef PID_IX821
int api_card_recv_callback( char* pbuf, unsigned int len )
{
	unsigned long cardNum = 0;
	char carNumString[50];
	int i;
	char* cardData = NULL;

	if(len < 4)
	{
		return 0;
	}
	cardData=pbuf;
	

	for(i = 0; cardData && i < 4; i++, cardData++)
	{
		cardNum = (cardNum << 8) + *cardData;
	}

	snprintf(carNumString, 50, "%u", cardNum);

	Send_CardSwip(carNumString);
	dprintf("type:0x%02X, card number:%s\n", pbuf[2], carNumString);
}
#else
int api_card_recv_callback( char* pbuf, unsigned int len )
{
	unsigned long cardNum = 0;
	char carNumString[50];
	int i;
	char* cardData = NULL;

	if(len < 4)
	{
		return 0;
	}

	if(len == 9)
	{
		cardData =  pbuf + 3;
	}
	else if(len == 10)
	{
		cardData =  pbuf + 4;
	}

	for(i = 0; cardData && i < 4; i++, cardData++)
	{
		cardNum = (cardNum << 8) + *cardData;
	}

	snprintf(carNumString, 50, "%u", cardNum);

	Send_CardSwip(carNumString);
	dprintf("type:0x%02X, card number:%s\n", pbuf[2], carNumString);
}
#endif
