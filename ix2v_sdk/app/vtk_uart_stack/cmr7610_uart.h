
#ifndef CMR7610_UART_H
#define CMR7610_UART_H

#define CMR7610_POOL_LENGTH			20

typedef struct _CMR7610_BUFFER_POOL_{
	volatile unsigned short ptr_i;
	volatile unsigned short ptr_o;
	unsigned char Buffer[CMR7610_POOL_LENGTH];
} CMR7610_BUFFER_POOL;


#define CMR7610_PACKET_MAX_LENGTH			10

typedef struct {
	unsigned char len;
	unsigned char data[CMR7610_PACKET_MAX_LENGTH];
} Cmr7610OnePacket;


#endif


