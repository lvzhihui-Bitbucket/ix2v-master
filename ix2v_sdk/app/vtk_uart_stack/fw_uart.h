
#ifndef _FW_UART_H_
#define _FW_UART_H_

#include "utility.h"

// callback to notify
#define CALLBACK_MSG_READ_VERSION_REQ           0
#define CALLBACK_MSG_READ_VERSION_RSP_OK        1
#define CALLBACK_MSG_READ_VERSION_RSP_ERR       2
#define CALLBACK_MSG_READ_VERSION_RSP_TIMEOUT   3
#define CALLBACK_MSG_UPGRADE_START_REQ          4
#define CALLBACK_MSG_UPGRADE_START_RSP_OK       5
#define CALLBACK_MSG_UPGRADE_START_RSP_ERR      6
#define CALLBACK_MSG_UPGRADE_START_RSP_TIMEOUT  7
#define CALLBACK_MSG_UPGRADE_DATA_REQ           8
#define CALLBACK_MSG_UPGRADE_DATA_RSP_OK        9
#define CALLBACK_MSG_UPGRADE_DATA_RSP_ERR       10
#define CALLBACK_MSG_UPGRADE_DATA_RSP_TIMEOUT   11
#define CALLBACK_MSG_UPGRADE_OVER_REQ           12
#define CALLBACK_MSG_UPGRADE_OVER_RSP_OK        13
#define CALLBACK_MSG_UPGRADE_OVER_RSP_ERR       14
#define CALLBACK_MSG_UPGRADE_OVER_RSP_TIMEOUT   15
#define CALLBACK_MSG_UPGRADE_SUCCESSFULL        16

int api_fw_uart_upgrade_recv(unsigned char* precvbuf, int len );
int api_fw_uart_upgrade_wait_over( void );
int api_fw_uart_upgrade_start_buff( unsigned char* pfilebuf, int len, int fw_version, msg_process process );
int api_fw_uart_upgrade_start_file( char* filename, int fw_version, msg_process process );
int api_fw_uart_upgrade_stop( void );

#endif
