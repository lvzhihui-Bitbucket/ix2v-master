#include <sys/types.h>

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <error.h>
#include <signal.h>
#include <pthread.h>
#include <semaphore.h>
#include "cJSON.h"
#include <math.h>

#include "uart.h"
#include "cmr7610_uart.h"
#include "utility.h"
#include "elog.h"
#include "task_Beeper.h"
#include "obj_PublicInformation.h"
#include "define_file.h"

static Serial_Task_Info 		tty_cmr7610;
static pthread_mutex_t	tty_cmr7610_stiLock = PTHREAD_MUTEX_INITIALIZER;

#define RCV_RESET_TIMER		500	//ms

#define CMR7610_UART_CMD_PACK_HEAD				0x02
#define CMR7610_UART_CMD_PACK_TAIL				0x03
#define CMR7610_UART_CMD_PACK_LEN_OFF			1
#define CMR7610_PACKET_MIN_LENGTH				3

#define CMD_REC_OK      0x0A // 当收到一个正确的数据包，回复
#define CMD_REC_ER      0x0B // 当收到一个错误的数据包，回复
#define CMD_FEC_INIT    0x30 // 按索引号DATA1，初始化预置的FEC参数
#define CMD_ZOOM_IN     0x31 // zoom -= 0.01*DATA1
#define CMD_ZOOM_OUT    0x32 // zoom += 0.01*DATA1
#define CMD_SHIFT_LEFT  0x33 // shift_x += DATA1
#define CMD_SHIFT_RIGHT 0x34 // shift_x -= DATA1
#define CMD_SHIFT_UP    0x35 // shift_y -= DATA1
#define CMD_SHIFT_DOWN  0x36 // shift_y += DATA1
#define CMD_GET_FW_VER  0x37 // 获取7610版本号
#define CMD_RSP_FW_VER  0x38 // 7610回复版本号（DATA为字符格式）
#define CMD_GET_STATE   0x39 // 获取7610当前工作状态
#define CMD_RSP_STATE   0x3A // 7610回复当前工作状态(DATA1: 0：IDLE,1：WORKING)
#define CMD_RESET       0x3B // 使7610复位
#define CMD_REQ_ILLUM	0x3c
#define CMD_RSP_ILLUM	0x3d

int cmr7610_send_pack(char cmd, char data);

static CMR7610_BUFFER_POOL HABufferPool={0};

static unsigned short GetBufferLastPos(unsigned short iPos)
{
	if( iPos == 0 ) iPos = CMR7610_POOL_LENGTH-1; else iPos--;
	return iPos;
}
static unsigned short GetBufferNextPos(unsigned short iPos)
{
	iPos++; if( iPos >= CMR7610_POOL_LENGTH ) iPos = 0;
	return iPos;
}
static unsigned short GetBufferLength(void)
{
	if( HABufferPool.ptr_i >= HABufferPool.ptr_o )
		return ( HABufferPool.ptr_i-HABufferPool.ptr_o);
	else
		return ( HABufferPool.ptr_i+CMR7610_POOL_LENGTH-HABufferPool.ptr_o);
}
static void PushIntoBufferPool(unsigned char data)
{
	HABufferPool.Buffer[HABufferPool.ptr_i] = data;
	HABufferPool.ptr_i = GetBufferNextPos(HABufferPool.ptr_i);
	if( HABufferPool.ptr_i == HABufferPool.ptr_o )
		HABufferPool.ptr_o = GetBufferNextPos(HABufferPool.ptr_o);
}
static uint8 PollOutBufferPool(unsigned char *pdata)
{
	if( HABufferPool.ptr_i == HABufferPool.ptr_o )
	{
		return 0;
	}
	else
	{
		*pdata = HABufferPool.Buffer[HABufferPool.ptr_o];
		HABufferPool.ptr_o = GetBufferNextPos(HABufferPool.ptr_o);
		return 1;
	}
}

static OS_TIMER timerRcvReset;
static Cmr7610OnePacket packet = {.len = 0};

static unsigned char UartCheckSum( char* pbuf, int len )
{
	unsigned char checksum = 0;
	int i;
	for( i = 0; i < len; i++ )
	{
		checksum ^= pbuf[i];
	}
	return checksum;
}
void api_cmr7610_recv_callback(char *data,int len)
{
	if(data[2]!=CMD_REC_OK&&data[2]!=CMD_REC_ER)
		cmr7610_send_pack(CMD_REC_OK,0);
	switch(data[2])
	{
		case CMD_RSP_ILLUM:
			if(0)
			{
				unsigned char* ptrDat;;
				ptrDat =(unsigned char*) data;
				int i;
				printf("cmr7610 recv len = %d: ",len);
				for( i = 0; i < len; i++ )
				{
					printf(" %02x ",ptrDat[i]);	
				}
				printf("\n\r"); 
			}
			set_sensor7610_lut(data+3);
			break;
	}
}
static void ttyS1_cmr7610_rcv_handle(char *data, int len)
{
	unsigned short cks,cks_in;
	int i;
	int dataLen;
	
#if 0
	unsigned char* ptrDat;;
	ptrDat =(unsigned char*) data;
	printf("cmr7610 recv len = %d: ",len);
	for( i = 0; i < len; i++ )
	{
		printf(" %02x ",ptrDat[i]);	
	}
	printf("\n\r"); 
#endif	
#if 1
	for(i = 0; i < len; i++)
	{
		PushIntoBufferPool(data[i]);
	}
	
	while( PollOutBufferPool(&packet.data[packet.len]) )
	{
		OS_RetriggerTimer(&timerRcvReset);

		packet.len++;

		if(packet.data[0] != CMR7610_UART_CMD_PACK_HEAD)
		{
			packet.len = 0;
			dprintf("cmr7610 uart cmd pack recv err!\n");
			continue;
		}
		
		if(packet.len > CMR7610_UART_CMD_PACK_LEN_OFF)
		{
			dataLen = packet.data[CMR7610_UART_CMD_PACK_LEN_OFF];
		}
		else
		{
			dataLen = 0;
		}

		if(dataLen == packet.len)
		{
			if(packet.len > CMR7610_PACKET_MIN_LENGTH && packet.data[packet.len-1] == CMR7610_UART_CMD_PACK_TAIL && packet.data[packet.len-2] == UartCheckSum(packet.data+1, packet.len-3))
			{
				api_cmr7610_recv_callback(packet.data, packet.len);
			}
			else
			{
				dprintf("cmr7610 uart cmd pack recv err!\n");
			}
			packet.len = 0;
			continue;
		}

		if(packet.len >= CMR7610_PACKET_MAX_LENGTH)
		{
			packet.len = 0;
			dprintf("cmr7610 uart cmd pack recv too long!\n");
			continue;
		}
	}
#endif
}

static void RcvResetCallBack(void)
{
	packet.len = 0;
	OS_StopTimer(&timerRcvReset);
}

int init_cmr7610_uart(void)
{
	#if defined(PID_IX850)||defined(PID_IX821)
	if( !start_serial_task(&tty_cmr7610, "/dev/ttySAK1", 115200, 8, 'N', 1, &ttyS1_cmr7610_rcv_handle) )
	#else
	if( !start_serial_task(&tty_cmr7610, "/dev/ttySAK2", 115200, 8, 'N', 1, &ttyS1_cmr7610_rcv_handle) )
	#endif
	{
	 	printf("--------------------------cmr7610 uart open  Success!\n");
	}
	else
	{
	 	printf("--------------------------cmr7610 uart open  failuar!\n");
	}	
	HABufferPool.ptr_i = 0;
	HABufferPool.ptr_o = 0;
	packet.len = 0;
	OS_CreateTimer(&timerRcvReset, RcvResetCallBack, RCV_RESET_TIMER/25);
	return 0;
}

int close_cmr7610_uart(void)
{
	stop_serial_task(&tty_cmr7610);
	return 0;
}

int uart_send_data(char *data, int len)
{	
#if 0
	int i;
	unsigned char* ptrDat;;
	ptrDat =(unsigned char*)data;
	dprintf("cmr7610 send len = %d: ",len);
	for( i = 0; i < len; i++ )
	{
		printf(" %02x ",ptrDat[i]);	
	}
	printf("\n\r"); 
#endif	

	pthread_mutex_lock(&tty_cmr7610_stiLock);	
	push_serial_data(&tty_cmr7610, data, len);
	pthread_mutex_unlock(&tty_cmr7610_stiLock);

	return 0;
}

int cmr7610_send_pack(char cmd, char data)
{
	unsigned char datBuf[10];
	datBuf[0] = 0x02;
	datBuf[1] = 6; //length
	datBuf[2] = cmd;
	datBuf[3] = data;
	datBuf[4] = datBuf[1]^datBuf[2]^datBuf[3];
	datBuf[5] = 0x03;	
	return uart_send_data(datBuf,6);
}

int mode_821[9]={0,1,6,7,8,5,2,3,4};
int Cmr7610Config_Callback(cJSON *cmd)
{
	char* msg = GetEventItemString(cmd, "CMD");
	int val = GetEventItemInt(cmd, "DATA");
	char* pbType = API_PublicInfo_Read_String(PB_CMR_TYPE);
	int pbOnState = API_PublicInfo_Read_Int(PB_CMR_ON_STATE);
	int pbMode = API_PublicInfo_Read_Int(PB_CMR_FEC_MODE);
	int pbShiftX = API_PublicInfo_Read_Int(PB_CMR_FEC_SHIFTX);
	int pbShiftY = API_PublicInfo_Read_Int(PB_CMR_FEC_SHIFTY);
	int pbShiftZ = API_PublicInfo_Read_Int(PB_CMR_FEC_SHIFTZ);
	int facMode = API_Para_Read_Int("FactoryMode");
	int facShiftX = API_Para_Read_Int("FactoryShiftx");
	int facShiftY = API_Para_Read_Int("FactoryShifty");
	int facShiftZ = API_Para_Read_Int("FactoryShiftz");
	int shift;
    if(pbType && !strcmp(pbType, "cmr7160") && pbOnState)
	{
		if(!strcmp(msg, "version"))
		{
			cmr7610_send_pack(CMD_GET_FW_VER, 0);
		}
		else if(!strcmp(msg, "state"))
		{
			cmr7610_send_pack(CMD_GET_STATE, 0);
		}
		else if(!strcmp(msg, "mode"))
		{
			if(val>=0 && val<=8)
			{
				if(val == 1)
				{
					#if	defined(PID_IX850)
					val = 5;
					#elif defined(PID_IX821)
					val = 9;
					#elif defined(PID_IX611)
					val = 10;
					#else
					val = 1;
					#endif
				}
				else
				{
					#if defined(PID_IX821)||defined(PID_IX611)
					val = mode_821[val];
					#endif
				}
				if(val != pbMode)
				{
					API_PublicInfo_Write_Int(PB_CMR_FEC_MODE, val);
					cmr7610_send_pack(CMD_FEC_INIT, val);
					API_PublicInfo_Write_Int(PB_CMR_FEC_SHIFTX, 0);
					API_PublicInfo_Write_Int(PB_CMR_FEC_SHIFTY, 0);
					API_PublicInfo_Write_Int(PB_CMR_FEC_SHIFTZ, 0);
				}
				
			}
		}
		else if(!strcmp(msg, "mode_next"))
		{
			if(pbMode<8)
			{
				pbMode++;
			}
			else
			{
				pbMode = 0;
			}
			API_PublicInfo_Write_Int(PB_CMR_FEC_MODE, pbMode);
			cmr7610_send_pack(CMD_FEC_INIT, pbMode);
			if(pbShiftX<0)
			{
				cmr7610_send_pack(CMD_SHIFT_LEFT, abs(pbShiftX));
			}
			else
			{
				cmr7610_send_pack(CMD_SHIFT_RIGHT, pbShiftX);
			}
			if(pbShiftY<0)
			{
				cmr7610_send_pack(CMD_SHIFT_UP, abs(pbShiftY));
			}
			else
			{
				cmr7610_send_pack(CMD_SHIFT_DOWN, pbShiftY);
			}
			if(pbShiftZ<0)
			{
				cmr7610_send_pack(CMD_ZOOM_OUT, abs(pbShiftZ));
			}
			else
			{
				cmr7610_send_pack(CMD_ZOOM_IN, pbShiftZ);
			}
		}
		else if(!strcmp(msg, "reset"))
		{
			cmr7610_send_pack(CMD_FEC_INIT, facMode);
			shift = facShiftX;
			if(shift<0)
			{
				cmr7610_send_pack(CMD_SHIFT_LEFT, abs(shift));
			}
			else
			{
				cmr7610_send_pack(CMD_SHIFT_RIGHT, shift);
			}
			shift = facShiftY;
			if(shift<0)
			{
				cmr7610_send_pack(CMD_SHIFT_UP, abs(shift));
			}
			else
			{
				cmr7610_send_pack(CMD_SHIFT_DOWN, shift);
			}
			shift = facShiftZ;
			if(shift<0)
			{
				cmr7610_send_pack(CMD_ZOOM_OUT, abs(shift));
			}
			else
			{
				cmr7610_send_pack(CMD_ZOOM_IN, shift);
			}
			API_PublicInfo_Write_Int(PB_CMR_FEC_MODE, facMode);
			API_PublicInfo_Write_Int(PB_CMR_FEC_SHIFTX, facShiftX);
			API_PublicInfo_Write_Int(PB_CMR_FEC_SHIFTY, facShiftY);
			API_PublicInfo_Write_Int(PB_CMR_FEC_SHIFTZ, facShiftZ);
		}
		else if(!strcmp(msg, "save"))
		{
			API_Para_Write_Int("UserMode", pbMode);
			API_Para_Write_Int("UserShiftx", facMode==pbMode? (pbShiftX-facShiftX) : pbShiftX);
			API_Para_Write_Int("UserShifty", facMode==pbMode? (pbShiftY-facShiftY) : pbShiftY);
			API_Para_Write_Int("UserShiftz", facMode==pbMode? (pbShiftZ-facShiftZ) : pbShiftZ);
			FecParaSave();
		}
		else if(!strcmp(msg, "factory_save"))
		{
			API_Para_Write_Int("UserMode", pbMode);
			API_Para_Write_Int("UserShiftx", 0);
			API_Para_Write_Int("UserShifty", 0);
			API_Para_Write_Int("UserShiftz", 0);
			API_Para_Write_Int("FactoryMode", pbMode);
			API_Para_Write_Int("FactoryShiftx", pbShiftX);
			API_Para_Write_Int("FactoryShifty", pbShiftY);
			API_Para_Write_Int("FactoryShiftz", pbShiftZ);
			FecParaSave();
		}
		else if(!strcmp(msg, "factory_next"))
		{
			if(!pbMode)
			{
				#if	defined(PID_IX850)
				pbMode = 5;
				#else
				pbMode = 1;
				#endif
			}
			else
			{
				pbMode = 0;
			}
			API_PublicInfo_Write_Int(PB_CMR_FEC_MODE, pbMode);
			cmr7610_send_pack(CMD_FEC_INIT, pbMode);
			if(pbShiftX<0)
			{
				cmr7610_send_pack(CMD_SHIFT_LEFT, abs(pbShiftX));
			}
			else
			{
				cmr7610_send_pack(CMD_SHIFT_RIGHT, pbShiftX);
			}
			if(pbShiftY<0)
			{
				cmr7610_send_pack(CMD_SHIFT_UP, abs(pbShiftY));
			}
			else
			{
				cmr7610_send_pack(CMD_SHIFT_DOWN, pbShiftY);
			}
			if(pbShiftZ<0)
			{
				cmr7610_send_pack(CMD_ZOOM_OUT, abs(pbShiftZ));
			}
			else
			{
				cmr7610_send_pack(CMD_ZOOM_IN, pbShiftZ);
			}
		}
		else if(!strcmp(msg, "shiftl"))
		{
			#if defined(PID_IX821)||defined(PID_IX611)
			if(pbShiftX<120)
			{
				pbShiftX +=4;
				API_PublicInfo_Write_Int(PB_CMR_FEC_SHIFTX, pbShiftX);
				cmr7610_send_pack(CMD_SHIFT_RIGHT, 4);
			}
			#else
			if(pbShiftX>-120)
			{
				pbShiftX -=4;
				API_PublicInfo_Write_Int(PB_CMR_FEC_SHIFTX, pbShiftX);
				cmr7610_send_pack(CMD_SHIFT_LEFT, 4);
			}
			#endif
		}
		else if(!strcmp(msg, "shiftr"))
		{
			#if defined(PID_IX821)||defined(PID_IX611)
			if(pbShiftX>-120)
			{
				pbShiftX -=4;
				API_PublicInfo_Write_Int(PB_CMR_FEC_SHIFTX, pbShiftX);
				cmr7610_send_pack(CMD_SHIFT_LEFT, 4);
			}
			#else
			if(pbShiftX<120)
			{
				pbShiftX +=4;
				API_PublicInfo_Write_Int(PB_CMR_FEC_SHIFTX, pbShiftX);
				cmr7610_send_pack(CMD_SHIFT_RIGHT, 4);
			}
			#endif
		}
		else if(!strcmp(msg, "shiftup"))
		{
			#if defined(PID_IX821)||defined(PID_IX611)
			if(pbShiftY<120)
			{
				pbShiftY +=4;
				API_PublicInfo_Write_Int(PB_CMR_FEC_SHIFTY, pbShiftY);
				cmr7610_send_pack(CMD_SHIFT_DOWN, 4);
			}
			#else
			if(pbShiftY>-120)
			{
				pbShiftY -=4;
				API_PublicInfo_Write_Int(PB_CMR_FEC_SHIFTY, pbShiftY);
				cmr7610_send_pack(CMD_SHIFT_UP, 4);
			}
			#endif
		}
		else if(!strcmp(msg, "shiftdn"))
		{
			#if defined(PID_IX821)||defined(PID_IX611)
			if(pbShiftY>-120)
			{
				pbShiftY -=4;
				API_PublicInfo_Write_Int(PB_CMR_FEC_SHIFTY, pbShiftY);
				cmr7610_send_pack(CMD_SHIFT_UP, 4);
			}
			#else
			if(pbShiftY<120)
			{
				pbShiftY +=4;
				API_PublicInfo_Write_Int(PB_CMR_FEC_SHIFTY, pbShiftY);
				cmr7610_send_pack(CMD_SHIFT_DOWN, 4);
			}
			#endif
		}
		else if(!strcmp(msg, "shiftzo"))
		{
			if(pbShiftZ>-20)
			{
				pbShiftZ--;
				API_PublicInfo_Write_Int(PB_CMR_FEC_SHIFTZ, pbShiftZ);
				cmr7610_send_pack(CMD_ZOOM_OUT, 1);
			}
		}
		else if(!strcmp(msg, "shiftzi"))
		{
			if(pbShiftZ<20)
			{
				pbShiftZ++;
				API_PublicInfo_Write_Int(PB_CMR_FEC_SHIFTZ, pbShiftZ);
				cmr7610_send_pack(CMD_ZOOM_IN, 1);
			}
		}
	}

}

void Cmr7610_PowerOnInit(void)
{
	int facMode = API_Para_Read_Int("FactoryMode");
	int facShiftX = API_Para_Read_Int("FactoryShiftx");
	int facShiftY = API_Para_Read_Int("FactoryShifty");
	int facShiftZ = API_Para_Read_Int("FactoryShiftz");
	int usrMode = API_Para_Read_Int("UserMode");
	int usrShiftX = API_Para_Read_Int("UserShiftx");
	int usrShiftY = API_Para_Read_Int("UserShifty");
	int usrShiftZ = API_Para_Read_Int("UserShiftz");
	int pbMode = API_PublicInfo_Read_Int(PB_CMR_FEC_MODE);
	int pbShiftX = API_PublicInfo_Read_Int(PB_CMR_FEC_SHIFTX);
	int pbShiftY = API_PublicInfo_Read_Int(PB_CMR_FEC_SHIFTY);
	int pbShiftZ = API_PublicInfo_Read_Int(PB_CMR_FEC_SHIFTZ);
	int ioMode,ioShiftX,ioShiftY,ioShiftZ,shift;
	if(facMode == usrMode)
	{
		ioMode = facMode;
		ioShiftX = facShiftX + usrShiftX;
		ioShiftY = facShiftY + usrShiftY;
		ioShiftZ = facShiftZ + usrShiftZ;
	}
	else
	{
		ioMode = usrMode;
		ioShiftX = usrShiftX;
		ioShiftY = usrShiftY;
		ioShiftZ = usrShiftZ;
	}
	//dprintf("Cmr7610_PowerOnInit io=[%d %d %d %d]\n", ioMode, ioShiftX, ioShiftY, ioShiftZ);
	//dprintf("Cmr7610_PowerOnInit pb=[%d %d %d %d]\n", pbMode, pbShiftX, pbShiftY, pbShiftZ);
	cmr7610_send_pack(CMD_FEC_INIT, ioMode);
	dprintf("Cmr7610_PowerOnInit mode=[%d]\n", ioMode);
	//usleep(10*1000);
	shift = ioShiftX;
	dprintf("Cmr7610_PowerOnInit shiftx=[%d]\n", shift);
	if(shift<0)
	{
		cmr7610_send_pack(CMD_SHIFT_LEFT, abs(shift));
	}
	else
	{
		cmr7610_send_pack(CMD_SHIFT_RIGHT, shift);
	}
	shift = ioShiftY;
	//usleep(10*1000);
	dprintf("Cmr7610_PowerOnInit shifty=[%d]\n", shift);
	if(shift<0)
	{
		cmr7610_send_pack(CMD_SHIFT_UP, abs(shift));
	}
	else
	{
		cmr7610_send_pack(CMD_SHIFT_DOWN, shift);
	}
	shift = ioShiftZ;
	dprintf("Cmr7610_PowerOnInit shiftz=[%d]\n", shift);
	//usleep(10*1000);
	if(shift<0)
	{
		cmr7610_send_pack(CMD_ZOOM_OUT, abs(shift));
	}
	else
	{
		cmr7610_send_pack(CMD_ZOOM_IN, shift);
	}

	API_PublicInfo_Write_Int(PB_CMR_FEC_MODE, ioMode);
	API_PublicInfo_Write_Int(PB_CMR_FEC_SHIFTX, ioShiftX);
	API_PublicInfo_Write_Int(PB_CMR_FEC_SHIFTY, ioShiftY);
	API_PublicInfo_Write_Int(PB_CMR_FEC_SHIFTZ, ioShiftZ);
}

void Cmr7610_PowerOff(void)
{
	API_PublicInfo_Write_Int(PB_CMR_FEC_MODE, 0);
	API_PublicInfo_Write_Int(PB_CMR_FEC_SHIFTX, 0);
	API_PublicInfo_Write_Int(PB_CMR_FEC_SHIFTY, 0);
	API_PublicInfo_Write_Int(PB_CMR_FEC_SHIFTZ, 0);
}

void FecParaSave(void)
{
	MakeDir(IO_Cus_Path);
	SetJsonToFile(IO_Factory_FILE_NAME, API_Para_Read_Public("FEC_Config"));
}