
#ifndef CARD_UART_H
#define CARD_UART_H

#define CARD_POOL_LENGTH			20

typedef struct _CARD_BUFFER_POOL_{
	volatile unsigned short ptr_i;
	volatile unsigned short ptr_o;
	unsigned char Buffer[CARD_POOL_LENGTH];
} CARD_BUFFER_POOL;


#define CARD_PACKET_MAX_LENGTH			10

typedef struct {
	unsigned char len;
	unsigned char data[CARD_PACKET_MAX_LENGTH];
} CardOnePacket;

int init_card_uart(void);
int close_card_uart(void);

#endif


