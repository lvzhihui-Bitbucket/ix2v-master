
#ifndef VDP_UART_H
#define  VDP_UART_H

// 1 = STM8���յ�����ָ��,��N329����; - ����ָ���
#define UART_TYPE_S2N_REC_DT_CMD				104

// 2 = N329��STM8����:����ָ�� - macType+uNew(�¾�ָ��)+����ָ���
#define UART_TYPE_N2S_SND_DT_CMD				105

// 3 = STM8ִ�з���ָ��,  ���ط��ͽ����N329 - * ���ͽ��
#define UART_TYPE_S2N_SND_RESULT				106

// 4 = N329��STM8����: ����PT2259 - ͨ��ѡ��(0=MIC; 1=SPK)+˥��DB��
#define UART_TYPE_N2S_PT2259_CTRL				4

// 5 = N329��STM8����: ExtRinger - 0=OFF; 1=ON
#define UART_TYPE_N2S_EXTRINGER_CTRL			5

// 6 = STM8��N329����:   DIP״̬(�ı�) - 5λDIP���ص�ֵ
#define UART_TYPE_S2N_DIP_STATUS				6

// 7 = STM8��N329����:   DoorBell״̬(�ı�) - 0=�ͷ�; 1=���� 
#define UART_TYPE_S2N_DOORBELL_STATUS			7

// 8 = N329��STM8��ȡDIP״̬
#define UART_TYPE_N2S_APPLY_DIP_STATUS			8

// 9 = STM8��N329Ӧ��:   DIP״̬  - 5λDIP���ص�ֵ
#define UART_TYPE_S2N_REPLY_DIP_STATUS			9

// 10 = N329��STM8��ȡDoorBell״̬
#define UART_TYPE_N2S_APPLY_DOORBELL_STATUS		10

// 11 = STM8��N329Ӧ��:   DoorBell״̬  - 0=�ͷ�; 1=���� 
#define UART_TYPE_S2N_REPLY_DOORBELL_STATUS		11

// 12 = N329��STM8����LINKING
#define UART_TYPE_N2S_LINKING					12



// lzh_20161207_s
#define UART_TYPE_N2S_BUS_ACK_SND   			13
#define UART_TYPE_S2N_BUS_ACK_RCV   			14
// lzh_20161207_e


//15 = STM8��N329����:  ����ֵ
#define UART_TYPE_S2N_TOUCHKEY_STATUS     		15      //lyx 20170721

//16 = N329��STM8����: LEDָʾ
#define UART_TYPE_N2S_LED_CTRL					16

//17 = N329��STM8����Application״̬
#define UART_TYPE_N2S_REPOTR_APP_STATUS		17

//18 =  N329��STM8����NOTIFY_STATE                                   
#define UART_TYPE_N2S_NOTIFY_STATE				18

//19 =  STM8��N329����REQUEST_STATE
#define UART_TYPE_S2N_REQUEST_STATE                       19

//20 = STM8��N329����NOTIFY_RESET
#define UART_TYPE_S2N_NOTIFY_RESET                          20

//21 = N329��STM8����RESETָ��
#define UART_TYPE_N2S_RESET                          21


//22 = N329��STM8����: ����AMP_MUTE
#define UART_TYPE_N2S_MUTE_CTRL				22	 //lyx_20170921

//23 = N329��STM8����: ����WIFI_POWER
#define UART_TYPE_N2S_WIFI_CTRL				23

//24 = N329��STM8����TALK_STATE
#define UART_TYPE_N2S_TALK_STATE			24    //lyx_20170930	

#define UART_TYPE_STM8_VERSION_GET      25   //cao_20180604 
#define UART_TYPE_STM8_VERSION_ASK      26   //cao_20180604 

#define UART_TYPE_LOCAL_TIME_GET        27   //cao_20181116
#define UART_TYPE_LOCAL_TIME_UPDATE     28   //cao_20181116 

#define UART_TYPE_BEEP_C				29	//N329��STM8����BEEP

//dh_20200303
#define UART_TYPE_SEND_REBOOT_LOG		40 //  n329->stm8���� REBOOT��¼
#define UART_TYPE_GET_REBOOT_LOG		41 //  
#define UART_TYPE_GET_REBOOT_LOG_RSP	42 //  

#define UART_TYPE_FIRMWARE_M2S        	44   //lzh_20210727	 	master->tiny1616
#define UART_TYPE_FIRMWARE_S2M     		45   //lzh_20210727		tiny1616->master

#define UART_TYPE_GetSn        			46   //master->tiny1616	��ȡ���к�
#define UART_TYPE_GetSn_RSP     		47   //tiny1616->master	��ȡ���к�Ӧ��

#define UART_TYPE_SetSn        			48   //master->tiny1616	�������к�
#define UART_TYPE_SetSn_RSP     		49   //tiny1616->master	�������к�Ӧ��

#define UART_TYPE_AMP_MUTE    				52 

#define UART_TYPE_SWIP_IC_CARD    				54 

#define UART_TYPE_MEM_ACCESS_REQ		101		//soc->mcu 访问eeprom申请
#define UART_TYPE_MEM_ACCESS_RSP		102		//mcu->soc 访问eeprom回复
#define UART_TYPE_LOOP_BACK				103		//soc->mcu
#define UART_TYPE_R_M_CMD_REP			107		// mcu->soc 收到外部模块指令报告
#define UART_TYPE_S_M_CMD_REQ			108		// soc->mcu 转发外部模块指令申请
#define UART_TYPE_S_M_CMD_RSP			109		// mcu->soc 转发外部模块指令回复

#define UART_TYPE_OUTPUT_ACT			110		//soc->mcu 执行器控制
#define UART_TYPE_OUTPUT_BIN			111		//soc->mcu io口控制
#define UART_TYPE_OUTPUT_PWM			112		//soc->mcu PWM口控制
#define UART_TYPE_OUTPUT_LED			113		//soc->mcu LED控制

#define UART_TYPE_INTPUT_EVT_KEY		114		//mcu->soc KEY输入事件
#define UART_TYPE_INTPUT_EVT_BIN		115		//mcu->soc bin输入事件
#define UART_TYPE_INTPUT_EVT_DIP		116		//mcu->soc DIP输入事件
#define UART_TYPE_INTPUT_EVT_ADC		117		//mcu->soc ADC输入事件

#define UART_TYPE_INTPUT_READ_KEY_REQ	118		//soc->mcu KEY读取申请
#define UART_TYPE_INTPUT_READ_KEY_RSP	119		//mcu->soc KEY读取回复
#define UART_TYPE_INTPUT_READ_BIN_REQ	120		//soc->mcu BIN读取申请
#define UART_TYPE_INTPUT_READ_BIN_RSP	121		//mcu->soc BIN读取回复
#define UART_TYPE_INTPUT_READ_DIP_REQ	122		//soc->mcu DIP读取申请
#define UART_TYPE_INTPUT_READ_DIP_RSP	123		//mcu->soc DIP读取回复
#define UART_TYPE_INTPUT_READ_ADC_REQ	124		//soc->mcu ADC读取申请
#define UART_TYPE_INTPUT_READ_ADC_RSP	125		//mcu->soc ADC读取回复
#define UART_TYPE_MEM_CHANGED      		126    //soc->mcu 通知eeprom内容改变

#define UART_TYPE_SWIP_ID_CARD    				127

#define UART_TYPE_BEEP_C				61	//N329��STM8����BEEP
typedef enum{
BUZZER_DI,  // 0	
BUZZER_DIDI, // 1
BUZZER_DIDIDI, // 2	
BUZZER_LONGDI, // 3
}BUZZER_TYPE;

/////////////////////////////////////////////////////////////////////////////
#define TASK_TYPE_MFG_SN    	201
#define TASK_TYPE_MEM_ACESS    	202
#define TASK_TYPE_OUTPUT    	211
#define TASK_TYPE_INPUT_READ  	212
#define TASK_TYPE_INPUT_EVT    	213
#define TASK_TYPE_ZONE      	214
#define TASK_TYPE_REST      	221
#define TASK_TYPE_HEART_BEAT  	222
#define TASK_TYPE_REBOOT    	223
#define TASK_TYPE_ISP      		231
#define TASK_TYPE_MCU_INFO    	232
#define TASK_TYPE_DTS      		241
#define TASK_TYPE_MDS      		242
#define TASK_TYPE_RFID      	243
#define TASK_TYPE_DTS_ADDR_ER  	244
#define TASK_TYPE_DTS_CHSUM_ER  245
#define TASK_TYPE_DTS_HEAD_ER  	246
#define TASK_TYPE_MDS_CHSUM_ER  247
#define TASK_TYPE_MDS_HEAD_ER  	248

// UART FIRMWARE SUB MSG TYPE
#define FIRMWARE_VERSON_REQ		0x01
#define FIRMWARE_VERSON_RSP		0x02
#define UPDATE_FIRMWARE_REQ		0x03
#define UPDATE_FIRMWARE_RSP		0x04
#define FIRMWARE_DATA_TRA_REQ	0x05
#define FIRMWARE_DATA_TRA_RSP	0x06
#define FIRMWARE_TRA_CHECK_REQ	0x07
#define FIRMWARE_TRA_CHECK_RSP	0x08

#define POOL_LENGTH			(200)		// 100

typedef struct _BUFFER_POOL_{
	volatile unsigned short ptr_i;
	volatile unsigned short ptr_o;
	unsigned char Buffer[POOL_LENGTH];
} BUFFER_POOL;

#define PACKET_LENGTH			(200)	// 40

typedef struct {
	unsigned char len;
	unsigned char data[PACKET_LENGTH];
} OnePacket;

/*******************************************************************************************
 * @fn:		Init_vdp_uart
 *
 * @brief:	��ʼ��uart�˿�
 *
 * @param:  	none
 *
 * @return: 	0/ok, -1/err
*******************************************************************************************/
int Init_vdp_uart(void);

/*******************************************************************************************
 * @fn:		vdp_uart_send_data
 *
 * @brief:	���ڷ������ݰ�����
 *
 * @param:  	*data	- ������ָ��
 * @param:  	len		- ���ݰ�����
 *
 * @return: 	0/ok, -1/err
*******************************************************************************************/
int vdp_uart_send_data(char *data, int len);

/*******************************************************************************************
 * @fn:		vdp_uart_recv_data
 *
 * @brief:	���ڽ������ݰ�����
 *
 * @param:  	buf - ����ָ��
 * @param:  	len - ���ݳ���
 *
 * @return: 	0/ok, -1/err
*******************************************************************************************/
int vdp_uart_recv_data( char* buf, int len);

/*******************************************************************************************
 * @fn:		close_vdp_uart
 *
 * @brief:	�رմ��ڼ������Դ
 *
 * @param:  	none
 *
 * @return: 	0/ok, -1/err
*******************************************************************************************/
int close_vdp_uart(void);

int api_uart_recv_callback( char* pbuf, unsigned int len );

int api_uart_send_pack( unsigned char cmd, char* pbuf, unsigned int len );

void LogSendUartCmd(const char* prefix, char cmd, char* pbuf, unsigned int len);
//////////////////////////////////////////////////////////////////////////////////////////////////
// ������ӿڶ���
//////////////////////////////////////////////////////////////////////////////////////////////////

//���߽��ս��������·��ͨ�ű�־��
typedef enum
{
	COM_RCV_NONE = 0,	//���շ�����
	COM_RCV_DAT,		//���߽��յ�����(..\IND)
	COM_RCV_ACK,		//���߽��յ�ACK�ź�(..\ACON)
	COM_RCV_ERR,		//���߽��մ���(..\IND)
} COM_RCV_FLAG;

//���߷��ͽ��������·��ͨ�ű�־��
typedef enum
{
	COM_TRS_NONE = 0,	//���շ�����	
	COM_TRS_DAT,		//���߷���������(..\LCON)
	COM_TRS_ACK,		//���߷�����ACK�ź�(..\LCON)
	COM_TRS_ERR,		//���߷��ʹ���(..\LCON)
	COM_TRS_DAT_HAVE_ACK,		//���߷���������(..\LCON)
} COM_TRS_FLAG;

//���ߴ�����
typedef enum
{	BUS_ERR_NONE = 0,		//����֡����
	BUS_ERR_RCV_LEAD_L_MIN,	//����֡ͷ����С����Сֵ
	BUS_ERR_RCV_LEAD_G_MAX,	//����֡ͷ���ȴ������ֵ
	BUS_ERR_RCV_PACK_L_MIN,	//����֡����Ϊ0
	BUS_ERR_RCV_BIT_ERR,	//����֡λ����С����Сֵ
	BUS_ERR_TRS_LEAD_ERR,	//����֡ͷ���ʹ���
	BUS_ERR_TRS_BIT_ERR,	//����֡λ���ʹ���
} BUS_ERR;

#define COMM_RCV_FLAG_ENABLE(a,b)	{ phComRcvFlag = a; stateError = b; }
#define COMM_TRS_FLAG_ENABLE(a,b)	{ phComTrsFlag = a; stateError = b; }

unsigned char Ph_AckWrite(void);
unsigned char Ph_DataWrite(unsigned char *pdat,unsigned char len,unsigned char uNew);
unsigned char Ph_PollingReadResult(unsigned char *pdat,COM_RCV_FLAG *flag);
unsigned char Ph_PollingWriteResult(COM_TRS_FLAG *flag);

#endif


