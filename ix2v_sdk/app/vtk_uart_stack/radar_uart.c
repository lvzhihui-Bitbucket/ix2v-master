#include <sys/types.h>

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <error.h>
#include <signal.h>
#include <pthread.h>
#include <semaphore.h>
#include "cJSON.h"

#include "uart.h"
#include "radar_uart.h"
#include "utility.h"

#include "elog.h"
#include "obj_PublicInformation.h"
#include "obj_IoInterface.h"

Serial_Task_Info 		tty_radar;
static pthread_mutex_t	tty_radar_stiLock = PTHREAD_MUTEX_INITIALIZER;

static sem_t				radar_recv_sem;
char radar_rsp_buff[30];
char radar_rsp_len=0;

#define RCV_RESET_TIMER		500	//ms
static OneRadarPacket packet;

static RADAR_BUFFER_POOL HABufferPool={0};

static int radar_online=0;

static unsigned short GetBufferLastPos(unsigned short iPos)
{
	if( iPos == 0 ) iPos = RADAR_POOL_LENGTH-1; else iPos--;
	return iPos;
}
static unsigned short GetBufferNextPos(unsigned short iPos)
{
	iPos++; if( iPos >= RADAR_POOL_LENGTH ) iPos = 0;
	return iPos;
}
static unsigned short GetBufferLength(void)
{
	if( HABufferPool.ptr_i >= HABufferPool.ptr_o )
		return ( HABufferPool.ptr_i-HABufferPool.ptr_o);
	else
		return ( HABufferPool.ptr_i+RADAR_POOL_LENGTH-HABufferPool.ptr_o);
}
static void PushIntoBufferPool(unsigned char data)
{
	HABufferPool.Buffer[HABufferPool.ptr_i] = data;
	HABufferPool.ptr_i = GetBufferNextPos(HABufferPool.ptr_i);
	if( HABufferPool.ptr_i == HABufferPool.ptr_o )
		HABufferPool.ptr_o = GetBufferNextPos(HABufferPool.ptr_o);
}
static uint8 PollOutBufferPool(unsigned char *pdata)
{
	if( HABufferPool.ptr_i == HABufferPool.ptr_o )
	{
		return 0;
	}
	else
	{
		*pdata = HABufferPool.Buffer[HABufferPool.ptr_o];
		HABufferPool.ptr_o = GetBufferNextPos(HABufferPool.ptr_o);
		return 1;
	}
}

static OS_TIMER timerRcvReset;

unsigned short radar_checksum( char* pbuf, int len )
{
	unsigned short checksum = 0;
	int i;
	for( i = 0; i < len; i++ )
	{
		checksum += pbuf[i];
		//printf("checksum dat[%d]=0x%02x\n",i,pbuf[i]);
	}
	//printf("checksum=0x%04x\n",checksum);
	return checksum;
}	

void ttyS1_rcv_handle(char *data, int len)
{
	unsigned short cks,cks_in;
	int i;
	int dataLen;
	
#if 0
	unsigned char* ptrDat;;
	ptrDat =(unsigned char*) data;
	dprintf("uart recv len = %d: ",len);
	for( i = 0; i < len; i++ )
	{
		printf(" %02x ",ptrDat[i]);	
	}
	printf("\n\r"); 
#endif	

	for(i = 0; i < len; i++)
	{
		PushIntoBufferPool(data[i]);
	}
	
	while( PollOutBufferPool(&packet.data[packet.len]) )
	{
		OS_RetriggerTimer(&timerRcvReset);
		// get head
		if(packet.data[0] != RADAR_UART_CMD_RSP_HEAD)
		{
			packet.len = 0;
			dprintf("radar uart cmd pack recv err!\n");
			continue;
		}
		packet.len++;			
		// get length byte
		if(packet.len <= RADAR_UART_CMD_PACK_LEN_OFF)
			continue;

		// get data length
		dataLen = packet.data[RADAR_UART_CMD_PACK_LEN_OFF]+RADAR_UART_CMD_PACK_DAT_OFF+sizeof(cks);
		if( dataLen >= RADAR_PACKET_MAX_LENGTH )
		{
			packet.len = 0;
			dprintf("radar uart cmd pack recv too long!\n");
			continue;
		}

		// contiune receive
		if(packet.len < dataLen)
			continue;
		
		cks = radar_checksum( &packet.data[RADAR_UART_CMD_PACK_HEAD_OFF], dataLen-sizeof(cks));
		cks_in = packet.data[dataLen-1];
		cks_in <<= 8;
		cks_in += packet.data[dataLen-2];
		if( cks != cks_in )
			dprintf("radar uart cmd pack checksum errr,cks[0x%04x]!=[0x%04x]\n",cks,cks_in);
		else
			api_radar_recv_callback(packet.data+RADAR_UART_CMD_PACK_CMD_OFF, dataLen-1-sizeof(cks));
		packet.len = 0;
	}
}

int radar_uart_send_data(char *data, int len)
{
#if 0
	int i;
	unsigned char* ptrDat;;
	ptrDat =(unsigned char*)data;
	dprintf("radar send len = %d: ",len);
	for( i = 0; i < len; i++ )
	{
		printf(" %02x ",ptrDat[i]);	
	}
	printf("\n\r"); 
#endif		
	pthread_mutex_lock(&tty_radar_stiLock);	
	push_serial_data(&tty_radar, data, len);
	pthread_mutex_unlock(&tty_radar_stiLock);
	return 0;
}

static void RcvResetCallBack(void)
{
	packet.len = 0;
	OS_StopTimer(&timerRcvReset);
}

int init_radar_uart(void)
{
	//if(access("/usr/modules/usbgadget/setty", F_OK) == 0)
	if( !start_serial_task(&tty_radar, access("/usr/modules/usbgadget/setty", F_OK) == 0?"/dev/ttySAK0":"/dev/ttySAK1", 115200, 8, 'N', 1, &ttyS1_rcv_handle) )
	{
	 	printf("--------------------------radar uart open  Success!\n");
	}
	else
	{
	 	printf("--------------------------radar uart open  failuar!\n");
	}	
	HABufferPool.ptr_i = 0;
	HABufferPool.ptr_o = 0;
	packet.len = 0;
	OS_CreateTimer(&timerRcvReset, RcvResetCallBack, RCV_RESET_TIMER/25);
	return 0;
}

int close_radar_uart(void)
{
	stop_serial_task(&tty_radar);
	return 0;
}

int radar_send_pack(char head, char cmd, char* pbuf, unsigned int len )
{
	unsigned short checksum;
	char send_buf[RADAR_PACKET_MAX_LENGTH];
	int ret;

	if( len > RADAR_PACKET_MAX_LENGTH ) len = RADAR_PACKET_MAX_LENGTH;

	send_buf[RADAR_UART_CMD_PACK_HEAD_OFF] 	= head;
	send_buf[RADAR_UART_CMD_PACK_CMD_OFF]	= cmd;
	send_buf[RADAR_UART_CMD_PACK_LEN_OFF]	= len;

	if( len ) memcpy( send_buf + RADAR_UART_CMD_PACK_DAT_OFF, pbuf, len );

	checksum = radar_checksum( send_buf, len + RADAR_UART_CMD_PACK_DAT_OFF );

	send_buf[RADAR_UART_CMD_PACK_DAT_OFF+len+0] = checksum&0xff;
	send_buf[RADAR_UART_CMD_PACK_DAT_OFF+len+1] = (checksum>>8)&0xff;

	ret = radar_uart_send_data(send_buf,RADAR_UART_CMD_PACK_DAT_OFF+len+sizeof(checksum));

	return ret;
}

int api_radar_request_pack(char cmd, char* pbuf, unsigned int len ,int wait_ack)
{
	int ret=0;
	if(wait_ack)
		sem_init(&radar_recv_sem,0,0);
	ret=radar_send_pack(RADAR_UART_CMD_REQ_HEAD, cmd, pbuf, len);
	if(wait_ack)
	{
	 	if(sem_wait_ex2(&radar_recv_sem, 1000) != 0)
	 	{
			return -1;
	 	}
		if(radar_rsp_len>=3&&radar_rsp_buff[0]==cmd)
		{
			return radar_rsp_buff[2];
		}
		return 0;
	}
	return ret;
}

int api_radar_response_pack(char cmd, char* pbuf, unsigned int len )
{
	return radar_send_pack(RADAR_UART_CMD_RSP_HEAD, cmd, pbuf, len);
}

int api_radar_recv_callback( char* pbuf, unsigned int len )
{
	char rsp_cmd,rsp_len;

	if(len < 1)
	{
		return 0;
	}

	#if 1	
	int i;
	dprintf("api_radar_recv_callback len = %d: ",len);
	for( i = 0; i < len; i++ )
	{
		printf(" %02x ",pbuf[i]);	
	}
	printf("\n\r"); 
	//PrintCurrentTime(101);
	#endif	

	rsp_cmd = pbuf[0];
	rsp_len = pbuf[1];
	//ShellCmdPrintf("RADAR_CMD_recv: cmd[0x%02x], len[%d], result[%d]\n", rsp_cmd, rsp_len, pbuf[2]);
	radar_online=1;
	radar_rsp_len=len;
	if(radar_rsp_len>30)
		radar_rsp_len=30;
	memcpy(radar_rsp_buff,pbuf,radar_rsp_len);
	sem_post(&radar_recv_sem);
	switch(rsp_cmd)
	{
		// 运动感应距离
		case RADAR_CMD_DISTANCE_LEVEL_SET:
			//printf("RADAR_CMD_DISTANCE_LEVEL_SET: cmd[0x%02x], len[%d], result[%d]\n", rsp_cmd, rsp_len, pbuf[2]);
			break;

		case RADAR_CMD_DISTANCE_LEVEL_GET:
			//printf("RADAR_CMD_DISTANCE_LEVEL_GET: cmd[0x%02x], len[%d], level[%d]\n", rsp_cmd, rsp_len, pbuf[2]);
			break;

		// 亮灯时间
		case RADAR_CMD_LAMP_ON_TIME_SET:
		case RADAR_CMD_LAMP_ON_TIME_GET:
			break;

		// 光敏阈值
		case RADAR_CMD_LUX_THRESHOLD_SET:
		case RADAR_CMD_LUX_THRESHOLD_GET:
			break;

		// 运动检测deltra阈值
		case RADAR_CMD_DELTA_VALUE_SET:
		case RADAR_CMD_DELTA_VALUE_GET:
			break;

		// 微动检测距离
		case RADAR_CMD_MOTION_DISTANCE_LEVEL_SET:
		case RADAR_CMD_MOTION_DISTANCE_LEVEL_GET:
			break;

		// 开关灯
		case RADAR_CMD_LAMP_SWITCH_SET:
			break;

		// 设置占空比
		case RADAR_CMD_LAMP_DUTY_SET:
			break;

		// 打开/关闭雷达
		case RADAR_CMD_OPEN_CLOSE_SET:
		case RADAR_CMD_OPEN_CLOSE_GET:
			break;

		// 是否保存雷达设置的开关
		case RADAR_CMD_SAVE_SWITCH_SET:
		case RADAR_CMD_SAVE_SWITCH_GET:
			break;

		// 系统复位
		case RADAR_CMD_SYSTEM_RESET:
			break;

		// debug command
		// 寄存器读写
		case RADAR_CMD_REGISTER_WRITE:
		case RADAR_CMD_REGISTER_READ:
			break;

		// 内存读写
		case RADAR_CMD_MEMORY_WRITE:
		case RADAR_CMD_MEMORY_READ:
		case RADAR_CMD_MEMORY_DUMP:
			break;

		// flash写
		case RADAR_CMD_FLASH_WRITE:
			break;
	}
}

int api_radar_set_distance_level( int level )
{
	char buffer[2];
	buffer[0] = level;
	return api_radar_request_pack(RADAR_CMD_DISTANCE_LEVEL_SET,buffer,1,1);
}

int api_radar_get_distance_level( void )
{
	int ret;
	
	if((ret=api_radar_request_pack(RADAR_CMD_DISTANCE_LEVEL_GET,NULL,0,1))>=0)
	{
		return ret;
	#if 0
		if(radar_rsp_len>=3&&radar_rsp_buff[0]==RADAR_CMD_DISTANCE_LEVEL_GET)
		{
			return radar_rsp_buff[2];
		}
		#endif
	}
	return -1;
}

int api_radar_set_motion_distance_level( int level )
{
	char buffer[2];
	buffer[0] = level;
	return api_radar_request_pack(RADAR_CMD_MOTION_DISTANCE_LEVEL_SET,buffer,1,1);
}
/*
ix850 radar test: radard cmd len dat1 dat2 ...\n\teg: radard 2 1 15
*/
int ShellCmd_radar_debug(cJSON *cmd)
{
	char* pdat;
	int i, cdt, len;
	char data[RADAR_PACKET_LENGTH];
	int ret;

    char* str_cmd = GetShellCmdInputString(cmd, 1);	
    char* str_len = GetShellCmdInputString(cmd, 2);
    printf("radar debug cmd[%s], len[%s]\n",str_cmd, str_len);

	cdt = atoi(str_cmd);
	len = atoi(str_len);
	for( i = 0; i < len; i++ )
	{
		pdat= GetShellCmdInputString(cmd, 3+i);
		data[i] = atoi(pdat);
	}

	if((ret=api_radar_request_pack(cdt,data,len,1))>=0)
	{
		if(radar_rsp_len>=3)
			ShellCmdPrintf("RADAR_debug_recv: cmd[0x%02x], len[%d], result[%d],ret=%d\n", radar_rsp_buff[0], radar_rsp_len, radar_rsp_buff[2],ret);
	}
	else
		ShellCmdPrintf("RADAR_debug_err");
	//ShellCmdPrintf("ShellCmd_radar_debug,cmd[%d],len[%d]\n",cdt, len);

	return 0;
}

int radar_modules_check(void)
{
	int cnt=3;
	radar_online=0;
	while(radar_online==0&&cnt--)
	{
		
		api_radar_request_pack(RADAR_CMD_DISTANCE_LEVEL_GET,NULL,0,1);
		
		
		usleep(200*1000);
	}
	return radar_online;
}
void radar_modules_init(void)
{
	int disance;
	int save_flag=0;
	int ret;
	char buff[10];
	cJSON* temp = NULL;
	if(radar_modules_check())
	{
		temp = cJSON_CreateTrue();
		
		if((ret=api_radar_request_pack(RADAR_CMD_OPEN_CLOSE_GET,NULL,0,1))!=1)
		{
			//printf("!=!=======%s%d,%d\n",__func__,__LINE__,ret);
			save_flag=1;
			usleep(100*1000);
			buff[0]=1;
			api_radar_request_pack(RADAR_CMD_OPEN_CLOSE_SET,buff,1,1);
		}
		usleep(100*1000);
		if((ret=api_radar_request_pack(RADAR_CMD_LAMP_ON_TIME_GET,NULL,0,1))!=2)
		{
			//printf("!=!=======%s%d,%d\n",__func__,__LINE__,ret);
			save_flag=1;
			usleep(100*1000);
			buff[0]=2;
			api_radar_request_pack(RADAR_CMD_LAMP_ON_TIME_SET,buff,1,1);
		}	
		usleep(100*1000);
		disance=API_Para_Read_Int(RADAR_DISTANCE_LEVEL);
		if((ret=api_radar_get_distance_level())!=disance)
		{
			//printf("!=!=======%s%d,%d\n",__func__,__LINE__,ret);
			save_flag=1;
		}
		usleep(100*1000);
		api_radar_set_distance_level(disance);
		if(save_flag)
		{
			usleep(100*1000);
			buff[0]=1;
			api_radar_request_pack(RADAR_CMD_SAVE_SWITCH_SET,buff,1,1);
		}
		else
		{
			//printf("=====all saved=====%s%d\n",__func__,__LINE__);
		}
		
		//api_radar_set_dis{tance_level(disance);
	}
	else
	{
		temp = cJSON_CreateFalse();
	}

	API_PublicInfo_Write(PB_RADAR_MODULE, temp);
	cJSON_Delete(temp);
}