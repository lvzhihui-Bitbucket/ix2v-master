#include <stdio.h>

#include "task_DtCaller.h"
#include "obj_SYS_VER_INFO.h"
#include "elog_forcall.h"
#include "obj_PublicInformation.h"
#include "task_Event.h"
#include "obj_GetInfoByIp.h"
#include "obj_IoInterface.h"
#include "define_file.h"

#define LocalMemoDir		JPEG_STORE_DIR	//"/mnt/nand1-2/UserData/video"
#define SdMemoDir		VIDEO_STORE_DIR//"/mnt/sdcard/video"
#define MaxLocalRecFile	15
#define MaxLocalRecFileTime		3600*24*30
int checkMemSpaceTimer(int time);

int get_onedir_oldest_file(char *path,char *file)
{
	DIR *dir = NULL;
	struct dirent *entry;
	char fullname[100];
	int ret=0;
	int oldest_time=0;
	
	
	dir = opendir(path);  
	if(dir == NULL)
	{
		printf("---open filedir failed!----\r\n");     
		return -1;
	}
	
	

	while(entry = readdir(dir))
	{

		  //������..���͡�.������Ŀ¼
		 if(strcmp(entry->d_name,".") == 0 || strcmp(entry->d_name,"..") == 0)
		 {
		   	continue;
		 }
		
		
		snprintf(fullname,100,"%s/%s",path,entry->d_name);

		struct stat st; 
		
      		if ( stat( fullname, &st ) == -1 )
      		{
			printf(" get file stat error\n");     
      		}
    
           	if( S_ISDIR(st.st_mode) )      //������ļ��в�������
           	{
                 	continue;				//��Ŀ¼����ѭ�� 
           	}     
	         else 
	         {	
	         	if(oldest_time==0||st.st_mtime<oldest_time)		
	         	{
				strcpy(file,fullname);
				oldest_time=st.st_mtime;
				ret=1;
			}
	         }

	}
	
	closedir(dir);
	
	return ret;
}

void LocalRecSpaceManager(void)
{	
	int try_cnt=0;
	char filename[200];
	while(try_cnt++<10&&GetVideoNum()>=MaxLocalRecFile)
	{
		if(get_onedir_oldest_file(LocalMemoDir,filename))
		{
			remove(filename);
		}
	}
}

int VDIRManager_GetNewFile(char *file)
{
	int sd_link;
	char filedir[100];
	char filepath[200];
	time_t t;
	struct tm *tblock; 
	
	t = time(NULL); 
	tblock=localtime(&t);

	
	//sprintf( strtime,"%02d%02d%02d_%02d%02d%02d.avi",tblock->tm_year-100,tblock->tm_mon+1,tblock->tm_mday,tblock->tm_hour,tblock->tm_min,tblock->tm_sec);
	if(sd_link=Judge_SdCardLink())
	{
		sprintf(filedir,"%s%02d%02d%02d/",SdMemoDir,(tblock->tm_year-100)>0?(tblock->tm_year-100):0,tblock->tm_mon+1,tblock->tm_mday);
		sprintf(file,"%s%02d%02d%02d.mp4",filedir,tblock->tm_hour,tblock->tm_min,tblock->tm_sec);
	}
	else
	{
		//sprintf(filedir,"%s/%02d%02d%02d",LocalMemoDir,(tblock->tm_year-100)>0?(tblock->tm_year-100):0,tblock->tm_mon+1,tblock->tm_mday);
		sprintf(filedir,"%s",LocalMemoDir);
		sprintf(file,"%s%02d%02d%02d%02d%02d%02d.mp4",filedir,(tblock->tm_year-100)>0?(tblock->tm_year-100):0,tblock->tm_mon+1,tblock->tm_mday,tblock->tm_hour,tblock->tm_min,tblock->tm_sec);
		//LocalRecSpaceManager();
	}
	create_multi_dir(filedir);
	API_Add_TimingCheck(checkMemSpaceTimer, 10);
	

	return 0;
}

int DelRecFileBeforeTime(char *path, int deltime)
{
	DIR *dir = NULL;
	struct dirent *entry;
	char fullname[100];
	char cmd[100];
	struct tm *tblock; 	
	time_t now_time = time(NULL); 
	tblock=localtime(&now_time);
	
	
	dir = opendir(path);  
	if(dir == NULL)
	{
		printf("---open filedir failed!----\r\n");     
		return -1;
	}
	while(entry = readdir(dir))
	{
		 if(strcmp(entry->d_name,".") == 0 || strcmp(entry->d_name,"..") == 0)
		 {
		   	continue;
		 }
		
		
		snprintf(fullname,100,"%s/%s",path,entry->d_name);

		struct stat st; 
		
		if ( stat( fullname, &st ) == -1 )
		{
			printf(" get file stat error\n");     
		}

		if( S_ISDIR(st.st_mode) )      
		{
			if((now_time - st.st_mtime) > deltime)
			{
				snprintf(cmd,100,"rm -r %s",fullname);
				system(cmd);	
			}
					
		}     
		else 
		{	
			if((now_time - st.st_mtime) > deltime)
			{
				remove(fullname);				
			}
		}

	}
	
	closedir(dir);
	
	return 0;
}

int getDiscUseSize(char *path)
{
	DIR *dir = NULL;
	char fullname[100];
	struct dirent *entry;
	struct stat st;
	int sizeuse = 0;

	dir = opendir(path);  
	if(dir == NULL)
	{
		printf("---open filedir failed!----\r\n");     
		return -1;
	}
	while(entry = readdir(dir))
	{
		if(strcmp(entry->d_name,".") == 0 || strcmp(entry->d_name,"..") == 0)
		{
			continue;
		}
		snprintf(fullname,100,"%s/%s",path,entry->d_name);
		if ( stat( fullname, &st ) == 0 )
		{
			if( S_ISDIR(st.st_mode) )      
			{
				continue;				
			}     
			else 
			{
				sizeuse += st.st_size;
			}
		}
	}
	closedir(dir);
	return sizeuse/(1024*1024);
}
int get_onedir_oldest_filedir(char *path,char *file)
{
	DIR *dir = NULL;
	struct dirent *entry;
	char fullname[100];
	int ret=0;
	int oldest_time=0;
	
	dir = opendir(path);  
	if(dir == NULL)
	{
		printf("---open filedir failed!----\r\n");     
		return -1;
	}
	while(entry = readdir(dir))
	{
		 if(strcmp(entry->d_name,".") == 0 || strcmp(entry->d_name,"..") == 0)
		 {
		   	continue;
		 }
		
		
		snprintf(fullname,100,"%s/%s",path,entry->d_name);

		struct stat st; 
		
		if ( stat( fullname, &st ) == -1 )
		{
			printf(" get file stat error\n");     
		}

		if( S_ISDIR(st.st_mode) )      
		{
			if(oldest_time==0||st.st_mtime<oldest_time)		
			{
				strcpy(file,fullname);
				oldest_time=st.st_mtime;
				ret=1;
			}
		}  
		else
		{
			if(oldest_time==0||st.st_mtime<oldest_time)		
			{
				strcpy(file,fullname);
				oldest_time=st.st_mtime;
				ret=1;
			}
		}   
		
	}
	
	closedir(dir);
	
	return ret;
}

int ifDiscHaveFile(char *path)
{
	int ret=0;
	DIR *dir = NULL;
	struct dirent *entry;
	dir = opendir(path);  
	if(dir == NULL)
	{
		printf("---open filedir failed!----\r\n");     
		return 0;
	}
	while(entry = readdir(dir))
	{
		if(strcmp(entry->d_name,".") != 0 && strcmp(entry->d_name,"..") != 0)
		{
			ret = 1;
			break;
		}
	}
	closedir(dir);
	return ret;
}

int memSpaceManager(int filesize)
{
	int sizeLimit,freesize,usesize,sizeNeed,totalsize; 
	char filename[200];
	char filepath[100];
	char cmd[100];
	int sd_link;
	if(sd_link = Judge_SdCardLink())
	{
		snprintf(filepath,100,"%s",SdMemoDir);
	}
	else
	{
		snprintf(filepath,100,"%s",LocalMemoDir);
	}
	if( access( filepath, F_OK ) < 0 )
	{
		mkdir( filepath, 0777 );
	}
	if(sd_link)
	{
		getDiscSize(filepath, &freesize, &totalsize);	
		sizeLimit = totalsize - 100;
		sizeNeed = sizeLimit/2>(filesize + 100)? sizeLimit/2 : (filesize + 100);
		dprintf("limitsize=%d freesize=%d totalsize=%d\n", sizeLimit, freesize, totalsize);
		API_PublicInfo_Write_Int(PB_MEM_LIMIT_SIZE, sizeLimit);
		if(freesize < filesize + 100 )
		{
			while(getDiscFreeSize(filepath) < sizeNeed && ifDiscHaveFile(filepath))
			{
				if(get_onedir_oldest_filedir(filepath,filename))
				{
					//ShellCmdPrintf("get_onedir_oldest_file %s \n", filename);
					snprintf(cmd,100,"rm -r %s",filename);
					system(cmd);
				}
			}
			if(getDiscFreeSize(filepath) < filesize + 100)
			{
				dprintf("memSpaceManager error!!!\n");
				snprintf(cmd,100,"rm -r %s",SdMemoDir);
				system(cmd);
				return -1;//空间维护失败
			}
		}
		else
		{
			DelRecFileBeforeTime(filepath, MaxLocalRecFileTime);
		}
	}
	else
	{
		freesize = getDiscFreeSize(filepath);
		usesize = getDiscUseSize(filepath);
		sizeLimit = freesize + usesize - 10;
		sizeNeed = (10+sizeLimit/2)>(filesize + 10)? (10+sizeLimit/2) : (filesize + 10); 
		dprintf("limitsize=%d freesize=%d usesize=%d\n", sizeLimit, freesize, usesize);
		API_PublicInfo_Write_Int(PB_MEM_LIMIT_SIZE, sizeLimit);
		if(freesize < filesize + 10 )
		{
			while(getDiscFreeSize(filepath) < sizeNeed && GetVideoNum())
			{
				if(get_onedir_oldest_file(filepath,filename))
				{
					remove(filename);
				}
			}
			if(getDiscFreeSize(filepath) < filesize + 10)
			{
				dprintf("memSpaceManager error!!!\n");
				snprintf(cmd,100,"rm -r %s",LocalMemoDir);
				system(cmd);
				return -1;//空间维护失败
			}
		}
		else
		{
			DelRecFileBeforeTime(filepath, MaxLocalRecFileTime);
			//LocalRecSpaceManager();
		}
	}
	

	return 0;
}

int checkMemSpaceTimer(int time)
{
	if(time == 10)
	{
		memSpaceManager(2);
		return 2;
	}
	return 0;
}

int ShellCmd_MemSpace(cJSON *cmd)
{
	char* jsonstr = GetShellCmdInputString(cmd, 1);
	int ret = memSpaceManager(atoi(jsonstr));

	ShellCmdPrintf("memSpaceManager ret =%d \n", ret);
}