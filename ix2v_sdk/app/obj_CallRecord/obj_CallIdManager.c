#include <stdio.h>

#include "task_DtCaller.h"
#include "obj_SYS_VER_INFO.h"
#include "elog_forcall.h"
#include "obj_PublicInformation.h"
#include "task_Event.h"
#include "obj_GetInfoByIp.h"
#include "obj_IoInterface.h"

static int call_sn=0;

int CreateNewCallID(char *call_id)
{
	//pthread_mutex_lock(&DsCallRecord_lock);
	call_sn=API_Para_Read_Int(CALL_Next_Sn);
	sprintf(call_id,"%s%04d",GetSysVerInfo_Sn(),call_sn);
	call_sn++;
	API_Para_Write_Int(CALL_Next_Sn, call_sn);
	//pthread_mutex_unlock(&DsCallRecord_lock);
	return 0;
}


