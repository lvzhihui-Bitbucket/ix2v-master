#include <stdio.h>

#include "task_DtCaller.h"
#include "obj_SYS_VER_INFO.h"
#include "elog_forcall.h"
#include "obj_PublicInformation.h"
#include "task_Event.h"
#include "obj_GetInfoByIp.h"
#include "obj_IoInterface.h"
#include "task_Event.h"
#include "ffmpeg_rec.h"

static char Cur_Rec_Path[200]={0};
pthread_mutex_t RecManager_lock=PTHREAD_MUTEX_INITIALIZER;
#define RecValidTime			15
#define RecManagerWait_Max	3
time_t Rec_Last_time=0;
void RecManagerCallBack(void *arg);
typedef struct
{
	int state;
	int ret;
	sem_t sem;
	char filepath[120];
}RecManagerWait_Stru;
RecManagerWait_Stru RecManagerWait_List[RecManagerWait_Max]={0};
int RecManager_FileReq(cJSON *cmd)
{
	cJSON *rsp;
	char Cur_Rec_Path_temp[200];
	int retsult=-1;
	//char *str=cJSON_Print(cmd);
	//printf("%s:\n%s\n",__func__,str);
	//free(str);
	time_t cur_time=time(NULL);
	//char *from=cJSON_GetStringValue(cJSON_GetObjectItemCaseSensitive(cmd,"From"));
	//char *ref=cJSON_GetStringValue(cJSON_GetObjectItemCaseSensitive(cmd,"ID"));
	char *ctrl=cJSON_GetStringValue(cJSON_GetObjectItemCaseSensitive(cmd,"Ctrl"));
	RecManagerWait_Stru *pwait=NULL;
	GetJsonDataPro(cmd, "Wait", &pwait);
	
	pthread_mutex_lock(&RecManager_lock);
	if(API_Para_Read_Int(EVR_ENABLE))
	{
		if(ctrl!=NULL&&strcmp(ctrl,"Stop")==0)
		{
			api_channel_rec_stop(0);
		}
		else
		{
			if(abs(cur_time-Rec_Last_time)<=RecValidTime&&strlen(Cur_Rec_Path)>0)
			{
				retsult=0;
				{
					rsp=cJSON_Duplicate(cmd,1);
					cJSON_ReplaceItemInObject(rsp,EVENT_KEY_EventName,cJSON_CreateString(EventRecFileRsp));
					cJSON_AddStringToObject(rsp,"Rec_File",Cur_Rec_Path);
					API_Event_Json(rsp);
					cJSON_Delete(rsp);
				}
			}
			else
			{
				if(VDIRManager_GetNewFile(Cur_Rec_Path)==0)
				{
					api_channel_rec_stop(0);
					sprintf(Cur_Rec_Path_temp,"%s.tmp.mp4",Cur_Rec_Path);
					//#ifdef PID_IX850
					//if(strstr(Cur_Rec_Path,"sdcard")!=NULL&&api_rec_one_file_start(0,0,Cur_Rec_Path_temp,strstr(Cur_Rec_Path,"sdcard")!=NULL?10:3,RecManagerCallBack)>=0)
					//#else
					if(api_rec_one_file_start(0,0,Cur_Rec_Path_temp,strstr(Cur_Rec_Path,"sdcard")!=NULL?10:3,RecManagerCallBack)>=0)
					//#endif
					{
						Rec_Last_time=cur_time;
						retsult=0;
						{
							rsp=cJSON_Duplicate(cmd,1);
							cJSON_ReplaceItemInObject(rsp,EVENT_KEY_EventName,cJSON_CreateString(EventRecFileRsp));
							cJSON_AddStringToObject(rsp,"Rec_File",Cur_Rec_Path);
							API_Event_Json(rsp);
							cJSON_Delete(rsp);
						}
					}
				}
			}
		}
	}
	if(pwait!=NULL&&pwait->state)
	{
		pwait->ret=retsult;
		if(retsult>=0)
		{
			strcpy(pwait->filepath,Cur_Rec_Path);
		}
		sem_post(&pwait->sem);
	}
	pthread_mutex_unlock(&RecManager_lock);
	return 1;
}

int RecManager_StopRecReq(cJSON *cmd)
{
	#if 0
	pthread_mutex_lock(&RecManager_lock);
	api_channel_rec_stop(0);
	pthread_mutex_unlock(&RecManager_lock);
	#endif
}
void API_trigOneRec(void)
{
	cJSON *rec_event;
	rec_event=cJSON_CreateObject();
	cJSON_AddStringToObject(rec_event, EVENT_KEY_EventName,EventRecFileReq);
	cJSON_AddStringToObject(rec_event, "From","trigOneRec");  
	//cJSON_AddStringToObject(rec_event, "ID",call_id);
	API_Event_Json(rec_event);
	cJSON_Delete(rec_event);
}
RecManagerWait_Stru *GetOneRecManagerWait(void)
{
	int i;
	RecManagerWait_Stru *ret=NULL;
	pthread_mutex_lock(&RecManager_lock);
	for(i=0;i<RecManagerWait_Max;i++)
	{
		if(RecManagerWait_List[i].state==0)
		{
			RecManagerWait_List[i].state=1;
			ret=&RecManagerWait_List[i];
			break;
		}
	}
	pthread_mutex_unlock(&RecManager_lock);
	
	return ret;
}
void ReleaseOneRecManagerWait(RecManagerWait_Stru *pwait)
{
	if(pwait==NULL)
		return;
	
	pthread_mutex_lock(&RecManager_lock);
	pwait->state=0;
	pthread_mutex_unlock(&RecManager_lock);
}
	
int API_ReqOneRecFile(char *filepath)
{
	int ret=-1;
	RecManagerWait_Stru *pwait;
	pwait=GetOneRecManagerWait();
	if(pwait==NULL)
		return -1;
	
	sem_init(&pwait->sem,0,0);
	
	cJSON *rec_event;
	rec_event=cJSON_CreateObject();
	cJSON_AddStringToObject(rec_event, EVENT_KEY_EventName,EventRecFileReq);
	cJSON_AddStringToObject(rec_event, "From","ReqOneRecFile");
	cJSON_AddNumberToObject(rec_event,"Wait",(int)pwait);
	
	//cJSON_AddStringToObject(rec_event, "ID",call_id);
	API_Event_Json(rec_event);
	cJSON_Delete(rec_event);
	if( sem_wait_ex2(&pwait->sem, 2000) ==0)
	{

		if(pwait->ret>=0)
		{
			if(filepath)
			{
				strcpy(filepath,pwait->filepath);
			}
			ret=0;
		}
	}
	ReleaseOneRecManagerWait(pwait);
	return ret;
}
void RecManagerCallBack(void *arg)
{
	char *pch;
	char cmd[200];
	char file_name[200];
	ffmpeg_rec_dat2 *pin=(ffmpeg_rec_dat2 *)arg;
	
	if(pin->CurSec>=15)
	{
		strcpy(file_name,pin->out_file);
		if((pch=strstr(file_name,".tmp"))!=NULL)
		{
			pch[0]=0;
			snprintf(cmd,200,"mv %s %s",pin->out_file,file_name);
			system(cmd);
		}
	}
}
