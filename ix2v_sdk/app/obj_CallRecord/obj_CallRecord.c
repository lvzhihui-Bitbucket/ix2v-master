#include <stdio.h>

#include "task_DtCaller.h"
#include "obj_SYS_VER_INFO.h"
#include "elog_forcall.h"
#include "obj_PublicInformation.h"
#include "task_Event.h"
#include "obj_GetInfoByIp.h"
#include "obj_IoInterface.h"


pthread_mutex_t DsCallRecord_lock=PTHREAD_MUTEX_INITIALIZER;
cJSON *One_DsCallRecord=NULL;
#define CallRecord_Key_Time			"Time"
#define CallRecord_Key_CallId			"Call_ID"
#define CallRecord_Key_CallType		"Call_Type"
#define CallRecord_Key_CallTarget		"Call_Target"
#define CallRecord_Key_CallSource		"Call_Source"
#define CallRecord_Key_RecFile		"Rec_File"
#define CallRecord_Key_Delete			"Delete"
#define CallRecord_Key_Unread		"Unread"
#define CallRecord_Key_Miss			"Miss"
#define CallRecord_Key_Remark		"Remark"

//cJSON *CallRecordTb=NULL;
//#define CallRecordTbPath	"/mnt/nand1-2/UserData/CallRecord.json"

cJSON *CallRecord_tb_create_item(int time,char *call_id,char *type,cJSON *src_info,cJSON *tar_info, char *rec_file,cJSON *unread,cJSON *miss,cJSON *del,char *remark);


int Event_DsCallRecord(cJSON *cmd)
{
	cJSON *rec_event;
	static time_t rec_start_time;
	pthread_mutex_lock(&DsCallRecord_lock);
    char* eventName = cJSON_GetStringValue(cJSON_GetObjectItemCaseSensitive(cmd, EVENT_KEY_EventName));
    char* callserverState = cJSON_GetStringValue(API_PublicInfo_Read(PB_CALL_SER_STATE));
    int Call_Part = API_PublicInfo_Read_Bool(PB_CALL_PART);
    //char* Call_Tar_DtAddr = cJSON_GetStringValue(API_PublicInfo_Read(PB_CALL_TAR_DTADDR));
	char *call_id=cJSON_GetStringValue(API_PublicInfo_Read(PB_CALL_ID));
	 cJSON* src_info = API_PublicInfo_Read(PB_CALL_SOURCE);
	// if(src_info!=NULL)
	//	printf_json(src_info,PB_CALL_SOURCE);
	cJSON* tar_info = API_PublicInfo_Read(PB_CALL_TARGET);
     
    if(callserverState == NULL)
    {
    	pthread_mutex_unlock(&DsCallRecord_lock);
        return 0;
    }
#if 0
	if(CallRecordTb==NULL)
		CallRecord_tb_load();
#endif
//printf("1111111111%s:%d\n",__func__,__LINE__);
    //dprintf("callserverState = %s, Call_Part = %d, Call_Tar_DtAddr = %s\n", callserverState, Call_Part, Call_Tar_DtAddr);
 if(eventName && !strcmp(eventName, EventCall))
    {
        char* callEvent = cJSON_GetStringValue(cJSON_GetObjectItemCaseSensitive(cmd, "CALL_EVENT"));
        if(callEvent)
        {
            if(!strcmp(callEvent, "Calling_Cancel"))
            {
				if(One_DsCallRecord!=NULL)
				{
					if(cJSON_IsTrue(cJSON_GetObjectItemCaseSensitive(One_DsCallRecord, CallRecord_Key_Miss))&&(time(NULL)-rec_start_time)<10)
					{
						cJSON_ReplaceItemInObjectCaseSensitive(One_DsCallRecord,CallRecord_Key_Miss,cJSON_CreateFalse());
					}
				}
            }
        }
	pthread_mutex_unlock(&DsCallRecord_lock);
	return;
    }
   	if(callserverState!=NULL)
    {
        if(strcmp(callserverState, "Wait")==0)
        {
			//CallVoice("Event_CALL_CLOSE");
		if(One_DsCallRecord!=NULL)
		{
			//printf_json(One_DsCallRecord, "22222");
			#if 0
			API_TB_Add(CallRecordTb,One_DsCallRecord);
			CallRecord_tb_save();
			#endif
			#if 0
			rec_event=cJSON_CreateObject();
			cJSON_AddStringToObject(rec_event, EVENT_KEY_EventName,EventRecFileReq);
			cJSON_AddStringToObject(rec_event, "Ctrl","Stop"); 
			cJSON_AddStringToObject(rec_event, "From","CallRecord");  
			cJSON_AddStringToObject(rec_event, "ID",call_id);
			API_Event_Json(rec_event);
			cJSON_Delete(rec_event);
			#else
			time_t cur=time(NULL);
			
			RecManager_StopRecReq(NULL);
			usleep(100*1000);
			#endif
			if((cur-rec_start_time)<5&&cJSON_IsTrue(cJSON_GetObjectItemCaseSensitive(One_DsCallRecord, CallRecord_Key_Miss)))
			{
				//printf("1111111111%s:%d\n",__func__,__LINE__);
				cJSON_ReplaceItemInObjectCaseSensitive(One_DsCallRecord,CallRecord_Key_Miss,cJSON_CreateFalse());
			}
				
			API_TB_AddByName("DSCallRecord",One_DsCallRecord);
			if((cur-rec_start_time)<2||strcmp("-",cJSON_GetStringValue(cJSON_GetObjectItemCaseSensitive(One_DsCallRecord,"Rec_File")))!=0)
			{
				SendOneCallRecordToIM(One_DsCallRecord);
				SendMissCallToIM(One_DsCallRecord);
			}
			cJSON_Delete(One_DsCallRecord);
			One_DsCallRecord=NULL;
		}
        }
        else if(strcmp(callserverState, "Ring")==0||(strcmp("VMSipCall",API_PublicInfo_Read_String(PB_CALL_SER_TYPE))==0&&strcmp(callserverState, "Invite")==0))
        {
			//CallVoice("Event_CALL_SUCC");
			if((API_PublicInfo_Read(PB_CALL_IPCALLER)!=NULL&&strcmp("IxMainCall",API_PublicInfo_Read_String(PB_CALL_IPCALLER_TYPE))==0)||API_PublicInfo_Read(PB_CALL_TAR_IxDev)!=NULL)
			{
				
				// printf_json(API_PublicInfo_Read(PB_CALL_SOURCE),"8888");
				  //printf_json(API_PublicInfo_Read(PB_CALL_INFO),"7777");
				 if(One_DsCallRecord!=NULL)
				 {
					if(strcmp(call_id,cJSON_GetStringValue(cJSON_GetObjectItemCaseSensitive(One_DsCallRecord,PB_CALL_ID)))!=0)
					{
						cJSON_Delete(One_DsCallRecord);
						One_DsCallRecord=NULL;
					}
				 }
				  if(One_DsCallRecord==NULL)
				  {
				  	 //printf_json(API_PublicInfo_Read(PB_CALL_SOURCE),"9999");
					// printf_json(API_PublicInfo_Read("Call_ID"),PB_CALL_ID);
					// if(call_id!=NULL)
					 //	printf_json(API_PublicInfo_Read(PB_CALL_ID),call_id);
					  //if(src_info!=NULL)
					 //	printf_json(src_info,PB_CALL_SOURCE);
					  // if(tar_info!=NULL)
					 //	printf_json(tar_info,PB_CALL_TARGET);
					One_DsCallRecord=CallRecord_tb_create_item(rec_start_time=time(NULL),call_id,API_PublicInfo_Read_String(PB_CALL_IPCALLER_TYPE),cJSON_Duplicate(src_info,1),cJSON_Duplicate(tar_info,1),"-",cJSON_CreateTrue(),cJSON_CreateTrue(),cJSON_CreateFalse(),"-");
					//printf_json(One_DsCallRecord, "11221");
					#if 0
					rec_event=cJSON_CreateObject();
					cJSON_AddStringToObject(rec_event, EVENT_KEY_EventName,EventRecFileReq);
					cJSON_AddStringToObject(rec_event, "From","CallRecord");  
					cJSON_AddStringToObject(rec_event, "ID",call_id);
					API_Event_Json(rec_event);
					cJSON_Delete(rec_event);
					#endif
					char rec_file[200];
					if(API_ReqOneRecFile(rec_file)==0)
					{
						 cJSON_ReplaceItemInObject(One_DsCallRecord,CallRecord_Key_RecFile,cJSON_CreateString(rec_file));
						 SendRecEventToIM(One_DsCallRecord);
					}
				  }
			}
        }
        else if(strcmp(callserverState, "Ack")==0|| strcmp(callserverState, "RemoteAck")==0)
        {
        	#if 1
        	if(API_PublicInfo_Read(PB_CALL_IPCALLER)!=NULL&&strcmp("IxMainCall",API_PublicInfo_Read_String(PB_CALL_IPCALLER_TYPE))==0)
        	{
    			char *call_id=cJSON_GetStringValue(API_PublicInfo_Read(PB_CALL_ID));
			 cJSON* src_info = API_PublicInfo_Read(PB_CALL_SOURCE);
			 cJSON* tar_info = API_PublicInfo_Read(PB_CALL_TARGET);
			 if(One_DsCallRecord!=NULL)
			 {
				if(strcmp(call_id,cJSON_GetStringValue(cJSON_GetObjectItemCaseSensitive(One_DsCallRecord,CallRecord_Key_CallId)))!=0)
				{
					cJSON_Delete(One_DsCallRecord);
					One_DsCallRecord=NULL;
					One_DsCallRecord=CallRecord_tb_create_item(rec_start_time=time(NULL),call_id,API_PublicInfo_Read_String(PB_CALL_IPCALLER_TYPE),cJSON_Duplicate(src_info,1),cJSON_Duplicate(tar_info,1),"-",cJSON_CreateTrue(),cJSON_CreateFalse(),cJSON_CreateFalse(),"-");
				}
				else
				{
					if(cJSON_GetObjectItemCaseSensitive(One_DsCallRecord, CallRecord_Key_CallTarget))
					{
						cJSON_ReplaceItemInObjectCaseSensitive(One_DsCallRecord,CallRecord_Key_CallTarget,cJSON_Duplicate(tar_info,1));
					}
					if(cJSON_GetObjectItemCaseSensitive(One_DsCallRecord, CallRecord_Key_Miss))
					{
						cJSON_ReplaceItemInObjectCaseSensitive(One_DsCallRecord,CallRecord_Key_Miss,cJSON_CreateFalse());
					}
				}
			 }
			 else
				One_DsCallRecord=CallRecord_tb_create_item(time(NULL),call_id,API_PublicInfo_Read_String(PB_CALL_IPCALLER_TYPE),cJSON_Duplicate(src_info,1),cJSON_Duplicate(tar_info,1),"-",cJSON_CreateTrue(),cJSON_CreateFalse(),cJSON_CreateFalse(),"-");	
			  
        	}
		#endif
        }
    }
pthread_mutex_unlock(&DsCallRecord_lock);
	return 1;
}
int EventRecFileRsp_CallRecord(cJSON *cmd)
{
	#if 0
	char from[100];
	char id[30];
	char rec_file[200];
	char *str=cJSON_Print(cmd);
	printf("%s:\n%s\n",__func__,str);
	free(str);
	pthread_mutex_lock(&DsCallRecord_lock);
	//printf("%s1111111111\n",__func__);
	if(One_DsCallRecord==NULL)
	{
	 	pthread_mutex_unlock(&DsCallRecord_lock);
	 	return 0;
	 }
	//printf("%s22222222222\n",__func__);
	 if(GetJsonDataPro(cmd,"From",from)==0)
	 {
	 	pthread_mutex_unlock(&DsCallRecord_lock);
	 	return 0;
	 }
	 //printf("%s3333333\n",__func__);
	 if(strcmp(from,"CallRecord")!=0)
	{
	 	pthread_mutex_unlock(&DsCallRecord_lock);
	 	return 0;
	 }
	 //printf("%s444444444\n",__func__);
	 if(GetJsonDataPro(cmd,"ID",id)==0)
	 {
	 	pthread_mutex_unlock(&DsCallRecord_lock);
	 	return 0;
	 }
	 //printf("%s5555555555\n",__func__);
	  if(strcmp(id,cJSON_GetStringValue(cJSON_GetObjectItemCaseSensitive(One_DsCallRecord,CallRecord_Key_CallId)))!=0)
	{
	 	pthread_mutex_unlock(&DsCallRecord_lock);
	 	return 0;
	 }
	  //printf("%s6666666\n",__func__);
	   if(GetJsonDataPro(cmd,"Rec_File",rec_file)==0)
	   {
	 	pthread_mutex_unlock(&DsCallRecord_lock);
	 	return 0;
	 }	
	   printf("%s7777777777:%s\n",__func__,rec_file);
	  cJSON_ReplaceItemInObject(One_DsCallRecord,CallRecord_Key_RecFile,cJSON_CreateString(rec_file));
	  SendRecEventToIM(One_DsCallRecord);
	  pthread_mutex_unlock(&DsCallRecord_lock);
	  #endif
	  return 1;
}
cJSON *CallRecord_tb_create_item(int time,char *call_id,char *type,cJSON *src_info,cJSON *tar_info, char *rec_file,cJSON *unread,cJSON *miss,cJSON *del,char *remark)
{
	cJSON *item=NULL;
	item=cJSON_CreateObject();
	char buff[50];
	
	if(item!=NULL)
	{
		if(time==0)
			cJSON_AddStringToObject(item,CallRecord_Key_Time,"");
		else if(time>0) 
		{
			struct tm *tblock; 
			tblock=localtime(&time);
			sprintf( buff,"%02d-%02d-%02d %02d:%02d",tblock->tm_year-100,tblock->tm_mon+1,tblock->tm_mday,tblock->tm_hour,tblock->tm_min);
			cJSON_AddStringToObject(item,CallRecord_Key_Time,buff);
		}
		if(call_id!=NULL)
			cJSON_AddStringToObject(item,CallRecord_Key_CallId,call_id);
		if(type!=NULL)
			cJSON_AddStringToObject(item,CallRecord_Key_CallType,type);
		if(src_info!=NULL)
			cJSON_AddItemToObject(item,CallRecord_Key_CallSource,src_info);
		if(tar_info!=NULL)
			cJSON_AddItemToObject(item,CallRecord_Key_CallTarget,tar_info);
		if(rec_file!=NULL)
			cJSON_AddStringToObject(item,CallRecord_Key_RecFile,rec_file);
		if(unread!=NULL)
			cJSON_AddItemToObject(item,CallRecord_Key_Unread,unread);
		if(miss!=NULL)
			cJSON_AddItemToObject(item,CallRecord_Key_Miss,miss);
		if(del!=NULL)
			cJSON_AddItemToObject(item,CallRecord_Key_Delete,del);
		if(remark!=NULL)
			cJSON_AddStringToObject(item,CallRecord_Key_Remark,remark);
	}
	
	return item;
}

void printf_json(cJSON *cmd,char *func)
{
#if 1
	char *str=cJSON_Print(cmd);
	if(str!=NULL)
	{
		printf("%s:\n%s\n",func,str);
		free(str);
	}
	else
	{
		printf("%s:it isn't json\n",func);
	}
#endif
}

int JudgeAutoRecByNbr(char *addr)
{
	return 1;
}
int JudgeIXGImByNbr(char *addr)
{
	char room[5]={0};
	memcpy(room,addr+4,4);
	if(atoi(room)>100)
		return 1;
	return 0;
}
void SendOneCallRecordToIM(cJSON *record)
{
	#if 1
	//printf_json(One_DsCallRecord, "33333");
	int i;
	cJSON *devs=MyCjson_GetObjectItem(record, PB_CALL_TAR_IxDev);
	cJSON *one_dev;
	char ip_addr[20];
	int ip;
	for(i=0;i<cJSON_GetArraySize(devs);i++)
	{
		one_dev=cJSON_GetArrayItem(devs,i);
		if(strstr(GetEventItemString(one_dev, IX2V_IX_TYPE),"VM"))
			continue;
			
		GetJsonDataPro(one_dev, PB_CALL_TAR_IxDevNum, ip_addr);
		if(JudgeAutoRecByNbr(ip_addr))
		{
			GetJsonDataPro(one_dev, PB_CALL_TAR_IxDevIP, ip_addr);
			ip=inet_addr(ip_addr);
			API_RemoteTableAdd(ip,"IMCallRecord",record);
		}
	}
	#endif
		
}

void SendMissCallToIM(cJSON *record)
{
	#if 1
	if(!cJSON_IsTrue(cJSON_GetObjectItemCaseSensitive(record,"Miss")))
	{
		return;
	}
	cJSON *miss_event=cJSON_CreateObject();
	cJSON_AddStringToObject(miss_event, EVENT_KEY_EventName,EventMissCall);
	cJSON_AddStringToObject(miss_event, "STATE","ON");
	cJSON *src_addr=MyCjson_GetObjectItem(MyCjson_GetObjectItem(record, PB_CALL_SOURCE),IX2V_IX_ADDR);
	if(src_addr)
		cJSON_AddStringToObject(miss_event,PB_CALL_SOURCE,src_addr->valuestring);
	cJSON *tar_addr=MyCjson_GetObjectItem(record, PB_CALL_TAR_IXADDR);
	if(tar_addr)
		cJSON_AddStringToObject(miss_event,PB_CALL_TARGET,tar_addr->valuestring);
	cJSON *devs=MyCjson_GetObjectItem(record, PB_CALL_TAR_IxDev);
	cJSON *one_dev;
	char ip_addr[20];
	int ip;
	int i;
	for(i=0;i<cJSON_GetArraySize(devs);i++)
	{
		one_dev=cJSON_GetArrayItem(devs,i);
		if(strstr(GetEventItemString(one_dev, IX2V_IX_TYPE),"VM"))
			continue;
		GetJsonDataPro(one_dev, PB_CALL_TAR_IxDevNum, ip_addr);
		if(JudgeAutoRecByNbr(ip_addr))
		{
			GetJsonDataPro(one_dev, PB_CALL_TAR_IxDevIP, ip_addr);
			ip=inet_addr(ip_addr);
			API_XD_EventJson(ip,miss_event);
		}
	}
	cJSON_Delete(miss_event);
	#endif
}

void SendRecEventToIM(cJSON *record)
{
	cJSON *rev_event=cJSON_CreateObject();
	cJSON_AddStringToObject(rev_event, EVENT_KEY_EventName,EventHaveRec);
	cJSON *src_addr=MyCjson_GetObjectItem(MyCjson_GetObjectItem(record, PB_CALL_SOURCE),IX2V_IX_ADDR);
	if(src_addr)
		cJSON_AddStringToObject(rev_event,PB_CALL_SOURCE,src_addr->valuestring);
	cJSON *tar_addr=MyCjson_GetObjectItem(record, PB_CALL_TAR_IXADDR);
	if(tar_addr)
		cJSON_AddStringToObject(rev_event,PB_CALL_TARGET,tar_addr->valuestring);
	cJSON *devs=MyCjson_GetObjectItem(record, PB_CALL_TAR_IxDev);
	cJSON *one_dev;
	char ip_addr[20];
	int ip;
	int i;
	for(i=0;i<cJSON_GetArraySize(devs);i++)
	{
		one_dev=cJSON_GetArrayItem(devs,i);
		GetJsonDataPro(one_dev, PB_CALL_TAR_IxDevNum, ip_addr);
		if(JudgeAutoRecByNbr(ip_addr)&&JudgeIXGImByNbr(ip_addr))
		{
			GetJsonDataPro(one_dev, PB_CALL_TAR_IxDevIP, ip_addr);
			ip=inet_addr(ip_addr);
			API_XD_EventJson(ip,rev_event);
		}
	}
	cJSON_Delete(rev_event);
}
#include "define_Command.h"

int IXGMissCallEvent_Process(cJSON *cmd)
{
	//printf_json(cmd,"11111have miss call :");
	return 1;
	
}
void sendCodeUnlockEvent(void)
{
	cJSON *target_array=cJSON_CreateArray();
	char buff[11];
	sprintf(buff,"%s010101",GetSysVerInfo_bd());
	if(IXS_GetByNBR(NULL, NULL, buff, NULL, target_array))
	{
		cJSON *tar=cJSON_CreateObject();
		cJSON_AddStringToObject(tar,PB_CALL_TAR_IXADDR,buff);
		cJSON_AddItemToObject(tar,PB_CALL_TAR_IxDev,cJSON_Duplicate(target_array,1));
		char rec_file[200];
		cJSON *source_info=cJSON_CreateObject();
		cJSON_AddStringToObject(source_info,"NAME",GetSysVerInfo_name());
		cJSON_AddStringToObject(source_info,PB_CALL_TAR_IxDevNum,GetSysVerInfo_BdRmMs());
		cJSON_AddStringToObject(source_info,PB_CALL_TAR_IxDevIP,GetSysVerInfo_IP());
		
		if(API_ReqOneRecFile(rec_file)==0)
		{
			cJSON *event=CallRecord_tb_create_item(time(NULL),"","Unlock",source_info,tar,rec_file,cJSON_CreateTrue(),cJSON_CreateTrue(),cJSON_CreateFalse(),"-");
			SendOneCallRecordToIM(event);
			cJSON_Delete(event);
		}
	}
	cJSON_Delete(target_array);
	
}

#define EventRecord_Key_Time		"Time"
#define EventRecord_Key_Type		"Type"
#define EventRecord_Key_Recorder	"Recorder"
#define EventRecord_Key_RecFile		"Rec_File"
#define EventRecord_Key_Unread		"Unread"
#define EventRecord_Key_Detail	"Detail"

cJSON *EventRecord_tb_create_item(int time,char *type,cJSON *recorder,cJSON *detail,cJSON *unread,char *rec_file)
{
	cJSON *item=NULL;
	item=cJSON_CreateObject();
	char buff[50];
	
	if(item!=NULL)
	{
		if(time==0)
			cJSON_AddStringToObject(item,EventRecord_Key_Time,"");
		else if(time>0) 
		{
			struct tm *tblock; 
			tblock=localtime(&time);
			sprintf( buff,"%02d-%02d-%02d %02d:%02d",tblock->tm_year-100,tblock->tm_mon+1,tblock->tm_mday,tblock->tm_hour,tblock->tm_min);
			cJSON_AddStringToObject(item,EventRecord_Key_Time,buff);
		}
		if(type!=NULL)
			cJSON_AddStringToObject(item,EventRecord_Key_Type,type);
		if(recorder!=NULL)
			cJSON_AddItemToObject(item,EventRecord_Key_Recorder,recorder);
		if(rec_file!=NULL)
			cJSON_AddStringToObject(item,EventRecord_Key_RecFile,rec_file);
		if(unread!=NULL)
			cJSON_AddItemToObject(item,EventRecord_Key_Unread,unread);
		if(detail!=NULL)
			cJSON_AddItemToObject(item,EventRecord_Key_Detail,detail);
	}
	
	return item;
}

cJSON *CreateOneEventRecord(int time,char *type,cJSON *recorder,cJSON *detail,char *rec_file)
{
	if(recorder==NULL)
	{
		recorder=cJSON_CreateObject();
		cJSON_AddStringToObject(recorder,"NAME",GetSysVerInfo_name());
		cJSON_AddStringToObject(recorder,PB_CALL_TAR_IxDevNum,GetSysVerInfo_BdRmMs());
		cJSON_AddStringToObject(recorder,PB_CALL_TAR_IxDevIP,GetSysVerInfo_IP());
	}
	return EventRecord_tb_create_item(time,type,recorder?cJSON_Duplicate(recorder,1):NULL,detail?cJSON_Duplicate(detail,1):NULL,cJSON_CreateTrue(),rec_file);
}
void AddOneEventRecordToLocal(cJSON *event)
{
	API_TB_AddByName("EventRecord",event);
}
void AddOneEventRecordToRemote(int ip,cJSON *event)
{
	API_RemoteTableAdd(ip,"EventRecord",event);
}



