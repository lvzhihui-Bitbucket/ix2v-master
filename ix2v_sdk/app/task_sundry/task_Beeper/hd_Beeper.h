/**
  ******************************************************************************
  * @file    hd_beeper.h
  * @author  WGC
  * @version V1.0.0
  * @date    2012.10.25
  * @brief   This file contains the headers of hd_beeper.
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2012 V-Tec</center></h2>
  ******************************************************************************
  */ 
#ifndef HD_beeper_H
#define HD_beeper_H

#include "RTOS.h"

// Define Task Vars and Structures----------------------------------------------

//BEEP?????????
extern const uint8* const ptrBeepRomTab[];

	//BEEP??
#define BEEP_ON         		1
#define BEEP_OFF        		0

	//BEEP??????
#define BEEP_SOUND		0
#define BEEP_GAP			1

// Define task interface-------------------------------------------------------------

typedef struct 
{
	uint8 type;			// Beeper_Type
	uint8 state;			// BEEPER: ON / OFF
	uint8 stage;			// BEEPER: SOUND / GAP
	uint8 count;			// ????		
} BEEP_RUN;
extern BEEP_RUN beep_run;

// Define Task 2 items----------------------------------------------------------


// Define Task others-----------------------------------------------------------
void Beep_Init( void );	
void Sound_Enable( void );
void Sound_Disable( void );
void Beep_On( uint8 beeptype );	
void Beep_Off( void );
uint8 Get_BeepState( void );


// Define API-------------------------------------------------------------------



#endif

