/**
  ******************************************************************************
  * @file    task_Sundry.h
  * @author  zeng
  * @version V1.0.0
  * @date    2012.08.15
  * @brief   This file contains the headers of the power task_3.
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2012 V-Tec</center></h2>
  ******************************************************************************
  */
#ifndef TASK_Sundry_H
#define TASK_sundry_H

#include "task_survey.h"
#include "task_Beeper.h"
#include "task_RTC.h"
#include "task_Led.h"
#include "task_Talk.h"
#include "task_Power.h"
#include "task_Relay.h"

/*******************************************************************************
                         Define task event flag
*******************************************************************************/

/*******************************************************************************
                  Define task vars, structures and macro
*******************************************************************************/

#define MSG_TYPE_BEEPER  		0
#define MSG_TYPE_LED     		1
#define MSG_TYPE_POWER   		2
#define MSG_TYPE_RING    		3
#define MSG_TYPE_TALK    		4
#define MSG_TYPE_RELAY  		5
#define MSG_TYPE_RTC     		6
#define MSG_TYPE_CAMERA		7
#define MSG_TYPE_VOICEPROMPT	8

//cao_20151118
extern Loop_vdp_common_buffer	vdp_sundry_mesg_queue;
extern Loop_vdp_common_buffer	vdp_sundry_sync_queue;

void vtk_TaskInit_sundry(int priority);



#endif
