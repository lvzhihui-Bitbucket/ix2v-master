/**
  ******************************************************************************
  * @file    obj_Led.c
  * @author  zeng
  * @version V1.0.0
  * @date    2012.08.15
  * @brief   This file contains the functions of the obj_Led
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2012 V-Tec</center></h2>
  ******************************************************************************
*/

#include "hd_Led.h"
#include "obj_Led.h"
#include "task_survey.h"

#define LED_TIMER_CYC   50

const uint8	FastFlashing[3]				= {2,	5,5};
const uint8	NormalFlashing[3]			= {2,	15,15};
const uint8	ShortIrregularFlashing[5]	= {4,	5,20,5,5};
const uint8	LongIrregularFlashing[7]	= {6,	5,20,5,5,5,5};

#if 0
const uint8	FAST_FLASH[3]		= {2,	5,5};		//lyx 20170721	   
const uint8	DOUBLE_FLASH[3]	= {2,	20,20};	

LED_PROPERTY ledTalk;
LED_PROPERTY ledUnlock;
LED_PROPERTY ledPower;   //lyx
LED_PROPERTY ledMenu;
LED_PROPERTY ledUp;
LED_PROPERTY ledDown;
LED_PROPERTY ledBack;
LED_PROPERTY ledDisturb; 
#endif

typedef struct
{
	int cur_scene;
	int boot_state;	// 0 = boot finish,1 = kernel boot,2 = app boot,3 = boot error	// -1 = undefine
	int call_state;	//0= idle,1=ring,2=tlak,3 = divert
	int monitor_state;// 0 = idle,1 = local mon,2 = remote mon
	int fwupdate;
	int not_disturb;
	int miss_event;

	unsigned char 	ledCtrlType;
	unsigned char 	ledState;
	unsigned char 	ledFlashCnt;
	unsigned char 	ledFlashTabIndex;
	unsigned char* 	ledFlashTab;
	unsigned short 	ledTiming;
	LedCrtl			ledCtrl;
}Led_Display_Run_Stru;

Led_Display_Run_Stru	Led_Display_Run = {.cur_scene = 0};


OS_TIMER timerLedFlash;

static int led_second_ctr = 0; // zfz_20190617


void Set_Led_DisplayScene(uint8 msg);
Led_DisplayScene_e Get_Led_DisplayScene(void);

/*********************************************************************
 * @fn      LedInit()
 *
 * @brief   Led init power on
 *
 * @param   none
 *
 * @return  none
 *********************************************************************/
void LedInit(void)	//R
{
	hd_LedInit();
	
	//指示灯闪烁或亮多久
	OS_CreateTimer(&timerLedFlash, TimerLedFlashCallBack, LED_TIMER_CYC/25);
	OS_RetriggerTimer(&timerLedFlash);
	memset(&Led_Display_Run,0,sizeof(Led_Display_Run_Stru));
	
	//Led_Display_Run.cur_scene = -1;	
	Led_Display_Run.boot_state = -1;		// lzh_20171021
		
	//装载回调函数指针
	#if 0
	ledTalk.ledCtrl     = hdLedTalkCtrl;
	ledUnlock.ledCtrl = hdLedUnlockCtrl;
	ledPower.ledCtrl  = hdLedPowerCtrl;      //lyx
	ledMenu.ledCtrl   = hdLedMenuCtrl; 
	ledUp.ledCtrl   	     = hdLedUpCtrl; 
	ledDown.ledCtrl   = hdLedDownCtrl;
	ledBack.ledCtrl     = hdLedBackCtrl;
	ledDisturb.ledCtrl = hdLedNoDisturbCtrl;

	ledTalk.ledFlashCtrl     = hdLedTalkFlashCtrl;    //lyx 20170727
	ledUnlock.ledFlashCtrl = hdLedUnlockFlashCtrl;
	ledPower.ledFlashCtrl  = hdLedPowerFlashCtrl;     
	ledMenu.ledFlashCtrl   = hdLedMenuFlashCtrl; 
	ledUp.ledFlashCtrl        = hdLedUpFlashCtrl; 
	ledDown.ledFlashCtrl   = hdLedDownFlashCtrl;
	ledBack.ledFlashCtrl     = hdLedBackFlashCtrl;
	ledDisturb.ledFlashCtrl = hdLedNoDisturbFlashCtrl;
	#endif
}

/*********************************************************************
 * @fn      TimerLedFlashCallBack()
 *
 * @brief   control led falsh
 *
 * @param   none
 *
 * @return  none
 *********************************************************************/
// zfz_20190617
void TimerLedFlashCallBack(void)	//R //50ms软定时的回调函数
{
	Led_DisplayScene_e disp_scene = Get_Led_DisplayScene();
	if(Led_Display_Run.cur_scene != disp_scene)
	{
		Led_Display_Run.cur_scene = disp_scene;
		
		switch(Led_Display_Run.cur_scene)
		{
			// lzh_20171021_s
			case Led_DisplayScene_Undefine:
				hdLedRedCtrl(LED_OFF);
				//usleep(10000);
				hdLedBlueCtrl(LED_OFF);
				Led_Display_Run.ledCtrl = hdLedGreenCtrl;
				Led_Display_Run.ledCtrlType = LED_STATUS_ShortIrregularFlashing;
				Led_Display_Run.ledFlashTab = ShortIrregularFlashing;
				Led_Display_Run.ledFlashCnt = 0;
				Led_Display_Run.ledFlashTabIndex = 1;
				Led_Display_Run.ledCtrl(LED_OFF);
				break;
			// lzh_20171021_e
			case Led_DisplayScene_Normal:
				hdLedRedCtrl(LED_OFF);
				//usleep(10000);
				hdLedBlueCtrl(LED_OFF);
				Led_Display_Run.ledCtrl = hdLedGreenCtrl;
				Led_Display_Run.ledCtrlType = LED_STATUS_LightON;
				Led_Display_Run.ledFlashTab = NormalFlashing;
				Led_Display_Run.ledFlashCnt = 0;
				Led_Display_Run.ledFlashTabIndex = 1;
				Led_Display_Run.ledCtrl(LED_ON);
				break;

			case Led_DisplayScene_KernelBoot:
				hdLedRedCtrl(LED_OFF);
				//usleep(10000);
				hdLedBlueCtrl(LED_OFF);
				Led_Display_Run.ledCtrl = hdLedGreenCtrl;
				Led_Display_Run.ledCtrlType = LED_STATUS_NormalFlashing;
				Led_Display_Run.ledFlashTab = NormalFlashing;
				Led_Display_Run.ledFlashCnt = 0;
				Led_Display_Run.ledFlashTabIndex = 1;
				Led_Display_Run.ledCtrl(LED_OFF);
				break;

			case Led_DisplayScene_AppBoot:
				hdLedRedCtrl(LED_OFF);
				//usleep(10000);
				hdLedBlueCtrl(LED_OFF);
				Led_Display_Run.ledCtrl = hdLedGreenCtrl;
				Led_Display_Run.ledCtrlType = LED_STATUS_ShortIrregularFlashing;
				Led_Display_Run.ledFlashTab = ShortIrregularFlashing;
				Led_Display_Run.ledFlashCnt = 0;
				Led_Display_Run.ledFlashTabIndex = 1;
				Led_Display_Run.ledCtrl(LED_OFF);
				break;

			case Led_DisplayScene_Ring:
				hdLedRedCtrl(LED_OFF);
				//hdLedBlueCtrl(LED_OFF);
				//usleep(10000);
				hdLedGreenCtrl(LED_OFF);
				Led_Display_Run.ledCtrl = hdLedBlueCtrl;
				Led_Display_Run.ledCtrlType = LED_STATUS_NormalFlashing;
				Led_Display_Run.ledFlashTab = NormalFlashing;
				Led_Display_Run.ledFlashCnt = 0;
				Led_Display_Run.ledFlashTabIndex = 1;
				Led_Display_Run.ledCtrl(LED_OFF);
				break;

			case Led_DisplayScene_MonitorOrTalk:
				hdLedRedCtrl(LED_OFF);
				//hdLedBlueCtrl(LED_OFF);
				//usleep(10000);
				hdLedGreenCtrl(LED_OFF);
				Led_Display_Run.ledCtrl = hdLedBlueCtrl;
				Led_Display_Run.ledCtrlType = LED_STATUS_LightON;
				Led_Display_Run.ledFlashTab = NormalFlashing;
				Led_Display_Run.ledFlashCnt = 0;
				Led_Display_Run.ledFlashTabIndex = 1;
				Led_Display_Run.ledCtrl(LED_ON);
				break;

			case Led_DisplayScene_Diverting:
				hdLedRedCtrl(LED_OFF);
				//hdLedBlueCtrl(LED_OFF);
				//usleep(10000);
				hdLedGreenCtrl(LED_OFF);
				Led_Display_Run.ledCtrl = hdLedBlueCtrl;
				Led_Display_Run.ledCtrlType = LED_STATUS_FastFlashing;
				Led_Display_Run.ledFlashTab = FastFlashing;
				Led_Display_Run.ledFlashCnt = 0;
				Led_Display_Run.ledFlashTabIndex = 1;
				Led_Display_Run.ledCtrl(LED_OFF);
				break;

			case Led_DisplayScene_RemoteMonitor:
				hdLedRedCtrl(LED_OFF);
				//hdLedBlueCtrl(LED_OFF);
				//usleep(10000);
				hdLedGreenCtrl(LED_OFF);
				Led_Display_Run.ledCtrl = hdLedBlueCtrl;
				Led_Display_Run.ledCtrlType = LED_STATUS_FastFlashing;
				Led_Display_Run.ledFlashTab = FastFlashing;
				Led_Display_Run.ledFlashCnt = 0;
				Led_Display_Run.ledFlashTabIndex = 1;
				Led_Display_Run.ledCtrl(LED_OFF);
				break;

			case Led_DisplayScene_MissEvent:
				//hdLedRedCtrl(LED_OFF);
				hdLedBlueCtrl(LED_OFF);
				//usleep(10000);
				hdLedGreenCtrl(LED_OFF);
				Led_Display_Run.ledCtrl = hdLedRedCtrl;
				Led_Display_Run.ledCtrlType = LED_STATUS_NormalFlashing;
				Led_Display_Run.ledFlashTab = NormalFlashing;
				Led_Display_Run.ledFlashCnt = 0;
				Led_Display_Run.ledFlashTabIndex = 1;
				Led_Display_Run.ledCtrl(LED_ON);
				break;	

			case Led_DisplayScene_NotDisturb:
				//hdLedRedCtrl(LED_OFF);
				hdLedBlueCtrl(LED_OFF);
				//usleep(10000);
				hdLedGreenCtrl(LED_OFF);
				Led_Display_Run.ledCtrl = hdLedRedCtrl;
				Led_Display_Run.ledCtrlType = LED_STATUS_LightON;
				Led_Display_Run.ledFlashTab = NormalFlashing;
				Led_Display_Run.ledFlashCnt = 0;
				Led_Display_Run.ledFlashTabIndex = 1;
				Led_Display_Run.ledCtrl(LED_ON);
				break;	

			case Led_DisplayScene_FwUpdate:
				//hdLedRedCtrl(LED_OFF);
				hdLedBlueCtrl(LED_OFF);
				//usleep(10000);
				hdLedGreenCtrl(LED_OFF);
				Led_Display_Run.ledCtrl = hdLedRedCtrl;
				Led_Display_Run.ledCtrlType = LED_STATUS_ShortIrregularFlashing;
				Led_Display_Run.ledFlashTab = ShortIrregularFlashing;
				Led_Display_Run.ledFlashCnt = 0;
				Led_Display_Run.ledFlashTabIndex = 1;
				Led_Display_Run.ledCtrl(LED_OFF);
				break;		
		}
	}
	else if( !led_second_ctr )
	{
		led_second_ctr = 1;
		#if 0
		switch(Led_Display_Run.cur_scene)
		{
			case Led_DisplayScene_Undefine:
				hdLedRedCtrl(LED_OFF);
				hdLedBlueCtrl(LED_OFF);
				break;
			case Led_DisplayScene_Normal:
				hdLedRedCtrl(LED_OFF);
				hdLedBlueCtrl(LED_OFF);
				break;

			case Led_DisplayScene_KernelBoot:
				hdLedRedCtrl(LED_OFF);
				hdLedBlueCtrl(LED_OFF);
				break;

			case Led_DisplayScene_AppBoot:
				hdLedRedCtrl(LED_OFF);
				hdLedBlueCtrl(LED_OFF);
				break;

			case Led_DisplayScene_Ring:
				hdLedRedCtrl(LED_OFF);
				hdLedGreenCtrl(LED_OFF);
				break;

			case Led_DisplayScene_MonitorOrTalk:
				hdLedRedCtrl(LED_OFF);
				hdLedGreenCtrl(LED_OFF);
				break;

			case Led_DisplayScene_Diverting:
				hdLedRedCtrl(LED_OFF);
				hdLedGreenCtrl(LED_OFF);
				break;

			case Led_DisplayScene_RemoteMonitor:
				hdLedRedCtrl(LED_OFF);
				hdLedGreenCtrl(LED_OFF);
				break;

			case Led_DisplayScene_MissEvent:
				hdLedBlueCtrl(LED_OFF);
				hdLedGreenCtrl(LED_OFF);
				break;	

			case Led_DisplayScene_NotDisturb:
				hdLedBlueCtrl(LED_OFF);
				hdLedGreenCtrl(LED_OFF);
				break;	

			case Led_DisplayScene_FwUpdate:
				hdLedBlueCtrl(LED_OFF);
				hdLedGreenCtrl(LED_OFF);
				break;	
		}
		#endif	
	}
	switch(Led_Display_Run.ledCtrlType)
	{
		case LED_STATUS_LightOFF:
			return;
		case LED_STATUS_LightON:
			break;
		case LED_STATUS_FastFlashing:
		case LED_STATUS_NormalFlashing:
		case LED_STATUS_ShortIrregularFlashing:
		case LED_STATUS_LongIrregularFlashing:
			if( ++Led_Display_Run.ledFlashCnt == Led_Display_Run.ledFlashTab[Led_Display_Run.ledFlashTabIndex])
			{
				if(++Led_Display_Run.ledFlashTabIndex > Led_Display_Run.ledFlashTab[0])
				{
					Led_Display_Run.ledFlashTabIndex = 1;
				}
				Led_Display_Run.ledFlashCnt = 0;
				if(Led_Display_Run.ledFlashTabIndex % 2)
				{
					Led_Display_Run.ledState = 0;
					Led_Display_Run.ledCtrl(LED_OFF);
				} 
				else
				{
					Led_Display_Run.ledState = 1;
					Led_Display_Run.ledCtrl(LED_ON);
				}
			}
			break;
	}
	
	OS_RetriggerTimer(&timerLedFlash);
}


/*********************************************************************
						LED定时处理
 *********************************************************************/
 #if 0
void LedTimerProcess(LED_PROPERTY* led)	//R
{

	if(led->ledCtrlType == LED_CTRL_TYPE_OFF)
	{
		return;
	}
	else if((led->ledCtrlType == LED_CTRL_TYPE_FAST_FLASH) ||(led->ledCtrlType == LED_CTRL_TYPE_DOUBLE_FLASH))
	{
		if( ++led->ledFlashCnt == led->ledFlashTab[led->ledFlashTabIndex] )
		{
			if(++led->ledFlashTabIndex > led->ledFlashTab[0])
			{
				led->ledFlashTabIndex = 1;
			}
			led->ledFlashCnt = 0;
			if(led->ledFlashTabIndex % 2)	
			{
				led->ledState = 0;
				led->ledCtrl(LED_OFF);
			}
			else
			{
				led->ledState = 1;
				led->ledCtrl(LED_ON);
			}
		}
	}
	
    // LED timing
    if( led->ledTiming )
    {
        led->ledTiming--;
        if( !(led->ledTiming) )
        {
			led->ledCtrlType = LED_CTRL_TYPE_OFF;
			led->ledState = 0;
			led->ledCtrl(LED_OFF);
        }
    }

}
#endif
/*********************************************************************
						LED控制处理
 *********************************************************************/
 #if 0
void LedProcess(LED_PROPERTY* led, uint8 ledCtrlType, uint16 iTime)	//R
{
	led->ledTiming = iTime*(100/LED_TIMER_CYC);	//装载动作定时(0为永久控制)
	led->ledCtrlType = ledCtrlType;				//动作类型

	switch(ledCtrlType)
	{
		//常灭 
		case LED_CTRL_TYPE_OFF:
			led->ledState = 0;
			led->ledCtrl(LED_OFF);
			break;           
		//常亮 或 亮多久灭
		case LED_CTRL_TYPE_ON: 
			led->ledState = 1;
			led->ledCtrl(LED_ON);
			break;
		// 单闪 或 单闪多久灭
		case LED_CTRL_TYPE_FAST_FLASH:
			led->ledFlashTab = FAST_FLASH;
			led->ledState = 0;			//初始化: 灭
			led->ledFlashCnt = 0;		//定时的计数
			led->ledFlashTabIndex = 1;	//表INDEX
			led->ledCtrl(LED_OFF);
			break;
		// 双闪 或 双闪多久灭
		case LED_CTRL_TYPE_DOUBLE_FLASH:
			led->ledFlashTab = DOUBLE_FLASH;
			led->ledState = 0;			//初始化: 灭
			led->ledFlashCnt = 0;		//定时的计数
			led->ledFlashTabIndex = 1;	//表INDEX
			led->ledCtrl(LED_OFF);
			break;

		case LED_CTRL_FAST_FLASH:
			led->ledFlashCtrl(LED_FLASH_FAST);  
			break;

		case LED_CTRL_SLOW_FLASH:
			led->ledFlashCtrl(LED_FLASH_SLOW);
			break;

		case LED_CTRL_IRREGULAR_FLASH:              //lyx 20170728
			led->ledFlashCtrl(LED_FLASH_IRREGULAR);
			break;
	}
}
#endif
/*********************************************************************
						获取LED状态
 *********************************************************************/
 #if 0
uint8 GetLedState(LED_PROPERTY* led)	//R
{
	return led->ledCtrlType;
}
 #endif
/*********************************************************************
 * @fn      LedMessageProcessing()
 *
 * @brief   process task msg to control led display
 *
 * @param   iMsg - on/off/flash slow/falsh fast
 * @param   iSubMsg - led id
 *
 * @return  none
 *********************************************************************/
void LedMessageProcessing( uint8 ledCtrlType, uint8 led_select, uint16 iTime )	//R
{
	dprintf("----------------------get a Led message scene = %d\n", ledCtrlType);
	#if 0
	//LED_TAKL控制
	if( led_select & LED_SELECT_TAKL )
	{
		LedProcess(&ledTalk, ledCtrlType, iTime);	//LED_TAKL的运行参数, 控制类型,动作定时(定时到,则OFF)
	}

	//LED_UNLOCK控制
	if( led_select & LED_SELECT_UNLOCK )
	{
		LedProcess(&ledUnlock, ledCtrlType, iTime);	//LED_UNLOCK的运行参数, 控制类型,动作定时(定时到,则OFF)
	}

	//LED_POWER控制         //lyx
	if( led_select & LED_SELECT_POWER )
	{
		LedProcess(&ledPower, ledCtrlType, iTime);	//LED_POWER的运行参数, 控制类型,动作定时(定时到,则OFF)
	}

	//LED_MENU控制         //lyx
	if( led_select & LED_SELECT_MENU )
	{
		LedProcess(&ledMenu, ledCtrlType, iTime);		//LED_MENU的运行参数, 控制类型,动作定时(定时到,则OFF)
	}

	//LED_UP控制		   //lyx
	if( led_select & LED_SELECT_UP )
	{
		LedProcess(&ledUp, ledCtrlType, iTime);	//LED_UP的运行参数, 控制类型,动作定时(定时到,则OFF)
	}

	//LED_DOWN控制		   //lyx
	if( led_select & LED_SELECT_DOWN )
	{
		LedProcess(&ledDown, ledCtrlType, iTime);    //LED_DOWN的运行参数, 控制类型,动作定时(定时到,则OFF)
	}

	//LED_BACK控制		   //lyx
	if( led_select & LED_SELECT_BACK )
	{
		LedProcess(&ledBack, ledCtrlType, iTime);	      //LED_BACK的运行参数, 控制类型,动作定时(定时到,则OFF)
	}

	//LED_DISTURB控制		   //lyx
	if( led_select & LED_SELECT_DISTURB )
	{
		LedProcess(&ledDisturb, ledCtrlType, iTime);	//LED_DISTURB的运行参数, 控制类型,动作定时(定时到,则OFF)
	}
	#endif
	Set_Led_DisplayScene(ledCtrlType);
	led_second_ctr = 0;  // zfz_20190617
}


void Set_Led_DisplayScene(uint8 msg)
{
	switch(msg)
	{
		case Led_DisplayMsg_KernelBoot:
			Led_Display_Run.boot_state = 1;
			break;
			
		case Led_DisplayMsg_AppBoot:
			Led_Display_Run.boot_state = 2;
			break;

		case Led_DisplayMsg_BootFinish:
			Led_Display_Run.boot_state = 0;
			break;

		case Led_DisplayMsg_BootError:
			Led_Display_Run.boot_state = 3;
			break;

		case Led_DisplayMsg_CallRing:
			if(Led_Display_Run.call_state != 3)
			Led_Display_Run.call_state = 1;
			break;

		case Led_DisplayMsg_CallTalk:
			if(Led_Display_Run.call_state != 3)
			Led_Display_Run.call_state = 2;
			break;

		case Led_DisplayMsg_CallDivert:
			Led_Display_Run.call_state = 3;
			break;	

		case Led_DisplayMsg_CallClose:
			Led_Display_Run.call_state = 0;
			break;

		case Led_DisplayMsg_LocalMonitor:
			Led_Display_Run.monitor_state= 1;
			break;

		case Led_DisplayMsg_RemoteMonitor:
			Led_Display_Run.monitor_state = 2;
			break;
			
		case Led_DisplayMsg_MonitorClose:
			Led_Display_Run.monitor_state = 0;
			break;

		case Led_DisplayMsg_MissEvent:
			Led_Display_Run.miss_event = 1;
			break;

		case Led_DisplayMsg_CheckEvent:
			Led_Display_Run.miss_event = 0;
			break;	

		case Led_DisplayMsg_NotDisturb:
			Led_Display_Run.not_disturb = 1;
			break;

		case Led_DisplayMsg_CloudBeDisturbed:
			Led_Display_Run.not_disturb = 0;
			break;
			
		case Led_DisplayMsg_FwUpdate:
			Led_Display_Run.fwupdate = 1;
			break;

		case Led_DisplayMsg_FwUpdateFinish:
			Led_Display_Run.fwupdate = 0;
			break;
			
		default:
			break;
	}
}

Led_DisplayScene_e Get_Led_DisplayScene(void)
{
	// lzh_20171021_s
	if( Led_Display_Run.boot_state == -1 )
		return Led_DisplayScene_Undefine;
	// lzh_20171021_e

	if(Led_Display_Run.boot_state == 1)
	{
		return Led_DisplayScene_KernelBoot;
	}
	
	if(Led_Display_Run.boot_state == 2)
	{
		return Led_DisplayScene_AppBoot;
	}

	if(Led_Display_Run.boot_state == 3)
	{
		return Led_DisplayScene_BootError;
	}
	
	if(Led_Display_Run.fwupdate == 1)
	{
		return Led_DisplayScene_FwUpdate;
	}

	if(Led_Display_Run.call_state == 3)
	{
		return Led_DisplayScene_Diverting;
	}

	if(Led_Display_Run.call_state == 2 || Led_Display_Run.monitor_state == 1)
	{
		return Led_DisplayScene_MonitorOrTalk;
	}

	if(Led_Display_Run.call_state == 1)
	{
		return Led_DisplayScene_Ring;
	}
	
	if(Led_Display_Run.monitor_state == 2)		//czn_20171002
	{
		return Led_DisplayScene_RemoteMonitor;
	}
	
	if(Led_Display_Run.miss_event == 1)
	{
		return Led_DisplayScene_MissEvent;
	}

	if(Led_Display_Run.not_disturb == 1)
	{
		return Led_DisplayScene_NotDisturb;
	}

	return Led_DisplayScene_Normal;
}
