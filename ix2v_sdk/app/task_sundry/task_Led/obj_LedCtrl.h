/**
  ******************************************************************************
  * @file    obj_LedCtrl.h
  * @author  cao
  * @version V1.0.0
  * @date    2022.02.14
  * @brief   This file contains the headers of the obj_LedCtrl.
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2012 V-Tec</center></h2>
  ******************************************************************************
  */
#ifndef _obj_LedCtrl_H
#define _obj_LedCtrl_H

// Define Object Property-------------------------------------------------------
typedef enum
{
    LED_RED = 0x01,
    LED_GREEN = 0x02,
    LED_BLUE = 0x04,
}LED_ID;

typedef enum
{
    CTRL_LED_ON,
    CTRL_LED_OFF,
    CTRL_LED_FLASH_SLOW,
    CTRL_LED_FLASH_FAST,
    CTRL_LED_FLASH_IRREGULAR,
}LedCtrlType;



// Define Object Function - Public----------------------------------------------
void LedMcuCtrl(LED_ID ledId, LedCtrlType ctrlType);

// Define Object Function - Private---------------------------------------------

#endif

