/**
  ******************************************************************************
  * @file    obj_KeypadLed.c
  * @author  cao
  * @version V1.0.0
  * @date    2022.02.14
  * @brief   This file contains the functions of the obj_KeypadLed
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2012 V-Tec</center></h2>
  ******************************************************************************
*/
#if defined( PID_IX611 )
#include "obj_KeypadLed.h"
#include "obj_gpio.h"

static KEYPAD_LED_PROPERTY keypadLed;
static int FastFlashTable[] = {2, 1000, 1000};
static int SlowFlashTable[] = {2, 1500, 1500};
static int IrregularFlashTable[] = {6, 100, 100, 100, 100, 300, 300};

static void KeypadLedFlashCtrl(KEYPAD_LED_PROPERTY* led)
{
    if(led->flashIndex%2)
    {
        (*(led->hdCtrl))(led, led->color, 0);
    }
    else
    {
        (*(led->hdCtrl))(led, led->color, 1);
    }

    OS_SetTimerPeriod(&led->timer, led->flashTab[led->flashIndex]/25);
    OS_RetriggerTimer(&led->timer);

    if(++(led->flashIndex) > led->flashTab[0])
    {
        led->flashIndex = 1;
    }
}

static void TimerKeypadLedCtrCallBack(void)
{
    KeypadLedFlashCtrl(&keypadLed);
}

static void KeypadLedHdCrtl(KEYPAD_LED_PROPERTY* led, int color, int state)
{
    if(state)
    {
        switch (color)
        {
            #if defined( PID_IX611 )
            case LED_RED:
                if(!led->redState)
                {
                    led->redState = 1;
            		RGB_R_TURN_SET();
                }
                if(led->greenState)
                {
                    led->greenState = 0;
            		RGB_G_TURN_RESET();
                }
                if(led->blueState)
                {
                    led->blueState = 0;
            		RGB_B_TURN_RESET();
                }
                break;
            case LED_GREEN:
                if(!led->greenState)
                {
                    led->greenState = 1;
            		RGB_G_TURN_SET();
                }
                if(led->redState)
                {
                    led->redState = 0;
            		RGB_R_TURN_RESET();
                }
                if(led->blueState)
                {
                    led->blueState = 0;
            		RGB_B_TURN_RESET();
                }
                break;
            case LED_BLUE:
                if(!led->blueState)
                {
                    led->blueState = 1;
            		RGB_B_TURN_SET();
                }
                if(led->redState)
                {
                    led->redState = 0;
            		RGB_R_TURN_RESET();
                }
                if(led->greenState)
                {
                    led->greenState = 0;
            		RGB_G_TURN_RESET();
                }
                break;
            case LED_PURPLE:
                if(!led->blueState)
                {
                    led->blueState = 1;
            		RGB_B_TURN_SET();
                }
                if(!led->redState)
                {
                    led->redState = 1;
            		RGB_R_TURN_SET();
                }
                if(led->greenState)
                {
                    led->greenState = 0;
            		RGB_G_TURN_RESET();
                }
                break;
            #endif

            #if defined( PID_IX611 )
            case LED_MK_BL:
                if(!led->mkBlState)
                {
                    led->mkBlState = 1;
            		MK_BL_TURN_ON();
                }
                break;
            #endif
        }
    }
    else
    {
        #if defined( PID_IX611 )
        if(led->greenState)
        {
            led->greenState = 0;
            RGB_G_TURN_RESET();
        }
        if(led->redState)
        {
            led->redState = 0;
            RGB_R_TURN_RESET();
        }
        if(led->blueState)
        {
            led->blueState = 0;
            RGB_B_TURN_RESET();
        }
        #endif

        #if defined( PID_IX611 )
        if(led->mkBlState)
        {
            led->mkBlState = 0;
            MK_BL_TURN_OFF();
        }
        #endif
    }
}

void KeypadLedCtrlInit(void)
{
#if defined( PID_IX611 )
    keypadLed.state = 0;
    keypadLed.redState = 0;
    keypadLed.greenState = 0;
    keypadLed.blueState = 0;
    keypadLed.mkBlState = 0;
#endif
    keypadLed.hdCtrl = KeypadLedHdCrtl;
    OS_CreateTimer(&keypadLed.timer, TimerKeypadLedCtrCallBack, 100/25);
    (*keypadLed.hdCtrl)(&keypadLed, 0, 0);
}



void API_KeypadLedCtrl(LED_COLOR ledColor, LedCtrlType ctrlType)
{
    OS_StopTimer(&keypadLed.timer);
    keypadLed.color = ledColor;
    keypadLed.flashIndex = 1;
    keypadLed.state = ctrlType;
    switch (ctrlType)
    {
        case KEYPAD_LED_ON:
            (*keypadLed.hdCtrl)(&keypadLed, ledColor, 1);
            break;
        case KEYPAD_LED_OFF:
            (*keypadLed.hdCtrl)(&keypadLed, ledColor, 0);
            break;
        case KEYPAD_LED_FLASH_FAST:
            keypadLed.flashTab = FastFlashTable;
            KeypadLedFlashCtrl(&keypadLed);
            break;
        case KEYPAD_LED_FLASH_SLOW:
            keypadLed.flashTab = SlowFlashTable;
            KeypadLedFlashCtrl(&keypadLed);
            break;
        case KEYPAD_LED_FLASH_IRREGULAR:
            keypadLed.flashTab = IrregularFlashTable;
            KeypadLedFlashCtrl(&keypadLed);
            break;
        
        default:
            break;
    }
}
#endif
#if	defined(PID_IX850)|| defined( PID_IX622)|| defined( PID_IX821)
void API_KeypadLedCtrl(int ledColor, int ctrlType)
{
}
#endif