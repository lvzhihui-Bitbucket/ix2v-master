/**
  ******************************************************************************
  * @file    obj_KeypadLed.h
  * @author  cao
  * @version V1.0.0
  * @date    2022.02.14
  * @brief   This file contains the headers of the obj_KeypadLed.
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2012 V-Tec</center></h2>
  ******************************************************************************
  */
#ifndef _obj_KeypadLed_H
#define _obj_KeypadLed_H

#include "obj_multi_timer.h"


// Define Object Property-------------------------------------------------------
typedef enum
{
    LED_RED,
    LED_GREEN,
    LED_BLUE,
    LED_PURPLE,
    LED_MK_BL,
}LED_COLOR;

typedef enum
{
    KEYPAD_LED_ON,
    KEYPAD_LED_OFF,
    KEYPAD_LED_FLASH_SLOW,
    KEYPAD_LED_FLASH_FAST,
    KEYPAD_LED_FLASH_IRREGULAR,
}LedCtrlType;

typedef void (*HdCrtl)(void*, int, int);
typedef struct
{
    OS_TIMER timer;
    int state;
    LED_COLOR color;
    #if defined( PID_IX611 )
    int redState;
    int greenState;
    int blueState;
    #endif

    #if defined( PID_IX611 )
    int mkBlState;
    #endif
    int* flashTab;
    int flashIndex;
    HdCrtl hdCtrl;
}KEYPAD_LED_PROPERTY;

// Define Object Function - Public----------------------------------------------
void API_KeypadLedCtrl(LED_COLOR ledColor, LedCtrlType ctrlType);

// Define Object Function - Private---------------------------------------------

#endif

