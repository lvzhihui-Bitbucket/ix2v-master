/**
  ******************************************************************************
  * @file    obj_MyCtrlLed.h
  * @author  cao
  * @version V1.0.0
  * @date    2022.02.14
  * @brief   This file contains the headers of the obj_MyCtrlLed.
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2012 V-Tec</center></h2>
  ******************************************************************************
  */
#ifndef _obj_MyCtrlLed_H
#define _obj_MyCtrlLed_H


// Define Object Property-------------------------------------------------------
typedef enum
{
    MY_LED_LOCK,
    MY_LED_TALK,
    MY_LED_CALL1,
    MY_LED_CALL2,
    MY_LED_CALL3,
    MY_LED_CALL4,
    MY_LED_NAMEPLATE,
}LED_SELECT;

typedef enum
{
    MY_LED_ON,
    MY_LED_OFF,
    MY_LED_FLASH_SLOW,
    MY_LED_FLASH_FAST,
    MY_LED_FLASH_IRREGULAR,
}MyLedCtrlType;

typedef void (*MyLedHdCrtl)(int);

typedef struct
{
    int name;
    int initCtrlType;
    int state;
    int ctrlType;
    int flashIndex;
    int timeCnt;        //控制定时计数
    int timing;         //控制定时总时间
    int flashTimeCnt;   //闪烁定时计数
    int* flashTab;
    MyLedHdCrtl hdCtrl;
}MY_LED_PROPERTY;

// Define Object Function - Public----------------------------------------------
void API_MyLedCtrl(LED_SELECT led, MyLedCtrlType ctrlType, int time);

// Define Object Function - Private---------------------------------------------

#endif

