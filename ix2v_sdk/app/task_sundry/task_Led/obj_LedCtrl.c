/**
  ******************************************************************************
  * @file    obj_LedCtrl.c
  * @author  cao
  * @version V1.0.0
  * @date    2022.02.14
  * @brief   This file contains the functions of the obj_LedCtrl
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2012 V-Tec</center></h2>
  ******************************************************************************
*/
#include "obj_LedCtrl.h"
#include "vdp_uart.h"
#include "obj_multi_timer.h"
#include "obj_PublicInformation.h"

static OS_TIMER timerLedCtrl;

//100ms软定时的回调函数
static void TimerLedCtrCallBack(void)
{

	OS_RetriggerTimer(&timerLedCtrl);
}

void LedCtrlInit(void)	//R
{
	//指示灯检测状态改变定时器
	OS_CreateTimer(&timerLedCtrl, TimerLedCtrCallBack, 100/25);
	OS_RetriggerTimer(&timerLedCtrl);
}

void LedMcuCtrl(LED_ID ledId, LedCtrlType ctrlType)
{
	char buff[2];

	buff[0] = ctrlType;
	buff[1] = ledId;
	api_uart_send_pack(UART_TYPE_N2S_LED_CTRL, buff, 2);
}
