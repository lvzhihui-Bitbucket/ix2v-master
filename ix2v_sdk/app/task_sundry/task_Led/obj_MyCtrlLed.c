/**
  ******************************************************************************
  * @file    obj_MyCtrlLed.c
  * @author  cao
  * @version V1.0.0
  * @date    2022.02.14
  * @brief   This file contains the functions of the obj_MyCtrlLed
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2012 V-Tec</center></h2>
  ******************************************************************************
*/
#include "obj_MyCtrlLed.h"
#include "obj_gpio.h"
#include "obj_multi_timer.h"
#include "utility.h"

#define MY_LED_TIMER_BASE      100

#if	defined(PID_IX611)|| defined( PID_IX622)|| defined( PID_IX821)
static void MyLedLockCtrl(int state)
{
    if(state)
    {
        STATE_LED_TURN_ON();
    }
    else
    {
        STATE_LED_TURN_OFF();
    }
}
#endif

#if	defined( PID_IX622)
static void MyLedTalkCtrl(int state)
{
    if(state)
    {
        TALK_LED_TURN_ON();
    }
    else
    {
        TALK_LED_TURN_OFF();
    }
}

static void MyLedCall1Ctrl(int state)
{
    if(state)
    {
        LED_CALL_X_RESET(0);
    }
    else
    {
        LED_CALL_X_SET(0);
    }
}
static void MyLedCall2Ctrl(int state)
{
    if(state)
    {
        LED_CALL_X_RESET(1);
    }
    else
    {
        LED_CALL_X_SET(1);
    }
}
static void MyLedCall3Ctrl(int state)
{
    if(state)
    {
        LED_CALL_X_RESET(2);
    }
    else
    {
        LED_CALL_X_SET(2);
    }
}
static void MyLedCall4Ctrl(int state)
{
    if(state)
    {
        LED_CALL_X_RESET(3);
    }
    else
    {
        LED_CALL_X_SET(3);
    }
}

static void MyLedNameplateCtrl(int state)
{
    if(state)
    {
        NameplateLight_Ctrl(GetNameplateLightDuty(0));
    }
    else
    {
        NameplateLight_Ctrl(0);
    }
}
#endif

static MY_LED_PROPERTY myLedTab[] = 
{
#if	defined(PID_IX611)|| defined( PID_IX622)
    {.name = MY_LED_LOCK, .initCtrlType = MY_LED_OFF, .hdCtrl = MyLedLockCtrl},
#endif

#if	defined( PID_IX622)
    {.name = MY_LED_TALK, .initCtrlType = MY_LED_OFF, .hdCtrl = MyLedTalkCtrl},
    {.name = MY_LED_CALL1, .initCtrlType = MY_LED_ON, .hdCtrl = MyLedCall1Ctrl},
    {.name = MY_LED_CALL2, .initCtrlType = MY_LED_ON, .hdCtrl = MyLedCall2Ctrl},
    {.name = MY_LED_CALL3, .initCtrlType = MY_LED_ON, .hdCtrl = MyLedCall3Ctrl},
    {.name = MY_LED_CALL4, .initCtrlType = MY_LED_ON, .hdCtrl = MyLedCall4Ctrl},
    {.name = MY_LED_NAMEPLATE, .initCtrlType = MY_LED_ON, .hdCtrl = MyLedNameplateCtrl},
#endif
};

const static int MY_LED_NUM = (sizeof(myLedTab)/sizeof(myLedTab[0]));
static OS_TIMER myLedCtrlTimer;
static int FastFlashTable[] = {2, 500, 500};
static int SlowFlashTable[] = {2, 2000, 2000};
static int IrregularFlashTable[] = {6, 100, 100, 100, 100, 300, 300};
static int myLedInitState = 0;

static void MyLedCtrl(MY_LED_PROPERTY* led, MyLedCtrlType ctrlType, int time)
{
    led->ctrlType = ctrlType;
    led->timing = (time < 0 ? 0 : time*1000/MY_LED_TIMER_BASE);
    led->timeCnt = 0;

    switch (ctrlType)
    {
        case MY_LED_ON:
            led->state = 1;
            (*(led->hdCtrl))(led->state);
            break;
        case MY_LED_OFF:
            led->state = 0;
            (*(led->hdCtrl))(led->state);
            break;
        case MY_LED_FLASH_FAST:
            led->flashTab = FastFlashTable;
            led->flashIndex = 1;
            led->flashTimeCnt = 0;
            led->state = 1;
            (*(led->hdCtrl))(led->state);
            break;
        case MY_LED_FLASH_SLOW:
            led->flashTab = SlowFlashTable;
            led->flashIndex = 1;
            led->flashTimeCnt = 0;
            led->state = 1;
            (*(led->hdCtrl))(led->state);
            break;
        case MY_LED_FLASH_IRREGULAR:
            led->flashTab = IrregularFlashTable;
            led->flashIndex = 1;
            led->flashTimeCnt = 0;
            led->state = 1;
            (*(led->hdCtrl))(led->state);
            break;
        
        default:
            break;
    }
}

static void MyLedFlashCtrl(MY_LED_PROPERTY* led)
{
    //总定时到则恢复初始控制状态
    if(led->timing)
    {
        if(++led->timeCnt >= led->timing)
        {
            MyLedCtrl(led, led->initCtrlType, 0);
            return;
        }
    }

    //闪烁时间到，则改变状态
    if(led->ctrlType == MY_LED_FLASH_FAST || led->ctrlType == MY_LED_FLASH_SLOW || led->ctrlType == MY_LED_FLASH_IRREGULAR)
    {
        if(++led->flashTimeCnt >= (led->flashTab[led->flashIndex]/MY_LED_TIMER_BASE))
        {
            led->flashTimeCnt = 0;
            led->state = (led->flashIndex%2) ? 1 : 0;
            (*(led->hdCtrl))(led->state);
            if(++(led->flashIndex) > led->flashTab[0])
            {
                led->flashIndex = 1;
            }
        }
    }
}

static void TimerMyLedCtrCallBack(void)
{
    int i;
    for(i = 0; i < MY_LED_NUM; i++)
    {
        MyLedFlashCtrl(&(myLedTab[i]));
    }
    OS_RetriggerTimer(&myLedCtrlTimer);
}

void MyLedCtrlInit(void)
{
    if(!myLedInitState)
    {
        myLedInitState = 1;
        #if	defined( PID_IX622)|| defined( PID_IX821)
        MK_LedStopFlashing();
        system("insmod /usr/modules/back_vision_drv.ko");
        #endif

        int i;
        for(i = 0; i < MY_LED_NUM; i++)
        {
            MyLedCtrl(&(myLedTab[i]), myLedTab[i].initCtrlType, 0);
        }

        OS_CreateTimer(&myLedCtrlTimer, TimerMyLedCtrCallBack, MY_LED_TIMER_BASE/25);
        OS_RetriggerTimer(&myLedCtrlTimer);
    }
}

void API_MyLedCtrl(LED_SELECT led, MyLedCtrlType ctrlType, int time)
{
    if(myLedInitState)
    {
        dprintf("API_MyLedCtrl led = %d, ctrlType = %d, time = %d\n", led, ctrlType, time);
        int i;
        for(i = 0; i < MY_LED_NUM; i++)
        {
            if(led == myLedTab[i].name)
            {
                MyLedCtrl(&(myLedTab[i]), ctrlType, time);
                break;
            }
        }
    }
}
