/**
  ******************************************************************************
  * @file    task_Talk.c
  * @author  zeng
  * @version V1.0.0
  * @date    2012.08.15
  * @brief   This file contains the functions of task_Talk
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2012 V-Tec</center></h2>
  ******************************************************************************
  */
#include "RTOS.h"
#include "OSQ.h"
#include "OSTIME.h"
#include "task_Sundry.h"
#include "task_survey.h"
#include "task_Talk.h"
#include "obj_TalkState.h"
#include "obj_PT2259.h"
#include "hd_Audio.h"


/******************************************************************************
 * @fn      vtk_TaskInit_Talk()
 *
 * @brief   Task talk initialize
 *
 * @param   priority - Task priority
 *
 * @return  none
 *****************************************************************************/
void vtk_TaskInit_Talk( void )
{
	HD_Audio_Init();
	
    	// talk state parameter init
    	TalkStateInit();
}

/******************************************************************************
 * @fn      vtk_TaskProcessEvent_Talk()
 *
 * @brief   Task talk loop routine
 *
 * @param   none
 *
 * @return  none
 *****************************************************************************/
void vtk_TaskProcessEvent_Talk(MsgTalk *msgTalk)
{
    	if( msgTalk->msgType == TALK_ON )
    	{
       	 	if( talkState.talkingState != TALK_ON )
        	{
            		EnterTalkingState();
        	}
    	}
    	else if( msgTalk->msgType == TALK_OFF )
    	{
       	 	ExitTalkingState();
    	}
}

/******************************************************************************
 * @fn      TalkStateInit()
 *
 * @brief   talk state parameter init
 *
 * @param   none
 *
 * @return  none
 *****************************************************************************/
void TalkStateInit(void)
{
    	talkState.talkingState = TALK_OFF;
    	SPKMuteCtrl(MUTE_ON);                     //lyx 20170502
    	MICMuteCtrl(MUTE_ON);
    	NJW1124Direct(NJW1124_IDLE);

    	//PT2259_Reset();
		PT2259_Set(CHANNEL_MIC,ConvertMicVolumnData(8));
}

/******************************************************************************
 * @fn      API_TalkOn()
 *
 * @brief   Provide an application interface for other task to requst talk on
 *
 * @param   none
 *
 * @return  none
 *****************************************************************************/
void API_TalkOn(void)
{
    	MsgTalk msgTalk;

 	msgTalk.head.msg_source_id 	= 0;
	msgTalk.head.msg_target_id 	= MSG_ID_SUNDRY;
	msgTalk.head.msg_type 		= MSG_TYPE_TALK;
	msgTalk.head.msg_sub_type 	= 0;
    	msgTalk.msgMainType = MSG_TYPE_TALK;
    	msgTalk.msgType = TALK_ON;

	push_vdp_common_queue(&vdp_sundry_mesg_queue, (char*)&msgTalk, sizeof(MsgTalk));
		
}

/******************************************************************************
 * @fn      API_TalkOff()
 *
 * @brief   Provide an application interface for other task to requst talk off
 *
 * @param   none
 *
 * @return  none
 *****************************************************************************/
void API_TalkOff(void)
{
    MsgTalk msgTalk;

 	msgTalk.head.msg_source_id 	= 0;
	msgTalk.head.msg_target_id 	= MSG_ID_SUNDRY;
	msgTalk.head.msg_type 		= MSG_TYPE_TALK;
	msgTalk.head.msg_sub_type 	= 0;
    msgTalk.msgMainType = MSG_TYPE_TALK;
    msgTalk.msgType = TALK_OFF;

	push_vdp_common_queue(&vdp_sundry_mesg_queue, (char*)&msgTalk, sizeof(MsgTalk));
}

// lzh_20170802_s
/******************************************************************************
 * @fn      API_Speaker_Adjust()
 *
 * @brief   Provide an application interface for talk adjust
 *
 * @param   none
 *
 * @return  current volume
 *****************************************************************************/
int API_Speaker_Adjust( int inc )
{
	return adjust_speaker_volume(inc);
}

int API_Get_Speaker_Volume( void )
{
	return get_speaker_volume();
}

// lzh_20170802_e


