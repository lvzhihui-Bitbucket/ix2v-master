/**
  ******************************************************************************
  * @file    obj_TalkState.h
  * @author  zeng
  * @version V1.0.0
  * @date    2012.09.25
  * @brief   This file contains the headers of the obj_TalkState.
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2012 V-Tec</center></h2>
  ******************************************************************************
  */ 
#ifndef OBJ_TALK_STATE_H
#define OBJ_TALK_STATE_H

/*******************************************************************************
                         Define object property
*******************************************************************************/
#define PRESS_TO_TALK_VOL	20
#define TALK_CHANNEL_CLOSE	79

typedef struct
{
    unsigned char talkingState;
    unsigned char SPK_Volume;
    unsigned char MIC_Volume;
} TALK_STATE;

extern TALK_STATE talkState;

/*******************************************************************************
                      Declare object funtion - public
*******************************************************************************/
unsigned char GetTalkingState(void);
// lzh_20170802_s
int Get_SPK_Volume(void);
void Set_SPK_Volume(int volume);
// lzh_20170802_e
unsigned char Get_MIC_Volume(void);
void Set_MIC_Volume(unsigned char scale);
void SPK_MuteOn(void);
void SPK_MuteOff(void);
void MIC_MuteOn(void);
void MIC_MuteOff(void);
void TalkStateInit(void);
void Process_Talk_SYS_EVENT_MSG(void);

/*******************************************************************************
                      Declare object funtion - private
*******************************************************************************/
void EnterTalkingState(void);
void ExitTalkingState(void);

// lzh_20170802_s
// para: 		inc - 0/decrease, 1/increase
// return: 	-1/already min/max, 0/ok
int adjust_speaker_volume( int inc );
int get_speaker_volume( void );
// lzh_20170802_e


#endif


