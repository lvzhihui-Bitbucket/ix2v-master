/**
  ******************************************************************************
  * @file    obj_PT2259.c
  * @author  zhangxijun
  * @version V1.0.0
  * @date    2013.03.19
  * @brief   This file contains the functions of PT2259
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2012 V-Tec</center></h2>
  ******************************************************************************
  */
#include "RTOS.h"
#include "OSQ.h"
#include "OSTIME.h"
#include "task_Sundry.h"
#include "task_survey.h"
#include "task_Talk.h"
#include "obj_TalkState.h"
#include "obj_PT2259.h"
#include "hd_Audio.h"
#include "vdp_uart.h"

#include "task_Hal.h"
#include "hal_gpio_def.h"


/*------------------------------------------------------------------------
			受话音量等级: 设置等级参数 => PT2259设置值
入口:	受话设置的音量等级

处理:
		spk_volume_select			return
		0 					=>		15
		1 					=>		14
		2 					=>		13
		3 					=>		12
		4 					=>		11
		5 					=>		10
		6 					=>		9
		7 					=>		8
		8 					=>		7
		9 					=>		6

出口:	PT2259对应的音量值
------------------------------------------------------------------------*/
uint8 ConvertSpkVolumnData(uint8 spk_volume_select)
{
    	return ( (9-spk_volume_select)*1 );
}

/*------------------------------------------------------------------------
			送话音量等级: 设置等级参数 => PT2259设置值
入口:	送话设置的音量等级

处理:
		spk_volume_select			return
		0 					=>		18
		1 					=>		16
		2 					=>		14
		3 					=>		12
		4 					=>		10
		5 					=>		8
		6 					=>		6
		7 					=>		4
		8 					=>		2
		9 					=>		0

出口:	PT2259对应的音量值
------------------------------------------------------------------------*/
uint8 ConvertMicVolumnData(uint8 mic_volume_select)
{
	return ( (9 - mic_volume_select)*2 );
}


/*------------------------------------------------------------------------
					PT2259 Reset
------------------------------------------------------------------------*/
void PT2259_Reset(void)
{
/*
	SI2C_WriteRegistor( SI2C_CH_1, PT2259_WR, REG_CLEAR_ALL, REG_MUTE_SEL );// LCC
*/	
}

/*------------------------------------------------------------------------
						PT2225音量设置
入口:


处理:
		spk_volume_select			return
		0 					=>		18
		1 					=>		16
		2 					=>		14
		3 					=>		12
		4 					=>		10
		5 					=>		8
		6 					=>		6
		7 					=>		4
		8 					=>		2
		9 					=>		0

出口:	PT2259对应的音量值
------------------------------------------------------------------------*/
void PT2259_Set(uint8 channel_select, uint8 db_pt2259)         //lyx 20170720
{
	uint8 buff[2];
	static int PT2259_fd=-1;
	if(PT2259_fd==-1)
	{
		PT2259_fd = open("/dev/pt2259",O_RDWR);
	}
	if(PT2259_fd==-1)
		return;
	switch(channel_select)
	{
    		case CHANNEL_MIC:
/*
			buff[0] = 0;
			buff[1] = db_pt2259;
			// 4 = N329向STM8申请: 控制PT2259 - 通道选择(0=MIC; 1=SPK)+衰减DB数
			api_uart_send_pack(UART_TYPE_N2S_PT2259_CTRL, buff, 2);
*/

			printf("---------CHANNEL_MIC-------\r\n");	
			buff[0] = 0;
			buff[1] = db_pt2259;
			ioctl( PT2259_fd, PT2259_SET, buff );
			break;

    		case CHANNEL_SPK:
/*
			buff[0] = 1;
			buff[1] = db_pt2259;
			// 4 = N329向STM8申请: 控制PT2259 - 通道选择(0=MIC; 1=SPK)+衰减DB数
			api_uart_send_pack(UART_TYPE_N2S_PT2259_CTRL, buff, 2);
*/
			printf("---------CHANNEL_SPK-------\r\n");	
			buff[0] = 1;
			buff[1] = db_pt2259;
			ioctl( PT2259_fd, PT2259_SET, buff );
        		break;

				
    		case CHANNEL_ALL:
/*
			buff[0] = 0;
			buff[1] = db_pt2259;
			// 4 = N329向STM8申请: 控制PT2259 - 通道选择(0=MIC; 1=SPK)+衰减DB数
			api_uart_send_pack(UART_TYPE_N2S_PT2259_CTRL, buff, 2);
			buff[0] = 1;
			buff[1] = db_pt2259;
			// 4 = N329向STM8申请: 控制PT2259 - 通道选择(0=MIC; 1=SPK)+衰减DB数
			api_uart_send_pack(UART_TYPE_N2S_PT2259_CTRL, buff, 2);
*/

			printf("---------CHANNEL_ALL-------\r\n");	
			buff[0] = 0;
			buff[1] = db_pt2259;
			ioctl( PT2259_fd, PT2259_SET, buff );
			buff[0] = 1;
			buff[1] = db_pt2259;
			ioctl( PT2259_fd, PT2259_SET, buff );
        		break;
	}
}
//#include "obj_GP_OutProxy.h"
#include "obj_gpio.h"
#include "obj_IoInterface.h"
void OpenDtTalkWithPara(int spk_vol,int mic_vol)
{
	API_Bin_ctrl(	"AU_PWR_EN",1);
	PT2259_Set(CHANNEL_SPK,spk_vol);
	PT2259_Set(CHANNEL_MIC,mic_vol);
	//AMPMUTE_SET();
	AMP_TALK_OPEN();
}
void CloseDtTalk(void)
{
	#if 1
	API_Bin_ctrl(	"AU_PWR_EN",0);
	//AMPMUTE_RESET();
	AMP_TALK_CLOSE();
	#endif
}
void OpenDtTalk(void)
{
	OpenDtTalkWithPara(API_Para_Read_Int(AU_DT_SPK_DEC),API_Para_Read_Int(AU_DT_MIC_DEC));
}
void OpenAppTalk(void)
{
	API_Bin_ctrl(	"AU_PWR_EN",0);
	//PT2259_Set(CHANNEL_SPK,ConvertSpkVolumnData(spk_vol));
	//PT2259_Set(CHANNEL_MIC,ConvertMicVolumnData(mic_vol));
	//AMPMUTE_SET();
	AMP_TALK_OPEN();
}