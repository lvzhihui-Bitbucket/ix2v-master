/**
  ******************************************************************************
  * @file    hd_Audio.c
  * @author  zeng
  * @version V1.0.0
  * @date    2012.08.15
  * @brief   This file contains the functions of hd_Audio
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2012 V-Tec</center></h2>
  ******************************************************************************
  */
#include "RTOS.h"
#include "OSQ.h"
#include "OSTIME.h"
#include "task_Sundry.h"
#include "task_survey.h"
#include "task_Talk.h"
#include "obj_TalkState.h"
#include "obj_PT2259.h"
#include "hd_Audio.h"

#include "hal_gpio_def.h"
#include "task_Hal.h"


unsigned char spkState;


void HD_Audio_Init(void)
{
}

/***********************************************************************
 * @fn      MICMuteCtrl
 *
 * @brief   enable or disable microphone mute function
 *
 * @param   en  - enable or disable
 *
 * @return  none
 ************************************************************************/
void MICMuteCtrl(uint8 mic_mute_contorl)
{
	int powerOn;
		
	powerOn = mic_mute_contorl? 1:0;

	
    	if (mic_mute_contorl == MUTE_ON)
    	{
       		//GPIO_SetBits(MIC_MUTE_GPIO_PORT, MIC_MUTE_GPIO_PIN);
       		//ioctl( hal_fd, MIC_MUTE_ENABLE, &powerOn );      //lyx 20170502
    	}
    	else
    	{
       		//GPIO_ResetBits(MIC_MUTE_GPIO_PORT, MIC_MUTE_GPIO_PIN);
       		//ioctl( hal_fd, MIC_MUTE_ENABLE, &powerOn );      //lyx 20170502
    	}

}

/***********************************************************************
 * @fn      SPKMuteCtrl
 *
 * @brief   enable or disable speaker mute function
 *
 * @param   en  - enable or disable
 *
 * @return  none
 ************************************************************************/
void SPKMuteCtrl(uint8 spk_mute_control)
{

	int powerOn;
		
	powerOn = spk_mute_control? 0:1;


    	if (spk_mute_control == MUTE_ON)
    	{
       		if( spkState != MUTE_ON )
        	{
            		//GPIO_ResetBits(AMPMUTE_GPIO_PORT, AMPMUTE_GPIO_PIN);
            		//ioctl( hal_fd, AMP_MUTE_ENABLE, &powerOn );      //lyx 20170502
            		spkState = MUTE_ON;
        	}
    	}
    	else
    	{
        	if( spkState != MUTE_OFF )
        	{
	            	//GPIO_SetBits(AMPMUTE_GPIO_PORT, AMPMUTE_GPIO_PIN);
			//ioctl( hal_fd, AMP_MUTE_ENABLE, &powerOn );      //lyx 20170502
	            	spkState = MUTE_OFF;
        	}
    	}
 
}

/*------------------------------------------------------------------------
						免提控制
入口:	uStatus
RTSW = 0: 1124输出,TX
RTSW = 1: 1124输入,RX
RTSW = X: 1124双向,AUTO
参数说明：
uStatus：0/待机状态;1/正常通话;2/广播状态;3/监听状态
njw1124_mode:保存在通话状态下1124的工作模式(若为0时不要保存)
注意：进入正常通话状态前，需要先开启LM386的音频充电一段时间（此时1124为关闭状态），然后在开启1124进行通话
------------------------------------------------------------------------*/
void NJW1124Direct(uint8 status_1124)
{

   	//lyx 20170502

	if( status_1124 == NJW1124_IDLE )		//置为输出，用在振铃期间防止MIC和室外机声音干扰	//RTSW口为输出低
	{
/*
		GPIO_InitTypeDef  GPIO_InitStructure;

		GPIO_InitStructure.GPIO_Pin 	= RTSW_GPIO_PIN;
		GPIO_InitStructure.GPIO_Mode 	= GPIO_Mode_OUT;
		GPIO_InitStructure.GPIO_Speed 	= GPIO_Speed_2MHz;
		GPIO_InitStructure.GPIO_OType 	= GPIO_OType_PP;
		GPIO_InitStructure.GPIO_PuPd  	= GPIO_PuPd_NOPULL;
		GPIO_Init(RTSW_GPIO_PORT, &GPIO_InitStructure);

		GPIO_ResetBits(RTSW_GPIO_PORT,RTSW_GPIO_PIN);
*/

		dprintf("AUDIO >>>NJW1124_STATUS = %d.\n", status_1124);
		//ioctl( hal_fd, NJW1124_DIR_SET, &status_1124 );

	}
	else if( status_1124 == NJW1124_TALK ) 		//IDLE 双向输入，用在通话状态	//RTSW口为输入（OC）
	{
/*
		GPIO_InitTypeDef  GPIO_InitStructure;

		GPIO_InitStructure.GPIO_Pin 	= RTSW_GPIO_PIN;
		GPIO_InitStructure.GPIO_Mode 	= GPIO_Mode_IN;
		GPIO_InitStructure.GPIO_Speed 	= GPIO_Speed_2MHz;
		GPIO_InitStructure.GPIO_OType 	= GPIO_OType_OD;
		GPIO_InitStructure.GPIO_PuPd  	= GPIO_PuPd_NOPULL;
		GPIO_Init(RTSW_GPIO_PORT, &GPIO_InitStructure);
*/

		dprintf("AUDIO >>>NJW1124_STATUS = %d.\n", status_1124);
		//ioctl( hal_fd, NJW1124_DIR_SET, &status_1124 );
	}
	
}


