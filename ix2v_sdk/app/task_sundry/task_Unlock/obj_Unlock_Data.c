/**
  ******************************************************************************
  * @file    obj_Unlock_Data.c
  * @author  zxj
  * @version V00.01.00
  * @date    2013.01.11
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2012 V-Tec</center></h2>
  ******************************************************************************
  */ 
#if 0

#include "obj_Unlock_Data.h"



UNLOCK_CONIFG	Lock_Config;

/*------------------------------------------------------------------------
						Unlock_Data_init
入口:  
	无

处理:
	初始化
------------------------------------------------------------------------*/
void Unlock_Data_Init(void)	//A
{
	uint8 read_buf;

	//锁1	
	API_Event_IoServer_InnerRead_All(UNLOCK1_MODE, (uint8*)&Lock_Config.lock1_mode);	//Unlock1开锁模式
	API_Event_IoServer_InnerRead_All(UNLOCK1_TIMING, (uint8*)&read_buf);				//Unlock1开锁延时
	if (read_buf == 1)	
	{
		Lock_Config.lock1_timer = read_buf * 6;	//单位为100ms, 设置1S时, 实际开锁延时为 1 * 6 * 100 =600ms
	}
	else
	{
		Lock_Config.lock1_timer = read_buf * 10;	//单位为100ms, 设置nS时, 实际开锁延时为 n * 10 * 100 =n(Kms)
	}
	//锁2
	API_Event_IoServer_InnerRead_All(UNLOCK2_MODE, (uint8*)&Lock_Config.lock2_mode);	//Unlock1开锁模式
	API_Event_IoServer_InnerRead_All(UNLOCK2_TIMING, (uint8*)&read_buf);				//Unlock1开锁延时
	if (read_buf == 1)	
	{
		Lock_Config.lock2_timer = read_buf * 6;	//单位为100ms, 设置1S时, 实际开锁延时为 1 * 6 * 100 =600ms
	}
	else
	{
		Lock_Config.lock2_timer = read_buf * 10;	//单位为100ms, 设置nS时, 实际开锁延时为 n * 10 * 100 =n(Kms)
	}
}
#endif
/*********************************************************************************************************
**  End Of File
*********************************************************************************************************/
