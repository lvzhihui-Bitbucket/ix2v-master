/**
  ******************************************************************************
  * @file    task_Unlock.h
  * @author  zxj
  * @version V00.01.00
  * @date    2012.08.22
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2012 V-Tec</center></h2>
  ******************************************************************************
  */ 

#ifndef _task_Unlock_H
#define _task_Unlock_H
//KP: V000103	//ID: V000101
#if 0
#include "RTOS.h"
#include "BSP.h"

#include "obj_Unlock_State.h"


// Define Task Vars and Structures----------------------------------------------
	//�ڼ�����
#define LOCK1						1	//��1
#define LOCK2						2	//��2

#define MSG_UNLOCK_SOURCE_KEY       0	//20140625	
#define MSG_UNLOCK_SOURCE_CMD       1	//20140625


// Define task interface-------------------------------------------------------------
	//resource semaphores------------------------

	//MailBoxes----------------------------------
typedef struct MSG_UNLOCK_STRU	
{	
    uint8   msgMainType;
	uint8	msg_type;						//��Ϣ����
	uint8	lock_ID;						//�İ���:  1=��һ����,2=�ڶ�����
    uint8   msg_unlock_source;		//20140625		
}	MSG_UNLOCK;
typedef struct MSG_UNLOCK_TIMER_STRU	
{	
    uint8   msgMainType;
	uint8	msg_type;						//��Ϣ����
	uint8	lock_ID;						//�İ���:  1=��һ����,2=�ڶ�����
	uint8	unlock_timer;					//������ʱ
    uint8   msg_unlock_source;		//20140625		
}	MSG_UNLOCK_TIMER;
	//msg_type
#define MSG_TYPE_LOCK_OPEN			0
#define MSG_TYPE_LOCK_CLOSE			1
#define MSG_TYPE_LOCK_TIMER_CLOSE	2
#define MSG_TYPE_LOCK_ERROR			3	//20131108_UnlockSecond
#define MSG_TYPE_LOCK_OPEN_TIMER	4

	//Queues-------------------------------------

	//Task events--------------------------------

	//Event objects------------------------------

// Define Task 2 items----------------------------------------------------------
	//����ʱ
extern OS_TIMER unlock1_timing;		//����1��ʱ
extern OS_TIMER unlock2_timing;		//����2��ʱ
extern OS_TIMER response_timing;	//����2��֤ʵ��ʱ	//20131108_UnlockSecond
extern OS_TIMER unlock_disable_timing;		//����ʹ�ܶ�ʱ	//20140625
//extern OS_TASK tcb_unlock;                     //Task-control-blocks
void vtk_TaskInit_Unlock(void);


// Define Task others-----------------------------------------------------------
void vtk_TaskProcessEvent_Unlock(MSG_UNLOCK *msgUnlock);
void Unlock1_Timer_Off(void);
void Unlock2_Timer_Off(void);
void Response_Timer_Off(void);	//20131108_UnlockSecond
void Unlock_Disable_Off(void);	//20140625



// Define API-------------------------------------------------------------------
void API_Unlock(uint8 msg_type_temp, uint8 msg_lock_ID_temp, uint8 source);	//20140625
void API_Unlock_Timer(uint8 msg_type_temp , uint8 msg_lock_ID_temp, uint8 unlock_timer, uint8 source);	//20140625

	/*void API_Unlock1(void)
	���:	��

	����:	������Ϣ��Unlock

	����:	��
	*/
	#define API_Unlock1(source)	API_Unlock(MSG_TYPE_LOCK_OPEN , LOCK1, source)	//20140625
			
	/*void API_Unlock1_Timer(uint8 unlock_timer, uint8 source)
	���:	��

	����:	������Ϣ��Unlock

	����:	��
	*/
	#define API_Unlock1_Timer(unlock_timer, source)	API_Unlock_Timer(MSG_TYPE_LOCK_OPEN_TIMER , LOCK1, unlock_timer, source)	//20140625

	/*void API_Lock1(void)
	���:	��

	����:	������Ϣ��Unlock

	����:	��
	*/
	#define API_Lock1()		API_Unlock(MSG_TYPE_LOCK_CLOSE , LOCK1, NULL)	//20140625

	/*void API_Unlock2(void)
	���:	��

	����:	������Ϣ��Unlock

	����:	��
	*/
	#define API_Unlock2(source)	API_Unlock(MSG_TYPE_LOCK_OPEN , LOCK2, source)	//20140625
			

	/*void API_Set_Relay_Unlock(void)
	���:	��

	����:	���ݿ���ģʽ�ÿ����̵�����ʼ״̬

	����:	��
	*/
	#define API_Set_Relay_Unlock()	Set_Relay_Unlock(1)

	//20131108_UnlockSecond
	#define API_Unlock2_Error()	API_Unlock(MSG_TYPE_LOCK_ERROR , LOCK2, NULL)	//20140625

#else
#include "RTOS.h"
#include "task_Relay.h"

#define LOCK1						1	//��1
#define LOCK2						2	//��2

#define MSG_TYPE_LOCK_OPEN			0
#define MSG_TYPE_LOCK_CLOSE			1
#define MSG_TYPE_LOCK_TIMER_CLOSE	2
#define MSG_TYPE_LOCK_ERROR			3	//20131108_UnlockSecond
#define MSG_TYPE_LOCK_OPEN_TIMER	4

#define MSG_UNLOCK_SOURCE_KEY       0	//20140625	
#define MSG_UNLOCK_SOURCE_CMD       1	//20140625
void API_Unlock(uint8 msg_type_temp, uint8 msg_lock_ID_temp, uint8 source);

#define API_Unlock1(source)	{if(GetStm8CodeVersion() < 3) {API_Unlock(MSG_TYPE_LOCK_OPEN , LOCK1, source); BEEP_CONFIRM();} else API_Relay(MSG_TYPE_RELAY_OPEN);}
#define API_Unlock2(source)	{if(GetStm8CodeVersion() < 3) {API_Unlock(MSG_TYPE_LOCK_OPEN , LOCK2, source); BEEP_CONFIRM();} else API_Relay(MSG_TYPE_RELAY_OPEN);}
#endif

void unlock_mode_set( int mode );
int unlock_mode_get( void );
int unlock_level_set( int level );

#endif
