/**
  ******************************************************************************
  * @file    obj_Unlock_State.h
  * @author  zxj
  * @version V00.01.00
  * @date    2012.08.22
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2012 V-Tec</center></h2>
  ******************************************************************************
  */ 

#ifndef _obj_Unlock_State_H
#define _obj_Unlock_State_H
#if 0
//KP: V000103	//ID: V000101

#include "RTOS.h"
#include "BSP.h"


// Define Object Property-------------------------------------------------------
typedef struct UNLOCK_RUN_STRU	
{	
	uint8	lock1_state;				//锁1的状态(开锁/关锁)
	uint16	lock1_timer_count;			//锁1定时计数
	uint8	lock2_state;				//锁2的状态(开锁/关锁)
	uint16	lock2_timer_count;			//锁2定时计数
    uint8   cmd_unlock_disable_flag;    //20140625
    uint8   unlock_disable_timer;       //20140625
}	UNLOCK_RUN;
extern UNLOCK_RUN Lock_Run;
extern uint8 unlock_second_ok;	//20131108_UnlockSecond

// Define Object Function - Public----------------------------------------------
void Lock_Open_Timer(uint8 lock_ID, uint8 unlock_timer, uint8 source);	//20140625
void Lock_Open(uint8 lock_ID, uint8 source);	//20140625
void Lock_Open_Process(uint8 lock_ID, uint8 source);	//20140625
void Lock_Close(uint8 lock_ID);
uint8 Get_Unlock1_State(uint8 lock_ID);
void Set_Relay_Unlock(uint8 flag_init);
void Set_Unlock_Second_OK(void);	//20131108_UnlockSecond


// Define Object Function - Private---------------------------------------------

#endif
#endif
