/**
  ******************************************************************************
  * @file    obj_Unlock_Data.h
  * @author  zxj
  * @version V00.01.00
  * @date    2013.01.11
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2012 V-Tec</center></h2>
  ******************************************************************************
  */ 

#ifndef _obj_Unlock_Data_H
#define _obj_Unlock_Data_H
#if 0
#include "RTOS.h"
#include "BSP.h"


// Define Object Property-------------------------------------------------------
#define NORMAL_OPEN 	0
#define NORMAL_CLOSE 	1

typedef struct UNLOCK_CONIFG_STRU
{
	uint8	lock1_mode;
	uint16	lock1_timer;
	uint8	lock2_mode;
	uint16	lock2_timer;
    uint8   cmd_unlock_disable_timer;   //20140625
}	UNLOCK_CONIFG;
extern UNLOCK_CONIFG	Lock_Config;


// Define Object Function - Public----------------------------------------------
void Unlock_Data_Init(void);


// Define Object Function - Private---------------------------------------------

#endif
#endif
