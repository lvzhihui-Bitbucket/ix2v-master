/**
  ******************************************************************************
  * @file    task_Unlock.c
  * @author  zxj
  * @version V00.01.00
  * @date    2012.08.22
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2012 V-Tec</center></h2>
  ******************************************************************************
  */ 
//KP: V000103	//ID: V000101
#include "task_Sundry.h"
#include "task_Unlock.h"

#include "vdp_uart.h"
//#include "task_VideoMenu.h"
//#include "obj_menu_data.h"

#include "task_Led.h"

#define Ulock_Sprite_Disp_x	60
#define Ulock_Sprite_Disp_y	340
#if 0
#include "task_Sundry.h"
#include "obj_Unlock_State.h"
#include "obj_Unlock_Data.h"
//#include "task_Beeper.h"	


//����ʱ
OS_TIMER unlock1_timing;		//����1��ʱ
OS_TIMER unlock2_timing;		//����2��ʱ
OS_TIMER response_timing;		//����2��֤ʵ��ʱ	//20131108_UnlockSecond	
OS_TIMER unlock_disable_timing;		//����ʹ�ܶ�ʱ	//20140625

#else
OS_TIMER unlock1_timing;		//����1��ʱ
OS_TIMER unlock2_timing;		//����2��ʱ
typedef struct
{
	int lock1_state;
	int lock2_state;
	int lock1_timer_count;
	int lock2_timer_count;
}Lock_Run_Stru;

Lock_Run_Stru Lock_Run;

void Unlock1_Timer_Off(void);
void Unlock2_Timer_Off(void);
#endif
/*------------------------------------------------------------------------
						task_init
------------------------------------------------------------------------*/
static int fd;
void vtk_TaskInit_Unlock(void)
{
#if 0
	//run_state init
	Lock_Run.lock1_state = 0;
	Lock_Run.lock2_state = 0;
	
	//���ݿ���ģʽ,�ÿ����̵�����ʼ״̬
	Set_Relay_Unlock(0);	
	
	//��������ʱ(δ����)
	OS_CreateTimer(&unlock1_timing, Unlock1_Timer_Off, 100);	//100ms
	OS_CreateTimer(&unlock2_timing, Unlock2_Timer_Off, 100);	//100ms
	OS_CreateTimer(&response_timing, Response_Timer_Off, 2000);	//2S	//20131108_UnlockSecond
	OS_CreateTimer(&unlock_disable_timing, Unlock_Disable_Off, 100);	//100ms	//20140625
#else
	Lock_Run.lock1_state = 0;
	Lock_Run.lock2_state = 0;
	OS_CreateTimer(&unlock1_timing, Unlock1_Timer_Off, 100/25);	//100ms
	OS_CreateTimer(&unlock2_timing, Unlock2_Timer_Off, 100/25);	//100ms
#endif
// lzh_20231212_s
	fd = open("/dev/poweron", O_RDWR);
	if( fd < 0)
	{
		perror("open /dev/poweon\n");
	}
// lzh_20231212_e
}

/*------------------------------------------------------------------------
						task_process
------------------------------------------------------------------------*/
#if 0
void vtk_TaskProcessEvent_Unlock(MSG_UNLOCK *msgUnlock)
{

    switch (msgUnlock->msg_type)
    {
        case MSG_TYPE_LOCK_OPEN:		//����: ʹ�����õĿ�����ʱ
            Lock_Open(msgUnlock->lock_ID, msgUnlock->msg_unlock_source);	//20140625	
            break;
                
		case MSG_TYPE_LOCK_OPEN_TIMER:	//����: ʹ����ڲ����Ŀ�����ʱ
            Lock_Open_Timer(((MSG_UNLOCK_TIMER*)msgUnlock)->lock_ID, ((MSG_UNLOCK_TIMER*)msgUnlock)->unlock_timer, ((MSG_UNLOCK_TIMER*)msgUnlock)->msg_unlock_source);	//20140625
			break;			
			
        case MSG_TYPE_LOCK_CLOSE:		//����
            Lock_Close(msgUnlock->lock_ID);
            break;					 

        case MSG_TYPE_LOCK_TIMER_CLOSE:	//��ʱ����
            Lock_Close(msgUnlock->lock_ID);
            break;				

		case MSG_TYPE_LOCK_ERROR:		//���ڶ�������ʱ�޵ȵ�֤ʵ,����������ʾ��	//20131108_UnlockSecond
			//BEEP_ERROR();
			break;
            
        default:				//������Ϣ
			;
            break;
    }

}
#endif
/*------------------------------------------------------------------------
				����ʱ: ��1������ʱ��,������Ϣ,ʹ����
------------------------------------------------------------------------*/
void Unlock1_Timer_Off(void)
{
#if 0
	MSG_UNLOCK	send_msg_to_unlock;	

	Lock_Run.lock1_timer_count++;
	
	if (Lock_Run.lock1_timer_count >= Lock_Config.lock1_timer)	//��ʱ��
	{	
		send_msg_to_unlock.msgMainType = MSG_TYPE_UNLOCK;
		send_msg_to_unlock.msg_type = MSG_TYPE_LOCK_TIMER_CLOSE;
		send_msg_to_unlock.lock_ID = LOCK1; 
		
		if (OS_Q_Put(&sundryQ, &send_msg_to_unlock, sizeof(MSG_UNLOCK)))	//��Ϣ����ʧ��
		{
			OS_RetriggerTimer(&unlock1_timing);
		}
		else	//��Ϣ���ͳɹ�
		{
			OS_StopTimer(&unlock1_timing);	
		}
	}	
	else	//��ʱδ��
	{	
		OS_RetriggerTimer(&unlock1_timing);
	}
#else	
	if(Lock_Run.lock1_state == 0)
	{
		
		return;
	}
	
	Lock_Run.lock1_timer_count++;
	
	if(Lock_Run.lock1_state == 1)
	{
		if (Lock_Run.lock1_timer_count >= 10)	//��ʱ��
		{	
			Lock_Run.lock1_state = 2;
			Lock_Run.lock1_timer_count = 0;
			///API_SpriteDisplay_XY(Ulock_Sprite_Disp_x,Ulock_Sprite_Disp_y,SPRITE_UNLOCK);	
		}	
		
	}	
	else if(Lock_Run.lock1_state == 2)
	{
		if (Lock_Run.lock1_timer_count >= 20)	//��ʱ��
		{	
			Lock_Run.lock1_state = 0;
			Lock_Run.lock1_timer_count = 0;
			///API_SpriteClose(Ulock_Sprite_Disp_x,Ulock_Sprite_Disp_y,SPRITE_UNLOCK);
			OS_StopTimer(&unlock1_timing);
			//will_add_ix2	UnLockMenu();

			//will_add_ix2	API_LED_UNLOCK_OFF();
			
			return;
		}	
		
	}
	
	OS_RetriggerTimer(&unlock1_timing);

#endif

}

/*------------------------------------------------------------------------
				����ʱ: ��2������ʱ��,������Ϣ,ʹ����
------------------------------------------------------------------------*/
void Unlock2_Timer_Off(void)
{
#if 0
	MSG_UNLOCK	send_msg_to_unlock;	

	Lock_Run.lock2_timer_count++;
	
	if (Lock_Run.lock2_timer_count >= Lock_Config.lock2_timer)	//��ʱ��
	{	
		send_msg_to_unlock.msgMainType = MSG_TYPE_UNLOCK;
		send_msg_to_unlock.msg_type = MSG_TYPE_LOCK_TIMER_CLOSE;
		send_msg_to_unlock.lock_ID = LOCK2; 
		
		if (OS_Q_Put(&sundryQ, &send_msg_to_unlock, sizeof(MSG_UNLOCK)))	//��Ϣ����ʧ��
		{
			OS_RetriggerTimer(&unlock2_timing);
		}
		else	//��Ϣ���ͳɹ�
		{
			OS_StopTimer(&unlock2_timing);	
		}
	}	
	else	//��ʱδ��
	{	
		OS_RetriggerTimer(&unlock2_timing);
	}	
#else
	if(Lock_Run.lock2_state == 0)
	{
		//UnLockMenu();
		return;
	}
	
	Lock_Run.lock2_timer_count++;
	
	if(Lock_Run.lock2_state == 1)
	{
		if (Lock_Run.lock2_timer_count >= 10)	//��ʱ��
		{	
			Lock_Run.lock2_state = 2;
			Lock_Run.lock2_timer_count = 0;
			///API_SpriteDisplay_XY(Ulock_Sprite_Disp_x,Ulock_Sprite_Disp_y,SPRITE_UNLOCK2);
		}	
		
	}	
	else if(Lock_Run.lock2_state == 2)
	{
		if (Lock_Run.lock2_timer_count >= 20)	//��ʱ��
		{	
			Lock_Run.lock2_state = 0;
			Lock_Run.lock2_timer_count = 0;
			///API_SpriteClose(Ulock_Sprite_Disp_x,Ulock_Sprite_Disp_y,SPRITE_UNLOCK2);
			OS_StopTimer(&unlock2_timing);
			//will_add_ix2	UnLockMenu();

			//will_add_ix2	API_LED_UNLOCK_OFF();
			
			return;
		}	
		
	}
	
	OS_RetriggerTimer(&unlock2_timing);
#endif
}

/*------------------------------------------------------------------------
		����ʱ: ��ʱ�޵ȵ��ڶ������Ŀ���֤ʵ,������Ϣ
------------------------------------------------------------------------*/
//20131108_UnlockSecond
void Response_Timer_Off(void)	
{
#if 0
	if (unlock_second_ok)
	{
		unlock_second_ok = 0;
		API_Unlock2_Error();	
	}
#endif
}

/*------------------------------------------------------------------------
				����ʱ: ָ�����ֹ��ʱ��
------------------------------------------------------------------------*/
//20140625
void Unlock_Disable_Off(void)	
{
#if 0
	Lock_Run.unlock_disable_timer++;
	
	if (Lock_Run.unlock_disable_timer >= Lock_Config.cmd_unlock_disable_timer)	//��ʱ��
	{	
		Lock_Run.cmd_unlock_disable_flag = 0;
		OS_StopTimer(&unlock_disable_timing);	
	}	
	else	//��ʱδ��
	{	
		OS_RetriggerTimer(&unlock_disable_timing);
	}
#endif
}

/*------------------------------------------------------------------------
						������Ϣ=>Unlock(����)
------------------------------------------------------------------------*/
void API_Unlock(uint8 msg_type_temp , uint8 msg_lock_ID_temp, uint8 source)	//20140625	
{
#if 0
	MSG_UNLOCK	send_msg_to_unlock;	

    send_msg_to_unlock.msgMainType = MSG_TYPE_UNLOCK;
	send_msg_to_unlock.msg_type = msg_type_temp;
	send_msg_to_unlock.lock_ID = msg_lock_ID_temp; 
	send_msg_to_unlock.msg_unlock_source = source;	//20140625 	
    OS_Q_Put(&sundryQ, &send_msg_to_unlock, sizeof(MSG_UNLOCK));
#endif
	unsigned char buf[3];
	buf[0] = msg_type_temp;
	buf[1] = msg_lock_ID_temp;
	buf[2] = source;


	//will_add_ix2	api_uart_send_pack(UART_TYPE_N2S_UNLOCK_STATE,buf, 2);   
	//usleep(1000000);

	//will_add_ix2	API_LED_UNLOCK_ON();

	if(msg_lock_ID_temp == LOCK1)
	{
		
		//API_SpriteDisplay_XY(Ulock_Sprite_Disp_x,Ulock_Sprite_Disp_y,SPRITE_UNLOCK);
		//usleep(2000000);
		//API_SpriteClose(Ulock_Sprite_Disp_x,Ulock_Sprite_Disp_y,SPRITE_UNLOCK);
		if(Lock_Run.lock1_state == 0)
		{
			//BEEP_CONFIRM();
			//will_add_ix2	api_uart_send_pack(UART_TYPE_N2S_UNLOCK_STATE,buf, 3);
			Lock_Run.lock1_state = 1;
			Lock_Run.lock1_timer_count = 0;
			//will_add_ix2	LockMenuTiming(3200);
			OS_RetriggerTimer(&unlock1_timing);
		}
		
	}

	if(msg_lock_ID_temp == LOCK2)
	{
		//API_SpriteDisplay_XY(Ulock_Sprite_Disp_x,Ulock_Sprite_Disp_y,SPRITE_UNLOCK2);
		//usleep(2000000);
		//API_SpriteClose(Ulock_Sprite_Disp_x,Ulock_Sprite_Disp_y,SPRITE_UNLOCK2);
		if(Lock_Run.lock2_state == 0)
		{
			//BEEP_CONFIRM();
			//will_add_ix2	api_uart_send_pack(UART_TYPE_N2S_UNLOCK_STATE,buf, 3);
			Lock_Run.lock2_state = 1;
			Lock_Run.lock2_timer_count = 0;
			//will_add_ix2	LockMenuTiming(3200);
			OS_RetriggerTimer(&unlock2_timing);
		}
	}
	//API_Voice_Unlock();
}

/*------------------------------------------------------------------------
						������Ϣ=>Unlock(����)
------------------------------------------------------------------------*/
void API_Unlock_Timer(uint8 msg_type_temp , uint8 msg_lock_ID_temp, uint8 unlock_timer, uint8 source)	//20140625
{
#if 0
	MSG_UNLOCK_TIMER	send_msg_to_unlock;	

    send_msg_to_unlock.msgMainType = MSG_TYPE_UNLOCK;
	send_msg_to_unlock.msg_type = msg_type_temp;
	send_msg_to_unlock.lock_ID = msg_lock_ID_temp;
	send_msg_to_unlock.unlock_timer = unlock_timer;
	send_msg_to_unlock.msg_unlock_source = source;	//20140625 	
    OS_Q_Put(&sundryQ, &send_msg_to_unlock, sizeof(MSG_UNLOCK_TIMER));
#endif
}

// lzh_20231212_s
// mode: 1/normal close, 0/normal open

#define IOCTL_SET_UNLOCK_MODE              _IOW('u', 1, unsigned int*)
#define IOCTL_GET_UNLOCK_MODE              _IOR('u', 2, unsigned int*)
#define IOCTL_SET_UNLOCK_LEVEL             _IOW('u', 3, unsigned int*)
#define IOCTL_SET_MK_STOP_FLASHING         _IOW('u', 4, unsigned int*)

void MK_LedStopFlashing( int state )
{
	int ret;
	int msg;
	if( fd < 0 )
	{
		perror("open");
		return -1;
	}
	else
	{
		msg = state;
		ret = ioctl(fd, IOCTL_SET_MK_STOP_FLASHING, &msg);
		printf("MK_LedStopFlashing=[%d],result=[%d]\n",msg,ret);
		return ret;
	}
}

void unlock_mode_set( int mode )
{
	int ret;
	int msg;
	if( fd < 0 )
	{
		perror("open");
		return -1;
	}
	else
	{
		msg = mode;
		ret = ioctl(fd, IOCTL_SET_UNLOCK_MODE, &msg);
		printf("unlock_mode_set=[%d],result=[%d]\n",msg,ret);
		return ret;
	}
}

int unlock_mode_get( void )
{
	int ret;
	int msg;
	if( fd < 0 )
	{
		perror("open");
		return -1;
	}
	else
	{
		ret = ioctl(fd, IOCTL_GET_UNLOCK_MODE, &msg);
		printf("unlock_mode_get=[%d],result=[%d]\n",msg, ret);
		return msg;
	}
}

int unlock_level_set( int level )
{
	int ret;
	int msg;
	if( fd < 0 )
	{
		perror("open");
		return -1;
	}
	else
	{
		msg = level;
		ret = ioctl(fd, IOCTL_SET_UNLOCK_LEVEL, &msg);
		printf("unlock_level_set=[%d],result=[%d]\n",msg,ret);
		return ret;
	}
}
// lzh_20231212_e

/*********************************************************************************************************
**  End Of File
*********************************************************************************************************/
