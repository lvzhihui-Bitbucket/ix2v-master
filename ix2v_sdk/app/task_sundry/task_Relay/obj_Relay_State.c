/**
  ******************************************************************************
  * @file    obj_Relay_State.c
  * @author  zxj
  * @version V00.01.00
  * @date    2012.08.22
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2012 V-Tec</center></h2>
  ******************************************************************************
  */ 
//KP: V000103	//ID: V000101

#include "obj_Relay_State.h"
#include "task_Relay.h"
#include "task_Hal.h"
//#include "../../task_debug_sbu/task_debug_sbu.h"
#include "task_survey.h"
#include "hal_gpio_def.h"



RELAY_RUN Relay_Run;

/*------------------------------------------------------------------------
								Lock_Open_Timer	(开锁)
入口:  
	无

处理:
	开锁启动

返回: 
	无
------------------------------------------------------------------------*/
void Relay_Open_Timer(unsigned char relay_timer)
{
	int power;
	
	if (relay_timer == 1)	
	{
		Relay_Run.relay_timer = relay_timer * 6;	//单位为100ms, 设置1S时, 实际开锁延时为 1 * 6 * 100 =600ms
	}
	else
	{
		Relay_Run.relay_timer = relay_timer * 10;	//单位为100ms, 设置nS时, 实际开锁延时为 n * 10 * 100 =n(Kms)
	}
	
	if (Relay_Run.relay_mode == NORMAL_OPEN)	//常开模式
	{
		power = 1;
	}
	else	//常闭模式
	{
		power = 0;	
	}
	
	//ioctl( hal_fd, RL_CON_ENABLE, &power );

	dprintf("Relay >>>Relay_Run.relay_state = %d.\n", Relay_Run.relay_state );
	
	//开锁标志	
	Relay_Run.relay_state = 1;

	//启动开锁定时
	Relay_Run.relay_timer_count = 0;
	
	if(Relay_Run.relay_timer != 0)
	{
		OS_RetriggerTimer(&relayTiming);
	}
	else
	{
		OS_StopTimer(&relayTiming);
	}
}
/*------------------------------------------------------------------------
								Lock_Close	(关锁)
入口:  
	无

处理:
	关锁

返回: 
	无
------------------------------------------------------------------------*/
void Relay_Close(void)
{
	int power;
	
	if (Relay_Run.relay_mode == NORMAL_OPEN)	//常开模式
	{
		power = 0;	
	}
	else	//常闭模式
	{
		power = 1;	
	}

	//ioctl( hal_fd, RL_CON_ENABLE, &power );

	dprintf("Relay >>>Relay_Run.relay_state = %d.\n", Relay_Run.relay_state );

	//关锁标志	
	Relay_Run.relay_state = 0;
	
	//关闭开锁定时
	OS_StopTimer(&relayTiming);	
}

/*------------------------------------------------------------------------
						读取锁状态
入口:  
	无

处理:
	读取锁状态

返回: 
	0 = 关锁
	1 = 开锁
------------------------------------------------------------------------*/
uint8 Get_Relay_State(void)
{
	return (Relay_Run.relay_state);
}

/*********************************************************************************************************
**  End Of File
*********************************************************************************************************/
