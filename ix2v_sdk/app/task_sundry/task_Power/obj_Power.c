/**
  ******************************************************************************
  * @file    obj_Power.c
  * @author  zeng
  * @version V1.0.0
  * @date    2012.08.15
  * @brief   This file contains the functions of the obj_Power
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2012 V-Tec</center></h2>
  ******************************************************************************
*/

#include "obj_Power.h"

PowerStruct power;


/*********************************************************************
 * @fn      GetPowerTftState()
 *
 * @brief   Get the power of tft state, on or off
 *
 * @param   none
 *
 * @return  power.videoState
 *********************************************************************/
unsigned char GetPowerTftState(void)
{
    return power.tftState;
}

/*********************************************************************
 * @fn      GetPowerTalkState()
 *
 * @brief   Get the power of talk state, on or off
 *
 * @param   none
 *
 * @return  power.talkState
 *********************************************************************/
unsigned char GetPowerTalkState(void)
{
    return power.talkState;
}

/*********************************************************************
 * @fn      GetPowerRingState()
 *
 * @brief   Get the power of ring state, on or off
 *
 * @param   none
 *
 * @return  power.ringState
 *********************************************************************/
unsigned char GetPowerRingState(void)
{
    return power.ringState;
}

/*********************************************************************
 * @fn      GetPowerBeepState()
 *
 * @brief   Get the power of beep state, on or off
 *
 * @param   none
 *
 * @return  power.beepState
 *********************************************************************/
unsigned char GetPowerBeepState(void)
{
    return power.beepState;
}

/*********************************************************************
 * @fn      GetPowerVideoState()
 *
 * @brief   Get the Video of beep state, on or off
 *
 * @param   none
 *
 * @return  power.videoState
 *********************************************************************/
unsigned char GetPowerVideoState(void)
{
    return power.videoState;
}

/*********************************************************************
 * @fn      GetPowerExtRingState()
 *
 * @brief   Get the Video of beep state, on or off
 *
 * @param   none
 *
 * @return  power.videoState
 *********************************************************************/
unsigned char GetPowerExtRingState(void)
{
    return power.extRingState;
}

