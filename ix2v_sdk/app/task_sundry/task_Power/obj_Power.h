/**
  ******************************************************************************
  * @file    obj_Power.h
  * @author  zeng
  * @version V1.0.0
  * @date    2012.08.15
  * @brief   This file contains the headers of the power obj_Power.
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2012 V-Tec</center></h2>
  ******************************************************************************
*/
#ifndef OBJ_POWER_H
#define OBJ_POWER_H

/*******************************************************************************
                         Define object property
*******************************************************************************/
// 
typedef struct
{
	unsigned char tftState;
	unsigned char talkState;
	unsigned char ringState;
	unsigned char beepState;
	unsigned char videoState;
	unsigned char extRingState;
}PowerStruct;

extern PowerStruct power;

/*******************************************************************************
                      Declare object funtion - public
*******************************************************************************/
unsigned char GetPowerTftState(void);
unsigned char GetPowerTalkState(void);
unsigned char GetPowerRingState(void);
unsigned char GetPowerBeepState(void);
unsigned char GetPowerVideoState(void);
unsigned char GetPowerExtRingState(void);


/*******************************************************************************
                      Declare object funtion - private
*******************************************************************************/



#endif


