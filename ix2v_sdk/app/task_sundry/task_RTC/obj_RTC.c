/**
  ******************************************************************************
  * @file    obj_RTC.c
  * @author  zeng
  * @version V1.0.0
  * @date    2012.06.01
  * @brief   This file contains the function of obj_RTC.
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2012 V-Tec</center></h2>
  ******************************************************************************
  */ 

#include "obj_RTC.h"
#include "task_RTC.h"
#include "RTOS.h"
#include "OSTIME.h"
#include "task_survey.h"	
///

unsigned short rtcSyncTrigger;
unsigned char rtcHaveSet;

unsigned char rtcHaveSync;

/*******************************************************************************
 * @fn      RtcInit()
 *
 * @brief   init rtc block
 *
 * @param   none
 *
 * @return  none
 ******************************************************************************/
void RtcInit(void)
{        
    
    /*
    RTC_DateTypeDef date;
    RTC_TimeTypeDef time;
    date.RTC_Year = 0x13;
    date.RTC_Month = 0x03;
    date.RTC_Date = 0x26;
    RtcSetDate(date);
    time.RTC_Hours = 0x14;
    time.RTC_Minutes = 0x40;
    time.RTC_Seconds = 0x00;
    RtcSetTime(time);
    */
    hd_RtcInit();
}

/*******************************************************************************
 * @fn      BinToBcd()
 *
 * @brief   turn BIN format to BCD format
 *
 * @param   iBin - bin val
 *
 * @return  bcd - BCD code
 ******************************************************************************/
unsigned char BinToBcd(unsigned char iBin )
{
    unsigned char bcd;
    bcd = (iBin/10)<<4;
    bcd |= (iBin%10);
    return bcd;
}

/*******************************************************************************
 * @fn      RtcSetDate()
 *
 * @brief   set date to rtc
 *
 * @param   iDate
 *
 * @return  none
 ******************************************************************************/
void RtcSetDate(RTC_DateTypeDef iDate)
{
    hd_RtcSetDate(iDate);
}

/*******************************************************************************
 * @fn      RtcGetDate()
 *
 * @brief   get date from rtc 
 *
 * @param   none
 *
 * @return  date in RTC_DateTypeDef struct
 ******************************************************************************/
RTC_DateTypeDef RtcGetDate(void)
{
    return hd_RtcGetDate();
}

/*******************************************************************************
 * @fn      RtcSetTime()
 *
 * @brief   set time to rtc
 *
 * @param   iDate - SetTime init data, in BCD,24 hours format
 *
 * @return  none
 ******************************************************************************/
void RtcSetTime(RTC_TimeTypeDef iTime)
{
    hd_RtcSetTime(iTime);
}

/*******************************************************************************
 * @fn      RtcGetTime()
 *
 * @brief   get time from rtc
 *
 *
 * @return  time, in BCD,24 hours format
 ******************************************************************************/
RTC_TimeTypeDef RtcGetTime(void)
{
    return hd_RtcGetTime();
}

/*******************************************************************************
 * @fn      RtcSyncSend()
 *
 * @brief   Send the rtc sync data
 *
 * @param   none
 *
 * @return  none
 ******************************************************************************/
void RtcSyncSend(void)
{
    RtcTypeDef rtcData;
    rtcData.date = RtcGetDate();
    rtcData.time = RtcGetTime();

    //go_server	//zxj_sc6
    //API_Event_GoServer_ToWrite(COBID6_RTC_SERVICE, 8, (uint8 *)(&rtcData));
}


/*******************************************************************************
 * @fn      RtcSyncReceive()
 *
 * @brief   Receive the rtc sync data
 *
 * @param   iRecDate - data receive from sync
 *
 * @return  none
 ******************************************************************************/
void RtcSyncReceive(RtcTypeDef iRecDate)
{
    RtcSetDate(iRecDate.date);
    RtcSetTime(iRecDate.time);
    rtcHaveSet = 1;
}

/*******************************************************************************
 * @fn      RtcSyncRequest()
 *
 * @brief   Send rtc sync request to rtc server
 *
 * @param   none
 *
 * @return  none
 ******************************************************************************/
void RtcSyncRequest(void)
{
    unsigned char dataTemp = 1;
    //API_Event_GoServer_ToWrite(COBID7_RTC_CLIENT, 1, &dataTemp);
}
/*******************************************************************************
 * @fn      RtcTimerEventProcess()
 *
 * @brief   rtc timer timeout event handler, to request sync, or to send sync
 *
 * @param   none
 *
 * @return  none
 ******************************************************************************/
void RtcTimerEventProcess(void)
{
    unsigned char rtcServer;
    
    rtcSyncTrigger++;
    // rtc hava not sync yet
    if( !rtcHaveSync )
    {
        //API_Event_IoServer_InnerRead_All( RTC_SERVER_ENABLE, (unsigned char*)&(rtcServer) );
        // if rtc server, send sync brd
        if( rtcServer )
        {
            RtcSyncSend();
        }
        rtcHaveSync = 1;
    }
    
    if( rtcSyncTrigger == (RTC_SYNC_CYC*1195) )
    {
        //cao_20151118_willadd API_MEMO_GET_RTC();
    }
    
    // time is up to RTC_SYNC_CYC cycle, send sync if it's rtc server
    if( rtcSyncTrigger >= (RTC_SYNC_CYC*1200) )
    {
        rtcSyncTrigger = 0;
        //API_Event_IoServer_InnerRead_All( RTC_SERVER_ENABLE, (unsigned char*)&(rtcServer) );
        if( rtcServer )
        {
            RtcSyncSend();
  //          API_Survey_MsgInform(INFORM_RTC_SET);
        }
    }
    
}

/*******************************************************************************
 * @fn      GetRtcState()
 *
 * @brief   get the rtc state
 *
 * @param   none
 *
 * @return  0/ day, 1/ night, ff/ not sync
 ******************************************************************************/
unsigned char GetRtcState(unsigned char msg_id)
{
    if( !rtcHaveSet )
    {
        return 0xff; // not sync
    }
    else
    {
        RtcTypeDef rtcData;
        int dayNightLine;
		char temp[20];
        //API_Event_IoServer_InnerRead_All( DAY_NIGHIT_DELIMIT, temp);
		dayNightLine = atoi(temp);
        rtcData.time = RtcGetTime();
        if( (rtcData.time.RTC_Hours >= BinToBcd((dayNightLine>>8)&0xFF)) && (rtcData.time.RTC_Hours < BinToBcd(dayNightLine&0xFF)) )
        {
            return 0; // day
        }
        else
        {
            return 1; // night
        }
    }
}


