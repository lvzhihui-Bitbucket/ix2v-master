#ifndef _obj_IXGMainCall_process_h
#define _obj_IXGMainCall_process_h


void IXGMainCall_Wait_Process(CALLSERVER_STRU *Msg_CallServer);
void IXGMainCall_Invite_Process(CALLSERVER_STRU *Msg_CallServer);
void IXGMainCall_Ring_Process(CALLSERVER_STRU *Msg_CallServer);
void IXGMainCall_Ack_Process(CALLSERVER_STRU *Msg_CallServer);
void IXGMainCall_SourceBye_Process(CALLSERVER_STRU *Msg_CallServer);
void IXGMainCall_TargetBye_Process(CALLSERVER_STRU *Msg_CallServer);
void IXGMainCall_Transfer_Process(CALLSERVER_STRU *Msg_CallServer);

void IXGMainCall_MenuDisplay_ToInvite(void);
void IXGMainCall_MenuDisplay_ToRing(void);
void IXGMainCall_MenuDisplay_ToAck(void);
void IXGMainCall_MenuDisplay_ToBye(void);
void IXGMainCall_MenuDisplay_ToWait(void);
void IXGMainCall_MenuDisplay_ToTransfer(void);


#endif
