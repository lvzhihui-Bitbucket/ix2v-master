#if 1	//IX2_TEST
#include <stdio.h>
#include "task_CallServer.h"
#include "obj_BeCalled_State.h"
#include "obj_Caller_State.h"
//#include "../task_DtBeCalled/task_DtBeCalled.h"
//#include "../task_DtCaller/task_DtCaller.h"
#include "task_IpBeCalled.h"
#include "task_IpCaller.h"

//#include "task_Ring.h"
#include "task_Power.h"
#include "task_Beeper.h"
#include "task_Led.h"
//#include "obj_menu_data.h"
//#include "task_VideoMenu.h"
//#include "obj_TableProcess.h"
//#include "obj_call_record.h"
//#include "obj_memo.h"

#include "obj_IxCallScene2_Active_process.h"
#include "elog_forcall.h"
#include "obj_Talk_Servi.h"
#include "task_Event.h"


//extern CALL_RECORD_DAT_T call_record_temp;
//extern int call_record_flag;


void IxCallScene2_Active_Wait_Process(CALLSERVER_STRU *Msg_CallServer)
{
	int i;
	char paraString[500]={0};
	cJSON *call_para;
	cJSON *call_target;
	cJSON *tar_list;
	switch(Msg_CallServer->call_type)
	{
		case IxCallScene2_Active:						//INTERCOM:  IX-MASTER -> IX-ANOTHER 
			switch(Msg_CallServer->msg_type)
			{
				case CallServer_Msg_Invite:		//czn_20190527
					
					//czn_20190107_s
					#if 0
					if(API_Business_Request(Business_State_IntercomCall) == 0)
					{
						//usleep(200000);
						//API_Beep(BEEP_TYPE_DI3);
						//popDisplayLastMenu();
						CallServer_Business_Respones(Msg_CallServer->msg_source_id,CallServer_Msg_Invite,1);
						return;
					}
					#endif
					//czn_20190107_e
					if(Get_IpCaller_State() != 0)
					{
						API_IpCaller_ForceClose();
					}
					call_para=cJSON_Parse(Msg_CallServer->json_para);
					if(call_para==NULL)
					{
						log_w("IxMainCall:para parse err");
						CallServer_Business_Respones(Msg_CallServer->msg_source_id,CallServer_Msg_Invite,1);
						//API_vbu_CallVideoOff();
						return;
					}
					call_target=cJSON_GetObjectItemCaseSensitive(call_para, CallPara_Target);
					if(call_target==NULL)
					{
						log_w("IxMainCall:have no target");
						cJSON_Delete(call_para);
						CallServer_Business_Respones(Msg_CallServer->msg_source_id,CallServer_Msg_Invite,1);
						//API_vbu_CallVideoOff();
						return;
					}
					if(CallServer_Run.target_array!=NULL)
					{
						cJSON_Delete(CallServer_Run.target_array);
						
					}
					CallServer_Run.target_array=cJSON_CreateObject();
					if(CallServer_Run.target_array==NULL)
					{
						//cJSON_Delete(CallServer_Run.target_array);
						log_w("DxMainCall:create target array err");
						cJSON_Delete(call_para);
						CallServer_Business_Respones(Msg_CallServer->msg_source_id,CallServer_Msg_Invite,1);
						//API_vbu_CallVideoOff();
						return;
					}
					CallServer_Run.divert=0;
					CallServer_Run.caller_mask=0;
					if(GetJsonDataPro(call_target,CallTarget_IxDev,&tar_list)==0)
					{
						cJSON_Delete(call_para);
						CallServer_Business_Respones(Msg_CallServer->msg_source_id,CallServer_Msg_Invite,1);
						return;
						
					}
					else
					{
						if(GetJsonDataPro(call_target,CallTarget_Input,paraString)==1)
						{
							cJSON_AddStringToObject(CallServer_Run.target_array,CallTarget_Input,paraString);
						}
						cJSON_AddItemToObject(CallServer_Run.target_array,CallTarget_IxDev,cJSON_Duplicate(tar_list,1));
					}
					
				
					cJSON_Delete(call_para);
					
					//CallServer_Business_Respones(Msg_CallServer->msg_source_id,CallServer_Msg_Invite,0);
					usleep(1000);
	
					CallServer_Run.state = CallServer_Invite;
					CallServer_Run.call_type = Msg_CallServer->call_type;
					CallServer_Run.with_local_menu = 1;
					
					CallServer_Run.timer = time(NULL);
					CallServer_Run.caller_mask=1;
					API_IpCaller_Invite(IpCaller_IxSys);
					IxCallScene2_Active_MenuDisplay_ToInvite();
					
					
					break;
					
			}
			break;
			
		default:
			break;
	}
		
	
	
}

void IxCallScene2_Active_Invite_Process(CALLSERVER_STRU *Msg_CallServer)
{
	cJSON *call_para;
	cJSON *hook_para;
	//switch(Msg_CallServer->call_type)
	switch(CallServer_Run.call_type)
	{	
		case IxCallScene2_Active:						//INTERCOM: DX_Master -> DT-ANOTHER
			if(CallServer_Run.call_type != IxCallScene2_Active)
			{
				//will_add
			}
			switch(Msg_CallServer->msg_type)
			{	
				case CallServer_Msg_Invite:
					if(time(NULL) - CallServer_Run.timer > 5)
					{
						//CallServer_Run.state = CallServer_Wait;
						//IxCallScene3_MenuDisplay_ToWait();

						API_CallServer_Timeout();
					}
					BEEP_ERROR();
					IxCallScene2_Active_MenuDisplay_ToInviteFail(0);
					CallServer_Business_Respones(Msg_CallServer->msg_source_id,CallServer_Msg_Invite,1);
					break;
					
				case CallServer_Msg_InviteOk:
					CallServer_Run.state = CallServer_Ring;
					CallServer_Run.timer = time(NULL);
					API_IpCaller_Ring(IpCaller_IxSys);
					IxCallScene2_Active_MenuDisplay_ToRing();
					break;	

				case CallServer_Msg_InviteFail:
					#if 0
					if(TargetAddrErr_Process(CallServer_Run.para_type,CallServer_Run.para_length,CallServer_Run.para_buff) ==0)
					{
						//will_add
					}
					#endif
					API_IpCaller_Bye(IpCaller_IxSys);
					CallServer_Run.state = CallServer_Wait;
					BEEP_ERROR();
					//IxCallScene3_MenuDisplay_ToWait();
					IxCallScene2_Active_MenuDisplay_ToInviteFail(1);
					break;

				case CallServer_Msg_RemoteAck:
					call_para=cJSON_Parse(Msg_CallServer->json_para);
					if(call_para!=NULL&&(hook_para=cJSON_GetObjectItem(call_para,CallTarget_IxHook))!=NULL)
					{
						//CallServer_Run.target_hook_dev = Msg_CallServer->target_dev_list[0];
						CallServer_Run.state = CallServer_Ack;
						CallServer_Run.timer = time(NULL);
						cJSON_AddItemToObject(CallServer_Run.target_array,CallTarget_IxHook,cJSON_Duplicate(hook_para,1));
						
						API_IpCaller_Ack(IpCaller_IxSys);
						IxCallScene2_Active_MenuDisplay_ToAck();
					}
					cJSON_Delete(call_para);
					break;

				case CallServer_Msg_LocalBye:
					API_IpCaller_Cancel(IpCaller_IxSys);
					CallServer_Run.state = CallServer_Wait;
					IxCallScene2_Active_MenuDisplay_ToWait();
					break;

				case CallServer_Msg_RemoteBye:
					//CallServer_Run.target_hook_dev= Msg_CallServer->target_dev_list[0];
					API_IpCaller_Bye(IpCaller_IxSys);
					CallServer_Run.state = CallServer_Wait;
					IxCallScene2_Active_MenuDisplay_ToWait();
					break;

				case CallServer_Msg_Timeout:
					//API_IpCaller_ForceClose();
					API_IpCaller_Cancel(IpCaller_IxSys);
					CallServer_Run.state = CallServer_Wait;
					IxCallScene2_Active_MenuDisplay_ToWait();
					break;

				case CallServer_Msg_DtSrDisconnect:
					break;

				case CallServer_Msg_NetCallLinkDisconnect:
					API_IpCaller_ForceClose();
					CallServer_Run.state = CallServer_Wait;
					IxCallScene2_Active_MenuDisplay_ToWait();
					break;

				default:
					break;
			}
			break;
	
		default:
			break;
	}
}

void IxCallScene2_Active_Ring_Process(CALLSERVER_STRU *Msg_CallServer)
{
	cJSON *call_para;
	cJSON *hook_para;
	//switch(Msg_CallServer->call_type)
	switch(CallServer_Run.call_type)
	{	
		case IxCallScene2_Active:						//INTERCOM: DX_Master -> DT-ANOTHER
			if(CallServer_Run.call_type != IxCallScene2_Active)
			{
				//will_add
			}
			switch(Msg_CallServer->msg_type)
			{	
				case CallServer_Msg_Invite:
					if(time(NULL) - CallServer_Run.timer > 5)
					{
						//CallServer_Run.state = CallServer_Wait;
						//IxCallScene3_MenuDisplay_ToWait();

						API_CallServer_Timeout();
					}
					IxCallScene2_Active_MenuDisplay_ToInviteFail(1);
					CallServer_Business_Respones(Msg_CallServer->msg_source_id,CallServer_Msg_Invite,1);
					break;
					
				case CallServer_Msg_RemoteAck:
					call_para=cJSON_Parse(Msg_CallServer->json_para);
					if(call_para!=NULL&&(hook_para=cJSON_GetObjectItem(call_para,CallTarget_IxHook))!=NULL)
					{
						//CallServer_Run.target_hook_dev = Msg_CallServer->target_dev_list[0];
						CallServer_Run.state = CallServer_Ack;
						CallServer_Run.timer = time(NULL);
						cJSON_AddItemToObject(CallServer_Run.target_array,CallTarget_IxHook,cJSON_Duplicate(hook_para,1));
						
						API_IpCaller_Ack(IpCaller_IxSys);
						IxCallScene2_Active_MenuDisplay_ToAck();
					}
					cJSON_Delete(call_para);
					break;

				case CallServer_Msg_LocalBye:
					API_IpCaller_Cancel(IpCaller_IxSys);
					CallServer_Run.state = CallServer_Wait;
					IxCallScene2_Active_MenuDisplay_ToWait();
					break;

				case CallServer_Msg_RemoteBye:
					//CallServer_Run.target_hook_dev= Msg_CallServer->target_dev_list[0];
					API_IpCaller_Bye(IpCaller_IxSys);
					CallServer_Run.state = CallServer_Wait;
					IxCallScene2_Active_MenuDisplay_ToWait();
					break;
					
				case CallServer_Msg_Timeout:
					//API_DtCaller_ForceClose();
					API_IpCaller_Cancel(IpCaller_IxSys);
					CallServer_Run.state = CallServer_Wait;
					IxCallScene2_Active_MenuDisplay_ToWait();
					break;

				case CallServer_Msg_DtSrDisconnect:
					
					break;

				case CallServer_Msg_NetCallLinkDisconnect:
					API_IpCaller_ForceClose();
					CallServer_Run.state = CallServer_Wait;
					IxCallScene2_Active_MenuDisplay_ToWait();
					break;

				default:
					break;
			}
			break;
	
		default:
			break;
	}
	
}

void IxCallScene2_Active_Ack_Process(CALLSERVER_STRU *Msg_CallServer)
{
	//switch(Msg_CallServer->call_type)
	switch(CallServer_Run.call_type)
	{
		case IxCallScene2_Active:						//INTERCOM: DX_Master -> DT-ANOTHER
			if(CallServer_Run.call_type != IxCallScene2_Active)
			{
				//will_add
			}
			switch(Msg_CallServer->msg_type)
			{	
				case CallServer_Msg_Invite:
					if(time(NULL) - CallServer_Run.timer > 5)
					{
						//CallServer_Run.state = CallServer_Wait;
						//IxCallScene3_MenuDisplay_ToWait();

						API_CallServer_Timeout();
					}
					IxCallScene2_Active_MenuDisplay_ToInviteFail(1);
					CallServer_Business_Respones(Msg_CallServer->msg_source_id,CallServer_Msg_Invite,1);
					break;
					
				case CallServer_Msg_LocalBye:
					API_IpCaller_Cancel(IpCaller_IxSys);
					CallServer_Run.state = CallServer_Wait;
					IxCallScene2_Active_MenuDisplay_ToWait();
					break;

				case CallServer_Msg_RemoteBye:
					API_IpCaller_Bye(IpCaller_IxSys);
					CallServer_Run.state = CallServer_Wait;
					IxCallScene2_Active_MenuDisplay_ToWait();
					break;
					
				case CallServer_Msg_Timeout:
					//API_DtCaller_ForceClose();
					API_IpCaller_Cancel(IpCaller_IxSys);
					CallServer_Run.state = CallServer_Wait;
					IxCallScene2_Active_MenuDisplay_ToWait();
					break;

				case CallServer_Msg_DtSrDisconnect:
					break;

				case CallServer_Msg_NetCallLinkDisconnect:
					API_IpCaller_ForceClose();
					CallServer_Run.state = CallServer_Wait;
					IxCallScene2_Active_MenuDisplay_ToWait();
					break;

				default:
					break;
			}
			break;
			
		default:
			break;
	}
	
	
}
void IxCallScene2_Active_SourceBye_Process(CALLSERVER_STRU *Msg_CallServer)
{
	//char detail[LOG_DESC_LEN + 1];
	
	switch(Msg_CallServer->call_type)
	{	
		case IxCallScene2_Active:						//INTERCOM: DX_Master -> DT-ANOTHER
			break;
	
		default:
			break;
	}
	
}

void IxCallScene2_Active_TargetBye_Process(CALLSERVER_STRU *Msg_CallServer)
{
	//char detail[LOG_DESC_LEN + 1];

	switch(Msg_CallServer->call_type)
	{	
		case IxCallScene2_Active:						//INTERCOM: DX_Master -> DT-ANOTHER
			break;
			
		default:
			break;
	}
	
}

void IxCallScene2_Active_Transfer_Process(CALLSERVER_STRU *Msg_CallServer)
{
	//char detail[LOG_DESC_LEN + 1];

	switch(Msg_CallServer->call_type)
	{	
		case IxCallScene2_Active:						//INTERCOM: DX_Master -> DT-ANOTHER
			break;
			
		default:
			break;
	}
	
}


void IxCallScene2_Active_MenuDisplay_ToInvite(void)
{
	log_d("DxMainCall:ToInvite");
	//API_vbu_CallVideoOn();
	API_PublicInfo_Write(PB_CALL_INFO,GetCallserverStatePb());
	API_Event_By_Name(EventCallState);
}
void IxCallScene2_Active_MenuDisplay_ToInviteFail(int fail_type)
{
	log_i("IxMainCall:ToInviteFail");
	API_PublicInfo_Write(PB_CALL_INFO,GetCallserverStatePb());
	API_Event_By_Name(EventCallState);
	Send_CallEvent("Calling_Error", "IxMainCall:ToInviteFail");
}


void IxCallScene2_Active_MenuDisplay_ToRing(void)
{
	log_d("IxMainCall:ToRing");
	API_PublicInfo_Write(PB_CALL_INFO,GetCallserverStatePb());
	API_Event_By_Name(EventCallState);
	Send_CallEvent("Calling_Succ", NULL);
}

void IxCallScene2_Active_MenuDisplay_ToAck(void)
{
	cJSON *au_para;
	char ip_addr[20];
	log_i("IxMainCall:ToAck");
	API_PublicInfo_Write(PB_CALL_INFO,GetCallserverStatePb());
	API_Event_By_Name(EventCallState);
	
	OpenAppTalk();
	au_para=cJSON_CreateObject();
	if(au_para!=NULL)
	{
		
		cJSON_AddStringToObject(au_para,TalkType,TalkType_LocalAndUdp);
		GetCallTarIxDevHookIp(ip_addr);
		
		
		cJSON_AddStringToObject(au_para,TargetIP,ip_addr); 
			
		API_TalkService_on(au_para);

		cJSON_Delete(au_para);
	}
}

void IxCallScene2_Active_MenuDisplay_ToBye(void)
{
	log_d("IxMainCall:ToBye");
	API_TalkService_off();
	API_PublicInfo_Write(PB_CALL_INFO,GetCallserverStatePb());
	API_Event_By_Name(EventCallState);
}

void IxCallScene2_Active_MenuDisplay_ToWait(void)
{
	log_i("IxMainCall:Close");	
	API_TalkService_off();
	API_PublicInfo_Write(PB_CALL_INFO,GetCallserverStatePb());
	API_Event_By_Name(EventCallState);
	Send_CallEvent("Calling_Close", NULL);
}

#endif