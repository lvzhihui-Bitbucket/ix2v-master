#include <stdio.h>
#include "task_CallServer.h"
#include "../obj_BeCalled_State.h"
#include "../obj_Caller_State.h"
//#include "../task_DtBeCalled/task_DtBeCalled.h"
//#include "../task_DtCaller/task_DtCaller.h"
#if 0	//IX2_TEST
#include "../task_IpBeCalled/task_IpBeCalled.h"
#include "../task_IpCaller/task_IpCaller.h"

#include "../../task_Sundry/task_Ring/task_Ring.h"
#include "../../task_Sundry/task_Power/task_Power.h"
#include "../../task_Sundry/task_Led/task_Led.h"
#include "../../task_Sundry/task_Ring/task_Ring.h"
#include "../../task_io_server/obj_TableProcess.h"
#include "../../task_io_server/obj_call_record.h"
#include "../../task_io_server/obj_memo.h"
#include "../../task_io_server/task_IoServer.h"
//IX2_TEST #include "../../task_VideoMenu/task_VideoMenu.h"

#include "obj_IxCallScene5_process.h"

extern CALL_RECORD_DAT_T call_record_temp;
extern int call_record_flag;

#if 1
void IxCallScene5_Wait_Process(CALLSERVER_STRU *Msg_CallServer)
{
	//char detail[LOG_DESC_LEN + 1];
	unsigned char dtbecalled_type=0,dtcaller_type=0,ipbecalled_type=0,ipcaller_type=0;
	//uint8 slaveMaster;
	//API_Event_IoServer_InnerRead_All(MASTER_SLAVE_SET, (uint8*)&slaveMaster);

	switch(Msg_CallServer->call_type)
	{
		case IxCallScene5:					//DOORCALL: DS-> DX-MASTER -> DX-SLAVE 
			
			switch(Msg_CallServer->msg_type)
			{
				case CallServer_Msg_Invite: 
					//czn_20190107_s
					if(API_Business_Request(Business_State_SipCallTest) == 0)
					{
						//usleep(200000);
						//API_Beep(BEEP_TYPE_DI3);
						//popDisplayLastMenu();
						CallServer_Business_Respones(Msg_CallServer->msg_source_id,CallServer_Msg_Invite,1);
						return;
					}
					//czn_20190107_e
					if(Get_IpCaller_State() != 0)
					{
						API_IpCaller_ForceClose();
					}
					#if 0
					Load_CallRule_Para();
					
					#ifdef DivertErrRecoverNormal
					if((Get_SipAccount_State() == 0 && Get_SipReg_ErrCode() != 401)&&(CallServer_Run.call_rule == CallRule_TransferNoAck||CallServer_Run.call_rule == CallRule_TransferIm))
					{
						CallServer_Run.call_rule = CallRule_Normal;
					}
					#endif	
					#endif
					
					{
						//dtbecalled_type = DtBeCalled_MainCall;
						ipcaller_type = IpCaller_TransferTest;
						
						CallServer_Run.state = CallServer_Invite;
						CallServer_Run.call_type = Msg_CallServer->call_type;
						CallServer_Run.with_local_menu = 1;
						//CallServer_Run.s_addr = Msg_CallServer->partner_addr;
						//will_add CallServer_Run.t_addr = local_addr;
						CallServer_Run.timer = time(NULL);
						//API_DtBeCalled_Invite(dtbecalled_type, &CallServer_Run.s_addr);
						API_IpCaller_Invite(ipcaller_type, NULL);
						CallServer_Run.rule_act = 1;
						CallServer_Run.state = CallServer_Transfer;
						IxCallScene5_MenuDisplay_ToTransfer();
						CallServer_Business_Respones(Msg_CallServer->msg_source_id,CallServer_Msg_Invite,0);
					}
					break;

				default:
					break;
			}
			break;
	
		default:
			break;
	}
		
	
	
}

void IxCallScene5_Invite_Process(CALLSERVER_STRU *Msg_CallServer)
{
	unsigned char dtbecalled_type=0,dtcaller_type=0,ipbecalled_type=0,ipcaller_type=0;

	switch(Msg_CallServer->call_type)
	{
		case IxCallScene5:					//DOORCALL: DS-> DX-MASTER -> DX-SLAVE 
			switch(Msg_CallServer->msg_type)
			{
				case CallServer_Msg_Invite:
					break;
					
				case CallServer_Msg_RemoteRing:
					break;	

				case CallServer_Msg_LocalAck:
					break;	

				case CallServer_Msg_RemoteAck:
					break;

				case CallServer_Msg_LocalBye:
					break;

				case CallServer_Msg_RemoteBye:
					break;

				case CallServer_Msg_Timeout:
					break;

				case CallServer_Msg_DtSrDisconnect:
					break;

				case CallServer_Msg_NetCallLinkDisconnect:
					break;

				case CallServer_Msg_LocalUnlock1:
					break;

				case CallServer_Msg_LocalUnlock2:
					break;

				case CallServer_Msg_RemoteUnlock1:
					break;

				case CallServer_Msg_RemoteUnlock2:
					break;

				default:
					break;
			}
			break;
			
		default:
			break;
	}
}

void IxCallScene5_Ring_Process(CALLSERVER_STRU *Msg_CallServer)
{
	unsigned char dtbecalled_type=0,dtcaller_type=0,ipbecalled_type=0,ipcaller_type=0;
	uint8 slaveMaster;
	
	switch(Msg_CallServer->call_type)
	{
		case IxCallScene5:					//DOORCALL: DS-> DX-MASTER -> DX-SLAVE 
			//dtbecalled_type = DtBeCalled_MainCall;
			
			{
				ipcaller_type = IpCaller_TransferTest;
			}
			if(CallServer_Run.call_type != IxCallScene5)	//czn_20181030
			{
				return;
			}
			switch(Msg_CallServer->msg_type)
			{
				case CallServer_Msg_RingNoAck:
					
					CallServer_Run.state = CallServer_TargetBye;
					//CallServer_Run.t_addr = Msg_CallServer->partner_addr;
					API_IpCaller_Cancel(ipcaller_type);
					CallServer_Run.state = CallServer_Wait;
					IxCallScene5_MenuDisplay_ToWait();
					break;
					
				case CallServer_Msg_RemoteRing:
					break;	
	

				case CallServer_Msg_RemoteAck:
					CallServer_Run.state = CallServer_Ack;
					CallServer_Run.with_local_menu = 0;
					//CallServer_Run.t_addr = Msg_CallServer->partner_addr;
					CallServer_Run.timer = time(NULL);
					
					//API_DtBeCalled_Ack(dtbecalled_type);
					API_IpCaller_Ack(ipcaller_type,NULL);
					IxCallScene5_MenuDisplay_ToAck();
					break;


				case CallServer_Msg_LocalBye:
					CallServer_Run.state = CallServer_TargetBye;
					//CallServer_Run.t_addr = Msg_CallServer->partner_addr;
					//API_DtBeCalled_Bye(dtbecalled_type);
					API_IpCaller_Cancel(ipcaller_type);
					CallServer_Run.state = CallServer_Wait;
					IxCallScene5_MenuDisplay_ToWait();
					break;

				case CallServer_Msg_RemoteBye:
					#if 0
					if(CallServer_Run.call_rule == CallRule_TransferIm)
					{
						API_DtBeCalled_Cancel(dtbecalled_type);
						API_IpCaller_Cancel(ipcaller_type);
						CallServer_Run.state = CallServer_Wait;
						DxCallScene1_MenuDisplay_ToWait();
						usleep(1000000);
						
						CallServer_Run.call_rule = CallRule_Normal;
						CallServer_Run.rule_act = 0;
						dtbecalled_type = DtBeCalled_MainCall;
						ipcaller_type = IpCaller_MainCall;
			
						CallServer_Run.state = CallServer_Invite;
						//CallServer_Run.call_type = Msg_CallServer->call_type;
						CallServer_Run.with_local_menu = 1;
						//CallServer_Run.s_addr = Msg_CallServer->partner_addr;
						//will_add CallServer_Run.t_addr = local_addr;
						CallServer_Run.timer = time(NULL);
						API_DtBeCalled_Invite(dtbecalled_type, &CallServer_Run.s_addr);
						
						API_IpCaller_Invite(ipcaller_type, NULL);
						
						CallServer_Run.state = CallServer_Ring;
						DxCallScene1_MenuDisplay_ToRing();
					}
					else
					{
						if(Msg_CallServer->partner_addr.ip == GetLocalIp())
						{
							CallServer_Run.state = CallServer_SourceBye;
							API_DtBeCalled_Cancel(dtbecalled_type);
							API_IpCaller_Cancel(ipcaller_type);
							CallServer_Run.state = CallServer_Wait;
						}
						else
						{
							if(Msg_CallServer->partner_addr.rt == LIPHONE_RT && CallServer_Run.rule_act == 0)
								return;
							
							CallServer_Run.state = CallServer_TargetBye;
							API_DtBeCalled_Bye(dtbecalled_type);
							API_IpCaller_Cancel(ipcaller_type);
							CallServer_Run.state = CallServer_Wait;
						}
						DxCallScene1_MenuDisplay_ToWait();
					}
					#else
					
						
						CallServer_Run.state = CallServer_TargetBye;
						//API_DtBeCalled_Bye(dtbecalled_type);
						API_IpCaller_Cancel(ipcaller_type);
						CallServer_Run.state = CallServer_Wait;
					
					IxCallScene5_MenuDisplay_ToWait();
					#endif
					break;

				case CallServer_Msg_Timeout:
					//API_DtBeCalled_ForceClose();
					//API_IpCaller_ForceClose();
					//API_DtBeCalled_Bye(dtbecalled_type);
					API_IpCaller_Cancel(ipcaller_type);
					CallServer_Run.state = CallServer_Wait;
					IxCallScene5_MenuDisplay_ToWait();
					break;

				
				case CallServer_Msg_NetCallLinkDisconnect:
					//API_DtBeCalled_Bye(dtbecalled_type);
					API_IpCaller_ForceClose();
					CallServer_Run.state = CallServer_Wait;
					IxCallScene5_MenuDisplay_ToWait();
					break;

					
				default:
					break;
			}
			break;
				
		default:
			break;
	}
	
}

void IxCallScene5_Ack_Process(CALLSERVER_STRU *Msg_CallServer)
{
	unsigned char dtbecalled_type=0,dtcaller_type=0,ipbecalled_type=0,ipcaller_type=0;
	
	switch(Msg_CallServer->call_type)
	{
		case IxCallScene5:					//DOORCALL: DS-> DX-MASTER -> DX-SLAVE 
			
				ipcaller_type = IpCaller_TransferTest;
			
			if(CallServer_Run.call_type != IxCallScene5)	//czn_20181030
			{
				return;
			}
			switch(Msg_CallServer->msg_type)
			{
					
				case CallServer_Msg_RemoteRing:
					break;	

				case CallServer_Msg_LocalAck:
					break;	

				case CallServer_Msg_RemoteAck:
					break;

				case CallServer_Msg_LocalBye:
					
						CallServer_Run.state = CallServer_TargetBye;
						//CallServer_Run.t_addr = Msg_CallServer->partner_addr;
						//API_DtBeCalled_Bye(dtbecalled_type);
						API_IpCaller_Bye(ipcaller_type,NULL);
						CallServer_Run.state = CallServer_Wait;
					
					IxCallScene5_MenuDisplay_ToWait();
					break;

				case CallServer_Msg_RemoteBye:
					
					
						CallServer_Run.state = CallServer_TargetBye;
						//API_DtBeCalled_Bye(dtbecalled_type);
						API_IpCaller_Bye(ipcaller_type,NULL);
						CallServer_Run.state = CallServer_Wait;
		
					IxCallScene5_MenuDisplay_ToWait();
					break;

				case CallServer_Msg_Timeout:
					//API_DtBeCalled_ForceClose();
					//API_IpCaller_ForceClose();
					//API_DtBeCalled_Bye(dtbecalled_type);
					API_IpCaller_Cancel(ipcaller_type);
					CallServer_Run.state = CallServer_Wait;
					IxCallScene5_MenuDisplay_ToWait();
					break;

			
				case CallServer_Msg_NetCallLinkDisconnect:
					//API_DtBeCalled_Bye(dtbecalled_type);
					API_IpCaller_ForceClose();
					CallServer_Run.state = CallServer_Wait;
					IxCallScene5_MenuDisplay_ToWait();
					break;

				case CallServer_Msg_Redail:
					//if(CallServer_Run.rule_act == 1)
					{
						API_IpCaller_Redail(ipcaller_type);
						CallServer_Run.state = CallServer_Transfer;
						CallServer_Run.timer = time(NULL); 
					}
					break;
				default:
					break;
			}
			break;
			
		default:
			break;
	}
	
	
}
void IxCallScene5_SourceBye_Process(CALLSERVER_STRU *Msg_CallServer)
{
	//char detail[LOG_DESC_LEN + 1];
	
	switch(Msg_CallServer->call_type)
	{
		case IxCallScene5:					//DOORCALL: DS-> DX-MASTER -> DX-SLAVE 
			switch(Msg_CallServer->msg_type)
			{
				case CallServer_Msg_Invite:
					break;
					
				case CallServer_Msg_RemoteRing:
					break;	

				case CallServer_Msg_LocalAck:
					break;	

				case CallServer_Msg_RemoteAck:
					break;

				case CallServer_Msg_LocalBye:
					break;

				case CallServer_Msg_RemoteBye:
					break;

				case CallServer_Msg_Timeout:
					break;

				case CallServer_Msg_DtSrDisconnect:
					break;

				case CallServer_Msg_NetCallLinkDisconnect:
					break;

				case CallServer_Msg_LocalUnlock1:
					break;

				case CallServer_Msg_LocalUnlock2:
					break;

				case CallServer_Msg_RemoteUnlock1:
					break;

				case CallServer_Msg_RemoteUnlock2:
					break;

				default:
					break;
			}
			break;
	
		default:
			break;
	}
	
}

void IxCallScene5_TargetBye_Process(CALLSERVER_STRU *Msg_CallServer)
{
	//char detail[LOG_DESC_LEN + 1];

	switch(Msg_CallServer->call_type)
	{
		case IxCallScene5:					//DOORCALL: DS-> DX-MASTER -> DX-SLAVE 
			switch(Msg_CallServer->msg_type)
			{
				case CallServer_Msg_Invite:
					break;
					
				case CallServer_Msg_RemoteRing:
					break;	

				case CallServer_Msg_LocalAck:
					break;	

				case CallServer_Msg_RemoteAck:
					break;

				case CallServer_Msg_LocalBye:
					break;

				case CallServer_Msg_RemoteBye:
					break;

				case CallServer_Msg_Timeout:
					break;

				case CallServer_Msg_DtSrDisconnect:
					break;

				case CallServer_Msg_NetCallLinkDisconnect:
					break;

				case CallServer_Msg_LocalUnlock1:
					break;

				case CallServer_Msg_LocalUnlock2:
					break;

				case CallServer_Msg_RemoteUnlock1:
					break;

				case CallServer_Msg_RemoteUnlock2:
					break;

				default:
					break;
			}
			break;
			
		default:
			break;
	}
	
}

void IxCallScene5_Transfer_Process(CALLSERVER_STRU *Msg_CallServer)
{
	unsigned char dtbecalled_type=0,dtcaller_type=0,ipbecalled_type=0,ipcaller_type=0;

	switch(Msg_CallServer->call_type)
	{
		case IxCallScene5:					//DOORCALL: DS-> DX-MASTER -> DX-SLAVE 
			//dtbecalled_type = DtBeCalled_MainCall;
			ipcaller_type = IpCaller_TransferTest;
			if(CallServer_Run.call_type != IxCallScene5)	//czn_20181030
			{
				return;
			}
			switch(Msg_CallServer->msg_type)
			{
				case CallServer_Msg_Invite:
					if(time(NULL) - CallServer_Run.timer > 5)
					{
						
						{
							CallServer_Run.state = CallServer_Wait;
							IxCallScene5_MenuDisplay_ToWait();
						}
						
					}
					CallServer_Business_Respones(Msg_CallServer->msg_source_id,CallServer_Msg_Invite,0);
					break;
					
				case CallServer_Msg_RingNoAck:
					CallServer_Run.state = CallServer_TargetBye;
					//CallServer_Run.t_addr = Msg_CallServer->partner_addr;
					//API_DtBeCalled_Bye(dtbecalled_type);
					API_IpCaller_Cancel(ipcaller_type);
					CallServer_Run.state = CallServer_Wait;
					IxCallScene5_MenuDisplay_ToWait();
					break;
					
				case CallServer_Msg_InviteOk:
					CallServer_Run.state = CallServer_Ring;
					CallServer_Run.with_local_menu = 0;
					CallServer_Run.timer = time(NULL);
					
					API_IpCaller_Ring(ipcaller_type,NULL);
					IxCallScene5_MenuDisplay_ToRing();
					break;
					
				case CallServer_Msg_InviteFail:
					//#ifdef DivertErrRecoverNormal
					
					{
						//API_DtBeCalled_Bye(dtbecalled_type);
						API_IpCaller_Cancel(ipcaller_type);
						CallServer_Run.state = CallServer_Wait;
						IxCallScene5_MenuDisplay_ToWait();
					}
					
					break;

				case CallServer_Msg_RemoteAck:
					//if(Msg_CallServer->partner_addr.rt == LIPHONE_RT)
					{
						CallServer_Run.state = CallServer_Ack;
						CallServer_Run.with_local_menu = 0;
						//CallServer_Run.t_addr = Msg_CallServer->partner_addr;
						CallServer_Run.timer = time(NULL);
						
						//API_DtBeCalled_Ack(dtbecalled_type);
						API_IpCaller_Ack(ipcaller_type,NULL);
						IxCallScene5_MenuDisplay_ToAck();
					}
					break;

				case CallServer_Msg_DtSlaveAck:
					
					break;

				case CallServer_Msg_LocalBye:
					
					break;

				case CallServer_Msg_RemoteBye:
					#if 0	//czn_20170605
					if(Msg_CallServer->partner_addr.ip == GetLocalIp())
					{
						CallServer_Run.state = CallServer_SourceBye;
						API_DtBeCalled_Cancel(dtbecalled_type);
						API_IpCaller_Cancel(ipcaller_type);
						CallServer_Run.state = CallServer_Wait;
					}
					else
					#endif
					#if 0
					if(CallServer_Run.call_rule == CallRule_TransferIm)
					{
						API_DtBeCalled_Cancel(dtbecalled_type);
						API_IpCaller_Cancel(ipcaller_type);
						CallServer_Run.state = CallServer_Wait;
						DxCallScene1_MenuDisplay_ToWait();
						usleep(1000000);
						
						CallServer_Run.call_rule = CallRule_Normal;
						CallServer_Run.rule_act = 0;
						dtbecalled_type = DtBeCalled_MainCall;
						ipcaller_type = IpCaller_MainCall;
			
						CallServer_Run.state = CallServer_Invite;
						//CallServer_Run.call_type = Msg_CallServer->call_type;
						CallServer_Run.with_local_menu = 1;
						//CallServer_Run.s_addr = Msg_CallServer->partner_addr;
						//will_add CallServer_Run.t_addr = local_addr;
						CallServer_Run.timer = time(NULL);
						API_DtBeCalled_Invite(dtbecalled_type, &CallServer_Run.s_addr);
						
						API_IpCaller_Invite(ipcaller_type, NULL);
						
						CallServer_Run.state = CallServer_Ring;
						DxCallScene1_MenuDisplay_ToRing();
					}
					else
					{
						{
							CallServer_Run.state = CallServer_TargetBye;
							API_DtBeCalled_Bye(dtbecalled_type);
							API_IpCaller_Cancel(ipcaller_type);
							CallServer_Run.state = CallServer_Wait;
						}
						DxCallScene1_MenuDisplay_ToWait();
					}
					#else
					{
						CallServer_Run.state = CallServer_TargetBye;
						//API_DtBeCalled_Bye(dtbecalled_type);
						API_IpCaller_Cancel(ipcaller_type);
						CallServer_Run.state = CallServer_Wait;
					}
					IxCallScene5_MenuDisplay_ToWait();
					#endif
					break;

				case CallServer_Msg_Timeout:
					//API_DtBeCalled_ForceClose();
					API_IpCaller_ForceClose();
					CallServer_Run.state = CallServer_Wait;
					IxCallScene5_MenuDisplay_ToWait();
					break;

				case CallServer_Msg_DtSrDisconnect:
					//API_DtBeCalled_ForceClose();
					//API_IpCaller_ForceClose();
					API_IpCaller_Cancel(ipcaller_type);		//czn_20171030
					CallServer_Run.state = CallServer_Wait;
					IxCallScene5_MenuDisplay_ToWait();
					break;

				case CallServer_Msg_NetCallLinkDisconnect:
					//API_DtBeCalled_Bye(dtbecalled_type);
					API_IpCaller_ForceClose();
					CallServer_Run.state = CallServer_Wait;
					IxCallScene5_MenuDisplay_ToWait();
					break;
				#if 0
				case CallServer_Msg_LocalUnlock1:
					CallServer_Run.with_local_menu |= 0x80;
					API_DtBeCalled_Unlock1(dtbecalled_type);
					
					//API_Beep(BEEP_TYPE_DL1);
					API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_BeCalledUnlock1);
					break;

				case CallServer_Msg_LocalUnlock2:
					CallServer_Run.with_local_menu |= 0x80;
					API_DtBeCalled_Unlock2(dtbecalled_type);

					//API_Beep(BEEP_TYPE_DL1);
					API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_BeCalledUnlock2);
					break;
				
				
				case CallServer_Msg_RemoteUnlock1:
					CallServer_Run.with_local_menu &= ~0x80;
					API_DtBeCalled_Unlock1(dtbecalled_type);
					API_IpCaller_Unlock1(ipcaller_type,&Msg_CallServer->partner_addr);
					break;

				case CallServer_Msg_RemoteUnlock2:
					CallServer_Run.with_local_menu &= ~0x80;
					API_DtBeCalled_Unlock2(dtbecalled_type);
					API_IpCaller_Unlock2(ipcaller_type,&Msg_CallServer->partner_addr);
					break;
				#endif
				default:
					break;
			}
			break;
			
		default:
			break;
	}
	
}


void IxCallScene5_MenuDisplay_ToInvite(void)
{
	switch(CallServer_Run.call_type)
	{
		case IxCallScene5:					//DOORCALL: DS-> DX-MASTER -> DX-SLAVE 
			break;
	
		default:
			break;
	}
}


void IxCallScene5_MenuDisplay_ToRing(void)
{
	char disp_name[21] = {0};
	char tempData[42];
	
	switch(CallServer_Run.call_type)
	{
		case IxCallScene5:					//DOORCALL: DS-> DX-MASTER -> DX-SLAVE 
			//if(CallServer_Run.rule_act == 1)
			{
				//API_VideoTurnOff_KeepPower();		//czn_20180514
				//break;
			}
			API_LedDisplay_CallRing();
			break;
	
		default:
			break;
	}
		
	
}

void IxCallScene5_MenuDisplay_ToAck(void)
{
	switch(CallServer_Run.call_type)
	{
		case IxCallScene5:					//DOORCALL: DS-> DX-MASTER -> DX-SLAVE 
			//if(CallServer_Run.rule_act == 1)
			{
				usleep(1000000);
				//API_POWER_VIDEO_ON();
				//API_POWER_UDP_TALK_ON();
				//call_record_temp.property	= NORMAL;
				//break;
				//API_POWER_EXT_RING_OFF();		//czn_20170809
				API_LedDisplay_CallTalk();
				API_TalkOn();//API_POWER_TALK_ON();
				API_POWER_UDP_TALK_ON();
			}
			
			break;
	
		default:
			break;
	}
	
}

void IxCallScene5_MenuDisplay_ToBye(void)
{
	switch(CallServer_Run.call_type)
	{
		case IxCallScene5:					//DOORCALL: DS-> DX-MASTER -> DX-SLAVE 
			//czn_20190107_s
			API_Business_Close(Business_State_SipCallTest);	
			//czn_20190107_e
			API_LedDisplay_CallClose();
			API_TalkOff();
			API_POWER_TALK_OFF();
			//API_POWER_VIDEO_OFF();
			API_RingStop();
			API_POWER_EXT_RING_OFF();		//czn_20170809
			//API_VideoTurnOff();
			API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_StopOneSipCallTest);
			//usleep(500000);
			//API_VideoTurnOff();

			//Monitor_Camera_Close(DS1_ADDRESS);
			break;
			
		default:
			break;
	}
	
}

void IxCallScene5_MenuDisplay_ToWait(void)
{
	switch(CallServer_Run.call_type)
	{
		case IxCallScene5:					//DOORCALL: DS-> DX-MASTER -> DX-SLAVE 
			//czn_20190107_s
			API_Business_Close(Business_State_SipCallTest);	
			//czn_20190107_e
			API_LedDisplay_CallClose();
			API_TalkOff();
			API_POWER_TALK_OFF();
			//API_POWER_VIDEO_OFF();
			API_RingStop();
			API_POWER_EXT_RING_OFF();		//czn_20170809
			//API_VideoTurnOff();
			API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_StopOneSipCallTest);
			//usleep(500000);
			//API_VideoTurnOff();
			//Monitor_Camera_Close(DS1_ADDRESS);
			break;
	
		default:
			break;
	}
}

void IxCallScene5_MenuDisplay_ToTransfer(void)
{	
	char disp_name[21] = {0};
	
	switch(CallServer_Run.call_type)
	{
		case IxCallScene5:	
			
			#if 1
			API_LedDisplay_CallDivert();
			API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_DivertTestOn);
			//API_VideoTurnOn();
			API_RingStop();
			API_POWER_EXT_RING_OFF();		//czn_20170809

			//Monitor_Camera_Open(DS1_ADDRESS);
			#endif
			break;

		default:
			break;
	}
}
#endif
#endif