#include <stdio.h>
#include "task_CallServer.h"
#include "obj_BeCalled_State.h"
#include "obj_Caller_State.h"
//#include "../task_DtBeCalled/task_DtBeCalled.h"
//#include "../task_DtCaller/task_DtCaller.h"
//#include "task_IpBeCalled.h"
//#include "task_IpCaller.h"
#include "task_DtCaller.h"
#include "task_SipCaller.h"
#include "elog_forcall.h"
#include "task_Event.h"
#include "define_Command.h"
#include "task_StackAI.h"
#include "task_Event.h"
#include "obj_Talk_Servi.h"
#include "obj_UnitSR.h"
#if 0
#include "task_VoicePrompt.h"
#include "task_Ring.h"
#include "task_Power.h"
#include "task_Led.h"
#include "task_Ring.h"
#include "task_Beeper.h"
#include "obj_TableProcess.h"
#include "obj_call_record.h"
#include "obj_memo.h"
#include "task_IoServer.h"
#include "task_VideoMenu.h"
#include "obj_menu_data.h"

#include 	"obj_DxMainCall_process.h"
#include 	"MENU_010_SipConfig.h"
#include 	"obj_VideoProxySetting.h"
#include	"obj_CallTransferPara.h"

extern CALL_RECORD_DAT_T call_record_temp;
extern int call_record_flag;
CallTransferRunningParaStru_t TargetCallTransferRunningPara;

Global_Addr_Stru GetLocal_IpGlobalAddr(void);

// lzh_20180816_s
int API_CameraCtrl(int onoff);
// lzh_20180816_e
#endif



void DxMainCall_Wait_Process(CALLSERVER_STRU *Msg_CallServer)
{

	int i;
	char paraString[500]={0};
	cJSON *call_para;
	cJSON *call_target;
	cJSON *divert_target;
	int dt_addr;
	char *event_str;
	cJSON *event;
	int room_num;
	char room_str[10];
	switch(Msg_CallServer->call_type)
	{
		case DxMainCall:					//DOORCALL: DS-> IX-MASTER/IX-SLAVE
			
			switch(Msg_CallServer->msg_type)
			{
				case CallServer_Msg_Invite:
					log_i("DxMainCall:Start:%s",Msg_CallServer->json_para);
					//ClearMonSrRequest();
					linphone_becall_cancel();
					if(Get_DtCaller_State() != 0)
					{
						API_DtCaller_ForceClose();
					}
					call_para=cJSON_Parse(Msg_CallServer->json_para);
					if(call_para==NULL)
					{
						log_w("DxMainCall:para parse err");
						CallServer_Business_Respones(Msg_CallServer->msg_source_id,CallServer_Msg_Invite,1);
						//API_vbu_CallVideoOff();
						return;
					}
					call_target=cJSON_GetObjectItemCaseSensitive(call_para, CallPara_Target);
					if(call_target==NULL)
					{
						log_w("DxMainCall:have no target");
						cJSON_Delete(call_para);
						CallServer_Business_Respones(Msg_CallServer->msg_source_id,CallServer_Msg_Invite,1);
						//API_vbu_CallVideoOff();
						return;
					}
					
					//cJSON_Delete(call_para);
					if(CallServer_Run.target_array!=NULL)
					{
						cJSON_Delete(CallServer_Run.target_array);
						
					}
					CallServer_Run.target_array=cJSON_CreateObject();
					if(CallServer_Run.target_array==NULL)
					{
						//cJSON_Delete(CallServer_Run.target_array);
						log_w("DxMainCall:create target array err");
						cJSON_Delete(call_para);
						CallServer_Business_Respones(Msg_CallServer->msg_source_id,CallServer_Msg_Invite,1);
						//API_vbu_CallVideoOff();
						return;
					}
					
					

					
					
					CallServer_Run.divert=0;
					CallServer_Run.caller_mask=0;
					//call_para=cJSON_Parse(Msg_CallServer->json_para);
					//if(call_para!=NULL)
					if(strcmp(API_PublicInfo_Read_String(PB_SIP_CONNECT_NETWORK),"NONE")!=0)
					{
						divert_target=cJSON_GetObjectItemCaseSensitive(call_para, CallPara_Divert);
						if(divert_target!=NULL)
						{
							if(GetJsonDataPro(divert_target,CallTarget_SipAcc,paraString)!=0)
							{
								//cJSON_Delete(call_para);
								
								if(Get_SipCaller_State() != 0)
								{
									API_SipCaller_ForceClose();
								}
								cJSON_AddStringToObject(CallServer_Run.target_array,CallTarget_SipAcc,paraString);
								log_d("DxMainCall:start divert:%s",paraString);

								CallServer_Run.divert=1;
								CallServer_Run.state = CallServer_Invite;
								CallServer_Run.call_type = Msg_CallServer->call_type;
								API_SipCaller_Invite(SipCaller_Ix2SipCall);
								CallServer_Run.caller_mask|=0x02;
							}
						}
					}
					if(GetJsonDataPro(call_target,CallTarget_DtAddr,paraString)==0)
					{
						if(CallServer_Run.caller_mask==0)
						{
							log_w("DxMainCall:unkown target");
							cJSON_Delete(call_para);
							CallServer_Business_Respones(Msg_CallServer->msg_source_id,CallServer_Msg_Invite,1);
							//API_vbu_CallVideoOff();
							return;
						}
						cJSON_Delete(call_para);
						Send_CallEvent("Calling_Start", NULL);
						//usleep(200*1000);

					}
					else
					{
						cJSON_AddStringToObject(CallServer_Run.target_array,CallTarget_DtAddr,paraString);
						dt_addr = strtoul(paraString,NULL,0);
						CallServer_Run.state = CallServer_Init;
						CallServer_Run.call_type = Msg_CallServer->call_type;
						API_PublicInfo_Write(PB_CALL_INFO,GetCallserverStatePb());
						Send_CallEvent("Calling_Start", NULL);
						//usleep(200*1000);
						
						if(DtMainCallLink(dt_addr)==0)
						{
							if(CallServer_Run.caller_mask==0)
							{
								#if 0
								divert_target=cJSON_GetObjectItemCaseSensitive(call_para, CallPara_Divert2);
								if(divert_target!=NULL)
								{
									if(GetJsonDataPro(divert_target,CallTarget_SipAcc,paraString)!=0)
									{
										//cJSON_Delete(call_para);
										
										if(Get_SipCaller_State() != 0)
										{
											API_SipCaller_ForceClose();
										}
										cJSON_AddStringToObject(CallServer_Run.target_array,CallTarget_SipAcc,paraString);
										log_d("DxMainCall:start divert:%s",paraString);

										CallServer_Run.divert=1;
										CallServer_Run.state = CallServer_Invite;
										CallServer_Run.call_type = Msg_CallServer->call_type;
										API_SipCaller_Invite(SipCaller_Ix2SipCall);
										CallServer_Run.caller_mask|=0x02;
										CallServer_Run.caller_mask|=0x01;
									}
								}
								#endif
								cJSON_Delete(call_para);
								if(CallServer_Run.caller_mask==0)
								{
									Send_CallEvent("Calling_Error", "DxMainCall:link im=%02x fail", dt_addr);
									
									log_w("DxMainCall:link im=%02x fail",dt_addr);
									CallServer_Business_Respones(Msg_CallServer->msg_source_id,CallServer_Msg_Invite,1);
									//API_vbu_CallVideoOff();
									CallServer_Run.state = CallServer_Wait;
									API_PublicInfo_Write(PB_CALL_INFO,GetCallserverStatePb());
									return;
								}
							}
						}
						else
						{
							cJSON_Delete(call_para);
							CallServer_Run.caller_mask|=0x01;
						}
					}
					if(DtMainCallSRReq()==0)
					{
						CallServer_Run.caller_mask&=(~0x01);
						if(CallServer_Run.caller_mask==0)
						{
							Send_CallEvent("Calling_Busy", NULL);
							log_w("DxMainCall:req SR fail");
							CallServer_Business_Respones(Msg_CallServer->msg_source_id,CallServer_Msg_Invite,1);
							//API_vbu_CallVideoOff();
							CallServer_Run.state = CallServer_Wait;
							API_PublicInfo_Write(PB_CALL_INFO,GetCallserverStatePb());
							return;
						}
						
					}
					else if(CallServer_Run.caller_mask&0x01)
					{
						CallServer_Run.state = CallServer_Invite;
						CallServer_Run.call_type = Msg_CallServer->call_type;
						API_DtCaller_Invite(DtCaller_MainCall);
					}
					
					
					
					
					CallServer_Run.state = CallServer_Invite;
					CallServer_Run.call_type = Msg_CallServer->call_type;
					
					CallServer_Run.timer = time(NULL);
					
					
					CallServer_Business_Respones(Msg_CallServer->msg_source_id,CallServer_Msg_Invite,0);
					
					DxMainCall_Inform_ToInvite();
					
					
					
					
					
					
					//API_add_log_item(LOG_CallTest_Level+1,Log_CallTest_Title,"Call Start",NULL);
					break;

				default:
					break;
			}
			break;
			
	
		default:
			break;
	}
		
	
	
}



void DxMainCall_Invite_Process(CALLSERVER_STRU *Msg_CallServer)
{
	unsigned char dtbecalled_type=0,dtcaller_type=0,ipbecalled_type=0,ipcaller_type=0;

	//switch(Msg_CallServer->call_type)
	{
		//case IxCallScene1_Active:					//DOORCALL: DS-> DX-MASTER -> DX-SLAVE 
			switch(Msg_CallServer->msg_type)
			{
				case CallServer_Msg_Invite:
					if(time(NULL) - CallServer_Run.timer > 5)
					{
						//CallServer_Run.state = CallServer_Wait;
						//IxCallScene3_MenuDisplay_ToWait();

						API_CallServer_Timeout();
					}
					//BEEP_ERROR();
					DxMainCall_Inform_ToInviteFail(0);
					CallServer_Business_Respones(Msg_CallServer->msg_source_id,CallServer_Msg_Invite,1);
					break;
					
				case CallServer_Msg_InviteOk:
					
					// lzh_20180905_s 接收到转呼叫应答则启动转呼叫功能
				
					CallServer_Run.rule_act = 0;
					CallServer_Run.state = CallServer_Ring;
					CallServer_Run.timer = time(NULL);
					API_DtCaller_Ring(DtCaller_MainCall);
					if(SR_State.in_use == FALSE)
					{
						//链路创建
						//API_SR_CallerCreate();
						SR_Routing_Create(CT08_DS_CALL_ST);
						
					}
					DxMainCall_Inform_ToRing();
									
					break;	

				case CallServer_Msg_InviteFail:
					API_DtCaller_Cancel(DtCaller_MainCall);
					CallServer_Run.caller_mask&=(~0x01);
					if(CallServer_Run.caller_mask==0)
					{
						CallServer_Run.state = CallServer_Wait;
						//BEEP_ERROR();
						//IxCallScene3_MenuDisplay_ToWait();
						DxMainCall_Inform_ToInviteFail(1);
						if(CallServer_Run.divert==1)
						{
							API_SipCaller_Cancel(SipCaller_Ix2SipCall);
						}
					}
					break;
				case CallServer_Msg_AppInviteFail:
					API_SipCaller_Cancel(SipCaller_Ix2SipCall);
					CallServer_Run.caller_mask&=(~0x02);
					if(CallServer_Run.caller_mask==0)
					{
						CallServer_Run.state = CallServer_Wait;
						//BEEP_ERROR();
						//IxCallScene3_MenuDisplay_ToWait();
						DxMainCall_Inform_ToInviteFail(1);
						
					}
					break;

				case CallServer_Msg_RemoteAck:
		
					CallServer_Run.state = CallServer_Ack;
					CallServer_Run.timer = time(NULL);
					//CallServer_Run.target_hook_dev = Msg_CallServer->target_dev_list[0];
					API_DtCaller_Ack(DtCaller_MainCall);
					DxMainCall_Inform_ToAck();
					if(CallServer_Run.divert==1)
					{
						API_SipCaller_Cancel(SipCaller_Ix2SipCall);
					}
					break;

				case CallServer_Msg_LocalBye:
					Send_CallEvent("Calling_Cancel", NULL);
					API_DtCaller_Cancel(DtCaller_MainCall);
					CallServer_Run.state = CallServer_Wait;
					DxMainCall_Inform_ToWait();
					if(CallServer_Run.divert==1)
					{
						API_SipCaller_Cancel(SipCaller_Ix2SipCall);
					}
					break;

				case CallServer_Msg_RemoteBye:
					//CallServer_Run.target_hook_dev = Msg_CallServer->target_dev_list[0];
					API_DtCaller_Bye(DtCaller_MainCall);
					CallServer_Run.state = CallServer_Wait;
					DxMainCall_Inform_ToWait();
					if(CallServer_Run.divert==1)
					{
						API_SipCaller_Cancel(SipCaller_Ix2SipCall);
					}
					break;

				case CallServer_Msg_Timeout:
					//API_IpCaller_ForceClose();
					API_DtCaller_Cancel(DtCaller_MainCall);
					CallServer_Run.state = CallServer_Wait;
					DxMainCall_Inform_ToWait();
					if(CallServer_Run.divert==1)
					{
						API_SipCaller_Cancel(SipCaller_Ix2SipCall);
					}
					break;

				case CallServer_Msg_DtSrDisconnect:
					break;

				case CallServer_Msg_NetCallLinkDisconnect:
					API_DtCaller_ForceClose();
					CallServer_Run.state = CallServer_Wait;
					DxMainCall_Inform_ToWait();
					if(CallServer_Run.divert==1)
					{
						API_SipCaller_Cancel(SipCaller_Ix2SipCall);
					}
					break;
					
				case CallServer_Msg_AppBye:
					if(CallServer_Run.divert==1)
					{
						API_SipCaller_Cancel(SipCaller_Ix2SipCall);
					}
					if(CallServer_Run.caller_mask==0x02)
					{
						CallServer_Run.state = CallServer_Wait;
						DxMainCall_Inform_ToWait();
					}
					break;
				case CallServer_Msg_AppInviteOk:
					if(CallServer_Run.divert==1)
					{
						API_SipCaller_Ring(SipCaller_Ix2SipCall);
					}
					if(CallServer_Run.caller_mask==0x02)
					{
						CallServer_Run.state = CallServer_Ring;
						CallServer_Run.timer = time(NULL);
						DxMainCall_Inform_ToRing();
					}
					break;
				case CallServer_Msg_AppAck:
					if(CallServer_Run.divert==1)
					{
						#if 0
						extern unsigned char myAddr;
						unsigned char addr_save;
						addr_save = myAddr;
					        myAddr = GetTargetDtAddr();
						
						#if 0
						if(CallServer_Run.state == CallServer_VirtualHook)
						{
							unsigned char ext_data[2]={0x55,0xaa};
							API_Stack_APT_Without_ACK_Data(addr_save,ST_TALK,2,ext_data); 
						}
						else
						#endif
						{
							API_Stack_APT_Without_ACK(addr_save,ST_TALK);
						}
						
					        //OS_Delay(200);
					        usleep(200*1000);
					        myAddr = addr_save;
						#endif
						#if 0
						API_DtCaller_Ack(DtCaller_MainCall);
						#else
						API_DtCaller_Cancel(DtCaller_MainCall);
						#endif
						API_SipCaller_Ack(SipCaller_Ix2SipCall);	
						
						CallServer_Run.state = CallServer_Ack;
						CallServer_Run.timer = time(NULL);
						DxMainCall_Inform_ToAppAck();
					}
					break;
				default:
					break;
			}
			
	}
}

void DxMainCall_Ring_Process(CALLSERVER_STRU *Msg_CallServer)
{
	cJSON *call_para;
	cJSON *divert_target;
	char paraString[50];
	//switch(Msg_CallServer->call_type)
	{
		//case IxCallScene1_Active:					//DOORCALL: DS-> IX-MASTER/IX-SLAVE

			// lzh_20180907_s
			// 因为转呼接收到振铃信号后CallServer_Run.state从CallServer_Transfer状态切换为CallServer_Ring状态故调用API_IpCaller_Ack时
			// 需要知道类型
			
			switch(Msg_CallServer->msg_type)
			{
				case CallServer_Msg_Invite:
					if(time(NULL) - CallServer_Run.timer > 5)
					{
						//CallServer_Run.state = CallServer_Wait;
						//IxCallScene3_MenuDisplay_ToWait();
						API_CallServer_Timeout();
					}
					DxMainCall_Inform_ToInviteFail(1);
					CallServer_Business_Respones(Msg_CallServer->msg_source_id,CallServer_Msg_Invite,1);
					break;
				case CallServer_Msg_Redail:
					if((time(NULL) - CallServer_Run.timer > 3)&&(CallServer_Run.caller_mask&0x01))
					{
						Callback_DtCaller_ToRedial_MainCallIM(NULL);
					}
					break;
					
				case CallServer_Msg_AppInviteFail:
					API_SipCaller_Cancel(SipCaller_Ix2SipCall);
					CallServer_Run.caller_mask&=(~0x02);
					if(CallServer_Run.caller_mask==0)
					{
						CallServer_Run.state = CallServer_Wait;
						//BEEP_ERROR();
						//IxCallScene3_MenuDisplay_ToWait();
						DxMainCall_Inform_ToWait();
						
					}
					break;	

				// lzh_20180905_s
				case CallServer_Msg_RingNoAck:
					#if 0
					if( CallServer_Run.call_rule == CallRule_TransferNoAck && CallServer_Run.rule_act == 0)
					{
						//CallServer_Run.rule_act = 1;
						//CallServer_Run.state = CallServer_Transfer;
					}	
					#endif
					break;
				// lzh_20180905_e

				
				case CallServer_Msg_RemoteAck:
					CallServer_Run.state = CallServer_Ack;
					CallServer_Run.timer = time(NULL);

					API_DtCaller_Ack(DtCaller_MainCall);
					if(CallServer_Run.divert==1)
					{
						CallServer_Run.caller_mask&=(~0x02);
						API_SipCaller_Bye(SipCaller_Ix2SipCall);
					}
					DxMainCall_Inform_ToAck();
					if(CallServer_Run.divert==1)
					{
						API_SipCaller_Cancel(SipCaller_Ix2SipCall);
					}
					break;

				case CallServer_Msg_LocalBye:
					Send_CallEvent("Calling_Cancel", NULL);
					API_DtCaller_Cancel(DtCaller_MainCall);
					CallServer_Run.state = CallServer_Wait;
					DxMainCall_Inform_ToWait();
					if(CallServer_Run.divert==1)
					{
						API_SipCaller_Cancel(SipCaller_Ix2SipCall);
					}
					break;

				case CallServer_Msg_RemoteBye:

					API_DtCaller_Bye(DtCaller_MainCall);
					CallServer_Run.state = CallServer_Wait;
					DxMainCall_Inform_ToWait();
					if(CallServer_Run.divert==1)
					{
						API_SipCaller_Cancel(SipCaller_Ix2SipCall);
					}
					break;
					
				case CallServer_Msg_Timeout:
					
					API_DtCaller_Cancel(DtCaller_MainCall); 				
					CallServer_Run.state = CallServer_Wait;
					DxMainCall_Inform_ToWait();
					if(CallServer_Run.divert==1)
					{
						API_SipCaller_Cancel(SipCaller_Ix2SipCall);
					}
						
					break;

				case CallServer_Msg_DtSrDisconnect:
					
					break;

				case CallServer_Msg_NetCallLinkDisconnect:
					API_DtCaller_ForceClose();
					CallServer_Run.state = CallServer_Wait;
					DxMainCall_Inform_ToWait();
					if(CallServer_Run.divert==1)
					{
						API_SipCaller_Cancel(SipCaller_Ix2SipCall);
					}
					break;
				case CallServer_Msg_RemoteUnlock1:
					DxMainCall_Inform_ToUnlock(1);
					break;

				case CallServer_Msg_RemoteUnlock2:
					DxMainCall_Inform_ToUnlock(2);
					break;
				case CallServer_Msg_Transfer:
					if(CallServer_Run.divert==1)
						return;
					call_para=cJSON_Parse(Msg_CallServer->json_para);
					if(call_para==NULL)
					{
						log_w("DxMainCall:para parse err");
						//CallServer_Business_Respones(Msg_CallServer->msg_source_id,CallServer_Msg_Invite,1);
						return;
					}
					divert_target=cJSON_GetObjectItemCaseSensitive(call_para, CallPara_Divert);
					if(divert_target==NULL)
					{
						log_w("DxMainCall:have no divert target");
						cJSON_Delete(call_para);
						//CallServer_Business_Respones(Msg_CallServer->msg_source_id,CallServer_Msg_Invite,1);
						return;
					}
					if(GetJsonDataPro(divert_target,CallTarget_SipAcc,paraString)==0)
					{
						log_w("DxMainCall:unkown target");
						cJSON_Delete(call_para);
						//CallServer_Business_Respones(Msg_CallServer->msg_source_id,CallServer_Msg_Invite,1);
						return;
					}
					cJSON_Delete(call_para);
					if(CallServer_Run.target_array==NULL)
					{
						CallServer_Run.target_array=cJSON_CreateObject();
					}
					
					if(CallServer_Run.target_array==NULL)
					{
						//cJSON_Delete(CallServer_Run.target_array);
						log_w("DxMainCall:create target array err");
						//CallServer_Business_Respones(Msg_CallServer->msg_source_id,CallServer_Msg_Invite,1);
						return;
					}
					if(Get_SipCaller_State() != 0)
					{
						API_SipCaller_ForceClose();
					}
					CallServer_Run.caller_mask|=0x02;
					cJSON_AddStringToObject(CallServer_Run.target_array,CallTarget_SipAcc,paraString);
					log_d("DxMainCall:start divert:%s",paraString);
					CallServer_Run.divert=1;
					API_SipCaller_Invite(SipCaller_Ix2SipCall);
					
					CallServer_Run.caller_mask&=(~0x01);
					API_DtCaller_Cancel(DtCaller_MainCall);
					//API_Stack_APT_Without_ACK(DS1_ADDRESS,ST_TALK);
					API_PublicInfo_Write(PB_CALL_INFO,GetCallserverStatePb());
					API_Event_By_Name(EventCallState);
					break;

				case CallServer_Msg_AppBye:
					if(CallServer_Run.divert==1)
					{
						API_SipCaller_Cancel(SipCaller_Ix2SipCall);
						if(CallServer_Run.caller_mask==0x02)
						{
							CallServer_Run.state = CallServer_Wait;
							DxMainCall_Inform_ToWait();
						}
					}
					break;
				case CallServer_Msg_AppInviteOk:
					if(CallServer_Run.divert==1)
					{
						API_SipCaller_Ring(SipCaller_Ix2SipCall);
					}
					
					break;
				case CallServer_Msg_AppAck:
					if(CallServer_Run.divert==1)
					{
						#if 0
						extern unsigned char myAddr;
						unsigned char addr_save;
						addr_save = myAddr;
					        myAddr = GetTargetDtAddr();
						
						#if 0
						if(CallServer_Run.state == CallServer_VirtualHook)
						{
							unsigned char ext_data[2]={0x55,0xaa};
							API_Stack_APT_Without_ACK_Data(addr_save,ST_TALK,2,ext_data); 
						}
						else
						#endif
						{
							API_Stack_APT_Without_ACK(addr_save,ST_TALK);
						}
						
					        //OS_Delay(200);
					        usleep(200*1000);
					        myAddr = addr_save;
						#endif
						#if 0
						API_DtCaller_Ack(DtCaller_MainCall);
						#else
						API_DtCaller_Cancel(DtCaller_MainCall);
						#endif
						API_SipCaller_Ack(SipCaller_Ix2SipCall);	
						
						CallServer_Run.state = CallServer_Ack;
						CallServer_Run.timer = time(NULL);
						DxMainCall_Inform_ToAppAck();
					}
					break;
				default:
					break;
			}
			
	}
	
}

void DxMainCall_Ack_Process(CALLSERVER_STRU *Msg_CallServer)
{
	//unsigned char ipcaller_type = 0;

	//switch(Msg_CallServer->call_type)
	{
		//case IxCallScene1_Active:					//DOORCALL: DS-> DX-MASTER -> DX-SLAVE 

			// lzh_20180907_s
			// 因为转呼接收到振铃信号后CallServer_Run.state从CallServer_Transfer状态切换为CallServer_Ring状态故调用API_IpCaller_Ack时
			// 需要知道类型
			switch(Msg_CallServer->msg_type)
			{
				case CallServer_Msg_Invite:
					if(time(NULL) - CallServer_Run.timer > 5)
					{
						//CallServer_Run.state = CallServer_Wait;
						//IxCallScene3_MenuDisplay_ToWait();

						API_CallServer_Timeout();
					}
					DxMainCall_Inform_ToInviteFail(1);
					CallServer_Business_Respones(Msg_CallServer->msg_source_id,CallServer_Msg_Invite,1);
					break;
					
				case CallServer_Msg_LocalBye:
					Send_CallEvent("Calling_Cancel", NULL);
					API_DtCaller_Cancel(DtCaller_MainCall);
					CallServer_Run.state = CallServer_Wait;
					DxMainCall_Inform_ToWait();
					if(CallServer_Run.divert==1)
					{
						API_SipCaller_Cancel(SipCaller_Ix2SipCall);
					}
					break;

				case CallServer_Msg_RemoteBye:
					API_DtCaller_Bye(DtCaller_MainCall);
					CallServer_Run.state = CallServer_Wait;
					DxMainCall_Inform_ToWait();
					if(CallServer_Run.divert==1)
					{
						API_SipCaller_Cancel(SipCaller_Ix2SipCall);
					}
					break;
					
				case CallServer_Msg_Timeout:
					//API_DtCaller_ForceClose();
					API_DtCaller_Cancel(DtCaller_MainCall);
					CallServer_Run.state = CallServer_Wait;
					DxMainCall_Inform_ToWait();
					if(CallServer_Run.divert==1)
					{
						API_SipCaller_Cancel(SipCaller_Ix2SipCall);
					}
					break;

				case CallServer_Msg_DtSrDisconnect:
					API_DtCaller_Cancel(DtCaller_MainCall);
					CallServer_Run.state = CallServer_Wait;
					DxMainCall_Inform_ToWait();
					if(CallServer_Run.divert==1)
					{
						API_SipCaller_Cancel(SipCaller_Ix2SipCall);
					}
					break;

				case CallServer_Msg_NetCallLinkDisconnect:
					API_DtCaller_ForceClose();
					CallServer_Run.state = CallServer_Wait;
					DxMainCall_Inform_ToWait();
					if(CallServer_Run.divert==1)
					{
						API_SipCaller_Cancel(SipCaller_Ix2SipCall);
					}
					break;

				case CallServer_Msg_RemoteUnlock1:
					DxMainCall_Inform_ToUnlock(1);
					break;

				case CallServer_Msg_RemoteUnlock2:
					DxMainCall_Inform_ToUnlock(2);
					break;

				case CallServer_Msg_AppBye:
					if(CallServer_Run.divert==1)
					{
						API_DtCaller_Cancel(DtCaller_MainCall);
						CallServer_Run.state = CallServer_Wait;
						
						API_SipCaller_Cancel(SipCaller_Ix2SipCall);
						DxMainCall_Inform_ToWait();
					}
					break;

				default:
					break;
			}
			
	}
	
	
}
void DxMainCall_SourceBye_Process(CALLSERVER_STRU *Msg_CallServer)
{
	//char detail[LOG_DESC_LEN + 1];
	
	//switch(Msg_CallServer->call_type)
	{
		//case IxCallScene1_Active:					//DOORCALL: DS-> DX-MASTER -> DX-SLAVE 
			switch(Msg_CallServer->msg_type)
			{
				case CallServer_Msg_Invite:
					break;
					
				case CallServer_Msg_RemoteRing:
					break;	

				case CallServer_Msg_LocalAck:
					break;	

				case CallServer_Msg_RemoteAck:
					break;

				case CallServer_Msg_LocalBye:
					break;

				case CallServer_Msg_RemoteBye:
					break;

				case CallServer_Msg_Timeout:
					break;

				case CallServer_Msg_DtSrDisconnect:
					break;

				case CallServer_Msg_NetCallLinkDisconnect:
					break;

				case CallServer_Msg_LocalUnlock1:
					break;

				case CallServer_Msg_LocalUnlock2:
					break;

				case CallServer_Msg_RemoteUnlock1:
					break;

				case CallServer_Msg_RemoteUnlock2:
					break;

				default:
					break;
			}
			
	}
	
}

void DxMainCall_TargetBye_Process(CALLSERVER_STRU *Msg_CallServer)
{
	//char detail[LOG_DESC_LEN + 1];

	//switch(Msg_CallServer->call_type)
	{
		//case IxCallScene1_Active:					//DOORCALL: DS-> DX-MASTER -> DX-SLAVE 
			switch(Msg_CallServer->msg_type)
			{
				case CallServer_Msg_Invite:
					break;
					
				case CallServer_Msg_RemoteRing:
					break;	

				case CallServer_Msg_LocalAck:
					break;	

				case CallServer_Msg_RemoteAck:
					break;

				case CallServer_Msg_LocalBye:
					break;

				case CallServer_Msg_RemoteBye:
					break;

				case CallServer_Msg_Timeout:
					break;

				case CallServer_Msg_DtSrDisconnect:
					break;

				case CallServer_Msg_NetCallLinkDisconnect:
					break;

				case CallServer_Msg_LocalUnlock1:
					break;

				case CallServer_Msg_LocalUnlock2:
					break;

				case CallServer_Msg_RemoteUnlock1:
					break;

				case CallServer_Msg_RemoteUnlock2:
					break;

				default:
					break;
			}
		
	}
	
}

void DxMainCall_Transfer_Process(CALLSERVER_STRU *Msg_CallServer)
{
	unsigned char ipbecalled_type=0;
	#if 0
	//switch(Msg_CallServer->call_type)
	{
		//case IxCallScene1_Active:					//  DS call -> phone
			
			switch(Msg_CallServer->msg_type)
			{			
				case CallServer_Msg_Invite:
					break;
					
				case CallServer_Msg_RingNoAck:
					CallServer_Run.state = CallServer_TargetBye;
					API_IpCaller_Cancel(IpCaller_Transfer);
					CallServer_Run.state = CallServer_Wait;
					IxCallScene1_Active_MenuDisplay_ToWait();
					break;
					
				case CallServer_Msg_InviteOk:
					CallServer_Run.state = CallServer_Ring;
					CallServer_Run.with_local_menu = 0;
					CallServer_Run.timer = time(NULL);
					
					API_IpCaller_Ring(IpCaller_Transfer,NULL);
					IxCallScene1_Active_MenuDisplay_ToRing();
					break;
					
				case CallServer_Msg_InviteFail:
					API_IpCaller_Cancel(IpCaller_Transfer);
					CallServer_Run.state = CallServer_Wait;
					IxCallScene1_Active_MenuDisplay_ToWait();
					break;

				case CallServer_Msg_RemoteAck:
					CallServer_Run.state = CallServer_Ack;
					CallServer_Run.with_local_menu = 0;
					//CallServer_Run.t_addr = Msg_CallServer->partner_addr;
					CallServer_Run.timer = time(NULL);
					
					API_IpCaller_Ack(IpCaller_Transfer,NULL);
					IxCallScene1_Active_MenuDisplay_ToAck();
					break;

				case CallServer_Msg_LocalBye:					
					// lzh_20180914_s
					API_IpCaller_Cancel(IpCaller_Transfer);
					CallServer_Run.state = CallServer_Wait;
					IxCallScene1_Active_MenuDisplay_ToWait();					
					// lzh_20180914_s
					break;

				case CallServer_Msg_RemoteBye:
					if(Msg_CallServer->target_dev_num > 0 && Msg_CallServer->target_dev_list[0].ip_addr == CallServer_Run.target_dev_list[0].ip_addr)
					{
						CallServer_Run.state = CallServer_TargetBye;
						API_IpCaller_Cancel(IpCaller_Transfer);
						CallServer_Run.state = CallServer_Wait;					

						IxCallScene1_Active_MenuDisplay_ToWait();
					}
					break;

				case CallServer_Msg_Timeout:
					API_IpCaller_ForceClose();
					CallServer_Run.state = CallServer_Wait;
					IxCallScene1_Active_MenuDisplay_ToWait();
					break;

				case CallServer_Msg_NetCallLinkDisconnect:
					//API_DtBeCalled_Bye(dtbecalled_type);
					API_IpCaller_ForceClose();
					CallServer_Run.state = CallServer_Wait;
					IxCallScene1_Active_MenuDisplay_ToWait();
					break;

				case CallServer_Msg_LocalUnlock1:
					CallServer_Run.with_local_menu |= 0x80;
					//API_Beep(BEEP_TYPE_DL1);
					API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_BeCalledUnlock1);
					break;

				case CallServer_Msg_LocalUnlock2:
					CallServer_Run.with_local_menu |= 0x80;
					//API_Beep(BEEP_TYPE_DL1);
					API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_BeCalledUnlock2);
					break;

				case CallServer_Msg_RemoteUnlock1:
					CallServer_Run.with_local_menu &= ~0x80;
					API_IpCaller_Unlock1(IpCaller_Transfer,NULL);
					break;

				case CallServer_Msg_RemoteUnlock2:
					CallServer_Run.with_local_menu &= ~0x80;
					API_IpCaller_Unlock2(IpCaller_Transfer,NULL);
					break;

				default:
					break;
			}
			
	}
	#endif
	
}



void DxMainCall_Inform_ToInvite(void)
{
	log_d("DxMainCall:ToInvite");
	API_vbu_CallVideoOn();
	API_PublicInfo_Write(PB_CALL_INFO,GetCallserverStatePb());
	API_Event_By_Name(EventCallState);
	//Send_CallEvent("Calling_Start");
}

void DxMainCall_Inform_ToInviteFail(int fail_type)
{
	log_i("DxMainCall:ToInviteFail");
	Send_CallEvent("Calling_Error", "DxMainCall:ToInviteFail");
	API_PublicInfo_Write(PB_CALL_INFO,GetCallserverStatePb());
	API_Event_By_Name(EventCallState);
	API_vbu_CallVideoOff();
}


void DxMainCall_Inform_ToRing(void)
{
	
	Send_CallEvent("Calling_Succ", NULL);
	API_PublicInfo_Write(PB_CALL_INFO,GetCallserverStatePb());	
	API_Event_By_Name(EventCallState);
	log_d("DxMainCall:ToRing");
	
	
	
	//video_turn_on_init();
	//api_ak_vi_ch_stream_preview_on(0,0); 
	//open_call_video();
	//API_vbu_CallVideoOn();
}

void DxMainCall_Inform_ToAck(void)
{
	log_d("DxMainCall:ToAck");
	API_PublicInfo_Write(PB_CALL_INFO,GetCallserverStatePb());
	API_Event_By_Name(EventCallState);
	OpenDtTalk();
	SetCallDsViToFull();
}

void DxMainCall_Inform_ToAppAck(void)
{
	cJSON *au_para;
	char temp[100];
	char *ptr;
	char sip_ser[50];
	log_d("DxMainCall:ToAck");
	API_PublicInfo_Write(PB_CALL_INFO,GetCallserverStatePb());
	API_Event_By_Name(EventCallState);
	GetVtkMediaTransAuPort();
	OpenAppTalk();
	au_para=cJSON_CreateObject();
	if(au_para!=NULL)
	{
		
		cJSON_AddStringToObject(au_para,TalkType,TalkType_LocalAndRtp);
		get_sip_master_ser(sip_ser);
		if((ptr=strstr(sip_ser,":"))!=NULL)
		{
			*ptr=0;
		}
		sprintf(temp,"%s:%d",sip_ser,GetVtkMediaTransAuPort());
		#if 0
		cJSON_AddNumberToObject(au_para,MIC_VOLUME_PHY,-10); 
		cJSON_AddNumberToObject(au_para,MIC_GAIN_PHY,1); 
		cJSON_AddNumberToObject(au_para,SPK_VOLUME_PHY,-10);
		cJSON_AddNumberToObject(au_para,SPK_GAIN_PHY,3);
		#endif
		API_TalkService_on(au_para);
		cJSON_Delete(au_para);
	}
	
}
void DxMainCall_Inform_ToBye(void)
{
	log_i("DxMainCall:Close1");
	//api_ak_vi_ch_stream_preview_off(0);
	//close_call_video();
	API_vbu_CallVideoOff();
	Memo_Stop();
	API_PublicInfo_Write(PB_CALL_INFO,GetCallserverStatePb());
	API_Event_By_Name(EventCallState);
	API_TalkService_off();
	CloseDtTalk();
}

void DxMainCall_Inform_ToWait(void)
{
	log_i("DxMainCall:Close");	
	//set_menu_with_video_off();
	//api_ak_vi_ch_stream_preview_off(0);
	//close_call_video();
	API_vbu_CallVideoOff();
	//Memo_Stop();
	API_vbu_MemoForceOff();
	API_PublicInfo_Write(PB_CALL_INFO,GetCallserverStatePb());
	API_Event_By_Name(EventCallState);
	API_TalkService_off();
	CloseDtTalk();
	Send_CallEvent("Calling_Close", NULL);
	usleep(200*1000);
	if(SR_State.in_use == TRUE)
	{
		//API_SR_CallerClose();
		SR_Routing_Close(CT08_DS_CALL_ST);
		//sr_create_flag = 0;
	}
	
}

void DxMainCall_Inform_ToTransfer(void)
{	
	log_d("DxMainCall:ipg mode divert");
	API_PublicInfo_Write(PB_CALL_INFO,GetCallserverStatePb());
	API_Event_By_Name(EventCallState);
}

void DxMainCall_Inform_ToUnlock(int lock_id)
{
	log_d("DxMainCall:Unlock%d",lock_id);
	API_Event_Unlock(lock_id==1?"RL1":"RL2", callscene_str[CallServer_Run.call_type]);
}

