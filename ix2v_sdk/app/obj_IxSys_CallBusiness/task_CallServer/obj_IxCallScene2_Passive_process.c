#if 1	//IX2_TEST
#include <stdio.h>
#include "task_CallServer.h"
#include "obj_BeCalled_State.h"
#include "obj_Caller_State.h"
#include "task_IpBeCalled.h"
#include "task_IpCaller.h"
#include "obj_IxCallScene2_Passive_process.h"
#include "elog_forcall.h"
#include "obj_Talk_Servi.h"
#include "task_Event.h"



void IxCallScene2_Passive_Wait_Process(CALLSERVER_STRU *Msg_CallServer)
{
	cJSON *call_para;
	cJSON *call_source;
	
	switch(Msg_CallServer->call_type)
	{	
		case IxCallScene2_Passive:				 //INTERCOM:   IX-ANOTHER -> IX-MASTER
			
			switch(Msg_CallServer->msg_type)
			{
				case CallServer_Msg_Invite: 
					//czn_20190107_s
					#if 0
					if(API_Business_Request(Business_State_BeIntercomCall) == 0)
					{
						//usleep(200000);
						//API_Beep(BEEP_TYPE_DI3);
						//popDisplayLastMenu();
						CallServer_Business_Respones(Msg_CallServer->msg_source_id,CallServer_Msg_Invite,1);
						return;
					}
					#endif
					//czn_20190107_e
					if(Get_IpBeCalled_State() != 0)
					{
						API_IpBeCalled_ForceClose();
					}
					call_para=cJSON_Parse(Msg_CallServer->json_para);
					if(call_para==NULL)
					{
						log_w("IxMainCall:para parse err");
						CallServer_Business_Respones(Msg_CallServer->msg_source_id,CallServer_Msg_Invite,1);
						//API_vbu_CallVideoOff();
						return;
					}
					call_source=cJSON_GetObjectItemCaseSensitive(call_para, CallPara_Source);
					if(call_source==NULL)
					{
						log_w("IxMainCall:have no source");
						cJSON_Delete(call_para);
						CallServer_Business_Respones(Msg_CallServer->msg_source_id,CallServer_Msg_Invite,1);
						//API_vbu_CallVideoOff();
						return;
					}
					if(CallServer_Run.source_info!=NULL)
					{
						cJSON_Delete(CallServer_Run.target_array);
						
					}
					CallServer_Run.source_info=cJSON_Duplicate(call_source,1);
					
					//Load_CallRule_Para();
					// lzh_20180812_s					
					//Load_CallRule_Para();
					// lzh_20180812_e					
					CallServer_Run.call_rule = CallRule_Normal;
													
					CallServer_Run.state = CallServer_Invite;	
					CallServer_Run.call_type = Msg_CallServer->call_type;
					//sleep(1);
					CallServer_Run.with_local_menu = 1;
					//CallServer_Run.source_dev = Msg_CallServer->source_dev;
					CallServer_Run.rule_act = 0;		//czn_20190116

					//printf("!!!123!!!!!!!!!!!IxCallScene1_Passive_Wait_Process[CallServer_Msg_Invite]!!!!!!!!!!!!!!!!\n");

					//Get_SelfDevInfo(Msg_CallServer->source_dev.ip_addr, &CallServer_Run.target_hook_dev); 
					
					//printf("!!!124!!!!!!!!!!!IxCallScene1_Passive_Wait_Process[CallServer_Msg_Invite]!!!!!!!!!!!!!!!!\n");

					CallServer_Run.timer = time(NULL);
					
					API_IpBeCalled_Invite(IpBeCalled_IxSys, NULL);			

					//printf("!!!125!!!!!!!!!!!IxCallScene1_Passive_Wait_Process[CallServer_Msg_Invite]!!!!!!!!!!!!!!!!\n");

					// �ֻ�������Ϊ����ת����������
					if( CallServer_Run.call_rule != CallRule_TransferIm )
					{
						CallServer_Run.state = CallServer_Ring;
						IxCallScene2_Passive_MenuDisplay_ToRing();
					}
					
					CallServer_Business_Respones(Msg_CallServer->msg_source_id,CallServer_Msg_Invite,0);
					//Load_CallRule_Para();
					
					
					break;

				default:
					break;
			}
			break;
			
	
		default:
			break;
	}
		
	
	
}

void IxCallScene2_Passive_Invite_Process(CALLSERVER_STRU *Msg_CallServer)
{
	unsigned char dtbecalled_type=0,dtcaller_type=0,ipbecalled_type=0,ipcaller_type=0;

	//switch(Msg_CallServer->call_type)
	switch(CallServer_Run.call_type)
	{	
		case IxCallScene2_Passive:					//INTERCOM: DT_ANOTHER -> DX-MASTER -> DX-SLAVE 
			break;
			
		default:
			break;
	}
}

void IxCallScene2_Passive_Ring_Process(CALLSERVER_STRU *Msg_CallServer)
{
	//switch(Msg_CallServer->call_type)
	switch(CallServer_Run.call_type)
	{
		case IxCallScene2_Passive:					//INTERCOM:   IX-ANOTHER -> IX-MASTER
			
			if(CallServer_Run.call_type != IxCallScene2_Passive)
			{
				//will_add
			}
			switch(Msg_CallServer->msg_type)
			{
				case CallServer_Msg_Invite:		//czn_20171030
					if(time(NULL) - CallServer_Run.timer > 5)
					{
						{
							CallServer_Run.state = CallServer_Wait;
							IxCallScene2_Passive_MenuDisplay_ToWait();
						}
						
					}
					else
					{
						//if(CallServer_Run.s_addr.rt == Msg_CallServer->partner_addr.rt && CallServer_Run.s_addr.code == Msg_CallServer->partner_addr.code)
						{
							//CallServer_Run.timer = time(NULL);
							//API_DtBeCalled_Invite(dtbecalled_type, &CallServer_Run.s_addr);
						}
					}
					CallServer_Business_Respones(Msg_CallServer->msg_source_id,CallServer_Msg_Invite,0);
					break;
				case CallServer_Msg_RingNoAck:
			
					CallServer_Run.state = CallServer_TargetBye;
					//CallServer_Run.t_addr = Msg_CallServer->partner_addr;
					API_IpBeCalled_Bye(IpBeCalled_IxSys);
					CallServer_Run.state = CallServer_Wait;
					IxCallScene2_Passive_MenuDisplay_ToWait();
					break;
					
				case CallServer_Msg_RemoteRing:
					break;	

				case CallServer_Msg_LocalAck:
					CallServer_Run.state = CallServer_Ack;
					CallServer_Run.with_local_menu = 1;
					CallServer_Run.timer = time(NULL); 
					
					API_IpBeCalled_Ack(IpBeCalled_IxSys);
					
					IxCallScene2_Passive_MenuDisplay_ToAck();
					break;	

				case CallServer_Msg_RemoteAck:
					break;

				case CallServer_Msg_DtSlaveAck:
					break;

				case CallServer_Msg_LocalBye:
					CallServer_Run.state = CallServer_TargetBye;
					//CallServer_Run.t_addr = Msg_CallServer->partner_addr;
					API_IpBeCalled_Bye(IpBeCalled_IxSys);
					CallServer_Run.state = CallServer_Wait;
					IxCallScene2_Passive_MenuDisplay_ToWait();
					break;

				case CallServer_Msg_RemoteBye:
					API_IpBeCalled_Cancel(IpBeCalled_IxSys);
					CallServer_Run.state = CallServer_Wait;
					IxCallScene2_Passive_MenuDisplay_ToWait();
					break;

				case CallServer_Msg_Timeout:
					//API_DtBeCalled_ForceClose();
					//API_IpCaller_ForceClose();
					API_IpBeCalled_Bye(IpBeCalled_IxSys);
					CallServer_Run.state = CallServer_Wait;
					IxCallScene2_Passive_MenuDisplay_ToWait();
					break;

				case CallServer_Msg_DtSrDisconnect:
					break;

				case CallServer_Msg_NetCallLinkDisconnect:
					API_IpBeCalled_ForceClose();
					CallServer_Run.state = CallServer_Wait;
					IxCallScene2_Passive_MenuDisplay_ToWait();
					break;

				case CallServer_Msg_LocalUnlock1:
					
					break;

				case CallServer_Msg_LocalUnlock2:
					
					break;

				case CallServer_Msg_RemoteUnlock1:
					
					break;

				case CallServer_Msg_RemoteUnlock2:
					
					break;
					
				case CallServer_Msg_Redail:	//czn_20171030
				#if 0
					if(!RingGetState())
					{
						API_RingPlay(Get_CallRingScene_ByAddr(CallServer_Run.s_addr));
						API_POWER_EXT_RING_ON();		//czn_20170809
						API_IpCaller_Redail(ipcaller_type);
					}
				#endif
					break;
					
				default:
					break;
			}
			break;
			
	
		default:
			break;
	}
	
}

void IxCallScene2_Passive_Ack_Process(CALLSERVER_STRU *Msg_CallServer)
{
	//switch(Msg_CallServer->call_type)
	switch(CallServer_Run.call_type)
	{
		case IxCallScene2_Passive:					//INTERCOM: DT_ANOTHER -> DX-MASTER -> DX-SLAVE 
			
			if(CallServer_Run.call_type != IxCallScene2_Passive)
			{
				//will_add
			}
			switch(Msg_CallServer->msg_type)
			{
				case CallServer_Msg_Invite:
					if(time(NULL) - CallServer_Run.timer > 5)
					{
						CallServer_Run.state = CallServer_Wait;
						IxCallScene2_Passive_MenuDisplay_ToWait();
					}
					CallServer_Business_Respones(Msg_CallServer->msg_source_id,CallServer_Msg_Invite,0);
					break;
					
				case CallServer_Msg_RemoteRing:
					break;	

				case CallServer_Msg_LocalAck:
					break;	

				case CallServer_Msg_RemoteAck:
					break;

				case CallServer_Msg_LocalBye:
					
					{
						CallServer_Run.state = CallServer_TargetBye;
						//CallServer_Run.t_addr = Msg_CallServer->partner_addr;
						API_IpBeCalled_Bye(IpBeCalled_IxSys);
						CallServer_Run.state = CallServer_Wait;
					}
					IxCallScene2_Passive_MenuDisplay_ToWait();
					break;

				case CallServer_Msg_RemoteBye:
					API_IpBeCalled_Cancel(IpBeCalled_IxSys);
					CallServer_Run.state = CallServer_Wait;
					IxCallScene2_Passive_MenuDisplay_ToWait();
					break;

				case CallServer_Msg_Timeout:
					//API_DtBeCalled_ForceClose();
					//API_IpCaller_ForceClose();
					API_IpBeCalled_Bye(IpBeCalled_IxSys);
					//API_IpCaller_Cancel(ipcaller_type);
					CallServer_Run.state = CallServer_Wait;
					IxCallScene2_Passive_MenuDisplay_ToWait();
					break;

				case CallServer_Msg_DtSrDisconnect:
					break;

				case CallServer_Msg_NetCallLinkDisconnect:
					API_IpBeCalled_ForceClose();
					CallServer_Run.state = CallServer_Wait;
					IxCallScene2_Passive_MenuDisplay_ToWait();
					break;


				case CallServer_Msg_RemoteUnlock1:
					break;

				case CallServer_Msg_RemoteUnlock2:
					break;

				default:
					break;
			}
			break;
			
			
		default:
			break;
	}
	
	
}
void IxCallScene2_Passive_SourceBye_Process(CALLSERVER_STRU *Msg_CallServer)
{
	//char detail[LOG_DESC_LEN + 1];
	
	switch(Msg_CallServer->call_type)
	{	
		case IxCallScene2_Passive:					//INTERCOM: DT_ANOTHER -> DX-MASTER -> DX-SLAVE 
			break;
			
		default:
			break;
	}
	
}

void IxCallScene2_Passive_TargetBye_Process(CALLSERVER_STRU *Msg_CallServer)
{
	//char detail[LOG_DESC_LEN + 1];

	switch(Msg_CallServer->call_type)
	{	
		case IxCallScene2_Passive:					//INTERCOM: DT_ANOTHER -> DX-MASTER -> DX-SLAVE 
			break;
			
		default:
			break;
	}
	
}

void IxCallScene2_Passive_Transfer_Process(CALLSERVER_STRU *Msg_CallServer)
{
#if 0
	//char detail[LOG_DESC_LEN + 1];
	unsigned char dtbecalled_type=0,dtcaller_type=0,ipbecalled_type=0,ipcaller_type=0;
	
	switch(Msg_CallServer->call_type)
	{	
		case IxCallScene2:					//INTERCOM: DT_ANOTHER -> DX-MASTER -> DX-SLAVE 
			//dtbecalled_type = DtBeCalled_IntercomCall;
			ipcaller_type = IpCaller_Transfer;
			
			switch(Msg_CallServer->msg_type)
			{
				case CallServer_Msg_Invite:
					if(time(NULL) - CallServer_Run.timer > 5)
					{
						
						{
							CallServer_Run.state = CallServer_Wait;
							IxCallScene2_MenuDisplay_ToWait();
						}
						
					}
					CallServer_Business_Respones(Msg_CallServer->msg_source_id,CallServer_Msg_Invite,0);
					break;
					
				case CallServer_Msg_RingNoAck:
					CallServer_Run.state = CallServer_TargetBye;
					CallServer_Run.t_addr = Msg_CallServer->partner_addr;
					//API_DtBeCalled_Bye(dtbecalled_type);
					API_IpCaller_Cancel(ipcaller_type);
					CallServer_Run.state = CallServer_Wait;
					IxCallScene2_MenuDisplay_ToWait();
					break;
					
				case CallServer_Msg_InviteOk:
					CallServer_Run.state = CallServer_Ring;
					CallServer_Run.with_local_menu = 0;
					CallServer_Run.timer = time(NULL);
					
					API_IpCaller_Ring(ipcaller_type,NULL);
					IxCallScene2_MenuDisplay_ToRing();
					break;
					
				case CallServer_Msg_InviteFail:	
					//API_DtBeCalled_Bye(dtbecalled_type);
					API_IpCaller_Cancel(ipcaller_type);
					CallServer_Run.state = CallServer_Wait;
					IxCallScene2_MenuDisplay_ToWait();
					break;

				case CallServer_Msg_RemoteAck:
					if(Msg_CallServer->partner_addr.rt == LIPHONE_RT)
					{
						CallServer_Run.state = CallServer_Ack;
						CallServer_Run.with_local_menu = 0;
						CallServer_Run.t_addr = Msg_CallServer->partner_addr;
						CallServer_Run.timer = time(NULL);
						
						//API_DtBeCalled_Ack(dtbecalled_type);
						API_IpCaller_Ack(ipcaller_type,&CallServer_Run.t_addr);
						IxCallScene2_MenuDisplay_ToAck();
					}
					break;

				case CallServer_Msg_DtSlaveAck:
					
					break;

				case CallServer_Msg_LocalBye:
					
					break;

				case CallServer_Msg_RemoteBye:
					#if 0	//czn_20170605
					if(Msg_CallServer->partner_addr.ip == GetLocalIp())
					{
						CallServer_Run.state = CallServer_SourceBye;
						API_DtBeCalled_Cancel(dtbecalled_type);
						API_IpCaller_Cancel(ipcaller_type);
						CallServer_Run.state = CallServer_Wait;
					}
					else
					#endif
					{
						CallServer_Run.state = CallServer_TargetBye;
						//API_DtBeCalled_Bye(dtbecalled_type);
						API_IpCaller_Cancel(ipcaller_type);
						CallServer_Run.state = CallServer_Wait;
					}
					IxCallScene2_MenuDisplay_ToWait();
					break;

				case CallServer_Msg_Timeout:
					//API_DtBeCalled_ForceClose();
					API_IpCaller_ForceClose();
					CallServer_Run.state = CallServer_Wait;
					IxCallScene2_MenuDisplay_ToWait();
					break;

				case CallServer_Msg_DtSrDisconnect:
					//API_DtBeCalled_ForceClose();
					//API_IpCaller_ForceClose();
					API_IpCaller_Cancel(ipcaller_type);		//czn_20171030
					CallServer_Run.state = CallServer_Wait;
					IxCallScene2_MenuDisplay_ToWait();
					break;

				case CallServer_Msg_NetCallLinkDisconnect:
					//API_DtBeCalled_Bye(dtbecalled_type);
					API_IpCaller_ForceClose();
					CallServer_Run.state = CallServer_Wait;
					IxCallScene2_MenuDisplay_ToWait();
					break;
				}
			break;
			
		default:
			break;
	}

#endif
	
}


void IxCallScene2_Passive_MenuDisplay_ToInvite(void)
{
	switch(CallServer_Run.call_type)
	{	
		case IxCallScene2_Passive:					//INTERCOM: DT_ANOTHER -> DX-MASTER -> DX-SLAVE 
			break;
				
		default:
			break;
	}
}


void IxCallScene2_Passive_MenuDisplay_ToRing(void)//czn_20190127
{
	//char disp_name[21] = {0};
	//char para_buff[21];
	char rm_nbr[11]={0};
	char tempData[42]={0};
	int max_list,i;
	char *ch;
	char paraString[500]={0};
	//VIDEO_PROXY_JSON objJson;
	cJSON *root = NULL;
	usleep(100*1000);
	API_PublicInfo_Write(PB_CALL_INFO,GetCallserverStatePb());
	API_Event_By_Name(EventCallState);
	
}

void IxCallScene2_Passive_MenuDisplay_ToAck(void)
{
	usleep(100*1000);
	API_PublicInfo_Write(PB_CALL_INFO,GetCallserverStatePb());
	API_Event_By_Name(EventCallState);
	
	cJSON *au_para;
	char ip_addr[20];
	OpenAppTalk();
	au_para=cJSON_CreateObject();
	if(au_para!=NULL)
	{
		
		cJSON_AddStringToObject(au_para,TalkType,TalkType_LocalAndUdp);
		//GetCallTarIxDevHookIp(ip_addr);
		GetJsonDataPro(CallServer_Run.source_info, PB_CALL_TAR_IxDevIP, ip_addr);
		
		cJSON_AddStringToObject(au_para,TargetIP,ip_addr); 
			
		API_TalkService_on(au_para);

		cJSON_Delete(au_para);
	}
}

void IxCallScene2_Passive_MenuDisplay_ToBye(void)
{
	usleep(100*1000);
	API_TalkService_off();
	API_PublicInfo_Write(PB_CALL_INFO,GetCallserverStatePb());
	API_Event_By_Name(EventCallState);
	
}

void IxCallScene2_Passive_MenuDisplay_ToWait(void)
{
	usleep(100*1000);
	API_TalkService_off();
	API_PublicInfo_Write(PB_CALL_INFO,GetCallserverStatePb());
	API_Event_By_Name(EventCallState);
	
}

void IxCallScene2_Passive_MenuDisplay_ToTransfer(void)
{	
	//char disp_name[21] = {0};

}
#endif