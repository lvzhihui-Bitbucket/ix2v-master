#ifndef _task_CallServer_h
#define _task_CallServer_h

#include "RTOS.h"
#include "OSTIME.h"
#include "task_survey.h"
#include "obj_IP_Call_Link.h"
#include "obj_business_manage.h"
//#include "video_multicast_common.h"
#include "obj_ipc_manager.h"
#include "obj_PublicInformation.h"
//efine DivertErrRecoverNormal
//typedef enum
//{
//	CallServer_Business = 0,
//	RecvMsg_Analyze,
//}CallServer_Msg_Type;
//
//
//typedef enum
//{
//	RecvMsg_VtkUnicast = 0,
//	RecvMsg_VtkMulticast,
//	RecvMsg_Sip,
//}RecvMsg_Type;

#define CALLSERVER_BUSINESS_LOG_LEVEL				9
#define CALLSERVER_ERROR_LOG_LEVEL				10

#define CallServer_Invite_LimitTime					12
#define CallServer_Ring_LimitTime					600
#define CallServer_Ack_LimitTime						600
#define CallServer_Bye_LimitTime						12


#define	CALLSERVER_RESPONSE_OK		(1<<6)
#define	CALLSERVER_RESPONSE_ERR		(1<<7)

#define DX432_RT		30
#define LIPHONE_RT		31
#define CallPara_Target		"Target"
#define CallPara_Source		"Source"
#define CallPara_Divert		"Divert"
#define CallPara_Divert2		"Divert2"
#define CallTarget_DtAddr		PB_CALL_TAR_DTADDR//"Call_Tar_DtAddr"
#define CallTarget_DtLogicAddr		PB_CALL_TAR_DT_LOGIC_ADDR//"Call_Tar_DtAddr"

#define CallTarget_SipAcc			PB_CALL_TAR_SIPACC//"Call_Tar_SipAcc"
#define CallTarget_Input			PB_CALL_TAR_INPUT//"Call_Tar_DtAddr"
#define CallTarget_IxDev			PB_CALL_TAR_IxDev//"Call_Tar_DtAddr"
#define CallTarget_IxHook			PB_CALL_TAR_IxHook//"Call_Tar_DtAddr"
#define CallTarget_IxHookDivert			"Call_Tar_IxHookDivert"


extern const char *callserver_state_str[];
extern const char *callserver_msg_str[];
extern const char *callscene_str[];
extern const int callscene_str_max;
#if 0
typedef enum
{
	DxCallScene1 = 1,					//DOORCALL: DS-> IX-MASTER/IX-SLAVE 
	DxCallScene2,						//INTERCOM:  IX-ANOTHER -> IX-MASTER
	DxCallScene3,						//INTERCOM:  IX-MASTER -> IX-ANOTHER 
	DxCallScene4,						//INNERCALL:  IX-MASTER -> IX-SLAVE 
	DxCallScene5,						//INNERCALL:  IX-SLAVE -> IX-MASTER 
}DxCallScene_t;
#endif
typedef enum
{
	IxCallScene1_Active = 1,				//DOORCALL: DS-> IX-IM 
	IxCallScene1_Passive,				//DOORCALL: DS-> IX-IM
	IxCallScene2_Active,				//INTERCOM:  IX-IM -> IX-IM
	IxCallScene2_Passive,				//INTERCOM:  IX-IM -> IX-IM 
	IxCallScene3_Active,				//INNERCALL:  IX-IM -> IX-IM 
	IxCallScene3_Passive,				//INNERCALL:  IX-IM -> IX-IM 
	IxCallScene4,						//IX50 -> VirtualUser		//czn_20190107
	IxCallScene5,						//test:IX50 -> VirtualUser
	IxCallScene8,						//phoneCALL: Phone -> DX-MASTER 
	IxCallScene9,						//DOORCALL:DS->DT-IM(IPG)			//call_dtim
	IxCallScene10_Active,				//GLCALL:GL->IX-IM
	IxCallScene10_Passive,
	IxCallScene11_Passive,			//DS->IX-IM WITH LOCAL DIVERT
	DxMainCall,
	IXGMainCall,
	SipCall,
	VMSipCall,
}DxCallScene_t;

typedef enum
{
	IxDevType_Ds = 0,
	IxDevType_Im,
}IxDevType_e;

typedef enum
{
	IxCallServer_ParaType1 = 1,				//BD_NBR + RM_NBR
}IxCallServer_ParaType_t;

typedef enum
{
	CallServer_Wait = 0,
	CallServer_Invite,
	CallServer_Ring,
	CallServer_Ack,
	CallServer_SourceBye,
	CallServer_TargetBye,
	CallServer_Transfer,
	CallServer_LeaveMsg,
	CallServer_RemoteAck,
	CallServer_Init,
}CallServer_State;

typedef enum
{
	CallRule_Normal = 0,
	CallRule_NoDisturb,
	CallRule_TransferIm,
	CallRule_TransferNoAck,
	CallRule_LeaveMsgIm,
	CallRule_LeaveMsgNoAck,
}CallRule_E;
#if 0
typedef enum
{
	CallServer_AsCallSource = 0,
	CallServer_AsCallTarget,
	CallServer_AsForceCallSource,
}CallServer_WorkMode;
#endif

typedef enum
{
	CallServer_Msg_Invite = 0,
	CallServer_Msg_InviteOk,
	CallServer_Msg_InviteFail,
	CallServer_Msg_RingNoAck,
	CallServer_Msg_RemoteRing,
	CallServer_Msg_LocalAck,
	CallServer_Msg_RemoteAck,
	CallServer_Msg_DtSlaveAck,
	CallServer_Msg_LocalBye,
	CallServer_Msg_RemoteBye,
	CallServer_Msg_ByeOk,
	CallServer_Msg_DtSrDisconnect,
	CallServer_Msg_NetCallLinkDisconnect,
	CallServer_Msg_Timeout,
	CallServer_Msg_LocalUnlock1,
	CallServer_Msg_LocalUnlock2,
	CallServer_Msg_RemoteUnlock1,
	CallServer_Msg_RemoteUnlock2,
	CallServer_Msg_Redail,			//czn_20171030
	CallServer_Msg_Transfer,
	CallServer_Msg_ThreadHeartbeatReq,
	CallServer_Msg_AppBye,
	CallServer_Msg_AppInviteOk,
	CallServer_Msg_AppAck,
	CallServer_Msg_AppInviteFail,
}CallServer_Msg_Type;

#pragma pack(1)


//czn_20180813_s
typedef struct
{
	int	ip_addr;
	char	dev_type[21];
	char bd_rm_ms[11];
	char global_nbr[11];
	char name[41];
}Call_Dev_Info;

#define MAX_CALL_TARGET_NUM		32
//czn_20180813_e

// lzh_20180910_s
typedef struct
{
	char			sip_server[16];
	char 			sip_account[20];
}sip_account_Info;

// lzh_20180910_e

#define  MAX_IMSLAVE_NUMS				10
#define MAX_CALLSERVER_PARABUF_LEN		20

typedef enum
{
	VIDEO_SOURCE_IX_DEVICE = 0,
	VIDEO_SOURCE_IPC,
}VIDEO_SOURCE;

typedef enum
{
	DISABLE = 0,
	ENABLE,
}VIDEO_VALID;


typedef struct
{
	unsigned char 		state;
	unsigned char 		call_type;
	time_t				timer;
	int					with_local_menu;
	int					call_rule;
	int					rule_act;		//0 = normal,1 = transfer, 2 = leave msg
	cJSON				*target_array;
	
	int					divert;	
	int					caller_mask;
	cJSON				*source_info;
	char 				call_id[20];
	//int					ix2divert;
	#if 1
	//Call_Dev_Info		source_dev;		// ���з����豸��Ϣ
	//Call_Dev_Info		target_hook_dev;// ���з����豸��Ϣ
	
	//int					tdev_nums;		// ���з���Ŀ���豸����
	//Call_Dev_Info		target_dev_list[MAX_CALL_TARGET_NUM];	// ���з���Ŀ���豸��Ϣ
	
	// lzh_20180912_s
	int					caller_divert_state;		// ��ǰ�豸divert��״̬
	sip_account_Info 	caller_master_sip_divert;	// ��ǰ�豸divert��sip�ʺ�
	int					callee_divert_state;		// Ӧ���豸divert��״̬
	sip_account_Info 	callee_master_sip_divert;	// Ӧ���豸divert��sip�ʺ�
	int					callee_rsp_command_id;		// ��ǰ��ҪӦ�������Ӧ��id
	// lzh_20180912_e


	//VIDEO_SOURCE		videoSource;				// ��ǰ��Ƶ��ԴIX����IPC
	//VIDEO_SOURCE 		defaultVideoSource;			// Ĭ����Ƶ��ԴIX����IPC
	//VIDEO_VALID			ixVideoSourceValid;			// IX��ƵԴ�Ƿ���Ч
	//VIDEO_VALID 		ipcVideoSourceValid;		// IPC��ƵԴ�Ƿ���Ч
	//int					ixVideoIp;					// IX��ƵIP��ַ
	//char				ipcVideoName[42];			// IPC��Ƶ��ʾ����
	//char				ixVideoName[42];			// IX��Ƶ��ʾ����
	//onvif_login_info_t 	ipcInfo;
	#endif
}CallServer_Run_Stru;
#pragma pack()

extern CallServer_Run_Stru CallServer_Run;
extern int MasterOrSlaveSetting;
extern Global_Addr_Stru DxMaster_Addr;


typedef struct
{
	unsigned char 		msg_target_id;		// ��Ϣ��Ŀ�����?
	unsigned char 		msg_source_id;		// ��Ϣ��Դ����
	unsigned char 		msg_type;			// ��Ϣ������
	unsigned char  		msg_sub_type;		 //��Ϣ��������
	unsigned char 		call_type;
	//Global_Addr_Stru 		partner_addr;
	
	#if 0
	Call_Dev_Info			source_dev;
	int					target_dev_num;
	union
	{
		Call_Dev_Info			target_dev_list[MAX_CALL_TARGET_NUM];
		sip_account_Info		sip_acc;
	};
	#endif
	char 				json_para[500];
	//Global_Addr_Stru 		partner_addr2;
}CALLSERVER_STRU;

#define CallServerMsgExtJsonPara		((int)&(((CALLSERVER_STRU*)0)->json_para[0]))
#define CallServerWithJsonPara_Length(Para_Len)		((int)&(((CALLSERVER_STRU*)0)->json_para[Para_Len+1]))		//czn_20190527

#define CallServerMsgExtTargetList_Length		((int)&(((CALLSERVER_STRU*)0)->target_dev_list[0]))
#define CallServerWithTargetList_Length(Tar_Num)		((int)&(((CALLSERVER_STRU*)0)->target_dev_list[Tar_Num]))		//czn_20190527


typedef enum
{
	CALL_LINK_BUSY, 			
	CALL_LINK_ERROR1, 		
	CALL_LINK_ERROR2,		
	CALL_LINK_ERROR3,		
	
}CALL_ERR_CODE;




uint8 VdpCallServer_Remote_Data_Dispatch(uint8 *pbuf,uint8 len);
void CallServer_Business_Process(CALLSERVER_STRU *Msg_CallServer);

void CallServer_Wait_Process(CALLSERVER_STRU *Msg_CallServer);
void CallServer_Invite_Process(CALLSERVER_STRU *Msg_CallServer);
void CallServer_Ring_Process(CALLSERVER_STRU *Msg_CallServer);
void CallServer_Ack_Process(CALLSERVER_STRU *Msg_CallServer);
void CallServer_SourceBye_Process(CALLSERVER_STRU *Msg_CallServer);
void CallServer_TargetBye_Process(CALLSERVER_STRU *Msg_CallServer);
void CallServer_Transfer_Process(CALLSERVER_STRU *Msg_CallServer);
void CallServer_RemoteAck_Process(CALLSERVER_STRU *Msg_CallServer);

void CallServer_StateInvalid_Process(CALLSERVER_STRU *Msg_CallServer);
void CallServer_MsgInvalid_Process(CALLSERVER_STRU *Msg_CallServer);


void CallServer_Timer_Callback(void);
cJSON *GetCallserverStatePb(void);

//int API_CallServer_Common(uint8 msg_type, uint8 call_type, Global_Addr_Stru partner_addr,Global_Addr_Stru *partner_addr2);
int API_CallServer_Common(uint8 msg_type);
void CallServer_Business_Respones(unsigned char respones_id,unsigned char msg_type,unsigned char result);
int GetCallInfo_JudgeWithLocalMenu(void);
int GetCallInfo_JudgeIsLocalUnlock(void);
void DxCall_MenuDisplay_ToInvite(void);
void DxCall_MenuDisplay_ToRing(void);
void DxCall_MenuDisplay_ToAck(void);
void DxCall_MenuDisplay_ToBye(void);
void DxCall_MenuDisplay_ToWait(void);
void Send_CallEvent(char *event_type, const char* format, ...);


//#define API_CallServer_BeInvite(call_type,source_dev)								API_CallServer_Common2(CallServer_Msg_Invite,call_type,source_dev,0,NULL)
//#define API_CallServer_Invite(call_type,target_num,target_list)							API_CallServer_Common(CallServer_Msg_Invite,call_type,NULL,target_num,target_list)
#define API_CallServer_InviteOk()										API_CallServer_Common(CallServer_Msg_InviteOk)
#define API_CallServer_InviteFail()										API_CallServer_Common(CallServer_Msg_InviteFail)

#define API_CallServer_LocalAck()										API_CallServer_Common(CallServer_Msg_LocalAck)
#define API_CallServer_RemoteAck()							API_CallServer_Common(CallServer_Msg_RemoteAck)


#define API_CallServer_LocalBye()					API_CallServer_Common(CallServer_Msg_LocalBye)
#define API_CallServer_RemoteBye()		API_CallServer_Common(CallServer_Msg_RemoteBye)
//#define API_CallServer_ByeOk(call_type,target_dev)			API_CallServer_Common(CallServer_Msg_ByeOk)

#define API_CallServer_DtSr_Disconnect()						API_CallServer_Common(CallServer_Msg_DtSrDisconnect)
//#define API_CallServer_NetCallLink_Disconnect()				API_CallServer_Common(CallServer_Msg_NetCallLinkDisconnect,0,NULL,0,NULL)

#define API_CallServer_LocalUnlock1(call_type)				API_CallServer_Common(CallServer_Msg_LocalUnlock1)
#define API_CallServer_LocalUnlock2(call_type)				API_CallServer_Common(CallServer_Msg_LocalUnlock2)
#define API_CallServer_RemoteUnlock1()				API_CallServer_Common(CallServer_Msg_RemoteUnlock1)
#define API_CallServer_RemoteUnlock2()				API_CallServer_Common(CallServer_Msg_RemoteUnlock2)

#define API_CallServer_RingNoAck()				API_CallServer_Common(CallServer_Msg_RingNoAck)
#define API_CallServer_Timeout()				API_CallServer_Common(CallServer_Msg_Timeout)

#define API_CallServer_Redail()				API_CallServer_Common(CallServer_Msg_Redail)	//czn_20171030

//#define API_CallServer_Transfer(call_type)				API_CallServer_Common(CallServer_Msg_Transfer)

#define API_CallServer_AppBye()		API_CallServer_Common(CallServer_Msg_AppBye)
#define API_CallServer_AppInviteOk()		API_CallServer_Common(CallServer_Msg_AppInviteOk)
#define API_CallServer_AppAck()		API_CallServer_Common(CallServer_Msg_AppAck)
#define API_CallServer_AppInviteFail()										API_CallServer_Common(CallServer_Msg_AppInviteFail)
#endif
