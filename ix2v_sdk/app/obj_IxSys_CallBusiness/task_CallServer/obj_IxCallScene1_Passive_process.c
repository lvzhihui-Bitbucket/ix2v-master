#if 1	//IX2_TEST
#include <stdio.h>
#include "task_CallServer.h"
#include "obj_BeCalled_State.h"
#include "obj_Caller_State.h"
#include "task_IpBeCalled.h"
#include "task_IpCaller.h"
#include "obj_IxCallScene1_Passive_process.h"
#include "task_Led.h"
#include "obj_Ring.h"
#include "au_service.h"
#include  "task_Event.h"
#include "elog_forcall.h"
#include "obj_Talk_Servi.h"

//extern CALL_RECORD_DAT_T call_record_temp;
//extern int call_record_flag;
#if 1

void IxCallScene1_Passive_Wait_Process(CALLSERVER_STRU *Msg_CallServer)
{
	cJSON *call_para;
	cJSON *call_source;
	switch(Msg_CallServer->call_type)
	{
		case IxCallScene1_Passive:					//DOORCALL: DS-> IX-MASTER/IX-SLAVE
			
			switch(Msg_CallServer->msg_type)
			{
				case CallServer_Msg_Invite: 

					printf("!!!!!!!!!!!!!!IxCallScene1_Passive_Wait_Process[CallServer_Msg_Invite]!!!!!!!!!!!!!!!!\n");

					//czn_20190107_s
					#if 0
					if(API_Business_Request(Business_State_BeMainCall) == 0)
					{
						//usleep(200000);
						//API_Beep(BEEP_TYPE_DI3);
						//popDisplayLastMenu();
						CallServer_Business_Respones(Msg_CallServer->msg_source_id,CallServer_Msg_Invite,1);
						return;
					}
					#endif
					//czn_20190107_e
					if(Get_IpBeCalled_State() != 0)
					{
						API_IpBeCalled_ForceClose();
					}

					//printf("!!!111!!!!!!!!!!!IxCallScene1_Passive_Wait_Process[CallServer_Msg_Invite]!!!!!!!!!!!!!!!!\n");
					call_para=cJSON_Parse(Msg_CallServer->json_para);
					if(call_para==NULL)
					{
						log_w("IxMainCall:para parse err");
						CallServer_Business_Respones(Msg_CallServer->msg_source_id,CallServer_Msg_Invite,1);
						//API_vbu_CallVideoOff();
						return;
					}
					call_source=cJSON_GetObjectItemCaseSensitive(call_para, CallPara_Source);
					if(call_source==NULL)
					{
						log_w("IxMainCall:have no source");
						cJSON_Delete(call_para);
						CallServer_Business_Respones(Msg_CallServer->msg_source_id,CallServer_Msg_Invite,1);
						//API_vbu_CallVideoOff();
						return;
					}
					if(CallServer_Run.source_info!=NULL)
					{
						cJSON_Delete(CallServer_Run.target_array);
						
					}
					CallServer_Run.source_info=cJSON_Duplicate(call_source,1);
					
					//Load_CallRule_Para();
					// lzh_20180812_s					
					//Load_CallRule_Para();
					// lzh_20180812_e					
					CallServer_Run.call_rule = CallRule_Normal;
													
					CallServer_Run.state = CallServer_Invite;	
					CallServer_Run.call_type = Msg_CallServer->call_type;
					//sleep(1);
					CallServer_Run.with_local_menu = 1;
					//CallServer_Run.source_dev = Msg_CallServer->source_dev;
					CallServer_Run.rule_act = 0;		//czn_20190116

					//printf("!!!123!!!!!!!!!!!IxCallScene1_Passive_Wait_Process[CallServer_Msg_Invite]!!!!!!!!!!!!!!!!\n");

					//Get_SelfDevInfo(Msg_CallServer->source_dev.ip_addr, &CallServer_Run.target_hook_dev); 
					
					//printf("!!!124!!!!!!!!!!!IxCallScene1_Passive_Wait_Process[CallServer_Msg_Invite]!!!!!!!!!!!!!!!!\n");

					CallServer_Run.timer = time(NULL);
					
					API_IpBeCalled_Invite(IpBeCalled_IxSys, NULL);			

					//printf("!!!125!!!!!!!!!!!IxCallScene1_Passive_Wait_Process[CallServer_Msg_Invite]!!!!!!!!!!!!!!!!\n");

					// �ֻ�������Ϊ����ת����������
					if( CallServer_Run.call_rule != CallRule_TransferIm )
					{
						CallServer_Run.state = CallServer_Ring;
						IxCallScene1_Passive_MenuDisplay_ToRing();
					}
					else
					{
						CallServer_Run.rule_act = 1;
						//API_IpBeCalled_Invite(IpBeCalled_IxSys, NULL);
						//API_IpBeCalled_ForceClose();
						CallServer_Run.state = CallServer_Transfer;	
						IxCallScene1_Passive_MenuDisplay_ToTransfer();
					}
					CallServer_Business_Respones(Msg_CallServer->msg_source_id,CallServer_Msg_Invite,0);
					
					break;

				default:
					break;
			}
			break;
			
	
		default:
			break;
	}
		
	
	
}

void IxCallScene1_Passive_Invite_Process(CALLSERVER_STRU *Msg_CallServer)
{
	unsigned char dtbecalled_type=0,dtcaller_type=0,ipbecalled_type=0,ipcaller_type=0;

	switch(Msg_CallServer->call_type)
	{
		case IxCallScene1_Passive:					//DOORCALL: DS-> DX-MASTER -> DX-SLAVE 
			switch(Msg_CallServer->msg_type)
			{
				case CallServer_Msg_Invite:
					break;
					
				case CallServer_Msg_RemoteRing:
					break;	

				case CallServer_Msg_LocalAck:
					break;	

				case CallServer_Msg_RemoteAck:
					break;

				case CallServer_Msg_LocalBye:
					break;

				case CallServer_Msg_RemoteBye:
					break;

				case CallServer_Msg_Timeout:
					break;

				case CallServer_Msg_DtSrDisconnect:
					break;

				case CallServer_Msg_NetCallLinkDisconnect:
					break;

				case CallServer_Msg_LocalUnlock1:
					break;

				case CallServer_Msg_LocalUnlock2:
					break;

				case CallServer_Msg_RemoteUnlock1:
					break;

				case CallServer_Msg_RemoteUnlock2:
					break;

				default:
					break;
			}
			break;
			
			
		default:
			break;
	}
}

void IxCallScene1_Passive_Ring_Process(CALLSERVER_STRU *Msg_CallServer)
{
	//switch(Msg_CallServer->call_type)
	//{
	//	case IxCallScene1_Passive:					//DOORCALL: DS-> IX-MASTER/IX-SLAVE
			
			if(CallServer_Run.call_type != IxCallScene1_Passive)
			{
				//will_add
			}
			
			
			switch(Msg_CallServer->msg_type)
			{
				case CallServer_Msg_Invite:		//czn_20171030
					if(time(NULL) - CallServer_Run.timer > 5)
					{
						{
							CallServer_Run.state = CallServer_Wait;
							IxCallScene1_Passive_MenuDisplay_ToWait();
						}
						
					}
					else
					{
						//if(CallServer_Run.s_addr.rt == Msg_CallServer->partner_addr.rt && CallServer_Run.s_addr.code == Msg_CallServer->partner_addr.code)
						{
							//CallServer_Run.timer = time(NULL);
							//API_DtBeCalled_Invite(dtbecalled_type, &CallServer_Run.s_addr);
						}
					}
					CallServer_Business_Respones(Msg_CallServer->msg_source_id,CallServer_Msg_Invite,0);
					break;
				case CallServer_Msg_RingNoAck:
			
					CallServer_Run.state = CallServer_TargetBye;
					//CallServer_Run.t_addr = Msg_CallServer->partner_addr;
					API_IpBeCalled_Bye(IpBeCalled_IxSys);
					CallServer_Run.state = CallServer_Wait;
					IxCallScene1_Passive_MenuDisplay_ToWait();
					break;
					
				case CallServer_Msg_RemoteRing:
					break;	

				case CallServer_Msg_LocalAck:
					CallServer_Run.state = CallServer_Ack;
					CallServer_Run.with_local_menu = 1;
					CallServer_Run.timer = time(NULL); 
					
					API_IpBeCalled_Ack(IpBeCalled_IxSys);
					
					IxCallScene1_Passive_MenuDisplay_ToAck();
					break;	

				case CallServer_Msg_RemoteAck:
					break;

				case CallServer_Msg_DtSlaveAck:
					break;

				case CallServer_Msg_LocalBye:
					CallServer_Run.state = CallServer_TargetBye;
					//CallServer_Run.t_addr = Msg_CallServer->partner_addr;
					API_IpBeCalled_Bye(IpBeCalled_IxSys);
					CallServer_Run.state = CallServer_Wait;
					IxCallScene1_Passive_MenuDisplay_ToWait();
					break;

				case CallServer_Msg_RemoteBye:
					//if(CallServer_Run.source_dev.ip_addr==Msg_CallServer->target_dev_list[0].ip_addr)
					{
						API_IpBeCalled_Cancel(IpBeCalled_IxSys);
						CallServer_Run.state = CallServer_Wait;
						IxCallScene1_Passive_MenuDisplay_ToWait();
					}
					break;

				case CallServer_Msg_Timeout:
					//API_DtBeCalled_ForceClose();
					//API_IpCaller_ForceClose();
					API_IpBeCalled_Bye(IpBeCalled_IxSys);
					CallServer_Run.state = CallServer_Wait;
					IxCallScene1_Passive_MenuDisplay_ToWait();
					break;

				case CallServer_Msg_DtSrDisconnect:
					break;

				case CallServer_Msg_NetCallLinkDisconnect:
					API_IpBeCalled_ForceClose();
					CallServer_Run.state = CallServer_Wait;
					IxCallScene1_Passive_MenuDisplay_ToWait();
					break;

				case CallServer_Msg_LocalUnlock1:
					CallServer_Run.with_local_menu |= 0x80;
					API_IpBeCalled_Unlock1(IpBeCalled_IxSys);
					//API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_BeCalledUnlock1);
					break;

				case CallServer_Msg_LocalUnlock2:
					CallServer_Run.with_local_menu |= 0x80;
					API_IpBeCalled_Unlock2(IpBeCalled_IxSys);

					//API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_BeCalledUnlock2);
					break;

				case CallServer_Msg_RemoteUnlock1:
					
					break;

				case CallServer_Msg_RemoteUnlock2:
					
					break;
					
				case CallServer_Msg_Redail:	//czn_20171030
				#if 0
					if(!RingGetState())
					{
						API_RingPlay(Get_CallRingScene_ByAddr(CallServer_Run.s_addr));
						API_POWER_EXT_RING_ON();		//czn_20170809
						API_IpCaller_Redail(ipcaller_type);
					}
				#endif
					break;

				case CallServer_Msg_Transfer:
					//printf("!!!!!!!!!!!!recv CallServer_Msg_Transfer msg\n\n\n");
					//API_IpBeCalled_ForceClose();
					CallServer_Run.rule_act = 1;
					//API_IpBeCalled_Invite(IpBeCalled_IxSys, NULL);
					CallServer_Run.state = CallServer_Transfer;	
					IxCallScene1_Passive_MenuDisplay_ToTransfer();
					break;
					
				default:
					break;
			}
			//break;
			
		//default:
		//	break;
	//}
	
}

void IxCallScene1_Passive_Ack_Process(CALLSERVER_STRU *Msg_CallServer)
{
	cJSON *tar_list;
	//switch(Msg_CallServer->call_type)
	//{
	//	case IxCallScene1_Passive:					//DOORCALL: DS-> DX-MASTER -> DX-SLAVE 
			
			if(CallServer_Run.call_type != IxCallScene1_Passive)
			{
				//will_add
			}
			switch(Msg_CallServer->msg_type)
			{
				case CallServer_Msg_Invite:
					if(time(NULL) - CallServer_Run.timer > 5)
					{
						CallServer_Run.state = CallServer_Wait;
						IxCallScene1_Passive_MenuDisplay_ToWait();
					}
					CallServer_Business_Respones(Msg_CallServer->msg_source_id,CallServer_Msg_Invite,0);
					break;
					
				case CallServer_Msg_RemoteRing:
					break;	

				case CallServer_Msg_LocalAck:
					break;	

				case CallServer_Msg_RemoteAck:
					break;

				case CallServer_Msg_LocalBye:
					
					{
						CallServer_Run.state = CallServer_TargetBye;
						//CallServer_Run.t_addr = Msg_CallServer->partner_addr;
						API_IpBeCalled_Bye(IpBeCalled_IxSys);
						CallServer_Run.state = CallServer_Wait;
					}
					IxCallScene1_Passive_MenuDisplay_ToWait();
					break;

				case CallServer_Msg_RemoteBye:
					//if(CallServer_Run.source_dev.ip_addr==Msg_CallServer->target_dev_list[0].ip_addr)
					{
						API_IpBeCalled_Cancel(IpBeCalled_IxSys);
						CallServer_Run.state = CallServer_Wait;
						IxCallScene1_Passive_MenuDisplay_ToWait();
					}
					break;

				case CallServer_Msg_Timeout:
					//API_DtBeCalled_ForceClose();
					//API_IpCaller_ForceClose();
					API_IpBeCalled_Bye(IpBeCalled_IxSys);
					//API_IpCaller_Cancel(ipcaller_type);
					CallServer_Run.state = CallServer_Wait;
					IxCallScene1_Passive_MenuDisplay_ToWait();
					break;

				case CallServer_Msg_DtSrDisconnect:
					break;

				case CallServer_Msg_NetCallLinkDisconnect:
					API_IpBeCalled_ForceClose();
					CallServer_Run.state = CallServer_Wait;
					IxCallScene1_Passive_MenuDisplay_ToWait();
					break;

				case CallServer_Msg_LocalUnlock1:
					API_IpBeCalled_Unlock1(IpBeCalled_IxSys);
					//API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_BeCalledUnlock1);	//czn_20191123
					break;

				case CallServer_Msg_LocalUnlock2:
					API_IpBeCalled_Unlock2(IpBeCalled_IxSys);
					//API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_BeCalledUnlock2);
					break;

				case CallServer_Msg_RemoteUnlock1:
					break;

				case CallServer_Msg_RemoteUnlock2:
					break;

				default:
					break;
			}
	//		break;
			
	
	//	default:
	//		break;
	//}
	
	
}
void IxCallScene1_Passive_SourceBye_Process(CALLSERVER_STRU *Msg_CallServer)
{
	//char detail[LOG_DESC_LEN + 1];
	
	switch(Msg_CallServer->call_type)
	{
		case IxCallScene1_Passive:					//DOORCALL: DS-> DX-MASTER -> DX-SLAVE 
			switch(Msg_CallServer->msg_type)
			{
				case CallServer_Msg_Invite:
					break;
					
				case CallServer_Msg_RemoteRing:
					break;	

				case CallServer_Msg_LocalAck:
					break;	

				case CallServer_Msg_RemoteAck:
					break;

				case CallServer_Msg_LocalBye:
					break;

				case CallServer_Msg_RemoteBye:
					break;

				case CallServer_Msg_Timeout:
					break;

				case CallServer_Msg_DtSrDisconnect:
					break;

				case CallServer_Msg_NetCallLinkDisconnect:
					break;

				case CallServer_Msg_LocalUnlock1:
					break;

				case CallServer_Msg_LocalUnlock2:
					break;

				case CallServer_Msg_RemoteUnlock1:
					break;

				case CallServer_Msg_RemoteUnlock2:
					break;

				default:
					break;
			}
			break;
			
	
		default:
			break;
	}
	
}

void IxCallScene1_Passive_TargetBye_Process(CALLSERVER_STRU *Msg_CallServer)
{
	//char detail[LOG_DESC_LEN + 1];

	switch(Msg_CallServer->call_type)
	{
		case IxCallScene1_Passive:					//DOORCALL: DS-> DX-MASTER -> DX-SLAVE 
			switch(Msg_CallServer->msg_type)
			{
				case CallServer_Msg_Invite:
					break;
					
				case CallServer_Msg_RemoteRing:
					break;	

				case CallServer_Msg_LocalAck:
					break;	

				case CallServer_Msg_RemoteAck:
					break;

				case CallServer_Msg_LocalBye:
					break;

				case CallServer_Msg_RemoteBye:
					break;

				case CallServer_Msg_Timeout:
					break;

				case CallServer_Msg_DtSrDisconnect:
					break;

				case CallServer_Msg_NetCallLinkDisconnect:
					break;

				case CallServer_Msg_LocalUnlock1:
					break;

				case CallServer_Msg_LocalUnlock2:
					break;

				case CallServer_Msg_RemoteUnlock1:
					break;

				case CallServer_Msg_RemoteUnlock2:
					break;

				default:
					break;
			}
			break;
			
		default:
			break;
	}
	
}

void IxCallScene1_Passive_Transfer_Process(CALLSERVER_STRU *Msg_CallServer)
{
	//unsigned char ipbecalled_type=0;
	switch(Msg_CallServer->msg_type)//czn_20190116
	{
		case CallServer_Msg_LocalBye:
			CallServer_Run.state = CallServer_TargetBye;
			//CallServer_Run.t_addr = Msg_CallServer->partner_addr;
			API_IpBeCalled_Bye(IpBeCalled_IxSys);
			CallServer_Run.state = CallServer_Wait;
			IxCallScene1_Passive_MenuDisplay_ToWait();
			break;

		case CallServer_Msg_RemoteBye:
			API_IpBeCalled_Cancel(IpBeCalled_IxSys);
			CallServer_Run.state = CallServer_Wait;
			IxCallScene1_Passive_MenuDisplay_ToWait();
			break;

		case CallServer_Msg_Timeout:
			//API_DtBeCalled_ForceClose();
			//API_IpCaller_ForceClose();
			API_IpBeCalled_Cancel(IpBeCalled_IxSys);
			//API_IpCaller_Cancel(ipcaller_type);
			CallServer_Run.state = CallServer_Wait;
			IxCallScene1_Passive_MenuDisplay_ToWait();
			break;
	}
	#if 0
	switch(Msg_CallServer->call_type)
	{
		case IxCallScene1:					//DOORCALL: DS-> DX-MASTER -> DX-SLAVE 
			//dtbecalled_type = DtBeCalled_MainCall;
			ipcaller_type = IpCaller_Transfer;
			
			switch(Msg_CallServer->msg_type)
			{
				case CallServer_Msg_Invite:
					if(time(NULL) - CallServer_Run.timer > 5)
					{
						
						{
							CallServer_Run.state = CallServer_Wait;
							IxCallScene1_MenuDisplay_ToWait();
						}
						
					}
					CallServer_Business_Respones(Msg_CallServer->msg_source_id,CallServer_Msg_Invite,0);
					break;
					
				case CallServer_Msg_RingNoAck:
					CallServer_Run.state = CallServer_TargetBye;
					CallServer_Run.t_addr = Msg_CallServer->partner_addr;
					//API_DtBeCalled_Bye(dtbecalled_type);
					API_IpCaller_Cancel(ipcaller_type);
					CallServer_Run.state = CallServer_Wait;
					IxCallScene1_MenuDisplay_ToWait();
					break;
					
				case CallServer_Msg_InviteOk:
					CallServer_Run.state = CallServer_Ring;
					CallServer_Run.with_local_menu = 0;
					CallServer_Run.timer = time(NULL);
					
					API_IpCaller_Ring(ipcaller_type,NULL);
					IxCallScene1_MenuDisplay_ToRing();
					break;
					
				case CallServer_Msg_InviteFail:
					#ifdef DivertErrRecoverNormal
					if(CallServer_Run.call_rule == CallRule_TransferIm)
					{
						//API_DtBeCalled_Cancel(dtbecalled_type);
						API_IpCaller_Cancel(ipcaller_type);
						CallServer_Run.state = CallServer_Wait;
						call_record_flag = 0;
						IxCallScene1_MenuDisplay_ToWait();
						usleep(1000000);
						
						CallServer_Run.call_rule = CallRule_Normal;
						CallServer_Run.rule_act = 0;
						//dtbecalled_type = DtBeCalled_MainCall;
						//ipcaller_type = IpCaller_MainCall;
			
						CallServer_Run.state = CallServer_Invite;
						//CallServer_Run.call_type = Msg_CallServer->call_type;
						CallServer_Run.with_local_menu = 1;
						//CallServer_Run.s_addr = Msg_CallServer->partner_addr;
						//will_add CallServer_Run.t_addr = local_addr;
						CallServer_Run.timer = time(NULL);
						//API_DtBeCalled_Invite(dtbecalled_type, &CallServer_Run.s_addr);
						
						API_IpCaller_Invite(ipcaller_type, NULL);
						
						CallServer_Run.state = CallServer_Ring;
						IxCallScene1_MenuDisplay_ToRing();
						//CallServer_Business_Respones(Msg_CallServer->msg_source_id,CallServer_Msg_Invite,0);
					}
					else
					{
						//API_DtBeCalled_Bye(dtbecalled_type);
						API_IpCaller_Cancel(ipcaller_type);
						CallServer_Run.state = CallServer_Wait;
						IxCallScene1_MenuDisplay_ToWait();
					}
					#else
					//API_DtBeCalled_Bye(dtbecalled_type);
					API_IpCaller_Cancel(ipcaller_type);
					CallServer_Run.state = CallServer_Wait;
					IxCallScene1_MenuDisplay_ToWait();
					#endif
					break;

				case CallServer_Msg_RemoteAck:
					if(Msg_CallServer->partner_addr.rt == LIPHONE_RT)
					{
						CallServer_Run.state = CallServer_Ack;
						CallServer_Run.with_local_menu = 0;
						CallServer_Run.t_addr = Msg_CallServer->partner_addr;
						CallServer_Run.timer = time(NULL);
						
						//API_DtBeCalled_Ack(dtbecalled_type);
						API_IpCaller_Ack(ipcaller_type,&CallServer_Run.t_addr);
						IxCallScene1_MenuDisplay_ToAck();
					}
					break;

				case CallServer_Msg_DtSlaveAck:
					
					break;

				case CallServer_Msg_LocalBye:
					
					break;

				case CallServer_Msg_RemoteBye:
					#if 0	//czn_20170605
					if(Msg_CallServer->partner_addr.ip == GetLocalIp())
					{
						CallServer_Run.state = CallServer_SourceBye;
						API_DtBeCalled_Cancel(dtbecalled_type);
						API_IpCaller_Cancel(ipcaller_type);
						CallServer_Run.state = CallServer_Wait;
					}
					else
					#endif
					#if 0
					if(CallServer_Run.call_rule == CallRule_TransferIm)
					{
						API_DtBeCalled_Cancel(dtbecalled_type);
						API_IpCaller_Cancel(ipcaller_type);
						CallServer_Run.state = CallServer_Wait;
						IxCallScene1_MenuDisplay_ToWait();
						usleep(1000000);
						
						CallServer_Run.call_rule = CallRule_Normal;
						CallServer_Run.rule_act = 0;
						dtbecalled_type = DtBeCalled_MainCall;
						ipcaller_type = IpCaller_MainCall;
			
						CallServer_Run.state = CallServer_Invite;
						//CallServer_Run.call_type = Msg_CallServer->call_type;
						CallServer_Run.with_local_menu = 1;
						//CallServer_Run.s_addr = Msg_CallServer->partner_addr;
						//will_add CallServer_Run.t_addr = local_addr;
						CallServer_Run.timer = time(NULL);
						API_DtBeCalled_Invite(dtbecalled_type, &CallServer_Run.s_addr);
						
						API_IpCaller_Invite(ipcaller_type, NULL);
						
						CallServer_Run.state = CallServer_Ring;
						IxCallScene1_MenuDisplay_ToRing();
					}
					else
					{
						{
							CallServer_Run.state = CallServer_TargetBye;
							API_DtBeCalled_Bye(dtbecalled_type);
							API_IpCaller_Cancel(ipcaller_type);
							CallServer_Run.state = CallServer_Wait;
						}
						IxCallScene1_MenuDisplay_ToWait();
					}
					#else
					{
						CallServer_Run.state = CallServer_TargetBye;
						//API_DtBeCalled_Bye(dtbecalled_type);
						API_IpCaller_Cancel(ipcaller_type);
						CallServer_Run.state = CallServer_Wait;
					}
					IxCallScene1_MenuDisplay_ToWait();
					#endif
					break;

				case CallServer_Msg_Timeout:
					//API_DtBeCalled_ForceClose();
					API_IpCaller_ForceClose();
					CallServer_Run.state = CallServer_Wait;
					IxCallScene1_MenuDisplay_ToWait();
					break;

				case CallServer_Msg_DtSrDisconnect:
					//API_DtBeCalled_ForceClose();
					//API_IpCaller_ForceClose();
					API_IpCaller_Cancel(ipcaller_type);		//czn_20171030
					CallServer_Run.state = CallServer_Wait;
					IxCallScene1_MenuDisplay_ToWait();
					break;

				case CallServer_Msg_NetCallLinkDisconnect:
					//API_DtBeCalled_Bye(dtbecalled_type);
					API_IpCaller_ForceClose();
					CallServer_Run.state = CallServer_Wait;
					IxCallScene1_MenuDisplay_ToWait();
					break;

				case CallServer_Msg_LocalUnlock1:
					CallServer_Run.with_local_menu |= 0x80;
					//API_DtBeCalled_Unlock1(dtbecalled_type);
					
					//API_Beep(BEEP_TYPE_DL1);
					API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_BeCalledUnlock1);
					break;

				case CallServer_Msg_LocalUnlock2:
					CallServer_Run.with_local_menu |= 0x80;
					//API_DtBeCalled_Unlock2(dtbecalled_type);

					//API_Beep(BEEP_TYPE_DL1);
					API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_BeCalledUnlock2);
					break;

				case CallServer_Msg_RemoteUnlock1:
					CallServer_Run.with_local_menu &= ~0x80;
					//API_DtBeCalled_Unlock1(dtbecalled_type);
					API_IpCaller_Unlock1(ipcaller_type,&Msg_CallServer->partner_addr);
					break;

				case CallServer_Msg_RemoteUnlock2:
					CallServer_Run.with_local_menu &= ~0x80;
					//API_DtBeCalled_Unlock2(dtbecalled_type);
					API_IpCaller_Unlock2(ipcaller_type,&Msg_CallServer->partner_addr);
					break;

				default:
					break;
			}
			break;
			
		default:
			break;
	}
	#endif
	
}


void IxCallScene1_Passive_MenuDisplay_ToInvite(void)
{
	switch(CallServer_Run.call_type)
	{
		case IxCallScene1_Passive:					//DOORCALL: DS-> DX-MASTER -> DX-SLAVE 
			break;
			
		default:
			break;
	}
}
#if 0
void GetIxVideoIpAddress(VIDEO_PROXY_JSON objJson)
{
	char address[11];
	int i;
	
	for(i = 0; i < 10 && objJson.ixDevice.addr[i]; i++)
	{
		address[i] = toupper(objJson.ixDevice.addr[i]);
	}
	address[i] = 0;

	//��IX�豸��Ƶ
	if(!strcmp(address, "NONE"))
	{
		CallServer_Run.ixVideoIp = 0;
	}
	//������е��豸�ṩ��Ƶ
	else if(address[0] == 0 || !strcmp(address, "MYSELF"))
	{
		CallServer_Run.ixVideoIp = CallServer_Run.source_dev.ip_addr;
	}
	//����IX�豸��Ƶ
	else
	{
		GetIpRspData data;
		if(API_GetIpNumberFromNet(objJson.ixDevice.addr, NULL, NULL, 2, 1, &data) == 0)
		{
			CallServer_Run.ixVideoIp = data.Ip[0];
		}
		else
		{
			CallServer_Run.ixVideoIp = 0;
		}
	}

}
#endif
void IxCallScene1_Passive_MenuDisplay_ToRing(void)//czn_20190127
{
	//char disp_name[21] = {0};
	//char para_buff[21];
	char rm_nbr[11]={0};
	char tempData[42]={0};
	int max_list,i;
	char *ch;
	char paraString[500]={0};
	//VIDEO_PROXY_JSON objJson;
	cJSON *root = NULL;
	usleep(100*1000);
	API_PublicInfo_Write(PB_CALL_INFO,GetCallserverStatePb());
	API_Event_By_Name(EventCallState);
	#if 0
	switch(CallServer_Run.call_type)
	{
		case IxCallScene1_Passive:					//DOORCALL: DS-> DX-MASTER -> DX-SLAVE 
			API_LedDisplay_CallRing();
			root = cJSON_CreateObject();	
    		cJSON_AddStringToObject(root, "RingType", "RING_CALL");
			cJSON_AddStringToObject(root, "Call_DeviceAddr", CallServer_Run.source_dev.bd_rm_ms);
			API_RingPlay(root);
			cJSON_Delete(root);

			//api_video_c_service_turn_on( 1, CallServer_Run.source_dev.ip_addr, 0, 1000 );	
			printf("!!!!!!!!!!!!!!CALLSERVER start video multicast subscriber!!!!!!!!!!!!!!!!\n");

			cJSON* win = cJSON_CreateString("WIN2");
			Ds_Show_Open(1,win);	
			cJSON_Delete(win);
			api_video_c_service_turn_on( 1, CallServer_Run.source_dev.ip_addr, 1/*ip_video_multicast*/, 2/*Resolution_720P*/, 0, 6000 );
			#if 0
			memo_vd_playback_stop();
			SetPipShowPos();
			CallServer_Run.defaultVideoSource = VIDEO_SOURCE_IX_DEVICE;
			API_io_server_UDP_to_read_one_remote(CallServer_Run.source_dev.ip_addr, VIDEO_PROXY_SET, paraString);
			printf("-IxCallScene1_Passive--- paraString=%s\n", paraString);
			ParseVideoProxyObject(paraString, &objJson);
			if(!strcmp(objJson.type, "IPC"))
			{
				CallServer_Run.defaultVideoSource = VIDEO_SOURCE_IPC;
				if(objJson.have_ipcinfo)
				{
					SetVideoProxyInfoShow(objJson.ipcDevice.NAME, objJson.ipcInfo.rtsp_url, objJson.ipcInfo.width, objJson.ipcInfo.height, objJson.ipcInfo.vd_type);
				}
				else
				{
					SetVideoProxyShow(objJson.ipcDevice.IP, objJson.ipcDevice.NAME, objJson.ipcDevice.USER, objJson.ipcDevice.PWD);
				}
			}
			else if(!strcmp(objJson.type, "IX"))
			{
				if(objJson.have_ipcinfo)
				{
					SetVideoProxyInfoShow(objJson.ipcDevice.NAME, objJson.ipcInfo.rtsp_url, objJson.ipcInfo.width, objJson.ipcInfo.height, objJson.ipcInfo.vd_type);
				}
			}

			GetIxVideoIpAddress(objJson);
			if(CallServer_Run.ixVideoIp != 0)
			{
				vd_printf("CallServer_Run.ixVideoIp = %0x08x\n", CallServer_Run.ixVideoIp);	
				if(open_dsmonitor_client(0,CallServer_Run.ixVideoIp,REASON_CODE_CALL,150,Resolution_720P) == 0)
				{
					SetVideoProxyDsShowPos();
				}
			}
			
			//API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_BecalledOn);
			//API_VideoTurnOn();
			//API_RingPlay(Get_CallRingScene_ByAddr(CallServer_Run.source_dev));		//
			//IX2_TEST API_POWER_EXT_RING_ON();		//czn_20170809
			call_record_temp.type 				= CALL_RECORD;
			call_record_temp.subType 			= IN_COMING;
			call_record_temp.property				= MISSED;
			call_record_temp.target_node			= 0;//CallServer_Run.s_addr.gatewayid;		//czn_20170329
			//call_record_temp.target_id			= CallServer_Run.s_addr.rt*32 + CallServer_Run.s_addr.code;
			call_record_temp.target_id			= 0;//CallServer_Run.s_addr.code;
			strcpy(call_record_temp.name,"---");
			strcpy(call_record_temp.input,"---");
			strcpy(call_record_temp.relation, "-");
			call_record_flag = 1;
			
			//if(Get_CallPartnerName_ByAddr(CallServer_Run.s_addr,disp_name) == 0)
			ch = tempData+1;
			get_device_addr_and_name_disp_str(0, CallServer_Run.source_dev.bd_rm_ms, NULL, NULL, CallServer_Run.source_dev.name, ch);
			
			strcpy(call_record_temp.name,ch);
			
			tempData[0] = strlen(ch);
			API_add_Inform_with_data_to_VideoMenu_queue(MSG_7_BRD_SUB_BecalledName, tempData, tempData[0]+1);
			
			usleep(2000*1000);
			if (CallingRecordProcess(tempData) == 0)
			{
				strncpy(call_record_temp.relation,tempData,40);
			}
			#endif
			
			break;
			
	
		default:
			break;
	}
		
	#endif
}

void IxCallScene1_Passive_MenuDisplay_ToAck(void)
{
	usleep(100*1000);
	API_PublicInfo_Write(PB_CALL_INFO,GetCallserverStatePb());
	API_Event_By_Name(EventCallState);

	cJSON *au_para;
	char ip_addr[20];
	OpenAppTalk();
	au_para=cJSON_CreateObject();
	if(au_para!=NULL)
	{
		
		cJSON_AddStringToObject(au_para,TalkType,TalkType_LocalAndUdp);
		//GetCallTarIxDevHookIp(ip_addr);
		GetJsonDataPro(CallServer_Run.source_info, PB_CALL_TAR_IxDevIP, ip_addr);
		
		cJSON_AddStringToObject(au_para,TargetIP,ip_addr); 
			
		API_TalkService_on(au_para);

		cJSON_Delete(au_para);
	}
	#if 0
	switch(CallServer_Run.call_type)
	{
		case IxCallScene1_Passive:					//DOORCALL: DS-> DX-MASTER -> DX-SLAVE 
			API_RingStop();
			usleep(100*1000);
			API_talk_on_by_type(LocalAndUdp,CallServer_Run.source_dev.ip_addr,AUDIO_CLIENT_UNICAST_PORT, AUDIO_SERVER_UNICAST_PORT);
			//TalkServiceTest(CallServer_Run.source_dev.ip_addr);
			//API_talk_on_by_unicast(CallServer_Run.source_dev.ip_addr,AUDIO_CLIENT_UNICAST_PORT, AUDIO_SERVER_UNICAST_PORT);
			API_LedDisplay_CallTalk();
			
			//IX2_TEST API_POWER_EXT_RING_OFF();		//czn_20170809
			
			API_TalkOn();//API_POWER_TALK_ON();

			//API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_BecalledTalkOn);
			//call_record_temp.property	= NORMAL;
			break;
	
		default:
			break;
	}
	#endif
}

void IxCallScene1_Passive_MenuDisplay_ToBye(void)
{
	usleep(100*1000);
	API_TalkService_off();
	API_PublicInfo_Write(PB_CALL_INFO,GetCallserverStatePb());
	API_Event_By_Name(EventCallState);
	#if 0
	switch(CallServer_Run.call_type)
	{
		case IxCallScene1_Passive:					//DOORCALL: DS-> DX-MASTER -> DX-SLAVE 
			//czn_20190107_s
			API_Business_Close(Business_State_BeMainCall);
			//czn_20190107_e
			API_talk_off();
			api_video_c_service_turn_off(1);	
			//close_monitor_client();
			//close_dsmonitor_client(0);
			API_LedDisplay_CallClose();
			API_TalkOff();
			//API_POWER_TALK_OFF();
			API_RingStop();
			//IX2_TEST API_POWER_EXT_RING_OFF();		//czn_20170809
			//API_VideoTurnOff();
			//API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_BecalledOff);
			break;
	
		default:
			break;
	}
	#endif
}

void IxCallScene1_Passive_MenuDisplay_ToWait(void)
{
	usleep(100*1000);
	API_TalkService_off();
	API_PublicInfo_Write(PB_CALL_INFO,GetCallserverStatePb());
	API_Event_By_Name(EventCallState);
	#if 0
	switch(CallServer_Run.call_type)
	{
		case IxCallScene1_Passive:					//DOORCALL: DS-> DX-MASTER -> DX-SLAVE 
			//czn_20190107_s
			API_Business_Close(Business_State_BeMainCall);
			//czn_20190107_e
			API_talk_off();
			api_video_c_service_turn_off(1);	
			//close_monitor_client();
			//close_dsmonitor_client(0);
			API_LedDisplay_CallClose();
			API_TalkOff();
			//API_POWER_TALK_OFF();
			API_RingStop();
			//IX2_TEST API_POWER_EXT_RING_OFF();		//czn_20170809
			//API_VideoTurnOff();
			//API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_BecalledOff);
			//API_AutoTest_BecalledCancel();		//czn_20190412
			break;
	
		default:
			break;
	}
	#endif
}

void IxCallScene1_Passive_MenuDisplay_ToTransfer(void)
{	
	//char disp_name[21] = {0};

}

#endif
#endif