#ifndef _obj_IxCallScene3_Passive_process_h
#define _obj_IxCallScene3_Passive_process_h


void IxCallScene3_Passive_Wait_Process(CALLSERVER_STRU *Msg_CallServer);
void IxCallScene3_Passive_Invite_Process(CALLSERVER_STRU *Msg_CallServer);
void IxCallScene3_Passive_Ring_Process(CALLSERVER_STRU *Msg_CallServer);
void IxCallScene3_Passive_Ack_Process(CALLSERVER_STRU *Msg_CallServer);
void IxCallScene3_Passive_SourceBye_Process(CALLSERVER_STRU *Msg_CallServer);
void IxCallScene3_Passive_TargetBye_Process(CALLSERVER_STRU *Msg_CallServer);
void IxCallScene3_Passive_Transfer_Process(CALLSERVER_STRU *Msg_CallServer);

void IxCallScene3_Passive_MenuDisplay_ToInvite(void);
void IxCallScene3_Passive_MenuDisplay_ToRing(void);
void IxCallScene3_Passive_MenuDisplay_ToAck(void);
void IxCallScene3_Passive_MenuDisplay_ToBye(void);
void IxCallScene3_Passive_MenuDisplay_ToWait(void);
void IxCallScene3_Passive_MenuDisplay_ToInviteFail(int fail_type);


#endif
