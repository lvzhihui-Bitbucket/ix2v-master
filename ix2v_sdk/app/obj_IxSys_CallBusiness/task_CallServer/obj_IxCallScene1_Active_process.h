#ifndef _obj_IxCallScene1_Active_process_h
#define _obj_IxCallScene1_Active_process_h


void IxCallScene1_Active_Wait_Process(CALLSERVER_STRU *Msg_CallServer);
void IxCallScene1_Active_Invite_Process(CALLSERVER_STRU *Msg_CallServer);
void IxCallScene1_Active_Ring_Process(CALLSERVER_STRU *Msg_CallServer);
void IxCallScene1_Active_Ack_Process(CALLSERVER_STRU *Msg_CallServer);
void IxCallScene1_Active_SourceBye_Process(CALLSERVER_STRU *Msg_CallServer);
void IxCallScene1_Active_TargetBye_Process(CALLSERVER_STRU *Msg_CallServer);
void IxCallScene1_Active_Transfer_Process(CALLSERVER_STRU *Msg_CallServer);

void IxCallScene1_Active_MenuDisplay_ToInvite(void);
void IxCallScene1_Active_MenuDisplay_ToRing(void);
void IxCallScene1_Active_MenuDisplay_ToAck(void);
void IxCallScene1_Active_MenuDisplay_ToBye(void);
void IxCallScene1_Active_MenuDisplay_ToWait(void);
void IxCallScene1_Active_MenuDisplay_ToTransfer(void);
void IxCallScene1_Active_MenuDisplay_ToInviteFail(int fail_type);

#endif
