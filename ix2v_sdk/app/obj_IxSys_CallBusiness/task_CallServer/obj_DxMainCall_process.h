#ifndef _obj_DxMainCall_process_h
#define _obj_DxMainCall_process_h


void DxMainCall_Wait_Process(CALLSERVER_STRU *Msg_CallServer);
void DxMainCall_Invite_Process(CALLSERVER_STRU *Msg_CallServer);
void DxMainCall_Ring_Process(CALLSERVER_STRU *Msg_CallServer);
void DxMainCall_Ack_Process(CALLSERVER_STRU *Msg_CallServer);
void DxMainCall_SourceBye_Process(CALLSERVER_STRU *Msg_CallServer);
void DxMainCall_TargetBye_Process(CALLSERVER_STRU *Msg_CallServer);
void DxMainCall_Transfer_Process(CALLSERVER_STRU *Msg_CallServer);

void DxMainCall_Inform_ToInvite(void);
void DxMainCall_Inform_ToRing(void);
void DxMainCall_Inform_ToAck(void);
void DxMainCall_Inform_ToBye(void);
void DxMainCall_Inform_ToWait(void);
void DxMainCall_Inform_ToTransfer(void);
void DxMainCall_Inform_ToInviteFail(int fail_type);
void DxMainCall_Inform_ToUnlock(int lock_id);

#endif
