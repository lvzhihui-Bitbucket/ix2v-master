#if 0	//IX2_TEST
#include <stdio.h>
#include "task_CallServer.h"
#include "obj_BeCalled_State.h"
#include "obj_Caller_State.h"
#include "task_IpBeCalled.h"
#include "task_IpCaller.h"
#include "obj_IxCallScene11_Passive_process.h"

extern CALL_RECORD_DAT_T call_record_temp;
extern int call_record_flag;


void IxCallScene11_Passive_Wait_Process(CALLSERVER_STRU *Msg_CallServer)
{
	switch(Msg_CallServer->call_type)
	{
		case IxCallScene11_Passive:					//DOORCALL: DS-> IX-MASTER/IX-SLAVE
			
			switch(Msg_CallServer->msg_type)
			{
				case CallServer_Msg_Invite: 
					//czn_20190107_s
					#if 0
					if(API_Business_Request(Business_State_BeMainCall) == 0)
					{
						//usleep(200000);
						//API_Beep(BEEP_TYPE_DI3);
						//popDisplayLastMenu();
						CallServer_Business_Respones(Msg_CallServer->msg_source_id,CallServer_Msg_Invite,1);
						return;
					}
					#endif
					//czn_20190107_e
					if(Get_IpBeCalled_State() != 0)
					{
						API_IpBeCalled_ForceClose();
					}
					if(Get_IpCaller_State() != 0)
					{
						API_IpCaller_ForceClose();
					}
					//Load_CallRule_Para();
					// lzh_20180812_s					
					Load_CallRule_Para();
					// lzh_20180812_e					
													
					CallServer_Run.state = CallServer_Invite;	
					CallServer_Run.call_type = Msg_CallServer->call_type;
					//sleep(1);
					CallServer_Run.with_local_menu = 1;
					CallServer_Run.source_dev = Msg_CallServer->source_dev;
					CallServer_Run.rule_act = 0;		//czn_20190116
					Get_SelfDevInfo(Msg_CallServer->source_dev.ip_addr, &CallServer_Run.target_hook_dev); 
					
					CallServer_Run.timer = time(NULL);
					
					API_IpBeCalled_Invite(IpBeCalled_NewIxSys, NULL);					
					
					// 分机若设置为立即转呼则不予理睬
					if( CallServer_Run.call_rule != CallRule_TransferIm )
					{
						CallServer_Run.state = CallServer_Ring;
						IxCallScene11_Passive_MenuDisplay_ToRing();
						printf("!!!!!!!!!!!!!!CallRule_TransferIm\n");
					}
					else
					{
						printf("11111111111111111CallRule_TransferIm\n");
						CallServer_Run.rule_act = 1;
						API_IpCaller_Invite(IpCaller_Transfer2Sip,NULL);
						//API_IpBeCalled_Invite(IpBeCalled_IxSys, NULL);
						//API_IpBeCalled_ForceClose();
						CallServer_Run.state = CallServer_Ring;	
						IxCallScene11_Passive_MenuDisplay_ToRing();
						DivertStateNotice_SimuDivert(CallServer_Run.source_dev);
						SipCfg_T *sip_cfg;
						sip_cfg = GetSipConfig();
						API_VtkMediaTrans_StartJpgPush(1,sip_cfg->serverIp,sip_cfg->account,sip_cfg->password,sip_cfg->divert);
					}
					CallServer_Business_Respones(Msg_CallServer->msg_source_id,CallServer_Msg_Invite,0);
					
					break;

				default:
					break;
			}
			break;
			
	
		default:
			break;
	}
		
	
	
}

void IxCallScene11_Passive_Invite_Process(CALLSERVER_STRU *Msg_CallServer)
{
	unsigned char dtbecalled_type=0,dtcaller_type=0,ipbecalled_type=0,ipcaller_type=0;

	switch(Msg_CallServer->call_type)
	{
		case IxCallScene11_Passive:					//DOORCALL: DS-> DX-MASTER -> DX-SLAVE 
			switch(Msg_CallServer->msg_type)
			{
				case CallServer_Msg_Invite:
					break;
					
				case CallServer_Msg_RemoteRing:
					break;	

				case CallServer_Msg_LocalAck:
					break;	

				case CallServer_Msg_RemoteAck:
					break;

				case CallServer_Msg_LocalBye:
					break;

				case CallServer_Msg_RemoteBye:
					break;

				case CallServer_Msg_Timeout:
					break;

				case CallServer_Msg_DtSrDisconnect:
					break;

				case CallServer_Msg_NetCallLinkDisconnect:
					break;

				case CallServer_Msg_LocalUnlock1:
					break;

				case CallServer_Msg_LocalUnlock2:
					break;

				case CallServer_Msg_RemoteUnlock1:
					break;

				case CallServer_Msg_RemoteUnlock2:
					break;

				default:
					break;
			}
			break;
			
			
		default:
			break;
	}
}

void IxCallScene11_Passive_Ring_Process(CALLSERVER_STRU *Msg_CallServer)
{
	SipCfg_T* sip_cfg;
	switch(Msg_CallServer->call_type)
	{
		case IxCallScene11_Passive:					//DOORCALL: DS-> IX-MASTER/IX-SLAVE
			
			if(CallServer_Run.call_type != IxCallScene11_Passive)
			{
				//will_add
			}
			
			
			switch(Msg_CallServer->msg_type)
			{
				case CallServer_Msg_Invite:		//czn_20171030
					if(time(NULL) - CallServer_Run.timer > 5)
					{
						{
							CallServer_Run.state = CallServer_Wait;
							IxCallScene11_Passive_MenuDisplay_ToWait();
						}
						
					}
					else
					{
						//if(CallServer_Run.s_addr.rt == Msg_CallServer->partner_addr.rt && CallServer_Run.s_addr.code == Msg_CallServer->partner_addr.code)
						{
							//CallServer_Run.timer = time(NULL);
							//API_DtBeCalled_Invite(dtbecalled_type, &CallServer_Run.s_addr);
						}
					}
					CallServer_Business_Respones(Msg_CallServer->msg_source_id,CallServer_Msg_Invite,0);
					break;
				// lzh_20210616_s
				case CallServer_Msg_InviteOk:			
					//sip_cfg = GetSipConfig();
					API_IpCaller_Ring(IpCaller_Transfer2Sip,NULL);
					//API_VtkMediaTrans_StartJpgPush(1,sip_cfg->serverIp,sip_cfg->account,sip_cfg->password,sip_cfg->divert);
					DivertStateNotice_DivertRing(CallServer_Run.source_dev);
					break;
				// lzh_20210616_e
				case CallServer_Msg_RingNoAck:
			
					CallServer_Run.state = CallServer_TargetBye;
					//CallServer_Run.t_addr = Msg_CallServer->partner_addr;
					API_IpCaller_Cancel(IpCaller_Transfer2Sip);
					API_IpBeCalled_Bye(IpBeCalled_NewIxSys);
					CallServer_Run.state = CallServer_Wait;
					IxCallScene11_Passive_MenuDisplay_ToWait();
					break;
					
				case CallServer_Msg_RemoteRing:
					break;	

				case CallServer_Msg_LocalAck:
					CallServer_Run.state = CallServer_Ack;
					CallServer_Run.with_local_menu = 1;
					CallServer_Run.timer = time(NULL); 
					API_IpCaller_Cancel(IpCaller_Transfer2Sip);
					API_IpBeCalled_Ack(IpBeCalled_NewIxSys);
					
					IxCallScene11_Passive_MenuDisplay_ToAck();
					break;	

				case CallServer_Msg_RemoteAck:
					CallServer_Run.state = CallServer_RemoteAck;
					CallServer_Run.with_local_menu = 1;
					CallServer_Run.timer = time(NULL); 
					API_IpCaller_Ack(IpCaller_Transfer2Sip,NULL);
					API_IpBeCalled_Ack(IpBeCalled_NewIxSys);
					
					IxCallScene11_Passive_MenuDisplay_ToRemoteAck();
					break;

				case CallServer_Msg_DtSlaveAck:
					break;

				case CallServer_Msg_LocalBye:
					CallServer_Run.state = CallServer_TargetBye;
					//CallServer_Run.t_addr = Msg_CallServer->partner_addr;
					API_IpCaller_Cancel(IpCaller_Transfer2Sip);
					API_IpBeCalled_Bye(IpBeCalled_NewIxSys);
					CallServer_Run.state = CallServer_Wait;
					IxCallScene11_Passive_MenuDisplay_ToWait();
					break;

				case CallServer_Msg_RemoteBye:
					if(Msg_CallServer->target_dev_num>0&&CallServer_Run.source_dev.ip_addr==Msg_CallServer->target_dev_list[0].ip_addr)
					{
						API_IpCaller_Cancel(IpCaller_Transfer2Sip);
						API_IpBeCalled_Cancel(IpBeCalled_NewIxSys);
						CallServer_Run.state = CallServer_Wait;
						IxCallScene11_Passive_MenuDisplay_ToWait();
					}
					else if(Msg_CallServer->target_dev_num==0)
					{
						if(CallServer_Run.call_rule != CallRule_TransferIm)
						{
							CallServer_Run.state = CallServer_TargetBye;
							//CallServer_Run.t_addr = Msg_CallServer->partner_addr;
							API_IpCaller_Cancel(IpCaller_Transfer2Sip);
							API_IpBeCalled_Bye(IpBeCalled_NewIxSys);
							CallServer_Run.state = CallServer_Wait;
							IxCallScene11_Passive_MenuDisplay_ToWait();
						}
					}
					break;

				case CallServer_Msg_Timeout:
					//API_DtBeCalled_ForceClose();
					//API_IpCaller_ForceClose();
					if( CallServer_Run.call_rule == CallRule_TransferNoAck && CallServer_Run.rule_act==0)
					{
						CallServer_Run.state = CallServer_Transfer;
						CallServer_Run.rule_act = 1;
						API_IpCaller_Invite(IpCaller_Transfer2Sip,NULL);
						IxCallScene11_Passive_MenuDisplay_ToTransfer();
						DivertStateNotice_Divert(CallServer_Run.source_dev);
						SipCfg_T *sip_cfg;
						sip_cfg = GetSipConfig();
						API_VtkMediaTrans_StartJpgPush(1,sip_cfg->serverIp,sip_cfg->account,sip_cfg->password,sip_cfg->divert);
					}
					else
					{
						API_IpCaller_Cancel(IpCaller_Transfer2Sip);
						API_IpBeCalled_Bye(IpBeCalled_NewIxSys);
						CallServer_Run.state = CallServer_Wait;
						IxCallScene11_Passive_MenuDisplay_ToWait();
					}
					
					break;

				case CallServer_Msg_DtSrDisconnect:
					break;

				case CallServer_Msg_NetCallLinkDisconnect:
					API_IpCaller_Cancel(IpCaller_Transfer2Sip);
					API_IpBeCalled_ForceClose();
					CallServer_Run.state = CallServer_Wait;
					IxCallScene11_Passive_MenuDisplay_ToWait();
					break;

				case CallServer_Msg_LocalUnlock1:
					CallServer_Run.with_local_menu |= 0x80;
					API_IpBeCalled_Unlock1(IpBeCalled_NewIxSys);
					API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_BeCalledUnlock1);
					break;

				case CallServer_Msg_LocalUnlock2:
					CallServer_Run.with_local_menu |= 0x80;
					API_IpBeCalled_Unlock2(IpBeCalled_NewIxSys);

					API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_BeCalledUnlock2);
					break;

				case CallServer_Msg_RemoteUnlock1:
					API_IpBeCalled_Unlock1(IpBeCalled_NewIxSys);
					break;

				case CallServer_Msg_RemoteUnlock2:
					API_IpBeCalled_Unlock2(IpBeCalled_NewIxSys);
					break;
					
				case CallServer_Msg_Redail:	//czn_20171030
				#if 0
					if(!RingGetState())
					{
						API_RingPlay(Get_CallRingScene_ByAddr(CallServer_Run.s_addr));
						API_POWER_EXT_RING_ON();		//czn_20170809
						API_IpCaller_Redail(ipcaller_type);
					}
				#endif
					break;
				#if 0
				case CallServer_Msg_Transfer:
					//printf("!!!!!!!!!!!!recv CallServer_Msg_Transfer msg\n\n\n");
					//API_IpBeCalled_ForceClose();
					CallServer_Run.rule_act = 1;
					//API_IpBeCalled_Invite(IpBeCalled_IxSys, NULL);
					CallServer_Run.state = CallServer_Transfer;	
					IxCallScene11_Passive_MenuDisplay_ToTransfer();
					break;
				#endif	
				default:
					break;
			}
			break;
			
		default:
			break;
	}
	
}

void IxCallScene11_Passive_Ack_Process(CALLSERVER_STRU *Msg_CallServer)
{
	
	switch(Msg_CallServer->call_type)
	{
		case IxCallScene11_Passive:					//DOORCALL: DS-> DX-MASTER -> DX-SLAVE 
			
			if(CallServer_Run.call_type != IxCallScene11_Passive)
			{
				//will_add
			}
			switch(Msg_CallServer->msg_type)
			{
				case CallServer_Msg_Invite:
					if(time(NULL) - CallServer_Run.timer > 5)
					{
						CallServer_Run.state = CallServer_Wait;
						IxCallScene11_Passive_MenuDisplay_ToWait();
					}
					CallServer_Business_Respones(Msg_CallServer->msg_source_id,CallServer_Msg_Invite,0);
					break;
					
				case CallServer_Msg_RemoteRing:
					break;	

				case CallServer_Msg_LocalAck:
					break;	

				case CallServer_Msg_RemoteAck:
					break;

				case CallServer_Msg_LocalBye:
					
					{
						CallServer_Run.state = CallServer_TargetBye;
						//CallServer_Run.t_addr = Msg_CallServer->partner_addr;
						API_IpBeCalled_Bye(IpBeCalled_NewIxSys);
						CallServer_Run.state = CallServer_Wait;
					}
					IxCallScene11_Passive_MenuDisplay_ToWait();
					break;

				case CallServer_Msg_RemoteBye:
					if(CallServer_Run.source_dev.ip_addr==Msg_CallServer->target_dev_list[0].ip_addr)
					{
						API_IpBeCalled_Cancel(IpBeCalled_NewIxSys);
						CallServer_Run.state = CallServer_Wait;
						IxCallScene11_Passive_MenuDisplay_ToWait();
					}
					break;

				case CallServer_Msg_Timeout:
					//API_DtBeCalled_ForceClose();
					//API_IpCaller_ForceClose();
					API_IpBeCalled_Bye(IpBeCalled_NewIxSys);
					//API_IpCaller_Cancel(ipcaller_type);
					CallServer_Run.state = CallServer_Wait;
					IxCallScene11_Passive_MenuDisplay_ToWait();
					break;

				case CallServer_Msg_DtSrDisconnect:
					break;

				case CallServer_Msg_NetCallLinkDisconnect:
					API_IpBeCalled_ForceClose();
					CallServer_Run.state = CallServer_Wait;
					IxCallScene11_Passive_MenuDisplay_ToWait();
					break;

				case CallServer_Msg_LocalUnlock1:
					API_IpBeCalled_Unlock1(IpBeCalled_NewIxSys);
					API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_BeCalledUnlock1);	//czn_20191123
					break;

				case CallServer_Msg_LocalUnlock2:
					API_IpBeCalled_Unlock2(IpBeCalled_NewIxSys);
					API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_BeCalledUnlock2);
					break;

				case CallServer_Msg_RemoteUnlock1:
					break;

				case CallServer_Msg_RemoteUnlock2:
					break;

				default:
					break;
			}
			break;
			
	
		default:
			break;
	}
	
	
}

void IxCallScene11_Passive_RemoteAck_Process(CALLSERVER_STRU *Msg_CallServer)
{
	SipCfg_T* sip_cfg;
	switch(Msg_CallServer->call_type)
	{
		case IxCallScene11_Passive:					//DOORCALL: DS-> IX-MASTER/IX-SLAVE
			
			if(CallServer_Run.call_type != IxCallScene11_Passive)
			{
				//will_add
			}
			
			
			switch(Msg_CallServer->msg_type)
			{
				case CallServer_Msg_Invite:		//czn_20171030
					if(time(NULL) - CallServer_Run.timer > 5)
					{
						{
							CallServer_Run.state = CallServer_Wait;
							IxCallScene11_Passive_MenuDisplay_ToWait();
						}
						
					}
					else
					{
						//if(CallServer_Run.s_addr.rt == Msg_CallServer->partner_addr.rt && CallServer_Run.s_addr.code == Msg_CallServer->partner_addr.code)
						{
							//CallServer_Run.timer = time(NULL);
							//API_DtBeCalled_Invite(dtbecalled_type, &CallServer_Run.s_addr);
						}
					}
					CallServer_Business_Respones(Msg_CallServer->msg_source_id,CallServer_Msg_Invite,0);
					break;
					
				case CallServer_Msg_RemoteRing:
					break;	

				case CallServer_Msg_RemoteAck:
		
					break;

				case CallServer_Msg_LocalBye:
					CallServer_Run.state = CallServer_TargetBye;
					//CallServer_Run.t_addr = Msg_CallServer->partner_addr;
					API_IpCaller_Cancel(IpCaller_Transfer2Sip);
					API_IpBeCalled_Bye(IpBeCalled_NewIxSys);
					CallServer_Run.state = CallServer_Wait;
					IxCallScene11_Passive_MenuDisplay_ToWait();
					break;

				case CallServer_Msg_RemoteBye:
					if(Msg_CallServer->target_dev_num>0&&CallServer_Run.source_dev.ip_addr==Msg_CallServer->target_dev_list[0].ip_addr)
					{
						API_IpCaller_Cancel(IpCaller_Transfer2Sip);
						API_IpBeCalled_Cancel(IpBeCalled_NewIxSys);
						CallServer_Run.state = CallServer_Wait;
						IxCallScene11_Passive_MenuDisplay_ToWait();
					}
					else if(Msg_CallServer->target_dev_num==0)
					{
						CallServer_Run.state = CallServer_TargetBye;
						//CallServer_Run.t_addr = Msg_CallServer->partner_addr;
						API_IpCaller_Cancel(IpCaller_Transfer2Sip);
						API_IpBeCalled_Bye(IpBeCalled_NewIxSys);
						CallServer_Run.state = CallServer_Wait;
						IxCallScene11_Passive_MenuDisplay_ToWait();
					}
					break;

				case CallServer_Msg_Timeout:
					//API_DtBeCalled_ForceClose();
					//API_IpCaller_ForceClose();
					API_IpCaller_Cancel(IpCaller_Transfer2Sip);
					API_IpBeCalled_Bye(IpBeCalled_NewIxSys);
					CallServer_Run.state = CallServer_Wait;
					IxCallScene11_Passive_MenuDisplay_ToWait();
					break;

				case CallServer_Msg_DtSrDisconnect:
					break;

				case CallServer_Msg_NetCallLinkDisconnect:
					API_IpCaller_Cancel(IpCaller_Transfer2Sip);
					API_IpBeCalled_ForceClose();
					CallServer_Run.state = CallServer_Wait;
					IxCallScene11_Passive_MenuDisplay_ToWait();
					break;

				case CallServer_Msg_LocalUnlock1:
					CallServer_Run.with_local_menu |= 0x80;
					API_IpBeCalled_Unlock1(IpBeCalled_NewIxSys);
					API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_BeCalledUnlock1);
					break;

				case CallServer_Msg_LocalUnlock2:
					CallServer_Run.with_local_menu |= 0x80;
					API_IpBeCalled_Unlock2(IpBeCalled_NewIxSys);

					API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_BeCalledUnlock2);
					break;

				case CallServer_Msg_RemoteUnlock1:
					API_IpBeCalled_Unlock1(IpBeCalled_NewIxSys);
					break;

				case CallServer_Msg_RemoteUnlock2:
					API_IpBeCalled_Unlock2(IpBeCalled_NewIxSys);
					break;
					
				case CallServer_Msg_Redail:	//czn_20171030
				#if 0
					if(!RingGetState())
					{
						API_RingPlay(Get_CallRingScene_ByAddr(CallServer_Run.s_addr));
						API_POWER_EXT_RING_ON();		//czn_20170809
						API_IpCaller_Redail(ipcaller_type);
					}
				#endif
					break;
				#if 0
				case CallServer_Msg_Transfer:
					//printf("!!!!!!!!!!!!recv CallServer_Msg_Transfer msg\n\n\n");
					//API_IpBeCalled_ForceClose();
					CallServer_Run.rule_act = 1;
					//API_IpBeCalled_Invite(IpBeCalled_IxSys, NULL);
					CallServer_Run.state = CallServer_Transfer;	
					IxCallScene11_Passive_MenuDisplay_ToTransfer();
					break;
				#endif	
				default:
					break;
			}
			break;
			
		default:
			break;
	}
	
}
void IxCallScene11_Passive_SourceBye_Process(CALLSERVER_STRU *Msg_CallServer)
{
	//char detail[LOG_DESC_LEN + 1];
	
	switch(Msg_CallServer->call_type)
	{
		case IxCallScene11_Passive:					//DOORCALL: DS-> DX-MASTER -> DX-SLAVE 
			switch(Msg_CallServer->msg_type)
			{
				case CallServer_Msg_Invite:
					break;
					
				case CallServer_Msg_RemoteRing:
					break;	

				case CallServer_Msg_LocalAck:
					break;	

				case CallServer_Msg_RemoteAck:
					break;

				case CallServer_Msg_LocalBye:
					break;

				case CallServer_Msg_RemoteBye:
					break;

				case CallServer_Msg_Timeout:
					break;

				case CallServer_Msg_DtSrDisconnect:
					break;

				case CallServer_Msg_NetCallLinkDisconnect:
					break;

				case CallServer_Msg_LocalUnlock1:
					break;

				case CallServer_Msg_LocalUnlock2:
					break;

				case CallServer_Msg_RemoteUnlock1:
					break;

				case CallServer_Msg_RemoteUnlock2:
					break;

				default:
					break;
			}
			break;
			
	
		default:
			break;
	}
	
}

void IxCallScene11_Passive_TargetBye_Process(CALLSERVER_STRU *Msg_CallServer)
{
	//char detail[LOG_DESC_LEN + 1];

	switch(Msg_CallServer->call_type)
	{
		case IxCallScene11_Passive:					//DOORCALL: DS-> DX-MASTER -> DX-SLAVE 
			switch(Msg_CallServer->msg_type)
			{
				case CallServer_Msg_Invite:
					break;
					
				case CallServer_Msg_RemoteRing:
					break;	

				case CallServer_Msg_LocalAck:
					break;	

				case CallServer_Msg_RemoteAck:
					break;

				case CallServer_Msg_LocalBye:
					break;

				case CallServer_Msg_RemoteBye:
					break;

				case CallServer_Msg_Timeout:
					break;

				case CallServer_Msg_DtSrDisconnect:
					break;

				case CallServer_Msg_NetCallLinkDisconnect:
					break;

				case CallServer_Msg_LocalUnlock1:
					break;

				case CallServer_Msg_LocalUnlock2:
					break;

				case CallServer_Msg_RemoteUnlock1:
					break;

				case CallServer_Msg_RemoteUnlock2:
					break;

				default:
					break;
			}
			break;
			
		default:
			break;
	}
	
}

void IxCallScene11_Passive_Transfer_Process(CALLSERVER_STRU *Msg_CallServer)
{
	//unsigned char ipbecalled_type=0;
	SipCfg_T* sip_cfg;
	switch(Msg_CallServer->msg_type)//czn_20190116
	{
		case CallServer_Msg_Timeout:
			//API_DtBeCalled_ForceClose();
			//API_IpCaller_ForceClose();
			API_IpCaller_Cancel(IpCaller_Transfer2Sip);
			API_IpBeCalled_Cancel(IpBeCalled_NewIxSys);
			//API_IpCaller_Cancel(ipcaller_type);
			CallServer_Run.state = CallServer_Wait;
			IxCallScene11_Passive_MenuDisplay_ToWait();
			break;
		case CallServer_Msg_InviteOk:	
			CallServer_Run.state = CallServer_Ring;
			API_IpCaller_Ring(IpCaller_Transfer2Sip,NULL);
			//sip_cfg = GetSipConfig();
			//API_VtkMediaTrans_StartJpgPush(1,sip_cfg->serverIp,sip_cfg->account,sip_cfg->password,sip_cfg->divert);
			break;
		case CallServer_Msg_InviteFail:	
			API_IpCaller_Cancel(IpCaller_Transfer2Sip);
			API_IpBeCalled_Cancel(IpBeCalled_NewIxSys);
			//API_IpCaller_Cancel(ipcaller_type);
			CallServer_Run.state = CallServer_Wait;
			IxCallScene11_Passive_MenuDisplay_ToWait();
			break;
		case CallServer_Msg_LocalBye:
			CallServer_Run.state = CallServer_TargetBye;
			//CallServer_Run.t_addr = Msg_CallServer->partner_addr;
			API_IpCaller_Cancel(IpCaller_Transfer2Sip);
			API_IpBeCalled_Bye(IpBeCalled_NewIxSys);
			CallServer_Run.state = CallServer_Wait;
			IxCallScene11_Passive_MenuDisplay_ToWait();
			break;

		case CallServer_Msg_RemoteBye:
			if(Msg_CallServer->target_dev_num>0&&CallServer_Run.source_dev.ip_addr==Msg_CallServer->target_dev_list[0].ip_addr)
			{
				API_IpCaller_Cancel(IpCaller_Transfer2Sip);
				API_IpBeCalled_Cancel(IpBeCalled_NewIxSys);
				CallServer_Run.state = CallServer_Wait;
				IxCallScene11_Passive_MenuDisplay_ToWait();
			}
			else if(Msg_CallServer->target_dev_num==0)
			{
				CallServer_Run.state = CallServer_TargetBye;
				//CallServer_Run.t_addr = Msg_CallServer->partner_addr;
				API_IpCaller_Cancel(IpCaller_Transfer2Sip);
				API_IpBeCalled_Bye(IpBeCalled_NewIxSys);
				CallServer_Run.state = CallServer_Wait;
				IxCallScene11_Passive_MenuDisplay_ToWait();
			}
			break;
		case CallServer_Msg_RemoteAck:
			CallServer_Run.state = CallServer_RemoteAck;
			CallServer_Run.with_local_menu = 1;
			CallServer_Run.timer = time(NULL); 
			API_IpCaller_Ack(IpCaller_Transfer2Sip,NULL);
			API_IpBeCalled_Ack(IpBeCalled_NewIxSys);
			
			IxCallScene11_Passive_MenuDisplay_ToRemoteAck();
			break;
		case CallServer_Msg_LocalUnlock1:
			CallServer_Run.with_local_menu |= 0x80;
			API_IpBeCalled_Unlock1(IpBeCalled_NewIxSys);
			API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_BeCalledUnlock1);
			break;

		case CallServer_Msg_LocalUnlock2:
			CallServer_Run.with_local_menu |= 0x80;
			API_IpBeCalled_Unlock2(IpBeCalled_NewIxSys);

			API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_BeCalledUnlock2);
			break;

		case CallServer_Msg_RemoteUnlock1:
			API_IpBeCalled_Unlock1(IpBeCalled_NewIxSys);
			break;

		case CallServer_Msg_RemoteUnlock2:
			API_IpBeCalled_Unlock2(IpBeCalled_NewIxSys);
			break;
	}
	
	
}


void IxCallScene11_Passive_MenuDisplay_ToInvite(void)
{
	switch(CallServer_Run.call_type)
	{
		case IxCallScene11_Passive:					//DOORCALL: DS-> DX-MASTER -> DX-SLAVE 
			break;
			
		default:
			break;
	}
}


void IxCallScene11_Passive_MenuDisplay_ToRing(void)//czn_20190127
{
	//char disp_name[21] = {0};
	//char para_buff[21];
	char rm_nbr[11]={0};
	char tempData[42]={0};
	int max_list,i;
	char *ch;
	int resolution;
	char paraString[500]={0};
	VIDEO_PROXY_JSON objJson;
	
	switch(CallServer_Run.call_type)
	{
		case IxCallScene11_Passive:					//DOORCALL: DS-> DX-MASTER -> DX-SLAVE 
			memo_vd_playback_stop();
			SetPipShowPos();
			CallServer_Run.defaultVideoSource = VIDEO_SOURCE_IX_DEVICE;
			API_io_server_UDP_to_read_one_remote(CallServer_Run.source_dev.ip_addr, VIDEO_PROXY_SET, paraString);
			printf("-IxCallScene11_Passive--- paraString=%s\n", paraString);
			ParseVideoProxyObject(paraString, &objJson);
			if(!strcmp(objJson.type, "IPC"))
			{
				CallServer_Run.defaultVideoSource = VIDEO_SOURCE_IPC;
				if(objJson.have_ipcinfo)
				{
					SetVideoProxyInfoShow(objJson.ipcDevice.NAME, objJson.ipcInfo.rtsp_url, objJson.ipcInfo.width, objJson.ipcInfo.height, objJson.ipcInfo.vd_type);
				}
				else
				{
					SetVideoProxyShow(objJson.ipcDevice.IP, objJson.ipcDevice.NAME, objJson.ipcDevice.USER, objJson.ipcDevice.PWD);
				}
			}
			else if(!strcmp(objJson.type, "IX"))
			{
				if(objJson.have_ipcinfo)
				{
					SetVideoProxyInfoShow(objJson.ipcDevice.NAME, objJson.ipcInfo.rtsp_url, objJson.ipcInfo.width, objJson.ipcInfo.height, objJson.ipcInfo.vd_type);
				}
			}

			//open_monitor_client_remote(CallServer_Run.source_dev.ip_addr,0,REASON_CODE_CALL,1,150,NULL,NULL);
			if( CallServer_Run.call_rule != CallRule_TransferIm )
			{
				resolution=Resolution_720P;
			}
			else
			{
				resolution=Resolution_480P;
			}
			GetIxVideoIpAddress(objJson);
			if(CallServer_Run.ixVideoIp != 0)
			{
				if(open_dsmonitor_client(0,CallServer_Run.ixVideoIp,REASON_CODE_CALL,150,resolution) == 0)
				{
					SetVideoProxyDsShowPos();
				}
			}
			
			//IX2_AD API_talk_off();
			API_LedDisplay_CallRing();
			API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_BecalledOn);
			//API_VideoTurnOn();
			//if(CallServer_Run.call_rule == CallRule_Normal)
				API_RingPlay(Get_CallRingScene_ByAddr(CallServer_Run.source_dev));		//
			//IX2_TEST API_POWER_EXT_RING_ON();		//czn_20170809
			call_record_temp.type 				= CALL_RECORD;
			call_record_temp.subType 			= IN_COMING;
			call_record_temp.property				= MISSED;
			call_record_temp.target_node			= 0;//CallServer_Run.s_addr.gatewayid;		//czn_20170329
			//call_record_temp.target_id			= CallServer_Run.s_addr.rt*32 + CallServer_Run.s_addr.code;
			call_record_temp.target_id			= 0;//CallServer_Run.s_addr.code;
			strcpy(call_record_temp.name,"---");
			strcpy(call_record_temp.input,"---");
			strcpy(call_record_temp.relation, "-");
			call_record_flag = 1;
			
			//if(Get_CallPartnerName_ByAddr(CallServer_Run.s_addr,disp_name) == 0)
			#if 0
			if((tempData[0] = strlen(CallServer_Run.source_dev.name))>0)
			{
				memcpy(&tempData[1],CallServer_Run.source_dev.name,tempData[0]);
				strcpy(call_record_temp.name,CallServer_Run.source_dev.name);
			}
			else
			{
				tempData[0] = strlen(CallServer_Run.source_dev.bd_rm_ms);
				memcpy(&tempData[1],CallServer_Run.source_dev.bd_rm_ms,tempData[0]);
			}
			#endif
			
			/*
			strcpy(rm_nbr,CallServer_Run.source_dev.bd_rm_ms);
			
			ch = tempData+1;
			if(!strcmp(GetSysVerInfo_bd(), "0099") && !memcmp(rm_nbr, "0099", 4))
			{
				sprintf(ch,"DS%d",atol(rm_nbr+8));
				//strcpy(target_dev[i].name,record[i].BD_RM_MS+4);
			}
			else
			{
				//strcpy(target_dev[i].name,record[i].BD_RM_MS);
				strcpy(ch,"DS");
				memcpy(ch+2,rm_nbr,4);
				sprintf(ch+strlen(ch),"(%d)",atol(rm_nbr+8));
			}
			
			if(strlen(CallServer_Run.source_dev.name)>=1&&strcmp(CallServer_Run.source_dev.name, "-"))
			{
				strcat(ch," ");
				strcat(ch,CallServer_Run.source_dev.name);
			}
			*/
			
			ch = tempData+1;
			get_device_addr_and_name_disp_str(0, CallServer_Run.source_dev.bd_rm_ms, NULL, NULL, CallServer_Run.source_dev.name, ch);
			
			strcpy(call_record_temp.name,ch);
			//strcpy(ch,"CallScene11");
			tempData[0] = strlen(ch);
			API_add_Inform_with_data_to_VideoMenu_queue(MSG_7_BRD_SUB_BecalledName, tempData, tempData[0]+1);
			
			
			usleep(2000*1000);
			if (CallingRecordProcess(tempData) == 0)
			{
				strncpy(call_record_temp.relation,tempData,40);
			}
			#if 0
			if(memo_video_record_start("BeCalled") == 0)
			{
				strncpy(call_record_temp.relation,one_vd_record.filename,40);
			}
			#endif
			break;
			
	
		default:
			break;
	}
		
	
}

void IxCallScene11_Passive_MenuDisplay_ToAck(void)
{
	switch(CallServer_Run.call_type)
	{
		case IxCallScene11_Passive:					//DOORCALL: DS-> DX-MASTER -> DX-SLAVE 
			//au_service_set_type(0);
			API_RingStop();
			usleep(100*1000);
			API_talk_on_by_type(0,CallServer_Run.source_dev.ip_addr,AUDIO_CLIENT_UNICAST_PORT, AUDIO_SERVER_UNICAST_PORT);
			API_LedDisplay_CallTalk();
			//if(CallServer_Run.call_rule == CallRule_Normal)
				
			//IX2_TEST API_POWER_EXT_RING_OFF();		//czn_20170809
			
			//API_TalkOn();//API_POWER_TALK_ON();
			API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_BecalledTalkOn);
			call_record_temp.property	= NORMAL;
			break;
	
		default:
			break;
	}
	
}

void IxCallScene11_Passive_MenuDisplay_ToRemoteAck(void)
{
	switch(CallServer_Run.call_type)
	{
		case IxCallScene11_Passive:					//DOORCALL: DS-> DX-MASTER -> DX-SLAVE 
			//au_service_set_type(2);
			API_talk_on_by_type(2,CallServer_Run.source_dev.ip_addr,AUDIO_CLIENT_UNICAST_PORT, AUDIO_SERVER_UNICAST_PORT);
			API_LedDisplay_CallTalk();
			//if(CallServer_Run.call_rule == CallRule_Normal)
				API_RingStop();
			//IX2_TEST API_POWER_EXT_RING_OFF();		//czn_20170809
			
			//API_TalkOn();//API_POWER_TALK_ON();
			API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_PhoneTalkOn);
			call_record_temp.property	= NORMAL;
			DivertStateNotice_DivertAck(CallServer_Run.source_dev);
			dsmonitor_client_change_resolution(0,CallServer_Run.source_dev.ip_addr,Resolution_240P);
			break;
	
		default:
			break;
	}
	
}


void IxCallScene11_Passive_MenuDisplay_ToBye(void)
{
	switch(CallServer_Run.call_type)
	{
		case IxCallScene11_Passive:					//DOORCALL: DS-> DX-MASTER -> DX-SLAVE 
			//czn_20190107_s
			API_Business_Close(Business_State_BeMainCall);
			//czn_20190107_e
			API_talk_off();
			//close_monitor_client();
			close_dsmonitor_client(0);
			API_LedDisplay_CallClose();
			//API_TalkOff();
			//API_POWER_TALK_OFF();
			//if(CallServer_Run.call_rule == CallRule_Normal)
				API_RingStop();
			//IX2_TEST API_POWER_EXT_RING_OFF();		//czn_20170809
			//API_VideoTurnOff();
			API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_BecalledOff);
			break;
	
		default:
			break;
	}
	
	if(call_record_flag == 1)
	{
		call_record_flag = 0;
		api_register_one_call_record( &call_record_temp );
	}
}

void IxCallScene11_Passive_MenuDisplay_ToWait(void)
{
	switch(CallServer_Run.call_type)
	{
		case IxCallScene11_Passive:					//DOORCALL: DS-> DX-MASTER -> DX-SLAVE 
			API_VtkMediaTrans_StopJpgPush();
			//czn_20190107_s
			API_Business_Close(Business_State_BeMainCall);
			//czn_20190107_e
			API_talk_off();
			//close_monitor_client();
			close_dsmonitor_client(0);
			API_LedDisplay_CallClose();
			//API_TalkOff();
			//API_POWER_TALK_OFF();
			//if(CallServer_Run.call_rule == CallRule_Normal)
				API_RingStop();
			//IX2_TEST API_POWER_EXT_RING_OFF();		//czn_20170809
			//API_VideoTurnOff();
			API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_BecalledOff);
			API_AutoTest_BecalledCancel();		//czn_20190412
			break;
	
		default:
			break;
	}

	if(call_record_flag == 1)
	{
		call_record_flag = 0;
		api_register_one_call_record( &call_record_temp );
	}

}

void IxCallScene11_Passive_MenuDisplay_ToTransfer(void)
{	
	//char disp_name[21] = {0};
	char tempData[42]={0};
	char *ch;
	switch(CallServer_Run.call_type)
	{
		case IxCallScene11_Passive:	
			if(CallServer_Run.call_rule == CallRule_TransferIm)
			{
				API_LedDisplay_CallDivert();
				//IX2_TEST API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_BeCalledDivert);
				//API_VideoTurnOn();
				//IX2_TEST API_RingStop();
				//IX2_TEST API_POWER_EXT_RING_OFF();		//czn_20170809
				call_record_temp.type 				= CALL_RECORD;
				call_record_temp.subType 			= IN_COMING;
				call_record_temp.property				= MISSED;
				call_record_temp.target_node			=0;// CallServer_Run.s_addr.gatewayid;		//czn_20170329
				call_record_temp.target_id			=0;// CallServer_Run.s_addr.rt*32 + CallServer_Run.s_addr.code;
				strcpy(call_record_temp.name,"---");
				strcpy(call_record_temp.input,"---");
				strcpy(call_record_temp.relation, "-");
				call_record_flag = 1;
				
				//if(Get_CallPartnerName_ByAddr(CallServer_Run.s_addr,disp_name) == 0)
				{
				//	strcpy(call_record_temp.name,disp_name);
				}
				ch = tempData+1;
				get_device_addr_and_name_disp_str(0, CallServer_Run.source_dev.bd_rm_ms, NULL, NULL, CallServer_Run.source_dev.name, ch);
				
				strcpy(call_record_temp.name,ch);

				tempData[0] = strlen(ch);
				//IX2_TEST API_add_Inform_with_data_to_VideoMenu_queue(MSG_7_BRD_SUB_BecalledName, tempData, tempData[0]+1);
			}
			else
			{
				API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_BeCalledDivert);
				API_LedDisplay_CallDivert();
				//IX2_TEST API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_BeCalledDivert);
				//IX2_TEST API_VideoTurnOff();
				//IX2_TEST API_RingStop();
				//IX2_TEST API_POWER_EXT_RING_OFF();		//czn_20170809
				dsmonitor_client_change_resolution(0,CallServer_Run.source_dev.ip_addr,Resolution_240P);
			}
			break;

		default:
			break;
	}
}

#endif