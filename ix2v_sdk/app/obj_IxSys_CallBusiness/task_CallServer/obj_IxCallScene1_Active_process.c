#include <stdio.h>
#include "task_CallServer.h"
#include "obj_BeCalled_State.h"
#include "obj_Caller_State.h"
//#include "../task_DtBeCalled/task_DtBeCalled.h"
//#include "../task_DtCaller/task_DtCaller.h"
//#include "../task_IpBeCalled/task_IpBeCalled.h"
#if 1
#include "task_IpCaller.h"
#include "task_SipCaller.h"

#include "obj_IxCallScene1_Active_process.h"
#include "obj_GetIpByInput.h"
#include "elog_forcall.h"
#include "obj_Talk_Servi.h"
#include "task_Event.h"
#include "obj_IoInterface.h"

#define R8001TbPath	"/mnt/nand1-2/UserRes/R8001.json"
static cJSON *r8001Table=NULL;
int GetIPByInput_FromR8001(const char* input, IpCacheRecord_T record[])
{
	char bd_rm_ms[11] = {0};
	int len;
	int i;
	int cnt=0;
	char room_num[11];
	char ip_type[10];
	char ip[20];
	int record_num;
	cJSON *one_node;
	
	GetBdRmMsByInput(input, bd_rm_ms);
	//printf("11111111111FromR8001,%s:%s,%08x\n",input, bd_rm_ms,r8001Table);
	if(r8001Table == NULL)
	{
		r8001Table=GetJsonFromFile(R8001TbPath);
		return 0;
	}
	record_num=cJSON_GetArraySize(r8001Table);
	if(record_num==0)
	{
		return 0;
	}
	//printf("22222222FromR8001,%d\n",r8001Table->record_cnt);
	len=strlen(bd_rm_ms);
	for(i = 0; i < record_num; i++)
	{
	
		one_node=cJSON_GetArrayItem(r8001Table,i);
		if(GetJsonDataPro(one_node,"BD_RM_MS", room_num)&&GetJsonDataPro(one_node,"IP_STATIC", ip_type))
		{
			if(memcmp(room_num,bd_rm_ms,len>8?8:len)==0&&strstr(ip_type,"STATIC")!=NULL)
			{
				if(GetJsonDataPro(one_node,"IP_ADDR", ip))
				{
					strcpy(record[cnt].BD_RM_MS,room_num);
					record[cnt].ip= inet_addr(ip);
					cnt++;
				}
			}
		}
		#if 0
		if(get_r8001_record_items_ip(i,ip_type,ip,room_num)==0)
		{
			//printf("33333333333FromR8001,%d,[%s][%s][%s]\n",i,ip_type,ip,room_num);
			if(memcmp(room_num,bd_rm_ms,len>8?8:len)==0&&strstr(ip_type,"STATIC")!=NULL)
			{
				printf("33333333333FromR8001,%d,[%s][%s][%s]\n",i,ip_type,ip,room_num);
				strcpy(record[cnt].BD_RM_MS,room_num);
				record[cnt].ip= inet_addr(ip);
				cnt++;
			}
		}
		#endif
	}
	return cnt;
}
cJSON *GetIxCallTargetByInput(char *input)
{
#if 1
	int getNum;
	cJSON *tar_list;
	cJSON *node;
	char ip_addr[20];
	int i;
	IpCacheRecord_T record[MAX_CALL_TARGET_NUM];
	if((getNum=GetIPByInput_FromR8001(input,record))==0)
	{
		getNum = API_GetIpByInput(input, record);
		if(getNum == 0)
			getNum = API_GetIpByInput(input, record);
	}
	if(getNum==0)
		return NULL;
	tar_list=cJSON_CreateArray();

	for(i = 0; i < getNum; i++)
	{
		node=cJSON_CreateObject();
		record[i].BD_RM_MS[10]=0;
		cJSON_AddStringToObject(node,PB_CALL_TAR_IxDevNum,record[i].BD_RM_MS);
		cJSON_AddStringToObject(node,PB_CALL_TAR_IxDevIP,my_inet_ntoa(record[i].ip,ip_addr));

		//dprintf("0000000000000000000000000000000 %s\n", record[i].BD_RM_MS);

		if(memcmp(record[i].BD_RM_MS+8,"01",2)==0)
		{
			cJSON_InsertItemInArray(tar_list,0,node);
		}
	#if defined(PID_IX611) || defined(PID_IX850) || defined(PID_IX622)|| defined(PID_IX821)
		else if(memcmp(record[i].BD_RM_MS+8,"50", 2) < 0)
		{
			cJSON_AddItemToArray(tar_list, node);
			//dprintf("111111111111111111111111111111111111 %s\n", record[i].BD_RM_MS);
		}
	#else
		else
		{
			//dprintf("222222222222222222222222222222222222 %s\n", record[i].BD_RM_MS);
			cJSON_AddItemToArray(tar_list, node);
		}
	#endif
	}
	return tar_list;
#endif
}


void IxCallScene1_Active_Wait_Process(CALLSERVER_STRU *Msg_CallServer)
{
	int i;
	char paraString[500]={0};
	cJSON *call_para;
	cJSON *call_target;
	cJSON *divert_target;
	cJSON *tar_list;
	switch(Msg_CallServer->call_type)
	{
		case IxCallScene1_Active:					//DOORCALL: DS-> IX-MASTER/IX-SLAVE
			
			switch(Msg_CallServer->msg_type)
			{
				case CallServer_Msg_Invite:
					log_i("IxMainCall:Start:%s",Msg_CallServer->json_para);
					linphone_becall_cancel();
					if(Get_IpCaller_State() != 0)
					{
						API_IpCaller_ForceClose();
					}
					if(JudgeIsMonitorTalking())
					{
						Monitor_IpTalkForceClose();
						api_video_s_service_turn_off();
					}
					call_para=cJSON_Parse(Msg_CallServer->json_para);
					if(call_para==NULL)
					{
						log_w("IxMainCall:para parse err");
						CallServer_Business_Respones(Msg_CallServer->msg_source_id,CallServer_Msg_Invite,1);
						//API_vbu_CallVideoOff();
						return;
					}

					API_PublicInfo_Write_Int(PB_IX_CALL_KEY, GetJsonObjectItemInt(call_para, PB_IX_CALL_KEY));

					call_target=cJSON_GetObjectItemCaseSensitive(call_para, CallPara_Target);
					if(call_target==NULL)
					{
						log_w("IxMainCall:have no target");
						cJSON_Delete(call_para);
						CallServer_Business_Respones(Msg_CallServer->msg_source_id,CallServer_Msg_Invite,1);
						//API_vbu_CallVideoOff();
						return;
					}
					if(CallServer_Run.target_array!=NULL)
					{
						cJSON_Delete(CallServer_Run.target_array);
						
					}
					CallServer_Run.target_array=cJSON_CreateObject();
					if(CallServer_Run.target_array==NULL)
					{
						//cJSON_Delete(CallServer_Run.target_array);
						log_w("DxMainCall:create target array err");
						cJSON_Delete(call_para);
						CallServer_Business_Respones(Msg_CallServer->msg_source_id,CallServer_Msg_Invite,1);
						//API_vbu_CallVideoOff();
						return;
					}
					API_PublicInfo_Write_Bool(PB_CALL_IX2Divert, 0);
					CallServer_Run.divert=0;
					CallServer_Run.caller_mask=0;
					if(strcmp(API_PublicInfo_Read_String(PB_SIP_CONNECT_NETWORK),"NONE")!=0)
					{
						divert_target=cJSON_GetObjectItemCaseSensitive(call_para, CallPara_Divert);
						if(divert_target!=NULL)
						{
							if(GetJsonDataPro(divert_target,CallTarget_SipAcc,paraString)!=0)
							{
								//cJSON_Delete(call_para);
								
								if(Get_SipCaller_State() != 0)
								{
									API_SipCaller_ForceClose();
								}
								cJSON_AddStringToObject(CallServer_Run.target_array,CallTarget_SipAcc,paraString);
								log_d("DxMainCall:start divert:%s",paraString);

								CallServer_Run.divert=1;
								CallServer_Run.state = CallServer_Invite;
								CallServer_Run.call_type = Msg_CallServer->call_type;
								API_SipCaller_Invite(SipCaller_Ix2SipCall);
								CallServer_Run.caller_mask|=0x02;
							}
						}
					}
					if(GetJsonDataPro(call_target,CallTarget_IxDev,&tar_list)==0)
					{
						if(CallServer_Run.caller_mask==0)
						{
							log_w("DxMainCall:unkown target");
							cJSON_Delete(call_para);
							IxCallScene1_Active_MenuDisplay_ToInviteFail(0);
							CallServer_Business_Respones(Msg_CallServer->msg_source_id,CallServer_Msg_Invite,1);
							//API_vbu_CallVideoOff();
						}
						return;
						
					}
					else
					{
						if(GetJsonDataPro(call_target,CallTarget_Input,paraString)==1)
						{
							cJSON_AddStringToObject(CallServer_Run.target_array,CallTarget_Input,paraString);
						}
						if(GetJsonDataPro(call_target,PB_CALL_TAR_NAME,paraString)==1)
						{
							cJSON_AddStringToObject(CallServer_Run.target_array,PB_CALL_TAR_NAME,paraString);
						}
						if(GetJsonDataPro(call_target,PB_CALL_TAR_IXADDR,paraString)==1)
						{
							cJSON_AddStringToObject(CallServer_Run.target_array,PB_CALL_TAR_IXADDR,paraString);
						}
						cJSON_AddItemToObject(CallServer_Run.target_array,CallTarget_IxDev,cJSON_Duplicate(tar_list,1));

						CallServer_Run.caller_mask|=1;
					}
					
					#if 1
					Call_Dev_Info dev_info;
					if(GetCallTarIxDevInfo(0,&dev_info)==0)
					{
						int link_ret;
						if((link_ret=Check_TargetDev_CallLink(dev_info.ip_addr,Business_State_BeMainCall))<0)		//czn_20190527
						{
							CallServer_Run.caller_mask&=(~0x01);
							if(CallServer_Run.caller_mask==0)
							{
								CallServer_Business_Respones(Msg_CallServer->msg_source_id,CallServer_Msg_Invite,1);
								if(link_ret==-3)
									Send_CallEvent("Calling_Busy", NULL);
								else
									Send_CallEvent("Calling_Error", NULL);
								if(link_ret==-1)
									API_Event_By_Name(Event_IXS_ReqUpdateTb);
								cJSON_Delete(call_para);
								//cJSON_Delete(tar_list);
								return;
							}
							
						}
					}
					#endif
					
					//cJSON_AddStringToObject(CallServer_Run.target_array,CallTarget_Input,paraString);
					//cJSON_AddItemToObject(CallServer_Run.target_array,CallTarget_IxDev,tar_list);
					cJSON_Delete(call_para);
					
					//CallServer_Business_Respones(Msg_CallServer->msg_source_id,CallServer_Msg_Invite,0);
					usleep(1000);
					
				
					
					

					
					
					CallServer_Run.state = CallServer_Invite;
					CallServer_Run.call_type = Msg_CallServer->call_type;
					CallServer_Run.with_local_menu = 1;
					
					CallServer_Run.timer = time(NULL);
					//CallServer_Run.caller_mask=1;
					if(CallServer_Run.caller_mask&0x01)
						API_IpCaller_Invite(IpCaller_IxSys);
					IxCallScene1_Active_MenuDisplay_ToInvite();
					
					break;

				default:
					break;
			}
			break;
			
	
		default:
			break;
	}
		
	
	
}



void IxCallScene1_Active_Invite_Process(CALLSERVER_STRU *Msg_CallServer)
{
	unsigned char dtbecalled_type=0,dtcaller_type=0,ipbecalled_type=0,ipcaller_type=0;
	cJSON *call_para;
	cJSON *hook_para;
	
	switch(Msg_CallServer->msg_type)
	{
		case CallServer_Msg_Invite:
			if(time(NULL) - CallServer_Run.timer > 5)
			{
				//CallServer_Run.state = CallServer_Wait;
				//IxCallScene3_MenuDisplay_ToWait();

				API_CallServer_Timeout();
				//BEEP_ERROR();
				IxCallScene1_Active_MenuDisplay_ToInviteFail(0);
			}
			CallServer_Business_Respones(Msg_CallServer->msg_source_id,CallServer_Msg_Invite,1);
			break;
			
		case CallServer_Msg_InviteOk:
			if(CallServer_Run.call_rule==CallRule_TransferIm&&CallServer_Run.divert==0)
			{
				CallServer_Run.caller_mask|=0x02;
				CallServer_Run.divert=1;
				cJSON_AddStringToObject(CallServer_Run.target_array,CallTarget_SipAcc,CallServer_Run.callee_master_sip_divert.sip_account);
				API_SipCaller_Invite(SipCaller_Ix2SipCall);
			}
			CallServer_Run.rule_act = 0;
			CallServer_Run.state = CallServer_Ring;
			CallServer_Run.timer = time(NULL);
			API_IpCaller_Ring(IpCaller_IxSys);
			IxCallScene1_Active_MenuDisplay_ToRing();					
			break;	

		case CallServer_Msg_InviteFail:
			
			API_IpCaller_Bye(IpCaller_IxSys);
			CallServer_Run.caller_mask&=(~0x01);
			if(CallServer_Run.caller_mask==0)
			{
				CallServer_Run.state = CallServer_Wait;
				//BEEP_ERROR();
				//IxCallScene3_MenuDisplay_ToWait();
				IxCallScene1_Active_MenuDisplay_ToInviteFail(1);
			}
			break;

		case CallServer_Msg_RemoteAck:
			call_para=cJSON_Parse(Msg_CallServer->json_para);
			#if 0
			if(call_para!=NULL&&(hook_para=cJSON_GetObjectItem(call_para,CallTarget_IxHook))!=NULL)
			{
				//CallServer_Run.target_hook_dev = Msg_CallServer->target_dev_list[0];
				CallServer_Run.state = CallServer_Ack;
				CallServer_Run.timer = time(NULL);
				cJSON_AddItemToObject(CallServer_Run.target_array,CallTarget_IxHook,cJSON_Duplicate(hook_para,1));
				
				API_IpCaller_Ack(ipcaller_type);
				IxCallScene1_Active_MenuDisplay_ToAck();
			}
			#endif
			if(call_para!=NULL&&(hook_para=cJSON_GetObjectItem(call_para,CallTarget_IxHook))!=NULL)
			{
				//CallServer_Run.target_hook_dev = Msg_CallServer->target_dev_list[0];
				CallServer_Run.state = CallServer_Ack;
				CallServer_Run.timer = time(NULL);
				cJSON_AddItemToObject(CallServer_Run.target_array,CallTarget_IxHook,cJSON_Duplicate(hook_para,1));
				
				API_IpCaller_Ack(ipcaller_type);
				if(CallServer_Run.divert==1)
				{
					CallServer_Run.caller_mask&=(~0x02);
					API_SipCaller_Bye(SipCaller_Ix2SipCall);
				}
				IxCallScene1_Active_MenuDisplay_ToAck();

				if(CallServer_Run.divert==1)
				{
					API_SipCaller_Cancel(SipCaller_Ix2SipCall);
				}
			}
			cJSON_Delete(call_para);
			
			break;

		case CallServer_Msg_LocalBye:
			Send_CallEvent("Calling_Cancel", NULL);
			API_IpCaller_Cancel(IpCaller_IxSys);
			CallServer_Run.state = CallServer_Wait;
			IxCallScene1_Active_MenuDisplay_ToWait();
			break;

		case CallServer_Msg_RemoteBye:
			API_IpCaller_Bye(IpCaller_IxSys);
			CallServer_Run.state = CallServer_Wait;
			IxCallScene1_Active_MenuDisplay_ToWait();
			break;

		case CallServer_Msg_Timeout:
			//API_IpCaller_ForceClose();
			API_IpCaller_Cancel(IpCaller_IxSys);
			CallServer_Run.state = CallServer_Wait;
			IxCallScene1_Active_MenuDisplay_ToWait();
			break;

		case CallServer_Msg_DtSrDisconnect:
			break;

		case CallServer_Msg_NetCallLinkDisconnect:
			API_IpCaller_ForceClose();
			CallServer_Run.state = CallServer_Wait;
			IxCallScene1_Active_MenuDisplay_ToWait();
			break;
		case CallServer_Msg_AppInviteFail:
			API_SipCaller_Cancel(SipCaller_Ix2SipCall);
			CallServer_Run.caller_mask&=(~0x02);
			if(CallServer_Run.caller_mask==0)//||CallServer_Run.call_rule == CallRule_TransferNoAck)
			{
				API_IpCaller_Cancel(ipcaller_type);
				CallServer_Run.state = CallServer_Wait;
				//BEEP_ERROR();
				//IxCallScene3_MenuDisplay_ToWait();
				IxCallScene1_Active_MenuDisplay_ToWait();
				
			}
			else
			{
				API_PublicInfo_Write(PB_CALL_INFO,GetCallserverStatePb());
				API_Event_By_Name(EventCallState);
			}
			break;
		case CallServer_Msg_AppBye:
			if(CallServer_Run.divert==1)
			{
				API_SipCaller_Cancel(SipCaller_Ix2SipCall);
				CallServer_Run.caller_mask&=(~0x02);
				if(CallServer_Run.caller_mask==0x00)//||CallServer_Run.call_rule == CallRule_TransferNoAck)
				{
					API_IpCaller_Cancel(ipcaller_type);
					CallServer_Run.state = CallServer_Wait;
					IxCallScene1_Active_MenuDisplay_ToWait();
				}
				else
				{
					API_PublicInfo_Write(PB_CALL_INFO,GetCallserverStatePb());
					API_Event_By_Name(EventCallState);
				}
			}
			break;
		case CallServer_Msg_AppInviteOk:
			if(CallServer_Run.divert==1)
			{
				API_SipCaller_Ring(SipCaller_Ix2SipCall);
				if(CallServer_Run.caller_mask==0x02)
				{
					CallServer_Run.state = CallServer_Ring;
					CallServer_Run.timer = time(NULL);
					//API_IpCaller_Ring(IpCaller_IxSys);
					IxCallScene1_Active_MenuDisplay_ToRing();
				}
			}
			
			break;
		case CallServer_Msg_AppAck:
			if(CallServer_Run.divert==1)
			{
				#if 0
				extern unsigned char myAddr;
				unsigned char addr_save;
				addr_save = myAddr;
			        myAddr = GetTargetDtAddr();
				
				#if 0
				if(CallServer_Run.state == CallServer_VirtualHook)
				{
					unsigned char ext_data[2]={0x55,0xaa};
					API_Stack_APT_Without_ACK_Data(addr_save,ST_TALK,2,ext_data); 
				}
				else
				#endif
				{
					API_Stack_APT_Without_ACK(addr_save,ST_TALK);
				}
				
			        //OS_Delay(200);
			        usleep(200*1000);
			        myAddr = addr_save;
				#endif
				#if 0
				API_DtCaller_Ack(DtCaller_MainCall);
				#else
				//API_DtCaller_Cancel(DtCaller_MainCall);
				#endif
				API_IpCaller_Cancel(ipcaller_type);
				cJSON_AddStringToObject(CallServer_Run.target_array,CallTarget_IxHook,"phone");
				API_SipCaller_Ack(SipCaller_Ix2SipCall);	
				
				CallServer_Run.state = CallServer_Ack;
				CallServer_Run.timer = time(NULL);
				IxCallScene1_Active_MenuDisplay_ToAppAck();
			}
			break;
		default:
			break;
	}
		
			
			
}

void IxCallScene1_Active_Ring_Process(CALLSERVER_STRU *Msg_CallServer)
{
	unsigned char ipcaller_type = 0;
	cJSON *call_para;
	cJSON *divert_target;
	cJSON *hook_para;
	char paraString[50];
			
	ipcaller_type = IpCaller_IxSys;
	
	
	switch(Msg_CallServer->msg_type)
	{
		case CallServer_Msg_Invite:
			if(time(NULL) - CallServer_Run.timer > 5)
			{
				//CallServer_Run.state = CallServer_Wait;
				//IxCallScene3_MenuDisplay_ToWait();

				API_CallServer_Timeout();
				//IxCallScene1_Active_MenuDisplay_ToInviteFail(1);
			}
			
			CallServer_Business_Respones(Msg_CallServer->msg_source_id,CallServer_Msg_Invite,1);
			break;

		// lzh_20180905_s
		case CallServer_Msg_RingNoAck:
			if( CallServer_Run.call_rule == CallRule_TransferNoAck && (CallServer_Run.caller_mask&0x02)==0)
			{
				//CallServer_Run.rule_act = 1;
				//CallServer_Run.state = CallServer_Transfer;
				CallServer_Run.caller_mask|=0x02;
				CallServer_Run.divert=1;
				cJSON_AddStringToObject(CallServer_Run.target_array,CallTarget_SipAcc,CallServer_Run.callee_master_sip_divert.sip_account);
				API_SipCaller_Invite(SipCaller_Ix2SipCall);
			}					
			break;
		// lzh_20180905_e

		
		case CallServer_Msg_RemoteAck:
			call_para=cJSON_Parse(Msg_CallServer->json_para);
			if(call_para!=NULL&&(hook_para=cJSON_GetObjectItem(call_para,CallTarget_IxHook))!=NULL)
			{
				//CallServer_Run.target_hook_dev = Msg_CallServer->target_dev_list[0];
				
				CallServer_Run.state = CallServer_Ack;
				CallServer_Run.timer = time(NULL);
				cJSON_AddItemToObject(CallServer_Run.target_array,CallTarget_IxHook,cJSON_Duplicate(hook_para,1));
				if(hook_para=cJSON_GetObjectItem(call_para,CallTarget_IxHookDivert))
				{
					cJSON_AddItemToObject(CallServer_Run.target_array,CallTarget_IxHookDivert,cJSON_Duplicate(hook_para,1));
				}
				API_IpCaller_Ack(ipcaller_type);
				if(CallServer_Run.divert==1)
				{
					CallServer_Run.caller_mask&=(~0x02);
					API_SipCaller_Bye(SipCaller_Ix2SipCall);
				}
				IxCallScene1_Active_MenuDisplay_ToAck();

				if(CallServer_Run.divert==1)
				{
					API_SipCaller_Cancel(SipCaller_Ix2SipCall);
				}
			}
			cJSON_Delete(call_para);
			break;

		case CallServer_Msg_LocalBye:
			Send_CallEvent("Calling_Cancel", NULL);
			API_IpCaller_Cancel(ipcaller_type);
			usleep(100*1000);
			CallServer_Run.state = CallServer_Wait;
			IxCallScene1_Active_MenuDisplay_ToWait();
			if(CallServer_Run.divert==1)
			{
				API_SipCaller_Cancel(SipCaller_Ix2SipCall);
			}
			break;

		case CallServer_Msg_RemoteBye:
			//CallServer_Run.target_hook_dev = Msg_CallServer->target_dev_list[0];

			API_IpCaller_Bye(ipcaller_type);
			CallServer_Run.caller_mask&=(~0x01);
			if(CallServer_Run.caller_mask==0x00)
			{
				CallServer_Run.state = CallServer_Wait;
				IxCallScene1_Active_MenuDisplay_ToWait();
				if(CallServer_Run.divert==1)
				{
					API_SipCaller_Cancel(SipCaller_Ix2SipCall);
				}
			}
			break;
			
		case CallServer_Msg_Timeout:
			// ���ж�Ϊת��״̬��ʱ�˳������������ʱת��
			if( CallServer_Run.call_rule == CallRule_TransferNoAck && (CallServer_Run.caller_mask&0x02)==0)
			{
				//CallServer_Run.rule_act = 1;
				//CallServer_Run.state = CallServer_Transfer;
				//API_IpCaller_ForceClose();
				InformIm_ToDivert();
				CallServer_Run.caller_mask|=0x02;
				CallServer_Run.divert=1;
				cJSON_AddStringToObject(CallServer_Run.target_array,CallTarget_SipAcc,CallServer_Run.callee_master_sip_divert.sip_account);
				API_SipCaller_Invite(SipCaller_Ix2SipCall);
			}
			else
			{
				API_IpCaller_Cancel(ipcaller_type); 				
				CallServer_Run.state = CallServer_Wait;
				IxCallScene1_Active_MenuDisplay_ToWait();		
				if(CallServer_Run.divert==1)
				{
					API_SipCaller_Cancel(SipCaller_Ix2SipCall);
				}
			}
			
			break;

		case CallServer_Msg_DtSrDisconnect:
			
			break;

		case CallServer_Msg_NetCallLinkDisconnect:
			API_IpCaller_ForceClose();
			CallServer_Run.state = CallServer_Wait;
			IxCallScene1_Active_MenuDisplay_ToWait();
			break;

		case CallServer_Msg_AppInviteFail:
			API_SipCaller_Cancel(SipCaller_Ix2SipCall);
			CallServer_Run.caller_mask&=(~0x02);
			if(CallServer_Run.caller_mask==0||CallServer_Run.call_rule == CallRule_TransferNoAck)
			{
				API_IpCaller_Cancel(ipcaller_type);
				CallServer_Run.state = CallServer_Wait;
				//BEEP_ERROR();
				//IxCallScene3_MenuDisplay_ToWait();
				IxCallScene1_Active_MenuDisplay_ToWait();
				
			}
			else
			{
				API_PublicInfo_Write(PB_CALL_INFO,GetCallserverStatePb());
				API_Event_By_Name(EventCallState);	
			}
			break;
		case CallServer_Msg_AppBye:
			if(CallServer_Run.divert==1)
			{
				API_SipCaller_Cancel(SipCaller_Ix2SipCall);
				if(CallServer_Run.caller_mask==0x02||CallServer_Run.call_rule == CallRule_TransferNoAck)
				{
					API_IpCaller_Cancel(ipcaller_type);
					CallServer_Run.state = CallServer_Wait;
					IxCallScene1_Active_MenuDisplay_ToWait();
				}
				else
				{
					CallServer_Run.caller_mask&=(~0x02);
					API_PublicInfo_Write(PB_CALL_INFO,GetCallserverStatePb());
					API_Event_By_Name(EventCallState);	
				}
				CallServer_Run.caller_mask&=(~0x02);
			}
			break;
		case CallServer_Msg_AppInviteOk:
			if(CallServer_Run.divert==1)
			{
				API_SipCaller_Ring(SipCaller_Ix2SipCall);
				Send_CallEvent("Calling_App_Succ", NULL);
			}
			
			break;
		case CallServer_Msg_AppAck:
			if(CallServer_Run.divert==1)
			{
				#if 0
				extern unsigned char myAddr;
				unsigned char addr_save;
				addr_save = myAddr;
			        myAddr = GetTargetDtAddr();
				
				#if 0
				if(CallServer_Run.state == CallServer_VirtualHook)
				{
					unsigned char ext_data[2]={0x55,0xaa};
					API_Stack_APT_Without_ACK_Data(addr_save,ST_TALK,2,ext_data); 
				}
				else
				#endif
				{
					API_Stack_APT_Without_ACK(addr_save,ST_TALK);
				}
				
			        //OS_Delay(200);
			        usleep(200*1000);
			        myAddr = addr_save;
				#endif
				#if 0
				API_DtCaller_Ack(DtCaller_MainCall);
				#else
				//API_DtCaller_Cancel(DtCaller_MainCall);
				#endif
				API_IpCaller_Cancel(ipcaller_type);
				cJSON_AddStringToObject(CallServer_Run.target_array,CallTarget_IxHook,"phone");
				API_SipCaller_Ack(SipCaller_Ix2SipCall);	
				
				CallServer_Run.state = CallServer_Ack;
				CallServer_Run.timer = time(NULL);
				IxCallScene1_Active_MenuDisplay_ToAppAck();
			}
			break;
		case CallServer_Msg_Transfer:	
			if(CallServer_Run.divert==1)
				return;
			call_para=cJSON_Parse(Msg_CallServer->json_para);
			if(call_para==NULL)
			{
				log_w("DxMainCall:para parse err");
				//CallServer_Business_Respones(Msg_CallServer->msg_source_id,CallServer_Msg_Invite,1);
				return;
			}
			divert_target=cJSON_GetObjectItemCaseSensitive(call_para, CallPara_Divert);
			if(divert_target==NULL)
			{
				log_w("DxMainCall:have no divert target");
				cJSON_Delete(call_para);
				//CallServer_Business_Respones(Msg_CallServer->msg_source_id,CallServer_Msg_Invite,1);
				return;
			}
			if(GetJsonDataPro(divert_target,CallTarget_SipAcc,paraString)==0)
			{
				log_w("DxMainCall:unkown target");
				cJSON_Delete(call_para);
				//CallServer_Business_Respones(Msg_CallServer->msg_source_id,CallServer_Msg_Invite,1);
				return;
			}
			cJSON_Delete(call_para);
			if(CallServer_Run.target_array==NULL)
			{
				CallServer_Run.target_array=cJSON_CreateObject();
			}
			
			if(CallServer_Run.target_array==NULL)
			{
				//cJSON_Delete(CallServer_Run.target_array);
				log_w("DxMainCall:create target array err");
				//CallServer_Business_Respones(Msg_CallServer->msg_source_id,CallServer_Msg_Invite,1);
				return;
			}
			if(Get_SipCaller_State() != 0)
			{
				API_SipCaller_ForceClose();
			}
			CallServer_Run.caller_mask|=0x02;
			cJSON_AddStringToObject(CallServer_Run.target_array,CallTarget_SipAcc,paraString);
			log_d("DxMainCall:start divert:%s",paraString);
			CallServer_Run.divert=1;
			API_SipCaller_Invite(SipCaller_Ix2SipCall);
			
			CallServer_Run.caller_mask&=(~0x01);
			API_IpCaller_Cancel(ipcaller_type);
			//API_Stack_APT_Without_ACK(DS1_ADDRESS,ST_TALK);
			API_PublicInfo_Write(PB_CALL_INFO,GetCallserverStatePb());
			API_Event_By_Name(EventCallState);
			break;
			
		default:
			break;
	}
		
}

void IxCallScene1_Active_Ack_Process(CALLSERVER_STRU *Msg_CallServer)
{
	unsigned char ipcaller_type = 0;

	//switch(Msg_CallServer->call_type)
	
			
			
	ipcaller_type = IpCaller_IxSys;

	switch(Msg_CallServer->msg_type)
	{
		case CallServer_Msg_Invite:
			
			if(time(NULL) - CallServer_Run.timer > 5)
			{
				//CallServer_Run.state = CallServer_Wait;
				//IxCallScene3_MenuDisplay_ToWait();

				API_CallServer_Timeout();
			}
			//IxCallScene1_Active_MenuDisplay_ToInviteFail(1);
			CallServer_Business_Respones(Msg_CallServer->msg_source_id,CallServer_Msg_Invite,1);
			break;
			
		case CallServer_Msg_LocalBye:
			Send_CallEvent("Calling_Cancel", NULL);
			API_IpCaller_Cancel(ipcaller_type);
			CallServer_Run.state = CallServer_Wait;
			IxCallScene1_Active_MenuDisplay_ToWait();
			if(CallServer_Run.divert==1)
			{
				API_SipCaller_Cancel(SipCaller_Ix2SipCall);
			}
			break;

		case CallServer_Msg_RemoteBye:
			API_IpCaller_Bye(ipcaller_type);
			CallServer_Run.state = CallServer_Wait;
			IxCallScene1_Active_MenuDisplay_ToWait();
			if(CallServer_Run.divert==1)
			{
				API_SipCaller_Cancel(SipCaller_Ix2SipCall);
			}
			break;
			
		case CallServer_Msg_Timeout:
			//API_DtCaller_ForceClose();
			API_IpCaller_Cancel(ipcaller_type);
			CallServer_Run.state = CallServer_Wait;
			IxCallScene1_Active_MenuDisplay_ToWait();
			if(CallServer_Run.divert==1)
			{
				API_SipCaller_Cancel(SipCaller_Ix2SipCall);
			}	
			break;

		case CallServer_Msg_DtSrDisconnect:
			break;

		case CallServer_Msg_NetCallLinkDisconnect:
			API_IpCaller_ForceClose();
			CallServer_Run.state = CallServer_Wait;
			IxCallScene1_Active_MenuDisplay_ToWait();
			if(CallServer_Run.divert==1)
			{
				API_SipCaller_Cancel(SipCaller_Ix2SipCall);
			}
			break;

		case CallServer_Msg_RemoteUnlock1:
			//if((!(CallServer_Run.with_local_menu & 0x01)) && memcmp(&CallServer_Run.t_addr,&Msg_CallServer->partner_addr,sizeof(Global_Addr_Stru)) == 0)
			{
				CallServer_Run.with_local_menu &= ~0x80;
				API_IpCaller_Unlock1(ipcaller_type);
			}
			break;

		case CallServer_Msg_RemoteUnlock2:
			//if((!(CallServer_Run.with_local_menu & 0x01)) && memcmp(&CallServer_Run.t_addr,&Msg_CallServer->partner_addr,sizeof(Global_Addr_Stru)) == 0)
			{
				CallServer_Run.with_local_menu &= ~0x80;
				API_IpCaller_Unlock2(ipcaller_type);
			}
			break;
		case CallServer_Msg_AppBye:
			if(CallServer_Run.divert==1)
			{
				API_IpCaller_Cancel(ipcaller_type);
				CallServer_Run.state = CallServer_Wait;
				
				API_SipCaller_Cancel(SipCaller_Ix2SipCall);
				IxCallScene1_Active_MenuDisplay_ToWait();
			}
			break;	

		default:
			break;
	}
			
	
}
void IxCallScene1_Active_SourceBye_Process(CALLSERVER_STRU *Msg_CallServer)
{
	//char detail[LOG_DESC_LEN + 1];
	
	switch(Msg_CallServer->call_type)
	{
		case IxCallScene1_Active:					//DOORCALL: DS-> DX-MASTER -> DX-SLAVE 
			switch(Msg_CallServer->msg_type)
			{
				case CallServer_Msg_Invite:
					break;
					
				case CallServer_Msg_RemoteRing:
					break;	

				case CallServer_Msg_LocalAck:
					break;	

				case CallServer_Msg_RemoteAck:
					break;

				case CallServer_Msg_LocalBye:
					break;

				case CallServer_Msg_RemoteBye:
					break;

				case CallServer_Msg_Timeout:
					break;

				case CallServer_Msg_DtSrDisconnect:
					break;

				case CallServer_Msg_NetCallLinkDisconnect:
					break;

				case CallServer_Msg_LocalUnlock1:
					break;

				case CallServer_Msg_LocalUnlock2:
					break;

				case CallServer_Msg_RemoteUnlock1:
					break;

				case CallServer_Msg_RemoteUnlock2:
					break;

				default:
					break;
			}
			break;
			
	
		default:
			break;
	}
	
}

void IxCallScene1_Active_TargetBye_Process(CALLSERVER_STRU *Msg_CallServer)
{
	//char detail[LOG_DESC_LEN + 1];

	switch(Msg_CallServer->call_type)
	{
		case IxCallScene1_Active:					//DOORCALL: DS-> DX-MASTER -> DX-SLAVE 
			switch(Msg_CallServer->msg_type)
			{
				case CallServer_Msg_Invite:
					break;
					
				case CallServer_Msg_RemoteRing:
					break;	

				case CallServer_Msg_LocalAck:
					break;	

				case CallServer_Msg_RemoteAck:
					break;

				case CallServer_Msg_LocalBye:
					break;

				case CallServer_Msg_RemoteBye:
					break;

				case CallServer_Msg_Timeout:
					break;

				case CallServer_Msg_DtSrDisconnect:
					break;

				case CallServer_Msg_NetCallLinkDisconnect:
					break;

				case CallServer_Msg_LocalUnlock1:
					break;

				case CallServer_Msg_LocalUnlock2:
					break;

				case CallServer_Msg_RemoteUnlock1:
					break;

				case CallServer_Msg_RemoteUnlock2:
					break;

				default:
					break;
			}
			break;
			
		default:
			break;
	}
	
}

void IxCallScene1_Active_Transfer_Process(CALLSERVER_STRU *Msg_CallServer)
{
#if 0
	unsigned char ipbecalled_type=0;
	#if 1
	switch(Msg_CallServer->call_type)
	{
		case IxCallScene1_Active:					//  DS call -> phone
			
			switch(Msg_CallServer->msg_type)
			{			
				case CallServer_Msg_Invite:
					break;
					
				case CallServer_Msg_RingNoAck:
					CallServer_Run.state = CallServer_TargetBye;
					API_IpCaller_Cancel(IpCaller_Transfer);
					CallServer_Run.state = CallServer_Wait;
					IxCallScene1_Active_MenuDisplay_ToWait();
					break;
					
				case CallServer_Msg_InviteOk:
					CallServer_Run.state = CallServer_Ring;
					CallServer_Run.with_local_menu = 0;
					CallServer_Run.timer = time(NULL);
					
					API_IpCaller_Ring(IpCaller_Transfer,NULL);
					IxCallScene1_Active_MenuDisplay_ToRing();
					break;
					
				case CallServer_Msg_InviteFail:
					API_IpCaller_Cancel(IpCaller_Transfer);
					CallServer_Run.state = CallServer_Wait;
					IxCallScene1_Active_MenuDisplay_ToWait();
					break;

				case CallServer_Msg_RemoteAck:
					CallServer_Run.state = CallServer_Ack;
					CallServer_Run.with_local_menu = 0;
					//CallServer_Run.t_addr = Msg_CallServer->partner_addr;
					CallServer_Run.timer = time(NULL);
					
					API_IpCaller_Ack(IpCaller_Transfer,NULL);
					IxCallScene1_Active_MenuDisplay_ToAck();
					break;

				case CallServer_Msg_LocalBye:					
					// lzh_20180914_s
					API_IpCaller_Cancel(IpCaller_Transfer);
					CallServer_Run.state = CallServer_Wait;
					IxCallScene1_Active_MenuDisplay_ToWait();					
					// lzh_20180914_s
					break;

				case CallServer_Msg_RemoteBye:
					if(Msg_CallServer->target_dev_num > 0 && Msg_CallServer->target_dev_list[0].ip_addr == CallServer_Run.target_dev_list[0].ip_addr)
					{
						CallServer_Run.state = CallServer_TargetBye;
						API_IpCaller_Cancel(IpCaller_Transfer);
						CallServer_Run.state = CallServer_Wait;					

						IxCallScene1_Active_MenuDisplay_ToWait();
					}
					break;

				case CallServer_Msg_Timeout:
					API_IpCaller_ForceClose();
					CallServer_Run.state = CallServer_Wait;
					IxCallScene1_Active_MenuDisplay_ToWait();
					break;

				case CallServer_Msg_NetCallLinkDisconnect:
					//API_DtBeCalled_Bye(dtbecalled_type);
					API_IpCaller_ForceClose();
					CallServer_Run.state = CallServer_Wait;
					IxCallScene1_Active_MenuDisplay_ToWait();
					break;

				case CallServer_Msg_LocalUnlock1:
					CallServer_Run.with_local_menu |= 0x80;
					//API_Beep(BEEP_TYPE_DL1);
					API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_BeCalledUnlock1);
					break;

				case CallServer_Msg_LocalUnlock2:
					CallServer_Run.with_local_menu |= 0x80;
					//API_Beep(BEEP_TYPE_DL1);
					API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_BeCalledUnlock2);
					break;

				case CallServer_Msg_RemoteUnlock1:
					CallServer_Run.with_local_menu &= ~0x80;
					API_IpCaller_Unlock1(IpCaller_Transfer,NULL);
					break;

				case CallServer_Msg_RemoteUnlock2:
					CallServer_Run.with_local_menu &= ~0x80;
					API_IpCaller_Unlock2(IpCaller_Transfer,NULL);
					break;

				default:
					break;
			}
			break;
			
		default:
			break;
	}
	#endif
#endif	
}



void IxCallScene1_Active_MenuDisplay_ToInvite(void)
{
	Send_CallEvent("Calling_Start", NULL);
	CreateNewCallID(CallServer_Run.call_id);
	if(CallServer_Run.source_info!=NULL)
	{
		cJSON_Delete(CallServer_Run.source_info);
	}
	CallServer_Run.source_info=cJSON_CreateObject();
	cJSON_AddStringToObject(CallServer_Run.source_info,"NAME",GetSysVerInfo_name());
	cJSON_AddStringToObject(CallServer_Run.source_info,PB_CALL_TAR_IxDevNum,GetSysVerInfo_BdRmMs());
	cJSON_AddStringToObject(CallServer_Run.source_info,PB_CALL_TAR_IxDevIP,GetSysVerInfo_IP());
	API_PublicInfo_Write(PB_CALL_INFO,GetCallserverStatePb());
	API_Event_By_Name(EventCallState);				
}

void IxCallScene1_Active_MenuDisplay_ToInviteFail(int fail_type)
{
	log_i("IxMainCall:ToInviteFail");
	API_PublicInfo_Write(PB_CALL_INFO,GetCallserverStatePb());
	API_Event_By_Name(EventCallState);
	Send_CallEvent("Calling_Error", "IxMainCall:ToInviteFail");
}


void IxCallScene1_Active_MenuDisplay_ToRing(void)
{
	log_d("IxMainCall:ToRing");
	API_PublicInfo_Write(PB_CALL_INFO,GetCallserverStatePb());
	API_Event_By_Name(EventCallState);
	Send_CallEvent("Calling_Succ", NULL);
}

void IxCallScene1_Active_MenuDisplay_ToAck(void)
{
	cJSON *au_para;
	char ip_addr[20];
	log_i("IxMainCall:ToAck");
	
	
	OpenAppTalk();
	au_para=cJSON_CreateObject();
	if(au_para!=NULL)
	{
		API_RingStop();
		//if(WaveGetState()!=0)
		//	usleep(100*1000);
		int try_cnt=0;
		int hook_divert=0;
		while(try_cnt++<100&&WaveGetState()!=0)
		{
			usleep(50*1000);
		}
		cJSON_AddStringToObject(au_para,TalkType,TalkType_LocalAndUdp);
		
		if(GetJsonDataPro(CallServer_Run.target_array, CallTarget_IxHookDivert, &hook_divert)&&hook_divert==1)
			cJSON_AddStringToObject(au_para,TalkVolPara,AU_PHONE);
		else
			API_PublicInfo_Write_Bool(PB_CALL_IX2Divert, 0);//CallServer_Run.ix2divert=0;
		
		GetCallTarIxDevHookIp(ip_addr);
		
		
		cJSON_AddStringToObject(au_para,TargetIP,ip_addr); 
			
		API_TalkService_on(au_para);

		cJSON_Delete(au_para);
	}
	usleep(200*1000);
	API_PublicInfo_Write(PB_CALL_INFO,GetCallserverStatePb());
	API_Event_By_Name(EventCallState);
}

void IxCallScene1_Active_MenuDisplay_ToBye(void)
{
	log_d("IxMainCall:ToBye");
	API_PublicInfo_Write_Int(PB_IX_CALL_KEY, 0);
	API_TalkService_off();
	API_PublicInfo_Write(PB_CALL_INFO,GetCallserverStatePb());
	API_Event_By_Name(EventCallState);
}

void IxCallScene1_Active_MenuDisplay_ToWait(void)
{
	log_i("IxMainCall:Close");	
	API_PublicInfo_Write_Int(PB_IX_CALL_KEY, 0);
	API_TalkService_off();
	API_PublicInfo_Write(PB_CALL_INFO,GetCallserverStatePb());
	API_Event_By_Name(EventCallState);
	Send_CallEvent("Calling_Close", NULL);
}

void IxCallScene1_Active_MenuDisplay_ToTransfer(void)
{	
	
}

void IxCallScene1_Active_MenuDisplay_ToAppAck(void)
{
	cJSON *au_para;
	char temp[100];
	char *ptr;
	char sip_ser[50];
	log_d("IxMainCall:ToAck");
	API_PublicInfo_Write(PB_CALL_INFO,GetCallserverStatePb());
	API_Event_By_Name(EventCallState);
	GetVtkMediaTransAuPort();
	OpenAppTalk();
	au_para=cJSON_CreateObject();
	if(au_para!=NULL)
	{
		
		cJSON_AddStringToObject(au_para,TalkType,TalkType_LocalAndRtp);
		get_sip_master_ser(sip_ser);
		if((ptr=strstr(sip_ser,":"))!=NULL)
		{
			*ptr=0;
		}
		sprintf(temp,"%s:%d",sip_ser,GetVtkMediaTransAuPort());
		#if 0
		cJSON_AddNumberToObject(au_para,MIC_VOLUME_PHY,-10); 
		cJSON_AddNumberToObject(au_para,MIC_GAIN_PHY,1); 
		cJSON_AddNumberToObject(au_para,SPK_VOLUME_PHY,-10);
		cJSON_AddNumberToObject(au_para,SPK_GAIN_PHY,3);
		#endif
		API_RingStop();
		//if(WaveGetState()!=0)
		//	usleep(100*1000);
		int try_cnt=0;
		while(try_cnt++<100&&WaveGetState()!=0)
		{
			usleep(50*1000);
		}
		API_TalkService_on(au_para);
		cJSON_Delete(au_para);
	}
	
}
#endif
