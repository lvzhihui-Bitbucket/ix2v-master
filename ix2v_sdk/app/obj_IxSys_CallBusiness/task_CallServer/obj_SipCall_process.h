#ifndef _obj_SipCall_process_h
#define _obj_SipCall_process_h


void SipCall_Wait_Process(CALLSERVER_STRU *Msg_CallServer);
void SipCall_Invite_Process(CALLSERVER_STRU *Msg_CallServer);
void SipCall_Ring_Process(CALLSERVER_STRU *Msg_CallServer);
void SipCall_Ack_Process(CALLSERVER_STRU *Msg_CallServer);
void SipCall_SourceBye_Process(CALLSERVER_STRU *Msg_CallServer);
void SipCall_TargetBye_Process(CALLSERVER_STRU *Msg_CallServer);
void SipCall_Transfer_Process(CALLSERVER_STRU *Msg_CallServer);

void SipCall_Inform_ToInvite(void);
void SipCall_Inform_ToRing(void);
void SipCall_Inform_ToAck(void);
void SipCall_Inform_ToBye(void);
void SipCall_Inform_ToWait(void);
void SipCall_Inform_ToTransfer(void);
void SipCall_Inform_ToInviteFail(int fail_type);
void SipCall_Inform_ToUnlock(int lock_id);

#endif
