#include <stdio.h>
#include "task_CallServer.h"
#include "obj_BeCalled_State.h"
#include "obj_Caller_State.h"
//#include "../task_DtBeCalled/task_DtBeCalled.h"
//#include "../task_DtCaller/task_DtCaller.h"
//#include "task_IpBeCalled.h"
//#include "task_IpCaller.h"
#include "task_DtCaller.h"
#include "task_SipCaller.h"
#include "elog_forcall.h"
#include "task_Event.h"
#include "define_Command.h"
#include "task_StackAI.h"
#include "task_Event.h"
#include "obj_Talk_Servi.h"
#include "obj_UnitSR.h"
#if 0
#include "task_VoicePrompt.h"
#include "task_Ring.h"
#include "task_Power.h"
#include "task_Led.h"
#include "task_Ring.h"
#include "task_Beeper.h"
#include "obj_TableProcess.h"
#include "obj_call_record.h"
#include "obj_memo.h"
#include "task_IoServer.h"
#include "task_VideoMenu.h"
#include "obj_menu_data.h"

#include 	"obj_DxMainCall_process.h"
#include 	"MENU_010_SipConfig.h"
#include 	"obj_VideoProxySetting.h"
#include	"obj_CallTransferPara.h"

extern CALL_RECORD_DAT_T call_record_temp;
extern int call_record_flag;
CallTransferRunningParaStru_t TargetCallTransferRunningPara;

Global_Addr_Stru GetLocal_IpGlobalAddr(void);

// lzh_20180816_s
int API_CameraCtrl(int onoff);
// lzh_20180816_e
#endif



void SipCall_Wait_Process(CALLSERVER_STRU *Msg_CallServer)
{

	int i;
	char paraString[500]={0};
	cJSON *call_para;
	cJSON *call_target;
	cJSON *divert_target;
	int dt_addr;
	char *event_str;
	cJSON *event;
	int room_num;
	char room_str[10];
	switch(Msg_CallServer->call_type)
	{
		case SipCall:					//DOORCALL: DS-> IX-MASTER/IX-SLAVE
		case VMSipCall:	
			switch(Msg_CallServer->msg_type)
			{
				case CallServer_Msg_Invite:
					
					log_i("SipCall:Start:%s",Msg_CallServer->json_para);
					//ClearMonSrRequest();
					linphone_becall_cancel();
					if(Get_SipCaller_State() != 0)
					{
						API_SipCaller_ForceClose();
					}
					call_para=cJSON_Parse(Msg_CallServer->json_para);
					if(call_para==NULL)
					{
						log_w("SipCall:para parse err");
						CallServer_Business_Respones(Msg_CallServer->msg_source_id,CallServer_Msg_Invite,1);
						//API_vbu_CallVideoOff();
						return;
					}
					printf_json(call_para,__func__);
					#if 0
					call_target=cJSON_GetObjectItemCaseSensitive(call_para, CallPara_Target);
					if(call_target==NULL)
					{
						log_w("SipCall:have no target");
						cJSON_Delete(call_para);
						CallServer_Business_Respones(Msg_CallServer->msg_source_id,CallServer_Msg_Invite,1);
						//API_vbu_CallVideoOff();
						return;
					}
					#endif
					//cJSON_Delete(call_para);
					if(CallServer_Run.target_array!=NULL)
					{
						cJSON_Delete(CallServer_Run.target_array);
						
					}
					CallServer_Run.target_array=cJSON_CreateObject();
					if(CallServer_Run.target_array==NULL)
					{
						//cJSON_Delete(CallServer_Run.target_array);
						log_w("SipCall:create target array err");
						cJSON_Delete(call_para);
						CallServer_Business_Respones(Msg_CallServer->msg_source_id,CallServer_Msg_Invite,1);
						//API_vbu_CallVideoOff();
						return;
					}
					
					

					
					
					CallServer_Run.divert=0;
					CallServer_Run.caller_mask=0;
					//call_para=cJSON_Parse(Msg_CallServer->json_para);
					//if(call_para!=NULL)
					call_target=cJSON_GetObjectItemCaseSensitive(call_para, CallPara_Target);
					if(strcmp(API_PublicInfo_Read_String(PB_SIP_CONNECT_NETWORK),"NONE")!=0)
					{
						printf_json(call_para,"2222222");
						//divert_target=cJSON_GetObjectItemCaseSensitive(call_para, CallPara_Divert);
						//if(divert_target!=NULL)
						{
							if(GetJsonDataPro(call_para,CallTarget_SipAcc,paraString)!=0||(call_target&&GetJsonDataPro(call_target,CallTarget_SipAcc,paraString)!=0))
							{
								printf_json(call_para,paraString);
								//cJSON_Delete(call_para);
								
								if(Get_SipCaller_State() != 0)
								{
									API_SipCaller_ForceClose();
								}
								cJSON_AddStringToObject(CallServer_Run.target_array,CallTarget_SipAcc,paraString);
								log_d("SipCall:start divert:%s",paraString);

								CallServer_Run.divert=1;
								CallServer_Run.state = CallServer_Invite;
								CallServer_Run.call_type = Msg_CallServer->call_type;
								API_SipCaller_Invite(SipCaller_Ix2SipCall);
								CallServer_Run.caller_mask|=0x02;
								if(GetJsonDataPro(call_target,CallTarget_Input,paraString)==1)
								{
									cJSON_AddStringToObject(CallServer_Run.target_array,CallTarget_Input,paraString);
								}
								if(GetJsonDataPro(call_target,PB_CALL_TAR_NAME,paraString)==1)
								{
									cJSON_AddStringToObject(CallServer_Run.target_array,PB_CALL_TAR_NAME,paraString);
								}
								if(GetJsonDataPro(call_target,PB_CALL_TAR_IXADDR,paraString)==1)
								{
									cJSON_AddStringToObject(CallServer_Run.target_array,PB_CALL_TAR_IXADDR,paraString);
								}

								cJSON *tar_list;
								if(GetJsonDataPro(call_target,CallTarget_IxDev,&tar_list))
									cJSON_AddItemToObject(CallServer_Run.target_array,CallTarget_IxDev,cJSON_Duplicate(tar_list,1));
							}
						}
					}
					
					if(CallServer_Run.caller_mask==0)
					{
						log_w("SipCall:unkown target");
						cJSON_Delete(call_para);
						CallServer_Business_Respones(Msg_CallServer->msg_source_id,CallServer_Msg_Invite,1);
						//API_vbu_CallVideoOff();
						return;
					}
					cJSON_Delete(call_para);
					Send_CallEvent("Calling_Start", NULL);
					//usleep(200*1000);

					
					
					
					
					
					CallServer_Run.state = CallServer_Invite;
					CallServer_Run.call_type = Msg_CallServer->call_type;
					
					CallServer_Run.timer = time(NULL);
					
					
					CallServer_Business_Respones(Msg_CallServer->msg_source_id,CallServer_Msg_Invite,0);
					
					SipCall_Inform_ToInvite();
					
					
					
					
					
					
					//API_add_log_item(LOG_CallTest_Level+1,Log_CallTest_Title,"Call Start",NULL);
					break;

				default:
					break;
			}
			break;
			
	
		default:
			break;
	}
		
	
	
}



void SipCall_Invite_Process(CALLSERVER_STRU *Msg_CallServer)
{
	unsigned char dtbecalled_type=0,dtcaller_type=0,ipbecalled_type=0,ipcaller_type=0;

	//switch(Msg_CallServer->call_type)
	{
		//case IxCallScene1_Active:					//DOORCALL: DS-> DX-MASTER -> DX-SLAVE 
			switch(Msg_CallServer->msg_type)
			{
				case CallServer_Msg_Invite:
					if(time(NULL) - CallServer_Run.timer > 5)
					{
						//CallServer_Run.state = CallServer_Wait;
						//IxCallScene3_MenuDisplay_ToWait();

						API_CallServer_Timeout();
					}
					//BEEP_ERROR();
					SipCall_Inform_ToInviteFail(0);
					CallServer_Business_Respones(Msg_CallServer->msg_source_id,CallServer_Msg_Invite,1);
					break;
					
				
				case CallServer_Msg_AppInviteFail:
					API_SipCaller_Cancel(SipCaller_Ix2SipCall);
					CallServer_Run.caller_mask&=(~0x02);
					if(CallServer_Run.caller_mask==0)
					{
						CallServer_Run.state = CallServer_Wait;
						//BEEP_ERROR();
						//IxCallScene3_MenuDisplay_ToWait();
						SipCall_Inform_ToInviteFail(1);
						
					}
					break;


				case CallServer_Msg_Timeout:
					//API_IpCaller_ForceClose();
					//API_DtCaller_Cancel(DtCaller_MainCall);
					CallServer_Run.state = CallServer_Wait;
					SipCall_Inform_ToWait();
					if(CallServer_Run.divert==1)
					{
						API_SipCaller_Cancel(SipCaller_Ix2SipCall);
					}
					break;

				case CallServer_Msg_DtSrDisconnect:
					break;

				case CallServer_Msg_NetCallLinkDisconnect:
					//API_DtCaller_ForceClose();
					CallServer_Run.state = CallServer_Wait;
					SipCall_Inform_ToWait();
					if(CallServer_Run.divert==1)
					{
						API_SipCaller_Cancel(SipCaller_Ix2SipCall);
					}
					break;
					
				case CallServer_Msg_AppBye:
					if(CallServer_Run.divert==1)
					{
						API_SipCaller_Cancel(SipCaller_Ix2SipCall);
					}
					if(CallServer_Run.caller_mask==0x02)
					{
						CallServer_Run.state = CallServer_Wait;
						SipCall_Inform_ToWait();
					}
					break;
				case CallServer_Msg_AppInviteOk:
					if(CallServer_Run.divert==1)
					{
						API_SipCaller_Ring(SipCaller_Ix2SipCall);
					}
					if(CallServer_Run.caller_mask==0x02)
					{
						CallServer_Run.state = CallServer_Ring;
						CallServer_Run.timer = time(NULL);
						SipCall_Inform_ToRing();
					}
					break;
				case CallServer_Msg_AppAck:
					if(CallServer_Run.divert==1)
					{
						#if 0
						extern unsigned char myAddr;
						unsigned char addr_save;
						addr_save = myAddr;
					        myAddr = GetTargetDtAddr();
						
						#if 0
						if(CallServer_Run.state == CallServer_VirtualHook)
						{
							unsigned char ext_data[2]={0x55,0xaa};
							API_Stack_APT_Without_ACK_Data(addr_save,ST_TALK,2,ext_data); 
						}
						else
						#endif
						{
							API_Stack_APT_Without_ACK(addr_save,ST_TALK);
						}
						
					        //OS_Delay(200);
					        usleep(200*1000);
					        myAddr = addr_save;
						#endif
						#if 0
						API_DtCaller_Ack(DtCaller_MainCall);
						#else
						//API_DtCaller_Cancel(DtCaller_MainCall);
						#endif
						API_SipCaller_Ack(SipCaller_Ix2SipCall);	
						
						CallServer_Run.state = CallServer_Ack;
						CallServer_Run.timer = time(NULL);
						SipCall_Inform_ToAppAck();
					}
					break;
				case CallServer_Msg_LocalBye:
					Send_CallEvent("Calling_Cancel", NULL);
					//API_DtCaller_Cancel(DtCaller_MainCall);
					CallServer_Run.state = CallServer_Wait;
					SipCall_Inform_ToWait();
					//if(CallServer_Run.divert==1)
					{
						API_SipCaller_Cancel(SipCaller_Ix2SipCall);
					}
					break;
				default:
					break;
			}
			
	}
}

void SipCall_Ring_Process(CALLSERVER_STRU *Msg_CallServer)
{
	cJSON *call_para;
	cJSON *divert_target;
	char paraString[50];
	//switch(Msg_CallServer->call_type)
	{
		//case IxCallScene1_Active:					//DOORCALL: DS-> IX-MASTER/IX-SLAVE

			// lzh_20180907_s
			// 因为转呼接收到振铃信号后CallServer_Run.state从CallServer_Transfer状态切换为CallServer_Ring状态故调用API_IpCaller_Ack时
			// 需要知道类型
			
			switch(Msg_CallServer->msg_type)
			{
				case CallServer_Msg_Invite:
					if(time(NULL) - CallServer_Run.timer > 5)
					{
						//CallServer_Run.state = CallServer_Wait;
						//IxCallScene3_MenuDisplay_ToWait();
						API_CallServer_Timeout();
					}
					SipCall_Inform_ToInviteFail(1);
					CallServer_Business_Respones(Msg_CallServer->msg_source_id,CallServer_Msg_Invite,1);
					break;
				case CallServer_Msg_Redail:
					if((time(NULL) - CallServer_Run.timer > 3)&&(CallServer_Run.caller_mask&0x01))
					{
						Callback_DtCaller_ToRedial_MainCallIM(NULL);
					}
					break;
					
				case CallServer_Msg_AppInviteFail:
					API_SipCaller_Cancel(SipCaller_Ix2SipCall);
					CallServer_Run.caller_mask&=(~0x02);
					if(CallServer_Run.caller_mask==0)
					{
						CallServer_Run.state = CallServer_Wait;
						//BEEP_ERROR();
						//IxCallScene3_MenuDisplay_ToWait();
						SipCall_Inform_ToWait();
						
					}
					break;	

				case CallServer_Msg_LocalBye:
					Send_CallEvent("Calling_Cancel", NULL);
					//API_DtCaller_Cancel(DtCaller_MainCall);
					CallServer_Run.state = CallServer_Wait;
					SipCall_Inform_ToWait();
					//if(CallServer_Run.divert==1)
					{
						API_SipCaller_Cancel(SipCaller_Ix2SipCall);
					}
					break;
					
				case CallServer_Msg_Timeout:
					
					//API_DtCaller_Cancel(DtCaller_MainCall); 				
					CallServer_Run.state = CallServer_Wait;
					SipCall_Inform_ToWait();
					if(CallServer_Run.divert==1)
					{
						API_SipCaller_Cancel(SipCaller_Ix2SipCall);
					}
						
					break;

				case CallServer_Msg_DtSrDisconnect:
					
					break;

				case CallServer_Msg_NetCallLinkDisconnect:
					//API_DtCaller_ForceClose();
					CallServer_Run.state = CallServer_Wait;
					SipCall_Inform_ToWait();
					if(CallServer_Run.divert==1)
					{
						API_SipCaller_Cancel(SipCaller_Ix2SipCall);
					}
					break;
				case CallServer_Msg_RemoteUnlock1:
					SipCall_Inform_ToUnlock(1);
					break;

				case CallServer_Msg_RemoteUnlock2:
					SipCall_Inform_ToUnlock(2);
					break;

				case CallServer_Msg_AppBye:
					if(CallServer_Run.divert==1)
					{
						API_SipCaller_Cancel(SipCaller_Ix2SipCall);
						if(CallServer_Run.caller_mask==0x02)
						{
							CallServer_Run.state = CallServer_Wait;
							SipCall_Inform_ToWait();
						}
					}
					break;
				case CallServer_Msg_AppInviteOk:
					if(CallServer_Run.divert==1)
					{
						API_SipCaller_Ring(SipCaller_Ix2SipCall);
					}
					
					break;
				case CallServer_Msg_AppAck:
					if(CallServer_Run.divert==1)
					{
						#if 0
						extern unsigned char myAddr;
						unsigned char addr_save;
						addr_save = myAddr;
					        myAddr = GetTargetDtAddr();
						
						#if 0
						if(CallServer_Run.state == CallServer_VirtualHook)
						{
							unsigned char ext_data[2]={0x55,0xaa};
							API_Stack_APT_Without_ACK_Data(addr_save,ST_TALK,2,ext_data); 
						}
						else
						#endif
						{
							API_Stack_APT_Without_ACK(addr_save,ST_TALK);
						}
						
					        //OS_Delay(200);
					        usleep(200*1000);
					        myAddr = addr_save;
						#endif
						#if 0
						API_DtCaller_Ack(DtCaller_MainCall);
						#else
						//API_DtCaller_Cancel(DtCaller_MainCall);
						#endif
						API_SipCaller_Ack(SipCaller_Ix2SipCall);	
						
						CallServer_Run.state = CallServer_Ack;
						CallServer_Run.timer = time(NULL);
						SipCall_Inform_ToAppAck();
					}
					break;
				default:
					break;
			}
			
	}
	
}

void SipCall_Ack_Process(CALLSERVER_STRU *Msg_CallServer)
{
	//unsigned char ipcaller_type = 0;

	//switch(Msg_CallServer->call_type)
	{
		//case IxCallScene1_Active:					//DOORCALL: DS-> DX-MASTER -> DX-SLAVE 

			// lzh_20180907_s
			// 因为转呼接收到振铃信号后CallServer_Run.state从CallServer_Transfer状态切换为CallServer_Ring状态故调用API_IpCaller_Ack时
			// 需要知道类型
			switch(Msg_CallServer->msg_type)
			{
				case CallServer_Msg_Invite:
					if(time(NULL) - CallServer_Run.timer > 5)
					{
						//CallServer_Run.state = CallServer_Wait;
						//IxCallScene3_MenuDisplay_ToWait();

						API_CallServer_Timeout();
					}
					SipCall_Inform_ToInviteFail(1);
					CallServer_Business_Respones(Msg_CallServer->msg_source_id,CallServer_Msg_Invite,1);
					break;

				case CallServer_Msg_NetCallLinkDisconnect:
					//API_DtCaller_ForceClose();
					CallServer_Run.state = CallServer_Wait;
					SipCall_Inform_ToWait();
					if(CallServer_Run.divert==1)
					{
						API_SipCaller_Cancel(SipCaller_Ix2SipCall);
					}
					break;
				case CallServer_Msg_LocalBye:
					Send_CallEvent("Calling_Cancel", NULL);
					//API_DtCaller_Cancel(DtCaller_MainCall);
					CallServer_Run.state = CallServer_Wait;
					SipCall_Inform_ToWait();
					//if(CallServer_Run.divert==1)
					{
						API_SipCaller_Cancel(SipCaller_Ix2SipCall);
					}
					break;
				case CallServer_Msg_RemoteUnlock1:
					SipCall_Inform_ToUnlock(1);
					break;

				case CallServer_Msg_RemoteUnlock2:
					SipCall_Inform_ToUnlock(2);
					break;

				case CallServer_Msg_AppBye:
					if(CallServer_Run.divert==1)
					{
						//API_DtCaller_Cancel(DtCaller_MainCall);
						CallServer_Run.state = CallServer_Wait;
						
						API_SipCaller_Cancel(SipCaller_Ix2SipCall);
						SipCall_Inform_ToWait();
					}
					break;

				default:
					break;
			}
			
	}
	
	
}
void SipCall_SourceBye_Process(CALLSERVER_STRU *Msg_CallServer)
{
	//char detail[LOG_DESC_LEN + 1];
	
	//switch(Msg_CallServer->call_type)
	{
		//case IxCallScene1_Active:					//DOORCALL: DS-> DX-MASTER -> DX-SLAVE 
			switch(Msg_CallServer->msg_type)
			{
				case CallServer_Msg_Invite:
					break;
					
				case CallServer_Msg_RemoteRing:
					break;	

				case CallServer_Msg_LocalAck:
					break;	

				case CallServer_Msg_RemoteAck:
					break;

				case CallServer_Msg_LocalBye:
					break;

				case CallServer_Msg_RemoteBye:
					break;

				case CallServer_Msg_Timeout:
					break;

				case CallServer_Msg_DtSrDisconnect:
					break;

				case CallServer_Msg_NetCallLinkDisconnect:
					break;

				case CallServer_Msg_LocalUnlock1:
					break;

				case CallServer_Msg_LocalUnlock2:
					break;

				case CallServer_Msg_RemoteUnlock1:
					break;

				case CallServer_Msg_RemoteUnlock2:
					break;

				default:
					break;
			}
			
	}
	
}

void SipCall_TargetBye_Process(CALLSERVER_STRU *Msg_CallServer)
{
	//char detail[LOG_DESC_LEN + 1];

	//switch(Msg_CallServer->call_type)
	{
		//case IxCallScene1_Active:					//DOORCALL: DS-> DX-MASTER -> DX-SLAVE 
			switch(Msg_CallServer->msg_type)
			{
				case CallServer_Msg_Invite:
					break;
					
				case CallServer_Msg_RemoteRing:
					break;	

				case CallServer_Msg_LocalAck:
					break;	

				case CallServer_Msg_RemoteAck:
					break;

				case CallServer_Msg_LocalBye:
					break;

				case CallServer_Msg_RemoteBye:
					break;

				case CallServer_Msg_Timeout:
					break;

				case CallServer_Msg_DtSrDisconnect:
					break;

				case CallServer_Msg_NetCallLinkDisconnect:
					break;

				case CallServer_Msg_LocalUnlock1:
					break;

				case CallServer_Msg_LocalUnlock2:
					break;

				case CallServer_Msg_RemoteUnlock1:
					break;

				case CallServer_Msg_RemoteUnlock2:
					break;

				default:
					break;
			}
		
	}
	
}

void SipCall_Transfer_Process(CALLSERVER_STRU *Msg_CallServer)
{
	unsigned char ipbecalled_type=0;
	#if 0
	//switch(Msg_CallServer->call_type)
	{
		//case IxCallScene1_Active:					//  DS call -> phone
			
			switch(Msg_CallServer->msg_type)
			{			
				case CallServer_Msg_Invite:
					break;
					
				case CallServer_Msg_RingNoAck:
					CallServer_Run.state = CallServer_TargetBye;
					API_IpCaller_Cancel(IpCaller_Transfer);
					CallServer_Run.state = CallServer_Wait;
					IxCallScene1_Active_MenuDisplay_ToWait();
					break;
					
				case CallServer_Msg_InviteOk:
					CallServer_Run.state = CallServer_Ring;
					CallServer_Run.with_local_menu = 0;
					CallServer_Run.timer = time(NULL);
					
					API_IpCaller_Ring(IpCaller_Transfer,NULL);
					IxCallScene1_Active_MenuDisplay_ToRing();
					break;
					
				case CallServer_Msg_InviteFail:
					API_IpCaller_Cancel(IpCaller_Transfer);
					CallServer_Run.state = CallServer_Wait;
					IxCallScene1_Active_MenuDisplay_ToWait();
					break;

				case CallServer_Msg_RemoteAck:
					CallServer_Run.state = CallServer_Ack;
					CallServer_Run.with_local_menu = 0;
					//CallServer_Run.t_addr = Msg_CallServer->partner_addr;
					CallServer_Run.timer = time(NULL);
					
					API_IpCaller_Ack(IpCaller_Transfer,NULL);
					IxCallScene1_Active_MenuDisplay_ToAck();
					break;

				case CallServer_Msg_LocalBye:					
					// lzh_20180914_s
					API_IpCaller_Cancel(IpCaller_Transfer);
					CallServer_Run.state = CallServer_Wait;
					IxCallScene1_Active_MenuDisplay_ToWait();					
					// lzh_20180914_s
					break;

				case CallServer_Msg_RemoteBye:
					if(Msg_CallServer->target_dev_num > 0 && Msg_CallServer->target_dev_list[0].ip_addr == CallServer_Run.target_dev_list[0].ip_addr)
					{
						CallServer_Run.state = CallServer_TargetBye;
						API_IpCaller_Cancel(IpCaller_Transfer);
						CallServer_Run.state = CallServer_Wait;					

						IxCallScene1_Active_MenuDisplay_ToWait();
					}
					break;

				case CallServer_Msg_Timeout:
					API_IpCaller_ForceClose();
					CallServer_Run.state = CallServer_Wait;
					IxCallScene1_Active_MenuDisplay_ToWait();
					break;

				case CallServer_Msg_NetCallLinkDisconnect:
					//API_DtBeCalled_Bye(dtbecalled_type);
					API_IpCaller_ForceClose();
					CallServer_Run.state = CallServer_Wait;
					IxCallScene1_Active_MenuDisplay_ToWait();
					break;

				case CallServer_Msg_LocalUnlock1:
					CallServer_Run.with_local_menu |= 0x80;
					//API_Beep(BEEP_TYPE_DL1);
					API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_BeCalledUnlock1);
					break;

				case CallServer_Msg_LocalUnlock2:
					CallServer_Run.with_local_menu |= 0x80;
					//API_Beep(BEEP_TYPE_DL1);
					API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_BeCalledUnlock2);
					break;

				case CallServer_Msg_RemoteUnlock1:
					CallServer_Run.with_local_menu &= ~0x80;
					API_IpCaller_Unlock1(IpCaller_Transfer,NULL);
					break;

				case CallServer_Msg_RemoteUnlock2:
					CallServer_Run.with_local_menu &= ~0x80;
					API_IpCaller_Unlock2(IpCaller_Transfer,NULL);
					break;

				default:
					break;
			}
			
	}
	#endif
	
}



void SipCall_Inform_ToInvite(void)
{
	log_d("SipCall:ToInvite");
	CreateNewCallID(CallServer_Run.call_id);
	if(CallServer_Run.source_info!=NULL)
	{
		cJSON_Delete(CallServer_Run.source_info);
	}
	CallServer_Run.source_info=cJSON_CreateObject();
	cJSON_AddStringToObject(CallServer_Run.source_info,"NAME",GetSysVerInfo_name());
	cJSON_AddStringToObject(CallServer_Run.source_info,PB_CALL_TAR_IxDevNum,GetSysVerInfo_BdRmMs());
	cJSON_AddStringToObject(CallServer_Run.source_info,PB_CALL_TAR_IxDevIP,GetSysVerInfo_IP());
	API_PublicInfo_Write(PB_CALL_INFO,GetCallserverStatePb());
	API_Event_By_Name(EventCallState);
	//Send_CallEvent("Calling_Start");
}

void SipCall_Inform_ToInviteFail(int fail_type)
{
	log_i("SipCall:ToInviteFail");
	Send_CallEvent("Calling_Error", "SipCall:ToInviteFail");
	API_PublicInfo_Write(PB_CALL_INFO,GetCallserverStatePb());
	API_Event_By_Name(EventCallState);
}


void SipCall_Inform_ToRing(void)
{
	
	
	API_PublicInfo_Write(PB_CALL_INFO,GetCallserverStatePb());	
	API_Event_By_Name(EventCallState);
	log_d("SipCall:ToRing");
	Send_CallEvent("Calling_Succ", NULL);
	
	
	//video_turn_on_init();
	//api_ak_vi_ch_stream_preview_on(0,0); 
	//open_call_video();
	//API_vbu_CallVideoOn();
}

void SipCall_Inform_ToAck(void)
{
	log_d("SipCall:ToAck");
	API_PublicInfo_Write(PB_CALL_INFO,GetCallserverStatePb());
	API_Event_By_Name(EventCallState);
	OpenDtTalk();
	SetCallDsViToFull();
}

void SipCall_Inform_ToAppAck(void)
{
	cJSON *au_para;
	char temp[100];
	char *ptr;
	char sip_ser[50];
	log_d("SipCall:ToAck");
	API_PublicInfo_Write(PB_CALL_INFO,GetCallserverStatePb());
	API_Event_By_Name(EventCallState);
	GetVtkMediaTransAuPort();
	OpenAppTalk();
	au_para=cJSON_CreateObject();
	if(au_para!=NULL)
	{
		API_RingStop();
		//if(WaveGetState()!=0)
		//	usleep(100*1000);
		int try_cnt=0;
		int hook_divert=0;
		while(try_cnt++<100&&WaveGetState()!=0)
		{
			usleep(50*1000);
		}
		cJSON_AddStringToObject(au_para,TalkType,TalkType_LocalAndRtp);
		get_sip_master_ser(sip_ser);
		if((ptr=strstr(sip_ser,":"))!=NULL)
		{
			*ptr=0;
		}
		sprintf(temp,"%s:%d",sip_ser,GetVtkMediaTransAuPort());
		
		API_TalkService_on(au_para);
		cJSON_Delete(au_para);
	}
	
}
void SipCall_Inform_ToBye(void)
{
	log_i("SipCall:Close1");
	//api_ak_vi_ch_stream_preview_off(0);
	//close_call_video();
	
	API_PublicInfo_Write(PB_CALL_INFO,GetCallserverStatePb());
	API_Event_By_Name(EventCallState);
	API_TalkService_off();
}

void SipCall_Inform_ToWait(void)
{
	log_i("SipCall:Close");	
	//set_menu_with_video_off();
	//api_ak_vi_ch_stream_preview_off(0);
	//close_call_video();
	//Memo_Stop();
	API_PublicInfo_Write(PB_CALL_INFO,GetCallserverStatePb());
	API_Event_By_Name(EventCallState);
	API_TalkService_off();
	Send_CallEvent("Calling_Close", NULL);
	
}

void SipCall_Inform_ToTransfer(void)
{	
	log_d("SipCall:ipg mode divert");
	API_PublicInfo_Write(PB_CALL_INFO,GetCallserverStatePb());
	API_Event_By_Name(EventCallState);
}

void SipCall_Inform_ToUnlock(int lock_id)
{
	log_d("SipCall:Unlock%d",lock_id);
	API_Event_Unlock(lock_id==1?"RL1":"RL2", callscene_str[CallServer_Run.call_type]);
}

