#if 0	//IX2_TEST
#include <stdio.h>
#include "task_CallServer.h"
#include "../obj_BeCalled_State.h"
#include "../obj_Caller_State.h"
//#include "../task_DtBeCalled/task_DtBeCalled.h"
//#include "../task_DtCaller/task_DtCaller.h"
#include "../task_IpBeCalled/task_IpBeCalled.h"
#include "../task_IpCaller/task_IpCaller.h"

#include "../../task_Sundry/task_Ring/task_Ring.h"
#include "../../task_Sundry/task_Power/task_Power.h"
#include "../../task_Sundry/task_Led/task_Led.h"
#include "../../task_Sundry/task_Ring/task_Ring.h"
#include "../../task_io_server/obj_TableProcess.h"
#include "../../task_io_server/obj_call_record.h"
#include "../../task_io_server/obj_memo.h"
#include "../../task_io_server/task_IoServer.h"
//IX2_TEST #include "../../task_VideoMenu/task_VideoMenu.h"

#include "obj_IxCallScene4_process.h"

extern CALL_RECORD_DAT_T call_record_temp;
extern int call_record_flag;

#if 1
void IxCallScene4_Wait_Process(CALLSERVER_STRU *Msg_CallServer)
{
	//char detail[LOG_DESC_LEN + 1];
	unsigned char ipbecalled_type=IpBeCalled_LinphoneCall;
	//uint8 slaveMaster;
	//API_Event_IoServer_InnerRead_All(MASTER_SLAVE_SET, (uint8*)&slaveMaster);

	switch(Msg_CallServer->call_type)
	{
		case IxCallScene4:					//DOORCALL: DS-> DX-MASTER -> DX-SLAVE 
			
			switch(Msg_CallServer->msg_type)
			{
				case CallServer_Msg_Invite: 
					//czn_20190107_s
					#if 0
					if(API_Business_Request(Business_State_BeSipCall) == 0)
					{
						//usleep(200000);
						//API_Beep(BEEP_TYPE_DI3);
						//popDisplayLastMenu();
						CallServer_Business_Respones(Msg_CallServer->msg_source_id,CallServer_Msg_Invite,1);
						return;
					}
					#endif
					//czn_20190107_e
					if(Get_IpBeCalled_State() != 0)
					{
						API_IpBeCalled_ForceClose();
					}
					
					
					//Load_CallRule_Para();
					
					//if(slaveMaster != 0 || CallServer_Run.call_rule == CallRule_Normal || CallServer_Run.call_rule == CallRule_TransferNoAck)
					{
						ipbecalled_type = IpBeCalled_LinphoneCall;
						
			
						CallServer_Run.state = CallServer_Invite;
						CallServer_Run.call_type = Msg_CallServer->call_type;
						CallServer_Run.with_local_menu = 1;
						//CallServer_Run.s_addr = Msg_CallServer->partner_addr;
						//will_add CallServer_Run.t_addr = local_addr;
						CallServer_Run.timer = time(NULL);
						API_IpBeCalled_Invite(ipbecalled_type, NULL);
						CallServer_Run.state = CallServer_Ring;
						IxCallScene4_MenuDisplay_ToRing();
						CallServer_Business_Respones(Msg_CallServer->msg_source_id,CallServer_Msg_Invite,0);
					}
					break;

				default:
					break;
			}
			break;
	
		default:
			break;
	}
		
	
	
}

void IxCallScene4_Invite_Process(CALLSERVER_STRU *Msg_CallServer)
{
	
}

void IxCallScene4_Ring_Process(CALLSERVER_STRU *Msg_CallServer)
{
	unsigned char ipbecalled_type=0;
	
	switch(Msg_CallServer->call_type)
	{
		case IxCallScene4:					//DOORCALL: DS-> DX-MASTER -> DX-SLAVE 
			ipbecalled_type = IpBeCalled_LinphoneCall;
			if(CallServer_Run.call_type != IxCallScene4)
			{
				//will_add
			}
			switch(Msg_CallServer->msg_type)
			{
				case CallServer_Msg_Invite:		//czn_20171030
					if(time(NULL) - CallServer_Run.timer > 5)
					{
					#if 0
						if(CallServer_Run.s_addr.rt == Msg_CallServer->partner_addr.rt && CallServer_Run.s_addr.code == Msg_CallServer->partner_addr.code)
						{
							if(!RingGetState())
							{
								//API_RingPlay(Get_CallRingScene_ByAddr(CallServer_Run.s_addr));
								API_POWER_EXT_RING_ON();		//czn_20170809
								//API_IpCaller_Redail(ipcaller_type);
							}
						}
						else
						{
							CallServer_Run.state = CallServer_Wait;
							IxCallScene4_MenuDisplay_ToWait();
						}
					#endif
						
					}
					else
					{
					#if 0
						if(CallServer_Run.s_addr.rt == Msg_CallServer->partner_addr.rt && CallServer_Run.s_addr.code == Msg_CallServer->partner_addr.code)
						{
							//CallServer_Run.timer = time(NULL);
							API_DtBeCalled_Invite(dtbecalled_type, &CallServer_Run.s_addr);
						}
					#endif
					}
					CallServer_Business_Respones(Msg_CallServer->msg_source_id,CallServer_Msg_Invite,0);
					break;
				case CallServer_Msg_RingNoAck:
					//API_Event_IoServer_InnerRead_All(MASTER_SLAVE_SET, (uint8*)&slaveMaster);
					
			
					CallServer_Run.state = CallServer_TargetBye;
					//CallServer_Run.t_addr = Msg_CallServer->partner_addr;
					
					
					API_IpBeCalled_Bye(ipbecalled_type);
				
					
					CallServer_Run.state = CallServer_Wait;
					IxCallScene4_MenuDisplay_ToWait();
					
					break;
					
				case CallServer_Msg_RemoteRing:
					break;	

				case CallServer_Msg_LocalAck:
					CallServer_Run.state = CallServer_Ack;
					CallServer_Run.with_local_menu = 1;
					CallServer_Run.timer = time(NULL); 
					
					API_IpBeCalled_Ack(ipbecalled_type);
			
					IxCallScene4_MenuDisplay_ToAck();
					break;	

				case CallServer_Msg_LocalBye:
					CallServer_Run.state = CallServer_TargetBye;
					//CallServer_Run.t_addr = Msg_CallServer->partner_addr;
					API_IpBeCalled_Bye(ipbecalled_type);
					
					CallServer_Run.state = CallServer_Wait;
					IxCallScene4_MenuDisplay_ToWait();
					break;

				case CallServer_Msg_RemoteBye:
					
					
					CallServer_Run.state = CallServer_SourceBye;
					API_IpBeCalled_Cancel(ipbecalled_type);
					CallServer_Run.state = CallServer_Wait;
					
					IxCallScene4_MenuDisplay_ToWait();
					break;

				case CallServer_Msg_Timeout:
					//API_DtBeCalled_ForceClose();
					//API_IpCaller_ForceClose();
					API_IpBeCalled_Bye(ipbecalled_type);
					
					CallServer_Run.state = CallServer_Wait;
					IxCallScene4_MenuDisplay_ToWait();
					break;

				case CallServer_Msg_DtSrDisconnect:
					API_IpBeCalled_ForceClose();
					//API_IpCaller_ForceClose();
					CallServer_Run.state = CallServer_Wait;
					IxCallScene4_MenuDisplay_ToWait();
					break;

				case CallServer_Msg_NetCallLinkDisconnect:
					//API_DtBeCalled_Bye(dtbecalled_type);
					//API_IpCaller_ForceClose();
					//CallServer_Run.state = CallServer_Wait;
					//DxCallScene1_MenuDisplay_ToWait();
					break;

				case CallServer_Msg_RemoteUnlock1:
					//CallServer_Run.with_local_menu &= ~0x80;
					//API_DtBeCalled_Unlock1(dtbecalled_type);
					//API_IpCaller_Unlock1(ipcaller_type,&Msg_CallServer->partner_addr);
					break;

				case CallServer_Msg_RemoteUnlock2:
					//CallServer_Run.with_local_menu &= ~0x80;
					//API_DtBeCalled_Unlock2(dtbecalled_type);
					//API_IpCaller_Unlock2(ipcaller_type,&Msg_CallServer->partner_addr);
					break;
					
				case CallServer_Msg_Redail:	//czn_20171030
					if(!RingGetState())
					{
						//API_RingPlay(Get_CallRingScene_ByAddr(CallServer_Run.s_addr));
						API_POWER_EXT_RING_ON();		//czn_20170809
						//API_IpCaller_Redail(ipcaller_type);
					}
					break;
					
				default:
					break;
			}
			break;
			
		
			
		default:
			break;
	}
	
}

void IxCallScene4_Ack_Process(CALLSERVER_STRU *Msg_CallServer)
{
	unsigned ipbecalled_type=0;
	
	switch(Msg_CallServer->call_type)
	{
		case IxCallScene4:					//DOORCALL: DS-> DX-MASTER -> DX-SLAVE 
			ipbecalled_type = IpBeCalled_LinphoneCall;
			if(CallServer_Run.call_type != IxCallScene4)
			{
				//will_add
			}
			switch(Msg_CallServer->msg_type)
			{
				case CallServer_Msg_Invite:
					if(time(NULL) - CallServer_Run.timer > 5)
					{
						CallServer_Run.state = CallServer_Wait;
						IxCallScene4_MenuDisplay_ToWait();
					}
					CallServer_Business_Respones(Msg_CallServer->msg_source_id,CallServer_Msg_Invite,0);
					break;
					
				case CallServer_Msg_RemoteRing:
					break;	

				case CallServer_Msg_LocalAck:
					break;	

				case CallServer_Msg_RemoteAck:
					break;

				case CallServer_Msg_LocalBye:
					CallServer_Run.state = CallServer_TargetBye;
					//CallServer_Run.t_addr = Msg_CallServer->partner_addr;
					API_IpBeCalled_Bye(ipbecalled_type);
					
					CallServer_Run.state = CallServer_Wait;
					IxCallScene4_MenuDisplay_ToWait();
					break;

				case CallServer_Msg_RemoteBye:
					
					
					CallServer_Run.state = CallServer_SourceBye;
					API_IpBeCalled_Cancel(ipbecalled_type);
					CallServer_Run.state = CallServer_Wait;
					
					IxCallScene4_MenuDisplay_ToWait();
					break;

				case CallServer_Msg_Timeout:
					//API_DtBeCalled_ForceClose();
					//API_IpCaller_ForceClose();
					API_IpBeCalled_Bye(ipbecalled_type);
					
					CallServer_Run.state = CallServer_Wait;
					IxCallScene4_MenuDisplay_ToWait();
					break;

				case CallServer_Msg_DtSrDisconnect:
					API_IpBeCalled_ForceClose();
					//API_IpCaller_ForceClose();
					CallServer_Run.state = CallServer_Wait;
					IxCallScene4_MenuDisplay_ToWait();
					break;

				case CallServer_Msg_NetCallLinkDisconnect:
					//API_DtBeCalled_Bye(dtbecalled_type);
					//API_IpCaller_ForceClose();
					//CallServer_Run.state = CallServer_Wait;
					//DxCallScene1_MenuDisplay_ToWait();
					break;

				case CallServer_Msg_RemoteUnlock1:
					//CallServer_Run.with_local_menu &= ~0x80;
					//API_DtBeCalled_Unlock1(dtbecalled_type);
					//API_IpCaller_Unlock1(ipcaller_type,&Msg_CallServer->partner_addr);
					break;

				case CallServer_Msg_RemoteUnlock2:
					//CallServer_Run.with_local_menu &= ~0x80;
					//API_DtBeCalled_Unlock2(dtbecalled_type);
					//API_IpCaller_Unlock2(ipcaller_type,&Msg_CallServer->partner_addr);
					break;

				default:
					break;
			}
			break;
	
		default:
			break;
	}
	
	
}
void IxCallScene4_SourceBye_Process(CALLSERVER_STRU *Msg_CallServer)
{
	//char detail[LOG_DESC_LEN + 1];
	
	switch(Msg_CallServer->call_type)
	{
		case IxCallScene4:					//DOORCALL: DS-> DX-MASTER -> DX-SLAVE 
			switch(Msg_CallServer->msg_type)
			{
				case CallServer_Msg_Invite:
					break;
					
				case CallServer_Msg_RemoteRing:
					break;	

				case CallServer_Msg_LocalAck:
					break;	

				case CallServer_Msg_RemoteAck:
					break;

				case CallServer_Msg_LocalBye:
					break;

				case CallServer_Msg_RemoteBye:
					break;

				case CallServer_Msg_Timeout:
					break;

				case CallServer_Msg_DtSrDisconnect:
					break;

				case CallServer_Msg_NetCallLinkDisconnect:
					break;

				case CallServer_Msg_LocalUnlock1:
					break;

				case CallServer_Msg_LocalUnlock2:
					break;

				case CallServer_Msg_RemoteUnlock1:
					break;

				case CallServer_Msg_RemoteUnlock2:
					break;

				default:
					break;
			}
			break;
			
		default:
			break;
	}
	
}

void IxCallScene4_TargetBye_Process(CALLSERVER_STRU *Msg_CallServer)
{
	//char detail[LOG_DESC_LEN + 1];

	switch(Msg_CallServer->call_type)
	{
		case IxCallScene4:					//DOORCALL: DS-> DX-MASTER -> DX-SLAVE 
			switch(Msg_CallServer->msg_type)
			{
				case CallServer_Msg_Invite:
					break;
					
				case CallServer_Msg_RemoteRing:
					break;	

				case CallServer_Msg_LocalAck:
					break;	

				case CallServer_Msg_RemoteAck:
					break;

				case CallServer_Msg_LocalBye:
					break;

				case CallServer_Msg_RemoteBye:
					break;

				case CallServer_Msg_Timeout:
					break;

				case CallServer_Msg_DtSrDisconnect:
					break;

				case CallServer_Msg_NetCallLinkDisconnect:
					break;

				case CallServer_Msg_LocalUnlock1:
					break;

				case CallServer_Msg_LocalUnlock2:
					break;

				case CallServer_Msg_RemoteUnlock1:
					break;

				case CallServer_Msg_RemoteUnlock2:
					break;

				default:
					break;
			}
			break;
			
		default:
			break;
	}
	
}

void IxCallScene4_Transfer_Process(CALLSERVER_STRU *Msg_CallServer)
{
	
}


void IxCallScene4_MenuDisplay_ToInvite(void)
{
	switch(CallServer_Run.call_type)
	{
		case IxCallScene4:					//DOORCALL: DS-> DX-MASTER -> DX-SLAVE 
			break;
			
	
		default:
			break;
	}
}


void IxCallScene4_MenuDisplay_ToRing(void)
{
	char disp_name[21] = {0};
	char tempData[42];

	switch(CallServer_Run.call_type)
	{	
		case IxCallScene4:					//INNERCALL: DX-MASTER -> DX-SLAVE & DT-SLAVE
			//api_spk_volume_set( 0 );
			API_LedDisplay_CallRing();
			//Set_IPCallLink_Status(CLink_AsBeCalled);
			//Set_IPCallLink_BeCalledData(&IpBeCalled_Obj.BeCalled_Run.s_addr);
			//API_RingPlay(Get_CallRingScene_ByAddr(CallServer_Run.s_addr));
			API_POWER_EXT_RING_ON();		//czn_20170809
			BEEP_PhoneCall();//API_Beep(BEEP_TYPE_DI8);

			API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_InnerBeCallOn);
			
			call_record_temp.type 				= CALL_RECORD;
			call_record_temp.subType 			= IN_COMING;
			call_record_temp.property				= MISSED;
			//call_record_temp.target_node			= CallServer_Run.s_addr.gatewayid;		//czn_20170329
			//call_record_temp.target_id			= CallServer_Run.s_addr.rt*32 + CallServer_Run.s_addr.code;
			strcpy(call_record_temp.name,"phone");
			strcpy(call_record_temp.input,"---");
			strcpy(call_record_temp.relation, "-");
			call_record_flag = 1;
			#if 0
			if(Get_CallPartnerName_ByAddr(CallServer_Run.s_addr,disp_name) == 0)
			{
				tempData[0] = strlen(disp_name);
				memcpy(&tempData[1], disp_name, tempData[0]);
				API_add_Inform_with_data_to_VideoMenu_queue(MSG_7_BRD_SUB_InnerCallName, tempData, tempData[0]+1);
				strcpy(call_record_temp.name,disp_name);
			}
			#endif
			strcpy(&tempData[1],"Phone Call");
			strcpy(call_record_temp.name,&tempData[1]);
			tempData[0] = strlen(&tempData[1]);
				
			API_add_Inform_with_data_to_VideoMenu_queue(MSG_7_BRD_SUB_InnerCallName, tempData, tempData[0]+1);	
			break;
			
		default:
			break;
	}
		
	
}

void IxCallScene4_MenuDisplay_ToAck(void)
{
	switch(CallServer_Run.call_type)
	{
		case IxCallScene4:					//INNERCALL: DX-MASTER -> DX-SLAVE & DT-SLAVE
			//LoadLinPhoneAudioCaptureVol();//LoadAudioCaptureVol();
			//LoadLinPhoneAudioPlaybackVol();
			API_LedDisplay_CallTalk();
			
			BEEP_PhoneCall_Stop();//API_RingStop();
			API_POWER_EXT_RING_OFF();		//czn_20170809
			API_TalkOn();//API_POWER_TALK_ON();
			API_POWER_UDP_TALK_ON();
			API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_InnerCallTalkOn);

			call_record_temp.property	= NORMAL;
			break;
		
		default:
			break;
	}
	
}

void IxCallScene4_MenuDisplay_ToBye(void)
{
	switch(CallServer_Run.call_type)
	{		
		case IxCallScene4:					//INNERCALL: DX-MASTER -> DX-SLAVE & DT-SLAVE
			//czn_20190107_s
			API_Business_Close(Business_State_BeSipCall);	
			//czn_20190107_e
			API_LedDisplay_CallClose();
			Set_IPCallLink_Status(CLink_Idle);
			API_TalkOff();//API_POWER_TALK_OFF();
			BEEP_PhoneCall_Stop();//API_RingStop();
			API_POWER_EXT_RING_OFF();		//czn_20170809
			API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_InnerBeCallOff);
			//linphone_mon_lasttime_update(); 	//czn_20181119
			break;
	
		default:
			break;
	}
	
	if(call_record_flag == 1)
	{
		call_record_flag = 0;
		api_register_one_call_record( &call_record_temp );
	}
}

void IxCallScene4_MenuDisplay_ToWait(void)
{
	switch(CallServer_Run.call_type)
	{	
		case IxCallScene4:					//INNERCALL: DX-MASTER -> DX-SLAVE & DT-SLAVE
			//czn_20190107_s
			API_Business_Close(Business_State_BeSipCall);	
			//czn_20190107_e
			API_LedDisplay_CallClose();
			Set_IPCallLink_Status(CLink_Idle);
			API_TalkOff();//API_POWER_TALK_OFF();
			BEEP_PhoneCall_Stop();//API_RingStop();
			API_POWER_EXT_RING_OFF();		//czn_20170809
			API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_InnerBeCallOff);
			//linphone_mon_lasttime_update(); 	//czn_20181119
			break;
			
		default:
			break;
	}

	if(call_record_flag == 1)
	{
		call_record_flag = 0;
		api_register_one_call_record( &call_record_temp );
	}

}


void IxCallScene4_MenuDisplay_ToTransfer(void)
{	
	
}
#endif
#endif
