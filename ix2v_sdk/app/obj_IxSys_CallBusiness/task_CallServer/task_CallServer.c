#include <stdio.h>
#include "task_CallServer.h"
#include "obj_BeCalled_State.h"
#include "obj_Caller_State.h"
//#include "task_IpBeCalled.h"
//#include "task_IpCaller.h"
#include "task_DtCaller.h"
#include "obj_SYS_VER_INFO.h"
#include "elog_forcall.h"
#include "obj_PublicInformation.h"
#include "task_Event.h"
#include "obj_ThreadHeartBeatService.h"
#include "obj_MDS_DR.h"
#include "obj_GetInfoByIp.h"
#include "obj_IoInterface.h"
#include "obj_TableSurver.h"

OS_TIMER timer_callserver;

CallServer_Run_Stru CallServer_Run;

Loop_vdp_common_buffer	vdp_callserver_mesg_queue;
Loop_vdp_common_buffer	vdp_callserver_sync_queue;

const char *callserver_msg_str[]=
{
	"Invite",
	"InviteOk",
	"InviteFail",
	"RingNoAck",
	"RemoteRing",
	"LocalAck",
	"RemoteAck",
	"DtSlaveAck",
	"LocalBye",
	"RemoteBye",
	"ByeOk",
	"DtSrDisconnect",
	"NetCallLinkDisconnect",
	"Timeou",
	"LocalUnlock1",
	"LocalUnlock2",
	"RemoteUnlock1",
	"RemoteUnlock2",
	"Redail",			//czn_20171030
	"Transfer",
	"ThreadHeartbeatReq",
	"AppBye",
	"AppInviteOk",
	"AppAck",
};
const int callserver_msg_str_max=sizeof(callserver_msg_str)/sizeof(callserver_msg_str[0]);

const char *callserver_state_str[]=
{
	"Wait",
	"Invite",
	"Ring",
	"Ack",
	"SourceBye",
	"TargetBye",
	"Transfer",
	"LeaveMsg",
	"RemoteAck",
	"Init",
};
const int callserver_state_str_max=sizeof(callserver_state_str)/sizeof(callserver_state_str[0]);
const char *callscene_str[]=
{
	"not-specified",
	"IxMainCall",//"IxCallScene1_Active",				//DOORCALL: DS-> IX-IM 
	"BeIxMainCall",//"IxCallScene1_Passive",				//DOORCALL: DS-> IX-IM
	"IxIntercomCall",//"IxCallScene2_Active",				//INTERCOM:  IX-IM -> IX-IM
	"BeIxIntercomCall",//"IxCallScene2_Passive",				//INTERCOM:  IX-IM -> IX-IM 
	"IxInnerCall",//"IxCallScene3_Active",				//INNERCALL:  IX-IM -> IX-IM 
	"BeIxInnerCall",//"IxCallScene3_Passive",				//INNERCALL:  IX-IM -> IX-IM 
	"Discard",//"IxCallScene4",						//IX50 -> VirtualUser		//czn_20190107
	"Discard",//"IxCallScene5",						//test:IX50 -> VirtualUser
	"Discard",//"IxCallScene8",						//phoneCALL: Phone -> DX-MASTER 
	"Discard",//"IxCallScene9",						//DOORCALL:DS->DT-IM(IPG)			//call_dtim
	"IxMainCall_New",//"IxCallScene10_Active",				//GLCALL:GL->IX-IM
	"BeIxMainCall_New",//"IxCallScene10_Passive",
	"Discard",//"IxCallScene11_Passive",			//DS->IX-IM WITH LOCAL DIVERT
	"DxMainCall",
	"IXGMainCall",
	"SipCall",
	"VMSipCall",
};
const int callscene_str_max=sizeof(callscene_str)/sizeof(callscene_str[0]);


//CALL_RECORD_DAT_T call_record_temp;
//int call_record_flag = 0;

vdp_task_t	task_callserver;
void* vdp_callserver_task( void* arg );

int MasterOrSlaveSetting = 0;
Global_Addr_Stru DxMaster_Addr={101,0,30,0};

// lzh_20180912_s
int Get_SipConfig_Account_info(char *server, char* divert_account, char* divert_passord, char* port );
// lzh_20180912_e

void Load_CallRule_Para(void)
{
	#if 0
	uint8 tranferset;
	uint8 slaveMaster;
	char temp[20];
	if((tranferset = Get_NoDisturbSetting()) == 0)	//czn_20170805
	{
		API_Event_IoServer_InnerRead_All(CallScene_SET, temp);
		tranferset = atoi(temp);
	}
	CallServer_Run.call_rule = CallRule_Normal;
	
	API_Event_IoServer_InnerRead_All(MASTER_SLAVE_SET, temp);
	slaveMaster = atoi(temp);

	if(slaveMaster == 0)
	{
		if(tranferset == 0)
		{
			CallServer_Run.call_rule = CallRule_Normal;
		}
		if(tranferset == 1 ||tranferset == 2)
		{
			CallServer_Run.call_rule = CallRule_NoDisturb;
		}
		if(tranferset == 3)
		{
			CallServer_Run.call_rule = CallRule_TransferNoAck;
		}
		if(tranferset == 4)
		{
			CallServer_Run.call_rule = CallRule_TransferIm;
		}
	}
	else
	{
		if(tranferset == 0 || tranferset == 3 || tranferset == 4)
		{
			CallServer_Run.call_rule = CallRule_Normal;
		}
		
		if(tranferset == 1 ||tranferset == 2)
		{
			CallServer_Run.call_rule = CallRule_NoDisturb;
		}
	}
	CallServer_Run.rule_act = 0;
	// lzh_20180912_s	
	API_Event_IoServer_InnerRead_All(DivertTime, temp);
	slaveMaster = atoi(temp);
	
	if( CallServer_Run.call_rule == CallRule_TransferIm )
		CallServer_Run.caller_divert_state	= 0x80;
	else if( CallServer_Run.call_rule == CallRule_TransferNoAck )		
		CallServer_Run.caller_divert_state	= (0x80|slaveMaster);
	else
		CallServer_Run.caller_divert_state = 0;
	
	char sip_server[50];
	char sip_divert_accout[50];
	char sip_divert_password[50];
	char sip_port[50];	
	Get_SipConfig_Account_info(sip_server,sip_divert_accout,sip_divert_password,sip_port);
	strncpy(CallServer_Run.caller_master_sip_divert.sip_server,sip_server,16);
	strncpy(CallServer_Run.caller_master_sip_divert.sip_account,sip_divert_accout,20);	
	// lzh_20180912_e
	#endif
}

void CallServer_Business_Process(CALLSERVER_STRU *Msg_CallServer)
{
	//printf("!!!callserver state = %d,call_type = %d recv msg = %d\n",CallServer_Run.state,Msg_CallServer->call_type,Msg_CallServer->msg_type);
	char *msg_str,*state_str,*cur_calltype_str,*from_thread,*calltype_str;
	char uk[]="UNKOWN";
	if(Msg_CallServer->msg_type>=callserver_msg_str_max)
		msg_str=uk;
	else
		msg_str=callserver_msg_str[Msg_CallServer->msg_type];
	
	if(Msg_CallServer->call_type>=callscene_str_max)
		calltype_str=uk;
	else
		calltype_str=callscene_str[Msg_CallServer->call_type];
	
	if(CallServer_Run.state==CallServer_Wait)
		cur_calltype_str=callscene_str[0];
	else if(CallServer_Run.call_type>=callscene_str_max)
		cur_calltype_str=uk;
	else
		cur_calltype_str=callscene_str[CallServer_Run.call_type];
	if(CallServer_Run.state>=callserver_state_str_max)
		state_str=uk;
	else
		state_str=callserver_state_str[CallServer_Run.state];
	vdp_task_t *ptask = GetTaskAccordingMsgID(Msg_CallServer->msg_source_id);
	if(ptask==NULL)
		from_thread=uk;
	else
		from_thread=ptask->task_name;
	//printf("callsever recv :%s,calltype:%s,from:%s,cur calltype:%s,state:%s\n",msg_str,calltype_str,from_thread,cur_calltype_str,state_str);		
	log_d("callsever recv :%s,callscene:%s,from:%s,cur callscene:%s,state:%s",msg_str,calltype_str,from_thread,cur_calltype_str,state_str);
	if(Msg_CallServer->msg_type==CallServer_Msg_ThreadHeartbeatReq)		//czn_20190910
	{
		RecvThreadHeartbeatReply(CallServer_Heartbeat_Mask);
		return;
	}
	//if(CallServer_Run.state!=CallServer_Wait&&CallServer_Run.call_type != Msg_CallServer->call_type&&(time(NULL)-CallServer_Run.timer)<60)
	//	return;
	switch(CallServer_Run.state)
	{
		case CallServer_Wait:
			CallServer_Wait_Process(Msg_CallServer);
			break;
			
		case CallServer_Invite:
			CallServer_Invite_Process(Msg_CallServer);
			break;
			
		case CallServer_Ring:
			CallServer_Ring_Process(Msg_CallServer);
			break;

		case CallServer_Ack:
			CallServer_Ack_Process(Msg_CallServer);
			break;	

		case CallServer_SourceBye:
			CallServer_SourceBye_Process(Msg_CallServer);
			break;	

		case CallServer_TargetBye:
			CallServer_TargetBye_Process(Msg_CallServer);
			break;

		case CallServer_Transfer:
			CallServer_Transfer_Process(Msg_CallServer);
			break;
		case CallServer_RemoteAck:
			CallServer_RemoteAck_Process(Msg_CallServer);
			break;
		default:
			CallServer_Run.state=CallServer_Wait;
			CallServer_StateInvalid_Process(Msg_CallServer);
			break;
	}
}

void CallServer_Wait_Process(CALLSERVER_STRU *Msg_CallServer)
{
	switch(Msg_CallServer->call_type)
	{
		case IxCallScene1_Passive:					
			IxCallScene1_Passive_Wait_Process(Msg_CallServer);
			break;
		case DxMainCall:					
			DxMainCall_Wait_Process(Msg_CallServer);
			break;
		case VMSipCall:
		case SipCall:					
			SipCall_Wait_Process(Msg_CallServer);
			break;
		case IXGMainCall:					
			IXGMainCall_Wait_Process(Msg_CallServer);
			break;
		case IxCallScene1_Active:					
			IxCallScene1_Active_Wait_Process(Msg_CallServer);
			break;

		case IxCallScene2_Active:					
			IxCallScene2_Active_Wait_Process(Msg_CallServer);
			break;	

		case IxCallScene2_Passive:					
			IxCallScene2_Passive_Wait_Process(Msg_CallServer);
			break;	
		#if 0	
		case IxCallScene1_Passive:					
			IxCallScene1_Passive_Wait_Process(Msg_CallServer);
			break;

		
			
		
			
		case IxCallScene3_Active:					
			IxCallScene3_Active_Wait_Process(Msg_CallServer);
			break;
			
		case IxCallScene3_Passive:					
			IxCallScene3_Passive_Wait_Process(Msg_CallServer);
			break;
		case IxCallScene11_Passive:					
			IxCallScene11_Passive_Wait_Process(Msg_CallServer);
			break;	
		
		case IxCallScene4:
			IxCallScene4_Wait_Process(Msg_CallServer);
			break;

		case IxCallScene5:
			IxCallScene5_Wait_Process(Msg_CallServer);
			break;
		#endif
			
		default:
			break;
	}
		
	
	
}

void CallServer_Invite_Process(CALLSERVER_STRU *Msg_CallServer)
{
	switch(CallServer_Run.call_type)
	{
		case IxCallScene1_Passive:					
			IxCallScene1_Passive_Invite_Process(Msg_CallServer);
			break;
		case DxMainCall:					
			DxMainCall_Invite_Process(Msg_CallServer);
			break;
		case VMSipCall:	
		case SipCall:					
			SipCall_Invite_Process(Msg_CallServer);
			break;	
		case IXGMainCall:					
			IXGMainCall_Invite_Process(Msg_CallServer);
			break;	
		case IxCallScene1_Active:					
			IxCallScene1_Active_Invite_Process(Msg_CallServer);
			break;
			
		case IxCallScene2_Active:					
			IxCallScene2_Active_Invite_Process(Msg_CallServer);
			break;	

		case IxCallScene2_Passive:					
			IxCallScene2_Passive_Invite_Process(Msg_CallServer);
			break;
		#if 0	
		case IxCallScene1_Passive:					
			IxCallScene1_Passive_Invite_Process(Msg_CallServer);
			break;
			
		
			
		
			
		case IxCallScene3_Active:					
			IxCallScene3_Active_Invite_Process(Msg_CallServer);
			break;
			
		case IxCallScene3_Passive:					
			IxCallScene3_Passive_Invite_Process(Msg_CallServer);
			break;
		case IxCallScene11_Passive:					
			IxCallScene11_Passive_Invite_Process(Msg_CallServer);
			break;	

		
		case IxCallScene4:
			IxCallScene4_Invite_Process(Msg_CallServer);
			break;

		case IxCallScene5:
			IxCallScene5_Invite_Process(Msg_CallServer);
			break;	
		#endif		
		default:
			break;
	}
}

void CallServer_Ring_Process(CALLSERVER_STRU *Msg_CallServer)
{
	unsigned char dtbecalled_type=0,dtcaller_type=0,ipbecalled_type=0,ipcaller_type=0;
	
	switch(CallServer_Run.call_type)
	{
		case IxCallScene1_Passive:					
			IxCallScene1_Passive_Ring_Process(Msg_CallServer);
			break;
		case DxMainCall:					
			DxMainCall_Ring_Process(Msg_CallServer);
			break;
		case VMSipCall:	
		case SipCall:					
			SipCall_Ring_Process(Msg_CallServer);
			break;
		case IXGMainCall:					
			IXGMainCall_Ring_Process(Msg_CallServer);
			break;	
		case IxCallScene1_Active:					
			IxCallScene1_Active_Ring_Process(Msg_CallServer);
			break;

		case IxCallScene2_Active:					
			IxCallScene2_Active_Ring_Process(Msg_CallServer);
			break;	

		case IxCallScene2_Passive:					
			IxCallScene2_Passive_Ring_Process(Msg_CallServer);
			break;
		#if 0	
		case IxCallScene1_Passive:					
			IxCallScene1_Passive_Ring_Process(Msg_CallServer);
			break;
			
		
			
		
			
		case IxCallScene3_Active:					
			IxCallScene3_Active_Ring_Process(Msg_CallServer);
			break;
			
		case IxCallScene3_Passive:					
			IxCallScene3_Passive_Ring_Process(Msg_CallServer);
			break;
		case IxCallScene11_Passive:					
			IxCallScene11_Passive_Ring_Process(Msg_CallServer);
			break;	

		
		case IxCallScene4:
			IxCallScene4_Ring_Process(Msg_CallServer);
			break;

		case IxCallScene5:
			IxCallScene5_Ring_Process(Msg_CallServer);
			break;
		#endif	
		default:
			break;
	}
	
}

void CallServer_Ack_Process(CALLSERVER_STRU *Msg_CallServer)
{
	unsigned char dtbecalled_type=0,dtcaller_type=0,ipbecalled_type=0,ipcaller_type=0;
	
	switch(CallServer_Run.call_type)
	{
		case IxCallScene1_Passive:					
			IxCallScene1_Passive_Ack_Process(Msg_CallServer);
			break;
		case DxMainCall:					
			DxMainCall_Ack_Process(Msg_CallServer);
			break;
		case VMSipCall:	
		case SipCall:					
			SipCall_Ack_Process(Msg_CallServer);
			break;
		case IXGMainCall:					
			IXGMainCall_Ack_Process(Msg_CallServer);
			break;
		case IxCallScene1_Active:					
			IxCallScene1_Active_Ack_Process(Msg_CallServer);
			break;

		case IxCallScene2_Active:					
			IxCallScene2_Active_Ack_Process(Msg_CallServer);
			break;	

		case IxCallScene2_Passive:					
			IxCallScene2_Passive_Ack_Process(Msg_CallServer);
			break;
		#if 0	
		case IxCallScene1_Passive:					
			IxCallScene1_Passive_Ack_Process(Msg_CallServer);
			break;
			
		case IxCallScene2_Active:					
			IxCallScene2_Active_Ack_Process(Msg_CallServer);
			break;
			
	
			
		case IxCallScene3_Active:					
			IxCallScene3_Active_Ack_Process(Msg_CallServer);
			break;
			
		case IxCallScene3_Passive:					
			IxCallScene3_Passive_Ack_Process(Msg_CallServer);
			break;
		case IxCallScene11_Passive:					
			IxCallScene11_Passive_Ack_Process(Msg_CallServer);
			break;
		
		case IxCallScene4:
			IxCallScene4_Ack_Process(Msg_CallServer);
			break;

		case IxCallScene5:
			IxCallScene5_Ack_Process(Msg_CallServer);
			break;	
		#endif
		default:
			break;
	}
	
	
}
void CallServer_SourceBye_Process(CALLSERVER_STRU *Msg_CallServer)
{
	//char detail[LOG_DESC_LEN + 1];
	
	switch(CallServer_Run.call_type)
	{
		case IxCallScene1_Passive:					
			IxCallScene1_Passive_SourceBye_Process(Msg_CallServer);
			break;
		case DxMainCall:					
			DxMainCall_SourceBye_Process(Msg_CallServer);
			break;	
		case IxCallScene1_Active:					
			IxCallScene1_Active_SourceBye_Process(Msg_CallServer);
			break;
		case IxCallScene2_Active:					
			IxCallScene2_Active_SourceBye_Process(Msg_CallServer);
			break;	
		#if 0	
		case IxCallScene1_Passive:					
			IxCallScene1_Passive_SourceBye_Process(Msg_CallServer);
			break;
			
		
			
		case IxCallScene2_Passive:					
			IxCallScene2_Passive_SourceBye_Process(Msg_CallServer);
			break;
			
		case IxCallScene3_Active:					
			IxCallScene3_Active_SourceBye_Process(Msg_CallServer);
			break;
			
		case IxCallScene3_Passive:					
			IxCallScene3_Passive_SourceBye_Process(Msg_CallServer);
			break;
		case IxCallScene11_Passive:					
			IxCallScene11_Passive_SourceBye_Process(Msg_CallServer);
			break;

		
		case IxCallScene4:
			IxCallScene4_SourceBye_Process(Msg_CallServer);
			break;

		case IxCallScene5:
			IxCallScene5_SourceBye_Process(Msg_CallServer);
			break;	
		#endif
		default:
			break;
	}
	
}

void CallServer_TargetBye_Process(CALLSERVER_STRU *Msg_CallServer)
{
	//char detail[LOG_DESC_LEN + 1];

	switch(CallServer_Run.call_type)
	{
		case IxCallScene1_Passive:					
			IxCallScene1_Passive_TargetBye_Process(Msg_CallServer);
			break;
		case DxMainCall:					
			DxMainCall_TargetBye_Process(Msg_CallServer);
			break;	
		case IxCallScene1_Active:					
			IxCallScene1_Active_TargetBye_Process(Msg_CallServer);
			break;
			
		case IxCallScene2_Active:					
			IxCallScene2_Active_TargetBye_Process(Msg_CallServer);
			break;	
		#if 0	
		case IxCallScene1_Passive:					
			IxCallScene1_Passive_TargetBye_Process(Msg_CallServer);
			break;
			
		
			
		case IxCallScene2_Passive:					
			IxCallScene2_Passive_TargetBye_Process(Msg_CallServer);
			break;
			
		case IxCallScene3_Active:					
			IxCallScene3_Active_TargetBye_Process(Msg_CallServer);
			break;
			
		case IxCallScene3_Passive:					
			IxCallScene3_Passive_TargetBye_Process(Msg_CallServer);
			break;
		case IxCallScene11_Passive:					
			IxCallScene11_Passive_TargetBye_Process(Msg_CallServer);
			break;

		
		case IxCallScene4:
			IxCallScene4_TargetBye_Process(Msg_CallServer);
			break;

		case IxCallScene5:
			IxCallScene5_TargetBye_Process(Msg_CallServer);
			break;	
		#endif
		default:
			break;
	}
	
}

void CallServer_Transfer_Process(CALLSERVER_STRU *Msg_CallServer)
{
	//char detail[LOG_DESC_LEN + 1];

	switch(CallServer_Run.call_type)
	{
		case DxMainCall:					
			DxMainCall_Transfer_Process(Msg_CallServer);
			break;	
		
		case IxCallScene1_Active:					
			IxCallScene1_Active_Transfer_Process(Msg_CallServer);
			break;
		#if 0	
		case IxCallScene1_Passive:					
			IxCallScene1_Passive_Transfer_Process(Msg_CallServer);
			break;
			
		case IxCallScene2_Active:					
			IxCallScene2_Active_Transfer_Process(Msg_CallServer);
			break;
			
		case IxCallScene2_Passive:					
			IxCallScene2_Passive_Transfer_Process(Msg_CallServer);
			break;
			
		case IxCallScene3_Active:					
			IxCallScene3_Active_Transfer_Process(Msg_CallServer);
			break;
			
		case IxCallScene3_Passive:					
			IxCallScene3_Passive_Transfer_Process(Msg_CallServer);
			break;

		case IxCallScene11_Passive:					
			IxCallScene11_Passive_Transfer_Process(Msg_CallServer);
			break;

		
		case IxCallScene4:
			IxCallScene4_Transfer_Process(Msg_CallServer);
			break;

		case IxCallScene5:
			IxCallScene5_Transfer_Process(Msg_CallServer);
			break;
		#endif
		default:
			break;
	}
	
}

void CallServer_RemoteAck_Process(CALLSERVER_STRU *Msg_CallServer)
{
	//char detail[LOG_DESC_LEN + 1];

	switch(CallServer_Run.call_type)
	{
			
		case IxCallScene11_Passive:					
			//IxCallScene11_Passive_RemoteAck_Process(Msg_CallServer);
			break;
			
		default:
			break;
	}
	
}

void CallServer_ExceptCall_Process(CALLSERVER_STRU *Msg_CallServer)
{


}
/*------------------------------------------------------------------------
					Caller_StateInvalid_Process
���?    msg_caller

����:	��ȷ��״̬�������?
����:   ��
		
------------------------------------------------------------------------*/
void CallServer_StateInvalid_Process(CALLSERVER_STRU *Msg_CallServer)	
{
	//char detail[LOG_DESC_LEN + 1];
	//snprintf(detail,LOG_DESC_LEN+1,"[E]StateInvalid_S%d",Caller_Run.state);
	//API_add_log_item(CALLER_ERROR_LOG_LEVEL,"S_CALL",detail,NULL);

	//Caller_Run.state = CALLER_WAITING;
}

/*------------------------------------------------------------------------
				Caller_MsgInvalid_Process
���?    msg_caller

����:	�Բ�ȷ��msg���д������򲻲���

		
------------------------------------------------------------------------*/
void CallServer_MsgInvalid_Process(CALLSERVER_STRU *Msg_CallServer)	
{
  
}


/*------------------------------------------------------------------------
			CallServer_Timer_Callback
------------------------------------------------------------------------*/
void CallServer_Timer_Callback(void)	//R_
{
#if 0
	CALLSERVER_STRU	send_msg;
	unsigned int			timeout_flag;
	
	timeout_flag = 0;

	CallServer_Run.timer++;	//��ʱ�ۼ�
		
	switch (CallServer_Run.state)
	{
		case CallServer_Invite:		//�ȴ�����Ӧ��
			if (CallServer_Run.timer >= CallServer_Invite_LimitTime)
			{
				timeout_flag = 1;
			}
			break;

		case CallServer_Ring:	//�ȴ�����ժ��	
			if (CallServer_Run.timer >= CallServer_Ring_LimitTime)
			{
				timeout_flag = 1;
			}
			break;
			
		case CallServer_Ack:		//�ȴ����йһ�
			if (CallServer_Run.timer >= CallServer_Ack_LimitTime)
			{
				timeout_flag = 1;
			}
			break;

		case CallServer_SourceBye:		//�ȴ�����Ӧ��
		case CallServer_TargetBye:
			if (CallServer_Run.timer >= CallServer_Bye_LimitTime)
			{
				timeout_flag = 1;
			}
			break;
			
		default:				//״̬�쳣����
			timeout_flag = 1;
		  	break;
	}
        
	if (timeout_flag)
	{
		send_msg.msg_head.msg_type 		= CallServer_Msg_Timeout;
		send_msg.msg_head.msg_sub_type	= 0;	
		if (OS_Q_Put(&q_callserver, &send_msg, CALLSERVER_STRU_HEAD))
		{
			Caller_Run.timer--;
			OS_RetriggerTimer(&timer_callserver);	//�������? �ٴδ�����ʱ, �Ա��´η�����Ϣ
		}
		else 
		{
			OS_StopTimer(&timer_callserver);		//��Ϣѹ����гɹ�? �رն�ʱ
			OS_SignalEvent(TASK_EVENT_CTRL_LOCAL_MSG,&tcb_callserver);
		}			
	}
	else
	{
	  	OS_RetriggerTimer(&timer_callserver);
	}
	#endif
}


/*------------------------------------------------------------------------
						OutCall&CallIn Task Process
------------------------------------------------------------------------*/


void callserver_mesg_data_process(void *Msg,int len )	//R_
{
	
	//vtk_TaskProcessEvent_Caller(&DtCaller_Obj,(CALLER_STRUCT *)Msg);
	CallServer_Business_Process((CALLSERVER_STRU*)Msg);
}

//OneCallType	OneMyCallObject;

void init_vdp_callserver_task(void)
{
	CallServer_Run.state = CallServer_Wait;
	CallServer_Run.target_array=NULL;
	CallServer_Run.source_info=NULL;
	init_vdp_common_queue(&vdp_callserver_mesg_queue, 3500, (msg_process)callserver_mesg_data_process, &task_callserver);	//czn_20190527
	init_vdp_common_queue(&vdp_callserver_sync_queue, 1000, NULL, 								  &task_callserver);
	init_vdp_common_task(&task_callserver, MSG_ID_CallServer, vdp_callserver_task, &vdp_callserver_mesg_queue, &vdp_callserver_sync_queue);

	//init_vdp_dtbecalled_task();
	init_vdp_dtcaller_task();
	init_vdp_ipbecalled_task();
	init_vdp_ipcaller_task();
	init_vdp_sipcaller_task();
	printf("======init_vdp_callserver_task===========\n");	
}

void exit_callserver_task(void)
{
	
}

void* vdp_callserver_task( void* arg )
{
	vdp_task_t*	 ptask 			= (vdp_task_t*)arg;
	p_vdp_common_buffer pdb 	= 0;
	int	size;
	thread_log_add(__func__);
	API_PublicInfo_Write(PB_CALL_INFO,GetCallserverStatePb());
	API_Event_By_Name(EventCallState);
	ptask->task_StartCompleted = 1;
	while( ptask->task_run_flag )
	{
		size = pop_vdp_common_queue(ptask->p_msg_buf, &pdb, VDP_QUEUE_POLLING_TIME);
		if( size > 0 )
		{
			(*ptask->p_msg_buf->process)(pdb,size);
			purge_vdp_common_queue( ptask->p_msg_buf );
		}
	}
	return 0;
}

int Get_CallScene_ByPara(uint8 para_type,uint8 para_len,uint8 *para)
{
	if(para_type == IxCallServer_ParaType1)
	{
		char para_temp[5] = {0};
		
		memcpy(para_temp,para,4);

		if(strcmp(para_temp,"0000") == 0)
		{
			return IxCallScene1_Active;
		}
		else
		{
			return IxCallScene2_Active;
		}
	}

	return -1;
}

int Load_TargetAddr_ByPara(uint8 para_type,uint8 para_len,uint8 *para)
{
#if 0
	#if 0
	CallServer_Run.tdev_nums = 2;
	CallServer_Run.t_addr_ext[0].gatewayid = 0;
	CallServer_Run.t_addr_ext[0].ip = inet_addr("192.168.1.201");
	CallServer_Run.t_addr_ext[0].rt = 0;
	CallServer_Run.t_addr_ext[0].code = 0;
	CallServer_Run.t_addr_ext[1].gatewayid = 0;
	CallServer_Run.t_addr_ext[1].ip = inet_addr("192.168.1.202");
	CallServer_Run.t_addr_ext[1].rt = 0;
	CallServer_Run.t_addr_ext[1].code = 0;
	#endif
	
	Global_Addr_Stru addr;
	CallServer_Run.tdev_nums = 0;
	
	CACHE_TABLE_T data;
	bprintf("API_GetIpByBdRmMs start:%s\n",para);
	if(para_type == IxCallServer_ParaType1)
	{
		if(API_GetIpByBdRmMs(para, &data) == 0)
		{
			CallServer_Run.tdev_nums = 1;
			CallServer_Run.t_addr_ext[0].gatewayid = 0;
			CallServer_Run.t_addr_ext[0].ip = inet_addr(data.IP);
			CallServer_Run.t_addr_ext[0].rt = 0;
			CallServer_Run.t_addr_ext[0].code = 0;
			bprintf("API_GetIpByBdRmMs ok:%s\n",para);
			return 0;
		}
		else
		bprintf("API_GetIpByBdRmMs fail:%s\n",para);
	}
	return -1;
	#endif
}
int Get_SelfParaType1Info(uint8 *dev_type,uint8 *para_buff)
{
	if(dev_type != NULL)
	{
		*dev_type = IxDevType_Ds;
	}
	
	if(para_buff != NULL)
	{
		//sprintf(para_buff,"0001%04d",GetSysVerInfo().id);
		sprintf(para_buff,GetSysVerInfo_BdRmMs());
	}
	
	return 0;
}

void Get_SelfDevInfo(int ip, Call_Dev_Info *dev_info)
{
	memset(dev_info,0,sizeof(Call_Dev_Info));
	dev_info->ip_addr = GetLocalIpByDevice(GetNetDeviceNameByTargetIp(ip));
	strcpy(dev_info->bd_rm_ms,GetSysVerInfo_BdRmMs());
	strcpy(dev_info->name,GetSysVerInfo_name());
}

int Get_CallScene_BySourseInfo(Call_Dev_Info dev_info)		//czn_20190506
{
	int ms;
	ms = atol(dev_info.bd_rm_ms+8);
	
	if(memcmp(dev_info.bd_rm_ms+4,"9901",4) == 0 ||memcmp(dev_info.bd_rm_ms+4,"9902",4) == 0)
	{
		return IxCallScene1_Passive;
	}
	if(memcmp(dev_info.bd_rm_ms+4,"0000",4) == 0)
	{
		//ms = atol(dev_info.bd_rm_ms+8);
		if(ms >= 1&&ms <= 48)
			return IxCallScene1_Passive;
		if(ms >= 51&&ms <= 98)
			return IxCallScene2_Passive;
	}
	
	if(memcmp(dev_info.bd_rm_ms,GetSysVerInfo_BdRmMs(),8) == 0)
	{
		//ms = atol(dev_info.bd_rm_ms+8);
		if(ms >= 1&&ms <= 32)
			return IxCallScene3_Passive;
		if(ms >= 51&&ms <= 72)
			return IxCallScene1_Passive;
	}
	if(ms >= 1&&ms <= 32)
		return IxCallScene2_Passive;
	if(ms >= 51&&ms <= 72)		
		return IxCallScene1_Passive;
}

int TargetAddrErr_Process(uint8 para_type,uint8 para_len,uint8 *para)
{
	#if 0
	CACHE_TABLE_T data;
	//bprintf("API_GetIpByBdRmMs start:%s\n",para);
	if(para_type == IxCallServer_ParaType1)
	{
		if(API_GetIpByBdRmMsFromNet(para, &data) == 0)
		{
			return 0;
		}
		
	}
	#endif
	return -1;
}

int API_CallServer_Common(uint8 msg_type)
{

	CALLSERVER_STRU	send_msg;	
	char rev[5];
	int   rev_len;
	vdp_task_t* ptask = NULL;
	int i;
	//int send_len;
	//OS_TASK_EVENT MyEvents;	

	//��֯������Ϣ��BeCalled
	send_msg.msg_source_id 	= GetMsgIDAccordingPid(pthread_self());
	send_msg.msg_target_id	= MSG_ID_CallServer;
	send_msg.msg_type		= msg_type;
	send_msg.call_type		= CallServer_Run.call_type; //0;
	
	

	if(push_vdp_common_queue(task_callserver.p_msg_buf, (char *)&send_msg, CallServerMsgExtJsonPara) != 0)
	{
		return -1;
	}

	
	
	return 0;
}
#if 0
int API_CallServer_Common2(uint8 msg_type, uint8 call_type, Call_Dev_Info *source_dev,int target_num,void *target_list)//R
{
	CALLSERVER_STRU	send_msg;	
	char rev[5];
	int   rev_len;
	vdp_task_t* ptask = NULL;
	int i;
	int send_len;
	//OS_TASK_EVENT MyEvents;	

	//��֯������Ϣ��BeCalled
	send_msg.msg_source_id 	= GetMsgIDAccordingPid(pthread_self());
	send_msg.msg_target_id	= MSG_ID_CallServer;
	send_msg.msg_type		= msg_type;
	send_msg.call_type		= call_type;
	
	if(source_dev != NULL)
	{
		send_msg.source_dev = *source_dev;
	}

	if(push_vdp_common_queue(task_callserver.p_msg_buf, (char *)&send_msg, CallServerMsgExtTargetList_Length) != 0)
	{
		return -1;
	}

	if(msg_type == CallServer_Msg_Invite && ptask != NULL)
	{
		if(ptask ->p_syc_buf != NULL)
		{
			rev_len = 5;
			if(WaitForBusinessACK(ptask->p_syc_buf,msg_type,rev,&rev_len,1000) == 1)
			{
				if(rev_len >= 5)
				{
					if(rev[4] != 0)
					{
						return -1;
					}
				}
			}
		}
	}	

	return 0;
}
#endif

void CallServer_Business_Respones(unsigned char respones_id,unsigned char msg_type,unsigned char result)
{
	unsigned char rev[5];
	vdp_task_t* ptask = NULL;
	
	ptask = GetTaskAccordingMsgID(respones_id);

	if(ptask == NULL)
		return;
	
	rev[0] = respones_id;
	rev[1] = MSG_ID_CallServer;
	rev[2] = msg_type |0x80;
	rev[3] = 0;
	rev[4] = result;
	
	push_vdp_common_queue(ptask->p_syc_buf, rev, 5);
}

int GetCallInfo_JudgeWithLocalMenu(void)
{
	return (CallServer_Run.with_local_menu&0x01)? 1 : 0;
}

int GetCallInfo_JudgeIsLocalUnlock(void)
{
	return (CallServer_Run.with_local_menu&0x80)? 1 : 0;
}



//extern one_vtk_table* nameListTable;

int Get_CallPartnerName_ByAddr(Global_Addr_Stru addr,char *name)
{
	#if 0
	if(CallServer_Run.call_type == IxCallScene2_Master || CallServer_Run.call_type == DxCallScene2_Slave)
	{
		addr.rt = 0;
	}
	#endif
	#if 0
	if(addr.rt == 10)
	{
		if(Get_MonResName_ByAddr(DS1_ADDRESS+addr.code,name) != 0)
		{
			sprintf(name,"DS-%d",addr.code+1);
		}
		return 0;
	}
	else if(addr.rt == 0)
	{
		int name_keyindex,logicaddr_index,i,str_len;
		char str_buff[21];
		one_vtk_dat		*precord;
		if(nameListTable != NULL)
		{
			name_keyindex = get_keyname_index(nameListTable, "NAME");
			logicaddr_index = get_keyname_index(nameListTable, "ADDR");
			//czn_20160812_e
			if(name_keyindex == -1  || logicaddr_index == -1)
				return -1;
			
			
			for(i = 0;i < nameListTable->record_cnt;i++)
			{
				precord = get_one_vtk_record_without_keyvalue(nameListTable, i);
				
				if(precord == NULL)
					return -1;
				
				str_len = 8;
				get_one_record_string( precord, logicaddr_index, str_buff, &str_len );
				str_buff[str_len] = 0;

				if(addr.code == atol(str_buff))
				{
					str_len = 8;
					get_one_record_string( precord, name_keyindex, name, &str_len );
					name[str_len] = 0;
					break;
				}
			}
		}
		
		if(nameListTable == NULL || i >= nameListTable->record_cnt)
		{
			sprintf(name,"IM%d",addr.code);
		}
		return 0;
	}
	else if(addr.rt == DX432_RT)
	{
		strcpy(name,"InnerCall");
		return 0;
	}
	else if(addr.rt >= 1 && addr.rt <= 3)
	{
		strcpy(name,"InnerCall");
		return 0;
	}
	else if(addr.rt == 16)
	{
		strcpy(name,"Guard Station");
		return 0;
	}
	else if(addr.rt == LIPHONE_RT)
	{
		strcpy(name,"PhoneCall");
		return 0;
	}
	#endif
	return -1;
}
//czn_20190118_s
#if 0
Ring_Scene_Type Get_CallRingScene_ByAddr(Call_Dev_Info dev_info)
{
	Ring_Scene_Type result = RING_DS;
	DeviceTypeAndArea_T type_area;
	type_area = GetDeviceTypeAndAreaByNumber(dev_info.bd_rm_ms);

	if( type_area.type == TYPE_DS )
	{
		if( type_area.area == CommonArea )
		{
			result = RING_CDS;
		}
		else
		{
			result = RING_DS;
		}
	}
	if( type_area.type == TYPE_OS )
	{
		result = RING_OS;
	}
	else if( type_area.type == TYPE_IM || type_area.type == TYPE_GL )
	{
		if( type_area.type == TYPE_IM && type_area.area == SameRoom )
		{
			result = RING_InnerCall;
		}
		else
		{
			result = RING_Intercom;
		}
	}
	return result;
}
#endif
//czn_20190118_e
uint8 IfCallServerBusy(void)
{
	return (CallServer_Run.state != 0);//(CallServer_Run.state != 0 || Mon_Run.state != 0);
}

int API_CallserverThreadHeartbeatReq( void )	//czn_20190910
{
	VDP_MSG_HEAD 	msg;

	msg.msg_source_id 	= MSG_ID_CallServer; //GetMsgIDAccordingPid(pthread_self());	
	//video_menu_buf.head.msg_source_id 	= GetMsgIDAccordingPid(pthread_self());
	msg.msg_target_id 	= MSG_ID_CallServer;
	msg.msg_type		= CallServer_Msg_ThreadHeartbeatReq;
	msg.msg_sub_type	= 0;

	push_vdp_common_queue( task_callserver.p_msg_buf, (char*)&msg, sizeof(VDP_MSG_HEAD) );
	return 0;
}

#include "obj_GetIpByNumber.h"

int CallBackTest(char* rm)
{
#if 0
	GetIpRspData data;
	Call_Dev_Info target_dev;
	if(API_GetIpNumberFromNet(rm, NULL, NULL, 2, 1, &data) != 0)
	{					
		return -1;
	}
	memset(&target_dev,0,sizeof(Call_Dev_Info));
	strcpy(target_dev.bd_rm_ms,GetSysVerInfo_BdRmMs());
	target_dev.ip_addr = inet_addr(GetSysVerInfo_IP());
	
	if(Send_CmdCallbackReq(data.Ip[0],target_dev)!=0)
	{
		return -1;
	}
	return 0;
#endif
}

int CallTalkTest(void)
{
#if 1
	if(CallServer_Run.state == CallServer_Ring)
	{
		API_CallServer_LocalAck();
	}
	else if(CallServer_Run.state == CallServer_Ack)
	{
		API_CallServer_LocalBye();
	}
	#endif
}

int CallStop(void)
{
	API_CallServer_LocalBye();
}

int API_CallServer_Start(int call_type,cJSON *call_para)
{
	CALLSERVER_STRU	send_msg;	
	char rev[5];
	int   rev_len;
	vdp_task_t* ptask = NULL;
	int i;
	int send_len;
	char *pstr;
	//OS_TASK_EVENT MyEvents;	

	//��֯������Ϣ��BeCalled
	send_msg.msg_source_id 	= GetMsgIDAccordingPid(pthread_self());
	send_msg.msg_target_id	= MSG_ID_CallServer;
	send_msg.msg_type		= CallServer_Msg_Invite;
	send_msg.call_type		= call_type;
	pstr=cJSON_PrintUnformatted(call_para);
	if(pstr==NULL)
		return -1;
	send_len=strlen(pstr);
	if(send_len>=500)
	{
		free(pstr);
		return -1;
	}
	strcpy(send_msg.json_para,pstr);
	free(pstr);
	send_len=CallServerWithJsonPara_Length(send_len);
	
	
	ptask = GetTaskAccordingMsgID(send_msg.msg_source_id);
	if(ptask != NULL)
	{
		if(ptask ->p_syc_buf != NULL)
		{
			p_vdp_common_buffer pdb = 0;
			int pop_cnt = 0;
			while(pop_vdp_common_queue( ptask ->p_syc_buf,&pdb,1)>0 && pop_cnt++ < 10)
			{
				purge_vdp_common_queue(ptask ->p_syc_buf);
			}
		}
	}
	
	if(push_vdp_common_queue(task_callserver.p_msg_buf, (char *)&send_msg, send_len) != 0)
	{
		return -1;
	}

	if(ptask != NULL)
	{
		if(ptask ->p_syc_buf != NULL)
		{
			rev_len = 5;
			if(WaitForBusinessACK(ptask->p_syc_buf,CallServer_Msg_Invite,rev,&rev_len,1000) == 1)
			{
				if(rev_len >= 5)
				{
					if(rev[4] != 0)
					{
						return -1;
					}
				}
			}
		}
	}
	return 0;
}
int API_CallServer_IxRemoteAck(int hook_ip)
{
	CALLSERVER_STRU	send_msg;	
	char rev[5];
	int   rev_len;
	//vdp_task_t* ptask = NULL;
	int i;
	int send_len;
	//cjson *json_para;
	//char *pstr;
	//OS_TASK_EVENT MyEvents;	

	//��֯������Ϣ��BeCalled
	send_msg.msg_source_id 	= GetMsgIDAccordingPid(pthread_self());
	send_msg.msg_target_id	= MSG_ID_CallServer;
	send_msg.msg_type		= CallServer_Msg_RemoteAck;
	send_msg.call_type		= 0;
	//json_para=
	my_inet_ntoa(hook_ip,send_msg.json_para);
	//memcpy(send_msg.json_para,&tar_dev,sizeof(Call_Dev_Info));
	
	send_len=CallServerWithJsonPara_Length(strlen(send_msg.json_para));
	if(push_vdp_common_queue(task_callserver.p_msg_buf, (char *)&send_msg, send_len)!=0)
		return -1;
	return 0;
}
int API_CallServer_StartDivert(int call_type,cJSON *call_para)
{
	CALLSERVER_STRU	send_msg;	
	char rev[5];
	int   rev_len;
	vdp_task_t* ptask = NULL;
	int i;
	int send_len;
	char *pstr;
	//OS_TASK_EVENT MyEvents;	

	//��֯������Ϣ��BeCalled
	send_msg.msg_source_id 	= GetMsgIDAccordingPid(pthread_self());
	send_msg.msg_target_id	= MSG_ID_CallServer;
	send_msg.msg_type		= CallServer_Msg_Transfer;
	send_msg.call_type		= call_type;
	pstr=cJSON_Print(call_para);
	if(pstr==NULL)
		return -1;
	send_len=strlen(pstr);
	if(send_len>=500)
	{
		free(pstr);
		return -1;
	}
	strcpy(send_msg.json_para,pstr);
	free(pstr);
	send_len=CallServerWithJsonPara_Length(send_len);
	
	#if 0
	ptask = GetTaskAccordingMsgID(send_msg.msg_source_id);
	if(ptask != NULL)
	{
		if(ptask ->p_syc_buf != NULL)
		{
			p_vdp_common_buffer pdb = 0;
			int pop_cnt = 0;
			while(pop_vdp_common_queue( ptask ->p_syc_buf,&pdb,1)>0 && pop_cnt++ < 10)
			{
				purge_vdp_common_queue(ptask ->p_syc_buf);
			}
		}
	}
	#endif
	if(push_vdp_common_queue(task_callserver.p_msg_buf, (char *)&send_msg, send_len) != 0)
	{
		return -1;
	}
	#if 0
	if(ptask != NULL)
	{
		if(ptask ->p_syc_buf != NULL)
		{
			rev_len = 5;
			if(WaitForBusinessACK(ptask->p_syc_buf,CallServer_Msg_Invite,rev,&rev_len,1000) == 1)
			{
				if(rev_len >= 5)
				{
					if(rev[4] != 0)
					{
						return -1;
					}
				}
			}
		}
	}
	#endif

	return 0;
}


cJSON *GetCallserverState(void)
{
	static cJSON *state=NULL;
	cJSON *target_array=NULL;
	char *cur_calltype_str,*cur_state_str;
	char uk[]="UNKOWN";

	if(state!=NULL)
		cJSON_Delete(state);
	
	state=cJSON_CreateObject();
	if(state==NULL)
		return NULL;
	if(CallServer_Run.state==CallServer_Wait)
		cur_calltype_str=callscene_str[0];
	else if(CallServer_Run.call_type>=callscene_str_max)
		cur_calltype_str=uk;
	else
	{
		cur_calltype_str=callscene_str[CallServer_Run.call_type];
		target_array=cJSON_Duplicate(CallServer_Run.target_array,1);
	}
	if(CallServer_Run.state>=callserver_state_str_max)
		cur_state_str=uk;
	else
		cur_state_str=callserver_state_str[CallServer_Run.state];
	cJSON_AddStringToObject(state,"STATE",cur_state_str);
	cJSON_AddStringToObject(state,"SCENE",cur_calltype_str);
	if(target_array!=NULL)
		cJSON_AddItemToObject(state,"TARGER_ARRAY",target_array);
	return state;
}
extern cJSON *GetDtCallerState(void);
extern cJSON *GetSipCallerState(void);
extern cJSON *GetLinphoneState(void);
extern cJSON *GetVtkMediaTransState(void);
cJSON *GetCallserverAllState(void)
{
	static cJSON *all_state=NULL;
	int i;
	cJSON *one_ins;
	
	
	char buff[50];
	
	if(all_state!=NULL)
		cJSON_Delete(all_state);
	
	all_state=cJSON_CreateObject();
	if(all_state==NULL)
		return NULL;
	one_ins=GetCallserverState();
	if(one_ins!=NULL)
		cJSON_AddItemReferenceToObject(all_state,"CallServer",one_ins);
	
	one_ins=GetDtCallerState();
	if(one_ins!=NULL)
		cJSON_AddItemReferenceToObject(all_state,"DtCaller",one_ins);

	one_ins=GetSipCallerState();
	if(one_ins!=NULL)
		cJSON_AddItemReferenceToObject(all_state,"SipCaller",one_ins);

	one_ins=GetLinphoneState();
	if(one_ins!=NULL)
		cJSON_AddItemReferenceToObject(all_state,"Linphone",one_ins);

	one_ins=GetVtkMediaTransState();
	if(one_ins!=NULL)
		cJSON_AddItemReferenceToObject(all_state,"DivertTcpLink",one_ins);

	return all_state;
}
cJSON *GetCallserverStatePb(void)
{
	static cJSON *state=NULL;
	int i;
	cJSON *one_ins;
	cJSON *target_array=NULL;
	char *cur_calltype_str,*cur_state_str;
	char uk[]="UNKOWN";
	
	char buff[50];
	
	if(state!=NULL)
		cJSON_Delete(state);
	
	state=cJSON_CreateObject();
	if(state==NULL)
		return NULL;
	
	
	if(CallServer_Run.state>=callserver_state_str_max)
		cur_state_str=uk;
	else
		cur_state_str=callserver_state_str[CallServer_Run.state];
	
	cJSON_AddStringToObject(state,PB_CALL_SER_STATE,cur_state_str);
	if(CallServer_Run.state!=CallServer_Wait)
	{
		if(CallServer_Run.caller_mask&0x02)
			cJSON_AddTrueToObject(state,PB_CALL_SIP);
		else
			cJSON_AddFalseToObject(state,PB_CALL_SIP);
		
		if(CallServer_Run.call_type>=callscene_str_max)
			cur_calltype_str=uk;
		else
		{
			cur_calltype_str=callscene_str[CallServer_Run.call_type];
			//target_array=cJSON_Duplicate(CallServer_Run.target_array,1);
		}
		cJSON_AddStringToObject(state,PB_CALL_SER_TYPE,cur_calltype_str);
		if(CallServer_Run.call_type==IxCallScene1_Passive||CallServer_Run.call_type==IxCallScene2_Passive||CallServer_Run.call_type==IxCallScene3_Passive)
		{
			target_array=cJSON_Duplicate(CallServer_Run.source_info,1);
			if(target_array!=NULL)
				cJSON_AddItemToObject(state,PB_CALL_SOURCE,target_array);
		}
		else
		{
			target_array=cJSON_Duplicate(CallServer_Run.target_array,1);
			if(target_array!=NULL)
				cJSON_AddItemToObject(state,PB_CALL_TARGET,target_array);
		}

		if(CallServer_Run.call_type==DxMainCall)
		{
			cJSON_AddTrueToObject(state,PB_CALL_PART);
			one_ins=cJSON_AddObjectToObject(state,PB_CALL_DETAIL);
			cJSON_AddItemToObject(one_ins,PB_CALL_DTCALLER,cJSON_Duplicate(GetDtCallerState(),1));
			cJSON_AddItemToObject(one_ins,PB_CALL_SIPCALLER,cJSON_Duplicate(GetSipCallerState(),1));
		}
		if(CallServer_Run.call_type==IXGMainCall)
		{
			target_array=cJSON_Duplicate(CallServer_Run.source_info,1);
			if(target_array!=NULL)
				cJSON_AddItemToObject(state,PB_CALL_SOURCE,target_array);
			cJSON_AddFalseToObject(state,PB_CALL_PART);
			one_ins=cJSON_AddObjectToObject(state,PB_CALL_DETAIL);
			cJSON_AddItemToObject(one_ins,PB_CALL_DTCALLER,cJSON_Duplicate(GetDtCallerState(),1));
			cJSON_AddItemToObject(one_ins,PB_CALL_IPBECALLED,cJSON_Duplicate(GetIpBecalledState(),1));
		}
		if(CallServer_Run.call_type==IxCallScene1_Active)
		{
			target_array=cJSON_Duplicate(CallServer_Run.source_info,1);
			if(target_array!=NULL)
				cJSON_AddItemToObject(state,PB_CALL_SOURCE,target_array);
			cJSON_AddStringToObject(state,PB_CALL_ID,CallServer_Run.call_id);
			cJSON_AddTrueToObject(state,PB_CALL_PART);
			one_ins=cJSON_AddObjectToObject(state,PB_CALL_DETAIL);
			cJSON_AddItemToObject(one_ins,PB_CALL_IPCALLER,cJSON_Duplicate(GetIpCallerState(),1));
			cJSON_AddItemToObject(one_ins,PB_CALL_SIPCALLER,cJSON_Duplicate(GetSipCallerState(),1));
		}
		if(CallServer_Run.call_type==IxCallScene2_Active)
		{
			cJSON_AddTrueToObject(state,PB_CALL_PART);
			one_ins=cJSON_AddObjectToObject(state,PB_CALL_DETAIL);
			cJSON_AddItemToObject(one_ins,PB_CALL_IPCALLER,cJSON_Duplicate(GetIpCallerState(),1));
			//cJSON_AddItemToObject(one_ins,PB_CALL_SIPCALLER,cJSON_Duplicate(GetSipCallerState(),1));
		}
		if(CallServer_Run.call_type==IxCallScene1_Passive||CallServer_Run.call_type==IxCallScene2_Passive||CallServer_Run.call_type==IxCallScene3_Passive)
		{
			cJSON_AddTrueToObject(state,PB_CALL_PART);
			one_ins=cJSON_AddObjectToObject(state,PB_CALL_DETAIL);
			cJSON_AddItemToObject(one_ins,PB_CALL_IPBECALLED,cJSON_Duplicate(GetIpBecalledState(),1));
		}
		if(CallServer_Run.call_type==SipCall||CallServer_Run.call_type==VMSipCall)
		{
			cJSON_AddTrueToObject(state,PB_CALL_PART);
			target_array=cJSON_Duplicate(CallServer_Run.source_info,1);
			if(target_array!=NULL)
				cJSON_AddItemToObject(state,PB_CALL_SOURCE,target_array);
			cJSON_AddStringToObject(state,PB_CALL_ID,CallServer_Run.call_id);
			one_ins=cJSON_AddObjectToObject(state,PB_CALL_DETAIL);
			//cJSON_AddItemToObject(one_ins,PB_CALL_DTCALLER,cJSON_Duplicate(GetDtCallerState(),1));
			cJSON_AddItemToObject(one_ins,PB_CALL_SIPCALLER,cJSON_Duplicate(GetSipCallerState(),1));
		}
	}
	return state;
}
int ShellCmd_Call(cJSON *cmd)
{
	char *para=GetShellCmdInputString(cmd, 1);
	cJSON *jpara;
	
	if(para==NULL)
	{
		ShellCmdPrintf("hava no para");
		return 1;
	}
	if(strcmp(para,"state")==0)
	{	
		jpara=GetCallserverAllState();
		char* temp;
		temp=cJSON_Print(jpara);
		if(temp==NULL)
		{
			ShellCmdPrintf("Get State File");
			return 1;
		}
		ShellCmdPrintf(temp);
		free(temp);
	}
	else if(strcmp(para,"start")==0)
	{
		int callscene;
		para=GetShellCmdInputString(cmd, 2);
		
		if(para==NULL)
		{
			ShellCmdPrintf("hava no callscene");
			return 1;
		}
			
		for(callscene=1;callscene<callscene_str_max;callscene++)
		{
			if(strcmp(para,callscene_str[callscene])==0)
			{
				break;
			}
		}
		if(callscene>=callscene_str_max)
		{
			ShellCmdPrintf("unkown callscene");
			return 1;
		}
		
		para=GetShellCmdInputString(cmd, 3);
		if(para==NULL)
		{
			ShellCmdPrintf("have no json para");
			return 1;
		}
		jpara=cJSON_Parse(para);
		if(jpara!=NULL)
		{
			API_CallServer_Start(callscene,jpara);
			ShellCmdPrintf("Call Start");
			cJSON_Delete(jpara);
		}
		else
		{
			ShellCmdPrintf("json para is err");
		}
	}
	else if(strcmp(para,"divert")==0)
	{
		int callscene;
		para=GetShellCmdInputString(cmd, 2);
		
		if(para==NULL)
		{
			ShellCmdPrintf("hava no callscene");
			return 1;
		}
			
		for(callscene=1;callscene<callscene_str_max;callscene++)
		{
			if(strcmp(para,callscene_str[callscene])==0)
			{
				break;
			}
		}
		if(callscene>=callscene_str_max)
		{
			ShellCmdPrintf("unkown callscene");
			return 1;
		}
		
		para=GetShellCmdInputString(cmd, 3);
		if(para==NULL)
		{
			ShellCmdPrintf("have no json para");
			return 1;
		}
		jpara=cJSON_Parse(para);
		if(jpara!=NULL)
		{
			API_CallServer_Start(callscene,jpara);
			ShellCmdPrintf("Call Divert");
			cJSON_Delete(jpara);
		}
		else
		{
			ShellCmdPrintf("json para is err");
		}
	}
	else if(strcmp(para,"cancel")==0)
	{
		API_CallServer_LocalBye();
		ShellCmdPrintf("Call Cancel");
	}
	else if(strcmp(para,"link_dtim")==0)
	{
		#include "define_Command.h"

		#include "task_StackAI.h"
		int dt_addr;
		para=GetShellCmdInputString(cmd, 2);
		if(para==NULL)
		{
			ShellCmdPrintf("have no dt_addr");
			return 1;
		}
		dt_addr = strtoul(para,NULL,0);
		API_Stack_APT_Without_ACK(dt_addr, MR_LINKING_ST);
		#if 0
		if(DtMainCallLink(dt_addr)==1)
		{
			ShellCmdPrintf("link ok");
		}
		else
			ShellCmdPrintf("link fail");
		#endif
	}
	else if(strcmp(para,"ix_search")==0)
	{
		para=GetShellCmdInputString(cmd, 2);
		if(para==NULL)
		{
			ShellCmdPrintf("have no input_addr");
			return 1;
		}
		jpara=GetIxCallTargetByInput(para);
		if(jpara==NULL)
		{
			ShellCmdPrintf("have no dev");
			return 1;
		}	
		para=cJSON_Print(jpara);
		ShellCmdPrintf("%s",para);	
		free(para);
		cJSON_Delete(jpara);
	}
	return 0;
}
void M4KeyToCall(char *buff)
{
	if(buff[2]&0x40)
	{
		if( IfCallServerBusy() )
		{
			API_CallServer_LocalBye();
			return;
		}
			
		cJSON *call_para,*target;
		char temp[10];
		call_para=cJSON_CreateObject();
		if(call_para)
		{
			target=cJSON_AddObjectToObject(call_para,CallPara_Target);
			sprintf(temp,"0x%02x",0x84+(((buff[2]&0x0f)-1)<<2));
			cJSON_AddStringToObject(target,CallTarget_DtAddr,temp);
			target=cJSON_AddObjectToObject(call_para,CallPara_Divert);
			cJSON_AddStringToObject(target,CallTarget_SipAcc,"0e001ce06322");
			API_CallServer_Start(DxMainCall,call_para);
			cJSON_Delete(call_para);
		}
	}
}

void Send_CallEvent(char *event_type, const char* format, ...)
{
	char output[400] = {0};
	if(format)
	{
		va_list valist;
		va_start(valist, format);
		vsnprintf(output, 400, format, valist);
		va_end(valist);
	}

	cJSON *event;
	int room_num;
	char room_str[10];

	event=cJSON_CreateObject();
	cJSON_AddStringToObject(event, EVENT_KEY_EventName,EventCall);
	cJSON_AddStringToObject(event, "CALL_EVENT" ,event_type);
	if(CallServer_Run.call_type==DxMainCall)
	{
		room_num=GetTargetDtAddr();
		room_num=((room_num-0x80)>>2);
		if(room_num==0)
			room_num=32;
		sprintf(room_str,"%02d",room_num);
		cJSON_AddStringToObject(event, "ROOM_NBR",room_str);
	}

	cJSON_AddStringToObject(event, EVENT_KEY_Debug, output);
	
	API_Event_Json(event);
	cJSON_Delete(event);
}


/////////////////
void ShellCmd_Call_string(char* str_value)
{
	printf("%s\n",str_value);
	cJSON *target = cJSON_Parse(str_value);
	API_CallServer_Start(1,target);
	cJSON_Delete(target);
}
void ShellCmd_Call_close(void)
{
	API_CallServer_LocalBye();
}
/////////////////
void IXMainCall_CallStart(char *input,cJSON *tar_list)
{
	cJSON *para;
	cJSON *target_para;
	if(input==NULL&&tar_list)
		return;
	para=cJSON_CreateObject();
	target_para=cJSON_AddObjectToObject(para,"Target");
	if(input!=NULL)
		cJSON_AddStringToObject(target_para,CallTarget_Input,input);
	if(tar_list!=NULL)
		cJSON_AddItemToObject(target_para,CallTarget_IxDev,cJSON_Duplicate(tar_list,1));
	API_CallServer_Start(IxCallScene1_Active,para);
	cJSON_Delete(para);
}
static int API_CallServer_EventPush(int call_type,int msg_type,cJSON *call_para)
{
	CALLSERVER_STRU	*psend_msg=NULL;	
	char rev[5];
	int   rev_len;
	vdp_task_t* ptask = NULL;
	int i;
	int send_len;
	char *pstr;
	if(call_para==NULL)
	{
		send_len=CallServerWithJsonPara_Length(0);
		psend_msg=malloc(send_len);
		
	}
	else
	{
		pstr=cJSON_PrintUnformatted(call_para);
		if(pstr==NULL)
			return -1;
		send_len=strlen(pstr);
		send_len=CallServerWithJsonPara_Length(send_len);
		psend_msg=malloc(send_len);
	}
	//OS_TASK_EVENT MyEvents;	
	if(psend_msg==NULL)
		return -1;
	//printf_json(call_para,__func__);
	//��֯������Ϣ��BeCalled
	psend_msg->msg_source_id 	= GetMsgIDAccordingPid(pthread_self());
	psend_msg->msg_target_id	= MSG_ID_CallServer;
	psend_msg->msg_type		= msg_type;
	psend_msg->call_type		= call_type;
	
	
	//printf("11111111%s:%d len=%d\n",__func__,__LINE__,send_len);
	#if 0
	if(send_len>=500)
	{
		free(pstr);
		return -1;
	}
	#endif
	//printf("11111111%s:%d len=%d\n",__func__,__LINE__,send_len);
	strcpy(psend_msg->json_para,pstr);
	free(pstr);
	
	
	
	if(push_vdp_common_queue(task_callserver.p_msg_buf, (char *)psend_msg, send_len) != 0)
	{
		free(psend_msg);
		return -1;
	}
	free(psend_msg);
	return 0;
}
static cJSON *GetVmCallTarget(char *ixAddr)
{
	cJSON* devTb = NULL;
	cJSON* Where = cJSON_CreateObject();
	cJSON* View = cJSON_CreateArray();

	cJSON_AddStringToObject(Where, IX2V_IX_ADDR, ixAddr);

	//IXS_ProxyGetTb(&devTb, NULL, R8001AndSearch);
	#if 0
	cJSON* record = cJSON_CreateObject();
	cJSON_AddItemToArray(View, record);
	cJSON_AddStringToObject(record, IX2V_IP_ADDR, "");
	cJSON_AddStringToObject(record, IX2V_IX_ADDR, "");
	cJSON_AddStringToObject(record, IX2V_IX_TYPE, "");
	#endif
	//devTb=GetDataSetList();
	
	
	API_TB_SelectBySortByName(TB_NAME_VM_MAT, View, Where, 0);

	
	cJSON_Delete(Where);
	//cJSON_DeleteItemFromArray(View,0);
	return View;
}
static cJSON *GetCallTargetProcess( char *addr )
{
	cJSON *tar_array=NULL;
	cJSON *ixs_array=NULL;
	cJSON *dxg_array=NULL;
	cJSON *node;
	char ix_addr[11]={0};

	
	//bprintf("1111111111%s\n",addr);
	
	tar_array=cJSON_CreateArray();
	if(memcmp(addr+8,"49",2)!=0)
		API_JsonGetIpByNumber(NULL, NULL, addr, NULL, tar_array);
	//printf_json(tar_array, "111111");
	//if(cJSON_GetArraySize(target_array)>0)
	//	return target_array;
	//cJSON_Delete(target_array);
	if(memcmp(addr+4,"0000",4)!=0)
		memcpy(ix_addr,addr,8);
	else
		memcpy(ix_addr,addr,10);
	ixs_array=GetIXSByIX_ADDR(ix_addr);
	//printf_json(ixs_array, "2222222");
	if(memcmp(addr+4,"00",2)!=0&&memcmp(addr+6,"00",2)!=0)
	{
		char buff[11]={0};
		memcpy(buff,addr,6);
		memset(buff+6,'0',2);
		dxg_array=GetIXSByIX_ADDR(buff);

		strcat(ix_addr,"01");
		cJSON_ArrayForEach(node,dxg_array)
		{
			if(GetJsonDataPro(node,IX2V_IX_TYPE,buff))
			{
				if(strcmp(buff,"IXG")==0)
				{
					cJSON_ReplaceItemInObjectCaseSensitive(node,IX2V_IX_ADDR,cJSON_CreateString(ix_addr));
					cJSON_AddItemToArray(ixs_array,cJSON_Duplicate(node,1));
				}
			}
		}
		cJSON_Delete(dxg_array);
	}
	cJSON_ArrayForEach(node,ixs_array)
	{
		char* ipString = GetEventItemString(node,  IX2V_IP_ADDR);
		int tempCnt = cJSON_GetArraySize(tar_array);
		int index;
		for(index = 0; index < tempCnt; index++)
		{
			cJSON* tempDevice = cJSON_GetArrayItem(tar_array, index);
			if(inet_addr(GetEventItemString(tempDevice,  IX2V_IP_ADDR)) == inet_addr(ipString))
			{
				break;
			}
		}

		if(index >= tempCnt)
		{
			cJSON_AddItemToArray(tar_array,cJSON_Duplicate(node,1));
		}
	}
	cJSON_Delete(ixs_array);
	if(cJSON_GetArraySize(tar_array)==0&&API_Para_Read_Int(SIP_ENABLE)==1)
	{
		cJSON_Delete(tar_array);
		memcpy(ix_addr,addr,8);
		ix_addr[8]=0;
		//bprintf("1111111111%s\n",addr);
		tar_array=GetVmCallTarget(ix_addr);
		//printf_json(tar_array,__func__);
	}
	#if 0
	if(cJSON_GetArraySize(target_array)==0)
	{
		IXS_GetByNBR(NULL, NULL, addr, NULL, target_array);
	}
	#endif
	return tar_array;
}

int EventBECall_Process(cJSON *cmd)
{
	char buff[100];
	char bd_rm_ms[11];
	int i;
	int call_type;
	cJSON *call_tar;
	cJSON *divert_tar;
	cJSON *cmd_temp;
	cJSON *one_node;
	if(GetJsonDataPro(cmd, "CallType", buff)==0)
	{
		return 0;
	}
	for(i=1;i<callscene_str_max;i++)
	{
		if(strcmp(buff,callscene_str[i])==0)
		{
			break;
		}
	}
	if(i>=callscene_str_max)
	{
		return 0;
	}
	char* lanState = API_PublicInfo_Read_String(PB_SUB_LAN_STATE);
	//printf("11111111%s:%d\n",__func__,__LINE__);
	
	if(lanState==NULL || strcmp(lanState, "Connected")!=0)
	{
		Send_CallEvent("Calling_Error", "IxMainCall:net not connected!");
		return 0;
	}
	
	char* callserverState = cJSON_GetStringValue(API_PublicInfo_Read(PB_CALL_SER_STATE));
	#if !defined(PID_IX850)
	if(strcmp(callserverState, "Wait"))
	{
		//ShellCmdPrintf("EventBECall state:%s\n", callserverState);
		return 0;
	}
	#endif
	call_type=i;
	cmd_temp=cJSON_Duplicate(cmd,1);
	if(strcmp(buff,"IxMainCall")==0||strcmp(buff,"IxIntercomCall")==0||strcmp(buff,"IxInnerCall")==0)
	{
		//will_add
		int ifIxMainCall = (!strcmp(buff, "IxMainCall") ? 1 : 0);

		if(GetJsonDataPro(cmd_temp, CallPara_Target, &call_tar)==1)
		{
			if(cJSON_GetObjectItemCaseSensitive(call_tar, CallTarget_IxDev)==NULL)
			{
				cJSON *target_array=NULL;//cJSON_CreateArray();
				if(GetJsonDataPro(call_tar, CallTarget_Input, buff))
				{
					cJSON *tb_seach=R8001_G_L_NBRSearch(buff);
					if(cJSON_GetArraySize(tb_seach)>0)
					{
						one_node=cJSON_GetArrayItem(tb_seach,0);
						if(GetJsonDataPro(one_node, IX2V_IX_ADDR, buff))
						{
							target_array=GetCallTargetProcess(buff);//IXS_GetByNBR(NULL, NULL, buff, NULL, target_array);
							if(cJSON_GetArraySize(target_array)>0)
							{
								if(GetJsonDataPro(one_node, IX2V_IX_NAME, buff))
									cJSON_AddStringToObject(call_tar,PB_CALL_TAR_NAME,buff);
							}
						}
					}
					else
					{
						GetBdRmMsByInput(buff,bd_rm_ms);
						if(memcmp(buff,bd_rm_ms,8)==0||If_SearchByixAddr(bd_rm_ms))
							target_array=GetCallTargetProcess(bd_rm_ms);//IXS_GetByNBR(NULL, NULL, bd_rm_ms, buff, target_array);
						else
							target_array=cJSON_CreateArray();
					}
					cJSON_Delete(tb_seach);
					
				}
				else if (GetJsonDataPro(call_tar, PB_CALL_TAR_IxDevNum,buff))
				{
					target_array=GetCallTargetProcess(buff);//IXS_GetByNBR(NULL, NULL, buff, NULL, target_array);
				}

				//主呼叫，剔除OS
				if(ifIxMainCall)
				{
					cJSON *wh=cJSON_CreateObject();
					cJSON_AddStringToObject(wh, IX2V_IX_TYPE, DeviceTypeToString(TYPE_OS));
					API_TB_Delete(target_array, wh);
					cJSON_Delete(wh);
				}
				//printf_json(target_array,buff);
				if(cJSON_GetArraySize(target_array)>0)
				{
					CallTarIxDevSort(target_array);
					cJSON_AddItemToObject(call_tar,CallTarget_IxDev,target_array);
					one_node=cJSON_GetArrayItem(target_array,0);
					buff[0]=0;
					GetJsonDataPro(one_node, IX2V_IX_TYPE, buff);
					if(strcmp(buff,"VM")==0)
					{
						cJSON_ReplaceItemInObject(cmd_temp,"CallType",cJSON_CreateString("VMSipCall"));
						call_type=VMSipCall;
						if(GetJsonDataPro(one_node, "VM_ACC", buff))	
							cJSON_AddStringToObject(call_tar,CallTarget_SipAcc,buff);
						
						GetJsonDataPro(one_node, IX2V_IX_ADDR, bd_rm_ms);
						cJSON_AddStringToObject(call_tar,PB_CALL_TAR_IXADDR,bd_rm_ms);
						
						if(GetJsonDataPro(call_tar, PB_CALL_TAR_NAME, buff)==0)
						{
							if(GetJsonDataPro(one_node, IX2V_IX_NAME, buff))
								cJSON_AddStringToObject(call_tar,PB_CALL_TAR_NAME,buff);
						}
					}
					else
					{
						if(GetJsonDataPro(call_tar, PB_CALL_TAR_NAME, buff)==0)
						{
							char ip_addr[20];
							DeviceInfo dev_info;
							GetJsonDataPro(one_node, IX2V_IX_ADDR, ip_addr);
							
							cJSON* View=GetDataSetListByIX_ADDR(ip_addr);
							if(cJSON_GetArraySize(View)>0)
							{	
								if(GetJsonDataPro(cJSON_GetArrayItem(View,0), IX2V_IX_NAME, dev_info.name1))
									cJSON_AddStringToObject(call_tar,PB_CALL_TAR_NAME,dev_info.name1);

							}
							else
							{
								GetJsonDataPro(one_node, IX2V_IP_ADDR, ip_addr);
								
								if(API_GetInfoByIp(inet_addr(ip_addr),2,&dev_info)==0)
									cJSON_AddStringToObject(call_tar,PB_CALL_TAR_NAME,dev_info.name1);
							}
							if(View)
								cJSON_Delete(View);
						}

						GetJsonDataPro(one_node, IX2V_IX_ADDR, bd_rm_ms);

						cJSON_AddStringToObject(call_tar,PB_CALL_TAR_IXADDR,bd_rm_ms);
						
						if(API_Para_Read_Int(SIP_ENABLE)==1&&judge_ixim_divert_on_by_addr(bd_rm_ms))
						{
							if(get_sip_ix_im_acc_from_tb2(bd_rm_ms, buff,NULL)>=0)
							{
								divert_tar=cJSON_AddObjectToObject(cmd_temp,CallPara_Divert);
								cJSON_AddStringToObject(divert_tar,CallTarget_SipAcc,buff);
							}
						}
					}
				}
				else
				{
					Send_CallEvent("Calling_Error", "IxMainCall:Search Fail");
					cJSON_Delete(target_array);
					cJSON_Delete(cmd_temp);
					cmd_temp = NULL;
					return 0;
				}
			}
		}
	}
	else if(strcmp(buff,"DxMainCall")==0)
	{
		if(GetJsonDataPro(cmd, "Target", &call_tar)==1)
		{
			if(GetJsonDataPro(call_tar, "Call_Tar_DtAddr", buff))
			{
				int dt_addr;
				dt_addr = strtoul(buff,NULL,0);
				dt_addr=((dt_addr&0x7f)>>2);
				if(dt_addr==0)
					dt_addr=32;
				snprintf(buff, 10, "%02d", dt_addr);
				DR_CallDisplayProcess(CALLING_START, buff);

			}
		}
	}
	//printf_json(cmd_temp, __func__);
	API_CallServer_EventPush(call_type,CallServer_Msg_Invite,cmd_temp);
	cJSON_Delete(cmd_temp);
	return 1;
}
int EventBECallRetrig_Process(cJSON *cmd)
{
	char buff[100];
	int i;
	int call_type;
	call_type=0;
	if(GetJsonDataPro(cmd, "CallType", buff)==1)
	{
		for(i=1;i<callscene_str_max;i++)
		{
			if(strcmp(buff,callscene_str[i])==0)
			{
				break;
			}
		}
		if(i<callscene_str_max)
		{
			call_type=i;
		}
	}
	API_CallServer_EventPush(call_type,CallServer_Msg_Redail,cmd);
	return 1;
}
int EventBECallClose_Process(cJSON *cmd)
{
	char buff[100];
	int i;
	int call_type;
	call_type=0;
	if(GetJsonDataPro(cmd, "CallType", buff)==1)
	{
		for(i=1;i<callscene_str_max;i++)
		{
			if(strcmp(buff,callscene_str[i])==0)
			{
				break;
			}
		}
		if(i<callscene_str_max)
		{
			call_type=i;
		}
	}
	API_CallServer_EventPush(call_type,CallServer_Msg_LocalBye,cmd);
	return 1;
}
int EventBECallBye_Process(cJSON *cmd)
{
	char buff[100];
	int i;
	int call_type;
	int msg_type;
	call_type=0;
	msg_type=CallServer_Msg_RemoteBye;
	
	if(GetJsonDataPro(cmd, "CallType", buff)==1)
	{
		for(i=1;i<callscene_str_max;i++)
		{
			if(strcmp(buff,callscene_str[i])==0)
			{
				break;
			}
		}
		if(i<callscene_str_max)
		{
			call_type=i;
		}
	}
	if(GetJsonDataPro(cmd, EVENT_KEY_Message, buff)==1)
	{
		if(strcmp(buff,"No_Disturb")==0)
		{
			msg_type=CallServer_Msg_InviteFail;
		}
		if(strcmp(buff,"Link_fail")==0)
		{
			msg_type=CallServer_Msg_InviteFail;
		}
	}
	if(GetJsonDataPro(cmd, "From", buff)==1)
	{
		if(strcmp(buff,"App")==0)
		{
			msg_type=CallServer_Msg_AppBye;
		}
	}
	API_CallServer_EventPush(call_type,msg_type,cmd);
	return 1;
}
int EventBECallRing_Process(cJSON *cmd)
{
	char buff[100];
	int i;
	int call_type;
	int msg_type;
	call_type=0;
	msg_type=CallServer_Msg_InviteOk;
	if(GetJsonDataPro(cmd, "CallType", buff)==1)
	{
		for(i=1;i<callscene_str_max;i++)
		{
			if(strcmp(buff,callscene_str[i])==0)
			{
				break;
			}
		}
		if(i<callscene_str_max)
		{
			call_type=i;
		}
	}
	if(GetJsonDataPro(cmd, "From", buff)==1)
	{
		if(strcmp(buff,"App")==0)
		{
			msg_type=CallServer_Msg_AppInviteOk;
		}
	}
	API_CallServer_EventPush(call_type,msg_type,cmd);
	return 1;
}
int EventBECallTalk_Process(cJSON *cmd)
{
	char buff[100];
	int i;
	int call_type;
	int msg_type;
	call_type=0;
	msg_type=CallServer_Msg_RemoteAck;
	if(GetJsonDataPro(cmd, "CallType", buff)==1)
	{
		for(i=1;i<callscene_str_max;i++)
		{
			if(strcmp(buff,callscene_str[i])==0)
			{
				break;
			}
		}
		if(i<callscene_str_max)
		{
			call_type=i;
		}
	}
	if(GetJsonDataPro(cmd, "From", buff)==1)
	{
		if(strcmp(buff,"App")==0)
		{
			msg_type=CallServer_Msg_AppAck;
		}
	}
	API_CallServer_EventPush(call_type,msg_type,cmd);
	return 1;
}
int EventBECallDivert_Process(cJSON *cmd)
{
	char buff[100];
	int i;
	int call_type;
	call_type=0;
	if(GetJsonDataPro(cmd, "CallType", buff)==1)
	{
		for(i=1;i<callscene_str_max;i++)
		{
			if(strcmp(buff,callscene_str[i])==0)
			{
				break;
			}
		}
		if(i<callscene_str_max)
		{
			call_type=i;
		}
	}
	API_CallServer_EventPush(call_type,CallServer_Msg_Transfer,cmd);
	return 1;
}
int EventBECallAnswer_Process(cJSON *cmd)
{
	char buff[100];
	int i;
	int call_type;
	call_type=0;
	if(GetJsonDataPro(cmd, "CallType", buff)==1)
	{
		for(i=1;i<callscene_str_max;i++)
		{
			if(strcmp(buff,callscene_str[i])==0)
			{
				break;
			}
		}
		if(i<callscene_str_max)
		{
			call_type=i;
		}
	}
	API_CallServer_EventPush(call_type,CallServer_Msg_LocalAck,cmd);
	return 1;
}
void DtCallBackCmd_process(int address)
{
	cJSON *call_event;
	char  *event_str;
	cJSON *tar_list;
	cJSON *call_tar;
	cJSON *divert_tar;
	char buff[100];
	call_event=cJSON_CreateObject();
	cJSON_AddStringToObject(call_event, EVENT_KEY_EventName,EventBECall);
	cJSON_AddStringToObject(call_event, "CallType","DxMainCall");
	cJSON_AddStringToObject(call_event, "From","DT");
	call_tar=cJSON_AddObjectToObject(call_event,"Target");
	sprintf(buff,"0x%02x",address);
	cJSON_AddStringToObject(call_tar,"Call_Tar_DtAddr",buff);
	if(judge_dtim_divert_on_by_addr(address))
	{
		if(get_sip_dt_im_acc_from_tb(address, buff,NULL)>=0)
		{
			divert_tar=cJSON_AddObjectToObject(call_event,CallPara_Divert);
			cJSON_AddStringToObject(divert_tar,CallTarget_SipAcc,buff);
		}
	}
	API_Event_Json(call_event);
	
	cJSON_Delete(call_event);
}

#if 0
pthread_mutex_t DsCallRecord_lock=PTHREAD_MUTEX_INITIALIZER;
cJSON *One_DsCallRecord=NULL;
#define CallRecord_Key_CallId			"Call_ID"
#define CallRecord_Key_CallTarget		"Call_Target"
#define CallRecord_Key_CallSource		"Call_Source"
#define CallRecord_Key_Addr			"Address"
#define CallRecord_Key_Name			"Name"
static int call_sn=0;
int CreateNewCallID(char *call_id)
{
	pthread_mutex_lock(&DsCallRecord_lock);
	call_sn=API_Para_Read_Int(CALL_Next_Sn);
	sprintf(call_id,"%s%04d",GetSysVerInfo_Sn(),call_sn);
	call_sn++;
	API_Para_Write_Int(CALL_Next_Sn, call_sn);
	pthread_mutex_unlock(&DsCallRecord_lock);
	return 0;
}
int Event_DsCallRecord(cJSON *cmd)
{
	pthread_mutex_lock(&DsCallRecord_lock);
    char* eventName = cJSON_GetStringValue(cJSON_GetObjectItemCaseSensitive(cmd, EVENT_KEY_EventName));
    char* callserverState = cJSON_GetStringValue(API_PublicInfo_Read(PB_CALL_SER_STATE));
    int Call_Part = API_PublicInfo_Read_Bool(PB_CALL_PART);
    char* Call_Tar_DtAddr = cJSON_GetStringValue(API_PublicInfo_Read(PB_CALL_TAR_DTADDR));
    
    if(callserverState == NULL)
    {
    	pthread_mutex_unlock(&DsCallRecord_lock);
        return 0;
    }


    //dprintf("callserverState = %s, Call_Part = %d, Call_Tar_DtAddr = %s\n", callserverState, Call_Part, Call_Tar_DtAddr);

   	if(callserverState)
    {
        if(!strcmp(callserverState, "Wait"))
        {
			//CallVoice("Event_CALL_CLOSE");
        }
        else if(Call_Part && (!strcmp(callserverState, "Ring")))
        {
			//CallVoice("Event_CALL_SUCC");
			if(API_PublicInfo_Read(PB_CALL_IPCALLER)!=NULL)
			{
				
			}
        }
        else if(Call_Part && (!strcmp(callserverState, "Ack") || !strcmp(callserverState, "RemoteAck")))
        {
        		
        		if(API_PublicInfo_Read_Bool(PB_CALL_SIP)==1||API_PublicInfo_Read(PB_CALL_IPCALLER)!=NULL||API_PublicInfo_Read(PB_CALL_IPBECALLED)!=NULL)
        			BEEP_CONFIRM();
       		else
				CallVoice("Event_CALL_TALK");
        }
    }
pthread_mutex_unlock(&DsCallRecord_lock);
	return 1;
}
#endif
