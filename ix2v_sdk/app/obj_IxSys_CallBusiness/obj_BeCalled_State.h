/**
  ******************************************************************************
  * @file    obj_BeCalled_State.h
  * @author  czn
  * @version V00.01.00 (basic on vsip)
  * @date    2014.11.07
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2014 V-Tec</center></h2>
  ******************************************************************************
  */ 

#ifndef _obj_BeCalled_State_H
#define _obj_BeCalled_State_H

#include "RTOS.h"
#include "OSTIME.h"
#include "task_survey.h"
#include "obj_IP_Call_Link.h"


// Define Object Property-------------------------------------------------------
	//Callback





	//BeCalled_Run

extern uint8 BeCalled_UnlockId;
extern uint8 BeCalled_ErrorCode;
	//摘机模式
#define BECALLED_MN_PICKUP	0
#define BECALLED_SJ_PICKUP	1

#define BECALLED_WAITING		0
#define BECALLED_RINGING		1
#define BECALLED_ACK			2
#define BECALLED_BYE			3
#define BECALLED_TRANSFER      	4

	//第几把锁
#define	BECALLED_UNLOCK1		1
#define	BECALLED_UNLOCK2		2

#define BECALLED_MAX_SUBMSG		16

#define MAX_BECALLED_MSGBUF_LEN		20

extern const char *becalled_state_str[];
extern const int becalled_state_str_max;
// Define task interface-------------------------------------------------------------

typedef struct {
	unsigned char 		msg_target_id;		// 消息的目标对象
	unsigned char 		msg_source_id;		// 消息的源对象
	unsigned char 		msg_type;			// 消息的类型
	unsigned char  		msg_sub_type;		 //消息的子类型
	unsigned char 		call_type;
	Global_Addr_Stru 		s_addr;
	Global_Addr_Stru 		t_addr;
}BECALLED_STRUCT;



#define BECALLED_STRUCT_BASIC_LENGTH  (uint16)&(((BECALLED_STRUCT*)0)->ext_buf[0])

typedef struct BECALLED_RUN_STRU	//Caller_run
{	
	unsigned char			state;					//呼叫状态
	unsigned char			call_type;				//呼叫类型
	Global_Addr_Stru 		s_addr;
	Global_Addr_Stru 		t_addr;
	unsigned short		timer;					//呼叫定时
	unsigned short		checklink_error;
}BECALLED_RUN;

// Define Object Property-------------------------------------------------------
typedef struct BECALLED_CONFIG_STRU	//bdu_zxj
{	
  	uint16	limit_ringing_time;	//呼叫等待定时
	uint16	limit_ack_time;		//呼叫通话定时
	uint8	limit_bye_time;
	uint16	limit_transfer_time;
	uint16	limit_autoredial_time;
}  BECALLED_CONFIG;

extern BECALLED_CONFIG BeCalled_Config;

typedef struct
{
	unsigned char call_type;
	void 		(*callback)(BECALLED_STRUCT *msg);
} STRUCT_BECALLED_CALLTYPE_CALLBACK;


	
typedef struct 
{
	BECALLED_RUN 		BeCalled_Run;
	BECALLED_CONFIG 	BeCalled_Config;
	OS_TIMER 			*timer_becalled;
	vdp_task_t			*task_becalled;
		
	int					support_calltype_num;
	STRUCT_BECALLED_CALLTYPE_CALLBACK		*to_ring_callback;
	STRUCT_BECALLED_CALLTYPE_CALLBACK		*to_ack_callback;
	STRUCT_BECALLED_CALLTYPE_CALLBACK		*to_bye_callback;
	STRUCT_BECALLED_CALLTYPE_CALLBACK		*to_wait_callback;
	STRUCT_BECALLED_CALLTYPE_CALLBACK		*to_timeout_callback;
	STRUCT_BECALLED_CALLTYPE_CALLBACK		*to_forceclose_callback;
	STRUCT_BECALLED_CALLTYPE_CALLBACK		*to_error_callback;
	STRUCT_BECALLED_CALLTYPE_CALLBACK		*to_unclock_callback;
	void		(*Config_Data_Init)(void);
	void 	(*MenuDisplay_ToRing)(void);
	void 	(*MenuDisplay_ToAck)(void);
	void 	(*MenuDisplay_ToBye)(void);
	void 	(*MenuDisplay_ToWait)(void);
}OBJ_BECALLED_STRU;

	//msg_type
typedef enum
{
	BECALLED_MSG_INVITE	= 1,
	BECALLED_MSG_RINGING,
	BECALLED_MSG_ACK,
	BECALLED_MSG_CANCEL,
	BECALLED_MSG_BYE,
	BECALLED_MSG_TRANSFER,
	BECALLED_MSG_ERROR,
	BECALLED_MSG_REDIAL,
	BECALLED_MSG_UNLOCK1,
	BECALLED_MSG_UNLOCK2,
	BECALLED_MSG_TIMEOUT,
	BECALLED_MSG_GETSTATE,
	BECALLED_MSG_FORCECLOSE,
}BECALLED_MSG_TYPE;

typedef enum
{
	BECALLED_TOUT_TIMEOVER = 0,
	BECALLED_TOUT_CHECKLINK,
}BECALLED_TIMEOUTMSG_SUBTYPE;

typedef enum
{
	BECALLED_ERROR_INVITEFAIL			= 1,
	BECALLED_ERROR_DTCALLER_QUIT,
	BECALLED_ERROR_DTBECALLED_QUIT,
	BECALLED_ERROR_UINTLINK_CLEAR,
}BECALLED_ERROR_TYPE;

		//结束呼叫_类型


#define	BECALLED_UNLOCK1				1
#define	BECALLED_UNLOCK2				2

// Define Task 2 items----------------------------------------------------------


//void vtk_TaskInit_BeCalled(void);	
void vtk_TaskProcessEvent_BeCalled(OBJ_BECALLED_STRU *becalled_obj,BECALLED_STRUCT  *msg_becalled);

//Define the functions
void BeCalled_Waiting_Process(OBJ_BECALLED_STRU *becalled_obj,BECALLED_STRUCT  *msg_becalled);
void BeCalled_Ringing_Process(OBJ_BECALLED_STRU *becalled_obj,BECALLED_STRUCT  *msg_becalled);
void BeCalled_Ack_Process(OBJ_BECALLED_STRU *becalled_obj,BECALLED_STRUCT  *msg_becalled);
void BeCalled_Bye_Process(OBJ_BECALLED_STRU *becalled_obj,BECALLED_STRUCT  *msg_becalled);
void BeCalled_Transfer_Process(OBJ_BECALLED_STRU *becalled_obj,BECALLED_STRUCT  *msg_becalled);
void BeCalled_StateInvalid_Process(OBJ_BECALLED_STRU *becalled_obj,BECALLED_STRUCT *msg_becalled);
void BeCalled_MsgInvalid_Process(OBJ_BECALLED_STRU *becalled_obj,BECALLED_STRUCT *msg_becalled);
//uint8 Get_BeCalled_State(void);
//uint16 Get_BeCalled_PartnerAddr(void);
//void Get_BeCalled_State_Rsp(BECALLED_STRUCT *msg_becalled);
//void IpBeCalled_Business_Rps(BECALLED_STRUCT *msg,unsigned char result);

// Define Task others-----------------------------------------------------------
void BeCalled_Timer_Callback(OBJ_BECALLED_STRU *becalled_obj);

// Define Object Function - Public---------------------------------------------
uint8 BeCalled_To_Redial(OBJ_BECALLED_STRU *becalled_obj,BECALLED_STRUCT *msg);
void BeCalled_To_Ringing(OBJ_BECALLED_STRU *becalled_obj,BECALLED_STRUCT *msg);
void BeCalled_To_Transfer(OBJ_BECALLED_STRU *becalled_obj,BECALLED_STRUCT *msg);
void BeCalled_To_Ack(OBJ_BECALLED_STRU *becalled_obj,BECALLED_STRUCT *msg);
void BeCalled_To_Bye(OBJ_BECALLED_STRU *becalled_obj,BECALLED_STRUCT *msg);
void BeCalled_To_Waiting(OBJ_BECALLED_STRU *becalled_obj,BECALLED_STRUCT *msg);
void BeCalled_To_Unlock(OBJ_BECALLED_STRU *becalled_obj,BECALLED_STRUCT *msg);
void BeCalled_To_Timeout(OBJ_BECALLED_STRU *becalled_obj,BECALLED_STRUCT *msg);
void BeCalled_To_Error(OBJ_BECALLED_STRU *becalled_obj,BECALLED_STRUCT *msg);
void BeCalled_To_ForceClose(OBJ_BECALLED_STRU *becalled_obj,BECALLED_STRUCT *msg);
// Define Object Function - Private---------------------------------------------


// Define Object Function - Other-----------------------------------------------



#endif
