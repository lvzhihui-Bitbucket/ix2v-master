/**
  ******************************************************************************
  * @file    task_Called.c
  * @author  czn
  * @version V00.02.00 (basic on vsip)
  * @date    2014.11.06
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2014 V-Tec</center></h2>
  ******************************************************************************
  */ 
  
#include <sys/types.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <error.h>
#include <signal.h>
#include <pthread.h>
#include <semaphore.h>
#include <sched.h>

#if 1

#include "obj_Caller_State.h"
#include "task_CallServer.h"
#include "task_IpCaller.h"
//#include "./obj_IpCaller_ByType_Callback/obj_IpCaller_MainCall_Callback.h"
#include "obj_IpCaller_IxSys_Callback.h"



#include "OSTIME.h"

OS_TIMER timer_ipcaller;

OBJ_CALLER_STRU IpCaller_Obj;

Loop_vdp_common_buffer	vdp_ipcaller_mesg_queue;
Loop_vdp_common_buffer	vdp_ipcaller_sync_queue;


vdp_task_t	task_ipcaller;
void* vdp_ipcaller_task( void* arg );

void IpCaller_Config_Data_Init(void);
void IpCaller_MenuDisplay_ToInvite(void);
void IpCaller_MenuDisplay_ToRing(void);
void IpCaller_MenuDisplay_ToAck(void);
void IpCaller_MenuDisplay_ToBye(void);
void IpCaller_MenuDisplay_ToWait(void);
void IpCaller_Timer_Callback(void);

#define IpCaller_Support_CallTypeNum	1


const STRUCT_CALLER_CALLTYPE_CALLBACK TABLE_CALLTYPE_IPCALLER_TOINVITE_CALLBACK[IpCaller_Support_CallTypeNum]={
	//calltype					//callback

	{IpCaller_IxSys, 			Callback_Caller_ToInvite_IxSys,},
};


const STRUCT_CALLER_CALLTYPE_CALLBACK TABLE_CALLTYPE_IPCALLER_TORINGING_CALLBACK[IpCaller_Support_CallTypeNum]={
	//calltype					//callback
	
	//{IpCaller_MainCall, 				Callback_Caller_ToRinging_MainCall,},		//小区主机呼叫分机
	{IpCaller_IxSys, 			Callback_Caller_ToRinging_IxSys,},
	
};

const STRUCT_CALLER_CALLTYPE_CALLBACK TABLE_CALLTYPE_IPCALLER_TOACK_CALLBACK[IpCaller_Support_CallTypeNum]={
	//calltype					//callback

	//{IpCaller_MainCall, 			Callback_Caller_ToAck_MainCall,},			//小区主机呼叫分机
	{IpCaller_IxSys, 		Callback_Caller_ToAck_IxSys,},
	
};


const STRUCT_CALLER_CALLTYPE_CALLBACK TABLE_CALLTYPE_IPCALLER_TOBYE_CALLBACK[]={
	//calltype					//callback
	
	//{IpCaller_MainCall, 			Callback_Caller_ToBye_MainCall,},			//小区主机呼叫分机
	{IpCaller_IxSys, 		Callback_Caller_ToBye_IxSys,},
	
};


const STRUCT_CALLER_CALLTYPE_CALLBACK TABLE_CALLTYPE_IPCALLER_TOWAITING_CALLBACK[]={
	//calltype					//callback

	//{IpCaller_MainCall, 		Callback_Caller_ToWaiting_MainCall,},		//小区主机呼叫分机
	{IpCaller_IxSys, 		Callback_Caller_ToWaiting_IxSys,},	
	
	
};

const STRUCT_CALLER_CALLTYPE_CALLBACK TABLE_CALLTYPE_IPCALLER_TOUNLOCK_CALLBACK[]={
	//calltype					//callback
	
	//{IpCaller_MainCall, 		Callback_Caller_ToUnlock_MainCall,},		//小区主机呼叫分机
	{IpCaller_IxSys, 		Callback_Caller_ToUnlock_IxSys,},
	
};

const STRUCT_CALLER_CALLTYPE_CALLBACK TABLE_CALLTYPE_IPCALLER_TOTIMEOUT_CALLBACK[IpCaller_Support_CallTypeNum]={
	//calltype					//callback

	//{IpCaller_MainCall, 		Callback_Caller_ToTimeout_MainCall,},		//小区主机呼叫分机
	{IpCaller_IxSys, 		Callback_Caller_ToTimeout_IxSys,},
	
};


const STRUCT_CALLER_CALLTYPE_CALLBACK TABLE_CALLTYPE_IPCALLER_TOERROR_CALLBACK[IpCaller_Support_CallTypeNum]={
	//calltype					//callback

	//{IpCaller_MainCall, 		Callback_Caller_ToError_MainCall,},		//小区主机呼叫分机
	{IpCaller_IxSys, 		Callback_Caller_ToError_IxSys,},
	
};


const STRUCT_CALLER_CALLTYPE_CALLBACK TABLE_CALLTYPE_IPCALLER_TOFORCECLOSE_CALLBACK[IpCaller_Support_CallTypeNum]={
	//calltype					//callback

	//{IpCaller_MainCall, 		Callback_Caller_ToForceClose_MainCall,},		//小区主机呼叫分机
	{IpCaller_IxSys, 		Callback_Caller_ToForceClose_IxSys,},	
	
};

const STRUCT_CALLER_CALLTYPE_CALLBACK TABLE_CALLTYPE_IPCALLER_TOREDAIL[IpCaller_Support_CallTypeNum]={		//czn_20171030
	//calltype					//callback

	//{IpCaller_MainCall, 		Callback_Caller_ToRedial_MainCall,},		//小区主机呼叫分机
	{IpCaller_IxSys, 		Callback_Caller_ToRedial_IxSys,},	
	
};
/*------------------------------------------------------------------------
			Caller_Task_Init
------------------------------------------------------------------------*/
void IpCaller_Obj_Init(void)	//R_
{
	//任务初始化
	IpCaller_Obj.Caller_Run.state = CALLER_WAITING;
	IpCaller_Obj.timer_caller = &timer_ipcaller;
	IpCaller_Obj.task_caller = &task_ipcaller;
	
	IpCaller_Obj.Config_Data_Init = IpCaller_Config_Data_Init;

	IpCaller_Obj.to_invite_callback = TABLE_CALLTYPE_IPCALLER_TOINVITE_CALLBACK;
	IpCaller_Obj.to_ring_callback = TABLE_CALLTYPE_IPCALLER_TORINGING_CALLBACK; 
	IpCaller_Obj.to_ack_callback= TABLE_CALLTYPE_IPCALLER_TOACK_CALLBACK;
	IpCaller_Obj.to_bye_callback= TABLE_CALLTYPE_IPCALLER_TOBYE_CALLBACK;
	IpCaller_Obj.to_wait_callback= TABLE_CALLTYPE_IPCALLER_TOWAITING_CALLBACK;
	IpCaller_Obj.to_unclock_callback= TABLE_CALLTYPE_IPCALLER_TOUNLOCK_CALLBACK;
	IpCaller_Obj.to_timeout_callback= TABLE_CALLTYPE_IPCALLER_TOTIMEOUT_CALLBACK;
	IpCaller_Obj.to_error_callback= TABLE_CALLTYPE_IPCALLER_TOERROR_CALLBACK;
	IpCaller_Obj.to_forceclose_callback= TABLE_CALLTYPE_IPCALLER_TOFORCECLOSE_CALLBACK;
	IpCaller_Obj.to_redail_callback = TABLE_CALLTYPE_IPCALLER_TOREDAIL;		//czn_20171030

	IpCaller_Obj.support_calltype_num = IpCaller_Support_CallTypeNum;
	IpCaller_Obj.MenuDisplay_ToInvite = IpCaller_MenuDisplay_ToInvite;
	IpCaller_Obj.MenuDisplay_ToRing= IpCaller_MenuDisplay_ToRing;
	IpCaller_Obj.MenuDisplay_ToAck= IpCaller_MenuDisplay_ToAck;
	IpCaller_Obj.MenuDisplay_ToBye= IpCaller_MenuDisplay_ToBye;
	IpCaller_Obj.MenuDisplay_ToWait= IpCaller_MenuDisplay_ToWait;

	
	//创建软定时(未启动)
	OS_CreateTimer(&timer_ipcaller, IpCaller_Timer_Callback, 1000/25);
}



/*------------------------------------------------------------------------
			Get_Caller_State
返回:
	0-WAITING
	1-BUSY
------------------------------------------------------------------------*/
uint8 Get_IpCaller_State(void)	//R
{
	if (IpCaller_Obj.Caller_Run.state==CALLER_WAITING)
	{
		return (0);
	}
	return (1);
}


/*------------------------------------------------------------------------
			Get_Caller_PartnerAddr
------------------------------------------------------------------------*/
//uint16 Get_Caller_PartnerAddr(void)	//R
//{
//	return (Caller_Run.partner_IA);
//}





/*------------------------------------------------------------------------
						OutCall&CallIn Task Process
------------------------------------------------------------------------*/
void ipcaller_mesg_data_process(void *Msg,int len )	//R_
{
	//bprintf("!!!ipcaller state = %d,call_type = %d,recv msg = %d\n",IpCaller_Obj.Caller_Run.state,((CALLER_STRUCT *)Msg)->call_type,((CALLER_STRUCT *)Msg)->msg_type);
	vtk_TaskProcessEvent_Caller(&IpCaller_Obj,(CALLER_STRUCT *)Msg);
	
}

//OneCallType	OneMyCallObject;

void init_vdp_ipcaller_task(void)
{
	init_vdp_common_queue(&vdp_ipcaller_mesg_queue, 1000, (msg_process)ipcaller_mesg_data_process, &task_ipcaller);
	init_vdp_common_queue(&vdp_ipcaller_sync_queue, 100, NULL, 								  &task_ipcaller);
	init_vdp_common_task(&task_ipcaller, MSG_ID_IpCalller, vdp_ipcaller_task, &vdp_ipcaller_mesg_queue, &vdp_ipcaller_sync_queue);
}

void exit_ipcaller_task(void)
{
	
}

void* vdp_ipcaller_task( void* arg )
{
	vdp_task_t*	 ptask 			= (vdp_task_t*)arg;
	p_vdp_common_buffer pdb 	= 0;
	int	size;
	IpCaller_Obj_Init();
	ptask->task_StartCompleted = 1;
	while( ptask->task_run_flag )
	{
		size = pop_vdp_common_queue(ptask->p_msg_buf, &pdb, VDP_QUEUE_POLLING_TIME);
		if( size > 0 )
		{
			(*ptask->p_msg_buf->process)(pdb,size);
			purge_vdp_common_queue( ptask->p_msg_buf );
		}
	}
	return 0;
}

void IpCaller_Timer_Callback(void)
{
	Caller_Timer_Callback(&IpCaller_Obj);
}

void IpCaller_Config_Data_Init(void)
{
	switch(IpCaller_Obj.Caller_Run.call_type)
	{
		case IpCaller_IxSys:
		case IpCaller_NewIxSys	:
		case IpCaller_DTIM:
			IpCaller_Obj.Caller_Config.limit_invite_time = 5;
			IpCaller_Obj.Caller_Config.limit_ringing_time = 40;
			IpCaller_Obj.Caller_Config.limit_ack_time = 90;
			IpCaller_Obj.Caller_Config.limit_bye_time = 5;
			break;
			
		case IpCaller_Transfer:
		case IpCaller_TransferVirtual:	
			// lzh_20191107_s
			IpCaller_Obj.Caller_Config.limit_invite_time = 30; //12;
			// lzh_20191107_e
			IpCaller_Obj.Caller_Config.limit_ringing_time = 40;
			IpCaller_Obj.Caller_Config.limit_ack_time = 120;
			IpCaller_Obj.Caller_Config.limit_bye_time = 5;
			break;
		
	}
}
void IpCaller_MenuDisplay_ToInvite(void)
{
#if 0
	extern Global_Addr_Stru Register_DxIm_List[];
	Set_IPCallLink_Status(CLink_AsCaller);
	Register_DxIm_List[0].ip = GatewayId_Trs2_IpAddr(Register_DxIm_List[0].gatewayid);
	Set_IPCallLink_CallerData(&Register_DxIm_List[0]);
#endif
}

void IpCaller_MenuDisplay_ToRing(void)
{
}

void IpCaller_MenuDisplay_ToAck(void)
{
	//Set_IPCallLink_CallerData(&CallServer_Run.t_addr);
}
void IpCaller_MenuDisplay_ToBye(void)
{
	//Set_IPCallLink_Status(CLink_Idle);
}
void IpCaller_MenuDisplay_ToWait(void)
{
	//Set_IPCallLink_Status(CLink_Idle);
}

int API_IpCaller_Common(uint8 msg_type, uint8 call_type, Global_Addr_Stru *s_addr,Global_Addr_Stru *t_addr)	//R
{
	CALLER_STRUCT	send_msg;	
	char rev[5];
	int   rev_len;
	vdp_task_t* ptask = NULL;
	//OS_TASK_EVENT MyEvents;	

	//组织发送消息给BeCalled
	send_msg.msg_source_id 	= GetMsgIDAccordingPid(pthread_self());
	send_msg.msg_target_id	= MSG_ID_CallServer;
	send_msg.msg_type		= msg_type;
	send_msg.msg_sub_type	= 0;
	send_msg.call_type		= call_type;
	
	if(s_addr != NULL)
	{
		send_msg.s_addr = *s_addr;
	}
	if(t_addr != NULL)
	{
		send_msg.t_addr = *t_addr;
	}
	#if 0
	if(msg_type == CALLER_MSG_INVITE)
	{
		ptask = GetTaskAccordingMsgID(send_msg.msg_source_id);
		if(ptask != NULL)
		{
			if(ptask ->p_syc_buf != NULL)
			{
				WaitForBusinessACK(ptask->p_syc_buf,msg_type,rev,&rev_len,1);
			}
		}
	}
	#endif
	if(push_vdp_common_queue(task_ipcaller.p_msg_buf, (char *)&send_msg, sizeof(CALLER_STRUCT)) != 0)
	{
		return -1;
	}
	#if 0
	if(msg_type == CALLER_MSG_INVITE && ptask != NULL)
	{
		if(ptask ->p_syc_buf != NULL)
		{
			if(WaitForBusinessACK(ptask->p_syc_buf,msg_type,rev,&rev_len,3000) == 1)
			{
				if(rev_len >= 5)
				{
					if(rev[4] != 0)
					{
						return -1;
					}
				}
			}
		}
	}
	#endif

	return 0;
}

void IpCaller_Respones(unsigned char respones_id,unsigned char msg_type,unsigned char result)
{
	unsigned char rev[5];
	vdp_task_t* ptask = NULL;
	
	ptask = GetTaskAccordingMsgID(respones_id);

	if(ptask == NULL)
		return;
	
	rev[0] = respones_id;
	rev[1] = MSG_ID_CallServer;
	rev[2] = msg_type |0x80;
	rev[3] = 0;
	rev[4] = result;
	
	push_vdp_common_queue(ptask->p_syc_buf, rev, 5);
}



///////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////

cJSON *GetIpCallerState(void)
{
	static cJSON *state=NULL;
	char *cur_calltype_str,*cur_state_str;
	char uk[]="UNKOWN";

	if(state!=NULL)
		cJSON_Delete(state);
	
	state=cJSON_CreateObject();
	if(state==NULL)
		return NULL;
	if(IpCaller_Obj.Caller_Run.state==CALLER_WAITING)
		cur_calltype_str="not-specified";
	else
	{
		if(CallServer_Run.call_type>=callscene_str_max)
			cur_calltype_str=uk;
		else
		{
			cur_calltype_str=callscene_str[CallServer_Run.call_type];
			//target_array=cJSON_Duplicate(CallServer_Run.target_array,1);
		}
	}
	#if 0
	else if(IpCaller_Obj.Caller_Run.call_type>=dtcaller_type_str_max)
		cur_calltype_str=uk;
	else
		cur_calltype_str=dtcaller_type_str[DtCaller_Obj.Caller_Run.call_type];
	#endif
	
	if(IpCaller_Obj.Caller_Run.state>=caller_state_str_max)
		cur_state_str=uk;
	else
		cur_state_str=caller_state_str[IpCaller_Obj.Caller_Run.state];
	cJSON_AddStringToObject(state,PB_CALL_IPCALLER_STATE,cur_state_str);
	cJSON_AddStringToObject(state,PB_CALL_IPCALLER_TYPE,cur_calltype_str);
	//cJSON_AddNumberToObject(state,"TIMER",DtCaller_Obj.Caller_Run.timer);
	return state;
}


#endif
/*********************************************************************************************************
**  End Of File
*********************************************************************************************************/

