/**
  ******************************************************************************
  * @file    obj_Caller_IPGVtkCall_Callback.c
  * @author  czn
  * @version V00.01.00 
  * @date    2016.01.21
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */
 #if 1
#include "task_CallServer.h"
#include "obj_Caller_State.h"
#include "task_IpCaller.h"



#include "obj_IpCaller_IxSys_Callback.h"

//czn_20190412
/*------------------------------------------------------------------------
						�ز�����
------------------------------------------------------------------------*/
void Callback_Caller_ToRedial_IxSys(CALLER_STRUCT *msg)		//czn_20171030
{
	#if 0
	int i;
	if(CallServer_Run.call_type == IxCallScene2)
	{
		for(i = 0;i < Register_DxIm_Num;i ++)
		{
			Send_VtkUnicastCmd_SourceRedailReq(Register_DxIm_List[i]);
		}
	}
	else
	{
		Send_VtkUnicastCmd_SourceRedailReq(IpCaller_Obj.Caller_Run.t_addr);
	}
	#endif
}

int GetCallTarIxDevNum(void)
{
	cJSON *tar_list;
	tar_list=cJSON_GetObjectItemCaseSensitive(CallServer_Run.target_array, CallTarget_IxDev);
	if(tar_list==NULL)
		return 0;
	return cJSON_GetArraySize(tar_list);
}
int GetCallTarIxDevInfo(int index,Call_Dev_Info *dev_info)
{
	cJSON *tar_list;
	tar_list=cJSON_GetObjectItemCaseSensitive(CallServer_Run.target_array, CallTarget_IxDev);
	if(tar_list==NULL||index>=cJSON_GetArraySize(tar_list))
		return -1;
	cJSON *one_node=cJSON_GetArrayItem(tar_list,index);
	char ip_addr[20];
	if(dev_info!=NULL)
	{
		memset(dev_info,0,sizeof(Call_Dev_Info));
		GetJsonDataPro(one_node, PB_CALL_TAR_IxDevNum, dev_info->bd_rm_ms);
		GetJsonDataPro(one_node, PB_CALL_TAR_IxDevIP, ip_addr);
		dev_info->ip_addr=inet_addr(ip_addr);
	}
	return 0;
}
void CallTarIxDevSort(cJSON *target_array)
{
	int i;
	int size=cJSON_GetArraySize(target_array);
	if(size<=1)
		return;
	cJSON *one_node;
	cJSON *first_node;
	char bd_rm_ms[11];
	first_node=cJSON_GetArrayItem(target_array,0);
	GetJsonDataPro(first_node, PB_CALL_TAR_IxDevNum,bd_rm_ms);
	if(memcmp(bd_rm_ms+8,"01",2)==0)
		return;
	for(i=1;i<size;i++)
	{
		one_node=cJSON_GetArrayItem(target_array,i);
		GetJsonDataPro(one_node, PB_CALL_TAR_IxDevNum,bd_rm_ms);
		if(memcmp(bd_rm_ms+8,"01",2)==0)
		{
			one_node=cJSON_DetachItemFromArray(target_array,i);
			cJSON_InsertItemInArray(target_array,0,one_node);
			return;
		}
	}
	
}
int GetBeCallSrcIxDevInfo(Call_Dev_Info *dev_info)
{
	char ip_addr[20];
	if(CallServer_Run.source_info==NULL)
		return -1;
	memset(dev_info,0,sizeof(Call_Dev_Info));
	GetJsonDataPro(CallServer_Run.source_info, PB_CALL_TAR_IxDevNum, dev_info->bd_rm_ms);
	GetJsonDataPro(CallServer_Run.source_info, PB_CALL_TAR_IxDevIP, ip_addr);
	dev_info->ip_addr=inet_addr(ip_addr);
	
	return 0;
}
int GetCallTarIxDevHookIp(char *ip_addr)
{
	char temp[20];
	if(GetJsonDataPro(CallServer_Run.target_array, CallTarget_IxHook, temp)==0)
		return -1;
	if(ip_addr!=NULL)
		strcpy(ip_addr,temp);
	return inet_addr(temp);
}
int GetBeCallRspID(void)
{
	return CallServer_Run.callee_rsp_command_id;
}
/*------------------------------------------------------------------------
						��������
------------------------------------------------------------------------*/

void Callback_Caller_ToInvite_IxSys(CALLER_STRUCT *msg)	//R_
{
	int i;
	Call_Dev_Info dev_info;
	Call_Dev_Info src_dev_info;
	for(i = 0;i <GetCallTarIxDevNum(); i ++)
	{
		if(GetCallTarIxDevInfo(i,&dev_info)!= 0)
			continue;
		if( i == 0 )
		{
			VideoDebugLogInit();
			Get_SelfDevInfo(dev_info.ip_addr,&src_dev_info);
			if( (CallServer_Run.callee_divert_state=Send_InviteCmd_VtkUnicast_master(dev_info.ip_addr,src_dev_info,dev_info,&CallServer_Run.callee_master_sip_divert)) >= 0 )
			{
				#if defined(PID_IX611)||defined(SUPPORT_IX2_DIVERT) || defined(PID_IX622)
				CallServer_Run.call_rule = CallRule_Normal;

				#else
				//czn_20190412_s
				if( CallServer_Run.callee_divert_state == 0x80 )
				{
					CallServer_Run.call_rule = CallRule_TransferIm;
					//set_autotest_call_result_divert();
					//snprintf(detail,60,"IP-CALL:call divert, SIP account (%s)",CallServer_Run.callee_master_sip_divert.sip_account);
					//API_AutoTest_LogWrite(detail,strlen(detail));
					//API_AutoTest_LogWrite("IP-CALL:call succ, target divert",strlen("IP-CALL:call succ, target divert"));
				}
				else if( CallServer_Run.callee_divert_state > 0x80 )
				{
					CallServer_Run.call_rule = CallRule_TransferNoAck;
					//set_autotest_call_result_divert();
					//set_autotest_call_result_succ();
					//API_AutoTest_LogWrite("IP-CALL:call succ, target rings",strlen("IP-CALL:call succ, target rings"));
				}
				else
				{
					CallServer_Run.call_rule = CallRule_Normal;	
					//set_autotest_call_result_succ();
					//API_AutoTest_LogWrite("IP-CALL:call succ, target rings",strlen("IP-CALL:call succ, target rings"));
				}
			
				//czn_20190412_e
				#endif
				// ͬ���ȴ����յ�ҵ��Ӧ���֪ͨcallserver�����������д���
				API_CallServer_InviteOk();				
				//printf("Send_InviteCmd_VtkUnicast_master ok,divert_state=0x%08x\n",CallServer_Run.callee_divert_state);
				//printf("divert server=%s\n",CallServer_Run.callee_master_sip_divert.sip_server);
				//printf("divert account=%s\n",CallServer_Run.callee_master_sip_divert.sip_account);
			}
			else
			{
				printf("Send_InviteCmd_VtkUnicast_master failed!\n");
				#if 0
				CallServer_Run.call_rule = CallRule_Normal;	
				set_autotest_call_result_busy();
				printf("Send_InviteCmd_VtkUnicast_master failed!\n");
				API_AutoTest_LogWrite("IP-CALL:call failed, target is BUSY",strlen("IP-CALL:call failed, target is BUSY"));
				#endif
			}
		}
		// ����ת����Ҫ�����ӷֻ��ĺ���
		else// if( CallServer_Run.call_rule != CallRule_TransferIm )	//czn_20190110
		{
			usleep(200*1000);
			Send_InviteCmd_VtkUnicast_slave(dev_info.ip_addr,src_dev_info,dev_info);
		}
		IpCaller_Obj.Caller_Run.timer = 0;
	}
}

/*------------------------------------------------------------------------
						���гɹ�,����Ringing
------------------------------------------------------------------------*/
void Callback_Caller_ToRinging_IxSys(CALLER_STRUCT *msg)	//R_
{
	if( CallServer_Run.call_rule == CallRule_TransferNoAck )	// ��Ϊ��ʱת��������ʱ����ת��ʱ��Ϊ׼
	{	
		IpCaller_Obj.Caller_Config.limit_ringing_time = ((CallServer_Run.callee_divert_state&(~0x80))-2);	//czn_20190111
	}
	else
	{
		IpCaller_Obj.Caller_Config.limit_ringing_time = 40;
	}
}

/*------------------------------------------------------------------------
						�ֻ�ժ��,����ACK
------------------------------------------------------------------------*/
void Callback_Caller_ToAck_IxSys(CALLER_STRUCT *msg)	//R_
{

	int i;
	Call_Dev_Info dev_info;
	Call_Dev_Info src_dev_info;
	int hook_ip;
	hook_ip=GetCallTarIxDevHookIp(NULL);
	for(i = 0;i < GetCallTarIxDevNum();i ++)
	{
		if(GetCallTarIxDevInfo(i,&dev_info)!= 0)
			continue;
		if(hook_ip!= dev_info.ip_addr)
		{
			SendRemoteCallLinkHoldReq(dev_info.ip_addr);	//czn_20190527
			
		}
	}

	for(i = 0;i < GetCallTarIxDevNum();i ++)
	{
		if(GetCallTarIxDevInfo(i,&dev_info)!= 0)
			continue;
		Get_SelfDevInfo(dev_info.ip_addr,&src_dev_info);
		if(hook_ip!= dev_info.ip_addr)
		{
			Send_ByeCmd_VtkUnicast(dev_info.ip_addr,src_dev_info,dev_info);
		}
	}
}

/*------------------------------------------------------------------------
						�����һ�,����Waiting
------------------------------------------------------------------------*/



void Callback_Caller_ToBye_IxSys(CALLER_STRUCT *msg)	//R_
{
	int i;
	Call_Dev_Info dev_info;
	Call_Dev_Info src_dev_info;
	IpCaller_Obj.Caller_Run.timer = 0;
	OS_StopTimer(IpCaller_Obj.timer_caller);
	IpCaller_Obj.Caller_Run.state = CALLER_WAITING;
	for(i = 0;i < GetCallTarIxDevNum();i ++)
	{
		if(GetCallTarIxDevInfo(i,&dev_info)!= 0)
			continue;
		Get_SelfDevInfo(dev_info.ip_addr,&src_dev_info);
		//if(CallServer_Run.t_addr.ip != CallServer_Run.t_addr_ext[i].ip)
		{
			Send_ByeCmd_VtkUnicast(dev_info.ip_addr,src_dev_info,dev_info);
		}
	}	
	for(i = 0;i < GetCallTarIxDevNum();i ++)
	{
		if(GetCallTarIxDevInfo(i,&dev_info)!= 0)
			continue;
		SendRemoteCallLinkFreeReq(dev_info.ip_addr);	//czn_20190527
	}
}

/*------------------------------------------------------------------------
				�ֻ��һ�(��Ԫ��·ǿ���ͷ�),����Waitting
------------------------------------------------------------------------*/
void Callback_Caller_ToWaiting_IxSys(CALLER_STRUCT *msg)	//R_
{
	int i;
	Call_Dev_Info dev_info;
	Call_Dev_Info src_dev_info;
	for(i = 0;i < GetCallTarIxDevNum();i ++)
	{
		if(GetCallTarIxDevInfo(i,&dev_info)!= 0)
			continue;
		Get_SelfDevInfo(dev_info.ip_addr,&src_dev_info);
		//if(CallServer_Run.t_addr.ip != CallServer_Run.t_addr_ext[i].ip)
		{
			Send_ByeCmd_VtkUnicast(dev_info.ip_addr,src_dev_info,dev_info);
		}
	}	
	for(i = 0;i < GetCallTarIxDevNum();i ++)
	{
		if(GetCallTarIxDevInfo(i,&dev_info)!= 0)
			continue;
		SendRemoteCallLinkFreeReq(dev_info.ip_addr);	//czn_20190527
	}
}


/*------------------------------------------------------------------------
						��������
------------------------------------------------------------------------*/
void Callback_Caller_ToUnlock_IxSys(CALLER_STRUCT *msg)	//R_
{

}

/*------------------------------------------------------------------------
					Caller���г�ʱ����(�������˳�����״̬)						
------------------------------------------------------------------------*/
void Callback_Caller_ToTimeout_IxSys(CALLER_STRUCT *msg)	//R_
{
	//_IP_Call_Link_Run_Stru partner_call_link;
	//int  quit_flag = 0;
	int i;
	char detail[60];
	
	if(msg->msg_sub_type == CALLER_TOUT_TIMEOVER)
	{
		if(IpCaller_Obj.Caller_Run.state != CALLER_WAITING)
		{
			IpCaller_Obj.Caller_Run.timer = 0;
			OS_StopTimer(IpCaller_Obj.timer_caller);
			//czn_20170411_s
			unsigned char state_save;
			state_save = IpCaller_Obj.Caller_Run.state;
			//IpCaller_Obj.Caller_Run.state = CALLER_WAITING;
			//API_talk_off();

			//Set_IPCallLink_Status(CLink_Idle);
			if(state_save == CALLER_INVITE)
			{
				API_CallServer_InviteFail();
			}
			else 
 			{
				API_CallServer_Timeout();
			}
		}
		//czn_20170411_e
	}
	
	
}

/*------------------------------------------------------------------------
				���յ���Ϣ(): ������					
------------------------------------------------------------------------*/
void Callback_Caller_ToError_IxSys(CALLER_STRUCT *msg)	
{
	#if 0
	IpCaller_Business_Rps(msg,0);
	
	switch(Caller_ErrorCode)
	{
		case CALLER_ERROR_DTBECALLED_QUIT:
		case CALLER_ERROR_UINTLINK_CLEAR:
		case CALLER_ERROR_INVITEFAIL:
			
			
			//API_Send_BusinessRsp_ByUdp(PHONE_TYPE_CALLER,Caller_Run.call_type,Caller_Run.call_sub_type);
			if(Caller_Run.state != CALLER_WAITING)
			{
				Send_VtkUnicastCmd_StateNotice(DsAndGl,&Caller_Run.s_addr,&Caller_Run.t_addr,VTKU_CALL_STATE_BYE);
				Caller_Run.state = CALLER_WAITING;
		
				//�رն�ʱ
				Caller_Run.timer = 0;
				OS_StopTimer(&timer_caller);
				Set_IPCallLink_Status(CLink_Idle);
				API_talk_off();
			}
			//will_add_czn OS_StopTimer(&timer_caller);
			break;
		
	}
	#endif
}

void Callback_Caller_ToForceClose_IxSys(CALLER_STRUCT *msg)
{
	int i;
	
	//if(Caller_Run.state != CALLER_WAITING)
	{
		//if(msg->msg_type == CallServer_Msg_DtSrDisconnect)
		{
			#if 0
				for(i = 0;i < CallServer_Run.tdev_nums;i ++)
				{
					//if(CallServer_Run.t_addr.ip != CallServer_Run.t_addr_ext[i].ip)
					{
						Send_ByeCmd_VtkUnicast2(CallServer_Run.target_dev_list[i]);
					}
				}
			#endif
		}
		
		IpCaller_Obj.Caller_Run.state = CALLER_WAITING;

		//�رն�ʱ
		IpCaller_Obj.Caller_Run.timer = 0;

	}
}
void InformIm_ToDivert(void)
{
	int i;
	Call_Dev_Info dev_info;
	Call_Dev_Info src_dev_info;
	for(i = 0;i < GetCallTarIxDevNum();i ++)
	{
		if(GetCallTarIxDevInfo(i,&dev_info)!= 0)
			continue;
		Get_SelfDevInfo(dev_info.ip_addr,&src_dev_info);
		//if(CallServer_Run.t_addr.ip != CallServer_Run.t_addr_ext[i].ip)
		{
			Send_TransferReqCmd_VtkUnicast(dev_info.ip_addr,src_dev_info,dev_info);
		}
	}	
}
#endif
