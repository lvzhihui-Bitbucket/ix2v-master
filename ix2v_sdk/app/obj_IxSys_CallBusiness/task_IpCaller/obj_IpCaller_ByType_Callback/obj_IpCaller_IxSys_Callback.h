#ifndef _obj_Caller_IxSys_Callback_H
#define _obj_Caller_IxSys_Callback_H

void Callback_Caller_ToRedial_IxSys(CALLER_STRUCT *msg);
void Callback_Caller_ToInvite_IxSys(CALLER_STRUCT *msg);
void Callback_Caller_ToRinging_IxSys(CALLER_STRUCT *msg)	;
void Callback_Caller_ToAck_IxSys(CALLER_STRUCT *msg);
void Callback_Caller_ToBye_IxSys(CALLER_STRUCT *msg);
void Callback_Caller_ToWaiting_IxSys(CALLER_STRUCT *msg);
void Callback_Caller_ToUnlock_IxSys(CALLER_STRUCT *msg);
void Callback_Caller_ToTimeout_IxSys(CALLER_STRUCT *msg);
void Callback_Caller_ToError_IxSys(CALLER_STRUCT *msg);
void Callback_Caller_ToForceClose_IxSys(CALLER_STRUCT *msg);
#endif