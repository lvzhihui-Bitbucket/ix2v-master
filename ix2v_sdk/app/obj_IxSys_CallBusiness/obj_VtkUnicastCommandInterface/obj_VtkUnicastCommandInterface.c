/**
  ******************************************************************************
  * @file    task_CommandInterface.c
  * @author  czn
  * @version V00.01.00 
  * @date    2016.04.19
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 
#if 1
#include "obj_VtkUnicastCommandInterface.h"
#include "obj_IP_Call_Link.h"
#include "vtk_udp_stack_c5_ipc_cmd.h"
#include "task_CallServer.h"
#include "obj_PublicInformation.h"
#include  "task_Event.h"
#include "obj_IoInterface.h"
//#include "task_IoServer.h"
//#include "../../task_Sundry/task_Beeper/task_Beeper.h"
//#include "../../task_Sundry/task_Unlock/task_Unlock.h"
//#define IXG_TEST
CMD_INTERFACE_RUN_STRU	CmdInterface_Run;
//czn_020160422
//czn_20160516


void Set_CmdInterface_State(uint8 newstate)
{
	CmdInterface_Run.state = newstate;
}

void MonitorToUnlock(int lock_id);

int API_VtkUnicastCmd_Analyzer(unsigned short node_id,int source_ip,int cmd,int sn,uint8 *pkt , uint16 len)	//czn_20170328
{
#if 0
	if(CallServer_Run.state != CallServer_Wait&& CallServer_Run.call_type == IxCallScene9)
	{
		API_VtkDtUnicastCmd_Analyzer(node_id,source_ip,cmd,sn, pkt,len);

		return 0;
	}
#endif
	int rev = -1;
	VtkUnicastCmd_Stru *VtkUnicastCmd = (VtkUnicastCmd_Stru *)pkt;
	uint16 vtkcmd;
	Global_Addr_Stru gaddr,taddr;
	uint8 intercomen;
	Call_Dev_Info self_info;
	int i;
	int callscene;
	char  *event_str;
	cJSON *call_event;
	cJSON *call_tar;
	cJSON *call_src;
	char ip_addr[20];
	char dt_addr[5];
	char sip_acc[25];
	switch(cmd)
	{
		#if 1		//czn_20190506
		case VTK_CMD_DIAL_1003:
			
				//if(CallServer_Run.state == CallServer_Wait)
				{
					gaddr.gatewayid 	= node_id;	
					gaddr.ip			= source_ip;

					#if defined(PID_IX611) || defined(PID_IX622) || defined(PID_IX850)|| defined(PID_IX821)
					{
						
					}
					#else
					Get_SelfDevInfo(source_ip, &self_info);
					if(strcmp(VtkUnicastCmd->target_dev.bd_rm_ms,self_info.bd_rm_ms) == 0)
					{
						if((callscene = Get_CallScene_BySourseInfo(VtkUnicastCmd->source_dev))> 0)
						{
							CallServer_Run.callee_rsp_command_id = sn;						
							//API_CallServer_BeInvite(callscene,&VtkUnicastCmd->source_dev);
							call_event=cJSON_CreateObject();
							cJSON_AddStringToObject(call_event, EVENT_KEY_EventName,EventBECall);
							cJSON_AddStringToObject(call_event, "CallType",callscene_str[callscene]);
							cJSON_AddStringToObject(call_event, "From","UDP");
							call_src=cJSON_AddObjectToObject(call_event,"Source");
							cJSON_AddStringToObject(call_src,PB_CALL_TAR_IxDevNum,VtkUnicastCmd->source_dev.bd_rm_ms);
							cJSON_AddStringToObject(call_src,PB_CALL_TAR_IxDevIP,my_inet_ntoa(VtkUnicastCmd->source_dev.ip_addr,ip_addr));
							event_str=cJSON_Print(call_event);
							if(event_str)
							{
								API_Event(event_str);
								free(event_str);
							}
							
							cJSON_Delete(call_event);
						}
					}
					else
					{
						printf("calling room id is NOT matched!!!\n");
					}
					#endif
					rev = 0;
				}
				
			break;
		#endif
		case VTK_CMD_DIAL_REP_1083:
			if(CallServer_Run.state == CallServer_Invite )//&& CallServer_Run.t_addr.ip == source_ip)
			{
			
				//for(i = 0;i < CallServer_Run.tdev_nums; i ++)
				{
					//if(CallServer_Run.target_dev_list[i].ip_addr == source_ip)
					{
						//API_CallServer_InviteOk();
						//break;
					}
				}
				call_event=cJSON_CreateObject();
				cJSON_AddStringToObject(call_event, EVENT_KEY_EventName,EventBECallRing);
				cJSON_AddStringToObject(call_event, "From","UDP");

				event_str=cJSON_Print(call_event);
				if(event_str)
				{
					API_Event(event_str);
					free(event_str);
				}
				cJSON_Delete(call_event);
				rev = 0;
			}
			
			break;

		case VTK_CMD_STATE_1005:
			if( VtkUnicastCmd->call_type == GlMonMr )	//czn_20190529
			{
				if(VtkUnicastCmd->call_code == CALL_STATE_TALK)
				{
					Monitor_IpTalkReq_Deal(source_ip,sn);
				}	
				else if(VtkUnicastCmd->call_code == CALL_STATE_BYE)
				{
					Monitor_IpTalkCloseReq_Deal(source_ip);
				}
				break;
			}
			if(VtkUnicastCmd->call_code == CALL_STATE_TALK)
			{
				if(CallServer_Run.state == CallServer_Invite || CallServer_Run.state == CallServer_Ring)
				{
					//API_CallServer_IxRemoteAck(VtkUnicastCmd->target_dev.ip_addr);
					call_event=cJSON_CreateObject();
					cJSON_AddStringToObject(call_event, EVENT_KEY_EventName,EventBECallTalk);
					cJSON_AddStringToObject(call_event,CallTarget_IxHook,my_inet_ntoa(VtkUnicastCmd->target_dev.ip_addr,ip_addr));
					cJSON_AddStringToObject(call_event, "From","UDP");

					event_str=cJSON_Print(call_event);
					if(event_str)
					{
						API_Event(event_str);
						free(event_str);
					}
					cJSON_Delete(call_event);
					rev = 0;
				}
			}
			else if(VtkUnicastCmd->call_code == CALL_STATE_BYE)
			{
				if(CallServer_Run.state != CallServer_Wait)
				{
				#if 0
					if(VtkUnicastCmd->source_dev.ip_addr == source_ip)
					{
						API_CallServer_RemoteBye();
					}
					else
					{
						API_CallServer_RemoteBye();
					}
				#endif
					call_event=cJSON_CreateObject();
					cJSON_AddStringToObject(call_event, EVENT_KEY_EventName,EventBECallBye);
					cJSON_AddStringToObject(call_event, "From","UDP");

					event_str=cJSON_Print(call_event);
					if(event_str)
					{
						API_Event(event_str);
						free(event_str);
					}
					cJSON_Delete(call_event);
					rev = 0;
				}
			}
			break;
		
		case VTK_CMD_STATE2_1025:
			if(CallServer_Run.state != CallServer_Wait)
			{
				if(VtkUnicastCmd->call_code == CALL_STATE2_SIMU_DIVERT)
				{
					//API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_SimuDivertStartting);
					//CallServer_Run.ix2divert=1;
					API_PublicInfo_Write_Bool(PB_CALL_IX2Divert, 1);
					API_Event_By_Name(EventCallState);
				}
				if(VtkUnicastCmd->call_code == CALL_STATE2_DIVERT)
				{
					//API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_DivertStartting);
					//CallServer_Run.ix2divert=1;
					API_PublicInfo_Write_Bool(PB_CALL_IX2Divert, 1);
					API_Event_By_Name(EventCallState);
				}
				if(VtkUnicastCmd->call_code == CALL_STATE2_DIVERT_RING)
				{

				}
				if(VtkUnicastCmd->call_code == CALL_STATE2_DIVERT_ACK)
				{
					//API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_PhoneAnswer);
					if(CallServer_Run.state == CallServer_Invite || CallServer_Run.state == CallServer_Ring)
					{
						//API_CallServer_IxRemoteAck(VtkUnicastCmd->target_dev.ip_addr);
						call_event=cJSON_CreateObject();
						cJSON_AddStringToObject(call_event, EVENT_KEY_EventName,EventBECallTalk);
						cJSON_AddStringToObject(call_event,CallTarget_IxHook,my_inet_ntoa(VtkUnicastCmd->target_dev.ip_addr,ip_addr));
						cJSON_AddTrueToObject(call_event, CallTarget_IxHookDivert);
						cJSON_AddStringToObject(call_event, "From","UDP");

						event_str=cJSON_PrintUnformatted(call_event);
						if(event_str)
						{
							API_Event(event_str);
							free(event_str);
						}
						cJSON_Delete(call_event);
						rev = 0;
					}
				}
			}
			break;	
		#if 0	
		case VTK_CMD_STATE_REP_1085:
			if(CallServer_Run.state == CallServer_SourceBye || CallServer_Run.state == CallServer_TargetBye)
			{
				if(VtkUnicastCmd->source_dev.ip_addr == source_ip)
					API_CallServer_ByeOk(CallServer_Run.call_type, &VtkUnicastCmd->source_dev);
				else
					API_CallServer_ByeOk(CallServer_Run.call_type, &VtkUnicastCmd->target_dev);
				rev = 0;
			}
			break;
		#endif
		case VTK_CMD_UNLOCK_E003:
			if(VtkUnicastCmd->call_code == 0)
			{
				API_Event_Unlock("RL1", "IX-UDP");
			}
			else
			{
				API_Event_Unlock("RL2", "IX-UDP");
			}
			break;
			
		case CMD_CALLBACK_REQ:		//czn_20190412
			Recv_CallbackReq(source_ip,sn,(CmdCallbackReq_Stru*)pkt);
			break;	

		case CMD_CALL_MON_LINK_MODIFY_REQ:		//czn_20190527
			//Recv_CallLinkMondifyReq(pkt,len);
			break;
		case VTK_CMD_TRANSFER_E012:
			if(API_Para_Read_Int(SIP_ENABLE)==1&&get_sip_ix_im_acc_from_tb2(VtkUnicastCmd->target_dev.bd_rm_ms, sip_acc,NULL)>=0)
			{
				
				call_event=cJSON_CreateObject();
			
				call_tar=cJSON_AddObjectToObject(call_event,CallPara_Divert);
				cJSON_AddStringToObject(call_tar,CallTarget_SipAcc,sip_acc);
				API_CallServer_StartDivert(0,call_event);
				cJSON_Delete(call_event);
			}
			
			break;
	}
			
			
	return rev;
}

// lzh_20180912_s
// ���з�����
int Send_InviteCmd_VtkUnicast_master(int target_ip, Call_Dev_Info call_src,Call_Dev_Info call_tar,sip_account_Info *divert_info)
{

	//czn_20190412_s
	#if 0
	int rev;
	rev = Send_AutoTestCall_Req(target_info);
	if(rev == 1)
		return 0;
	else if(rev == 2)
		return -1;
	#endif
	//czn_20190412_e
	VtkUnicastCmd_Stru VtkUnicastCmd_req;
	VtkUnicastCmd_Stru VtkUnicastCmd_rsp;
	
	int	response_len = sizeof(VtkUnicastCmd_Stru);

	memset( (char*)&VtkUnicastCmd_req, 0, sizeof(VtkUnicastCmd_Stru) );
	//Get_SelfDevInfo(target_info.ip_addr,&VtkUnicastCmd_req.source_dev);
	VtkUnicastCmd_req.source_dev=call_src;
	//VtkUnicastCmd_req.source_dev 	= CallServer_Run.source_dev;
	VtkUnicastCmd_req.target_dev 	= call_tar;
	#if defined(PID_IX611)||defined(SUPPORT_IX2_DIVERT) || defined(PID_IX622)
	if( api_udp_c5_ipc_send_req(target_ip, VTK_CMD_DIAL2_1023, (char*)&VtkUnicastCmd_req,sizeof(VtkUnicastCmd_Stru), (char*)&VtkUnicastCmd_rsp, &response_len) == 0 )
	#else
	#if 1
	char paraString[200]={0};
	if(API_ReadOldPara(target_ip, "1711", paraString) == 1)
	{
		if(ParseDivertAbilityPara(paraString)==1)
		{
			if( api_udp_c5_ipc_send_req(target_ip, VTK_CMD_DIAL2_1023, (char*)&VtkUnicastCmd_req,sizeof(VtkUnicastCmd_Stru), (char*)&VtkUnicastCmd_rsp, &response_len) == 0 )
			{
				if(divert_info!=NULL)
					*divert_info= VtkUnicastCmd_rsp.master_sip_divert;
				return 0;
			}
		}
	}
	#endif
	if( api_udp_c5_ipc_send_req(target_ip, VTK_CMD_DIAL_1003, (char*)&VtkUnicastCmd_req,sizeof(VtkUnicastCmd_Stru), (char*)&VtkUnicastCmd_rsp, &response_len) == 0 )
	#endif
	{		
		//CallServer_Run.callee_divert_state 		= VtkUnicastCmd_rsp.rspstate;
		if(divert_info!=NULL)
			*divert_info= VtkUnicastCmd_rsp.master_sip_divert;
		return VtkUnicastCmd_rsp.rspstate;
	}
	else
	{
		return -1;
	}

}
#if 0
int Send_Invite2Cmd_VtkUnicast_master(Call_Dev_Info target_info)
{

	//czn_20190412_s
	int rev;
	rev = Send_AutoTestCall_Req(target_info);
	if(rev == 1)
		return 0;
	else if(rev == 2)
		return -1;
	//czn_20190412_e
	VtkUnicastCmd_Stru VtkUnicastCmd_req;
	VtkUnicastCmd_Stru VtkUnicastCmd_rsp;
	
	int	response_len = sizeof(VtkUnicastCmd_Stru);

	memset( (char*)&VtkUnicastCmd_req, 0, sizeof(VtkUnicastCmd_Stru) );

	VtkUnicastCmd_req.source_dev 	= CallServer_Run.source_dev;
	VtkUnicastCmd_req.target_dev 	= target_info;
	
	if( api_udp_c5_ipc_send_req(target_info.ip_addr, VTK_CMD_DIAL2_1023, (char*)&VtkUnicastCmd_req,sizeof(VtkUnicastCmd_Stru), (char*)&VtkUnicastCmd_rsp, &response_len) == 0 )
	{		
		CallServer_Run.callee_divert_state 		= VtkUnicastCmd_rsp.rspstate;
		CallServer_Run.callee_master_sip_divert = VtkUnicastCmd_rsp.master_sip_divert;
		return 0;
	}
	else
	{
		return -1;
	}
}
#endif
int Send_InviteCmd_VtkUnicast_slave(int target_ip, Call_Dev_Info call_src,Call_Dev_Info call_tar)
{
	VtkUnicastCmd_Stru VtkUnicastCmd_req;

	memset( (char*)&VtkUnicastCmd_req, 0, sizeof(VtkUnicastCmd_Stru) );

	VtkUnicastCmd_req.source_dev 	= call_src;
	//Get_SelfDevInfo(target_info.ip_addr,&VtkUnicastCmd_req.source_dev);
	VtkUnicastCmd_req.target_dev 	= call_tar;
	
	return api_udp_c5_ipc_send_data(target_ip,VTK_CMD_DIAL_1003,(char*)&VtkUnicastCmd_req,sizeof(VtkUnicastCmd_Stru));		//czn_20190610
}
// ���з�����
#if 0
int Send_Invite2Cmd_VtkUnicast_slave(Call_Dev_Info target_info)
{
	VtkUnicastCmd_Stru VtkUnicastCmd_req;

	memset( (char*)&VtkUnicastCmd_req, 0, sizeof(VtkUnicastCmd_Stru) );

	VtkUnicastCmd_req.source_dev 	= CallServer_Run.source_dev;
	VtkUnicastCmd_req.target_dev 	= target_info;
	
	return api_udp_c5_ipc_send_data(target_info.ip_addr,VTK_CMD_DIAL2_1023,(char*)&VtkUnicastCmd_req,sizeof(VtkUnicastCmd_Stru));		//czn_20190610
}
#endif
// ���з�Ӧ��
#if 1
int Send_RingCmd_VtkUnicast(int target_ip,int rsp_id,Call_Dev_Info call_src,Call_Dev_Info call_tar,int divert_state,sip_account_Info *divert_info)
{
	VtkUnicastCmd_Stru VtkUnicastCmd_rsp;

	memset( (char*)&VtkUnicastCmd_rsp, 0, sizeof(VtkUnicastCmd_Stru) );

	VtkUnicastCmd_rsp.source_dev 		= call_src;
	VtkUnicastCmd_rsp.target_dev 		= call_tar;
	VtkUnicastCmd_rsp.rspstate			= divert_state;
	if(divert_info!=NULL)
		VtkUnicastCmd_rsp.master_sip_divert = *divert_info;

	// ����Ҫ��ͬ��Ӧ���id
	//api_udp_c5_ipc_send_data(target_info.ip_addr,VTK_CMD_DIAL_REP_1083,(char*)&VtkUnicastCmd_rsp,sizeof(VtkUnicastCmd_Stru));
	
	if(divert_info!=NULL)
		api_udp_c5_ipc_send_rsp( target_ip, VTK_CMD_DIAL_REP_1083, rsp_id, (char*)&VtkUnicastCmd_rsp,sizeof(VtkUnicastCmd_Stru) );
	else
		api_udp_c5_ipc_send_rsp( target_ip, VTK_CMD_DIAL_REP_1083, rsp_id, (char*)&VtkUnicastCmd_rsp,VtkUnicastCmd_Stru_WithoutSip_Length );
	
	
	return 0;
}

int Send_TalkCmd_VtkUnicast(int target_ip,Call_Dev_Info call_src,Call_Dev_Info call_tar)
{
	VtkUnicastCmd_Stru VtkUnicastCmd;
	
	VtkUnicastCmd.call_code			= CALL_STATE_TALK;	
	VtkUnicastCmd.source_dev 		= call_src;
	VtkUnicastCmd.target_dev 		= call_tar;

	//api_udp_c5_ipc_send_data_by_nodeid(target_addr.gatewayid,VTK_CMD_STATE_1005,(char*)&VtkUnicastCmd,sizeof(VtkUnicastCmd_Stru));
	api_udp_c5_ipc_send_data(target_ip,VTK_CMD_STATE_1005,(char*)&VtkUnicastCmd,sizeof(VtkUnicastCmd_Stru));
	
	return 0;
}
#endif
int Send_ByeCmd_VtkUnicast(int target_ip,Call_Dev_Info call_src,Call_Dev_Info call_tar)
{
	VtkUnicastCmd_Stru VtkUnicastCmd;

	VtkUnicastCmd.call_code		= CALL_STATE_BYE;

	VtkUnicastCmd.source_dev = call_src;
	//Get_SelfDevInfo(target_info.ip_addr,&VtkUnicastCmd.source_dev);
	VtkUnicastCmd.target_dev = call_tar;

	//api_udp_c5_ipc_send_data_by_nodeid(target_addr.gatewayid,VTK_CMD_STATE_1005,(char*)&VtkUnicastCmd,sizeof(VtkUnicastCmd_Stru));
	api_udp_c5_ipc_send_data(target_ip,VTK_CMD_STATE_1005,(char*)&VtkUnicastCmd,sizeof(VtkUnicastCmd_Stru));
	
	return 0;
}
int Send_TransferReqCmd_VtkUnicast(int target_ip,Call_Dev_Info call_src,Call_Dev_Info call_tar)
{
	VtkUnicastCmd_Stru VtkUnicastCmd;

	//VtkUnicastCmd.rspstate			= 1;

	VtkUnicastCmd.source_dev = call_src;
	VtkUnicastCmd.target_dev = call_tar;
	//api_udp_c5_ipc_send_rsp_by_nodeid(target_addr.gatewayid,VTK_CMD_STATE_REP_1085,CmdInterface_Run.sn_save,(char*)&VtkUnicastCmd,sizeof(VtkUnicastCmd_Stru));
	//api_udp_c5_ipc_send_data_by_nodeid(target_addr.gatewayid,VTK_CMD_STATE_REP_1085,(char*)&VtkUnicastCmd,sizeof(VtkUnicastCmd_Stru));
	//printf("111111111%s:%s\n",__func__,VtkUnicastCmd.target_dev.bd_rm_ms);
	api_udp_c5_ipc_send_data(target_ip,VTK_CMD_TRANSFER_E012,(char*)&VtkUnicastCmd,sizeof(VtkUnicastCmd_Stru));
	return 0;
}
#if 0
int Send_ByeCmd_VtkUnicast2(Call_Dev_Info target_info)
{
	VtkUnicastCmd_Stru VtkUnicastCmd;

	VtkUnicastCmd.call_code		= CALL_STATE_BYE;

	VtkUnicastCmd.source_dev = CallServer_Run.source_dev;
	VtkUnicastCmd.target_dev = CallServer_Run.target_hook_dev;

	api_udp_c5_ipc_send_data(target_info.ip_addr,VTK_CMD_STATE_1005,(char*)&VtkUnicastCmd,sizeof(VtkUnicastCmd_Stru));	//czn_20190610
	
	return 0;
}

int Send_AckCmd_VtkUnicast(Call_Dev_Info target_info)
{
	VtkUnicastCmd_Stru VtkUnicastCmd;

	VtkUnicastCmd.rspstate			= 1;

	VtkUnicastCmd.source_dev = CallServer_Run.source_dev;
	VtkUnicastCmd.target_dev = CallServer_Run.target_hook_dev;
	//api_udp_c5_ipc_send_rsp_by_nodeid(target_addr.gatewayid,VTK_CMD_STATE_REP_1085,CmdInterface_Run.sn_save,(char*)&VtkUnicastCmd,sizeof(VtkUnicastCmd_Stru));
	//api_udp_c5_ipc_send_data_by_nodeid(target_addr.gatewayid,VTK_CMD_STATE_REP_1085,(char*)&VtkUnicastCmd,sizeof(VtkUnicastCmd_Stru));
	api_udp_c5_ipc_send_data(target_info.ip_addr,VTK_CMD_STATE_REP_1085,(char*)&VtkUnicastCmd,sizeof(VtkUnicastCmd_Stru));
	return 0;
}
#endif
//czn_020160422_s
int API_Get_Partner_Calllink_NewCmd(vdp_task_t *ptask,Global_Addr_Stru *target_addr,_IP_Call_Link_Run_Stru *partner_call_link)
{
#if 0
	VtkUnicastCmd_Stru VtkUnicastCmd;
	int rlen = sizeof(_IP_Call_Link_Run_Stru);
	int target_ip;

	target_ip = target_addr->ip;

	//VtkUnicastCmd.cmd_type 		= VTK_CMD_LINK_1001>> 8;
	//VtkUnicastCmd.cmd_sub_type 	= VTK_CMD_LINK_1001& 0xff; 

	// lzh_20160503_s
	VtkUnicastCmd.call_type		= DsAndGl;		// ��ͬ�� GlMonMr ���ɣ����Ӻͺ��е�link��Ҫ����
	// lzh_20160503_e
	VtkUnicastCmd.target_idh		= target_addr->rt;
	VtkUnicastCmd.target_idl		= target_addr->code;
	char *rbuf = (char *)malloc(rlen);

	if(rbuf == NULL)
	{
		return -1;
	}
	//if(api_udp_c5_ipc_send_req1(target_ip,VTK_CMD_LINK_1001,(char*)&VtkUnicastCmd,sizeof(VtkUnicastCmd_Stru))==-1)
	// lzh_20160512_s
	//if(api_udp_c5_ipc_send_req(target_ip,VTK_CMD_LINK_1001,(char*)&VtkUnicastCmd,sizeof(VtkUnicastCmd_Stru),ptask,rbuf,&rlen) == -1)
	//czn_20170328	if(api_udp_c5_ipc_send_req(target_ip,VTK_CMD_LINK_1001,(char*)&VtkUnicastCmd,sizeof(VtkUnicastCmd_Stru),rbuf,&rlen) == -1)
	if(api_udp_c5_ipc_send_req_by_nodeid(target_addr->gatewayid,VTK_CMD_LINK_1001,(char*)&VtkUnicastCmd,sizeof(VtkUnicastCmd_Stru),rbuf,&rlen) == -1)
	// lzh_20160512_e
	{
		bprintf("api_udp_c5_ipc_send_req\n");
		free(rbuf);
		return -1;
	}
	
	memcpy(partner_call_link,rbuf,sizeof(_IP_Call_Link_Run_Stru));

	free(rbuf);
#endif
	return 0;
	
	
}

int Send_UnlinkCmd(vdp_task_t *ptask,Global_Addr_Stru *target_addr,_IP_Call_Link_Run_Stru *partner_call_link)		
{
#if 0
	VtkUnicastCmd_Stru VtkUnicastCmd;
	
	if(partner_call_link->Status == CLink_Idle)
	{
		return -1;
	}
	
	//VtkUnicastCmd.cmd_type 		= VTK_CMD_UNLINK_1002>> 8;
	//VtkUnicastCmd.cmd_sub_type 	= VTK_CMD_UNLINK_1002& 0xff;
	VtkUnicastCmd.call_type		= DsAndGl;
	// lzh_20160512_s
	//if(api_udp_c5_ipc_send_req(target_addr->ip,VTK_CMD_UNLINK_1002,(char*)&VtkUnicastCmd,sizeof(VtkUnicastCmd_Stru),ptask,NULL,NULL) == -1)
	//czn_20170328	if(api_udp_c5_ipc_send_req(target_addr->ip,VTK_CMD_UNLINK_1002,(char*)&VtkUnicastCmd,sizeof(VtkUnicastCmd_Stru),NULL,NULL) == -1)
	if(api_udp_c5_ipc_send_req_by_nodeid(target_addr->gatewayid,VTK_CMD_UNLINK_1002,(char*)&VtkUnicastCmd,sizeof(VtkUnicastCmd_Stru),NULL,NULL) == -1)
	// lzh_20160512_e
	{
		return -1;
	}
	if(partner_call_link->Status == CLink_AsBeCalled)
	{
		bprintf("Status == CLink_AsBeCalled %08x,%08x\n",partner_call_link->BeCalled_Data.Call_Source.ip,GetLocalIp());
		//if(partner_call_link->BeCalled_Data.Call_Source.ip !=GetLocalIp())
		{
			// lzh_20160512_s		
			//if(api_udp_c5_ipc_send_req(partner_call_link->BeCalled_Data.Call_Source.ip,VTK_CMD_UNLINK_1002,(char*)&VtkUnicastCmd,sizeof(VtkUnicastCmd_Stru),ptask,NULL,NULL) == -1)
			//czn_20170328	if(api_udp_c5_ipc_send_req(partner_call_link->BeCalled_Data.Call_Source.ip,VTK_CMD_UNLINK_1002,(char*)&VtkUnicastCmd,sizeof(VtkUnicastCmd_Stru),NULL,NULL) == -1)
			if(api_udp_c5_ipc_send_req_by_nodeid(partner_call_link->BeCalled_Data.Call_Source.gatewayid,VTK_CMD_UNLINK_1002,(char*)&VtkUnicastCmd,sizeof(VtkUnicastCmd_Stru),NULL,NULL) == -1)
			// lzh_20160512_e			
			{
				return -1;
			}

		}
	}

	if(partner_call_link->Status == CLink_AsCaller)
	{
		bprintf("Status == CLink_AsCaller %08x,%08x\n",partner_call_link->Caller_Data.Call_Target.ip,GetLocalIp());
		//czn_20170328	if(partner_call_link->Caller_Data.Call_Target.ip !=GetLocalIp())
		{
			// lzh_20160512_s			
			//if(api_udp_c5_ipc_send_req(partner_call_link->Caller_Data.Call_Target.ip,VTK_CMD_UNLINK_1002,(char*)&VtkUnicastCmd,sizeof(VtkUnicastCmd_Stru),ptask,NULL,NULL) == -1)
			//czn_20170328	if(api_udp_c5_ipc_send_req(partner_call_link->Caller_Data.Call_Target.ip,VTK_CMD_UNLINK_1002,(char*)&VtkUnicastCmd,sizeof(VtkUnicastCmd_Stru),NULL,NULL) == -1)
			if(api_udp_c5_ipc_send_req_by_nodeid(partner_call_link->Caller_Data.Call_Target.gatewayid,VTK_CMD_UNLINK_1002,(char*)&VtkUnicastCmd,sizeof(VtkUnicastCmd_Stru),NULL,NULL) == -1)
			// lzh_20160512_e				
			{
				return -1;
			}

		}
	}
	
	if(partner_call_link->Status == CLink_AsCallServer)
	{
		bprintf("Status == CLink_AsCallServer %08x,%08x\n",partner_call_link->Caller_Data.Call_Target.ip,GetLocalIp());
		//czn_20170328	if(partner_call_link->BeCalled_Data.Call_Source.ip !=GetLocalIp() && partner_call_link->BeCalled_Data.Call_Source.ip != target_addr->ip)
		if(partner_call_link->BeCalled_Data.Call_Source.gatewayid != target_addr->gatewayid)
		{
			// lzh_20160512_s		
			//if(api_udp_c5_ipc_send_req(partner_call_link->BeCalled_Data.Call_Source.ip,VTK_CMD_UNLINK_1002,(char*)&VtkUnicastCmd,sizeof(VtkUnicastCmd_Stru),ptask,NULL,NULL) == -1)
			//czn_20170328	if(api_udp_c5_ipc_send_req(partner_call_link->BeCalled_Data.Call_Source.ip,VTK_CMD_UNLINK_1002,(char*)&VtkUnicastCmd,sizeof(VtkUnicastCmd_Stru),NULL,NULL) == -1)
			if(api_udp_c5_ipc_send_req_by_nodeid(partner_call_link->BeCalled_Data.Call_Source.gatewayid,VTK_CMD_UNLINK_1002,(char*)&VtkUnicastCmd,sizeof(VtkUnicastCmd_Stru),NULL,NULL) == -1)
			// lzh_20160512_e			
			{
				return -1;
			}

		}
		//czn_20170328	if(partner_call_link->Caller_Data.Call_Target.ip !=GetLocalIp() && partner_call_link->Caller_Data.Call_Target.ip != target_addr->ip)
		if(partner_call_link->Caller_Data.Call_Target.gatewayid != target_addr->gatewayid)
		{
			//czn_20170328	if(api_udp_c5_ipc_send_req(partner_call_link->Caller_Data.Call_Target.ip,VTK_CMD_UNLINK_1002,(char*)&VtkUnicastCmd,sizeof(VtkUnicastCmd_Stru),NULL,NULL) == -1)		
			if(api_udp_c5_ipc_send_req_by_nodeid(partner_call_link->Caller_Data.Call_Target.gatewayid,VTK_CMD_UNLINK_1002,(char*)&VtkUnicastCmd,sizeof(VtkUnicastCmd_Stru),NULL,NULL) == -1)
			{
				return -1;
			}

		}

		
	}

	if(partner_call_link->Status == CLink_HaveLocalCall)
	{
		//czn_will_add
	}
#endif
	return 0;
}
//czn_020160422_e

//czn_20161008_s
#if 1
int Send_UnlockCmd_VtkUnicast(Call_Dev_Info target_info,unsigned char locknum)
{
	VtkUnicastCmd_Stru VtkUnicastCmd;
	VtkUnicastCmd.call_type		= DsAndGl;
	VtkUnicastCmd.call_code		= locknum;
	
	//return api_udp_c5_ipc_send_data_by_nodeid(t_addr.gatewayid,VTK_CMD_UNLOCK_E003,(char*)&VtkUnicastCmd,sizeof(VtkUnicastCmd_Stru));
	return api_udp_c5_ipc_send_data(target_info.ip_addr,VTK_CMD_UNLOCK_E003,(char*)&VtkUnicastCmd,sizeof(VtkUnicastCmd_Stru));
}

int Send_UnlockCmd_ByIp(int ip, unsigned char locknum)
{
	VtkUnicastCmd_Stru VtkUnicastCmd;
	VtkUnicastCmd.call_type		= DsAndGl;
	VtkUnicastCmd.call_code		= locknum;
	
	return api_udp_c5_ipc_send_data(ip,VTK_CMD_UNLOCK_E003,(char*)&VtkUnicastCmd,sizeof(VtkUnicastCmd_Stru));
}

//czn_20161008_e

//czn_20161008_s
int Send_RLCUnlock_VtkUnicast(int target_ip,char *msg,int msg_len)
{
	
	//return api_udp_c5_ipc_send_data_by_nodeid(t_addr.gatewayid,VTK_CMD_UNLOCK_E003,(char*)&VtkUnicastCmd,sizeof(VtkUnicastCmd_Stru));
	return api_udp_c5_ipc_send_data(target_ip,VTK_CMD_RLCUNLOCK_E004,(char*)msg,msg_len);
}
//czn_20161008_e

int Send_VtkUnicastCmd_SourceRedailReq(Call_Dev_Info target_info)		//czn_20171030
{

	VtkUnicastCmd_Stru VtkUnicastCmd;
	
	VtkUnicastCmd.call_type = 0;
	#if 0
	if(CallServer_Run.call_type == DxCallScene2)
	{
		VtkUnicastCmd.source_idh		= 0;
	}
	else
	{
		VtkUnicastCmd.source_idh		= CallServer_Run.s_addr.rt;
	}
	#endif
	VtkUnicastCmd.call_code		= 0;
	
	//return api_udp_c5_ipc_send_data_by_nodeid(t_addr->gatewayid,VTK_CMD_REDAIL_E011,(char*)&VtkUnicastCmd,sizeof(VtkUnicastCmd_Stru));
	return api_udp_c5_ipc_send_data_without_ack(target_info.ip_addr,VTK_CMD_REDAIL_E011,(char*)&VtkUnicastCmd,sizeof(VtkUnicastCmd_Stru));
}
#endif
// lzh_20180912_e
#if 0
int Send_TransferReqCmd_VtkUnicast(Call_Dev_Info target_info)
{
	VtkUnicastCmd_Stru VtkUnicastCmd;

	//VtkUnicastCmd.rspstate			= 1;

	VtkUnicastCmd.source_dev = CallServer_Run.source_dev;
	VtkUnicastCmd.target_dev = CallServer_Run.target_hook_dev;
	//api_udp_c5_ipc_send_rsp_by_nodeid(target_addr.gatewayid,VTK_CMD_STATE_REP_1085,CmdInterface_Run.sn_save,(char*)&VtkUnicastCmd,sizeof(VtkUnicastCmd_Stru));
	//api_udp_c5_ipc_send_data_by_nodeid(target_addr.gatewayid,VTK_CMD_STATE_REP_1085,(char*)&VtkUnicastCmd,sizeof(VtkUnicastCmd_Stru));
	api_udp_c5_ipc_send_data(target_info.ip_addr,VTK_CMD_TRANSFER_E012,(char*)&VtkUnicastCmd,sizeof(VtkUnicastCmd_Stru));
	return 0;
}
#endif
//czn_20190216_s
#if 1
int Send_CmdCallbackReq(int	req_dev,Call_Dev_Info target_dev)
{
	CmdCallbackReq_Stru req_cmd;
	CmdCallbackRsp_Stru rsp_cmd;
	
	int response_len = sizeof(CmdCallbackRsp_Stru);
	
	req_cmd.target_dev = target_dev;
	
	if( api_udp_c5_ipc_send_req(req_dev, CMD_CALLBACK_REQ, (char*)&req_cmd,sizeof(CmdCallbackReq_Stru), (char*)&rsp_cmd, &response_len) == 0 )
	{		
		return rsp_cmd.result;
	}	

	return -1;	
}
#endif
int ReqSelectDs_Callback(char *ds_num,char *im_num)
{
	
	
	Call_Dev_Info target_dev;
	
	cJSON *target_array=cJSON_CreateArray();
	IXS_GetByNBR(NULL, NULL, ds_num, NULL, target_array);
	if(cJSON_GetArraySize(target_array)==0)
	{
		cJSON_Delete(target_array);
		return -1;
	}
	char ip_addr[20];
	if(GetJsonDataPro(cJSON_GetArrayItem(target_array,0), IX2V_IP_ADDR, ip_addr)==0)
	{
		cJSON_Delete(target_array);
		return -1;
	}	
	cJSON_Delete(target_array);
	memset(&target_dev,0,sizeof(Call_Dev_Info));
	strcpy(target_dev.bd_rm_ms,im_num);
	target_dev.ip_addr = inet_addr(GetSysVerInfo_IP());
	
	if(Send_CmdCallbackReq(inet_addr(ip_addr),target_dev)!=0)
	{
		return -1;
	}

	return 0;
}
int Send_CmdCallbackRsp(int rsp_ip,int rsp_id,int result)//result = 0 ,ok,result = 1,busy
{
	CmdCallbackRsp_Stru rsp_cmd; 

	rsp_cmd.result = result;

	api_udp_c5_ipc_send_rsp( rsp_ip, CMD_CALLBACK_RSP, rsp_id, (char*)&rsp_cmd,sizeof(CmdCallbackRsp_Stru) );
	
	return 0;
}
void Recv_CallbackReq(int source_ip,int sn,CmdCallbackReq_Stru *rmsg)
{
	if(CallServer_Run.state != CallServer_Wait ) 
	{
		Send_CmdCallbackRsp(source_ip,sn,1);
		return;
	}
	
	Send_CmdCallbackRsp(source_ip,sn,0);
	char buff[100];
	cJSON *call_event;
	//char  *event_str;
	cJSON *tar_list;
	cJSON *call_tar;
	call_event=cJSON_CreateObject();
	cJSON_AddStringToObject(call_event, EVENT_KEY_EventName,EventBECall);
	cJSON_AddStringToObject(call_event, "CallType","IxMainCall");
	cJSON_AddStringToObject(call_event, "From","AutoCallBack");
	call_tar=cJSON_AddObjectToObject(call_event,"Target");

	cJSON_AddStringToObject(call_tar, PB_CALL_TAR_INPUT,rmsg->target_dev.bd_rm_ms);
		
	API_Event_Json(call_event);
	cJSON_Delete(call_event);
	//API_CallServer_Invite(IxCallScene1_Active, 1, &rmsg->target_dev);
}


#include "obj_Talk_Servi.h"
int DsMonTalkIp=0;
int DsMonTalkTime=0;
int Monitor_IpTalkReq_Deal(int ip,int id)	//czn_20190529
{
	//pthread_mutex_lock(&MonitorObj_Lock);
	VtkUnicastCmd_Stru rsp_cmd;
	rsp_cmd.call_type = 0x34;
	rsp_cmd.call_code = CALL_STATE_TALK;
	int cur_time=time(NULL);
	//API_talk_on_by_unicast(psource->target_ip,AUDIO_SERVER_UNICAST_PORT,AUDIO_CLIENT_UNICAST_PORT );
	if(!IfCallServerBusy()&&(DsMonTalkIp==0||get_au_service_state()==0||(cur_time-DsMonTalkTime)>180))
		rsp_cmd.rspstate = 0;
	else
		rsp_cmd.rspstate = 1;
	if(rsp_cmd.rspstate==0)
	{
		cJSON *au_para;
		char ip_addr[20];
		if(get_au_service_state())
			API_TalkService_off();
		OpenAppTalk();
		
		
		au_para=cJSON_CreateObject();
		if(au_para!=NULL)
		{
			
			cJSON_AddStringToObject(au_para,TalkType,TalkType_LocalAndUdp);
			//GetCallTarIxDevHookIp(ip_addr);
			
			my_inet_ntoa(ip,ip_addr);
			cJSON_AddStringToObject(au_para,TargetIP,ip_addr); 
			
			API_RingStop();
			//if(WaveGetState()!=0)
			//	usleep(100*1000);
			int try_cnt=0;
			while(try_cnt++<100&&WaveGetState()!=0)
			{
				usleep(50*1000);
			}
			API_TalkService_on(au_para);
			cJSON_Delete(au_para);
		}
		
		DsMonTalkIp=ip;
		DsMonTalkTime=cur_time;
	}
	//{
	api_udp_c5_ipc_send_rsp(ip,VTK_CMD_STATE_REP_1085,id,(char *)&rsp_cmd,sizeof(VtkUnicastCmd_Stru));
	//}
	
	return 0;
}
int Monitor_IpTalkCloseReq_Deal(int ip)
{
	if(DsMonTalkIp==ip)
	{
		API_TalkService_off();
		DsMonTalkIp=0;
	}
}
int JudgeIsMonitorTalking(void)
{
	return (DsMonTalkIp!=0||get_au_service_state());
}
void Monitor_IpTalkForceClose(void)
{
	if(DsMonTalkIp!=0||get_au_service_state())
	{
		API_TalkService_off();
		DsMonTalkIp=0;
	}
}
int IxDsTalkOpenReq(int ds_ip)
{
	int rev = -1;
	//pthread_mutex_lock(&MonitorObj_Lock);
	//if(Ds_Menu_Para[ins].state == MONITOR_REMOTE)
	{
		VtkUnicastCmd_Stru VtkUnicastCmd,rsp_cmd;
		int len = sizeof(VtkUnicastCmd_Stru);
	
		//VtkUnicastCmd.call_type		= 0x34;
		//VtkUnicastCmd.call_code		= 1;
		VtkUnicastCmd.call_type		= 0x34;
		VtkUnicastCmd.call_code		= CALL_STATE_TALK;
		if( api_udp_c5_ipc_send_req( ds_ip,VTK_CMD_STATE_1005,(char*)&VtkUnicastCmd,sizeof(VtkUnicastCmd_Stru), (char*)&rsp_cmd, &len) == 0 )
		{
			if(rsp_cmd.rspstate == 0)
			{
				//API_talk_on_by_unicast(Ds_Menu_Para[ins].target_ip,AUDIO_SERVER_UNICAST_PORT,AUDIO_CLIENT_UNICAST_PORT );
				rev = 0;
			}
		}
	}
	
	//pthread_mutex_unlock(&MonitorObj_Lock);
	return rev;
}
int IxDsTalkCloseReq(int ds_ip)
{
	VtkUnicastCmd_Stru VtkUnicastCmd;

	//VtkUnicastCmd.call_type		= 0x34;
	//VtkUnicastCmd.call_code		= 1;
	VtkUnicastCmd.call_type		= 0x34;
	VtkUnicastCmd.call_code		= CALL_STATE_BYE;
	//if(one_monitor.state == MONITOR_REMOTE)
		api_udp_c5_ipc_send_data(ds_ip,VTK_CMD_STATE_1005,(char*)&VtkUnicastCmd,sizeof(VtkUnicastCmd_Stru));
}
#endif

//czn_20190216_e
/*********************************************************************************************************
**  End Of File
*********************************************************************************************************/

