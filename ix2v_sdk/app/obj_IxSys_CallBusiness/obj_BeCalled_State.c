/**
  ******************************************************************************
  * @file    obj_BeCalled_State.c
  * @author  czn
  * @version V00.01.00 (basic on vsip)
  * @date    2014.11.07
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2014 V-Tec</center></h2>
  ******************************************************************************
  */ 
#include "OSTIME.h"
#include "vtk_udp_stack_class.h"
#include "obj_BeCalled_State.h"

const char *becalled_state_str[]=
{
	"Wait",
	"Ring",
	"Ack",
	"Bye",
	"Transfer",
};
const int becalled_state_str_max=sizeof(becalled_state_str)/sizeof(becalled_state_str[0]);


uint8 BeCalled_UnlockId;
uint8 BeCalled_ErrorCode;

/*------------------------------------------------------------------------
			BeCalled_Task_Process: 根据状态对消息进行处理
------------------------------------------------------------------------*/
void vtk_TaskProcessEvent_BeCalled(OBJ_BECALLED_STRU *becalled_obj,BECALLED_STRUCT  *msg_becalled)	//R_
{
	printf("-------------ipbecalled recv cmd %04x state=%d\n",msg_becalled->msg_type, becalled_obj->BeCalled_Run.state);
	
	switch (becalled_obj->BeCalled_Run.state)
	{
		//WAITING状态
		case BECALLED_WAITING:
			BeCalled_Waiting_Process(becalled_obj,msg_becalled);
			break;
		
		//RINGING状态	
		case BECALLED_RINGING:
			BeCalled_Ringing_Process(becalled_obj,msg_becalled);
			break;

		//ACK状态
		case BECALLED_ACK:
			BeCalled_Ack_Process(becalled_obj,msg_becalled);
			break;

		//BYE状态	
		case BECALLED_BYE:
			BeCalled_Bye_Process(becalled_obj,msg_becalled);
			break;

		//TRANSFER状态	
		case BECALLED_TRANSFER:
			BeCalled_Transfer_Process(becalled_obj,msg_becalled);
			break;
		
		//异常状态	
		default:
			BeCalled_StateInvalid_Process(becalled_obj,msg_becalled);
			break;
	}

}

/*------------------------------------------------------------------------
			BeCalled_Timer_Callback
------------------------------------------------------------------------*/
void BeCalled_Timer_Callback(OBJ_BECALLED_STRU *becalled_obj)	//R_
{
//will_add_czn
	BECALLED_STRUCT	send_msg_to_becalled;
	uint8			timeout_flag;
	uint8			checklink_flag;
	
	timeout_flag=0;
	checklink_flag = 0;
	
	becalled_obj->BeCalled_Run.timer++;	//定时累加
	//bprintf("state = %d,timer= %d,limit_ringing_time = %d!!!\n",becalled_obj->BeCalled_Run.state,becalled_obj->BeCalled_Run.timer,becalled_obj->BeCalled_Config.limit_ringing_time);
	switch (becalled_obj->BeCalled_Run.state)
	{
		case BECALLED_RINGING:	//等待摘机
			if (becalled_obj->BeCalled_Run.timer >= becalled_obj->BeCalled_Config.limit_ringing_time)
			{
				timeout_flag=1;
			}
			if(becalled_obj->BeCalled_Run.timer > 0 && becalled_obj->BeCalled_Run.timer % ((ACK_RESPONSE_TIMEOUT+999)/1000) == 0)
			{
				checklink_flag = 1;
			}
			break;
		
		case BECALLED_ACK:		//等待挂机
			if (becalled_obj->BeCalled_Run.timer >= becalled_obj->BeCalled_Config.limit_ack_time)
			{
				timeout_flag=1;
			}
			if(becalled_obj->BeCalled_Run.timer > 0 && becalled_obj->BeCalled_Run.timer % ((ACK_RESPONSE_TIMEOUT+999)/1000) == 0)
			{
				checklink_flag = 1;
			}
			break;
			
		 #if 0
		case BECALLED_TRANSFER:	//等待呼叫转移成功
			if (becalled_obj->BeCalled_Run.timer >= becalled_obj->BeCalled_Config.limit_transfer_time)	
			{
				timeout_flag = 1;
			}
			break;
		 #endif
		 
		case BECALLED_BYE:		//等待结束应答
			if (becalled_obj->BeCalled_Run.timer >= becalled_obj->BeCalled_Config.limit_bye_time)	
			{
				timeout_flag = 1;
			}
			break;
			
		default:
			timeout_flag = 1;
		  	break;
	}
	
	if (timeout_flag)
	{
		send_msg_to_becalled.msg_type 		= BECALLED_MSG_TIMEOUT;
		send_msg_to_becalled.msg_sub_type	= BECALLED_TOUT_TIMEOVER;
		
		//if (OS_Q_Put(&q_be, &send_msg_to_becalled, sizeof(BECALLED_STRUCT)))
		if(push_vdp_common_queue(becalled_obj->task_becalled->p_msg_buf, (char *)&send_msg_to_becalled, sizeof(BECALLED_STRUCT)) !=0 )
		{
			becalled_obj->BeCalled_Run.timer--;
			OS_RetriggerTimer(becalled_obj->timer_becalled);	//队列溢出, 再次触发定时, 以便下次发送消息
		}
		else 
		{
			OS_StopTimer(becalled_obj->timer_becalled);		//消息压入队列成功, 关闭定时	
		}			
	}
	else
	{
		//AutoPowerOffReset();
		#if 0
		if(checklink_flag == 1)
		{
			send_msg_to_becalled.msg_type 		= BECALLED_MSG_TIMEOUT;
			send_msg_to_becalled.msg_sub_type	= BECALLED_TOUT_CHECKLINK;
		
			push_vdp_common_queue(becalled_obj->task_becalled->p_msg_buf, (char *)&send_msg_to_becalled, sizeof(BECALLED_STRUCT));
		}
		#endif
		OS_RetriggerTimer(becalled_obj->timer_becalled);
	}
}

/*------------------------------------------------------------------------
					BeCalled_Waiting_Process
入口:	msg_becalled

处理:	仅对BECALLED_MSG_INVITE消息进行处理: 进入RINGING状态

返回:	无
------------------------------------------------------------------------*/
void BeCalled_Waiting_Process(OBJ_BECALLED_STRU *becalled_obj,BECALLED_STRUCT  *msg_becalled)	//R_
{
	switch (msg_becalled->msg_type)
	{
		//MSG_ERROR
		case BECALLED_MSG_ERROR:
			BeCalled_To_Error(becalled_obj,msg_becalled);
			break;

		case BECALLED_MSG_FORCECLOSE:
			BeCalled_To_ForceClose(becalled_obj,msg_becalled);
			break;
			
		//收到INVIT, 进入RINIGING
		case BECALLED_MSG_INVITE:
			BeCalled_To_Ringing(becalled_obj,msg_becalled);
			break;

		//异常消息
		default:
			BeCalled_MsgInvalid_Process(becalled_obj,msg_becalled);
			break;
	}
}

/*------------------------------------------------------------------------
						BeCalled_Ringing_Process
入口:    msg_becalled

处理:	按照各种消息分别处理BECALLED_RINGING状态

返回:   无
		
------------------------------------------------------------------------*/
void BeCalled_Ringing_Process(OBJ_BECALLED_STRU *becalled_obj,BECALLED_STRUCT  *msg_becalled)	//R_
{
	switch (msg_becalled->msg_type)
	{

		//收到INVIT, 进入RINIGING
		case BECALLED_MSG_INVITE:
			BeCalled_To_Ringing(becalled_obj,msg_becalled);
			break;
			
		//MSG_ERROR
		case BECALLED_MSG_ERROR:
			BeCalled_To_Error(becalled_obj,msg_becalled);
			break;

		case BECALLED_MSG_FORCECLOSE:
			BeCalled_To_ForceClose(becalled_obj,msg_becalled);
			break;	
	
		//MAG_ACK
		case BECALLED_MSG_ACK:
			BeCalled_To_Ack(becalled_obj,msg_becalled);
			break;
		
		//REDIAL		
		case BECALLED_MSG_REDIAL:
			//BeCalled_To_Redial(msg_becalled->call_type, msg_becalled->address_s);
			break;
		
		//锁1控制	
		case BECALLED_MSG_UNLOCK1:
			BeCalled_To_Unlock(becalled_obj,msg_becalled);
			break;
		
		//锁2控制	
		case BECALLED_MSG_UNLOCK2:
			BeCalled_To_Unlock(becalled_obj,msg_becalled);
			break;
		
		//BYE	
		case BECALLED_MSG_BYE:
			BeCalled_To_Bye(becalled_obj,msg_becalled);
			break;
		
		//CANCEL
		case BECALLED_MSG_CANCEL:
			BeCalled_To_Waiting(becalled_obj,msg_becalled);
			break;
		
		//呼叫(拨号)超时
		case BECALLED_MSG_TIMEOUT:	
			BeCalled_To_Timeout(becalled_obj,msg_becalled);
			break;
		
		//异常消息	
		default:
			BeCalled_MsgInvalid_Process(becalled_obj,msg_becalled);
			break;
	}
	
}

/*------------------------------------------------------------------------
						BeCalled_Ack_Process
入口:    msg_becalled

处理:	按照各种消息分别处理BECALLED_ACK状态

返回:   无
		
------------------------------------------------------------------------*/
void BeCalled_Ack_Process(OBJ_BECALLED_STRU *becalled_obj,BECALLED_STRUCT  *msg_becalled)	//R_
{
	switch (msg_becalled->msg_type)
	{
		//MSG_ERROR
		case BECALLED_MSG_ERROR:
			BeCalled_To_Error(becalled_obj,msg_becalled);
			break;

		case BECALLED_MSG_FORCECLOSE:
			BeCalled_To_ForceClose(becalled_obj,msg_becalled);
			break;	
			
		//MAG_ACK
		case BECALLED_MSG_ACK:
			BeCalled_To_Ack(becalled_obj,msg_becalled);
			break;
			
		//呼叫转移
		case BECALLED_MSG_TRANSFER:
			//BeCalled_To_Transfer(msg_becalled->call_type,msg_becalled->address_t);
			break;
			
		//锁1控制	
		case BECALLED_MSG_UNLOCK1:
			BeCalled_To_Unlock(becalled_obj,msg_becalled);
			break;
		
		//锁2控制	
		case BECALLED_MSG_UNLOCK2:
			BeCalled_To_Unlock(becalled_obj,msg_becalled);
			break;

		//MSG_BYE
		case BECALLED_MSG_BYE:
			BeCalled_To_Bye(becalled_obj,msg_becalled);
			break;
		
		//接收到强制取消呼叫(链路取消 / 主叫取消), 则结束呼叫
		case BECALLED_MSG_CANCEL:
			BeCalled_To_Waiting(becalled_obj,msg_becalled);
			break;

		//呼叫(通话)超时
		case BECALLED_MSG_TIMEOUT:
			BeCalled_To_Timeout(becalled_obj,msg_becalled);
			break;
		
		//异常消息		
		default:
			BeCalled_MsgInvalid_Process(becalled_obj,msg_becalled);
			break;
	}
}

/*------------------------------------------------------------------------
					BeCalled_Bye_Process
入口:    msg_becalled

处理:	按照各种消息分别处理BECALLED_BYE状态

返回:   无
		
------------------------------------------------------------------------*/
void BeCalled_Bye_Process(OBJ_BECALLED_STRU *becalled_obj,BECALLED_STRUCT  *msg_becalled)	//R_
{
	switch(msg_becalled->msg_type)
	{
		//MSG_ERROR
		case BECALLED_MSG_ERROR:
			BeCalled_To_Error(becalled_obj,msg_becalled);
			break;

		case BECALLED_MSG_FORCECLOSE:
			BeCalled_To_ForceClose(becalled_obj,msg_becalled);
			break;	
			
		//收到200/OK, 返回WAITING状态
		case BECALLED_MSG_ACK:
			BeCalled_To_Waiting(becalled_obj,msg_becalled);
			break;
			
		case BECALLED_MSG_CANCEL:
			BeCalled_To_Waiting(becalled_obj,msg_becalled);
			break;
			
		//BYE超时响应, 返回WAITING状态		
		case BECALLED_MSG_TIMEOUT:
			BeCalled_To_Timeout(becalled_obj,msg_becalled);
			break;
	
		//异常消息
		default:
			BeCalled_MsgInvalid_Process(becalled_obj,msg_becalled);
			break;
	}
}

/*------------------------------------------------------------------------
		BeCalled_Transfer_Process
入口:    msg_becalled

处理:	按照各种消息分别处理BECALLED_TRANSFER状态

返回:   无
		
------------------------------------------------------------------------*/
void BeCalled_Transfer_Process(OBJ_BECALLED_STRU *becalled_obj,BECALLED_STRUCT  *msg_becalled)
{
#if 0
	switch (msg_becalled->msg_type)
	{	
		//锁1响应
		case BECALLED_MSG_UNLOCK1:
			BeCalled_To_Unlock(msg_becalled);
			break;
		
		//锁2响应
		case BECALLED_MSG_UNLOCK2:
			BeCalled_To_Unlock(msg_becalled);
			break;

		//BYE动作响应, 进入BYE状态
		case BECALLED_MSG_BYE:
			BeCalled_To_Bye(msg_becalled);
			break;
		
		//收到CANCEL, 返回WAITING状态	
		case BECALLED_MSG_CANCEL:
			BeCalled_To_Waiting(msg_becalled);
			break;

		//TRANSFER超时响应, 进入BYE状态	
		case BECALLED_MSG_TIMEOUT:
			BeCalled_To_Timeout(msg_becalled);
			break;
			
		//呼叫转移失败
		case BECALLED_MSG_ERROR:
			BeCalled_To_Ack(msg_becalled);
			break;

		case BECALLED_MSG_FORCECLOSE:
			BeCalled_To_ForceClose(msg_becalled);
			break;	
		
		//呼叫转移成功
		case BECALLED_MSG_RINGING:
			BeCalled_To_Waiting(msg_becalled);
			break;
			
		//异常消息		
		default:
			BeCalled_MsgInvalid_Process(msg_becalled);
			break;
	}
	#endif
}

/*------------------------------------------------------------------------
			 BeCalled_StateInvalid_Process
入口:    msg_becalled

处理:	不确定状态情况处理
返回:   无
		
------------------------------------------------------------------------*/
void BeCalled_StateInvalid_Process(OBJ_BECALLED_STRU *becalled_obj,BECALLED_STRUCT  *msg_becalled)	//R
{
	//......
}

/*------------------------------------------------------------------------
			BeCalled_MsgInvalid_Process
入口:    msg_becalled

处理:	对不确定msg进行处理，或不操作

		
------------------------------------------------------------------------*/
void BeCalled_MsgInvalid_Process(OBJ_BECALLED_STRU *becalled_obj,BECALLED_STRUCT *msg_becalled)	//R
{
	
}


int Get_BeCalled_ByType_CallbackID(const STRUCT_BECALLED_CALLTYPE_CALLBACK *pcallback,const uint8 callback_length,uint8 call_type);

/*------------------------------------------------------------------------
					BeCalled_To_Ringing
入口:  
	源地址，呼叫类型

处理:
	状态转移进入RINGING状态

返回: 
	1 = 处理失败 
	0 = 处理成功
------------------------------------------------------------------------*/
void BeCalled_To_Ringing(OBJ_BECALLED_STRU *becalled_obj,BECALLED_STRUCT *msg)	//R_
{
	int callback_id; 

	printf("-------------BeCalled_To_Ringing-------------------\n");
		 
	//装载Config 
	

	//初始化运行参数
	becalled_obj->BeCalled_Run.call_type		= msg->call_type;
	becalled_obj->BeCalled_Run.s_addr 			= msg->s_addr;
	becalled_obj->BeCalled_Run.t_addr			= msg->t_addr;
	becalled_obj->BeCalled_Run.state 			= BECALLED_RINGING;

	if(becalled_obj->Config_Data_Init != NULL)//BeCalled_Data_Init();
	{
		(*becalled_obj->Config_Data_Init)();
	}
	
	//RINGING定时
	becalled_obj->BeCalled_Run.timer = 0;
	OS_RetriggerTimer(becalled_obj->timer_becalled);

	
	callback_id = Get_BeCalled_ByType_CallbackID(becalled_obj->to_ring_callback,becalled_obj->support_calltype_num,msg->call_type);

	if (callback_id >= 0)
	{
		if(becalled_obj->to_ring_callback[callback_id].callback!=NULL)
		{
			(*becalled_obj->to_ring_callback[callback_id].callback)(msg);
		}
		//(*TABLE_CALLTYPE_BECALLED_TORINGING_CALLBACK[callback_id-1].callback)(msg);
	}

	if(becalled_obj->MenuDisplay_ToRing != NULL)
	{
		(*becalled_obj->MenuDisplay_ToRing)();
	}
}

/*------------------------------------------------------------------------
					BeCalled_To_Redial
入口:  
	call类型，电话号码数组指针

处理:
	执行重拨号操作

返回: 
	1 =	处理失败 
	0 = 处理成功
------------------------------------------------------------------------*/
uint8 BeCalled_To_Redial(OBJ_BECALLED_STRU *becalled_obj,BECALLED_STRUCT *msg)
{
	return 0;
}

/*------------------------------------------------------------------------
				BeCalled_To_Transfer
入口:  
	源地址,目标地址，call类型

处理:
	状态转移进入TRANSFER状态

返回: 
	无
------------------------------------------------------------------------*/
void BeCalled_To_Transfer(OBJ_BECALLED_STRU *becalled_obj,BECALLED_STRUCT *msg)
{

}

/*------------------------------------------------------------------------
					BeCalled_To_Ack
入口:  
	源地址，目标地址，control_type(bit4-7：进入ACK类型，bit0-3：消息子类型)

处理:
	状态转移进入ACK状态

返回: 
	无
------------------------------------------------------------------------*/
void BeCalled_To_Ack(OBJ_BECALLED_STRU *becalled_obj,BECALLED_STRUCT *msg)	//R_
{
	int callback_id;
	
	
	becalled_obj->BeCalled_Run.state = BECALLED_ACK;
	
	//开定时
	becalled_obj->BeCalled_Run.timer = 0;
	OS_RetriggerTimer(becalled_obj->timer_becalled);

	callback_id = Get_BeCalled_ByType_CallbackID(becalled_obj->to_ack_callback,becalled_obj->support_calltype_num,msg->call_type);

	if (callback_id >= 0)
	{
		if(becalled_obj->to_ack_callback[callback_id].callback!=NULL)
		{
			(*becalled_obj->to_ack_callback[callback_id].callback)(msg);
		}
		//(*TABLE_CALLTYPE_BECALLED_TORINGING_CALLBACK[callback_id-1].callback)(msg);
	}

	if(becalled_obj->MenuDisplay_ToAck!= NULL)
	{
		(*becalled_obj->MenuDisplay_ToAck)();
	}
}

/*------------------------------------------------------------------------
					BeCalled_To_Bye
入口:  
	control类型

处理:
	状态转移进入BYE状态

返回: 
	无
------------------------------------------------------------------------*/
void BeCalled_To_Bye(OBJ_BECALLED_STRU *becalled_obj,BECALLED_STRUCT *msg)	//R_
{
	int callback_id;
	
	//进入BYE状态
	becalled_obj->BeCalled_Run.state = BECALLED_BYE;
	
	becalled_obj->BeCalled_Run.timer = 0;
	OS_RetriggerTimer(becalled_obj->timer_becalled);
	

	callback_id = Get_BeCalled_ByType_CallbackID(becalled_obj->to_bye_callback,becalled_obj->support_calltype_num,msg->call_type);

	if (callback_id >= 0)
	{
		if(becalled_obj->to_bye_callback[callback_id].callback!=NULL)
		{
			(*becalled_obj->to_bye_callback[callback_id].callback)(msg);
		}
	}

	if(becalled_obj->MenuDisplay_ToBye!= NULL)
	{
		(*becalled_obj->MenuDisplay_ToBye)();
	}
}

/*------------------------------------------------------------------------
					BeCalled_To_Timeout
入口:  
	control类型

处理:
	超时处理

返回: 
	无
------------------------------------------------------------------------*/
void BeCalled_To_Timeout(OBJ_BECALLED_STRU *becalled_obj,BECALLED_STRUCT *msg)	//R_
{
	int callback_id;
	

	callback_id = Get_BeCalled_ByType_CallbackID(becalled_obj->to_timeout_callback,becalled_obj->support_calltype_num,becalled_obj->BeCalled_Run.call_type);

	if (callback_id >= 0)
	{
		if(becalled_obj->to_timeout_callback[callback_id].callback!=NULL)
		{
			(*becalled_obj->to_timeout_callback[callback_id].callback)(msg);
		}
	}

	if(becalled_obj->MenuDisplay_ToWait!= NULL)
	{
		(*becalled_obj->MenuDisplay_ToWait)();
	}
}

/*------------------------------------------------------------------------
					BeCalled_To_Waiting
入口:  
	control类型

处理:
	状态转移进入WAITING状态

返回: 
	无
------------------------------------------------------------------------*/
void BeCalled_To_Waiting(OBJ_BECALLED_STRU *becalled_obj,BECALLED_STRUCT *msg)	//R_
{
	int callback_id;


	becalled_obj->BeCalled_Run.state = BECALLED_WAITING;
	
	//关闭定时器
	becalled_obj->BeCalled_Run.timer = 0;
	OS_StopTimer(becalled_obj->timer_becalled);
	

	callback_id = Get_BeCalled_ByType_CallbackID(becalled_obj->to_wait_callback,becalled_obj->support_calltype_num,msg->call_type);

	if (callback_id >= 0)
	{
		if(becalled_obj->to_wait_callback[callback_id].callback!=NULL)
		{
			(*becalled_obj->to_wait_callback[callback_id].callback)(msg);
		}
	}

	if(becalled_obj->MenuDisplay_ToWait!= NULL)
	{
		(*becalled_obj->MenuDisplay_ToWait)();
	}
}

/*-----------------------------------------------
			BeCalled_To_Unlock
入口:
       call_type,	    lock_id:锁1 / 锁2

返回:
	无
-----------------------------------------------*/
void BeCalled_To_Unlock(OBJ_BECALLED_STRU *becalled_obj,BECALLED_STRUCT *msg)	//R_
{
	int callback_id;


	//记录开锁id，以供回调使用
	//BeCalled_UnlockId = msg->ext_buf[0];


	callback_id = Get_BeCalled_ByType_CallbackID(becalled_obj->to_unclock_callback,becalled_obj->support_calltype_num,msg->call_type);

	if (callback_id >= 0)
	{
		if(becalled_obj->to_unclock_callback[callback_id].callback!=NULL)
		{
			(*becalled_obj->to_unclock_callback[callback_id].callback)(msg);
		}
	}
}
/*-----------------------------------------------
			BeCalled_To_Error
入口:
       call_type,	    error_code

返回:
	无
-----------------------------------------------*/
void BeCalled_To_Error(OBJ_BECALLED_STRU *becalled_obj,BECALLED_STRUCT *msg)	//R_
{
	int callback_id;


	//记录开锁id，以供回调使用
	//BeCalled_ErrorCode = msg->ext_buf[0];
	
	callback_id = Get_BeCalled_ByType_CallbackID(becalled_obj->to_error_callback,becalled_obj->support_calltype_num,msg->call_type);

	if (callback_id >= 0)
	{
		if(becalled_obj->to_error_callback[callback_id].callback!=NULL)
		{
			(*becalled_obj->to_error_callback[callback_id].callback)(msg);
		}
	}

	if(becalled_obj->MenuDisplay_ToWait!= NULL)
	{
		(*becalled_obj->MenuDisplay_ToWait)();
	}
}

void BeCalled_To_ForceClose(OBJ_BECALLED_STRU *becalled_obj,BECALLED_STRUCT *msg)
{
	int callback_id;

	if(becalled_obj->BeCalled_Run.state != BECALLED_WAITING)
	{
		//状态机
		becalled_obj->BeCalled_Run.state = BECALLED_WAITING;
		
		//关闭定时
		becalled_obj->BeCalled_Run.timer = 0;
		OS_StopTimer(becalled_obj->timer_becalled);
		

		callback_id = Get_BeCalled_ByType_CallbackID(becalled_obj->to_forceclose_callback,becalled_obj->support_calltype_num,becalled_obj->BeCalled_Run.call_type);

		if (callback_id >= 0)
		{
			if(becalled_obj->to_forceclose_callback[callback_id].callback!=NULL)
			{
				(*becalled_obj->to_forceclose_callback[callback_id].callback)(msg);
			}
		}
	}

	//IpBeCalled_Business_Rps(msg,0);

	if(becalled_obj->MenuDisplay_ToWait!= NULL)
	{
		(*becalled_obj->MenuDisplay_ToWait)();
	}
}

int Get_BeCalled_ByType_CallbackID(const STRUCT_BECALLED_CALLTYPE_CALLBACK *pcallback,const uint8 callback_length,uint8 call_type)
{
	uint8 i;
	
	//搜索表找匹配的call_type, 取得索引号
	for (i=0; i<callback_length; i++)
	{
		if (pcallback[i].call_type == call_type)
		{
		
			return i;
		}
	}
	
	return (-1);
}
/*********************************************************************************************************
**  End Of File
*********************************************************************************************************/
