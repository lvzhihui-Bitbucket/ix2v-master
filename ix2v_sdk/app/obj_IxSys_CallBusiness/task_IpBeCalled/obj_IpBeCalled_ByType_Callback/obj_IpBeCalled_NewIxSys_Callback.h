#ifndef _obj_BeCalled_NewIxSys_Callback_H
#define _obj_BeCalled_NewIxSys_Callback_H

void Callback_BeCalled_ToRedial_NewIxSys(BECALLED_STRUCT *msg);
void Callback_BeCalled_ToTransfer_NewIxSys(BECALLED_STRUCT *msg);
void Callback_BeCalled_ToRinging_NewIxSys(BECALLED_STRUCT *msg);
void Callback_BeCalled_ToAck_NewIxSys(BECALLED_STRUCT *msg);
void Callback_BeCalled_ToBye_NewIxSys(BECALLED_STRUCT *msg);
void Callback_BeCalled_ToWaiting_NewIxSys(BECALLED_STRUCT *msg);
void Callback_BeCalled_ToUnlock_NewIxSys(BECALLED_STRUCT *msg);
void Callback_BeCalled_ToTimeout_NewIxSys(BECALLED_STRUCT *msg);
void Callback_BeCalled_ToError_NewIxSys(BECALLED_STRUCT *msg);
void Callback_BeCalled_ToForceClose_NewIxSys(BECALLED_STRUCT *msg);
#endif