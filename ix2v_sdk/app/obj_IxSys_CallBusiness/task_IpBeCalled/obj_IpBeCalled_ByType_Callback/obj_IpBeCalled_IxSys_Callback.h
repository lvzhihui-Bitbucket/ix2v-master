#ifndef _obj_BeCalled_IxSys_Callback_H
#define _obj_BeCalled_IxSys_Callback_H

void Callback_BeCalled_ToRedial_IxSys(BECALLED_STRUCT *msg);
void Callback_BeCalled_ToTransfer_IxSys(BECALLED_STRUCT *msg);
void Callback_BeCalled_ToRinging_IxSys(BECALLED_STRUCT *msg);
void Callback_BeCalled_ToAck_IxSys(BECALLED_STRUCT *msg);
void Callback_BeCalled_ToBye_IxSys(BECALLED_STRUCT *msg);
void Callback_BeCalled_ToWaiting_IxSys(BECALLED_STRUCT *msg);
void Callback_BeCalled_ToUnlock_IxSys(BECALLED_STRUCT *msg);
void Callback_BeCalled_ToTimeout_IxSys(BECALLED_STRUCT *msg);
void Callback_BeCalled_ToError_IxSys(BECALLED_STRUCT *msg);
void Callback_BeCalled_ToForceClose_IxSys(BECALLED_STRUCT *msg);
#endif