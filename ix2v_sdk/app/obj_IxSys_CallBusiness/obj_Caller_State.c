/**
  ******************************************************************************
  * @file    obj_Caller_State.c
  * @author  czn
  * @version V00.02.00 (basic on vsip
  * @date    2014.11.06
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2014 V-Tec</center></h2>
  ******************************************************************************
  */ 

#include "obj_Caller_State.h"




uint8 Caller_UnlockId;
uint8 Caller_ErrorCode;

const char *caller_state_str[]=
{
"Wait",
"Invite",
"Ring",
"Ack",
"Bye",
};
const int caller_state_str_max=sizeof(caller_state_str)/sizeof(caller_state_str[0]);
const char *caller_msg_str[]=
{
	"UNKOWN",
	"INVITE",
	"RINGING",
	"ACK",
	"CANCEL",
	"BYE",
	"TRANSFER",
	"ERROR",
	"REDIAL",
	"UNLOCK1",
	"UNLOCK2",
	"TIMEOUT",
	"GETSTATE",
	"FORCECLOSE",
};
const int caller_msg_str_max=sizeof(caller_msg_str)/sizeof(caller_msg_str[0]);
/*------------------------------------------------------------------------
						Caller_Task_Process
------------------------------------------------------------------------*/
void vtk_TaskProcessEvent_Caller(OBJ_CALLER_STRU *caller_obj,CALLER_STRUCT	*msg_caller)	//R_
{
	switch (caller_obj->Caller_Run.state)
	{
		//WAITING״̬
		case CALLER_WAITING:
			Caller_Waiting_Process(caller_obj,msg_caller);
			break;
		
		//INVITE״̬
		case CALLER_INVITE:
			Caller_Invite_Process(caller_obj,msg_caller);
			break;
		
		//RINGING״̬
		case CALLER_RINGING:
			Caller_Ringing_Process(caller_obj,msg_caller);
			break;
		
		//ACK״̬
		case CALLER_ACK:
			Caller_Ack_Process(caller_obj,msg_caller);
			break;
		
		//BYE״̬
		case CALLER_BYE:
			Caller_Bye_Process(caller_obj,msg_caller);
			break;
		
		//�쳣״̬
		default:
			Caller_StateInvalid_Process(caller_obj,msg_caller);
			break;
	}
}

/*------------------------------------------------------------------------
			Caller_Timer_Callback
------------------------------------------------------------------------*/
void Caller_Timer_Callback(OBJ_CALLER_STRU *caller_obj)	//R_
{
//will_add_czn
	CALLER_STRUCT	send_msg_to_caller;
	uint8			timeout_flag;
	uint8			checklink_flag;
	
	timeout_flag = 0;
	checklink_flag = 0;
	
	caller_obj->Caller_Run.timer++;	//��ʱ�ۼ�
		
	switch (caller_obj->Caller_Run.state)
	{
		case CALLER_INVITE:		//�ȴ�����Ӧ��
			if (caller_obj->Caller_Run.timer >= caller_obj->Caller_Config.limit_invite_time)
			{
				timeout_flag = 1;
			}
			break;

		case CALLER_RINGING:	//�ȴ�����ժ��	
			if (caller_obj->Caller_Run.timer >= caller_obj->Caller_Config.limit_ringing_time)
			{
				timeout_flag = 1;
			}
			
			if(caller_obj->Caller_Run.timer > 0 && caller_obj->Caller_Run.timer % ((ACK_RESPONSE_TIMEOUT+999)/1000) == 0)
			{
				checklink_flag = 1;
			}
			break;
			
		case CALLER_ACK:		//�ȴ����йһ�
			if (caller_obj->Caller_Run.timer >= caller_obj->Caller_Config.limit_ack_time)
			{
				timeout_flag = 1;
			}

			if(caller_obj->Caller_Run.timer > 0 && caller_obj->Caller_Run.timer % ((ACK_RESPONSE_TIMEOUT+999)/1000) == 0)
			{
				checklink_flag = 1;
			}
			break;

		case CALLER_BYE:		//�ȴ�����Ӧ��
			if (caller_obj->Caller_Run.timer >= caller_obj->Caller_Config.limit_bye_time)
			{
				timeout_flag = 1;
			}
			break;
			
		default:				//״̬�쳣����
			timeout_flag = 1;
		  	break;
	}

	
		
	if (timeout_flag)
	{
		send_msg_to_caller.msg_type 		= CALLER_MSG_TIMEOUT;
		send_msg_to_caller.msg_sub_type	= CALLER_TOUT_TIMEOVER;
		
		//if (OS_Q_Put(&q_phone, &send_msg_to_caller, sizeof(CALLER_STRUCT)))
		if(push_vdp_common_queue(caller_obj->task_caller->p_msg_buf, (char *)&send_msg_to_caller, sizeof(CALLER_STRUCT)) !=0 )
		{
			caller_obj->Caller_Run.timer--;
			OS_RetriggerTimer(caller_obj->timer_caller);	//�������, �ٴδ�����ʱ, �Ա��´η�����Ϣ
		}
		else 
		{
			OS_StopTimer(caller_obj->timer_caller);		//��Ϣѹ����гɹ�, �رն�ʱ	
		}			
	}
	else
	{
	#if 0
		if (checklink_flag == 1)
		{
			send_msg_to_caller.msg_type 		= CALLER_MSG_TIMEOUT;
			send_msg_to_caller.msg_sub_type	= CALLER_TOUT_CHECKLINK;
			
			push_vdp_common_queue(caller_obj->task_caller->p_msg_buf, (char *)&send_msg_to_caller, sizeof(CALLER_STRUCT));
					
		}
	#endif	
		//AutoPowerOffReset();
	  	OS_RetriggerTimer(caller_obj->timer_caller);
	
	}

	
	
}

/*------------------------------------------------------------------------
					Caller_Waiting_Process
���:    msg_caller

����:	���ո�����Ϣ�ֱ���CALLER_WAITING״̬

����:   ��
------------------------------------------------------------------------*/
void Caller_Waiting_Process(OBJ_CALLER_STRU *caller_obj,CALLER_STRUCT *msg_caller)	//R_
{
 	switch (msg_caller->msg_type)
 	{
 		case CALLER_MSG_ERROR:
			Caller_To_Error(caller_obj,msg_caller);
			break;	
			
 		case CALLER_MSG_FORCECLOSE:
			Caller_To_ForceClose(caller_obj,msg_caller);
			break;	
		//INVITE��Ӧ, ����INVITE״̬
		case CALLER_MSG_INVITE:
			Caller_To_Invite(caller_obj,msg_caller);
        		break;

		case CALLER_MSG_CANCEL:
			//IpCaller_Business_Rps(caller_obj,msg_caller,0);
			//API_CallServer_IpStateNoticeAckOk();
			break;
			
		//�쳣��Ϣ	
		default:
			Caller_MsgInvalid_Process(caller_obj,msg_caller);
			break;          
	}
}

/*------------------------------------------------------------------------
					Caller_Invite_Process
���:    msg_caller

����:	���ո�����Ϣ�ֱ���CALLER_INVIT״̬

����:   ��
		
------------------------------------------------------------------------*/
void Caller_Invite_Process(OBJ_CALLER_STRU *caller_obj,CALLER_STRUCT *msg_caller)	//R_
{
	switch (msg_caller->msg_type)
	{
		//REDIAL������Ӧ	
		case CALLER_MSG_REDIAL:
			Caller_To_Redial(caller_obj,msg_caller);		//czn_20171030
			break;
			
		//�յ�ERROR��Ϣ
		case CALLER_MSG_ERROR:
			Caller_To_Error(caller_obj,msg_caller);
			break;

		case CALLER_MSG_FORCECLOSE:
			Caller_To_ForceClose(caller_obj,msg_caller);
			break;		
		
		//�յ�ringing��Ϣ: ���гɹ�
		case CALLER_MSG_RINGING:
			Caller_To_Ringing(caller_obj,msg_caller);
			break;
			
		//�յ�Ack��Ϣ
		case CALLER_MSG_ACK:
			Caller_To_Ack(caller_obj,msg_caller);
			break;
			
		//�յ�bye����Ϣ: ���н���
		case CALLER_MSG_BYE:
			Caller_To_Waiting(caller_obj,msg_caller);
			break;

		//�յ�cancel��Ϣ: ȡ������
		case CALLER_MSG_CANCEL:
			Caller_To_Bye(caller_obj,msg_caller);
			break;

		//INVITE��ʱ, ����BYE״̬	
		case CALLER_MSG_TIMEOUT:
			Caller_To_Timeout(caller_obj,msg_caller);
			break;
			
		//�쳣��Ϣ	
		default:
			Caller_MsgInvalid_Process(caller_obj,msg_caller);
			break;          
	}
}

/*------------------------------------------------------------------------
					Caller_Ringing_Process
���:    msg_caller

����:	���ո�����Ϣ�ֱ���CALLER_RINGING״̬

����:   ��
		
------------------------------------------------------------------------*/
void Caller_Ringing_Process(OBJ_CALLER_STRU *caller_obj,CALLER_STRUCT *msg_caller)	//R_
{
	
	switch (msg_caller->msg_type)
	{
		//�յ�ERROR��Ϣ
		case CALLER_MSG_ERROR:
			Caller_To_Error(caller_obj,msg_caller);
			break;

		case CALLER_MSG_FORCECLOSE:
			Caller_To_ForceClose(caller_obj,msg_caller);
			break;		
			
		//REDIAL������Ӧ	
		case CALLER_MSG_REDIAL:
			//Caller_To_Redial(Caller_Run.call_type, Caller_Run.target_addr);
			break;
			
		case CALLER_MSG_ACK:
			Caller_To_Ack(caller_obj,msg_caller);
			break;
		
		//��1��Ӧ	
		case CALLER_MSG_UNLOCK1:
			Caller_To_Unlock(caller_obj,msg_caller);
			break;

		//��2��Ӧ	
		case CALLER_MSG_UNLOCK2:
			Caller_To_Unlock(caller_obj,msg_caller);
			break;
		
		//�յ�BYE��Ϣ: ���н���
		case CALLER_MSG_BYE:
			Caller_To_Waiting(caller_obj,msg_caller);
			break;
			
		//�յ�CANCELָ��: ȡ������
		case CALLER_MSG_CANCEL:
			Caller_To_Bye(caller_obj,msg_caller);
			break;
			
		//RINGING��ʱ, ����BYE״̬
		case CALLER_MSG_TIMEOUT:
			Caller_To_Timeout(caller_obj,msg_caller);
			break;
			
		//�쳣��Ϣ	
		default:
			Caller_MsgInvalid_Process(caller_obj,msg_caller);
			break;          
	}
}

/*------------------------------------------------------------------------
						Caller_Ack_Process
���:    msg_caller

����:	���ո�����Ϣ�ֱ���CALLER_ACK״̬

����:   ��
------------------------------------------------------------------------*/
void Caller_Ack_Process(OBJ_CALLER_STRU *caller_obj,CALLER_STRUCT *msg_caller)	//R_
{
	switch (msg_caller->msg_type)
	{
		//�յ�ERROR��Ϣ
		case CALLER_MSG_ERROR:
			Caller_To_Error(caller_obj,msg_caller);
			break;

		case CALLER_MSG_FORCECLOSE:
			Caller_To_ForceClose(caller_obj,msg_caller);
			break;		
		//��1��Ӧ
		case CALLER_MSG_UNLOCK1:
			Caller_To_Unlock(caller_obj,msg_caller);
			break;
		
		//��2��Ӧ
		case CALLER_MSG_UNLOCK2:
			Caller_To_Unlock(caller_obj,msg_caller);
			break;
			
		//�յ�RINGING��Ϣ: ����RINGING
		case CALLER_MSG_RINGING:
			Caller_To_Ringing(caller_obj,msg_caller);
			break;	
			
		//�յ�BYE��Ϣ: ���н���
		case CALLER_MSG_BYE:
			Caller_To_Waiting(caller_obj,msg_caller);
			break;
			
		//�յ�CANCELָ��: ȡ������
		case CALLER_MSG_CANCEL:
			Caller_To_Bye(caller_obj,msg_caller);
			break;
			
		//ACK��ʱ, ����BYE״̬
		case CALLER_MSG_TIMEOUT:
			Caller_To_Timeout(caller_obj,msg_caller);
			break;
		
			
		//�쳣��Ϣ
		default:
			Caller_MsgInvalid_Process(caller_obj,msg_caller);
			break;
	}
}

/*------------------------------------------------------------------------
					Caller_Bye_Process
���:    msg_caller

����:	���ո�����Ϣ�ֱ���CALLER_BYE״̬

����:   ��
		
------------------------------------------------------------------------*/
void Caller_Bye_Process(OBJ_CALLER_STRU *caller_obj,CALLER_STRUCT *msg_caller)	//R_
{
	switch (msg_caller->msg_type)
	{
		//�յ�ERROR��Ϣ
		case CALLER_MSG_ERROR:
			Caller_To_Error(caller_obj,msg_caller);
			break;

		case CALLER_MSG_FORCECLOSE:
			Caller_To_ForceClose(caller_obj,msg_caller);
			break;		
			
		//�յ�ACK, ����WAITING״̬
		case CALLER_MSG_ACK:
			Caller_To_Waiting(caller_obj,msg_caller);
			break;
		//�յ�BYE,����WAITING״̬
		case CALLER_MSG_BYE:
			Caller_To_Waiting(caller_obj,msg_caller);
			break;
		//BYE��ʱ, ����WAITING״̬
		case CALLER_MSG_TIMEOUT:
			Caller_To_Timeout(caller_obj,msg_caller);
			break;
		
		//�쳣��Ϣ
		default:
			Caller_MsgInvalid_Process(caller_obj,msg_caller);
			break;          
	}
}
/*------------------------------------------------------------------------
					Caller_StateInvalid_Process
���:    msg_caller

����:	��ȷ��״̬�������
����:   ��
		
------------------------------------------------------------------------*/
void Caller_StateInvalid_Process(OBJ_CALLER_STRU *caller_obj,CALLER_STRUCT *msg_caller)	//R
{
	//......
}

/*------------------------------------------------------------------------
				Caller_MsgInvalid_Process
���:    msg_caller

����:	�Բ�ȷ��msg���д������򲻲���

		
------------------------------------------------------------------------*/
void Caller_MsgInvalid_Process(OBJ_CALLER_STRU *caller_obj,CALLER_STRUCT *msg_caller)	//R
{
  	//......
}


int Get_Caller_ByType_CallbackID(const STRUCT_CALLER_CALLTYPE_CALLBACK *pcallback,const uint8 callback_length,uint8 call_type);

/*------------------------------------------------------------------------
						Caller_To_Invite
���:  
	�绰��������ָ��, ��������

����:


����: 
	1 = ����ʧ�� 
	0 = �����ɹ�
------------------------------------------------------------------------*/
uint8 Caller_To_Invite(OBJ_CALLER_STRU *caller_obj,CALLER_STRUCT *msg)	//R_
{
	int callback_id;
	
	
	//װ��Config
	//will_add Caller_Data_Init();

	
	

	//���в�����ʼ��
	caller_obj->Caller_Run.state 			= CALLER_INVITE;					//״̬��
	caller_obj->Caller_Run.call_type 		= msg->call_type;						//��������
	caller_obj->Caller_Run.s_addr		= msg->s_addr;						//Ŀ���ַ
	caller_obj->Caller_Run.t_addr			= msg->t_addr;

	if(caller_obj->Config_Data_Init != NULL)
	{
		(*caller_obj->Config_Data_Init)();
	}
	
	//����INVITE��ʱ	
	caller_obj->Caller_Run.timer = 0;
	OS_RetriggerTimer(caller_obj->timer_caller);
	
	callback_id = Get_Caller_ByType_CallbackID(caller_obj->to_invite_callback,caller_obj->support_calltype_num,msg->call_type);

	if (callback_id >= 0)
	{
		if(caller_obj->to_invite_callback[callback_id].callback!=NULL)
		{
			(*caller_obj->to_invite_callback[callback_id].callback)(msg);
		}
		//(*TABLE_CALLTYPE_BECALLED_TORINGING_CALLBACK[callback_id-1].callback)(msg);
	}

	if(caller_obj->MenuDisplay_ToInvite!= NULL)
	{
		(*caller_obj->MenuDisplay_ToInvite)();
	}
	
	return 0;


}

/*------------------------------------------------------------------------
						Caller_To_Redial
���:  
	�绰��������ָ��, ��������

����:
	ִ���ز��Ų���

����: 
	1 =	����ʧ�� 
	0 = �����ɹ�
------------------------------------------------------------------------*/
uint8 Caller_To_Redial(OBJ_CALLER_STRU *caller_obj,CALLER_STRUCT *msg)	//R	//ʵ����ʹ��//czn_20171030
{
	int callback_id;


	//״̬��

	//RINGING��ʱ
	//caller_obj->Caller_Run.timer = 0;
	//OS_RetriggerTimer(caller_obj->timer_caller);
	if(caller_obj->to_redail_callback == NULL)
		return 1;
	
	callback_id = Get_Caller_ByType_CallbackID(caller_obj->to_redail_callback,caller_obj->support_calltype_num,msg->call_type);

	if (callback_id >= 0)
	{
		if(caller_obj->to_redail_callback[callback_id].callback!=NULL)
		{
			(*caller_obj->to_redail_callback[callback_id].callback)(msg);
		}
		//(*TABLE_CALLTYPE_BECALLED_TORINGING_CALLBACK[callback_id-1].callback)(msg);
	}

	
	return 0;
}


/*------------------------------------------------------------------------
					Caller_To_Ringing
���:  
	�绰��������ָ��, ��������

����:
	����RINGING״̬

����: 
	��
------------------------------------------------------------------------*/
void Caller_To_Ringing(OBJ_CALLER_STRU *caller_obj,CALLER_STRUCT *msg)	//R_
{
	int callback_id;


	//״̬��
	caller_obj->Caller_Run.state = CALLER_RINGING;

	//RINGING��ʱ
	caller_obj->Caller_Run.timer = 0;
	OS_RetriggerTimer(caller_obj->timer_caller);
	
	callback_id = Get_Caller_ByType_CallbackID(caller_obj->to_ring_callback,caller_obj->support_calltype_num,msg->call_type);

	if (callback_id >= 0)
	{
		if(caller_obj->to_ring_callback[callback_id].callback!=NULL)
		{
			(*caller_obj->to_ring_callback[callback_id].callback)(msg);
		}
		//(*TABLE_CALLTYPE_BECALLED_TORINGING_CALLBACK[callback_id-1].callback)(msg);
	}

	if(caller_obj->MenuDisplay_ToRing!= NULL)
	{
		(*caller_obj->MenuDisplay_ToRing)();
	}
}

/*------------------------------------------------------------------------
					Caller_To_Ack
���:  
	�绰��������ָ��, control����

����:
	״̬ת�ƽ���ACK״̬

����: 
	��
------------------------------------------------------------------------*/
void Caller_To_Ack(OBJ_CALLER_STRU *caller_obj,CALLER_STRUCT *msg)	//R_
{
	int callback_id;


	//״̬��
	caller_obj->Caller_Run.state = CALLER_ACK;
	
	//ACK��ʱ
	caller_obj->Caller_Run.timer = 0;
	OS_RetriggerTimer(caller_obj->timer_caller);

	callback_id = Get_Caller_ByType_CallbackID(caller_obj->to_ack_callback,caller_obj->support_calltype_num,msg->call_type);

	if (callback_id >= 0)
	{
		if(caller_obj->to_ack_callback[callback_id].callback!=NULL)
		{
			(*caller_obj->to_ack_callback[callback_id].callback)(msg);
		}
		//(*TABLE_CALLTYPE_BECALLED_TORINGING_CALLBACK[callback_id-1].callback)(msg);
	}

	if(caller_obj->MenuDisplay_ToAck!= NULL)
	{
		(*caller_obj->MenuDisplay_ToAck)();
	}
}

/*------------------------------------------------------------------------
					Caller_To_Bye
���:  
	control����

����:
	״̬ת�ƽ���BYE״̬

����: 
	��
------------------------------------------------------------------------*/
void Caller_To_Bye(OBJ_CALLER_STRU *caller_obj,CALLER_STRUCT *msg)	//R_
{
	int callback_id;


	//״̬��
	caller_obj->Caller_Run.state = CALLER_BYE;

	//BYE��ʱ
	caller_obj->Caller_Run.timer = 0;
	OS_RetriggerTimer(caller_obj->timer_caller);

	callback_id = Get_Caller_ByType_CallbackID(caller_obj->to_bye_callback,caller_obj->support_calltype_num,msg->call_type);

	if (callback_id >= 0)
	{
		if(caller_obj->to_bye_callback[callback_id].callback!=NULL)
		{
			(*caller_obj->to_bye_callback[callback_id].callback)(msg);
		}
		//(*TABLE_CALLTYPE_BECALLED_TORINGING_CALLBACK[callback_id-1].callback)(msg);
	}

	if(caller_obj->MenuDisplay_ToBye!= NULL)
	{
		(*caller_obj->MenuDisplay_ToBye)();
	}
}

/*------------------------------------------------------------------------
					Caller_To_Timeout
���:  
	control����

����:
	��ʱ����

����: 
	��
------------------------------------------------------------------------*/
void Caller_To_Timeout(OBJ_CALLER_STRU *caller_obj,CALLER_STRUCT *msg)	//R_
{
	int callback_id;

	callback_id = Get_Caller_ByType_CallbackID(caller_obj->to_timeout_callback,caller_obj->support_calltype_num,caller_obj->Caller_Run.call_type);

	if (callback_id >= 0)
	{
		if(caller_obj->to_timeout_callback[callback_id].callback!=NULL)
		{
			(*caller_obj->to_timeout_callback[callback_id].callback)(msg);
		}
		//(*TABLE_CALLTYPE_BECALLED_TORINGING_CALLBACK[callback_id-1].callback)(msg);
	}

	if(caller_obj->MenuDisplay_ToWait!= NULL)
	{
		(*caller_obj->MenuDisplay_ToWait)();
	}
}
/*------------------------------------------------------------------------
					Caller_To_Waiting
���:  
       control����

����:
	״̬ת�ƽ���WAITING״̬

����: 
	��
------------------------------------------------------------------------*/
void Caller_To_Waiting(OBJ_CALLER_STRU *caller_obj,CALLER_STRUCT *msg)	//R_
{
	int callback_id;


	//״̬��
	caller_obj->Caller_Run.state = CALLER_WAITING;
	
	//�رն�ʱ
	caller_obj->Caller_Run.timer = 0;
	OS_StopTimer(caller_obj->timer_caller);

	callback_id = Get_Caller_ByType_CallbackID(caller_obj->to_wait_callback,caller_obj->support_calltype_num,msg->call_type);

	if (callback_id >= 0)
	{
		if(caller_obj->to_wait_callback[callback_id].callback!=NULL)
		{
			(*caller_obj->to_wait_callback[callback_id].callback)(msg);
		}
		//(*TABLE_CALLTYPE_BECALLED_TORINGING_CALLBACK[callback_id-1].callback)(msg);
	}

	if(caller_obj->MenuDisplay_ToWait!= NULL)
	{
		(*caller_obj->MenuDisplay_ToWait)();
	}
}

/*-----------------------------------------------
			Caller_Unlock
���:
       call_type,	    lock_id:��1 / ��2

����:
	��
-----------------------------------------------*/
void Caller_To_Unlock(OBJ_CALLER_STRU *caller_obj,CALLER_STRUCT *msg)	//R_
{
	int callback_id;


	//��¼����ID

	callback_id = Get_Caller_ByType_CallbackID(caller_obj->to_unclock_callback,caller_obj->support_calltype_num,msg->call_type);

	if (callback_id >= 0)
	{
		if(caller_obj->to_unclock_callback[callback_id].callback!=NULL)
		{
			(*caller_obj->to_unclock_callback[callback_id].callback)(msg);
		}
		//(*TABLE_CALLTYPE_BECALLED_TORINGING_CALLBACK[callback_id-1].callback)(msg);
	}
	
}

/*-----------------------------------------------
			BeCalled_To_Error
���:
       call_type,	    error_code

����:
	��
-----------------------------------------------*/
void Caller_To_Error(OBJ_CALLER_STRU *caller_obj,CALLER_STRUCT *msg)	
{
	int callback_id;


	//��¼����id���Թ��ص�ʹ��
	
	callback_id = Get_Caller_ByType_CallbackID(caller_obj->to_error_callback,caller_obj->support_calltype_num,msg->call_type);

	if (callback_id >= 0)
	{
		if(caller_obj->to_error_callback[callback_id].callback!=NULL)
		{
			(*caller_obj->to_error_callback[callback_id].callback)(msg);
		}
		//(*TABLE_CALLTYPE_BECALLED_TORINGING_CALLBACK[callback_id-1].callback)(msg);
	}
}

void Caller_To_ForceClose(OBJ_CALLER_STRU *caller_obj,CALLER_STRUCT *msg)
{
	int callback_id;

	if(caller_obj->Caller_Run.state != CALLER_WAITING)
	{
		//״̬��
		caller_obj->Caller_Run.state = CALLER_WAITING;
		
		//�رն�ʱ
		caller_obj->Caller_Run.timer = 0;
		OS_StopTimer(caller_obj->timer_caller);
		
		callback_id = Get_Caller_ByType_CallbackID(caller_obj->to_forceclose_callback,caller_obj->support_calltype_num,caller_obj->Caller_Run.call_type);

		if (callback_id >= 0)
		{
			if(caller_obj->to_forceclose_callback[callback_id].callback!=NULL)
			{
				(*caller_obj->to_forceclose_callback[callback_id].callback)(msg);
			}
			//(*TABLE_CALLTYPE_BECALLED_TORINGING_CALLBACK[callback_id-1].callback)(msg);
		}
	}
	if(caller_obj->MenuDisplay_ToWait!= NULL)
	{
		(*caller_obj->MenuDisplay_ToWait)();
	}
}

int Get_Caller_ByType_CallbackID(const STRUCT_CALLER_CALLTYPE_CALLBACK *pcallback,const uint8 callback_length,uint8 call_type)
{
	uint8 i;
	
	//��������ƥ���call_type, ȡ��������
	for (i=0; i<callback_length; i++)
	{
		if (pcallback[i].call_type == call_type)
		{
		
			return i;
		}
	}
	
	return (-1);
}

/*********************************************************************************************************
**  End Of File
*********************************************************************************************************/
