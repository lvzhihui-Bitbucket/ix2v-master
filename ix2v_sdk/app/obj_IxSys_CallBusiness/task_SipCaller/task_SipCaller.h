/**
  ******************************************************************************
  * @file    task_Caller.h
  * @author  czn
  * @version V00.02.00 (basic on vsip)
  * @date    2014.11.06
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2014 V-Tec</center></h2>
  ******************************************************************************
  */ 

#ifndef _task_SipCaller_H
#define _task_SipCaller_H

//#include "task_Survey.h"
#include "RTOS.h"
#include "OSTIME.h"
#include "task_survey.h"
#include "obj_Caller_State.h"

typedef enum
{	
	SipCaller_Ix2SipCall,
}SipCaller_CallType_t;

extern OBJ_CALLER_STRU SipCaller_Obj;

void init_vdp_sipcaller_task(void);
uint8 Get_SipCaller_State(void);	

int API_SipCaller_Common(uint8 msg_type, uint8 call_type, Global_Addr_Stru *s_addr,Global_Addr_Stru *t_addr);
void SipCaller_Respones(unsigned char respones_id,unsigned char msg_type,unsigned char result);

#define API_SipCaller_Invite(call_type)		API_SipCaller_Common(CALLER_MSG_INVITE,call_type,NULL,NULL)
#define API_SipCaller_Ring(call_type)		API_SipCaller_Common(CALLER_MSG_RINGING,call_type,NULL,NULL)
#define API_SipCaller_Ack(call_type)		API_SipCaller_Common(CALLER_MSG_ACK,call_type,NULL,NULL)
#define API_SipCaller_Cancel(call_type)			API_SipCaller_Common(CALLER_MSG_CANCEL,call_type,NULL,NULL)
#define API_SipCaller_Bye(call_type)				API_SipCaller_Common(CALLER_MSG_BYE,call_type,NULL,NULL)
#define API_SipCaller_ForceClose()					API_SipCaller_Common(CALLER_MSG_FORCECLOSE,0,NULL,NULL)
#endif
		