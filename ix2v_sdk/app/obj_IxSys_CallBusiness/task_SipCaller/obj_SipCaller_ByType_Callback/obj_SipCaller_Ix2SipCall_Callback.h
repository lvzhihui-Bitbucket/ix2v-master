#ifndef _obj_SipCaller_Ix2SipCall_Callback_H
#define _obj_SipCaller_Ix2SipCall_Callback_H

void Callback_SipCaller_ToRedial_Ix2SipCall(CALLER_STRUCT *msg);
void Callback_SipCaller_ToInvite_Ix2SipCall(CALLER_STRUCT *msg);
void Callback_SipCaller_ToRinging_Ix2SipCall(CALLER_STRUCT *msg)	;
void Callback_SipCaller_ToAck_Ix2SipCall(CALLER_STRUCT *msg);
void Callback_SipCaller_ToBye_Ix2SipCall(CALLER_STRUCT *msg);
void Callback_SipCaller_ToWaiting_Ix2SipCall(CALLER_STRUCT *msg);
void Callback_SipCaller_ToUnlock_Ix2SipCall(CALLER_STRUCT *msg);
void Callback_SipCaller_ToTimeout_Ix2SipCall(CALLER_STRUCT *msg);
void Callback_SipCaller_ToError_Ix2SipCall(CALLER_STRUCT *msg);
void Callback_SipCaller_ToForceClose_Ix2SipCall(CALLER_STRUCT *msg);



#endif