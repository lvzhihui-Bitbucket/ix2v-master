/**
  ******************************************************************************
  * @file    obj_Caller_IPGVtkCall_Callback.c
  * @author  czn
  * @version V00.01.00 
  * @date    2016.01.21
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */
#include "task_CallServer.h"
#include "obj_Caller_State.h"
#include "task_SipCaller.h"

// lzh_20170605_s
#include "linphone_interface.h"
// lzh_20170605_e

#include "obj_SipCaller_Ix2SipCall_Callback.h"

#include "elog_forcall.h"
#include "obj_TableSurver.h"

static cJSON *SipCallLog_Item=NULL;
cJSON *SipCallLog_create_item(int time,char *call_type,char *tar_acc,char *num,char *result);

int GetTargetSipAcc(char *sip_acc)
{
	//char paraString[40];
	//int dt_addr;
	if(GetJsonDataPro(CallServer_Run.target_array,CallTarget_SipAcc,sip_acc))
	{
		return 1;
	}
	return 0;
}

/*------------------------------------------------------------------------
						重拨处理
------------------------------------------------------------------------*/
void Callback_SipCaller_ToRedial_Ix2SipCall(CALLER_STRUCT *msg)
{
#if 0
	char cmdbuf[40];
		//DivertCaller_Business_Rps(msg,0);
		
		API_linphonec_Close();
		usleep(500000);
		IpCaller_Obj.Caller_Run.state = CALLER_INVITE;
		IpCaller_Obj.Caller_Run.timer = 0;
		OS_RetriggerTimer(IpCaller_Obj.timer_caller);
		Get_SipConfig_DirvertAccount(cmdbuf);
		API_linphonec_Invite(cmdbuf);
		char detail[LOG_DESC_LEN+1];
		snprintf(detail,LOG_DESC_LEN+1,"T%04d^Target Req Redial",divert_log_index);	
		//IX2_2Sip API_add_log_item(LOG_Divert_Level,Log_Devert_Title,detail,NULL);
#endif
}

/*------------------------------------------------------------------------
						呼叫申请
------------------------------------------------------------------------*/
void Callback_SipCaller_ToInvite_Ix2SipCall(CALLER_STRUCT *msg)	//R_
{
	unsigned char error_type;	//czn_20170411
	
	int i;
	
	
	//IpCaller_Respones(msg->msg_source_id,msg->msg_type,0);
	//return;
	//IX2_AD API_talk_off();		//czn_201704010

	char cmdbuf[40];
	char master_acc[40]={0};
	char master_pwd[40]={0};
	char sip_ser[80]={0};
	if(strcmp(API_PublicInfo_Read_String(PB_SIP_CONNECT_NETWORK),"NONE")==0)
	{
		API_CallServer_AppInviteFail();
		return;
	}
	GetTargetSipAcc(cmdbuf);
	get_sip_master_acc(master_acc);
	get_sip_master_pwd(master_pwd);
	get_sip_master_ser(sip_ser);
	API_VtkMediaTrans_StartJpgPush(1,sip_ser,master_acc,master_pwd,cmdbuf);
	API_linphonec_update_vdout_status();
	usleep(200*1000);
	API_linphonec_Invite(cmdbuf);
	log_d("SipCaller:ToInvite acc:%s",cmdbuf);
	API_VtkMediaTrans_StartViRtp();
	if(SipCallLog_Item)
		cJSON_Delete(SipCallLog_Item);
	if(CallServer_Run.call_type!=SipCall)
	{
		Call_Dev_Info dev_info;
		int ret;
		ret=GetCallTarIxDevInfo(0,&dev_info);
		
		SipCallLog_Item=SipCallLog_create_item(time(NULL),callscene_str[CallServer_Run.call_type],cmdbuf,(ret==0?dev_info.bd_rm_ms:NULL),"Fail");
	}
	else
		SipCallLog_Item=NULL;
}

/*------------------------------------------------------------------------
						呼叫成功,进入Ringing
------------------------------------------------------------------------*/
void Callback_SipCaller_ToRinging_Ix2SipCall(CALLER_STRUCT *msg)	//R_
{
	
	
	log_d("SipCaller:ToRing");
	if(SipCallLog_Item)
	{
		cJSON_ReplaceItemInObject(SipCallLog_Item,"RESULT",cJSON_CreateString("Succ"));
	}
}

/*------------------------------------------------------------------------
						分机摘机,进入ACK
------------------------------------------------------------------------*/
void Callback_SipCaller_ToAck_Ix2SipCall(CALLER_STRUCT *msg)	//R_
{
	
	log_d("SipCaller:ToAck");
	
	sleep(2);
	API_linphonec_Answer();
	usleep(1000000);
	//IX2_AD LoadAudioCaptureVol();
	//IX2_AD LoadLinPhoneAudioPlaybackVol();//LoadAudioPlaybackVol();
	if(SipCallLog_Item)
	{
		cJSON_ReplaceItemInObject(SipCallLog_Item,"RESULT",cJSON_CreateString("Succ"));
	}
}

/*------------------------------------------------------------------------
						主机挂机,返回Waiting
------------------------------------------------------------------------*/



void Callback_SipCaller_ToBye_Ix2SipCall(CALLER_STRUCT *msg)	//R_
{
	
	//API_talk_off();

	API_linphonec_Close();
	API_VtkMediaTrans_StopJpgPush();

	SipCaller_Obj.Caller_Run.timer = 0;
	OS_StopTimer(SipCaller_Obj.timer_caller);
	
	SipCaller_Obj.Caller_Run.state = CALLER_WAITING;


	if(SipCallLog_Item)
	{
		API_TB_AddByName(TB_NAME_SipCallLog,SipCallLog_Item);
		cJSON *desc=cJSON_CreateObject();
		int cont=API_PublicInfo_Read_Int(PB_SIP_CALL_CONT);
		if(cont==0)
			API_TB_CountByName(TB_NAME_SipCallLog,NULL);
		else
			cont++;
		cJSON_AddNumberToObject(desc,PB_SIP_CALL_CONT,cont);
		cJSON_AddStringToObject(desc,PB_SIP_CALL_LAST_TIME,GetEventItemString(SipCallLog_Item,"TIME"));
		API_TB_ReplaceDescByName(TB_NAME_SipCallLog,desc);
		cJSON_Delete(desc);
		cJSON_Delete(SipCallLog_Item);
		SipCallLog_Item=NULL;
	}
	
}

/*------------------------------------------------------------------------
				分机挂机(单元链路强制释放),返回Waitting
------------------------------------------------------------------------*/
void Callback_SipCaller_ToWaiting_Ix2SipCall(CALLER_STRUCT *msg)	//R_
{
	//IpCaller_Business_Rps(msg,0);
	
	//Send_VtkUnicastCmd_StateNoticeAck(DsAndGl,&Caller_Run.t_addr,1);
	//Set_IPCallLink_Status(CLink_Idle);
	//Send_AckCmd_VtkUnicast(msg->t_addr);

	//API_talk_off();

	API_linphonec_Close();
	API_VtkMediaTrans_StopJpgPush();

	
	log_d("SipCaller:ToWait");

	if(SipCallLog_Item)
	{
		API_TB_AddByName(TB_NAME_SipCallLog,SipCallLog_Item);
		cJSON *desc=cJSON_CreateObject();
		int cont=API_PublicInfo_Read_Int(PB_SIP_CALL_CONT);
		if(cont==0)
			API_TB_CountByName(TB_NAME_SipCallLog,NULL);
		else
			cont++;
		cJSON_AddNumberToObject(desc,PB_SIP_CALL_CONT,cont);
		cJSON_AddStringToObject(desc,PB_SIP_CALL_LAST_TIME,GetEventItemString(SipCallLog_Item,"TIME"));
		API_TB_ReplaceDescByName(TB_NAME_SipCallLog,desc);
		cJSON_Delete(desc);
		cJSON_Delete(SipCallLog_Item);
		SipCallLog_Item=NULL;
	}
}


/*------------------------------------------------------------------------
						开锁处理
------------------------------------------------------------------------*/
void Callback_SipCaller_ToUnlock_Ix2SipCall(CALLER_STRUCT *msg)	//R_
{

}

/*------------------------------------------------------------------------
					Caller呼叫超时处理(仅自身退出呼叫状态)						
------------------------------------------------------------------------*/
void Callback_SipCaller_ToTimeout_Ix2SipCall(CALLER_STRUCT *msg)	//R_
{
	//_IP_Call_Link_Run_Stru partner_call_link;
	//int  quit_flag = 0;
	
	
	if(msg->msg_sub_type == CALLER_TOUT_TIMEOVER)
	{
		if(SipCaller_Obj.Caller_Run.state != CALLER_WAITING)
		{
			
			//czn_20170411_s
			unsigned char state_save;
			state_save = SipCaller_Obj.Caller_Run.state;
			
			API_linphonec_Close();
			API_VtkMediaTrans_StopJpgPush();
			if(state_save == CALLER_INVITE)
			{
				{
					SipCaller_Obj.Caller_Run.timer = 0;
					OS_StopTimer(SipCaller_Obj.timer_caller);
					
					SipCaller_Obj.Caller_Run.state = CALLER_WAITING;
					API_talk_off();

					//Set_IPCallLink_Status(CLink_Idle);
				
					API_CallServer_AppInviteFail();
					
					log_w("SipCaller:Invite timeout");
				}
			}
			else
			{
				SipCaller_Obj.Caller_Run.timer = 0;
				OS_StopTimer(SipCaller_Obj.timer_caller);
				
				SipCaller_Obj.Caller_Run.state = CALLER_WAITING;
				API_talk_off();

				//API_CallServer_IpCallerQuit();
				//API_CallServer_Timeout();
				API_CallServer_AppBye();
				log_d("SipCaller:timeout");
			}
			if(SipCallLog_Item)
			{
				API_TB_AddByName(TB_NAME_SipCallLog,SipCallLog_Item);
				
				cJSON *desc=cJSON_CreateObject();
				int cont=API_PublicInfo_Read_Int(PB_SIP_CALL_CONT);
				if(cont==0)
					API_TB_CountByName(TB_NAME_SipCallLog,NULL);
				else
					cont++;
				cJSON_AddNumberToObject(desc,PB_SIP_CALL_CONT,cont);
				cJSON_AddStringToObject(desc,PB_SIP_CALL_LAST_TIME,GetEventItemString(SipCallLog_Item,"TIME"));
				API_TB_ReplaceDescByName(TB_NAME_SipCallLog,desc);
				cJSON_Delete(desc);
				cJSON_Delete(SipCallLog_Item);
				SipCallLog_Item=NULL;
			}
		}
		//czn_20170411_e
	}
	else if(msg->msg_sub_type == CALLER_TOUT_CHECKLINK)
	{		
	#if 0
		if(divert_video_answer == 0&&IpCaller_Obj.Caller_Run.state == CALLER_ACK && divert_reanwer<3)
		{
			if((IpCaller_Obj.Caller_Run.timer>0) && (IpCaller_Obj.Caller_Run.timer%6) == 0)
			{
				//char detail[LOG_DESC_LEN+1];
				//snprintf(detail,LOG_DESC_LEN+1,"[Divert]ReAnswer");	
				//API_add_log_item(0,"S_CSER",detail,NULL);
				API_linphonec_Answer();
				#ifdef EN_SIP_LOG
				snprintf(detail,LOG_DESC_LEN+1,"T%04d^ReAnswer%d",divert_log_index,++divert_reanwer);	
				//IX2_2Sip API_add_log_item(LOG_Divert_Level,Log_Devert_Title,detail,NULL);
				#endif
			}
		}
	#endif
	}
}

/*------------------------------------------------------------------------
				接收到消息(): 错误处理					
------------------------------------------------------------------------*/
void Callback_SipCaller_ToError_Ix2SipCall(CALLER_STRUCT *msg)	
{
	#if 0
	IpCaller_Business_Rps(msg,0);
	
	switch(Caller_ErrorCode)
	{
		case CALLER_ERROR_DTBECALLED_QUIT:
		case CALLER_ERROR_UINTLINK_CLEAR:
		case CALLER_ERROR_INVITEFAIL:
			
			
			//API_Send_BusinessRsp_ByUdp(PHONE_TYPE_CALLER,Caller_Run.call_type,Caller_Run.call_sub_type);
			if(Caller_Run.state != CALLER_WAITING)
			{
				Send_VtkUnicastCmd_StateNotice(DsAndGl,&Caller_Run.s_addr,&Caller_Run.t_addr,VTKU_CALL_STATE_BYE);
				Caller_Run.state = CALLER_WAITING;
		
				//关闭定时
				Caller_Run.timer = 0;
				OS_StopTimer(&timer_caller);
				Set_IPCallLink_Status(CLink_Idle);
				API_talk_off();
			}
			//will_add_czn OS_StopTimer(&timer_caller);
			break;
		
	}
	#endif
}

void Callback_SipCaller_ToForceClose_Ix2SipCall(CALLER_STRUCT *msg)
{
	
	
	//if(Caller_Run.state != CALLER_WAITING)
	{
		
		SipCaller_Obj.Caller_Run.state = CALLER_WAITING;

		//关闭定时
		SipCaller_Obj.Caller_Run.timer = 0;

		//Set_IPCallLink_Status(CLink_Idle);
		 API_talk_off();
		
		// lzh_20170605_s
		API_linphonec_Close();
		// lzh_20170605_e
		
	}
}

cJSON *SipCallLog_create_item(int time,char *call_type,char *tar_acc,char *num,char *result)
{
	cJSON *item=NULL;
	item=cJSON_CreateObject();
	char buff[50];
	
	if(item!=NULL)
	{
		if(time==0)
			cJSON_AddStringToObject(item,"TIME","");
		else if(time>0) 
		{
			struct tm *tblock; 
			tblock=localtime(&time);
			sprintf( buff,"%02d-%02d-%02d %02d:%02d",tblock->tm_year-100,tblock->tm_mon+1,tblock->tm_mday,tblock->tm_hour,tblock->tm_min);
			cJSON_AddStringToObject(item,"TIME",buff);
		}
		if(call_type!=NULL)
			cJSON_AddStringToObject(item,"TYPE",call_type);
		if(tar_acc!=NULL)
			cJSON_AddStringToObject(item,"TAR_ACC",tar_acc);
		if(result!=NULL)
			cJSON_AddStringToObject(item,"RESULT",result);
		if(num!=NULL)
			cJSON_AddStringToObject(item,"IX_ADDR",num);
	}
	
	return item;
}

