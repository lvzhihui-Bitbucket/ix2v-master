/**
  ******************************************************************************
  * @file    task_Called.c
  * @author  czn
  * @version V00.02.00 (basic on vsip)
  * @date    2014.11.06
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2014 V-Tec</center></h2>
  ******************************************************************************
  */ 
  
#include <sys/types.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <error.h>
#include <signal.h>
#include <pthread.h>
#include <semaphore.h>
#include <sched.h>



#include "obj_Caller_State.h"
#include "task_CallServer.h"
#include "task_SipCaller.h"

#include "OSTIME.h"
#include "elog_forcall.h"
#include "obj_SipCaller_Ix2SipCall_Callback.h"
#include "obj_TableSurver.h"

OS_TIMER timer_sipcaller;

OBJ_CALLER_STRU SipCaller_Obj;

Loop_vdp_common_buffer	vdp_sipcaller_mesg_queue;
Loop_vdp_common_buffer	vdp_sipcaller_sync_queue;


vdp_task_t	task_sipcaller;
void* vdp_sipcaller_task( void* arg );

void SipCaller_Config_Data_Init(void);
void SipCaller_MenuDisplay_ToInvite(void);
void SipCaller_MenuDisplay_ToRing(void);
void SipCaller_MenuDisplay_ToAck(void);
void SipCaller_MenuDisplay_ToBye(void);
void SipCaller_MenuDisplay_ToWait(void);
void SipCaller_Timer_Callback(void);
const char *sipcaller_type_str[]=
{
	"Ix2SipCall",
	
};
const int sipcaller_type_str_max=sizeof(sipcaller_type_str)/sizeof(sipcaller_type_str[0]);

#define SipCaller_Support_CallTypeNum	1


const STRUCT_CALLER_CALLTYPE_CALLBACK TABLE_CALLTYPE_SIPCALLER_TOINVITE_CALLBACK[SipCaller_Support_CallTypeNum]={
	//calltype					//callback
	{SipCaller_Ix2SipCall,			Callback_SipCaller_ToInvite_Ix2SipCall,},
};


const STRUCT_CALLER_CALLTYPE_CALLBACK TABLE_CALLTYPE_SIPCALLER_TORINGING_CALLBACK[SipCaller_Support_CallTypeNum]={
	//calltype					//callback
	

	{SipCaller_Ix2SipCall,			Callback_SipCaller_ToRinging_Ix2SipCall,},
};

const STRUCT_CALLER_CALLTYPE_CALLBACK TABLE_CALLTYPE_SIPCALLER_TOACK_CALLBACK[SipCaller_Support_CallTypeNum]={
	//calltype					//callback

	
	{SipCaller_Ix2SipCall,			Callback_SipCaller_ToAck_Ix2SipCall,},
};


const STRUCT_CALLER_CALLTYPE_CALLBACK TABLE_CALLTYPE_SIPCALLER_TOBYE_CALLBACK[SipCaller_Support_CallTypeNum]={
	//calltype					//callback
	
	
	{SipCaller_Ix2SipCall,			Callback_SipCaller_ToBye_Ix2SipCall,},
	
};


const STRUCT_CALLER_CALLTYPE_CALLBACK TABLE_CALLTYPE_SIPCALLER_TOWAITING_CALLBACK[SipCaller_Support_CallTypeNum]={
	//calltype					//callback

	
	{SipCaller_Ix2SipCall,			Callback_SipCaller_ToWaiting_Ix2SipCall,},
};

const STRUCT_CALLER_CALLTYPE_CALLBACK TABLE_CALLTYPE_SIPCALLER_TOUNLOCK_CALLBACK[SipCaller_Support_CallTypeNum]={
	//calltype					//callback
	
	
	{SipCaller_Ix2SipCall,			Callback_SipCaller_ToUnlock_Ix2SipCall,},
};

const STRUCT_CALLER_CALLTYPE_CALLBACK TABLE_CALLTYPE_SIPCALLER_TOTIMEOUT_CALLBACK[SipCaller_Support_CallTypeNum]={
	//calltype					//callback

	
	{SipCaller_Ix2SipCall,			Callback_SipCaller_ToTimeout_Ix2SipCall,},
};


const STRUCT_CALLER_CALLTYPE_CALLBACK TABLE_CALLTYPE_SIPCALLER_TOERROR_CALLBACK[SipCaller_Support_CallTypeNum]={
	//calltype					//callback

	
	{SipCaller_Ix2SipCall,			Callback_SipCaller_ToError_Ix2SipCall,},
};


const STRUCT_CALLER_CALLTYPE_CALLBACK TABLE_CALLTYPE_SIPCALLER_TOFORCECLOSE_CALLBACK[SipCaller_Support_CallTypeNum]={
	//calltype					//callback

	
	{SipCaller_Ix2SipCall,			Callback_SipCaller_ToForceClose_Ix2SipCall,},
};


/*------------------------------------------------------------------------
			Caller_Task_Init
------------------------------------------------------------------------*/
void SipCaller_Obj_Init(void)	//R_
{
	//任务初始化
	SipCaller_Obj.Caller_Run.state = CALLER_WAITING;
	SipCaller_Obj.timer_caller = &timer_sipcaller;
	SipCaller_Obj.task_caller = &task_sipcaller;
	
	SipCaller_Obj.Config_Data_Init = SipCaller_Config_Data_Init;

	SipCaller_Obj.to_invite_callback = TABLE_CALLTYPE_SIPCALLER_TOINVITE_CALLBACK;
	SipCaller_Obj.to_ring_callback = TABLE_CALLTYPE_SIPCALLER_TORINGING_CALLBACK; 
	SipCaller_Obj.to_ack_callback= TABLE_CALLTYPE_SIPCALLER_TOACK_CALLBACK;
	SipCaller_Obj.to_bye_callback= TABLE_CALLTYPE_SIPCALLER_TOBYE_CALLBACK;
	SipCaller_Obj.to_wait_callback= TABLE_CALLTYPE_SIPCALLER_TOWAITING_CALLBACK;
	SipCaller_Obj.to_unclock_callback= TABLE_CALLTYPE_SIPCALLER_TOUNLOCK_CALLBACK;
	SipCaller_Obj.to_timeout_callback= TABLE_CALLTYPE_SIPCALLER_TOTIMEOUT_CALLBACK;
	SipCaller_Obj.to_error_callback= TABLE_CALLTYPE_SIPCALLER_TOERROR_CALLBACK;
	SipCaller_Obj.to_forceclose_callback= TABLE_CALLTYPE_SIPCALLER_TOFORCECLOSE_CALLBACK;
	SipCaller_Obj.to_redail_callback = NULL;		//czn_20171030
	
	SipCaller_Obj.support_calltype_num = SipCaller_Support_CallTypeNum;
	SipCaller_Obj.MenuDisplay_ToInvite = SipCaller_MenuDisplay_ToInvite;
	SipCaller_Obj.MenuDisplay_ToRing= SipCaller_MenuDisplay_ToRing;
	SipCaller_Obj.MenuDisplay_ToAck= SipCaller_MenuDisplay_ToAck;
	SipCaller_Obj.MenuDisplay_ToBye= SipCaller_MenuDisplay_ToBye;
	SipCaller_Obj.MenuDisplay_ToWait= SipCaller_MenuDisplay_ToWait;

	
	//创建软定时(未启动)
	OS_CreateTimer(&timer_sipcaller, SipCaller_Timer_Callback, 1000/25);
}



/*------------------------------------------------------------------------
			Get_Caller_State
返回:
	0-WAITING
	1-BUSY
------------------------------------------------------------------------*/
uint8 Get_SipCaller_State(void)	//R
{
	if (SipCaller_Obj.Caller_Run.state==CALLER_WAITING)
	{
		return (0);
	}
	return (1);
}

int IfSipCallerRing(void)
{
	return (SipCaller_Obj.Caller_Run.state==CALLER_RINGING)?1:0;
}

/*------------------------------------------------------------------------
			Get_Caller_PartnerAddr
------------------------------------------------------------------------*/
//uint16 Get_Caller_PartnerAddr(void)	//R
//{
//	return (Caller_Run.partner_IA);
//}





/*------------------------------------------------------------------------
						OutCall&CallIn Task Process
------------------------------------------------------------------------*/
void sipcaller_mesg_data_process(void *Msg,int len )	//R_
{
	//bprintf("!!!dtcaller state = %d,recv msg = %d\n",DtCaller_Obj.Caller_Run.state,((CALLER_STRUCT *)Msg)->msg_type);
	char *msg_str,*state_str,*cur_calltype_str,*from_thread,*calltype_str;
	char uk[]="UNKOWN";
	CALLER_STRUCT * caller_msg=(CALLER_STRUCT *)Msg;
	if(caller_msg->msg_type>=caller_msg_str_max)
		msg_str=uk;
	else
		msg_str=caller_msg_str[caller_msg->msg_type];
	
	if(caller_msg->call_type>=sipcaller_type_str_max)
		calltype_str=uk;
	else
		calltype_str=sipcaller_type_str[caller_msg->call_type];
	
	if(SipCaller_Obj.Caller_Run.state==CALLER_WAITING)
		cur_calltype_str="not-specified";
	else if(SipCaller_Obj.Caller_Run.call_type>=sipcaller_type_str_max)
		cur_calltype_str=uk;
	else
		cur_calltype_str=sipcaller_type_str[SipCaller_Obj.Caller_Run.call_type];
	
	if(SipCaller_Obj.Caller_Run.state>=caller_state_str_max)
		state_str=uk;
	else
		state_str=caller_state_str[SipCaller_Obj.Caller_Run.state];
	vdp_task_t *ptask = GetTaskAccordingMsgID(caller_msg->msg_source_id);
	if(ptask==NULL)
		from_thread=uk;
	else
		from_thread=ptask->task_name;
	//printf("dtcaller recv :%s,calltype:%s,from:%s,cur calltype:%s,state:%s\n",msg_str,calltype_str,from_thread,cur_calltype_str,state_str);		
	log_d("sipcaller recv :%s,calltype:%s,from:%s,cur calltype:%s,state:%s",msg_str,calltype_str,from_thread,cur_calltype_str,state_str);
	vtk_TaskProcessEvent_Caller(&SipCaller_Obj,(CALLER_STRUCT *)Msg);
	
}

//OneCallType	OneMyCallObject;

void init_vdp_sipcaller_task(void)
{
	init_vdp_common_queue(&vdp_sipcaller_mesg_queue, 500, (msg_process)sipcaller_mesg_data_process, &task_sipcaller);
	init_vdp_common_queue(&vdp_sipcaller_sync_queue, 100, NULL, 								  &task_sipcaller);
	init_vdp_common_task(&task_sipcaller, MSG_ID_SipCalller, vdp_sipcaller_task, &vdp_sipcaller_mesg_queue, &vdp_sipcaller_sync_queue);
}

void exit_sipcaller_task(void)
{
	
}

void* vdp_sipcaller_task( void* arg )
{
	vdp_task_t*	 ptask 			= (vdp_task_t*)arg;
	p_vdp_common_buffer pdb 	= 0;
	int	size;
	SipCaller_Obj_Init();
	int cont;
	cJSON *desc;
	if(API_PublicInfo_Read_Int(PB_SIP_CALL_CONT)==0)
	{
		if((cont=API_TB_CountByName(TB_NAME_SipCallLog,NULL))>0)
		{
			 desc=cJSON_CreateObject();
			 
			cJSON_AddNumberToObject(desc,PB_SIP_CALL_CONT,cont);
			cJSON *view=cJSON_CreateArray();
			API_TB_SelectBySortByName(TB_NAME_SipCallLog, view, NULL,0);
			cJSON *item=cJSON_GetArrayItem(view,-1);
			cJSON_AddStringToObject(desc,PB_SIP_CALL_LAST_TIME,GetEventItemString(item,"TIME"));
			API_TB_ReplaceDescByName(TB_NAME_SipCallLog,desc);
			cJSON_Delete(desc);
			cJSON_Delete(view);
		}
		else
		{
			if(API_PublicInfo_Read_String(PB_SIP_CALL_LAST_TIME)==NULL)
			{
				desc=cJSON_CreateObject();
			 
				cJSON_AddNumberToObject(desc,PB_SIP_CALL_CONT,0);
				
				cJSON_AddStringToObject(desc,PB_SIP_CALL_LAST_TIME,"-");
				API_TB_ReplaceDescByName(TB_NAME_SipCallLog,desc);
				cJSON_Delete(desc);
				
			}
		}
		
	}
	ptask->task_StartCompleted = 1;
	while( ptask->task_run_flag )
	{
		size = pop_vdp_common_queue(ptask->p_msg_buf, &pdb, VDP_QUEUE_POLLING_TIME);
		if( size > 0 )
		{
			(*ptask->p_msg_buf->process)(pdb,size);
			purge_vdp_common_queue( ptask->p_msg_buf );
		}
	}
	return 0;
}

void SipCaller_Timer_Callback(void)
{
	//printf("xxxxxxxxxxxx %08x:%08x:%08x\n",(int)&timer_dtcaller,(int)DtCaller_Obj.timer_caller,(int)&DtCaller_Obj.timer_caller);
	Caller_Timer_Callback(&SipCaller_Obj);
}

void SipCaller_Config_Data_Init(void)
{
	switch(SipCaller_Obj.Caller_Run.call_type)
	{
		case SipCaller_Ix2SipCall:
			SipCaller_Obj.Caller_Config.limit_invite_time = 30;
			SipCaller_Obj.Caller_Config.limit_ringing_time = 30;
			SipCaller_Obj.Caller_Config.limit_ack_time = 120;
			SipCaller_Obj.Caller_Config.limit_bye_time = 5;
			break;	
	}
}
void SipCaller_MenuDisplay_ToInvite(void)
{

}

void SipCaller_MenuDisplay_ToRing(void)
{
}

void SipCaller_MenuDisplay_ToAck(void)
{
}
void SipCaller_MenuDisplay_ToBye(void)
{
}
void SipCaller_MenuDisplay_ToWait(void)
{
}

int API_SipCaller_Common(uint8 msg_type, uint8 call_type, Global_Addr_Stru *s_addr,Global_Addr_Stru *t_addr)	//R
{
	CALLER_STRUCT	send_msg;	
	char rev[5];
	int   rev_len;
	vdp_task_t* ptask = NULL;
	//OS_TASK_EVENT MyEvents;	

	//组织发送消息给BeCalled
	send_msg.msg_source_id 	= GetMsgIDAccordingPid(pthread_self());
	send_msg.msg_target_id	= MSG_ID_CallServer;
	send_msg.msg_type		= msg_type;
	send_msg.msg_sub_type	= 0;
	send_msg.call_type		= call_type;
	if(s_addr != NULL)
	{
		send_msg.s_addr = *s_addr;
	}
	if(t_addr != NULL)
	{
		send_msg.t_addr = *t_addr;
	}
	
	if(push_vdp_common_queue(task_sipcaller.p_msg_buf, (char *)&send_msg, sizeof(CALLER_STRUCT)) != 0)
	{
		return -1;
	}
	

	return 0;
}

void SipCaller_Respones(unsigned char respones_id,unsigned char msg_type,unsigned char result)
{
	unsigned char rev[5];
	vdp_task_t* ptask = NULL;
	
	ptask = GetTaskAccordingMsgID(respones_id);

	if(ptask == NULL)
		return;
	
	rev[0] = respones_id;
	rev[1] = MSG_ID_CallServer;
	rev[2] = msg_type |0x80;
	rev[3] = 0;
	rev[4] = result;
	
	push_vdp_common_queue(ptask->p_syc_buf, rev, 5);
}
cJSON *GetSipCallerState(void)
{
	static cJSON *state=NULL;
	char *cur_calltype_str,*cur_state_str;
	char uk[]="UNKOWN";

	if(state!=NULL)
		cJSON_Delete(state);
	
	state=cJSON_CreateObject();
	if(state==NULL)
		return NULL;
	if(SipCaller_Obj.Caller_Run.state==CALLER_WAITING)
		cur_calltype_str="not-specified";
	else if(SipCaller_Obj.Caller_Run.call_type>=sipcaller_type_str_max)
		cur_calltype_str=uk;
	else
		cur_calltype_str=sipcaller_type_str[SipCaller_Obj.Caller_Run.call_type];
	
	if(SipCaller_Obj.Caller_Run.state>=caller_state_str_max)
		cur_state_str=uk;
	else
		cur_state_str=caller_state_str[SipCaller_Obj.Caller_Run.state];
	cJSON_AddStringToObject(state,"SipCallerState",cur_state_str);
	cJSON_AddStringToObject(state,"SipCallerType",cur_calltype_str);
	//cJSON_AddNumberToObject(state,"TIMER",SipCaller_Obj.Caller_Run.timer);
	return state;
}
/*********************************************************************************************************
**  End Of File
*********************************************************************************************************/

