#ifndef _Obj_DtCaller_MainCallIM_Callback_H
#define _Obj_DtCaller_MainCallIM_Callback_H

void Callback_DtCaller_ToRedial_MainCallIM(CALLER_STRUCT *msg);
void Callback_DtCaller_ToInvite_MainCallIM(CALLER_STRUCT *msg);
void Callback_DtCaller_ToRinging_MainCallIM(CALLER_STRUCT *msg);
void Callback_DtCaller_ToAck_MainCallIM(CALLER_STRUCT *msg);
void Callback_DtCaller_ToBye_MainCallIM(CALLER_STRUCT *msg);
void Callback_DtCaller_ToWaiting_MainCallIM(CALLER_STRUCT *msg);
void Callback_DtCaller_ToUnlock_MainCallIM(CALLER_STRUCT *msg);
void Callback_DtCaller_ToTimeout_MainCallIM(CALLER_STRUCT *msg);
void Callback_DtCaller_ToError_MainCallIM(CALLER_STRUCT *msg);
void Callback_DtCaller_ToForceClose_MainCallIM(CALLER_STRUCT *msg);

#endif