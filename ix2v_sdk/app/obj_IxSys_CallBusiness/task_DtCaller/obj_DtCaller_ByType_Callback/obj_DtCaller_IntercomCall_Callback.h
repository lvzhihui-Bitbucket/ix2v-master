#ifndef _obj_DtCaller_IntercomCall_Callback_H
#define _obj_DtCaller_IntercomCall_Callback_H

void Callback_DtCaller_ToRedial_IntercomCall(CALLER_STRUCT *msg);
void Callback_DtCaller_ToInvite_IntercomCall(CALLER_STRUCT *msg);
void Callback_DtCaller_ToRinging_IntercomCall(CALLER_STRUCT *msg)	;
void Callback_DtCaller_ToAck_IntercomCall(CALLER_STRUCT *msg);
void Callback_DtCaller_ToBye_IntercomCall(CALLER_STRUCT *msg);
void Callback_DtCaller_ToWaiting_IntercomCall(CALLER_STRUCT *msg);
void Callback_DtCaller_ToUnlock_IntercomCall(CALLER_STRUCT *msg);
void Callback_DtCaller_ToTimeout_IntercomCall(CALLER_STRUCT *msg);
void Callback_DtCaller_ToError_IntercomCall(CALLER_STRUCT *msg);
void Callback_DtCaller_ToForceClose_IntercomCall(CALLER_STRUCT *msg);
#endif