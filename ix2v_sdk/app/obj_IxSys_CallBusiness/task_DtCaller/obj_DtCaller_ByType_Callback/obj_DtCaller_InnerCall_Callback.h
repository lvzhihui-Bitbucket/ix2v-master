#ifndef _obj_DtCaller_InnerCall_Callback_H
#define _obj_DtCaller_InnerCall_Callback_H

void Callback_DtCaller_ToRedial_InnerCall(CALLER_STRUCT *msg);
void Callback_DtCaller_ToInvite_InnerCall(CALLER_STRUCT *msg);
void Callback_DtCaller_ToRinging_InnerCall(CALLER_STRUCT *msg)	;
void Callback_DtCaller_ToAck_InnerCall(CALLER_STRUCT *msg);
void Callback_DtCaller_ToBye_InnerCall(CALLER_STRUCT *msg);
void Callback_DtCaller_ToWaiting_InnerCall(CALLER_STRUCT *msg);
void Callback_DtCaller_ToUnlock_InnerCall(CALLER_STRUCT *msg);
void Callback_DtCaller_ToTimeout_InnerCall(CALLER_STRUCT *msg);
void Callback_DtCaller_ToError_InnerCall(CALLER_STRUCT *msg);
void Callback_DtCaller_ToForceClose_InnerCall(CALLER_STRUCT *msg);
#endif