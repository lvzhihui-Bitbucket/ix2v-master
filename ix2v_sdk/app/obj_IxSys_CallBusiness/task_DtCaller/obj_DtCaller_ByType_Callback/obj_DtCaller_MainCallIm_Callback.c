/**
  ******************************************************************************
  * @file    obj_Caller_VtkCdsCallSt_Callback.c
  * @author  czn
  * @version V00.01.00 
  * @date    2016.01.21
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 
#include "task_CallServer.h"
#include "task_DtCaller.h"

//#include "obj_BeCalled_IPGCallIPG_Callback.h"
#include "obj_DtCaller_MainCallIm_Callback.h"
#include "define_Command.h"
//#include "obj_Survey_Setup.h"
#include "obj_UnitSR.h"
#include "task_StackAI.h"
#include "elog_forcall.h"
#include "cJSON.h"
#include "obj_IoInterface.h"

extern unsigned char myAddr;
//uint8 sr_create_flag = 0; 	//链路创建标志
//r_20150801
static int CallTarget_Type=0;

int ifDFCall(void)
{
	if(API_Para_Read_Int(DFCall_Enable))
		return CallTarget_Type;
	else
		return 0;
}

int DtMainCallLink(int dt_addr)
{
	if(InstructionCheck(dt_addr, MR_LINKING_ST)== 1)
	//if(AI_AppointFastLinkWithEventAck(dt_addr)==1)
	{
		CallTarget_Type=0;
		return 1;
	}
	return 0;
}
int DtRouterCallLink(int logic_addr)
{
	int ret=0;
	cJSON *view,*where;
	view=cJSON_CreateArray();
	where=cJSON_CreateObject();
	
	if(view&&where)
	{
		cJSON_AddNumberToObject(where,"DT_IM",logic_addr);
		if(API_TB_SelectBySortByName("DT_IM",view,where,0)>0)
		{
			ret=1;
			printf("DtRouterCallLink ok\n");
		}
	}
	cJSON_Delete(view);
	cJSON_Delete(where);
	return ret;
}
int DtMainCallSRReq(void)
{
	uint8 SR_apply;
	SR_apply = API_SR_Request(CT08_DS_CALL_ST); 
						//链路忙,不可剥夺
	if (SR_apply == SR_DISABLE)
	{
		return 0;
	}
	//链路忙,可剥夺
	else if (SR_apply == SR_ENABLE_DEPRIVED)	
	{
		API_SR_Deprived();	//复位系统链路状态字
		API_SR_Request(CT08_DS_CALL_ST);
	}
	//链路可直接使用
	else	 
	{	
		//API_SR_Deprived();	//复位系统链路状态字	
	}
	return 1;
}

int GetTargetDtAddr(void)
{
	char paraString[20];
	int dt_addr,dt_logic_addr;
	if(GetJsonDataPro(CallServer_Run.target_array,CallTarget_DtAddr,paraString))
	{
		dt_addr = strtoul(paraString,NULL,0);
		return dt_addr;
	}
	if(GetJsonDataPro(CallServer_Run.target_array,CallTarget_DtLogicAddr,paraString))
	{
		dt_logic_addr=atoi(paraString);
		if(dt_logic_addr<=32)
		{
			dt_addr=(0x80|(dt_logic_addr<<2));
			return dt_addr;
		}
	}
	return 0;
}

int GetTargetRouterAddr(int *router_id,int *room_id)
{
	char paraString[20];
	int logic_addr;
	
	if(GetJsonDataPro(CallServer_Run.target_array,CallTarget_DtLogicAddr,paraString))
	{
		logic_addr=atoi(paraString);
		if(router_id!=NULL)
		{
			*router_id=logic_addr/100;
		}
		if(room_id)
		{
			*room_id=logic_addr%100;
		}
		return 0;
	}
	
	return -1;
}
int GetRouterCallSourceAddr(void)
{
	char paraString[20];
	int logic_addr;
	
	if(GetJsonDataPro(CallServer_Run.source_info,IX2V_IX_ADDR,paraString))
	{
		return (0x34+atoi(paraString+8)-1);
	}
	return 0x34;
}
#if 1
/*------------------------------------------------------------------------
						重拨处理
------------------------------------------------------------------------*/
void Callback_DtCaller_ToRedial_MainCallIM(CALLER_STRUCT *msg)
{
	int dt_addr=GetTargetDtAddr();
	log_d("DtMainCallIM:ToRedial,Addr=%02x",dt_addr);
	
	API_Stack_APT_Without_ACK(dt_addr,LET_DIDONG_ONCE);
	
}

/*------------------------------------------------------------------------
						呼叫申请
------------------------------------------------------------------------*/
void Callback_DtCaller_ToInvite_MainCallIM(CALLER_STRUCT *msg)	//R_
{
	
	//unsigned char addr_save;
	//addr_save = myAddr;
    
	usleep(100*1000);
	if(IfBDUMode())
	{
		int router_id,room_id;
		if(GetTargetRouterAddr(&router_id,&room_id)==0)
		{
			Api_StartOneRouterCall(router_id,room_id);
		}
	}
	else
	{
		 int dt_addr=GetTargetDtAddr();
		log_d("DtMainCallIM:ToInvite,Addr=%02x",dt_addr);
		API_Stack_APT_Without_ACK(dt_addr,MAIN_CALL);
	}
	
}

/*------------------------------------------------------------------------
						呼叫成功,进入Ringing
------------------------------------------------------------------------*/
void Callback_DtCaller_ToRinging_MainCallIM(CALLER_STRUCT *msg)	//R_
{
	#if 0
	if(SR_State.in_use == FALSE)
	{
		//链路创建
		//API_SR_CallerCreate();
		SR_Routing_Create(CT08_DS_CALL_ST);
		
	}
	#endif
	int dt_addr=GetTargetDtAddr();
	log_d("DtMainCallIM:ToRing,Addr=%02x",dt_addr);
	
}

/*------------------------------------------------------------------------
						分机摘机,进入ACK
------------------------------------------------------------------------*/
void Callback_DtCaller_ToAck_MainCallIM(CALLER_STRUCT *msg)	//R_
{
	#if 0
	if(SR_State.in_use == FALSE)
	{
		//链路创建
		//API_SR_CallerCreate();
		SR_Routing_Create(CT08_DS_CALL_ST);
	}
	#endif
	API_SR_Talking();
	
	if(IfBDUMode())
	{
		int router_id,room_id;
		if(GetTargetRouterAddr(&router_id,&room_id)==0)
		{
			Api_RouterCallToTalk(router_id,room_id);
		}
	}
	else
	{
		int dt_addr=GetTargetDtAddr();
		log_d("DtMainCallIM:ToAck,Addr=%02x",dt_addr);
	}
}

/*------------------------------------------------------------------------
						主机挂机,返回Waiting
------------------------------------------------------------------------*/
void Callback_DtCaller_ToBye_MainCallIM(CALLER_STRUCT *msg)	//R_
{
	#if 0
	if(SR_State.in_use == TRUE)
	{
		//API_SR_CallerClose();
		SR_Routing_Close(CT08_DS_CALL_ST);
		//sr_create_flag = 0;
	}
	#endif
	if(IfBDUMode())
	{
		int router_id,room_id;
		if(GetTargetRouterAddr(&router_id,&room_id)==0)
		{
			Api_CloseOneRouterCall(router_id,room_id);
		}
	}
	else
	{
		int dt_addr=GetTargetDtAddr();
		log_d("DtMainCallIM:ToBye,Addr=%02x",dt_addr);
		API_Stack_APT_Without_ACK(dt_addr,S_CANCEL);
	}
	//printf("1111111111111\n");
	//extern OS_TIMER timer_dtcaller;
	DtCaller_Obj.Caller_Run.timer = 0;
	//printf("22222222222222 %08x:%08x:%08x\n",(int)&timer_dtcaller,(int)DtCaller_Obj.timer_caller,(int)&DtCaller_Obj.timer_caller);
	OS_StopTimer(DtCaller_Obj.timer_caller);
	//printf("333333333333333\n");
	DtCaller_Obj.Caller_Run.state = CALLER_WAITING;
	//printf("55555555555555\n");
	//发送挂机指令给分机,取消呼叫
	//Gateway_SendIM_Command(DS4_ADDRESS, Caller_Run.target_addr & 0x00ff, S_CANCEL);
	
	//Trans_To_CallsurveyPartner(Caller_Run.call_type,Caller_Run.source_addr,Caller_Run.target_addr,CALLSURVEY_MSG_CDS_CANCEL);
	//状态机
	//Caller_Run.state = CALLER_WAITING;
	
	//关闭定时
	//Caller_Run.timer = 0;
	//OS_StopTimer(&timer_caller);
	
	
	
}

/*------------------------------------------------------------------------
				分机挂机(单元链路强制释放),返回Waitting
------------------------------------------------------------------------*/
void Callback_DtCaller_ToWaiting_MainCallIM(CALLER_STRUCT *msg)	//R_
{
	#if 0
	if(SR_State.in_use == TRUE)
	{
		SR_Routing_Close(CT08_DS_CALL_ST);
	}
	#endif
	int dt_addr=GetTargetDtAddr();
	log_d("DtMainCallIM:ToBye,Addr=%02x",dt_addr);
}


/*------------------------------------------------------------------------
						开锁处理
------------------------------------------------------------------------*/
void Callback_DtCaller_ToUnlock_MainCallIM(CALLER_STRUCT *msg)	//R_
{
	int dt_addr=GetTargetDtAddr();
	log_d("DtMainCallIM:ToUnlock,Addr=%02x",dt_addr);
//	if(Business_Run.call_type == CT18_CDS_CALL_ST_D)
//	{
//		;
//	}
//	else if(Business_Run.call_type == CT28_ST_MONITOR_D)
//	{
//		if(Caller_UnlockId == CALLER_UNLOCK1)
//		{
//			//向小区主机发送开锁1指令
//			//Gateway_SendDS_Command(CdsCallIPG_Run.cds_addr, CdsCallIPG_Run.st_addr, ST_UNLOCK);
//			API_Stack_APT_Without_ACK(Caller_Run.target_addr&0x00ff,ST_UNLOCK);
//		}
//		else if(Caller_UnlockId == CALLER_UNLOCK2)
//		{
//			//向小区主机发送开锁2指令
//			//Gateway_SendDS_Command(CdsCallIPG_Run.cds_addr, CdsCallIPG_Run.st_addr, ST_UNLOCK_SECOND);
//			//API_Stack_APT_Without_ACK(CdsCallIPG_Run.cds_addr,ST_UNLOCK_SECOND);
//			API_Stack_APT_Without_ACK(Caller_Run.target_addr&0x00ff,ST_UNLOCK_SECOND);
//		}
//	}
}

/*------------------------------------------------------------------------
					Caller呼叫超时处理(仅自身退出呼叫状态)						
------------------------------------------------------------------------*/
void Callback_DtCaller_ToTimeout_MainCallIM(CALLER_STRUCT *msg)	//R_
{
	
	int dt_addr=GetTargetDtAddr();
	log_d("DtMainCallIM:ToTimeout,Addr=%02x",dt_addr);
	if(DtCaller_Obj.Caller_Run.state== CALLER_INVITE)
	{
	  // Caller_Run.state = CALLER_WAITING;
	   //API_Phone_VTKCaller_Dial2Error();
	  	 if(IfBDUMode())
		{
			int router_id,room_id;
			if(GetTargetRouterAddr(&router_id,&room_id)==0)
			{
				Api_CloseOneRouterCall(router_id,room_id);
			}
		}
		 else
	   		API_Stack_APT_Without_ACK(GetTargetDtAddr(),S_CANCEL);
	  
	   
		DtCaller_Obj.Caller_Run.timer = 0;
		OS_StopTimer(DtCaller_Obj.timer_caller);
		DtCaller_Obj.Caller_Run.state = CALLER_WAITING;
		
	   	API_CallServer_InviteFail();
	}
	else
	{
		#if 0
		if(SR_State.in_use == TRUE&& SR_State.manager_IA == myAddr)
		{
			SR_Routing_Close(CT08_DS_CALL_ST);
		}
		#endif
		DtCaller_Obj.Caller_Run.timer = 0;
		OS_StopTimer(DtCaller_Obj.timer_caller);
		DtCaller_Obj.Caller_Run.state = CALLER_WAITING;
		
		API_CallServer_Timeout();
	}
}
/*------------------------------------------------------------------------
				接收到消息(): 错误处理					
------------------------------------------------------------------------*/
void Callback_DtCaller_ToError_MainCallIM(CALLER_STRUCT *msg)	
{
#if 0
	switch(Caller_ErrorCode)
	{
		case CALLER_ERROR_INVITEFAIL:
			break;
		case CALLER_ERROR_IPBECALLED_QUIT:
		case CALLER_ERROR_UINTLINK_CLEAR:
			
			if(Caller_Run.state != CALLER_WAITING)
			{
				if(SR_State.in_use == TRUE && SR_State.manager_IA == myAddr)
				{
					SR_Routing_Close(CT08_DS_CALL_ST);
				}
				//状态机
				Caller_Run.state = CALLER_WAITING;
				
				//关闭定时
				Caller_Run.timer = 0;
				OS_StopTimer(&timer_caller);
				
				if(GetPowerAudioState() == POWER_ON)
				{
					OS_Delay(100);
					API_POWER_AUDIO_OFF();
				}
				API_POWER_VIDEORX_OFF();
				API_POWER_VIDEOTX_OFF();
			}
			
			break;
	}
#endif
}

void Callback_DtCaller_ToForceClose_MainCallIM(CALLER_STRUCT *msg)
{
	log_d("DtMainCallIM:ToForceClose");
	//if(Caller_Run.state != CALLER_WAITING)
	{
		if(SR_State.in_use == TRUE && SR_State.manager_IA == myAddr)
		{
			SR_Routing_Close(CT08_DS_CALL_ST);
		}
		
		//API_Stack_APT_Without_ACK((Caller_Run.t_addr.rt<<8)|(Caller_Run.t_addr.code<<2)|0x80,S_CANCEL);
		//状态机
		DtCaller_Obj.Caller_Run.timer = 0;
		OS_StopTimer(DtCaller_Obj.timer_caller);
		DtCaller_Obj.Caller_Run.state = CALLER_WAITING;
		
	}
}
#endif
