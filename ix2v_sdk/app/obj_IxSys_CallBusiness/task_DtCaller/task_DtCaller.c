/**
  ******************************************************************************
  * @file    task_Called.c
  * @author  czn
  * @version V00.02.00 (basic on vsip)
  * @date    2014.11.06
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2014 V-Tec</center></h2>
  ******************************************************************************
  */ 
  
#include <sys/types.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <error.h>
#include <signal.h>
#include <pthread.h>
#include <semaphore.h>
#include <sched.h>



#include "obj_Caller_State.h"
#include "task_CallServer.h"
#include "task_DtCaller.h"
#include "obj_DtCaller_IntercomCall_Callback.h"
#include "obj_DtCaller_InnerCall_Callback.h"
#include "obj_DtCaller_MainCallIm_Callback.h"

#include "OSTIME.h"
#include "elog_forcall.h"

OS_TIMER timer_dtcaller;

OBJ_CALLER_STRU DtCaller_Obj;

Loop_vdp_common_buffer	vdp_dtcaller_mesg_queue;
Loop_vdp_common_buffer	vdp_dtcaller_sync_queue;


vdp_task_t	task_dtcaller;
void* vdp_dtcaller_task( void* arg );

void DtCaller_Config_Data_Init(void);
void DtCaller_MenuDisplay_ToInvite(void);
void DtCaller_MenuDisplay_ToRing(void);
void DtCaller_MenuDisplay_ToAck(void);
void DtCaller_MenuDisplay_ToBye(void);
void DtCaller_MenuDisplay_ToWait(void);
void DtCaller_Timer_Callback(void);
const char *dtcaller_type_str[]=
{
	"MainCall",
	"IntercomCall",					
	"InnerCall",
};
const int dtcaller_type_str_max=sizeof(dtcaller_type_str)/sizeof(dtcaller_type_str[0]);

#define DtCaller_Support_CallTypeNum	2


const STRUCT_CALLER_CALLTYPE_CALLBACK TABLE_CALLTYPE_DTCALLER_TOINVITE_CALLBACK[DtCaller_Support_CallTypeNum]={
	//calltype					//callback

	//{DtCaller_IntercomCall, 		Callback_DtCaller_ToInvite_IntercomCall,},		
	//{DtCaller_InnerCall,			Callback_DtCaller_ToInvite_InnerCall,},

	{DtCaller_MainCall,			Callback_DtCaller_ToInvite_MainCallIM,},
};


const STRUCT_CALLER_CALLTYPE_CALLBACK TABLE_CALLTYPE_DTCALLER_TORINGING_CALLBACK[DtCaller_Support_CallTypeNum]={
	//calltype					//callback
	
	//{IpCallType_VtkUnicast, 			Callback_Caller_ToRinging_VtkUnicastCall,},		//小区主机呼叫分机
	//{DtCaller_IntercomCall, 		Callback_DtCaller_ToRinging_IntercomCall,},		
	//{DtCaller_InnerCall,			Callback_DtCaller_ToRinging_InnerCall,},	
	{DtCaller_MainCall,			Callback_DtCaller_ToRinging_MainCallIM,},
};

const STRUCT_CALLER_CALLTYPE_CALLBACK TABLE_CALLTYPE_DTCALLER_TOACK_CALLBACK[DtCaller_Support_CallTypeNum]={
	//calltype					//callback

	//{IpCallType_VtkUnicast, 		Callback_Caller_ToAck_VtkUnicastCall,},			//小区主机呼叫分机
	//{DtCaller_IntercomCall, 		Callback_DtCaller_ToAck_IntercomCall,},		
	//{DtCaller_InnerCall,			Callback_DtCaller_ToAck_InnerCall,},
	{DtCaller_MainCall,			Callback_DtCaller_ToAck_MainCallIM,},
};


const STRUCT_CALLER_CALLTYPE_CALLBACK TABLE_CALLTYPE_DTCALLER_TOBYE_CALLBACK[]={
	//calltype					//callback
	
	//{IpCallType_VtkUnicast, 			Callback_Caller_ToBye_VtkUnicastCall,},			//小区主机呼叫分机
	//{DtCaller_IntercomCall, 		Callback_DtCaller_ToBye_IntercomCall,},		
	//{DtCaller_InnerCall,			Callback_DtCaller_ToBye_InnerCall,},
	{DtCaller_MainCall,			Callback_DtCaller_ToBye_MainCallIM,},
};


const STRUCT_CALLER_CALLTYPE_CALLBACK TABLE_CALLTYPE_DTCALLER_TOWAITING_CALLBACK[]={
	//calltype					//callback

	//{IpCallType_VtkUnicast, 		Callback_Caller_ToWaiting_VtkUnicastCall,},		//小区主机呼叫分机
	//{DtCaller_IntercomCall, 		Callback_DtCaller_ToWaiting_IntercomCall,},		
	//{DtCaller_InnerCall,			Callback_DtCaller_ToWaiting_InnerCall,},
	{DtCaller_MainCall,			Callback_DtCaller_ToWaiting_MainCallIM,},
};

const STRUCT_CALLER_CALLTYPE_CALLBACK TABLE_CALLTYPE_DTCALLER_TOUNLOCK_CALLBACK[]={
	//calltype					//callback
	
	//{IpCallType_VtkUnicast, 		Callback_Caller_ToUnlock_VtkUnicastCall,},		//小区主机呼叫分机
	//{DtCaller_IntercomCall, 		Callback_DtCaller_ToUnlock_IntercomCall,},		
	//{DtCaller_InnerCall,			Callback_DtCaller_ToUnlock_InnerCall,},
	{DtCaller_MainCall,			Callback_DtCaller_ToUnlock_MainCallIM,},
};

const STRUCT_CALLER_CALLTYPE_CALLBACK TABLE_CALLTYPE_DTCALLER_TOTIMEOUT_CALLBACK[DtCaller_Support_CallTypeNum]={
	//calltype					//callback

	//{IpCallType_VtkUnicast, 		Callback_Caller_ToTimeout_VtkUnicastCall,},		//小区主机呼叫分机
	//{DtCaller_IntercomCall, 		Callback_DtCaller_ToTimeout_IntercomCall,},		
	//{DtCaller_InnerCall,			Callback_DtCaller_ToTimeout_InnerCall,},
	{DtCaller_MainCall,			Callback_DtCaller_ToTimeout_MainCallIM,},
};


const STRUCT_CALLER_CALLTYPE_CALLBACK TABLE_CALLTYPE_DTCALLER_TOERROR_CALLBACK[DtCaller_Support_CallTypeNum]={
	//calltype					//callback

	//{IpCallType_VtkUnicast, 		Callback_Caller_ToError_VtkUnicastCall,},		//小区主机呼叫分机
	//{DtCaller_IntercomCall, 		Callback_DtCaller_ToError_IntercomCall,},		
	//{DtCaller_InnerCall,			Callback_DtCaller_ToError_InnerCall,},
	{DtCaller_MainCall,			Callback_DtCaller_ToError_MainCallIM,},
};


const STRUCT_CALLER_CALLTYPE_CALLBACK TABLE_CALLTYPE_DTCALLER_TOFORCECLOSE_CALLBACK[DtCaller_Support_CallTypeNum]={
	//calltype					//callback

	//{IpCallType_VtkUnicast, 		Callback_Caller_ToForceClose_VtkUnicastCall,},		//小区主机呼叫分机
	//{DtCaller_IntercomCall, 		Callback_DtCaller_ToForceClose_IntercomCall,},		
	//{DtCaller_InnerCall,			Callback_DtCaller_ToForceClose_InnerCall,},
	{DtCaller_MainCall,			Callback_DtCaller_ToForceClose_MainCallIM,},
};


/*------------------------------------------------------------------------
			Caller_Task_Init
------------------------------------------------------------------------*/
void DtCaller_Obj_Init(void)	//R_
{
	//任务初始化
	DtCaller_Obj.Caller_Run.state = CALLER_WAITING;
	DtCaller_Obj.timer_caller = &timer_dtcaller;
	DtCaller_Obj.task_caller = &task_dtcaller;
	
	DtCaller_Obj.Config_Data_Init = DtCaller_Config_Data_Init;

	DtCaller_Obj.to_invite_callback = TABLE_CALLTYPE_DTCALLER_TOINVITE_CALLBACK;
	DtCaller_Obj.to_ring_callback = TABLE_CALLTYPE_DTCALLER_TORINGING_CALLBACK; 
	DtCaller_Obj.to_ack_callback= TABLE_CALLTYPE_DTCALLER_TOACK_CALLBACK;
	DtCaller_Obj.to_bye_callback= TABLE_CALLTYPE_DTCALLER_TOBYE_CALLBACK;
	DtCaller_Obj.to_wait_callback= TABLE_CALLTYPE_DTCALLER_TOWAITING_CALLBACK;
	DtCaller_Obj.to_unclock_callback= TABLE_CALLTYPE_DTCALLER_TOUNLOCK_CALLBACK;
	DtCaller_Obj.to_timeout_callback= TABLE_CALLTYPE_DTCALLER_TOTIMEOUT_CALLBACK;
	DtCaller_Obj.to_error_callback= TABLE_CALLTYPE_DTCALLER_TOERROR_CALLBACK;
	DtCaller_Obj.to_forceclose_callback= TABLE_CALLTYPE_DTCALLER_TOFORCECLOSE_CALLBACK;
	DtCaller_Obj.to_redail_callback = NULL;		//czn_20171030
	
	DtCaller_Obj.support_calltype_num = DtCaller_Support_CallTypeNum;
	DtCaller_Obj.MenuDisplay_ToInvite = DtCaller_MenuDisplay_ToInvite;
	DtCaller_Obj.MenuDisplay_ToRing= DtCaller_MenuDisplay_ToRing;
	DtCaller_Obj.MenuDisplay_ToAck= DtCaller_MenuDisplay_ToAck;
	DtCaller_Obj.MenuDisplay_ToBye= DtCaller_MenuDisplay_ToBye;
	DtCaller_Obj.MenuDisplay_ToWait= DtCaller_MenuDisplay_ToWait;

	
	//创建软定时(未启动)
	OS_CreateTimer(&timer_dtcaller, DtCaller_Timer_Callback, 1000/25);
}



/*------------------------------------------------------------------------
			Get_Caller_State
返回:
	0-WAITING
	1-BUSY
------------------------------------------------------------------------*/
uint8 Get_DtCaller_State(void)	//R
{
	if (DtCaller_Obj.Caller_Run.state==CALLER_WAITING)
	{
		return (0);
	}
	return (1);
}


/*------------------------------------------------------------------------
			Get_Caller_PartnerAddr
------------------------------------------------------------------------*/
//uint16 Get_Caller_PartnerAddr(void)	//R
//{
//	return (Caller_Run.partner_IA);
//}





/*------------------------------------------------------------------------
						OutCall&CallIn Task Process
------------------------------------------------------------------------*/
void dtcaller_mesg_data_process(void *Msg,int len )	//R_
{
	//bprintf("!!!dtcaller state = %d,recv msg = %d\n",DtCaller_Obj.Caller_Run.state,((CALLER_STRUCT *)Msg)->msg_type);
	char *msg_str,*state_str,*cur_calltype_str,*from_thread,*calltype_str;
	char uk[]="UNKOWN";
	CALLER_STRUCT * caller_msg=(CALLER_STRUCT *)Msg;
	if(caller_msg->msg_type>=caller_msg_str_max)
		msg_str=uk;
	else
		msg_str=caller_msg_str[caller_msg->msg_type];
	
	if(caller_msg->call_type>=dtcaller_type_str_max)
		calltype_str=uk;
	else
		calltype_str=dtcaller_type_str[caller_msg->call_type];
	
	if(DtCaller_Obj.Caller_Run.state==CALLER_WAITING)
		cur_calltype_str="not-specified";
	else if(DtCaller_Obj.Caller_Run.call_type>=dtcaller_type_str_max)
		cur_calltype_str=uk;
	else
		cur_calltype_str=dtcaller_type_str[DtCaller_Obj.Caller_Run.call_type];
	
	if(DtCaller_Obj.Caller_Run.state>=caller_state_str_max)
		state_str=uk;
	else
		state_str=caller_state_str[DtCaller_Obj.Caller_Run.state];
	vdp_task_t *ptask = GetTaskAccordingMsgID(caller_msg->msg_source_id);
	if(ptask==NULL)
		from_thread=uk;
	else
		from_thread=ptask->task_name;
	//printf("dtcaller recv :%s,calltype:%s,from:%s,cur calltype:%s,state:%s\n",msg_str,calltype_str,from_thread,cur_calltype_str,state_str);		
	log_d("dtcaller recv :%s,calltype:%s,from:%s,cur calltype:%s,state:%s,timer=%d",msg_str,calltype_str,from_thread,cur_calltype_str,state_str,DtCaller_Obj.Caller_Run.timer);
	vtk_TaskProcessEvent_Caller(&DtCaller_Obj,(CALLER_STRUCT *)Msg);
	
}

//OneCallType	OneMyCallObject;

void init_vdp_dtcaller_task(void)
{
	init_vdp_common_queue(&vdp_dtcaller_mesg_queue, 1000, (msg_process)dtcaller_mesg_data_process, &task_dtcaller);
	init_vdp_common_queue(&vdp_dtcaller_sync_queue, 100, NULL, 								  &task_dtcaller);
	init_vdp_common_task(&task_dtcaller, MSG_ID_DtCalller, vdp_dtcaller_task, &vdp_dtcaller_mesg_queue, &vdp_dtcaller_sync_queue);
	task_dtcaller.task_StartCompleted = 1;
}

void exit_dtcaller_task(void)
{
	
}

void* vdp_dtcaller_task( void* arg )
{
	vdp_task_t*	 ptask 			= (vdp_task_t*)arg;
	p_vdp_common_buffer pdb 	= 0;
	int	size;
	thread_log_add("%s",__func__);
	DtCaller_Obj_Init();
	while( ptask->task_run_flag )
	{
		size = pop_vdp_common_queue(ptask->p_msg_buf, &pdb, VDP_QUEUE_POLLING_TIME);
		if( size > 0 )
		{
			(*ptask->p_msg_buf->process)(pdb,size);
			purge_vdp_common_queue( ptask->p_msg_buf );
		}
	}
	return 0;
}

void DtCaller_Timer_Callback(void)
{
	//printf("xxxxxxxxxxxx %08x:%08x:%08x\n",(int)&timer_dtcaller,(int)DtCaller_Obj.timer_caller,(int)&DtCaller_Obj.timer_caller);
	Caller_Timer_Callback(&DtCaller_Obj);
}

void DtCaller_Config_Data_Init(void)
{
	switch(DtCaller_Obj.Caller_Run.call_type)
	{
		case DtCaller_MainCall:
		case DtCaller_IntercomCall:
		case DtCaller_IXGRM:	
			DtCaller_Obj.Caller_Config.limit_invite_time = 5;
			DtCaller_Obj.Caller_Config.limit_ringing_time = 60;
			DtCaller_Obj.Caller_Config.limit_ack_time = 90;
			DtCaller_Obj.Caller_Config.limit_bye_time = 5;
			break;
		case DtCaller_InnerCall:
			DtCaller_Obj.Caller_Config.limit_invite_time = 90;
			DtCaller_Obj.Caller_Config.limit_ringing_time = 90;
			DtCaller_Obj.Caller_Config.limit_ack_time = 90;
			DtCaller_Obj.Caller_Config.limit_bye_time = 5;
			break;
	}
}
void DtCaller_MenuDisplay_ToInvite(void)
{

}

void DtCaller_MenuDisplay_ToRing(void)
{
}

void DtCaller_MenuDisplay_ToAck(void)
{
}
void DtCaller_MenuDisplay_ToBye(void)
{
}
void DtCaller_MenuDisplay_ToWait(void)
{
}

int API_DtCaller_Common(uint8 msg_type, uint8 call_type, Global_Addr_Stru *s_addr,Global_Addr_Stru *t_addr)	//R
{
	CALLER_STRUCT	send_msg;	
	char rev[5];
	int   rev_len;
	vdp_task_t* ptask = NULL;
	//OS_TASK_EVENT MyEvents;	

	//组织发送消息给BeCalled
	send_msg.msg_source_id 	= GetMsgIDAccordingPid(pthread_self());
	send_msg.msg_target_id	= MSG_ID_CallServer;
	send_msg.msg_type		= msg_type;
	send_msg.msg_sub_type	= 0;
	send_msg.call_type		= call_type;
	if(s_addr != NULL)
	{
		send_msg.s_addr = *s_addr;
	}
	if(t_addr != NULL)
	{
		send_msg.t_addr = *t_addr;
	}
	#if 0
	if(msg_type == CALLER_MSG_INVITE)
	{
		ptask = GetTaskAccordingMsgID(send_msg.msg_source_id);
		if(ptask != NULL)
		{
			if(ptask ->p_syc_buf != NULL)
			{
				WaitForBusinessACK(ptask->p_syc_buf,msg_type,rev,&rev_len,1);
			}
		}
	}
	#endif
	if(push_vdp_common_queue(task_dtcaller.p_msg_buf, (char *)&send_msg, sizeof(CALLER_STRUCT)) != 0)
	{
		return -1;
	}
	#if 0
	if(msg_type == CALLER_MSG_INVITE && ptask != NULL)
	{
		if(ptask ->p_syc_buf != NULL)
		{
			if(WaitForBusinessACK(ptask->p_syc_buf,msg_type,rev,&rev_len,3000) == 1)
			{
				if(rev_len >= 5)
				{
					if(rev[4] != 0)
					{
						return -1;
					}
				}
			}
		}
	}
	#endif

	return 0;
}

void DtCaller_Respones(unsigned char respones_id,unsigned char msg_type,unsigned char result)
{
	unsigned char rev[5];
	vdp_task_t* ptask = NULL;
	
	ptask = GetTaskAccordingMsgID(respones_id);

	if(ptask == NULL)
		return;
	
	rev[0] = respones_id;
	rev[1] = MSG_ID_CallServer;
	rev[2] = msg_type |0x80;
	rev[3] = 0;
	rev[4] = result;
	
	push_vdp_common_queue(ptask->p_syc_buf, rev, 5);
}
cJSON *GetDtCallerState(void)
{
	static cJSON *state=NULL;
	char *cur_calltype_str,*cur_state_str;
	char uk[]="UNKOWN";

	if(state!=NULL)
		cJSON_Delete(state);
	
	state=cJSON_CreateObject();
	if(state==NULL)
		return NULL;
	if(DtCaller_Obj.Caller_Run.state==CALLER_WAITING)
		cur_calltype_str="not-specified";
	else if(DtCaller_Obj.Caller_Run.call_type>=dtcaller_type_str_max)
		cur_calltype_str=uk;
	else
		cur_calltype_str=dtcaller_type_str[DtCaller_Obj.Caller_Run.call_type];
	
	if(DtCaller_Obj.Caller_Run.state>=caller_state_str_max)
		cur_state_str=uk;
	else
		cur_state_str=caller_state_str[DtCaller_Obj.Caller_Run.state];
	cJSON_AddStringToObject(state,PB_CALL_DTCALLER_STATE,cur_state_str);
	cJSON_AddStringToObject(state,PB_CALL_DTCALLER_TYPE,cur_calltype_str);
	//cJSON_AddNumberToObject(state,"TIMER",DtCaller_Obj.Caller_Run.timer);
	return state;
}
/*********************************************************************************************************
**  End Of File
*********************************************************************************************************/

