
#ifndef _obj_XD2_H_
#define _obj_XD2_H_

#include "cJSON.h"
#include "tcp_Client_process.h"

#define XD2_DIRETORY_MANAGE_REQ						0x3000
#define XD2_DIRETORY_MANAGE_RSP						0x3001
#define XD2_WAIT_RSP_TIME_UNIT						20				//等待应答时间单元 ms
#define XD2_CMD_HEAD								"VTEK"
#define XD2_DIRETORY_MANAGE_TIME_OUT				3
#define XD2_DIRETORY_MANAGE_DAT_LEN					512
#define XD2_PACKET_MAX_LEN							1000

#define BusinessWaitUdpTime							2				//2 秒

typedef struct
{
    pthread_mutex_t 	lock;
    cJSON*				table;
}XD2_RUN;

typedef struct
{
	int					len;			//应答数据长度
	char*				data;			//应答数据
}XD2_RSP_DATA;

typedef struct
{
	int					waitFlag;			//等待回应标记
	short				rand;				//随机数
	int					cmd;				//命令
	int 				result;				//应答结果
	XD2_RSP_DATA 		rspData;			//应答数据
}XD2_REQ;

#pragma pack(2) 

typedef struct
{
	char				CLIENT_ID[32];			//字符串，最大长度 32 个字节，以0结束
	char				CLIENT_PSW[16];			//字符串，最大长度 16 个字节，以0结束
	char				CLIENT_OPT[8];			//字符串，最大长度8字节，以0结束
	short 				REQ_STATE;				//unsigned short，缺省为 0
	char 				REQ_DAT[XD2_DIRETORY_MANAGE_DAT_LEN];			//字符串，最大长度XD2_DIRETORY_MANAGE_DAT_LEN，以0结束
}XD2_FileManageReq;
typedef struct
{
	char				CLIENT_ID[32];			//字符串，最大长度 32 个字节，以0结束
	char				CLIENT_PSW[16];			//字符串，最大长度 16 个字节，以0结束
	char				CLIENT_OPT[8];			//字符串，最大长度8字节，以0结束
	short 				RSP_RESULT;				//unsigned short，0/ok，1/账号错误，2/密码错误，3/无效命令，4/操作失败，5/其他错误
	char 				RSP_DAT[XD2_DIRETORY_MANAGE_DAT_LEN];			//字符串，最大长度XD2_DIRETORY_MANAGE_DAT_LEN，以0结束
}XD2_FileManageRsp;
#pragma pack()

static void XD2_ReceiveCmdRsp(ProxyRemoteHead head, char* data, int len);
static int API_XD2_SendReq(short cmd, char* data, int len, int timeOut, XD2_RSP_DATA* dataOut);

int API_XD2_SendRsp(short cmd, short cmd_id, char* data, int len);
#endif

