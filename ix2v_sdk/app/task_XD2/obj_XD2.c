
#include <stdio.h>
#include <strings.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <sys/time.h>
#include <stdlib.h>
#include <memory.h>
#include <arpa/inet.h>
#include <arpa/nameser.h>
#include <netinet/in.h>
#include <net/if.h>
#include <netdb.h>
#include <poll.h>
#include <pthread.h>
#include <error.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <resolv.h>
#include "utility.h"

#include "elog.h"
#include "obj_TCP_Client.h"
#include "one_tiny_task.h"
#include "obj_IoInterface.h"
#include "obj_PublicInformation.h"
#include "vtk_udp_stack_class.h"
#include "tcp_Client_process.h"
#include "obj_XD2.h"

#define XD2_TCP_PORT					8864

static P_TCP_Client XD2_TcpConnect = NULL;

static void XD2_TcpRcvDataProcess(ProxyRemoteHead packHead, char* data, int dat_len)
{

	switch(packHead.cmd_type)
	{
		case XD2_DIRETORY_MANAGE_RSP:
			XD2_ReceiveCmdRsp(packHead, data, dat_len);
			break;

		case IX_DEVICE_REMOTE_NORMAL_TRANSFER_DAT:
			API_XD2_MSG(packHead.cmd_id, data, dat_len);
			break;
	}
}

void XD2_Init(void)
{
	if(XD2_TcpConnect == NULL)
	{
		if(strlen(API_Para_Read_String2(SIP_ACCOUNT))>0 && strlen(API_Para_Read_String2(SIP_PASSWORD))>0)
		{
			char server[200] = {0};
			char* net_dev_name = API_Para_Read_String2(SIP_NetworkSetting);
			net_dev_name = (net_dev_name && !strcmp(net_dev_name, "WLAN")) ? NET_WLAN0 : NET_ETH0;
			sscanf(API_Para_Read_String2(SIP_SERVER),"%[^:]", server);		

			XD2_TcpConnect = CreatOneTcpConenct(net_dev_name, server, XD2_TCP_PORT, API_Para_Read_String2(SIP_ACCOUNT), API_Para_Read_String2(SIP_PASSWORD), 1, XD2_TcpRcvDataProcess);
			dprintf("XD2 init %s\n", XD2_TcpConnect ? "success" : "fail:CreatOneTcpConenct error");
		}
		else
		{
			dprintf("XD2 init fail:io SIP_ACCOUNT or SIP_PASSWORD error\n");
		}
	}
	else
	{
		dprintf("XD2 has init already\n");
	}
}

void ReloadXD2ConnectPara(void)
{
	if(XD2_TcpConnect)
	{
		char server[200] = {0};
		char* net_dev_name = API_Para_Read_String2(SIP_NetworkSetting);
		net_dev_name = (net_dev_name && !strcmp(net_dev_name, "WLAN")) ? NET_WLAN0 : NET_ETH0;
		sscanf(API_Para_Read_String2(SIP_SERVER),"%[^:]", server);
		
		XD2_TcpConnect->port = XD2_TCP_PORT;
		snprintf(XD2_TcpConnect->network, 16, "%s", net_dev_name);
		snprintf(XD2_TcpConnect->ip, 16, "%s", server);
		snprintf(XD2_TcpConnect->id, 32, "%s", API_Para_Read_String2(SIP_ACCOUNT));
		snprintf(XD2_TcpConnect->pwd, 16, "%s", API_Para_Read_String2(SIP_PASSWORD));	
	}
}

cJSON* CreateXD2ConnectState(int state)
{
	cJSON* ret = cJSON_CreateObject();
	cJSON_AddStringToObject(ret, "XD2_STATE", state ? "Connect":"Disconnect");

	if(XD2_TcpConnect)
	{
		cJSON_AddStringToObject(ret, "XD2_NET_WORK", XD2_TcpConnect->network);
		cJSON_AddStringToObject(ret, "XD2_SERVER", XD2_TcpConnect->ip);
		cJSON_AddNumberToObject(ret, "XD2_PORT", XD2_TcpConnect->port);
		cJSON_AddStringToObject(ret, "XD2_ACCOUNT", XD2_TcpConnect->id);
		cJSON_AddStringToObject(ret, "XD2_PASSWORD", XD2_TcpConnect->pwd);
	}

	return ret;
}

void XD2_ReConnect(void)
{
	if(XD2_TcpConnect)
	{
		ReloadXD2ConnectPara();
		TCP_ReConnectToServer(XD2_TcpConnect);
	}
}

P_TCP_Client GetXD2TCPTask(void)
{
	return XD2_TcpConnect;
}

static XD2_RUN xd2Run = {.lock = PTHREAD_MUTEX_INITIALIZER, .table = NULL};

static XD2_REQ* CreateXD2_REQ(int cmd)
{
	XD2_REQ* xd2Req = NULL;
	pthread_mutex_lock(&xd2Run.lock);

	if(xd2Run.table == NULL)
	{
		xd2Run.table = cJSON_CreateArray();
	}

	xd2Req = malloc(sizeof(XD2_REQ));
	cJSON_AddItemToArray(xd2Run.table, cJSON_CreateNumber((int)xd2Req));

	xd2Req->rand = MyRand() + MyRand2(cmd);;
	xd2Req->waitFlag = 1;
	xd2Req->result = 0;
	xd2Req->cmd = cmd;
	xd2Req->rspData.len = 0;
	xd2Req->rspData.data = NULL;

	pthread_mutex_unlock(&xd2Run.lock);

	return xd2Req;
}

static void DeleteXD2_REQ(XD2_REQ* xd2Req)
{
	cJSON* element = NULL;

	pthread_mutex_lock(&xd2Run.lock);
	if(xd2Req)
	{
		free(xd2Req);
	}

	cJSON_ArrayForEach(element, xd2Run.table)
	{
		if(((int)xd2Req) == element->valueint)
		{
			cJSON_DetachItemViaPointer(xd2Run.table, element);
			cJSON_Delete(element);
			break;
		}
	}
	pthread_mutex_unlock(&xd2Run.lock);
}

static int API_XD2_SendReq(short cmd, char* data, int len, int timeOut, XD2_RSP_DATA* dataOut)
{
	int timeCnt;
	int ret, headLen;
	ProxyRemoteHead head;
	char sendData[XD2_TCP_BUFF_MAX];
	XD2_REQ* req;

	headLen = sizeof(head);
	if(len + headLen > XD2_TCP_BUFF_MAX)
	{
		return 0;
	}

	memcpy(head.head, XD2_CMD_HEAD, 4);
	head.cmd_type = cmd;
	head.dev_type = 0;
	head.data_len = len;

	if(timeOut)
	{
		req = CreateXD2_REQ(cmd);
		head.cmd_id = req->rand;
	}
	else
	{
		head.cmd_id = 0;
	}

	memcpy(sendData, &head, headLen);
	if(data && len > 0)
	{
		memcpy(sendData + headLen, data, head.data_len);
	}

	ret = API_TCP_Send(XD2_TcpConnect, sendData, headLen + head.data_len);

	if(timeOut)
	{
		timeCnt = timeOut*1000/XD2_WAIT_RSP_TIME_UNIT;
		while(timeCnt && ret)
		{
			usleep(XD2_WAIT_RSP_TIME_UNIT*1000);
			
			if(req->waitFlag == 0)
			{
				ret = req->result;
				if(ret)
				{
					if(dataOut != NULL)
					{
						(*dataOut).data = req->rspData.data;
						(*dataOut).len = req->rspData.len;
					}
				}
				break;
			}

			if(--timeCnt == 0)
			{
				ret = 0;
				break;
			}
		}
		req->waitFlag = 0;
		DeleteXD2_REQ(req);
	}

	return ret;
}

int API_XD2_SendRsp(short cmd, short cmd_id, char* data, int len)
{
	int headLen;
	ProxyRemoteHead head;
	char sendData[XD2_TCP_BUFF_MAX];

	headLen = sizeof(head);
	if(len + headLen > XD2_TCP_BUFF_MAX)
	{
		return 0;
	}

	memcpy(head.head, XD2_CMD_HEAD, 4);
	head.cmd_type = cmd;
	head.cmd_id = cmd_id;
	head.dev_type = 0;
	head.data_len = len;

	memcpy(sendData, &head, headLen);
	if(data && len > 0)
	{
		memcpy(sendData + headLen, data, head.data_len);
	}

	return API_TCP_Send(XD2_TcpConnect, sendData, headLen + head.data_len);
}


//接收到命令应答指令
static void XD2_ReceiveCmdRsp(ProxyRemoteHead head, char* data, int len)
{
	cJSON* element;
	XD2_REQ* req;

	pthread_mutex_lock(&xd2Run.lock);
	cJSON_ArrayForEach(element, xd2Run.table)
	{
		req = (XD2_REQ*)element->valueint;
		if(req->waitFlag && req->rand == head.cmd_id && (req->cmd + 1) == head.cmd_type)
		{
			if(head.data_len != len)
			{
				req->result = 0;
			}
			else
			{
				req->result = 1;
			}

			if(req->rspData.data == NULL)
			{
				req->rspData.data = malloc(len);
			}

			if(req->rspData.data != NULL)
			{
				memcpy(req->rspData.data, data, len);
			}
			req->rspData.len = len;
			req->waitFlag = 0;
		}
	}
	pthread_mutex_unlock(&xd2Run.lock);
}


/***************************************************************************************************/
/***************************************************************************************************/
/***************************************************************************************************/
/***************************************************************************************************/
/***************************************************************************************************/
/***************************************************************************************************/
/***************************************************************************************************/
/***************************************************************************************************/
/***************************************************************************************************/
/***************************************************************************************************/
int API_XD2_FileManageReq(char* data)
{
	XD2_FileManageReq req = {0};
	XD2_RSP_DATA dataOut;
	char* pos;
	int len, ret, i;
	char cpData[1000] = {0};
	snprintf(cpData, 1000, "%s", data);

	dataOut.data = NULL;
	dataOut.len = 0;

	snprintf(req.CLIENT_ID, 32, "%s", API_PublicInfo_Read_String(PB_MFG_SN));
	snprintf(req.CLIENT_PSW, 16, "%s", API_Para_Read_String2(PRJ_PWD));
	req.REQ_STATE = 0;
	for(pos = strtok(cpData, " "), i = 0, len = 0; pos && i < 3; pos = strtok(NULL," "), i++)
	{
		if(i == 0)
		{
			snprintf(req.CLIENT_OPT, 8, "%s", pos);
		}
		else
		{
			if(len < XD2_DIRETORY_MANAGE_DAT_LEN)
			{
				snprintf(req.REQ_DAT + len, XD2_DIRETORY_MANAGE_DAT_LEN - len, "%s", pos);
				len += ((strlen(pos) +1) < (XD2_DIRETORY_MANAGE_DAT_LEN - len) ? (strlen(pos) +1) : (XD2_DIRETORY_MANAGE_DAT_LEN - len));
			}
		}
	}

	dprintf("File cmd = %s, req.REQ_DAT = %s\n", data, req.REQ_DAT);

	ret = API_XD2_SendReq(XD2_DIRETORY_MANAGE_REQ, &req, sizeof(req) - XD2_DIRETORY_MANAGE_DAT_LEN + len, XD2_DIRETORY_MANAGE_TIME_OUT, &dataOut);

	if(dataOut.data)
	{
		XD2_FileManageRsp* rsp = (XD2_FileManageRsp*)dataOut.data;
		if(rsp->RSP_RESULT)
		{
			ret = 0;
		}
		dprintf("rsp->RSP_RESULT = %d, rsp->RSP_DAT = %s\n", rsp->RSP_RESULT, rsp->RSP_DAT);
		free(dataOut.data);
	}

	return ret;
}