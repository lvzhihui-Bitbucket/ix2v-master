/**
  ******************************************************************************
  * @file    task_XD2.c
  * @author  lvzhihui
  * @version V1.0.0
  * @date    2016.04.15
  * @brief   This file contains the functions of task_ix_proxy
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2012 V-Tec</center></h2>
  ******************************************************************************
  */
  
#include <sys/types.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <error.h>
#include <signal.h>
#include <pthread.h>
#include <semaphore.h>
#include <sched.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <errno.h>
#include <dirent.h>

#include "one_tiny_task.h"
#include "task_survey.h"
#include "task_XD2.h"
#include "elog_forcall.h"
#include "obj_XD2.h"
#include "obj_XD2_CmdProcess.h"   

static Loop_vdp_common_buffer	vdp_XD2_mesg_queue;
static vdp_task_t				task_XD2;

static void vdp_XD2_mesg_data_process(char* msg_data, int len)
{
	XD2_MSG* pMsg = (XD2_MSG*)msg_data;
	XD2_MSG_HEAD* pMsgHead = (XD2_MSG_HEAD*)pMsg->data;
	int headLen = sizeof(XD2_MSG_HEAD);

	if(GetCheckSumResult(ntohs(pMsgHead->CheckSUM), (char*)((char*)(pMsgHead) + headLen), pMsg->dataLen - headLen) == 0)
	{
		return;
	}

	dprintf("pMsgHead->OP = 0x%04x, Length = %d\n", ntohs(pMsgHead->OP), ntohs(pMsgHead->Length));

	switch (ntohs(pMsgHead->OP))
	{
		case XD2_CMD_FILE_UPLOAD_REQ:
			XD2_UploadFile(pMsg);
			break;

		case XD2_CMD_SET_DIVERT_REQ:
			XD2_SetDivertState(pMsg);
			break;

		case XD2_CMD_GetCallRecord_REQ:
			XD2_CheckCallRecord(pMsg);
			break;
	}
}

void vdp_XD2_task_init(void)
{
	init_vdp_common_queue(&vdp_XD2_mesg_queue, 5000, vdp_XD2_mesg_data_process, &task_XD2);
	init_vdp_common_task(&task_XD2, MSG_ID_XD2, vdp_public_task, &vdp_XD2_mesg_queue, NULL);
}

void API_XD2_MSG(short cmd_id, char* data, int len)
{
	XD2_MSG msg;

	msg.cmd_id = cmd_id;
	len = (len > XD2_PACKET_MAX_LEN ? XD2_PACKET_MAX_LEN : len);
	msg.dataLen = len+1;
	if(data && len > 0)
	{
		memcpy(msg.data, data, len);
	}
	msg.data[len] = 0;

	// ѹ�뱾�ض���
	push_vdp_common_queue(&vdp_XD2_mesg_queue, (char*)&msg, sizeof(msg) - XD2_PACKET_MAX_LEN + len);
}

static int FillXD2Package(char* sendData, XD2_MSG_HEAD head, char* data, int dataLen)
{
	int len = sizeof(head);
	int i;
	
	head.CheckSUM = 0;
	if(len + dataLen > XD2_MSG_DATA_MAX_LEN)
	{
		return 0;
	}

	if(data && dataLen)
	{
		for(i = 0, head.CheckSUM = 0; i < dataLen; i++)
		{
				head.CheckSUM += data[i];
		}
	}
	
	head.businessCode = htons(head.businessCode);
	head.OP = htons(head.OP);
	head.Session_ID = htons(head.Session_ID);
	head.RSP_ID = htons(head.RSP_ID);
	head.Length = htons(len + dataLen);
	head.CheckSUM = htons(head.CheckSUM);

	memcpy(sendData, (char*)&head, len);

	if(data && dataLen)
	{
		memcpy(sendData+len, data, dataLen);
		len += dataLen;
	}

	return len;
}

static int XD2_ResponeOnePacket(XD2_MSG_HEAD head, short cmd_id, char* pbuf, int len)
{
	int ret = 0;
	char sendData[XD2_MSG_DATA_MAX_LEN];
	char* cmdData = (char*)pbuf;

	len = FillXD2Package(sendData, head, cmdData, len);
	if(len)
	{
		ret = API_XD2_SendRsp(0x1008, cmd_id, sendData, len);
	}

	return ret;
}

void XD2_Respone(short cmd_id, unsigned short OP,unsigned short Session_ID, unsigned short RSP_ID, char* data, int len)
{
	XD2_MSG_HEAD head;
	int sendLen, packCnt, i;
	char sendData[XD2_MSG_DATA_MAX_LEN+3];

	head.OP = OP;

	if(!(head.OP & 0x8000))
	{
		head.OP |= 0x8000;
	}
	
	head.businessCode = 0;
	head.Session_ID = Session_ID;
	head.RSP_ID = RSP_ID;
	if(data && len)
	{
		log_d("XD2_Respone len=%d,data=%s\n", sendLen, data);
	
		packCnt = len/XD2_MSG_DATA_MAX_LEN+1;
		for(i = 0; i < packCnt; i++)
		{
			sendLen = (len - XD2_MSG_DATA_MAX_LEN*i > XD2_MSG_DATA_MAX_LEN ? XD2_MSG_DATA_MAX_LEN : len - XD2_MSG_DATA_MAX_LEN*i);
			sendData[0] = i+1;
			sendData[1] = packCnt;
			memcpy(sendData+2, data+XD2_MSG_DATA_MAX_LEN*i, sendLen);
			XD2_RSP_DELAY();
			XD2_ResponeOnePacket(head, cmd_id, sendData, sendLen+2);
		}
	}
	else
	{
		XD2_RSP_DELAY();
		XD2_ResponeOnePacket(head, cmd_id, NULL, 0);
	}
}


static int FillXD2ReportPackage(char* sendData, XD2_REPORT_HEAD head, char* data, int dataLen)
{
	int len = sizeof(head);
	int i;

	if(len + dataLen > XD2_MSG_DATA_MAX_LEN)
	{
		return 0;
	}
	
	head.CheckSUM = (head.SUB_OP >> 8) + (head.SUB_OP & 0x00FF);

	if(data && dataLen)
	{
		for(i = 0; i < dataLen; i++)
		{
				head.CheckSUM += data[i];
		}
	}
	
	head.businessCode = htons(head.businessCode);
	head.OP = htons(head.OP);
	head.Session_ID = htons(head.Session_ID);
	head.RSP_ID = htons(head.RSP_ID);
	head.Length = htons(len + dataLen);
	head.CheckSUM = htons(head.CheckSUM);
	head.SUB_OP = htons(head.SUB_OP);

	memcpy(sendData, (char*)&head, len);

	if(data && dataLen)
	{
		memcpy(sendData+len, data, dataLen);
		len += dataLen;
	}

	//dprintf("data=%s\n", data);

	return len;
}

int XD2_Report(unsigned short SUB_OP, unsigned short Session_ID, char* data, int len)
{
	char sendData[XD2_MSG_DATA_MAX_LEN];
	XD2_REPORT_HEAD head;
	int sendLen;
	int ret = 0;

	head.businessCode = 0;
	head.OP = XD2_CMD_REPORT_REQ;	
	head.Session_ID = Session_ID;
	head.RSP_ID = 0;
	head.SUB_OP = SUB_OP;

	log_d("XD2_Report sendLen=%d,data=%s\n", sendLen, data);

	sendLen = FillXD2ReportPackage(sendData, head, data, len);
	if(sendLen)
	{
		ret = API_XD2_SendRsp(0x1008, 0, sendData, sendLen);
	}

	return ret;
}

void API_XD2_Shell(char* op, char* data)
{
	XD2_MSG_HEAD head;
	char sendData[XD2_MSG_DATA_MAX_LEN];
	int len;
	if(data && op)
	{
		head.businessCode = 0;
		sscanf(op, "%hx", &head.OP);
		head.Session_ID = 0;
		head.RSP_ID = 0;
		len = FillXD2Package(sendData, head, data, strlen(data)+1);
		API_XD2_MSG(0, sendData, len);
	}
}
