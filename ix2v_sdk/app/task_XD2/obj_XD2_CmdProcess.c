/**
  ******************************************************************************
  * @file    obj_XD2_CmdProcess.c
  * @author  lvzhihui
  * @version V1.0.0
  * @date    2016.04.15
  * @brief   This file contains the functions of task_ix_proxy
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2012 V-Tec</center></h2>
  ******************************************************************************
  */
  
#include <sys/types.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <error.h>
#include <signal.h>
#include <pthread.h>
#include <semaphore.h>
#include <sched.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <errno.h>
#include <dirent.h>

#include "utility.h"
#include "task_XD2.h"
#include "define_file.h"
#include "define_string.h"
#include "cJSON.h"
#include "task_XD2.h"
#include "elog_forcall.h"
#include "obj_Ftp.h"   
#include "obj_IoInterface.h"
#include "obj_TableSurver.h"
#include "obj_PublicInformation.h"
#include "obj_TB_Sub_SIP_ACC.h"

static void XD2_ResponeJson(short cmd_id, unsigned short OP,unsigned short Session_ID, unsigned short RSP_ID, const cJSON* rspJson)
{
	if(rspJson)
	{
		char* rsp = cJSON_PrintUnformatted(rspJson);
		if(rsp)
		{
			XD2_Respone(cmd_id, OP, Session_ID, 1, rsp, strlen(rsp)+1);
			free(rsp);
		}
	}
}

/*********************************************************************************************
 ***********************************文件上传指令处理*******************************************
 ********************************************************************************************/
static int XD2_UploadFileCallback(int state, FTP_FILE_STATISTICS statistics, void* data)
{
	cJSON* rspJson = NULL;
	cJSON* jsonData = (cJSON*)data;
	cJSON* sessionIdJson = NULL;
	unsigned short sessionId = 0;
	static int reportCnt = 0;

	if(state == FTP_STATE_UP_ING)
	{
		int rate = statistics.now*100.0/statistics.total;;
		if(reportCnt != rate%5)
		{
			char info[100];
			rspJson = cJSON_Duplicate(jsonData, 1);
			cJSON_DeleteItemFromObjectCaseSensitive(rspJson, "SESSION_ID");
			cJSON_AddStringToObject(rspJson, "RESULT", "UPLOADING");
			reportCnt = rate%5;
			snprintf(info, 100, "%u/%u(%u%%)", statistics.now, statistics.total, rate);
			cJSON_AddStringToObject(rspJson, "INFO", info);
			sessionIdJson = cJSON_GetObjectItemCaseSensitive(jsonData, "SESSION_ID");
			if(cJSON_IsNumber(sessionIdJson))
			{
				sessionId = sessionIdJson->valueint;
			}
		}
	}
	else if(state == FTP_STATE_UP_ERR)
	{
		rspJson = cJSON_Duplicate(jsonData, 1);
		cJSON_DeleteItemFromObjectCaseSensitive(rspJson, "SESSION_ID");
		cJSON_AddStringToObject(rspJson, "RESULT", "UPLOAD_ERROR");
		sessionIdJson = cJSON_GetObjectItemCaseSensitive(jsonData, "SESSION_ID");
		if(cJSON_IsNumber(sessionIdJson))
		{
			sessionId = sessionIdJson->valueint;
		}

		//结束之后，需要释放内存。
		cJSON_Delete(jsonData);
	}
	else if(state == FTP_STATE_UP_OK)
	{
		rspJson = cJSON_Duplicate(jsonData, 1);
		cJSON_DeleteItemFromObjectCaseSensitive(rspJson, "SESSION_ID");
		cJSON_AddStringToObject(rspJson, "RESULT", "UPLOAD_OK");
		sessionIdJson = cJSON_GetObjectItemCaseSensitive(jsonData, "SESSION_ID");
		if(cJSON_IsNumber(sessionIdJson))
		{
			sessionId = sessionIdJson->valueint;
		}

		//结束之后，需要释放内存。
		cJSON_Delete(jsonData);
	}
	else if(state == FTP_STATE_UP_STOP)
	{
		rspJson = cJSON_Duplicate(jsonData, 1);
		cJSON_DeleteItemFromObjectCaseSensitive(rspJson, "SESSION_ID");
		cJSON_AddStringToObject(rspJson, "RESULT", "UPLOAD_STOP");
		sessionIdJson = cJSON_GetObjectItemCaseSensitive(jsonData, "SESSION_ID");
		if(cJSON_IsNumber(sessionIdJson))
		{
			sessionId = sessionIdJson->valueint;
		}

		//结束之后，需要释放内存。
		cJSON_Delete(jsonData);
	}

	if(rspJson)
	{
		char* rspString = cJSON_PrintUnformatted(rspJson);
		XD2_Report(XD2_REPORT_SUP_OB_UPLOAD, sessionId, rspString, strlen(rspString)+1);
		free(rspString);
		cJSON_Delete(rspJson);
	}

	return 0;
}


int XD2_UploadFile(XD2_MSG* msg)
{
	int ret = 0;
	XD2_MSG_HEAD* pMsgData = (XD2_MSG_HEAD*)msg->data;
	char* jsonString = (char*)((msg->data) + sizeof(XD2_MSG_HEAD));
	cJSON* jsonData = cJSON_Parse(jsonString);

	char* source = GetEventItemString(jsonData,  "SOURCE");
	char* target = GetEventItemString(jsonData,  "TARGET");
	char* server = GetEventItemString(jsonData,  "SERVER");

	//文件存在
	if(IsFileExist(source))
	{
		char targetDir[200];

		cJSON* callbackData = cJSON_CreateObject();
		cJSON_AddStringToObject(callbackData, "SERVER", server);
		cJSON_AddStringToObject(callbackData, "SOURCE", source);
		cJSON_AddStringToObject(callbackData, "TARGET", target);
		cJSON_AddNumberToObject(callbackData, "SESSION_ID", ntohs(pMsgData->Session_ID));
		snprintf(targetDir, 200, "%s/%s/%s", XD2_SEVERT_ROOT, API_Para_Read_String2(SIP_ACCOUNT), target);
		//启动成功
		if(FTP_StartUpload(server, source, targetDir, callbackData,  XD2_UploadFileCallback))
		{
			ret = 1;
		}
		else
		{
			cJSON_Delete(callbackData);
		}
	}

	cJSON_AddStringToObject(jsonData, "RESULT", ret ? IX2V_START : IX2V_ERROR);

	XD2_ResponeJson(msg->cmd_id, ntohs(pMsgData->OP), ntohs(pMsgData->Session_ID), 1, jsonData);
	cJSON_Delete(jsonData);

	return ret;
}


/*********************************************************************************************
 ***********************************检查呼叫记录表是否更新**************************************
 ********************************************************************************************/
static char* Get_IX_ADD_ByPhoneAcc(const char* phoneAcc, char* ixAddr)
{
	char* ret = "";

    cJSON* where = cJSON_CreateObject();
	cJSON_AddStringToObject(where, IX2V_Phone_Acc, phoneAcc);


	cJSON* newitem = cJSON_CreateObject();
	cJSON_AddStringToObject(newitem, IX2V_IX_ADDR, "");

	cJSON* view = cJSON_CreateArray();
	cJSON_AddItemToArray(view, newitem);

	if(API_TB_SelectBySortByName(TB_NAME_TB_Sub_SIP_ACC, view, where, 0))
	{
		newitem = cJSON_GetArrayItem(view, 1);
		ret = GetEventItemString(newitem,  IX2V_IX_ADDR);
	}

	if(strlen(ret) <= 10 && ixAddr)
	{
		strcpy(ixAddr, ret);
	}

	cJSON_Delete(where);
	cJSON_Delete(view);

	return ixAddr;
}

static cJSON* Get_CallRecord_By_IX_ADDR(const char* ixAddr, char* lastCallId, int* flag)
{
	cJSON* retTb = NULL;
	int callIdExistIndex = 0;
	int viewSize;
	int updateFlag = 0;

    cJSON* where = cJSON_CreateObject();
	cJSON_AddStringToObject(where, IX2V_Call_Type, "IxMainCall");

	cJSON* view = cJSON_CreateArray();
	viewSize = API_TB_SelectBySortByName(TB_NAME_DSCallRecord, view, where, 0);
	if(viewSize)
	{
		cJSON* callRecord;
		char* callTargetIxAddr;
		char* callId = NULL;
		int i;
	
		//查找call id是否存在。
		cJSON_ArrayForEach(callRecord, view)
		{
			callId = cJSON_GetStringValue(cJSON_GetObjectItemCaseSensitive(callRecord, "Call_ID"));
			if(callId && lastCallId && !strcmp(callId, lastCallId))
			{
				break;
			}
			callIdExistIndex++;
		}

		//如果call id存在，删除call id以前的记录
		if(callIdExistIndex < viewSize)
		{
			for(i = 0; i <= callIdExistIndex; i++)
			{
				cJSON_DeleteItemFromArray(view, 0);
			}

			if(callIdExistIndex == viewSize-1)
			{
				updateFlag = 0;
			}
			else
			{
				updateFlag = 1;
			}
		}
		else
		{
			updateFlag = 2;
		}

		viewSize = cJSON_GetArraySize(view);
		if(viewSize)
		{
			retTb = API_TB_CopyNewTableByName(TB_NAME_DSCallRecord);
			API_TB_Delete(retTb, NULL);
			cJSON_ArrayForEach(callRecord, view)
			{
				callTargetIxAddr = cJSON_GetStringValue(cJSON_GetObjectItemCaseSensitive(cJSON_GetObjectItemCaseSensitive(callRecord, "Call_Target"), "Call_Tar_IXAddr"));
				if(callTargetIxAddr && strlen(callTargetIxAddr) >= 8 && ixAddr && strlen(ixAddr) >= 8)
				{
					if(!strncmp(callTargetIxAddr, ixAddr, 8))
					{
						API_TB_Add(retTb, callRecord);
					}
				}
			}
		}
	}

	if(flag)
	{
		*flag = updateFlag;
	}

	cJSON_Delete(where);
	cJSON_Delete(view);

	return retTb;
}


int XD2_CheckCallRecord(XD2_MSG* msg)
{
	int ret = 0;
	XD2_MSG_HEAD* pMsgData = (XD2_MSG_HEAD*)msg->data;
	char* jsonString = (char*)((msg->data) + sizeof(XD2_MSG_HEAD));
	cJSON* jsonData = cJSON_Parse(jsonString);
	char ixAddr[11];
	int updateFlag;

	char* phoneId = GetEventItemString(jsonData,  "PHONE_ID");
	char* phoneAcc = GetEventItemString(jsonData,  IX2V_Phone_Acc);
	char* target = GetEventItemString(jsonData,  "TARGET");
	char* server = GetEventItemString(jsonData,  "SERVER");
	char* lastCallId = GetEventItemString(jsonData,  "LAST_CALL_ID");
	cJSON* callRecordTb = Get_CallRecord_By_IX_ADDR(Get_IX_ADD_ByPhoneAcc(phoneAcc, ixAddr), lastCallId, &updateFlag);

	log_d("ixAddr=%s, updateFlag = %d, lastCallId=%s\n", ixAddr, updateFlag, lastCallId);

	//无需更新
	if(updateFlag == 0)
	{
		cJSON_AddStringToObject(jsonData, "RESULT", "UP_TO_DATE");
	}
	//呼叫记录有更新
	else
	{
		char sourceFile[200];
		char targetDir[200];
		MakeDir(TEMP_Folder);
		snprintf(sourceFile, 200, "%s/%s_%s.json", TEMP_Folder, phoneAcc, phoneId);
		snprintf(targetDir, 200, "%s/%s/%s", XD2_SEVERT_ROOT, API_Para_Read_String2(SIP_ACCOUNT), target);
		SetJsonToFile(sourceFile, callRecordTb);

		cJSON* callbackData = cJSON_CreateObject();
		cJSON_AddStringToObject(callbackData, "SERVER", server);
		cJSON_AddStringToObject(callbackData, "SOURCE", sourceFile);
		cJSON_AddStringToObject(callbackData, "TARGET", target);
		cJSON_AddNumberToObject(callbackData, "SESSION_ID", ntohs(pMsgData->Session_ID));
		//启动成功
		if(FTP_StartUpload(server, sourceFile, targetDir, callbackData,  XD2_UploadFileCallback))
		{
			ret = 1;
			cJSON_AddStringToObject(jsonData, "RESULT", updateFlag == 1 ? "UPDATE_PART_START" : "UPDATE_ALL_START");
		}
		else
		{
			cJSON_Delete(callbackData);
			cJSON_AddStringToObject(jsonData, "RESULT", "UPDATE_ERROR");
		}

	}
	cJSON_Delete(callRecordTb);

	XD2_ResponeJson(msg->cmd_id, ntohs(pMsgData->OP), ntohs(pMsgData->Session_ID), 1, jsonData);
	cJSON_Delete(jsonData);

	return ret;
}

/*********************************************************************************************
 ***********************************设置或者查询转呼状态**************************************
 ********************************************************************************************/
int XD2_SetDivertState(XD2_MSG* msg)
{
	int ret = 0;
	XD2_MSG_HEAD* pMsgData = (XD2_MSG_HEAD*)msg->data;
	char* jsonString = (char*)((msg->data) + sizeof(XD2_MSG_HEAD));
	cJSON* jsonData = cJSON_Parse(jsonString);
	char ixAddr[11];

	char* target = GetEventItemString(jsonData,  "TARGET");
	Get_IX_ADD_ByPhoneAcc(target, ixAddr);
	char* state = GetEventItemString(jsonData,  "STATE");
	cJSON* rspJson = cJSON_CreateObject();
	cJSON_AddStringToObject(rspJson, "SOURCE", "IX850S");
	cJSON_AddStringToObject(rspJson, "TARGET", target);

	//查询转呼状态
	if(!strcmp(state, "CHECK"))
	{
		if(judge_ixim_divert_on_by_addr(ixAddr))
		{
			char *sip_net=API_PublicInfo_Read_String(PB_SIP_CONNECT_NETWORK);
			if(sip_net==NULL || strcmp(sip_net,"NONE")==0||API_Para_Read_Int(SIP_ENABLE)==0)
			{
				cJSON_AddStringToObject(rspJson, "STATE", "OFF");
				cJSON_AddStringToObject(rspJson, "INFO", "SIP ISSUE");
			}
			else if(get_sip_dev_acc_from_tb(DivertAccType_IxIm, ixAddr, NULL, NULL) <0)
			{
				cJSON_AddStringToObject(rspJson, "STATE", "OFF");
				cJSON_AddStringToObject(rspJson, "INFO", "NO ACC");
			}
			else
			{
				cJSON_AddStringToObject(rspJson, "STATE", "ON");
				cJSON_AddStringToObject(rspJson, "INFO", "");
			}
		}
		else
		{
			cJSON_AddStringToObject(rspJson, "STATE", "OFF");
			cJSON_AddStringToObject(rspJson, "INFO", "");
		}
		cJSON_AddStringToObject(rspJson, "RESULT", "SUCCESS");
	}
	//设置ON
	else if(!strcmp(state, "ON"))
	{
		switch(SetIxImDivertOn(ixAddr))
		{
			case 1:
				cJSON_AddStringToObject(rspJson, "STATE", "ON");
				cJSON_AddStringToObject(rspJson, "RESULT", "SUCCESS");
				break;
			case 2:
				cJSON_AddStringToObject(rspJson, "STATE", "OFF");
				cJSON_AddStringToObject(rspJson, "INFO", "NO ACC");
				cJSON_AddStringToObject(rspJson, "RESULT", "ERROR");
				break;
			case 3:
				cJSON_AddStringToObject(rspJson, "STATE", "OFF");
				cJSON_AddStringToObject(rspJson, "INFO", "SIP ISSUE");
				cJSON_AddStringToObject(rspJson, "RESULT", "ERROR");
				break;
			
			default:
				cJSON_AddStringToObject(rspJson, "RESULT", "ERROR");
				cJSON_AddStringToObject(rspJson, "INFO", "");
				break;
		}
	}
	else if(!strcmp(state, "OFF"))
	{
		SetIxImDivertOff(ixAddr);
		cJSON_AddStringToObject(rspJson, "STATE", "OFF");
		cJSON_AddStringToObject(rspJson, "INFO", "");
		cJSON_AddStringToObject(rspJson, "RESULT", "SUCCESS");
	}

	XD2_ResponeJson(msg->cmd_id, ntohs(pMsgData->OP), ntohs(pMsgData->Session_ID), 1, rspJson);
	cJSON_Delete(rspJson);
	cJSON_Delete(jsonData);

	return ret;
}
