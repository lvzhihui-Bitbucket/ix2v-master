
#ifndef _obj_TCP_Client_H_
#define _obj_TCP_Client_H_

#include "tcp_Client_process.h"
#include "one_tiny_task.h"
typedef void (*TCP_RCV_DATA_CB)(ProxyRemoteHead packHead, char* data, int dat_len);
#define XD2_TCP_BUFF_MAX				1200

// client
typedef struct
{	
	pthread_t 				pid;			// 任务id
	int						taskRunFlag;
	int					fd;
	char				network[16];
	char				ip[16];
	int					port;
	char				id[32];
	char				pwd[16];
	short 				cmd_id;
	short 				beatHeartTimeing;
	int  				autoConnectEnable;
	TCP_RCV_DATA_CB		recDataCB;
} TCP_Client, *P_TCP_Client;



P_TCP_Client CreatOneTcpConenct(char* net_dev_name, char *ip_str, int port, char* id, char* pwd, int autoConnectEnable, TCP_RCV_DATA_CB callback);
int DeleteOneTcpConnect(P_TCP_Client tcpTask);
int API_TCP_Send(P_TCP_Client tcpTask, char*data, int len);

#endif

