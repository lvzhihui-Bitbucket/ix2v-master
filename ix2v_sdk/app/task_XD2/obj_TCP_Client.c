
#include <stdio.h>
#include <strings.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <sys/time.h>
#include <stdlib.h>
#include <memory.h>
#include <arpa/inet.h>
#include <arpa/nameser.h>
#include <netinet/in.h>
#include <net/if.h>
#include <netdb.h>
#include <poll.h>
#include <pthread.h>
#include <error.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <resolv.h>
#include "utility.h"

#include "obj_TCP_Client.h"
#include "elog.h"
#include "tcp_Client_process.h"
#include "obj_PublicInformation.h"

static void TCP_ClientTaskProcess( void* arg )
{
	P_TCP_Client pTcpClient = (P_TCP_Client)arg;
	
	int recv_len, num, timeCnt, id;
	ProxyRemoteHead packHead;
	char recv_data[XD2_TCP_BUFF_MAX];	
	timeCnt = 0;
	id = 0;

	dprintf("---------------------TCP_ClientTaskProcess init...\n");
	pTcpClient->cmd_id = 0;
	pTcpClient->beatHeartTimeing = 0;
	pTcpClient->taskRunFlag = 1;

	dprintf("---------------------TCP_ClientTaskProcess start...\n");
	thread_log_add("%s:[%s:%d]",__func__,pTcpClient->ip,pTcpClient->port);
    while(pTcpClient->taskRunFlag)
    {
		if(pTcpClient->fd)
		{
			recv_len = sizeof(ProxyRemoteHead);
			
			num = recv(pTcpClient->fd, (char*)&packHead, recv_len, MSG_DONTWAIT);
			if(num == recv_len && !memcmp(packHead.head, "VTEK", 4) && packHead.data_len > 0)
			{
				recv_len = packHead.data_len;
				num = recv(pTcpClient->fd, recv_data, recv_len, MSG_DONTWAIT);
				
				//dprintf("num=%d, cmd_type=0X%04x, cmd_id=%d, dev_type=%d, data_len=%d\n", num, packHead.cmd_type, packHead.cmd_id, packHead.dev_type, packHead.data_len);
				
				if(num != recv_len)
				{
					//���ݰ��д�����ս��ջ��������
					recv_len = XD2_TCP_BUFF_MAX;
					while(recv(pTcpClient->fd, recv_data, recv_len, MSG_DONTWAIT) > 0);
				}
				else 
				{
					switch(packHead.cmd_type)
					{
						case IX_DEVICE_REMOTE_NORMAL_BEAT_HEART_RSP:
							pTcpClient->beatHeartTimeing = 0;
							break;
					}

					if(pTcpClient->recDataCB)
					{
						(pTcpClient->recDataCB)(packHead, recv_data, recv_len);
					}
				}
			}
			//TCP连接错误
			else if(num == 0)
			{
				TCP_ClientLoginOrLogoutReq(pTcpClient->fd, pTcpClient->cmd_id++, 0, pTcpClient->id, pTcpClient->pwd, 1);
				Api_tcp_client_disconnect(&pTcpClient->fd);
				dprintf("TCP disconnect fd = %d\n", pTcpClient->fd);
			}
			
			usleep(IX_DEVICE_REMOTE_TASK_CYCLE_TIME_MS*1000);

			if(++timeCnt >= IX_DEVICE_REMOTE_SEND_BEAT_HEART_TIME_S*1000/IX_DEVICE_REMOTE_TASK_CYCLE_TIME_MS)
			{
				timeCnt = 0;
				TCP_ClientBeatHeart(pTcpClient->fd, pTcpClient->cmd_id++, ++id);
			}
			
			//35秒没收到服务器的心跳回复，断开连接
			if(++(pTcpClient->beatHeartTimeing) >= IX_DEVICE_REMOTE_SEND_RECV_HEART_TIME_S*1000/IX_DEVICE_REMOTE_TASK_CYCLE_TIME_MS)
			{
				TCP_ClientLoginOrLogoutReq(pTcpClient->fd, pTcpClient->cmd_id++, 0, pTcpClient->id, pTcpClient->pwd, 1);
				Api_tcp_client_disconnect(&pTcpClient->fd);
				dprintf("TCP disconnect fd = %d\n", pTcpClient->fd);
			}
		}
		else
		{
			if(pTcpClient == GetXD2TCPTask())
			{
				ReloadXD2ConnectPara();
			}

			if(pTcpClient->autoConnectEnable)
			{
				int connectState;
				if(!TCP_ReConnectToServer(pTcpClient))
				{
					connectState = 0;
				}
				else
				{
					connectState = 1;
					id = 0;
					timeCnt = 0;
					pTcpClient->beatHeartTimeing = 0;
				}
				if(pTcpClient == GetXD2TCPTask())
				{
					cJSON* state = CreateXD2ConnectState(connectState);
					API_PublicInfo_Write(PB_XD2_STATE, state);
					cJSON_Delete(state);
				}
				if(!connectState)
				{
					sleep(10);
				}
			}
			usleep(10*100);
		}
    }

	if(pTcpClient->fd > 0)
	{
		TCP_ClientLoginOrLogoutReq(pTcpClient->fd, pTcpClient->cmd_id++, 0, pTcpClient->id, pTcpClient->pwd, 1);
		Api_tcp_client_disconnect(&pTcpClient->fd);	
	}
	free(pTcpClient);
	pTcpClient = NULL;
	dprintf("------------------TCP_ClientTaskProcess end...\n");
	pthread_exit(pTcpClient);
}

int TCP_ReConnectToServer(P_TCP_Client pTcpClient)
{	
	int ret = 0;

	if(pTcpClient == NULL)
	{
		return ret;
	}

	if(!(pTcpClient->network[0]) || !(pTcpClient->ip[0]) || !(pTcpClient->id[0]) || !(pTcpClient->pwd[0]))
	{
		return ret;
	}

	pTcpClient->cmd_id = 0;

	if(pTcpClient->fd > 0)
	{
		dprintf("Disconnect XD2:fd = %d\n", pTcpClient->fd);
		TCP_ClientLoginOrLogoutReq(pTcpClient->fd, pTcpClient->cmd_id++, 0, pTcpClient->id, pTcpClient->pwd, 1);
		Api_tcp_client_disconnect(&pTcpClient->fd);
		pTcpClient->fd = 0;
	}

	pTcpClient->fd = Api_tcp_client_connect_to_server(pTcpClient->network, pTcpClient->ip, pTcpClient->port, pTcpClient->id, pTcpClient->pwd);

	if(pTcpClient->fd <= 0)
	{
		dprintf("Connect  error:%s [ip=%s, port=%d] [%s, %s]\n", pTcpClient->network, pTcpClient->ip, pTcpClient->port, pTcpClient->id, pTcpClient->pwd);		
	}
	else
	{
		dprintf("Connect  success:fd=%d, %s [ip=%s, port=%d] [%s, %s]\n", pTcpClient->fd, pTcpClient->network, pTcpClient->ip, pTcpClient->port, pTcpClient->id, pTcpClient->pwd);		
		ret = 1;
	}

	return ret;		
}

P_TCP_Client CreatOneTcpConenct(char* net_dev_name, char *ip_str, int port, char* id, char* pwd, int autoConnectEnable, TCP_RCV_DATA_CB callback)
{	
	P_TCP_Client pTcpClient = (P_TCP_Client)malloc(sizeof(TCP_Client));
	if(pTcpClient == NULL)
	{
		return NULL;
	}

	pTcpClient->fd = 0;
	pTcpClient->port = port;
	snprintf(pTcpClient->network, 16, "%s", net_dev_name);
	snprintf(pTcpClient->ip, 16, "%s", ip_str);
	snprintf(pTcpClient->id, 32, "%s", id);
	snprintf(pTcpClient->pwd, 16, "%s", pwd);
	pTcpClient->autoConnectEnable = autoConnectEnable;
	pTcpClient->recDataCB = callback;

	// initial thread
	if(pthread_create(&pTcpClient->pid, NULL,(void*)TCP_ClientTaskProcess, pTcpClient) != 0 )
	{
		free(pTcpClient);
		pTcpClient = NULL;
		dprintf( "CreatOneTcpConenct pthread_create error! \n" );
	}
	
	return pTcpClient;
}

int DeleteOneTcpConnect(P_TCP_Client pTcpClient)
{
	int pid = pTcpClient->pid;

	// 等待线程结束
	if(pTcpClient->taskRunFlag)
	{
		pTcpClient->taskRunFlag = 0;
		if(pid != pthread_self())
		{
			pthread_join(pid, NULL);
		}
	}
	
	return 0;
}

int API_TCP_Send(P_TCP_Client pTcpClient, char*data, int len)
{
	int ret = 0;
	int i;

	if(pTcpClient == NULL)
	{
		return ret;
	}
	
	dprintf("API_TCP_Send len = %d\n", len);

	for(i = 0; i < 3; i++)
	{
		ret = send(pTcpClient->fd, data, len, MSG_DONTWAIT);
		if(ret > 0)
		{
			break;
		}
		dprintf("API_TCP_Send Error %d times\n", i+1);
		usleep(20*1000);
	}

	return ret;
}

