/**
  ******************************************************************************
  * @file    task_XD2.h
  * @author  zeng
  * @version V1.0.0
  * @date    2012.08.15
  * @brief   This file contains the headers of the power task_3.
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2012 V-Tec</center></h2>
  ******************************************************************************
  */
#ifndef _task_XD2_H
#define _task_XD2_H

#define XD2_SEVERT_ROOT				          "/home/userota/RECORDS_ROOT/"
#define XD2_MSG_DATA_MAX_LEN					1000
#define XD2_RSP_DELAY()							usleep(1*1000)

#define XD2_CMD_FILE_UPLOAD_REQ					0x0401
#define XD2_CMD_FILE_UPLOAD_RSP					0x8401

#define XD2_CMD_SET_DIVERT_REQ					0x0402
#define XD2_CMD_SET_DIVERT_RSP					0x8402

#define XD2_CMD_GetCallRecord_REQ				0x0403
#define XD2_CMD_GetCallRecord_RSP				0x8403



#define XD2_CMD_REPORT_REQ						0x0501
#define XD2_CMD_REPORT_RSP						0x8501

#define XD2_REPORT_SUP_OB_UPLOAD				0x0001

typedef struct
{
	short cmd_id;
	short dataLen;
	char  data[XD2_MSG_DATA_MAX_LEN+1];
}XD2_MSG;

typedef struct
{
	unsigned short businessCode;    //没用，凑包用的
	unsigned short OP;
	unsigned short Session_ID;
	unsigned short RSP_ID;
	unsigned short Length;
	unsigned short CheckSUM;
}XD2_MSG_HEAD;

typedef struct
{
	unsigned short businessCode;    //没用，凑包用的
	unsigned short OP;
	unsigned short Session_ID;
	unsigned short RSP_ID;
	unsigned short Length;
	unsigned short CheckSUM;
	unsigned short SUB_OP;
}XD2_REPORT_HEAD;

/*******************************************************************************
                         Define task event flag
*******************************************************************************/

/*******************************************************************************
                  Define task vars, structures and macro
*******************************************************************************/
void vdp_XD2_task_init(void);
void API_XD2_MSG(short cmd_id, char* data, int len);

int XD2_Report(unsigned short SUB_OP, unsigned short Session_ID, char* data, int len);
void XD2_Respone(short cmd_id, unsigned short OP,unsigned short Session_ID, unsigned short RSP_ID, char* data, int len);

#endif
