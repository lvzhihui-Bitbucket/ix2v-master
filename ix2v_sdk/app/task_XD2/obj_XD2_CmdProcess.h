/**
  ******************************************************************************
  * @file    obj_XD2_CmdProcess.h
  * @author  zeng
  * @version V1.0.0
  * @date    2012.08.15
  * @brief   This file contains the headers of the power task_3.
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2012 V-Tec</center></h2>
  ******************************************************************************
  */
#ifndef _obj_XD2_CmdProcess_H
#define _obj_XD2_CmdProcess_H


/*******************************************************************************
                         Define task event flag
*******************************************************************************/
int XD2_UploadFile(XD2_MSG* msg);
int XD2_SetDivertState(XD2_MSG* msg);
int XD2_CheckCallRecord(XD2_MSG* msg);

/*******************************************************************************
                  Define task vars, structures and macro
*******************************************************************************/


#endif
