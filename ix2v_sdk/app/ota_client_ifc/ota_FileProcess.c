

#include "cJSON.h"
#include <dirent.h>
#include "utility.h"
#include "unix_socket.h"
#include "define_file.h"
#include "ota_FileProcess.h"
#include "task_Led.h"
#include "obj_IoInterface.h"
#include "obj_PublicInformation.h"
#include "elog_forcall.h"


#define UPGRADE_JSON											"upgrade.json"
#define UPGRADE_SH												"upgrade.sh"
#define UPGRADE_DELETE											"UPGRADE_DELETE"

#define UPGRADE_JSON_KEY_Verify									"Verify"
#define UPGRADE_JSON_KEY_UpgradeType							"UpgradeType"
#define UPGRADE_JSON_KEY_FirmwareReset							"FirmwareReset"
#define UPGRADE_JSON_KEY_FirmwareParser							"FirmwareParser"
#define UPGRADE_JSON_KEY_MCU_Firmware							"MCU_Firmware"

#define DL_JSON_KEY_Verify									"Verify"
#define DL_JSON_KEY_PacketName								"PacketName"
#define DL_JSON_KEY_DownloadType							"DownloadType"
#define DL_JSON_KEY_Size									"Size"
#define DL_JSON_KEY_Info									"Info"
#define DL_JSON_KEY_DateTime								"DateTime"

#define CMD_LINE_LONG_MAX							200

#define MCU_DOWNLOAD_STATE_READY						0
#define MCU_DOWNLOAD_STATE_GET_VER						1
#define MCU_DOWNLOAD_STATE_ERROR						2
#define MCU_DOWNLOAD_STATE_STOP							3
#define MCU_DOWNLOAD_STATE_INSTALLING					4
#define MCU_DOWNLOAD_STATE_INSTALL_OVER					5

static int mcuUpdateState;
static int mcuUpdateBlk_all;
static int mcuUpdateBlk_cur;
static int mcuUpdateMsgId;

static void fw_uart_upgrade_callback(void* pbuf,int size)
{
	unsigned char msg_id,blk_all,blk_cur;
	unsigned char* pdatbuf = (unsigned char*)pbuf;

	msg_id	= pdatbuf[0];
	blk_all = pdatbuf[1];
	blk_cur = pdatbuf[2];
	
	mcuUpdateBlk_all = blk_all;
	mcuUpdateBlk_cur = blk_cur;
	mcuUpdateMsgId = msg_id;

	switch(msg_id)
	{
		case 2:
		case 3:
		case 6:
		case 7:
		case 10:
		case 11:
			//����
			mcuUpdateState = MCU_DOWNLOAD_STATE_ERROR;
			break;
		case 16:
			//��װ���
			mcuUpdateState = MCU_DOWNLOAD_STATE_INSTALL_OVER;
			break;
		case 0:
			//��ȡ�汾
			mcuUpdateState = MCU_DOWNLOAD_STATE_GET_VER;
			break;
		case 1:
		case 4:
		case 5:
		case 8:
		case 9:
			//��װ��
			mcuUpdateState = MCU_DOWNLOAD_STATE_INSTALLING;
			break;
		case 12:
		case 13:
		case 14:
		case 15:
			//ֹͣ��װ
			mcuUpdateState = MCU_DOWNLOAD_STATE_STOP;
			break;
	}
}

int get_upgrade_file_dir(int file_length,char *file_dir)	//-1,pls insert sd card,-2,no space
{
	unsigned long long freesize;
	char* dir;

	if(GetDirFreeSizeInMByte(DISK_SDCARD))
	{
		MakeDir(SDCARD_DOWNLOAD_PATH);
	}

	freesize = GetDirFreeSizeInMByte(SDCARD_DOWNLOAD_PATH);
	freesize = (freesize * 1024*1024);
	if(file_length > freesize || freesize == 0)
	{
		if(file_length > NAND_DOWNLOAD_LIMIT)
		{
			return -1;
		}
		else
		{
			MakeDir(NAND_DOWNLOAD_PATH);
			freesize = GetDirFreeSizeInMByte(NAND_DOWNLOAD_PATH);
			freesize = (freesize * 1024*1024);
			if(file_length > freesize || freesize == 0)
			{
				return -1;
			}
			dir = NAND_DOWNLOAD_PATH;
		}
	}
	else
	{
		dir = SDCARD_DOWNLOAD_PATH;
	}
	
	if(file_dir)
	{
		strcpy(file_dir, dir);
	}

	return 0;
}

int PopenProcess(const char* cmd, char* readBuf[], int bufLen, int lineMax)
{
	#define Popen_LINE_LONG_MAX	1000
	
	FILE *pf;
	char buffer[Popen_LINE_LONG_MAX];
	int lineNum = 0;
	int i;

	pf = popen(cmd, "r");

	if(pf == NULL)
	{
		log_e("Unknow Error!!!!!!!!\n");
		return lineNum;
	}
	
	for(lineNum = 0; fgets(buffer, Popen_LINE_LONG_MAX, pf) != NULL; lineNum++)
	{
		for(i = 0; buffer[i] != 0 && i < Popen_LINE_LONG_MAX; i++)
		{
			if(buffer[i] == '\r' || buffer[i] == '\n')
			{
				buffer[i] = 0;
				break;
			}
		}

		if(readBuf != NULL)
		{
			if(lineNum < lineMax && readBuf[lineNum] != NULL)
			{
				strncpy(readBuf[lineNum], buffer, bufLen-1);
				readBuf[lineNum][bufLen-1] = 0;
			}
		}
	}

	pclose(pf);

	return lineNum;
}

int IfZipHasFile(const char* zip, const char* fileName)
{
	char cmd[CMD_LINE_LONG_MAX];
	char linestr[CMD_LINE_LONG_MAX];
	int ret = 0;
	char* pReadBuf[1];

	pReadBuf[0] = linestr;

	snprintf(cmd, CMD_LINE_LONG_MAX, "unzip -l \"%s\" | grep -w \"%s\" | awk -F ' ' '{print $NF}' | awk -F '/' '{print $NF}'", zip, fileName);

	ret = PopenProcess(cmd, pReadBuf, CMD_LINE_LONG_MAX, 1);
	
	return ret;
}

int UnZipFile(const char* zip, const char* fileName, const char* unZipPath, char* filePath)
{
	char cmd[CMD_LINE_LONG_MAX];
	char readBuf[CMD_LINE_LONG_MAX];
	char readBuf2[CMD_LINE_LONG_MAX];
	int fileNum = 0;
	char* pReadBuf[200];
	int i;
	
	char* pReadBuf2[1];
	
	pReadBuf2[0] = readBuf2;
	pReadBuf[0] = readBuf;

	
	fileNum = IfZipHasFile(zip, fileName);
	if(fileNum == 0)
	{
		log_e("%s has not %s\n", zip, fileName);
		return fileNum;
	}
	else if(fileNum > 200)
	{
		fileNum = 200;
	}
	
	for(i = 1; i < fileNum; i++)
	{
		pReadBuf[i] = malloc(CMD_LINE_LONG_MAX);
	}

	snprintf(cmd, CMD_LINE_LONG_MAX, "unzip -l %s | grep -w \"%s\" | awk -F ' ' '{print $NF}'", zip, fileName);

	PopenProcess(cmd, pReadBuf, CMD_LINE_LONG_MAX, fileNum);


	if(filePath != NULL)
	{
		
		snprintf(cmd, CMD_LINE_LONG_MAX, "echo %s | sed 's/\\//\\\\\\//g'", fileName);
		
		PopenProcess(cmd, pReadBuf2, CMD_LINE_LONG_MAX, 1);
		
		snprintf(cmd, CMD_LINE_LONG_MAX, "echo %s | sed \"s/%s", pReadBuf[0], pReadBuf2[0]);
		strcat(cmd, ".*$//g\"");
		
		PopenProcess(cmd, pReadBuf2, CMD_LINE_LONG_MAX, 1);
		
		strcpy(filePath, pReadBuf2[0]);
	}

	for(i = 0; i < fileNum; i++)
	{
		snprintf(cmd, CMD_LINE_LONG_MAX, "unzip -oq %s %s -d %s", zip, pReadBuf[i], unZipPath);
		system(cmd);
		if(i >= 1)
		{
			free(pReadBuf[i]);
		}
	}
	
	return fileNum;
}


int FwUprade_Installing(char *file_path)
{
	int place;
	int ret;
	char tempFile[200];
	
	if(strstr(file_path, DISK_SDCARD) != NULL)
	{
		place = 0;
	}
	else
	{
		place = 1;
	}
	//IX2V test ret = start_updatefile_and_reboot_forsdcard(place, file_path);
	remove(file_path);
	snprintf(tempFile, 200, "%s.txt", file_path);
	remove(tempFile);
	
	return ret; 
}


int FwUpradeIXDeviceInstalling(char *file_path)
{
	int ret;
	char tempFile[200];
	
	if(IfZipHasFile(file_path, UPGRADE_SH_FILE))
	{
		snprintf(tempFile, 200, "%s.txt", file_path);
		remove(tempFile);
		ret = 0;
	}
	else
	{
		ret = FwUprade_Installing(file_path);
	}
	
	return ret; 
}

static int GetJsonNumber(const cJSON* json, const char* key)
{
	const cJSON* subJson = NULL;
	
	subJson = cJSON_GetObjectItemCaseSensitive( json, key);
	
	if (cJSON_IsNumber(subJson))
	{
		return subJson->valuedouble;
	}
	else
	{
		return 0;
	}
}

static int VarifyInstallPacket(cJSON* verifys)
{
	int 	verifyNum;
	int		ret = 1;
	int 	i;
	cJSON* 	temp;
	cJSON*	verify;
	char	iodata[20];

	if(verifys != NULL)
	{
		verifyNum = cJSON_GetArraySize(verifys);
		for(i = 0; i < verifyNum; i++)
		{
			verify = cJSON_GetArrayItem(verifys, i);
			temp = cJSON_GetArrayItem(verify, 0);
			switch(atoi(cJSON_GetStringValue(temp)))
			{
				case 0:
					API_Para_Read_String(DEV_MODEL, iodata);
					break;
				case 1:
					API_Para_Read_String(DEV_NAME, iodata);
					break;
			}
			temp = cJSON_GetArrayItem(verify, 1);
			if(strcmp(iodata, cJSON_GetStringValue(temp)))
			{
				log_i("verify error!!! verify =%s, iodata = %s.\n", cJSON_GetStringValue(temp), iodata);
				ret = 0;
			}
		}
	}

	return ret;
}


//返回-2）空间不足不允许更新, -1）时间相等不允许更新, 0）类型校验不通过，1）校验通过
int CheckDownloadIsAllowed(const char* txtFile, int ifCheckTime, OtaDlTxt_t* txtInfo)
{
	OtaDlTxt_t tempInfo;
	char	iodata[40] = {0};
	char*	dateTime;
	char	temp1[20];
	char	temp2[20];
	cJSON*	json;
	cJSON*	verifys;
	int 	ret;

	json = GetJsonFromFile(txtFile);
	if(json != NULL)
	{
		//У����Ϣ��
		verifys = cJSON_GetObjectItemCaseSensitive(json, DL_JSON_KEY_Verify);			
		ret = VarifyInstallPacket(verifys);
		if(ret)
		{
			tempInfo.fileLength = 0;
			GetJsonDataPro(json, DL_JSON_KEY_DownloadType, &tempInfo.dlType);
			GetJsonDataPro(json, DL_JSON_KEY_Size, &tempInfo.fileLength);
			GetJsonDataPro(json, DL_JSON_KEY_Info, tempInfo.info);
			GetJsonDataPro(json, DL_JSON_KEY_PacketName, tempInfo.packetName);
			if(txtInfo)
			{
				*txtInfo = tempInfo;
			}
		}

		//У��ʱ��
		if(ret && ifCheckTime)
		{
			snprintf(iodata, 20, "20%s", API_PublicInfo_Read_String(PB_FW_VER) + 7);
			dateTime = cJSON_GetStringValue(cJSON_GetObjectItemCaseSensitive(json, DL_JSON_KEY_DateTime));
			dprintf("iodata=%s, dateTime=%s\n", iodata, dateTime);
			//时间相等不允许更新
			if(dateTime && !strcmp(iodata, dateTime))
			{
				ret = -1;
				dprintf("The firmware is the latest and does not need to be updated.\n");
			}
		}
		//已经校验通过，继续校验空间大小
		if(ret == 1)
		{
			long long freeSize = GetDirFreeSizeInMByte(DISK_SDCARD);
			freeSize = (freeSize * 1024*1024);
			//SD卡空间足够
			if((freeSize > tempInfo.fileLength) && (freeSize > 0))
			{
				MakeDir(SDCARD_DOWNLOAD_PATH);
			}
			//判断nand1-2空间
			else 
			{
				MakeDir(NAND_DOWNLOAD_PATH);
				freeSize = GetDirFreeSizeInMByte(NAND_DOWNLOAD_PATH);
				freeSize = (freeSize * 1024*1024);
				if(freeSize < tempInfo.fileLength)
				{
					dprintf("CheckDownload No space: free size is %d byte, file size is %d byte\n", freeSize, tempInfo.fileLength);
					ret = -2;
				}
			}
		}
		else
		{
			dprintf("Varify install packet error.\n");
		}
		cJSON_Delete(json);
	}
	else
	{
		ret = 0;
		dprintf("%s is not json file.\n", txtFile);
	}
	return ret;
}

int InstallDownloadZip(int downloadType, char *file_path, int* rebootFlag)
{
	int ret = 1;
	switch(downloadType)
	{
		case 1:
		case 2:
		case 3:
		case 4:
		case 8:
			API_LedDisplay_FwUpdate();
			ret = InstallFirmware(file_path);
			API_LedDisplay_FwUpdateFinish();
			break;
		
		case 5:
			ret = InstallMcuCode(file_path);
			break;
		
		case 6:
			API_LedDisplay_FwUpdate();
			ret = InstallRes(file_path);
			API_LedDisplay_FwUpdateFinish();
			break;
		case 7:
			API_LedDisplay_FwUpdate();
			ret = InstallCustomerized(file_path);
			API_LedDisplay_FwUpdateFinish();
			break;
		case 9:
			break;
	}
	
	if(ret == 0)
	{
		ret = 1;
		*rebootFlag = 1;
	}
	else
	{
		*rebootFlag = 0;
	}
	
	
	return ret; 
}

int InstallRes(char *file_path)
{
	char cmd_buff[200];
	
	sprintf(cmd_buff,"unzip -o %s -d %s", file_path, PublicResFileDir);
	system(cmd_buff);
	
	//IX2V test PubResReloadToActFile();

	DeleteFileProcess(NULL, file_path);
	sprintf(cmd_buff,"%s.txt", file_path);
	DeleteFileProcess(NULL, cmd_buff);
	
	return 1; 
}

int InstallCustomerized(char *file_path)
{
	char cmd_buff[200];
	char dirName[200];
	char* pReadBuf[1];
	pReadBuf[0] = dirName;

	//ɾ������Ŀ¼
	DeleteFileProcess(CustomerFileDir, "*");
	
	//��ȡ����Ŀ¼���ϼ�Ŀ¼
	snprintf(cmd_buff, 200, "dirname %s", CustomerFileDir);
	PopenProcess(cmd_buff, pReadBuf, 200, 1);

	//��ѹ������Ŀ¼���ϼ�Ŀ¼��
	snprintf(cmd_buff, 200, "unzip -o %s -d %s", file_path, dirName);
	system(cmd_buff);
	
	dprintf("%s\n", cmd_buff);

	//IX2V test ReloadCustomerFile();
	
	DeleteFileProcess(NULL, file_path);
	sprintf(cmd_buff,"%s.txt", file_path);
	DeleteFileProcess(NULL, cmd_buff);
	
	return 1; 
}

int InstallFirmware(char *file_path)
{
	char	cmd[CMD_LINE_LONG_MAX];
	char	srcDir[CMD_LINE_LONG_MAX];
	char	Path[CMD_LINE_LONG_MAX];
	int 	fileNum;
	int 	i;
	cJSON*	upgradeParse = NULL;
	cJSON*	verifys;
	cJSON*	subJson;
	cJSON*	srcFile;
	cJSON*	tagFile;
	int 	verifyNum;
	int		upgradeType;
	int		ret = 1;
	
	char* pReadBuf[1];

	if(!IfZipHasFile(file_path, UPGRADE_JSON))
	{
		log_i("Have no %s.\n", UPGRADE_JSON);
		ret = -1;
		goto InstallFirmwareError;
	}

	//��ȡ�̼�Ŀ¼���ϼ�Ŀ¼
	snprintf(cmd, CMD_LINE_LONG_MAX, "dirname %s", file_path);
	pReadBuf[0] = Path;
	PopenProcess(cmd, pReadBuf, CMD_LINE_LONG_MAX, 1);

	//��ѹUPGRADE_JSON�ļ�����
	UnZipFile(file_path, UPGRADE_JSON, Path, srcDir);

	snprintf(cmd, CMD_LINE_LONG_MAX, "chmod 777 -R %s/%s/%s", Path, srcDir, UPGRADE_JSON);
	system(cmd);

	snprintf(cmd, CMD_LINE_LONG_MAX, "%s/%s/%s", Path, srcDir, UPGRADE_JSON);
	
	//����UPGRADE_JSON�ļ�
	upgradeParse = GetJsonFromFile(cmd);
	//û��UPGRADE_JSON�ļ�
	if(upgradeParse == NULL)
	{
		log_i("unzip %s error.\n", UPGRADE_JSON);
		ret = -2;
		goto InstallFirmwareError;
	}

	//ɾ��UPGRADE_JSON�ļ�����ʱ�ļ���
	snprintf(cmd, CMD_LINE_LONG_MAX, "echo %s | awk -F '/' '{print $1}'", srcDir);
	pReadBuf[0] = srcDir;
	PopenProcess(cmd, pReadBuf, CMD_LINE_LONG_MAX, 1);
	DeleteFileProcess(Path, srcDir);

	
	verifys = cJSON_GetObjectItemCaseSensitive(upgradeParse, UPGRADE_JSON_KEY_Verify);
	if(!VarifyInstallPacket(verifys))
	{
		log_i("VarifyInstallPacket verify error!!!\n");
		ret = -3;
		goto InstallFirmwareError;
	}

	subJson = cJSON_GetObjectItemCaseSensitive(upgradeParse, UPGRADE_JSON_KEY_UpgradeType);

	if(cJSON_IsNumber(subJson) )
	{
		upgradeType = subJson->valuedouble;
	}
	else
	{
		upgradeType = -1;
	}
	
	switch(upgradeType)
	{
		//��ͨ���·�ʽ
		case 0:
			subJson = cJSON_GetObjectItemCaseSensitive(upgradeParse, UPGRADE_JSON_KEY_FirmwareParser);
			fileNum = cJSON_GetArraySize(subJson);
			for(i = 0; i < fileNum; i++)
			{
				srcFile = cJSON_GetArrayItem(cJSON_GetArrayItem(subJson, i), 0);
				tagFile = cJSON_GetArrayItem(cJSON_GetArrayItem(subJson, i), 1);
				
				DeleteFileProcess(NULL, cJSON_GetStringValue(tagFile));
				if(UnZipFile(file_path, cJSON_GetStringValue(srcFile), Path, srcDir))
				{
					snprintf(cmd, CMD_LINE_LONG_MAX, "chmod 777 -R %s/%s/%s", Path, srcDir, cJSON_GetStringValue(srcFile));
					system(cmd);
					snprintf(cmd, CMD_LINE_LONG_MAX, "mv %s/%s/%s %s", Path, srcDir, cJSON_GetStringValue(srcFile), cJSON_GetStringValue(tagFile));
					system(cmd);
				}
			}
			
			//ɾ����ѹ�����ʱ�ļ�
			snprintf(cmd, CMD_LINE_LONG_MAX, "echo %s | awk -F '/' '{print $1}'", srcDir);
			pReadBuf[0] = srcDir;
			PopenProcess(cmd, pReadBuf, CMD_LINE_LONG_MAX, 1);
			DeleteFileProcess(Path, srcDir);

			if(strstr(file_path, SdUpgradePath) == NULL)
			{
				DeleteFileProcess(NULL, file_path);
				sprintf(cmd,"%s.txt", file_path);
				DeleteFileProcess(NULL, cmd);
			}
			if(ret == 1)
			{
				subJson = cJSON_GetObjectItemCaseSensitive(upgradeParse, UPGRADE_JSON_KEY_FirmwareReset);
				if(cJSON_IsNumber(subJson) && subJson->valuedouble)
				{
					ret = 0;
				}
			}
			break;
			
		//IAP���·�ʽ
		case 1:
			if(!IfZipHasFile(file_path, UPGRADE_SH))
			{
				log_i("Have no %s.\n", UPGRADE_SH);
				ret = -4;
				goto InstallFirmwareError;
			}
			else
			{
				if(GetDirFreeSizeInMByte(DISK_SDCARD))
				{
					MakeDir(SDCARD_DOWNLOAD_PATH);
					snprintf(cmd, CMD_LINE_LONG_MAX, "cp %s %s", file_path, SDCARD_DOWNLOAD_PATH);
				}
				else
				{
					MakeDir(NAND_DOWNLOAD_PATH);
					snprintf(cmd, CMD_LINE_LONG_MAX, "mv %s %s", file_path, NAND_DOWNLOAD_PATH);
				}
				system(cmd);
				ret = 0;
			}
			break;
			
		default:
			ret = -5;
			goto InstallFirmwareError;
			break;
	}
	
	InstallFirmwareError:
	cJSON_Delete(upgradeParse);

	if(ret == 0)
	{
		char fileName[100];
		API_Para_Write_String(LastUpdateCode, GetFileName(file_path, fileName));
		API_Para_Write_String(LastUpdateTime, GetCurrentTime(fileName, 100, "%Y-%m-%d %H:%M"));
		API_IOSaveImmediately();
	}
	
	return ret;
}


static void MCU_InstallDisplay(int msgId, int blk_all, int blk_cur)
{
	static int saveMsgId, saveBlk_all, saveBlk_cur;
	
	const char* FW_UART_CB_DISPLAY_STRING[] = 
	{
		"Read version...",
		"Read version ok!",
		"Read version err!",
		"Read version timeout!",
		"Upgrade start...",
		"Upgrade start ok!",
		"Upgrade start err!",
		"Upgrade start timeout!",
		"Upgrade data ...",
		"Upgrade data ok!",
		"Upgrade data err!",
		"Upgrade data timeout!",
		"Upgrade stop ...",
		"Upgrade stop ok!",
		"Upgrade stop err!",
		"Upgrade stop timeout!",
		"Upgrade successful!",
	};

	char disp[100];

	if(saveMsgId != msgId || saveBlk_all != blk_all || saveBlk_cur != blk_cur)
	{
		saveMsgId = msgId;
		saveBlk_all = blk_all;
		saveBlk_cur = blk_cur;

		if( blk_all )
		{
			snprintf(disp, 100, "%s[%d%%]            ", FW_UART_CB_DISPLAY_STRING[msgId],(blk_cur+1)*100/blk_all);
		}
		else
		{
			snprintf(disp, 100, "%s                  ", FW_UART_CB_DISPLAY_STRING[msgId]);
		}
		log_i(disp);
	}
}

int InstallMcuCode(char *file_path)
{
	char	cmd[CMD_LINE_LONG_MAX];
	char	srcDir[CMD_LINE_LONG_MAX];
	char	Path[CMD_LINE_LONG_MAX];
	char*	jsonString;
	char*	mcuBin = NULL;
	cJSON*	upgradeParse = NULL;
	cJSON*	verifys;
	cJSON*	srcFile;
	cJSON*	subJson;
	int		ret = 1;
	
	char* pReadBuf[1];
	
	if(!IfZipHasFile(file_path, UPGRADE_JSON))
	{
		log_i("Have no %s.\n", UPGRADE_JSON);
		ret = -1;
		goto InstallFirmwareError;
	}

	//��ȡ�̼�Ŀ¼���ϼ�Ŀ¼
	snprintf(cmd, CMD_LINE_LONG_MAX, "dirname %s", file_path);
	pReadBuf[0] = Path;
	PopenProcess(cmd, pReadBuf, CMD_LINE_LONG_MAX, 1);

	//��ѹUPGRADE_JSON�ļ�����
	UnZipFile(file_path, UPGRADE_JSON, Path, srcDir);

	snprintf(cmd, CMD_LINE_LONG_MAX, "chmod 777 -R %s/%s/%s", Path, srcDir, UPGRADE_JSON);
	system(cmd);

	snprintf(cmd, CMD_LINE_LONG_MAX, "%s/%s/%s", Path, srcDir, UPGRADE_JSON);
	
	//����UPGRADE_JSON�ļ�
	upgradeParse = GetJsonFromFile(cmd);

	//û��UPGRADE_JSON�ļ�
	if(upgradeParse == NULL)
	{
		log_i("unzip %s error.\n", UPGRADE_JSON);
		ret = -2;
		goto InstallFirmwareError;
	}

	//ɾ��UPGRADE_JSON�ļ�����ʱ�ļ���
	snprintf(cmd, CMD_LINE_LONG_MAX, "echo %s | awk -F '/' '{print $1}'", srcDir);
	pReadBuf[0] = srcDir;
	PopenProcess(cmd, pReadBuf, CMD_LINE_LONG_MAX, 1);
	DeleteFileProcess(Path, srcDir);
	
	verifys = cJSON_GetObjectItemCaseSensitive(upgradeParse, UPGRADE_JSON_KEY_Verify);
	if(!VarifyInstallPacket(verifys))
	{
		log_i("--------------------------------------verify error!!!\n");
		ret = -3;
		goto InstallFirmwareError;
	}

	srcFile = cJSON_GetObjectItemCaseSensitive(upgradeParse, UPGRADE_JSON_KEY_MCU_Firmware);
	if(srcFile != NULL)
	{
		int tempCnt = 0;
		
		if(UnZipFile(file_path, cJSON_GetStringValue(srcFile), Path, srcDir))
		{
			snprintf(cmd, CMD_LINE_LONG_MAX, "chmod 777 -R %s/%s/%s", Path, srcDir, cJSON_GetStringValue(srcFile));
			system(cmd);
			
			snprintf(cmd, CMD_LINE_LONG_MAX, "%s/%s/%s", Path, srcDir, cJSON_GetStringValue(srcFile));
			
			api_fw_uart_upgrade_stop();
			mcuUpdateState = MCU_DOWNLOAD_STATE_READY;
			if(api_fw_uart_upgrade_start_file(cmd, 0, fw_uart_upgrade_callback) == 0)
			{
				for(tempCnt = 0; tempCnt < 90; tempCnt++)
				{
					MCU_InstallDisplay(mcuUpdateMsgId, mcuUpdateBlk_all, mcuUpdateBlk_cur);
					
					usleep(1000*1000);

					if(mcuUpdateState == MCU_DOWNLOAD_STATE_INSTALL_OVER)
					{
						//��װ���
						break;
					}

					if(mcuUpdateState != MCU_DOWNLOAD_STATE_INSTALLING && tempCnt >= 10)
					{
						//10��ʱ�仹û��ʼ��װ
						break;
					}

					if(mcuUpdateState == MCU_DOWNLOAD_STATE_ERROR || mcuUpdateState == MCU_DOWNLOAD_STATE_STOP)
					{
						//ֹͣ��װ���ߴ���
						break;
					}
				}
			}
			api_fw_uart_upgrade_stop();
			if(mcuUpdateState != MCU_DOWNLOAD_STATE_INSTALL_OVER)
			{
				ret = -4;
				log_i("--------------------------------------install error %d!!!\n", mcuUpdateState);
			}
		}
	}
	
	//ɾ����ѹ�����ʱ�ļ�
	snprintf(cmd, CMD_LINE_LONG_MAX, "echo %s | awk -F '/' '{print $1}'", srcDir);
	pReadBuf[0] = srcDir;
	PopenProcess(cmd, pReadBuf, CMD_LINE_LONG_MAX, 1);
	DeleteFileProcess(Path, srcDir);
	
	if(ret == 1)
	{
		subJson = cJSON_GetObjectItemCaseSensitive(upgradeParse, UPGRADE_JSON_KEY_FirmwareReset);
		if(cJSON_IsNumber(subJson) && subJson->valuedouble)
		{
			ret = 0;
		}
	}
	
	InstallFirmwareError:
	cJSON_Delete(upgradeParse);

	if(strstr(file_path, SdUpgradePath) == NULL)
	{
		DeleteFileProcess(NULL, file_path);
		sprintf(cmd,"%s.txt", file_path);
		DeleteFileProcess(NULL, cmd);
	}
	
	return ret;
}


