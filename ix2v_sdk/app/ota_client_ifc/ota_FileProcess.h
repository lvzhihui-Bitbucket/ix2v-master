
#ifndef _ota_FileProcess_h_
#define _ota_FileProcess_h_

#define NAND_DOWNLOAD_LIMIT		25000000
#define UPGRADE_SH_FILE			"upgrade.sh"
#define OTA_DL_STR_LEN			20

typedef struct
{	
	int			dlType;
	int 		fileLength;
	char		info[OTA_DL_STR_LEN];
	char		packetName[OTA_DL_STR_LEN];
} OtaDlTxt_t;


int get_upgrade_file_dir(int file_length,char *file_dir);	//-1,pls insert sd card,-2,no space

int CheckDownloadIsAllowed(const char* txtFile, int ifCheckTime, OtaDlTxt_t* txtInfo);

#endif




