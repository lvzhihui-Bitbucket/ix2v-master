
#include "client_interface.h"
#include "unix_socket.h"

//#define DOWNLOAD_PATH	"/home/lzh/workspace"

unix_socket_t		comm_service_socket;

void otac_udp_data_process( char* pbuf, int len );

void comm_service_socket_send_data( char* pbuf, int len )
{
	unix_socket_send_data(&comm_service_socket, pbuf, len);
}

int init_comm_service( void )
{
	init_unix_socket(&comm_service_socket,0,OTAC_IF_UNIX_SOCKET_FILE,otac_udp_data_process);		// �ͻ���
	create_unix_socket_create(&comm_service_socket);
	return 0;
}

int deinit_comm_service(void)
{
	deinit_unix_socket(&comm_service_socket);
	return 0;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// api interface
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
int api_ifc_set_connect_server( char* server_ip_str )
{
	otac_if_request_t	if_req;
	memset( (char*)&if_req, 0, sizeof(otac_if_request_t) );	
	if_req.cmd = OTAC_IF_SET_CONNECT_SERVER;
	strcpy( if_req.req.server_connect.server_str, server_ip_str );	
	comm_service_socket_send_data( (char*)&if_req,sizeof(otac_if_request_t) );
	return 0;
}

int api_ifc_set_disconnect_server( void )
{
	otac_if_request_t	if_req;
	memset( (char*)&if_req, 0, sizeof(otac_if_request_t) );	
	if_req.cmd = OTAC_IF_SET_DISCONNECT_SERVER;	
	comm_service_socket_send_data( (char*)&if_req,sizeof(otac_if_request_t) );
	return 0;	
}

int api_ifc_get_server_status( void )
{
	otac_if_request_t	if_req;
	memset( (char*)&if_req, 0, sizeof(otac_if_request_t) );	
	if_req.cmd = OTAC_IF_GET_SERVER_DISCONNECTED;	
	comm_service_socket_send_data( (char*)&if_req,sizeof(otac_if_request_t) );
	return 0;	
}

int api_ifc_set_download_start( char *file_dir,char* file_code, int file_type, unsigned char* device_type, unsigned char* device_ver, unsigned char* device_id )
{
	otac_if_request_t	if_req;
	if_req.cmd = OTAC_IF_SET_DOWNLOAD_START;
	if_req.req.file_download.file_type = file_type;
	memcpy( if_req.req.file_download.file_code, file_code, 6);	
	strcpy( if_req.req.file_download.device_type, device_type );	
	strcpy( if_req.req.file_download.device_ver, device_ver );	
	strcpy( if_req.req.file_download.device_id, device_id );
	strcpy( if_req.req.file_download.file_dir, file_dir);
	comm_service_socket_send_data( (char*)&if_req,sizeof(otac_if_request_t));
	return 0;
}

int api_ifc_set_download_stop( void )
{
	otac_if_request_t	if_req;
	memset( (char*)&if_req, 0, sizeof(otac_if_request_t) );	
	if_req.cmd = OTAC_IF_SET_DOWNLOAD_STOP;
	comm_service_socket_send_data( (char*)&if_req,sizeof(otac_if_request_t) );
	return 0;
}

int api_ifc_get_download_result( void )
{
	otac_if_request_t	if_req;
	memset( (char*)&if_req, 0, sizeof(otac_if_request_t) );	
	if_req.cmd = OTAC_IF_GET_DOWNLOAD_RESULT;
	comm_service_socket_send_data( (char*)&if_req,sizeof(otac_if_request_t) );
	return 0;
}


