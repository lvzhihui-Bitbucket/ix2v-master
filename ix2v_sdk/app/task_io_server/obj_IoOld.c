/**
  ******************************************************************************
  * @file    obj_IoOld.c
  * @author  czb
  * @version V00.01.00
  * @date    2021.12.21
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2021 V-Tec</center></h2>
  ******************************************************************************
  */ 

#include "cJSON.h"
#include "obj_IoInterface.h"
#include "utility.h"
#include "vtk_udp_stack_io_server.h"
#include "define_file.h"
#include "obj_PublicInformation.h"

static cJSON* oldParaValue = NULL;

int SupportOldParaInit(void)
{
    char filePath[500];

    snprintf(filePath, 500, "%s/%s%s", IO_PARA_Path, MY_DEV_NAME, IO_OLD_ID_TO_VALUE_FILE_NAME);
    if(oldParaValue)
    {
        cJSON_Delete(oldParaValue);
        oldParaValue = NULL;
    }

    if(oldParaValue == NULL)
    {
        oldParaValue = GetJsonFromFile(filePath);
    }

    if(oldParaValue == NULL)
    {
        oldParaValue = cJSON_CreateObject();
    }

    return 1;
}

static const char* GetOldParaDefaultValue(char *paraId)
{
    char* ret;

    ret = cJSON_GetStringValue(cJSON_GetObjectItemCaseSensitive(oldParaValue, paraId));
    if(ret == NULL)
    {
        ret = "-";
    }

    return ret;
}

//0--读失败，1--读成功
static int OldParaBeRead(char *paraId, char *readData)
{
    char *paraName;
    int ret = 1;

    if(paraId == NULL)
    {
        return 0;
    }

    switch(atoi(paraId))
    {
        //特殊参数特殊处理
        case 0:
            ret = 0;
            break;
        
        //不特殊处理的参数，统一处理
        default:
            paraName = GetOldParaDefaultValue(paraId);
            //默认值空
            if(paraName[0] == '-')
            {
                strcpy(readData, paraName);
            }
            else
            {
                cJSON* result = API_Para_Read_Public(paraName);
                if(result)
                {
                    switch (result->type & 0xFF)
                    {
                        case cJSON_False:
                            sprintf(readData, "%d", 0);
                            break;

                        case cJSON_True:
                            sprintf(readData, "%d", 1);
                            break;
                
                        case cJSON_Number:
                            sprintf(readData, "%.0f", result->valuedouble);
                            break;

                        case cJSON_String:
                            sprintf(readData, "%s", result->valuestring);
                            break;

                        case cJSON_Array:
                        case cJSON_Object:
                            cJSON_PrintPreallocated(result, readData, IO_CMD_DATA_LEN, 0);
                            break;

                        default:
                            ret = 0;
                            break;
                    }
                }
                //新参数名不存在，则表中的值为参数值
                else
                {
                    strcpy(readData, paraName);
                }
            }
            break;
    }

	return ret;
}

//0--写失败，1--写成功
static int OldParaBeWrite(char *paraId, char *writeData)
{
    cJSON* writeJson;
    char *paraName;
    int ret = 1;

    if(paraId == NULL)
    {
        return 0;
    }

    switch(atoi(paraId))
    {
        //特殊参数特殊处理
        case 0:
            ret = 0;
            break;
        
        //不特殊处理的参数，统一处理
        default:
            paraName = GetOldParaDefaultValue(paraId);
            //默认值空
            if(paraName[0] == '-')
            {
                strcpy(writeData, paraName);
                ret = 0;
            }
            else
            {
                int type = API_Para_Read_Type(paraName);
                if(type)
                {
                    switch (type & 0xFF)
                    {
                        case cJSON_False:
                        case cJSON_True:
                        case cJSON_Number:
                            API_Para_Write_Int(paraName, atoi(writeData));
                            break;

                        case cJSON_String:
                            API_Para_Write_String(paraName, writeData);
                            break;
                        default:
                            ret = 0;
                            break;
                    }
                }
                //新参数名不存在，则表中的值为参数值
                else
                {
                    strcpy(writeData, paraName);
                    ret = 0;
                }
            }
            break;
    }

	return ret;
}

//0--写失败，1--写成功
static int OldParaBeDefault(char *paraId, char *readData)
{
    cJSON* writeJson;
    char *paraName;
    int ret = 1;

    if(paraId == NULL)
    {
        return 0;
    }

    switch(atoi(paraId))
    {
        //特殊参数特殊处理
        case 0:
            ret = 0;
            break;
        
        //不特殊处理的参数，统一处理
        default:
            paraName = GetOldParaDefaultValue(paraId);
            //默认值空
            if(paraName[0] == '-')
            {
                strcpy(readData, paraName);
            }
            else
            {
                if(API_RestoreDefault(paraName))
                {
                    strcpy(readData, paraName);
                }
                //新参数名不存在，则表中的值为参数值
                else
                {
                    OldParaBeRead(paraId, readData);
                }
            }
            break;
    }

	return ret;
}


int API_OldPara_ReomteResponse(int ip, int id, int cmd, UDP_IO_CMD_t * pData)
{
	UDP_IO_CMD_t  sData = {0};
	int len;
    char paraId[100] = {0};
    char value[IO_CMD_DATA_LEN+5] = {0};
    
	sData.msg_type = (cmd + 0x80)&0xFF;
	char *req_str=strstr(pData->realData,"<ID=");
	char *rsp_str=sData.realData;
	rsp_str[0]=0;
	while(req_str=strstr(req_str,"<ID="))
	{
		sscanf(req_str, "<ID=%[^,>]%*[,>]Value=%[^>]", paraId, value);

	    switch (cmd)
	    {
	        case UDP_IO_SERVER_CMD_R:
	            sData.result = !OldParaBeRead(paraId, value);
	            break;

	        case UDP_IO_SERVER_CMD_W:
	            sData.result = !OldParaBeWrite(paraId, value);
	            break;

	        case UDP_IO_SERVER_CMD_ONE_DEFAULT:
	            sData.result = !OldParaBeDefault(paraId, value);
	            break;

	        default:
	        	sData.result = 0;
	            break;
	    }

	    snprintf(rsp_str+strlen(rsp_str), IO_CMD_DATA_LEN, "<ID=%s,Value=%s>", paraId, value);
		req_str+=strlen("<ID=");
	}

	len = sizeof(sData)-IO_CMD_DATA_LEN+strlen(sData.realData)+1;
    dprintf("!!!!!!!!!!! sData.realData[%s]\n",sData.realData);

	api_udp_io_server_send_rsp(ip, cmd + 0x80, id, (const char*)&sData, len);
	return 0;
}

int API_OldPara_ReomteRequest(int targetIp, int cmd, char *paraId, char *pData)
{
	UDP_IO_CMD_t  	sData = {0};
	UDP_IO_CMD_t  	rData = {0};
	unsigned int 	rlen;
	unsigned int	slen;
    char id[IO_CMD_DATA_LEN] = {0};
	
	sData.msg_type 			= cmd & 0xFF;
	sData.device_address 	= 0xFFFFFFFF;

    if(pData[0] == 0)
    {
        snprintf(sData.realData, IO_CMD_DATA_LEN, "<ID=%s>", paraId);
    }
    else
    {
        snprintf(sData.realData, IO_CMD_DATA_LEN, "<ID=%s,Value=%s>", paraId, pData);
    }

	slen = sizeof(sData)-IO_CMD_DATA_LEN + strlen(sData.realData) + 1;
	rlen = sizeof(sData);

	if(api_udp_io_server_send_req(targetIp, cmd, (char*)&sData, slen, (char*)&rData, &rlen) == 0)
	{
		if(rData.result)
		{
            dprintf("Read or write reomte error!!!! paraId=%s\n", paraId);
		}
		else
		{
            sscanf(rData.realData, "<ID=%[^,>]%*[,>]Value=%[^>]", id, pData);
            if(paraId && !strcmp(id, paraId))
            {
                return 1;
            }
		}
	}

    return 0;
}

const char* GetIX_protocolVersion(int targetIp)
{
    char readData[500] = {0};

    API_OldPara_ReomteRequest(targetIp, UDP_IO_SERVER_CMD_R, "1012", readData);

    if(!strcmp(readData, "001"))
    {
        return "IX1/2";
    }
    else if(!strcmp(readData, "IX2V"))
    {
        return "IX2V";
    }

    return "";
}

int API_ReadOldPara(int ip, const char* ioID, char* readData)
{
	if(readData)
		readData[0]=0;
    return API_OldPara_ReomteRequest(ip, UDP_IO_SERVER_CMD_R, ioID, readData);
}

/*********************************************************************************************************
**  End Of File
**********************************************************************************************************/
