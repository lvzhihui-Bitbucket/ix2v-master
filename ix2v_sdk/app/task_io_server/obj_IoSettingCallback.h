/**
  ******************************************************************************
  * @file    obj_IoSettingCallback.c
  * @author  czb
  * @version V00.01.00
  * @date    2021.12.21
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2021 V-Tec</center></h2>
  ******************************************************************************
  */ 

#include "utility.h"
#include "cJSON.h"

//IO参数设置之后回调函数，old：旧参数值，new：新参数值
typedef void (*IO_SettingCallback)(cJSON *old, cJSON *new);
typedef struct
{
    char*               ioName;
    IO_SettingCallback  callback;
}IO_SettingCallback_S;

extern IO_SettingCallback_S IO_SETTING_CB_TABLE[];
extern const int  IO_SETTING_CB_TABLE_CNT;

/*********************************************************************************************************
**  End Of File
**********************************************************************************************************/
