/**
  ******************************************************************************
  * @file    obj_IoInterface.h
  * @author  czb
  * @version V00.01.00
  * @date    2021.12.21
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2021 V-Tec</center></h2>
  ******************************************************************************
  */ 

#include "cJSON.h"
#ifndef _obj_IoInterface_h
#define _obj_IoInterface_h

typedef int IO_bool;

#define IO_WRITE_DEFAULT_VALUE		  "Default_Value"
#define IO_DATA_LENGTH				  500
#define IO_CMD_DATA_LEN				  500

#if	defined(PID_IX611)||defined(PID_IX622)
    #define NOT_WITH_MCU        //不带MUC
#endif


#pragma pack(1)  //指定按1字节对齐
typedef struct
{
	unsigned char	msg_type;		
	int 			device_address;	
	char			result;
	char			property_id[5];		
	char			ptr_type;
	char			realData[IO_CMD_DATA_LEN];
}UDP_IO_CMD_t;
#pragma pack()

#define IO_Identifier                            "IO"

#define IO_MFG_SN-Info                            "MFG_SN-Info"
    #define IO_MFG_SN                             "MFG_SN"
    #define IO_MFG_SN_Source                      "MFG_SN.Source"
    #define IO_MFG_SN_CHECK                       "MFG_SN_CHECK"
	#define IO_EOC_MAC                             "EOC_MAC"

#define CALL_Nbr                            "CALL_Nbr"
    #define IO_DT_ADDR                           "DT_ADDR"
    #define IX_ADDR                             "IX_ADDR"
    #define IX_NAME                             "IX_NAME"
    #define G_NBR                               "G_NBR"
    #define L_NBR                               "L_NBR"

#define DEV_INFO                            "DEV_INFO"
    #define SysType                              "SysType"
    #define MenuType                             "MenuType"
    #define ASSO                                 "ASSO"
    #define USER_ABOUT_CONFIG                    "USER_ABOUT_CONFIG"
    #define MANAGER_ABOUT_CONFIG                 "MANAGER_ABOUT_CONFIG"
    #define DEV_NAME                             "DEV_NAME"
    #define DEV_MODEL                            "DEV_MODEL"
    #define CUS_DEV_NAME                         "CUS_DEV_NAME"
    #define CUS_DEV_MODEL                        "CUS_DEV_MODEL"
	#define G_NBR_SEARCH_ALLOW		"G_NBR_SEARCH_ALLOW"
	#define L_NBR_SEARCH_ALLOW		"L_NBR_SEARCH_ALLOW"

#define FM_VER_INFO                             "FM_VER_INFO"
    #define HD_VER                                  "HD_VER"
    #define FW_VER                                  "FW_VER"
    #define MCU_VER                                 "MCU_VER"
    #define FWVersionAndVerify                      "FWVersionAndVerify"

#define LANConfig                           "LANConfig"
    #define Lan_policy							"Lan_policy"
    #define Static_ip_setting					"Static_ip_setting"
        #define IP_Address					    "IP_Address"
        #define SubnetMask					    "SubnetMask"
        #define DefaultRoute					"DefaultRoute"

    #define Auto_ip_setting						"Auto_ip_setting"
        #define Autoip_IPMask					    "Autoip_IPMask"
        #define Autoip_IPGateway					"Autoip_IPGateway"
        #define Autoip_IPSegment					"Autoip_IPSegment"
        #define LanInterface_name					"LanInterface_name"
        #define DHCP_timout					        "DHCP_timout"
        #define AUTOIP_sleep					    "AUTOIP_sleep"
	#define Autoip_LastIP					    "Autoip_LastIP"	

#define WLANConfig                              "WLANConfig"
    #define WIFI_SWITCH							    "WIFI_SWITCH"
    #define WIFI_SSID_PARA					        "WIFI_SSID_PARA"

#define DEV_Manage                          "DEV_Manage"
    #define InstallerPassword                   "InstallerPassword"
    #define ManagePassword                      "ManagePassword"
    #define Search_Role                         "Search_Role"
    #define Search_Last_Server_Enable           "Search_Last_Server_Enable"
    #define Search_Server_Inform_Timer          "Search_Server_Inform_Timer"
    #define Search_Client_TimeoutCnt            "Search_Client_TimeoutCnt"
    #define Search_Device_Min_Timer             "Search_Device_Min_Timer"
    #define Search_Device_Timer                 "Search_Device_Timer"
    #define GetSearchListType                   "GetSearchListType"         //获取search列表方式，1：远程表方式，0：tftp直接取文件方式
    #define UI_Documents                        "UI_Documents"
    #define BDU_Enable                          "BDU_Enable"
    #define EOC_WORK_MODE                       "EOC_WORK_MODE"
    #define CERT_GoLive_Timer		            "CERT_GoLive_Timer"
    #define CHECK_TB_TIME		                "CHECK_TB_TIME"
    #define TB_TRANSFER_DIR		                "TB_TRANSFER_DIR"
	#define	AUTO_REBOOT_EN		                "AUTO_REBOOT_EN"
	#define DFCall_Enable				        "DFCall_Enable"
	#define	EVR_ENABLE				            "EVR_ENABLE"
	#define	DATASET_PARA				        "DATASET_PARA"
	#define	SettingTimeout				        "SettingTimeout"

#define NetConfig                           "NetConfig"
    #define IX_NT                             "IX_NT"
    #define Search_Record                       "Search_Record"
    #define LAN_Search_Timeout                  "LAN_Search_Timeout"
        #define Lan_S1_Timeout                  "Lan_S1_Timeout"
        #define Lan_S1_IntervalTime                 "Lan_S1_IntervalTime"
        #define Lan_S2_Enable                       "Lan_S2_Enable"
        #define Lan_S2_Timeout                      "Lan_S2_Timeout"
        #define Lan_S2_IntervalTime                 "Lan_S2_IntervalTime"
        #define Lan_S2_MinCnt                        "Lan_S2_MinCnt"


    #define WLAN_Search_Timeout                 "WLAN_Search_Timeout"
        #define Wlan_S1_Timeout                      "Wlan_S1_Timeout"
        #define Wlan_S1_IntervalTime                 "Wlan_S1_IntervalTime"
        #define Wlan_S2_Enable                       "Wlan_S2_Enable"
        #define Wlan_S2_Timeout                      "Wlan_S2_Timeout"
        #define Wlan_S2_IntervalTime                 "Wlan_S2_IntervalTime"
        #define Wlan_S2_MinCnt                       "Wlan_S2_MinCnt"
	
#define ElogConfig                           "ElogConfig"
    #define ELOG_FILE_SIZE                      "ELOG_FILE_SIZE"
    #define ELOG_PRINTF_LVL                     "ELOG_PRINTF_LVL"
    #define ELOG_FILE_LVL                       "ELOG_FILE_LVL"
    #define ELOG_UDP_LVL                        "ELOG_UDP_LVL"
    #define ELOG_IX_LVL                         "ELOG_IX_LVL"
    #define ELOG_UDP_IP                         "ELOG_UDP_IP"
    #define BusMonLevel                         "BusMonLevel"
    #define TLogEnable                          "TLogEnable"
    #define TLogIP_ADDR                         "TLogIP_ADDR"
    #define TLogEventCfg                        "TLogEventCfg"

#define IXD_Config                              "IXD_Config"
    #define PRJ_ID                                  "PRJ_ID"
    #define PRJ_PWD                                 "PRJ_PWD"
    #define PRJ_desc                                "PRJ_desc"
    #define IXD_CloudServer                         "IXD_CloudServer"
    #define IXD_AutoLogin                           "IXD_AutoLogin"
    #define IXD_AutoConnect                         "IXD_AutoConnect"
    #define Local_ProxyPriority                     "Local_ProxyPriority"
    #define Remote_ProxyPriority                    "Remote_ProxyPriority"
    #define Mobile_SSID_NAME1                       "Mobile_SSID_NAME1"
    #define Mobile_SSID_NAME2                       "Mobile_SSID_NAME2"
    #define Mobile_SSID_PWD1                       "Mobile_SSID_PWD1"
    #define Mobile_SSID_PWD2                       "Mobile_SSID_PWD2"

#define IXDHalEventConfig_Config                "HalEventConfig"
    #define IX611_KEY_TYPE                          "IX611_KEY_TYPE"
    #define HOME_KEY_IX_CALL_NBR                    "HOME_KEY_IX_CALL_NBR"
    #define HOME_KEY_IX_CALL_NBR2                    "HOME_KEY_IX_CALL_NBR2"
    #define HOME_KEY_IX_CALL_NBR3                    "HOME_KEY_IX_CALL_NBR3"
    #define HOME_KEY_IX_CALL_NBR4                    "HOME_KEY_IX_CALL_NBR4"

#define GP_OUT_Config                               "GP_OUT_Config"
    #define RL_TIMER                                "TIMER"
	#define RL_MODE                                 "MODE"
    #define RL1_MODE                                "RL1_MODE"
    #define RL2_MODE                                "RL2_MODE"
    #define Unlock1Pwd1                                "Unlock1Pwd1"
    #define Unlock1Pwd2                                "Unlock1Pwd2"
    #define Unlock2Pwd1                                "Unlock2Pwd1"
    #define Unlock2Pwd2                                "Unlock2Pwd2"
    #define AutoUnlockPwdLen                            "AutoUnlockPwdLen"
    #define KeypadSoundEnable                           "KeypadSoundEnable"     //键盘状态提示音
    #define UnlockRecordEnable                          "UnlockRecordEnable"
    #define UnlockCodeReportEnable                      "UnlockCodeReportEnable"
    #define Ring2ReportTime                             "Ring2ReportTime"
    #define RemoteDeviceEN                              "RemoteDeviceEN"
    #define RemoteDeviceIP                             "RemoteDeviceIP"
    #define RemoteDeviceSN                             "RemoteDeviceSN"
    #define RemoteDeviceGate                            "RemoteDeviceGate"
    #define RemoteCallCard                            "RemoteCallCard"

#define RING_CFG						                "RING_CFG"
    #define VoicePlay                                   "VoicePlay"
	    #define VOICE_VOLUME_IOID			            "VOICE_VOLUME_IOID"
        #define VOICE_LANGUAGE_SELECT_DIR               "VOICE_LANGUAGE_SELECT_DIR"

#define SipConfig						"SipConfig"
	#define SIP_ENABLE				"SIP_ENABLE"
	#define SIP_SERVER				"SIP_SERVER"
	#define SIP_PORT				"SIP_PORT"
	#define SIP_DIVERT				"SIP_DIVERT"
	#define SIP_DIV_PWD				"SIP_DIV_PWD"
	#define SIP_ACCOUNT			    "SIP_ACCOUNT"
	#define SIP_PASSWORD			"SIP_PASSWORD"
	#define SIP_DEFAULT_SER			"SIP_DEFAULT_SER"
	#define SIP_RESERVE_SER		    "SIP_RESERVE_SER"
	#define SIP_NetworkSetting		"SIP_NetworkSetting"
	#define Divert_Ability				"Divert_Ability"
	#define Divert_Acc_Array			"Divert_Acc_Array"

#define DateTimeConfig						"DateTimeConfig"
	#define TIME_SERVER		                    "TIME_SERVER"                   //网络时间服务器，可以是外网也可以是局域网
	#define TIME_ZONE				            "TIME_ZONE"                     //时区
	#define TIME_AutoUpdateEnable			    "TIME_AutoUpdateEnable"         //自动获取网络时间
    #define TIME_DT_ServerEnable                "TIME_DT_ServerEnable"          //作为DT系统时间服务器
    #define TIME_DT_AutoUpdateEnable            "TIME_DT_AutoUpdateEnable"      //使用DT系统上的时间自动更新
	#define TIME_UpdateTiming				    "TIME_UpdateTiming"             //使用定时获取网络上时间
	#define TIME_FORMAT					"TIME_FORMAT"
	#define DATE_FORMAT					"DATE_FORMAT"
  
#define AuVolConfig						"AuVolConfig"
	#define AU_VOL_NORM				"AU-VOL-NORM"
	#define AU_NORM					"AU-NORM"
	#define AU_VOL_SPEC					"AU-VOL-SPEC"
	#define AU_SPEC						"AU-SPEC"
	#define AU_DT_SPK_DEC				"AU-DT-SPK-DEC"
    	#define AU_DT_MIC_DEC				"AU-DT-MIC-DEC"
	#define AU_PHONE						"AU-PHONE"	
#define AecConfig							"AecConfig"
	#define Aec_En						"Aec_En"
	#define	AiNr							"AiNr"
		#define	AiNr_noise_suppress_db		"AiNr.noise_suppress_db"
		#define	AiNr_crc						"AiNr.crc"
		#define	AiNr_enable					"AiNr.enable"
	#define	AoNr								"AoNr"
		#define	AoNr_noise_suppress_db		"AoNr.noise_suppress_db"
		#define	AoNr_crc						"AoNr.crc"
		#define	AoNr_enable					"AoNr.enable"		
	#define Agc							"Agc"
		#define 	Agc_level				"Agc.level"
		#define 	Agc_max_gain			"Agc.max_gain"
		#define 	Agc_min_gain			"Agc.min_gain"
		#define 	Agc_near_sensitivity		"Agc.near_sensitivity"
		#define 	Agc_crc					"Agc.crc"
		#define 	Agc_enable				"Agc.enable"
	#define Aec							"Aec"
		#define 	Aec_au_out_threshold				"Aec.au_out_thresh"
		#define 	Aec_au_out_digi_gain				"Aec.au_out_digi_gain"
		#define 	Aec_au_in_digi_gain				"Aec.au_in_digi_gain"
		#define 	Aec_crc							"Aec.crc"
		#define 	Aec_tail							"Aec.tail"
		#define 	Aec_enable						"Aec.enable"
		#define 	Aec_farBreakdownThresh			"Aec.farBreakdownThresh"
#define IpcConfig "IpcConfig"
	#define IPC_Search_TM         		"IPC_Search_TM"
       #define IPC_USR_NAME 			"IPC_USR_NAME"
       #define IPC_USR_PWD			"IPC_USR_PWD"
       #define IPC_PIP_BIND			"IPC_PIP_BIND"
       #define IPC_APP_List				"IPC_APP_List"
       #define IPC_Quad_List			"IPC_Quad_List"
	#define DisablePIP				"DisablePIP"
	#define DisableAutoMemo				"DisableAutoMemo"
#define FW_Update "Update"
        #define FW_UpdateServer         "UpdateServer"
        #define FW_UpdateCode 			"UpdateCode"
        #define LastUpdateTime          "LastUpdateTime"
        #define LastUpdateCode          "LastUpdateCode"
        #define DxgFwServer         "DxgFwServer"
        #define DxgFwCode 			"DxgFwCode"
        #define DH_IM_FW_SERVER         "DH_IM_FW_SERVER"
        #define DH_IM_FW_CODE 			"DH_IM_FW_CODE"

#define CALL_Next_Sn						"CALL_Next_Sn"
#define Auto_Screen_Bright		"Auto_Screen_Bright"
	#define RADAR_DISTANCE_LEVEL				"RADAR_DISTANCE_LEVEL"
	#define SCREEN_HIGH_BRIGHT				"SCREEN_HIGH_BRIGHT"
	#define SCREEN_LOW_BRIGHT					"SCREEN_LOW_BRIGHT"
	#define SCREEN_HIGH_BRIGHT_TIME			"SCREEN_HIGH_BRIGHT_TIME"
	#define MENU_AUTO_RETURN_TIME			"MENU_AUTO_RETURN_TIME"
	#define BL_ON_ALAWYS						"BL_ON_ALAWYS"

#define NDM_Config		                "NDM_Config"
    #define NDM_S_BOOT		                "NDM_S_BOOT"
    #define NDM_C_BOOT		                "NDM_C_BOOT"
    #define NDM_S_TIME		                "NDM_S_TIME"
    #define NDM_S_GET_PB		            "NDM_S_GET_PB"
    #define NDM_C_TIME		                "NDM_C_TIME"
    #define NDM_C_CHECK		                "NDM_C_CHECK"
    #define NDM_S_IP_ADDR		            "NDM_S_IP_ADDR"
    #define NDM_S_MFG_SN		            "NDM_S_MFG_SN"

#define AutoTestPara		"AutoTestPara"

#define NameplateLevel		"NameplateLevel"
#define NightvisionLevel    "NightvisionLevel"
#define NightvisionDuty		"NightvisionDuty"
#define CMR7610NightvisionLevel    "7610NightvisionLevel"
#define CMR7610NightvisionDuty		"7610NightvisionDuty"
#define CMR7610NightvisionThreshold 	"7610NightvisionThreshold"

#define IXGRMMonLimitTime	"IXGRMMonLimitTime"
#define IXGRMMemoLimitTime	"IXGRMMemoLimitTime"
#define IXGRMOtherLimitTime	"IXGRMOtherLimitTime"
#define IXRListEnable	    "IXRListEnable"
#define CurLanguage	        "CurLanguage"
#define KeybordLanguage	    "KeybordLanguage"
#define LanguageSelect	    "LanguageSelect"
#define CardContactPara		"CardContactPara"

#define MainListType	    		"MainListType"
#define RoomNumDispType	  "RoomNumDispType"

#define ListSize	  "List_Size"
#define ListColor	  "List_Color"
#define ListLen	      "List_Len"
#define KeyBeepDis	"KeyBeepDis"
#define M4DispImOnline "M4DispImOnline"
#define insideBeep	"insideBeep"
#define DisCardTableRemote	"DisCardTableRemote"


//#define RADAR_MOTION_DISTANCE_LEVEL    	"RADAR_MOTION_DISTANCE_LEVEL"

int API_LoadIoDefineFile(const char* ioDefineFilePath);
int API_LoadIoIdToNameFile(const char* idToNameFilePath);
int API_LoadIoCusFile(const char* ioCusFilePath);

/*******************************************************************
 * const cJSON * API_Para_Read_Public(const char* key);
 * 
 * 注意返回的cJSON指针，使用完之后
 * 
 * 不能调用JSON_Delete()释放，
 * 
 * 不能调用JSON_Delete()释放，
 * 
 * 不能调用JSON_Delete()释放
 *******************************************************************/
const cJSON * API_Para_Read_Public(const char* key);
int API_Para_Read_Int(const char* paraName);
int API_Para_Read_String(const char* paraName, char* value);
char* API_Para_Read_String2(const char* paraName);

int API_Para_Write_Public(const cJSON* value);
int API_Para_Write_Int(const char* paraName, int value);
int API_Para_Write_String(const char* paraName, char* value);
int API_Para_Write_PublicByName(const char* name, const cJSON* value);

cJSON * API_Para_ReadById_Public(const char* paraId);
int API_Para_ReadByID_Int(const char* paraId);
int API_Para_ReadByID_String(const char* paraId, char* value);
int API_Para_WriteByID_Int(const char* paraId, int value);
int API_Para_WriteByID_String(const char* paraId, char* value);

/*******************************************************************
 * const cJSON* API_Para_Read_Default(const char* paraName)
 * const cJSON* API_Para_ReadByID_Default(const char* paraId);
 * 注意返回的cJSON指针，使用完之后
 * 
 * 不能调用JSON_Delete()释放，
 * 
 * 不能调用JSON_Delete()释放，
 * 
 * 不能调用JSON_Delete()释放
 *******************************************************************/
const cJSON* API_Para_Read_Default(const char* paraName);
int API_Para_Restore_Default(const char* paraName);

const cJSON* API_Para_ReadByID_Default(const char* paraId);
int API_Para_RestoreByID_Default(const char* paraId);

void InitIOServer(void);


//为了兼容本地旧程序
int API_OldPara_ReadByID(char *paraId, char *readData);
int API_OldPara_WriteByID(char *paraId, char *writeData);

//为了兼容远程旧程序
int API_Para_ReadReomteByID(int targetIp, char *paraId, char *readData);
int API_Para_WriteReomteByID(int targetIp, char *paraId, char *readData);
int API_Para_WriteDefaultReomteByID(int targetIp, char *paraId, char *readData);

int API_IO_ReadMcu(const char* key, char* pvalue);
int API_IO_WriteMcu(const char* key, char* pvalue);
int API_IO_UpdateMcu(void);


//返回1校验通过，返回0校验不通过
//校验文件中不存在参数名当作校验通过处理
int API_IO_Check(char* key, cJSON* value);

//为了使用方便，提供了string和int校验接口
int API_IO_CheckString(char* key, char* value);
int API_IO_CheckInt(char* key, int value);

//即刻保存IO参数
void API_IOSaveImmediately(void);

void MyElogJson(int level, const char* name, const cJSON *json);
void MyElogHex(int level, const char* hex, int len);
void MyPrintJson(const char* name, const cJSON *json);
#endif
/*********************************************************************************************************
**  End Of File
**********************************************************************************************************/
