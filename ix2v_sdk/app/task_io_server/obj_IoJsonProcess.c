/**
  ******************************************************************************
  * @file    obj_IoJsonProcess.c
  * @author  czb
  * @version V00.01.00
  * @date    2021.12.21
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2021 V-Tec</center></h2>
  ******************************************************************************
  */ 

#include "utility.h"
#include "cJSON.h"
#include <pthread.h>
#include "elog.h"
#include "define_file.h"
#include "obj_PublicInformation.h"
#include "obj_IoInterface.h"
#include "vdp_uart.h"
#include "obj_IoSettingCallback.h"

#define IO_KEY_NOT_USED "NOT_USED"

#define IO_TIMER_TimingLength   (3*1000)       //设置之后，3秒钟写文件

typedef struct
{
    char *name;
    char addrIndex;
    char len;
}IO_MCU_ADDR_S;

typedef struct
{
    cJSON *defJson;
    cJSON *setJson;
    cJSON *idToNameJson;
    cJSON *checkJson;
    OS_TIMER timer;
	pthread_mutex_t	lock;
}IO_RUN_S;


IO_MCU_ADDR_S MCU_ADDR_TABLE[] = 
{
    {"DT_ADDR", 0x20, 0x01},
    {"RL1_MODE", 0x21, 0x01},
    {"RL2_MODE", 0x22, 0x01},
    {"MFG_SN", 0x23, 0x14},
    {"MFG_SN_CHECK", 0x37, 0x10},
    {"BusMonLevel", 0x47, 0x01},
    {"EOC_WORK_MODE", 0x48, 0x01},
};

const int  MCU_ADDR_TABLE_CNT = sizeof(MCU_ADDR_TABLE)/sizeof(MCU_ADDR_TABLE[0]);


static IO_RUN_S ioRun = {.defJson = NULL, .setJson = NULL, .idToNameJson = NULL, .checkJson = NULL, .lock = PTHREAD_MUTEX_INITIALIZER};


//遍历json结构中所有的object的KEY，返回object
cJSON * MyCjson_GetObjectItem(cJSON *findJson, char *name)
{
    cJSON *current_element = NULL;
    cJSON *ret = NULL;

    if ((findJson == NULL) || (name == NULL))
    {
        return ret;
    }

    current_element = findJson;

    while(current_element != NULL)
    {
        if(current_element->string != NULL && !strcmp(name, current_element->string))
        {
            ret = current_element;
            return ret;
        }
        else if(cJSON_IsObject(current_element))
        {
            ret = MyCjson_GetObjectItem(current_element->child, name);
            if(ret != NULL)
            {
                return ret;
            }
        }
        current_element = current_element->next;
    }
    return ret;
}

//遍历json结构中所有的object的KEY，返回string
const char * MyCjson_GetObjectString(cJSON *findJson, char *name)
{
    char* ret = NULL;
    cJSON* tempJson = MyCjson_GetObjectItem(findJson, name);
    if(cJSON_IsString(tempJson))
    {
        ret = cJSON_GetStringValue(tempJson);
    }
    return ret;
}

cJSON * MyCjson_GetObjectParent(cJSON *findJson, char *name)
{
    cJSON *current_element = NULL;
    cJSON *ret = NULL;

    if ((findJson == NULL) || (name == NULL))
    {
        return ret;
    }

    current_element = findJson;

    while(current_element != NULL)
    {
        if(cJSON_GetObjectItemCaseSensitive(current_element, name) != NULL)
        {
            ret = current_element;
            return ret;
        }
        else if(cJSON_IsObject(current_element))
        {
            ret = MyCjson_GetObjectParent(current_element->child, name);
            if(ret != NULL)
            {
                return ret;
            }
        }
        current_element = current_element->next;
    }

    return ret;
}

int MyCjson_ReplaceItemByString(cJSON *findJson, const char *string, cJSON *replacement)
{
    cJSON *current_element = NULL;
    cJSON *parent = NULL;

    if ((findJson == NULL) || (replacement == NULL) || (string == NULL))
    {
        return 0;
    }

    parent = MyCjson_GetObjectParent(findJson, string);
    if(parent != NULL)
    {
        cJSON_ReplaceItemInObjectCaseSensitive(parent, string, replacement);
        return 1;
    }

    return 0;
}

int MyCjson_DeleteItemByString(cJSON *findJson, const char *string)
{
    cJSON *current_element = NULL;
    cJSON *parent = NULL;

    if ((findJson == NULL) || (string == NULL))
    {
        return 0;
    }

    parent = MyCjson_GetObjectParent(findJson, string);
    if(parent != NULL)
    {
        cJSON_DeleteItemFromObjectCaseSensitive(parent, string);
        return 1;
    }

    return 0;
}

cJSON * MyCjson_CreateJsonDifference(cJSON* difference, const cJSON *template, const cJSON *value)
{
    cJSON *current_element = NULL;
    cJSON *oneValue = NULL;

    if((template == NULL) || (value == NULL))
    {
        return NULL;
    }

    if(difference == NULL)
    {
        difference = cJSON_CreateObject();
    }

    current_element = template;

    while(current_element != NULL)
    {
        if(cJSON_IsObject(current_element))
        {
            MyCjson_CreateJsonDifference(difference, current_element->child, value);
        }
        else if(current_element->string != NULL)
        {
            oneValue = MyCjson_GetObjectItem(value, current_element->string);
            if(oneValue != NULL && !cJSON_Compare(oneValue, current_element, 1))
            {
                cJSON_AddItemToObject(difference, current_element->string, cJSON_Duplicate(oneValue, 1));
            }
        }
        current_element = current_element->next;
    }
    
    return difference;
}

static void SaveIoSettingValueFile(const char* filePath, const cJSON *defaultJson, const cJSON *setJson)
{
	FILE* file = NULL;
	char* string = NULL;

    if(filePath == NULL || setJson == NULL || defaultJson == NULL)
    {
        return;
    }

    cJSON * saveJson = MyCjson_CreateJsonDifference(NULL, defaultJson, setJson);
    SetJsonToFile(filePath, saveJson);
    cJSON_Delete(saveJson);
}

static int WriteOnePara(cJSON * defaultJson, cJSON * setJson, cJSON * writeValue)
{
    cJSON * settingValue = NULL;
    cJSON * defaultValue = NULL;
    int ret = -1;
    int i;

    if(writeValue == NULL)
    {
        return ret;
    }
    
    if(writeValue->string == NULL)
    {
        return ret;
    }

    //校验没有通过，不能写
    if(API_IO_Check(writeValue->string, writeValue) == 0)
    {
        log_v("API_IO_Check:%s check error!!!", writeValue->string);
        return ret;
    }

    settingValue = cJSON_Duplicate(MyCjson_GetObjectItem(setJson, writeValue->string), 1);
    //等于设置值，不处理
    if(cJSON_Compare(settingValue, writeValue, 1))
    {
        //不用处理
        ret = 0;
    }
    //不等于设置值，需要处理
    else
    {
        ret = 1;
        defaultValue = MyCjson_GetObjectItem(defaultJson, writeValue->string);
        //等于默认值，则删掉设置值表
        if(cJSON_Compare(defaultValue, writeValue, 1))
        {
            MyCjson_ReplaceItemByString(setJson, writeValue->string, cJSON_Duplicate(defaultValue, 1));
        }
        //不等于默认值，修改设置值表
        else
        {
            MyCjson_ReplaceItemByString(setJson, writeValue->string, cJSON_Duplicate(writeValue, 1));
        }

        if(cJSON_IsString(writeValue))
        {
            char valueString[200];
            strcpy(valueString, writeValue->valuestring);
            API_IO_SyncToMcu(writeValue->string, valueString);
        }
        else
        {
            int valueInt;
            if(cJSON_IsNumber(writeValue))
            {
                valueInt = writeValue->valueint;
                API_IO_SyncToMcu(writeValue->string, &valueInt);
            }
            else if(cJSON_IsTrue(writeValue))
            {
                valueInt = 1;
                API_IO_SyncToMcu(writeValue->string, &valueInt);
            }
            else if(cJSON_IsFalse(writeValue))
            {
                valueInt = 0;
                API_IO_SyncToMcu(writeValue->string, &valueInt);
            }
        }
    }

    for(i = 0; i < IO_SETTING_CB_TABLE_CNT; i++)
    {
        if(writeValue && !strcmp(IO_SETTING_CB_TABLE[i].ioName, writeValue->string) && IO_SETTING_CB_TABLE[i].callback)
        {
            API_Event_IO_Setting((int)(IO_SETTING_CB_TABLE[i].callback), settingValue, writeValue);
        }
    }
    cJSON_Delete(settingValue);
    return ret;
}

static void WriteMultiplePara(const cJSON* defaultJson, const cJSON* setJson, const cJSON* value, cJSON** result)
{
    cJSON * current_element = NULL;
    cJSON * defaultValue = NULL;
    cJSON * settingValue = NULL;
    int ret = 0;

    if(value == NULL)
    {
        *result = NULL;
        return;
    }

    if(*result == NULL)
    {
        *result = cJSON_CreateObject();
    }

    if(cJSON_IsObject(value))
    {
        current_element = value->child;
        while(current_element != NULL)
        {
            if(cJSON_IsObject(current_element))
            {
                WriteMultiplePara(defaultJson, setJson, current_element, result);
            }
            else if(current_element->string != NULL)
            {
                ret = WriteOnePara(defaultJson, setJson, current_element);
                cJSON_AddItemToObject(*result, current_element->string, cJSON_CreateNumber(ret));
            }
            current_element = current_element->next;
        }
    }
    else if(value->string != NULL)
    {
        ret = WriteOnePara(defaultJson, setJson, value);
        cJSON_AddItemToObject(*result, value->string, cJSON_CreateNumber(ret));
    }
}

static int IoCusFileProcess(const cJSON* defaultJson, const cJSON* cusJson)
{
    cJSON * current_element = NULL;
    cJSON * next_element = NULL;

    if(cusJson == NULL || defaultJson == NULL)
    {
        return -1;
    }

    if(cJSON_IsObject(cusJson))
    {
        current_element = cusJson->child;
        while(current_element != NULL)
        {
            next_element = current_element->next;
            if(cJSON_IsObject(current_element))
            {
                IoCusFileProcess(defaultJson, current_element);
            }
            else if(current_element->string != NULL)
            {
                MyCjson_ReplaceItemByString(defaultJson, current_element->string, cJSON_Duplicate(current_element, 1));
            }
            current_element = next_element;
        }
    }
    else if(cusJson->string != NULL)
    {
        MyCjson_ReplaceItemByString(defaultJson, cusJson->string, cJSON_Duplicate(cusJson, 1));
    }
    else
    {
        return -1;
    }

    return 0;
}

void MyPrintJson(const char* name, const cJSON *json)
{
    char* string;

    string = cJSON_PrintUnformatted(json);
    printf("%s = %s\n", name, string);
    free(string);
}

void MyElogJson(int level, const char* name, const cJSON *json)
{
    char string[1000] = {0};
    if(cJSON_PrintPreallocated(json, string, 1000, 0))
    {
        switch (level)
        {
            case ELOG_LVL_ASSERT:
                log_a("%s:%s", name, string);
                break;
            case ELOG_LVL_ERROR:
                log_e("%s:%s", name, string);
                break;
            case ELOG_LVL_WARN:
                log_w("%s:%s", name, string);
                break;
            case ELOG_LVL_INFO:
                log_i("%s:%s", name, string);
                break;
            case ELOG_LVL_DEBUG:
                log_d("%s:%s", name, string);
                break;
            case ELOG_LVL_VERBOSE:
                log_v("%s:%s", name, string);
                break;
            
            default:
                break;
        }
    }
}

void MyElogHex(int level, const char* hex, int len)
{
    char* string = NULL;
    int i;
    if(hex && len)
    {
        string = malloc(len*3);
        if(string)
        {
            for(i = 0; i < len; i++)
            {
                if(i == (len-1))
                {
                    sprintf((string + 3*i), "%02x", hex[i]);
                    string[3*i+2] = 0;
                }
                else
                {
                    sprintf((string + 3*i), "%02x ", hex[i]);
                }
            }
            switch (level)
            {
                case ELOG_LVL_ASSERT:
                    log_a(string);
                    break;
                case ELOG_LVL_ERROR:
                    log_e(string);
                    break;
                case ELOG_LVL_WARN:
                    log_w(string);
                    break;
                case ELOG_LVL_INFO:
                    log_i(string);
                    break;
                case ELOG_LVL_DEBUG:
                    log_d(string);
                    break;
                case ELOG_LVL_VERBOSE:
                    log_v(string);
                    break;
                
                default:
                    break;
            }
            free(string);
        }
    }
}

static void IO_Lock(void)
{
    pthread_mutex_lock(&ioRun.lock);
}
		
static void IO_Unlock(void)
{
    pthread_mutex_unlock(&ioRun.lock);
}

static void IOServerWriteFile(void)
{
    IO_Lock();
    SaveIoSettingValueFile(IO_DATA_VALUE_NAME, ioRun.defJson, ioRun.setJson);
    IO_Unlock();
}

void API_IOSaveImmediately(void)
{
	IOServerWriteFile();
}

void API_IOPropertyInit(void)
{
	OS_CreateTimer(&ioRun.timer, IOServerWriteFile, IO_TIMER_TimingLength/25);
}

//需要写文件
static void SettingValueUpdateReport(const char* keyName)
{
   	OS_RetriggerTimer(&ioRun.timer);
}

static int LoadIoCusFile(const char* ioCusFilePath)
{
    cJSON *cusJson = NULL;
    cJSON *notUsed = NULL;
    cJSON *element = NULL;

    cusJson = GetJsonFromFile(ioCusFilePath);

    //删除不需要的
    notUsed = cJSON_GetObjectItemCaseSensitive(cusJson, IO_KEY_NOT_USED);
    if(notUsed)
    {
        cJSON_ArrayForEach(element, notUsed)
        {
            if(cJSON_IsString(element))
            {
                MyCjson_DeleteItemByString(ioRun.defJson, element->valuestring);
            }
        }
        cJSON_DeleteItemFromObjectCaseSensitive(cusJson, IO_KEY_NOT_USED);
    }

    //修改已经存在的
    IoCusFileProcess(ioRun.defJson, cusJson);

    cJSON_Delete(cusJson);    
    return 1;
}

static int LoadIoSetFile(const char* ioSetFilePath)
{
    cJSON *setting = NULL;

    if(ioRun.setJson)
    {
        cJSON_Delete(ioRun.setJson);
    }
    ioRun.setJson = cJSON_Duplicate(ioRun.defJson, 1);
    setting = GetJsonFromFile(ioSetFilePath);
    IoCusFileProcess(ioRun.setJson, setting);
    cJSON_Delete(setting);

    return 1;
}

static int LoadIoCheckFile(const char* ioCheckFilePath)
{
    if(ioRun.checkJson == NULL)
    {
        ioRun.checkJson = GetJsonFromFile(ioCheckFilePath);
    }
    return 1;
}

int API_LoadIoDefineFile(const char* ioDefineFilePath)
{
    cJSON* loadJson = NULL;
    cJSON* element = NULL;
    char temp[500];

    IO_Lock();

    //导入统一表文件中的默认值
    loadJson = GetJsonFromFile(ioDefineFilePath);
    if(loadJson == NULL)
    {
        dprintf("%s error!!!!\n", ioDefineFilePath);
        IO_Unlock();
        return 0;
    }

    if(ioRun.defJson)
    {
        cJSON_Delete(ioRun.defJson);
    }

    ioRun.defJson = cJSON_CreateObject();

    cJSON_ArrayForEach(element, loadJson)
    {
        if(element->string)
        {
            cJSON_AddItemToObject(ioRun.defJson, element->string, cJSON_Duplicate(element, 1));
        }
    }
    cJSON_Delete(loadJson);

    //修改裁剪表定义的默认值
    snprintf(temp, 500, "%s/%s%s", IO_PARA_Path, MY_DEV_NAME, IO_Trim_FILE_NAME);
    LoadIoCusFile(temp);
    //修改订制表定义的默认值
    snprintf(temp, 500, "%s/%s%s", IO_Cus_Path, MY_DEV_NAME, IO_Cus_FILE_NAME);
    LoadIoCusFile(temp);

    snprintf(temp, 500, "%s", IO_Factory_FILE_NAME);
    LoadIoCusFile(temp);

    //重新生成设置值表
    LoadIoSetFile(IO_DATA_VALUE_NAME);

    //值范围校验
    snprintf(temp, 500, "%s/%s%s", IO_PARA_Path, MY_DEV_NAME, IO_Check_FILE_NAME);
    LoadIoCheckFile(temp);

    IO_Unlock();
    return 1;
}
int API_LoadIoIdToNameFile(const char* idToNameFilePath)
{
    IO_Lock();

    if(ioRun.idToNameJson == NULL)
    {
        ioRun.idToNameJson = GetJsonFromFile(idToNameFilePath);
    }

    if(ioRun.idToNameJson == NULL)
    {
        ioRun.idToNameJson = cJSON_CreateObject();
    }

    IO_Unlock();
    return 1;
}

int API_LoadIoCusFile(const char* ioCusFilePath)
{
    IO_Lock();

    //修改默认值表
    LoadIoCusFile(ioCusFilePath);

    //重新生成设置值表
    LoadIoSetFile(IO_DATA_VALUE_NAME);

    IO_Unlock();
    return 1;
}

//停止IO写文件
void StopIOSaveFile(void)
{
   	OS_StopTimer(&ioRun.timer);
}


const cJSON * API_ReadPara(const char* key)
{
    cJSON * value = NULL;

    IO_Lock();
    if(key && key[0])
    {
        value = MyCjson_GetObjectItem(ioRun.setJson, key);
    }
    else
    {
        value = ioRun.setJson;
    }
    IO_Unlock();
    return value;
}


//返回 0--有失败，1全部正确
int API_WritePara(const cJSON* value)
{
    cJSON* result = NULL;
    cJSON* element = NULL;
    int errorRet = 0;
    int ignoreRet = 0;
    int modifyRet = 0;

    IO_Lock();

    WriteMultiplePara(ioRun.defJson, ioRun.setJson, value, &result);
    cJSON_ArrayForEach(element, result)
    {
        if(element->valuedouble < 0)
        {
            //dprintf("%s write error!!!\n", element->string);
            errorRet++;
        }
        else if(element->valuedouble == 0)
        {
            //dprintf("%s write ignore!!!\n", element->string);
            ignoreRet++;
        }
        else
        {
            SettingValueUpdateReport(element->string);
            //dprintf("%s write modify!!!\n", element->string);
            modifyRet++;
        }
    }

    cJSON_Delete(result);
    //dprintf("ret1=%d, ret2=%d, ret3=%d\n", ret1, ret2, ret3);
    IO_Unlock();

    return (errorRet ? 0 : 1);
}

int API_IO_SyncToMcu(char* key, char* value)
{
    int i;

    char* ix2vDtAddrString;
    int mcuDtAddr = 0;
    int ix2vDtAddr = 0;

    char* ix2vLockModeString;
    int mcuLockMode = 0;
    int ix2vLockMode = 0;

    if(key == NULL || value == NULL)
    {
        return 0;
    }
    
    for(i = 0; i < MCU_ADDR_TABLE_CNT; i++)
    {
        if(!strcmp(key, MCU_ADDR_TABLE[i].name))
        {
            break;
        }
    }

    if(i >= MCU_ADDR_TABLE_CNT)
    {
        return 0;
    }

    //DT_ADDR
    if(!strcmp(IO_DT_ADDR, key))
    {
        if(!strcmp(value, "DS-2"))
        {
            ix2vDtAddr = 0x35;
        }
        else if(!strcmp(value, "DS-3"))
        {
            ix2vDtAddr = 0x36;
        }
        else if(!strcmp(value, "DS-4"))
        {
            ix2vDtAddr = 0x37;
        }
        else 
        {
            ix2vDtAddr = 0x34;
        }
        API_IO_WriteMcu(IO_DT_ADDR, &ix2vDtAddr);
    }
    //RL1_MODE
    else if(!strcmp(RL1_MODE, key))
    {
        if(!strcmp(value, "1-NC") || !strcmp(value, "1"))
        {
            ix2vLockMode = 1;
        }
        if(!strcmp(value, "0-NO") || !strcmp(value, "0"))
        {
            ix2vLockMode = 0;
        }
        else
        {
            ix2vLockMode = 0;
        }

        API_IO_WriteMcu(RL1_MODE, &ix2vLockMode);
    }
    //RL2_MODE
    else if(!strcmp(RL2_MODE, key))
    {
        if(!strcmp(value, "1-NC"))
        {
            ix2vLockMode = 1;
        }
        else
        {
            ix2vLockMode = 0;
        }

        API_IO_WriteMcu(RL2_MODE, &ix2vLockMode);
    }
    //BusMonLevel
    else if(!strcmp(BusMonLevel, key))
    {
        API_IO_WriteMcu(BusMonLevel, &value);
    }
	#ifdef PID_IX850
	else if(!strcmp(EOC_WORK_MODE, key))
	{
		char eoc_mode;
	        if(!strcmp(value, "master"))
	        {
	            eoc_mode = 1;
	        }
	        else
	        {
	            eoc_mode = 0;
	        }

        	API_IO_WriteMcu(EOC_WORK_MODE, &eoc_mode);
    }
	#endif
	
    return 1;
}

const char* API_GetParaNameById(const char* paraId)
{
    return cJSON_GetStringValue(cJSON_GetObjectItemCaseSensitive(ioRun.idToNameJson, paraId));
}

const cJSON * API_ReadDefault(const char* key)
{
    cJSON * ret = NULL;

    IO_Lock();
    if(key)
    {
        ret = MyCjson_GetObjectItem(ioRun.defJson, key);
    }
    else
    {
        ret = ioRun.defJson;
    }
    IO_Unlock();

    return ret;
}

int API_RestoreDefault(const char* key)
{
    int ret = 1;
    int i;

    IO_Lock();
    //log_d("API_RestoreDefault ------------------start\n");

    cJSON * defaultValue = MyCjson_GetObjectItem(ioRun.defJson, key);
    cJSON* settingValue = MyCjson_GetObjectItem(ioRun.setJson, key);

    if(defaultValue == NULL)
    {
        ret = 0;
    }

    if(!cJSON_Compare(settingValue, defaultValue, 1))
    {
        settingValue = cJSON_Duplicate(settingValue, 1);
        MyCjson_ReplaceItemByString(ioRun.setJson, key, cJSON_Duplicate(defaultValue, 1));
        SettingValueUpdateReport(key);

        if(cJSON_IsObject(settingValue))
        {
            for(i = 0; i < IO_SETTING_CB_TABLE_CNT; i++)
            {
                cJSON *oldValue = cJSON_GetObjectItemCaseSensitive(settingValue, IO_SETTING_CB_TABLE[i].ioName);
                if(oldValue && IO_SETTING_CB_TABLE[i].callback)
                {
                    API_Event_IO_Setting((int)(IO_SETTING_CB_TABLE[i].callback), oldValue, cJSON_GetObjectItemCaseSensitive(defaultValue, IO_SETTING_CB_TABLE[i].ioName));
                }
            }
        }
        else if(settingValue->string)
        {
            for(i = 0; i < IO_SETTING_CB_TABLE_CNT; i++)
            {
                if(!strcmp(IO_SETTING_CB_TABLE[i].ioName, settingValue->string) && IO_SETTING_CB_TABLE[i].callback)
                {
                    API_Event_IO_Setting((int)(IO_SETTING_CB_TABLE[i].callback), settingValue, defaultValue);
                }
            }
        }
        cJSON_Delete(settingValue);
    }

    //log_d("API_RestoreDefault ------------------end\n");

    IO_Unlock();

    return ret;
}

int API_IO_ReadMcu(const char* key, char* pvalue)
{
    char sendData[10];
    char recvDate[300];
    int i;
    int recvLen;
    int ret = 0;

    for(i = 0; i < MCU_ADDR_TABLE_CNT; i++)
    {
        if(!strcmp(key, MCU_ADDR_TABLE[i].name))
        {
            break;
        }
    }

    if(i < MCU_ADDR_TABLE_CNT)
    {
        #ifdef NOT_WITH_MCU
        ret = MCU_SimulateFlashReadWrite(0, MCU_ADDR_TABLE[i].addrIndex, MCU_ADDR_TABLE[i].len, pvalue);
        #else
        sendData[0] = 0;    //读
        sendData[1] = MCU_ADDR_TABLE[i].addrIndex;    //偏移地址
        sendData[2] = MCU_ADDR_TABLE[i].len;    //长度

        ret = api_uart_send_pack_and_wait_rsp(UART_TYPE_MEM_ACCESS_REQ, sendData, 3, recvDate, &recvLen, 100);
        if(ret)
        {
            ret = recvDate[3];
            if(pvalue)
            {
                memcpy(pvalue, recvDate+4, ret);
            }
        }
        #endif
    }
    dprintf("API_IO_ReadMcu key =%s, ret = %d\n", key, ret);

    return ret;
}

int API_IO_WriteMcu(const char* key, char* pvalue)
{
    char sendData[300];
    char recvDate[300];
    int i;
    int sendLen, recvLen;
    int ret = 0;

    for(i = 0; i < MCU_ADDR_TABLE_CNT; i++)
    {
        if(!strcmp(key, MCU_ADDR_TABLE[i].name))
        {
            break;
        }
    }

    if(i < MCU_ADDR_TABLE_CNT)
    {
        #ifdef NOT_WITH_MCU
        ret = MCU_SimulateFlashReadWrite(1, MCU_ADDR_TABLE[i].addrIndex, MCU_ADDR_TABLE[i].len, pvalue);
        #else
        sendData[0] = 1;    //写
        sendData[1] = MCU_ADDR_TABLE[i].addrIndex;    //偏移地址
        sendData[2] = MCU_ADDR_TABLE[i].len;    //长度
        memcpy(sendData+3, pvalue, MCU_ADDR_TABLE[i].len);
        sendLen = 3 + MCU_ADDR_TABLE[i].len;

        ret = api_uart_send_pack_and_wait_rsp(UART_TYPE_MEM_ACCESS_REQ, sendData, sendLen, recvDate, &recvLen, 100);
        if(ret)
        {
            if(!memcmp(recvDate+4, pvalue, recvDate[3]))
            {
                ret = recvDate[3];
                api_uart_send_pack(UART_TYPE_MEM_CHANGED, NULL, 0);
            }
        }
        #endif
    }

    dprintf("API_IO_WriteMcu key =%s, ret = %d\n", key, ret);

    return ret;
}

int API_IO_UpdateMcu(void)
{
    char* ix2vDtAddrString;
    int mcuDtAddr = 0;
    int ix2vDtAddr = 0;
    int ix2vBusMonLevel = 0;
    int mcuBusMonLevel = 0;

    char* ix2vLockModeString;
    int mcuLockMode = 0;
    int ix2vLockMode = 0;

    //DT_ADDR
    ix2vDtAddrString = API_Para_Read_String2(IO_DT_ADDR);
    API_IO_ReadMcu(IO_DT_ADDR, &mcuDtAddr);
    if(ix2vDtAddrString)
    {
        if(!strcmp(ix2vDtAddrString, "DS-1"))
        {
            ix2vDtAddr = 0x34;
        }
        else if(!strcmp(ix2vDtAddrString, "DS-2"))
        {
            ix2vDtAddr = 0x35;
        }
        else if(!strcmp(ix2vDtAddrString, "DS-3"))
        {
            ix2vDtAddr = 0x36;
        }
        else if(!strcmp(ix2vDtAddrString, "DS-4"))
        {
            ix2vDtAddr = 0x37;
        }

        if(ix2vDtAddr != mcuDtAddr)
        {
            API_IO_WriteMcu(IO_DT_ADDR, &ix2vDtAddr);
        }
    }

    //RL1_MODE
    ix2vLockModeString = API_Para_Read_String2(RL1_MODE);
    API_IO_ReadMcu(RL1_MODE, &mcuLockMode);
    if(ix2vLockModeString)
    {
        if(!strcmp(ix2vLockModeString, "0-NO") || !strcmp(ix2vLockModeString, "0"))
        {
            ix2vLockMode = 0;
        }
        else if(!strcmp(ix2vLockModeString, "1-NC") || !strcmp(ix2vLockModeString, "1"))
        {
            ix2vLockMode = 1;
        }

        if(ix2vLockMode != mcuLockMode)
        {
            API_IO_WriteMcu(RL1_MODE, &ix2vLockMode);
        }
    }

    //RL2_MODE
    ix2vLockModeString = API_Para_Read_String2(RL2_MODE);
    API_IO_ReadMcu(RL2_MODE, &mcuLockMode);
    if(ix2vLockModeString)
    {
        if(!strcmp(ix2vLockModeString, "0-NO"))
        {
            ix2vLockMode = 0;
        }
        else if(!strcmp(ix2vLockModeString, "1-NC"))
        {
            ix2vLockMode = 1;
        }

        if(ix2vLockMode != mcuLockMode)
        {
            API_IO_WriteMcu(RL2_MODE, &ix2vLockMode);
        }
    }

    //BusMonLevel
    ix2vBusMonLevel = API_Para_Read_Int(BusMonLevel);
    API_IO_ReadMcu(BusMonLevel, &mcuBusMonLevel);
    if(ix2vBusMonLevel != mcuBusMonLevel)
    {
        API_IO_WriteMcu(BusMonLevel, &ix2vBusMonLevel);
    }
	
	#ifdef PID_IX850
		
		
		char mcu_eoc_mode;
		char io_eoc_mode_int;
		char *io_eoc_mode = API_Para_Read_String2(EOC_WORK_MODE);
		if(io_eoc_mode)
		{
			   if(!strcmp(io_eoc_mode, "master"))
		        {
		            io_eoc_mode_int = 1;
		        }
		        else
		        {
		            io_eoc_mode_int = 0;
		        }
			 API_IO_ReadMcu(EOC_WORK_MODE, &mcu_eoc_mode);
		     
			if(io_eoc_mode_int!=mcu_eoc_mode)
	        		API_IO_WriteMcu(EOC_WORK_MODE, &io_eoc_mode_int);
	    }
	#endif
	
    return 1;
}

//返回1校验通过，返回0校验不通过
//校验文件中不存在参数名当作校验通过处理
int API_IO_Check(char* key, cJSON* value)
{
    cJSON* check;
    cJSON* checkValue;
    int ret = 0;
    int max, min, setValue;

    check = MyCjson_GetObjectItem(ioRun.checkJson, key);
    if(cJSON_IsArray(check))
    {
        checkValue = cJSON_GetArrayItem(check, 0);
        min = checkValue->valueint;
        checkValue = cJSON_GetArrayItem(check, 1);
        max = checkValue->valueint;

        if(cJSON_IsNumber(value))
        {
            setValue = value->valueint;
            if(min <= setValue && setValue <= max)
            {
                ret = 1;
            }
        }
        else if(cJSON_IsTrue(value))
        {
            setValue = 1;
            if(min <= setValue && setValue <= max)
            {
                ret = 1;
            }
        }
        else if(cJSON_IsFalse(value))
        {
            setValue = 0;
            if(min <= setValue && setValue <= max)
            {
                ret = 1;
            }
        }
        else if(cJSON_IsString(value))
        {
            setValue = atoi(value->valuestring);
            if(min <= setValue && setValue <= max)
            {
                ret = 1;
            }
        }
    }
    else if(cJSON_IsString(check))
    {
        if(cJSON_IsString(value))
        {
            ret = RegexCheck(check->valuestring, cJSON_GetStringValue(value));
        }
    }
    else
    {
        ret = 1;
    }

    return ret;
}

//为了使用方便，提供了string和int校验接口
int API_IO_CheckString(char* key, char* value)
{
    cJSON* check;
    cJSON* checkValue;
    int ret = 0;

    check = MyCjson_GetObjectItem(ioRun.checkJson, key);
    if(cJSON_IsString(check))
    {
        ret = RegexCheck(check->valuestring, value);
    }
    else if(cJSON_IsArray(check))
    {
        ret = 0;
    }
    else
    {
        ret = 1;
    }

    return ret;
}

int API_IO_CheckInt(char* key, int value)
{
    cJSON* check;
    cJSON* checkValue;
    int ret = 0;
    int max, min;

    check = MyCjson_GetObjectItem(ioRun.checkJson, key);
    if(cJSON_IsArray(check))
    {
        checkValue = cJSON_GetArrayItem(check, 0);
        min = checkValue->valueint;
        checkValue = cJSON_GetArrayItem(check, 1);
        max = checkValue->valueint;
        if(min <= value && value <= max)
        {
            ret = 1;
        }
    }
    else if(cJSON_IsString(check))
    {
        ret = 0;
    }
    else
    {
        ret = 1;
    }

}

#ifdef NOT_WITH_MCU
static char* mcuSimulateFlash = NULL;
static int mcuSimulateFlashLen = 0;


int MCU_SimulateFlashReadWrite(int readOrWrite, int index, int len, char* value)
{
	FILE* file = NULL;
    int myIndex;

    if(MCU_ADDR_TABLE_CNT == 0)
    {
        dprintf("MCU_SimulateFlashReadWrite error MCU_ADDR_TABLE_CNT == 0\n");
        return 0;
    }
    myIndex = MCU_ADDR_TABLE[0].addrIndex;

    if(index < myIndex || index + len > myIndex + mcuSimulateFlashLen || len <= 0)
    {
        dprintf("MCU_SimulateFlashReadWrite error len = %d\n", len);
        return 0;
    }

    //写
    if(readOrWrite)
    {
        if(memcmp(value, mcuSimulateFlash + index - myIndex, len))
        {
            memcpy(mcuSimulateFlash + index-myIndex, value, len);
            if(file=fopen(MCU_SIMULATE_FLASH_PATH,"w"))
            {
                fwrite(mcuSimulateFlash, 1, mcuSimulateFlashLen, file);
                fclose(file);
            }
        }
    }
    //读
    else
    {
        memcpy(value, mcuSimulateFlash + index - myIndex, len);
    }

    return len;
}
#endif




int MCU_SimulateFlashInit(void)
{
#ifdef NOT_WITH_MCU    
	FILE* file = NULL;
    int i;
    int ret = 0;

    if(MCU_ADDR_TABLE_CNT)
    {
        for(i = 0, mcuSimulateFlashLen = 0; i < MCU_ADDR_TABLE_CNT; i++)
        {
            mcuSimulateFlashLen += MCU_ADDR_TABLE[i].len;
        }

        if(mcuSimulateFlash)
        {
            free(mcuSimulateFlash);
        }
        mcuSimulateFlash = malloc(mcuSimulateFlashLen);

        if(file=fopen(MCU_SIMULATE_FLASH_PATH,"r"))
        {
            ret = fread(mcuSimulateFlash, 1, mcuSimulateFlashLen, file);
        }
        else if(file=fopen(MCU_SIMULATE_FLASH_PATH,"w"))
        {
            ret = fwrite(mcuSimulateFlash, 1, mcuSimulateFlashLen, file);
        }
        fclose(file);
    }
    return ret;
#endif
}


int API_Data_Edit_Check(char *key,char *value,cJSON *checkJson)
{
	cJSON* check;
    cJSON* checkValue;
    int ret = 0;
    int max, min, setValue;

    check = MyCjson_GetObjectItem(checkJson, key);
    if(cJSON_IsArray(check))
    {
        checkValue = cJSON_GetArrayItem(check, 0);
        min = checkValue->valueint;
        checkValue = cJSON_GetArrayItem(check, 1);
        max = checkValue->valueint;
	int i=0;
	while(value[i]!=0&&value[i]>='0'&&value[i]<='9')
		i++;
	
	
       // if(cJSON_IsNumber(value))
       if(value[i]==0&&i>0)
        {
            setValue = atoi(value);
            if(min <= setValue && setValue <= max)
            {
                ret = 1;
            }
        }
	#if 0
        else if(cJSON_IsTrue(value))
        {
            setValue = 1;
            if(min <= setValue && setValue <= max)
            {
                ret = 1;
            }
        }
        else if(cJSON_IsFalse(value))
        {
            setValue = 0;
            if(min <= setValue && setValue <= max)
            {
                ret = 1;
            }
        }
		#endif
    }
    else if(cJSON_IsString(check))
    {
        ret = RegexCheck(check->valuestring, value);
    }
    else
    {
        ret = 1;
    }


    return ret;
}
/*********************************************************************************************************
**  End Of File
**********************************************************************************************************/
