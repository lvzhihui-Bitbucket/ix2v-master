/**
  ******************************************************************************
  * @file    obj_IoJsonProcess.c
  * @author  czb
  * @version V00.01.00
  * @date    2021.12.21
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2021 V-Tec</center></h2>
  ******************************************************************************
  */ 

#include "cJSON.h"
#include "obj_IoInterface.h"
#include "utility.h"
#include "vtk_udp_stack_io_server.h"
#include "define_file.h"
#include "obj_PublicInformation.h"
#include <regex.h>

/*******************************************************************
 * const cJSON * API_Para_Read_Public(const char* key);
 * 
 * 注意返回的cJSON指针，使用完之后
 * 
 * 不能调用JSON_Delete()释放，
 * 
 * 不能调用JSON_Delete()释放，
 * 
 * 不能调用JSON_Delete()释放
 *******************************************************************/
const cJSON * API_Para_Read_Public(const char* key)
{
    return API_ReadPara(key);
}

int API_Para_Write_Public(const cJSON* value)
{
    return API_WritePara(value);
}

int API_Para_Write_PublicByName(const char* name, const cJSON* value)
{
    int ret;

    cJSON *para = cJSON_CreateObject();

    cJSON_AddItemReferenceToObject(para, name, value);
    ret = API_WritePara(para);
    cJSON_Delete(para);
    return ret;
}
cJSON * API_Para_ReadById_Public(const char* paraId)
{
    return API_Para_Read_Public(API_GetParaNameById(paraId));
}

int API_Para_Read_Int(const char* paraName)
{
    int retValue = 0;

    cJSON * value = API_ReadPara(paraName);
    if(cJSON_IsNumber(value))
    {
        retValue = value->valuedouble;
    }
    else if(cJSON_IsString(value))
    {
        if(!strcmp(value->valuestring, "true") || !strcmp(value->valuestring, "ON"))
        {
            retValue = 1;
        }
        else if(!strcmp(value->valuestring, "false") || !strcmp(value->valuestring, "OFF"))
        {
            retValue = 0;
        }
        else
        {
            retValue = atoi(value->valuestring);
        }
    }
    else if(cJSON_IsTrue(value))
    {
        retValue = 1;
    }
    else if(cJSON_IsFalse(value))
    {
        retValue = 0;
    }

    return retValue;
}

int API_Para_Read_Type(const char* paraName)
{
    int type = 0;

    cJSON * value = API_ReadPara(paraName);
    if(value)
    {
        type = value->type;
    }

    return type;
}


int API_Para_ReadByID_Int(const char* paraId)
{
    return API_Para_Read_Int(API_GetParaNameById(paraId));
}

int API_Para_Read_String(const char* paraName, char* value)
{
    int retValue = 0;

    cJSON *tempValue = API_ReadPara(paraName);
    if(cJSON_IsString(tempValue))
    {
        strcpy(value, tempValue->valuestring);
        retValue = 1;
    }
    return retValue;
}

char* API_Para_Read_String2(const char* paraName)
{
    char* ret = NULL;

    cJSON *tempValue = API_ReadPara(paraName);
    if(cJSON_IsString(tempValue))
    {
        ret = tempValue->valuestring;
    }

    return ret;
}

int API_Para_ReadByID_String(const char* paraId, char* value)
{
    return API_Para_Read_String(API_GetParaNameById(paraId), value);
}


int API_Para_Write_Int(const char* paraName, int value)
{
    int retValue = 0;

    if(paraName == NULL)
    {
        return retValue;
    }

    cJSON *tempValue = cJSON_CreateObject();

    cJSON_AddItemToObject(tempValue, paraName, cJSON_CreateNumber(value));
    retValue = API_Para_Write_Public(tempValue);
    cJSON_Delete(tempValue);

    //dprintf("Write int---%s:%d\n", paraName, value);

    return retValue;
}

int API_Para_WriteByID_Int(const char* paraId, int value)
{
    return API_Para_Write_Int(API_GetParaNameById(paraId), value);
}

int API_Para_Write_String(const char* paraName, char* value)
{
    int retValue = 0;

    if(paraName == NULL)
    {
        return retValue;
    }

    cJSON *tempValue = cJSON_CreateObject();

    cJSON_AddItemToObject(tempValue, paraName, cJSON_CreateString(value));

    retValue = API_Para_Write_Public(tempValue);
    cJSON_Delete(tempValue);
    return retValue;
}

int API_Para_WriteByID_String(const char* paraId, char* value)
{
    return API_Para_Write_String(API_GetParaNameById(paraId), value);
}

//注意返回的cJSON指针，使用完之后不能调用JSON_Delete()释放，不能调用JSON_Delete()释放，不能调用JSON_Delete()释放
const cJSON* API_Para_Read_Default(const char* paraName)
{
    return API_ReadDefault(paraName);
}

int API_Para_Restore_Default(const char* paraName)
{
    return API_RestoreDefault(paraName);
}

//注意返回的cJSON指针，使用完之后不能调用JSON_Delete()释放，不能调用JSON_Delete()释放，不能调用JSON_Delete()释放
const cJSON* API_Para_ReadByID_Default(const char* paraId)
{
    return API_ReadDefault(API_GetParaNameById(paraId));
}

int API_Para_RestoreByID_Default(const char* paraId)
{
    return API_RestoreDefault(API_GetParaNameById(paraId));
}

static void IOtoPB_Init(void)
{
    char temp[500];
    if(API_Para_Read_String(DEV_NAME, temp))
    {
        API_PublicInfo_Write_String(PB_DEV_NAME, temp);
    }
    if(API_Para_Read_String(DEV_MODEL, temp))
    {
        API_PublicInfo_Write_String(PB_DEV_MODEL, temp);
    }
    if(API_Para_Read_String(CUS_DEV_NAME, temp))
    {
        API_PublicInfo_Write_String(PB_CUS_DEV_NAME, temp);
    }
    if(API_Para_Read_String(IX_ADDR, temp))
    {
        API_PublicInfo_Write_String(PB_IX_ADDR, temp);
    }
    if(API_Para_Read_String(G_NBR, temp))
    {
        API_PublicInfo_Write_String(PB_G_NBR, temp);
    }
    if(API_Para_Read_String(L_NBR, temp))
    {
        API_PublicInfo_Write_String(PB_L_NBR, temp);
    }
    if(API_Para_Read_String(IX_NAME, temp))
    {
        API_PublicInfo_Write_String(PB_IX_NAME, temp);
    }

    API_PublicInfo_Write_String(PB_HD_VER, API_Para_Read_String2(HD_VER));
}

void InitIOServer(void)
{
    char temp[500];

	MCU_SimulateFlashInit();
    API_IOPropertyInit();
    SupportOldParaInit();
    API_LoadPublicInfoFile(PublicInfo_File_Name);
	API_LoadIoDefineFile(IO_PARA_Public_File_Name);

    snprintf(temp, 500, "%s/%s%s", IO_PARA_Path, MY_DEV_NAME, IO_ID_FILE_NAME);
    API_LoadIoIdToNameFile(temp);
    IOtoPB_Init();
}

void API_ReInitIOServer(void)
{
	API_LoadIoDefineFile(IO_PARA_Public_File_Name);
    IOtoPB_Init();
}


int API_OldPara_ReadByID(char *paraId, char *readData)
{
    cJSON * result = NULL;
    char* resultString;
    int ret = 0;

    result = API_Para_ReadById_Public(paraId);
    if(result)
    {
        switch (result->type & 0xFF)
        {
            case cJSON_False:
        	    sprintf(readData, "%d", 0);
                ret = 1;
                break;

            case cJSON_True:
        	    sprintf(readData, "%d", 1);
                ret = 1;
                break;
     
            case cJSON_Number:
        	    sprintf(readData, "%.0f", result->valuedouble);
                ret = 1;
                break;

            case cJSON_String:
                sprintf(readData, "%s", result->valuestring);
                ret = 1;
                break;

            case cJSON_Array:
            case cJSON_Object:
                resultString = cJSON_Print(result);
                if(resultString)
                {
                	sprintf(readData, IO_CMD_DATA_LEN, "%s", resultString);
                    free(resultString);
                    ret = 1;
                }
                break;
        }
        cJSON_Delete(result);
    }

	return ret;
}

int API_OldPara_WriteByID(char *paraId, char *writeData)
{
    cJSON * result = NULL;
    cJSON * setValue = NULL;
    int ret = 0;

    dprintf("API_OldPara_WriteByID paraId=%s, writeData=%s\n", paraId, writeData);

    result = API_Para_ReadByID_Default(paraId);
    if(result)
    {
        switch (result->type & 0xFF)
        {
            case cJSON_Number:
                ret = API_Para_WriteByID_Int(paraId, atoi(writeData));
                break;

            case cJSON_String:
                ret = API_Para_WriteByID_String(paraId, writeData);
                break;

            case cJSON_Array:
            case cJSON_Object:
                setValue = cJSON_CreateObject();
                cJSON_AddItemToObjectCS(setValue, API_GetParaNameById(paraId), cJSON_Parse(writeData));
                ret = API_Para_Write_Public(setValue);
                cJSON_Delete(result);
                break;
        }
    }

	return ret;
}

int API_Para_ReadReomteByID(int targetIp, char *paraId, char *readData)
{
    return API_OldPara_ReomteRequest(targetIp, UDP_IO_SERVER_CMD_R, paraId, readData);
}

int API_Para_WriteReomteByID(int targetIp, char *paraId, char *readData)
{
    return API_OldPara_ReomteRequest(targetIp, UDP_IO_SERVER_CMD_W, paraId, readData);
}

int API_Para_WriteDefaultReomteByID(int targetIp, char *paraId, char *readData)
{
    API_OldPara_ReomteRequest(targetIp, UDP_IO_SERVER_CMD_ONE_DEFAULT, paraId, readData);
}


static cJSON* API_GetObjectItem(cJSON *findJson, char *name)
{
    cJSON *current_element = NULL;
    cJSON *ret = NULL;
    char spliceName[100];

    if((findJson == NULL) || (name == NULL))
    {
        return ret;
    }

    if(findJson->string != NULL)
    {
        snprintf(spliceName, 100, "%s_%s", findJson->string, name);
    }
    else
    {
        snprintf(spliceName, 100, "%s", name);
    }

    current_element = findJson->child;
    while(current_element != NULL)
    {
        if(cJSON_IsObject(current_element))
        {
            ret = API_GetObjectItem(current_element, name);
            if(ret != NULL)
            {
                return ret;
            }
        }
        else if(current_element->string != NULL && !strcmp(spliceName, current_element->string))
        {
            ret = current_element;
            return ret;
        }
        current_element = current_element->next;
    }

    return ret;
}

int API_GetObjectInt(cJSON *findJson, char *name)
{
    int retValue = 0;

    cJSON * value = API_GetObjectItem(findJson, name);
    if(cJSON_IsNumber(value))
    {
        retValue = value->valuedouble;
    }
    else if(cJSON_IsString(value))
    {
        if(!strcmp(value->valuestring, "true") || !strcmp(value->valuestring, "ON"))
        {
            retValue = 1;
        }
        else if(!strcmp(value->valuestring, "false") || !strcmp(value->valuestring, "OFF"))
        {
            retValue = 0;
        }
        else
        {
            retValue = atoi(value->valuestring);
        }
    }
    else if(cJSON_IsTrue(value))
    {
        retValue = 1;
    }
    else if(cJSON_IsFalse(value))
    {
        retValue = 0;
    }

    return retValue;
}

char* API_GetObjectString(cJSON *findJson, char *name)
{
    char* retValue = NULL;

    cJSON * tempValue = API_GetObjectItem(findJson, name);
    if(cJSON_IsString(tempValue))
    {
        retValue = tempValue->valuestring;
    }

    return retValue;
}

//返回：1匹配成功，0匹配失败
int RegexCheck(const char* pattern, const char* checkString)
{
	int errorCode;
	int result = 0;
	regex_t regex;
	regmatch_t pm;
    char ebuf[128];

	/* 编译正则表达式*/
	errorCode = regcomp(&regex, pattern, REG_EXTENDED|REG_NEWLINE);
	if (errorCode == 0)
	{
		/* 对每一行应用正则表达式进行匹配 */
		errorCode = regexec(&regex, checkString, 1, &pm, 0);
		if (errorCode == 0) 
		{
			/* 输出处理结果 */
			//ShellCmdPrintf("pm.rm_so = %d, pm.rm_eo = %d\n", pm.rm_so, pm.rm_eo);
			result = 1;
		}
	}

	if(result == 0)
	{
		regerror(errorCode, &regex, ebuf, sizeof(ebuf));
		//ShellCmdPrintf("regerror:%s\n", ebuf);
	}

	/* 释放正则表达式 */
	regfree(&regex);
    //dprintf("pattern=%s,checkString=%s,RegexCheck:%s\n", pattern, checkString, result ? "success" : "error");
	return result;
}

/*********************************************************************************************************
**  End Of File
**********************************************************************************************************/
