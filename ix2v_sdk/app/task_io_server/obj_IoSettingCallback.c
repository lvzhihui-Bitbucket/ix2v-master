/**
  ******************************************************************************
  * @file    obj_IoSettingCallback.c
  * @author  czb
  * @version V00.01.00
  * @date    2021.12.21
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2021 V-Tec</center></h2>
  ******************************************************************************
  */ 

#include "utility.h"
#include "cJSON.h"
#include "obj_IoSettingCallback.h"
#include "obj_IoInterface.h"
#include "task_Event.h"

void IO_SetVoiceVolume(cJSON *old, cJSON *new)
{
    CallVoice("Event_VOLUME_ADJUST");
}
#ifdef PID_IX850
void IO_KeyBeepDis_Update(cJSON *old, cJSON *new)
{
	UpdateKeyBeepEn();
}
#endif
#ifdef PID_IX821
void IO_M4DispImOnline_Update(cJSON *old, cJSON *new)
{
	API_Event_By_Name(EventM4LEDDisplayImOnline);//UpdateKeyBeepEn();
	API_Event_By_Name(Event_IXS_ReqUpdateTb);
}
#endif
void IO_SetEOC_WorkMode(cJSON *old, cJSON *new);
void IO_SetMenuType(cJSON *old, cJSON *new);
void IO_SetIX_ADDR(cJSON *old, cJSON *new);

void IO_List_Update(cJSON *old, cJSON *new);

void IO_SetIX_NAME(cJSON *old, cJSON *new);
void IO_SetL_NBR(cJSON *old, cJSON *new);
void IO_SetG_NBR(cJSON *old, cJSON *new);


IO_SettingCallback_S IO_SETTING_CB_TABLE[] = 
{
    {VOICE_VOLUME_IOID, IO_SetVoiceVolume},
    {EOC_WORK_MODE, IO_SetEOC_WorkMode},
    {MenuType, IO_SetMenuType},
    {IX_ADDR, IO_SetIX_ADDR},
#ifdef PID_IX850
    {MainListType, IO_List_Update},
    {RoomNumDispType, IO_List_Update},
    {ListSize, IO_List_Update},
    {ListColor, IO_List_Update},
    {ListLen, IO_List_Update},
    {KeyBeepDis, IO_KeyBeepDis_Update},
#endif
#ifdef PID_IX821
	{M4DispImOnline,IO_M4DispImOnline_Update},
#endif

    {IX_NAME, IO_SetIX_NAME},
    {L_NBR, IO_SetL_NBR},
    {G_NBR, IO_SetG_NBR},

};
const int  IO_SETTING_CB_TABLE_CNT = sizeof(IO_SETTING_CB_TABLE)/sizeof(IO_SETTING_CB_TABLE[0]);

int EventIO_SettingCallback(cJSON* cmd)
{
	int callback = GetEventItemInt(cmd, "CALLBACK");
    if(callback)
    {
        ((IO_SettingCallback)callback)(cJSON_GetObjectItemCaseSensitive(cmd, "OLD_VALUE"), cJSON_GetObjectItemCaseSensitive(cmd, "NEW_VALUE"));
    }
}

/*********************************************************************************************************
**  End Of File
**********************************************************************************************************/
