
#ifndef _OBJ_GPIO_H_
#define _OBJ_GPIO_H_

#if defined( PID_IX850 )
#define AMPMUTE_GPIO_PIN	33
#define RADAR_GPIO_PIN      117
#elif defined( PID_IX611 )
#define AMPMUTE_GPIO_PIN	33

#if defined( IX_LINK )
#define ID_UART_RX_GPIO_PIN	34          // IX611 used for ID-UART-RX
#define RMII_RST_GPIO_PIN	35          // RMII reset, USER INGORE!!
#define UNUSED36_GPIO_PIN	36          // IX611 unused
#define STATELED_GPIO_PIN	37          // IX611 used for TALK LED
#define UNUSED39_GPIO_PIN	39          // IX611 unused
#define CALLKEY_GPIO_PIN	38
#else
#define CMR7610_TX_GPIO_PIN	34          // CMR7610 UART_TX
#define CMR7610_RX_GPIO_PIN	35          // CMR7610 UART_RX
#define ID_UART_RX_GPIO_PIN	36          // DH611 used for ID-UART-RX
#define ID_UART_TX_GPIO_PIN	37          // DH611 used for ID-UART-TX
#define STATELED_GPIO_PIN	42          // DH611 used for TALK LED

#define EOC_LINK_GPIO_PIN	        93
#define EOC_RESET_GPIO_PIN	        95
#define EOC_WORK_MODE_GPIO_PIN	    94
#define CALLKEY_GPIO_PIN	40
#endif



#define OUTIN_GPIO_PIN	    73
#define MK_BL_GPIO_PIN	    82
#define RGBR_GPIO_PIN	    85
#define RGBG_GPIO_PIN	    86
#define RGBB_GPIO_PIN	    87
#define UNLOCK_GPIO_PIN	    88
#if 1
#define LEDPWM_GPIO_PIN	    89
#endif
#define SENSOR_POWER_GPIO_PIN  	90
#elif defined( PID_IX622 )
#define AMPMUTE_GPIO_PIN	33      // ok
#define OUTIN_GPIO_PIN	    80      // ok
#define MK_BL_GPIO_PIN	    89      // ok   // 1:on, 0:off
#define LED_TALK_PIN	    86      // ok   // 1:on, 0:off
#define LED_UNLOCK_PIN	    87      // ok   // 1:on, 0:off
//#define UNLOCK_GPIO_PIN	    79      // ok
#define LEDPWM_GPIO_PIN	    90      // nigheview    ok
#define SENSOR_POWER_GPIO_PIN  	88  // ok
#define KEY_CALL_1	        75      // key1
#define KEY_CALL_2	        76      // key2
#define KEY_CALL_3	        77      // key3
#define KEY_CALL_4	        78      // key4
#define LED_CALL_1	        71      // led1
#define LED_CALL_2	        72      // led2
#define LED_CALL_3	        73      // led3
#define LED_CALL_4	        74      // led4
#define RADAR_GPIO_PIN      40
#define CALLKEY_GPIO_PIN	KEY_CALL_1
#define EOC_LINK_GPIO_PIN	        RADAR_GPIO_PIN
#define EOC_RESET_GPIO_PIN	        81
#define EOC_WORK_MODE_GPIO_PIN	    37

#elif defined( PID_IX821 )
#define AMPMUTE_GPIO_PIN	33      // ok
//#define OUTIN_GPIO_PIN	    80      // ok
//#define MK_BL_GPIO_PIN	    89      // ok   // 1:on, 0:off
//#define LED_TALK_PIN	    86      // ok   // 1:on, 0:off
//#define LED_UNLOCK_PIN	    87      // ok   // 1:on, 0:off
//#define UNLOCK_GPIO_PIN	    79      // ok
//#define LEDPWM_GPIO_PIN	    90      // nigheview    ok
#define SENSOR_POWER_GPIO_PIN  	74  // ok
//#define KEY_CALL_1	        75      // key1
//#define KEY_CALL_2	        76      // key2
//#define KEY_CALL_3	        77      // key3
//#define KEY_CALL_4	        78      // key4
//#define LED_CALL_1	        71      // led1
//#define LED_CALL_2	        72      // led2
//#define LED_CALL_3	        73      // led3
//#define LED_CALL_4	        74      // led4
//#define RADAR_GPIO_PIN      40
//#define CALLKEY_GPIO_PIN	KEY_CALL_1
#define DIP_PIN1	        80      // key1
#define DIP_PIN2	        79      // key2
#define DIP_PIN3	        78      // key3
#define DIP_PIN4	        77      // key4
#define DIP_PIN5	        76      // led1
#define DIP_PIN6	        75      // led1

#define DIP_PIN7	        88      // key1
#define DIP_PIN8	        89      // key2

#define EOC_LINK_GPIO_PIN	        73
#define EOC_RESET_GPIO_PIN	        72
#define EOC_WORK_MODE_GPIO_PIN	    71

#else
#define AMPMUTE_GPIO_PIN	76
#define LCD_PWM_GPIO_PIN	86
#define LCD_PWM_SET() 		gpio_output(LCD_PWM_GPIO_PIN, 1)
#define LCD_PWM_RESET() 	gpio_output(LCD_PWM_GPIO_PIN, 0)

#endif


#if defined(PID_IX850)
//#define SENSOR_POWER_SET()      API_Bin_ctrl("SENSOR_PWN_EN",1)
//#define SENSOR_POWER_RESET()	API_Bin_ctrl("SENSOR_PWN_EN",0)
#define SENSOR_RESET_GPIO_PIN  	41
#define SENSOR_POWER_SET()	    {   API_Bin_ctrl("SENSOR_PWN_EN",1);    \
                                    usleep(200*1000);                        \
                                   gpio_output(SENSOR_RESET_GPIO_PIN, 1);   \
                                   }

#define SENSOR_POWER_RESET()	{ API_Bin_ctrl("SENSOR_PWN_EN",0); \
                                    usleep(200*1000);                        \
                                gpio_output(SENSOR_RESET_GPIO_PIN, 0);}
//#define Set_EOC_WorkMode(x)     (x)?1:0;//{gpio_output(EOC_WORK_MODE_GPIO_PIN,  (x) ? 1 : 0); gpio_output(EOC_RESET_GPIO_PIN, 0); usleep(50*1000); gpio_output(EOC_RESET_GPIO_PIN, 1);}
#define Set_EOC_WorkMode(x)     {API_Bin_ctrl("EOC_ROLE",(x) ? 1 : 0); API_Bin_ctrl("EOC_RST",0); usleep(50*1000); API_Bin_ctrl("EOC_RST",1);}

#elif defined(PID_IX611)
#define SENSOR_RESET_GPIO_PIN  	41
#ifndef DH_LINK
#define SENSOR_POWER_SET()	    {   gpio_output(SENSOR_POWER_GPIO_PIN, 1);    \
                                    usleep(2000*1000);                        \
                                    gpio_output(SENSOR_RESET_GPIO_PIN, 1); \
                                    usleep(200*1000);                        \
                                   gpio_output(SENSOR_RESET_GPIO_PIN, 0);  \
                                    usleep(200*1000);                        \
                                   gpio_output(SENSOR_RESET_GPIO_PIN, 1);\
					usleep(200*1000);	}

#define SENSOR_POWER_RESET()	{  gpio_output(SENSOR_POWER_GPIO_PIN, 0);\
                                    usleep(20*1000);                        \
                                gpio_output(SENSOR_RESET_GPIO_PIN, 0);}
#define Set_EOC_WorkMode(x)     (x)?1:0;
#else
#define SENSOR_POWER_SET()	{  gpio_output(SENSOR_POWER_GPIO_PIN, 1);\
                                    usleep(200*1000);                        \
                                gpio_output(SENSOR_RESET_GPIO_PIN, 1);}
#define SENSOR_POWER_RESET()	{   gpio_output(SENSOR_POWER_GPIO_PIN, 0);    \
                                    usleep(200*1000);                        \
                                gpio_output(SENSOR_RESET_GPIO_PIN, 0);}
#define Set_EOC_WorkMode(x)     {gpio_output(EOC_WORK_MODE_GPIO_PIN,  (x) ? 1 : 0); usleep(50*1000); gpio_output(EOC_RESET_GPIO_PIN, 0); usleep(100*1000); gpio_output(EOC_RESET_GPIO_PIN, 1);}
#endif

#define UNLOCK_SET()	        gpio_output(UNLOCK_GPIO_PIN, 1);//{ unlock_level_set(1); /*gpio_output(UNLOCK_GPIO_PIN, 1);*/}
#define UNLOCK_RESET()	        gpio_output(UNLOCK_GPIO_PIN, 0);//{ unlock_level_set(0); /*gpio_output(UNLOCK_GPIO_PIN, 0);*/}
#define NIGHTVIEW_TURN_ON()	    {}
#define NIGHTVIEW_TURN_OFF()	{}
#define STATE_LED_TURN_ON()	    {gpio_output(STATELED_GPIO_PIN, 0);}
#define STATE_LED_TURN_OFF()	{gpio_output(STATELED_GPIO_PIN, 1);}
#define RGB_R_TURN_SET()	    {gpio_output(RGBR_GPIO_PIN, 1);}
#define RGB_R_TURN_RESET()	    {gpio_output(RGBR_GPIO_PIN, 0);}
#define RGB_G_TURN_SET()	    {gpio_output(RGBG_GPIO_PIN, 1);}
#define RGB_G_TURN_RESET()	    {gpio_output(RGBG_GPIO_PIN, 0);}
#define RGB_B_TURN_SET()	    {gpio_output(RGBB_GPIO_PIN, 1);}
#define RGB_B_TURN_RESET()	    {gpio_output(RGBB_GPIO_PIN, 0);}
#define MK_BL_TURN_ON()	        {gpio_output(MK_BL_GPIO_PIN, 1);}
#define MK_BL_TURN_OFF()	    {gpio_output(MK_BL_GPIO_PIN, 0);}
//{gpio_output(EOC_WORK_MODE_GPIO_PIN,  (x) ? 1 : 0); gpio_output(EOC_RESET_GPIO_PIN, 0); usleep(50*1000); gpio_output(EOC_RESET_GPIO_PIN, 1);}

#elif defined(PID_IX622)

#define SENSOR_RESET_GPIO_PIN  	41
#define SENSOR_POWER_SET()	{  gpio_output(SENSOR_POWER_GPIO_PIN, 1);\
                                    usleep(200*1000);                        \
                                gpio_output(SENSOR_RESET_GPIO_PIN, 1);}

#define SENSOR_POWER_RESET()	{  gpio_output(SENSOR_POWER_GPIO_PIN, 0);\
                                    usleep(200*1000);                        \
                                gpio_output(SENSOR_RESET_GPIO_PIN, 0);}

#define UNLOCK_SET()	        { unlock_level_set(1); /*gpio_output(UNLOCK_GPIO_PIN, 1);*/}
#define UNLOCK_RESET()	        { unlock_level_set(0); /*gpio_output(UNLOCK_GPIO_PIN, 0);*/}

#define NIGHTVIEW_TURN_ON()	    { /*gpio_output(LEDPWM_GPIO_PIN, 1);*/}
#define NIGHTVIEW_TURN_OFF()	{ /*gpio_output(LEDPWM_GPIO_PIN, 0);*/}

#define TALK_LED_TURN_ON()	    {gpio_output(LED_TALK_PIN, 1);}
#define TALK_LED_TURN_OFF()	    {gpio_output(LED_TALK_PIN, 0);}

#define STATE_LED_TURN_ON()	    {gpio_output(LED_UNLOCK_PIN, 1);}
#define STATE_LED_TURN_OFF()	{gpio_output(LED_UNLOCK_PIN, 0);}

#define LED_CALL_X_SET(x)	    {gpio_output(LED_CALL_1+x, 1);}     // turn off
#define LED_CALL_X_RESET(x)	    {gpio_output(LED_CALL_1+x, 0);}     // turn on

#define Set_EOC_WorkMode(x)     {gpio_output(EOC_WORK_MODE_GPIO_PIN,  (x) ? 1 : 0); usleep(50*1000); gpio_output(EOC_RESET_GPIO_PIN, 0); usleep(100*1000); gpio_output(EOC_RESET_GPIO_PIN, 1);}

#elif defined(PID_IX821)

#define SENSOR_RESET_GPIO_PIN  	41
#if 0
#define SENSOR_POWER_SET()	{    API_Bin_ctrl("SENSOR_PWN_EN",1);    \
                                    usleep(200*1000);                        \
                                gpio_output(SENSOR_RESET_GPIO_PIN, 1);}

#define SENSOR_POWER_RESET()	{    API_Bin_ctrl("SENSOR_PWN_EN",0);    \
                                    usleep(200*1000);                        \
                                gpio_output(SENSOR_RESET_GPIO_PIN, 0);}
#else
#define SENSOR_POWER_SET()	{   gpio_output(SENSOR_POWER_GPIO_PIN, 1);    \
                                    usleep(200*1000);                        \
                                gpio_output(SENSOR_RESET_GPIO_PIN, 1);}

#define SENSOR_POWER_RESET()	{   gpio_output(SENSOR_POWER_GPIO_PIN, 0);    \
                                    usleep(200*1000);                        \
                                gpio_output(SENSOR_RESET_GPIO_PIN, 0);}
#endif

//#define UNLOCK_SET()	        { unlock_level_set(1); /*gpio_output(UNLOCK_GPIO_PIN, 1);*/}
//#define UNLOCK_RESET()	        { unlock_level_set(0); /*gpio_output(UNLOCK_GPIO_PIN, 0);*/}

#define NIGHTVIEW_TURN_ON()	    { /*gpio_output(LEDPWM_GPIO_PIN, 1);*/}
#define NIGHTVIEW_TURN_OFF()	{ /*gpio_output(LEDPWM_GPIO_PIN, 0);*/}

#define TALK_LED_TURN_ON()	    
#define TALK_LED_TURN_OFF()	   

#define STATE_LED_TURN_ON()	   
#define STATE_LED_TURN_OFF()	

#define LED_CALL_X_SET(x)	    
#define LED_CALL_X_RESET(x)	  

#define Set_EOC_WorkMode(x)     {gpio_output(EOC_WORK_MODE_GPIO_PIN,  (x) ? 1 : 0); usleep(50*1000); gpio_output(EOC_RESET_GPIO_PIN, 0); usleep(100*1000); gpio_output(EOC_RESET_GPIO_PIN, 1);}

#else 

#define SENSOR_POWER_GPIO_PIN  	31
#define SENSOR_RESET_GPIO_PIN  	32
#define SENSOR_POWER_SET()	    {gpio_output(SENSOR_POWER_GPIO_PIN, 1);      \
                                    gpio_output(SENSOR_RESET_GPIO_PIN, 1); \
                                    usleep(20*1000);                        \
                                   gpio_output(SENSOR_RESET_GPIO_PIN, 0);  \
                                    usleep(20*1000);                        \
                                   gpio_output(SENSOR_RESET_GPIO_PIN, 1);}

#define SENSOR_POWER_RESET()	{gpio_output(SENSOR_POWER_GPIO_PIN, 0);  \
                                gpio_output(SENSOR_RESET_GPIO_PIN, 0);}
#endif

#endif

#define AMP_TALK_MASK		1
#define AMP_RING_MASK		2
#define AMP_BEEP_MASK		4

#define AMP_TALK_OPEN()	    api_amp_open(AMP_TALK_MASK)
#define AMP_TALK_CLOSE()	api_amp_close(AMP_TALK_MASK)

#define AMP_RING_OPEN()	    api_amp_open(AMP_RING_MASK)
#define AMP_RING_CLOSE()	api_amp_close(AMP_RING_MASK)

#define AMP_BEEP_OPEN()	    api_amp_open(AMP_BEEP_MASK)
#define AMP_BEEP_CLOSE()	api_amp_close(AMP_BEEP_MASK)

#define AMPMUTE_SET() 		gpio_output(AMPMUTE_GPIO_PIN, 1)
#define AMPMUTE_RESET() 	gpio_output(AMPMUTE_GPIO_PIN, 0)

void gpio_initial(void);
void gpio_deinitial(void);
int gpio_output(int pin, int hi);
int gpio_input(int pin);
