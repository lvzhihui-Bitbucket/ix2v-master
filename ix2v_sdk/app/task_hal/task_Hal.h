/**
  ******************************************************************************
  * @file    task_Hal.h
  * @author  zeng
  * @version V1.0.0
  * @date    2012.09.20
  * @brief   This file contains the headers of the power task_Power.
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2012 V-Tec</center></h2>
  ******************************************************************************
  */ 
#ifndef TASK_HAL_H
#define TASK_HAL_H

#include "task_survey.h"
#include "obj_gpio.h"

extern int hal_fd;

void start_services(void);
void start_services_ok(void);

#endif
