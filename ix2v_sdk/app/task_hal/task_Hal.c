
#include "task_Hal.h"
#include "vdp_uart.h" 
#include "elog.h"
#include <time.h>  
#include "obj_PublicInformation.h" 
#include "task_Event.h"
#include "task_Beeper.h"
#include "obj_UartBusiness.h"
#include "obj_IoInterface.h"
#include "define_file.h"
#include "obj_KeypadLed.h"
#include "obj_MyCtrlLed.h"

int hal_fd;
int hal_fd2;
static unsigned char app_state = 0;
static unsigned char buttonEffective = 0;

#define DATE_TIME_2022_01_01_00_00_01		1659761273UL

void KeyPad_Manage(unsigned char keyValue);

#if defined(PID_IX850)
static int radar_status = -1;
static pthread_t task_radar_status;

void read_radar_status_thread( void )
{
	int dat;
	printf("read_radar_status_thread...\n");
	sleep(2);
	radar_modules_init();
	sleep(1);
	while(1)
	{
		dat = gpio_input(RADAR_GPIO_PIN);
		if( radar_status != dat)
		{
			radar_status = dat;
			//printf("radar_status changed[0x%08x]\n",radar_status);
			//ShellCmdPrintf("radar_status changed[0x%08x]",radar_status);
			if( radar_status )
				Radar_Trig_ScrBr();//BEEP_ERROR();
			//else
			//	BEEP_INFORM();
			RadarStateChange(radar_status);
		}
		usleep(200*1000);
	}
}
int Get_Radar_Status(void)
{
	return radar_status;
}
#endif

#if defined(PID_IX622)
static int radar_status = -1;
static pthread_t task_radar_status;

void read_radar_status_thread( void )
{
	int dat;
	API_PublicInfo_Write_Bool(PB_RADAR_MODULE, false);
	while(1)
	{
		dat = gpio_input(RADAR_GPIO_PIN);
		if( radar_status != dat)
		{
			radar_status = dat;
			//printf("radar_status changed[0x%08x]\n",radar_status);
			if( radar_status )
				Radar_Trig_ScrBr();//BEEP_ERROR();
			API_PublicInfo_Write_Bool(PB_RADAR_MODULE, true);
		}
		usleep(200*1000);
	}
}
#endif

#if defined(PID_IX611)
#include <linux/input.h>
#include "virt_io.h"
static pthread_t task_tpkey;
static int keyType = 0;

//return 0--kp, 1--mk
int GetKeyType(void)
{
	return keyType;
}

//kpOrMk 0--kp, 1--mk
static void IX611_KeypadProcess(int kpOrMk, int keystate, int keycode)
{
	//还没启动完成，不处理键盘消息
	if(!app_state)
	{
		return;
	}

	if(keyType != kpOrMk)
	{
		keyType = kpOrMk;
		API_Para_Write_String(IX611_KEY_TYPE, keyType ? "MK" : "KP");
	}
	char* pbEocMacState = API_PublicInfo_Read_String(PB_EOC_Mac_State);
	char* pbEocState = API_PublicInfo_Read_String(PB_EOC_state);
	if(pbEocMacState&&strcmp(pbEocMacState, "Ok")!=0)
	{
		
		if(!IfHasSn())
		{	
			if(keystate)
			{
				BEEP_KEY();
				API_Event_By_Name(EventReqSNFromServer);
				
			}
			return;
		}
		if(strcmp(pbEocState,"unlink")!=0&& strcmp(pbEocMacState, "Ok")!=0)
		{
			if(keystate)
			{
				BEEP_KEY();
				API_Event_By_Name(EventSetEocMacBySN);
			}
			return;
		}
	}
	if(keystate)
	{
		if(GetKeyPadBackLightState() || kpOrMk)
		{
			BEEP_KEY();
		}
	}
	else
	{
		if(GetKeyPadBackLightState() || kpOrMk)
		{
			KeyPad_Manage(keycode);
		}
		else
		{
			SetKeyPadBackLightState(1);
		}
	}
}

void read_cap1203_touchkey_thread( void )
{
    struct input_event in;
	while(1)
	{
		if( read(hal_fd, &in, sizeof(struct input_event)) > 0) 
		{
			if(in.type == EV_KEY) 
			{
				printf("read touch key[0x%02x],state[%d]\n",in.code, in.value);

				#if defined(PID_IX611)
				IX611_KeypadProcess(0, in.value, in.code);
				#endif
			}
		}
		else
		{
			printf("read_cap1203_touchkey_thread ERROR!!!!!!!!!!!!\n");
			break;		
		}
	}
}

const uint16 ix611_keycode_tab[] =
{
        //row0
        '1', '4', '7', '*',
        //row1
        '2', '5', '8', '0',
        //row2
        '3', '6', '9', '#',
};


static pthread_t task_matrixkey;
void read_matrixkey_thread( void )
{
    struct input_event in;
	int index,value;
	while(1)
	{
		if( read(hal_fd2, &in, sizeof(struct input_event)) > 0) 
		{
			if(in.type == EV_MSC) 
			{
				index = (in.value>>4)&0x0f;
				value = (in.value&0x0f)?1:0;
				printf("read matrix key(0x%02x),state[%d]\n", ix611_keycode_tab[index], value );
				IX611_KeypadProcess(1, value, ix611_keycode_tab[index]);
			}
		}
		else
		{
			printf("read_matrixkey_thread ERROR!!!!!!!!!!!!\n");
			break;		
		}
	}
}
#if defined(PID_IX611)
static pthread_t task_callkey;
static int callkey_status = -1;
static int outkey_status = -1;
void read_callkey_outkey_thread( void )
{
	int callkey_tmp;
	int outkey_tmp;
	while(1)
	{
		if(app_state)
		{
			callkey_tmp = gpio_input( CALLKEY_GPIO_PIN );
			outkey_tmp = gpio_input( OUTIN_GPIO_PIN );
			if( callkey_tmp == gpio_input( CALLKEY_GPIO_PIN ) && callkey_tmp != callkey_status )
			{
				callkey_status = callkey_tmp;
				if( !callkey_status )
				{
					// call key pressed to do something
					printf("call key pressed!\n");
					BEEP_KEY();
					char* pbEocMacState = API_PublicInfo_Read_String(PB_EOC_Mac_State);
					char* pbEocState = API_PublicInfo_Read_String(PB_EOC_state);
					if(pbEocMacState&&strcmp(pbEocMacState, "Ok")!=0)
					{
						if(!IfHasSn())
						{
							API_Event_By_Name(EventReqSNFromServer);
							usleep(200*1000);
							continue;
						}
						if(strcmp(pbEocState,"unlink")!=0&& strcmp(pbEocMacState, "Ok")!=0)
						{
							API_Event_By_Name(EventSetEocMacBySN);
							usleep(200*1000);
							continue;
						}
					}
					
					API_Event_By_Name(EventIX_HomeCall);
					
				}
				else
				{
					// call key release to do something
					printf("call key release!\n");
				}
			}
			if( outkey_tmp == gpio_input( OUTIN_GPIO_PIN ) && outkey_tmp != outkey_status )
			{
				outkey_status = outkey_tmp;
				if( !outkey_status )
				{
					// out key pressed to do something
					printf("out key pressed!\n");
					cJSON *json = cJSON_CreateObject();
					cJSON_AddStringToObject(json, UART_HAL_EVENT, "HAL_EVENT_EXIT_KEY1");
					cJSON_AddStringToObject(json, UART_KEY_STATUS, "KEY_STATUS_PRESS");
					cJSON_AddStringToObject(json, UART_HAL_ID, GetMyTime());
					UartRecvHalEventProcess(json);
					cJSON_Delete(json);
				}
				else
				{
					// out key release to do something
					printf("out key release!\n");
				}
			}
		}
		usleep(200*1000);
	}
}
#endif

#endif
#if defined(PID_IX821)
static pthread_t task_dip;
//extern int gpio_input_pins[6];
int dip_input_pins[]	={DIP_PIN1,DIP_PIN2,DIP_PIN3,DIP_PIN4,DIP_PIN5, DIP_PIN6,DIP_PIN7,DIP_PIN8};
#define get_dsnum(dip)	((dip&0x07)==0?8:(dip&0x07))
#define get_m4keysort(dip)	((dip&0x10)?1:0)
#define get_m4keymapping(dip)	((dip&0x08)?1:0)
#define get_keymoduletype(dip)	((dip&0x20)?1:0)
#define get_dipunlocktime(dip)	((dip&0x40)?1:0)
#define get_dipprogmode(dip)	((dip&0x80)?1:0)
void read_dip_thread( void )
{
	int i,dat,cnt,dat1;
	int dip_cnt=sizeof(dip_input_pins)/sizeof(int);
	int one_read_flag=0;
	printf("read_ixg_addr_thread...\n");
	sleep(1);
	//dip_cnt=6;
	API_PublicInfo_Write_Int(PB_DIP_INT_DATA,-1);
	while(1)
	{
		dat = 0;
		for( i = 0; i< dip_cnt; i++ )
		{
			dat <<= 1;
			dat |= (!gpio_input(dip_input_pins[dip_cnt-1-i]));
		}
		usleep(100*1000);
		dat1 = 0;
		for( i = 0; i< dip_cnt; i++ )
		{
			dat1 <<= 1;
			dat1 |= (!gpio_input(dip_input_pins[dip_cnt-1-i]));
		}
		//dat+=1;
		
		if( dat1==dat &&API_PublicInfo_Read_Int(PB_DIP_INT_DATA) != dat )
		{
			//ixg_addr = dat;
			printf("DIP changed[0x%08x]\n",dat);
			printf("get_keymoduletype(dat):%d:\n",get_keymoduletype(dat),((dat&0x20)?1:0));
			//get_keymoduletype(dat)
			//ShellCmdPrintf("DIP changed[0x%08x]",dat);
			API_PublicInfo_Write_Int(PB_DIP_INT_DATA,dat);
			#if 1
			if(one_read_flag==0)
			{
				char buff[11];
				sprintf(buff,"%s0000%02d",GetSysVerInfo_bd(),get_dsnum(dat));
				if(strcmp(buff,API_Para_Read_String2(IX_ADDR))!=0)
					API_Para_Write_String(IX_ADDR, buff);
				
				if(strcmp(get_dsnum(dat)==1?"master":"slave",API_Para_Read_String2(EOC_WORK_MODE))!=0)
					API_Para_Write_String(EOC_WORK_MODE, get_dsnum(dat)==1?"master":"slave");
				
			}
			#endif
			if(API_PublicInfo_Read_Int(PB_M4_KEY_SORT)!=get_m4keysort(dat))
				API_PublicInfo_Write_Int(PB_M4_KEY_SORT,get_m4keysort(dat));
			if(API_PublicInfo_Read_Int(PB_M4_KEY_MAPPING)!=get_m4keymapping(dat))
				API_PublicInfo_Write_Int(PB_M4_KEY_MAPPING,get_m4keymapping(dat));
			if(API_PublicInfo_Read_Int(PB_KEY_MODULE_TYPE)!=get_keymoduletype(dat))
				API_PublicInfo_Write_Int(PB_KEY_MODULE_TYPE,get_keymoduletype(dat));
			printf("2222get_keymoduletype(dat):%d:\n",get_keymoduletype(dat),((dat&0x20)?1:0));

			#if 0
			if(one_read_flag==0||API_PublicInfo_Read_Int(PB_DIP_UNLOCK_TIME_SET)!=get_dipunlocktime(dat))
			{
				API_PublicInfo_Write_Int(PB_DIP_UNLOCK_TIME_SET,get_dipunlocktime(dat));
				int lock_time=(get_dipunlocktime(dat)==0?3:1);
				if(API_Para_Read_Int("RL1_TIMER")!=lock_time)
					API_Para_Write_Int("RL1_TIMER",lock_time);
				if(API_Para_Read_Int("RL2_TIMER")!=lock_time)
					API_Para_Write_Int("RL2_TIMER",lock_time);
					
			}
			#else
			if(API_PublicInfo_Read_Int(PB_DIP_UNLOCK_TIME_SET)!=get_dipunlocktime(dat))
			{
				API_PublicInfo_Write_Int(PB_DIP_UNLOCK_TIME_SET,get_dipunlocktime(dat));
				
				if(one_read_flag&&get_dipunlocktime(dat))
				{
					API_PublicInfo_Write_Int(PB_CardSetup_State, 3);
					API_Event_By_Name(EventManageCard);
				}
			}
			#endif
			
			if(API_PublicInfo_Read_Int(PB_PROG_MODE)!=get_dipprogmode(dat))
			{
				API_PublicInfo_Write_Int(PB_PROG_MODE,get_dipprogmode(dat));
				if(one_read_flag&&get_dipprogmode(dat))
					API_Event_By_Name(EventUpdateResFromSD);
			}
			if(get_dipprogmode(dat))
				BEEP_KEY();
			if(one_read_flag)
			{
				char* pbEocMacState = API_PublicInfo_Read_String(PB_EOC_Mac_State);
				char* pbEocState = API_PublicInfo_Read_String(PB_EOC_state);
				if(pbEocMacState)
				{
					if(!IfHasSn())
					{
						API_Event_By_Name(EventReqSNFromServer);
						return;
					}
					if(strcmp(pbEocState,"unlink")!=0&& strcmp(pbEocMacState, "Ok")!=0)
					{
						API_Event_By_Name(EventSetEocMacBySN);
						return;
					}
				}
			}
			else
				one_read_flag=1;
		}
		usleep(500*1000);
	}
}
#endif
#if defined(PID_IX622)
int API_EventMainCall(int callnumber)
{
	char temp[11];
	int ret;

	cJSON* jsonEvent = cJSON_CreateObject();
	snprintf(temp, 11, "%02d", callnumber);
	cJSON_AddStringToObject(jsonEvent, EVENT_KEY_EventName, EventIX_HomeCall);
	cJSON_AddStringToObject(jsonEvent, IX2V_IX_ADDR, temp);
	cJSON_AddNumberToObject(jsonEvent, PB_IX_CALL_KEY, callnumber);

	ret = API_Event_Json(jsonEvent);
	cJSON_Delete(jsonEvent);

	return ret;
}

static pthread_t task_callkey;
static int outkey_status = -1;
static int eocLinkState = -1;

void get_gpio_input(int* pdat, int len)
{	
	int i;
	for( i = 0; i < len; i++ )
	{
		pdat[i] = gpio_input( KEY_CALL_1 + i );
	}	
}

// lzh_20240903_s	// for ix622 testing
static int dtsend_time_cnt = 0;
// lzh_20240903_s
static int dsAddrSetState = 0;

static int ExitDsAddrSetState(int timing)
{
	if(dsAddrSetState)
	{
		dsAddrSetState =0;
		API_MyLedCtrl(MY_LED_LOCK, MY_LED_OFF, 0);
	}
	return 2;
}

void read_callkey_outkey_thread( void )
{
	#define CALL_KEY_TIMER_BASE      50
	#define CALL_KEY_TIME(x)      ((x)*1000/CALL_KEY_TIMER_BASE)
	int i;
	int callkey_status[4] = {-1,-1,-1,-1};
	int callkey_tmp[4];
	int callkeyTimeCnt[4] = {0,0,0,0};
	int outkey_tmp;
	//int outkey_valid=-1;
	while(1)
	{
		get_gpio_input(callkey_tmp,4);
		for( i = 0; i < 4; i++ )
		{
			if(callkey_tmp[i] == gpio_input(KEY_CALL_1+i))
			{
				if(callkey_tmp[i] != callkey_status[i])
				{
					callkey_status[i] = callkey_tmp[i];
					if(callkey_status[i] && callkeyTimeCnt[i] > 0)
					{
						log_i("call key %d release, callkeyTimeCnt[%d] = %d",i, i, callkeyTimeCnt[i]);
						if(callkeyTimeCnt[i] < CALL_KEY_TIME(3))
						{
							if(i == 0 && dsAddrSetState)
							{
								//设置主机地址
								char bd_rm_ms[11] = {0};
								int addr = atoi(GetSysVerInfo_Ms());
								addr++;
								if(addr > 8 || addr < 1)
								{
									addr = 1;
								}
								snprintf(bd_rm_ms, 11, "00990000%02d", addr);
								SetSysVerInfo_BdRmMs(bd_rm_ms);
								API_MyLedCtrl(MY_LED_TALK, MY_LED_FLASH_FAST, addr);
								API_Add_TimingCheck(ExitDsAddrSetState, 30);
							}
							else
							{
								if(buttonEffective)
								{
									char* pbEocMacState = API_PublicInfo_Read_String(PB_EOC_Mac_State);
									char* pbEocState = API_PublicInfo_Read_String(PB_EOC_state);
									int filter_flag=0;
									if(pbEocMacState)
									{
										if(!IfHasSn())
										{
											API_Event_By_Name(EventReqSNFromServer);
											filter_flag=1;
										}
										else if(strcmp(pbEocState,"unlink")!=0&& strcmp(pbEocMacState, "Ok")!=0)
										{
											API_Event_By_Name(EventSetEocMacBySN);
											filter_flag=1;
										}
									}
									if(filter_flag==0)
									{
										API_EventMainCall(i+1);
									}
								}
							}
						}
						else if(callkeyTimeCnt[i] >= CALL_KEY_TIME(3) && callkeyTimeCnt[i] < CALL_KEY_TIME(6))
						{
							if(i == 0)
							{
								if(!API_PublicInfo_Read_Bool(PB_StartUp5MinutesLater) && !dsAddrSetState)
								{
									dsAddrSetState = 1;
									API_MyLedCtrl(MY_LED_LOCK, MY_LED_ON, 0);
									API_Add_TimingCheck(ExitDsAddrSetState, 30);
								} 
								else if(dsAddrSetState)
								{
									dsAddrSetState = 0;
									API_MyLedCtrl(MY_LED_LOCK, MY_LED_OFF, 0);
									API_Del_TimingCheck(ExitDsAddrSetState);
								}
								else
								{
									API_MyLedCtrl(MY_LED_CALL1+i, MY_LED_OFF, 3);
								}
							}
							else if(i == 1 || i == 2)
							{
								if(!API_PublicInfo_Read_Bool(PB_StartUp5MinutesLater))
								{
									//修改订制表定义的默认值
									int changeFlag = 0;
									char temp[500];
									snprintf(temp, 500, "%s/%s%s", IO_Cus_Path, MY_DEV_NAME, IO_Cus_FILE_NAME);
									cJSON* cusJson = GetJsonFromFile(temp);
									//按下的是第3个按键，按键2和按键3的呼叫目标交换
									if(i == 2)
									{
										if(cusJson)
										{
											if(!cJSON_GetObjectItemCaseSensitive(cusJson, HOME_KEY_IX_CALL_NBR2) && !cJSON_GetObjectItemCaseSensitive(cusJson, HOME_KEY_IX_CALL_NBR3))
											{
												cJSON_AddItemToObject(cusJson, HOME_KEY_IX_CALL_NBR2, cJSON_Duplicate(API_Para_Read_Default(HOME_KEY_IX_CALL_NBR3), 1));
												cJSON_AddItemToObject(cusJson, HOME_KEY_IX_CALL_NBR3, cJSON_Duplicate(API_Para_Read_Default(HOME_KEY_IX_CALL_NBR2), 1));
												changeFlag = 1;
											}
										}
										else
										{
											cusJson = cJSON_CreateObject();
											cJSON_AddItemToObject(cusJson, HOME_KEY_IX_CALL_NBR2, cJSON_Duplicate(API_Para_Read_Default(HOME_KEY_IX_CALL_NBR3), 1));
											cJSON_AddItemToObject(cusJson, HOME_KEY_IX_CALL_NBR3, cJSON_Duplicate(API_Para_Read_Default(HOME_KEY_IX_CALL_NBR2), 1));
											changeFlag = 1;
										}
									}
									//按下的是第2个按键，按键顺序不变
									else if(i == 1)
									{
										if(cusJson)
										{
											if(cJSON_GetObjectItemCaseSensitive(cusJson, HOME_KEY_IX_CALL_NBR2))
											{
												cJSON_DeleteItemFromObjectCaseSensitive(cusJson, HOME_KEY_IX_CALL_NBR2);
												changeFlag = 1;
											}

											if(cJSON_GetObjectItemCaseSensitive(cusJson, HOME_KEY_IX_CALL_NBR3))
											{
												cJSON_DeleteItemFromObjectCaseSensitive(cusJson, HOME_KEY_IX_CALL_NBR3);
												changeFlag = 1;
											}
										}
									}

									if(changeFlag)
									{
										MakeDir(IO_Cus_Path);
										SetJsonToFile(temp, cusJson);
										API_ReInitIOServer();
										BEEP_CONFIRM();
									}
									else
									{
										BEEP_ERROR();
									}
									cJSON_Delete(cusJson);
								}
								else
								{
									API_MyLedCtrl(MY_LED_CALL1+i, MY_LED_OFF, 3);
								}
							}
							else
							{
								API_MyLedCtrl(MY_LED_CALL1+i, MY_LED_OFF, 3);
							}
						}
						else if(callkeyTimeCnt[i] >= CALL_KEY_TIME(6) && callkeyTimeCnt[i] < CALL_KEY_TIME(9))
						{
							if(i == 0)
							{
								if(!API_PublicInfo_Read_Bool(PB_StartUp5MinutesLater))
								{
									API_PublicInfo_Write_Int(PB_CardSetup_State, 3);
									API_Event_By_Name(EventManageCard);
								}
								else
								{
									API_MyLedCtrl(MY_LED_CALL1+i, MY_LED_OFF, 3);
								}
							}
							else
							{
								API_MyLedCtrl(MY_LED_CALL1+i, MY_LED_OFF, 3);
							}
						}
						else if(callkeyTimeCnt[i] >= CALL_KEY_TIME(9) && callkeyTimeCnt[i] < CALL_KEY_TIME(12))
						{
							if(i == 0)
							{
								if(!API_PublicInfo_Read_Bool(PB_StartUp5MinutesLater))
								{
									API_Event_UpdateStart(API_Para_Read_String2(FW_UpdateServer), API_Para_Read_String2(FW_UpdateCode), 1, "call key 1");
								}
								else
								{
									API_MyLedCtrl(MY_LED_CALL1+i, MY_LED_OFF, 3);
								}
							}
							else
							{
								API_MyLedCtrl(MY_LED_CALL1+i, MY_LED_OFF, 3);
							}
						}
						else if(callkeyTimeCnt[i] >= CALL_KEY_TIME(12) && callkeyTimeCnt[i] < CALL_KEY_TIME(15))
						{
							if(i == 0)
							{
								if(!API_PublicInfo_Read_Bool(PB_StartUp5MinutesLater))
								{
									FactoryDefaultProcess();
								}
								else
								{
									API_MyLedCtrl(MY_LED_CALL1+i, MY_LED_OFF, 3);
								}
							}
							else
							{
								API_MyLedCtrl(MY_LED_CALL1+i, MY_LED_OFF, 3);
							}
						}
					}
					callkeyTimeCnt[i] = 0;
				}
				else
				{
					if(!callkey_status[i])
					{							
						switch (callkeyTimeCnt[i])
						{
							case 0:
							case CALL_KEY_TIME(3):
							case CALL_KEY_TIME(6):
							case CALL_KEY_TIME(9):
							case CALL_KEY_TIME(12):
								BEEP_KEY();
								API_MyLedCtrl(MY_LED_CALL1+i, MY_LED_OFF, 1);
								log_i("call key %d pressed %ds", i, callkeyTimeCnt[i]*CALL_KEY_TIMER_BASE/1000);
								break;
							case CALL_KEY_TIME(15):
								BEEP_ERROR();
								API_MyLedCtrl(MY_LED_CALL1+i, MY_LED_OFF, 3);
								log_i("call key %d pressed %ds", i, callkeyTimeCnt[i]*CALL_KEY_TIMER_BASE/1000);
								break;
							
							default:
								break;
						}
						callkeyTimeCnt[i]++;
					}
				}
			}
		}
		outkey_tmp = gpio_input( OUTIN_GPIO_PIN );
		if( outkey_tmp == gpio_input( OUTIN_GPIO_PIN ) && outkey_tmp != outkey_status )
		{
			outkey_status = outkey_tmp;
			if( !outkey_status )
			{
				// out key pressed to do something
				printf("out key pressed!\n");
				cJSON *json = cJSON_CreateObject();
				cJSON_AddStringToObject(json, UART_HAL_EVENT, "HAL_EVENT_EXIT_KEY1");
				cJSON_AddStringToObject(json, UART_KEY_STATUS, "KEY_STATUS_PRESS");
				cJSON_AddStringToObject(json, UART_HAL_ID, GetMyTime());
				UartRecvHalEventProcess(json);
				cJSON_Delete(json);
			}
			else
			{
				// out key release to do something
				printf("out key release!\n");
			}
		}
		
		outkey_tmp = gpio_input( EOC_LINK_GPIO_PIN );
		if( outkey_tmp == gpio_input( EOC_LINK_GPIO_PIN ) && outkey_tmp != eocLinkState )
		{
			eocLinkState = outkey_tmp;
			char* linkState = API_PublicInfo_Read_String(PB_EOC_LINK_STATE);
			char* eocWorkMode = API_Para_Read_String2(EOC_WORK_MODE);
			//从未连接到连接状态,并且工作在salve模式下
			if((!linkState || strcmp(linkState, "Linked")) && eocLinkState && (!eocWorkMode || strcmp(eocWorkMode, "master")))
			{
				API_Set_Lan_Config(NULL);
			}
			API_PublicInfo_Write_String(PB_EOC_LINK_STATE, eocLinkState ? "Linked" : "Unlink");
			API_PublicInfo_Write_String(PB_EOC_LINK_CHANGE_TIME, get_time_string());
		}
		usleep(CALL_KEY_TIMER_BASE*1000);
		// lzh_20240903_s	// for ix622 testing
		if( ++dtsend_time_cnt >= 20*3 )		// 3s
		{
			dtsend_time_cnt = 0;
			//system("echo unlock > /dev/dtsnd");			
		}
		// lzh_20240903_s	// for ix622 testing
	}
}

#endif

void gpio_initial(void);

void start_services(void)
{
	struct timeval 	tv;  
	MakeDir(LOG_DIR);
	gpio_initial();

//雷达
#if defined(PID_IX850) || defined(PID_IX622)
	printf("starting radar status addr...\n");
	pthread_create(&task_radar_status,NULL,(void*)read_radar_status_thread,NULL);
#endif

//DS2411
#if defined(PID_IX850) || defined(PID_IX622)|| defined(PID_IX821)
	hal_fd = open("/dev/ds2411",O_RDWR);

	if( hal_fd == -1 )
	{
		hal_fd = open("/dev/minictrl",O_RDWR);
	}
#endif

//触摸按键 ix482se-event1(touchkey), ix611-event1(touchkey)&event2(matrixckey)
#if defined(PID_IX611)
	hal_fd = open("/dev/input/event0",O_RDWR);
	if( hal_fd != -1 )
	{
		printf("open dev input event0 \n");
		pthread_create(&task_tpkey,NULL,(void*)read_cap1203_touchkey_thread,NULL);
	}

	hal_fd2 = open("/dev/input/event1",O_RDWR);
	if( hal_fd2 != -1 )
	{
		printf("open dev input event1 \n");
		pthread_create(&task_tpkey,NULL,(void*)read_matrixkey_thread,NULL);
	}
#endif

//call key
#if defined(PID_IX611) || defined(PID_IX622)
	pthread_create(&task_callkey,NULL,(void*)read_callkey_outkey_thread,NULL);
#endif
#if defined(PID_IX821)
pthread_create(&task_dip,NULL,(void*)read_dip_thread,NULL);
#endif

    gettimeofday(&tv, NULL);
	if(tv.tv_sec < DATE_TIME_2022_01_01_00_00_01)
	{
		tv.tv_sec = DATE_TIME_2022_01_01_00_00_01;
		if(settimeofday(&tv, NULL)== -1)
		{
			printf("settimeofday error:%s\n", strerror(errno)); 
		}
		else
		{
			system("hwclock -w");
		}
	}	
}

static int StartUpFiveMinutesLater(int timing)
{
	//dprintf("StartUpFiveMinutesLater %d\n", timing);
	API_PublicInfo_Write_Bool(PB_StartUp5MinutesLater, 1);
	return 2;
}

void start_services_ok(void)
{
	if(!app_state)
	{
		while (!OS_GetIfTaskStartCompleted())
		{
			sleep(1);
		}

	#if defined(PID_IX611)
		char* keypadType = API_Para_Read_String2(IX611_KEY_TYPE);
		if(keypadType && (!strcmp(keypadType, "MK") || atoi(keypadType)))
		{
			keyType = 1;
		}
		else
		{
			keyType = 0;
		}
		KeyPad_Manage_Init(0);
		NightVisionPwm_Init();
		MyLedCtrlInit();
	#endif
		API_PublicInfo_Write_String(PB_System_State, "Standby");
		int time = API_Para_Read_Int(SettingTimeout);
		time = (time ? time : 300);
		API_Add_TimingCheck(StartUpFiveMinutesLater, time);
		CheckHaveCustomerizedSound();
		app_state = 1;
		api_uart_send_pack(UART_TYPE_N2S_REPOTR_APP_STATUS, &app_state, 1);

	#if defined(PID_IX611) || defined(PID_IX622)//|| defined(PID_IX821)
		API_Event_Unlock2("RL1", 0, "Powner on");
	#endif
		API_Event_By_Name(EventStartComplete);
		log_i("Start up complete.");
		API_PublicInfo_Write_String(PB_APP_UP_TIME, TimeToString(GetSystemBootTime()));
	}
}

#if defined(PID_IX622)
//IX622底板测试程序
static int IX622test(int timing)
{
	char* systemState = API_PublicInfo_Read_String(PB_System_State);
	if(systemState && !strcmp(systemState, "Standby"))
	{
		API_EventMainCall(1);
	}
	return 1;
}
#endif

void IX622_StartCompleted(void)
{
	#if defined(PID_IX622)
	if(!buttonEffective)
	{
		buttonEffective = 1;
		NightVisionPwm_Init();
		CmrDevManager_init();
		API_PublicInfo_Write_String(PB_APP_UP_TIME, TimeToString(GetSystemBootTime()));

		API_MyLedCtrl(MY_LED_CALL1, MY_LED_ON, 0);
		API_MyLedCtrl(MY_LED_CALL2, MY_LED_ON, 0);
		API_MyLedCtrl(MY_LED_CALL3, MY_LED_ON, 0);
		API_MyLedCtrl(MY_LED_CALL4, MY_LED_ON, 0);
		API_MyLedCtrl(MY_LED_NAMEPLATE, MY_LED_ON, 0);

		//IX622底板测试程序
		//IX622test(0);
		//API_Add_TimingCheck(IX622test, 10);
		//IX622底板测试程序
	}
	#endif
}

#if defined(PID_IX821)
void IX821_StartCompleted(void)
{
	static int one_flag=0;
	if(!one_flag)
	{
		one_flag=1;
		CmrDevManager_init();
		API_Event_By_Name(EventM4LEDDisplayBySort);
	}
}
#endif
