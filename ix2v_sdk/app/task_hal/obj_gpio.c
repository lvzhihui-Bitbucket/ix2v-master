
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <pthread.h>

#include "obj_gpio.h"
#include "obj_IoInterface.h"

#define GPIO_EXPORT			"/sys/class/gpio/export"
#define GPIO_UNEXPORT		"/sys/class/gpio/unexport"
#define GPIO_DIRECTION		"/sys/class/gpio/gpio%d/direction"
#define GPIO_PULLENABLE		"/sys/class/gpio/gpio%d/pull_enable"
#define GPIO_VALUE			"/sys/class/gpio/gpio%d/value"

#if defined(PID_IX850)
static int gpio_output_pins[2]	={AMPMUTE_GPIO_PIN,SENSOR_RESET_GPIO_PIN};
int gpio_input_pins[1]	={RADAR_GPIO_PIN};
#endif
#if defined(PID_IX611)
//static int gpio_output_pins[10]	={AMPMUTE_GPIO_PIN,SENSOR_POWER_GPIO_PIN,STATELED_GPIO_PIN,MK_BL_GPIO_PIN,RGBR_GPIO_PIN,RGBG_GPIO_PIN,RGBB_GPIO_PIN,UNLOCK_GPIO_PIN,LEDPWM_GPIO_PIN,SENSOR_RESET_GPIO_PIN};
//static int gpio_output_pins[9]	={AMPMUTE_GPIO_PIN,SENSOR_POWER_GPIO_PIN,STATELED_GPIO_PIN,MK_BL_GPIO_PIN,RGBR_GPIO_PIN,RGBG_GPIO_PIN,RGBB_GPIO_PIN,UNLOCK_GPIO_PIN,SENSOR_RESET_GPIO_PIN};	
#if defined( IX_LINK )
static int gpio_output_pins[]	={AMPMUTE_GPIO_PIN,SENSOR_POWER_GPIO_PIN,STATELED_GPIO_PIN,MK_BL_GPIO_PIN,RGBR_GPIO_PIN,RGBG_GPIO_PIN,RGBB_GPIO_PIN,SENSOR_RESET_GPIO_PIN,UNLOCK_GPIO_PIN};	
static int gpio_input_pins[]	={CALLKEY_GPIO_PIN,OUTIN_GPIO_PIN};
#else
static int gpio_output_pins[]	={AMPMUTE_GPIO_PIN,SENSOR_POWER_GPIO_PIN,STATELED_GPIO_PIN,MK_BL_GPIO_PIN,RGBR_GPIO_PIN,RGBG_GPIO_PIN,RGBB_GPIO_PIN,SENSOR_RESET_GPIO_PIN,UNLOCK_GPIO_PIN,EOC_RESET_GPIO_PIN,EOC_WORK_MODE_GPIO_PIN};	
static int gpio_input_pins[]	={CALLKEY_GPIO_PIN,OUTIN_GPIO_PIN,EOC_LINK_GPIO_PIN};
#endif
#endif

#if defined(PID_IX622)
static int gpio_output_pins[12]	={AMPMUTE_GPIO_PIN,SENSOR_POWER_GPIO_PIN,MK_BL_GPIO_PIN,LED_TALK_PIN,LED_UNLOCK_PIN,SENSOR_RESET_GPIO_PIN,LED_CALL_1,LED_CALL_2,LED_CALL_3,LED_CALL_4,EOC_RESET_GPIO_PIN,EOC_WORK_MODE_GPIO_PIN};	
int gpio_input_pins[6]	={KEY_CALL_1,KEY_CALL_2,KEY_CALL_3,KEY_CALL_4,OUTIN_GPIO_PIN, RADAR_GPIO_PIN};
//int gpio_input_pins[5]	={KEY_CALL_1,KEY_CALL_2,KEY_CALL_3,KEY_CALL_4,OUTIN_GPIO_PIN};
#endif
#if defined(PID_IX821)
static int gpio_output_pins[]	={AMPMUTE_GPIO_PIN,SENSOR_RESET_GPIO_PIN,EOC_RESET_GPIO_PIN,EOC_WORK_MODE_GPIO_PIN,SENSOR_POWER_GPIO_PIN};	
int gpio_input_pins[]	={DIP_PIN1,DIP_PIN2,DIP_PIN3,DIP_PIN4,DIP_PIN5, DIP_PIN6,DIP_PIN7, DIP_PIN8,EOC_LINK_GPIO_PIN};
//int gpio_input_pins[5]	={KEY_CALL_1,KEY_CALL_2,KEY_CALL_3,KEY_CALL_4,OUTIN_GPIO_PIN};
#endif

#if defined(PID_IX611) || defined(PID_IX622)
void NightVisionPwm_Init( void )
{
	
	char cmd[100];
	if(access("/usr/modules/night_vision_drv.ko", F_OK) == 0)
	{
		if(access("/dev/night_vision", F_OK) == 0)
			system("rmmod night_vision_drv");
		system("insmod /usr/modules/night_vision_drv.ko");
		strcpy(cmd,"echo stop > /dev/night_vision ");
		system(cmd);
		//sync();
	}
	else
	{
		FILE *p=NULL;
		p = fopen(GPIO_EXPORT,"w");
		fprintf(p,"%d",LEDPWM_GPIO_PIN);
		fclose(p);
		
		snprintf(cmd, 200, GPIO_DIRECTION, LEDPWM_GPIO_PIN);
		p = fopen(cmd,"w");
		fprintf(p,"out");
		fclose(p);

		gpio_output(LEDPWM_GPIO_PIN, 0);
	}
	
	//Sound_Disable();
}

int GetNvDuty(void)
{
	int lev;
	cJSON *duty=NULL;
	if(API_Para_Read_Int("IO_SENSOR_TYPE")!=3)
	{
		lev=API_Para_Read_Int(NightvisionLevel);
		duty=API_ReadPara(NightvisionDuty);
	}
	else
	{
		lev=API_Para_Read_Int(CMR7610NightvisionLevel);
		duty=API_ReadPara(CMR7610NightvisionDuty);
	}
	if(duty==NULL)
		return 90;
	cJSON *node=cJSON_GetArrayItem(duty,lev);
	if(node==NULL)
		return 90;
	int rev=node->valueint;
	if(rev>99||rev<0)
		return 90;
	return rev;
		
}
/*------------------------------------------------------------------------
						Sound_Enable
------------------------------------------------------------------------*/
void NightVisionPwm_Enable( void )
{
	if(access("/dev/night_vision", F_OK) == 0)
	{
		int duty=GetNvDuty();
		char cmd[100];
		sprintf(cmd,"echo set 1000 %d > /dev/night_vision",duty*10);
		system(cmd);
		sprintf(cmd,"echo start > /dev/night_vision");
		system(cmd);
		
	}
	else
	{
		#if !defined(PID_IX821)
		gpio_output(LEDPWM_GPIO_PIN, 1);
		#endif
	}
}

/*------------------------------------------------------------------------
						Sound_Off
------------------------------------------------------------------------*/
void NightVisionPwm_Disable( void )
{
	//printf("Beep  >>>beep_run.state = %d.\n", pwn_off );

	if(access("/dev/night_vision", F_OK) == 0)
	{
		char cmd[100];
		sprintf(cmd,"echo stop > /dev/night_vision");
		system(cmd);
	}
	else
	{
		#if !defined(PID_IX821)
		gpio_output(LEDPWM_GPIO_PIN, 0);
		#endif
	}

	//ioctl( hal_fd, PWN_OUT_SET, &pwn_off );
}
#endif

#if defined(PID_IX622)

//onOrNormal 1 -- ON, 0 -- Normal
int GetNameplateLightDuty(int onOrNormal)
{
	int ret;
	cJSON *node = cJSON_GetArrayItem(API_ReadPara(NightvisionDuty), API_Para_Read_Int(onOrNormal ? NightvisionLevel : NameplateLevel) - 1);
	if(cJSON_IsNumber(node))
	{
		ret = node->valueint;
	}
	else
	{
		ret = onOrNormal ? 100 : 50;
	}
	return ret;
}

void NameplateLight_Ctrl(int duty)
{
	if(duty <= 0)
	{
		if(access("/dev/back_vision", F_OK) == 0)
		{
			char cmd[100];
			sprintf(cmd,"echo stop > /dev/back_vision");
			system(cmd);
		}
		else
		{
			#if !defined(PID_IX821)
			gpio_output(MK_BL_GPIO_PIN, 0);
			#endif
		}
	}
	else
	{
		if(access("/dev/back_vision", F_OK) == 0)
		{
			char cmd[100];
			duty = duty*10;
			sprintf(cmd,"echo set 1000 %d > /dev/back_vision", duty >= 1000 ? 999 : duty);
			system(cmd);
			sprintf(cmd,"echo start > /dev/back_vision");
			system(cmd);
		}
		else
		{
			#if !defined(PID_IX821)
			gpio_output(MK_BL_GPIO_PIN, 1);
			#endif
		}
	}
}
#endif


void gpio_initial(void)
{

	char temp[201];	
	FILE *p=NULL;
	int i;
	int mcuLockMode;
	// add output pin
	for( i = 0; i< sizeof(gpio_output_pins)/sizeof(int); i++ )
	{
		p = fopen(GPIO_EXPORT,"w");
		fprintf(p,"%d",gpio_output_pins[i]);
		fclose(p);		
	}
	
	// set output pin
	for( i = 0; i< sizeof(gpio_output_pins)/sizeof(int); i++ )
	{
		snprintf(temp, 200, GPIO_DIRECTION, gpio_output_pins[i]);
		p = fopen(temp,"w");
		fprintf(p,"out");
		fclose(p);
		#if defined(PID_IX611)
		switch(gpio_output_pins[i])
		{
			case STATELED_GPIO_PIN:
				STATE_LED_TURN_OFF()
				break;
			case LEDPWM_GPIO_PIN:
				NIGHTVIEW_TURN_OFF()
				break;
			#ifdef DH_LINK
			case EOC_RESET_GPIO_PIN:
				gpio_output(gpio_output_pins[i], 1);
				break;		
			case EOC_WORK_MODE_GPIO_PIN:
				gpio_output(gpio_output_pins[i], 1);
				break;	
			#endif
		}
		#elif defined(PID_IX622)
		switch(gpio_output_pins[i])
		{
			case LED_UNLOCK_PIN:
				STATE_LED_TURN_OFF()
				break;
			case LED_TALK_PIN:
				TALK_LED_TURN_OFF();
				break;
			case LEDPWM_GPIO_PIN:
				NIGHTVIEW_TURN_OFF()
				break;
			case MK_BL_GPIO_PIN:
				//NameplateLight_Ctrl(0);
				break;
			case LED_CALL_1:
			case LED_CALL_2:
			case LED_CALL_3:
			case LED_CALL_4:
				LED_CALL_X_SET(gpio_output_pins[i]-LED_CALL_1);
				break;

			case EOC_RESET_GPIO_PIN:
				gpio_output(gpio_output_pins[i], 1);
				break;		
			case EOC_WORK_MODE_GPIO_PIN:
				gpio_output(gpio_output_pins[i], 1);
				break;
		}
		#elif defined(PID_IX821)
		switch(gpio_output_pins[i])
		{
			case EOC_RESET_GPIO_PIN:
				gpio_output(gpio_output_pins[i], 1);
				break;		
			case EOC_WORK_MODE_GPIO_PIN:
				gpio_output(gpio_output_pins[i], 1);
				break;
		}
		#endif
	}
#if defined(PID_IX611) || defined(PID_IX850)
	// add input pin
	for( i = 0; i< sizeof(gpio_input_pins)/sizeof(int); i++ )
	{
		p = fopen(GPIO_EXPORT,"w");
		fprintf(p,"%d",gpio_input_pins[i]);
		fclose(p);		
	}
	// set input pin
	for( i = 0; i< sizeof(gpio_input_pins)/sizeof(int); i++ )
	{
		snprintf(temp, 200, GPIO_DIRECTION, gpio_input_pins[i]);
		p = fopen(temp,"w");
		fprintf(p,"in");
		fclose(p);	
	}
	// pull down enable
	for( i = 0; i< sizeof(gpio_input_pins)/sizeof(int); i++ )
	{
		if( gpio_input_pins[i] != 0 )
		{
			snprintf(temp, 200, GPIO_PULLENABLE, gpio_input_pins[i]);
			p = fopen(temp,"w");
			fprintf(p,"1");
			fclose(p);	
		}	
	}

#endif

#if defined(PID_IX622)||defined(PID_IX821)
	// add input pin
	for( i = 0; i< sizeof(gpio_input_pins)/sizeof(int); i++ )
	{
		p = fopen(GPIO_EXPORT,"w");
		fprintf(p,"%d",gpio_input_pins[i]);
		fclose(p);		
	}
	// set input pin
	for( i = 0; i< sizeof(gpio_input_pins)/sizeof(int); i++ )
	{
		snprintf(temp, 200, GPIO_DIRECTION, gpio_input_pins[i]);
		p = fopen(temp,"w");
		fprintf(p,"in");
		fclose(p);	
	}
	// pull down enable
	for( i = 0; i< sizeof(gpio_input_pins)/sizeof(int); i++ )
	{
		if( gpio_input_pins[i] != 0 )
		{
			snprintf(temp, 200, GPIO_PULLENABLE, gpio_input_pins[i]);
			p = fopen(temp,"w");
			#if defined(PID_IX821)
			if(gpio_input_pins[i] == EOC_LINK_GPIO_PIN)
			#else
			if(gpio_input_pins[i] == RADAR_GPIO_PIN)
			#endif
			{
				fprintf(p,"0");
			}
			else
			{
				fprintf(p,"1");
			}
			fclose(p);	
		}	
	}
#endif

}

void gpio_deinitial(void)
{
	FILE *p=NULL;
	int i;
	// del output pin
	for( i = 0; i< sizeof(gpio_output_pins)/sizeof(int); i++ )
	{
		p = fopen(GPIO_UNEXPORT,"w");
		fprintf(p,"%d",gpio_output_pins[i]);
		fclose(p);		
	}
}

int gpio_output(int pin, int hi)
{
	char temp[201];	
	FILE *p=NULL;
	// set pin value
	snprintf(temp, 200, GPIO_VALUE, pin);
	p = fopen(temp,"w");
	fprintf(p,"%d",hi);
	fclose(p);
	return 0;
}

int gpio_input(int pin)
{
	int ret;
	char temp[201];	
	FILE *p=NULL;
	// read pin value
	snprintf(temp, 200, GPIO_VALUE, pin);
	p = fopen(temp,"r");
	// seek to start
	fseek(p , 0 , 0);
	fread(temp , 1, 1 ,p);
	temp[1]=0;
	ret = atoi(temp);		
	fclose(p);
	
	return ret;
}
#if defined(PID_IX850) || defined(PID_IX622)

void radar_gpio_reset(void)
{
	#if defined(PID_IX850)
	char temp[201];	
	FILE *p=NULL;
	
	p = fopen(GPIO_EXPORT,"w");
	fprintf(p,"%d",RADAR_GPIO_PIN);
	fclose(p);		

// set input pin


	snprintf(temp, 200, GPIO_DIRECTION, RADAR_GPIO_PIN);
	p = fopen(temp,"w");
	fprintf(p,"in");
	fclose(p);	

	snprintf(temp, 200, GPIO_PULLENABLE, RADAR_GPIO_PIN);
	p = fopen(temp,"w");
	fprintf(p,"1");
	fclose(p);
	#endif
}
#endif
/////////////////////////////
#include "RTOS.h"
#include "OSQ.h"
#include "OSTIME.h"

static int amp_mute_mask=0;
static int dt_stack_mute=0;
static time_t dt_stack_mute_start;
pthread_mutex_t ampmute_lock = PTHREAD_MUTEX_INITIALIZER;
OS_TIMER dt_stack_mute_timer;
void dt_stack_mute_timer_callback(void)
{
	API_StackMute_Off();
}
int dt_stack_mute_timer_start(void)
{
	static unsigned char init_flag=0;
	if(init_flag==0)
	{
		OS_CreateTimer( &dt_stack_mute_timer, dt_stack_mute_timer_callback, 600/25);
		init_flag=1;
	}
	OS_RetriggerTimer( &dt_stack_mute_timer );	
}
void api_amp_open(int mask)
{
	pthread_mutex_lock(&ampmute_lock);
	amp_mute_mask|=mask;
	if((amp_mute_mask&AMP_TALK_MASK)&&dt_stack_mute)
	{
		if((time(NULL)-dt_stack_mute_start)>5)
		{
			dt_stack_mute=0;

			AMPMUTE_SET();
		}
	}
	else
		AMPMUTE_SET();
	pthread_mutex_unlock(&ampmute_lock);
}
void api_amp_close(int mask)
{
	pthread_mutex_lock(&ampmute_lock);
	amp_mute_mask&=(~mask);
	if(amp_mute_mask==0)
		AMPMUTE_RESET();
	pthread_mutex_unlock(&ampmute_lock);
}
void API_StackRecvMute_On(void)
{
	pthread_mutex_lock(&ampmute_lock);
	dt_stack_mute=1;
	dt_stack_mute_start=time(NULL);
	if(amp_mute_mask&AMP_TALK_MASK)
		AMPMUTE_RESET();
	dt_stack_mute_timer_start();
	pthread_mutex_unlock(&ampmute_lock);	
	
}
void API_StackSendMute_On(void)
{
	int delay_flag=0;
	pthread_mutex_lock(&ampmute_lock);
	dt_stack_mute=1;
	dt_stack_mute_start=time(NULL);
	if(amp_mute_mask&AMP_TALK_MASK)
	{
		delay_flag=1;
		AMPMUTE_RESET();
	}
	dt_stack_mute_timer_start();
	pthread_mutex_unlock(&ampmute_lock);
	if(delay_flag)
		usleep(100*1000);
}
void API_StackMute_Off(void)
{
	pthread_mutex_lock(&ampmute_lock);
	dt_stack_mute=0;
	if(amp_mute_mask!=0)
		AMPMUTE_SET();
	pthread_mutex_unlock(&ampmute_lock);	
}
int Get_Dt_Stack_Mute(void)
{
	return dt_stack_mute;
}