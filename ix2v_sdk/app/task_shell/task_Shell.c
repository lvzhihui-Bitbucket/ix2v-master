/**
  ******************************************************************************
  * @file    task_Shell.c
  * @author  czb
  * @version V00.01.00
  * @date    2022.2.11
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 

#include <stdio.h>
#include <sys/sysinfo.h>
#include "task_Shell.h"
#include "task_survey.h"
#include "define_file.h"

#define SHELL_KEY_callBack			"callBack"
#define SHELL_KEY_describe			"describe"


SH_CMD_RUN_S shellRun = {.data = NULL, .outputFile = NULL, .task.task_run_flag = 0, .XD_IP = 0};

const char* GetShellCmdInputString(const cJSON *input, int index)
{
	char* ret;
	ret = cJSON_GetStringValue(cJSON_GetArrayItem(input, index));
	return (ret == NULL ? "" : ret);
}

cJSON* GetShellCmdInputJson(const cJSON *input, int index)
{
	char* string = GetShellCmdInputString(input, index);
	cJSON* ret = cJSON_Parse(string);
	if((string != NULL) && (ret == NULL))
	{
		ShellCmdPrintf("Input NO.%d JSON data error: %s\n", index+1, string);
	}

	return ret;
}

cJSON* ShellGetJsonFromFile(const char* filePath)
{
	FILE* file = NULL;
	char* json = NULL;
	cJSON* retJson = NULL;
	char absoluteFilePath[SHELL_OUTPUT_LEN] = {0};
	char temp[SHELL_OUTPUT_LEN];

	if(filePath)
	{
		if(filePath[0] == '/')
		{
			strcpy(absoluteFilePath, filePath);
		}
		else
		{
			getcwd(temp, SHELL_OUTPUT_LEN);
			snprintf(absoluteFilePath, SHELL_OUTPUT_LEN, "%s/%s", temp, filePath);
		}
	}
	else
	{
		return retJson;
	}
	
	if((file=fopen(absoluteFilePath,"r")) != NULL)
	{
		fseek(file,0, SEEK_END);
		
		int size = ftell(file);
		
		if((json = malloc(size+1)) == NULL )
		{
			fclose(file);
			ShellCmdPrintf("File %s is too large.\n", absoluteFilePath);
			return retJson;
		}

		fseek(file,0, SEEK_SET);
		
		fread(json, 1, size, file);
		
		json[size] = 0;
		
		fclose(file);

		retJson = cJSON_Parse(json);
		if(retJson == NULL)
		{
			ShellCmdPrintf("File %s is error.\n", absoluteFilePath);
		}

		free(json);
	}
	else
	{
		ShellCmdPrintf("File %s is not exist.\n", absoluteFilePath);
	}

	return retJson;
}

const char* GetShellCmdJsonString(const cJSON *cmdJson, const char* key)
{
	return cJSON_GetStringValue(cJSON_GetObjectItemCaseSensitive(cmdJson, key));
}

cJSON * CmdLineToCjson(const char* cmdLine)
{
	if(cmdLine == NULL)
	{
		return NULL;
	}

	cJSON * cmdJson = cJSON_CreateArray();
    char data[SHELL_INPUT_LEN+1];
	int len, i, dataIndex;

	for(len = strlen(cmdLine), data[0] = 0, dataIndex = 0, i = 0; i < len; i++)
	{
		if(cmdLine[i] == '\\' && i+1 < len && (cmdLine[i+1] == ' ' || cmdLine[i+1] == '\t' || cmdLine[i+1] == '\n' || cmdLine[i+1] == '\r' ))
		{
			if(dataIndex < SHELL_INPUT_LEN)
			{
				data[dataIndex++] = cmdLine[++i];
			}
		}
		else if(cmdLine[i] == ' ' || cmdLine[i] == '\t' || cmdLine[i] == '\n' || cmdLine[i] == '\r')
		{
			data[dataIndex] = 0;
			if(dataIndex)
			{
				cJSON_AddItemToArray(cmdJson, cJSON_CreateString(data));
				dataIndex = 0;
				data[dataIndex] = 0;
			}
		}
		else
		{
			if(dataIndex < SHELL_INPUT_LEN)
			{
				data[dataIndex++] = cmdLine[i];
			}
		}
	}

	if(dataIndex)
	{
		data[dataIndex] = 0;
		cJSON_AddItemToArray(cmdJson, cJSON_CreateString(data));
	}

	//MyPrintJson(cmdLine, cmdJson);

	return cmdJson;
}
static int ShellCmdProcess(int ip, ShellCmdSource source, char* cmdLine)
{
	cJSON * cmd = NULL;
	cJSON * callBack = NULL;
	char* inputCmdStr;
	char output[SHELL_OUTPUT_LEN];
	int writeFileFlag = 0;
	long long timeConsuming = time_since_last_call(-1);

	cJSON *inputCmd = CmdLineToCjson(cmdLine);
	int cmdSize = cJSON_GetArraySize(inputCmd);

	shellRun.source = source;
	switch (source)
	{
		case SourceUDP:
			shellRun.UDP_IP = ip;
			break;
		case SourceIXD:
			shellRun.XD_IP = ip;
			break;

		default:
			break;
	}

	inputCmdStr = GetShellCmdInputString(inputCmd, cmdSize - 1);
	if(inputCmdStr != NULL)
	{
		if(!strncmp(inputCmdStr, ">>", strlen(">>")))
		{
			shellRun.outputFile = fopen(inputCmdStr + strlen(">>"),"a+");
			writeFileFlag = 1;
		}
		else if(!strncmp(inputCmdStr, ">", strlen(">")))
		{
			shellRun.outputFile = fopen(inputCmdStr + strlen(">"),"w+");
			writeFileFlag = 1;
		}

		if(writeFileFlag)
		{
			cJSON_DeleteItemFromArray(inputCmd, cmdSize - 1);
		}
	}

	if(writeFileFlag == 0)
	{
		inputCmdStr = GetShellCmdInputString(inputCmd, cmdSize - 2);
		if(inputCmdStr != NULL)
		{
			if(!strcmp(inputCmdStr, ">>"))
			{
				shellRun.outputFile = fopen(GetShellCmdInputString(inputCmd, cmdSize - 1),"a+");
				writeFileFlag = 1;
			}
			else if(!strcmp(inputCmdStr, ">"))
			{
				shellRun.outputFile = fopen(GetShellCmdInputString(inputCmd, cmdSize - 1),"w+");
				writeFileFlag = 1;
			}

			if(writeFileFlag)
			{
				cJSON_DeleteItemFromArray(inputCmd, cmdSize - 1);
				cJSON_DeleteItemFromArray(inputCmd, cmdSize - 2);
			}
		}
	}

	inputCmdStr = GetShellCmdInputString(inputCmd, 0);
	cJSON_ArrayForEach(cmd, shellRun.data)
	{
		if(inputCmdStr != NULL && !strcmp(inputCmdStr, cmd->string))
		{
			callBack = cJSON_GetObjectItemCaseSensitive(cmd, SHELL_KEY_callBack);
			if(((ShellCmdCallBack)(callBack->valueint)) != NULL)
			{
				((ShellCmdCallBack)(callBack->valueint))(inputCmd);
			}
			inputCmdStr = NULL;
			break;
		}
	}

	if(inputCmdStr)
	{
		ShellCmdPrintf("%s is not found!!!\n", inputCmdStr);
	}

	if(shellRun.outputFile)
	{
		fclose(shellRun.outputFile);
		shellRun.outputFile = NULL;
	}

	getcwd(output, SHELL_OUTPUT_LEN);

	timeConsuming = time_since_last_call(timeConsuming);

	ShellCmdPrintf("%s # time:%dms\n", output, timeConsuming/1000);
	cJSON_Delete(inputCmd);

	return 1;
}

void ShellCmdUartInputProcess(void)
{
    char input[1000];

	if(0)
	{
		if(fgets(input, 1000, stdin))
		{
			API_ShellCommand(0, SourceUart, input);
		}
	}
	else
	{
		sleep(10);
	}
}

void vdp_shell_mesg_data_process(char* msg_data,int len)
{
	MsgShell* pMsgShell = (MsgShell*)msg_data;

   // cJSON * cmdJson = cJSON_Parse(pMsgShell->data);

	ShellCmdProcess(pMsgShell->ip, pMsgShell->source, pMsgShell->data);

//	cJSON_Delete(cmdJson);
}

void* vdp_shell_task( void* arg )
{
	vdp_task_t*	 ptask 		= (vdp_task_t*)arg;
	p_vdp_common_buffer pdb 	= 0;
	int	size;
	thread_log_add(__func__);
	while( ptask->task_run_flag )
	{
		size = pop_vdp_common_queue( ptask->p_msg_buf, &pdb, 0);
		if( size > 0 )
		{
			(*ptask->p_msg_buf->process)(pdb,size);
			purge_vdp_common_queue( ptask->p_msg_buf );
		}
	}
	return 0;
}

int ShellCmd_Help(cJSON *cmd)
{
	cJSON* element;
	char* cmdStr;

	switch(cJSON_GetArraySize(cmd))
	{
		case 1:
			cJSON_ArrayForEach(element, shellRun.data)
			{
				ShellCmdPrintf("%s:\n\t%s\n", element->string, cJSON_GetStringValue(cJSON_GetObjectItemCaseSensitive(element, SHELL_KEY_describe)));
			}
			break;
		case 2:
			cmdStr = GetShellCmdInputString(cmd, 1);
			element = cJSON_GetObjectItemCaseSensitive(shellRun.data, cmdStr);
			if(element)
			{
				ShellCmdPrintf("%s:\n\t%s\n", element->string, cJSON_GetStringValue(cJSON_GetObjectItemCaseSensitive(element, SHELL_KEY_describe)));
			}
			else
			{
				ShellCmdPrintf("Not find %s!!!\n", cmdStr);
			}
			break;

		default:
			ShellCmdPrintf("Input error!!!\n");
			break;
	}

	return 1;
}

int GetShellTaskState(void)
{
	return shellRun.task.task_run_flag;
}

int ShellCmd_Task(cJSON *cmd)
{
	char* cmdStr = GetShellCmdInputString(cmd, 1);

	if(cmdStr == NULL)
	{
		ShellCmdPrintf("Please input task -c/-o/-s\n");
	}
	else if(!strcmp(cmdStr, "-c"))
	{
		if(GetShellTaskState())
		{
			exit_vdp_common_queue(&shellRun.msgQ);
			exit_vdp_common_task(&shellRun.task);
		}
		ShellCmdPrintf("Close shell task\n");
	}
	else if(!strcmp(GetShellCmdInputString(cmd, 1), "-o"))
	{
		if(!GetShellTaskState())
		{
			init_vdp_common_queue(&shellRun.msgQ, 10000, (msg_process)vdp_shell_mesg_data_process, &shellRun.msgQ);
			init_vdp_common_task(&shellRun.task, MSG_ID_SHELL, vdp_shell_task, &shellRun.msgQ, NULL);
		}
		ShellCmdPrintf("Open shell task\n");
	}
	else if(!strcmp(GetShellCmdInputString(cmd, 1), "-s"))
	{
		ShellCmdPrintf(GetShellTaskState() ? "Shell task is open\n" : "Shell task is close\n");
	}

	return 1;
}

int task_ShellInit(void)
{
	MakeDir(SHELL_PATH);
	chdir(SHELL_PATH);
	ShellCmdAdd("help", ShellCmd_Help, "List all commands");
	ShellCmdAdd("task", ShellCmd_Task, "Control shell task: -o(open), -c(close), -s(status)");
	ShellCmdInit();
}

int ShellCmdAdd(const char* cmd, ShellCmdCallBack callBack, const char* describe)
{
	cJSON *temp;
	cJSON *cmdObj;

    if(shellRun.data == NULL)
	{
		shellRun.data = cJSON_CreateObject();
	}

	temp = cJSON_GetObjectItemCaseSensitive(shellRun.data, cmd);
	if(temp == NULL)
	{
		cmdObj = cJSON_CreateObject();
		cJSON_AddItemToObject(cmdObj, SHELL_KEY_callBack, cJSON_CreateNumber((int)callBack));
		cJSON_AddItemToObject(cmdObj, SHELL_KEY_describe, cJSON_CreateString(describe));
		cJSON_AddItemToObject(shellRun.data, cmd, cmdObj);
		return 1;
	}

	return 0;
}

int ShellCmdDelete(char* cmd)
{
    if(shellRun.data != NULL)
	{
		cJSON_DeleteItemFromObjectCaseSensitive(shellRun.data, cmd);
	}

	return 1;
}

void ShellReport(char* printInfo)
{
	if(shellRun.XD_IP)
	{
		IxReportShell(shellRun.XD_IP, printInfo);
	}
}

static void ShellCmdPrintString(char* printInfo)
{
	if(shellRun.outputFile)
	{
		fputs(printInfo, shellRun.outputFile);
	}
	else
	{
		if(shellRun.source == SourceUart)
		{
			printf("%s", printInfo);
		}
		else if(shellRun.source == SourceUDP)
		{
			if(shellRun.UDP_IP)
			{
				api_udp_shell_send_data(shellRun.UDP_IP, printInfo);
			}
		}
		else if(shellRun.source == SourceIXD)
		{
			if(shellRun.XD_IP)
			{
				IxReportShell(shellRun.XD_IP, printInfo);
			}
		}
	}
}


int ShellCmdPrintf(const char* format, ...)
{
	char output[SHELL_OUTPUT_LEN] = {0};
	va_list valist;
	int ret;

    va_start(valist, format);
	ret = vsnprintf(output, SHELL_OUTPUT_LEN, format, valist);
    va_end(valist);

	ShellCmdPrintString(output);

	return ret;
}

void ShellCmdPrintJson(cJSON* json)
{
	char* output;

	output = cJSON_Print(json);
	if(output)
	{
		ShellCmdPrintString(output);
		free(output);
		ShellCmdPrintString("\n");
	}
}

int API_ShellCommand(int ip, ShellCmdSource source, const char* cmd)
{
	MsgShell msg;
	int len;
	int ret = 0;
	
	if(shellRun.task.task_run_flag&&GetAutoTestState()==0)
	{
		len = strlen(cmd);
		if(len > SHELL_INPUT_LEN)
		{
			return ret;
		}

		msg.ip 			= ip;
		msg.source 		= source;
		strcpy(msg.data, cmd);

		len += (sizeof(ip) + sizeof(source) + 1);
		ret = !push_vdp_common_queue(&shellRun.msgQ,  &msg, len);
	}
	else
	{
		ret = ShellCmdProcess(ip, source, cmd);
	}

    return ret;
}

/*********************************************************************************************************
**  End Of File
*********************************************************************************************************/

