/**
  ******************************************************************************
  * @file    obj_ShellCmd.c
  * @author  czb
  * @version V00.01.00
  * @date    2022.2.11
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 

#include <dirent.h>
#include <sys/stat.h>
#include "task_Shell.h"
#include "define_string.h"
#include "obj_LedCtrl.h"
#include "obj_IoInterface.h"
#include "obj_PublicInformation.h"
#include "elog_forcall.h"
#include "obj_video_cs_shelltest.h"
#include "obj_PublicMulticastCmd.h"
#include "ota_client_ifc.h"
#include "obj_MDS_Output.h"
#include <regex.h>
#include "obj_lvgl_msg.h"
#include "obj_IXS_Proxy.h"
#include "obj_Ftp.h"   
#include "obj_http_cmd.h"
#include "utility.h"

#ifdef true
#undef true
#endif
#define true ((cJSON_bool)1)

#ifdef false
#undef false
#endif
#define false ((cJSON_bool)0)


int ShellCmd_LedCtrl(cJSON *cmd)
{
	if(cJSON_GetArraySize(cmd) >= 3)
	{
		int ledId = 0;
		char* ledIdString = GetShellCmdInputString(cmd, 1);
		int ledCtrl = -1;
		char* ledCtrlString = GetShellCmdInputString(cmd, 2);

		if(strchr(ledIdString, 'g'))
		{
			ledId += LED_GREEN;
		}

		if(strchr(ledIdString, 'r'))
		{
			ledId += LED_RED;
		}

		if(strchr(ledIdString, 'b'))
		{
			ledId += LED_BLUE;
		}
		
		if(!strcmp(ledIdString, "m4"))
		{
			API_M4_LED(atoi(GetShellCmdInputString(cmd, 2)), atoi(GetShellCmdInputString(cmd, 3)));
			return 1;
		}
		else if(!strcmp(ledIdString, "mk"))
		{
			API_MK_LED(atoi(GetShellCmdInputString(cmd, 2)));
			return 1;
		}
		else if(!strcmp(ledIdString, "m4all"))
		{
			API_M4_ALL_LED(atoi(GetShellCmdInputString(cmd, 2)), atoi(GetShellCmdInputString(cmd, 3)));
		}
		else if(!strcmp(ledIdString, "eth0") || !strcmp(ledIdString, "wlan0"))
		{
			Api_Network_GetSn(ledIdString);
		}
		else if(!strcmp(ledIdString, "keypad"))
		{
			ledId = atoi(GetShellCmdInputString(cmd, 2));
			ledCtrl = atoi(GetShellCmdInputString(cmd, 3));
			#ifdef PID_IX622
			API_KeypadLedCtrl(ledId, ledCtrl);
			#endif
			ShellCmdPrintf("API_KeypadLedCtrl ledId = %d, ledCtrl = %d\n", ledId, ledCtrl);
		}

		if(!strcmp(ledCtrlString, "on"))
		{
			ledCtrl = CTRL_LED_ON;
		}
		else if(!strcmp(ledCtrlString, "off"))
		{
			ledCtrl = CTRL_LED_OFF;
		}
		else if(!strcmp(ledCtrlString, "slow"))
		{
			ledCtrl = CTRL_LED_FLASH_SLOW;
		}
		else if(!strcmp(ledCtrlString, "fast"))
		{
			ledCtrl = CTRL_LED_FLASH_FAST;
		}
		else if(!strcmp(ledCtrlString, "irregular"))
		{
			ledCtrl = CTRL_LED_FLASH_IRREGULAR;
		}
	
		if(ledId == 0 || ledCtrl < 0)
		{
			return 0;
		}

		LedMcuCtrl(ledId, ledCtrl);
	}

	return 1;
}

int ShellCmd_PublicInfo(cJSON * input)
{
	cJSON * value = NULL;
	char *command;
	char *paraName;

	command = GetShellCmdInputString(input, 1);
	paraName = GetShellCmdInputString(input, 2);
	if(!strcmp(command, "r"))
	{
		value = API_PublicInfo_Read(paraName);
		ShellCmdPrintJson(value);
	}
	else if(!strcmp(command, "w"))
	{
		value = GetShellCmdInputJson(input, 3);
		API_PublicInfo_Write(paraName, value);
		cJSON_Delete(value);

		value = API_PublicInfo_Read(paraName);
		ShellCmdPrintJson(value);
	}
	else
	{
		value = API_PublicInfo_Read(paraName);
		ShellCmdPrintJson(value);
	}
	return 1;
}

int ShellCmd_GetRemotePublicInfo(cJSON * input)
{
	cJSON * value = NULL;
	char *ipString;
	char *paraName;

	ipString = GetShellCmdInputString(input, 1);
	paraName = GetShellCmdInputString(input, 2);
	value = API_GetRemotePb(inet_addr(ipString), paraName);
	ShellCmdPrintJson(value);
	cJSON_Delete(value);
	return 1;
}

int ShellCmd_Pwd(cJSON *cmd)
{
	char output[SHELL_OUTPUT_LEN];

	getcwd(output, SHELL_OUTPUT_LEN);
	ShellCmdPrintf("%s\n", output);
	return 1;
}

extern int unlockSetCnt;
extern int unlockResetCnt;

int ShellCmd_Cd(cJSON *cmd)
{
	char* path;
	path = GetShellCmdInputString(cmd, 1);
	path = (path ? path : "/");
	if(chdir(path) != 0)
	{
		ShellCmdPrintf("can't cd to %s: No such file or directory\n", path);
	}

	return 1;
}

static int ShellCmd_Os(cJSON *cmd)
{
	int ret = 0;
	char output[SHELL_OUTPUT_LEN+1] = {0};
	FILE* stream[2];
	int pid;
	int readLen, readIndex;

	int timeOut;

	cJSON *cmdtemp = cJSON_Duplicate(cmd, 1);

	if(!strcmp(GetShellCmdInputString(cmd, 1), "-t"))
	{
		timeOut = atoi(GetShellCmdInputString(cmd, 2));
		if(timeOut == 0 || timeOut > 120)
		{
			timeOut = 120;
		}
		cJSON_DeleteItemFromArray(cmdtemp, 2);
		cJSON_DeleteItemFromArray(cmdtemp, 1);
		cJSON_DeleteItemFromArray(cmdtemp, 0);
	}
	else
	{
		cJSON_DeleteItemFromArray(cmdtemp, 0);
		timeOut = 1;
	}
	
	pid = mypopen2("r", stream, cmdtemp);
	while(pid > 0)
	{
		readLen = myfread(stream, output, SHELL_OUTPUT_LEN, timeOut);
		if(readLen == -1)
		{
			ShellCmdPrintf("myfread error!\n");
			break;
		}
		else if(readLen >= 0)
		{
			output[readLen] = 0;
			ShellCmdPrintf(output);
			ret = 1;
			if(readLen < SHELL_OUTPUT_LEN)
			{
				break;
			}
		}
	}

	mypclose(stream, &pid);


	cJSON_Delete(cmdtemp);
	
	return ret;
}

static void JsonMultilevelConversion(const cJSON* input, cJSON* output)
{
    cJSON * current_element = NULL;

    if(input == NULL || input->child == NULL || output == NULL)
    {
        return;
    }

	current_element = input->child;
	while (current_element)
	{
		if(cJSON_IsObject(current_element))
		{
			JsonMultilevelConversion(current_element, output);
		}
		else if(current_element->string != NULL)
		{
			cJSON_AddItemToObject(output, current_element->string, cJSON_Duplicate(current_element, 1));
		}
		current_element = current_element->next;
	}
}

int ShellCmd_Io(cJSON * input)
{
	cJSON * value = NULL;
	char *command;
	char *paraName;

	command = GetShellCmdInputString(input, 1);
	paraName = GetShellCmdInputString(input, 2);
	if(!strcmp(command, "r"))
	{
		ShellCmdPrintJson(API_Para_Read_Public(paraName));
	}
	else if(!strcmp(command, "w"))
	{
		cJSON* writeValue = cJSON_CreateObject();

		value = GetShellCmdInputJson(input, 3);
		if(cJSON_IsObject(value))
		{
			JsonMultilevelConversion(value, writeValue);
			cJSON_Delete(value);
		}
		else
		{
			cJSON_AddItemToObject(writeValue, paraName, value);
		}

		API_Para_Write_Public(writeValue);
		cJSON_Delete(writeValue);
	
		ShellCmdPrintJson(API_Para_Read_Public(paraName));
	}
	else if(!strcmp(command, "default"))
	{
		ShellCmdPrintJson(API_Para_Read_Default(paraName));
	}
	else if(!strcmp(command, "restore"))
	{
		API_Para_Restore_Default(paraName);
		ShellCmdPrintJson(API_Para_Read_Public(paraName));
	}
	else if(!strcmp(command, "check"))
	{
		value = GetShellCmdInputJson(input, 3);		
		ShellCmdPrintf("IO Check %s:%s is %s.\n", paraName, GetShellCmdInputString(input, 3), API_IO_Check(paraName, value) ? "true" : "false");
		cJSON_Delete(value);
	}
	else
	{
		ShellCmdPrintJson(API_Para_Read_Public(NULL));
	}
	return 1;
}

int ShellCmd_ElogCtrl(cJSON *cmd)
{
	int ret = 1;

	char* command = GetShellCmdInputString(cmd, 1);
	char* lvlStr = GetShellCmdInputString(cmd, 2);

 	if(command && !strcmp(command, "state"))
	{
		ShellCmdPrintJson(my_elog_get_state());
	}
	else if(command && !strcmp(command, "restore"))
	{
		ShellCmdPrintJson(my_elog_restore());
	}
	else if(command && lvlStr && !strcmp(command, "udp"))
	{
		ShellCmdPrintf("Elog %s level is %d.\n", command, my_elog_set_udp_lvl(atoi(lvlStr)));
	}
	else if(command && lvlStr && !strcmp(command, "file"))
	{
		ShellCmdPrintf("Elog %s level is %d.\n", command, my_elog_set_file_lvl(atoi(lvlStr)));
	}
	else if(command && lvlStr && !strcmp(command, "uart"))
	{
		ShellCmdPrintf("Elog %s level is %d.\n", command, my_elog_set_printf_lvl(atoi(lvlStr)));
	}
	else if(command && lvlStr && !strcmp(command, "xd"))
	{
		ShellCmdPrintf("Elog %s level is %d.\n", command, my_elog_set_ix_lvl(atoi(lvlStr)));
	}
	else if(command && lvlStr && !strcmp(command, "all"))
	{
		my_elog_set_udp_lvl(atoi(lvlStr));
		my_elog_set_file_lvl(atoi(lvlStr));
		my_elog_set_printf_lvl(atoi(lvlStr));
		my_elog_set_ix_lvl(atoi(lvlStr));
		ShellCmdPrintJson(my_elog_get_state());
	}
	else if(command && lvlStr && !strcmp(command, "ip"))
	{
		ShellCmdPrintf("Elog udp %s is %s.\n", command, my_elog_set_udp_ip(lvlStr));
	}

	return 1;
}

int ShellCmd_Ixd(cJSON *cmd)
{
	int ret = 0;
	char *command;
	char *para;

	command = GetShellCmdInputString(cmd, 1);
	if(command == NULL)
	{
		return ret;
	}
	else if(!strcmp(command, "state"))
	{
		ShellCmdPrintJson(GetIxProxyConnectState());
		ret = 1;
	}
	else if(!strcmp(command, "start"))
	{
		para = GetShellCmdInputString(cmd, 2);
		XDRemoteDisconnect();
		API_Para_Write_Int(IXD_AutoConnect, 1);
		XDRemoteConnect(para);
		ShellCmdPrintf("XD remote connect.\n");
		ret = 1;
	}
	else if(!strcmp(command, "stop"))
	{
		API_Para_Write_Int(IXD_AutoConnect, 0);
		XDRemoteDisconnect();
		ShellCmdPrintf("XD remote disconnect.\n");
		ret = 1;
	}
	else if(!strcmp(command, "search"))
	{
		para = GetShellCmdInputString(cmd, 2);
		if(para)
		{
			ret = strlen(para) + 1;
		}
		else
		{
			ret = 0;
		}

		IxProxySearchDevices(para, ret);
		ret = 1;
	}
	else if(!strcmp(command, "get"))
	{
		para = GetShellCmdInputString(cmd, 2);
		if(para)
		{
			ret = strlen(para) + 1;
		}
		else
		{
			ret = 0;
		}

		IxProxyGetDeviceInfo(para, ret);
		ret = 1;
	}
	else if(!strcmp(command, "report"))
	{
		char* dataString = GetShellCmdInputString(cmd, 2);
		DX821_ReportString(dataString);
		ShellCmdPrintf("Event Report:%s\n", dataString);
	}
	return ret;
}

int ShellCmd_Route(cJSON *cmd)
{
	int ret = 0;
	char *command;

	command = GetShellCmdInputString(cmd, 1);
	if(command == NULL)
	{
		return ret;
	}

	if(RegexCheck("^(WLAN)|(LAN)|(4G)$", command))
	{
		CB_SET_SipRoute(command);//lv_msg_send(MSG_SETTING_SIP_ROUTE, command);
		ShellCmdPrintf("route %s.\n", command);
		ret = 1;
	}

	return ret;
}

int ShellCmd_IX_Service(cJSON *cmd)
{
	char* ctrlStr = GetShellCmdInputString(cmd, 1);
	cJSON *cfg, *where, *view, *result;

	if(ctrlStr == NULL)
	{
		ShellCmdPrintf("Please input ixs parameter: search\n");
		return 0;
	}

	if(!strcmp(ctrlStr, "search"))
	{
		cfg = GetShellCmdInputJson(cmd, 2);
		where = GetShellCmdInputJson(cmd, 3);
		view = GetShellCmdInputJson(cmd, 4);
		ShellCmdPrintJson(cfg);
		ShellCmdPrintJson(where);
		result = API_IXS_Search(cfg, where, view, 1);
		ShellCmdPrintJson(result);
		cJSON_Delete(cfg);
		cJSON_Delete(where);
		cJSON_Delete(view);
		cJSON_Delete(result);
	}
	else if(!strcmp(ctrlStr, "get"))
	{
		view = GetShellCmdInputJson(cmd, 3);
		result = API_GetPbIo(inet_addr(GetShellCmdInputString(cmd, 2)), NULL, view);
		ShellCmdPrintJson(result);
		cJSON_Delete(view);
		cJSON_Delete(result);
	}

	
	return 1;
}

static int ShellCmd_Update(cJSON *cmd)
{
	char* ctrlStr = GetShellCmdInputString(cmd, 1);

	if(ctrlStr == NULL)
	{
		ShellCmdPrintf("Please input update parameter: start/cancel\n");
		return 0;
	}

	if(!strcmp(ctrlStr, "start"))
	{
		API_Event_UpdateStart(GetShellCmdInputString(cmd, 2), GetShellCmdInputString(cmd, 3), 0, "shell");
		ShellCmdPrintf("Start download\n");
	}
	else if(!strcmp(ctrlStr, "cancel"))
	{
		API_OtaCancel();
		ShellCmdPrintf("Cancel download\n");
	}

	return 1;
}

static int ShellCmd_Event(cJSON *cmd)
{
	char* eventStr = GetShellCmdInputString(cmd, 1);
	cJSON *event;

	if(eventStr == NULL)
	{
		ShellCmdPrintf("Please input event data.\n");
		return 0;
	}

	event = GetShellCmdInputJson(cmd, 1);
	if(event)
	{
		API_Event(eventStr);
		ShellCmdPrintJson(event);
		cJSON_Delete(event);
	}
	
	return 1;
}

char CharToHex(char input)
{
	char ret = 0;

	if(input >= '0' && input <= '9')
	{
		ret = input - '0';
	}
	else if(input >= 'a' && input <= 'f')
	{
		ret = input - 'a' + 10;
	}
	else if(input >= 'A' && input <= 'F')
	{
		ret = input - 'A' + 10;
	}

	return ret;
}

static int ShellCmd_Uart(cJSON *cmd)
{
	char* hexString = GetShellCmdInputString(cmd, 1);
	char* timeString = GetShellCmdInputString(cmd, 2);
	int inputLen, outLen, i;
	char ch0, ch1;
	char sendData[200];
	char recvData[200];
	char printBuff[400+1];
	int timeOut;

	if(hexString == NULL || (inputLen = strlen(hexString))%2)
	{
		ShellCmdPrintf("HexString error.\n");
		return 0;
	}

	ShellCmdPrintf("Uart send:%s\n", hexString);

	for(i = 0; i < inputLen/2 && i < 200; i++)
	{
		ch0 = CharToHex(hexString[2*i]);
		ch1 = CharToHex(hexString[2*i+1]);
		sendData[i] = (ch0 << 4 | ch1);
	}

	timeOut = (timeString ? atoi(timeString) : 200);

	if(api_uart_send_pack_and_wait_rsp(sendData[0], sendData+1, inputLen/2-1, recvData, &outLen, timeOut))
	{
		for(i = 0; i < outLen; i++)
		{
			sprintf(printBuff+2*i, "%02X", recvData[i]);	
		}
		printBuff[2*i] = 0;

		ShellCmdPrintf("Uart recv:%s\n", printBuff);
	}
	return 1;
}

static char* PrintHex(char* target, int targetLen, char* source, int sourceLen)
{
	int i;
	
	for(i = 0; i < sourceLen; i++)
	{
		if(2*i < targetLen-2)
		{
			sprintf(target+2*i, "%02X", source[i]);	
		}
	}

	if(2*i < targetLen-1)
	{
		target[2*i] = 0;
	}

	return target;
}

static int ShellCmd_DJ9eeprom(cJSON *cmd)
{
	char* rOrW = GetShellCmdInputString(cmd, 1);
	char* addr = GetShellCmdInputString(cmd, 2);
	char* eepromAddr = GetShellCmdInputString(cmd, 3);
	char* eepromLen = GetShellCmdInputString(cmd, 4);
	char* data = GetShellCmdInputString(cmd, 5);

	int inputLen, outLen, i, sendIndex;
	char ch0, ch1;
	char sendString[200];
	char sendData[200];
	char recvData[200];
	char printBuff[400+1];

	if(strlen(addr) != 2 || strlen(eepromAddr) != 2 || strlen(eepromLen) != 2 || strlen(data)+22 >= 200)
	{
		ShellCmdPrintf("addr || eepromAddr || eepromLen || data error.\n");
		return 0;
	}

	inputLen = strlen("6900002D8415D308000005");
	sendIndex = 0;
	memcpy(sendString, "6900002D8415D308000005", inputLen);
	sendIndex += inputLen;
	memcpy(sendString+8, addr, 2);

	memcpy(sendString+18, eepromAddr, 2);
	memcpy(sendString+20, eepromLen, 2);

	if(!strcmp(rOrW, "r") || !strcmp(rOrW, "R"))
	{
		memcpy(sendString+16, "00", 2);
	}
	else if(!strcmp(rOrW, "w") || !strcmp(rOrW, "W"))
	{
		memcpy(sendString+16, "02", 2);
		if(data[0] == 0 || (inputLen = strlen(data))%2)
		{
			ShellCmdPrintf("input data error.\n");
			return 0;
		}
		ch0 = CharToHex(eepromLen[0]);
		ch1 = CharToHex(eepromLen[1]);
		if((ch0 << 4 | ch1) != inputLen/2)
		{
			ShellCmdPrintf("input data error.\n");
			return 0;
		}
		memcpy(sendString+22, data, inputLen);
		sendIndex += inputLen;
	}
	else
	{
		ShellCmdPrintf("input read or write error.\n");
		return 0;
	}

	sendString[sendIndex] = 0;

	inputLen = sendIndex;
	for(sendIndex = 0, i = 0; i < inputLen/2 && sendIndex < 200; i++)
	{
		ch0 = CharToHex(sendString[2*i]);
		ch1 = CharToHex(sendString[2*i+1]);
		sendData[sendIndex++] = (ch0 << 4 | ch1);
	}

	//数据包长度计算
	sendData[7] = sendIndex - 3;

	for(ch0 = 0, i = 3; i < sendIndex; i++)
	{
		if(i != 6)
		{
			ch0 += sendData[i];
		}
	}
	sendData[6] = ch0;

	ShellCmdPrintf("Uart send:%s\n", PrintHex(printBuff, 400, sendData, sendIndex));

	if(api_uart_send_pack_and_wait_rsp(sendData[0], sendData+1, sendIndex-1, recvData, &outLen, 200))
	{
		ShellCmdPrintf("Uart recv:%s\n", PrintHex(printBuff, 400, recvData, outLen));
	}
	return 1;
}

static int ShellCmd_GpOut(cJSON *cmd)
{
	cJSON* data = GetShellCmdInputJson(cmd, 1);
	int ret;

	if(!cJSON_IsObject(data))
	{
		ShellCmdPrintf("GP out data intput error.\n");
		cJSON_Delete(data);
		return 0;
	}

	if(IX2V_GP_OUT(data))
	{
		ret = 1;
	}
	else
	{
		ret = 0;
	}

	ShellCmdPrintf("GP output %s.\n", ret ? "success" : "error");

	cJSON_Delete(data);

	return ret;
}

static int ShellCmd_RegCheck(cJSON *cmd)
{
	int errorCode;
	int result = 0;
	regex_t regex;
	regmatch_t pm;
	char ebuf[128];

	char * pattern = GetShellCmdInputString(cmd, 1);
	char * checkString = GetShellCmdInputString(cmd, 2);

	ShellCmdPrintf("pattern=%s,checkString=%s\n", pattern, checkString);

	/* 编译正则表达式*/
	errorCode = regcomp(&regex, pattern, REG_EXTENDED|REG_NEWLINE);
	if (errorCode == 0)
	{
		/* 对每一行应用正则表达式进行匹配 */
		errorCode = regexec(&regex, checkString, 1, &pm, 0);
		if (errorCode == 0) 
		{
			/* 输出处理结果 */
			ShellCmdPrintf("pm.rm_so = %d, pm.rm_eo = %d\n", pm.rm_so, pm.rm_eo);
			result = 1;
		}
	}

	if(result == 0)
	{
		regerror(errorCode, &regex, ebuf, sizeof(ebuf));
		ShellCmdPrintf("regerror:%s\n", ebuf);
	}

	/* 释放正则表达式 */
	regfree(&regex);

	return result;
}

static int ShellCmd_Sntp(cJSON *cmd)
{
	char* cmdString = GetShellCmdInputString(cmd, 1);
	char* serverString = GetShellCmdInputString(cmd, 2);
	char* zoneString = GetShellCmdInputString(cmd, 3);
	int server, zone, ret;

	if(!strcmp(cmdString, "get"))
	{
		if(serverString == NULL)
		{
			serverString = API_Para_Read_String2(TIME_SERVER);
		}
		server = inet_addr(serverString);

		if(zoneString)
		{
			zone = atoi(zoneString);
		}
		else
		{
			zone = API_Para_Read_Int(TIME_ZONE);
		}
		 
		ret = sync_time_from_sntp_server(server, zone);
		ShellCmdPrintf("sntp get server:%s timezone:%d %s!\n", serverString, zone, ret == 0 ? "Sucess" : "Error");
	}
	if(!strcmp(cmdString,"test"))
	{
		#ifdef PID_IX821
		sync_time_from_vtkdevice();
		#endif
	}

	return 1;
}

int Modules4G_AT_Send(char *data, int len);
static int ShellCmd_AtCmd(cJSON *cmd)
{
	char* dataString = GetShellCmdInputString(cmd, 1);

	if(dataString)
	{
		ShellCmdPrintf("AT Send:%s\n", dataString);
		Modules4G_AT_Send(dataString, strlen(dataString));
	}
	return 1;
}

static int ShellCmd_Reboot(cJSON *cmd)
{
	char* dataString = GetShellCmdInputString(cmd, 1);
	ShellCmdPrintf("Shell:request reboot\n");
	HardwareRestart(dataString);
	return 1;
}


static int ShellCmd_KeypadLed(cJSON *cmd)
{
	char* ledColor = GetShellCmdInputString(cmd, 1);
	char* ledState = GetShellCmdInputString(cmd, 2);
	#include "obj_KeypadLed.h"
	int color, state;

	if(!strcmp(ledColor, "blue"))
	{
		color = LED_BLUE;
	}
	else if(!strcmp(ledColor, "green"))
	{
		color = LED_GREEN;
	}
	else if(!strcmp(ledColor, "red"))
	{
		color = LED_RED;
	}

	if(!strcmp(ledState, "on"))
	{
		state = KEYPAD_LED_ON;
	}
	else if(!strcmp(ledState, "off"))
	{
		state = KEYPAD_LED_OFF;
	}
	else if(!strcmp(ledState, "fast"))
	{
		state = KEYPAD_LED_FLASH_FAST;
	}
	else if(!strcmp(ledState, "slow"))
	{
		state = KEYPAD_LED_FLASH_SLOW;
	}
	else if(!strcmp(ledState, "irregular"))
	{
		state = KEYPAD_LED_FLASH_IRREGULAR;
	}
	#ifdef PID_IX622
	API_KeypadLedCtrl(color, state);
	#endif
	return 1;
}


static int ShellCmd_Search(cJSON *cmd)
{
	cJSON* searchCtrl = GetShellCmdInputJson(cmd, 1);
	cJSON* resultTable = cJSON_CreateArray();
	char* result;

	IXS_Search(GetShellCmdJsonString(searchCtrl, "Net"), searchCtrl, GetShellCmdJsonString(searchCtrl, "BD_Nbr"), GetShellCmdJsonString(searchCtrl, "Type"), resultTable);
	result = GetShellCmdJsonString(searchCtrl, "Result");
	if(result && !strcmp(result, "Count"))
	{
		ShellCmdPrintf("%d\n", cJSON_GetArraySize(resultTable));
	}
	else if(result && !strcmp(result, "Difference"))
	{
		char temp[SHELL_OUTPUT_LEN];
		cJSON* diffTable = cJSON_CreateArray();
		cJSON* inputTable = ShellGetJsonFromFile(GetShellCmdJsonString(searchCtrl, "InputTable"));
		cJSON* recordDiff = NULL;
		cJSON* recordResult = NULL;
		cJSON* recordInput = NULL;
		int inputTableCnt, resultTableCnt, i, j;
		char* inputSn;
		char* resultSn;

		strcpy(temp, get_time_string());
		resultTableCnt = cJSON_GetArraySize(resultTable);

		for(i = 0; i < resultTableCnt; i++)
		{
			recordResult = cJSON_GetArrayItem(resultTable, i);
			resultSn = GetShellCmdJsonString(recordResult, DM_KEY_MFG_SN);
			inputTableCnt = cJSON_GetArraySize(inputTable);
			for(j = 0; j < inputTableCnt; j++)
			{
				recordInput = cJSON_GetArrayItem(inputTable, j);
				inputSn = GetShellCmdJsonString(recordInput, DM_KEY_MFG_SN);
				if(!strcmp(inputSn, resultSn))
				{
					break;
				}
			}

			//不是新增的
			if(j < inputTableCnt)
			{
				//差异的记录
				if(!cJSON_Compare(cJSON_GetObjectItemCaseSensitive(recordInput, IX2V_IP_ADDR), cJSON_GetObjectItemCaseSensitive(recordResult, IX2V_IP_ADDR), 1) || 
				!cJSON_Compare(cJSON_GetObjectItemCaseSensitive(recordInput, IX2V_IX_ADDR), cJSON_GetObjectItemCaseSensitive(recordResult, IX2V_IX_ADDR), 1))
				{
					recordDiff = cJSON_Duplicate(recordInput, 1);
					cJSON_AddStringToObject(recordDiff, "Verify", "Refor");
					cJSON_AddStringToObject(recordDiff, "Time", temp);
					cJSON_AddItemToArray(diffTable, recordDiff);

					recordDiff = cJSON_Duplicate(recordResult, 1);
					cJSON_AddStringToObject(recordDiff, "Verify", "Difference");
					cJSON_AddStringToObject(recordDiff, "Time", temp);
					cJSON_AddItemToArray(diffTable, recordDiff);
				}
				cJSON_DeleteItemFromArray(inputTable, j);
			}
			//新增的设备
			else
			{
				recordDiff = cJSON_Duplicate(recordResult, 1);
				cJSON_AddStringToObject(recordDiff, "Verify", "On");
				cJSON_AddStringToObject(recordDiff, "Time", temp);
				cJSON_AddItemToArray(diffTable, recordDiff);
			}
		}

		//掉线的设备
		cJSON_ArrayForEach(recordInput, inputTable)
		{
			recordDiff = cJSON_Duplicate(recordInput, 1);
			cJSON_AddStringToObject(recordDiff, "Verify", "Off");
			cJSON_AddStringToObject(recordDiff, "Time", temp);
			cJSON_AddItemToArray(diffTable, recordDiff);
		}

		ShellCmdPrintJson(diffTable);
		cJSON_Delete(inputTable);
		cJSON_Delete(diffTable);
	}
	else
	{
		ShellCmdPrintJson(resultTable);
	}

	cJSON_Delete(resultTable);
	cJSON_Delete(searchCtrl);

	return 1;
}

static int ShellCmd_SearchByNumber(cJSON *cmd)
{
	cJSON* searchCtrl = GetShellCmdInputJson(cmd, 1);
	cJSON* resultTable = cJSON_CreateArray();
	char* result;

	IXS_GetByNBR(GetShellCmdJsonString(searchCtrl, "Net"), searchCtrl, GetShellCmdJsonString(searchCtrl, IX2V_IX_ADDR), GetShellCmdJsonString(searchCtrl, "Input"), resultTable);
	result = GetShellCmdJsonString(searchCtrl, "Result");
	if(result && !strcmp(result, "Count"))
	{
		ShellCmdPrintf("%d\n", cJSON_GetArraySize(resultTable));
	}
	else if(result && !strcmp(result, "Difference"))
	{
		char temp[SHELL_OUTPUT_LEN];
		char* filePath = GetShellCmdJsonString(searchCtrl, "InputTable");
		cJSON* diffTable = cJSON_CreateArray();
		cJSON* inputTable = NULL;
		cJSON* recordDiff = NULL;
		cJSON* recordResult = NULL;
		cJSON* recordInput = NULL;
		int inputTableCnt, resultTableCnt, i, j;
		char* inputSn;
		char* resultSn;
		
		if(filePath)
		{
			char inputTableFilePath[SHELL_OUTPUT_LEN];
			if(filePath[0] == '/')
			{
				strcpy(inputTableFilePath, filePath);
			}
			else
			{
				getcwd(temp, SHELL_OUTPUT_LEN);
				snprintf(inputTableFilePath, SHELL_OUTPUT_LEN, "%s/%s", temp, filePath);
			}
			inputTable = GetJsonFromFile(inputTableFilePath);
		}

		strcpy(temp, get_time_string());
		resultTableCnt = cJSON_GetArraySize(resultTable);

		for(i = 0; i < resultTableCnt; i++)
		{
			recordResult = cJSON_GetArrayItem(resultTable, i);
			resultSn = GetShellCmdJsonString(recordResult, IX2V_IP_ADDR);
			inputTableCnt = cJSON_GetArraySize(inputTable);
			for(j = 0; j < inputTableCnt; j++)
			{
				recordInput = cJSON_GetArrayItem(inputTable, j);
				inputSn = GetShellCmdJsonString(recordInput, IX2V_IP_ADDR);
				if(!strcmp(inputSn, resultSn))
				{
					break;
				}
			}

			//不是新增的
			if(j < inputTableCnt)
			{
				//差异的记录
				if(!cJSON_Compare(recordInput, recordResult, 1))
				{
					recordDiff = cJSON_Duplicate(recordInput, 1);
					cJSON_AddStringToObject(recordDiff, "Verify", "Refor");
					cJSON_AddStringToObject(recordDiff, "Time", temp);
					cJSON_AddItemToArray(diffTable, recordDiff);

					recordDiff = cJSON_Duplicate(recordResult, 1);
					cJSON_AddStringToObject(recordDiff, "Verify", "Difference");
					cJSON_AddStringToObject(recordDiff, "Time", temp);
					cJSON_AddItemToArray(diffTable, recordDiff);
				}
				cJSON_DeleteItemFromArray(inputTable, j);
			}
			//新增的设备
			else
			{
				recordDiff = cJSON_Duplicate(recordResult, 1);
				cJSON_AddStringToObject(recordDiff, "Verify", "On");
				cJSON_AddStringToObject(recordDiff, "Time", temp);
				cJSON_AddItemToArray(diffTable, recordDiff);
			}
		}

		//掉线的设备
		cJSON_ArrayForEach(recordInput, inputTable)
		{
			recordDiff = cJSON_Duplicate(recordInput, 1);
			cJSON_AddStringToObject(recordDiff, "Verify", "Off");
			cJSON_AddStringToObject(recordDiff, "Time", temp);
			cJSON_AddItemToArray(diffTable, recordDiff);
		}

		ShellCmdPrintJson(diffTable);
		cJSON_Delete(inputTable);
		cJSON_Delete(diffTable);
	}
	else
	{
		ShellCmdPrintJson(resultTable);
	}

	cJSON_Delete(resultTable);
	cJSON_Delete(searchCtrl);

	return 1;
}

static int ShellCmd_GetInfo(cJSON *cmd)
{
	cJSON* getInfoPara = GetShellCmdInputJson(cmd, 1);
	cJSON* resultTable = cJSON_CreateArray();
	cJSON* inputTable = ShellGetJsonFromFile(GetShellCmdJsonString(getInfoPara, "InputTable"));
	char* result;
	int getInfoCnt;

	getInfoCnt = IXS_Info(inputTable, resultTable);
	result = GetShellCmdJsonString(getInfoPara, "Result");
	if(result && !strcmp(result, "Count"))
	{
		ShellCmdPrintf("%d\n", getInfoCnt);
	}
	else
	{
		ShellCmdPrintJson(resultTable);
	}

	cJSON_Delete(resultTable);
	cJSON_Delete(getInfoPara);

	return 1;
}

static int ShellCmd_GetIpBySn(cJSON *cmd)
{
	cJSON* resultTable = cJSON_CreateArray();
	IXS_GetByMFG_SN(GetShellCmdInputString(cmd, 1), resultTable);
	ShellCmdPrintJson(resultTable);
	cJSON_Delete(resultTable);
	return 1;
}

static int ShellCmd_sortJsonTable(cJSON *cmd)
{
	cJSON* inputTable = NULL;

	if(inputTable = ShellGetJsonFromFile(GetShellCmdInputString(cmd, 1)))
	{
		char* order;
		order = GetShellCmdInputString(cmd, 3);
		order = (order ? order : "Ascending");

		if(SortJsonTable(inputTable, GetShellCmdInputString(cmd, 2), strcmp(order, "Ascending")))
		{
			ShellCmdPrintJson(inputTable);
		}
		else
		{
			ShellCmdPrintf("Input parameter error!!!\n");
		}
		cJSON_Delete(inputTable);
	}
	
	return 1;
}

static int ShellCmd_SetDivertState(cJSON *cmd)
{
	if(API_SetDivertState(API_Para_Read_String2(IX_ADDR), GetShellCmdInputString(cmd, 1), GetShellCmdInputString(cmd, 2), GetShellCmdInputString(cmd, 3), NULL) > 0)
	{
		ShellCmdPrintf("Set divert state success\n");
	}
	else
	{
		ShellCmdPrintf("Set divert state error\n");
	}
	
	return 1;
}

static int ShellCmd_Get(cJSON *cmd)
{
	cJSON* devTb = NULL;
	char test1[1000] = {0};
	char test2[1000] = {0};
	char test3[1000] = {0};
	char* getPara = GetShellCmdInputString(cmd, 1);

	if(!strcmp(getPara, "DevTb"))
	{
		char* tbSelect = GetShellCmdInputString(cmd, 2);
		if(tbSelect && !strcmp(tbSelect, "R8001"))
		{
			IXS_ProxyGetTb(&devTb, NULL, R8001);
		}
		else if(tbSelect && !strcmp(tbSelect, "Search"))
		{
			IXS_ProxyGetTb(&devTb, NULL, Search);
		}
		else 
		{
			IXS_ProxyGetTb(&devTb, NULL, R8001AndSearch);
		}
		ShellCmdPrintJson(devTb);
		cJSON_Delete(devTb);
	}
	else if(!strcmp(getPara, "IXS_S"))
	{
		ShellCmdPrintJson(IXS_ProxyGetState());
	}
	else if(!strcmp(getPara, "UI_LIST"))
	{
		char* pid = GetShellCmdInputString(cmd, 2);
		if(pid)
		{
			//ShellCmdPrintJson(GetUI_List(pid));
		}
	}
	else if(!strcmp(getPara, "P_VER"))
	{
		ShellCmdPrintf(GetIX_protocolVersion(inet_addr(GetShellCmdInputString(cmd, 2))));
	}
	else if(!strcmp(getPara, "OLD_IO"))
	{
		char readData[500];
		if(API_ReadOldPara(inet_addr(GetShellCmdInputString(cmd, 2)), GetShellCmdInputString(cmd, 3), readData))
		{
			ShellCmdPrintf("%s\n", readData);
		}
		else
		{
			ShellCmdPrintf("Read error.\n");
		}
	}
	else if(!strcmp(getPara, "sscanf"))
	{
		sscanf(GetShellCmdInputString(cmd, 2), GetShellCmdInputString(cmd, 3), test1, test2, test3);
		ShellCmdPrintf("test1=%s,test1=%s,test3=%s\n", test1, test2, test3);
	}
	else if(!strcmp(getPara, "strtod"))
	{
		char* input = GetShellCmdInputString(cmd, 2);
		ShellCmdPrintf("%s:strtod:%f\n", input, strtod(input, NULL));
	}
	else if(!strcmp(getPara, "MD5"))
	{
		cJSON* input = cJSON_Parse(GetShellCmdInputString(cmd, 2));
		if(CalculateJsonMd5(input, test1))
		{
			ShellCmdPrintf("MD5:%s\n", test1);
		}
		else
		{
			ShellCmdPrintf("MD5:Error\n", test1);
		}
		cJSON_Delete(input);
	}
	else
	{
		char* ip = GetShellCmdInputString(cmd, 1);
		float pingTime = 0;
		ShellCmdPrintf("ping: %s result: %s pingTime: %f ms\n", ip, PingNetwork2(ip, &pingTime) ? "error" : "success", pingTime);
	}

	return 1;
}

static int ShellCmd_Ftp(cJSON *cmd)
{
	char* option = GetShellCmdInputString(cmd, 1);
	char* server = GetShellCmdInputString(cmd, 2);
	char* fileName = GetShellCmdInputString(cmd, 3);
	char* storeDir = GetShellCmdInputString(cmd, 4);

	if(!strcmp(option, "download"))
	{
		if(!strcmp(server, "cancel"))
		{
			ShellCmdPrintf("FTP stop download %s/%s.\n", storeDir, fileName);
			FTP_StopDownload(fileName, storeDir);
		}
		else
		{
			ShellCmdPrintf("FTP start download %s %s/%s.\n", server, storeDir, fileName);
			FTP_StartDownload(server, fileName, storeDir, NULL,  NULL);
		}
	}
	else if(!strcmp(option, "upload"))
	{
		if(!strcmp(GetShellCmdInputString(cmd, 5), "cancel"))
		{
			ShellCmdPrintf("FTP stop upload %s:%s/%s.\n", server, storeDir, fileName);
			FTP_StopUpload(server, fileName, storeDir);
		}
		else
		{
			ShellCmdPrintf("FTP start upload %s:%s/%s.\n", server, storeDir, fileName);
			FTP_StartUpload(server, fileName, storeDir, NULL,  NULL);
		}
	}
	else
	{
		cJSON* element;
		char cmdString[500] = {0};
		int i;
		int arraySize = cJSON_GetArraySize(cmd);
		for(i = 3; i < arraySize; i++)
		{
			strcat(cmdString, GetShellCmdInputString(cmd, i));
			strcat(cmdString, " ");
		}

		ShellCmdPrintf("FTP %s %s.\n", cmdString, (FTP_Url(GetShellCmdInputString(cmd, 1), GetShellCmdInputString(cmd, 2), cmdString) ? "error" : "success"));
	}
	return 1;
}

static int ShellCmd_XD2(cJSON *cmd)
{
	char* option = GetShellCmdInputString(cmd, 1);

	if(!strcmp(option, "file"))
	{
		int i, arraySize = cJSON_GetArraySize(cmd);
		char fileCmd[500];
		for(i = 2, fileCmd[0] = 0; i < arraySize; i++)
		{
			strcat(fileCmd, GetShellCmdInputString(cmd, i));
			if(i < arraySize - 1)
			{
				strcat(fileCmd, " ");
			}
		}
		API_XD2_FileManageReq(fileCmd);
	}
	else if(!strcmp(option, "reconnect"))
	{
		XD2_ReConnect();
	}
	else if(!strcmp(option, "cmd"))
	{
		API_XD2_Shell(GetShellCmdInputString(cmd, 2), GetShellCmdInputString(cmd, 3));
	}
	return 1;
}

int ShellCmd_IperfTest(cJSON *cmd)
{
	int ret;
	ret = API_IperfTestStart(inet_addr(GetShellCmdInputString(cmd, 1)), inet_addr(GetShellCmdInputString(cmd, 2)));
	ShellCmdPrintf("Iperf test start %s\n", ret ? "success" : "error");
	return 1;
}

#if defined( PID_IX850 )

void CellClick(int line, int col)
{
    dprintf("line = %d, col =%d\n", line, col);
}

void FilterClick(cJSON* filter)
{
    dprintf("Name = %s, Value =%s\n", GetEventItemString(filter,  "Name"), GetEventItemString(filter,  "Value"));
}

static int ShellCmd_Setting(cJSON *cmd)
{
	#include "MenuSetting.h"

	static SETTING_VIEW* jsonPage = NULL;

	char* operate = GetShellCmdInputString(cmd, 1);
	if(!strcmp(operate, "root"))
	{
		MainMenu_Reset_Time();
		API_EnterSettingRoot(GetShellCmdInputString(cmd, 2));
	}
	else if(!strcmp(operate, "page"))
	{
		MainMenu_Reset_Time();
		API_SettingChangePage(GetShellCmdInputString(cmd, 2));
	}
	else if(!strcmp(operate, "close"))
	{
		MainMenu_Reset_Time();
		API_SettingMenuClose();
	}
	else if(!strcmp(operate, "jsonpage"))
	{
		char buf[200];
		char* menuName = GetShellCmdInputString(cmd, 2);
		snprintf(buf, 200, "/mnt/nand1-1/App/res/SETTING-PAGE/%s.json", menuName);
        cJSON* pageData = GetJsonFromFile(buf);
		API_JsonMenuDisplay(&jsonPage, pageData, menuName,NULL);
		cJSON_Delete(pageData);
	}
	else if(!strcmp(operate, "jsonclose"))
	{
		API_JsonMenuDelete(&jsonPage);
	}
	else if(!strcmp(operate, "return"))
	{
		MainMenu_Reset_Time();
		API_SettingMenuReturn();
	}
	return 1;
}
#endif

int ShellCmd_AutoTest(cJSON *cmd);
int ShellCmd_Table(cJSON *cmd);
int ShellCmd_RemoteTable(cJSON *cmd);
int ShellCmd_LocalTable(cJSON *cmd);
int ShellCmd_Tlog(cJSON *cmd);

extern int ShellCmd_ms7024_display(cJSON *cmd);
extern int ShellCmd_msti(cJSON *cmd);
extern int ShellCmd_mste(cJSON *cmd);
extern int ShellCmd_mstipc_on(cJSON *cmd);
extern int ShellCmd_mstipc_off(cJSON *cmd);
extern int ShellCmd_mstds_on(cJSON *cmd);
extern int ShellCmd_mstds_off(cJSON *cmd);
extern int ShellCmd_vdipc_on(cJSON *cmd);
extern int ShellCmd_vdipc_off(cJSON *cmd);
extern int ShellCmd_vdds_on(cJSON *cmd);
extern int ShellCmd_vdds_off(cJSON *cmd);

extern int ShellCmd_ds_udpsend(cJSON *cmd);
extern int ShellCmd_ds_rtpsend(cJSON *cmd);

extern int ShellCmd_IpcManage(cJSON *cmd);
extern int ShellCmd_ListTest(cJSON *cmd);
extern int ShellCmd_FontTest(cJSON *cmd);
extern int ShellCmd_FFmpeg(cJSON *cmd);
extern int ShellCmd_Talk(cJSON *cmd);
extern int ShellCmd_scr_test(cJSON *cmd);

extern int ShellCmd_sensor_adjust(cJSON *cmd);
extern int ShellCmd_touchpad_io(cJSON *cmd);

extern int ShellCmd_Call(cJSON *cmd);
extern int ShellCmd_Wlan(cJSON *cmd);
extern int ShellCmd_Lan(cJSON *cmd);
extern int ShellCmd_Iperf(cJSON *cmd);

extern int ShellCmd_touchkey_io(cJSON *cmd);
extern int ShellCmd_SipManage(cJSON *cmd);
extern int ShellCmd_Card(cJSON *cmd);

extern int ShellCmd_fetch_ixg_addr(cJSON *cmd);

extern int ShellCmd_radar_debug(cJSON *cmd);
extern int ShellCmd_main_list(cJSON *cmd);
extern int IXGRM_Shell_Test(cJSON *cmd);

extern int ShellCmd_bar_test(cJSON *cmd);
extern int IX850SMainMenu_Shell_Test(cJSON *cmd);
extern int ShellCmd_MemSpace(cJSON *cmd);
extern int Eoc_Libmstar_Shell_Test (cJSON *cmd);

extern int IX850SPageCfg_Shell_Test(cJSON *cmd);
extern int IX850SMenuCfg_Shell_Test(cJSON *cmd);
extern int IX850SMenuCfg_Choose_Shell_Test(cJSON *cmd);
extern int ShellCmd_ModeTip(cJSON *cmd);

int Menu_Wifi_Shell_Test(cJSON *cmd);
int Menu_Setting_Shell_Test(cJSON *cmd);

int ShellCmd_Dataset(cJSON *cmd);

void ShellCmd_remote_unlock(cJSON *cmd);

int ShellCmdInit(void)
{
	ShellCmdAdd("pwd", ShellCmd_Pwd, "Print work dir");
	ShellCmdAdd("cd", ShellCmd_Cd, "Change dir");
	ShellCmdAdd("os", ShellCmd_Os, "System commands:\n\tBe executed by execvp().\n\tFor example:os [-t 5] ls -al [> 1.txt]");
	ShellCmdAdd("led", ShellCmd_LedCtrl, "LED control:\n\tFor example:led r[gb] on[off, slow, fast, irregular]");
	ShellCmdAdd("pb", ShellCmd_PublicInfo, "Read/write public information:\n\tFor example:pb r/w MFG_SN");
	ShellCmdAdd("getrpb", ShellCmd_GetRemotePublicInfo, "Read remote public information:\n\tFor example:getrpb MFG_SN");
	ShellCmdAdd("io", ShellCmd_Io, "For example:\n\tio r/w/default/restore MY_NAME");
	ShellCmdAdd("tb", ShellCmd_Table, "For example:\n\ttb count/add/del/select/update/load/drop fileName Where ...");
	ShellCmdAdd("rtb", ShellCmd_RemoteTable, "For example:\n\trtb ip tbName count/add/del/select/update Where (View/record)");
	ShellCmdAdd("ltb", ShellCmd_LocalTable, "For example:\n\tltb tbName count/add/del/select/update Where (View/record)");
	ShellCmdAdd("elog", ShellCmd_ElogCtrl, "For example:\n\telog state\n\telog udp 6\n\telog file 6\n\telog uart 6\n\telog xd 6\n\telog all 6\n\telog ip 192.168.243.188\n");
	ShellCmdAdd("test", ShellCmd_AutoTest, "For example:\n\ttest start /mnt/nfs/test.ts\n\ttest stop\n\ttest state");
	ShellCmdAdd("ixs", ShellCmd_IX_Service, "For example:\n\tixs search");
	ShellCmdAdd("update", ShellCmd_Update, "For example:\n\tupdate start 47.106.104.38 471000");
	ShellCmdAdd("event", ShellCmd_Event, "For example:\n\tevent {\"EventName\":\"XXX\"}");
	ShellCmdAdd("uart", ShellCmd_Uart, "For example:\n\tuart 100007");
	ShellCmdAdd("gpout", ShellCmd_GpOut, "For example:\n\tgpo actuator LOCK1 {\"bin_ouput\":ture}");
	ShellCmdAdd("reg", ShellCmd_RegCheck, "For example:\n\treg ^\\d[10]$ 0123456789");
	ShellCmdAdd("sntp", ShellCmd_Sntp, "For example:\n\tsntp get 131.188.3.220 20");
	ShellCmdAdd("at", ShellCmd_AtCmd, "For example:\n\tat AT");
	ShellCmdAdd("reboot", ShellCmd_Reboot, "For example:\n\treboot");
	ShellCmdAdd("divert", ShellCmd_SetDivertState, "For example:\n\tdivert 0099000101 0099000201 1");
	ShellCmdAdd("ftp", ShellCmd_Ftp, "For example:\n\tftp download 47.106.104.38 /home/userota/611-test /mnt/nand1-2/\n\tftp upload 47.106.104.38 /mnt/nand1-1/App/611-test /home/userota/");
	ShellCmdAdd("xd2", ShellCmd_XD2, "For example:\n\txd2 reconnect");
	// lzh_20220302

	#if defined( PID_IX850 )
	//ShellCmdAdd("ds_preview", ShellCmd_ds_preview, "ds_preview channel onoff\n\teg:ds_preview 1 0 0");
	//ShellCmdAdd("ds_udpsend", ShellCmd_ds_udpsend, "ds_udpsend type addr port\n\teg:ds_udpsend 0 1 0 3232278720 166024");
	//ShellCmdAdd("ds_ipcsend", ShellCmd_ds_rtpsend, "ds_ipssend type addr port\n\teg:ds_ipcsend 0 1 1 192.168.111.12 5262");
	ShellCmdAdd("sensor", ShellCmd_sensor_adjust, "For example:\n\t sensor bright/color/contrast/link inc/dec/0-9");
	ShellCmdAdd("scr_test", ShellCmd_scr_test, "dx821 menu test \n\teg:scr_test 0-3");
	ShellCmdAdd("radard", ShellCmd_radar_debug, "ix850 radar test: radard cmd len dat1 dat2 ...\n\teg: radard 2 1 15");
	ShellCmdAdd("main_list", ShellCmd_main_list, "ix850 main list test: \n main_list name/room del/tb/file json_tb/file_path");
	ShellCmdAdd("main_menu", IX850SMainMenu_Shell_Test, "ix850 main menu test: ...");
	ShellCmdAdd("menu_cfg", IX850SMenuCfg_Shell_Test, "ix850 main menu test \n\teg: menu_cfg load");
	ShellCmdAdd("page_cfg", IX850SPageCfg_Shell_Test, "ix850 main menu test \n\teg: page_cfg create");
	ShellCmdAdd("menu_choose", IX850SMenuCfg_Choose_Shell_Test, "ix850 main menu test \n\teg: menu_choose load");
	ShellCmdAdd("dataset", ShellCmd_Dataset, "For example:\n\tdataset\n\tdataset update\n\tdataset update {\"ListSource\":{\"IXS\":false,\"DXG\":false,\"RES\":true},\"OnlineMark\":false,\"ListFilter\":{\"GW_ADDR\":\"\"},\"ListType\":{\"IX_TYPE\":\"IM\"},\"ListSort\":\"IX_ADDR\",\"ListOrder\":\"INC/DEC\"}");
	ShellCmdAdd("setting", ShellCmd_Setting, "For example:\n\tsetting root name\n\tsetting page name\n\tsetting close\n\tsetting return");
	ShellCmdAdd("wifi", Menu_Wifi_Shell_Test, "For example:\n\teg: wifi load");
	ShellCmdAdd("setting", Menu_Setting_Shell_Test, "For example:\n\teg: wifi load");
	ShellCmdAdd("tip_mode", ShellCmd_ModeTip, "For example:\n\teg: tip_mode show string");
	#elif defined( PID_IX611 ) || defined( PID_IX622 )
	ShellCmdAdd("kLed", ShellCmd_KeypadLed, "For example:\n\t kLed blue irregular");
	ShellCmdAdd("sensor", ShellCmd_sensor_adjust, "For example:\n\t sensor bright/color/contrast/link inc/dec/0-9");
	#endif

	ShellCmdAdd("monc", ShellCmd_Monc, MoncDescribe);
	ShellCmdAdd("mons", ShellCmd_Mons, MonsDescribe);
	ShellCmdAdd("xd", ShellCmd_Ixd, "For example:\n\txd state/start/stop/report");

	ShellCmdAdd("ipc", ShellCmd_IpcManage, "For example:\n\tipc search/login/show/close key2 key3 ...");
	ShellCmdAdd("ffmpeg", ShellCmd_FFmpeg, "For example:\n\tffmpeg filter/rec/play file_in size_str filter_str file_out");
	ShellCmdAdd("talk", ShellCmd_Talk, "talk on/off/state");
	ShellCmdAdd("call", ShellCmd_Call, "talk start/cancel/state");
	ShellCmdAdd("wlan", ShellCmd_Wlan, "wlan state/open/close/add_ssid/get_ssid/save_ssid/earch");
	ShellCmdAdd("lan", ShellCmd_Lan, "lan state/auto/static/get_para/save_para");
	ShellCmdAdd("iperf", ShellCmd_Iperf, "start one iperf test");
	ShellCmdAdd("msip", ShellCmd_SipManage, "sip manager");
	ShellCmdAdd("route", ShellCmd_Route, "For example:\n\troute WLAN/LAN/4G");
	ShellCmdAdd("card", ShellCmd_Card, "For example:\n\tcard swipe/state/room");
	ShellCmdAdd("search", ShellCmd_Search, "For example:\n\tsearch {\"Net\":\"eth0\",\"BD_Nbr\":\"0099\",\"Type\":\"IM\",\"InputTable\":\"2.txt\",\"Result\":\"Difference\",\"Lan_S1_Timeout\":300,\"Lan_S1_IntervalTime\":80,\"Lan_S2_Enable\":1,\"Lan_S2_Timeout\":200,\"Lan_S2_IntervalTime\":80,\"Wlan_S1_Timeout\":300,\"Wlan_S1_IntervalTime\":80,\"Wlan_S2_Enable\":1,\"Wlan_S2_Timeout\":300,\"Wlan_S2_IntervalTime\":80}");
	ShellCmdAdd("search_nbr", ShellCmd_SearchByNumber, "For example:\n\tsearch_nbr {\"Net\":\"eth0\",\"IX_ADDR\":\"00990001\",\"Input\":\"0001\",\"InputTable\":\"2.txt\",\"Result\":\"Difference\",\"Lan_S1_Timeout\":300,\"Lan_S1_IntervalTime\":80,\"Wlan_S1_Timeout\":300,\"Wlan_S1_IntervalTime\":80}");
	ShellCmdAdd("get_info", ShellCmd_GetInfo, "For example:\n\tget_info {\"InputTable\":\"2.txt\",\"Result\":\"List\"}");
	ShellCmdAdd("get_ip_by_sn", ShellCmd_GetIpBySn, "For example:\n\tget_ip_by_sn 6400000004ae");
	ShellCmdAdd("sort_json_table", ShellCmd_sortJsonTable, "For example:\n\tsort_json_table 1.txt Ascending/Descending");
	ShellCmdAdd("get", ShellCmd_Get, "For example:\n\tget DevTb");
	ShellCmdAdd("dj9eeprom", ShellCmd_DJ9eeprom, "For example:\n\tdj9eeprom r 84 00 05");
	ShellCmdAdd("mems", ShellCmd_MemSpace, "For example:\n\tmems 10");
	ShellCmdAdd("eoc", Eoc_Libmstar_Shell_Test, "For example:\n\eoc help");
	ShellCmdAdd("tlog", ShellCmd_Tlog, "For example:\n\ttlog start 192.168.243.118\n\ttlog stop\n\ttlog test EventCall\n\t");
	ShellCmdAdd("http", ShellCmd_Http, "For example:\n\http url cmd data");
	ShellCmdAdd("dhiperf", ShellCmd_IperfTest, "For example:\n\tiperf 192.168.243.30 192.168.243.31");

	ShellCmdAdd("remote_unlock", ShellCmd_remote_unlock, "For example:\n\remote_unlock devip devid gate cardid");
}



/*********************************************************************************************************
**  End Of File
*********************************************************************************************************/

