/**
  ******************************************************************************
  * @file    obj_ShellCmd.c
  * @author  czb
  * @version V00.01.00
  * @date    2022.2.11
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 

#include <dirent.h>
#include <sys/stat.h>
#include "task_Shell.h"

typedef struct
{
    int             runFlag;
    pthread_t	    tid;
  	cJSON	        *testScript;
	ShellCmdSource	source;
	int				ip;
}AutoTestRun_S;

#define AUTO_TEST_DELAY			"DELAY"
#define AUTO_TEST_CMD			"CMD"
#define AUTO_TEST_LOOP			"LOOP"
#define AUTO_TEST_CYCLES		"CYCLES"
#define AUTO_TEST_END_LOOP		"END_LOOP"

static AutoTestRun_S autoTestRun = {.testScript = NULL, .runFlag = 0};

//遍历json结构中所有的object的KEY，返回object
static void MyAutoTest(cJSON *testJson, int *runFlag, int ip, ShellCmdSource source)
{
    cJSON *current_element = NULL;
    cJSON *loopSycles = NULL;
	int loopCnt;

    if(testJson == NULL)
    {
        return;
    }

	if(testJson->child == NULL)
    {
        return;
    }

    current_element = testJson->child;

    while(current_element != NULL && (*runFlag))
    {
		if(!strncmp(current_element->string, AUTO_TEST_DELAY, strlen(AUTO_TEST_DELAY)) && cJSON_IsNumber(current_element))
        {
	        usleep(1000*current_element->valueint);
        }
        else if(!strncmp(current_element->string, AUTO_TEST_CMD, strlen(AUTO_TEST_CMD)) && cJSON_IsString(current_element))
        {
            API_ShellCommand(ip, source, current_element->valuestring);
        }
        else if(!strncmp(current_element->string, AUTO_TEST_LOOP, strlen(AUTO_TEST_LOOP)) && cJSON_IsObject(current_element))
        {
			loopSycles = cJSON_GetObjectItemCaseSensitive(current_element, AUTO_TEST_CYCLES);
			loopCnt = loopSycles->valueint;
			while((loopCnt--) && (*runFlag))
			{
	            MyAutoTest(current_element, runFlag, ip, source);
			}
        }
        current_element = current_element->next;
    }
}


static void* AutoTestProcess(void* arg)
{
    AutoTestRun_S* test = (AutoTestRun_S*)arg;
	if(test->runFlag)
	{
		MyAutoTest(test->testScript, &(test->runFlag), test->ip, test->source);
	}

	cJSON_Delete(test->testScript);
	test->testScript = NULL;
	test->runFlag = 0;
}

static char *trim(char *str) 
{ 
    if (str == NULL || *str == '\0') 
    { 
        return str; 
    } 

	//去除行尾空格
    int len = strlen(str); 
    char *p = str + len - 1; 
    while (p >= str && isspace(*p)) 
    { 
        *p = '\0'; --p; 
    } 

	//去除行首空格
	len = 0;
    p = str;
    while (*p != '\0' && isspace(*p)) 
    { 
        ++p; ++len; 
    }
    memmove(str, p, strlen(str) - len + 1); 

    return str; 
} 

static void TestScriptParse(cJSON* script, FILE* file)
{
	char cmdLine[SHELL_INPUT_LEN] = {0};
	char jsonKey[50];
	int delayCnt = 0;
	int loopCnt = 0;
	int cmdCnt = 0;

	while(fgets(cmdLine, SHELL_INPUT_LEN, file) != NULL)
	{
		trim(cmdLine);

		if(cmdLine[0] == '#')
		{
			continue;
		}
		else if(!strncmp(cmdLine, AUTO_TEST_DELAY, strlen(AUTO_TEST_DELAY)))
		{
			snprintf(jsonKey, 50, "%s%d", AUTO_TEST_DELAY, delayCnt++);
			cJSON_AddNumberToObject(script, jsonKey, atoi(trim(cmdLine + strlen(AUTO_TEST_DELAY))));
		}
		else if(!strncmp(cmdLine, AUTO_TEST_END_LOOP, strlen(AUTO_TEST_END_LOOP)))
		{
			return;
		}
		else if(!strncmp(cmdLine, AUTO_TEST_LOOP, strlen(AUTO_TEST_LOOP)))
		{
			cJSON* loopJson = cJSON_CreateObject();
			snprintf(jsonKey, 50, "%s%d", AUTO_TEST_LOOP, loopCnt++);
			cJSON_AddItemToObject(script, jsonKey, loopJson);
			cJSON_AddNumberToObject(loopJson, AUTO_TEST_CYCLES, atoi(trim(cmdLine + strlen(AUTO_TEST_LOOP))));
			TestScriptParse(loopJson, file);
		}
		else if(cmdLine[0] != 0)
		{
			snprintf(jsonKey, 50, "%s%d", AUTO_TEST_CMD, cmdCnt++);
			cJSON_AddStringToObject(script, jsonKey, cmdLine);
		}
	}
}

static cJSON * GetTestScriptJsonFromFile(const char* filePath)
{
	FILE* file = NULL;
	cJSON* retJson = NULL;
	
	if((file=fopen(filePath,"r")) != NULL)
	{
		retJson = cJSON_CreateObject();
		TestScriptParse(retJson, file);
		fclose(file);
	}

	return retJson;
}

static int AutoTestStart(const char* filePath, int ip, ShellCmdSource source)
{
	FILE* file = NULL;

	if(autoTestRun.runFlag)
	{
		return -3;
	}
	
	if((file=fopen(filePath,"r")) == NULL)
	{
		return -1;
	}

	autoTestRun.testScript = cJSON_CreateObject();
	TestScriptParse(autoTestRun.testScript, file);

	fclose(file);
	
	autoTestRun.ip = ip;
	autoTestRun.source = source;
	autoTestRun.runFlag = 1;
    if(pthread_create(&autoTestRun.tid, NULL, AutoTestProcess, (void*)&autoTestRun) != 0)
	{
		autoTestRun.runFlag = 0;
		return -2;
	}

	return 0;
}

static int AutoTestStop(void)
{
	if(autoTestRun.runFlag)
	{
		autoTestRun.runFlag = 0;
		pthread_join(autoTestRun.tid, NULL);
	}
	return 0;
}

 int GetAutoTestState(void)
{
	return autoTestRun.runFlag;
}
char * GetAutoTestScript(void)
{
	if(autoTestRun.runFlag)
	{
		return cJSON_Print(autoTestRun.testScript);
	}
	return NULL;
		
}
int ShellCmd_AutoTest(cJSON *cmd)
{
	char* testCtrlStr = GetShellCmdInputString(cmd, 1);
	char* testFileStr = GetShellCmdInputString(cmd, 2);

	if(testCtrlStr == NULL)
	{
		ShellCmdPrintf("Please input test parameter: start/stop/state\n");
	}
	else if(!strcmp(testCtrlStr, "start"))
	{
		ShellCmdSource source = SourceUart;
		int ip = 0;
		char* sourceStr = GetShellCmdInputString(cmd, 3);
		if(sourceStr)
		{
			if(!strcmp(sourceStr, "udp"))
			{
				source = SourceUDP;
			}
			else if(!strcmp(sourceStr, "ixd"))
			{
				source = SourceIXD;
			}

			ip = inet_addr(GetShellCmdInputString(cmd, 4));
		}	

		switch(AutoTestStart(testFileStr, ip, source))
		{
			case -1:
				ShellCmdPrintf("Auto test open file:%s error!!!\n", testFileStr);
				break;
			case -2:
				ShellCmdPrintf("Create pthread: AutoTestProcess error!!!\n");
				break;
			case -3:
				ShellCmdPrintf("Auto test is already running!!!\n");
				break;
			case 0:
				ShellCmdPrintf("Start auto test according to %s\n", testFileStr);
				break;

		}
	}
	else if(!strcmp(testCtrlStr, "stop"))
	{
		AutoTestStop();
		ShellCmdPrintf("Auto test is Stopped\n");
	}
	else if(!strcmp(testCtrlStr, "state"))
	{
		ShellCmdPrintf("Auto test is %s\n", GetAutoTestState() ? "running":"Stopped");
		char *script=GetAutoTestScript();
		if(script!=NULL)
		{
			ShellCmdPrintf("AutoTestScript:\n%s",script);
			free(script);
		}
	}
	else
	{
		ShellCmdPrintf("Auto test input error!!!\n");
	}
}




/*********************************************************************************************************
**  End Of File
*********************************************************************************************************/

