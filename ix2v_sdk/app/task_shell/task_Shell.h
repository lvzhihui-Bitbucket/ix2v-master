/**
  ******************************************************************************
  * @file    task_Shell.h
  * @author  czb
  * @version V00.01.00
  * @date    2022.2.11
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 

#ifndef _task_Shell_H
#define _task_Shell_H

#include "cJSON.h"
#include "RTOS.h"
#include "utility.h"

// Define Object Property-------------------------------------------------------
#define SHELL_INPUT_LEN			  2000
#define SHELL_OUTPUT_LEN			2000

typedef enum
{
    SourceUart,
    SourceUDP,
    SourceIXD,
}ShellCmdSource;

typedef int (*ShellCmdCallBack)(cJSON *cmd);

typedef struct
{
    int 			                ip;
    ShellCmdSource            source;
    char 	                    data[SHELL_INPUT_LEN+1];
} MsgShell;

typedef struct
{
		vdp_task_t			  		    task;
		Loop_vdp_common_buffer  	msgQ;
    cJSON                     *data;
    int 			                UDP_IP;
    int 			                XD_IP;
    ShellCmdSource            source;
  	FILE	                    *outputFile;
}SH_CMD_RUN_S;

extern SH_CMD_RUN_S shellRun;

int task_ShellInit(void);

const char* GetShellCmdInputString(const cJSON *input, int index);

int ShellCmdAdd(const char* cmd, ShellCmdCallBack callBack, const char* describe);
int ShellCmdDelete(char* cmd);
int API_ShellCommand(int ip, ShellCmdSource source, const char* cmd);

void ShellCmdTCPInputProcess(int ip, const char *input);
int ShellCmdPrintf(const char* format, ...);

#endif


