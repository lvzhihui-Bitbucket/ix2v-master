

#include "ip_video_cs_control.h"

VIDEO_CS_STATE 		global_cs_state;
ip_video_trans_type	video_c_type;
ip_camera_type		video_s_type;

// 当前视频源设备id
int				video_source_id = 0;
/*******************************************************************************************
 * @fn:		init_video_cs_service
 *
 * @brief:	初始化视频CS服务
 *
 * @param:	none
 *
 * @return: 	-1/err, 0/ok
*******************************************************************************************/
int init_video_cs_service(void)
{	
	// show test: will be moved
	PrintCurrentTime(11180);
	api_monitor_show_initial();
	PrintCurrentTime(11181);
	ffmpeg_init();
	init_ipc_rtp_send_list();
	// 
	PrintCurrentTime(11182);
	ip_video_control_init();
	PrintCurrentTime(11183);
	ip_camera_control_init();
	PrintCurrentTime(11184);

	vtk_TaskInit_video_reliability();

	global_cs_state	= VIDEO_CS_IDLE;
	video_c_type	= ip_video_none;
	video_s_type	= ip_camera_none;
	return 0;
}


/*******************************************************************************************
 * @fn:		api_get_video_cs_service_state
 *
 * @brief:	得到当前视频CS的状态 – 0-空闲/1-客户端/2-服务器端/3-C&S
 *
 * @param:	none
 *
 * @return: 	VIDEO_CS_STATE
*******************************************************************************************/
VIDEO_CS_STATE api_get_video_cs_service_state(void)
{
	return global_cs_state;
}

/*******************************************************************************************
 * @fn:		api_video_s_service_turn_on
 *
 * @brief:	开启服务器端视频输出服务 – 若为组播则为测试服务，强制打开视频组播输出
 *
 * @param:	trans_type	- 传输类型
 * @param:  	client_ip		- 客户端IP地址
 * @param:  	period		- 服务时间
 *
 * @return: 	-1/err, 0/ok
*******************************************************************************************/
int api_video_s_service_turn_on( ip_camera_type trans_type, int client_ip, int period )
{
	api_camera_server_turn_on( 0, trans_type, client_ip, period );	
	global_cs_state	= VIDEO_CS_SERVER;	
	video_s_type	= trans_type;
	return 0;
}

/*******************************************************************************************
 * @fn:		api_video_s_service_turn_off
 *
 * @brief:	关闭服务器端视频输出服务
 *
 * @param:	none
 *
 * @return: 	-1/err, 0/ok
*******************************************************************************************/
int api_video_s_service_turn_off( void )
{
	//czn_20170726
#if 0
	api_camera_server_turn_off();
	global_cs_state	= VIDEO_CS_IDLE;	
	video_s_type	= ip_camera_none;
	return 0;
#else
	delete_all_userlist();
	return 0;
#endif
}

/*******************************************************************************************
 * @fn:		api_video_c_service_turn_on
 *
 * @brief:	申请服务器端视频输出服务
 *
 * @param:  server_ip	- 服务器端IP地址
 * @param:	trans_type	- 传输类型
 * @param:	trans_reso 	- 分辨率
 * @param:  dev_id		- 服务器端设备id
 * @param:  period		- 服务时间
 *
 * @return: -1/err, 0/ok
*******************************************************************************************/
int api_video_c_service_turn_on( int ins, int server_ip, ip_video_trans_type trans_type, ip_video_trans_reso trans_reso, int dev_id, int period )
{
	api_video_client_turn_on(ins,trans_type,server_ip, dev_id, period, trans_reso);

	global_cs_state	= VIDEO_CS_CLIENT;
	video_c_type	= trans_type;

	//VIDEO_CLIENT_NOTIFY_REMOTE_ON();		//czn_20161008
		
	return 0;
}

/*******************************************************************************************
 * @fn:		api_video_c_service_turn_off
 *
 * @brief:	关闭客户器端视频输入服务
 *
 * @param:	none
 *
 * @return: 	-1/err, 0/ok
*******************************************************************************************/
int api_video_c_service_turn_off( int ins )
{
	api_video_client_turn_off(ins);

	global_cs_state	= VIDEO_CS_IDLE;	
	video_c_type	= ip_video_none;	

	//VIDEO_CLIENT_NOTIFY_REMOTE_OFF();		//czn_20161008
	
	return 0;
}

//////////////////////////////////////////////////////////////////////////////////////////////
// test code
//////////////////////////////////////////////////////////////////////////////////////////////
void test_transfer_video_on(int ins, int type, int reso, char* ipstr)
{
	ip_video_trans_reso trans_reso;
	trans_reso = Resolution_240P + reso;
	api_video_c_service_turn_on( ins, inet_addr(ipstr), (type==0)?ip_video_multicast:ip_video_unicast, trans_reso, 0, 3600 );  // 1000
}

void test_transfer_video_off(int ins)
{
	api_video_c_service_turn_off(ins);	
}

/*
API-JSON化：不仅仅用于Shell，也用于监视、呼叫、录像等业务
"IX_VIDEO_SUB_C": {
  "IP_ADDR": "192.145.4.32:1234",
  "RESO": "720P",
  "RESO_ITEM": [{"720P":2,"480P":1,"240P":0}],
  "CAST"："MUL",
  "CAST_ITEM": [{"UNI":1,"MUL":0}],  
  "TMOUT": 100000,            
} 
*/