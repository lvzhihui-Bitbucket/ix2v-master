#include <stdio.h>
#include <sys/sysinfo.h>
#include "task_Shell.h"
#include "task_survey.h"
#include "obj_GetIpByInput.h"
#include "ip_video_cs_control.h"
#include "obj_ak_decoder.h"
#include "task_Event.h"
#include "gui_lib.h"
#include "define_file.h"
#include "obj_Talk_Servi.h"
#include "obj_VtkUnicastCommandInterface.h"
#include "obj_GetInfoByIp.h"

//"Start Monitor DS/IPC:{\"TargetType\":\"DS/IPC\",\"TargetAddr\":\"192.168.243.222\",\"TargetNum\":\"0099000001\",\"Time\":600,\"TransType\":\"Mul/Uni\",
//\"DSResolution\":\"240P/480P/720P\",\"IPC_CH\":1/2/3,\"DisplayX\":0,\"DisplayY\":0,\"DisplayW\":400,\"DisplayH\":300,\"Codec_Ins\":0/1/2/3}"
cJSON *GetVideoCleintState(int ins);
cJSON *GetVideoServerState(int ins);
cJSON *GetVideoCleintAllState(void);
cJSON *GetVideoServerAllState(void);
int API_IX_video_c_open ( int ins, cJSON* IX_VIDEO_SUB_C);
int Ds_Show_Open(int ins,cJSON* display);

int ShellCmd_Monc(cJSON *cmd)
{
	char *para=GetShellCmdInputString(cmd, 1);
	char buff[50];
	int ip_addr;
	int ins;
	int trans_type;
	int trans_reso;
	int dispx,dispy,dispw,disph;
	char num[11];
	int time;
	IpCacheRecord_T record[32];
	int rev;
	cJSON *jpara,*para_arr;//*root;
	int i,array_num;
	cJSON *IX_VIDEO_SUB_C;
	cJSON *display;
	if(para==NULL)
	{
		ShellCmdPrintf("hava no para");
		return 1;
	}
	if(strcmp(para,"-s")==0)
	{
		para=GetShellCmdInputString(cmd, 2);
		if(para==NULL)
		{
			ShellCmdPrintf("hava no ins para");
			return 1;
		}
		ins=atoi(para);
		if(ins>=4)
		{
			ShellCmdPrintf("ins must be 0/1/2/3");
			return 1;
		}
		
		
		if((para=GetShellCmdInputString(cmd, 3))==NULL)
		{
			ShellCmdPrintf("have no cfg para");
			return 1;
		}
		if(para[0]!='{')
		{
			ShellCmdPrintf("unkown cfg para");
			return 1;
		}
		
		jpara=cJSON_Parse(para);
		if(jpara==NULL)
		{
			ShellCmdPrintf("json para parse error");
			return 1;
		}

		IX_VIDEO_SUB_C=cJSON_GetObjectItem(jpara,"IX_VIDEO_SUB_C");
		if(IX_VIDEO_SUB_C==NULL)
		{
			ShellCmdPrintf("have no IX_VIDEO_SUB_C para");
			cJSON_Delete(jpara);
			return 1;
		}
		display=cJSON_GetObjectItem(jpara,"display");
		if(display==NULL)
		{
			ShellCmdPrintf("have no display para");
			cJSON_Delete(jpara);
			return 1;
		}
		#if 1
		Ds_Show_Open(ins,display);
		API_IX_video_c_open (ins,IX_VIDEO_SUB_C);
		cJSON_Delete(jpara);
		ShellCmdPrintf("strart tmonitor ds");
		#endif
	}
	
	else if(strcmp(para,"-c")==0)
	{
		//para=GetShellCmdInputString(cmd, 2);
		if((para=GetShellCmdInputString(cmd, 2))==NULL)
		{
			ShellCmdPrintf("have no ins para");
			return 1;
		}
		if((ins=atoi(para))>=4)
		{
			ShellCmdPrintf("ins must be 0/1/2/3");	
			return 1;
		}
		ShellCmdPrintf("close tmonitor ds");
		api_video_c_service_turn_off(ins);
	
        	Clear_ds_show_layer(ins);
		#if 0
		i=2;
		while((para=GetShellCmdInputString(cmd, i))!=NULL)
		//if(para!=NULL)
		{
			i++;
			jpara=cJSON_Parse(para);
			if(jpara==NULL)
			{
				ShellCmdPrintf("json para parse error");
				return 1;
			}

			#if 0
			para_arr = cJSON_GetObjectItemCaseSensitive(root, "PARA_ARRAY");
			if(para_arr==NULL)
			{
				array_num=1;
				jpara=root;
			}
			else
			{
				array_num=cJSON_GetArraySize(para_arr);
				jpara=cJSON_GetArrayItem(para_arr,0);
			}
			for(i=0;i<array_num;i++)
			{
				if(i>0)
				{
					jpara=cJSON_GetArrayItem(para_arr,i);
				}
			#endif
			rev=GetJsonDataPro(jpara,"TargetType",buff);
			if(rev==0||strcmp(buff,"DS")==0)
			{
				if(GetJsonDataPro(jpara,"Codec_Ins",&ins)==0)
					ins=0;
				ShellCmdPrintf("close tmonitor ds");
				api_video_c_service_turn_off(ins);
				//clear_one_layer(ins + AK_VO_LAYER_VIDEO_1,1024, 600);
		        	Clear_ds_show_layer(ins);
			}
			//}
			cJSON_Delete(jpara);
		}
		if(i==2)
		{
			ShellCmdPrintf("hava no json para");
			return 1;
		}
		#endif
	}
	else if(strcmp(para,"state")==0)
	{
		#if 0
		para=GetShellCmdInputString(cmd, 2);
		if(para==NULL)
		{
			ShellCmdPrintf("hava no ins para");
			return 1;
		}
		i=atoi(para);
		if(i>=4)
		{
			ShellCmdPrintf("ins must be 0/1/2/3");
			return 1;
		}
		#endif
		jpara=GetVideoCleintAllState();
		char* temp;
		temp=cJSON_Print(jpara);
		if(temp==NULL)
		{
			ShellCmdPrintf("Get State File");
			return 1;
		}
		ShellCmdPrintf(temp);
		free(temp);
	}
	else
	{
		ShellCmdPrintf("Unkown para");
	}
	return 1;
}

int ShellCmd_Mons(cJSON *cmd)
{
	char *para=GetShellCmdInputString(cmd, 1);
	int ins;
	int rev;
	cJSON *jpara;//*root;
	int i,array_num;
	int win;
	int trans_type;
	int send_ip;
	int send_port;
	char  buff[50];
	char *ptr;
	if(para==NULL)
	{
		ShellCmdPrintf("hava no para");
		return 1;
	}
	if(strcmp(para,"-state")==0)
	{
		#if 0
		para=GetShellCmdInputString(cmd, 2);
		if(para==NULL)
		{
			ShellCmdPrintf("hava no ins para");
			return 1;
		}
		i=atoi(para);
		if(i>=3)
		{
			ShellCmdPrintf("ins must be 0/1/2");
			return 1;
		}
		#endif
		jpara=GetVideoServerAllState();
		char* temp;
		temp=cJSON_Print(jpara);
		if(temp==NULL)
		{
			ShellCmdPrintf("Get State File");
			return 1;
		}
		ShellCmdPrintf(temp);
		free(temp);
	}
	else if(strcmp(para,"-oview")==0)
	{
		para=GetShellCmdInputString(cmd, 2);
		if(para==NULL)
		{
			ShellCmdPrintf("hava no ins para");
			return 1;
		}
		ins=atoi(para);
		if(ins>=3)
		{
			ShellCmdPrintf("ins must be 0/1/2");
			return 1;
		}
		para=GetShellCmdInputString(cmd, 3);
		if(para==NULL)
		{
			ShellCmdPrintf("hava no win para");
			return 1;
		}
		if(strcmp(para,"WIN1")==0)
		{
			win=0;
		}
		else if(strcmp(para,"WIN2")==0)
		{
			win=1;
		}
		else if(strcmp(para,"WIN3")==0)
		{
			win=2;
		}
		else if(atoi(para)<=4)
		{
			win=atoi(para);
		}
		else
		{
			ShellCmdPrintf("win must be WIN1/WIN2/WIN3");
			return 1;
		}
		ShellCmdPrintf("open preview");
		api_ak_vi_ch_stream_preview_on(ins,win);  
		
	}
	else if(strcmp(para,"-cview")==0)
	{
		para=GetShellCmdInputString(cmd, 2);
		if(para==NULL)
		{
			ShellCmdPrintf("hava no ins para");
			return 1;
		}
		ins=atoi(para);
		if(ins>=3)
		{
			ShellCmdPrintf("ins must be 0/1/2");
			return 1;
		}
		
		ShellCmdPrintf("close preview");
		api_ak_vi_ch_stream_preview_off(ins);  
		
	}
	else if(strcmp(para,"hide_win")==0)
	{
		para=GetShellCmdInputString(cmd, 2);
		if(para==NULL)
		{
			ShellCmdPrintf("hava no ins para");
			return 1;
		}
		ins=atoi(para);
		if(ins>=3)
		{
			ShellCmdPrintf("ins must be 0/1/2");
			return 1;
		}
		api_ak_vi_ch_stream_preview_hide(ins);
	}
	else if(strcmp(para,"ch_win")==0)
	{
		para=GetShellCmdInputString(cmd, 2);
		if(para==NULL)
		{
			ShellCmdPrintf("hava no ins para");
			return 1;
		}
		ins=atoi(para);
		if(ins>=3)
		{
			ShellCmdPrintf("ins must be 0/1/2");
			return 1;
		}
		para=GetShellCmdInputString(cmd, 3);
		if(para==NULL)
		{
			ShellCmdPrintf("hava no win para");
			return 1;
		}
		win=atoi(para);
		if(win>4)
		{
			ShellCmdPrintf("ins must be 0/1/2/3/4");
			return 1;
		}
		api_ak_vi_ch_stream_preview_change_win(ins,win);
	}
	else if(strcmp(para,"-add_rtp")==0)
	{
		para=GetShellCmdInputString(cmd, 2);
		if(para==NULL)
		{
			ShellCmdPrintf("hava no ins para");
			return 1;
		}
		ins=atoi(para);
		if(ins>=3)
		{
			ShellCmdPrintf("ins must be 0/1/2");
			return 1;
		}
		para=GetShellCmdInputString(cmd, 3);
		if(para==NULL)
		{
			ShellCmdPrintf("hava no trans_type para");
			return 1;
		}
		if(strcmp(para,"MUL")==0)
		{
			trans_type=0;
		}
		else if(strcmp(para,"UNI")==0)
		{
			trans_type=1;
		}
		else
		{
			ShellCmdPrintf("trans_type must be MUL/UNI");
			return 1;
		}
		para=GetShellCmdInputString(cmd, 4);
		if(para==NULL)
		{
			ShellCmdPrintf("hava no ip para");
			return 1;
		}
		if((ptr=strstr(para,":"))==NULL)
		{
			ShellCmdPrintf("hava no port para");
			return 1;
		}
		memcpy(buff,para,ptr-para);
		buff[ptr-para]=0;
		send_ip=inet_addr(buff);
		send_port=atoi(ptr+1);
		ShellCmdPrintf("add rtp node");
		api_rtp_add_one_send_node(ins,trans_type,buff,send_port); 
		
	}
	else if(strcmp(para,"-del_rtp")==0)
	{
		para=GetShellCmdInputString(cmd, 2);
		if(para==NULL)
		{
			ShellCmdPrintf("hava no ins para");
			return 1;
		}
		ins=atoi(para);
		if(ins>=3)
		{
			ShellCmdPrintf("ins must be 0/1/2");
			return 1;
		}
		para=GetShellCmdInputString(cmd, 3);
		if(para==NULL)
		{
			ShellCmdPrintf("hava no trans_type para");
			return 1;
		}
		if(strcmp(para,"MUL")==0)
		{
			trans_type=0;
		}
		else if(strcmp(para,"UNI")==0)
		{
			trans_type=1;
		}
		else
		{
			ShellCmdPrintf("trans_type must be MUL/UNI");
			return 1;
		}
		para=GetShellCmdInputString(cmd, 4);
		if(para==NULL)
		{
			ShellCmdPrintf("hava no ip para");
			return 1;
		}
		if((ptr=strstr(para,":"))==NULL)
		{
			ShellCmdPrintf("hava no port para");
			return 1;
		}
		memcpy(buff,para,ptr-para);
		buff[ptr-para]=0;
		send_ip=inet_addr(buff);
		send_port=atoi(ptr+1);
		ShellCmdPrintf("del rtp node");
		api_rtp_del_one_send_node(ins,trans_type,buff,send_port); 
		
	}
	else if(strcmp(para,"-del_all_rtp")==0)
	{
		para=GetShellCmdInputString(cmd, 2);
		if(para==NULL)
		{
			ShellCmdPrintf("hava no ins para");
			return 1;
		}
		ins=atoi(para);
		if(ins>=3)
		{
			ShellCmdPrintf("ins must be 0/1/2");
			return 1;
		}
		
		ShellCmdPrintf("del all rtp node");
		api_rtp_del_all_send_node(ins); 
		
	}
	else if(strcmp(para,"-add_udp")==0)
	{
		para=GetShellCmdInputString(cmd, 2);
		if(para==NULL)
		{
			ShellCmdPrintf("hava no ins para");
			return 1;
		}
		ins=atoi(para);
		if(ins>=3)
		{
			ShellCmdPrintf("ins must be 0/1/2");
			return 1;
		}
		para=GetShellCmdInputString(cmd, 3);
		if(para==NULL)
		{
			ShellCmdPrintf("hava no trans_type para");
			return 1;
		}
		if(strcmp(para,"MUL")==0)
		{
			trans_type=0;
		}
		else if(strcmp(para,"UNI")==0)
		{
			trans_type=1;
		}
		else
		{
			ShellCmdPrintf("trans_type must be MUL/UNI");
			return 1;
		}
		para=GetShellCmdInputString(cmd, 4);
		if(para==NULL)
		{
			ShellCmdPrintf("hava no ip para");
			return 1;
		}
		if((ptr=strstr(para,":"))==NULL)
		{
			ShellCmdPrintf("hava no port para");
			return 1;
		}
		memcpy(buff,para,ptr-para);
		buff[ptr-para]=0;
		send_ip=inet_addr(buff);
		send_port=atoi(ptr+1);
		ShellCmdPrintf("add udp node");
		api_udp_add_one_send_node(ins,trans_type,send_ip,send_port); 
		
	}
	else if(strcmp(para,"-del_udp")==0)
	{
		para=GetShellCmdInputString(cmd, 2);
		if(para==NULL)
		{
			ShellCmdPrintf("hava no ins para");
			return 1;
		}
		ins=atoi(para);
		if(ins>=3)
		{
			ShellCmdPrintf("ins must be 0/1/2");
			return 1;
		}
		para=GetShellCmdInputString(cmd, 3);
		if(para==NULL)
		{
			ShellCmdPrintf("hava no trans_type para");
			return 1;
		}
		if(strcmp(para,"MUL")==0)
		{
			trans_type=0;
		}
		else if(strcmp(para,"UNI")==0)
		{
			trans_type=1;
		}
		else
		{
			ShellCmdPrintf("trans_type must be MUL/UNI");
			return 1;
		}
		para=GetShellCmdInputString(cmd, 4);
		if(para==NULL)
		{
			ShellCmdPrintf("hava no ip para");
			return 1;
		}
		if((ptr=strstr(para,":"))==NULL)
		{
			ShellCmdPrintf("hava no port para");
			return 1;
		}
		memcpy(buff,para,ptr-para);
		buff[ptr-para]=0;
		send_ip=inet_addr(buff);
		send_port=atoi(ptr+1);
		ShellCmdPrintf("del udp node");
		api_udp_del_one_send_node(ins,trans_type,send_ip,send_port); 
		
	}
	else if(strcmp(para,"start_quad")==0)
	{
		API_MonQuad_start();
	}
	else if(strcmp(para,"to_quad")==0)
	{
		VdShowToQuad();
	}
	else if(strcmp(para,"to_full")==0)
	{
		para=GetShellCmdInputString(cmd, 2);
		if(para==NULL)
		{
			ShellCmdPrintf("hava no win para");
			return 1;
		}
		win=atoi(para);
		EnterMonFull(win);
	}
	else if(strcmp(para,"exit_quad")==0)
	{
		ExitQuadMenu();
	}
	else if(strcmp(para,"start_pip")==0)
	{
		API_MonPip_start();
	}
	else if(strcmp(para,"sw_pip")==0)
	{
		PipShowSwitch();
	}
	else if(strcmp(para,"debug")==0)
	{
		ipc_show_debug();
		decoder_debug();
	}
	else if(strcmp(para,"voice_test")==0)
	{
		CallVoice("Event_CALL_SUCC");
	}
	else if(strcmp(para,"voice_test1")==0)
	{
		CallVoice("Event_CALL_FAIL_DND");
	}
	else if(strcmp(para,"cvbs_mcu")==0)
	{
		para=GetShellCmdInputString(cmd, 2);
		dt_video_output_sw(atoi(para));
	}
	else if(strcmp(para,"get_th_log")==0)
	{
		
		jpara=get_thread_log();
		char* temp;
		temp=cJSON_Print(jpara);
		if(temp==NULL)
		{
			ShellCmdPrintf("get_th_log Fail");
			return 1;
		}
		ShellCmdPrintf(temp);
		free(temp);
	}
	else if(strcmp(para,"rtsp_ser_start")==0)
	{
		para=GetShellCmdInputString(cmd, 2);
		if(para==NULL)
		{
			ShellCmdPrintf("hava no ins para");
			return 1;
		}
		ins=atoi(para);
		if(ins>=3)
		{
			ShellCmdPrintf("ins must be 0/1/2");
			return 1;
		}
		
		send_ip=inet_addr(GetSysVerInfo_IP());
		send_port=60001+ins;
		ShellCmdPrintf("rtsp_ser_start %d",ins);
		api_rtsp_udp_add_one_send_node(ins,1,send_ip,send_port); 
		
	}
	else if(strcmp(para,"rtsp_ser_stop")==0)
	{
		para=GetShellCmdInputString(cmd, 2);
		if(para==NULL)
		{
			ShellCmdPrintf("hava no ins para");
			return 1;
		}
		ins=atoi(para);
		if(ins>=3)
		{
			ShellCmdPrintf("ins must be 0/1/2");
			return 1;
		}
		
		
		send_ip=inet_addr(GetSysVerInfo_IP());
		send_port=60001+ins;
		ShellCmdPrintf("del udp node");
		api_rtsp_udp_del_one_send_node(ins,1,send_ip,send_port); 
		
	}
	else if(strcmp(para,"rtsp_ser_teston")==0)
	{
		para=GetShellCmdInputString(cmd, 2);
		if(para==NULL)
		{
			ShellCmdPrintf("hava no ins para");
			return 1;
		}
		ins=atoi(para);
		if(ins>=3)
		{
			ShellCmdPrintf("ins must be 0/1/2");
			return 1;
		}
		
		send_ip=inet_addr(GetShellCmdInputString(cmd, 3));
		send_port=60001+ins;
		ShellCmdPrintf("rtsp_ser_start %d",ins);
		api_rtsp_udp_add_one_send_node(ins,1,send_ip,send_port); 
		
	}
	else if(strcmp(para,"rtsp_ser_testoff")==0)
	{
		para=GetShellCmdInputString(cmd, 2);
		if(para==NULL)
		{
			ShellCmdPrintf("hava no ins para");
			return 1;
		}
		ins=atoi(para);
		if(ins>=3)
		{
			ShellCmdPrintf("ins must be 0/1/2");
			return 1;
		}
		
		
		send_ip=inet_addr(GetShellCmdInputString(cmd, 3));
		send_port=60001+ins;
		ShellCmdPrintf("del udp node");
		api_rtsp_udp_del_one_send_node(ins,1,send_ip,send_port); 
		
	}
	else
	{
		ShellCmdPrintf("Unkown para");
	}
	return 1;
}
float GetCPURatio(void);
cJSON *GetSystemStat(void)
{
	cJSON *root=cJSON_CreateObject();
	if(root==NULL)
		return NULL;
	char buff[50];
	sprintf(buff,"%.02f",GetCPURatio());
	cJSON_AddStringToObject(root,"CPU",buff);
	sprintf(buff,"%d kb",GetMemAvailable());
	cJSON_AddStringToObject(root,"MEM",buff);
	return root;
}
extern cJSON *GetLayerShowState(int ins);
extern cJSON *GetTransInfoState(int ins);
extern cJSON *GetSubscriberState(int ins);
extern cJSON *GetCodecState(int ins);
cJSON *GetVideoCleintState(int ins)
{
	cJSON *StateGroup[4]={NULL};
	char addr[20];
	cJSON *subState;
	if(ins>=4)
		return NULL;
	
	//if(StateGroup[ins]!=NULL)
	//	cJSON_Delete(StateGroup[ins]);
	StateGroup[ins]=cJSON_CreateObject();
	if(StateGroup[ins]==NULL)
		return NULL;
	
	//cJSON_AddNumberToObject(StateGroup[ins],"VideoCleintIns",ins);
	
	subState=GetSubscriberState(ins);
	if(subState!=NULL)
		cJSON_AddItemToObject(StateGroup[ins],"SubscriberState",subState);
	
	subState=GetTransInfoState(ins);
	if(subState!=NULL)
		cJSON_AddItemToObject(StateGroup[ins],"TransInfoState",subState);
	
	subState=GetCodecState(ins);
	if(subState!=NULL)
		cJSON_AddItemToObject(StateGroup[ins],"CodecStat",subState);
	
	subState=GetLayerShowState(ins);
	if(subState!=NULL)
		cJSON_AddItemToObject(StateGroup[ins],"LayerShowState",subState);	
	//subState=GetSystemStat();
	//if(subState!=NULL)
	//	cJSON_AddItemToObject(StateGroup[ins],"SystemStat",subState);
	return StateGroup[ins];
}

cJSON *GetVideoCleintAllState(void)
{
	static cJSON *all_state=NULL;
	int i;
	cJSON *one_ins;
	
	char buff[50];
	
	if(all_state!=NULL)
		cJSON_Delete(all_state);
	
	all_state=cJSON_CreateObject();
	if(all_state==NULL)
		return NULL;
	one_ins=GetSystemStat();
	if(one_ins!=NULL)
		cJSON_AddItemToObject(all_state,"SystemStat",one_ins);

	for(i=0;i<4;i++)
	{
		if(GetSubscriberStateInt(i)!=0)
		{
			sprintf(buff,"ins%d",i);
			one_ins=GetVideoCleintState(i);
			if(one_ins!=NULL)
				cJSON_AddItemToObject(all_state,buff,one_ins);
		}
		
	}

	return all_state;
	
}


cJSON *GetUdpTransList(int ins);
cJSON *GetViState(int ins);
cJSON *GetEncState(int ins);


cJSON *GetVideoServerState(int ins)
{
	cJSON *StateGroup[3]={NULL};
	cJSON *subState;
	if(ins>=3)
		return NULL;
	
	//if(StateGroup[ins]!=NULL)
	//	cJSON_Delete(StateGroup[ins]);
	StateGroup[ins]=cJSON_CreateObject();
	if(StateGroup[ins]==NULL)
		return NULL;
	
	//cJSON_AddNumberToObject(StateGroup[ins],"VideoServerIns",ins);
	
	subState=GetViState(ins);
	if(subState!=NULL)
		cJSON_AddItemToObject(StateGroup[ins],"ViState",subState);
	
	subState=GetEncState(ins);
	if(subState!=NULL)
		cJSON_AddItemToObject(StateGroup[ins],"EncState",subState);
	
	subState=GetUdpTransList(ins);
	if(subState!=NULL)
		cJSON_AddItemToObject(StateGroup[ins],"UdpTransList",subState);
	subState=GetSystemStat();
	if(subState!=NULL)
		cJSON_AddItemToObject(StateGroup[ins],"SystemStat",subState);
	
	return StateGroup[ins];
}

cJSON *GetVideoServerAllState(void)
{
	static cJSON *all_state=NULL;
	int i;
	cJSON *one_ins;
	
	char buff[50];
	
	if(all_state!=NULL)
		cJSON_Delete(all_state);
	
	all_state=cJSON_CreateObject();
	if(all_state==NULL)
		return NULL;
	one_ins=GetSystemStat();
	if(one_ins!=NULL)
		cJSON_AddItemToObject(all_state,"SystemStat",one_ins);

	for(i=0;i<3;i++)
	{
		if(GetViStateInt(i)!=0)
		{
			sprintf(buff,"ins%d",i);
			one_ins=GetVideoServerState(i);
			if(one_ins!=NULL)
				cJSON_AddItemToObject(all_state,buff,one_ins);
		}
		
	}

	return all_state;
	
}
int API_IX_video_c_open ( int ins, cJSON* IX_VIDEO_SUB_C)
{
/*
	"IX_VIDEO_SUB_C": {
  "IP_ADDR": "192.145.4.32:1234",
  "RESO": "720P",
  "RESO_ITEM": [{"720P":2,"480P":1,"240P":0}],
  "CAST":"MUL",
  "CAST_ITEM": [{"UNI":1,"MUL":0}],  
  "TMOUT": 100000,            
} 
*/
	char buff[100];
	int ip_addr;
	int trans_type;
	int trans_reso;
	int time;
	int rev;
	rev=GetJsonDataPro(IX_VIDEO_SUB_C,"IP_ADDR",buff);
	if(rev=0)
		return -1;
	ip_addr=inet_addr(buff);
	rev=GetJsonDataPro(IX_VIDEO_SUB_C,"RESO",buff);
	if(rev==0||strcmp(buff,"240P")==0)
		trans_reso=Resolution_240P;
	else if(strcmp(buff,"480P")==0)
		trans_reso=Resolution_480P;
	else
		trans_reso=Resolution_720P;
	rev=GetJsonDataPro(IX_VIDEO_SUB_C,"CAST",buff);
	if(rev==0||strcmp(buff,"MUL")==0)
		trans_type=ip_video_multicast;
	else
		trans_type=ip_video_unicast;
	rev=GetJsonDataPro(IX_VIDEO_SUB_C,"TMOUT",&time);
	if(rev=0)
		time=60*1000;
	time=time/1000;
	
	return api_video_c_service_turn_on( ins, ip_addr, trans_type, trans_reso, 0, time ); 
}
int Ds_Show_Open(int ins,cJSON* display)
{
/*
	{
"WIN_DEFINE": {
    "WIN1": "FULL",
    "WIN2": "Q1",
    "WIN3": "Q2",
    "WIN4": "Q3",
    "WIN5": "Q4",
    "WIN6": "PIP"
},

"SCR_DEFINE": {
  "FULL":[0,0,1024,600],
  "Q1":[0,0,512,300],
  "Q2":[512,0,512,300],
  "Q3":[0,300,512,300],
  "Q4":[512,300,512,300],
  "PIP":[512,300,512,300]
 }
} 
*/
	int dispx,dispy,dispw,disph;
	dispx=0;
	dispy=0;
	dispw=1024;
	disph=600;
	if(cJSON_IsString(display))
	{
		if(strcmp(display->valuestring,"WIN1")==0)
		{
			dispx=0;
			dispy=0;
			dispw=1024;
			disph=600;
		}
		else if(strcmp(display->valuestring,"WIN2")==0)
		{
			dispx=0;
			dispy=0;
			dispw=512;
			disph=300;
		}
		else if(strcmp(display->valuestring,"WIN3")==0)
		{
			dispx=512;
			dispy=0;
			dispw=512;
			disph=300;
		}
		else if(strcmp(display->valuestring,"WIN4")==0)
		{
			dispx=0;
			dispy=300;
			dispw=512;
			disph=300;
		}
		else if(strcmp(display->valuestring,"WIN5")==0)
		{
			dispx=512;
			dispy=300;
			dispw=512;
			disph=300;
		}
		else if(strcmp(display->valuestring,"WIN6")==0)
		{
			dispx=512;
			dispy=300;
			dispw=512;
			disph=300;
		}
		
	}
	else
	{
		;
	}
	return Set_ds_show_pos(ins,dispx,dispy,dispw,disph);
}
////////////////////////////////////////////

void Get_Win_ShowPos(int win_id, int* x, int* y, int* width, int* height)
{
	int vdshowH;
	const int bkgd_w1=MY_DISP_HOR_RES;
	const int bkgd_h1=MY_DISP_VER_RES;

	switch(win_id)
	{
		case 0:
			*x = 0;
			*y = 0;
			*width = bkgd_w1;
			*height = bkgd_h1;
			break;
		case 1:
			*x = 0;
			*y = 0;
			*width = bkgd_w1/2;
			*height = bkgd_h1/2;
			break;
		case 2:
			*x = bkgd_w1/2;
			*y = 0;
			*width = bkgd_w1/2;
			*height = bkgd_h1/2;
			break;
		case 3:
			*x = 0;
			*y = bkgd_h1/2;
			*width = bkgd_w1/2;
			*height = bkgd_h1/2;
			break;
		case 4:
			*x = bkgd_w1/2;
			*y = bkgd_h1/2;
			*width = bkgd_w1/2;
			*height = bkgd_h1/2;
			break;
		case 5:
			//608, 30, 320, 240,
			*x = 608;
			*y = 30;
			*width = 320;
			*height = 240;
			break;
		case -1:
			//608, 30, 320, 240,
			*x = 0;
			*y = 0;
			*width = 0;
			*height = 0;
			break;	
	}
	
}
cJSON *Get_Win_ShowPos_json(int win_id)
{
	int x,y,w,h;
	if(win_id>5)
		return NULL;
	Get_Win_ShowPos(win_id,&x,&y,&w,&h);
	
	cJSON *win=cJSON_CreateObject();
	
	cJSON_AddNumberToObject(win,"X",x);
	cJSON_AddNumberToObject(win,"Y",y);
	cJSON_AddNumberToObject(win,"W",w);
	cJSON_AddNumberToObject(win,"H",h);

	return win;
}

typedef struct
{
	int  deviceType;
	int  monState;
	char target_ip[21];
}Mon_Quad_Run;

Mon_Quad_Run MonWin[4]={0};
 ///////////////////////////////////////////
 pthread_mutex_t DsMon_lock=PTHREAD_MUTEX_INITIALIZER;
int EventMon_Process(cJSON *cmd)
{
	char buff[100] = {0};
	char ip_buff[20]={0};
	int ip_addr;
	int ins = 0;
	int winid = 0;
	int ch = 0;
	int link;
	int ret;
	int cache=0;
	int i;
	char* eventName = cJSON_GetStringValue(cJSON_GetObjectItemCaseSensitive(cmd, EVENT_KEY_EventName));
	if(eventName)
	{
		GetJsonDataPro(cmd,"DevType",buff);
		GetJsonDataPro(cmd,"MonIns",&ins);
		cache=0;
		GetJsonDataPro(cmd,"Cache",&cache);
		if(!strcmp(eventName, EventMonOpen))
		{
			//printf("22222222%s:%d:%08x\n",__func__,__LINE__,(int)cmd);
			if(strcmp(buff,"DS")==0)
			{
				int x,y,w,h;
				GetJsonDataPro(cmd,"MonWin",&winid);
				GetJsonDataPro(cmd,"DevIp",ip_buff);
				GetJsonDataPro(cmd,"MonLink",&link);
				ip_addr=inet_addr(ip_buff);
				int remote_ip;
				unsigned char remote_dev_id;
				
				if(MonWin[ins].monState!=0&&MonWin[ins].deviceType==0&&strcmp(MonWin[ins].target_ip,ip_buff)!=0)
				{
					MonWin[ins].monState =0;
					api_video_c_service_turn_off(ins);
				}
				for(i=0;i<4;i++)
				{
					if(MonWin[i].monState!=0&&MonWin[i].deviceType==0&&strcmp(MonWin[i].target_ip,ip_buff)==0)
					{
						if(ins==i)
						 	continue;
						return 0;
					}
				}
				if(link)
				{
					int link_result = send_ip_mon_link_req( ip_addr,0x40,1, &remote_ip, &remote_dev_id );
					if( link_result == 0 )
					{

						#if 1
						Get_Win_ShowPos(winid,&x,&y,&w,&h);
						Set_ds_show_pos(ins,x,y,w,h);
						if(cache)
							Clear_ds_show_layer(ins);
						#endif
						api_video_c_service_turn_on( ins, ip_addr, ip_video_multicast, 2, 0, 600 );
						
						
						MonWin[ins].deviceType = 0;
						MonWin[ins].monState =1;
						memcpy(MonWin[ins].target_ip, ip_buff, 20);
					}
					else
					{
						api_mon_req_result(ins,"ds mon link err!");
					}
				}
				else
				{
					#if 1
					Get_Win_ShowPos(winid,&x,&y,&w,&h);
					Set_ds_show_pos(ins,x,y,w,h);
					if(cache)
						Clear_ds_show_layer(ins);
					#endif
					api_video_c_service_turn_on( ins, ip_addr, ip_video_multicast, 2, 0, 600 );
					//api_video_c_service_turn_on( ins, ip_addr, ip_video_multicast, 1, 0, 600 );
	
					MonWin[ins].deviceType = 0;
					MonWin[ins].monState =1;
					memcpy(MonWin[ins].target_ip, ip_buff, 20);
				}
				
			}
			else if(strcmp(buff,"IPC")==0)
			{
				GetJsonDataPro(cmd,"MonWin",&winid);
				GetJsonDataPro(cmd,"MonCh",&ch);
				if(GetJsonDataPro(cmd,"DevIp",buff) != 0)
				{
					cJSON* chdat = cJSON_CreateArray();
					cJSON* win = Get_Win_ShowPos_json(winid);
					cJSON *pSub = NULL;
					ShellCmdPrintf("GetIpcChdat IP= %s\n", buff);
					GetIpcChdat(buff, chdat);				
					pSub = cJSON_GetArrayItem(chdat, 0);
					if(pSub != NULL)
					{					
						Start_OneIpc_Show(ins, cJSON_GetArrayItem(cJSON_GetObjectItemCaseSensitive(pSub, "CH_DAT"), ch), win, 0);
						MonWin[ins].deviceType = 1;
						MonWin[ins].monState =1;
					}
					cJSON_Delete(chdat);
					cJSON_Delete(win);
				}
				else
				{
					cJSON *info;
					cJSON* win = Get_Win_ShowPos_json(winid);
					GetJsonDataPro(cmd,"IpcInfo",&info);
					Start_OneIpc_Show(ins, info, win, 0);
					if(cache)
						Clear_ds_show_layer(ins);
					MonWin[ins].deviceType = 1;
					MonWin[ins].monState =1;
					cJSON_Delete(win);
				}
				
			}
			else if(strcmp(buff,"CAMERA")==0)
			{
				GetJsonDataPro(cmd,"MonWin",&winid);
				api_ak_vi_ch_stream_preview_on(ins,winid);  
			}
		}
		else if(!strcmp(eventName, EventMonClose))
		{
			//printf("22222222%s:%d\n",__func__,__LINE__);
			if(strcmp(buff,"DS")==0)
			{
				MonWin[ins].monState =0;
				api_video_c_service_turn_off(ins);
				stop_receive_decode_process(ins);
			}
			else if(strcmp(buff,"IPC")==0)
			{
				MonWin[ins].monState =0;
				Stop_OneIpc_Show(ins);
				#ifdef PID_IX850
				ipc_preview_delete();
				#endif
			}
			else if(strcmp(buff,"CAMERA")==0)
			{
				api_ak_vi_ch_stream_preview_off(ins);  
			}
		}
		else if(!strcmp(eventName, EventMonState))
		{
			char* message = GetEventItemString(cmd, "MonReqResult");
			if(message)
			{
				//API_TIPS(message);
				LV_API_TIPS(message,1);
			}
		}
		else if(!strcmp(eventName, EventGetMonList))
		{
			cJSON* devTb = NULL;
			IXS_ProxyGetTb(&devTb, NULL, 1);
			cJSON* view = cJSON_CreateArray();
			cJSON* where = cJSON_CreateObject();
			cJSON_AddStringToObject(where, "IX_TYPE", "DS");
			//cJSON_AddStringToObject(where, "DevModel", "DS");
			cJSON_AddStringToObject(where, "IX_ADDR", GetSysVerInfo_bd());
			//cJSON_AddStringToObject(where, "Platform", "IX1/2");
			if(API_TB_SelectBySort(devTb, view, where, 0) > 0)
			{
				//ShellCmdPrintJson(view);
			}
			cJSON_Delete(devTb);
			cJSON_Delete(view);
			cJSON_Delete(where);
		}
	}	
}

void api_ds_show(int ins_num, int win_id, char* ip, int link)
{
	if(ins_num>3)
	{
		ShellCmdPrintf("ins must be 0/1/2/3");
		return -1;
	}
	if(MonWin[ins_num].monState)
	{
		if(MonWin[ins_num].deviceType == 0)
		{
			if(GetSubscriberServerAddr(ins_num) == inet_addr(ip))
			{
				ShellCmdPrintf("ds ip[%s] is on!\n", ip);
				return -1;
			}
			else
			{
				api_ds_stop(ins_num);
				//usleep(1000*1000);
			}
		}
	}
	cJSON *mon_event;
	mon_event=cJSON_CreateObject();
	cJSON_AddStringToObject(mon_event, EVENT_KEY_EventName,EventMonOpen);
	cJSON_AddStringToObject(mon_event, "DevType","DS");
	cJSON_AddStringToObject(mon_event, "DevIp",ip);
	cJSON_AddNumberToObject(mon_event,"MonIns",ins_num);
	cJSON_AddNumberToObject(mon_event,"MonWin",win_id);
	cJSON_AddNumberToObject(mon_event,"MonLink",link);

	API_Event_Json(mon_event);
	
	cJSON_Delete(mon_event);
}
void api_ds_show2(int ins_num, int win_id, char* ip, int link,int cache)
{
	if(ins_num>3)
	{
		ShellCmdPrintf("ins must be 0/1/2/3");
		return -1;
	}
	if(MonWin[ins_num].monState)
	{
		if(MonWin[ins_num].deviceType == 0)
		{
			if(GetSubscriberServerAddr(ins_num) == inet_addr(ip))
			{
				ShellCmdPrintf("ds ip[%s] is on!\n", ip);
				return -1;
			}
			else
			{
				api_ds_stop(ins_num);
				//usleep(1000*1000);
			}
		}
	}
	cJSON *mon_event;
	mon_event=cJSON_CreateObject();
	cJSON_AddStringToObject(mon_event, EVENT_KEY_EventName,EventMonOpen);
	cJSON_AddStringToObject(mon_event, "DevType","DS");
	cJSON_AddStringToObject(mon_event, "DevIp",ip);
	cJSON_AddNumberToObject(mon_event,"MonIns",ins_num);
	cJSON_AddNumberToObject(mon_event,"MonWin",win_id);
	cJSON_AddNumberToObject(mon_event,"MonLink",link);
	cJSON_AddNumberToObject(mon_event,"Cache",cache);

	API_Event_Json(mon_event);
	
	cJSON_Delete(mon_event);
}
void api_ds_stop(int ins_num)
{
	if(ins_num>3)
	{
		ShellCmdPrintf("ins must be 0/1/2/3");
		return -1;
	}
	cJSON *mon_event;
	mon_event=cJSON_CreateObject();
	cJSON_AddStringToObject(mon_event, EVENT_KEY_EventName,EventMonClose);
	cJSON_AddStringToObject(mon_event, "DevType","DS");
	cJSON_AddNumberToObject(mon_event,"MonIns",ins_num);

	API_Event_Json(mon_event);
	
	cJSON_Delete(mon_event);
}
void api_ds_stop_force(int ins_num)
{
	if(ins_num>3)
	{
		ShellCmdPrintf("ins must be 0/1/2/3");
		return -1;
	}
	cJSON *mon_event;
	mon_event=cJSON_CreateObject();
	cJSON_AddStringToObject(mon_event, EVENT_KEY_EventName,EventMonClose);
	cJSON_AddStringToObject(mon_event, "DevType","DS");
	cJSON_AddNumberToObject(mon_event,"MonIns",ins_num);
	cJSON_AddTrueToObject(mon_event,"Force");
	//API_Event_Json(mon_event);
	EventMon_Process(mon_event);
	cJSON_Delete(mon_event);
}
void api_ds_stop_force_by_ip(int ds_ip)
{
	int i;
	for(i=0;i<4;i++)
	{
		if(MonWin[i].monState!=0&&MonWin[i].deviceType==0&&inet_addr(MonWin[i].target_ip)==ds_ip)
		{
			api_ds_stop_force(i);
			break;
		}
	}
}
void api_ipc_show(int ins_num, int win_id, int ch, char* ip)
{
	if(ins_num>3)
	{
		ShellCmdPrintf("ins must be 0/1/2/3");
		return -1;
	}
	cJSON *mon_event;
	mon_event=cJSON_CreateObject();
	cJSON_AddStringToObject(mon_event, EVENT_KEY_EventName,EventMonOpen);
	cJSON_AddStringToObject(mon_event, "DevType","IPC");
	cJSON_AddStringToObject(mon_event, "DevIp",ip);
	cJSON_AddNumberToObject(mon_event,"MonIns",ins_num);
	cJSON_AddNumberToObject(mon_event,"MonWin",win_id);
	cJSON_AddNumberToObject(mon_event,"MonCh",ch);
	
	API_Event_Json(mon_event);
	
	cJSON_Delete(mon_event);
}
void api_ipc_show_info(int ins_num, int win_id, cJSON* info,int cache)
{
	if(ins_num>3)
	{
		ShellCmdPrintf("ins must be 0/1/2/3");
		return -1;
	}
	cJSON *mon_event;
	mon_event=cJSON_CreateObject();
	cJSON_AddStringToObject(mon_event, EVENT_KEY_EventName,EventMonOpen);
	cJSON_AddStringToObject(mon_event, "DevType","IPC");
	cJSON_AddItemReferenceToObject(mon_event, "IpcInfo",info);
	cJSON_AddNumberToObject(mon_event,"MonIns",ins_num);
	cJSON_AddNumberToObject(mon_event,"MonWin",win_id);
	//cJSON_AddNumberToObject(mon_event,"MonCh",ch);
	cJSON_AddNumberToObject(mon_event,"Cache",cache);
	API_Event_Json(mon_event);
	
	cJSON_Delete(mon_event);
}
void api_ipc_stop(int ins_num)
{
	if(ins_num>3)
	{
		ShellCmdPrintf("ins must be 0/1/2/3");
		return -1;
	}
	cJSON *mon_event;
	mon_event=cJSON_CreateObject();
	cJSON_AddStringToObject(mon_event, EVENT_KEY_EventName,EventMonClose);
	cJSON_AddStringToObject(mon_event, "DevType","IPC");
	cJSON_AddNumberToObject(mon_event,"MonIns",ins_num);

	API_Event_Json(mon_event);
	
	cJSON_Delete(mon_event);
}

void api_mon_req_result(int ins_num, char* ret)
{
	cJSON *mon_event;
	mon_event=cJSON_CreateObject();
	cJSON_AddStringToObject(mon_event, EVENT_KEY_EventName,EventMonState);
	cJSON_AddNumberToObject(mon_event,"MonIns",ins_num);
	cJSON_AddStringToObject(mon_event,"MonReqResult",ret);

	API_Event_Json(mon_event);
	
	cJSON_Delete(mon_event);
}

const char* GetDeviceNameByIp(char* ip)
{
	DeviceInfo dev_info;
	static char	devName[21] = {0};
	API_GetInfoByIp(inet_addr(ip),2,&dev_info);
	memcpy(devName, dev_info.name1, 20);
	return devName;
}
int API_Monitor_start( void )
{

}

void API_ExitMonitor(void)
{
	int i;
	for( i = 0; i < 4; i++ )
	{
		if(MonWin[i].monState)
		{
			if(MonWin[i].deviceType == 0)
			{
				api_ds_stop(i);
			}
			else
			{
				api_ipc_stop(i);
			}
		}
	}
}

int Get_MonitorState(void)
{
	int i;
	for( i = 0; i < 4; i++ )
	{
		if(MonWin[i].monState)
		{
			return 1;
		}
	}
	return 0;
}
int Get_MonitorInsState(int index)
{
	return MonWin[index].monState;
}
#if 1
int DsTalkDeal(int ins)
{
	int rev = -1;
	cJSON *au_para;
	if(MonWin[ins].monState)
	{
		VtkUnicastCmd_Stru VtkUnicastCmd,rsp_cmd;
		int len = sizeof(VtkUnicastCmd_Stru);
	
		VtkUnicastCmd.call_type		= 0x34;
		VtkUnicastCmd.call_code		= CALL_STATE_TALK;
		if( api_udp_c5_ipc_send_req(inet_addr(MonWin[ins].target_ip),VTK_CMD_STATE_1005,(char*)&VtkUnicastCmd,sizeof(VtkUnicastCmd_Stru), (char*)&rsp_cmd, &len) == 0 )
		{
			if(rsp_cmd.rspstate == 0)
			{
				rev = 0;
				OpenAppTalk();
				au_para=cJSON_CreateObject();
				if(au_para!=NULL)
				{					
					cJSON_AddStringToObject(au_para,TalkType,TalkType_LocalAndUdp);
					cJSON_AddStringToObject(au_para,TargetIP,MonWin[ins].target_ip); 						
					API_TalkService_on(au_para);
					cJSON_Delete(au_para);
				}
			}
		}
	}
	return rev;
}
int DsTalkClose(int ins)
{
	VtkUnicastCmd_Stru VtkUnicastCmd;

	VtkUnicastCmd.call_type		= 0x34;
	VtkUnicastCmd.call_code		= CALL_STATE_BYE;
	if(MonWin[ins].monState)
		api_udp_c5_ipc_send_data(inet_addr(MonWin[ins].target_ip),VTK_CMD_STATE_1005,(char*)&VtkUnicastCmd,sizeof(VtkUnicastCmd_Stru));
	API_TalkService_off();
	return 0;
}
#endif

int DsUnlock1Deal(int ins)
{
	if(MonWin[ins].target_ip[0])
		API_Event_UnlockRequest(MonWin[ins].target_ip, "RL1", "Monitor_ToUnlock");
}
int DsUnlock2Deal(int ins)
{
	if(MonWin[ins].target_ip[0])
		API_Event_UnlockRequest(MonWin[ins].target_ip, "RL2", "Monitor_ToUnlock");	
}

int API_Monitor_shortcut( char* rm, char* name  )
{
	char* lanState = API_PublicInfo_Read_String(PB_SUB_LAN_STATE);
	if(lanState && !strcmp(lanState, "Connected"))
	{
		cJSON* devTb = cJSON_CreateArray();
		IXS_GetByNBR(NULL, NULL, rm, NULL, devTb);
		if(cJSON_GetArraySize(devTb))
		{
			char* ip = GetEventItemString(cJSON_GetArrayItem(devTb, 0),  IX2V_IP_ADDR);
			ShellCmdPrintf("API_Monitor_shortcut IP= %s\n", ip);
			// lzh_20230612_s
			api_ds_show(0, 0, ip, 1);
			// lzh_20230612_e
		}
		cJSON_Delete(devTb);
	}	
}