
#include "video_utility.h"
#include "define_file.h"

///////////////////////////////////////////////////////////////////////////////////////////////
// 组播服务
///////////////////////////////////////////////////////////////////////////////////////////////
int join_multicast_group2(char* net_device_name, int socket_fd, int mcg_addr )
{
	struct ifreq ifr;
	struct ip_mreq mreq;
	
	//Get Loacl IP Addr
	memset(ifr.ifr_name, 0, IFNAMSIZ);	
	sprintf(ifr.ifr_name,"%s", net_device_name);
	if(ioctl(socket_fd, SIOCGIFADDR, &ifr) == -1)//使用 SIOCGIFADDR 获取接口地址
	{
		printf("ioctl call failure!msg:%s\n", strerror(errno));
		goto add_multicast_group_error;
	}
	
	//Added The Multicast Group
	memset(&mreq, 0, sizeof(struct ip_mreq));
	mreq.imr_multiaddr.s_addr = mcg_addr;
	mreq.imr_interface.s_addr = htonl(inet_network(inet_ntoa(((struct sockaddr_in *)&ifr.ifr_addr)->sin_addr)));	// 本机地址加入mcg_addr组播组
	if(setsockopt(socket_fd, IPPROTO_IP, IP_ADD_MEMBERSHIP, &mreq, sizeof(struct ip_mreq)) == -1)
	{
		printf("Setsockopt IPPROTO_IP::IP_ADD_MEMBERSHIP Failure!\n");
		goto add_multicast_group_error;
	}

	// lzh_20170711_s
	//printf("join_multicast_group2: local_ip=0x%08x,mcg_addr=0x%08x\n", mreq.imr_interface.s_addr, mreq.imr_multiaddr.s_addr);
	// lzh_20170711_e

	return 0;
add_multicast_group_error:
	return -1;
}

int leave_multicast_group2(char* net_device_name, int socket_fd, int mcg_addr )
{
	struct ifreq ifr;
	struct ip_mreq mreq;

	//Get Loacl IP Addr
	sprintf(ifr.ifr_name,"%s", net_device_name);
	if(ioctl(socket_fd, SIOCGIFADDR, &ifr) == -1)
	{
		printf("ioctl call failure!msg:%s\n", strerror(errno));
		goto leave_multicast_group_error;
	}
	//Leave The Multicast Group
	memset(&mreq, 0, sizeof(struct ip_mreq));
	mreq.imr_multiaddr.s_addr = mcg_addr;
	mreq.imr_interface.s_addr = htonl(inet_network(inet_ntoa(((struct sockaddr_in *)&ifr.ifr_addr)->sin_addr)));
	if(setsockopt(socket_fd, IPPROTO_IP, IP_DROP_MEMBERSHIP, &mreq, sizeof(struct ip_mreq)) == -1)		// 本机地址离开mcg_addr组播组
	{
		printf("Setsockopt IPPROTO_IP::IP_DROP_MEMBERSHIP Failure!\n");
		goto leave_multicast_group_error;
	}

	// lzh_20170711_s
	//printf("leave_multicast_group2: local_ip=0x%08x,mcg_addr=0x%08x\n", mreq.imr_interface.s_addr, mreq.imr_multiaddr.s_addr);
	// lzh_20170711_e
	
	return 0;
	
leave_multicast_group_error:
	return -1;
}

///////////////////////////////////////////////////////////////////////////////////////////////
// SDCARD服务
///////////////////////////////////////////////////////////////////////////////////////////////
static int getSdcardSize( char *path, int *freeSize, int *totalSize ) 
{
	if(!path || !freeSize || !totalSize)
	{
		return -1;
	}

	if(strlen(path) >= strlen(DISK_SDCARD) && !memcmp(path, DISK_SDCARD, strlen(DISK_SDCARD)))
	{
		if(!Judge_SdCardLink())
		{
			*freeSize = 0;
			*totalSize = 0;
			return 0;
		}		
	}

	struct statfs myStatfs;
	if( statfs(path, &myStatfs) == -1 ) 
		return -1;
	 
	 //濞夈劍妲憀ong long閿涘矂浼╅崗宥嗘殶閹诡喗瀛╅崙锟?
	 *freeSize = (int)((((long long)myStatfs.f_bsize * (long long)myStatfs.f_bfree) / (long long) (1024*1024)));
	 *totalSize = (int)((((long long)myStatfs.f_bsize * (long long)myStatfs.f_blocks) /(long long) (1024*1024)));

	 return 0;
}

int getSdcardSpace( void )
{
	int freeSize_ = 0;
	int totalSize_ = 0;

	if( getSdcardSize(	DISK_SDCARD, &freeSize_, &totalSize_ ) != 0 )
		return -1;

	printf(" totalSize_ : %d    freeSize_: %d \n", totalSize_, freeSize_ );
	
	//if( freeSize_ >= 20 )
	//	return 0;
	
	return freeSize_;
}

int GetPhotoNum(const char *path)
{

	char linestr[100];
	int ret = 0;
	
	snprintf(linestr, 100, "ls -l %s|grep \".jpg\"|wc -l\n", path);
	
	FILE *pf = popen(linestr,"r");
	if(pf == NULL)
	{
		return -1;
	}

	if(fgets(linestr,100,pf) != NULL)
	{
		ret = atoi(linestr);
	}
	else
	{
		ret = -2;
	}

	printf(" ---------------GetPhotoNum : %d \n", ret);
	
	pclose(pf);
	
	return ret;
}

int Judge_SdCardLink(void)
{
	int ret = 0;
	int file;
	DIR *dir = NULL;
	dir = opendir(DISK_SDCARD);  
	if(dir == NULL)
	{
		return ret;
	}
	closedir(dir);

    file = open(SDCARD_DEVICE_FILE, O_RDWR);
	if(file <= 0)
	{
		printf("---open device %s failed!----\n", SDCARD_DEVICE_FILE); 
		DeleteFileProcess(DISK_SDCARD, NULL);    
		return ret;
	}
	close(file);

	char buffer[500];
	char fileSystem[100];
	char mountOn[100];
	FILE *pf = popen("df -h /mnt/sdcard", "r");

	if(pf == NULL)
	{
		printf("---open cmd df -h /mnt/sdcard failed!----\n");
		return ret;
	}
	
	while(fgets(buffer, 500, pf))
	{
		sscanf(buffer, "%s%*s%*s%*s%*s%s", fileSystem, mountOn);
		if(!strcmp(fileSystem, SDCARD_DEVICE_FILE) && !strcmp(mountOn, DISK_SDCARD))
		{
			ret = 1;
			break;
		}
	}
	pclose(pf);

	if(!ret)
	{
		DeleteFileProcess(DISK_SDCARD, NULL); 
	}
	
	return ret;
}

int API_GetSDCardSize(int *freesize,int *totalsize)
{
	return getSdcardSize(DISK_SDCARD, freesize,totalsize);
}

int Delete_VideoFile(char *filename)
{
	char full_file_name[200];
	char* temp;

	//if(strstr(filename, ".jpg") != NULL)
	if(Judge_SdCardLink() == 0) 
		temp = JPEG_STORE_DIR;
	else
		temp = VIDEO_STORE_DIR;
		
	snprintf(full_file_name,200,"%s/%s",temp,filename);
	
	remove(full_file_name);

	return 0;
}

int GetVideoNum(void)
{

	char linestr[100];
	int ret = 0;
	char* path;
	if(Judge_SdCardLink() == 0) 
		path = JPEG_STORE_DIR;
	else
		path = VIDEO_STORE_DIR;
	
	snprintf(linestr, 100, "ls -l %s|grep \".mp4\"|wc -l\n", path);
	
	FILE *pf = popen(linestr,"r");
	if(pf == NULL)
	{
		return -1;
	}

	if(fgets(linestr,100,pf) != NULL)
	{
		ret = atoi(linestr);
	}
	else
	{
		ret = -2;
	}

	printf(" ---------------GetVideoNum : %d \n", ret);
	
	pclose(pf);
	
	return ret;
}


