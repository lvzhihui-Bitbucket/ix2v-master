#ifndef _OBJ_AVI_FILE_PARSER_H_
#define _OBJ_AVI_FILE_PARSER_H_

#include "utility.h"

//////////////////////////////////////////////////////////////////////////  
#define AVIIF_KEYFRAME  				0x00000010L		// ��������Ĺؼ�֡��־
#define AVIIF_LIST  					0x00000001L  
  
// Flags for dwFlags|AVI澶翠腑鐨勬爣蹇椾�?
#define AVIFILEINFO_HASINDEX        	0x00000010  // �Ƿ������� 
#define AVIFILEINFO_MUSTUSEINDEX    	0x00000020  
#define AVIFILEINFO_ISINTERLEAVED   	0x00000100  
#define AVIFILEINFO_WASCAPTUREFILE  	0x00010000  
#define AVIFILEINFO_COPYRIGHTED     	0x00020000  
  
// ���������AVIͷ��С 
#define MAX_ALLOWED_AVI_HEADER_SIZE 131072  
  
// �򲻿��ļ� 
#define ERROR_OPEN_AVI              		0  
// ������ЧAVI  
#define ERROR_INVALID_AVI           		1  
// ��ЧAVI  
#define SUCCESS_VALID_AVI           		2  


typedef uint32 			DWORD;		// ˫��
typedef uint16 			WORD;		// ���� 
typedef DWORD 			LONG;		// ���峤���� 
typedef uint8 			BYTE;		// �ֽ�
typedef DWORD 			FourCC;		// ����four cc;
//typedef int				bool;


#ifndef FALSE
#define FALSE 0
#endif

#ifndef TRUE
#define TRUE 1
#endif

// ����fourcc��Ӧ������ֵ��avi�ļ��б������С��
#define FOURCC_RIFF		0x46464952
#define FOURCC_AVI		0x20495641
#define FOURCC_LIST		0x5453494C
#define FOURCC_hdrl  	0x6C726468
#define FOURCC_avih		0x68697661
#define FOURCC_strl		0x6C727473
#define FOURCC_strh		0x68727473
#define FOURCC_strf		0x66727473
#define FOURCC_STRD		0x64727473
#define FOURCC_vids		0x73646976
#define FOURCC_auds		0x73647561
#define FOURCC_INFO		0x4F464E49
#define FOURCC_ISFT		0x54465349
#define FOURCC_idx1		0x31786469
#define FOURCC_movi		0x69766F6D
#define FOURCC_JUNK		0x4B4E554A
#define FOURCC_vprp		0x70727076
#define FOURCC_PAD		0x20444150
#define FOURCC_DIV3		861292868
#define FOURCC_DIVX		1482049860
#define FOURCC_XVID		1145656920
#define FOURCC_DX50		808802372
  
#define FOURCC_fmt		0x20746D66    // for WAVE files
#define FOURCC_data		0x61746164   // for WAVE files
#define FOURCC_WAVE		0x45564157   // for WAVE files

#define FOURCC_00dc		0x63643030   // for WAVE files
#define FOURCC_01wb		0x62773130   // for WAVE files

#define FOURCC_H264		0x34363268
#define FOURCC_H265		0x31766568

// ��ɫ��
typedef struct  
{  
	BYTE rgbBlue;                       	// ��  
	BYTE rgbGreen;                       	// ��
	BYTE rgbRed;                        	// ��
	BYTE rgbReserved;                   	// ���� 
} RGBQUAD;  
  
// AVI��ͷ�� 
typedef struct  
{  
	FourCC fcc;                         // ����Ϊ avih  
	DWORD cb;                           // �����ݽṹ�Ĵ�С�������������8���ֽڣ�fcc��cb������
	DWORD dwMicroSecPerFrame;           // ��Ƶ֡���ʱ�䣨�Ժ���Ϊ��λ��
	DWORD dwMaxBytesPerSec;             // ���AVI�ļ������������
	DWORD dwPaddingGranularity;         // ������������
	DWORD dwFlags;                      // AVI�ļ���ȫ�ֱ�ǣ������Ƿ����������
	DWORD dwTotalFrames;                // ��֡��
	DWORD dwInitialFrames;              // Ϊ������ʽָ����ʼ֡�����ǽ�����ʽӦ��ָ��Ϊ0��
	DWORD dwStreams;                    // ���ļ����������ĸ���
	DWORD dwSuggestedBufferSize;        // �����ȡ���ļ��Ļ����С��Ӧ���������Ŀ飩
	DWORD dwWidth;                      // ��Ƶͼ��Ŀ���������Ϊ��λ��
	DWORD dwHeight;                     // ��Ƶͼ��ĸߣ�������Ϊ��λ��
	DWORD dwReserved[4];                // ����
} AVIMainHeader;  //4*16 = 64
  
  // ����������� 
typedef struct  
{  
	uint16 left;                     // ��߾�
	uint16 top;                      // ���߾�
	uint16 right;                    // �ұ߾�
	uint16 bottom;                   // �ױ߾�
}RECT;  
  
  // AVI��ͷ��
typedef struct  
{  
	FourCC fcc;                         // ����Ϊ strh
	DWORD cb;                           // �����ݽṹ�Ĵ�С,�����������8���ֽ�(fcc��cb������)
	FourCC fccType;                     // ��������: auds(��Ƶ��) vids(��Ƶ��) mids(MIDI��) txts(������)  
	FourCC fccHandler;                  // ָ�����Ĵ����ߣ���������Ƶ��˵���ǽ�����
	DWORD dwFlags;                      // ��ǣ��Ƿ�����������������ɫ���Ƿ�仯�� 
	WORD wPriority;                     // �������ȼ������ж����ͬ���͵���ʱ���ȼ���ߵ�ΪĬ������  
	WORD wLanguage;                     // ����  
	DWORD dwInitialFrames;              // Ϊ������ʽָ����ʼ֡�� 
	DWORD dwScale;                      // ÿ֡��Ƶ��С������Ƶ������С
	DWORD dwRate;                       // dwScale/dwRate��ÿ�������
	DWORD dwStart;                      // ���Ŀ�ʼʱ��
	DWORD dwLength;                     // ���ĳ��ȣ���λ��dwScale��dwRate�Ķ����йأ�
	DWORD dwSuggestedBufferSize;        // ��ȡ��������ݽ���ʹ�õĻ����С
	DWORD dwQuality;                    // �����ݵ�����ָ�꣨0 ~ 10,000��
	DWORD dwSampleSize;                 // Sample�Ĵ�С 
	RECT rcFrame;                       // ָ�����������Ƶ����������������Ƶ�������е���ʾλ�ã���Ƶ��������AVIMAINHEADER�ṹ�е�dwWidth��dwHeight����
} AVIStreamHeader;  //4*16 = 64
  
  // λͼͷ 
typedef struct  
{  
	DWORD  biSize;  
	LONG   biWidth;  
	LONG   biHeight;  
	WORD   biPlanes;  
	WORD   biBitCount;  
	DWORD  biCompression;  
	DWORD  biSizeImage;  
	LONG   biXPelsPerMeter;  
	LONG   biYPelsPerMeter;  
	DWORD  biClrUsed;  
	DWORD  biClrImportant;  
} BitmapInfoHeader;	//4*10 = 40
  
  // λͼ��Ϣ
typedef struct  
{  
	BitmapInfoHeader bmiHeader;         // λͼͷ
	RGBQUAD bmiColors[1];               // ��ɫ��
} BitmapInfo;  
  
  // ��Ƶ������Ϣ
typedef struct  
{  
	WORD wFormatTag;  
	WORD nChannels;                     // ������
	DWORD nSamplesPerSec;               // ������
	DWORD nAvgBytesPerSec;              // ÿ���������
	WORD nBlockAlign;                   // ���ݿ�����־
	WORD wBitsPerSample;                // ÿ�β�����������
	WORD cbSize;                        // ��С
} WaveFormatEx;  //18
  
  // �����ڵ���Ϣ
typedef struct  
{  
	DWORD dwChunkId;                    // �����ݿ�����ַ���(00dc 01wb)
	DWORD dwFlags;                      // ˵�������ݿ��ǲ��ǹؼ�֡���ǲ��ǡ�rec ���б�����Ϣ
	DWORD dwOffset;                     // �����ݿ����ļ��е�ƫ����
	DWORD dwSize;                       // �����ݿ�Ĵ�С
} AVIIndexEntry;  

// ������Ϣ
typedef struct
{
	FourCC fcc;                         // ����Ϊ��idx1��
	DWORD cb;                           // �����ݽṹ�Ĵ�С�������������8���ֽڣ�fcc��cb������
	uint32 position;                  // ������ʼλ��ƫ��
	AVIIndexEntry*  videoIndexMap;      // ��Ƶ������,00dc��ת�������α�ʾ
	AVIIndexEntry*  audioIndexMap;		// ��Ƶ������,00wb��ת�������α�ʾ
} AVIIndex;

typedef enum
{
	 RECORD_PLAYBACK_IDLE,
	 RECORD_PLAYBACK_START,
	 RECORD_PLAYBACK_RUN,
	 RECORD_PLAYBACK_STOP,
	 RECORD_PLAYBACK_PAUSE,
	 RECORD_PLAYBACK_END,
	 RECORD_PLAYBACK_REPLAY,
}RECORD_PLAYBACK_STATE_t;


typedef struct
{
    //��AVI�ļ� 
	FILE* aviFd;  
	
	pthread_mutex_t 	lock;

	//�Ƿ�����Чavi
	int isValid;

	//�Ƿ�����Ƶ
	int hasVideo;  

	//�Ƿ�����Ƶ
	int hasAudio;
	
	// �ļ����Ȯ害
	uint32 fLen; 
	
	//aviͷ
	AVIMainHeader aviMainHeader;  
	
	//avi��Ƶ��ͷ
	AVIStreamHeader aviVideoStreamHeader;  
	
	//avi��Ƶ��ͷ
	AVIStreamHeader aviAudioStreamHeader;
	
	//λͼ��Ϣ
	BitmapInfo bitmapInfo;
	
	//��Ƶ��Ϣ
	WaveFormatEx waveInfo;
	
	//// ������Ϣ
	AVIIndex aviIndex; 	
	
	// movi�Ŀ�ʼλ��ƫ��
	uint32 moviOff;  

	//�����Ƶ֡��С
	uint32 maxFrameSize;  
	
	// ��ǰ��Ƶλ������(�����е�λ��,�±�)
	//uint32 currVideoIndex;
} AVIFileParset;

uint8 	parseAviFile(AVIFileParset* aviParset, const char * fileName);
int 	getVideoFrame(AVIFileParset* aviParset, char* buf, int index);
int 	getAudioFrame(AVIFileParset* aviParset, char* buf, int index);
int 	getTotalVideoFrames(AVIFileParset* aviParset);
int 	getTotalAudioFrames(AVIFileParset* aviParset);
void 	CloseAviFile(AVIFileParset* aviParset);
void getVideoSize(AVIFileParset* aviParset, int* width, int* height, int* vdtype, int* fps);


#endif


