#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <dirent.h>
#include <fcntl.h>
#include <getopt.h>
#include <unistd.h>

#include "ak_common.h"
#include "ak_log.h"
#include "ak_common_video.h"
#include "ak_venc.h"
#include "ak_thread.h"
#include "ak_mem.h"
#include "ak_vi.h"

#include "jpeg_enc.h"
#include "jpeg_dec.h"

extern JPEG_PLAYBACK_DAT_T JpegInfo;

struct ak_one_encoder* ak_new_one_encode( int type, int dst_w, int dst_h )
{
    int ret = -1;
	struct ak_one_encoder *pOneEncoder;
	
	pOneEncoder = (struct ak_one_encoder *)malloc(sizeof(struct ak_one_encoder));
	pOneEncoder->en_type = type;
	pOneEncoder->width = dst_w;
	pOneEncoder->height = dst_h;
    /* open venc */
    struct venc_param ve_param;
	if( type == 0 )
	{
		ve_param.enc_out_type = MJPEG_ENC_TYPE;
	}
	else if( type == 1 )
	{
		ve_param.enc_out_type = H264_ENC_TYPE;
	}
	else
	{
		ve_param.enc_out_type = HEVC_ENC_TYPE;
	}
	ve_param.width  = dst_w;            //resolution width
    ve_param.height = dst_h;           //resolution height
    ve_param.fps    = 20;               //fps set
    ve_param.goplen = 50;               //gop set
    ve_param.target_kbps = 800;         //k bps
    ve_param.max_kbps    = 1024;        //max kbps
    ve_param.br_mode     = BR_MODE_CBR; //br mode
    ve_param.minqp       = 25;          //qp set
    ve_param.maxqp       = 50;          //qp max value
    ve_param.initqp       = (ve_param.minqp + ve_param.maxqp)/2;    //qp value
    ve_param.jpeg_qlevel = JPEG_QLEVEL_DEFAULT;     //jpeg qlevel
    ve_param.chroma_mode = CHROMA_4_2_0;            //chroma mode
    ve_param.max_picture_size = 0;                  //0 means default
    ve_param.enc_level        = 30;                 //enc level
    ve_param.smart_mode       = 0;                  //smart mode set
    ve_param.smart_goplen     = 100;                //smart mode value
    ve_param.smart_quality    = 50;                 //quality
    ve_param.smart_static_value = 0;                //value
    ve_param.enc_out_type = MJPEG_ENC_TYPE;           //enc type

	ve_param.profile = PROFILE_JPEG;
	#if 0
	if(ve_param.enc_out_type == H264_ENC_TYPE)
	{
		/* profile type */
		ve_param.profile	  = PROFILE_MAIN;

	}
	else if(ve_param.enc_out_type == MJPEG_ENC_TYPE)
	{
		/* jpeg enc profile */
		//ve_param.initqp = 80;
		ve_param.profile = PROFILE_JPEG;
	}
	else
	{
		/* hevc profile */
		ve_param.profile = PROFILE_HEVC_MAIN;
	}
	#endif
	
    ret = ak_venc_open(&ve_param, &pOneEncoder->venc_handle);
    if (ret || (-1 == pOneEncoder->venc_handle) )
    {
        ak_print_error_ex(MODULE_ID_VENC, "open venc failed\n");
		if( pOneEncoder )
		{
			free(pOneEncoder);
			pOneEncoder = NULL;
		}
		return NULL;
    }
	 /* close the venc*/
	//ak_venc_close(pOneEncoder->venc_handle);
	return pOneEncoder;
}


struct ak_one_encoder *ptr_jpegenc_array[4]={0};
int jpegInit_capture(void)
{

	ptr_jpegenc_array[0] = ak_new_one_encode(0, 1280, 720);
	//ptr_jpegenc_array[0] = ak_new_one_encode(0, 640, 480);
	printf("jpegInit_capture handle = %d\n", ptr_jpegenc_array[0]->venc_handle);
    return 0;
	
}

//extern int Convert_Format(char* psrc, int len);
//extern char TarDat[];

int API_JpegCaptureing(unsigned char *data, int read_len)
{
    int ret = -1;
    FILE *save_fp = NULL;
	//char file_name[128] = "/mnt/nand1-2/photo/pho1.jpg";
	//struct video_input_frame frame;
	//memset(&frame, 0x00, sizeof(frame));
	if ( jpegState_get() != CAPTURE_RUN )
	{
		return -1;
	}
	char fullFileName[100];
	snprintf(fullFileName,100,"%s/%s",JPEG_STORE_DIR,JpegInfo.FileName);
	printf("-----fullFileName:%s-----\r\n", fullFileName);
	save_fp = fopen(fullFileName, "w+");
	if( save_fp == NULL )
	{
		printf("open JpegEncode file failed...\n");
		return -1;
	}

	//if( Convert_Format(data,640*480*3/2) == 0 )
	//	printf("ConvertFormat OK!!!\n");
	//else
	//	printf("ConvertFormat just err!!!");

	
	/* send it to encode */
	struct video_stream *stream = ak_mem_alloc(MODULE_ID_VENC, sizeof(struct video_stream));
	//frame.mdinfo = (void*)dat;
	ret = ak_venc_encode_frame(ptr_jpegenc_array[0]->venc_handle, data, 1280*720*3/2, NULL, stream);
	if (ret)
	{
		/* send to encode failed */
		ak_print_error_ex(MODULE_ID_VENC, "send to encode failed\n");
		ak_venc_close(ptr_jpegenc_array[0]->venc_handle);
	}
	else
	{
		printf( "-------get encode_frame data[%s] len[%d]----------\n",stream->data, stream->len );
		fwrite(stream->data, stream->len, 1, save_fp);
		ak_venc_release_stream(ptr_jpegenc_array[0]->venc_handle, stream);
		ak_mem_free(stream);
		fclose(save_fp);
		API_JpegCaputureStop();
	}
	return 0;
}



//-------------------------------------------------------------------------------------
// 播放接口
//-------------------------------------------------------------------------------------

int API_JpegCaptureStart(char* file_name,CallBackFun fun)
{   
	if ( jpegState_get() == JPEG_STATE_IDLE )
	{
		// check&create JPEG store directory
		if( access( JPEG_STORE_DIR, F_OK ) < 0 )
		{
			if( mkdir( JPEG_STORE_DIR, 777 ) < 0 )
			{
				return -3;
			}
		}
		
		int GetPhotoNum(const char *path);
		int photoNum = GetPhotoNum(JPEG_STORE_DIR);
		
		if(photoNum >= 200 || photoNum < 0)
		{
			return	-2;
		}

		//if( jpegInit()==0)
		if( jpegInit_capture() == 0 )  // lzh_20171117
		{
			printf("------Jpeg  Inited!!!------\r\n");
			jpegState_set(JPEG_INIT);
		}

		if ( jpegState_get() == JPEG_INIT )
	    {
	    	//延时一秒拍照
	    	//sleep(1);
			jpegState_set(CAPTURE_RUN);
			
	    	strcpy(JpegInfo.FileName, file_name);
			JpegInfo.CallBack = fun;
			
			return 0;
	    }
	}

	return -1;

}



int API_JpegCaputureStop(void)
{
	if ( jpegState_get() != JPEG_STATE_IDLE )
	{
		//ak_venc_close(ptr_jpegenc_array[0]->venc_handle);
		
		jpegState_set(JPEG_STATE_IDLE);
		
		if( JpegInfo.CallBack )
		{
			JpegInfo.CallBack();
		}
	}
}




