
#ifndef _JPEG_ENC_H_
#define _JPEG_ENC_H_

struct ak_one_encoder
{
    int 			venc_handle;
    int 			en_type;
	unsigned int 	width;
	unsigned int 	height;
};


typedef  void (*CallBackFun)(void);
struct ak_one_encoder* ak_new_one_encode( int type, int dst_w, int dst_h );

int jpegInit_capture(void);
int API_JpegCaptureStart(char* file_name,CallBackFun fun);
int API_JpegCaptureing(unsigned char *data, int read_len);
int API_JpegCaputureStop(void);

#endif


