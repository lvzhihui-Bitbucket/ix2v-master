
#include "video_record.h"
#include "one_tiny_task.h"

/* Flags in avih */
#define AVIF_HASINDEX       	0x00000010  // Index at end of file?
#define AVIF_ISINTERLEAVED  	0x00000100
#define AVIF_TRUSTCKTYPE    	0x00000800  // Use CKType to find key frames?
#define AVIIF_KEYFRAME      	0x00000010L /* this frame is a key frame.*/

void avi_write_uint16( avi_t* p_vai, unsigned short  w )
{
	if(p_vai->File == NULL)
	{
		return;
	}
	
	fputc( ( w      ) & 0xff, p_vai->File );
	fputc( ( w >> 8 ) & 0xff, p_vai->File );
}

void avi_write_uint32( avi_t* p_vai, unsigned int dw )
{
	if(p_vai->File == NULL)
	{
		return;
	}
	
	fputc( ( dw      ) & 0xff, p_vai->File );
	fputc( ( dw >> 8 ) & 0xff, p_vai->File );
	fputc( ( dw >> 16) & 0xff, p_vai->File );
	fputc( ( dw >> 24) & 0xff, p_vai->File );
}

void avi_write_fourcc( avi_t* p_vai, char* p_fcc )
{
	if(p_vai->File == NULL)
	{
		return;
	}
	
	fputc( p_fcc[0], p_vai->File );
	fputc( p_fcc[1], p_vai->File );
	fputc( p_fcc[2], p_vai->File );
	fputc( p_fcc[3], p_vai->File );
}

void avi_write_header( avi_t* p_vai )
{
	if(p_vai->File == NULL)
	{
		return;
	}
	
	avi_write_fourcc( p_vai, "RIFF" );
	avi_write_uint32( p_vai, p_vai->Riff > 0 ? p_vai->Riff - 8 : 0xFFFFFFFF );
	avi_write_fourcc( p_vai, "AVI " );

	avi_write_fourcc( p_vai, "LIST" );
	//sizof(hdrl) + avih  		//sizeof(LIST) + VIDEO list			//sizeof(LIST) + AUDIO list
	avi_write_uint32( p_vai,  	4 + 64 + 8+116 + 8+94);		
	
	avi_write_fourcc( p_vai, "hdrl" );

	avi_write_fourcc( p_vai, "avih" );
	avi_write_uint32( p_vai, 64 - 8);
	avi_write_uint32( p_vai, 1000000 / p_vai->Fps );
	avi_write_uint32( p_vai, 0xffffffff );
	avi_write_uint32( p_vai, 0 );
	avi_write_uint32( p_vai, AVIF_HASINDEX|AVIF_ISINTERLEAVED|AVIF_TRUSTCKTYPE);
	avi_write_uint32( p_vai, p_vai->TotalFrame );
	avi_write_uint32( p_vai, 0 );
	avi_write_uint32( p_vai, 1 );
	avi_write_uint32( p_vai, 1000000 );
	avi_write_uint32( p_vai, p_vai->Width );
	avi_write_uint32( p_vai, p_vai->Height );
	avi_write_uint32( p_vai, 0 );
	avi_write_uint32( p_vai, 0 );
	avi_write_uint32( p_vai, 0 );
	avi_write_uint32( p_vai, 0 );

	// VIDEO
	avi_write_fourcc( p_vai, "LIST" );
	avi_write_uint32( p_vai,  4 + 64 + 8 + 40);		//116
	avi_write_fourcc( p_vai, "strl" );

	//AVIStreamHeader
	avi_write_fourcc( p_vai, "strh" );
	avi_write_uint32( p_vai, 64 - 8);
	avi_write_fourcc( p_vai, "vids" );
	avi_write_fourcc( p_vai, p_vai->Fcc );
	avi_write_uint32( p_vai, 0 );
	avi_write_uint32( p_vai, 0 );
	avi_write_uint32( p_vai, 0 );
	avi_write_uint32( p_vai, 1000 );
	avi_write_uint32( p_vai, p_vai->Fps * 1000 );
	avi_write_uint32( p_vai, 0 );
	avi_write_uint32( p_vai, p_vai->VideoFrame );
	avi_write_uint32( p_vai, 1024*1024 );
	avi_write_uint32( p_vai, -1 );
	avi_write_uint32( p_vai, p_vai->Width * p_vai->Height );
	avi_write_uint32( p_vai, 0 );
	avi_write_uint16( p_vai, p_vai->Width );
	avi_write_uint16( p_vai, p_vai->Height );

	//strf
	avi_write_fourcc( p_vai, "strf" );
	//strf len
	avi_write_uint32( p_vai,  40);
	//BitmapInfoHeader
	avi_write_uint32( p_vai,  40);
	avi_write_uint32( p_vai,  p_vai->Width );
	avi_write_uint32( p_vai,  p_vai->Height );
	avi_write_uint16( p_vai,  1 );
	avi_write_uint16( p_vai,  24 );
	avi_write_fourcc( p_vai,  p_vai->Fcc );
	avi_write_uint32( p_vai,  p_vai->Width * p_vai->Height );
	avi_write_uint32( p_vai,  0 );
	avi_write_uint32( p_vai,  0 );
	avi_write_uint32( p_vai,  0 );
	avi_write_uint32( p_vai,  0 );

	// AUDIO
	avi_write_fourcc( p_vai,  "LIST" );
	avi_write_uint32( p_vai,  4 + 64 + 8+18);	//94
	avi_write_fourcc( p_vai,  "strl" );

	//AVIStreamHeader
	avi_write_fourcc( p_vai,  "strh" );
	avi_write_uint32( p_vai,  64 - 8);
	avi_write_fourcc( p_vai,  "auds" );
	avi_write_uint32( p_vai,  1 );			// fccHandler
	avi_write_uint32( p_vai,  0 );			// dwFlags
	avi_write_uint16( p_vai,  0 );			// wPriority
	avi_write_uint16( p_vai,  0 );			// wLanguage
	avi_write_uint32( p_vai,  1);			//dwInitialFrames
	avi_write_uint32( p_vai,  1 );			// dwScale
	avi_write_uint32( p_vai,  8000 );		// dwRate
	avi_write_uint32( p_vai,  0 );			// dwStart
	avi_write_uint32( p_vai,  p_vai->AudioFrame );		// dwLength
	avi_write_uint32( p_vai,  128);			// dwSuggestedBufferSize
	avi_write_uint32( p_vai,  0 );			// dwQuality
	avi_write_uint32( p_vai,  1 );			// dwSampleSize 8000
	avi_write_uint16( p_vai,  0 );			// left
	avi_write_uint16( p_vai,  0 );			// top
	avi_write_uint16( p_vai,  0 );			// right
	avi_write_uint16( p_vai,  0 );			// bottom

	//strf
	avi_write_fourcc( p_vai,  "strf" );
	//strf len
	avi_write_uint32( p_vai,  18);
	//WaveFormatEx
	avi_write_uint16( p_vai,  1 );			// format
	avi_write_uint16( p_vai,  1 );			// channel
	avi_write_uint32( p_vai,  8000 );		// nSamplesPerSec
	avi_write_uint32( p_vai,  16000 );		// nAvgBytesPerSec
	avi_write_uint16( p_vai,  2 );			// nBlockAlign
	avi_write_uint16( p_vai,  16 );			// wBitsPerSample
	avi_write_uint16( p_vai,  18);			// cbSize

	avi_write_fourcc( p_vai, "LIST" );
	avi_write_uint32( p_vai,  p_vai->MoviEnd > 0 ? p_vai->MoviEnd - p_vai->Movi: 0xFFFFFFFF);
	avi_write_fourcc( p_vai, "movi" );
}


int avi_init( avi_t* p_vai, const char* fileName, float fps, char* p_fcc, int w, int h )
{
	FILE* file;
	
	file = fopen( fileName, "w+");
	if(file == NULL)
	{
		printf("111111111111 p_vai->File = NULL\n");
		return 0;
	}
	
	pthread_mutex_init( &p_vai->lock, 0);
	
	pthread_mutex_lock(&p_vai->lock);
	
	p_vai->File			= file;
	p_vai->Fps 			= fps;
	p_vai->Width 		= w;
	p_vai->Height		= h;
	p_vai->Movi			= 0;
	p_vai->MoviEnd		= 0;
	p_vai->Riff 		= 0;

	p_vai->TotalFrame 	= 0;
	p_vai->AudioFrame 	= 0;
	p_vai->VideoFrame 	= 0;
	p_vai->pVideoIndexMap = NULL;
	p_vai->pAudioIndexMap = NULL;

	memcpy( p_vai->Fcc, p_fcc, 4 );
	
	avi_write_header( p_vai );

	p_vai->Movi = ftell( p_vai->File ) - 4;

	pthread_mutex_unlock(&p_vai->lock);

	return 1;
}

void avi_write_idx( avi_t* p_vai )
{
	if(p_vai->File == NULL)
	{
		printf("111111111111 p_vai->File = NULL\n");
		return;
	}
	
	avi_write_fourcc( p_vai, "idx1" );

	avi_write_uint32( p_vai,  (p_vai->VideoFrame+p_vai->AudioFrame) * sizeof(AVIIndexEntry));

	fwrite( p_vai->pVideoIndexMap, p_vai->VideoFrame*sizeof(AVIIndexEntry), 1, p_vai->File );
	fwrite( p_vai->pAudioIndexMap, p_vai->AudioFrame*sizeof(AVIIndexEntry), 1, p_vai->File );
}

void avi_write_video( avi_t* p_avi, char* p_data, int size  )
{	
	int key_frame = 0;
	
	pthread_mutex_lock(&p_avi->lock);
	
	if(p_avi->File == NULL)
	{
		pthread_mutex_unlock(&p_avi->lock);
		return;
	}

	long long  i_pos = ftell( p_avi->File );

	if((p_data[4] & 0x1F) == 7)
		key_frame = 1;
	
	/* chunk header */
	avi_write_fourcc( p_avi, "00dc" );
	if( size & 0x01 )
	{
		avi_write_uint32( p_avi, size+1 );
	}
	else
	{
		avi_write_uint32( p_avi, size );
	}
	
	fwrite( p_data, size, 1, p_avi->File );

	if( size & 0x01 )
	{
		/* pad */
		fputc( 0, p_avi->File );
		size++;
	}
	
	p_avi->VideoFrame++;
	
	p_avi->pVideoIndexMap = realloc(p_avi->pVideoIndexMap, sizeof(AVIIndexEntry)*p_avi->VideoFrame);

	p_avi->pVideoIndexMap[p_avi->VideoFrame-1].dwChunkId = FOURCC_00dc;
	p_avi->pVideoIndexMap[p_avi->VideoFrame-1].dwFlags = key_frame ? AVIIF_KEYFRAME : 0 ;
 	p_avi->pVideoIndexMap[p_avi->VideoFrame-1].dwOffset = i_pos - p_avi->Movi;
	p_avi->pVideoIndexMap[p_avi->VideoFrame-1].dwSize = size;

	pthread_mutex_unlock(&p_avi->lock);
}

void avi_write_audio( avi_t* p_avi, char* p_data, int size  )
{
	pthread_mutex_lock(&p_avi->lock);

	if(p_avi->File == NULL)
	{
		pthread_mutex_unlock(&p_avi->lock);
		return;
	}
	
	long long   i_pos = ftell( p_avi->File );
	/* chunk header */
	avi_write_fourcc( p_avi, "01wb" );
	avi_write_uint32( p_avi, size );

	fwrite( p_data, size, 1, p_avi->File );
	
	p_avi->AudioFrame++;
	
	p_avi->pAudioIndexMap = realloc(p_avi->pAudioIndexMap, sizeof(AVIIndexEntry)*p_avi->AudioFrame);

	p_avi->pAudioIndexMap[p_avi->AudioFrame-1].dwChunkId = FOURCC_01wb;
	p_avi->pAudioIndexMap[p_avi->AudioFrame-1].dwFlags = AVIIF_KEYFRAME;
 	p_avi->pAudioIndexMap[p_avi->AudioFrame-1].dwOffset = i_pos - p_avi->Movi;
	p_avi->pAudioIndexMap[p_avi->AudioFrame-1].dwSize = size;

	pthread_mutex_unlock(&p_avi->lock);
}

void avi_end( avi_t* p_avi, int index )
{
	pthread_mutex_lock(&p_avi->lock);

	if(p_avi->File == NULL)
	{
		pthread_mutex_unlock(&p_avi->lock);
		return;
	}

	p_avi->TotalFrame = p_avi->VideoFrame;
	p_avi->MoviEnd = ftell( p_avi->File );

	/* write index */
	avi_write_idx( p_avi );

	p_avi->Riff = ftell( p_avi->File );

	/* Fix header */
	fseek( p_avi->File, 0, SEEK_SET );
	
	avi_write_header( p_avi );

	if(p_avi->pVideoIndexMap != NULL)
	{
		free(p_avi->pVideoIndexMap);
		p_avi->pVideoIndexMap = NULL;
	}
	if(p_avi->pAudioIndexMap != NULL)
	{
		free(p_avi->pAudioIndexMap);
		p_avi->pAudioIndexMap = NULL;
	}
	
	if(p_avi->File != NULL)
	{
		fclose(p_avi->File);
		p_avi->File = NULL;
	}

	sync();
	
	printf( "avi file written\n" );
	printf( "  - codec: %4.4s\n", p_avi->Fcc );
	printf( "  - size: %dx%d\n", p_avi->Width, p_avi->Height );
	//printf( "  - fps: %.3f\n", p_avi->Fps );
	//printf( "  - TotalFrame: %d\n", p_avi->TotalFrame );
	//printf( "  - AudioFrame: %d\n", p_avi->AudioFrame );
	//printf( "  - VideoFrame: %d\n", p_avi->VideoFrame );
	//printf( "  - movi: %lld\n", p_avi->Movi );
	//printf( "  - movi end: %lld\n", p_avi->MoviEnd );

	if( p_avi->CallBack )
		(p_avi->CallBack)(index);
	
	pthread_mutex_unlock(&p_avi->lock);
	
	//pthread_mutex_destroy(&p_avi->lock);
}

int GetAviFileSize(avi_t* p_avi)
{
	pthread_mutex_lock(&p_avi->lock);
	if(p_avi->File == NULL)
	{
		pthread_mutex_unlock(&p_avi->lock);
		return 0;
	}
	//fseek(p_avi->File, 0, SEEK_END);
	long long file_size = ftell(p_avi->File);
	pthread_mutex_unlock(&p_avi->lock);
	return file_size / (long long) (1024*1024);
}

//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
avi_t avi={0};

pthread_mutex_t aviRecordLock = PTHREAD_MUTEX_INITIALIZER;
int API_RecordStart( char* file_name, int sec, CallBackFun fun )
{
	pthread_mutex_lock(&aviRecordLock);
	
	if( avi.State == RECORD_IDLE )
	{
		if( getSdcardSpace() < 20 )
		{
			pthread_mutex_unlock(&aviRecordLock);
			return -2;
		}

		// check&create video store directory
		if( access( VIDEO_STORE_DIR, F_OK ) < 0 )
		{
			if( mkdir( VIDEO_STORE_DIR, 777 ) < 0 )
			{
				pthread_mutex_unlock(&aviRecordLock);
				return -3;
			}
		}

		char fullFileName[100];
		
		snprintf(fullFileName,100,"%s/%s",VIDEO_STORE_DIR,file_name);
		strcpy(avi.fileName, file_name);
		
		avi.TotalSec	= ((sec > 60)? 60 : sec) * FRAMES_PER_SEC;	// zfz_20190617 //czn_20170401	will_change	avi.TotalSec 	= sec * 25;
		avi.CurrSec		= 0;
		avi.CallBack 	= fun;
		avi.IDR 		= 0;
		avi.AudioFlag 	= 0;
		
		if(avi_init( &avi, fullFileName, FRAMES_PER_SEC, "h264", 1280, 720 ))
		{
			avi.State = RECORD_START;
		}
		else
		{
			pthread_mutex_unlock(&aviRecordLock);
			return -1;
		}
	}
	
	pthread_mutex_unlock(&aviRecordLock);
	return 0;
}

void API_Recording( char* p_data, int size )
{
	pthread_mutex_lock(&aviRecordLock);

	if( avi.State != RECORD_IDLE)
	{
		//printf("API_Recording State=%d,size=%d,p_data[4]=%d\n", avi.State, size, p_data[4]);
		if( avi.IDR == 0 && p_data[0] == 0 && p_data[1] == 0 && p_data[2] == 0 && p_data[3] == 1 )
		{
			if( ( (p_data[4]) & (0x1f) ) == 7)
			{
				avi.IDR = 1;
			}
		}

		if( avi.IDR && size > 0 )
		{
			char tempData[128];
			uint8 packetCnt;
			memset(tempData, 0, 128);
					
			avi_write_video( &avi, p_data, size);

			for(packetCnt = (avi.CurrSec % FRAMES_PER_SEC) ? 6 : 11; packetCnt > 0; packetCnt--) // zfz_20190617
			{
				API_WriteEmptyAudio(tempData, 128);
			}

			if(avi.State == RECORD_START)
			{
				avi.State = RECORD_RUN;
			}
			
			if( ++avi.CurrSec >= avi.TotalSec)
			{
				avi_end( &avi, 0 );
				avi.IDR = 0;
				avi.State = RECORD_IDLE;
			}
		}
	}

	pthread_mutex_unlock(&aviRecordLock);
}

int API_RecordEnd(void)
{
	int retValue = 0;
	//printf( "%s(%d)-<%s> \n", __FILE__, __LINE__, __FUNCTION__ ) ;
	pthread_mutex_lock(&aviRecordLock);
	
	if( avi.State == RECORD_RUN )
	{
		avi_end( &avi, 0 );
		avi.IDR = 0;
		
		retValue = 0;
	}
	else if( avi.State == RECORD_START)
	{
		//printf( "%s(%d)-<%s> \n", __FILE__, __LINE__, __FUNCTION__ ) ;
		if(avi.pVideoIndexMap != NULL)
		{
			free(avi.pVideoIndexMap);
			avi.pVideoIndexMap = NULL;
		}
		if(avi.pAudioIndexMap != NULL)
		{
			free(avi.pAudioIndexMap);
			avi.pAudioIndexMap = NULL;
		}
		
		if(avi.File != NULL)
		{
			fclose(avi.File);
			avi.File = NULL;
		}

		//文件有错，删除
		char tempChar[100];
		snprintf(tempChar,100, "rm %s/%s\n", VIDEO_STORE_DIR, avi.fileName);
		system(tempChar);
		
		sync();
		
		if( avi.CallBack )
		(avi.CallBack)(0);
		
		retValue = -1;
	}
	avi.State = RECORD_IDLE;
	
	pthread_mutex_unlock(&aviRecordLock);
	//printf( "%s(%d)-<%s> \n", __FILE__, __LINE__, __FUNCTION__ ) ;
	return retValue;
}

int API_WriteAudio( char* p_data, int size )
{
	if( avi.IDR )
	{
		avi.AudioFlag = 1;
		avi_write_audio( &avi, p_data, size );
	}
	return 0;
}

int API_WriteEmptyAudio( char* p_data, int size )
{
	if(!avi.AudioFlag)
	{
		avi_write_audio( &avi, p_data, size );
	}
	return 0;
}

#define 	MAX_REC_INS		4
REC_INSTANCE_t rec_ins_array[MAX_REC_INS]={0};

int Record_initial(  REC_INSTANCE_t* pins )
{
	//pthread_mutex_init( &pins->rec_mux_state_lock, 0);	
	pins->rec_mux_ins.State		= RECORD_IDLE;
	pins->rec_mux_ins.IDR		= 0;	
	return 0;
}

int api_rec_mux_initial(void)
{
	int i;
	for( i = 0; i < MAX_REC_INS; i++ )
	{
		Record_initial( &rec_ins_array[i]);
	}
}

/**
 * @fn:		API_RecordStart_mux
 *
 * @brief:	启动录像
 *
 * @param:	index		    - record 实例
 * @param:	vdtype	    	- 0-h264，1-h265
 * @param:	fps	    		- 录像帧率
 * @param:	file_name	    - 录像文件名
 * @param:	sec	    		- 录像时长
 * @param:	fileSize	    - 录像文件大小限制，缺省为0
 * @param:	fun	    		- 录像回调函数 - 发送通知消息
 *
 * @return: -1/err, 0/ok
 */
int API_RecordStart_mux( int index, int vdtype, int fps, char* file_name, int sec, int fileSize, CallBackFun fun )
{
	char fullFileName[100];
	char videotype[20];
	if(index >= MAX_REC_INS)
		return -2;
	if(rec_ins_array[index].lock_init_flag==0)
	{
		pthread_mutex_init( &rec_ins_array[index].rec_mux_state_lock, 0);
		rec_ins_array[index].lock_init_flag=1;
	}
	pthread_mutex_lock(&rec_ins_array[index].rec_mux_state_lock);
	if(Judge_SdCardLink() == 1) 
	{
		if( access( VIDEO_STORE_DIR, F_OK ) < 0 )
		{
			if( mkdir( VIDEO_STORE_DIR, 777 ) < 0 )
			{
				return -2;
			}
		}
		snprintf(fullFileName,100,"%s/%s",VIDEO_STORE_DIR,file_name);
		
	}
	else
	{
		if( access( JPEG_STORE_DIR, F_OK ) < 0 )
		{
			if( mkdir( JPEG_STORE_DIR, 777 ) < 0 )
			{
				return -1;
			}
		}
		snprintf(fullFileName,100,"%s/%s",JPEG_STORE_DIR,file_name);

	}	
	//printf("API_RecordStart_mux index=%d state=%d\n", index, rec_ins_array[index].rec_mux_ins.State);
	if( rec_ins_array[index].rec_mux_ins.State == RECORD_IDLE )
	{
		strcpy(rec_ins_array[index].rec_mux_ins.fileName, file_name);
		
		rec_ins_array[index].rec_mux_ins.TotalSec	= sec * fps;	
		rec_ins_array[index].rec_mux_ins.CurrSec	= 0;
		rec_ins_array[index].rec_mux_ins.CallBack 	= fun;
		rec_ins_array[index].rec_mux_ins.IDR 		= 0;
		rec_ins_array[index].rec_mux_ins.AudioFlag 	= 0;
		strcpy(videotype, vdtype==1? "hev1" : "h264");//h265 hevc
		rec_ins_array[index].rec_mux_ins.TotalSize	= fileSize;
		if(avi_init( &rec_ins_array[index].rec_mux_ins, fullFileName, fps, videotype, 1920, 1088 ))
		{
			rec_ins_array[index].rec_mux_ins.State = RECORD_START;
			printf("API_RecordStart_mux index=%d videotype=%s FileName=%s\n",index, videotype, fullFileName);
		}
		else
		{
			pthread_mutex_unlock(&rec_ins_array[index].rec_mux_state_lock);
			return -1;
		}
	}
	
	pthread_mutex_unlock(&rec_ins_array[index].rec_mux_state_lock);
	
	return 0;
}

/**
 * @fn:		API_Recording_mux
 *
 * @brief:	在码流循环中调用
 *
 * @param:	index		    - record 实例
  * @param:	vdtype	    	- 0-h264，1-h265* 
 * @param:	p_data	    	- 录像数据指针
 * @param:	size	    	- 录像数据大小
 *
 * @return: none
 */
void API_Recording_mux( char* p_data, int size, int index, int vdtype )
{
	//printf("API_Recording_mux index=%d size=%d p_data[0]=%02x p_data[1]=%02x p_data[2]=%02x p_data[3]=%02x p_data[4]=%02x\n", index,size,p_data[0],p_data[1],p_data[2],p_data[3],p_data[4]);
	pthread_mutex_lock(&rec_ins_array[index].rec_mux_state_lock);

	if( rec_ins_array[index].rec_mux_ins.State != RECORD_IDLE)
	{
		if( rec_ins_array[index].rec_mux_ins.IDR == 0 && p_data[0] == 0 && p_data[1] == 0 && p_data[2] == 0 && p_data[3] == 1 )
		{
			//printf("API_Recording_mux index=%d size=%d p_data[0]=%02x p_data[1]=%02x p_data[2]=%02x p_data[3]=%02x p_data[4]=%02x\n", index,size,p_data[0],p_data[1],p_data[2],p_data[3],p_data[4]);
			if(vdtype == 0)//h264
			{
				if( ( (p_data[4]) & (0x1f) ) == 7)
				{
					rec_ins_array[index].rec_mux_ins.IDR = 1;
					printf("API_Recording_mux index=%d H264 IDR OK\n", index);
				}
			}
			else
			{
				if( ( (p_data[4]) & (0x7e) ) == 0x40)
				{
					rec_ins_array[index].rec_mux_ins.IDR = 1;
					printf("API_Recording_mux index=%d H265 IDR OK\n", index);
				}
			}
		}

		if( rec_ins_array[index].rec_mux_ins.IDR && size > 0 )
		{
			//printf("API_Recording_mux index=%d IDR=%d\n", index, rec_ins_array[index].rec_mux_ins.IDR);
			char tempData[128];
			uint8 packetCnt;
			memset(tempData, 0, 128);
					
			avi_write_video( &rec_ins_array[index].rec_mux_ins, p_data, size);

			for(packetCnt = (rec_ins_array[index].rec_mux_ins.CurrSec % FRAMES_PER_SEC) ? 6 : 11; packetCnt > 0; packetCnt--) // zfz_20190617
			{
				API_WriteEmptyAudio(tempData, 128);
			}

			if(rec_ins_array[index].rec_mux_ins.State == RECORD_START)
			{
				rec_ins_array[index].rec_mux_ins.State = RECORD_RUN;
			}

			if(rec_ins_array[index].rec_mux_ins.TotalSize != 0)
			{
				if(GetAviFileSize(&rec_ins_array[index].rec_mux_ins) >= rec_ins_array[index].rec_mux_ins.TotalSize)
				{
					avi_end( &rec_ins_array[index].rec_mux_ins, index );
					rec_ins_array[index].rec_mux_ins.IDR = 0;
					rec_ins_array[index].rec_mux_ins.State = RECORD_IDLE;
				}
				//printf("GetAviFileSize %d\n", fileSize);
				rec_ins_array[index].rec_mux_ins.CurrSec++;
			}
			else
			{
				if( ++rec_ins_array[index].rec_mux_ins.CurrSec >= rec_ins_array[index].rec_mux_ins.TotalSec)
				{
					printf("API_Recording_mux index=%d CurrSec=%d\n", index, rec_ins_array[index].rec_mux_ins.CurrSec);
					avi_end( &rec_ins_array[index].rec_mux_ins, index );
					rec_ins_array[index].rec_mux_ins.IDR = 0;
					rec_ins_array[index].rec_mux_ins.State = RECORD_IDLE;
				}
			}
		}
	}

	pthread_mutex_unlock(&rec_ins_array[index].rec_mux_state_lock);
}

/**
 * @fn:		API_RecordEnd_mux
 *
 * @brief:	停止录像
 *
 * @param:	index - record 实例
 *
 * @return: none
 */
int API_RecordEnd_mux(int index)
{
	int retValue = 0;

	printf("API_RecordEnd_mux index=%d state=%d\n", index, rec_ins_array[index].rec_mux_ins.State);
	pthread_mutex_lock(&rec_ins_array[index].rec_mux_state_lock);
	
	if( rec_ins_array[index].rec_mux_ins.State == RECORD_RUN )
	{
		avi_end( &rec_ins_array[index].rec_mux_ins, index );
		rec_ins_array[index].rec_mux_ins.IDR = 0;
		
		retValue = 0;
	}
	else if( rec_ins_array[index].rec_mux_ins.State == RECORD_START)
	{
		if(rec_ins_array[index].rec_mux_ins.pVideoIndexMap != NULL)
		{
			free(rec_ins_array[index].rec_mux_ins.pVideoIndexMap);
			rec_ins_array[index].rec_mux_ins.pVideoIndexMap = NULL;
		}
		if(rec_ins_array[index].rec_mux_ins.pAudioIndexMap != NULL)
		{
			free(rec_ins_array[index].rec_mux_ins.pAudioIndexMap);
			rec_ins_array[index].rec_mux_ins.pAudioIndexMap = NULL;
		}
		
		if(rec_ins_array[index].rec_mux_ins.File != NULL)
		{
			fclose(rec_ins_array[index].rec_mux_ins.File);
			rec_ins_array[index].rec_mux_ins.File = NULL;
		}

		//文件有错，删除
		char tempChar[100];
		if(Judge_SdCardLink() == 1)
			snprintf(tempChar,100, "rm %s/%s\n", VIDEO_STORE_DIR, rec_ins_array[index].rec_mux_ins.fileName);
		else
			snprintf(tempChar,100, "rm %s/%s\n", JPEG_STORE_DIR, rec_ins_array[index].rec_mux_ins.fileName);
		system(tempChar);
		
		sync();
		
		//if( rec_ins_array[index].rec_mux_ins.CallBack )
		//(rec_ins_array[index].rec_mux_ins.CallBack)(index);
		
		retValue = -1;
	}
	rec_ins_array[index].rec_mux_ins.State = RECORD_IDLE;
	
	pthread_mutex_unlock(&rec_ins_array[index].rec_mux_state_lock);
	return retValue;
}





