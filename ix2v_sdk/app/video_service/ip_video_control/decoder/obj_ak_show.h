
#ifndef _OBJ_AK_SHOW_H_
#define _OBJ_AK_SHOW_H_

#include "obj_ak_decoder.h"

typedef enum
{
	H264_SHOW_IDLE,
	H264_SHOW_START,
	H264_SHOW_RUN,
	H264_SHOW_STOP,
}
H264_SHOW_STATE_t;

typedef struct
{
	// thread
	pthread_mutex_t 	h264_show_state_lock;
	// state
	H264_SHOW_STATE_t	state;

	// attribute
	int					width;
	int					height;
	int					fps;
	int					baudrate;
	// show size
	int					disp_posx;
	int					disp_posy;
	int 				disp_width;
	int					disp_height;
	int					disp_update_flag;
	int 				IDR;
	int					vdec_layer;
	struct ak_one_decoder *ptr_vdec;		
} H264_SHOW_INSTANCE_t;


int h264_show_initial( H264_SHOW_INSTANCE_t *pins, int vdec_layer );
void set_h264_show_state(H264_SHOW_INSTANCE_t *pins, H264_SHOW_STATE_t state);
H264_SHOW_STATE_t get_h264_show_state(H264_SHOW_INSTANCE_t *pins);
int h264_show_start( H264_SHOW_INSTANCE_t *pins, int width, int height, int fps, int baudrate , int dispx, int dispy, int dispw, int disph, int videotype );
int h264_show_update( H264_SHOW_INSTANCE_t *pins, int width, int height, int fps, int baudrate , int dispx, int dispy, int dispw, int disph, int videotype );
int h264_show_stop( H264_SHOW_INSTANCE_t *pins );
int h264_show_dump_buf( H264_SHOW_INSTANCE_t *pins, char* pdatbuf, int datlen,int tick);


#endif



