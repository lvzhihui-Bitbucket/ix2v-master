
#ifndef _VIDEO_RECORD_H_
#define _VIDEO_RECORD_H_

#include "video_utility.h"

typedef enum
{
	 RECORD_IDLE,
	 RECORD_START,
	 RECORD_RUN,
}
RECORD_STATE_t;

typedef  void (*CallBackFun)(int ins);

typedef struct
{
	FILE*				File;
	pthread_mutex_t		lock;
	float			 	Fps;
	char				Fcc[4];
	unsigned short   	Width;
	unsigned short   	Height;
	long long 			Movi;
	long long 			MoviEnd;
	long long  			Riff;
	int 				TotalFrame;
	int 				AudioFrame;
	int 				VideoFrame;
	
	AVIIndexEntry	*pVideoIndexMap;
	AVIIndexEntry	*pAudioIndexMap;
	
	int				IDR;
	char			fileName[50];
	int				AudioFlag;
	int				TotalSec;
	int				CurrSec;	
	RECORD_STATE_t  State;
	CallBackFun 	CallBack;	
	int 			TotalSize;
} avi_t;

typedef struct
{
	// thread
	pthread_mutex_t 	rec_mux_state_lock;
	avi_t				rec_mux_ins;
	uint8			lock_init_flag;

}REC_INSTANCE_t;


int API_WriteAudio( char* p_data, int size );
void API_Recording( char* p_data, int size );

int API_RecordStart( char* file_name, int sec, CallBackFun fun );
int API_RecordEnd(void);

int api_rec_mux_initial(void);

/**
 * @fn:		API_RecordStart_mux
 *
 * @brief:	启动录像
 *
 * @param:	index		    - record 实例
 * @param:	vdtype	    	- 0-h264，1-h265
 * @param:	fps	    		- 录像帧率
 * @param:	file_name	    - 录像文件名
 * @param:	sec	    		- 录像时长
 * @param:	fileSize	    - 录像文件大小限制，缺省为0
 * @param:	fun	    		- 录像回调函数 - 发送通知消息
 *
 * @return: -1/err, 0/ok
 */
int API_RecordStart_mux( int index, int vdtype, int fps, char* file_name, int sec, int fileSize, CallBackFun fun );

/**
 * @fn:		API_Recording_mux
 *
 * @brief:	在码流循环中调用
 *
 * @param:	index		    - record 实例
  * @param:	vdtype	    	- 0-h264，1-h265* 
 * @param:	p_data	    	- 录像数据指针
 * @param:	size	    	- 录像数据大小
 *
 * @return: none
 */
void API_Recording_mux( char* p_data, int size, int num, int vdtype );

/**
 * @fn:		API_RecordEnd_mux
 *
 * @brief:	停止录像
 *
 * @param:	index - record 实例
 *
 * @return: none
 */
int API_RecordEnd_mux(int num);

#endif



