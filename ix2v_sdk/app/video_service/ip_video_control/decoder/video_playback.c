
#include "obj_ak_decoder.h"
#include "video_playback.h"
#include "obj_ak_show.h"
#include "one_tiny_task.h"
#include "gui_lib.h"
//#include "task_VideoMenu.h"

//-------------------------------------------------------------------------------------
// 显示接口
//-------------------------------------------------------------------------------------
#define 	MAX_PLAYBACK_INS		2

H264_SHOW_INSTANCE_t	playback_ins_array[MAX_PLAYBACK_INS];

int api_playback_initial(void)
{
	int i,layer;
	layer = AK_VO_LAYER_VIDEO_1;
	for( i = 0; i < MAX_PLAYBACK_INS; i++ )
	{
		h264_show_initial( &playback_ins_array[i],layer++);
	}
}

int api_playback_show( int index, int width, int height, int fps, int baudrate , int dispx, int dispy, int dispw, int disph, int videotype )
{
	return h264_show_start( &playback_ins_array[index], width, height, fps, baudrate , dispx, dispy, dispw, disph, videotype );	
}

int api_playback_update( int index, int width, int height, int fps, int baudrate , int dispx, int dispy, int dispw, int disph, int videotype )
{
	return h264_show_update( &playback_ins_array[index], width, height, fps, baudrate , dispx, dispy, dispw, disph, videotype  );
}

int api_playback_just_running(int index)
{
	H264_SHOW_STATE_t cur;
	pthread_mutex_lock( &playback_ins_array[index].h264_show_state_lock );
	cur = get_h264_show_state(&playback_ins_array[index]);
	pthread_mutex_unlock( &playback_ins_array[index].h264_show_state_lock );
	if( cur == H264_SHOW_RUN )
		return 1;
	else
		return 0;
}

int api_playback_stop( int index )
{
	return h264_show_stop( &playback_ins_array[index] );
}

int api_playback_dump_buf( int index, char* pdatbuf, int datlen )
{
	return h264_show_dump_buf( &playback_ins_array[index], pdatbuf, datlen,0);
}

//-------------------------------------------------------------------------------------
// 播放任务
//-------------------------------------------------------------------------------------
void set_playback_state(AV_PLAYBACK_DAT_T* pusr_dat, RECORD_PLAYBACK_STATE_t state)
{
	pthread_mutex_lock(&pusr_dat->aviPlaybackStateLock);
	pusr_dat->State = state;
	pthread_mutex_unlock(&pusr_dat->aviPlaybackStateLock);
}

RECORD_PLAYBACK_STATE_t get_playback_state(AV_PLAYBACK_DAT_T* pusr_dat)
{
	RECORD_PLAYBACK_STATE_t state;
	pthread_mutex_lock(&pusr_dat->aviPlaybackStateLock);
	state = pusr_dat->State;
	pthread_mutex_unlock(&pusr_dat->aviPlaybackStateLock);
	return state;
}


int task_av_playback_init( void* arg )
{
	one_tiny_task_t 	*ptiny_task = (one_tiny_task_t*)arg;
	AV_PLAYBACK_DAT_T 	*pusr_dat 	= (AV_PLAYBACK_DAT_T*)ptiny_task->pusr_dat;	
	AVIFileParset 		*pavi_par 	= &pusr_dat->aviParset;

	int index 		= pusr_dat->index;
	int width		= pusr_dat->width;
	int height		= pusr_dat->height;
	int fps			= pusr_dat->fps;
	int baudrate	= pusr_dat->baudrate;
	int dispx		= pusr_dat->dispx;
	int dispy		= pusr_dat->dispy;
	int dispw		= pusr_dat->dispw;
	int disph		= pusr_dat->disph;
	int videotype	= pusr_dat->videotype;
	
	if( get_pane_type() == 1 )
	{
		Set_ds_show_pos(0,0,0,bkgd_w,bkgd_h/2);
	}
	else
	{
		Set_ds_show_pos(0,0,0,bkgd_w,bkgd_h);
	}
	api_playback_show(index, width, height, fps, baudrate, dispx, dispy, dispw, disph,videotype);

	//int rate		= pavi_par->waveInfo.nSamplesPerSec;
	//int channels	= pavi_par->waveInfo.nChannels;
	//alsa_write_init( NULL, rate, channels, 5 );

	printf( "-------task_av_playback_init----------\n" );
	
	return 0;
}

int task_av_playback_process( void* arg )
{
	one_tiny_task_t 	*ptiny_task = (one_tiny_task_t*)arg;
	AV_PLAYBACK_DAT_T 	*pusr_dat 	= (AV_PLAYBACK_DAT_T*)ptiny_task->pusr_dat;	
	AVIFileParset 		*pavi_par 	= &pusr_dat->aviParset;
	
	// audio initial		
	char *au_readBuffer	= malloc(pavi_par->aviAudioStreamHeader.dwSuggestedBufferSize);
	int auTotalFrames	= pavi_par->aviAudioStreamHeader.dwLength;	
	int au_sf_len = 0;
	int au_samples;		

	// video initial
	char* vd_readBuffer = malloc(pavi_par->maxFrameSize);
	int vdTotalFrames 	= pavi_par->aviVideoStreamHeader.dwLength;
	//int vd_sf_len = 0;
	int time_use;
	int IDR = 0;
	int MicroSecPerFrame;
	struct timeval start;
	struct timeval end;
	int size;
	
	set_playback_state(pusr_dat,RECORD_PLAYBACK_RUN);

	MicroSecPerFrame  = pavi_par->aviMainHeader.dwMicroSecPerFrame - 10000;

	printf( "------------task_av_playback_process[%s]----------------\n",pusr_dat->filename );

replay:
	pusr_dat->vd_play_time = 0;
	pusr_dat->vd_play_frame = 0;
    while( one_tiny_task_is_running(ptiny_task) )
	{
		if( get_playback_state(pusr_dat) == RECORD_PLAYBACK_RUN )
		{
			// audio playing
			if(au_sf_len < auTotalFrames)
			{
				#if 0
				au_samples = getAudioFrame( pavi_par, au_readBuffer, au_sf_len++);	
				if( au_samples )
				{
					alsa_write( NULL, (unsigned char*)au_readBuffer, au_samples);
				}
				#endif
			}
			// video playing
			gettimeofday( &start, NULL );
			if( (pusr_dat->vd_play_frame == FRAMES_PER_SEC-1) && (pusr_dat->CallBack != NULL) )
			{
				pusr_dat->CallBack(pusr_dat->vd_play_time+1);
			}
			if(pusr_dat->vd_play_frame + 15*pusr_dat->vd_play_time >= vdTotalFrames)
			{
				set_playback_state(pusr_dat,RECORD_PLAYBACK_END);				
				continue;
			}
			size = getVideoFrame(pavi_par, vd_readBuffer, pusr_dat->vd_play_frame + 15*pusr_dat->vd_play_time);			
			if(size)
			{
				if( IDR == 0 && vd_readBuffer[0] == 0 && vd_readBuffer[1] == 0 && vd_readBuffer[2] == 0 && vd_readBuffer[3] == 1 )
				{
					if(pusr_dat->videotype == 0)//h264
					{
						if( (vd_readBuffer[4] & 0x1F) == 7 )
						{
							printf( "-------getH264frame IDR----------\n" );
							IDR = 1;
						}
					}
					else
					{
						if( ( (vd_readBuffer[4]) & (0x7e) ) == 0x40)
						{
							printf( "-------getH265frame IDR----------\n" );
							IDR = 1;
						}
					}
				}					
				if( IDR )
				{
					//printf( "-------frame[%d] sec[%d]----------\n",pusr_dat->vd_play_frame, pusr_dat->vd_play_time );
					api_playback_dump_buf(pusr_dat->index,vd_readBuffer,size);
				}
			}
			if(pusr_dat->vd_play_frame < FRAMES_PER_SEC-1)
			{
				pusr_dat->vd_play_frame++;
			}
			else
			{
				pusr_dat->vd_play_frame = 0;
				pusr_dat->vd_play_time++;
			}
			gettimeofday( &end, NULL );
			
			time_use = (end.tv_sec - start.tv_sec) * 1000000 + (end.tv_usec - start.tv_usec);
		
			if( (MicroSecPerFrame - time_use ) > 0 )
			{
				usleep(  MicroSecPerFrame - time_use);
			}								
		}
		else if( get_playback_state(pusr_dat) == RECORD_PLAYBACK_PAUSE || get_playback_state(pusr_dat) == RECORD_PLAYBACK_START || get_playback_state(pusr_dat) == RECORD_PLAYBACK_END)
		{
			usleep( 160 );
		}
		else if(get_playback_state(pusr_dat) == RECORD_PLAYBACK_REPLAY)
		{
			usleep(100);
			set_playback_state(pusr_dat,RECORD_PLAYBACK_RUN);				
			goto replay;
		}
		else if(get_playback_state(pusr_dat) == RECORD_PLAYBACK_STOP)
		{
			break;
		}
		usleep(100);
    }
	if( au_readBuffer )
	{
		free( au_readBuffer );
	}			
	if( vd_readBuffer )
	{
		free( vd_readBuffer );
	}
    return 0;
}
int task_av_playback_exit( void* arg )
{
	one_tiny_task_t 	*ptiny_task = (one_tiny_task_t*)arg;
	AV_PLAYBACK_DAT_T 	*pusr_dat 	= (AV_PLAYBACK_DAT_T*)ptiny_task->pusr_dat;	
	AVIFileParset 		*pavi_par 	= &pusr_dat->aviParset;

	// audio exit
	//alsa_write_uninit( NULL );

	// close file	
	CloseAviFile(pavi_par);

	// reset state
	set_playback_state(pusr_dat,RECORD_PLAYBACK_IDLE);

	printf( "-------task_av_playback_exit----------\n" );
	
	return 0;
}

//-------------------------------------------------------------------------------------
// 播放接口
//-------------------------------------------------------------------------------------
int playback_start( AV_PLAYBACK_DAT_T* pusr_dat, char* file_name, int* sec, PlaybackCallBackFun fun )
{
	AVIFileParset *pavi_par 	= &pusr_dat->aviParset;

	char full_file_name[100];
	int ret = 0;

	printf( "------------playback_start[%s]----------------\n",file_name );

	if( get_playback_state(pusr_dat) == RECORD_PLAYBACK_IDLE )
	{
		set_playback_state(pusr_dat,RECORD_PLAYBACK_START);
			
		printf( "------------playback[%s]----------------\n",file_name );
		
		if(Judge_SdCardLink() == 1) 
			snprintf(full_file_name,100,"%s/%s",VIDEO_STORE_DIR,file_name);
		else
			snprintf(full_file_name,100,"%s/%s",JPEG_STORE_DIR,file_name);
			
		if( parseAviFile(pavi_par, full_file_name) != SUCCESS_VALID_AVI )
		{
			CloseAviFile(pavi_par);
			set_playback_state(pusr_dat,RECORD_PLAYBACK_IDLE);
			return -1;
		}

		// intial global data
		strcpy( pusr_dat->filename, file_name );
		pusr_dat->CallBack 	= fun;
		
		*sec = (getTotalVideoFrames(pavi_par) / FRAMES_PER_SEC );
		getVideoSize(pavi_par, &pusr_dat->width, &pusr_dat->height, &pusr_dat->videotype, &pusr_dat->fps);

		printf( "-playback_start width[%d] height[%d] videotype[%d] videolen[%d]s-\n", pusr_dat->width, pusr_dat->height, pusr_dat->videotype, *sec);
		
		ret = one_tiny_task_start( &pusr_dat->tiny_task, task_av_playback_init, task_av_playback_process, task_av_playback_exit, (void*)pusr_dat );
		
		if( ret == -1 )
		{
			CloseAviFile(pavi_par);
			set_playback_state(pusr_dat,RECORD_PLAYBACK_IDLE);
			return -1;
		}
		else
		{
			return 0;
		}
	}
	else
	{
		printf( "------------playback_start state fail----------------\n");	
		return -1;
	}
	
}

int playback_pause( AV_PLAYBACK_DAT_T* pusr_dat )
{
	if( get_playback_state(pusr_dat) == RECORD_PLAYBACK_RUN )
		set_playback_state(pusr_dat, RECORD_PLAYBACK_PAUSE);

	return 0;
}

int playback_replay( AV_PLAYBACK_DAT_T* pusr_dat )
{
	if( get_playback_state(pusr_dat) == RECORD_PLAYBACK_END )
		set_playback_state(pusr_dat, RECORD_PLAYBACK_REPLAY);

	return 0;
}

int playback_continue( AV_PLAYBACK_DAT_T* pusr_dat )
{
	if( get_playback_state(pusr_dat) == RECORD_PLAYBACK_PAUSE )
		set_playback_state(pusr_dat, RECORD_PLAYBACK_RUN);

	return 0;	
}

int playback_close( AV_PLAYBACK_DAT_T* pusr_dat )
{
	#if 0
	// close inner cycle
	while( get_playback_state(pusr_dat) != RECORD_PLAYBACK_IDLE)
	{		
		// 若为启动阶段，则等待启动完成
		if(get_playback_state(pusr_dat) != RECORD_PLAYBACK_START)
		{
			set_playback_state(pusr_dat, RECORD_PLAYBACK_STOP);
			break;
		}
		usleep(10000);
	}
	#endif
	// close outer cycle
	one_tiny_task_stop( &pusr_dat->tiny_task );
	//usleep(500*1000);
	// video exit
	api_playback_stop(pusr_dat->index);
	return 0;	
}


//-------------------------------------------------------------------------------------
// 播放实例接口
//-------------------------------------------------------------------------------------
AV_PLAYBACK_DAT_T	av_playback_dat =
{
	.aviPlaybackStateLock 	= PTHREAD_MUTEX_INITIALIZER,
	.index 					= 0,
	.width 					= 1280,
	.height					= 720,
	.fps					= 15,
	.baudrate				= 512,
	.dispx					= 0,
	.dispy					= 0,
	.dispw					= 1280,
	.disph					= 800,
	.videotype				= 0,
};

int API_PlaybackStart( char* file_name, int* sec, PlaybackCallBackFun fun )
{
	static int initialonce = 1;
	if( initialonce )
	{
		printf("++++++api_playback_initial only once+++++\n");
		initialonce = 0;
		api_playback_initial();		
	}
	
	return playback_start(&av_playback_dat, file_name, sec, fun);
}

int API_PlaybackPause( void )
{
	return playback_pause(&av_playback_dat);
}

int API_PlaybackReplay( void )
{
	return playback_replay(&av_playback_dat);
}

int API_PlaybackContinue( void )
{
	return playback_continue(&av_playback_dat);
}

int API_PlaybackClose( void )
{
	return playback_close(&av_playback_dat);
}

RECORD_PLAYBACK_STATE_t API_GetPlaybackState(void)
{
	return get_playback_state(&av_playback_dat);
}

