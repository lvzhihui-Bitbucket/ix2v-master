
#ifndef _AK_DECODER_H_
#define	_AK_DECODER_H_

#include "ak_common.h"
#include "ak_log.h" 
#include "ak_vdec.h"
#include "ak_vo.h"
#include "ak_thread.h"
#include "ak_mem.h"
#include "ak_tde.h"

#define		DECODER_STATUS_IDLE		0
#define		DECODER_STATUS_START	1
#define		DECODER_STATUS_RUN		2
#define		DECODER_STATUS_STOP		3

#if defined( LVGL_SUPPORT_TDE )	// TDE_VIDEO_COMBINE
#include 	"lvgl.h"

struct ak_lvgl_video_win
{	
	int					win_en;			// 显示窗口使能
	int					win_id;			// 显示窗口ID
	lv_obj_t* 			parent;			// 显示窗口的父窗口
	lv_obj_t* 			video_win;		// 显示img的容器
	int					foreground;		// 0:background, 1:foreground
    lv_img_dsc_t 		img_lv_video;	// 显示Image对象
	void*				video_dma_buf;	// 显示缓冲
	unsigned long 		video_dma_buf_paddr;
	pthread_mutex_t		en_lock;
};
#endif	

struct ak_one_decoder
{
    int 			in_type;
    int 			out_type;	
	unsigned int 	src_width;
	unsigned int 	src_height;
	unsigned int 	dst_top;
	unsigned int 	dst_left;
	unsigned int 	dst_width;
	unsigned int 	dst_height;
	unsigned int 	dst_layer;

	int				dec_status;			// 0:idle, 1:start, 2:run, 3:stop
	//ak_mutex_t		dec_mutex;

	int				dec_handle;
	//ak_pthread_t	recv_frame_id;
	//ak_mutex_t		frame_mutex;
	pthread_t 			recv_frame_id;			// 任务id
	pthread_attr_t 		pattr;
	pthread_mutex_t 	frame_lock;
	struct ak_vdec_frame 	frame;	
	struct ak_vo_obj 		obj;
	int frame_cnt;
	int debug;

#if defined( LVGL_SUPPORT_TDE )	// TDE_VIDEO_COMBINE
	struct ak_lvgl_video_win	*pvd_win;
#endif	
};

struct ak_one_decoder* ak_new_one_decode2( int in_type, int src_w, int src_h, int out_type, int dst_layer );
int ak_push_frame(struct ak_one_decoder *pOneDecoder, unsigned char *pdat, int len );
int ak_del_one_decode(struct ak_one_decoder *pOneDecoder);

int Set_ds_show_pos(int ins_num, int x, int y, int width, int height);
int Clear_ds_show_layer(int ins_num);

#endif



