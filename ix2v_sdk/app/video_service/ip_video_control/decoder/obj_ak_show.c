
#include "obj_ak_show.h"
#include "elog_forcall.h"

// initial one instance
int h264_show_initial( H264_SHOW_INSTANCE_t *pins, int vdec_layer )
{
	pthread_mutex_init( &pins->h264_show_state_lock, 0);	
	pins->state		= H264_SHOW_IDLE;
	pins->IDR		= 0;	
	pins->ptr_vdec	= NULL;
	pins->vdec_layer= vdec_layer;
	return 0;
}

void set_h264_show_state(H264_SHOW_INSTANCE_t *pins, H264_SHOW_STATE_t state)
{
	pins->state = state;
}

H264_SHOW_STATE_t get_h264_show_state(H264_SHOW_INSTANCE_t *pins)
{
	return pins->state;
}

// start one h264 show 
int h264_show_start( H264_SHOW_INSTANCE_t *pins, int width, int height, int fps, int baudrate , int dispx, int dispy, int dispw, int disph, int videotype )
{
	int ret=0;
	pthread_mutex_lock( &pins->h264_show_state_lock );

	if( get_h264_show_state(pins) != H264_SHOW_IDLE )
	{
		log_d( "%s(%d)-<%s> fail\n", __FILE__, __LINE__, __FUNCTION__ ) ;	
		pthread_mutex_unlock( &pins->h264_show_state_lock );	
		return -1;
	}

	set_h264_show_state(pins,H264_SHOW_START);
	
	pins->width 		= width;
	pins->height		= height;
	pins->fps			= fps;
	pins->baudrate		= baudrate;
	pins->disp_posx		= dispx;
	pins->disp_posy		= dispy;
	pins->disp_width	= dispw;
	pins->disp_height	= disph;
	pins->disp_update_flag = 0;

	pins->IDR		= 0;

	//SetLayerPos(pins->vdec_layer, dispx, dispy, dispw, disph);
	//if(pins->ptr_vdec==NULL)
	{
		if( videotype )
		{
			log_d( "h265_show_thread_processing...\n");
			pins->ptr_vdec = ak_new_one_decode2(2 ,width,height,AK_TILE_YUV,pins->vdec_layer);//AK_CODEC_H265
		}
		else
		{
			log_d( "h264_show_thread_processing...\n");
			#if !defined(PID_IX850)
			pins->ptr_vdec = ak_new_one_decode2(0 ,width,height,AK_TILE_YUV,pins->vdec_layer);//AK_CODEC_H264
			#else
			pins->ptr_vdec = ak_new_one_decode2(0 ,width,height,AK_YUV420SP,pins->vdec_layer);
			#endif
		}
	}
	if(pins->ptr_vdec!=NULL)
		set_h264_show_state(pins,H264_SHOW_RUN);	
	else
	{
		set_h264_show_state(pins,H264_SHOW_IDLE);
		ret=-1;
	}
	
	log_d( "%s(%d)-<%s> ok\n", __FILE__, __LINE__, __FUNCTION__ ) ;		

	pthread_mutex_unlock( &pins->h264_show_state_lock );	
	return ret;
}

// start one h264 show 
int h264_show_update( H264_SHOW_INSTANCE_t *pins, int width, int height, int fps, int baudrate , int dispx, int dispy, int dispw, int disph, int videotype )
{
	pthread_mutex_lock( &pins->h264_show_state_lock );

	if( get_h264_show_state(pins) == H264_SHOW_IDLE )
	{
		log_d( "%s(%d)-<%s> fail\n", __FILE__, __LINE__, __FUNCTION__ ) ;	
		pthread_mutex_unlock( &pins->h264_show_state_lock );	
		return -1;
	}
	pins->width 		= width;
	pins->height		= height;
	pins->fps			= fps;
	pins->baudrate		= baudrate;
	pins->disp_posx		= dispx;
	pins->disp_posy		= dispy;
	pins->disp_width	= dispw;
	pins->disp_height	= disph;
	pins->disp_update_flag = 1;

	log_d( "%s(%d)-<%s> ok\n", __FILE__, __LINE__, __FUNCTION__ ) ;	
	
	pthread_mutex_unlock( &pins->h264_show_state_lock );
	
	return 0;
}

// stop one h264 show 
int h264_show_stop( H264_SHOW_INSTANCE_t *pins )
{
	pthread_mutex_lock( &pins->h264_show_state_lock );
	log_d( "h264_show_stop start!!!\n") ;		
	if( get_h264_show_state( pins ) != H264_SHOW_IDLE )
	{
		ak_del_one_decode(pins->ptr_vdec);
		pins->ptr_vdec=NULL;
		set_h264_show_state(pins, H264_SHOW_IDLE ); 
		log_d( "%s(%d)-<%s> ok\n", __FILE__, __LINE__, __FUNCTION__ ) ;		
	}
	pthread_mutex_unlock( &pins->h264_show_state_lock );
	return 0;
}

// h264 data source trigger h264 show 
int h264_show_dump_buf( H264_SHOW_INSTANCE_t *pins, char* pdatbuf, int datlen,int tick)
{	
	//pthread_mutex_lock( &pins->h264_show_state_lock );
	if( get_h264_show_state( pins ) != H264_SHOW_RUN )
	{
		//pthread_mutex_unlock( &pins->h264_show_state_lock );
		return -1;
	}
	
	long long cur_time;
	cur_time = time_since_last_call(-1);	
	
	//rtp_sender_send_with_ts_wan(pdatbuf, datlen,tick,0);
	//rtp_sender_send_with_ts_uni(pdatbuf, datlen,tick,0);
	//printf("44444444444444task_monitor4ds_process\n");
	ak_push_frame( pins->ptr_vdec,pdatbuf, datlen);

	//printf("==AK decode:[%d]\n",time_since_last_call(cur_time));	
	
	//pthread_mutex_unlock( &pins->h264_show_state_lock );	
	return 0;
}



