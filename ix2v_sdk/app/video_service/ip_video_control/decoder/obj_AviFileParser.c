
#include "obj_AviFileParser.h"

uint32 read_le_dw( char* p )
{
	return (uint32)( (p[3] << 24) |  (p[2] << 16) | (p[1] << 8) | p[0] ) ;
}	

uint16 read_le_w( char* p )
{
	return (uint16) (uint32)( (p[1] << 8) | p[0] ) ;
}

int readFile( AVIFileParset* aviParset, char* buf, uint32 len )  
{  
	uint32 ret = fread( buf, 1, len, aviParset->aviFd );
	if( ret != len )
	{  
		if( feof( aviParset->aviFd ) != 0 )
			return TRUE;
	}  

	return FALSE;  
} 

int readFileDW( AVIFileParset* aviParset, uint32* dw )  
{  
	char tmpBuf[4];  
	int ret = FALSE;
	
	 ret	= readFile( aviParset, tmpBuf, 4 );
	*dw 	= read_le_dw( tmpBuf );
	
	return ret;  
} 

void readFileW( AVIFileParset* aviParset, uint16* w )  
{  
	char tmpBuf[2] = {0};
	
	readFile( aviParset, tmpBuf, 2 );
	
	*w =  read_le_w( tmpBuf );  
} 

int readFileTagSize( AVIFileParset* aviParset, DWORD* fourcc, uint32* size, DWORD* fourcc2 )  
{  
	char tmpBuf[12] = {0};  
	int ret = readFile( aviParset, tmpBuf, 12 );
	
	*fourcc	 = read_le_dw( tmpBuf );
	*size 	 = read_le_dw( tmpBuf + 4 );
	*fourcc2 = read_le_dw( tmpBuf + 8 );
	
	return ret;  
}

uint8 parseAviFile( AVIFileParset* aviParset, const char * fileName)  
{  
	DWORD fourcc = 0;  
	DWORD fourcc2 = 0, temLen = 0;  
	int hasIndex = FALSE;  

	pthread_mutex_init( &aviParset->lock, 0);
	
	pthread_mutex_lock(&aviParset->lock);

	aviParset->aviIndex.videoIndexMap = NULL;
	aviParset->aviIndex.audioIndexMap = NULL;

	aviParset->aviFd = fopen( fileName, "rb" );	
	if( aviParset->aviFd == NULL )
	{
		pthread_mutex_unlock(&aviParset->lock);
		return ERROR_OPEN_AVI;
	}
	
	readFileTagSize( aviParset, &fourcc, &aviParset->fLen, &fourcc2 );
	
	if( fourcc != FOURCC_RIFF || fourcc2 != FOURCC_AVI )
	{
		pthread_mutex_unlock(&aviParset->lock);
		return ERROR_INVALID_AVI;
	}
	
	while(1)
	{  
		uint32 size = 0;  
		int isEof;

		isEof = readFileDW( aviParset, &fourcc );
		if(isEof)
		{
			break;
		}

		isEof = readFileDW( aviParset, &size );
		if(isEof)
		{
			break;
		}
	  
		if( fourcc == FOURCC_LIST )
	    {
			readFileDW( aviParset, &fourcc2 );  
            switch ( fourcc2 )
            {  
            	case FOURCC_hdrl:
                {  
                    if( size > MAX_ALLOWED_AVI_HEADER_SIZE )
                	{
                		pthread_mutex_unlock(&aviParset->lock);
                    	return ERROR_INVALID_AVI;  
                	}

		        	// 跳过hdrl
					uint32 off = 4;  
					while( off < size )
					{  
						readFileDW( aviParset, &fourcc );  
                       	switch( fourcc )
                        {  
							case FOURCC_avih:
								{
									aviParset->aviMainHeader.fcc = FOURCC_avih;  
									readFileDW(aviParset, &aviParset->aviMainHeader.cb );
									readFileDW(aviParset, &aviParset->aviMainHeader.dwMicroSecPerFrame );
									readFileDW(aviParset, &aviParset->aviMainHeader.dwMaxBytesPerSec );
									readFileDW(aviParset, &aviParset->aviMainHeader.dwPaddingGranularity );
									readFileDW(aviParset, &aviParset->aviMainHeader.dwFlags );
									readFileDW(aviParset, &aviParset->aviMainHeader.dwTotalFrames );
									readFileDW(aviParset, &aviParset->aviMainHeader.dwInitialFrames );
									readFileDW(aviParset, &aviParset->aviMainHeader.dwStreams );
									readFileDW(aviParset, &aviParset->aviMainHeader.dwSuggestedBufferSize );
									readFileDW(aviParset, &aviParset->aviMainHeader.dwWidth );
									readFileDW(aviParset, &aviParset->aviMainHeader.dwHeight );

									if( (AVIFILEINFO_HASINDEX & aviParset->aviMainHeader.dwFlags) == AVIFILEINFO_HASINDEX )
										 hasIndex = TRUE;
		  
									// 跳过保留字段
									fseek( aviParset->aviFd, 16, SEEK_CUR );  
									// 跳过avih以及长度各四个字节
									off += aviParset->aviMainHeader.cb + 8;  
								}  
								break;
										
                   			case FOURCC_LIST:
                            	{  
                               		uint32 avListLen = 0;
									uint32 fourcc = 0;
							
									readFileDW( aviParset, &avListLen );
									readFileDW( aviParset, &fourcc );
									
                                	if( fourcc != FOURCC_strl )
									{
										pthread_mutex_unlock(&aviParset->lock);
										return ERROR_INVALID_AVI;  
									}
  
			                        // 跳过strl 
			                        int tmpOff = 4;  
			                        AVIStreamHeader aviStreamHeader = {0};
											
									while( tmpOff < avListLen )
									{  
										readFileDW( aviParset, &fourcc );  
										tmpOff += 4;  
										if( fourcc == FOURCC_strh )
										{  
											aviStreamHeader.fcc = FOURCC_strh;  
											readFileDW(aviParset, &aviStreamHeader.cb );  
											readFileDW(aviParset, &aviStreamHeader.fccType );  
											readFileDW(aviParset, &aviStreamHeader.fccHandler );
											readFileDW(aviParset, &aviStreamHeader.dwFlags );  
											readFileW(aviParset, &aviStreamHeader.wPriority );  
											readFileW(aviParset, &aviStreamHeader.wLanguage );  
											readFileDW(aviParset, &aviStreamHeader.dwInitialFrames ); 
											readFileDW(aviParset, &aviStreamHeader.dwScale );  
											readFileDW(aviParset, &aviStreamHeader.dwRate );  
											readFileDW(aviParset, &aviStreamHeader.dwStart );  
											readFileDW(aviParset, &aviStreamHeader.dwLength );  
											readFileDW(aviParset, &aviStreamHeader.dwSuggestedBufferSize );  
											readFileDW(aviParset, &aviStreamHeader.dwQuality );  
											readFileDW(aviParset, &aviStreamHeader.dwSampleSize );  
											readFileW(aviParset, &aviStreamHeader.rcFrame.left );  
											readFileW(aviParset, &aviStreamHeader.rcFrame.top );  
											readFileW(aviParset, &aviStreamHeader.rcFrame.right );  
											readFileW(aviParset, &aviStreamHeader.rcFrame.bottom );  


											// 跳过长度 ??
											tmpOff += 4;  
											tmpOff += aviStreamHeader.cb;  
										}  
										else if ( fourcc == FOURCC_strf )
										{  
											uint32 tmpLen = 0;
											readFileDW( aviParset, &tmpLen );
											
											if (aviStreamHeader.fccType == FOURCC_vids)  
											{  
												aviParset->hasVideo = TRUE;  
												aviParset->aviVideoStreamHeader = aviStreamHeader;  
												readFileDW(aviParset, &aviParset->bitmapInfo.bmiHeader.biSize );  
												readFileDW(aviParset, &aviParset->bitmapInfo.bmiHeader.biWidth );  
												readFileDW(aviParset, &aviParset->bitmapInfo.bmiHeader.biHeight );  
												readFileW(aviParset, &aviParset->bitmapInfo.bmiHeader.biPlanes );  
												readFileW(aviParset, &aviParset->bitmapInfo.bmiHeader.biBitCount );  
												readFileDW(aviParset, &aviParset->bitmapInfo.bmiHeader.biCompression );  
												readFileDW(aviParset, &aviParset->bitmapInfo.bmiHeader.biSizeImage );  
												readFileDW(aviParset, &aviParset->bitmapInfo.bmiHeader.biXPelsPerMeter );  
												readFileDW(aviParset, &aviParset->bitmapInfo.bmiHeader.biYPelsPerMeter );  
												readFileDW(aviParset, &aviParset->bitmapInfo.bmiHeader.biClrUsed );  
												readFileDW(aviParset, &aviParset->bitmapInfo.bmiHeader.biClrImportant );  
												if( tmpLen > aviParset->bitmapInfo.bmiHeader.biSize )  
													fseek( aviParset->aviFd, tmpLen - aviParset->bitmapInfo.bmiHeader.biSize, SEEK_CUR );
											}  
											else if (aviStreamHeader.fccType == FOURCC_auds)  
											{  
												aviParset->hasAudio = TRUE;  
												aviParset->aviAudioStreamHeader = aviStreamHeader;  
												readFileW(aviParset, &aviParset->waveInfo.wFormatTag );  
												readFileW(aviParset, &aviParset->waveInfo.nChannels );  
												readFileDW(aviParset, &aviParset->waveInfo.nSamplesPerSec );  
												readFileDW(aviParset, &aviParset->waveInfo.nAvgBytesPerSec );  
												readFileW(aviParset, &aviParset->waveInfo.nBlockAlign );  
												readFileW(aviParset, &aviParset->waveInfo.wBitsPerSample );  
												readFileW(aviParset, &aviParset->waveInfo.cbSize );  

												if( tmpLen > aviParset->waveInfo.cbSize )
													fseek( aviParset->aviFd, tmpLen-aviParset->waveInfo.cbSize, SEEK_CUR );
											}  

											// 跳过长度??
											tmpOff += 4;  
											tmpOff += tmpLen;  
										}  
										else if( fourcc == FOURCC_JUNK )
										{  
											uint32 tmpLen = 0;
											readFileDW(aviParset, &tmpLen );  
											fseek( aviParset->aviFd, tmpLen, SEEK_CUR);  

											// 跳过长度
											tmpOff += 4;  
											tmpOff += tmpLen;  
										}  
										else if( fourcc == FOURCC_vprp )
										{  
											uint32 tmpLen = 0;
											readFileDW(aviParset, &tmpLen );  
											fseek( aviParset->aviFd, tmpLen, SEEK_CUR );  

											// 跳过长度
											tmpOff += 4;  
											tmpOff += tmpLen;  
										}  
									}  

									off += avListLen + 8;  
								}  
                            	break;  

							case FOURCC_JUNK:
								{  
									// 跳过JUNK
									off += 4;  
									uint32 tmpLen = 0;
									readFileDW(aviParset, &tmpLen );  
									fseek( aviParset->aviFd, tmpLen, SEEK_CUR );  

									// 跳过长度
									off += 4;  
									off += tmpLen;  
								}  
								break;  
						}  
					}  
            	}  
            	break; 
					
				case FOURCC_INFO:
				{  
					readFileDW(aviParset, &fourcc );  
					if( fourcc == FOURCC_ISFT )  
					{  
						uint32 tmpLen = 0;
						readFileDW(aviParset, &tmpLen );  
						char resize[tmpLen];  
						readFile(aviParset, resize, tmpLen );  
					}  
				}  
				break;

            	case FOURCC_movi:
				{  
					aviParset->moviOff = ftell( aviParset->aviFd ) - 4;  
					if( hasIndex )
					{  
						// 跳过movi,直接到idx处
						fseek( aviParset->aviFd, size - 4, SEEK_CUR );  
					}  
				}  
				break;  
			}  
    	}  
		else if( fourcc == FOURCC_idx1 )  
        {  
			aviParset->aviIndex.fcc = FOURCC_idx1;  
			aviParset->aviIndex.cb = size;  

			int tmpOff = 0;
			int videoCnt = 0;
			int audioCnt = 0;
			
			while ( tmpOff < aviParset->aviIndex.cb )  
			{  
                AVIIndexEntry tmpEntry;  
				
				readFileDW(aviParset,&tmpEntry.dwChunkId);
                readFileDW(aviParset,&tmpEntry.dwFlags);  
                readFileDW(aviParset,&tmpEntry.dwOffset);  
                readFileDW(aviParset,&tmpEntry.dwSize);
				
                if (tmpEntry.dwSize > aviParset->maxFrameSize)  
                    aviParset->maxFrameSize = tmpEntry.dwSize;  
				
                // 视频数据  
                if (tmpEntry.dwChunkId == FOURCC_00dc)  
                {  
                	videoCnt++;
                	aviParset->aviIndex.videoIndexMap = realloc( aviParset->aviIndex.videoIndexMap, sizeof(AVIIndexEntry)*videoCnt );
					memcpy(aviParset->aviIndex.videoIndexMap+videoCnt-1, &tmpEntry, sizeof(AVIIndexEntry));
                }  
                else if (tmpEntry.dwChunkId == FOURCC_01wb)  
                {  
                	audioCnt++;
                    aviParset->aviIndex.audioIndexMap = realloc( aviParset->aviIndex.audioIndexMap, 16*audioCnt );
					memcpy(aviParset->aviIndex.audioIndexMap+audioCnt-1, &tmpEntry, sizeof(AVIIndexEntry));
                }  
                // 一个索引信息的长度  
                tmpOff += sizeof(AVIIndexEntry);  

			}  
        }  
        else if (fourcc ==  FOURCC_JUNK)  
        {  
			// 跳过 
			fseek( aviParset->aviFd, size, SEEK_CUR );  
        }  
	} 
	pthread_mutex_unlock(&aviParset->lock);
	return SUCCESS_VALID_AVI;
}  

int getVideoFrame( AVIFileParset* aviParset, char* buf, int index) 
{  
	pthread_mutex_lock(&aviParset->lock);
	
    if ((AVIFILEINFO_HASINDEX & aviParset->aviMainHeader.dwFlags) != AVIFILEINFO_HASINDEX)  
    {
    	pthread_mutex_unlock(&aviParset->lock);
        return 0;  
    }

  	if(index >= aviParset->aviVideoStreamHeader.dwLength)
  	{
  		pthread_mutex_unlock(&aviParset->lock);
		return 0;
	}

	if(aviParset->aviFd == NULL)
	{
		pthread_mutex_unlock(&aviParset->lock);
		return 0;
	}
	
    fseek(aviParset->aviFd, aviParset->moviOff + aviParset->aviIndex.videoIndexMap[index].dwOffset + 8, SEEK_SET);  
    int ret = fread(buf, 1, aviParset->aviIndex.videoIndexMap[index].dwSize, aviParset->aviFd);  
	
	pthread_mutex_unlock(&aviParset->lock);

    return ret;  
} 

int getAudioFrame( AVIFileParset* aviParset, char* buf, int index) 
{  
	pthread_mutex_lock(&aviParset->lock);
	
    if ((AVIFILEINFO_HASINDEX & aviParset->aviMainHeader.dwFlags) != AVIFILEINFO_HASINDEX) 
    {
    	pthread_mutex_unlock(&aviParset->lock);
        return 0;  
    }

  	if(index >= aviParset->aviAudioStreamHeader.dwLength)
  	{
  		pthread_mutex_unlock(&aviParset->lock);
		return 0;
	}

	if(aviParset->aviFd == NULL)
	{
		pthread_mutex_unlock(&aviParset->lock);
		return 0;
	}
		
    fseek(aviParset->aviFd, aviParset->moviOff + aviParset->aviIndex.audioIndexMap[index].dwOffset + 8, SEEK_SET);  
    int ret = fread(buf, 1, aviParset->aviIndex.audioIndexMap[index].dwSize, aviParset->aviFd);  

	int samples = ret/(aviParset->waveInfo.nChannels*(aviParset->waveInfo.wBitsPerSample/8));
	
	pthread_mutex_unlock(&aviParset->lock);

    return samples;  
}

int getTotalVideoFrames(AVIFileParset* aviParset) 
{
	return aviParset->aviVideoStreamHeader.dwLength;
}

int getTotalAudioFrames(AVIFileParset* aviParset) 
{
	return aviParset->aviAudioStreamHeader.dwLength;
}

void getVideoSize(AVIFileParset* aviParset, int* width, int* height, int* vdtype, int* fps)
{
	*width = aviParset->aviMainHeader.dwWidth;
	*height = aviParset->aviMainHeader.dwHeight;
	*vdtype = aviParset->aviVideoStreamHeader.fccHandler == FOURCC_H264? 0 : 1;
	*fps	= aviParset->aviVideoStreamHeader.dwRate/1000;
	//printf("getVideoSize: fps[%d] dwRate[%d]\n", *fps,aviParset->aviVideoStreamHeader.dwRate);
}

void CloseAviFile(AVIFileParset* aviParset)
{
	pthread_mutex_lock(&aviParset->lock);
	
	if(aviParset->aviIndex.videoIndexMap != NULL)
	{
		free(aviParset->aviIndex.videoIndexMap);
		aviParset->aviIndex.videoIndexMap = NULL;
	}
	
	if(aviParset->aviIndex.audioIndexMap != NULL)
	{
		free(aviParset->aviIndex.audioIndexMap);
		aviParset->aviIndex.audioIndexMap = NULL;
	}

	if(aviParset->aviFd != NULL)
	{
		fclose(aviParset->aviFd);
		aviParset->aviFd = NULL;
	}
	
	pthread_mutex_unlock(&aviParset->lock);

	pthread_mutex_destroy(&aviParset->lock);
}


