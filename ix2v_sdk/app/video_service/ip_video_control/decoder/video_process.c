
#include <stdio.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/timeb.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <sys/ioctl.h>
#include <sys/ipc.h>
#include <sys/socket.h>
#include <sys/resource.h>
#include <sys/time.h>
#include <sys/types.h>
#include <errno.h>
#include <linux/fb.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/un.h>
#include <signal.h>
#include <dirent.h>
#include <pthread.h>
#include <net/if.h>

#include "video_process.h" 

#include "udp_fragment_opt.h"
#include "utility.h"

#include "obj_ak_decoder.h"
#include "obj_ak_show.h"
#include "cJSON.h"
#include "elog_forcall.h"
#include "obj_PublicInformation.h"
#include "task_Event.h"

#define DEC_SIZE	(150*1024)

static H264_SHOW_INSTANCE_t	monitor_ins_array[4];

int api_monitor_show_initial(void)
{
	int i,layer;
	layer = AK_VO_LAYER_VIDEO_1;
	for( i = 0; i < 4; i++ )
	{
		h264_show_initial( &monitor_ins_array[i],layer++);
	}
}

int api_monitor_show_start( int index, int width, int height, int videotype )
{
	int ret = h264_show_start( &monitor_ins_array[index], width, height, 0, 0 , 0, 0, 0, 0, videotype );	
	if(ret == -1)
	{
		char buff[100];
		sprintf(buff,"open decode %s err!", videotype==1? "h265" : "h264");
		api_mon_req_result(index, buff);
	}
	else 
	{
		//api_mon_req_result(index, "decode ok!");
	}
	return ret;
}

int api_monitor_show_stop( int index )
{
	return h264_show_stop( &monitor_ins_array[index] );
}

int api_monitor_show_buf( int index, char* pdatbuf, int datlen,int tick)
{
	return h264_show_dump_buf( &monitor_ins_array[index], pdatbuf, datlen,tick);
}
int Get_Monitor_Show_state(int index)
{
	if(index>=4)
		return -1;
	return monitor_ins_array[index].state;
	
}
void decoder_debug(void)
{
	int i;
	for(i=0;i<4;i++)
	{
		if(monitor_ins_array[i].ptr_vdec!=NULL)
		ShellCmdPrintf("decoder[%d],state[%d],debug[%d],cnt[%d]",i,monitor_ins_array[i].state,monitor_ins_array[i].ptr_vdec->debug,monitor_ins_array[i].ptr_vdec->frame_cnt);
	}
}
int task_monitor4ds_init( void* arg )
{
	one_tiny_task_t *ptiny_task = (one_tiny_task_t*)arg;
	monitor4ds_dat_t *pusr_dat = (monitor4ds_dat_t*)ptiny_task->pusr_dat;	

	long long cur_time;
	cur_time = time_since_last_call(-1);	
	
	CVideoPackProcessor_InitNode(pusr_dat->ds_ch);
	if(api_monitor_show_start(pusr_dat->ds_ch, 1280, 720, 0)!=0)
		return -1;
	if( ( pusr_dat->Receive.Sd = socket( PF_INET, SOCK_DGRAM, 0 )  ) == -1 )
	{
		api_monitor_show_stop(pusr_dat->ds_ch);
		log_e("creating socket failed!");
		return -1;
	}
	// 设置超时
	struct timeval timeout;
	timeout.tv_sec =	1;
	timeout.tv_usec =	0;
	if( setsockopt(pusr_dat->Receive.Sd, SOL_SOCKET, SO_RCVTIMEO, &timeout, sizeof(timeout)) == -1 ) 
	{
		api_monitor_show_stop(pusr_dat->ds_ch);
		log_d("set socket timeout failed!\n");
	}	
	
	bzero( &pusr_dat->Receive.sock_addr, sizeof(pusr_dat->Receive.sock_addr) );
	pusr_dat->Receive.sock_addr.sin_family		= AF_INET;
	pusr_dat->Receive.sock_addr.sin_addr.s_addr	= htonl(INADDR_ANY);
	pusr_dat->Receive.sock_addr.sin_port		= htons( pusr_dat->Receive.Port);
	
	if( bind(pusr_dat->Receive.Sd, (struct sockaddr*)&pusr_dat->Receive.sock_addr, sizeof(pusr_dat->Receive.sock_addr)) != 0 )
	{	
		close( pusr_dat->Receive.Sd );		
		perror( "bind" );
		return -1;
	}
	
	if( pusr_dat->Receive.type )	
		join_multicast_group2(pusr_dat->Receive.net_device_name, pusr_dat->Receive.Sd, pusr_dat->Receive.addr );
	log_d("task_monitor4ds_init ins[%d] time[%d]\n",pusr_dat->ds_ch, time_since_last_call(cur_time));	
	return 0;
}

int task_monitor4ds_process( void* arg )
{
	one_tiny_task_t *ptiny_task = (one_tiny_task_t*)arg;
	monitor4ds_dat_t *pusr_dat = (monitor4ds_dat_t*)ptiny_task->pusr_dat;	
	int size = 0;
	u_int32_t addr_len = sizeof( pusr_dat->Receive.sock_addr );
	struct 	sockaddr_in sock_addr;
	udp_fragment_t	recv_fragment;
	AVPackNode *Node;		
	unsigned char msgMulticast_Data[DEC_SIZE];
	int IDR = 0;
	int errorCnt=0;
	
	fd_set fds;
	int ret;
	struct timeval tv={1,0};
	int save_frameid=0;
	pusr_dat->state			= 1;
	log_d("======task_monitor4ds_process run!!!!!=======\n");
	//sleep(3);
	usleep(100*1000);
	while( one_tiny_task_is_running(ptiny_task) )
	{	
		FD_ZERO(&fds);
		FD_SET(pusr_dat->Receive.Sd,&fds);
		
		tv.tv_sec = 0;
		tv.tv_usec = 200*1000;	// 100ms
		ret = select( pusr_dat->Receive.Sd + 1, &fds, NULL, NULL, &tv );
		//usleep(10);
		//printf("00000000000task_monitor4ds_process\n");
		if(ret>0)
		{
			//printf("111111111111111task_monitor4ds_process\n");
			size = recvfrom( pusr_dat->Receive.Sd, (unsigned char*)&recv_fragment, sizeof(udp_fragment_t), 0, (struct sockaddr*)&sock_addr, &addr_len );
			//printf("222222222222222task_monitor4ds_process\n");
			if( size == 0 )
			{
				log_d("======recvfrom broken!=======\n");			
			}
			else if( size < 0 )
			{
				log_e("======recvfrom error!=======\n");			
				errorCnt++;
				if(errorCnt >= 10)
					stop_receive_decode_process(pusr_dat->ds_ch);
			}
			else
			{
				errorCnt = 0;
				if(CVideoPackProcessor_ProcPack(pusr_dat->ds_ch, (unsigned char*)&recv_fragment,size) == 0)
				{
					Node = CVideoPackProcessor_PickPack(pusr_dat->ds_ch);
					if( Node != NULL )
					{
						size = Node->Content.Len;		
						memcpy(msgMulticast_Data,Node->Content.Buffer,size);
						
						//printf("r:frame_sn=%d,len=%d,t=%d\n",Node->Content.FrameNo,size,Node->Content.Timestamp);
						//printf("333333333333333task_monitor4ds_process %d\n",Node->Content.FrameNo);
						//CVideoPackProcessor_ReturnNode(pusr_dat->ds_ch,Node);
						//if( IDR == 0 && msgMulticast_Data[0] == 0 && msgMulticast_Data[1] == 0 && msgMulticast_Data[2] == 0 && msgMulticast_Data[3] == 1 )
						if(save_frameid==0)
							save_frameid=Node->Content.FrameNo-1;
						if(IDR == 0 && msgMulticast_Data[0] == 0 && msgMulticast_Data[1] == 0 && msgMulticast_Data[2] == 0 && msgMulticast_Data[3] == 1 )
						{
							if( ( (msgMulticast_Data[4]) & (0x1f) ) == 7)   // 等待关键帧 SPS + PPS + IDR 
							{
								IDR = 1;				
								printf("-------IDR OK!!!!!!!!!!!!!!!-------\n");
								//save_frameid=Node->Content.FrameNo-1;
							}
							
						}
						
						
		
						//if( IDR&&save_frameid==(Node->Content.FrameNo-1))
						//if( IDR&&size>2000)	
						if( IDR)
						{	
							//PrintCurrentTime(0);
							save_frameid=Node->Content.FrameNo;
							api_monitor_show_buf(pusr_dat->ds_ch, msgMulticast_Data, size,Node->Content.Timestamp);
							//ak_push_frame( ptr_vdec_array[0],(unsigned char*)msgMulticast_Data, size);
							//PrintCurrentTime(1);
							//IDR=0;
							//API_Recording_mux( msgMulticast_Data, size, pusr_dat->ds_ch, 0 );
						}
						else
							bprintf("-------IDR NOT OK!!!!!!!!!!!!!!!-------\n");
						CVideoPackProcessor_ReturnNode(pusr_dat->ds_ch,Node);
					}
				}
			}
			//printf("555555555555task_monitor4ds_process\n");
		}
	}	
	log_d("======task_monitor4ds_process exit!!!!!====1===\n");
	return 0;
}

int task_monitor4ds_exit( void* arg )
{
	one_tiny_task_t *ptiny_task = (one_tiny_task_t*)arg;
	monitor4ds_dat_t *pusr_dat = (monitor4ds_dat_t*)ptiny_task->pusr_dat;	
	log_d("======task_monitor4ds_exit ch=%d !!!!!=======\n", pusr_dat->ds_ch);
	if( pusr_dat->Receive.type ) leave_multicast_group2(pusr_dat->Receive.net_device_name, pusr_dat->Receive.Sd, pusr_dat->Receive.addr );	
	close( pusr_dat->Receive.Sd );
	CVideoPackProcessor_ClearNode(pusr_dat->ds_ch);	
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

static monitor4ds_dat_t		monitor4ds[4];

int start_receive_decode_process(int index, int server_ip, int trans_ip, unsigned short trans_port, int trans_type )
{	
	monitor4ds_dat_t* pins = &monitor4ds[index];
	//log_e("======start_receive_decode_process index=%d state=%d!!!!!=======\n", index, pins->state);
	if( pins->state )
		return -1;
	pins->ds_ch = index;
	// 登记h.264接收类型、端口号、地址
	pins->Receive.addr	= trans_ip;
	pins->Receive.Port	= trans_port;
	pins->Receive.type	= trans_type;
	pins->Receive.net_device_name = GetNetDeviceNameByTargetIp(server_ip);
	if( one_tiny_task_start2( &pins->task_process, task_monitor4ds_init, task_monitor4ds_process, task_monitor4ds_exit, (void*)pins ) == 0 )
	{
		log_d("start video decode[%d]",index);
		return 0;
	}
	else
	{
		return -1;
	}
}

int stop_receive_decode_process(int index)
{
	long long cur_time;
	cur_time = time_since_last_call(-1);	
	monitor4ds_dat_t* pins = &monitor4ds[index];
	//log_e("======stop_receive_decode_process index=%d state=%d!!!!!=======\n", index, pins->state);
	one_tiny_task_stop2( &pins->task_process );
	//close_h264_dec_device();
	//api_monitor_show_stop(pins->ds_ch);
	//usleep(500*1000);
	api_monitor_show_stop(pins->ds_ch);
	pins->state = 0;
	//log_e("==stop_receive_decode_process ins[%d] time[%d] \n",index, time_since_last_call(cur_time));	
	log_d("stop video decode[%d]",index);
	return 0;
}

extern char* my_inet_ntoa(int ip_n, char* ip_a);
extern cJSON *GetLayerShowState(int ins);
cJSON *GetTransInfoState(int ins)
{
	cJSON *state;
	char addr[20];
	state=cJSON_CreateObject();
	if(state==NULL)
		return NULL;
	if(monitor4ds[ins].state)
	{
		cJSON_AddStringToObject(state,"TransState","Running");
		cJSON_AddStringToObject(state,"Net_Dev_Name",monitor4ds[ins].Receive.net_device_name);
		cJSON_AddStringToObject(state,"Recv_Addr",my_inet_ntoa( monitor4ds[ins].Receive.addr,addr));
		
		cJSON_AddNumberToObject(state,"Trans_Port",monitor4ds[ins].Receive.Port);
		log_d("Recv_Addr:%s,port:%d\n",addr,monitor4ds[ins].Receive.Port);
		if(monitor4ds[ins].Receive.type)
			cJSON_AddStringToObject(state,"Trans_Type","Multicast");	
		else
			cJSON_AddStringToObject(state,"Trans_Type","Unicast");	
		
	}
	else
		cJSON_AddStringToObject(state,"TransState","Idle");
	return state;
}

cJSON *GetCodecState(int ins)
{
	cJSON *state;
	state=cJSON_CreateObject();
	if(state==NULL)
		return NULL;
	if(monitor_ins_array[ins].state == H264_SHOW_IDLE)
	{
		cJSON_AddStringToObject(state,"H264_SHOW","IDLE");
	}
	else if(monitor_ins_array[ins].state == H264_SHOW_START)
	{
		cJSON_AddStringToObject(state,"H264_SHOW","START");
	}
	else if(monitor_ins_array[ins].state == H264_SHOW_RUN)
	{
		cJSON_AddStringToObject(state,"H264_SHOW","RUN");
		//printf("monitor_ins_array[ins].ptr_vdec:%08x\n",monitor_ins_array[ins].ptr_vdec);
		#if 1
		if(monitor_ins_array[ins].ptr_vdec->dec_status== DECODER_STATUS_IDLE)
		{
			cJSON_AddStringToObject(state,"CODEC_STATE","IDLE");
		}
		else if(monitor_ins_array[ins].ptr_vdec->dec_status== DECODER_STATUS_START)
		{
			cJSON_AddStringToObject(state,"CODEC_STATE","START");
		}
		else if(monitor_ins_array[ins].ptr_vdec->dec_status== DECODER_STATUS_RUN)
		{
			cJSON_AddStringToObject(state,"CODEC_STATE","RUN");
		}
		else if(monitor_ins_array[ins].ptr_vdec->dec_status== DECODER_STATUS_STOP)
		{
			cJSON_AddStringToObject(state,"CODEC_STATE","STOP");
		}
		else
		{
			cJSON_AddStringToObject(state,"CODEC_STATE","UNKOWN");
		}
		#endif
	}
	else if(monitor_ins_array[ins].state == H264_SHOW_STOP)
	{
		cJSON_AddStringToObject(state,"H264_SHOW","STOP");
	}
	else
	{
		cJSON_AddStringToObject(state,"H264_SHOW","UNKOWN");
	}
	
	
	return state;
}

