
#include "obj_ak_decoder.h"
#include "jpeg_dec.h"
#include "define_file.h"

JPEG_PLAYBACK_DAT_T JpegInfo;

int jpegState_init( void )
{
	pthread_mutex_init( &JpegInfo.jpegState_lock, 0);	
	JpegInfo.state= JPEG_STATE_IDLE;
	return 0;
}

int  jpegState_get( void )
{
	int state;
	pthread_mutex_lock( &JpegInfo.jpegState_lock );		
	state = JpegInfo.state;
	pthread_mutex_unlock( &JpegInfo.jpegState_lock );
	return state;
}

int jpegState_set( int state )
{
	pthread_mutex_lock( &JpegInfo.jpegState_lock );		
	JpegInfo.state= state;		
	pthread_mutex_unlock( &JpegInfo.jpegState_lock );		
	return 0;
}


struct ak_one_decoder *ptr_jpegdec_array[4]={0};
int jpegInit_playback( void )
{
	
	ptr_jpegdec_array[0] = ak_new_one_decode2(1,1280,720,AK_YUV420SP,AK_VO_LAYER_VIDEO_2);//AK_CODEC_MJPEG

	#if 0
	if( get_pane_type()	== 0 )
	{
		ptr_jpegdec_array[0] = ak_new_one_decode( AK_CODEC_MJPEG,1280,720,AK_YUV420SP,0,0,1013,800,AK_VO_LAYER_VIDEO_2);
	}
	else
	{
		ptr_jpegdec_array[0] = ak_new_one_decode( AK_CODEC_MJPEG,1280,720,AK_YUV420SP,0,0,800,1014,AK_VO_LAYER_VIDEO_2);
	}
	#endif
	return 0;
}

int JpegDecode(char* file_name)
{
	 int ret = -1;
	
	 /* read video file stream and send to decode, */
	 int read_len = 0;
	 unsigned char *data = NULL;
	
	  //strcpy(file_name,"/mnt/nand1-2/photo/background.jpg");
	  FILE *fp = fopen(file_name, "r+");
				  
	  ak_print_normal(MODULE_ID_VDEC, "open record file: %s \n", file_name);
	  if(fp == NULL)
	  {
		ak_print_normal(MODULE_ID_VDEC, "open record file: %s \n", JPEG_BACKGROUND);
	  	fp = fopen(JPEG_BACKGROUND, "r+");
		if(fp == NULL)
		{
			printf("open %s error!\n", fp);
			return -1;
		}
	  }

	  /* get the file size */
	  fseek(fp, 0, SEEK_END);
	  int file_size = ftell(fp);
	  if(! file_size)
	  {
			ak_print_normal(MODULE_ID_VDEC, "read record file size: %d \n", file_size);
	  		fclose(fp);
			return -1;
	  }
	  data = (unsigned char *)ak_mem_alloc(MODULE_ID_VDEC, file_size);

	  /* set file from the beginning */
	  fseek(fp, 0, SEEK_SET);

	  /* read the record file stream */
	  memset(data, 0x00, sizeof(data));
	  read_len = fread(data, sizeof(char), file_size, fp);

	  /* send the data to decode */
	  ak_print_normal(MODULE_ID_VDEC, "size is [%d] \n", read_len);
	  if(read_len > 0)
	  {
		  //printf( "-------frame[%d],len[%d]----------\n",vd_sf_len,size );
		 ak_push_frame(ptr_jpegdec_array[0],data, read_len);
		 ak_sleep_ms(10); 
	  }

	  /* inform that sending data is over */
	  //ak_vdec_end_stream(ptr_jpegdec_array[0]->dec_handle);
	
	  fclose(fp);
	  if (NULL != data)
		  ak_mem_free(data);




	return 0;
}

//-------------------------------------------------------------------------------------
// ���Žӿ�
//-------------------------------------------------------------------------------------

int API_JpegPlaybackStart( char* file_name)
{
	if(jpegState_get() == JPEG_STATE_IDLE)
	{
		if( jpegInit_playback() == 0 )
		{
			printf("------JpegInited!!!------\r\n");
			jpegState_set(JPEG_INIT);
		}
		if ( jpegState_get() == JPEG_INIT )
		{
			printf("---------JPG  PlayBack Start!--------\r\n");
			if (JpegDecode(file_name) == 0)
			{
				jpegState_set(PLAYBACK_RUN);
				printf("---------JPG  PlayBack Success!--------\r\n");
				return 0;
			}
			else
				return -1;
		}

	}

	else if(jpegState_get() == PLAYBACK_RUN)
	{
		if (JpegDecode(file_name) == 0)
		{
			jpegState_set(PLAYBACK_RUN);
			printf("---------JPG  PlayBack Success222!--------\r\n");
		}
	}
}


int API_JpegPlaybackClose( void )
{
	if ( jpegState_get() == PLAYBACK_RUN )
	{
		ak_del_one_decode(ptr_jpegdec_array[0]);
		
	}
	jpegState_set(JPEG_STATE_IDLE);
}




