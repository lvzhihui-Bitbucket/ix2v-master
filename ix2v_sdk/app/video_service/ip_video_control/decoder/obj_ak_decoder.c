
#include <stdio.h>
#include <ctype.h>  
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>  
#include <unistd.h>

#include "obj_ak_decoder.h"
#include "elog_forcall.h"
#include "cJSON.h"
#include "lvgl.h"
#include "gui_lib.h"

pthread_mutex_t decoder_com_lock = PTHREAD_MUTEX_INITIALIZER;

struct ak_layer_pos layer_pos[8]={0};	//store the pos
int 				layerShow[8]={0};	//display enable

#if defined( LVGL_SUPPORT_TDE )	// TDE_VIDEO_COMBINE
static struct ak_lvgl_video_win	lvgl_vd_win[8]=
{
	{
		.win_en = 0,
		.parent = NULL,
		.video_win = NULL,
		.video_dma_buf = NULL,
		.en_lock = PTHREAD_MUTEX_INITIALIZER,
	},
	{
		.win_en = 0,
		.parent = NULL,
		.video_win = NULL,
		.video_dma_buf = NULL,
		.en_lock = PTHREAD_MUTEX_INITIALIZER,
	},
	{
		.win_en = 0,
		.parent = NULL,
		.video_win = NULL,
		.video_dma_buf = NULL,
		.en_lock = PTHREAD_MUTEX_INITIALIZER,
	},
	{
		.win_en = 0,
		.parent = NULL,
		.video_win = NULL,
		.video_dma_buf = NULL,
		.en_lock = PTHREAD_MUTEX_INITIALIZER,
	},
	{
		.win_en = 0,
		.parent = NULL,
		.video_win = NULL,
		.video_dma_buf = NULL,
		.en_lock = PTHREAD_MUTEX_INITIALIZER,
	},
	{
		.win_en = 0,
		.parent = NULL,
		.video_win = NULL,
		.video_dma_buf = NULL,
		.en_lock = PTHREAD_MUTEX_INITIALIZER,
	},
	{
		.win_en = 0,
		.parent = NULL,
		.video_win = NULL,
		.video_dma_buf = NULL,
		.en_lock = PTHREAD_MUTEX_INITIALIZER,
	},
	{
		.win_en = 0,
		.parent = NULL,
		.video_win = NULL,
		.video_dma_buf = NULL,
		.en_lock = PTHREAD_MUTEX_INITIALIZER,
	},
};

static inline void set_tde_layer( struct ak_tde_layer *p_tde_layer, unsigned short lw, unsigned short lh,
				                  unsigned short x, unsigned short y, unsigned short w, unsigned short h,
				                  unsigned int p, enum ak_gp_format f ){
	if(p_tde_layer != NULL)
	{
		p_tde_layer->width 			= lw;
		p_tde_layer->height 		= lh;
		p_tde_layer->pos_left 		= x;
		p_tde_layer->pos_top 		= y;
		p_tde_layer->pos_width 		= w;
		p_tde_layer->pos_height 	= h;
		p_tde_layer->phyaddr 		= p;
		p_tde_layer->format_param 	= f;
	}
	return;
}

int set_lvgl_video_parent( int index, lv_obj_t* parent, int foreground )
{
	lvgl_vd_win[index].parent = parent;
	lvgl_vd_win[index].foreground = foreground;
	return 0;
}

int init_one_lvgl_video_win( int index, int x, int y, int w, int h )
{
	printf("22222222%s:%d\n",__func__,__LINE__);
	pthread_mutex_lock(&lvgl_vd_win[index].en_lock);
	if( lvgl_vd_win[index].win_en )
	{
		printf("22222222%s:%d\n",__func__,__LINE__);
		pthread_mutex_unlock(&lvgl_vd_win[index].en_lock);
		return -1;
	}
	lvgl_vd_win[index].win_en = 1;
	pthread_mutex_unlock(&lvgl_vd_win[index].en_lock);
	printf("22222222%s:%d\n",__func__,__LINE__);
	lvgl_vd_win[index].win_id = index;	
	vtk_lvgl_lock();
    // create disp win
    printf("22222222%s:%d\n",__func__,__LINE__);
	lvgl_vd_win[index].video_win = lv_img_create(lvgl_vd_win[index].parent);
	lv_obj_set_size(lvgl_vd_win[index].video_win, w, h);
	lv_obj_set_pos(lvgl_vd_win[index].video_win, x, y);
	lv_obj_set_style_bg_opa(lvgl_vd_win[index].video_win, LV_OPA_0, 0);
	lv_obj_set_style_bg_color(lvgl_vd_win[index].video_win,lv_color_hex(0),0);
	if( lvgl_vd_win[index].foreground )
		lv_obj_move_foreground(lvgl_vd_win[index].video_win);
	else
		lv_obj_move_background(lvgl_vd_win[index].video_win);
	printf("22222222%s:%d\n",__func__,__LINE__);
	// create disp buf
	#if !defined(PID_IX850)
	if( lvgl_vd_win[index].video_dma_buf != NULL )	ak_mem_dma_free(lvgl_vd_win[index].video_dma_buf);
	lvgl_vd_win[index].video_dma_buf = ak_mem_dma_alloc(1, w * h * sizeof(lv_color_t));
	#else
	if( lvgl_vd_win[index].video_dma_buf == NULL )
	{
		lvgl_vd_win[index].video_dma_buf = ak_mem_dma_alloc(MODULE_ID_VO, w * h * sizeof(lv_color_t));
		ak_mem_dma_vaddr2paddr((void*)lvgl_vd_win[index].video_dma_buf , ( unsigned long * )&lvgl_vd_win[index].video_dma_buf_paddr);
		printf("22222222%s:%d:%08x\n",__func__,__LINE__,lvgl_vd_win[index].video_dma_buf);
	}
	#endif
	ak_mem_dma_vaddr2paddr((void*)lvgl_vd_win[index].video_dma_buf , ( unsigned long * )&lvgl_vd_win[index].video_dma_buf_paddr);
	lvgl_vd_win[index].img_lv_video.header.always_zero = 0;
	lvgl_vd_win[index].img_lv_video.header.w  = w;
	lvgl_vd_win[index].img_lv_video.header.h  = h;
	lvgl_vd_win[index].img_lv_video.data_size = w * h * LV_COLOR_SIZE / 8;
	lvgl_vd_win[index].img_lv_video.header.cf = LV_IMG_CF_TRUE_COLOR;
	lvgl_vd_win[index].img_lv_video.data = (const unsigned char*)lvgl_vd_win[index].video_dma_buf ;
	lv_img_set_src(lvgl_vd_win[index].video_win, &lvgl_vd_win[index].img_lv_video);
	printf("22222222%s:%d\n",__func__,__LINE__);
	vtk_lvgl_unlock();
	printf("22222222%s:%d\n",__func__,__LINE__);
	printf("init_one_lvgl_video_win ok\n");
	return 0;
}

int deinit_one_lvgl_video_win( int index )
{
	pthread_mutex_lock(&lvgl_vd_win[index].en_lock);
	if( !lvgl_vd_win[index].win_en )
	{
		pthread_mutex_unlock(&lvgl_vd_win[index].en_lock);
		return -1;
	}
	lvgl_vd_win[index].win_en = 0;
	pthread_mutex_unlock(&lvgl_vd_win[index].en_lock);

	vtk_lvgl_lock();
    // destory disp buf
    #if !defined(PID_IX850)
	if( lvgl_vd_win[index].video_dma_buf != NULL )	ak_mem_dma_free(lvgl_vd_win[index].video_dma_buf);
	lvgl_vd_win[index].video_dma_buf = NULL;
#endif
    // destory disp win
    //lv_img_destructor(NULL,lvgl_vd_win[index].video_win);
    #if !defined(PID_IX850)
	lv_obj_del(lvgl_vd_win[index].video_win);
	#endif
	vtk_lvgl_unlock();
	printf("deinit_one_lvgl_video_win ok\n");
	return 0;
}

int	show_one_lvgl_frame(struct ak_lvgl_video_win *lvgl_win,struct ak_vdec_frame *frame)
{
	pthread_mutex_lock(&lvgl_win->en_lock);
	if( lvgl_win == NULL || lvgl_win->win_en == 0 ) 
	{
		pthread_mutex_unlock(&lvgl_win->en_lock);
		return -1;
	}
	pthread_mutex_unlock(&lvgl_win->en_lock);

	//printf("show_one_lvgl_frame,id=%d\n",frame->id);
	vtk_lvgl_lock();
	if(lvgl_win->parent)
	{
		int width = lvgl_win->img_lv_video.header.w;
		int height = lvgl_win->img_lv_video.header.h;
	    struct ak_tde_layer tde_layer_src;
	    struct ak_tde_layer tde_layer_video;
	    ak_mem_dma_vaddr2paddr( (void*)frame->yuv_data.data, ( unsigned long * )&tde_layer_src.phyaddr );
		set_tde_layer( &tde_layer_src, frame->yuv_data.pitch_width, frame->yuv_data.pitch_height, 0, 0, frame->width, frame->height, tde_layer_src.phyaddr, GP_FORMAT_YUV420SP );
		set_tde_layer( &tde_layer_video, width, height, 0, 0,  width, height, lvgl_win->video_dma_buf_paddr, GP_FORMAT_RGB565 );
		ak_tde_opt_scale( &tde_layer_src, &tde_layer_video );
		lv_obj_invalidate(lvgl_win->parent);
	}
	vtk_lvgl_unlock();
	return 0;
}

int clear_one_lvgl_win(int index)
{
	printf("22222222%s:%d:%d:%d\n",__func__,__LINE__,lvgl_vd_win[index].video_win,lvgl_vd_win[index].win_en);
	vtk_lvgl_lock();
	printf("22222222%s:%d\n",__func__,__LINE__);
	if( lvgl_vd_win[index].video_dma_buf == NULL ) return -1;
	memset(lvgl_vd_win[index].video_dma_buf,0,lvgl_vd_win[index].img_lv_video.data_size);
	lv_obj_invalidate(lvgl_vd_win[index].video_win);
	printf("22222222%s:%d\n",__func__,__LINE__);
	vtk_lvgl_unlock();
	printf("22222222%s:%d\n",__func__,__LINE__);
}

int close_one_lvgl_video_win( int index )
{
	if( lvgl_vd_win[index].video_dma_buf != NULL )	ak_mem_dma_free(lvgl_vd_win[index].video_dma_buf);
	lvgl_vd_win[index].video_dma_buf = NULL;
	lvgl_vd_win[index].video_win	 = NULL;
}

int decoder_match_lvgl_video_win( struct ak_one_decoder* pdec, int index )
{
	pdec->pvd_win = &lvgl_vd_win[index];
	return 0;
}
#endif	

#if 0
struct ak_tde_cmd tde_cmd_param;
struct ak_tde_layer tde_layer_src = { 640 , 480 , 0 , 0 , 640 , 480 , 0 , GP_FORMAT_TILED32X4 };
struct ak_tde_layer tde_layer_tgt = { 640 , 480 , 0 , 0 , 640 , 480 , 0 , GP_FORMAT_YUV420SP } ;


char SrcDat[1382400+100];
char TarDat[1382400+100];

int Convert_Format(char* psrc, int len )
{
	memset( &tde_cmd_param , 0 , sizeof( struct ak_tde_cmd ) );	
	tde_cmd_param.opt = GP_OPT_SCALE;

    ak_mem_dma_vaddr2paddr( SrcDat , ( unsigned long * )&tde_layer_src.phyaddr );
	memcpy(SrcDat,psrc,len);	
    ak_mem_dma_vaddr2paddr( TarDat , ( unsigned long * )&tde_layer_tgt.phyaddr );	
	
    tde_cmd_param.tde_layer_src = tde_layer_src;
    tde_cmd_param.tde_layer_dst = tde_layer_tgt;	
	
    if( ak_tde_opt( &tde_cmd_param ) == 0 )
	{
		printf("===========================ok===============\n");
	}
	else
	{
		printf("===========================er===============\n");		
	}
}

#endif



static int set_decoder_status(struct ak_one_decoder *pdecoder, int status )
{
	//ak_thread_mutex_lock( &pdecoder->dec_mutex );
	pdecoder->dec_status = status;
	log_e( "set_decoder_status = %d\n", status) ;		
	//ak_thread_mutex_unlock( &pdecoder->dec_mutex );
	return 0;
}

static int get_decoder_status(struct ak_one_decoder *pdecoder )
{
	int status;
	//ak_thread_mutex_lock( &pdecoder->dec_mutex );
	status = pdecoder->dec_status;
	//ak_thread_mutex_unlock( &pdecoder->dec_mutex );
	log_e( "get_decoder_status = %d\n", status) ;		
	return status;
}

static int frame_cnt = 0;
static int frame_filter = 0;
static void *vdec_recv_frame(void *arg)
{
	struct ak_one_decoder *pdecoder = (struct ak_one_decoder*)arg;
    int ret = -1;
    int status = 0;

	set_decoder_status( pdecoder, DECODER_STATUS_RUN );
	printf("decoder[%d] status is DECODER_STATUS_RUN!!!\n",pdecoder->dec_handle);			
	//usleep(10*1000);
	//rtp_process_set_high_prio();
    /* a loop for getting the frame and display */
    do{
		#if 1
		
		//pthread_mutex_lock( &pdecoder->frame_lock);
		//if(pdecoder->frame_cnt<2)
		ret = ak_vdec_get_frame(pdecoder->dec_handle, &pdecoder->frame);
        if(ret == 0)
        {	
        	if(pdecoder->frame_cnt<2)
        	{
        	}
        	pdecoder->frame_cnt++;
			//printf("vdec_recv_frame cnt=%d\n", frame_cnt);
        	//if( (++frame_cnt&1) == 0 )
			//if( pdecoder->frame_cnt <=2 )
			if( (pdecoder->frame_cnt&1) == 1 )
				show_one_frame2(&pdecoder->obj,&pdecoder->frame,pdecoder);
			if(pdecoder->frame_cnt<2)
				printf("555555555%s:%d\n",__func__,__LINE__);
			//API_JpegCaptureing(pdecoder->frame.yuv_data.data, 1382400);//frame.tileyuv_data
			ak_vdec_release_frame(pdecoder->dec_handle, &pdecoder->frame);
			if(pdecoder->frame_cnt<2)
				printf("555555555%s:%d\n",__func__,__LINE__);
			// jpeg_capture_process
			//pthread_mutex_unlock( &pdecoder->frame_lock);
			//ak_sleep_ms(1);
			//usleep(3*1000);
        }
        else
        {
			/* get frame failed , sleep 10ms before next cycle*/
			//pthread_mutex_unlock( &pdecoder->frame_lock);
			//ak_sleep_ms(10);
			usleep(1000);
        }
		#endif
        /* check the status finished */
        ak_vdec_get_decode_finish(pdecoder->dec_handle, &status);
        /* true means finished */
        if (status)
        {
			log_e("decoder[%d] status is DECODER_STATUS_STOPing !!!\n",pdecoder->dec_handle);			
			set_decoder_status( pdecoder, DECODER_STATUS_STOP );
			break;
        }
    }while(1);
}

void decode_stream(struct ak_one_decoder *pOneDecoder, unsigned char *data, int read_len)
{
    int send_len = 0;
    int dec_len = 0;
    int ret = 0;

	//frame_cnt++;
	//printf("decode_stream cnt=%d\n", frame_cnt);
	
    /* cycle to send the data to decode */
    while (read_len > 0)
    {
        /* send stream to decode */
		//printf("11111111111decode_stream len[%d] handle_id[%d]\n",read_len, pOneDecoder->dec_handle);		
       	ret = ak_vdec_send_stream(pOneDecoder->dec_handle, &data[send_len], read_len, 1, &dec_len);
		//printf("2222222222222222222decode_stream ret[%d]\n",ret);
        if (ret !=  0)
        {
           log_e( "write video data failed!\n");
            break;
        }
        /* record the length to send to decoder */
        read_len -= dec_len;
        send_len += dec_len;
    }
}

int ak_del_one_decode(struct ak_one_decoder *pOneDecoder)
{
	frame_cnt = 0;
	frame_filter = 0;
	if( pOneDecoder != NULL )
	{
		pthread_mutex_lock(&decoder_com_lock);
		#if 0
		if( get_decoder_status(pOneDecoder) != DECODER_STATUS_RUN )
		{
			pthread_mutex_unlock(&decoder_com_lock);
			log_e("get_decoder_status != DECODER_STATUS_RUN!!!\n");			
			//ak_print_normal(MODULE_ID_VDEC,"ak_decoder[%d] is starting, please waiti...\n",pOneDecoder->dec_handle);
			return 1;
		}
		#endif

		//pthread_mutex_lock(&decoder_com_lock);
		int dec_id = pOneDecoder->dec_handle;
		log_e("ak_del_one_decode[%d] start1!!!\n",dec_id);

		//pthread_mutex_lock( &pOneDecoder->frame_lock);
		ak_vdec_clear_buff(pOneDecoder->dec_handle);
		usleep(10*1000);
		ak_vdec_end_stream(pOneDecoder->dec_handle);	
		//pthread_mutex_unlock( &pOneDecoder->frame_lock);
		log_e("ak_del_one_decode[%d] start2!!!\n",dec_id);
		int cnt=0;
		while(cnt++<100)
		{
			if( get_decoder_status(pOneDecoder) == DECODER_STATUS_STOP )
			{
				log_e("decoder[%d] status is DECODER_STATUS_STOPed !!!\n",pOneDecoder->dec_handle);			
				break;
			}
			usleep(100*1000);
			ak_vdec_end_stream(pOneDecoder->dec_handle);	
		}
		log_e("ak_del_one_decode[%d] start3!!!\n",dec_id);
		if(cnt>=100)
		{
			pthread_detach(pOneDecoder->recv_frame_id);
			pthread_cancel(pOneDecoder->recv_frame_id);
		}
		else
			pthread_join( pOneDecoder->recv_frame_id,NULL);

		set_decoder_status( pOneDecoder, DECODER_STATUS_IDLE );
		log_e("decoder[%d] status is DECODER_STATUS_IDLE ,frame cnt=%d!!!\n",pOneDecoder->dec_handle,pOneDecoder->frame_cnt);			
		//hong long time?
		ak_vdec_clear_buff(pOneDecoder->dec_handle);
		ak_vdec_close(pOneDecoder->dec_handle);
		log_e("ak_vdec_close ok!!!\n");		

		layerShow[pOneDecoder->dst_layer - AK_VO_LAYER_VIDEO_1] = 0;
		//sleep(1);
		#if defined( LVGL_SUPPORT_TDE )
		log_e("destory lvgl win!!!,pid=[%d]\n", pthread_self());
		deinit_one_lvgl_video_win(pOneDecoder->dst_layer-AK_VO_LAYER_VIDEO_1);
		#endif
		#if !defined( LVGL_SUPPORT_TDE )
			ak_vo_destroy_layer(pOneDecoder->dst_layer);
		#else
			pOneDecoder->pvd_win = NULL;
		#endif

		log_e("ak_vo_destroy_layer ok!!!\n");
		pthread_mutex_destroy( &pOneDecoder->frame_lock);
		pthread_mutex_unlock(&decoder_com_lock);
		if( pOneDecoder != NULL )
		{
			free(pOneDecoder);
			pOneDecoder = NULL;
		}

		log_e("ak_del_one_decode[%d] over!!!\n",dec_id);
		
		return 0;
	}
	else
		return -1;
}

//#define STORE_IN_FILE
#ifdef STORE_IN_FILE
#include <fcntl.h>
#define STORE_FILE_NAME		"h265rawdata.dat"
static int store_file_packegs = 0;
static int storefp;
int store_in_file(unsigned char *pdat, int len)
{
	if( store_file_packegs == -1 )
		return -1;
	
	if( store_file_packegs == 0 )
	{
		storefp = open(STORE_FILE_NAME,O_CREAT|O_WRONLY|O_TRUNC);		
		log_e("store_in_file[%s] starting!!!!\n",STORE_FILE_NAME);
	}
	if( storefp )
	{
		write(storefp,pdat,len);
	}
	store_file_packegs++;
	if( store_file_packegs >= 300 )
	{		
		if( storefp != NULL )
		{
			close(storefp);
			log_e("store_in_file[%s] completed!!!!\n",STORE_FILE_NAME);
			store_file_packegs = -1;
		}
	}
	return 0;
}
#endif
int ak_push_frame(struct ak_one_decoder *pOneDecoder, unsigned char *pdat, int len )
{
	if( pOneDecoder == NULL )
		return -1;
	
	//ak_thread_mutex_lock( &pOneDecoder->frame_mutex );
	//pthread_mutex_lock( &pOneDecoder->frame_lock);
#ifndef 	STORE_IN_FILE
	//printf("333333333333333ak_push_frame,%d\n",pOneDecoder->frame_cnt);
	
	//if(pOneDecoder->frame_cnt<=8)
	{
		//pOneDecoder->frame_cnt++;
		decode_stream( pOneDecoder, pdat, len );	
	}
	//else
	{
		//log_w("vdec queue full");
	}
#else
	if( store_in_file(pdat,len) == -1 )
	{
		decode_stream( pOneDecoder, pdat, len );		
	}
#endif
	//ak_thread_mutex_unlock( &pOneDecoder->frame_mutex );
	//pthread_mutex_unlock( &pOneDecoder->frame_lock);
	return 0;
}


int Set_ds_show_pos(int ins_num, int x, int y, int width, int height)
{
	return SetLayerPos(ins_num + AK_VO_LAYER_VIDEO_1, x, y, width, height);
}

int Clear_ds_show_layer(int ins_num)
{
	return close_one_layer(ins_num + AK_VO_LAYER_VIDEO_1);
}

int close_one_layer(int layer)
{
	int id = layer-AK_VO_LAYER_VIDEO_1;
	layerShow[id] = 0;
	ak_vo_destroy_layer(layer);
	log_e("close_one_layer layer=%d id=%d \n",layer, id);
	return 0;
}

int SetLayerPos(int dst_layer, int dst_x, int dst_y, int dst_w, int dst_h)
{
	if( (dst_x|dst_y|dst_w|dst_h) == 0 )
		return -1;
	
	int num = dst_layer - AK_VO_LAYER_VIDEO_1;

	log_e("---SetLayerPos layer=%d posx=%d posy=%d width=%d height=%d---\n", dst_layer,dst_x,dst_y,dst_w,dst_h);
	printf("22222222%s:%d:%d\n",__func__,__LINE__,layerShow[num] );
	if( layerShow[num] )
	{
		printf("22222222%s:%d:%d\n",__func__,__LINE__,layerShow[num] );
		layerShow[num] = 0;
		clear_one_layer(num,layer_pos[num].width,layer_pos[num].height);
		printf("22222222%s:%d:%d\n",__func__,__LINE__,layerShow[num] );
		close_one_layer(dst_layer);
		printf("22222222%s:%d:%d\n",__func__,__LINE__,layerShow[num] );
	}
	//printf("22222222%s:%d:%d\n",__func__,__LINE__,layerShow[num] );
	#if !defined( LVGL_SUPPORT_TDE )
	/* create the video layer */
	struct ak_vo_layer_in video_layer;
	video_layer.create_layer.left  	= dst_x; 		//layer pos 
	video_layer.create_layer.top   	= dst_y; 		//layer pos
	video_layer.create_layer.width  = dst_w; 		//layer size
	video_layer.create_layer.height = dst_h; 		//layer size
	video_layer.layer_opt          	= 0;			//opt
	video_layer.format				= GP_OUTPUT_RGBXXX;
	if( ak_vo_create_video_layer(&video_layer, dst_layer) )
	{
		ak_vo_destroy_layer(dst_layer);
		if( ak_vo_create_video_layer(&video_layer, dst_layer) )
		{
			ak_print_error_ex(MODULE_ID_VDEC, "ak_vo_create_video_layer failed!\n");
			return -1;
		}
	}
	#else
	printf("22222222%s:%d:%d\n",__func__,__LINE__,layerShow[num] );
	init_one_lvgl_video_win(num,dst_x,dst_y,dst_w,dst_h);
	printf("22222222%s:%d:%d\n",__func__,__LINE__,layerShow[num] );
	#endif
    layer_pos[num].left 	= dst_x;
	layer_pos[num].top 		= dst_y;
    layer_pos[num].width 	= dst_w;
    layer_pos[num].height 	= dst_h;
	layerShow[num] 			= dst_layer;
	return 0;
}

int get_ds_show_layer(int index)
{
	return layerShow[index];
}
void set_ds_show_layer(int index, int layer)
{
	layerShow[index] = layer;
}
void set_default_show_layer(int index)
{
	layerShow[index] = index + AK_VO_LAYER_VIDEO_1;
}
void set_ds_layer_hidden(int index)
{
	layerShow[index] = 0;
}
void get_layer_pos(int index,int *width,int *height)
{
	*width=layer_pos[index].width;
	*height=layer_pos[index].height;
}
struct ak_one_decoder* ak_new_one_decode2( int in_type, int src_w, int src_h, int out_type, int dst_layer )
{
	struct ak_one_decoder *pOneDecoder;
	pthread_mutex_lock(&decoder_com_lock);
	log_e("ak_new_one_decode start!!!\n");

	pOneDecoder = (struct ak_one_decoder *)malloc(sizeof(struct ak_one_decoder));
	memset(pOneDecoder,0,sizeof(struct ak_one_decoder));
	pOneDecoder->dec_handle=-1;
	pOneDecoder->in_type	= in_type;
	pOneDecoder->out_type	= out_type;
	pOneDecoder->src_width	= src_w;
	pOneDecoder->src_height	= src_h;
	pOneDecoder->dst_layer	= dst_layer;
	pOneDecoder->frame_cnt =0;



	//ak_thread_mutex_init(&pOneDecoder->dec_mutex,0);	// must initial 0
	
	struct ak_vdec_param param = {0};
	param.vdec_type 		= pOneDecoder->in_type;
	param.sc_height 		= pOneDecoder->src_height;
	param.sc_width 			= pOneDecoder->src_width;
	param.output_type 		= pOneDecoder->out_type;	
	param.frame_buf_num		= 0;		// ??b????????6
	param.stream_buf_size 	= 0;		// 0 default 2M
	/* open the vdec */
	log_e("---ak_vdec_open: vd_width[%d]  vd_height[%d] vd_type[%d]---\n",param.sc_width, param.sc_height, param.vdec_type);
	printf("555555555%s:%d:%08x\n",__func__,__LINE__);
	if( ak_vdec_open(&param, &pOneDecoder->dec_handle) != 0 )
	{
		//ak_print_error_ex(MODULE_ID_VDEC, "ak_vdec_open failed!\n");
		printf("ak_vdec_open failed!\n");
		#if !defined( LVGL_SUPPORT_TDE )
		ak_vo_destroy_layer(pOneDecoder->dst_layer);
		#endif
		if( pOneDecoder )
		{
			free(pOneDecoder);
			pOneDecoder = NULL;
		}
		pthread_mutex_unlock(&decoder_com_lock);
		return NULL;
	}
	printf("555555555%s:%d:%08x,%d\n",__func__,__LINE__,dst_layer-AK_VO_LAYER_VIDEO_1);
	//ak_mutexattr_t attr;
	//ak_thread_mutex_init(&pOneDecoder->frame_mutex,&attr);
	pthread_mutex_init( &pOneDecoder->frame_lock, 0);
	set_decoder_status( pOneDecoder, DECODER_STATUS_START ); 
	log_e("decoder[%d] status is DECODER_STATUS_START!!!\n",pOneDecoder->dec_handle);
	#if defined( LVGL_SUPPORT_TDE )	// TDE_VIDEO_COMBINE
	int x,y,w,h;
	decoder_match_lvgl_video_win(pOneDecoder,dst_layer-AK_VO_LAYER_VIDEO_1);
	ak_vdec_clear_buff(pOneDecoder->dec_handle);
	#endif	
	pthread_attr_init( &pOneDecoder->pattr);
	pthread_create(&pOneDecoder->recv_frame_id,&pOneDecoder->pattr,(void*)vdec_recv_frame, (void*)pOneDecoder );
	/* get frame thread */
	//pthread_create(&pOneDecoder->recv_frame_id, vdec_recv_frame, (void*)pOneDecoder, ANYKA_THREAD_MIN_STACK_SIZE, -1);

	log_e("ak_new_one_decode[%d] ok!!!\n",pOneDecoder->dec_handle);
	pthread_mutex_unlock(&decoder_com_lock);
	return pOneDecoder;
}

int show_one_frame2(struct ak_vo_obj *obj, struct ak_vdec_frame *frame, struct ak_one_decoder *pdecoder)
{
	int layer = pdecoder->dst_layer;
   /* set the pos in the screen */
	int num = layer - AK_VO_LAYER_VIDEO_1;
	//printf("---show_one_frame2 layer=%d layerShow[%d]=%d\n", layer, num, layerShow[num]);
	if(layerShow[num]  == 0)
		return;
#if !defined( LVGL_SUPPORT_TDE )	// TDE_VIDEO_COMBINE
	int dst_layer   = layerShow[num];
	num = dst_layer - AK_VO_LAYER_VIDEO_1;
    int width		= layer_pos[num].width;
    int height		= layer_pos[num].height;
	//printf("---show_one_frame2 layer=%d x=%d y=%d width=%d height=%d\n", layer, 0, 0, width, height);
	/* obj to add to layer */
    memset(obj, 0x00, sizeof(struct ak_vo_obj));
    if (frame->data_type == AK_TILE_YUV)
    {
        /* if the data type is tileyuv */
        obj->format 		 	= GP_FORMAT_TILED32X4;
        obj->vo_tileyuv.data 	= frame->tileyuv_data;
    }
    else
    {
        /* if the data type is yuv420sp */
        obj->format 			= GP_FORMAT_YUV420SP;
        obj->cmd 				= GP_OPT_SCALE; //|GP_OPT_COLORKEY;     /* scale to screen 	*/
		//obj->colorkey.coloract 		= COLOR_DELETE; //COLOR_KEEP;		/* keep the color */
		//obj->colorkey.color_min		= 0; //0xfffff8;			/* min value */
		//obj->colorkey.color_max		= 0; //0xfffff8;			/* max value */
		
        obj->vo_layer.width 	= frame->yuv_data.pitch_width;  /* the real width 	*/
        obj->vo_layer.height 	= frame->yuv_data.pitch_height; /* the real height 	*/

        /* pos and range from the src */
        obj->vo_layer.clip_pos.top 		= 0;
        obj->vo_layer.clip_pos.left 	= 0;
        obj->vo_layer.clip_pos.width 	= frame->width;
        obj->vo_layer.clip_pos.height 	= frame->height;

        ak_mem_dma_vaddr2paddr(frame->yuv_data.data, &(obj->vo_layer.dma_addr));
		
		//printf("show_one_frame2 %d*%d %d*%d\n",frame->yuv_data.pitch_width,frame->yuv_data.pitch_height,frame->width,frame->height);
	}
	
    /* pos and range for the dst layer to contain the src */
    obj->dst_layer.top 		= 0;
    obj->dst_layer.left 	= 0;	
    obj->dst_layer.width 	= width;
    obj->dst_layer.height 	= height;
    ak_vo_add_obj(obj, dst_layer);
	ak_vo_refresh_cmd_set(AK_VO_REFRESH_VIDEO_GROUP&(1<<dst_layer));	
#else
	show_one_lvgl_frame(pdecoder->pvd_win,frame);
#endif	
    return 0;
}

////////////////////////////////////////////////////////////////////////////////

int clear_one_layer(int layer, int width, int height)
{
#if !defined( LVGL_SUPPORT_TDE )	// TDE_VIDEO_COMBINE

	struct ak_vo_obj obj;
	int id = layer+AK_VO_LAYER_VIDEO_1;
	unsigned char *data;
	
	printf("clear_one_layer id=%d\n", id);

	data = malloc(width*height*2);
    memset(data, 0, width*height*2);

    memset(&obj, 0x00, sizeof(struct ak_vo_obj));

	obj.colorkey.color_min		= 0; //0xfffff8;			/* min value */
	obj.colorkey.color_max		= 0xffffff; //0xfffff8;			/* max value */ 		
	obj.format 					= GP_OUTPUT_RGBXXX;   //GP_FORMAT_YUV420SP;
	obj.cmd						= GP_OPT_BLIT; //|GP_OPT_COLORKEY; 	/* scale to screen	*/
	obj.colorkey.coloract		= COLOR_KEEP; //COLOR_DELETE; //COLOR_KEEP;		/* keep the color */
	
	obj.vo_layer.width 	= width;	/* the real width	*/
	obj.vo_layer.height	= height; /* the real height	*/
	
	/* pos and range from the src */
	obj.vo_layer.clip_pos.top	= 0;
	obj.vo_layer.clip_pos.left 	= 0;
	obj.vo_layer.clip_pos.width	= width;
	obj.vo_layer.clip_pos.height = height;
	ak_mem_dma_vaddr2paddr(data, &(obj.vo_layer.dma_addr));

	printf("clear_one_layer width=%d height=%d\n", width, height);
    obj.dst_layer.top 		= 0;
    obj.dst_layer.left 		= 0;
    obj.dst_layer.width 	= width;
    obj.dst_layer.height 	= height;
    ak_vo_add_obj(&obj, id);
	ak_vo_refresh_cmd_set(AK_VO_REFRESH_VIDEO_GROUP&(1<<id));
	free(data);
#else
	clear_one_lvgl_win(layer);
#endif
    return 0;
}
#if 0
static void *join_test_t(void *arg)
{
	int t=(int)arg;
	printf("11111111join_test_t:%d\n",t);
}
void join_test(void)
{
	//int arg;
	int i=0;
	ak_pthread_t	recv_frame_id[1000];
	for(i=0;i<1000;i++)
	{
		ak_thread_create(&recv_frame_id[i], join_test_t, (void*)i, ANYKA_THREAD_MIN_STACK_SIZE, -1);
		ak_thread_join(recv_frame_id[i]);
		usleep(100*1000);
	}
}
#endif

cJSON *GetLayerShowState(int ins)
{
	cJSON *state;
	state=cJSON_CreateObject();
	if(state==NULL)
		return NULL;
	cJSON_AddNumberToObject(state,"layerShow",layerShow[ins]);	
	cJSON_AddNumberToObject(state,"DispX",layer_pos[ins].left);	
	cJSON_AddNumberToObject(state,"DispY",layer_pos[ins].top);
	cJSON_AddNumberToObject(state,"DispWidth",layer_pos[ins].width);
	cJSON_AddNumberToObject(state,"DispHeight",layer_pos[ins].height);

	return state;
}


int show_one_frame_yuvfile(int w, int h, char* file)
{
#if !defined( LVGL_SUPPORT_TDE )	// TDE_VIDEO_COMBINE
	int len = (w*h*3)/2;
	void *data = ak_mem_dma_alloc(MODULE_ID_APP, len);
	FILE *fp = fopen(file, "rb");
	if(fp== NULL)
	{
		return -1;
	}
	memset(data, 0, len);
    int ret = fread(data, 1, len, fp);  

	printf("show_one_frame_yuvfile read [%d] byte w*h=%d*%d\n", ret, w, h);
    struct ak_vo_obj	obj;
	memset((char*)&obj, 0, sizeof(struct ak_vo_obj));
	obj.format 				= GP_FORMAT_YUV420P;
	//obj.format 				= GP_FORMAT_YUV420SP;
	obj.cmd 				= GP_OPT_BLIT; 
	//obj.cmd					= GP_OPT_SCALE; 	/* scale to screen	*/
	obj.vo_layer.width 		= w;  /* the real width 	*/
	obj.vo_layer.height 	= h; /* the real height 	*/

	/* pos and range from the src */
	obj.vo_layer.clip_pos.top 		= 0;
	obj.vo_layer.clip_pos.left 		= 0;
	obj.vo_layer.clip_pos.width 	= w;
	obj.vo_layer.clip_pos.height 	= h;
	ak_mem_dma_vaddr2paddr(data, &(obj.vo_layer.dma_addr));

    /* pos and range for the dst layer to contain the src */
    obj.dst_layer.top 		= 0;
    obj.dst_layer.left 		= 0;	
    obj.dst_layer.width 	= 1024;
    obj.dst_layer.height 	= 600;
    ak_vo_add_obj(&obj, AK_VO_LAYER_VIDEO_1);
	ak_vo_refresh_cmd_set(AK_VO_REFRESH_VIDEO_GROUP&(1<<AK_VO_LAYER_VIDEO_1));

	fclose(fp);
	if(data)
        ak_mem_dma_free(data);
#else

#endif
    return 0;
}

void *dest_data=NULL;
int show_one_frame_filter(struct ak_vo_obj *obj, struct ak_vdec_frame *frame, struct ak_one_decoder *pdecoder)
{
	int layer = pdecoder->dst_layer;
	int num = layer - AK_VO_LAYER_VIDEO_1;
	if(layerShow[num]  == 0)
		return;
#if !defined( LVGL_SUPPORT_TDE )	// TDE_VIDEO_COMBINE
	int dst_layer   = layerShow[num];
	num = dst_layer - AK_VO_LAYER_VIDEO_1;
    int width		= layer_pos[num].width;
    int height		= layer_pos[num].height;

	int in_width = frame->yuv_data.pitch_width;
	int in_height = frame->yuv_data.pitch_height;
	int size = in_width * in_height * 3 / 2;
	if(!frame_filter)
	{
		if(dest_data)
		{
        	ak_mem_dma_free(dest_data);
		}		 
		dest_data = ak_mem_dma_alloc(MODULE_ID_APP, size/4);

	}
	frame_filter = 1;
	 
    memset(obj, 0x00, sizeof(struct ak_vo_obj));
    if (frame->data_type == AK_TILE_YUV)
    {
        /* if the data type is tileyuv */
        obj->format 		 	= GP_FORMAT_TILED32X4;
        obj->vo_tileyuv.data 	= frame->tileyuv_data;
    }
    else
    {
		Crop(frame->yuv_data.data, in_width, in_height, dest_data, in_width/2, in_height/2, in_width/4, 0);
        /* if the data type is yuv420sp */
        obj->format 			= GP_FORMAT_YUV420SP;
        obj->cmd 				= GP_OPT_BLIT; 
	
        obj->vo_layer.width 	= in_width/2;  /* the real width 	*/
        obj->vo_layer.height 	= in_height/2; /* the real height 	*/

        /* pos and range from the src */
        obj->vo_layer.clip_pos.top 		= 0;
        obj->vo_layer.clip_pos.left 	= 0;
        obj->vo_layer.clip_pos.width 	= frame->width/2;
        obj->vo_layer.clip_pos.height 	= frame->height/2;

        ak_mem_dma_vaddr2paddr(dest_data, &(obj->vo_layer.dma_addr));
		//printf("show_one_frame_filter %d*%d %d*%d\n",obj->vo_layer.width,obj->vo_layer.height,obj->vo_layer.clip_pos.width,obj->vo_layer.clip_pos.height);
		
	}
	
    obj->dst_layer.top 		= 0;
    obj->dst_layer.left 	= 0;	
    obj->dst_layer.width 	= width;
    obj->dst_layer.height 	= height;
    ak_vo_add_obj(obj, dst_layer);
	ak_vo_refresh_cmd_set(AK_VO_REFRESH_VIDEO_GROUP&(1<<dst_layer));
	
#else
	if( pdecoder->pvd_win == NULL ) return -1;
	int width = pdecoder->pvd_win->img_lv_video.header.w;
	int height = pdecoder->pvd_win->img_lv_video.header.h;
    struct ak_tde_layer tde_layer_src;
    struct ak_tde_layer tde_layer_video;
    ak_mem_dma_vaddr2paddr( (void*)frame->yuv_data.data , ( unsigned long * )&tde_layer_src.phyaddr ); 
	set_tde_layer( &tde_layer_src, frame->yuv_data.pitch_width, frame->yuv_data.pitch_height, 0, 0, frame->width, frame->height, tde_layer_src.phyaddr, GP_FORMAT_YUV420SP ); // ??????????
	set_tde_layer( &tde_layer_video, width, height, 0, 0,  width, height, pdecoder->pvd_win->video_dma_buf_paddr, GP_OUTPUT_RGBXXX ); 
	ak_tde_opt_scale( &tde_layer_src, &tde_layer_video ); 
	lv_obj_invalidate(pdecoder->pvd_win->video_win);   
#endif
    return 0;
}
