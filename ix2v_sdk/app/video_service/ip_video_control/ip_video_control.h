
#ifndef _IP_VIDEO_CONTROL_H
#define _IP_VIDEO_CONTROL_H

#include "video_subscriber_client.h"

typedef enum
{
	ip_video_idle,				// 无呼叫
	ip_video_caller,			// 主呼叫
	ip_video_becalled,			// 被呼叫
} ip_video_active;

typedef struct _ip_video_client_state_
{
	ip_video_trans_type	trans_type;
	ip_video_active		active;
	int 				server_ip;
	int 				server_dev_id;
	ip_video_trans_reso	trans_reso;
} ip_video_client_state;

/**
 * @fn:		ip_video_control_init
 *
 * @brief:	视频客户端初始化
 *
 * @param:	none
 *
 * @return: 	-1/err, 0/ok
 */
int ip_video_control_init(void);

/**
 * @fn:		api_video_client_turn_on
 *
 * @brief:	申请加入视频服务的视频组
 *
 * @param:		ins			- 申请实例
 * @param:		trans_type	- 传输类型
 * @param:  	server_ip	- 服务器IP地址
 * @param:  	dev_id		- 服务器端设备id
 * @param:  	period		- 服务时间
 * @param:  	trans_reso	- 传输分辨率
 *
 * @return: 	-1/err, 0/ok
 */
int api_video_client_turn_on(int ins, ip_video_trans_type trans_type, int server_ip, int dev_id, int period ,ip_video_trans_reso trans_reso);

/**
 * @fn:		api_video_client_turn_off
 *
 * @brief:	关闭视频服务
 *
 * @param:	ins	- 申请实例
 *
 * @return: 	-1/err, 0/ok
 */
int api_video_client_turn_off(int ins);

#endif





