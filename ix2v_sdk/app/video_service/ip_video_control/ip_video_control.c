/**
 * Description: 多路UDP拉取码流客户端申请处理
 * Author: 		lvzhihui
 * Create: 		2012-01-23
 * 
 */

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>      /* basic system data types */
#include <error.h>

#include "ip_video_cs_control.h"
#include "ip_video_control.h"

ip_video_client_state	multi_ip_video_client[4];

#define VIDEO_INNERPLAY_FILENAME		"Video_InnerPlay.avi"	//czn_20170412

/**
 * @fn:		ip_video_control_init
 *
 * @brief:	视频客户端初始化
 *
 * @param:	none
 *
 * @return: 	-1/err, 0/ok
 */
int ip_video_control_init(void)
{
	int i;
	for( i = 0; i < 4; i++ )
	{
		multi_ip_video_client[i].trans_type		= ip_video_none;
		multi_ip_video_client[i].active			= ip_video_idle;
		multi_ip_video_client[i].server_ip 		= 0;
		multi_ip_video_client[i].server_dev_id 	= 0;
		multi_ip_video_client[i].trans_reso		= Resolution_480P;
	}
	
	return init_one_subscriber_client();
}

/**
 * @fn:		api_video_client_turn_on
 *
 * @brief:	申请加入视频服务的视频组
 *
 * @param:		ins			- 申请实例
 * @param:		trans_type	- 传输类型
 * @param:  	server_ip	- 服务器IP地址
 * @param:  	dev_id		- 服务器端设备id
 * @param:  	period		- 服务时间
 * @param:  	trans_reso	- 传输分辨率
 *
 * @return: 	-1/err, 0/ok
 */
int api_video_client_turn_on(int ins, ip_video_trans_type trans_type, int server_ip, int dev_id, int period, ip_video_trans_reso trans_reso)
{
	int ret = 0;
	
	vd_client_msg_type client_req_msg;
	int file_playlen;	//czn_20170412	
	
	vd_printf("api_video_client_turn_on ins = %d ip = 0x%08x, dev_id = %d, trans_reso = %d, trans_type = %d\n",ins, server_ip, dev_id, trans_reso, trans_type);

	switch( trans_type )
	{  
		case ip_video_unicast:
		case ip_video_multicast:

			// apply for multicast or unicast			
			client_req_msg = api_one_video_client_subscribe_req_ext(ins, server_ip, dev_id, period, trans_reso, trans_type);			
			if( client_req_msg == VD_CLIENT_MSG_REQ_OK )
			{
				multi_ip_video_client[ins].trans_type 		= trans_type;
				multi_ip_video_client[ins].active			= ip_video_caller;		
				multi_ip_video_client[ins].server_ip 		= server_ip;
				multi_ip_video_client[ins].server_dev_id	= dev_id;
				multi_ip_video_client[ins].trans_reso		= trans_reso;
				vd_printf("api_one_video_client_subscribe_req over,server_ip=0x%08x, trans_reso = %d!\n",server_ip, trans_reso );	
			}
			else
			{
				ret = -1;
			}
			break;
		//czn_20170412_s
		case ip_video_file_play:
			// check if multicast or not
			if( multi_ip_video_client[ins].trans_type == ip_video_multicast )
			{
					multi_ip_video_client[ins].trans_type = ip_video_none;
					api_one_video_client_desubscribe_req_ext(ins, multi_ip_video_client[ins].server_ip, multi_ip_video_client[ins].trans_reso, multi_ip_video_client[ins].trans_type );
					usleep(100000);
			}
			
			if(API_PlaybackStart(VIDEO_INNERPLAY_FILENAME,&file_playlen,NULL) == 0)
			{
				multi_ip_video_client[ins].trans_type 		= ip_video_file_play;
				multi_ip_video_client[ins].active			= ip_video_caller;		
				multi_ip_video_client[ins].server_ip 		= server_ip;
				multi_ip_video_client[ins].server_dev_id	= dev_id;
			}
			else
			{
				ret = -1;	
			}
			break;
		//czn_20170412_e
		default:
			break;
	}	
	return ret;	
}

/**
 * @fn:		api_video_client_turn_off
 *
 * @brief:	关闭视频服务
 *
 * @param:	ins	- 申请实例
 *
 * @return: 	-1/err, 0/ok
 */
int api_video_client_turn_off( int ins )
{
	switch( multi_ip_video_client[ins].trans_type )
	{
		case ip_video_unicast:
		case ip_video_multicast:
			if( api_one_video_client_desubscribe_req_ext(ins, multi_ip_video_client[ins].server_ip, multi_ip_video_client[ins].trans_reso, multi_ip_video_client[ins].trans_type) == VD_CLIENT_MSG_CLOSE_OK )
			{
				multi_ip_video_client[ins].trans_type 	= ip_video_none;
				multi_ip_video_client[ins].active		= ip_video_idle;
				printf("unsubscribe_req_to_0x%08x ok!",multi_ip_video_client[ins].server_ip);
			}
			else
			{
			#if 1
				if(multi_ip_video_client[ins].active!= ip_video_idle)
				{
					multi_ip_video_client[ins].trans_type 	= ip_video_none;
					multi_ip_video_client[ins].active		= ip_video_idle;
					//stop_receive_decode_process(ins);
				}
			#endif
				printf("unsubscribe_req_to_0x%08x er!",multi_ip_video_client[ins].server_ip);
			}
			
			vd_printf("api_one_video_client_desubscribe_req_ext over!\n" );			
			break;
		//czn_20170412_s
		case ip_video_file_play:
			API_PlaybackClose();
			multi_ip_video_client[ins].trans_type 	= ip_video_none;			
			multi_ip_video_client[ins].active		= ip_video_idle;
			break;
		//czn_20170412_e
		default:
			break;
	}
	return 0;		
}

/*******************************************************************************************
 * @fn:		api_video_client_multicast_notify_off
 *
 * @brief:	服务器通知关闭客户端组播服务
 *
 * @param:	none
 *
 * @return: 	-1/err, 0/ok
*******************************************************************************************/
int api_video_client_multicast_notify_off( int server_ip, int ins )
{
	vd_printf("one multicast apply off, ip = 0x%08x\n",server_ip );

	// 登记状态
	multi_ip_video_client[ins].trans_type	= ip_video_none;
	multi_ip_video_client[ins].active		= ip_video_idle;
	
	return 0;
}
