#include <stdio.h>
#include <sys/sysinfo.h>
#include "task_Shell.h"
#include "task_survey.h"
#include "obj_GetIpByInput.h"
#include "ip_video_cs_control.h"
#include "obj_ak_decoder.h"
#include "task_Event.h"
#include "gui_lib.h"
#include "define_file.h"
#include "obj_Talk_Servi.h"
#include "obj_VtkUnicastCommandInterface.h"
#include "obj_GetInfoByIp.h"
enum
{
	CmrDevState_Ok=0,
	CmrDevState_Err,

};
typedef struct
{
	int opentime;
	int err_cnt;
	int ip_addr;
	short port;
	cJSON *rec;
}CmrDevManager_run_stru;
static CmrDevManager_run_stru CmrDevManager_run={0};
static pthread_mutex_t CmrDevManager_lock=PTHREAD_MUTEX_INITIALIZER;
#define CmrDevManager_rec_file		"/mnt/nand1-2/UserData/CmrDevManagerRec.json"
void CmrDevManager_init(void)
{
	pthread_mutex_lock(&CmrDevManager_lock);
	if(CmrDevManager_run.rec!=NULL)
	{
		pthread_mutex_unlock(&CmrDevManager_lock);
		return;
	}
	CmrDevManager_run.opentime=0;
	CmrDevManager_run.err_cnt=0;
	CmrDevManager_run.rec=GetJsonFromFile(CmrDevManager_rec_file);

	if(CmrDevManager_run.rec==NULL)
		CmrDevManager_run.rec=cJSON_CreateArray();
	pthread_mutex_unlock(&CmrDevManager_lock);
	#if 1
	int ip_addr;
	short port;
	get_server_trans_addr_port(0,1,0,&ip_addr,&port);
	CmrDevManager_run.ip_addr=ip_addr;
	CmrDevManager_run.port=port;
	api_udp_add_one_send_node(2,1,ip_addr,port);
	#if defined(PID_IX611) //|| defined(PID_IX622)
	int try_cnt=100;
	while(try_cnt--)
	{
		usleep(100*1000);
		if(CmrDevManager_run.opentime)
		{
			if(CmrDevManager_run.err_cnt)
			{
				sleep(3);
				CmrDevManager_run.opentime=0;
				api_udp_add_one_send_node(2,1,ip_addr,port);
			}
			break;
		}
	}
	#endif
	#endif
}
void CmrDevManager_add_one_record(int state,char *remark)
{
	cJSON *one_rec;
	char buff[100];
	pthread_mutex_lock(&CmrDevManager_lock);
	CmrDevManager_run.opentime++;
	if(state==CmrDevState_Err)
	{
		CmrDevManager_run.err_cnt++;
		if(CmrDevManager_run.opentime>1)
			API_Event_NameAndMsg(Event_VideoSerError,"CMR_ERR");
	}
	else
	{
		if(get_video_reliability_cmr_err())
			API_Event_NameAndMsg(Event_VideoSerError,"CMR_OK");
	}	
	if(CmrDevManager_run.opentime==1||state==CmrDevState_Err)
	{	
		one_rec=cJSON_CreateObject();
		strcpy(buff,API_PublicInfo_Read_String(PB_DATE));
		strcat(buff," ");
		strcat(buff,API_PublicInfo_Read_String(PB_TIME));
		//sprintf(buff,"%s %s",API_PublicInfo_Read_String(PB_DATE),API_PublicInfo_Read_String(PB_TIME));
		cJSON_AddStringToObject(one_rec,"TIME",buff);
		if(state==CmrDevState_Err)
			cJSON_AddStringToObject(one_rec,"STATE","Error");
		else
			cJSON_AddStringToObject(one_rec,"STATE","Ok");

		if(remark)
			cJSON_AddStringToObject(one_rec,"REMARK",remark);
		else
			cJSON_AddStringToObject(one_rec,"REMARK","-");
		cJSON_AddItemToArray(CmrDevManager_run.rec,one_rec);
		if(cJSON_GetArraySize(CmrDevManager_run.rec)>100)
			cJSON_DeleteItemFromArray(CmrDevManager_run.rec,0);
		SetJsonToFile(CmrDevManager_rec_file,CmrDevManager_run.rec);
		#ifdef PID_IX850
		if(state==CmrDevState_Err)
		{
			sprintf(buff,"Camera Error:%s",remark);
			vtk_lvgl_lock();
			//API_TIPS(buff);
			LV_API_TIPS(buff,4);
			vtk_lvgl_unlock();
		}
		#endif
	}
	sprintf(buff,"%d/%d",CmrDevManager_run.err_cnt,CmrDevManager_run.opentime);
	API_PublicInfo_Write_String(PB_CmrDevState,buff);
	pthread_mutex_unlock(&CmrDevManager_lock);
	if(CmrDevManager_run.opentime==1)
		api_udp_del_one_send_node(2,1,CmrDevManager_run.ip_addr,CmrDevManager_run.port);
	
}