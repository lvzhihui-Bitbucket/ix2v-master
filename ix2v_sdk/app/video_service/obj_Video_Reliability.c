#include "task_survey.h"
#include "define_file.h"
#include "elog.h"
#include "task_Event.h"
#include "task_Shell.h"
#include "obj_TableSurver.h"

Loop_vdp_common_buffer	video_reliability_mesg_queue;
Loop_vdp_common_buffer	video_reliability_sync_queue;
vdp_task_t				task_video_reliability = {.task_name = "video_reliability"};
void monitor_video_subscriber_list(void);
void video_reliability_process(char* msg_data, int len);
void* video_reliability_task( void* arg );
static int video_reliability_cmr_err=0;
static int video_reliability_cmr_reset=0;
static int video_reliability_client_req_err=0;
static int video_reliability_client_recv_err=0;

void vtk_TaskInit_video_reliability(void)
{
	init_vdp_common_queue(&video_reliability_mesg_queue, 500000, (msg_process)video_reliability_process, &task_video_reliability);
	init_vdp_common_queue(&video_reliability_sync_queue, 100, NULL, 								  &task_video_reliability);
	init_vdp_common_task(&task_video_reliability, MSG_ID_video_reliability, video_reliability_task, &video_reliability_mesg_queue, &video_reliability_sync_queue);
}

void vtk_TaskExit_video_reliability(void)
{
	exit_vdp_common_queue(&video_reliability_mesg_queue);
	exit_vdp_common_queue(&video_reliability_sync_queue);
	exit_vdp_common_task(&task_video_reliability);
}

void* video_reliability_task( void* arg )
{
	vdp_task_t*	 ptask 			= (vdp_task_t*)arg;
	p_vdp_common_buffer pdb 	= 0;
	int	size;
	thread_log_add(__func__);
	while( ptask->task_run_flag )
	{
		size = pop_vdp_common_queue(ptask->p_msg_buf, &pdb, 500);
		if( size > 0 )
		{
			(*ptask->p_msg_buf->process)(pdb,size);
			purge_vdp_common_queue( ptask->p_msg_buf );
		}
		else
		{
			monitor_video_subscriber_list();
		}
	}
	return 0;
}

void video_reliability_process(char* msg_data, int len)
{
	send_udp_enc_data2(0,msg_data,len,NULL);
}
static cJSON *video_error_cache=NULL;
static pthread_mutex_t video_error_Lock = PTHREAD_MUTEX_INITIALIZER;	
//static cJSON *video_error=NULL;
void monitor_video_subscriber_list(void)
{	
	int num;
	int target_ip;
	int source_ip;
	int i,j,z;
	cJSON *rpb;
	cJSON *rpb_ip;
	cJSON *node;
	  cJSON* element;
	char ip_str[30];
	char buff[100];
	int cur_time=time(NULL);
	pthread_mutex_lock(&video_error_Lock);
	if(video_error_cache==NULL)
		video_error_cache=cJSON_CreateArray();
	//if(video_error==NULL)
	//	video_error=cJSON_CreateArray();
	
	if(get_total_subscriber_list(0,0)==0&&get_total_subscriber_list(0,0)==0)
	{
		video_reliability_cmr_reset=0;
		video_reliability_cmr_err=0;
	}
	else if(video_reliability_cmr_err&&video_reliability_cmr_reset==0)
	{
		api_cmrerror_reset1();
		video_reliability_cmr_err=0;
		video_reliability_cmr_reset=1;
		cJSON_Delete(video_error_cache);
		video_error_cache=cJSON_CreateArray();
		node=cJSON_CreateObject();
		cJSON_AddStringToObject(node,"IM_IP","-");
		cJSON_AddStringToObject(node, "TIME", GetCurrentTime(buff, 100, "%Y-%m-%d %H:%M:%S"));
		cJSON_AddNumberToObject(node,"TYPE",5);
		API_TB_AddByName(TB_NAME_VideoSerError, node);
		cJSON_Delete(node);
	}	
	for(i=0;i<2;i++)
	{
		num=get_total_subscriber_list(0,i);
		if(num)
			source_ip=inet_addr(GetSysVerInfo_IP());
		for(j=0;j<num;j++)
		{
			target_ip=get_subscriber_ip(0,i,j);
			rpb=NULL;
			for(z=0;z<4;z++)
			{
				sprintf(buff,"VC_CH%d_SerIP",z);
				rpb_ip=API_GetRemotePb(target_ip,buff);
				if(rpb_ip==NULL)
					break;
				
				if(inet_addr(rpb_ip->valuestring)==source_ip)
				{
					sprintf(buff,"VC_CH%d_Bandwind",z);
					rpb=API_GetRemotePb(target_ip,buff);
					cJSON_Delete(rpb_ip);
					break;
				}
				cJSON_Delete(rpb_ip);
			}
			if(rpb)
			{
				if(rpb->valueint<100)
				{
					//printf("1111111%s:%d\n",__func__,_LINE__);
					my_inet_ntoa(target_ip, ip_str);
					//pintf("1111111%s:%d\n",__func__,_LINE__);
					cJSON_ArrayForEach(element, video_error_cache)
					{
						
						if(strcmp(GetEventItemString(element, "IM_IP"),ip_str)==0)
						{
							if(video_reliability_cmr_reset==0&&(cur_time-GetEventItemInt(element, "time"))>2&&rpb->valueint>0&&rpb->valueint<20)
							{
								api_cmrerror_reset1();
								video_reliability_cmr_err=0;
								video_reliability_cmr_reset=1;
								cJSON_Delete(video_error_cache);
								video_error_cache=cJSON_CreateArray();
								node=cJSON_CreateObject();
								cJSON_AddStringToObject(node,"IM_IP","-");
								cJSON_AddStringToObject(node, "TIME", GetCurrentTime(buff, 100, "%Y-%m-%d %H:%M:%S"));
								cJSON_AddNumberToObject(node,"TYPE",5);
								API_TB_AddByName(TB_NAME_VideoSerError, node);
								cJSON_Delete(node);
								break;
							}
							if(GetEventItemInt(element, "SAVE")==0&&(cur_time-GetEventItemInt(element, "time"))>4)
							{
								//cJSON_DetachItemViaPointer(video_error_cache,element);
								//cJSON_Delete(element);
								
								cJSON_AddStringToObject(element, "TIME", GetCurrentTime(buff, 100, "%Y-%m-%d %H:%M:%S"));
								//cJSON_DeleteItemFromObjectCaseSensitive(element,"time");
								cJSON_AddNumberToObject(element,"SAVE",1);
								int ret=API_TB_AddByName(TB_NAME_VideoSerError, element);
								//printf("1111111%s:%d:%d\n",__func__,__LINE__,ret);
								//cJSON_Delete(element);
								//API_Ring2(target_ip, video_reliability_cmr_err?"Camera error":"Recv error", 5);
							}
							
							
							//cJSON_Delete(rpb);
							break;
						}
					}
					if(element == NULL)
					{
						node=cJSON_CreateObject();
						
						cJSON_AddStringToObject(node,"IM_IP",ip_str);
						cJSON_AddNumberToObject(node,"time",cur_time);
						cJSON_AddNumberToObject(node,"TYPE",1);
						//cJSON_AddNumberToObject(node,"SAVE",0);
						cJSON_AddItemToArray(video_error_cache,node);
					}
					
				}
				else
				{
					cJSON_ArrayForEach(element, video_error_cache)
					{
						if(strcmp(GetEventItemString(element, "IM_IP"),ip_str)==0)
						{
							cJSON_DetachItemViaPointer(video_error_cache,element);
							cJSON_Delete(element);
							break;
						}
					}
				}
				cJSON_Delete(rpb);
			}
			else 
			{
				my_inet_ntoa(target_ip, ip_str);
				if(video_reliability_cmr_err)
				{
					cJSON_ArrayForEach(element, video_error_cache)
					{
						
						if(strcmp(GetEventItemString(element, "IM_IP"),ip_str)==0)
						{
						
							break;
						}
						
					}
					if(element == NULL)
					{
						node=cJSON_CreateObject();
						
						cJSON_AddStringToObject(node,"IM_IP",ip_str);
						cJSON_AddNumberToObject(node,"time",cur_time);
						cJSON_AddNumberToObject(node,"TYPE",1);
						//cJSON_AddNumberToObject(node,"SAVE",0);
						cJSON_AddItemToArray(video_error_cache,node);
						API_Ring2(target_ip, "Camera error", 5);
					}
					
				}
			}
		}
	}
	num=cJSON_GetArraySize(video_error_cache);
	for(i=0;i<num;i++)
	{
		element=cJSON_GetArrayItem(video_error_cache,i);
		target_ip=inet_addr(GetEventItemString(element, "IM_IP"));
		if(if_subscriber_ip(0,0,target_ip)==0&&if_subscriber_ip(0,1,target_ip)==0)
		{
			cJSON_DetachItemViaPointer(video_error_cache,element);
			cJSON_Delete(element);
			i--;
			num--;
			//printf("1111111%s:%d\n",__func__,__LINE__);
			//continue;
		}
		
		
	}
	pthread_mutex_unlock(&video_error_Lock);
}

int get_video_reliability_cmr_err(void)
{
	return video_reliability_cmr_err;
}

void client_report_recv_err_process(char *client_ip_str)
{
	cJSON *node;
	 cJSON* element=NULL;
	 char buff[100];
	 int client_ip=inet_addr(client_ip_str);
	pthread_mutex_lock(&video_error_Lock);
	if(if_subscriber_ip(0,0,client_ip)==1||if_subscriber_ip(0,1,client_ip)==1)
	{
		cJSON_ArrayForEach(element, video_error_cache)
		{
			
			if(inet_addr(GetEventItemString(element, "IM_IP"))==client_ip)
			{
				#if 0
				int cur_time=time(NULL);
				if(GetEventItemInt(element, "SAVE")==0&&(cur_time-GetEventItemInt(element, "time"))>2)
				{
					//cJSON_DetachItemViaPointer(video_error_cache,element);
					//cJSON_Delete(element);
					
					cJSON_AddStringToObject(element, "TIME", GetCurrentTime(buff, 100, "%Y-%m-%d %H:%M:%S"));
					//cJSON_DeleteItemFromObjectCaseSensitive(element,"time");
					cJSON_AddNumberToObject(element,"SAVE",1);
					int ret=API_TB_AddByName(TB_NAME_VideoSerError, element);
					printf("1111111%s:%d:%d\n",__func__,__LINE__,ret);
					//cJSON_Delete(element);
					API_Ring2(target_ip, video_reliability_cmr_err?"Camera error":"Recv error", 5);
				}
				#endif
				//cJSON_Delete(rpb);
				break;
			}
		}
	}
	pthread_mutex_unlock(&video_error_Lock);
	if(element==NULL)
	{
		node=cJSON_CreateObject();
		cJSON_AddStringToObject(node,"IM_IP",client_ip_str);
		cJSON_AddStringToObject(node, "TIME", GetCurrentTime(buff, 100, "%Y-%m-%d %H:%M:%S"));
		cJSON_AddNumberToObject(node,"TYPE",4);
		API_TB_AddByName(TB_NAME_VideoSerError, node);
		cJSON_Delete(node);
		//API_Ring2(client_ip, video_reliability_cmr_err?"Camera error":"*Recv error", 5);
		
	}
}
int Event_VideoSerError_Callback(cJSON *cmd)
{
	cJSON *node;
	char buff[100];
	char *msg=GetEventItemString(cmd,EVENT_KEY_Message);
	if(strcmp(msg,"REQ_IDR")==0)
	{
		trigger_send_key_frame(0);
	}
	else if(strcmp(msg,"CMR_ERR")==0)
	{
		node=cJSON_CreateObject();
		cJSON_AddStringToObject(node,"IM_IP","-");
		cJSON_AddStringToObject(node, "TIME", GetCurrentTime(buff, 100, "%Y-%m-%d %H:%M:%S"));
		cJSON_AddNumberToObject(node,"TYPE",0);
		API_TB_AddByName(TB_NAME_VideoSerError, node);
		cJSON_Delete(node);
		pthread_mutex_lock(&video_error_Lock);
		video_reliability_cmr_err=1;
		pthread_mutex_unlock(&video_error_Lock);
	}
	else if(strcmp(msg,"CMR_OK")==0)
	{
		pthread_mutex_lock(&video_error_Lock);
		video_reliability_cmr_err=0;
		pthread_mutex_unlock(&video_error_Lock);
	}
	else if(strstr(msg,"CLIENT_REQ_ERR")!=NULL)
	{
		node=cJSON_CreateObject();
		cJSON_AddStringToObject(node,"IM_IP",msg+strlen("CLIENT_REQ_ERR:"));
		cJSON_AddStringToObject(node, "TIME", GetCurrentTime(buff, 100, "%Y-%m-%d %H:%M:%S"));
		cJSON_AddNumberToObject(node,"TYPE",2);
		API_TB_AddByName(TB_NAME_VideoSerError, node);
		cJSON_Delete(node);
		video_reliability_client_req_err=1;
		//API_Ring2(inet_addr(msg+strlen("CLIENT_REQ_ERR:")), "Req error", 5);
	}
	else if(strstr(msg,"CLIENT_RECV_ERR")!=NULL)
	{
		client_report_recv_err_process(msg+strlen("CLIENT_RECV_ERR:"));
	}
	else if(strcmp(msg,"test")==0)
	{
		api_cmrerror_reset();
	}
	else if(strcmp(msg,"test1")==0)
	{
		api_cmrerror_reset1();
	}
	return 1;
}

int	task_video_reliability_queue( char* pdata, unsigned int len )
{
	if(task_video_reliability.task_run_flag == 0)
	{
		return -1;
	}
	return push_vdp_common_queue(task_video_reliability.p_msg_buf, pdata, len);
}
static pthread_mutex_t  thread_log_lock = PTHREAD_MUTEX_INITIALIZER;
static cJSON *thread_log=NULL;
#include<stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <syscall.h>
void thread_log_init(void)
{
	#if 1
	 pthread_mutex_lock(&thread_log_lock);
	 thread_log=cJSON_CreateArray();
	 pthread_mutex_unlock(&thread_log_lock);
	 #endif
}
void thread_log_add(const char * format,...)
{
	#if 1
	char info[200] = {0};
	va_list valist;
	int ret;

    va_start(valist, format);
	ret = vsnprintf(info, 200, format, valist);
    va_end(valist);
	
	 pthread_mutex_lock(&thread_log_lock);
	 if(cJSON_GetArraySize(thread_log)<200)
	 {
		 cJSON *node=cJSON_CreateObject();
		 cJSON_AddNumberToObject(node,"pid",syscall(SYS_gettid));
		cJSON_AddStringToObject(node,"info",info);
		cJSON_AddItemToArray(thread_log,node);
		 //SetJsonToFile("/mnt/nand1-2/UserData/thread_log",thread_log);
	 }
	 pthread_mutex_unlock(&thread_log_lock);
	 #endif
}
cJSON *get_thread_log(void)
{
	return thread_log;
}