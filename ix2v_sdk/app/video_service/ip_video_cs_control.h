
#ifndef _IP_VIDEO_CS_CONTORL_H_
#define _IP_VIDEO_CS_CONTORL_H_

#include "video_subscriber.h"
#include "ip_video_control.h"
#include "ip_camera_control.h"

typedef enum
{
	VIDEO_CS_IDLE,
	VIDEO_CS_CLIENT,
	VIDEO_CS_SERVER,
	VIDEO_CS_BOTH,
} VIDEO_CS_STATE;

/*******************************************************************************************
 * @fn:		init_video_cs_service
 *
 * @brief:	初始化视频CS服务
 *
 * @param:	none
 *
 * @return: 	-1/err, 0/ok
*******************************************************************************************/
int init_video_cs_service(void);

/*******************************************************************************************
 * @fn:		api_video_c_service_turn_on
 *
 * @brief:	申请服务器端视频输出服务
 *
 * @param:  	server_ip	- 服务器端IP地址
 * @param:		trans_type	- 传输类型
 * @param:  	trans_reso	- 服务器端分辨率
 * @param:  	dev_id		- 服务器端设备id
 * @param:  	period		- 服务时间
 *
 * @return: 	-1/err, 0/ok
*******************************************************************************************/
int api_video_c_service_turn_on( int ins, int server_ip, ip_video_trans_type trans_type, ip_video_trans_reso trans_reso, int dev_id, int period );


/*******************************************************************************************
 * @fn:		api_video_c_service_turn_off
 *
 * @brief:	关闭客户器端视频输入服务
 *
 * @param:	none
 *
 * @return: 	-1/err, 0/ok
*******************************************************************************************/
int api_video_c_service_turn_off( int ins );


#endif

