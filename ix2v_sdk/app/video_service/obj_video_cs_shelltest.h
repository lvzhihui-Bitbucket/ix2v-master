#ifndef _obj_video_cs_shelltest_h
#define _obj_video_cs_shelltest_h
#define MoncDescribe		"Start/Close Monitor DS/IPC:-s/-c {\"TargetType\":\"DS/IPC\",\"TargetAddr\":\"192.168.243.222\",\"TargetNum\":\"0099000001\",\"Time\":600,\"TransType\":\"Mul/Uni\",\"DSResolution\":\"240P/480P/720P\",\"IPC_CH\":1/2/3,\"DisplayX\":0,\"DisplayY\":0,\"DisplayW\":400,\"DisplayH\":300,\"Codec_Ins\":0/1/2/3}"
#define MonsDescribe		"Mons -state 0/1/2"
int ShellCmd_Monc(cJSON *cmd);
int ShellCmd_Mons(cJSON *cmd);
#endif
