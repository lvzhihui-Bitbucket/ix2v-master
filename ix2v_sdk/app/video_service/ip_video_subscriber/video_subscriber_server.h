
#ifndef _VIDEO_MULTICAST_SERVR_H
#define _VIDEO_MULTICAST_SERVR_H

#include "video_subscriber.h"
#include "subscriber_list.h"
#include "video_source_map.h"

typedef enum
{
	VD_SERVER_IDLE,
	VD_SERVER_ACTIVE,
} vd_server_state;


typedef enum
{
	// response
	VD_SERVER_MSG_RSP_OK,
	VD_SERVER_MSG_RSP_ERR,
	VD_SERVER_MSG_RSP_NO_PERFORMAT,	
	VD_SERVER_MSG_RSP_TIMEOUT,	
	// cancel
	VD_SERVER_MSG_CANCEL_OK,
	VD_SERVER_MSG_CANCEL_ERR,
	VD_SERVER_MSG_CANCEL_NO_RSP,	
	VD_SERVER_MSG_CANCEL_TIMEOUT,		
} vd_server_msg_type;

typedef struct _video_subscriber_server_t_
{
	udp_comm_rt 			udp;					//  udp交互模板实例
	vd_server_state 		state;					//	状态机
	vd_server_msg_type		msg;					//	最新返回消息
	send_sem_id_array		waitrsp_array;			//	业务应答同步队列
	int 					send_cmd_sn;			//	发送命令包序列号
}video_subscriber_server;


/**
 * @fn:		init_one_subscriber_server
 *
 * @brief:	服务器双网卡通信实例初始化
 *
 * @param:	none
 *
 * @return: -1/err, 0/ok
 */
int init_one_subscriber_server( void );

/**
 * @fn:		api_one_video_server_cancel_req
 *
 * @brief:	通知指定客户端退出视频组
 *
 * @param:  client_ip 	- 客户端IP地址
 * @param:  trans_reso 	- 传输分辨率
 * @param:  trans_type 	- 传输类型
 *
 * @return: -1/err, 0/ok
 */
vd_server_msg_type api_one_video_server_cancel_req( int client_ip, ip_video_trans_reso trans_reso, ip_video_trans_type trans_type );

#endif


