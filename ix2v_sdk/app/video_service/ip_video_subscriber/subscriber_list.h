
#ifndef _SUBSCRIBER_LIST_H
#define _SUBSCRIBER_LIST_H

#include <pthread.h>

// 订阅状态机
typedef enum
{
	IDLE,
	ACTIVATE_SREVICE,
	IN_SERVICE,
	DEACTIVATE_SERVICE,
} subscriber_state;

typedef struct	_subscriber_data_t
{
	int				reg_ip;			// 登记的客户端IP
	struct timeval	reg_tv;			// 登记的客户端申请时间 	
	int				reg_period;		// 登记的客户端申请播放时间 (s)
	int				reg_timer;		// 登记的客户端定时器
} subscriber_data;

#define MAX_SUBUSCRIBER_LIST_COUNT		50

typedef struct	_subscriber_list_t
{	
	subscriber_data		dat[MAX_SUBUSCRIBER_LIST_COUNT];	
	int					counter;
    pthread_mutex_t 	lock;	
} subscriber_list;


/**
 * @fn:		init_subscriber_list
 *
 * @brief:	订阅列表初始化
 *
 * @param:	none
 *
 * @return: none
 */
int init_subscriber_list( void );

/**
 * @fn:		get_total_subscriber_list
 *
 * @brief:	得到指定通道订阅列表的计数
 *
 * @param:	channel	- 发送通道
 * @param:	type	- 发送类型 - 0/组播，1/点播
 *
 * @return: 返回个数
 */
int get_total_subscriber_list(int channel, int type );

/**
 * @fn:		delete_all_userlist
 *
 * @brief:	释放所有列表的订阅节点
 *
 * @param:	none
 *
 * @return: 0
 */
int delete_all_userlist(void);

/**
 * @fn:		activate_subscriber_list_ext
 *
 * @brief:	添加目标节点到指定通道
 *
 * @param:	channel		- 订阅通道
 * @param:	type		- 订阅类型
 * @param:	reg_ip		- 订阅目标ip
 * @param:	reg_period	- 订阅时长
 *
 * @return: 总数
 */
int activate_subscriber_list_ext( int channel, int type, int reg_ip, int reg_period);

/**
 * @fn:		deactivate_subscriber_list_ext
 *
 * @brief:	释放目标节点到指定通道
 *
 * @param:	channel		- 订阅通道
 * @param:	type		- 订阅类型
 * @param:	reg_ip		- 订阅目标ip
 *
 * @return: 总数
 */
int deactivate_subscriber_list_ext( int channel, int type, int reg_ip);


#endif

