

#ifndef _VIDEO_SOURCE_MAP_H
#define _VIDEO_SOURCE_MAP_H

#include "video_subscriber.h"


/**
 * @fn:		get_server_multicast_addr_port
 *
 * @brief:	旧版本：兼容ix1、ix2协议，得到组播服务的地址和端口
 *
 * @param:	mcg_addr:	为本地视频服务器的组播地址
 * @param:	pport:		为本地视频服务器的端口号
 *
 * @return: -1/err, 0/ok
 */
int get_server_multicast_addr_port( int* mcg_addr, unsigned short *pport );

/**
 * @fn:		get_server_trans_addr_port
 *
 * @brief:	新版本：ix2v协议，得到组播地址和端口、单播地址和端口
 *
 * @param:	target_ip:	请求方目标ip地址
 * @param:	trans_type:	传输类型（0-组播，1-点播）
 * @param:	trans_reso:	传输分辨率
 * @param:	ipaddr:		返回发送目标ip地址
 * @param:	port:		返回发送目标ip端口
 *
 * @return: -1/err, 0/ok
 */
int get_server_trans_addr_port( int target_ip, ip_video_trans_type trans_type, ip_video_trans_reso trans_reso, int* ipaddr, unsigned short *port );

#endif
