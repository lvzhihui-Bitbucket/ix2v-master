/**
 * Description: ��Ƶ���Ļ��Ʒ�������ָ���շ�����
 * Author: 		lvzhihui
 * Create: 		2012-01-23
 * 
 */

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>      /* basic system data types */
#include <error.h>

#include "video_subscriber_server.h"
#include "obj_ThreadHeartBeatService.h"

vd_server_msg_type api_one_video_server_subscribe_rsp_ext0( int client_ip,vd_subscribe_req_ext0_pack*pclient_request, vd_response_result result, int mcg_addr, short port,int reslution);

// �������˷���ʵ��
video_subscriber_server	one_subscriber_server;

int 					server_inner_msg_process_pid; 				// �ڲ���Ϣ����pid
void* 					server_inner_msg_process( void* arg );		// eth0�����������


video_subscriber_server	wlan_one_subscriber_server;

int 					wlan_server_inner_msg_process_pid; 			// �ڲ���Ϣ����pid
void* 					wlan_server_inner_msg_process( void* arg ); // wlan�����������

int server_recv_busines_anaylasis( char* buf, int len );

/**
 * @fn:		init_one_subscriber_server
 *
 * @brief:	������˫����ͨ��ʵ����ʼ��
 *
 * @param:	none
 *
 * @return: -1/err, 0/ok
 */
int init_one_subscriber_server( void )
{
	// init state
	#if defined(PID_IX611) || defined(PID_IX622)
	//CmrDevManager_init();
	#endif
	one_subscriber_server.state = VD_SERVER_IDLE;
	// init udp
	init_one_udp_comm_rt_buff( &one_subscriber_server.udp, 2500,server_recv_busines_anaylasis, 2500, NULL );
	init_one_udp_comm_rt_type( &one_subscriber_server.udp, "multicast server", UDP_RT_TYPE_UNICAST, VIDEO_SERVER_CMD_RECV_PORT, VIDEO_CLIENT_CMD_RECV_PORT, NULL, NET_ETH0);
	// init business rsp wait array	
	init_one_send_array(&one_subscriber_server.waitrsp_array);
	if( pthread_create(&server_inner_msg_process_pid, 0, server_inner_msg_process, 0) )
	{
		return -1;
	}	
	one_subscriber_server.send_cmd_sn = 0;

	
	
	// init state
	wlan_one_subscriber_server.state = VD_SERVER_IDLE;
	#if 1
	// init udp
	init_one_udp_comm_rt_buff( &wlan_one_subscriber_server.udp, 500,server_recv_busines_anaylasis, 500, NULL );
	init_one_udp_comm_rt_type( &wlan_one_subscriber_server.udp, "multicast server", UDP_RT_TYPE_UNICAST, VIDEO_SERVER_CMD_RECV_PORT, VIDEO_CLIENT_CMD_RECV_PORT, NULL, NET_WLAN0);
	// init business rsp wait array	
	init_one_send_array(&wlan_one_subscriber_server.waitrsp_array);
	if( pthread_create(&wlan_server_inner_msg_process_pid, 0, wlan_server_inner_msg_process, 0) )
	{
		return -1;
	}	
	wlan_one_subscriber_server.send_cmd_sn = 0;
	#endif
	init_subscriber_list();
	init_udp_send_list();
	init_rtp_send_list();
	init_rec_save_list();
	init_rtsp_udp_send_list();

	vd_printf("init_one_subscriber_server ok!\n");

	return 0;
}

#define SERVER_POLLING_MS		100
void* server_inner_msg_process(void* arg )
{
	// 100ms��ʱ��ѯ
	while( 1 )
	{
		usleep(SERVER_POLLING_MS*1000);
		poll_all_business_recv_array( &one_subscriber_server.waitrsp_array, SERVER_POLLING_MS );
	}
	return 0;	
}

void* wlan_server_inner_msg_process(void* arg )
{
	// 100ms��ʱ��ѯ
	while( 1 )
	{
		usleep(SERVER_POLLING_MS*1000);
		poll_all_business_recv_array( &wlan_one_subscriber_server.waitrsp_array, SERVER_POLLING_MS );
	}
	return 0;	
}

vd_server_msg_type api_one_video_server_subscribe_rsp( int client_ip,vd_subscribe_req_ext_pack *pclient_request, vd_response_result result, int mcg_addr, unsigned short port);
vd_server_msg_type api_one_video_server_desubscribe_rsp( int client_ip,vd_desubscribe_req_ext_pack *pclient_request, vd_response_result result);


/**
 * @fn:		get_channel_according_to_resolution
 *
 * @brief:	���ݴ���ֱ���ƥ����Ƶͨ����
 *
 * @param:	trans_reso	- ����ֱ���
 *
 * @return: -1/err, 0-2/��Ƶͨ����
 */
int get_channel_according_to_resolution(ip_video_trans_reso trans_reso)
{
	int reso_w, reso_h;
	switch( trans_reso )
	{
		case Resolution_240P:
			reso_w = 320;
			reso_h = 240;
			break;

		case Resolution_480P:
			reso_w = 640;
			reso_h = 480;
			break;

		case Resolution_720P:
			reso_w = 1280;
			reso_h = 720;
			break;
	}
	return api_get_channel_from_resolution(reso_w,reso_h);
}

/**
 * @fn:		get_resolution_according_to_channel
 *
 * @brief:	���ݵ�ǰͨ��ƥ��ֱ�������
 *
 * @param:	channel	- ͨ����
 *
 * @return: �ֱ�������-ip_video_trans_reso
 */
ip_video_trans_reso get_resolution_according_to_channel(int channel)
{
	int reso_w, reso_h;
	api_ak_vi_get_channel_resolution(channel, &reso_w, &reso_h );
	if( reso_w == 320 && reso_h == 240 )
		return Resolution_240P;
	else if( reso_w == 640 && reso_h == 480 )
		return Resolution_480P;
	else if( reso_w == 1280 && reso_h == 720 )
		return Resolution_720P;
	else
		return Resolution_480P;
}

/**
 * @fn:		api_enable_video_server_transfer
 *
 * @brief:	������Ƶ�������Ƶ���
 *
 * @param:  	channel	 	- ����ͨ��
 * @param:  	send_mode	- ��������
 * @param:  	ip_addr	 	- �����ַ
 * @param:  	ip_port 	- ����˿ں�
 *
 * @return: 	-1/err, 0/ok
 */
int api_enable_video_server_transfer( int channel, int send_mode, int ip_addr, unsigned short ip_port )
{	
	one_subscriber_server.state = VD_SERVER_ACTIVE;
	// subscriber
	printf(">>>>>>>>>>>>>>>SUBSCRIBER-channel[%d]-send_mode[%d]-addr[0x%08x:%d]<<<<<<<<<<<<<<<<<<<<\n",channel,send_mode, ip_addr,ip_port);
	return api_udp_add_one_send_node(channel, send_mode, ip_addr, ip_port);
}

/**
 * @fn:		api_disable_video_server_transfer
 *
 * @brief:	ֹͣ��Ƶ�������Ƶ���
 *
 * @param:  	channel	 	- ����ͨ��
 * @param:  	send_mode	- ��������
 * @param:  	ip_addr	 	- �����ַ
 * @param:  	ip_port 	- ����˿ں�
 *
 * @return: 	-1/err, 0/ok
 */
int api_disable_video_server_transfer( int channel, int send_mode, int ip_addr, unsigned short ip_port )
{
	one_subscriber_server.state = VD_SERVER_IDLE;
	// desubscriber
	printf(">>>>>>>>>>>>>>>DESUBSCRIBER-channel[%d]-send_mode[%d]-addr[0x%08x:%d]<<<<<<<<<<<<<<<<<<<<\n",channel,send_mode, ip_addr,ip_port);
	return api_udp_del_one_send_node(channel, send_mode, ip_addr, ip_port);
}

// �������˽�������ҵ�����

/**
 * @fn:		server_recv_busines_anaylasis
 *
 * @brief:	��������ָ���
 *
 * @param:  	buf - ���ݰ�ָ�� 
 * @param:  	len	- ���ݰ�����
 *
 * @return: 	0
 */
 
 extern pthread_mutex_t RecManager_lock;
int server_recv_busines_anaylasis( char* buf, int len )
{	
	int				channel,type;
	int				mcg_addr;
	unsigned short	port;
	int				user_cnt;
	int				get_server_ip_result;
	int				i;
	

	// lzh_20160811_s
	// ��ͷ��Ҫȥ��
	baony_head* pbaony_head = (baony_head*)buf;
	//if( pbaony_head->type != IP8210_CMD_TYPE )
	//	return -1;
	len -= sizeof(baony_head);
	buf += sizeof(baony_head);
	// lzh_20160811_e

	// �������˽�������ҵ�����, ��Ҫ���˵�target ip ��4���ֽ�
	vd_subscribe_req_pack* 	prcvbuf = (vd_subscribe_req_pack*)(buf);
	vd_subscribe_rsp_pack*	prspbuf	= (vd_subscribe_rsp_pack*)(buf);
	vd_subscribe_req_ext_pack* 	prcvbuf_ext = (vd_subscribe_req_ext_pack*)(buf);
	vd_subscribe_rsp_ext_pack*	prspbuf_ext	= (vd_subscribe_rsp_ext_pack*)(buf);
	vd_desubscribe_req_ext_pack* 	preqbuf_desub = (vd_desubscribe_req_ext_pack*)(buf);
	vd_desubscribe_rsp_ext_pack*	prspbuf_desub = (vd_desubscribe_rsp_ext_pack*)(buf);
	vd_subscribe_req_ext0_pack *prcvbuf_ext0 = (vd_subscribe_req_ext0_pack*)(buf);

	printf("get_one_client_request,ip=0x%08x,cmd=%d\n",prcvbuf->target.ip,prcvbuf->target.cmd);
	//pthread_mutex_lock(&RecManager_lock);
	switch( prcvbuf->target.cmd )
	{
		case SUBSCRIBE_REQ:
			// newest protocol, support trans_type: 0-multicast, 1-unicast
			if( len >= sizeof(vd_subscribe_req_ext_pack) )
			{
				get_server_trans_addr_port( prcvbuf_ext->target.ip, prcvbuf_ext->trans_type, prcvbuf_ext->trans_reso, &mcg_addr, &port );
				channel = get_channel_according_to_resolution(prcvbuf_ext->trans_reso);
				printf("SUBSCRIBE_REQ: match channel[%d]\n",channel);
				PrintCurrentTime(100001);
				if( channel != -1 )
				{
					// �ظ��鲥���� - enable
					if( api_one_video_server_subscribe_rsp( prcvbuf_ext->target.ip, prcvbuf_ext, VD_RESPONSE_RESULT_ENABLE, mcg_addr, port ) == VD_SERVER_MSG_RSP_OK )
					{
						#if 1
						type = (prcvbuf_ext->trans_type==ip_video_multicast)?0:1;
						// ���ÿ���ָ���ͻ�����Ƶ�������
						if( api_enable_video_server_transfer( channel, type, mcg_addr, port ) > 0 )
						{
							// �Ǽǵ�user list
							vd_printf("activate_subscriber_list,channel=%d, type=%d, ip=0x%08x, time=%d\n",channel,type,prcvbuf_ext->target.ip,prcvbuf_ext->vd_multicast_time);
							user_cnt = activate_subscriber_list_ext(channel, type, prcvbuf_ext->target.ip, prcvbuf_ext->vd_multicast_time);
							vd_printf("activate_subscriber_list,user total = %d\n",user_cnt);
						}
						#else
						type =1;// (prcvbuf_ext->trans_type==ip_video_multicast)?0:1;
						// ���ÿ���ָ���ͻ�����Ƶ�������
						if( api_enable_video_server_transfer( channel, type, prcvbuf_ext->target.ip, port ) > 0 )
						{
							// �Ǽǵ�user list
							vd_printf("activate_subscriber_list,channel=%d, type=%d, ip=0x%08x, time=%d\n",channel,type,prcvbuf_ext->target.ip,prcvbuf_ext->vd_multicast_time);
							user_cnt = activate_subscriber_list_ext(channel, type, prcvbuf_ext->target.ip, prcvbuf_ext->vd_multicast_time);
							vd_printf("activate_subscriber_list,user total = %d\n",user_cnt);
						}
						#endif
					}
					else
						api_one_video_server_subscribe_rsp( prcvbuf_ext->target.ip, prcvbuf_ext, VD_RESPONSE_RESULT_NONE, 0, 0 );
				}
				else
					api_one_video_server_subscribe_rsp( prcvbuf_ext->target.ip, prcvbuf_ext, VD_RESPONSE_RESULT_NONE, 0, 0 );
			}
			else if( len >= sizeof(vd_subscribe_req_ext0_pack) )
			{
				get_server_trans_addr_port( prcvbuf_ext0->target.ip, ip_video_multicast, prcvbuf_ext0->resolution, &mcg_addr, &port );
				channel = get_channel_according_to_resolution(prcvbuf_ext0->resolution);
				printf("SUBSCRIBE_REQ: match channel[%d]\n",channel);
				if( channel != -1 )
				{

					// �ظ��鲥���� - enable
					if( api_one_video_server_subscribe_rsp_ext0(prcvbuf_ext0->target.ip,prcvbuf_ext0,VD_RESPONSE_RESULT_ENABLE,mcg_addr, port,prcvbuf_ext0->resolution)== VD_SERVER_MSG_RSP_OK )
					{
						//type = (prcvbuf_ext->trans_type==ip_video_multicast)?0:1;
						// ���ÿ���ָ���ͻ�����Ƶ�������
						#if 1
						if( api_enable_video_server_transfer( channel, 0, mcg_addr, port ) > 0 )
						#else
						if( api_enable_video_server_transfer( channel, 1, prcvbuf_ext0->target.ip, port ) > 0 )
						#endif
						{
							// �Ǽǵ�user list
							vd_printf("activate_subscriber_list,channel=%d, type=%d, ip=0x%08x, time=%d\n",channel,type,prcvbuf_ext->target.ip,prcvbuf_ext->vd_multicast_time);
							user_cnt = activate_subscriber_list_ext(channel, 0, prcvbuf_ext0->target.ip, prcvbuf_ext0->vd_multicast_time==0?600:prcvbuf_ext0->vd_multicast_time);
							vd_printf("activate_subscriber_list,user total = %d\n",user_cnt);
						}
					}
					else
						api_one_video_server_subscribe_rsp( prcvbuf_ext->target.ip, prcvbuf_ext, VD_RESPONSE_RESULT_NONE, 0, 0 );
				}
				else
					api_one_video_server_subscribe_rsp( prcvbuf_ext->target.ip, prcvbuf_ext, VD_RESPONSE_RESULT_NONE, 0, 0 );
			}
			// oldest procotol (have no resolution), and new procotol (have resolution)
			else if( len >= sizeof(vd_subscribe_req_pack) )
			{
				
				// �õ��鲥��ַ
				//get_server_multicast_addr_port( &mcg_addr, &port );
				get_server_trans_addr_port( prcvbuf->target.ip, ip_video_multicast, Resolution_480P, &mcg_addr, &port );
				channel = get_channel_according_to_resolution(Resolution_480P);
				// �ظ��鲥���� - enable
				if( api_one_video_server_subscribe_rsp( prcvbuf_ext->target.ip, prcvbuf_ext, VD_RESPONSE_RESULT_ENABLE, mcg_addr, port ) == VD_SERVER_MSG_RSP_OK )
				{
					// ���ÿ���ָ���ͻ�����Ƶ�鲥�������
					
					#if 1
					if( api_enable_video_server_transfer(channel, 0, mcg_addr, port ) > 0 )
					#else
					if( api_enable_video_server_transfer(channel, 1, prcvbuf_ext->target.ip, port ) > 0 )
					#endif
					{
						// �Ǽǵ�user list
						vd_printf("activate_subscriber_list,ip=0x%08x,time=%d\n",prcvbuf_ext->target.ip,prcvbuf->vd_multicast_time);
						user_cnt = activate_subscriber_list_ext(channel,0,prcvbuf->target.ip,prcvbuf->vd_multicast_time);
						vd_printf("activate_subscriber_list,user total = %d\n",user_cnt);
					}
				}
			}
			else
				api_one_video_server_subscribe_rsp( prcvbuf_ext->target.ip, prcvbuf_ext, VD_RESPONSE_RESULT_DISABLE, 0, 0 );
			 //printf_json(GetVideoServerAllState(),"11111");
			break;
		case DESUBSCRIBE_REQ:
			// newest protocol, support trans_type: 0-multicast, 1-unicast
			if( len >= sizeof(vd_desubscribe_req_ext_pack) )
			{
				api_one_video_server_desubscribe_rsp( preqbuf_desub->target.ip, preqbuf_desub, VD_RESPONSE_RESULT_ENABLE );
				// ע����user list
				vd_printf("deactivate_subscriber_list,ip=0x%08x\n",preqbuf_desub->target.ip);	
				
				type = (preqbuf_desub->trans_type==ip_video_multicast)?0:1;
				channel = get_channel_according_to_resolution(preqbuf_desub->trans_reso);
				printf("DESUBSCRIBE_REQ: match channel[%d]\n",channel);

				user_cnt = deactivate_subscriber_list_ext(channel, type, preqbuf_desub->target.ip);	
				vd_printf("disactivate_subscriber_list,user total = %d\n",user_cnt);
				if( type || (user_cnt <= 0) )
				{
					get_server_trans_addr_port( preqbuf_desub->target.ip, preqbuf_desub->trans_type, preqbuf_desub->trans_reso, &mcg_addr, &port );
					#if 1
					api_disable_video_server_transfer(channel, type, mcg_addr,port);
					#else
					api_disable_video_server_transfer(channel, 1, preqbuf_desub->target.ip,port);
					#endif
				}
			}
			else
			{
				api_one_video_server_desubscribe_rsp( preqbuf_desub->target.ip, preqbuf_desub, VD_RESPONSE_RESULT_ENABLE );
				// ע����user list
				vd_printf("deactivate_subscriber_list,ip=0x%08x\n",preqbuf_desub->target.ip);	
				
				for(channel=0;channel<3;channel++)
				{
					#if 1
					if(deactivate_subscriber_list_ext(channel,0,preqbuf_desub->target.ip)<=0)
					{
						if(channel==0)
							type=Resolution_720P;
						else if(channel==1)
							type=Resolution_480P;
						else 
							type=Resolution_240P;
						get_server_trans_addr_port( prcvbuf_ext0->target.ip, ip_video_multicast, type, &mcg_addr, &port );

						api_disable_video_server_transfer(channel,0,mcg_addr,port);
					}
					#else
					if(channel==0)
						type=Resolution_720P;
					else if(channel==1)
						type=Resolution_480P;
					else 
						type=Resolution_240P;
					get_server_trans_addr_port( prcvbuf_ext0->target.ip, ip_video_multicast, type, &mcg_addr, &port );
					deactivate_subscriber_list_ext(channel,1,preqbuf_desub->target.ip);
					api_disable_video_server_transfer(channel,1,preqbuf_desub->target.ip,port);
					#endif
				}
			}
			break;

		case CANCEL_RSP:
			// ����ҵ��Ӧ��
			if( trig_one_send_array( &one_subscriber_server.waitrsp_array, prspbuf->target.id, prspbuf->target.cmd ) < 0 )
				vd_printf("recv one vd_rsp none-match command,id=%d\n",	prspbuf->target.id);		
			else
				vd_printf("recv one vd_rsp match command ok,id=%d\n",prspbuf->target.id);
			#if 0
			user_cnt = deactivate_subscriber_list_ext(0,0, prspbuf->target.ip);
			vd_printf("cancel_subscriber_list,user total = %d\n",user_cnt);

			get_server_multicast_addr_port( &mcg_addr, &port );
			api_disable_video_server_transfer(0,0,mcg_addr,port);
			#endif
			for(channel=0;channel<3;channel++)
			{
				if(deactivate_subscriber_list_ext(channel,0,preqbuf_desub->target.ip)<=0)
				{
					if(channel==0)
						type=Resolution_720P;
					else if(channel==1)
						type=Resolution_480P;
					else 
						type=Resolution_240P;
					get_server_trans_addr_port( prcvbuf_ext0->target.ip, ip_video_multicast, type, &mcg_addr, &port );

					api_disable_video_server_transfer(channel,0,mcg_addr,port);
				}
			}
			break;
		case SUBSCRIBE_SER_HEARTBEAT:
			bprintf("111111111:SUBSCRIBE_SER_HEARTBEAT\n");
			RecvThreadHeartbeatReply(SubscriberSer_Heartbeat_Mask);
			break;
		default:
			break;
	}
	//pthread_mutex_unlock(&RecManager_lock);
	return 0;
}

/**
 * @fn:		api_one_video_server_subscribe_rsp
 *
 * @brief:	��Ӧ������Ƶ�������Ƶ��
 *
 * @param:  client_ip			- �ͻ���IP��ַ
 * @param:  pclient_request 	- ��Ƶ�������ݰ�ָ��
 * @param:  vd_response_result	- ��Ӧ���
 * @param:	mcg_addr  			- ���͵�ַ
 * @param:	port  				- ���Ͷ˿�
 *
 * @return: 	-1/err, 0/ok
 */
vd_server_msg_type api_one_video_server_subscribe_rsp( int client_ip, vd_subscribe_req_ext_pack *pclient_request, vd_response_result result, int mcg_addr, unsigned short port)
{	
	//vd_subscribe_rsp_pack	server_response; 
	vd_subscribe_rsp_ext_pack	server_response; 

	// �������
	server_response.target.ip			= client_ip;
	server_response.target.cmd			= SUBSCRIBE_RSP;
	server_response.target.id			= pclient_request->target.id;
	server_response.result				= result;
	server_response.vd_multicast_time	= pclient_request->vd_multicast_time;
	server_response.vd_multicast_port	= port;	
	server_response.vd_multicast_ip		= mcg_addr;
	server_response.trans_reso			= pclient_request->trans_reso;
	server_response.trans_type			= pclient_request->trans_type;
	server_response.version				= pclient_request->version|(30<<8);

	// ��������
	sem_t *presend_sem = one_udp_comm_trs_api( &one_subscriber_server.udp, server_response.target.ip, server_response.target.cmd, server_response.target.id,
				1, (char*)&server_response + sizeof(target_head), sizeof(vd_subscribe_rsp_ext_pack)-sizeof(target_head));
	
	vd_printf("api_one_video_server_subscribe_rsp...\n");
	
	// �ȴ�������ͨ��Ӧ��
	if( sem_wait_ex2(presend_sem, 5000) != 0 )
	{
		vd_printf("api_one_video_server_subscribe_rsp,wait ack --- timeout ---\n");		
		one_subscriber_server.msg	= VD_SERVER_MSG_RSP_TIMEOUT;	
		return one_subscriber_server.msg;
	}
	one_subscriber_server.msg	= VD_SERVER_MSG_RSP_OK;	
	vd_printf("api_one_video_server_subscribe_rsp ok!\n");
	return one_subscriber_server.msg;
}

vd_server_msg_type api_one_video_server_subscribe_rsp_ext0( int client_ip,vd_subscribe_req_ext0_pack*pclient_request, vd_response_result result, int mcg_addr, short port,int reslution)
{	
	vd_subscribe_rsp_ext0_pack	server_response; 

	// �������
	server_response.target.ip			= client_ip;
	server_response.target.cmd			= SUBSCRIBE_RSP;
	server_response.target.id			= pclient_request->target.id;
	server_response.result				= result;
	server_response.vd_multicast_time	= pclient_request->vd_multicast_time;
	server_response.vd_multicast_port	= port;	
	server_response.vd_multicast_ip		= mcg_addr;
	server_response.resolution=reslution;
	// ��������
	sem_t *presend_sem = one_udp_comm_trs_api( &one_subscriber_server.udp, server_response.target.ip, server_response.target.cmd, server_response.target.id,
				1, (char*)&server_response + sizeof(target_head), sizeof(vd_subscribe_rsp_ext_pack)-sizeof(target_head));
	
	vd_printf("api_one_video_server_subscribe_rsp...\n");
	if(presend_sem<=0)
		return 0;
	// �ȴ�������ͨ��Ӧ��
	if( sem_wait_ex2(presend_sem, 5000) != 0 )
	{
		vd_printf("api_one_video_server_subscribe_rsp,wait ack --- timeout ---\n");		
		one_subscriber_server.msg	= VD_SERVER_MSG_RSP_TIMEOUT;	
		return one_subscriber_server.msg;
	}
	one_subscriber_server.msg	= VD_SERVER_MSG_RSP_OK;	
	vd_printf("api_one_video_server_subscribe_rsp ok!\n");
	return one_subscriber_server.msg;
}
/**
 * @fn:		api_one_video_server_desubscribe_rsp
 *
 * @brief:	��Ӧ�˳���Ƶ�������Ƶ��
 *
 * @param:  	client_ip			- �ͻ���IP��ַ
 * @param:  	pclient_request 	- �����˳�ָ���ָ��
 * @param:  	vd_response_result	- ��Ӧ���
 *
 * @return: 	-1/err, 0/ok
 */
vd_server_msg_type api_one_video_server_desubscribe_rsp( int client_ip,vd_desubscribe_req_ext_pack *pclient_request, vd_response_result result)
{	
	vd_desubscribe_rsp_ext_pack	server_response; 

	// �������
	server_response.target.ip			= client_ip;
	server_response.target.cmd			= DESUBSCRIBE_RSP;
	server_response.target.id			= pclient_request->target.id;
	server_response.result				= result;
	server_response.trans_reso			= pclient_request->trans_reso;
	server_response.trans_type			= pclient_request->trans_type;
	server_response.version				= pclient_request->version;

	// ��������
	sem_t *presend_sem = one_udp_comm_trs_api( &one_subscriber_server.udp, server_response.target.ip, server_response.target.cmd, server_response.target.id, 
						1, (char*)&server_response + sizeof(target_head), sizeof(vd_desubscribe_rsp_ext_pack)-sizeof(target_head));
	
	vd_printf("api_one_video_server_desubscribe_rsp...\n");
	
	// �ȴ�������ͨ��Ӧ��
	if( sem_wait_ex2(presend_sem, 5000) != 0 )
	{
		vd_printf("api_one_video_server_desubscribe_rsp,wait ack --- timeout ---\n");		
		one_subscriber_server.msg	= VD_SERVER_MSG_RSP_TIMEOUT;	
		return one_subscriber_server.msg;
	}
	one_subscriber_server.msg	= VD_SERVER_MSG_RSP_OK;	
	vd_printf("api_one_video_server_desubscribe_rsp ok!\n");
	return one_subscriber_server.msg;
}

/**
 * @fn:		api_one_video_server_cancel_req
 *
 * @brief:	ָ֪ͨ���ͻ����˳���Ƶ��
 *
 * @param:  client_ip 	- �ͻ���IP��ַ
 * @param:  trans_reso 	- ����ֱ���
 * @param:  trans_type 	- ��������
 *
 * @return: -1/err, 0/ok
 */
vd_server_msg_type api_one_video_server_cancel_req( int client_ip, ip_video_trans_reso trans_reso, ip_video_trans_type trans_type )
{	
	vd_cancel_req_ext_pack	server_cancel; 
	
	// �������
	server_cancel.target.ip		= client_ip;
	server_cancel.target.cmd	= CANCEL_REQ;
	server_cancel.target.id		= ++one_subscriber_server.send_cmd_sn;
	server_cancel.result		= VD_RESPONSE_RESULT_NONE;
	server_cancel.trans_reso	= trans_reso;
	server_cancel.trans_type	= trans_type;
	server_cancel.version		= 1;

	// ����ȴ�ҵ��Ӧ�����
	//sem_t *pwaitrsp_sem = join_one_send_array(&one_subscriber_server.waitrsp_array,server_cancel.target.id, server_cancel.target.cmd+1,BUSINESS_RESEND_TIME, 1, NULL, 0 );
	sem_t *pwaitrsp_sem = join_one_send_array(&one_subscriber_server.waitrsp_array,server_cancel.target.id, server_cancel.target.cmd+0x80,BUSINESS_RESEND_TIME, 1, NULL, 0 );

	// ��������
	sem_t *presend_sem = one_udp_comm_trs_api( &one_subscriber_server.udp, server_cancel.target.ip, server_cancel.target.cmd, server_cancel.target.id,
					1, (char*)&server_cancel + sizeof(target_head), sizeof(vd_cancel_req_ext_pack)-sizeof(target_head));
	vd_printf("api_one_video_server_cancel_req...\n");

	if(pwaitrsp_sem == 0 || presend_sem == 0)
	{
		vd_printf("api_one_video_server_desubscribe_req,join array error---\n");
		one_subscriber_server.msg	= VD_SERVER_MSG_RSP_TIMEOUT;	
		return one_subscriber_server.msg;
	}
	
	// �ȴ��ͻ�����Ӧ��
	if( sem_wait_ex2(presend_sem, ACK_RESPONSE_TIMEOUT) != 0 )
	{
		vd_printf("api_one_video_server_cancel_req,wait ack --- timeout ---\n");		
		one_subscriber_server.msg	= VD_SERVER_MSG_RSP_TIMEOUT;	
		return one_subscriber_server.msg;
	}

	// �ȴ��ͻ���ҵ��ظ�2s
	if( sem_wait_ex2(pwaitrsp_sem, BUSINESS_WAIT_TIMEPUT) != 0 )
	{
		vd_printf("api_one_video_server_cancel_req,wait rsp ---have no rsp---\n");		
		one_subscriber_server.msg	= VD_SERVER_MSG_CANCEL_NO_RSP;		
		return one_subscriber_server.msg;
	}
	
	vd_printf("api_one_video_server_cancel_req ok!\n" );
	
	return one_subscriber_server.msg;
}

void api_subscriber_server_heartbeat(void)
{
	char buff[sizeof(baony_head)+sizeof(vd_subscribe_req_pack)];
	printf("111111%s:%d\n",__func__,__LINE__);
	vd_subscribe_req_pack *req_pack=(vd_subscribe_req_pack *)&buff[sizeof(baony_head)];
	req_pack->target.cmd=SUBSCRIBE_SER_HEARTBEAT;											
	push_udp_common_queue( &one_subscriber_server.udp.rmsg_buf, buff,sizeof(baony_head)+sizeof(vd_subscribe_req_pack));
}

