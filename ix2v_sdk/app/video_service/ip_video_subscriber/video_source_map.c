/**
 * Description: 旧版本（IX1、IX2协议）：获取组播地址; 新版本（IX2V协议）：获取组播地址、获取单播地址
 * Author: 		lvzhihui
 * Create: 		2012-01-23
 * 得到当前网卡的ip地址，根据通道号和传输类型得到当前传输的地址和端口号
 * 算法如下：
 * 1、若传输类型为组播：视频3个通道的端口相同（算法同旧方式），发送组播地址依次为：VIDEO_MULTICAST_ADDRESS_CH0/1/2
 * 2、若传输类型为点播：视频3个通道的端口不同（算法在旧方式上添加3个偏移量），发送地址为当前IP地址
 * 
 */

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>      /* basic system data types */
#include <error.h>

#include "video_source_map.h"

#define VIDEO_MULTICAST_ADDRESS			"236.6.8.1"
#define VIDEO_MULTICAST_ADDRESS_CH0		"236.6.8.1"
#define VIDEO_MULTICAST_ADDRESS_CH1		"236.6.8.2"
#define VIDEO_MULTICAST_ADDRESS_CH2		"236.6.8.3"

/**
 * @fn:		get_server_multicast_addr_port
 *
 * @brief:	旧版本：兼容ix1、ix2协议，得到组播服务的地址和端口
 *
 * @param:	mcg_addr:	为本地视频服务器的组播地址
 * @param:	pport:		为本地视频服务器的端口号
 *
 * @return: -1/err, 0/ok
 */
int get_server_multicast_addr_port( int* mcg_addr, unsigned short *pport )
{
	uint16 ip_node_id;
	ip_node_id = inet_network(GetSysVerInfo_IP())&0xfff;	// 主机字节序
	// 得到组播地址
	*mcg_addr	= inet_addr(VIDEO_MULTICAST_ADDRESS);
	// 根据当前ip_node_id得到组播端口号
	*pport = ip_node_id+VIDEO_SERVER_MULTICAST_PORT;	
	printf("get server multicast addr = %s, port = %d\n",VIDEO_MULTICAST_ADDRESS,*pport);
	return 0;
}

/**
 * @fn:		get_server_trans_addr_port
 *
 * @brief:	新版本：ix2v协议，得到组播地址和端口、单播地址和端口
 *
 * @param:	target_ip:	请求方目标ip地址
 * @param:	trans_type:	传输类型（0-组播，1-点播）
 * @param:	trans_reso:	传输分辨率
 * @param:	ipaddr:		返回发送目标ip地址
 * @param:	port:		返回发送目标ip端口
 *
 * @return: -1/err, 0/ok
 */
int get_server_trans_addr_port( int target_ip, ip_video_trans_type trans_type, ip_video_trans_reso trans_reso, int* ipaddr, unsigned short *port )
{
	uint16 ip_node_id;
	ip_node_id = inet_network(GetSysVerInfo_IP())&0xfff;	// 主机字节序

	if( trans_type == ip_video_multicast )
	{		
		// 得到组播地址
		if( trans_reso == Resolution_240P )
			*ipaddr	= inet_addr(VIDEO_MULTICAST_ADDRESS_CH0);
		else if( trans_reso == Resolution_480P )
			*ipaddr	= inet_addr(VIDEO_MULTICAST_ADDRESS_CH1);
		else
			*ipaddr	= inet_addr(VIDEO_MULTICAST_ADDRESS_CH2);
		// 根据当前ip_node_id得到组播端口号
		*port = ip_node_id+VIDEO_SERVER_MULTICAST_PORT+ip_node_id*3+trans_reso;
		printf("get multicast ip=%08x, port=%d\n", *ipaddr, *port);
	}
	else // if( trans_type == ip_video_unicast )
	{
		// 得到点播地址
		*ipaddr	= target_ip;
		// 根据当前ip_node_id得到点播端口号
		*port = VIDEO_SERVER_MULTICAST_PORT+ip_node_id*3+trans_reso;
		printf("get unicast ip=%08x, port=%d\n", *ipaddr, *port);
	}
	return 0;
}
