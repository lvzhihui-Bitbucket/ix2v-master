
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>      /* basic system data types */
#include <error.h>

#include "video_subscriber_client.h"
#include "cJSON.h"

video_subscriber_client	one_subscriber_client={0};

int 					client_inner_msg_process_pid; 				// 内部消息处理pid
void* 					client_inner_msg_process( void* arg );

video_subscriber_client	wlan_one_subscriber_client={0};

int 					wlan_client_inner_msg_process_pid; 				// 内部消息处理pid
void* 					wlan_client_inner_msg_process( void* arg );

int client_recv_busines_anaylasis( char* buf, int len );

int init_one_subscriber_client( void )
{
	int i;
	// init state
	for( i = 0; i < 4; i++ )
	{
		one_subscriber_client.data[i].state = VD_CLIENT_IDLE;
		one_subscriber_client.data[i].send_cmd_sn = 0;
	}
	// init udp
	init_one_udp_comm_rt_buff( &one_subscriber_client.udp, 500,client_recv_busines_anaylasis, 500, NULL );
	init_one_udp_comm_rt_type( &one_subscriber_client.udp, "multicast client", UDP_RT_TYPE_UNICAST, VIDEO_CLIENT_CMD_RECV_PORT, VIDEO_SERVER_CMD_RECV_PORT, NULL, NET_ETH0);
	// init business rsp wait array
	init_one_send_array(&one_subscriber_client.waitrsp_array);
	if( pthread_create(&client_inner_msg_process_pid, 0, client_inner_msg_process, 0) )
	{
		return -1;
	}

	// init state
	for( i = 0; i < 4; i++ )
	{
		wlan_one_subscriber_client.data[i].state = VD_CLIENT_IDLE;
		wlan_one_subscriber_client.data[i].send_cmd_sn = 0;
	}
	// init udp
	init_one_udp_comm_rt_buff( &wlan_one_subscriber_client.udp, 500,client_recv_busines_anaylasis, 500, NULL );
	init_one_udp_comm_rt_type( &wlan_one_subscriber_client.udp, "multicast client", UDP_RT_TYPE_UNICAST, VIDEO_CLIENT_CMD_RECV_PORT, VIDEO_SERVER_CMD_RECV_PORT, NULL, NET_WLAN0);
	// init business rsp wait array
	init_one_send_array(&wlan_one_subscriber_client.waitrsp_array);
	if( pthread_create(&wlan_client_inner_msg_process_pid, 0, wlan_client_inner_msg_process, 0) )
	{
		return -1;
	}
	
	vd_printf("init_one_subscriber_client ok!\n");
	
	return 0;
}

#define CLIENT_POLLING_MS		100
void* client_inner_msg_process(void* arg )
{
	// 100ms定时查询
	while( 1 )
	{
		usleep(CLIENT_POLLING_MS*1000);
		//poll_all_business_recv_array( &one_subscriber_client.waitrsp_array, CLIENT_POLLING_MS );
	}
	return 0;	
}

void* wlan_client_inner_msg_process(void* arg )
{
	// 100ms定时查询
	while( 1 )
	{
		usleep(CLIENT_POLLING_MS*1000);
		poll_all_business_recv_array( &wlan_one_subscriber_client.waitrsp_array, CLIENT_POLLING_MS );
	}
	return 0;	
}

vd_client_msg_type api_one_video_client_cancel_rsp(int ins, int server_ip, vd_cancel_req_ext_pack *pserver_request, vd_response_result result );

int api_video_client_multicast_notify_off( int server_ip, int ins );

video_subscriber_client* check_client_recv_cmd_ins_version( char* buf, int len, int *ptr_ins, int* ptr_version )
{
	vd_subscribe_rsp_pack*			ptr_subrsp			= (vd_subscribe_rsp_pack*)(buf);
	vd_subscribe_rsp_ext_pack*		ptr_subrsp_ext		= (vd_subscribe_rsp_ext_pack*)(buf);
	vd_desubscribe_rsp_pack* 		ptr_desubrsp		= (vd_desubscribe_rsp_pack*)(buf);
	vd_desubscribe_rsp_ext_pack* 	ptr_desubrsp_ext	= (vd_desubscribe_rsp_ext_pack*)(buf);
	vd_cancel_req_pack*				ptr_cacelbuf		= (vd_cancel_req_pack*)(buf);
	vd_cancel_req_ext_pack*			ptr_cacelbuf_ext	= (vd_cancel_req_ext_pack*)(buf);
	
	video_subscriber_client*			pone_subscriber_client;

	int i, ins, new_version;

	// check version
	if( ptr_subrsp->target.cmd == CANCEL_REQ )
	{
		if( len >= sizeof(vd_cancel_req_ext_pack) )
			new_version = 1;
		else
			new_version = 0;
	}
	else if( ptr_subrsp->target.cmd == SUBSCRIBE_RSP )
	{
		if( len >= sizeof(vd_subscribe_rsp_ext_pack) )
			new_version = 1;
		else
			new_version = 0;
	}
	else if( ptr_subrsp->target.cmd == DESUBSCRIBE_RSP )
	{
		if( len >= sizeof(vd_desubscribe_rsp_ext_pack) )
			new_version = 1;
		else
			new_version = 0;
	}
	else
	{
		new_version = -1;
		return NULL;
	}

	// check ins
	if(!strcmp(GetNetDeviceNameByTargetIp(ptr_subrsp->target.ip), NET_ETH0))
	{
		pone_subscriber_client = &one_subscriber_client;
	}
	else
	{
		pone_subscriber_client = &wlan_one_subscriber_client;
	}

	for( i = 0; i < 4; i++ )
	{
		if(	pone_subscriber_client->data[i].vd_ask_server_ip == ptr_subrsp->target.ip )
		{
			if( !new_version )
			{
				vd_printf("old command[%d], ip = 0x%08x ins = %d \n", ptr_subrsp->target.cmd, ptr_cacelbuf_ext->target.ip, i);
				break;
			}

			else if( ptr_subrsp->target.cmd == CANCEL_REQ )
			{
				if( pone_subscriber_client->data[i].trans_reso == ptr_cacelbuf_ext->trans_reso && pone_subscriber_client->data[i].trans_type == ptr_cacelbuf_ext->trans_type )
				{
					vd_printf("new CANCEL_REQ ip = 0x%08x ins = %d \n", ptr_cacelbuf_ext->target.ip, i);		
					break;
				}
			}
			else if( ptr_subrsp->target.cmd == SUBSCRIBE_RSP )
			{
				if( pone_subscriber_client->data[i].trans_reso == ptr_subrsp_ext->trans_reso && pone_subscriber_client->data[i].trans_type == ptr_subrsp_ext->trans_type )
				{
					vd_printf("new SUBSCRIBE_RSP ip = 0x%08x ins = %d \n", ptr_subrsp_ext->target.ip, i);
					break;
				}
			}
			else if( ptr_subrsp->target.cmd == DESUBSCRIBE_RSP )
			{
				if( pone_subscriber_client->data[i].trans_reso == ptr_desubrsp_ext->trans_reso && pone_subscriber_client->data[i].trans_type == ptr_desubrsp_ext->trans_type )
				{
					vd_printf("new DESUBSCRIBE_RSP ip = 0x%08x ins = %d \n", ptr_desubrsp_ext->target.ip, i);
					break;
				}
			}
		}
	}
	if(i>=4)
		return NULL;
	*ptr_ins 		= ((i== 4)?0:i);
	*ptr_version	= new_version;
	return pone_subscriber_client;
}

// 客户端接收命令业务分析
int client_recv_busines_anaylasis( char* buf, int len )
{
	// lzh_20160811_s
	// 包头需要去掉
	baony_head* pbaony_head = (baony_head*)buf;
	if( pbaony_head->type != IP8210_CMD_TYPE )
		return -1;
	len -= sizeof(baony_head);
	buf += sizeof(baony_head);
	// lzh_20160811_e

	// 客户端接收命令业务分析, 需要过滤掉target ip 的4个字节
	vd_subscribe_req_pack* 	prcvbuf 				= (vd_subscribe_req_pack*)(buf);
	vd_subscribe_req_ext_pack* 	prcvbuf_ext			= (vd_subscribe_req_ext_pack*)(buf);

	vd_desubscribe_rsp_ext_pack* prcvbuf_desub_ext	= (vd_desubscribe_rsp_ext_pack*)(buf);

	vd_subscribe_rsp_pack*	prspbuf					= (vd_subscribe_rsp_pack*)(buf);
	vd_subscribe_rsp_ext_pack*	prspbuf_ext			= (vd_subscribe_rsp_ext_pack*)(buf);
	vd_cancel_req_pack*		prcvcacelbuf			= (vd_cancel_req_pack*)(buf);
	vd_cancel_req_ext_pack*		prcvcacelbuf_ext	= (vd_cancel_req_ext_pack*)(buf);
	
	video_subscriber_client*	pone_subscriber_client;
	int i, ins, new_version;
	printf("333333333%s:%d\n",__func__,__LINE__);
	if( (pone_subscriber_client = check_client_recv_cmd_ins_version(buf,len,&ins,&new_version)) == NULL )
	{
		vd_printf(" ERR COMMAND[%d] !!!\n", prcvbuf->target.cmd);
		return -1;
	}
	printf("333333333%s:%d\n",__func__,__LINE__);
	// 业务处理
	switch( prcvbuf->target.cmd )
	{			
		case CANCEL_REQ:
			// send rsp command
			api_one_video_client_cancel_rsp(ins,prcvcacelbuf->target.ip, prcvcacelbuf_ext, VD_RESPONSE_RESULT_ENABLE);
			//stop_receive_decode_process(ins);
			pone_subscriber_client->data[ins].state	= VD_CLIENT_IDLE;
			api_video_client_multicast_notify_off(prcvcacelbuf->target.ip, ins);
			break;

		case SUBSCRIBE_RSP:
			
			vd_printf("recv SUBSCRIBE_RSP ip = 0x%08x ins=%d state=%d,send_cmd_sn=%d,rsp_id=%d\n", prspbuf->target.ip, ins, pone_subscriber_client->data[ins].state, pone_subscriber_client->data[ins].send_cmd_sn, prspbuf->target.id);		
			if( pone_subscriber_client->data[ins].state == VD_CLIENT_SUBSCRIBE && pone_subscriber_client->data[ins].send_cmd_sn == prspbuf->target.id )
			{
				// 业务应答为允许
				if( prspbuf->result == VD_RESPONSE_RESULT_ENABLE )
				{
					pone_subscriber_client->data[ins].state				= VD_CLIENT_ACTIVE;
					pone_subscriber_client->data[ins].msg				= VD_CLIENT_MSG_REQ_OK; 

					// 保存数据
					//pone_subscriber_client->vd_ask_server_ip	= prspbuf->target.ip;
					pone_subscriber_client->data[ins].vd_proxy_server_ip	= 0;
					pone_subscriber_client->data[ins].vd_multicast_ip 	= prspbuf->vd_multicast_ip;		
					pone_subscriber_client->data[ins].vd_multicast_port 	= prspbuf->vd_multicast_port;
					pone_subscriber_client->data[ins].vd_multicast_time 	= prspbuf->vd_multicast_time;
					if( new_version )
					{
						pone_subscriber_client->data[ins].trans_reso	= prspbuf_ext->trans_reso;
						pone_subscriber_client->data[ins].trans_type	= prspbuf_ext->trans_type;						
						if( prspbuf_ext->trans_type == ip_video_multicast )
						{
							vd_printf("ins[%d] recv one multicast ip(0x%08x),port(%d), ins=%d\n", ins,prspbuf_ext->vd_multicast_ip,prspbuf_ext->vd_multicast_port);
							// 启动组播接收
							pone_subscriber_client->data[ins].state	= VD_CLIENT_ACTIVE;
							start_receive_decode_process(ins, prspbuf_ext->target.ip, prspbuf_ext->vd_multicast_ip, prspbuf_ext->vd_multicast_port, 1);
						}
						else if( prspbuf_ext->trans_type == ip_video_unicast )
						{
							vd_printf("ins[%d] recv one unicast ip(0x%08x),port(%d)\n", ins, prspbuf_ext->vd_multicast_ip,prspbuf_ext->vd_multicast_port);
							// 启动点播接收
							pone_subscriber_client->data[ins].state	= VD_CLIENT_ACTIVE;
							start_receive_decode_process(ins, prspbuf->target.ip, prspbuf->vd_multicast_ip, prspbuf->vd_multicast_port, 0);
						}
						else
						{
							vd_printf("ins[%d] recv one unkown type!!!\n",ins);
						}
					}
					else
					{
						vd_printf("ins[%d] recv one multicast ip(0x%08x),port(%d), ins=%d\n", ins,prspbuf_ext->vd_multicast_ip,prspbuf_ext->vd_multicast_port);
						// 启动组播接收
						pone_subscriber_client->data[ins].state	= VD_CLIENT_ACTIVE;
						start_receive_decode_process(ins, prspbuf_ext->target.ip, prspbuf_ext->vd_multicast_ip, prspbuf_ext->vd_multicast_port, 1);
					}
				}
				// 检测应答信号为不允许
				else
				{					
					pone_subscriber_client->data[ins].state				= VD_CLIENT_IDLE;
					pone_subscriber_client->data[ins].msg				= VD_CLIENT_MSG_REQ_UNALLOW;
					vd_printf("VD_CLIENT_MSG_REQ_UNALLOW %d\n", prspbuf->result);		
				}
				if( trig_one_business_recv_array( &(pone_subscriber_client->waitrsp_array), prspbuf->target.id, prspbuf->target.cmd,NULL,0) < 0 )
				{
					vd_printf("recv one vd_rsp none-match command,id=%d\n",	prspbuf->target.id);		
					return;
				}
				else
				{
					
					vd_printf("recv one vd_rsp match command ok,id=%d\n",prspbuf->target.id);
					
				}
			}

			// 触发业务应答
			
			break;
		
		case DESUBSCRIBE_RSP:
			// if( one_subscriber_client.state	== VD_CLIENT_SUBSCRIBE && one_subscriber_client.send_cmd_sn == prspbuf->target.rsp_id )
			{
				pone_subscriber_client->data[ins].state				= VD_CLIENT_IDLE;
				pone_subscriber_client->data[ins].msg				= VD_CLIENT_MSG_REQ_OK; 
				pone_subscriber_client->data[ins].vd_ask_server_ip 	= 0;
				// 关闭组播接收
				stop_receive_decode_process(ins);
				pone_subscriber_client->data[ins].state	= VD_CLIENT_IDLE;
			}	
			// 触发业务应答
			if( trig_one_business_recv_array( &(pone_subscriber_client->waitrsp_array), prspbuf->target.id, prspbuf->target.cmd ,NULL,0) < 0 )
				vd_printf("recv one vd_rsp none-match command,id=%d\n",	prspbuf->target.id);		
			else
				vd_printf("recv one vd_rsp match command ok,id=%d\n",prspbuf->target.id);			
			break;			
		default:
			break;
	}
	return 0;
}

/*******************************************************************************************
 * @fn:		api_one_video_client_subscribe_req
 *
 * @brief:	申请加入对应ip的视频组播服务
 *
 * @param:  	server_ip		- 服务器IP地址
 * @param:  	dev_id		- 服务器端设备id 
 * @param:  	second	 	- 视频播放时间
 *
 * @return: 	-1/err, 0/ok
*******************************************************************************************/
vd_client_msg_type api_one_video_client_subscribe_req(int ins, int server_ip, int dev_id, int second )
{
	video_subscriber_client*	pone_subscriber_client;
	vd_subscribe_req_pack	client_request; 
		
	if(!strcmp(GetNetDeviceNameByTargetIp(server_ip), NET_ETH0))
	{
		pone_subscriber_client = &one_subscriber_client;
	}
	else
	{
		pone_subscriber_client = &wlan_one_subscriber_client;
	}


	// 状态判断
	if( pone_subscriber_client->data[ins].state != VD_CLIENT_IDLE )
		return VD_CLIENT_MSG_REQ_BUSY;

	// 状态初始化
	pone_subscriber_client->data[ins].state				= VD_CLIENT_SUBSCRIBE;
	pone_subscriber_client->data[ins].vd_ask_server_ip	= server_ip;
	
	// 命令组包
	client_request.target.ip		= server_ip;
	client_request.target.cmd		= SUBSCRIBE_REQ;
	client_request.target.id		= ++(pone_subscriber_client->data[ins].send_cmd_sn);
	client_request.result			= VD_RESPONSE_RESULT_NONE;
	client_request.vd_multicast_time= second;
	client_request.dev_id			= dev_id;

	vd_printf("api_one_video_client_subscribe_req to 0x%08x,ins =%d state=%d...\n", client_request.target.ip,ins,pone_subscriber_client->data[ins].state);
		
	// 加入等待业务应答队列	
	//sem_t *pwaitrsp_sem = join_one_send_array(&one_subscriber_client.waitrsp_array,client_request.target.id, client_request.target.cmd+1,BUSINESS_RESEND_TIME, 0, NULL, 0 );
	sem_t *pwaitrsp_sem = join_one_business_recv_array(&(pone_subscriber_client->waitrsp_array),client_request.target.id, client_request.target.cmd+0x80,BUSINESS_RESEND_TIME, NULL, 0 );
	
	// 发送数据
	sem_t *presend_sem = one_udp_comm_trs_api( &(pone_subscriber_client->udp), client_request.target.ip,client_request.target.cmd,client_request.target.id,
			1, (char*)&client_request+sizeof(target_head), sizeof(vd_subscribe_req_pack)-sizeof(target_head));
	
	vd_printf("api_one_video_client_subscribe_req...\n");

	if(pwaitrsp_sem == 0 || presend_sem == 0)
	{
		dprintf("api_one_video_client_subscribe_req,join array error---\n");
		pone_subscriber_client->data[ins].state	= VD_CLIENT_IDLE;
		pone_subscriber_client->data[ins].msg	= VD_CLIENT_MSG_REQ_TIMEOUT;		
		return pone_subscriber_client->data[ins].msg;
	}

	// 等待服务器通信应答
	if( sem_wait_ex2(presend_sem, ACK_RESPONSE_TIMEOUT) != 0 )
	{
		dprintf("api_one_video_client_subscribe_req,wait ack --- timeout ---\n");
		pone_subscriber_client->data[ins].state	= VD_CLIENT_IDLE;
		pone_subscriber_client->data[ins].msg	= VD_CLIENT_MSG_REQ_TIMEOUT;		
		return pone_subscriber_client->data[ins].msg;
	}
		
	// 等待服务器业务回复2s
	if( sem_wait_ex2(pwaitrsp_sem, 5000) != 0 )
	{
		dprintf("api_one_video_client_subscribe_req,wait rsp ---have no rsp---\n");		
		pone_subscriber_client->data[ins].state	= VD_CLIENT_IDLE;
		pone_subscriber_client->data[ins].msg	= VD_CLIENT_MSG_REQ_NO_RSP;		
		return pone_subscriber_client->data[ins].msg;
	}
	
	vd_printf("api_one_video_client_subscribe_req ok!,multi_addr=0x%08x,multi_port=0x%08x\n",pone_subscriber_client->data[ins].vd_multicast_ip,pone_subscriber_client->data[ins].vd_multicast_port);

	return pone_subscriber_client->data[ins].msg;
}

vd_client_msg_type api_one_video_client_subscribe_req_ext(int ins, int server_ip, int dev_id, int second, ip_video_trans_reso trans_reso, ip_video_trans_type trans_type)
{
	video_subscriber_client*	pone_subscriber_client;
	vd_subscribe_req_ext_pack	client_request; 
		
	if(!strcmp(GetNetDeviceNameByTargetIp(server_ip), NET_ETH0))
	{
		pone_subscriber_client = &one_subscriber_client;
	}
	else
	{
		pone_subscriber_client = &wlan_one_subscriber_client;
	}

	// 状态判断
	if( pone_subscriber_client->data[ins].state != VD_CLIENT_IDLE )
		return VD_CLIENT_MSG_REQ_BUSY;

	printf("=====000000000000000[%d]========================\n",ins);

	// 状态初始化
	pone_subscriber_client->data[ins].state				= VD_CLIENT_SUBSCRIBE;
	pone_subscriber_client->data[ins].vd_ask_server_ip	= server_ip;
	pone_subscriber_client->data[ins].trans_reso			= trans_reso;
	pone_subscriber_client->data[ins].trans_type			= trans_type;
	
	// 命令组包
	client_request.target.ip		= server_ip;
	client_request.target.cmd		= SUBSCRIBE_REQ;
	client_request.target.id		= ++(pone_subscriber_client->data[ins].send_cmd_sn);
	client_request.result			= VD_RESPONSE_RESULT_NONE;
	client_request.vd_multicast_time= second;
	client_request.dev_id			= dev_id;
	client_request.trans_reso		= trans_reso;
	client_request.trans_type		= trans_type;
	client_request.version			= 1;

	vd_printf("api_one_video_client_subscribe_req to 0x%08x,ins =%d state=%d...\n", client_request.target.ip,ins,pone_subscriber_client->data[ins].state);
		
	// 加入等待业务应答队列	
	//sem_t *pwaitrsp_sem = join_one_send_array(&one_subscriber_client.waitrsp_array,client_request.target.id, client_request.target.cmd+1,BUSINESS_RESEND_TIME, 0, NULL, 0 );
	sem_t *pwaitrsp_sem = join_one_business_recv_array(&(pone_subscriber_client->waitrsp_array),client_request.target.id, client_request.target.cmd+0x80,BUSINESS_RESEND_TIME, NULL, 0 );
	
	// 发送数据
	sem_t *presend_sem = one_udp_comm_trs_api( &(pone_subscriber_client->udp), client_request.target.ip,client_request.target.cmd,client_request.target.id,
			1, (char*)&client_request+sizeof(target_head), sizeof(vd_subscribe_req_ext_pack)-sizeof(target_head));
	
	vd_printf("api_one_video_client_subscribe_req...\n");

	if(pwaitrsp_sem == 0 || presend_sem == 0)
	{
		dprintf("api_one_video_client_subscribe_req,join array error---\n");
		pone_subscriber_client->data[ins].state	= VD_CLIENT_IDLE;
		pone_subscriber_client->data[ins].msg	= VD_CLIENT_MSG_REQ_TIMEOUT;		
		return pone_subscriber_client->data[ins].msg;
	}
	#if 0
	printf("333333333%s:%d\n",__func__,__LINE__);
	
	// 等待服务器通信应答
	if( sem_wait_ex2(presend_sem, ACK_RESPONSE_TIMEOUT) != 0 )
	{
		dprintf("api_one_video_client_subscribe_req,wait ack --- timeout ---\n");
		pone_subscriber_client->data[ins].state	= VD_CLIENT_IDLE;
		pone_subscriber_client->data[ins].msg	= VD_CLIENT_MSG_REQ_TIMEOUT;		
		return pone_subscriber_client->data[ins].msg;
	}
	#endif
	// 等待服务器业务回复2s
	if( sem_wait_ex2(pwaitrsp_sem, 5000) != 0 )
	{
		dprintf("api_one_video_client_subscribe_req,wait rsp ---have no rsp---\n");		
		pone_subscriber_client->data[ins].state	= VD_CLIENT_IDLE;
		pone_subscriber_client->data[ins].msg	= VD_CLIENT_MSG_REQ_NO_RSP;		
		dele_one_business_recv_array(&(pone_subscriber_client->waitrsp_array),client_request.target.id, client_request.target.cmd+0x80);
		return pone_subscriber_client->data[ins].msg;
	}
	vd_printf("api_one_video_client_subscribe_req ok!,multi_addr=0x%08x,multi_port=0x%08x\n",pone_subscriber_client->data[ins].vd_multicast_ip,pone_subscriber_client->data[ins].vd_multicast_port);

	return pone_subscriber_client->data[ins].msg;
}
/*******************************************************************************************
 * @fn:		api_one_video_client_desubscribe_req_ext
 *
 * @brief:	申请加入视频服务的视频组
 *
 * @param:  	server_ip		- 服务器IP地址
 *
 * @return: 	-1/err, 0/ok
*******************************************************************************************/
vd_client_msg_type api_one_video_client_desubscribe_req_ext(int ins, int server_ip, ip_video_trans_reso trans_reso, ip_video_trans_type trans_type)
{
	//vd_desubscribe_req_pack		client_request; 
	vd_desubscribe_req_ext_pack		client_request;
	
	video_subscriber_client*	pone_subscriber_client;

	if(!strcmp(GetNetDeviceNameByTargetIp(server_ip), NET_ETH0))
	{
		pone_subscriber_client = &one_subscriber_client;
		printf("multicast client eth0------------------\n");
	}
	else
	{
		pone_subscriber_client = &wlan_one_subscriber_client;
		printf("multicast client wlan0------------------\n");
	}

	if( pone_subscriber_client->data[ins].state == VD_CLIENT_IDLE )
	{
		stop_receive_decode_process(ins);
		return VD_CLIENT_MSG_REQ_NONE;
	}

	if( pone_subscriber_client->data[ins].state == VD_CLIENT_ACTIVE )
	{
		// 无条件关闭组播接收
		//pone_subscriber_client->data[ins].state	= VD_CLIENT_IDLE;
		//stop_receive_decode_process(ins);
	}
	
	pone_subscriber_client->data[ins].state		= VD_CLIENT_IDLE;
		
	// 命令组包
	client_request.target.ip		= server_ip;
	client_request.target.cmd		= DESUBSCRIBE_REQ;
	client_request.target.id		= ++(pone_subscriber_client->data[ins].send_cmd_sn);
	client_request.result			= VD_RESPONSE_RESULT_NONE;
	client_request.trans_reso		= trans_reso;
	client_request.trans_type		= trans_type;
	client_request.version			= 1;

	vd_printf("api_one_video_client_desubscribe_req_ext to 0x%08x,cmd =%d, id =%d ...\n", client_request.target.ip,client_request.target.cmd,client_request.target.id);
		
	// 加入等待业务应答队列
	//sem_t *pwaitrsp_sem = join_one_send_array(&one_subscriber_client.waitrsp_array,client_request.target.id, client_request.target.cmd+1,BUSINESS_RESEND_TIME, 0, NULL, 0 );
	sem_t *pwaitrsp_sem = join_one_business_recv_array(&(pone_subscriber_client->waitrsp_array),client_request.target.id, client_request.target.cmd+0x80,BUSINESS_RESEND_TIME, NULL, 0 );

	// 发送数据
	sem_t *presend_sem = one_udp_comm_trs_api( &(pone_subscriber_client->udp), client_request.target.ip,client_request.target.cmd,client_request.target.id,
			1, (char*)&client_request+sizeof(target_head), sizeof(vd_desubscribe_req_ext_pack)-sizeof(target_head));
	
	vd_printf("api_one_video_client_desubscribe_req_ext...\n");

	if(pwaitrsp_sem == 0 || presend_sem == 0)
	{
		vd_printf("api_one_video_client_desubscribe_req_ext,join array error---\n");
		pone_subscriber_client->data[ins].msg	= VD_CLIENT_MSG_CLOSE_TIMEOUT;		
		return pone_subscriber_client->data[ins].msg;
	}
	
	// 等待服务器通信应答
	if( sem_wait_ex2(presend_sem, ACK_RESPONSE_TIMEOUT) != 0 )
	{
		vd_printf("api_one_video_client_desubscribe_req_ext,wait ack --- timeout ---\n");
		pone_subscriber_client->data[ins].msg	= VD_CLIENT_MSG_CLOSE_TIMEOUT;	
		return pone_subscriber_client->data[ins].msg;
	}
	
	// 等待服务器业务回复2s
	if( sem_wait_ex2(pwaitrsp_sem, BUSINESS_WAIT_TIMEPUT) != 0 )
	{
		vd_printf("api_one_video_client_desubscribe_req_ext,wait rsp ---have no rsp---\n");		
		pone_subscriber_client->data[ins].msg	= VD_CLIENT_MSG_CLOSE_NO_RSP;	
		dele_one_business_recv_array(&(pone_subscriber_client->waitrsp_array),client_request.target.id, client_request.target.cmd+0x80);
		return pone_subscriber_client->data[ins].msg;
	}
	vd_printf("api_one_video_client_desubscribe_req_ext ok!\n" );

	return pone_subscriber_client->data[ins].msg;	
}


/*******************************************************************************************
 * @fn:		api_one_video_client_cancel_rsp
 *
 * @brief:	相应cancel的应答
 *
 * @param:  	server_ip			- 服务器IP地址
 * @param:	pserver_request	- 服务器请求命令
 * @param:	result			- 服务器请求结果
 *
 * @return: 	-1/err, 0/ok
*******************************************************************************************/
vd_client_msg_type api_one_video_client_cancel_rsp(int ins, int server_ip, vd_cancel_req_ext_pack *pserver_request, vd_response_result result )
{
	vd_cancel_rsq_ext_pack	client_rsp; 
	
	video_subscriber_client*	pone_subscriber_client;

	if(!strcmp(GetNetDeviceNameByTargetIp(server_ip), NET_ETH0))
	{
		pone_subscriber_client = &one_subscriber_client;
	}
	else
	{
		pone_subscriber_client = &wlan_one_subscriber_client;
	}
	pone_subscriber_client->data[ins].msg	= VD_CLIENT_MSG_CLOSE_OK;
	pone_subscriber_client->data[ins].state				= VD_CLIENT_IDLE;
	pone_subscriber_client->data[ins].msg				= VD_CLIENT_MSG_REQ_OK; 
	pone_subscriber_client->data[ins].vd_ask_server_ip 	= 0;
	// 状态初始化
	//pone_subscriber_client->data[ins].state	= VD_CLIENT_IDLE;
	
	// 命令组包
	client_rsp.target.ip		= server_ip;
	client_rsp.target.cmd		= CANCEL_RSP;
	client_rsp.target.id		= pserver_request->target.id;
	client_rsp.result			= result;
	client_rsp.trans_reso		= pserver_request->trans_reso;
	client_rsp.trans_type		= pserver_request->trans_type;
	client_rsp.version			= pserver_request->version;

	// 发送数据
	sem_t *presend_sem = one_udp_comm_trs_api( &(pone_subscriber_client->udp), client_rsp.target.ip,client_rsp.target.cmd,client_rsp.target.id,
			1, (char*)&client_rsp+sizeof(target_head), sizeof(vd_cancel_req_ext_pack)-sizeof(target_head));
	
	vd_printf("api_one_video_client_cancel_rsp...\n");
	if(presend_sem<=0)
		return -1;
	// 等待服务器通信应答
	if( sem_wait_ex2(presend_sem, 5000) != 0 )
	{
		dprintf("api_one_video_client_cancel_rsp,wait ack --- timeout ---\n");
		pone_subscriber_client->data[ins].msg	= VD_CLIENT_MSG_CLOSE_TIMEOUT;		
		return pone_subscriber_client->data[ins].msg;
	}
	
	vd_printf("api_one_video_client_cancel_rsp ok!\n" );

	pone_subscriber_client->data[ins].msg	= VD_CLIENT_MSG_CLOSE_OK;	
	return pone_subscriber_client->data[ins].msg;	
}

/*******************************************************************************************
*******************************************************************************************/
vd_client_msg_type api_one_video_client_change_resolution(int ins, int server_ip, int dev_id,int resolution)
{
	video_subscriber_client*	pone_subscriber_client;
	vd_subscribe_req_ext_pack	client_request; 
		
	if(!strcmp(GetNetDeviceNameByTargetIp(server_ip), NET_ETH0))
	{
		pone_subscriber_client = &one_subscriber_client;
	}
	else
	{
		pone_subscriber_client = &wlan_one_subscriber_client;
	}

	
	// 状态判断
	if( pone_subscriber_client->data[ins].state != VD_CLIENT_ACTIVE )
		return VD_CLIENT_MSG_REQ_BUSY;
	#if 0
	// 状态初始化
	pone_subscriber_client->data[ins].state				= VD_CLIENT_SUBSCRIBE;
	pone_subscriber_client->data[ins].vd_ask_server_ip	= server_ip;
	#endif
	// 命令组包
	client_request.target.ip		= server_ip;
	client_request.target.cmd		= SUBSCRIBE_REQ;
	client_request.target.id		= ++(pone_subscriber_client->data[ins].send_cmd_sn);
	client_request.result			= VD_RESPONSE_RESULT_NONE;
	client_request.vd_multicast_time= 0;
	client_request.dev_id			= dev_id;
	client_request.trans_reso		=resolution;

	vd_printf("api_one_video_client_change_resolution to 0x%08x,ins =%d state=%d...\n", client_request.target.ip,ins,pone_subscriber_client->data[ins].state);
		
	// 加入等待业务应答队列	
	//sem_t *pwaitrsp_sem = join_one_send_array(&one_subscriber_client.waitrsp_array,client_request.target.id, client_request.target.cmd+1,BUSINESS_RESEND_TIME, 0, NULL, 0 );
	//sem_t *pwaitrsp_sem = join_one_send_array(&(pone_subscriber_client->waitrsp_array),client_request.target.id, client_request.target.cmd+0x80,BUSINESS_RESEND_TIME, 0, NULL, 0 );
	
	// 发送数据
	sem_t *presend_sem = one_udp_comm_trs_api( &(pone_subscriber_client->udp), client_request.target.ip,client_request.target.cmd,client_request.target.id,
			1, (char*)&client_request+sizeof(target_head), sizeof(vd_subscribe_req_ext_pack)-sizeof(target_head));
	
	vd_printf("api_one_video_client_change_resolution...\n");

	if(presend_sem == 0)
	{
		dprintf("api_one_video_client_change_resolution,join array error---\n");
		pone_subscriber_client->data[ins].state	= VD_CLIENT_IDLE;
		pone_subscriber_client->data[ins].msg	= VD_CLIENT_MSG_REQ_TIMEOUT;		
		return pone_subscriber_client->data[ins].msg;
	}

	// 等待服务器通信应答
	if( sem_wait_ex2(presend_sem, ACK_RESPONSE_TIMEOUT) != 0 )
	{
		dprintf("api_one_video_client_change_resolution,wait ack --- timeout ---\n");
		pone_subscriber_client->data[ins].state	= VD_CLIENT_IDLE;
		pone_subscriber_client->data[ins].msg	= VD_CLIENT_MSG_REQ_TIMEOUT;		
		return pone_subscriber_client->data[ins].msg;
	}
		
	vd_printf("api_one_video_client_change_resolution ok!,multi_addr=0x%08x,multi_port=0x%08x\n",pone_subscriber_client->data[ins].vd_multicast_ip,pone_subscriber_client->data[ins].vd_multicast_port);

	return VD_CLIENT_MSG_CLOSE_OK;
}
extern char* my_inet_ntoa(int ip_n, char* ip_a);
cJSON *GetSubscriberState(int ins)
{
	cJSON *state;
	char addr[20];
	state=cJSON_CreateObject();
	if(state==NULL)
		return NULL;
	if(one_subscriber_client.data[ins].state==VD_CLIENT_IDLE)
	{
		cJSON_AddStringToObject(state,"Sub_State","IDLE");
	}
	else if(one_subscriber_client.data[ins].state==VD_CLIENT_SUBSCRIBE)
	{
		cJSON_AddStringToObject(state,"Sub_State","SUBSCRIBE");
	}
	else if(one_subscriber_client.data[ins].state==VD_CLIENT_ACTIVE)
	{
		cJSON_AddStringToObject(state,"Sub_State","ACTIVE");
	}
	else
	{
		cJSON_AddStringToObject(state,"Sub_State","UNKOWN");
	}
	if(one_subscriber_client.data[ins].trans_reso==Resolution_240P)
	{
		cJSON_AddStringToObject(state,"Resolution","240P");
	}
	else if(one_subscriber_client.data[ins].trans_reso==Resolution_480P)
	{
		cJSON_AddStringToObject(state,"Resolution","480P");
	}
	else if(one_subscriber_client.data[ins].trans_reso==Resolution_720P)
	{
		cJSON_AddStringToObject(state,"Resolution","720P");
	}
	else
	{
		cJSON_AddStringToObject(state,"Resolution","UNKOWN");
	}	
	cJSON_AddStringToObject(state,"Server_Addr",my_inet_ntoa(one_subscriber_client.data[ins].vd_ask_server_ip,addr));
	return state;
}
int GetSubscriberStateInt(int ins)
{
	return one_subscriber_client.data[ins].state;
}

int GetSubscriberServerAddr(int ins)
{
	return one_subscriber_client.data[ins].vd_ask_server_ip;
}