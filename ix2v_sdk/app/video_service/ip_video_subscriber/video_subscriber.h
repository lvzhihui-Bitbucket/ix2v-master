
#ifndef _VIDEO_SUBSCRIBER_H
#define _VIDEO_SUBSCRIBER_H

#include "vtk_udp_stack_class.h"
#include "vtk_udp_stack_c5_ipc_cmd.h"

#include "task_survey.h"
#include "sys_msg_process.h"
#include "elog_forcall.h"
#define	VDP_PRINTF_VIDEO

#ifdef	VDP_PRINTF_VIDEO
#define	vd_printf(fmt,...)	printf("[V]-["__FILE__"]-[%04d]-"fmt"",__LINE__,##__VA_ARGS__)
//#define	vd_printf(fmt,...)	log_e("[V]-["__FILE__"]-[%04d]-"fmt"",__LINE__,##__VA_ARGS__)
#else
#define	vd_printf(fmt,...)
#endif

typedef enum
{
	ip_video_none,				// 无
	ip_video_multicast,			// 组播类型
	ip_video_unicast,			// 点播类型
	ip_video_file_play,			//czn_20170412		for_fileplay
} ip_video_trans_type;

typedef enum
{
	Resolution_240P=0,
	Resolution_480P,
	Resolution_720P,
	Resolution_480P_Only,
}ip_video_trans_reso;


// C/S交互命令定义
typedef enum
{
	// communication ack
	COM_ACK=0,				//  client 	<-> server
	
	// subscribe couple
	SUBSCRIBE_REQ=0x0001,			//  client 	-> server
	SUBSCRIBE_RSP=0x0081,			//  server 	-> client
	
	// desubscribe couple
	DESUBSCRIBE_REQ=0x0002,		//  client 	-> server
	DESUBSCRIBE_RSP=0x0082,		//  server 	-> client
	
	// cancel couple
	CANCEL_REQ=0x0003,				//	server	-> client	
	CANCEL_RSP=0x0083,				//	client	-> server
	SUBSCRIBE_SER_HEARTBEAT=0x0004,	
} vd_subscribe_cmd;

// 服务器端response回复的result类型
typedef enum
{
	VD_RESPONSE_RESULT_NONE,		// 无效
	VD_RESPONSE_RESULT_ENABLE,		// 允许
	VD_RESPONSE_RESULT_DISABLE,		// 不允许
	//转到代理服务器
	VD_RESPONSE_RESULT_TO_PROXY,	// 允许到代理
} vd_response_result;

// 视频资源状态报告类型
typedef enum
{
	VD_NOTIFY_NORMAL,			// 视频信号正常
	VD_NOTIFY_NO_SIGNAL,		// 无视频信号	
	VD_NOTIFY_SIGNAL_RESET,		// 视频信号重启
} vd_notify_State;

// subscribe couple

// 客户端申请的数据包格式
typedef struct
{
	target_head			target;
	vd_response_result	result;				// 申请的回复结果
	int					vd_multicast_time;	// 视频组播的时间(s)
	int					dev_id;				// 申请视像头设备id
} vd_subscribe_req_pack;
#if 0
typedef enum
{
	Resolution_ext0_240P=0,
	Resolution_ext0_480P,
	Resolution_ext0_720P,
	//Resolution_480P_Only,
}Video_Resolution_ext0_e;
#endif
typedef struct
{
	target_head			target;
	vd_response_result	result;				// 申请的回复结果
	int					vd_multicast_time;	// 视频组播的时间(s)
	int					dev_id;				// 申请视像头设备id
	int					resolution;
} vd_subscribe_req_ext0_pack;


// 服务器端响应客户端的申请数据包格式
typedef struct
{
	target_head			target;
	vd_response_result	result;				// 申请的回复结果
	int					vd_multicast_time;	// 视频组播的时间(s)
	unsigned short		vd_multicast_port;	// 视频组播地端口号
	int					vd_multicast_ip;	// 视频组播地址
} vd_subscribe_rsp_pack;

typedef struct
{
	target_head			target;
	vd_response_result	result;				// 申请的回复结果
	int					vd_multicast_time;	// 视频组播的时间(s)
	short				vd_multicast_port;	// 视频组播地端口号
	int					vd_multicast_ip;	// 视频组播地址
	int					resolution;
} vd_subscribe_rsp_ext0_pack;

// desubscribe couple
typedef struct _vd_desubscribe_req_pack_t
{
	target_head			target;
	vd_response_result	result;
} vd_desubscribe_req_pack;

typedef struct _vd_desubscribe_rsp_pack_t
{
	target_head			target;
	vd_response_result	result;
} vd_desubscribe_rsp_pack;

// cancel couple
typedef struct _vd_cancel_req_pack_t
{
	target_head			target;
	vd_response_result	result;				// 申请的回复结果
} vd_cancel_req_pack;

typedef struct _vd_cancel_rsq_pack_t
{
	target_head			target;
	vd_response_result	result;				// 申请的回复结果
} vd_cancel_rsq_pack;

//////////////////////////////////////////////////////////////////////////////
typedef struct
{
	target_head			target;
	vd_response_result	result;				// 申请的回复结果
	int					vd_multicast_time;	// 视频组播/点播的时间(s)
	int					dev_id;				// 申请视像头设备id
	ip_video_trans_reso trans_reso;			// 0-240p，1-480p，2-720p
	ip_video_trans_type	trans_type;			// 1-multicast, 2-unicast
	int					version;			// 版本，保留
} vd_subscribe_req_ext_pack;

typedef struct
{
	target_head			target;
	vd_response_result	result;				// 申请的回复结果
	int					vd_multicast_time;	// 视频组播/点播的时间(s)
	unsigned short		vd_multicast_port;	// 视频组播地端口号
	int					vd_multicast_ip;	// 视频组播/点播地址
	ip_video_trans_reso trans_reso;			// 0-240p，1-480p，2-720p
	ip_video_trans_type	trans_type;			// 1-multicast, 2-unicast
	int					version;			// 版本，保留
} vd_subscribe_rsp_ext_pack;

typedef struct
{
	target_head			target;
	vd_response_result	result;
	ip_video_trans_reso trans_reso;			// 0-240p，1-480p，2-720p
	ip_video_trans_type	trans_type;			// 1-multicast, 2-unicast
	int					version;			// 版本，保留
} vd_desubscribe_req_ext_pack;

typedef struct
{
	target_head			target;
	vd_response_result	result;
	ip_video_trans_reso trans_reso;			// 0-240p，1-480p，2-720p
	ip_video_trans_type	trans_type;			// 1-multicast, 2-unicast
	int					version;			// 版本，保留
} vd_desubscribe_rsp_ext_pack;

typedef struct
{
	target_head			target;
	vd_response_result	result;				// 申请的回复结果
	ip_video_trans_reso trans_reso;			// 0-240p，1-480p，2-720p
	ip_video_trans_type	trans_type;			// 1-multicast, 2-unicast
	int					version;			// 版本，保留
} vd_cancel_req_ext_pack;

typedef struct
{
	target_head			target;
	vd_response_result	result;				// 申请的回复结果
	ip_video_trans_reso trans_reso;			// 0-240p，1-480p，2-720p
	ip_video_trans_type	trans_type;			// 1-multicast, 2-unicast
	int					version;			// 版本，保留
} vd_cancel_rsq_ext_pack;

#endif


