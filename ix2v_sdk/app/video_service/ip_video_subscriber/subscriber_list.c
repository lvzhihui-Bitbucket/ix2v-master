/**
 * Description: 订阅机制列表处理服务
 * Author: 		lvzhihui
 * Create: 		2012-01-23
 * 
 * 3个码流通道2中发送类型，供3x2个订阅列表
 * 
 */

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>      /* basic system data types */
#include <error.h>
#include <pthread.h>
#include <sys/time.h>
#include <time.h>

#include "subscriber_list.h"
#include "video_subscriber_server.h"
#include"cJSON.h"

#define SUBSCRIBER_MAX_CHANNEL	3		// 3 trans channels(ch0,ch1,ch2) - will add files type
#define SUBSCRIBER_MAX_TYPE		2		// 2 trans type(0-multicast,1-unicast)

subscriber_list	server_userlist_ext[SUBSCRIBER_MAX_CHANNEL][SUBSCRIBER_MAX_TYPE];

pthread_t server_userlist_monitor_pid;


/**
 * @fn:		server_userlist_monitor_process
 *
 * @brief:	订阅列表100ms定时处理服务程序
 *
 * @param:	none
 *
 * @return: none
 */
void* server_userlist_monitor_process(void* arg)
{
	int i, channel, type;
	int ip_addr, ip_port;	

	ip_video_trans_reso trans_reso;			// 0-240p，1-480p，2-720p
	ip_video_trans_type	trans_type;			// 1-multicast, 2-unicast

	// 定时查询user list
	while( 1 )
	{
		usleep(100000);

		for( channel = 0; channel < SUBSCRIBER_MAX_CHANNEL; channel++ )
		{
			for( type = 0; type < SUBSCRIBER_MAX_TYPE; type++ )
			{
				pthread_mutex_lock( &server_userlist_ext[channel][type].lock );
				for( i = 0; i < server_userlist_ext[channel][type].counter; i++ )
				{
					server_userlist_ext[channel][type].dat[i].reg_timer += 100;
					if( server_userlist_ext[channel][type].dat[i].reg_timer >= (server_userlist_ext[channel][type].dat[i].reg_period*1000) )
					{
						server_userlist_ext[channel][type].dat[i].reg_timer = 0;
						
						vd_printf("server_userlist_monitor_process deactivate one client ip = 0x%08x\n",server_userlist_ext[channel][type].dat[i].reg_ip);

						trans_reso = get_resolution_according_to_channel(channel);
						trans_type = (type==0)?ip_video_multicast:ip_video_unicast;

						api_one_video_server_cancel_req(server_userlist_ext[channel][type].dat[i].reg_ip,trans_reso,trans_type);				
						// 删除计时时间到的客户端
						server_userlist_ext[channel][type].dat[i].reg_tv		= server_userlist_ext[channel][type].dat[server_userlist_ext[channel][type].counter-1].reg_tv;
						server_userlist_ext[channel][type].dat[i].reg_ip		= server_userlist_ext[channel][type].dat[server_userlist_ext[channel][type].counter-1].reg_ip;
						server_userlist_ext[channel][type].dat[i].reg_period	= server_userlist_ext[channel][type].dat[server_userlist_ext[channel][type].counter-1].reg_period;
						server_userlist_ext[channel][type].dat[i].reg_timer		= server_userlist_ext[channel][type].dat[server_userlist_ext[channel][type].counter-1].reg_timer;
						server_userlist_ext[channel][type].counter--;

						// 点播类型：同时码流传输, 组播类型：无订阅则关闭视频服务
						if( type || (server_userlist_ext[channel][type].counter == 0) )
						{
							get_server_trans_addr_port( server_userlist_ext[channel][type].dat[i].reg_ip, trans_type, trans_reso, &ip_addr, &ip_port );
							
							api_disable_video_server_transfer(channel, type, ip_addr,ip_port);
						}
					}
				}		
				//VideoDebugLog();
				pthread_mutex_unlock( &server_userlist_ext[channel][type].lock );
			}
		}
	}
	return 0;
}
static int save_cnt=0;

void VideoDebugLog(void)
{
	#if 0
	cJSON *event;
	if(save_cnt<server_userlist_ext[0][0].counter&&server_userlist_ext[0][0].counter>0&&server_userlist_ext[0][0].dat[server_userlist_ext[0][0].counter-1].reg_timer>3*1000)
	{
		save_cnt=server_userlist_ext[0][0].counter;
		
		event =cJSON_CreateObject();
		cJSON_AddStringToObject(event, "EventName","ViDebug");
		cJSON_AddNumberToObject(event,"ClientNum",server_userlist_ext[0][0].counter);
		API_Event_Json(event);
		printf_json(event,__func__);
		cJSON_Delete(event);
	}
	if(save_cnt>0&&server_userlist_ext[0][0].counter==0)
	{
		save_cnt=0;
		event =cJSON_CreateObject();
		cJSON_AddStringToObject(event, "EventName","ViDebug");
		cJSON_AddStringToObject(event,"state","all client have dissub");
		API_Event_Json(event);
		printf_json(event,__func__);
		cJSON_Delete(event);
	}
	#endif
}
void VideoDebugLogInit(void)
{
	#if 0
	cJSON *event;
	if(save_cnt>0&&server_userlist_ext[0][0].counter>0)
	{
		event =cJSON_CreateObject();
		cJSON_AddStringToObject(event, "EventName","ViDebug");
		cJSON_AddStringToObject(event,"state","have client not dissub");
		API_Event_Json(event);
		printf_json(event,__func__);
		cJSON_Delete(event);
	}
	save_cnt=0;
	#endif
}
void VideoEventLog(char *format,...)
{
	//bprintf(fmt,...)
	char detail[200];
	char temp[200];
	va_list args;
	va_start(args, format);
	vsnprintf(temp,200,format,args);
	va_end(args);
	snprintf(detail,200,"[%d]%s",API_Para_Read_Int("CALL_Next_Sn"),temp);
	cJSON *event =cJSON_CreateObject();
	cJSON_AddStringToObject(event, "EventName","ViDebug");
	cJSON_AddStringToObject(event,"detail",detail);
	API_Event_Json(event);
	cJSON_Delete(event);
}
void UnlockEventLog(char *format,...)
{
	//bprintf(fmt,...)
	char detail[200];
	va_list args;
	va_start(args, format);
	vsnprintf(detail,200,format,args);
	va_end(args);
	
	cJSON *event =cJSON_CreateObject();
	cJSON_AddStringToObject(event, "EventName","UnlockDebug");
	cJSON_AddStringToObject(event,"detail",detail);
	API_Event_Json(event);
	cJSON_Delete(event);
}
/**
 * @fn:		init_subscriber_list
 *
 * @brief:	订阅列表初始化
 *
 * @param:	none
 *
 * @return: none
 */
int init_subscriber_list( void )
{
	int channel, type;
	for( channel = 0; channel < SUBSCRIBER_MAX_CHANNEL; channel++ )
	{
		for( type = 0; type < SUBSCRIBER_MAX_TYPE; type++ )
		{
			server_userlist_ext[channel][type].counter	= 0;
			pthread_mutex_init( &server_userlist_ext[channel][type].lock, 0);
		}
	}	
	if( pthread_create(&server_userlist_monitor_pid, 0, server_userlist_monitor_process, 0) )
	{
		return -1;
	}
	vd_printf("init_subscriber_list ok...\n");
	return 0;
}

/**
 * @fn:		get_total_subscriber_list
 *
 * @brief:	得到指定通道订阅列表的计数
 *
 * @param:	channel	- 发送通道
 * @param:	type	- 发送类型 - 0/组播，1/点播
 *
 * @return: 返回个数
 */
int get_total_subscriber_list(int channel, int type )
{
	int i;
	
	if(channel>=SUBSCRIBER_MAX_CHANNEL)
		return -1;
	if(type>=SUBSCRIBER_MAX_TYPE)
		return -1;
	
	pthread_mutex_lock( &server_userlist_ext[channel][type].lock );	
	
	i = server_userlist_ext[channel][type].counter;
	
	pthread_mutex_unlock( &server_userlist_ext[channel][type].lock );	

	return i;
}


int get_subscriber_ip(int channel, int type,int index)
{
	int ip;
	if(channel>=SUBSCRIBER_MAX_CHANNEL)
		return -1;
	if(type>=SUBSCRIBER_MAX_TYPE)
		return -1;
	pthread_mutex_lock( &server_userlist_ext[channel][type].lock );	
	if(index<server_userlist_ext[channel][type].counter)
		ip = server_userlist_ext[channel][type].dat[index].reg_ip;
	else
		ip=-1;
	pthread_mutex_unlock( &server_userlist_ext[channel][type].lock );	

	return ip;
}
int if_subscriber_ip(int channel, int type,int ip)
{
	int i;
	int rev=0;
	if(channel>=SUBSCRIBER_MAX_CHANNEL)
		return 0;
	if(type>=SUBSCRIBER_MAX_TYPE)
		return 0;
	pthread_mutex_lock( &server_userlist_ext[channel][type].lock );	
	for(i=0;i<server_userlist_ext[channel][type].counter;i++)
	{
		if(ip== server_userlist_ext[channel][type].dat[i].reg_ip)
		{
			rev=1;
			break;
		}
	}
	
	pthread_mutex_unlock( &server_userlist_ext[channel][type].lock );	

	return rev;
}
/**
 * @fn:		delete_all_userlist
 *
 * @brief:	释放所有列表的订阅节点
 *
 * @param:	none
 *
 * @return: 0
 */
int delete_all_userlist(void)
{
	int i, channel, type;
	int ip_addr, ip_port;
	ip_video_trans_reso trans_reso;			// 0-240p，1-480p，2-720p
	ip_video_trans_type	trans_type;			// 1-multicast, 2-unicast

	for( channel = 0; channel < SUBSCRIBER_MAX_CHANNEL; channel++ )
	{
		for( type = 0; type < SUBSCRIBER_MAX_TYPE; type++ )
		{
			trans_reso = get_resolution_according_to_channel(channel);
			trans_type = (type==0)?ip_video_multicast:ip_video_unicast;			
			pthread_mutex_lock( &server_userlist_ext[channel][type].lock );
			if(server_userlist_ext[channel][type].counter > 0)
			{
				for( i = 0; i < server_userlist_ext[channel][type].counter; i++ )
				{
					get_server_trans_addr_port( server_userlist_ext[channel][type].dat[i].reg_ip, trans_type, trans_reso, &ip_addr, &ip_port );
					api_one_video_server_cancel_req(server_userlist_ext[channel][type].dat[i].reg_ip,trans_reso,trans_type);
					// 点播类型：同时码流传输, 组播类型：无订阅则关闭视频服务
					if( type ) api_disable_video_server_transfer(channel, type, ip_addr,ip_port);
				}
				server_userlist_ext[channel][type].counter = 0;					
				if( !type )	api_disable_video_server_transfer(channel, type, ip_addr,ip_port);
			}
			pthread_mutex_unlock( &server_userlist_ext[channel][type].lock );
		}
	}	
	return 0;
}

/**
 * @fn:		activate_subscriber_list_ext
 *
 * @brief:	添加目标节点到指定通道
 *
 * @param:	channel		- 订阅通道
 * @param:	type		- 订阅类型
 * @param:	reg_ip		- 订阅目标ip
 * @param:	reg_period	- 订阅时长
 *
 * @return: 总数
 */
int activate_subscriber_list_ext( int channel, int type, int reg_ip, int reg_period )
{
	struct timeval tv;	
	int i;

	if( channel >= SUBSCRIBER_MAX_CHANNEL || type >= SUBSCRIBER_MAX_TYPE )
		return -1;

	pthread_mutex_lock( &server_userlist_ext[channel][type].lock );
	
	if( server_userlist_ext[channel][type].counter >= MAX_SUBUSCRIBER_LIST_COUNT )
	{
		pthread_mutex_unlock( &server_userlist_ext[channel][type].lock );	
		return -1;
	}
	
	for( i = 0; i < server_userlist_ext[channel][type].counter; i++ )
	{
		if( server_userlist_ext[channel][type].dat[i].reg_ip == reg_ip )
		{
			VideoEventLog("repeat subscriber[%d],ip=%08x,cnt=%d",channel,reg_ip,i);
			pthread_mutex_unlock( &server_userlist_ext[channel][type].lock );	
			return -1;
		}
	}
	
	gettimeofday(&tv, 0);
	server_userlist_ext[channel][type].dat[server_userlist_ext[channel][type].counter].reg_tv		= tv;
	server_userlist_ext[channel][type].dat[server_userlist_ext[channel][type].counter].reg_ip		= reg_ip;
	server_userlist_ext[channel][type].dat[server_userlist_ext[channel][type].counter].reg_period	= reg_period;
	server_userlist_ext[channel][type].dat[server_userlist_ext[channel][type].counter].reg_timer	= 0;

	server_userlist_ext[channel][type].counter++;
	i = server_userlist_ext[channel][type].counter;

	pthread_mutex_unlock( &server_userlist_ext[channel][type].lock );	

	trigger_send_key_frame(channel);
	#ifdef PID_IX850
	trigger_send_key_frame_delay(channel,20);
	#else
	trigger_send_key_frame_delay(channel,10);
	#endif
	if(channel==0)
		VideoEventLog("subscriber[%d],ip=%08x,cnt=%d",channel,reg_ip,i);
	return i;
}

/**
 * @fn:		deactivate_subscriber_list_ext
 *
 * @brief:	释放目标节点到指定通道
 *
 * @param:	channel		- 订阅通道
 * @param:	type		- 订阅类型
 * @param:	reg_ip		- 订阅目标ip
 *
 * @return: 总数
 */
int deactivate_subscriber_list_ext( int channel, int type, int reg_ip)
{
	int ret=-1;
	int i;
	pthread_mutex_lock( &server_userlist_ext[channel][type].lock );	
	
	for( i = 0; i < server_userlist_ext[channel][type].counter; i++ )
	{
		if( server_userlist_ext[channel][type].dat[i].reg_ip == reg_ip )
		{
			server_userlist_ext[channel][type].dat[i].reg_tv 		= server_userlist_ext[channel][type].dat[server_userlist_ext[channel][type].counter-1].reg_tv;
			server_userlist_ext[channel][type].dat[i].reg_ip 		= server_userlist_ext[channel][type].dat[server_userlist_ext[channel][type].counter-1].reg_ip;
			server_userlist_ext[channel][type].dat[i].reg_period	= server_userlist_ext[channel][type].dat[server_userlist_ext[channel][type].counter-1].reg_period;
			server_userlist_ext[channel][type].dat[i].reg_timer		= server_userlist_ext[channel][type].dat[server_userlist_ext[channel][type].counter-1].reg_timer;
			server_userlist_ext[channel][type].counter--;
			ret=server_userlist_ext[channel][type].counter;
			break;
		}
	}
	//i = server_userlist_ext[channel][type].counter;
	
	pthread_mutex_unlock( &server_userlist_ext[channel][type].lock );	
	
	return ret;
}

