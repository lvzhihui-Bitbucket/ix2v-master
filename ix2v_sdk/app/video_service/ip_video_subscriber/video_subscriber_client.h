

#ifndef _VIDEO_SUBSCRIBER_CLIENT_H
#define _VIDEO_SUBSCRIBER_CLIENT_H

#include "video_subscriber.h"

typedef enum
{
	VD_CLIENT_IDLE,				// 客户端空闲状态
	VD_CLIENT_SUBSCRIBE,		// 客户端申请状态
	VD_CLIENT_ACTIVE,			// 客户端接收组播状态
} vd_client_state;

typedef enum
{
	//request
	VD_CLIENT_MSG_REQ_NONE,
	VD_CLIENT_MSG_REQ_BUSY,
	VD_CLIENT_MSG_REQ_NO_RSP,
	VD_CLIENT_MSG_REQ_OK,
	VD_CLIENT_MSG_REQ_UNALLOW,
	VD_CLIENT_MSG_REQ_TIMEOUT,
	VD_CLIENT_MSG_REQ_TO_PROXY,
	//close
	VD_CLIENT_MSG_CLOSE_NONE,
	VD_CLIENT_MSG_CLOSE_ERR,
	VD_CLIENT_MSG_CLOSE_NO_RSP,
	VD_CLIENT_MSG_CLOSE_OK,
	VD_CLIENT_MSG_CLOSE_TIMEOUT,
} vd_client_msg_type;

typedef struct
{
	vd_client_state 		state;				// 状态机
	vd_client_msg_type		msg;				// 最新返回消息
	int 					send_cmd_sn;		// 发送命令包序列号
	int						vd_multicast_time;	// 视频组播/点播的时间(s)
	unsigned short			vd_multicast_port;	// 视频组播/点播发送端口号
	int						vd_multicast_ip;	// 视频组播/点播发送地址	
	int						vd_ask_server_ip;	// 视频组播/点播请求服务器地址	
	int						vd_proxy_server_ip;	// 视频组播/点播代理服务器地址	
	ip_video_trans_reso		trans_reso;			// 视频分辨率
	ip_video_trans_type		trans_type;			// 视频传输类型
}video_client_data;


typedef struct _video_subscriber_client_t_
{
	udp_comm_rt 			udp;				//	udp交互模板实例
	send_sem_id_array		waitrsp_array;		// 业务应答同步队列	
	// registor data
	video_client_data		data[4];
}video_subscriber_client;


int init_one_subscriber_client( void );

/*******************************************************************************************
 * @fn:		api_one_video_client_subscribe_req_ext
 *
 * @brief:	申请加入对应ip的视频组播服务
 *
 * @param:  	server_ip	- 服务器IP地址
 * @param:  	dev_id		- 服务器端设备id 
 * @param:  	second	 	- 视频播放时间
 * @param:  	channel		- 服务器视频通道号
 *
 * @return: 	-1/err, 0/ok
*******************************************************************************************/
vd_client_msg_type api_one_video_client_subscribe_req_ext(int ins, int server_ip, int dev_id, int second, ip_video_trans_reso trans_reso, ip_video_trans_type trans_type);

/*******************************************************************************************
 * @fn:		api_one_video_client_desubscribe_req_ext
 *
 * @brief:	申请加入视频服务的视频组
 *
 * @param:  	server_ip		- 服务器IP地址
 * @param:  	channel			- 服务器视频通道号
 *
 * @return: 	-1/err, 0/ok
*******************************************************************************************/
vd_client_msg_type api_one_video_client_desubscribe_req_ext(int ins, int server_ip, ip_video_trans_reso trans_reso, ip_video_trans_type trans_type);

#endif


