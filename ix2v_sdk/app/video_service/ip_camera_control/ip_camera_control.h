
#ifndef _IP_CAMERA_CONTROL_H
#define _IP_CAMERA_CONTROL_H

#include "video_subscriber_server.h"

typedef enum
{
	ip_camera_none,				// 无
	ip_camera_local,			// 本地视频
	ip_camera_multicast,		// 组播类型
	ip_camera_unicast,			// 点播类型
} ip_camera_type;

typedef enum
{
	ip_camera_idle,				// 本地空闲
	ip_camera_run,				// 本地已启动
} ip_camera_active;

typedef struct _ip_camera_server_state_
{
	ip_camera_type		type;			// 开启类型	
	ip_camera_active	active;			// 开启状态
	int 				ip;				// 客户端ip
	int					dev_id;			// 设备id
	int					send_ch;		// 发送通道
	int 				send_ip;		// 发送地址
	int					send_port;		// 发送端口
} ip_camera_server_state;

/**
 * @fn:		ip_camera_control_init
 *
 * @brief:	视频服务器端初始化
 *
 * @param:	none
 *
 * @return: -1/err, 0/ok
 */
int ip_camera_control_init(void);

/**
 * @fn:		api_camera_server_turn_on
 *
 * @brief:	申请加入视频服务
 *
 * @param:		channel		- 传输通道
 * @param:		trans_type	- 传输类型
 * @param:  	client_ip	- 客户端IP地址
 * @param:  	period		- 服务时间
 *
 * @return: 	-1/err, 0/ok
 */
int api_camera_server_turn_on( int channel, ip_camera_type trans_type, int client_ip, int period );

/**
 * @fn:		api_camera_server_turn_off
 *
 * @brief:	关闭视频服务
 *
 * @param:	none
 *
 * @return: 	-1/err, 0/ok
 */
int api_camera_server_turn_off(void);

//--------------------------------------
// test interface
//--------------------------------------
/*
camera部分（服务器端）单元测试部分：

1、视频通道的配置：
	。调用obj_ak_manager.h先的相关api

2、服务器端主动开启preview:
	。单元测试API-1：
		// 服务器端：开启本地视频预览
		void api_local_preview_on( int channel );
	。单元测试API-2：
		// 服务器端：关闭本地视频预览
		void api_local_preview_off( int channel );

3、服务器端主动UDP点播发送：
	。通道选择、发送目标、发送时间
	。单元测试API-1：
		// 服务器端：选择“一个通道”，”单播发送“到指定“客户端地址”，选择“发送时间”
		void api_capture_unicast_on(int channel, int client_ip, int period );
	。单元测试API-2：
		// 服务器端：关闭发送通道
		void api_capture_unicast_off(int channel);

4、服务器端主动UDP组播发送：
	。通道选择、发送目标、发送时间
	。单元测试API-1：
		// 服务器端：选择“一个通道”，”组播发送“到指定“客户端地址”，选择“发送时间”
		void api_capture_multicast_on(int channel, int client_ip, int period);
	。单元测试API-2：
		// 服务器端：关闭发送通道
		void api_capture_multicast_off(int channel);

*/
// 服务器端：开启本地视频预览
void api_local_preview_on( int channel );

// 服务器端：关闭本地视频预览
void api_local_preview_off( int channel );

// 服务器端：选择“一个通道”，”单播发送“到指定“客户端地址”，选择“发送时间”
void api_capture_unicast_on(int channel, int client_ip, int period );

// 服务器端：关闭发送通道
void api_capture_unicast_off(int channel);

// 服务器端：选择“一个通道”，”组播发送“到指定“客户端地址”，选择“发送时间”
void api_capture_multicast_on(int channel, int client_ip, int period);

// 服务器端：关闭发送通道
void api_capture_multicast_off(int channel);

#endif

