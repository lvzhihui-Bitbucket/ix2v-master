
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <getopt.h>
#include <stdio.h>
#include <errno.h>
#include <signal.h>
#include <ctype.h>
#include <stdlib.h>
#include <dirent.h>
#include <sys/stat.h>
#include <sys/ioctl.h>

#include "OSTIME.h"
#include "obj_ak_vi_camera.h"
#include "elog.h"
#include "obj_IoInterface.h"
#include "task_Shell.h"

int sensor_adjust_initial(void);
void CmrDevManager_init(void);

static SENSOR_IIC_PARA para_set;

static int sensor_para_type     =  SENSOR_TYPE_DC7120; //0;
static int sensor_para_fd       = -1;
static int sensor_crop_x     =  0;
static int sensor_crop_y     =  0;

#define VIDEO_DISPLAY_BRIGHT    "VIDEO_DISPLAY_BRIGHT"
#define VIDEO_DISPLAY_COLOR     "VIDEO_DISPLAY_COLOR"
#define VIDEO_DISPLAY_CONTRACT     "VIDEO_DISPLAY_CONTRACT"

int sensor_type_is_hm1246(void)
{
        return (sensor_para_type == SENSOR_TYPE_HM1246)?1:0;
}

int sensor_type_is_dc7160(void)
{
        return (sensor_para_type == SENSOR_TYPE_DC7160)?1:0;
}

int sensor_type_is_dc7120(void)
{
        return (sensor_para_type == SENSOR_TYPE_DC7120)?1:0;
}

int sensor_type_is_t527(void)
{
        return (sensor_para_type == SENSOR_TYPE_T527)?1:0;
}

int sensor_type_is_tw9912(void)
{
        return (sensor_para_type == SENSOR_TYPE_TW9912)?1:0;
}

int get_sensor_crop_x(void)
{
        return sensor_crop_x;
}

int get_sensor_crop_y(void)
{
        return sensor_crop_y;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////
#define SDCARD_SENSOR_TYPE_FILE		"/mnt/sdcard/sensor_type"

const char* sensor_type_descriptor[] = 
{
	"auto type",
	"1-dc7120",
	"2-dc7122",
	"3-dc7610",
};

const char* sensor_isp_file[] = 
{
	"isp_dc7120_dvp-H1V1P1.conf",
	"isp_dc7120_dvp-H1V1P1.conf",
	"isp_hm1246_dvp-H1V0P0.conf",
	"isp_avs7610_dvp-H1V1P1.conf",
};

const char* sensor_module_file[] = 
{
	"sensor_dc7120.ko",
	"sensor_dc7120.ko",
	"sensor_hm1246.ko",
	"sensor_avs7610.ko",
};

const int sensor_prefined_type[] =
{
        0,
        SENSOR_TYPE_DC7120,
        SENSOR_TYPE_HM1246,
        SENSOR_TYPE_DC7160,
};

int sensor_type_from_prefined(int prefined)
{
        int i;
        for( i = 0; i < sizeof(sensor_prefined_type)/sizeof(int); i++ )
        {
                if( prefined == sensor_prefined_type[i])
                        return i;       
        }
        return -1;
}

int read_sd_sensor_type(void)
{
        int file;
        int len,sensor_type;
        char buffer[100+1];
        file = open(SDCARD_SENSOR_TYPE_FILE, O_RDONLY);
	if(file > 0)
	{		
		len = read(file, buffer, 100);
		if( len < 0 )
		{
			printf("Read sdcard sensor type error!\n");
			close(file);
			return 0;
		}
		else
		{
			buffer[len] = '\0';
			printf("Read sdcard sensor type %s\n",buffer);
			if( buffer[0] <= '3')
			{
				sensor_type = buffer[0]-'0';
				printf("Read sdcard sensor type is [%s]\n",sensor_type_descriptor[sensor_type]);
				return sensor_type;
			}
			else
			{
				printf("Read sdcard sensor type UNKOWN!\n");
				return 0;
			}
		}
	}
	else
	{
		printf("CAN NOT Open sdcard sensor type file!\n");
		return 0;
	}
}

#define IO_SENSOR_TYPE	        "IO_SENSOR_TYPE"
#define IO_SENSOR_PWR_TIME	"IO_SENSOR_PWR_TIME"

int read_io_sensor_type(void)
{
        return API_Para_Read_Int(IO_SENSOR_TYPE);

}

extern int MAX_VI_STATIC_TIME;
// IO PARA: 0 / power on always， 1: power off at once, x: set time
int read_io_sensor_pwr_off(void)
{
        int power_on_time = API_Para_Read_Int(IO_SENSOR_PWR_TIME);
        if( power_on_time == -1 || !power_on_time ) 
                MAX_VI_STATIC_TIME = 2628000;           // always on 5 years
        else if(  power_on_time == 1 )
                MAX_VI_STATIC_TIME = 0;                 // power off at once
        else
                MAX_VI_STATIC_TIME = power_on_time;     // set time
        printf("=====read_io_sensor_pwr_time[%d]===========\n",MAX_VI_STATIC_TIME);
        return MAX_VI_STATIC_TIME;
}

static int cur_sensor_type;
static int sensor_probe_state = 0;
static int sensor_ini_state = 0;
/*
input:
        type: check sensor type
return:
        0: this type sensor HAVE NO INSMOD
        1: this type sensor HAVE INSMODDED
        2: this type sensor HAVE INSMODDED AND OPENNED
        3: other type sensor HAVE INSMODDED
        4: other type sensor HAVE INSMODDED AND OPENNED
        -1: current sensor-para open error
*/
int probe_sensor_module(int type)
{
#if defined(PID_IX850)|| defined(PID_IX611) || defined(PID_IX622)||defined(PID_IX821)
	char module_name[200];
        if( access("/dev/sensor_para",F_OK) != -1 )
        {
                if( sensor_para_fd != -1 ) 
                {
                        cur_sensor_type = sensor_type_from_prefined(sensor_para_type);
                        if( cur_sensor_type == type )
                        {
                                printf("the sensor [%d] alreay insmod ok and openned!\n",type);
                                return 2;       // this module insmoded and openned
                        }
                        else
                        {
                                printf("other sensor [%d] alreay insmod ok and openned!\n",cur_sensor_type);
                                return 4;       // other module insmoded and openned
                        }
                }
                else
                {       
                        if( sensor_para_open() == 0 )
                        {
                                cur_sensor_type = sensor_type_from_prefined(sensor_para_type);
                                sensor_para_close();
                                if( cur_sensor_type == type )
                                {
                                        printf("the sensor [%d] alreay insmod ok and NOT openned!\n",type);
                                        return 1;       // insmoded and NOT openned
                                }
                                else
                                {
                                        sensor_para_close();
                                        printf("the sensor [%d] is NOT same as current module[%d]]!\n",type, cur_sensor_type);
                                        return 3;
                                }
                        }
                        else
                        {
                                printf("ONE SNESOR MODULE IS INSMODED, SENSOR-PARA IS OPENNED ERROR!!\n");
                                return -1;
                        }
                }
        }
        else
        {
                printf("THE SNESOR MODULE[%d] IS NOT INSMOED!!\n",type);
                return 0;
        }
#endif
}

/*
ptype   - current sensor type
        1: dc7121
        2: dc7122
        3: dc7610
pstate  - current sensor state
        0: this type sensor HAVE NO INSMOD
        1: this type sensor HAVE INSMODDED
        2: this type sensor HAVE INSMODDED AND OPENNED
        3: other type sensor HAVE INSMODDED
        4: other type sensor HAVE INSMODDED AND OPENNED
        -1: current sensor-para open error
ini_err - current sensor inierr
*/
int link_sensor_module(int* ptype, int* pstate, int* ini_err)
{  
#if defined(PID_IX850)|| defined(PID_IX611) || defined(PID_IX622)||defined(PID_IX821)
        int probe_state;
        int temp_type = read_io_sensor_type();
        probe_state = probe_sensor_module(temp_type);
        // check result
        if( probe_state == 1 || probe_state == 2 )
        {
                if( ptype != NULL ) *ptype = temp_type;
                if( pstate != NULL ) *pstate = probe_state;
                if( ini_err != NULL ) *ini_err = sensor_ini_state;
                return 0;
        }
        else if( probe_state == 3 || probe_state == 4 )
        {
                if( ptype != NULL ) *ptype = cur_sensor_type;                
                if( pstate != NULL ) *pstate = probe_state;
                if( ini_err != NULL ) *ini_err = sensor_ini_state;
                return 0;
        }
        else if( probe_state == 0 )
        {
                if( ptype != NULL ) *ptype = 0;    
                if( pstate != NULL ) *pstate = probe_state;
                if( ini_err != NULL ) *ini_err = 0;
                return 0;
        }
        else
        {
                return -1;
        }
#endif        
}

int insmod_sensor_module(int sensor_type)
{
#if defined(PID_IX850)|| defined(PID_IX611) || defined(PID_IX622)||defined(PID_IX821)
        int try_times = 0;
	char module_name[200];

        if( sensor_type >= 4)
        {
                printf("input sensor type error!\n");
                return -1;
        }

        sensor_probe_state = probe_sensor_module(sensor_type);
        // this module tyoe have insmodded
        if( sensor_probe_state == 1 || sensor_probe_state == 2 )
        {
                printf("the sensor module have insmod ok!\n");
                return 0;
        }
        // rmmod old module
        if( sensor_probe_state == 3 || sensor_probe_state == 4 )
        {
                rmmod_sensor_module(cur_sensor_type);
                printf("old sensor module rmmod ok!\n");
        }
        // insmod new module
        if( sensor_type >= sizeof(sensor_module_file)/sizeof(char*) ) sensor_type = 0;
        snprintf( module_name, 200, "insmod /usr/modules/%s",sensor_module_file[sensor_type]);
        while( ++try_times <= 3 )
        {
                system("insmod /usr/modules/ak_isp.ko");
                system(module_name);
                usleep(10*1000);
                if( access("/dev/sensor_para",F_OK) != -1 )
                {
                        printf("insmod_sensor_module[%d] ok!\n",sensor_type);
                        return 0;
                        //break;
                }
                else
                {
                        printf("insmod_sensor_module[%d] er!\n",sensor_type);
                }
        }
        return -1;
#endif        
}

void vi_dev_close_forced(void);
int rmmod_sensor_module(int sensor_type)
{
#if defined(PID_IX850)|| defined(PID_IX611) || defined(PID_IX622)||defined(PID_IX821)
        int try_times = 0;
	char module_name[200];
        if( sensor_type >= 4)
        {
                printf("input sensor type error!\n");
                return -1;
        }
        // force close vi dev
        vi_dev_close_forced();        
	if( sensor_type >= sizeof(sensor_module_file)/sizeof(char*) ) sensor_type = 0;
	snprintf( module_name, 200, "rmmod /usr/modules/%s",sensor_module_file[sensor_type]);
        while( ++try_times <= 3 )
        {
                system("rmmod /usr/modules/ak_isp.ko");
                system(module_name);
                usleep(10*1000);
                if( access("/dev/sensor_para",F_OK) == -1 )
                {
                        printf("rmmod_sensor_module[%d] ok!\n",sensor_type);
                        return 0;
                }
                else
                {
                        printf("rmmod_sensor_module[%d] er!\n",sensor_type);
                }
        }
        return -1;
#endif        
}

int reset_sensor_module(void)
{
        int temp_type = read_io_sensor_type();
        rmmod_sensor_module(temp_type);
        return insmod_sensor_module(temp_type);
}
///////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @fn:		sensor_para_open
 *
 * @brief:	开启sensor参数调节设备，开启摄像头设备调用
 *
 * @param:	none
 *
 * @return:     -1/err, 0/ok
 */
int sensor_para_open(void)
{
        sensor_para_fd = open("/dev/sensor_para",O_RDWR);
        if( sensor_para_fd == -1 )
        {
                printf("error open\n");
                return -1;
        }
        else
        {
                // initial sensor type
                SENSOR_CHECK_TYPE para_get;
                if( ioctl( sensor_para_fd, SENSOR_TYPE_GET, (unsigned long)&para_get ) == 0 )
                {
                        sensor_para_type = para_get.type;
                        sensor_crop_x = (para_get.rev>>16)&0xff;
                        sensor_crop_y = para_get.rev&0xff;
			//printf("SENSOR_TYPE_GET ok[%d],crpy[x=%d,y=%d]\n", sensor_para_type,sensor_crop_x,sensor_crop_y); 
                        if( para_get.magic == MAGIC_FIXED )
                        {
                                sensor_ini_state = para_get.ext;        // i2c state
			        //printf("SENSOR_TYPE_GET I2C STATE[%d]\n", sensor_ini_state); 
                        }
                        else
                                sensor_ini_state = 0;
                        
		} 
		else 
		{  
			printf("SENSOR_TYPE_GET failed!: %s\n", strerror(errno));  
			return -1;  
		}  
        }
        return 0;
}
// 
int sensor_probe_status(void)
{
        int sensor_open_org;
        int sensor_result = -1;
        if( sensor_para_fd == -1 )
        {
                sensor_para_fd = open("/dev/sensor_para",O_RDWR);
                if( sensor_para_fd == -1 )
                {
                        printf("error open\n");
                        return -1;
                }
                sensor_open_org = 0;
        }
        else
                sensor_open_org = 1;
        // initial sensor type
        SENSOR_CHECK_TYPE para_get;
        if( ioctl( sensor_para_fd, SENSOR_TYPE_GET, (unsigned long)&para_get ) == 0 )
        {
                sensor_para_type = para_get.type;
                sensor_crop_x = (para_get.rev>>16)&0xff;
                sensor_crop_y = para_get.rev&0xff;
                //printf("SENSOR_TYPE_PROBE[%d],crpy[x=%d,y=%d]\n", sensor_para_type,sensor_crop_x,sensor_crop_y); 
                if( para_get.magic == MAGIC_FIXED )
                {
                        sensor_ini_state = para_get.ext;        // i2c state
                        //printf("SENSOR_TYPE_GET I2C STATE[%d]\n", sensor_ini_state); 
                }
                else
                        sensor_ini_state = 0;
                sensor_result = sensor_ini_state;                
        } 
        else 
                printf("SENSOR_TYPE_GET failed!: %s\n", strerror(errno));  
        // restore
        if( sensor_open_org == 0) 
        {
                close(sensor_para_fd);
                sensor_para_fd = -1;
        }
        return sensor_result;
}

/**
 * @fn:		sensor_para_close
 *
 * @brief:	关闭sensor参数调节设备，关闭摄像头设备时调用
 *
 * @param:	none
 *
 * @return:     -1/err, 0/ok
 */
int sensor_para_close(void)
{
        if( sensor_para_fd )
        {
                close(sensor_para_fd);
                sensor_para_fd = -1;
        }
        return 0;
}

/**
 * @fn:		sensor_para_adjust
 *
 * @brief:	sensor参数调节
 *
 * @param:	page    - 页地址
 * @param:	reg     - 寄存器地址
 * @param:	value   - 数据
 *
 * @return:     -1/err, 0/ok
 */
int sensor_para_write(int page, int reg, int value)
{
        if( sensor_para_fd == -1 )
        {
                printf("sensor para device have no openned\n");
                return -1;
        }
        para_set.page  = page;
        para_set.reg   = reg;
        para_set.value = value;

        printf("------ set sensor para, page[0x%02x],reg[0x%02x],value[0x%02x] ------\n",para_set.page,para_set.reg,para_set.value);

        ioctl( sensor_para_fd, SENSOR_PARA_SET, (unsigned long)&para_set );
        return 0;
}

int sensor_para_read(int page, int reg)
{
        SENSOR_IIC_PARA para_get;

        if( sensor_para_fd == -1 )
        {
                printf("sensor para device have no openned\n");
                return -1;
        }

        para_get.page  = page;
        para_get.reg   = reg;
        para_get.value = 0;

        printf("------ get sensor para, page[0x%02x],reg[0x%02x],value[0x%02x] ------\n",para_get.page,para_get.reg,para_get.value);

        if( ioctl( sensor_para_fd, SENSOR_PARA_GET, (unsigned long)&para_get ) == 0 )
        {
                printf("get sensor para[%d]\n", para_get.value);
                return para_get.value;
        } 
        else 
        {  
                printf("get sensor para failed: %s\n", strerror(errno));  
                return -1;  
        }  
}

/**
 * @fn:		sensor_cvbs_switch
 *
 * @brief:	sensor参数调节
 *
 * @param:	mode    - 0/bt601, 1/cvbs
 *
 * @return:     -1/err, 0/ok
 */
int sensor_cvbs_switch(int mode)
{
        return 0;
}

// T527 para
/*
Page0:
{0x68,0x88},// contrast
{0x69,0x80},// brightness
{0x6A,0x00},// hue sin
{0x6B,0x7F},// hue cos RONLENK    //lyx_20171219 0x60->0x7F
{0x6C,0x80},// chrome saturation
*/
#define T527_SENSOR_BRIGHT_REG          0x69
#define T527_SENSOR_COLOR_REG           0x6C
#define T527_SENSOR_CONTRAST_REG        0x68

const int T527_SENSOR_BRIGHT_STEP_TAB[10] =
{        
        0x60, 0x70, 0x78, 0x7C, 0x80, 0x88, 0x90, 0x98, 0xA0, 0xB0
};

const int T527_SENSOR_COLOR_STEP_TAB[10] =
{        
        0x60, 0x70, 0x78, 0x7C, 0x80, 0x88, 0x90, 0x98, 0xA0, 0xB0
};

const int T527_SENSOR_CONTRAST_STEP_TAB[10] =
{        
        0x60, 0x70, 0x78, 0x7C, 0x80, 0x88, 0x90, 0x98, 0xA0, 0xB0
};

// TW9912 para
/*
Page0:
{0x68,0x88},// contrast
{0x69,0x80},// brightness
{0x6A,0x00},// hue sin
{0x6B,0x7F},// hue cos RONLENK    //lyx_20171219 0x60->0x7F
{0x6C,0x80},// chrome saturation
*/
#define TW9912_SENSOR_BRIGHT_REG          0x10
#define TW9912_SENSOR_COLOR_U_REG         0x13 // 0x14(chrome v,default 0x80) 0x13(chrome u,defaule 0x80)
#define TW9912_SENSOR_COLOR_V_REG         0x14 // 0x14(chrome v,default 0x80) 0x13(chrome u,defaule 0x80)
#define TW9912_SENSOR_CONTRAST_REG        0x11

const int TW9912_SENSOR_BRIGHT_STEP_TAB[10] =
{        
        0xA0, 0x98, 0x90, 0x88, 0x80, 0x00, 0x08, 0x10, 0x18, 0x20
};

const int TW9912_SENSOR_COLOR_U_STEP_TAB[10] =
{        
        0x60, 0x70, 0x78, 0x7C, 0x80, 0x88, 0x90, 0x98, 0xA0, 0xB0
};

const int TW9912_SENSOR_COLOR_V_STEP_TAB[10] =
{        
        0x60, 0x70, 0x78, 0x7C, 0x80, 0x88, 0x90, 0x98, 0xA0, 0xB0
};

const int TW9912_SENSOR_CONTRAST_STEP_TAB[10] =
{        
        0x04, 0x14, 0x24, 0x34, 0x54, 0x64, 0x74, 0x84, 0x94, 0xA4
};

// DC7120 para
#define DC7120_SENSOR_BRIGHT_REG          0xD4C0
#define DC7120_SENSOR_COLOR_REG           0xD480
#define DC7120_SENSOR_CONTRAST_REG        0xD725        // is sharpness

const int DC7120_SENSOR_BRIGHT_STEP_TAB[10] =
{        
        0x88, 0x00, 0x08, 0x10, 0x18, 0x20, 0x28, 0x30, 0x40, 0x50
};

const int DC7120_SENSOR_COLOR_STEP_TAB[10] =
{        
        0x30, 0x38, 0x40, 0x48, 0x50, 0x58, 0x60, 0x70, 0x80, 0x90, 0xA0 
};

const int DC7120_SENSOR_CONTRAST_STEP_TAB[10] =        // is sharpness
{        
        0x08, 0x09, 0x0A, 0x12, 0x15, 0x18, 0x1A, 0x20, 0x26, 0x28
};

int sensor_para_set_brightness(int brightness)
{
        if( sensor_para_type == SENSOR_TYPE_T527 )
        {
                sensor_para_write(T527_PAGE0, T527_SENSOR_BRIGHT_REG, T527_SENSOR_BRIGHT_STEP_TAB[brightness]);
        }
        else if( sensor_para_type == SENSOR_TYPE_TW9912 )
        {
                sensor_para_write(0x00, TW9912_SENSOR_BRIGHT_REG, TW9912_SENSOR_BRIGHT_STEP_TAB[brightness]);
        }
        else if( sensor_para_type == SENSOR_TYPE_DC7120 )
        {
                sensor_para_write(0x00, DC7120_SENSOR_BRIGHT_REG, DC7120_SENSOR_BRIGHT_STEP_TAB[brightness]);
        }
        //API_Para_Write_Int(VIDEO_DISPLAY_BRIGHT,brightness);
        printf("sensor_brightness_adjust->[%02x]",brightness);
        return brightness;
}

int sensor_para_set_color(int color)
{
        if( sensor_para_type == SENSOR_TYPE_T527 )
        {
                sensor_para_write(T527_PAGE0, T527_SENSOR_COLOR_REG, T527_SENSOR_COLOR_STEP_TAB[color]);
        }
        else if( sensor_para_type == SENSOR_TYPE_TW9912 )
        {
                sensor_para_write(0x00, TW9912_SENSOR_COLOR_U_REG, TW9912_SENSOR_COLOR_U_STEP_TAB[color]);
                sensor_para_write(0x00, TW9912_SENSOR_COLOR_V_REG, TW9912_SENSOR_COLOR_V_STEP_TAB[color]);
        }
        else if( sensor_para_type == SENSOR_TYPE_DC7120 )
        {
                sensor_para_write(0x00, DC7120_SENSOR_COLOR_REG, DC7120_SENSOR_COLOR_STEP_TAB[color]);
        }
        //API_Para_Write_Int(VIDEO_DISPLAY_COLOR,color);
        printf("sensor_color_adjust->[%02x]",color);
        return color;
}

int sensor_para_set_contrast(int contrast)
{
        if( sensor_para_type == SENSOR_TYPE_T527 )
        {
                sensor_para_write(T527_PAGE0, T527_SENSOR_CONTRAST_REG, T527_SENSOR_CONTRAST_STEP_TAB[contrast]);
        }
        else if( sensor_para_type == SENSOR_TYPE_TW9912 )
        {
                sensor_para_write(0x00, TW9912_SENSOR_CONTRAST_REG, TW9912_SENSOR_CONTRAST_STEP_TAB[contrast]);
        }
        else if( sensor_para_type == SENSOR_TYPE_DC7120 )
        {
                sensor_para_write(0x00, DC7120_SENSOR_CONTRAST_REG, DC7120_SENSOR_CONTRAST_STEP_TAB[contrast]);
        }
        printf("sensor_contrast_adjust->[%02x]",contrast);
        return contrast;
}
// 调试接口（保留） eg: sensor debug r/w 0x00 0xd725 0x08
int sensor_para_debug_r(int addr, int reg_start, int num, char* pdat)
{
        int i;
        for( i = 0; i < num; i++ )
        {
                pdat[i] = sensor_para_read(addr, reg_start+i);
        }
        return 0;
}
int sensor_para_debug_w(int addr, int reg_start, int num, char* pdat)
{
        int i;
        for( i = 0; i < num; i++ )
        {
                sensor_para_write(addr, reg_start+i, pdat[i]);
        }
        return 0;
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

#define MAX_BRIGHTNESS_STEP       9             // 亮度最大等级
#define MIN_BRIGHTNESS_STEP       0             // 亮度最小等级
#define GAP_BRIGHTNESS_STEP       1             // 亮度调节步进

#define MAX_COLOR_STEP            9             // 色度最大等级
#define MIN_COLOR_STEP            0             // 色度最小等级
#define GAP_COLOR_STEP            1             // 色度调节步进

#define MAX_CONTRAST_STEP         9             // 对比度最大等级
#define MIN_CONTRAST_STEP         0             // 对比度最小等级
#define GAP_CONTRAST_STEP         1             // 对比度调节步进

static int brightness, color, contrast;

/**
 * @fn:		sensor_adjust_initial
 *
 * @brief:	参数初始化：开启sensor后从io文件读取当前亮度、色度参数
 *
 * @param:	none
 *
 * @return:     -1/err, 0/ok
 */
int sensor_adjust_initial(void)
{
        // brightness
        brightness = API_Para_Read_Int(VIDEO_DISPLAY_BRIGHT);
        //printf("sensor_adjust_initial(BRIGHT) = %d\n", brightness);
        // color
        color = API_Para_Read_Int(VIDEO_DISPLAY_COLOR);
        //printf("sensor_adjust_initial(COLOR) = %d\n", color);
        // contrast
        contrast = API_Para_Read_Int(VIDEO_DISPLAY_CONTRACT);
        //printf("sensor_adjust_initial(CONTRACT) = %d\n", contrast);
        //contrast = 5;

        //initial
        sensor_para_set_brightness(brightness);
        sensor_para_set_color(color);
        sensor_para_set_contrast(contrast);
	return 0;
}

/**
 * @fn:		sensor_brightness_adjust
 *
 * @brief:	亮度参数调节
 *
 * @param:	inc - 0/decrease, 1/increase
 *
 * @return:     -1/err, 0/ok
 */
int sensor_brightness_adjust(int inc)
{
        char temp[20];
        if( inc )
        {
                brightness += GAP_BRIGHTNESS_STEP; if( brightness > MAX_BRIGHTNESS_STEP) brightness = MIN_BRIGHTNESS_STEP;
        }
        else
        {
                if( brightness <= MIN_BRIGHTNESS_STEP ) brightness = MAX_BRIGHTNESS_STEP; else brightness -= GAP_BRIGHTNESS_STEP;
        }
        return sensor_para_set_brightness(brightness);
}

/**
 * @fn:		sensor_color_adjust
 *
 * @brief:	色度参数调节
 *
 * @param:	inc - 0/decrease, 1/increase
 *
 * @return:     -1/err, 0/ok
 */
int sensor_color_adjust(int inc)
{
        char temp[20];
        if( inc )
        {
                color += GAP_COLOR_STEP; if( color > MAX_COLOR_STEP) color = MIN_COLOR_STEP;
        }
        else
        {
                if( color <= MIN_COLOR_STEP ) color = MAX_COLOR_STEP; else color -= GAP_COLOR_STEP;
        }
        return sensor_para_set_color(color);
}

/**
 * @fn:		sensor_contrast_adjust
 *
 * @brief:	对比度参数调节
 *
 * @param:	inc - 0/decrease, 1/increase
 *
 * @return:     -1/err, 0/ok
 */
int sensor_contrast_adjust(int inc)
{
        if( inc )
        {
                contrast += GAP_CONTRAST_STEP; if( contrast > MAX_CONTRAST_STEP) contrast = MIN_CONTRAST_STEP;
        }
        else
        {
                if( contrast <= MIN_CONTRAST_STEP ) contrast = MAX_CONTRAST_STEP; else contrast -= GAP_CONTRAST_STEP;
        }
        return sensor_para_set_contrast(contrast);
}

/**
 * @fn:		sensor_contrast_adjust
 *
 * @brief:	对比度参数调节
 *
 * @param:	pbr - 亮度参数指针
 * @param:	pcl - 色度参数指针
 * @param:	pco - 对比度参数指针
 *
 * @return:     -1/err, 0/ok
 */
int get_sensor_cur_para(int* pbr, int* pcl, int* pco)
{
        *pbr = brightness;
        *pcl = color;
        *pco = contrast;

        printf("get current sensor orig para, brightness[%02x],color[%02x],contrast[%02x]",brightness,color,contrast);

        return 0;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
int sensor_is_power_off(void);
// usage: sensor bright/color/contrast inc/dec/0-9
// eg:  sensor bright inc, sensor  bright dec, sensor  bright 0-9
// eg: sensor color inc, sensor color dec, color 0-9
// eg: sensor contrast inc, sensor contrast dec, sensor contrast 0-9
// eg: sensor debug r addr[hex] reg[hex] value[hex]
// eg: sensor debug w addr[hex] reg[hex] value[hex]
int ShellCmd_sensor_adjust(cJSON *cmd)
{
        int sensor_type,sensor_state,sensor_ini;
    char pdat[200];
    char* str_type = GetShellCmdInputString(cmd, 1);
    char* str_mode = GetShellCmdInputString(cmd, 2);
    printf("sensor adjust type[%s] - mode[%s]\n",str_type,str_mode);

    if( strcmp(str_type,"bright") == 0 )
    {
        if( strcmp(str_mode,"inc") == 0 )
                sensor_brightness_adjust(1);
        else if( strcmp(str_mode,"dec") == 0 )
                sensor_brightness_adjust(0);
        else
                brightness = sensor_para_set_brightness(atoi(str_mode));
        ShellCmdPrintf("sensor brightness level[%d]=%02x\n",brightness,para_set.value);
    }
    else if( strcmp(str_type,"color") == 0 )
    {
        if( strcmp(str_mode,"inc") == 0 )
                sensor_color_adjust(1);
        else if( strcmp(str_mode,"dec") == 0 )
                sensor_color_adjust(0);
        else
                color = sensor_para_set_color(atoi(str_mode));
        ShellCmdPrintf("sensor color level[%d]=%02x\n",color,para_set.value);
    }
    else if( strcmp(str_type,"contrast") == 0 )
    {
        if( strcmp(str_mode,"inc") == 0 )
                sensor_contrast_adjust(1);
        else if( strcmp(str_mode,"dec") == 0 )
                sensor_contrast_adjust(0);
        else
                contrast = sensor_para_set_contrast(atoi(str_mode));
        ShellCmdPrintf("sensor contrast level[%d]=%02x\n",contrast,para_set.value);
    }
    else if( strcmp(str_type,"debug") == 0 )
    {
        // all hex16
        unsigned int add,reg;
        char* str_add = GetShellCmdInputString(cmd, 3);
        char* str_reg = GetShellCmdInputString(cmd, 4);
        char* str_dat = GetShellCmdInputString(cmd, 5);
        add = strtol(str_add,NULL,16);
        reg = strtol(str_reg,NULL,16);
        pdat[0] = strtol(str_dat,NULL,16);
        if( strcmp(str_mode,"r") == 0 )
        {
                sensor_para_debug_r(add,reg,1,pdat);
                ShellCmdPrintf("%d",pdat[0]);
        }
        else if( strcmp(str_mode,"w") == 0 )
        {
                sensor_para_debug_w(add,reg,1,pdat);
                ShellCmdPrintf("OK");
        }
        else
        {
                ShellCmdPrintf("ERR\n");
        }
        }
        else if(strcmp(str_type,"re_insmod")==0)
        {
                system("rmmod ak_isp");
                system("insmod /usr/modules/ak_isp.ko");
                ShellCmdPrintf("re_insmod");
        }
        // lzh_20240311_s
        else if(strcmp(str_type,"link")==0)
        {
                if( link_sensor_module(&sensor_type,&sensor_state,&sensor_ini) == 0 )
                {
                        // sensor power off status 2, power on i2c ok is 0, err is 1
                        if( sensor_is_power_off() )
                                sensor_ini = 2;
                        else
                                sensor_ini = sensor_probe_status();
                        ShellCmdPrintf("sensor link ok, type[%d],state[%d],ini[%d]\n[help]\ntype[1:7121,2:7122,3:7610,0:auto]\nstate[0:NOT INSMOD,1:FIXED INSMOD,2:FIXED OPENNED,3:UNFIXED INSMOD,4:UNFIXED OPENNED]\n\ini[0:ok,1:err,2:power off]\n",\
                                        sensor_type,sensor_state,sensor_ini);
                }
                else
                {
                        ShellCmdPrintf("sensor link failed!\r\n");
                }
        }
        else if(strcmp(str_type,"insmod")==0)           // ！！！NORMAL NOT TO USE，TO CHANGE IO PARA FOR CHANGE SENSOR TYPE
        {
                pdat[0] = strtol(str_mode,NULL,10);
                if( insmod_sensor_module(pdat[0]) == 0 )
                {
                        ShellCmdPrintf("sensor insmod %s ok\n[help]\ntype[1:7121,2:7122,3:7610]\n",str_mode);
                }
                else
                {
                        ShellCmdPrintf("sensor insmod %s failed!\n[help]\ntype[1:7121,2:7122,3:7610]\n",str_mode);
                }
        }
        else if(strcmp(str_type,"rmmod")==0)
        {
                pdat[0] = strtol(str_mode,NULL,10);
                if( rmmod_sensor_module(pdat[0]) == 0 )
                {
                        ShellCmdPrintf("sensor insmod %s ok\n[help]\ntype[1:7121,2:7122,3:7610]\n",str_mode);
                }
                else
                {
                        ShellCmdPrintf("sensor insmod %s failed!\n[help]\ntype[1:7121,2:7122,3:7610]\n",str_mode);
                }
        }
        else if(strcmp(str_type,"frame")==0)
        {
                CmrDevManager_init();
                ShellCmdPrintf("sensor get one frame, result in PB CmrDevState\n");
        }
        else if( strcmp(str_type,"reset")==0)
        {
                if( reset_sensor_module() == 0 )
                        ShellCmdPrintf("sensor reset successful!\n");
                else
                        ShellCmdPrintf("sensor reset failed!\n");                 
        }
        // lzh_20240311_e
        else
        {
                ShellCmdPrintf(">>>>>>>>>>>ShellCmd_sensor_adjust para err<<<<<<<<<<\n");
        }
}
