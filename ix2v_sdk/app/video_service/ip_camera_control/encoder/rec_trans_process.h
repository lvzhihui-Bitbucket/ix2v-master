
#ifndef _REC_TRANS_PROCESS_H
#define _REC_TRANS_PROCESS_H

/**
 * @fn:		api_rec_one_file_start
 *
 * @brief:	start one record 
 *
 * @param:	channel		    - channel index (0-2)
 * @param:	file_type	    - 0-h264,1-h265
 * @param:	file_name    	- file name
 * @param:	period	        - record time
 *
 * @return: -1/err, 0/ok
 */
int api_rec_one_file_start( int channel, int file_type, char* file_name, int period,void *call_back);

/**
 * @fn:		api_rec_one_file_stop
 *
 * @brief:	stop one record 
 *
 * @param:	channel		    - channel index (0-2)
 * @param:	file_type	    - 0-h264,1-h265
 * @param:	file_name    	- file name
 *
 * @return: -1/err, 0/ok
 */
int api_rec_one_file_stop( int channel, int file_type, char* file_name );

/**
 * @fn:		cb_local_ffmpeg_rec_init
 *
 * @brief:	start ffmpeg recording
 *
 * @param:	ch    			- 通道号
 * @param:	file_name    	- file name
 * @param:	period    		- recored time
 * @param:	enc_type	    - 0-h264,1-h265
 * @param:	fps	    		- fps
 * @param:	width    		- image width
 * @param:	height    		- image height
 *
 * @return: -1/err, 0/ok
 */
int cb_local_ffmpeg_rec_init( int ch, char* file_name, int period, int enc_type,  int fps, int width, int height ,void *call_back);

/**
 * @fn:		cb_local_ffmpeg_rec_proc
 *
 * @brief:	get one frame data
 *
 * @param:	ch    	- 通道号
 * @param:	pbuf    - data ptr
 * @param:	size    - data len
 * @param:	pts	    - timestamp
 * @param:	seq_no	- seq number
 * @param:	p0i1    - 0-P frame, 1-I frame
 *
 * @return: -1/err, 0/ok, 1/timeout
 */
int cb_local_ffmpeg_rec_proc( int ch, char* pbuf, int size, unsigned long long pts, unsigned long seq_no, int p0i1 );

/**
 * @fn:		cb_local_ffmpeg_rec_exit
 *
 * @brief:	stop ffmpeg recording
 *
 * @param:	ch    	- 通道号
 *
 * @return: -1/err, 0/ok
 */
int cb_local_ffmpeg_rec_exit( int ch );

#endif

