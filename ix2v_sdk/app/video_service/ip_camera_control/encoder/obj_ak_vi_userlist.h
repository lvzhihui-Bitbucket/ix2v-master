
#ifndef _OBJ_AK_VI_USELIST_H_
#define _OBJ_AK_VI_USELIST_H_

#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <list.h>

typedef void (*subscribe_callback_t)(int channel, char *enc_buff, int enc_len, void* extptr);

typedef struct
{
	struct list_head    	one_node;				// 列表头
	int						channel;				// 发送通道号
	subscribe_callback_t 	subscribe_callback;		// 发送编码帧回调函数指针
	int						subscribe_token;		// 订阅返回的token，作为注销凭证
	int						subscribe_type;			// 订阅的类型：0-udp，1-rtp
} subscribe_node_t;

/**
 * @fn:		api_add_one_subscriber
 *
 * @brief:	生成一个发送节点类型，并添加到指定列表指针
 *
 * @param:	plist 				- 列表指针
 * @param:	subscribe_token 	- 订阅得到的token
 * @param:	subscribe_type 		- 订阅的type
 * @param:	subscribe_callback 	- 发送回调函数指针
 * @param:	channel 			- 发送通道号
 *
 * @return: -1/err, 0/ok
 */
int api_add_one_subscriber(struct list_head *plist, int subscribe_token, int subscribe_type, subscribe_callback_t subscribe_callback, int channel );

/**
 * @fn:		api_del_one_subscriber
 *
 * @brief:	释放一个发送节点类型
 *
 * @param:	plist 				- 列表指针
 * @param:	subscribe_token 	- 需要释放的token
 *
 * @return: -1/err, 0/ok
 */
int api_del_one_subscriber(struct list_head *plist, int subscribe_token );

/**
 * @fn:		api_del_all_subscribers
 *
 * @brief:	释放当前列表的所有发送节点
 *
 * @param:	plist 				- 列表指针
 *
 * @return: -1/err, 0/ok
 */
int api_del_all_subscribers(struct list_head *plist );

/**
 * @fn:		api_callback_subscribers
 *
 * @brief:	指定列表处理数据的回调函数
 *
 * @param:	plist 	- 列表指针
 * @param:	data 	- 待发送数据指针
 * @param:	len 	- 待发送数据长度
 * @param:	extptr 	- 数据扩展指针
 *
 * @return: -1/err, 0/ok
 */
int api_callback_subscribers(struct list_head *plist,char *data, int len, void* extptr);

#endif
