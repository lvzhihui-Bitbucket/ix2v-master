/**
 * Description: 3路视频编码处理
 * Author:      lvzhihui
 * Create:      2012-01-23
*/

#include <stdio.h>
#include <ctype.h>  
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>  
#include <unistd.h>
#include <list.h>

#include "obj_ak_vi_encoder.h"
#include "cJSON.h"
#include "elog_forcall.h"
//#define IX_DS_TEST
#define MAX_ENCODER_CHANNEL     3

typedef struct
{
	int handle;         // 编码器句柄
	int profile;        // 编码profile类型
	int type;           // 编码264-265类型
	int fps;            // 编码帧率
	int bps;        	// 编码码率
	int quality;        // 编码质量

	int res_w;          // 编码分辨率宽度
	int res_h;          // 编码分辨率高度
}AK_ENCODER_T;

AK_ENCODER_T  all_ak_encoder[MAX_ENCODER_CHANNEL] =
{
	// channel 0
	{
		.fps		= 30,
		#if defined(PID_IX850) || defined(IX_DS_TEST)  || defined(PID_IX611) || defined(PID_IX622) ||defined(PID_IX821)
		.bps		= 2048,
		.type		= H264_ENC_TYPE,
		.profile	= PROFILE_HIGH,
		#else
		.bps		= 512,
		.type		= HEVC_ENC_TYPE,
		.profile	= PROFILE_HEVC_MAIN,
		#endif
		.quality	= 35,
		#if 0
		.type		= HEVC_ENC_TYPE,
		.profile	= PROFILE_HEVC_MAIN,
		#endif
		.res_w		= 1280,		
		.res_h		= 720,
		.handle=-1,
	},
	// channel 1
	{
		.fps		= 15,
		
		.quality	= 35,
		//#if	defined(PID_IX850)||defined(IX_DS_TEST) || defined(PID_IX611) || defined(PID_IX622) 
		#if	defined(IX_DS_TEST)
		.bps		= 2048,
		.type		= H264_ENC_TYPE,
		.profile	= PROFILE_BASE,
		#elif defined(PID_IX611)||defined(SUPPORT_IX2_DIVERT) || defined(PID_IX622)||defined(PID_IX850)||defined(PID_IX821)
		.bps		= 400,
		.type		= H264_ENC_TYPE,
		.profile	= PROFILE_HIGH,
		#else
		.bps		= 1024,
		.type		= H264_ENC_TYPE,
		.profile	= PROFILE_HIGH,

		#endif
		
		.res_w	    = 640,		
		.res_h	    = 480,
		.handle=-1,
	},
	// channel 2
	{
		.fps		= 15,
		.bps		= 200,
		.quality	= 35,
		.type		= H264_ENC_TYPE,
		.profile	= PROFILE_HIGH,
		.res_w	    = 320,		
		.res_h	    = 240,
		.handle=-1,
	}
};

void ak_encoder_reso_reset(int ch0_w, int ch0_h, int ch1_w, int ch1_h )
{
	all_ak_encoder[0].res_w = ch0_w;
	all_ak_encoder[0].res_h = ch0_h;
	all_ak_encoder[1].res_w = ch1_w;
	all_ak_encoder[1].res_h = ch1_h;
	all_ak_encoder[2].res_w = ch1_w/2;
	all_ak_encoder[2].res_h = ch1_h/2;
}

/**
 * @fn:		vi_ch_enc_open
 *
 * @brief:	打开编码设备
 *
 * @param:	ptr_enc - 编码实例指针
 *
 * @return: -1/err, 0/ok
 */
static int vi_ch_enc_open(AK_ENCODER_T *ptr_enc)
{
	 int ret = -1;
    /* open venc */
    struct venc_param ve_param;
	
	memset(&ve_param,0,sizeof(struct venc_param));
   
    ve_param.width          = ptr_enc->res_w;       //resolution width
    ve_param.height         = ptr_enc->res_h;       //resolution height
    ve_param.enc_out_type   = ptr_enc->type;        //enc type
    ve_param.profile        = ptr_enc->profile;     //profile
    ve_param.fps            = ptr_enc->fps;         //fps set
    ve_param.initqp         = ptr_enc->quality;    	//qp value
    ve_param.target_kbps    = ptr_enc->bps;    		//bps

    ve_param.goplen         = 3*15;            		//gop set
    ve_param.max_kbps       = 2048;            		//max kbps
    ve_param.br_mode        = BR_MODE_VBR;               		//br mode
    ve_param.minqp          = 25;              		//qp set
    ve_param.maxqp          = 50;              		//qp max value
    ve_param.jpeg_qlevel    = JPEG_QLEVEL_DEFAULT;  //jpeg qlevel
    ve_param.chroma_mode    = CHROMA_4_2_0;         //chroma mode
    ve_param.max_picture_size = 0;                  //0 means default
    ve_param.enc_level        = 30;                 //enc level
    ve_param.smart_mode       = 0;                  //smart mode set
    ve_param.smart_goplen     = 100;                //smart mode value
    ve_param.smart_quality    = 30; //50;                 //quality
    ve_param.smart_static_value = 0;                //value

    /* decode type is h264*/
   //log_d("vi_ch_enc_open1111");
    ret = ak_venc_open(&ve_param, &ptr_enc->handle);
	//log_d("vi_ch_enc_open2222");
	//printf("11111111%s:%d\n",__func__,__LINE__);
    if (ret || (-1 == ptr_enc->handle) )
    {
        ak_print_error_ex(MODULE_ID_APP, "dev open venc failed\n");
        return -1;
    }

    if (ve_param.width > 1920 && ve_param.height > 1080 && ve_param.initqp > 60)
    {
        ak_venc_set_stream_buff(ptr_enc->handle, 1*1024*1024);
    }
    return 0;
}

/**
 * @fn:		vi_ch_enc_close
 *
 * @brief:	关闭编码设备
 *
 * @param:	ptr_enc - 编码实例指针
 *
 * @return: -1/err, 0/ok
 */
static int vi_ch_enc_close(AK_ENCODER_T *ptr_enc)
{
	int ret = 0;
	if( ptr_enc->handle != -1 )
    {
        /* close the venc*/
        ret = ak_venc_close(ptr_enc->handle);
        ptr_enc->handle = -1;
    }
	return ret;
}

/**
 * @fn:		vi_ch_enc_config
 *
 * @brief:	配置编码设备
 *
 * @param:	ptr_enc - 编码实例指针
 * @param:	w       - 编码分辨率宽度
 * @param:	h       - 编码分辨率高度
 * @param:	fps     - 编码帧率
 * @param:	enc_type- 编码264-265类型
 * @param:	profile - 编码profile类型
 *
 * @return: -1/err, 0/ok
 */
static int vi_ch_enc_config(AK_ENCODER_T *ptr_enc, int w, int h, int fps, int enc_type, int profile )
{
    ptr_enc->fps        = fps;
    ptr_enc->type       = enc_type;
    ptr_enc->profile    = profile;
    ptr_enc->res_w      = w;
    ptr_enc->res_h      = h;
    return 0;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @fn:		api_vi_ch_enc_open
 *
 * @brief:	打开编码设备
 *
 * @param:	channel - 编码通道号
 *
 * @return: -1/err, 0/ok
 */
int api_vi_ch_enc_open(int channel)
{
    if( channel >= MAX_ENCODER_CHANNEL )
        return -1;
    AK_ENCODER_T *ptr_enc = &all_ak_encoder[channel];
	if(ptr_enc->handle==-1)
    vi_ch_enc_open(ptr_enc);
    return 0;
}

/**
 * @fn:		api_vi_ch_enc_get_handle
 *
 * @brief:	获取编码通道的句柄
 *
 * @param:	channel - 编码通道号
 *
 * @return: -1/err, 0/ok
 */
int api_vi_ch_enc_get_handle(int channel)
{
    if( channel >= MAX_ENCODER_CHANNEL )
        return -1;
    return all_ak_encoder[channel].handle;
}

/**
 * @fn:		api_vi_ch_enc_get
 *
 * @brief:	获取编码通道的配置参数
 *
 * @param:	channel - 编码通道号
 * @param:	w       - 编码分辨率宽度指针
 * @param:	h       - 编码分辨率高度指针
 * @param:	fps     - 编码帧率指针
 * @param:	enc_type- 编码264-265类型指针
 * @param:	profile - 编码profile类型指针
 *
 * @return: -1/err, 0/ok
 */
int api_vi_ch_enc_get(int channel, int *w, int *h, int *fps, int *enc_type, int *profile)
{
    if( channel >= MAX_ENCODER_CHANNEL )
        return -1;
    AK_ENCODER_T *ptr_enc = &all_ak_encoder[channel];
    if( w )         *w   = ptr_enc->res_w;
    if( h )         *h   = ptr_enc->res_h;
    if( fps )       *fps = ptr_enc->fps;
    if( enc_type )  *enc_type   = ptr_enc->type;
    if( profile )   *profile    = ptr_enc->profile;
    return 0;
}

/**
 * @fn:		api_vi_ch_enc_set
 *
 * @brief:	设置编码通道的配置参数
 *
 * @param:	channel - 编码通道号
 * @param:	w       - 编码分辨率宽度
 * @param:	h       - 编码分辨率高度
 * @param:	fps     - 编码帧率
 * @param:	enc_type- 编码264-265类型
 * @param:	profile - 编码profile类型
 *
 * @return: -1/err, 0/ok
 */
int api_vi_ch_enc_set(int channel, int w, int h, int fps, int enc_type, int profile)
{
    if( channel >= MAX_ENCODER_CHANNEL )
        return -1;
    AK_ENCODER_T *ptr_enc = &all_ak_encoder[channel];
    vi_ch_enc_config(ptr_enc, w, h, fps, enc_type, profile);
    return 0;
}

/**
 * @fn:		api_vi_ch_enc_close
 *
 * @brief:	关闭编码设备
 *
 * @param:	channel - 编码通道号
 *
 * @return: -1/err, 0/ok
 */
int api_vi_ch_enc_close(int channel)
{
    if( channel >= MAX_ENCODER_CHANNEL )
        return -1;
    AK_ENCODER_T *ptr_enc = &all_ak_encoder[channel];
    return vi_ch_enc_close(ptr_enc);
}
int api_ak_ch_enc_is_openned(int channel)
{
	if( channel >= MAX_ENCODER_CHANNEL )
		return 0;
	if(all_ak_encoder[channel].handle!=-1)
		return 1;
	return 0;
}
cJSON *GetEncState(int ins)
{
	cJSON *state;
	if(ins>=3)
		return NULL;
	state=cJSON_CreateObject();
	if(state==NULL)
		return NULL;
	if(all_ak_encoder[ins].type== H264_ENC_TYPE)
	{
		cJSON_AddStringToObject(state,"ENC_TYPE","H264");
	}
	else if(all_ak_encoder[ins].type== HEVC_ENC_TYPE)
	{
		cJSON_AddStringToObject(state,"ENC_TYPE","H265");
	}
	else if(all_ak_encoder[ins].type== MJPEG_ENC_TYPE)
	{
		cJSON_AddStringToObject(state,"ENC_TYPE","MJPEG");
	}
	else
	{
		cJSON_AddStringToObject(state,"ENC_TYPE","Unkown");
	}	
	 
	if(all_ak_encoder[ins].profile== PROFILE_MAIN)
	{
		cJSON_AddStringToObject(state,"PROFILE","MAIN");
	}
	else if(all_ak_encoder[ins].profile== PROFILE_HIGH)
	{
		cJSON_AddStringToObject(state,"PROFILE","HIGH");
	}
	else if(all_ak_encoder[ins].profile== PROFILE_BASE)
	{
		cJSON_AddStringToObject(state,"PROFILE","BASE");
	}
	else if(all_ak_encoder[ins].profile== PROFILE_C_BASE)
	{
		cJSON_AddStringToObject(state,"PROFILE","C_BASE");
	}
	else if(all_ak_encoder[ins].profile== PROFILE_HEVC_MAIN)
	{
		cJSON_AddStringToObject(state,"PROFILE","HEVC_MAIN");
	}
	else if(all_ak_encoder[ins].profile== PROFILE_HEVC_MAIN_STILL)
	{
		cJSON_AddStringToObject(state,"PROFILE","HEVC_MAIN_STILL");
	}
	else if(all_ak_encoder[ins].profile== PROFILE_HEVC_MAIN_INTRA)
	{
		cJSON_AddStringToObject(state,"PROFILE","HEVC_MAIN_INTRA");
	}
	else if(all_ak_encoder[ins].profile== PROFILE_JPEG)
	{
		cJSON_AddStringToObject(state,"PROFILE","JPEG");
	}
	else
	{
		cJSON_AddStringToObject(state,"PROFILE","Unkown");
	}
	cJSON_AddNumberToObject(state,"FPS",all_ak_encoder[ins].fps);

	cJSON_AddNumberToObject(state,"RES_W",all_ak_encoder[ins].res_w);
	cJSON_AddNumberToObject(state,"RES_H",all_ak_encoder[ins].res_h);
	
	return state;
}







