
#ifndef _OBJ_AK_VI_PREVIEW_H_
#define _OBJ_AK_VI_PREVIEW_H_

#include "ak_common.h"
#include "ak_common_graphics.h"
#include "ak_vo.h"
#include "ak_vi.h"
#include "ak_mem.h"
#include "ak_log.h"
#include "ak_tde.h"

#define MAX_PREVIEW_WINS		5

void ak_preview_reso_reset(int ch0_w, int ch0_h, int ch1_w, int ch1_h );

/**
 * @fn:		ak_vi_preview_set_input
 *
 * @brief:	配置一个preview的显示输入参数
 *
 * @param:	win 	- preview窗口
 * @param:	vi_w 	- 视频通道的宽度
 * @param:	vi_h 	- 视频通道的高度
 *
 * @return: -1/err, 0/ok
 */
int ak_vi_preview_set_input(int win, int vi_c, int vi_w, int vi_h);

/**
 * @fn:		ak_vi_preview_get_input
 *
 * @brief:	获取一个preview的显示参数
 *
 * @param:	win		- preview通道
 * @param:	vi_c 	- 显示窗口关联的视频通道
 * @param:	vi_w 	- 视频通道的宽度指针
 * @param:	vi_h 	- 视频通道的高度指针
 *
 * @return: -1/err, 0/ok
 */
int ak_vi_preview_get_input(int win, int* vi_c, int* vi_w, int* vi_h );

/**
 * @fn:		ak_vi_preview_set_output
 *
 * @brief:	配置一个preview的显示输出参数
 *
 * @param:	win 	- preview窗口
 * @param:	vo_x 	- preview显示x坐标
 * @param:	vo_y 	- preview显示y坐标
 * @param:	vo_w 	- preview显示宽度
 * @param:	vo_h 	- preview显示高度
 *
 * @return: -1/err, 0/ok
 */
int ak_vi_preview_set_output(int win, int vo_x, int vo_y, int vo_w, int vo_h);

/**
 * @fn:		ak_vi_preview_get
 *
 * @brief:	获取一个preview的显示参数
 *
 * @param:	win		- preview通道
 * @param:	vo_x 	- preview显示x坐标指针
 * @param:	vo_y 	- preview显示y坐标指针
 * @param:	vo_w 	- preview显示宽度指针
 * @param:	vo_h 	- preview显示高度指针
 *
 * @return: -1/err, 0/ok
 */
int ak_vi_preview_get_output(int win, int* vo_x, int* vo_y, int* vo_w, int* vo_h);

/**
 * @fn:		ak_vi_preview_start
 *
 * @brief:	启动一个preview通道
 *
 * @param:	win 	- preview通道
 * @param:	ch 		- 对应视频通道
 *
 * @return: -1/err, 0/ok
 */
int ak_vi_preview_start(int win, int ch );

/**
 * @fn:		ak_vi_preview_stop
 *
 * @brief:	停止一个preview通道
 *
 * @param:	win 	- preview通道
 *
 * @return: -1/err, 0/ok
 */
int ak_vi_preview_stop( int win );

/**
 * @fn:		ak_vi_preview_get_state
 *
 * @brief:	得到一个通道的状态
 *
 * @param:	win 	- preview通道
 *
 * @return: -1/err, channel/ok
 */
int ak_vi_preview_get_state( int win );

/**
 * @fn:		ak_vi_preview_frame
 *
 * @brief:	获取一个preview通道的帧数据
 *
 * @param:	win 	- preview通道
 * @param:	pframe	- 数据帧指针
 *
 * @return: -1/err, 0/ok
 */
int ak_vi_preview_frame( int win, void* pframe );

#endif




