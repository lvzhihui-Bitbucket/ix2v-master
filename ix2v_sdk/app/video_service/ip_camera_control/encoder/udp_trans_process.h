
#ifndef _MULTICAST_CAPTURE_PROCESS_H_
#define _MULTICAST_CAPTURE_PROCESS_H_

#include <stdio.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/timeb.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <sys/ioctl.h>
#include <sys/ipc.h>
#include <sys/socket.h>
#include <sys/resource.h>
#include <sys/time.h>
#include <sys/types.h>
#include <errno.h>
#include <linux/fb.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/un.h>
#include <signal.h>
#include <dirent.h>
#include <pthread.h> 

/**
 * @fn:		init_udp_send_list
 *
 * @brief:	3路UDP发送列表初始化
 *
 * @param:	none
 *
 * @return: -1/err, 0/ok
 */
void init_udp_send_list(void);

/**
 * @fn:		api_udp_add_one_send_node
 *
 * @brief:	add one send node, if the channel list have no send node, subscriber one udp token from encoder channel
 *
 * @param:	channel		- channel index (0-2)
 * @param:	send_mode	- send mode
 * @param:	send_addr	- send target ip addr
 * @param:	send_port	- send target ip port
 *
 * @return: -1/err, x/total target
 */
int api_udp_add_one_send_node( int channel, int send_mode, int send_addr, unsigned short send_port );

/**
 * @fn:		api_udp_del_one_send_node
 *
 * @brief:	delete one send node, if the channel list have no send node, desubscriber the udp token from encoder channel
 *
 * @param:	channel		- channel index (0-2)
 * @param:	send_mode	- send mode
 * @param:	send_addr	- send target ip addr
 * @param:	send_port	- send target ip port
 *
 * @return: -1/err, 0/ok
 */
int api_udp_del_one_send_node( int channel, int send_mode, int send_addr, unsigned short send_port );

#endif
