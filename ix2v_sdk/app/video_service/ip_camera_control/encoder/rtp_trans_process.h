#ifndef _RTP_TRANS_PROCESS_H
#define _RTP_TRANS_PROCESS_H

/**
 * @fn:		init_rtp_send_list
 *
 * @brief:	3路RTP发送列表初始化
 *
 * @param:	none
 *
 * @return: -1/err, 0/ok
 */
void init_rtp_send_list(void);

/**
 * @fn:		api_rtp_add_one_send_node
 *
 * @brief:	add one send node, if the channel list have no send node, subscriber one rtp token from encoder channel
 *
 * @param:	channel		    - channel index (0-2)
 * @param:	send_mode	    - send mode
 * @param:	send_ip	    	- send ip addr
 * @param:	send_port	    - send port
 *
 * @return: -1/err, x/total target
 */
int api_rtp_add_one_send_node( int channel, int send_mode, char* send_ip, short send_port );

/**
 * @fn:		api_rtp_del_one_send_node
 *
 * @brief:	delete one send node, if the channel list have no send node, desubscriber the rtp token from encoder channel
 *
 * @param:	channel		    - channel index (0-2)
 * @param:	send_mode	    - send mode
 * @param:	send_ip	    	- send ip addr
 * @param:	send_port	    - send port
 *
 * @return: -1/err, 0/ok
 */
int api_rtp_del_one_send_node( int channel, int send_mode, char* send_ip, short send_port );

/**
 * @fn:		api_rtp_get_one_send_session
 *
 * @brief:	get one send node's session
 *
 * @param:	channel		    - channel index (0-2)
 * @param:	send_mode	    - send mode
 * @param:	send_ip	    	- send ip addr
 * @param:	send_port	    - send port
 *
 * @return: NULL/err, NOT NULL/ok
 */
void* api_rtp_get_one_send_session( int channel, int send_mode, char* send_ip, short send_port );

/**
 * @fn:		api_rtp_del_all_send_node
 *
 * @brief:	delete all send node
 *
 * @param:	channel		    - channel index (0-2)
 *
 * @return: -1/err, 0/ok
 */
int api_rtp_del_all_send_node( int channel );

#endif
