
#ifndef _OBJ_AK_VI_CAMERA_H_
#define _OBJ_AK_VI_CAMERA_H_

#define T527_PAGE0   0x20
#define T527_PAGE1   0x21
#define T527_PAGE2   0x22
#define T527_PAGE3   0x23

#define SENSOR_TYPE_T527        1
#define SENSOR_TYPE_TW9912      2
#define SENSOR_TYPE_DC7120      3
#define SENSOR_TYPE_DC7160      4
#define SENSOR_TYPE_HM1246      5

#define MAGIC_FIXED  0xAA551234
typedef struct
{
   int type;
   int rev;
   // lzh_20240311 add
   int magic;  // fixed 0xaa551234
   int ext; 
   // lzh_20240311 add
}SENSOR_CHECK_TYPE;

typedef struct
{
   int page;
   int reg;
   int value;
}SENSOR_IIC_PARA;

#define SENSOR_TYPE_GET	_IOR('V', 18,  SENSOR_CHECK_TYPE)
#define SENSOR_PARA_SET	_IOW('V', 19,  SENSOR_IIC_PARA)
#define SENSOR_PARA_GET	_IOWR('V', 20,  SENSOR_IIC_PARA)
#define SENSOR_CVBS_SET _IOW('V', 21,  int)

int sensor_type_is_t527(void);
int sensor_type_is_tw9912(void);
int sensor_type_is_dc7120(void);
int sensor_type_is_dc7160(void);
int sensor_type_is_hm1246(void);
int get_sensor_crop_x(void);
int get_sensor_crop_y(void);

/**
 * @fn:		sensor_para_open
 *
 * @brief:	开启sensor参数调节设备，开启摄像头设备调用
 *
 * @param:	none
 *
 * @return:     -1/err, 0/ok
 */
int sensor_para_open(void);
int sensor_probe_status(void);

/**
 * @fn:		sensor_para_close
 *
 * @brief:	关闭sensor参数调节设备，关闭摄像头设备时调用
 *
 * @param:	none
 *
 * @return:     -1/err, 0/ok
 */
int sensor_para_close(void);

int sensor_adjust_initial(void);

int get_sensor_cur_para(int* pbr, int* pcl, int* pco);
int sensor_brightness_adjust(int inc);
int sensor_color_adjust(int inc);
int sensor_contrast_adjust(int inc);

// lzh_20240311_s
int read_io_sensor_pwr_off(void);
int insmod_sensor_module(int sensor_type);
int rmmod_sensor_module(int sensor_type);
int link_sensor_module(int* ptype, int* pstate, int* ini_err);
int reset_sensor_module(void);
// lzh_20240311_w
#endif
