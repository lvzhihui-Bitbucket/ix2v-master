
/**
 * Description: 3路视频码流管理
 * Author: 		lvzhihui
 * Create: 		2012-01-23
 * 
 * 组件函数调用相关顺序说明：
 * 若配置更改，调用api_ak_vi_set_channel_resolution：	配置当前通道的分辨率
 * 若配置更改，调用api_ak_vi_set_channel_encoder：		配置当前通道的编码参数
 * 若配置更改，调用api_ak_vi_set_channel_preview：		配置当前通道的预览位置和大小
 * 
 * 
 * api_ak_vi_trans_subscriber：		订阅机制启动指定通道码流传输
 * api_ak_vi_trans_desubscriber：	订阅机制停止指定通道码流传输
 */

#include <stdio.h>
#include <ctype.h>  
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>  
#include <unistd.h>
#include <list.h>

#include "obj_ak_vi_manager.h"
#include "obj_ak_vi_encoder.h"
#include "obj_ak_vi_preview.h"
#include "obj_ak_vi_camera.h"

#include "ak_common.h"
#include "ak_log.h"
#include "ak_common_video.h"
#include "ak_venc.h"
#include "ak_thread.h"
#include "ak_mem.h"
#include "ak_vi.h"
#include "cJSON.h"
#include "obj_gpio.h"
#include "elog_forcall.h"
#include "RTOS.h"
#include "OSQ.h"
#include "OSTIME.h"

#include "sensor_low_lut_detect.h"
void callback_sensor_low_lut_changed(int status);

#ifndef AK_SUCCESS
#define AK_SUCCESS 			0
#endif

#ifndef AK_FAILED
#define AK_FAILED 			-1
#endif

#define DEF_FRAME_DEPTH		2
#define AK_VI_CH_MAX		3



pthread_mutex_t vi_dev_lock=PTHREAD_MUTEX_INITIALIZER;
static int vi_dev_ref_cnt = 0;
static int vi_dev_static_time = 0;
static int vi_nv_light_state=0;
static int vi_nv_state=0;
static int vi_cvbs_enable=0;
static int vi_cvbs_state=0;
OS_TIMER vi_static_timer;
#if 0//PID_IX850
#define MAX_VI_STATIC_TIME		30
#define MAX_VI_STATIC_TIME_GAP	(60*1000/25)	// 1 sec
#else
//#define MAX_VI_STATIC_TIME		2628000		//(24*60*365*5)	//#define MAX_VI_STATIC_TIME		3
int MAX_VI_STATIC_TIME = 0;//2628000;
#define MAX_VI_STATIC_TIME_GAP	(60*1000/25)	// 1 sec
#endif

#define AK_VI_DEV_MAX_CH        3

#define isp_path_t527		        "/mnt/nand1-1/App/share/isp_t527_dvp-H1V0P0-2000-54M-2.conf"
#define isp_path_tw9912		        "/mnt/nand1-1/App/share/isp_tw9912_dvp-H1V0P1-2000-2.conf"
#define isp_path_dc7120		        "/mnt/nand1-1/App/share/isp_dc7120_dvp-H1V1P1.conf"
#define isp_path_dc7160		        "/mnt/nand1-1/App/share/isp_avs7610_dvp-H1V1P1.conf"
#define isp_path_hm1246		        "/mnt/nand1-1/App/share/isp_hm1246_dvp-H1V0P0.conf"

static ak_ch_vi_atrr_t  ak_ch_vi_atrr[AK_VI_DEV_MAX_CH] =
{
	// channel 0
	{
		.state				= 0,
		.vi_id				= 0,
		.vi_ch				= VIDEO_CHN0,
		.video_w			= 1280,		
		.video_h			= 720,
		.lock=PTHREAD_MUTEX_INITIALIZER,
		.sub_lock=PTHREAD_MUTEX_INITIALIZER,
	},
	// channel 1
	{
		.state				= 0,
		.vi_id				= 1,
		.vi_ch				= VIDEO_CHN1,
		.video_w			= 640,		
		.video_h			= 480,
		.lock=PTHREAD_MUTEX_INITIALIZER,
		.sub_lock=PTHREAD_MUTEX_INITIALIZER,
	},
	// channel 2
	{
		.state				= 0,
		.vi_id				= 2,
		.vi_ch				= VIDEO_CHN16,
		.video_w			= 320,		
		.video_h			= 240,
		.lock=PTHREAD_MUTEX_INITIALIZER,
		.sub_lock=PTHREAD_MUTEX_INITIALIZER,
	}
};

void ak_c_vi_reso_reset(int ch0_w, int ch0_h, int ch1_w, int ch1_h )
{
	ak_ch_vi_atrr[0].video_w = ch0_w;
	ak_ch_vi_atrr[0].video_h = ch0_h;
	ak_ch_vi_atrr[1].video_w = ch1_w;
	ak_ch_vi_atrr[1].video_h = ch1_h;
	ak_ch_vi_atrr[2].video_w = ch1_w/2;
	ak_ch_vi_atrr[2].video_h = ch1_h/2;
}

int sensor_is_power_off(void)
{
	if( !vi_dev_ref_cnt && !vi_dev_static_time )
		return 1;
	else
		return 0;
}

static void DS_NightVisionCtrl(int onOff)
{
	#if defined(PID_IX611)

	#if 0
	if(sensor_type_is_dc7160())
	{
		onOff ? DC7610_BackLightCtrl(1):DC7610_BackLightCtrl(0);
	}
	else
	#endif
	{
		onOff ? NightVisionPwm_Enable() : NightVisionPwm_Disable();
	}
	#elif defined(PID_IX622)
	if(sensor_type_is_dc7160())
	{
		onOff ? NameplateLight_Ctrl(GetNameplateLightDuty(1)) : NameplateLight_Ctrl(GetNameplateLightDuty(0));
	}
	else
	{
		onOff ? NightVisionPwm_Enable() : NightVisionPwm_Disable();
	}
	#elif defined(PID_IX850)||defined(PID_IX821)
	if(!sensor_type_is_dc7160())
	{
		onOff ? API_NV_ctrl(1,100) : API_NV_ctrl(0, 0);
	}
	#endif
}

/**
 * @fn:		vi_dev_open
 *
 * @brief:	open capture device
 *
 * @param:	none
 *
 * @return: -1/err, 0/ok
 */
static int vi_dev_open(void)
{
	int dev_id = VIDEO_DEV0;
    /* 
     * step 0: global value initialize
     */
    int ret = -1;  

	// lzh_20240311_add_s
	insmod_sensor_module(read_io_sensor_type());
	// lzh_20240311_add_e

	pthread_mutex_lock(&vi_dev_lock);
	
	//printf("=====vi_dev_open,vi_dev_ref_cnt[%d],vi_dev_static_time[%d],vi_nv_state[%d]=====\n",vi_dev_ref_cnt,vi_dev_static_time,vi_nv_state);

	if( vi_dev_ref_cnt > 0||vi_dev_static_time>0 )
	{
		if(vi_dev_ref_cnt==0&&vi_nv_state==1)	// sensor first open and need to turn on nv
		{
			vi_nv_light_state=1;
			ak_vi_enable_dev(dev_id);
			DS_NightVisionCtrl(1);
		}

		if(vi_dev_ref_cnt==0)
		{
			ak_vi_enable_dev(dev_id);
			#if defined(PID_IX850) || defined(PID_IX622)
			radar_gpio_reset();
			#endif
		}
		vi_dev_ref_cnt++;		
		vi_dev_static_time=MAX_VI_STATIC_TIME;

		if( sensor_type_is_dc7160() )
		{
			#if defined(PID_IX622)
			vi_nv_light_state=1;
			NameplateLight_Ctrl(GetNameplateLightDuty(1));
			#endif
			#if defined(PID_IX611)
			//NightVisionPwm_Enable();//DC7610_BackLightCtrl(1);
			#endif
			Cmr7610_PowerOnInit();
		}

		pthread_mutex_unlock(&vi_dev_lock);
		return 0;
	}
    /* open vi flow */
    /* 
     * step 1: open video input device
     */
     //log_d("vi_dev_open0000000000");
	SENSOR_POWER_SET();
	usleep(100*1000);
	//usleep(200*1000);
	//log_d("vi_dev_open111111111");
    ret = ak_vi_open(dev_id);
    if (AK_SUCCESS != ret) 
    {
        ak_print_error_ex(MODULE_ID_APP, "vi device %d open failed\n", dev_id);    
		pthread_mutex_unlock(&vi_dev_lock);
        return ret;
    }
	// open sensor para device, then read sensor type, and start checking low lut timing
	sensor_para_open();

    /*
     * step 2: load isp config
     */
	unsigned char* ptr_isp_type = NULL;
	if( sensor_type_is_t527() )
		ptr_isp_type = isp_path_t527;
	else if( sensor_type_is_tw9912() )
		ptr_isp_type = isp_path_tw9912;
	else if( sensor_type_is_dc7120() ) 
		ptr_isp_type = isp_path_dc7120;
	else if( sensor_type_is_dc7160() )
		ptr_isp_type = isp_path_dc7160;
	else if( sensor_type_is_hm1246() )
		ptr_isp_type = isp_path_hm1246;
	//log_d("vi_dev_open33333333333");	
	ret = ak_vi_load_sensor_cfg(dev_id, ptr_isp_type);
    if (AK_SUCCESS != ret) 
    {
        ak_print_error_ex(MODULE_ID_APP, "vi device %d load isp cfg [%s] failed!\n", dev_id, ptr_isp_type);    
		pthread_mutex_unlock(&vi_dev_lock);
        return ret;
    }
	//log_d("vi_dev_open4444444444");
#if 1
    /* 
     * step 3: get sensor support max resolution
     */
    RECTANGLE_S     res;                //max sensor resolution
    VI_DEV_ATTR     dev_attr;
    memset(&dev_attr, 0, sizeof(VI_DEV_ATTR));
    dev_attr.dev_id         = dev_id;

    /* get sensor resolution */
    ret = ak_vi_get_sensor_resolution(dev_id, &res);
    if (ret) 
    {
        ak_print_error_ex(MODULE_ID_APP, "Can't get dev[%d]resolution\n", dev_id);
        ak_vi_close(dev_id);
	    pthread_mutex_unlock(&vi_dev_lock);	
        return ret;
    } 
    else 
    {
        dev_attr.crop.width 	= res.width;
        dev_attr.crop.height 	= res.height;
		dev_attr.crop.left		= get_sensor_crop_x();
		dev_attr.crop.top		= get_sensor_crop_y();
        ak_print_normal_ex(MODULE_ID_APP, "Get Sensor Resolution w[%d],h[%d],Cropx[%d],Cropy[%d]\n",dev_attr.crop.width, dev_attr.crop.height, dev_attr.crop.left, dev_attr.crop.top);
    }
	//log_d("vi_dev_open55555555555555");
	if( sensor_type_is_t527() )
	{
		dev_attr.max_width      = res.width;
		dev_attr.max_height     = res.height;
		//dev_attr.sub_max_width  = 640;
		//dev_attr.sub_max_height = res.height;
		dev_attr.sub_max_width  = 320;
		dev_attr.sub_max_height = 240;

		//ak_c_vi_reso_reset(res.width,res.height, 640, 480);
		//ak_encoder_reso_reset(res.width,res.height, 640, 480);
		//ak_preview_reso_reset(res.width,res.height, 640, 480);
		ak_c_vi_reso_reset(res.width,res.height, 320, 240);
		ak_encoder_reso_reset(res.width,res.height, 320, 240); // for APP
		ak_preview_reso_reset(res.width,res.height, 320, 240);
	}
	else if( sensor_type_is_tw9912() )
	{
		dev_attr.max_width      = res.width;
		dev_attr.max_height     = res.height;
		dev_attr.sub_max_width  = 320;	//640
		dev_attr.sub_max_height = 240;	//480

		ak_c_vi_reso_reset(res.width,res.height, 320, 240);
		ak_encoder_reso_reset(res.width,res.height, 320, 240); // for APP
		ak_preview_reso_reset(res.width,res.height, 320, 240);		
	}
	else if( sensor_type_is_dc7120() || sensor_type_is_hm1246() )
	{
		dev_attr.max_width      = res.width;
		dev_attr.max_height     = res.height;
		dev_attr.sub_max_width  = 640;	//640
		dev_attr.sub_max_height = 480;	//480

		ak_c_vi_reso_reset(res.width,res.height, 640, 480);
		ak_encoder_reso_reset(res.width,res.height, 640, 480); // for APP
		#if 0
		ak_preview_reso_reset(res.width,res.height, 640, 480);	
		#endif
	}
	else if( sensor_type_is_dc7160() )
	{
		dev_attr.max_width      = res.width;
		dev_attr.max_height     = res.height;
		dev_attr.sub_max_width  = 640;	//640
		dev_attr.sub_max_height = 480;	//480

		ak_c_vi_reso_reset(res.width,res.height, 640, 480);
		ak_encoder_reso_reset(res.width,res.height, 640, 480); // for APP
		#if 0
		ak_preview_reso_reset(res.width,res.height, 640, 480);	
		#endif
	}
	//log_d("vi_dev_open666666666666");
#else
    /* 
     * step 3: get sensor support max resolution
     */
    RECTANGLE_S     res;                //max sensor resolution
    VI_DEV_ATTR     dev_attr;
    memset(&dev_attr, 0, sizeof(VI_DEV_ATTR));
    dev_attr.dev_id         = dev_id;
    dev_attr.crop.left      = 0;
    dev_attr.crop.top       = 0;
    dev_attr.crop.width     = 1280;
    dev_attr.crop.height    = 720;
    dev_attr.max_width      = 1280;
    dev_attr.max_height     = 720;
    dev_attr.sub_max_width  = 640;
    dev_attr.sub_max_height = 480;

    /* get sensor resolution */
    ret = ak_vi_get_sensor_resolution(dev_id, &res);
    if (ret) 
    {
        ak_print_error_ex(MODULE_ID_APP, "Can't get dev[%d]resolution\n", dev_id);
        ak_vi_close(dev_id);
	    pthread_mutex_unlock(&vi_dev_lock);	
        return ret;
    } 
    else 
    {
        ak_print_normal_ex(MODULE_ID_APP, "get dev res w:[%d]h:[%d]\n",res.width, res.height);
        dev_attr.crop.width = res.width;
        dev_attr.crop.height = res.height;
    }
#endif
    /* 
     * step 4: set vi device working parameters 
     * default parameters: 25fps, day mode
     */
    ret = ak_vi_set_dev_attr(dev_id, &dev_attr);
    if (ret) 
    {
        ak_print_error_ex(MODULE_ID_APP, "vi device %d set device attribute failed!\n", dev_id);
        ak_vi_close(dev_id);
        pthread_mutex_unlock(&vi_dev_lock);	
        return ret;
    }

	VI_CHN_ATTR chn_attr = {0};
	int index;
	for( index = 0; index < AK_VI_DEV_MAX_CH; index++ )
	{
		ak_ch_vi_atrr_t *pak_ch_vi_atrr = &ak_ch_vi_atrr[index];
		memset(&chn_attr, 0, sizeof(VI_CHN_ATTR));
		chn_attr.chn_id     	= pak_ch_vi_atrr->vi_ch;
		chn_attr.res.width  	= pak_ch_vi_atrr->video_w;
		chn_attr.res.height 	= pak_ch_vi_atrr->video_h;
		chn_attr.frame_depth 	=DEF_FRAME_DEPTH;
		/*disable frame control*/
#if 0
		chn_attr.frame_rate = 15;
		//chn_attr.frame_rate = 0;
#else
		//chn_attr.frame_rate = 25;
		chn_attr.frame_rate = 30;
#endif
		ret = ak_vi_set_chn_attr(pak_ch_vi_atrr->vi_ch, &chn_attr);
		if (ret) 
		{
			ak_print_error_ex(MODULE_ID_APP, "vi device set channel [%d] attribute failed!\n", pak_ch_vi_atrr->vi_ch);
			// return ret;  // lzh_20240313
		}
	}
	//log_d("vi_dev_open777777777");
	ret = ak_vi_enable_dev(dev_id);
	//log_d("vi_dev_open888888888");
	vi_dev_ref_cnt++;
	//vi_dev_ref_cnt+=1000;
	vi_dev_static_time=MAX_VI_STATIC_TIME;
	vi_nv_state=0;
	vi_nv_light_state=0;
	//#ifndef PID_IX850
	if( sensor_type_is_dc7120() || sensor_type_is_hm1246() )
	{
		sensor_low_lut_detect_start(callback_sensor_low_lut_changed);
	}
	else if( sensor_type_is_dc7160() )
	{
		#if defined(PID_IX622)
		vi_nv_light_state=1;
		NameplateLight_Ctrl(GetNameplateLightDuty(1));
		#endif
		#if defined(PID_IX611)
		sensor7610_low_lut_detect_start(callback_sensor_low_lut_changed);//NightVisionPwm_Enable();//DC7610_BackLightCtrl(1);
		#endif
		
	}
	//#endif
	vi_cvbs_state=0;
	#if 0
	if(vi_cvbs_enable)
	{
		SENSOR_POWER_SET();			// reset once
		if(sensor_cvbs_switch(1)==0)
		{
			dt_video_output_sw(1);
			vi_cvbs_state=1;
		}
	}
	#endif
	//usleep(100*1000);
	#if 0
	if(sensor_cvbs_switch(0)==0)
		{
			
		}
		dt_video_output_sw(0);
	#endif
	
	#ifndef PID_IX611
	sensor_probe_status();	// reget init ok status
	#endif
	
	pthread_mutex_unlock(&vi_dev_lock);

	printf("vi_dev_open[ref=%d]\n",vi_dev_ref_cnt);

	// initial sensor para
	sensor_adjust_initial();
	//log_d("vi_dev_open999");

	// connect to sensor driver, waiting for sensor low lut changed event
	
	if( sensor_type_is_dc7120() || sensor_type_is_hm1246() )
	{
		//usleep(1000*1000);
		//sensor_cvbs_switch(0);	// initial para		
		//dt_video_output_sw(0);
	}
	API_PublicInfo_Write_Int("CameraOnState", 1);
	if( sensor_type_is_dc7160() )
	{
		API_PublicInfo_Write_String("CameraType", "cmr7160");
		usleep(200*1000);
		Cmr7610_PowerOnInit();
	}

	#if defined(PID_IX850) || defined(PID_IX622)
	radar_gpio_reset();
	#endif
	return ret;
}

/**
 * @fn:		vi_dev_close
 *
 * @brief:	close capture device
 *
 * @param:	none
 *
 * @return: -1/err, 0/ok
 */
#if 1
static int vi_dev_close(void)
{
	int dev_id = VIDEO_DEV0;
    int ret = -1;   
       
    pthread_mutex_lock(&vi_dev_lock);
	//printf("=====vi_dev_close,vi_dev_ref_cnt[%d],vi_dev_static_time[%d],vi_nv_state[%d]=====\n",vi_dev_ref_cnt,vi_dev_static_time,vi_nv_state);
	if( --vi_dev_ref_cnt > 0)
	{
		ret = 0;
	}
	else if(vi_dev_static_time==0)
	{
		VideoEventLog("vi_close1");
		vi_dev_static_time = 0;
		ak_vi_disable_dev(dev_id);
		ret = ak_vi_close(dev_id);
		// close sensor para device
		sensor_low_lut_detect_stop();
		DS_NightVisionCtrl(0);
		sensor_para_close();
		SENSOR_POWER_RESET();
		API_PublicInfo_Write_Int("CameraOnState", 0);
		Cmr7610_PowerOff();
#if defined(PID_IX850)|| defined(PID_IX611) || defined(PID_IX622)||defined(PID_IX821)
		//system("rmmod sensor_dc7120");
		system("rmmod ak_isp");
		system("insmod /usr/modules/ak_isp.ko");
		//system("insmod /usr/modules/sensor_dc7120.ko");
		#if defined(PID_IX850) || defined(PID_IX622)
		radar_gpio_reset();
		// lzh_20240311_s
		// NOT PROCESS!! rmmod_sensor_module(read_io_sensor_type());
		// lzh_20240311_e
		#endif
#endif
	}
	else
	{
		//vi_nv_state=0;
		//vi_nv_state=0;
		//ak_vi_disable_dev(dev_id);	// lzh_20230313
		vi_nv_light_state=0;
		VideoEventLog("vi_close0");
		DS_NightVisionCtrl(0);
		vi_static_timer_start();
	}
	#if 0
	if(vi_dev_ref_cnt==0&&vi_cvbs_state==1)
	{
		SENSOR_POWER_SET();			// reset once
		if(sensor_cvbs_switch(0)==0)
		{
			
		}
		dt_video_output_sw(0);
		vi_cvbs_state=0;
	}
	#endif
	pthread_mutex_unlock(&vi_dev_lock);

	printf("vi_dev_close[ref=%d]\n",vi_dev_ref_cnt);

    return ret;
}
#else
static int vi_dev_close(void)
{
	int dev_id = VIDEO_DEV0;
    int ret = -1;   
       
    pthread_mutex_lock(&vi_dev_lock);

	if( --vi_dev_ref_cnt > 0)
	{
		ret = 0;
	}
	else //if(vi_dev_static_time==0)
	{
		vi_dev_static_time = 0;
		ak_vi_disable_dev(dev_id);
		ret = ak_vi_close(dev_id);
		// close sensor para device
		sensor_low_lut_detect_stop();
		DS_NightVisionCtrl(0);
		sensor_para_close();
		SENSOR_POWER_RESET();
		system("rmmod /usr/modules/ak_isp.ko");		
		system("insmod /usr/modules/ak_isp.ko");
	}
	//else
	//	vi_static_timer_start();
	pthread_mutex_unlock(&vi_dev_lock);

	printf("vi_dev_close[ref=%d]\n",vi_dev_ref_cnt);

    return ret;
}
#endif
void vi_dev_close_next(void)
{
	pthread_mutex_lock(&vi_dev_lock);
	vi_dev_static_time=0;
	pthread_mutex_unlock(&vi_dev_lock);
}
// lzh_20240312_s
void vi_dev_close_forced(void)
{	
	printf("VI DEV BE FORCED TO CLOSED START,vi_dev_static_time=%d\n",vi_dev_static_time);
	if( vi_dev_static_time )
	{
		OS_StopTimer( &vi_static_timer );
		vi_dev_close_next();
		vi_dev_ref_cnt = 1;
		vi_dev_close();
		printf("VI DEV BE FORCED TO CLOSED!!!,vi_dev_ref_cnt=%d\n",vi_dev_ref_cnt);
	}
}
// lzh_20240312_e
void vi_static_callback(void)
{
	int dev_id = VIDEO_DEV0;
	 pthread_mutex_lock(&vi_dev_lock);
	if(vi_dev_ref_cnt==0&&vi_dev_static_time>0)
	{
		//if(vi_dev_static_time>0)
		vi_dev_static_time--;
		if( vi_dev_static_time == 0&&vi_dev_ref_cnt==0)
		{
			ak_vi_disable_dev(dev_id);
			ak_vi_close(dev_id);
			// close sensor para device
			sensor_low_lut_detect_stop();
			DS_NightVisionCtrl(0);
			sensor_para_close();
			SENSOR_POWER_RESET();
			API_PublicInfo_Write_Int("CameraOnState", 0);
			Cmr7610_PowerOff();
			#if	defined(PID_IX850)|| defined(PID_IX611) || defined(PID_IX622)||defined(PID_IX821)
			//system("rmmod sensor_dc7120");
			system("rmmod ak_isp");
			system("insmod /usr/modules/ak_isp.ko");
			#if defined(PID_IX850) || defined(PID_IX622)
			radar_gpio_reset();
			#endif
			// lzh_20240311_s
			// NOT PROCESS!! rmmod_sensor_module(read_io_sensor_type());
			// lzh_20240311_e
			//system("insmod /usr/modules/sensor_dc7120.ko");
			#endif
			printf("====ak_vi delay closed!!=======================\n");
		}
	}
	
	pthread_mutex_unlock(&vi_dev_lock);

	if(vi_dev_static_time>0)
		OS_RetriggerTimer( &vi_static_timer );
}
int vi_static_timer_start(void)
{
	static unsigned char init_flag=0;
	if(init_flag==0)
	{
		OS_CreateTimer( &vi_static_timer, vi_static_callback, MAX_VI_STATIC_TIME_GAP);
		init_flag=1;
	}
	OS_RetriggerTimer( &vi_static_timer );	
}
/**
 * @fn:		vi_ch_read_frame
 *
 * @brief:	read one frame data
 *
 * @param:	arg - channel attribute ptr
 *
 * @return: none
 */
static void vi_ch_read_frame(void *arg)
{	
	int ret=0;
	struct video_input_frame frame;
	int rec_flag=0;
	int err_cnt=0;
	ak_ch_vi_atrr_t *pak_ch_vi_atrr=(ak_ch_vi_atrr_t *)arg;
	PrintCurrentTime(11111);
	if( vi_dev_open() != 0 )
	{
		VideoEventLog("open dev err");
		CmrDevManager_add_one_record(1,"open dev");
		return;
	}

	PrintCurrentTime(22222);
	ak_vi_enable_chn(pak_ch_vi_atrr->vi_ch);
	pak_ch_vi_atrr->sensor_output_cvbs = 0;
	//pak_ch_vi_atrr->task_run = 1;
	printf("channel[%d],ak_vi_get_frame----------------------\n",pak_ch_vi_atrr->vi_ch);
	//check_vi_cvbs_process();
	//dt_video_output_sw(1);
	//if(pak_ch_vi_atrr->encoder_ins_id != -1&&api_vi_ch_enc_get_handle(pak_ch_vi_atrr->encoder_ins_id)!=-1)
	PrintCurrentTime(33333);
	//	ak_venc_request_idr(api_vi_ch_enc_get_handle(pak_ch_vi_atrr->encoder_ins_id));
	while(pak_ch_vi_atrr->task_run)
	{
		#if 0
		check_vi_cvbs_process();
		// check sensor cvbs output flag
		//if( pak_ch_vi_atrr->sensor_output_cvbs )
		if(vi_cvbs_state)
		{
			if(pak_ch_vi_atrr->preview_ins_id==-1&&pak_ch_vi_atrr->trans_mask_bit==0&&pak_ch_vi_atrr->detach_break_flag==1)
			{
				pthread_mutex_lock(&pak_ch_vi_atrr->lock);
				pthread_detach(pthread_self());
				api_vi_ch_enc_close(pak_ch_vi_atrr->encoder_ins_id);
				pak_ch_vi_atrr->task_run = 0;
				ak_vi_disable_chn(pak_ch_vi_atrr->vi_ch);
				api_del_all_subscribers(&pak_ch_vi_atrr->enc_user_list);
				vi_dev_close();
				pak_ch_vi_atrr->state = 0;
				pthread_mutex_unlock(&pak_ch_vi_atrr->lock);
				break;
			}
			ak_sleep_ms(20);
			continue;
		}
		#endif
		//
		//if(rec_flag==0)
		//	PrintCurrentTime(444444);
		memset(&frame, 0x00, sizeof(frame));
		ret = ak_vi_get_frame(pak_ch_vi_atrr->vi_ch,&frame);
		if(rec_flag==0)
			PrintCurrentTime(55555);
		//printf("111111channel[%d],ak_vi_get_frame----------------------%d\n",pak_ch_vi_atrr->vi_ch,frame.vi_frame.len);
		if (!ret) 
		{
			err_cnt=0;
			if(rec_flag++==3)
			{
				if(pak_ch_vi_atrr->vi_ch==0)
					VideoEventLog("[%d]read frame ok",pak_ch_vi_atrr->vi_ch);
				CmrDevManager_add_one_record(0,NULL);
			}
			// preview callback
			if( pak_ch_vi_atrr->preview_ins_id >= 0  )
			{
				ak_vi_preview_frame( pak_ch_vi_atrr->preview_ins_id, (void*)&frame );
			}
			/* send it to encode */
			if( pak_ch_vi_atrr->encoder_ins_id != -1 && pak_ch_vi_atrr->trans_mask_bit!=0&&(pak_ch_vi_atrr->vi_ch==0||rec_flag%2==1))
			{
				//printf("11111111%s:%d\n",__func__,__LINE__);
				struct video_stream *stream = ak_mem_alloc(MODULE_ID_APP, sizeof(struct video_stream));
				if(If_trigger_key_frame(pak_ch_vi_atrr->encoder_ins_id))
				{
					ak_venc_request_idr(api_vi_ch_enc_get_handle(pak_ch_vi_atrr->encoder_ins_id));
				}
				else if(If_trigger_key_frame_delay(pak_ch_vi_atrr->encoder_ins_id))
				{
					//printf("11111111%s:%d\n",__func__,__LINE__);
					ak_venc_request_idr(api_vi_ch_enc_get_handle(pak_ch_vi_atrr->encoder_ins_id));
				}
				//printf("11111111%s:%d\n",__func__,__LINE__);
				ret = ak_venc_encode_frame(api_vi_ch_enc_get_handle(pak_ch_vi_atrr->encoder_ins_id), frame.vi_frame.data, frame.vi_frame.len, frame.mdinfo, stream);
				//printf("11111111%s:%d\n",__func__,__LINE__);
				if (ret)
				{
					/* send to encode failed */
					ak_print_error_ex(MODULE_ID_APP, "send to encode failed\n");
				}
				else 
				{
					//printf("11111111%s:%d\n",__func__,__LINE__);
					if(stream->len > 0)
					{
						//printf("11111111%s:%d\n",__func__,__LINE__);
						api_callback_subscribers(&pak_ch_vi_atrr->enc_user_list,stream->data,stream->len,(void*)stream);
						//printf("222222channel[%d],ak_vi_get_frame----------------------%d\n",pak_ch_vi_atrr->vi_ch,stream->len);
					}
					else
						ak_print_notice_ex(MODULE_ID_VENC, "encode err, maybe drop\n");
					ret = stream->len;		
					ak_venc_release_stream(api_vi_ch_enc_get_handle(pak_ch_vi_atrr->encoder_ins_id), stream);        
				}
				ak_mem_free(stream);
			}
			
			// release frame
			ak_vi_release_frame(pak_ch_vi_atrr->vi_ch, &frame);
			if(pak_ch_vi_atrr->preview_ins_id==-1&&pak_ch_vi_atrr->trans_mask_bit==0&&pak_ch_vi_atrr->detach_break_flag==1)
			{
				pthread_mutex_lock(&pak_ch_vi_atrr->lock);
				pthread_detach(pthread_self());
				pak_ch_vi_atrr->task_run = 0;
				ak_vi_disable_chn(pak_ch_vi_atrr->vi_ch);
				api_del_all_subscribers(&pak_ch_vi_atrr->enc_user_list);
				vi_dev_close();
				pak_ch_vi_atrr->state = 0;
				pthread_mutex_unlock(&pak_ch_vi_atrr->lock);
				break;
			}
			if(pak_ch_vi_atrr->preview_ins_id==-1&&pak_ch_vi_atrr->encoder_ins_id==-1)
				usleep(100);
			
		}
		else
		{
			//if(rec_flag++==0)
			if(rec_flag++==2)	// lzh_20240311, perhaps first/second frame have no data
			{
				if(pak_ch_vi_atrr->vi_ch==0)
					VideoEventLog("[%d]read frame err:%s",pak_ch_vi_atrr->vi_ch,API_PublicInfo_Read_String("CmrDevState"));
				CmrDevManager_add_one_record(1,"read frame");
				api_channel_rec_stop(pak_ch_vi_atrr->vi_ch);
			}
			
		    /* 
		     *	If getting too fast, it will have no data,
		     *	just take breath.
		     */
		    // ak_print_normal_ex(MODULE_ID_APP, "get frame failed!\n");
		   // if(vi_cvbs_state)
		   	if(err_cnt++>5)
		    		vi_dev_close_next();
			if(pak_ch_vi_atrr->preview_ins_id==-1&&pak_ch_vi_atrr->trans_mask_bit==0&&pak_ch_vi_atrr->detach_break_flag==1)
			{
				pthread_mutex_lock(&pak_ch_vi_atrr->lock);
				pthread_detach(pthread_self());
				pak_ch_vi_atrr->task_run = 0;
				ak_vi_disable_chn(pak_ch_vi_atrr->vi_ch);
				api_del_all_subscribers(&pak_ch_vi_atrr->enc_user_list);
				vi_dev_close();
				pak_ch_vi_atrr->state = 0;
				pthread_mutex_unlock(&pak_ch_vi_atrr->lock);
				break;
			}
		    printf("get frame failed!\n");
		    ak_sleep_ms(10);
		}
	}
}

/**
 * @fn:		vi_ch_open
 *
 * @brief:	open one channel
 *
 * @param:	pak_ch_vi_atrr - channel attribute ptr
 *
 * @return: -1/err, 0/ok
 */
static int vi_ch_open(ak_ch_vi_atrr_t *pak_ch_vi_atrr)
{
	int ret;

	pak_ch_vi_atrr->task_run = 0;
	pak_ch_vi_atrr->detach_break_flag=0;

	//ak_vi_enable_chn(pak_ch_vi_atrr->vi_ch);	

	pthread_attr_t 	attr_thread;
	pthread_attr_init( &attr_thread );
	pak_ch_vi_atrr->task_run=1;
	if( pthread_create(&pak_ch_vi_atrr->task_read_frame,&attr_thread,(void*)vi_ch_read_frame, pak_ch_vi_atrr) != 0 )
	{
		pak_ch_vi_atrr->task_run=0;
		printf( "Create task_H264_enc pthread error! \n" );
		return 0;
	}
	else
		return -1;
}

/**
 * @fn:		vi_ch_close
 *
 * @brief:	close one channel
 *
 * @param:	pak_ch_vi_atrr - channel attribute ptr
 *
 * @return: -1/err, 0/ok
 */
static int vi_ch_close(ak_ch_vi_atrr_t *pak_ch_vi_atrr)
{	
	if( pak_ch_vi_atrr->task_run )
	{
		pak_ch_vi_atrr->task_run = 0;
		pthread_join(pak_ch_vi_atrr->task_read_frame,NULL);
	}
	ak_vi_disable_chn(pak_ch_vi_atrr->vi_ch);
	return 0;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * @fn:		api_ak_vi_ch_close
 *
 * @brief:	close one channel
 *
 * @param:	channel - channel index (0-2)
 *
 * @return: -1/err, 0/ok
 */
int api_ak_vi_ch_close( int channel )
{
	ak_ch_vi_atrr_t *pak_ch_vi_atrr=&ak_ch_vi_atrr[channel];
	
	pthread_mutex_lock(&pak_ch_vi_atrr->lock);

	if(pak_ch_vi_atrr->state)
	{
		vi_ch_close(pak_ch_vi_atrr);
		api_del_all_subscribers(&pak_ch_vi_atrr->enc_user_list);
		vi_dev_close();
		pak_ch_vi_atrr->state = 0;
		printf("---------api_ak_vi_ch_close[%d] ok--\n",channel);
		pthread_mutex_unlock(&pak_ch_vi_atrr->lock);
		return 0;
	}
	else
	{
		pthread_mutex_unlock(&pak_ch_vi_atrr->lock);
		return -1;
	}
}

/**
 * @fn:		api_ak_vi_ch_open
 *
 * @brief:	open one channel
 *
 * @param:	channel - channel index (0-2)
 *
 * @return: -1/err, 0/ok
 */
int api_ak_vi_ch_open( int channel )
{
	ak_ch_vi_atrr_t *pak_ch_vi_atrr = &ak_ch_vi_atrr[channel];

	pthread_mutex_lock(&pak_ch_vi_atrr->lock);

	if( !pak_ch_vi_atrr->state )
	{
		//if( vi_dev_open() == 0 )
		{
			printf("---------api_ak_vi_ch_open[%d] ok--\n",channel);
			INIT_LIST_HEAD(&pak_ch_vi_atrr->enc_user_list);
			pak_ch_vi_atrr->enc_user_token	= 0;
			pak_ch_vi_atrr->encoder_ins_id	= -1;
			pak_ch_vi_atrr->preview_ins_id	= -1;
			pak_ch_vi_atrr->trans_mask_bit	= 0;
			vi_ch_open(pak_ch_vi_atrr);	
			pak_ch_vi_atrr->state = 1;
			pthread_mutex_unlock(&pak_ch_vi_atrr->lock);
			return 0;
		}
		//else
		//	return -1;
	}
	else
	{
		pthread_mutex_unlock(&pak_ch_vi_atrr->lock);
		return -1;
	}
}

/**
 * @fn:		api_ak_vi_ch_is_openned
 *
 * @brief:	check one channel is openned
 *
 * @param:	channel - channel index (0-2)
 *
 * @return: 0/closed, 1/openned
 */
int api_ak_vi_ch_is_openned( int channel )
{
	int status;
	ak_ch_vi_atrr_t *pak_ch_vi_atrr = &ak_ch_vi_atrr[channel];
	pthread_mutex_lock(&pak_ch_vi_atrr->lock);
	status = pak_ch_vi_atrr->state;
	pthread_mutex_unlock(&pak_ch_vi_atrr->lock);
	if(pak_ch_vi_atrr->state&&pak_ch_vi_atrr->detach_break_flag)
	{
		usleep(500*1000);
		pthread_mutex_lock(&pak_ch_vi_atrr->lock);
		status = pak_ch_vi_atrr->state;
		pthread_mutex_unlock(&pak_ch_vi_atrr->lock);
	}
	return status;
}
void api_cmrerror_reset(void)
{
	int i;
	//if( api_ak_vi_ch_is_openned(0) )
	{
		ak_ch_vi_atrr_t *pak_ch_vi_atrr = &ak_ch_vi_atrr[0];
		pthread_mutex_lock(&pak_ch_vi_atrr->lock);
		if(pak_ch_vi_atrr->state)
		{
			vi_ch_close(pak_ch_vi_atrr);
		//api_del_all_subscribers(&pak_ch_vi_atrr->enc_user_list);
			vi_dev_close();
			vi_ch_open(pak_ch_vi_atrr);
		}
		pthread_mutex_unlock(&pak_ch_vi_atrr->lock);
	}
}
void api_cmrerror_reset1(void)
{
	int i;
	ak_ch_vi_atrr_t *pak_ch_vi_atrr;
	//if( api_ak_vi_ch_is_openned(0) )
	{
		vi_dev_close_next();
		for(i=0;i<AK_VI_DEV_MAX_CH;i++)
		{
			pak_ch_vi_atrr=&ak_ch_vi_atrr[i];
			pthread_mutex_lock(&pak_ch_vi_atrr->lock);
			if(pak_ch_vi_atrr->state)
			{
				vi_dev_close();
			}
			pthread_mutex_unlock(&pak_ch_vi_atrr->lock);
			
		}
		for(i=0;i<AK_VI_DEV_MAX_CH;i++)
		{
			pak_ch_vi_atrr=&ak_ch_vi_atrr[i];
			pthread_mutex_lock(&pak_ch_vi_atrr->lock);
			if(pak_ch_vi_atrr->state)
			{
				vi_dev_open();
				ak_vi_enable_chn(pak_ch_vi_atrr->vi_ch);
			}
			pthread_mutex_unlock(&pak_ch_vi_atrr->lock);
			
		}
		//pthread_mutex_unlock(&pak_ch_vi_atrr->lock);
	}
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * @fn:		api_get_channel_from_resolution
 *
 * @brief:	get one channel index according resolution
 *
 * @param:	reso_w	- resolution width
 * @param:	reso_h	- resolution height
 *
 * @return: -1/err, 0-2/channel index
 */
int api_get_channel_from_resolution( int reso_w, int reso_h )
{
	int i, res_w,res_h;

	printf("input resolution: width=%d, height=%d\n",reso_w,reso_h);

	for( i = 0; i < AK_VI_CH_MAX; i++ )
	{
		api_ak_vi_get_channel_resolution(i, &res_w, &res_h );
		if( (reso_w == res_w) && (reso_h == res_h) )
			break;
	}
	if( i == AK_VI_CH_MAX )
	{
		printf("api_get_channel_from_resolution,NOT match!!\n");
		return -1;
	}
	else
	{
		printf("api_get_channel_from_resolution,match channel=%d\n",i);
		return i;
	}
}

/**
 * @fn:		api_ak_vi_set_channel_resolution
 *
 * @brief:	set one channel resolution
 *
 * @param:	channel	- channel index (0-2)
 * @param:	res_w	- resolution width
 * @param:	res_h   - resolution height
 *
 * @return: -1/err, 0/ok
 */
int api_ak_vi_set_channel_resolution(int channel, int res_w, int res_h )
{
	if( channel >= AK_VI_CH_MAX )
		return -1;

	if( channel == 0 )		
		ak_ch_vi_atrr[channel].vi_ch =VIDEO_CHN0;
	else if( channel == 1 )
		ak_ch_vi_atrr[channel].vi_ch =VIDEO_CHN1;
	else
		ak_ch_vi_atrr[channel].vi_ch =VIDEO_CHN16;
	ak_ch_vi_atrr[channel].vi_id 	= channel;
	ak_ch_vi_atrr[channel].video_w	= res_w;
	ak_ch_vi_atrr[channel].video_h	= res_h;	
	return 0;
}

/**
 * @fn:		api_ak_vi_get_channel_resolution
 *
 * @brief:	get one channel resolution
 *
 * @param:	channel	- channel index (0-2)
 * @param:	res_w	- resolution width ptr
 * @param:	res_h   - resolution height ptr
 *
 * @return: -1/err, 0/ok
 */
int api_ak_vi_get_channel_resolution(int channel, int* res_w, int* res_h )
{
	if( channel >= AK_VI_CH_MAX )
		return -1;

	*res_w = ak_ch_vi_atrr[channel].video_w;
	*res_h = ak_ch_vi_atrr[channel].video_h;
	return 0;
}

/**
 * @fn:		api_ak_vi_set_channel_encoder
 *
 * @brief:	set one channel attribute
 *
 * @param:	channel	- channel index (0-2)
 * @param:	fps		- fps
 * @param:	enc_type- encoder type
 * @param:	profile - profile
 *
 * @return: -1/err, 0/ok
 */
int api_ak_vi_set_channel_encoder(int channel, int fps, int enc_type, int profile )
{
	if( channel >= AK_VI_CH_MAX )
		return -1;

	if( ak_ch_vi_atrr[channel].encoder_ins_id != -1 )
	{
		ak_ch_vi_atrr[channel].vi_id = channel;
		api_vi_ch_enc_set(channel,ak_ch_vi_atrr[channel].video_w,ak_ch_vi_atrr[channel].video_h,fps,enc_type,profile);
		return 0;	
	}
	else
		return -1;
}

/**
 * @fn:		api_ak_vi_get_channel_encoder
 *
 * @brief:	get one channel attribute
 *
 * @param:	channel	- channel index (0-2)
 * @param:	fps		- fps ptr
 * @param:	enc_type- encoder type ptr
 * @param:	profile - profile ptr
 *
 * @return: -1/err, 0/ok
 */
int api_ak_vi_get_channel_encoder(int channel, int* fps, int* enc_type, int* profile )
{
	if( ak_ch_vi_atrr[channel].encoder_ins_id != -1 )
	{
		api_vi_ch_enc_get(channel,NULL,NULL,fps,enc_type,profile);
		return 0;	
	}
	else
		return -1;
}

/**
 * @fn:		api_ak_vi_set_preview
 *
 * @brief:	set one channel preview paras
 *
 * @param:	win		- win index (0-2)
 * @param:	dispx	- display start x
 * @param:	dispy	- display start y
 * @param:	dispw 	- display width
 * @param:	disph 	- display height
 *
 * @return: -1/err, 0/ok
 */
int api_ak_vi_set_preview(int win, int dispx, int dispy, int dispw, int disph )
{
	if( win >= MAX_PREVIEW_WINS )
		return -1;
	ak_vi_preview_set_output(win, dispx, dispy, dispw, disph);
	return 0;	
}

/**
 * @fn:		api_ak_vi_get_preview
 *
 * @brief:	get one channel preview paras
 *
 * @param:	channel	- channel index (0-2)
 * @param:	dispx	- display start x ptr
 * @param:	dispy	- display start y ptr
 * @param:	dispw 	- display width ptr
 * @param:	disph 	- display height ptr
 *
 * @return: -1/err, 0/ok
 */
int api_ak_vi_get_preview(int win, int* dispx, int* dispy, int* dispw, int* disph )
{
	if( win >= MAX_PREVIEW_WINS )
		return -1;
	ak_vi_preview_get_output(win, dispx, dispy, dispw, disph);
	return 0;
}

/**
 * @fn:		api_ak_vi_ch_stream_encode
 *
 * @brief:	open one channel with encoder
 *
 * @param:	channel	- channel index (0-2)
 * @param:	enable	- encoder enable
 *
 * @return: -1/err, 0/ok
 */
int api_ak_vi_ch_stream_encode(int channel, int enable )
{
	if( channel >= AK_VI_CH_MAX )
		return -1;

	ak_ch_vi_atrr_t *pak_ch_vi_atrr = &ak_ch_vi_atrr[channel];
	//log_d("open encode start11111111");
	
	if( enable )
	{
		PrintCurrentTime(100002);
		//log_d("open encode start2222222222");
		//printf("11111111111%s:%d\n",__func__,__LINE__);
		if( !api_ak_vi_ch_is_openned(channel) )
		{
			if( api_ak_vi_ch_open(channel) == -1 )
				return -1;
		}
		PrintCurrentTime(100003);
		//log_d("open encode start33333333");
		//printf("11111111111%s:%d\n",__func__,__LINE__);
		if( api_ak_vi_ch_is_openned(channel) )
		{
			//printf("11111111111%s:%d\n",__func__,__LINE__);
			//log_d("open encode start4444444");
			pthread_mutex_lock(&pak_ch_vi_atrr->lock);
			if( pak_ch_vi_atrr->encoder_ins_id == -1 )
			{
				api_vi_ch_enc_open(channel);
				usleep(100*1000);
				pak_ch_vi_atrr->encoder_ins_id 	= channel;
			}
			pthread_mutex_unlock(&pak_ch_vi_atrr->lock);
			
		}	
		PrintCurrentTime(100004);
		//printf("11111111111%s:%d\n",__func__,__LINE__);
		//log_d("open encode start555555555");
	}
	else
	{
		if( api_ak_vi_ch_is_openned(channel) )
		{
			if( pak_ch_vi_atrr->preview_ins_id == -1 )
					api_ak_vi_ch_close(channel);
			pthread_mutex_lock(&pak_ch_vi_atrr->lock);
			pthread_mutex_unlock(&pak_ch_vi_atrr->lock);
		}
	}
	return 0;
}

/**
 * @fn:		api_ak_vi_ch_stream_preview_on
 *
 * @brief:	open one channel with preview win
 *
 * @param:	channel	- channel index (0-2)
 * @param:	win		- preview win
 *
 * @return: -1/err, 0/ok
 */
 static pthread_mutex_t	 vi_sensor_lock=PTHREAD_MUTEX_INITIALIZER;
int api_ak_vi_ch_stream_preview_on( int channel, int win )
{
	int win_ch,win_state;
	if( channel >= AK_VI_CH_MAX || win >= MAX_PREVIEW_WINS )
		return -1;

	ak_ch_vi_atrr_t *pak_ch_vi_atrr = &ak_ch_vi_atrr[channel];
	pthread_mutex_lock(&vi_sensor_lock);
	if( !api_ak_vi_ch_is_openned(channel) )
	{
		if( api_ak_vi_ch_open(channel) == -1 )
		{
			pthread_mutex_unlock(&vi_sensor_lock);
			return -1;
		}
	}
	if( api_ak_vi_ch_is_openned(channel) )
	{
		win_state = ak_vi_preview_get_state(win);
		if( win_state )
		{
			ak_vi_preview_get_input(win, &win_ch, NULL, NULL);
			if( win_ch != channel )	
			{
				pak_ch_vi_atrr->preview_ins_id 	= -1;
				ak_vi_preview_stop(win);
				ak_vi_preview_set_input(win,channel,ak_ch_vi_atrr[channel].video_w,ak_ch_vi_atrr[channel].video_h);
				ak_vi_preview_start(win, channel);
			}
		}
		else
		{
			ak_vi_preview_set_input(win,channel,ak_ch_vi_atrr[channel].video_w,ak_ch_vi_atrr[channel].video_h);
			ak_vi_preview_start(win, channel);
		}
		pak_ch_vi_atrr->preview_ins_id 	= win;
	}
	pthread_mutex_unlock(&vi_sensor_lock);
	return -1;
}

/**
 * @fn:		api_ak_vi_ch_stream_preview_off
 *
 * @brief:	close one channel
 *
 * @param:	channel	- channel index (0-2)
 *
 * @return: -1/err, 0/ok
 */
int api_ak_vi_ch_stream_preview_off( int channel )
{
	if( channel >= AK_VI_CH_MAX )
		return -1;

	ak_ch_vi_atrr_t *pak_ch_vi_atrr = &ak_ch_vi_atrr[channel];
	pthread_mutex_lock(&vi_sensor_lock);
	if( api_ak_vi_ch_is_openned(channel) )
	{
		if( pak_ch_vi_atrr->preview_ins_id != -1 )
		{
			if(pak_ch_vi_atrr->preview_ins_id>=0)
				ak_vi_preview_stop(pak_ch_vi_atrr->preview_ins_id);
			if(pak_ch_vi_atrr->trans_mask_bit==0&&pak_ch_vi_atrr->detach_break_flag==1)
			{
				api_vi_ch_enc_close(pak_ch_vi_atrr->encoder_ins_id);
			
				api_ak_vi_ch_close(channel);
			}
			else
			{
				pak_ch_vi_atrr->preview_ins_id 	= -1;
				if( pak_ch_vi_atrr->encoder_ins_id == -1 )
					api_ak_vi_ch_close(channel);
			}
		}
	}
	pthread_mutex_unlock(&vi_sensor_lock);
	return 0;
}


int api_ak_vi_ch_stream_preview_hide(int channel)
{
	if( channel >= AK_VI_CH_MAX )
		return -1;

	ak_ch_vi_atrr_t *pak_ch_vi_atrr = &ak_ch_vi_atrr[channel];
	pthread_mutex_lock(&vi_sensor_lock);
	if( api_ak_vi_ch_is_openned(channel) )
	{
		if( pak_ch_vi_atrr->preview_ins_id >= 0 )
		{
			ak_vi_preview_stop(pak_ch_vi_atrr->preview_ins_id);
			pak_ch_vi_atrr->preview_ins_id 	= -2;
		}
	}
	pthread_mutex_unlock(&vi_sensor_lock);
	return 0;
}

int api_ak_vi_ch_stream_preview_change_win(int channel,int new_win)
{
	if( channel >= AK_VI_CH_MAX )
		return -1;

	ak_ch_vi_atrr_t *pak_ch_vi_atrr = &ak_ch_vi_atrr[channel];
	pthread_mutex_lock(&vi_sensor_lock);
	if( api_ak_vi_ch_is_openned(channel) )
	{
		if( pak_ch_vi_atrr->preview_ins_id >= 0 )
		{
			ak_vi_preview_stop(pak_ch_vi_atrr->preview_ins_id);
			pak_ch_vi_atrr->preview_ins_id 	= -2;
			
		}
		ak_vi_preview_set_input(new_win,channel,ak_ch_vi_atrr[channel].video_w,ak_ch_vi_atrr[channel].video_h);
		ak_vi_preview_start(new_win, channel);
		pak_ch_vi_atrr->preview_ins_id 	= new_win;
	}
	pthread_mutex_unlock(&vi_sensor_lock);
	return 0;
}
/**
 * @fn:		api_ak_vi_trans_subscriber
 *
 * @brief:	subscriber one channel encoded stream
 *
 * @param:	channel				- channel index (0-2)
 * @param:	subcribe_type		- subscribe type: 0-udp, 1-rtp, 2-file
 * @param:	subscribe_callback	- subscribed encoded stream process
 *
 * @return: -1/err, x/subscriber ok token
 */
pthread_mutex_t	 vi_trans_sub_lock=PTHREAD_MUTEX_INITIALIZER;
int api_ak_vi_trans_subscriber(int channel, int subcribe_type, subscribe_callback_t subscribe_callback )
{
	ak_ch_vi_atrr_t *pak_ch_vi_atrr = &ak_ch_vi_atrr[channel];
	int token;
	#if 1
	
	api_ak_vi_ch_stream_encode(channel, 1);
	//pthread_mutex_lock(&vi_trans_sub_lock);
	pthread_mutex_lock(&pak_ch_vi_atrr->sub_lock);
	if( api_ak_vi_ch_is_openned(channel) )
	{
		api_add_one_subscriber(&pak_ch_vi_atrr->enc_user_list, pak_ch_vi_atrr->enc_user_token, subcribe_type, subscribe_callback, channel );
		token=pak_ch_vi_atrr->enc_user_token;
		pak_ch_vi_atrr->enc_user_token++;
		pak_ch_vi_atrr->trans_mask_bit |= (1<<subcribe_type);
		pthread_mutex_unlock(&pak_ch_vi_atrr->sub_lock);
		return token;
	}
	else
	{
		pthread_mutex_unlock(&pak_ch_vi_atrr->sub_lock);
		return -1;
	}
	#else
	int ret;
	pak_ch_vi_atrr->enc_user_token++;
	pak_ch_vi_atrr->trans_mask_bit |= (1<<subcribe_type);
	ret=api_add_one_subscriber(&pak_ch_vi_atrr->enc_user_list, pak_ch_vi_atrr->enc_user_token, subcribe_type, subscribe_callback, channel );
	api_ak_vi_ch_stream_encode(channel, 1);
	return ret;
	#endif
}

/**
 * @fn:		api_ak_vi_trans_desubscriber
 *
 * @brief:	subscriber one channel encoded stream
 *
 * @param:	channel			- channel index (0-2)
  * @param:	subcribe_type	- subscribe type: 0-udp, 1-rtp, 2-file
 * @param:	subscribe_token	- subscriber got token
 *
 * @return: -1/err, 0/ok
 */
int api_ak_vi_trans_desubscriber(int channel, int subcribe_type, int subscribe_token )
{
	ak_ch_vi_atrr_t *pak_ch_vi_atrr = &ak_ch_vi_atrr[channel];
	//printf("111111111api_ak_vi_trans_desubscriber subcribe_type=%d,subscribe_token=%d\n",subcribe_type,subscribe_token);
	pthread_mutex_lock(&pak_ch_vi_atrr->sub_lock);
	if( api_del_one_subscriber(&pak_ch_vi_atrr->enc_user_list, subscribe_token) == 0 )
	{
		//printf("22222222222api_ak_vi_trans_desubscriber subcribe_type=%d,subscribe_token=%d\n",subcribe_type,subscribe_token);
		pak_ch_vi_atrr->trans_mask_bit &= (~(1<<subcribe_type));
		pthread_mutex_unlock(&pak_ch_vi_atrr->sub_lock);
		if( pak_ch_vi_atrr->trans_mask_bit == 0 )
		{
			//printf("3333333333333api_ak_vi_trans_desubscriber\n");
			//api_ak_vi_ch_stream_encode(channel,0);
			if(pak_ch_vi_atrr->task_read_frame==pthread_self())
				pak_ch_vi_atrr->detach_break_flag=1;
			else
				api_ak_vi_ch_stream_encode(channel,0);
		}
		
		return 0;
	}
	else
	{
		pthread_mutex_unlock(&pak_ch_vi_atrr->sub_lock);
		return -1;
	}
}
int api_ak_vi_trans_desubscriber_unlock(int channel, int subcribe_type, int subscribe_token )
{
	ak_ch_vi_atrr_t *pak_ch_vi_atrr = &ak_ch_vi_atrr[channel];
	//printf("111111111api_ak_vi_trans_desubscriber subcribe_type=%d,subscribe_token=%d\n",subcribe_type,subscribe_token);
	//pthread_mutex_lock(&vi_trans_sub_lock);
	if( api_del_one_subscriber(&pak_ch_vi_atrr->enc_user_list, subscribe_token) == 0 )
	{
		//printf("22222222222api_ak_vi_trans_desubscriber subcribe_type=%d,subscribe_token=%d\n",subcribe_type,subscribe_token);
		pak_ch_vi_atrr->trans_mask_bit &= (~(1<<subcribe_type));
		//pthread_mutex_unlock(&vi_trans_sub_lock);
		if( pak_ch_vi_atrr->trans_mask_bit == 0 )
		{
			//printf("3333333333333api_ak_vi_trans_desubscriber\n");
			//api_ak_vi_ch_stream_encode(channel,0);
			if(pak_ch_vi_atrr->task_read_frame==pthread_self())
				pak_ch_vi_atrr->detach_break_flag=1;
			else
				api_ak_vi_ch_stream_encode(channel,0);
		}
		
		return 0;
	}
	else
	{
		//pthread_mutex_unlock(&vi_trans_sub_lock);
		return -1;
	}
}

cJSON *GetViState(int ins)
{
	cJSON *state;
	if(ins>=3)
		return NULL;
	state=cJSON_CreateObject();
	if(state==NULL)
		return NULL;
	if(ak_ch_vi_atrr[ins].state == 0)
	{
		cJSON_AddStringToObject(state,"Vi_State","IDLE");
	}
	else 
	{
		cJSON_AddStringToObject(state,"Vi_State","RUNNING");
		
		cJSON_AddNumberToObject(state,"Video_w",ak_ch_vi_atrr[ins].video_w);
		cJSON_AddNumberToObject(state,"Video_h",ak_ch_vi_atrr[ins].video_h);

		if(ak_ch_vi_atrr[ins].preview_ins_id!=-1)
			cJSON_AddStringToObject(state,"Preview","Enable");
		else
			cJSON_AddStringToObject(state,"Preview","Disable");

		if(ak_ch_vi_atrr[ins].encoder_ins_id!=-1)
			cJSON_AddStringToObject(state,"Encoder","Enable");
		else
			cJSON_AddStringToObject(state,"Encoder","Disable");

		if( ak_ch_vi_atrr[ins].trans_mask_bit )
			cJSON_AddStringToObject(state,"Trans","Enable");
		else
			cJSON_AddStringToObject(state,"Trans","Disable");

		
			
		
	}
	
	
	return state;
}
int GetViStateInt(int ins)
{
	return ak_ch_vi_atrr[ins].state;
}
int IfStartEncode(int ins)
{
	if(ak_ch_vi_atrr[ins].encoder_ins_id!=-1)
		return 1;
	return 0;
}

/**
 * @fn:		api_ak_vi_ch_stream_cvbs_output
 *
 * @brief:	enable one channel cvbs output ** the channel should be turn on, other channels just be affacted!!
 *
 * @param:	channel	- channel index (0-2) 
 * @param:	mode	- 1: cvbs, 0.2: bt601
 *
 * @return: -1/err, 0/ok
 */
int api_ak_vi_ch_stream_cvbs_output( int channel, int mode )
{
	if( channel >= AK_VI_CH_MAX )
		return -1;

	ak_ch_vi_atrr_t *pak_ch_vi_atrr = &ak_ch_vi_atrr[channel];
	pthread_mutex_lock(&vi_sensor_lock);
	if( api_ak_vi_ch_is_openned(channel) )
	{
		if( mode <= 2 ) // && !pak_ch_vi_atrr->sensor_output_cvbs )
		{
			pak_ch_vi_atrr->sensor_output_cvbs = mode;
			SENSOR_POWER_SET();			// reset once
			sensor_cvbs_switch(mode);	// initial para
		}
	}
	pthread_mutex_unlock(&vi_sensor_lock);
	return 0;
}
void en_vi_cvbs(void)
{
	pthread_mutex_lock(&vi_dev_lock);
	vi_cvbs_enable=1;
	pthread_mutex_unlock(&vi_dev_lock);
}
void dis_vi_cvbs(void)
{
	pthread_mutex_lock(&vi_dev_lock);
	vi_cvbs_enable=0;
	pthread_mutex_unlock(&vi_dev_lock);
}	
int if_vi_cvbs_en(void)
{
	return vi_cvbs_enable;
}
void dt_video_output_sw(int mode)
{
}
void check_vi_cvbs_process(void)
{
#if 0
	return;

	int i;
	ak_ch_vi_atrr_t *pak_ch_vi_atrr ;
	pthread_mutex_lock(&vi_dev_lock);
	pak_ch_vi_atrr = &ak_ch_vi_atrr[0];
	//printf("vi_cvbs_enable =%d,vi_cvbs_state=%d,check_vi_cvbs_process00000000 %d:preview=%d,encoder=%d:%d:%d\n",vi_cvbs_enable,vi_cvbs_state,pak_ch_vi_atrr->task_run,pak_ch_vi_atrr->preview_ins_id,pak_ch_vi_atrr->encoder_ins_id, pak_ch_vi_atrr->trans_mask_bit,pak_ch_vi_atrr->detach_break_flag);
	if(vi_cvbs_enable==1)
	{
		if(vi_cvbs_state==0)
		{
			
			for(i=0;i<3;i++)
			{
				//printf("check_vi_cvbs_process111111111111\n");
				pak_ch_vi_atrr = &ak_ch_vi_atrr[i];
				if(pak_ch_vi_atrr->task_run&&pak_ch_vi_atrr->encoder_ins_id != -1 && pak_ch_vi_atrr->trans_mask_bit!=0)
					break;
			}
			if(i>=3)
			{
				printf("check_vi_cvbs_process222222222\n");
				SENSOR_POWER_SET();			// reset once
				if(sensor_cvbs_switch(1)==0)
				{
					usleep(100);
					dt_video_output_sw(1);
					vi_cvbs_state=1;
				}
				
			}
		}
		else
		{
			//printf("check_vi_cvbs_process3333333333\n");
			for(i=0;i<3;i++)
			{
				//printf("check_vi_cvbs_process44444444444\n");
				pak_ch_vi_atrr = &ak_ch_vi_atrr[i];
				if( pak_ch_vi_atrr->task_run&&pak_ch_vi_atrr->encoder_ins_id != -1 && pak_ch_vi_atrr->trans_mask_bit!=0)
					break;
			}
			if(i<3)
			{
				printf("check_vi_cvbs_process555555555\n");
				SENSOR_POWER_SET();			// reset once
				if(sensor_cvbs_switch(0)==0)
				{
					
				}
				dt_video_output_sw(0);
				vi_cvbs_state=0;
			}
		}
	}
	else
	{
		if(vi_cvbs_state==1)
		{
			SENSOR_POWER_SET();			// reset once
			if(sensor_cvbs_switch(0)==0)
			{
				
			}
			dt_video_output_sw(0);
			vi_cvbs_state=0;
		}
	}
	pthread_mutex_unlock(&vi_dev_lock);
#endif
}
/**
 * @fn:		api_ak_vi_get_sensor_low_lut_status
 *
 * @brief:	获取sensor夜视灯状态
 *
 * @param:	none
 *
 * @return:     0/day, 0/night, -1/err
 */
int api_ak_vi_get_sensor_low_lut_status( void )
{
	return get_sensor_low_lut_status();
}

/**
 * @fn:		callback_sensor_low_lut_changed
 *
 * @brief:	夜视灯状态改变的回调函数 - 在此函数中可以挂接消息发送事件
 *
 * @param:	status	- 0/day, 1/night
 *
 * @return: none
 */
 

 void callback_sensor_low_lut_changed(int status)
{
	pthread_mutex_lock(&vi_dev_lock);
	if(vi_dev_ref_cnt)
	{
		if( status )
		{
			printf("111SENSOR_DETECT_NIGHTVIEW_TURN_OFF\n");
			// to add msg event
 			vi_nv_light_state = 0;
		}
		else if(vi_nv_light_state == 0)
		{
			printf("1111SENSOR_DETECT_NIGHTVIEW_TURN_ON\n");
			// to add msg event
			DS_NightVisionCtrl(1);
			vi_nv_light_state=1;
		}
		vi_nv_state=!status;
	}
	else
	{
		vi_nv_state=!status;
	}
	pthread_mutex_unlock(&vi_dev_lock);
}
 int get_vi_dev_ref_cnt(void)
 {
	return vi_dev_ref_cnt;
 }
static int trigger_key_frame_flag[3]={0};
void trigger_send_key_frame(int ch)
{
	if(ch<3)
		trigger_key_frame_flag[ch]=1;
}

int If_trigger_key_frame(int ch)
{
	if(ch<3)
	{
		if(trigger_key_frame_flag[ch]==1)
		{
			trigger_key_frame_flag[ch]=0;
			return 1;
		}
	}
	return 0;
}
static int trigger_key_frame_flag2[3]={0};
void trigger_send_key_frame_delay(int ch,int delay)
{
	if(ch<3)
		trigger_key_frame_flag2[ch]=delay;
}

int If_trigger_key_frame_delay(int ch)
{
	if(ch<3)
	{
		
		if(trigger_key_frame_flag2[ch]>0)
		{
			if(--trigger_key_frame_flag2[ch]==0)
				return 1;
		}
	}
	return 0;
}
