
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <asm/types.h>
#include <linux/netlink.h>
#include <linux/socket.h>
#include <errno.h>
#include <syslog.h>

#include <pthread.h>

#include "sensor_low_lut_detect.h"

#define NETLINK_MAX_NLMSGSIZE 	256
#define NETLINK_TEST		17

int nl_sock_fd = -1;

static int netlink_sock_bind( unsigned int pid, unsigned int grp )
{
    	struct sockaddr_nl src_addr;
    	int sock_fd;

    	sock_fd = socket(PF_NETLINK, SOCK_DGRAM, NETLINK_TEST);
    	if(sock_fd == -1) 
	{
        	printf("error getting socket: %s\n", strerror(errno));
    	} 
	else 
	{
        	memset(&src_addr, 0, sizeof(src_addr));
        	src_addr.nl_family 	= AF_NETLINK;
        	src_addr.nl_pid 	= pid; /*unicast*/
        	src_addr.nl_groups 	= grp; /*broadcast*/
        	int status = bind(sock_fd, (struct sockaddr*)&src_addr, sizeof(src_addr));
        	if(status == -1) 
		{
           		printf( "bind failed: %s\n", strerror(errno));
            		sock_fd = -1;
            		close(sock_fd);
        	}
    	}
    	return sock_fd;
}

static int nl_msg_send (int sock_fd, char *data,unsigned int data_len, unsigned int dpid, unsigned int dgrp)
{
	struct sockaddr_nl dest_addr;
    	struct iovec iov;
    	struct msghdr msg;
    	char buf[NETLINK_MAX_NLMSGSIZE];
    	struct nlmsghdr *nlh = NULL;
    	int ret = 0;

    	nlh = (struct nlmsghdr*) buf;

    	if( NLMSG_SPACE(data_len) <= NETLINK_MAX_NLMSGSIZE ) 
	{
        	memset(buf, 0, sizeof(buf));
        	nlh->nlmsg_len = NLMSG_SPACE(data_len);
        	nlh->nlmsg_pid = getpid();
        	nlh->nlmsg_flags |= NLM_F_REQUEST ;
        	nlh->nlmsg_type = 0x26;
        	memcpy(NLMSG_DATA(nlh), data, data_len);

        	memset(&dest_addr,0,sizeof(dest_addr));
        	dest_addr.nl_family = AF_NETLINK;
        	dest_addr.nl_pid = dpid;
        	dest_addr.nl_groups = dgrp;

        	iov.iov_base = (void *)nlh;
        	iov.iov_len = NLMSG_SPACE(data_len);

        	memset(&msg, 0, sizeof(msg));
        	msg.msg_name = (void *)&dest_addr;
        	msg.msg_namelen = sizeof(dest_addr);
        	msg.msg_iov = &iov;
        	msg.msg_iovlen = 1;

        	if(sendmsg(sock_fd, &msg, 0) == -1) 
		{
            		printf( "get error sendmsg = %s\n",strerror(errno) );
        	}
    	}

    	return ret;
}

static void netlink_unicast_pid(int sock_fd )
{	
	char nlData[2];

	nlData[0] = '0';
	nlData[1] = 0;

	if(sock_fd > 0) 
	{
		nl_msg_send(sock_fd, nlData, 2, 0, 0);
	}
}

static void netlink_unicast_detect_on(int sock_fd)
{	
	char nlData[10];

	if( sock_fd <= 0)
		return; 

	memset(nlData, 0, 10);
	nlData[0] = 's';
	nlData[1] = 't';
	nlData[2] = 'a';
	nlData[3] = 'r';
	nlData[4] = 't';
	nlData[5] = 0;
	nl_msg_send(sock_fd, nlData, 6, 0, 0);
}

static void netlink_unicast_detect_off(int sock_fd)
{	
	char nlData[10];

	if( sock_fd <= 0)
		return; 

	memset(nlData, 0, 10);
	nlData[0] = 's';
	nlData[1] = 't';
	nlData[2] = 'o';
	nlData[3] = 'p';
	nlData[4] = 0;
	nl_msg_send(sock_fd, nlData, 5, 0, 0);
}

static int low_lut_status = -1;		// 0:not low lut, 1: low lut

void* netlink_wait_message(void* arg)
{
	cb_low_lut cb = (cb_low_lut)arg;

    	struct sockaddr_nl nl_addr;
    	struct iovec iov;
    	struct msghdr msg;
    	char   buf[NETLINK_MAX_NLMSGSIZE];
    	struct nlmsghdr *nlh = NULL;
	char*  nlData;

    	memset(&msg, 0, sizeof(msg));
    	nlh = (struct nlmsghdr*) buf;
    	iov.iov_base    = (void *)nlh;
    	msg.msg_name    = (void *)&nl_addr;
    	msg.msg_namelen = sizeof(nl_addr);
    	msg.msg_iov     = &iov;
    	msg.msg_iovlen  = 1;

    	nl_sock_fd = netlink_sock_bind( getpid(), 0);
	
    	if( nl_sock_fd < 0) 
	{
        	printf( "netlink_sock_bind error\n");
		return 0;
    	}

    	//netlink_unicast_pid( nl_sock_fd );
		netlink_unicast_detect_on(nl_sock_fd);

	printf( "waiting for netlink message...\n");

	low_lut_status = -1;	
    	while(1) 
	{
        	iov.iov_len = NETLINK_MAX_NLMSGSIZE;
        	memset(buf, 0 ,sizeof(buf));
		
        	if( recvmsg( nl_sock_fd, &msg, 0) > 0) 
		{
            		nlData = (char*)NLMSG_DATA(nlh);

            		switch(nlData[0])
            		{            	
                		case '1':
				if( low_lut_status != 1 )
				{
					low_lut_status = 1;
					(*cb)(low_lut_status);
				}
                    		break;
                		case '0':
				if( low_lut_status != 0 )
				{
					low_lut_status = 0;
					(*cb)(low_lut_status);
				}
                    		break;
                		default:
				printf("%s\n",nlData);
                    		break;
            		}
        	}
    	}
    	return 0;
}

static pthread_t pid_netlink = -1;

/**
 * @fn:		sensor_low_lut_detect_start
 *
 * @brief:	启动夜视灯的定时检测，sensor驱动会300ms检测一次，检测结果会通过cb回调函数返回
 *
 * @param:	cb - 夜视状态更改时的消息通知回调函数，回调函数参数 - 0/day(白天)，1/night(夜晚)
 *
 * @return:     -1/err, 0/ok
 */
int sensor_low_lut_detect_start(cb_low_lut cb)
{
	if( pid_netlink != -1 )
		return -1;

	if( pthread_create( &pid_netlink, NULL, netlink_wait_message, cb) != 0 )
	{
		perror( "create thread error!\n");
		return -1;
	}
	return 0;
}

/**
 * @fn:		sensor_low_lut_detect_stop
 *
 * @brief:	停止夜视灯的定时检测
 *
 * @param:	none
 *
 * @return: -1/err, 0/ok
 */
int sensor_low_lut_detect_stop(void)
{
	if( pid_netlink != -1 )
	{
		netlink_unicast_detect_off(nl_sock_fd);
		usleep(10*1000);
        close(nl_sock_fd);
		nl_sock_fd= -1;
		pthread_cancel(pid_netlink);
		pthread_join(pid_netlink,NULL);
		pid_netlink = -1;
		return 0;
	}
	else
		return -1;
}

/**
 * @fn:		get_sensor_low_lut_status
 *
 * @brief:	获取sensor夜视灯状态
 *
 * @param:	none
 *
 * @return:     0/day, 0/night
 */
int get_sensor_low_lut_status(void)
{
	return low_lut_status;
}
static short sensor7610_lut=-1;

void set_sensor7610_lut(char *lut)
{
	if(lut==NULL)
		sensor7610_lut=-999;
	else
		sensor7610_lut=((lut[0]<<8)|lut[1]);
}
int get_sensor7610_lut(void)
{
	return sensor7610_lut;
}
void* sensor7610_lut_read(void* arg)
{
	int lut;
	int threshold;
	int lut_status;
	int filter_flag=0;
	low_lut_status =1;	
	cb_low_lut cb = (cb_low_lut)arg;
	while(1)
	{
		if(filter_flag)
			usleep(200*1000);
		else
			usleep(1000*1000);
		threshold=API_Para_Read_Int("7610NightvisionThreshold");
		if(API_PublicInfo_Read_Int("CameraOnState"))
		{
			set_sensor7610_lut(NULL);
			cmr7610_send_pack(0x3c,0);
			usleep(100*1000);
			lut=get_sensor7610_lut();
			if(lut>-999)
			{
				//printf("sensor7610_lut_read:%d\n",lut);
				
				API_PublicInfo_Write_Int("Sensor7610_Lut", lut);
				#if 1
				if(lut<=threshold)
					lut_status=0;
				else
					lut_status=1;
				if(low_lut_status!=lut_status)
				{
					if(filter_flag==0)
						filter_flag=1;
					else
					{
						switch(lut_status)
			            		{            	
			                		case 1:
							if( low_lut_status != 1 )
								{
									low_lut_status = 1;
									(*cb)(low_lut_status);
								}
			                    		break;
			                		case 0:
								if( low_lut_status != 0 )
								{
									low_lut_status = 0;
									(*cb)(low_lut_status);
								}
			                    		break;
			                		default:
							//printf("%s\n",nlData);
			                    		break;
			            		}
						filter_flag=0;
					}
				}
				else
					filter_flag=0;
				#endif
			}
			
		}
		else
		{
			low_lut_status =1;
			filter_flag=0;
		}
	}
}
static pthread_t pid_cmr7610_lut = -1;

/**
 * @fn:		sensor_low_lut_detect_start
 *
 * @brief:	启动夜视灯的定时检测，sensor驱动会300ms检测一次，检测结果会通过cb回调函数返回
 *
 * @param:	cb - 夜视状态更改时的消息通知回调函数，回调函数参数 - 0/day(白天)，1/night(夜晚)
 *
 * @return:     -1/err, 0/ok
 */
int sensor7610_low_lut_detect_start(cb_low_lut cb)
{
	if( pid_cmr7610_lut != -1 )
		return -1;

	if( pthread_create( &pid_cmr7610_lut, NULL, sensor7610_lut_read, cb) != 0 )
	{
		perror( "create thread error!\n");
		return -1;
	}
	return 0;
}

