
/**
 * Description: 3路视频rtp发送类型处理
 * Author: 		lvzhihui
 * Create: 		2012-01-23
*/

#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <list.h>

#include "rtp_video_send.h"

#include "obj_ak_vi_userlist.h"
#include "obj_ak_vi_manager.h"
#include "cJSON.h"

typedef struct
{
	// udp type
	int					send_mode;		// 0:multicast,11:unicast
    VtkRtpSendObj_T*    send_sess;      // send session
	int					video_type;		// h264,h265
} VD_RTP_SEND_T;

typedef struct
{
	struct list_head    one_node;	// 节点指针
	VD_RTP_SEND_T		target;		// 发送目标
	int					channel;	// 视频通道
} VD_RTP_SEND_NODE_t;

typedef struct
{
	int					token;		// 订阅token	
	int                 send_num;  	// 发送总数
	int                 send_sn;  	// 发送序列号
	struct list_head    send_list; 	// 发送列表	
	pthread_mutex_t     lock;		// 队列锁
} VD_RTP_SEND_LIST_T;

static VD_RTP_SEND_LIST_T	one_rtp_send_list[3];

/*
int start_video_rtp_sender_wan(char* pserver, short port, int ts_inc);
int start_video_rtp_sender_lan(char* pserver, short port, int ts_inc);
// func:
//		delete one rtp session, if stop the video stream send, call this
// para:
//		none
// return:
//		0/ok, -1/error
//int stop_video_rtp_sender(VtkRtpSendObj_T *VtkRtpSendObj);
int stop_video_rtp_sender_wan(void);
int stop_video_rtp_sender_lan(void);

// lzh_20210222_s			
// func:
//		send one video packet data, if get one frame data, call this to send
// para:
//		buf:	data pointer
//		len:	data length
//		tick:	timestamp, if 0, use thread timestamp
// return:
//		0/ok, -1/error
//int rtp_sender_send_with_ts(VtkRtpSendObj_T *VtkRtpSendObj,char *buf, int len, int tick );
// lzh_20210222_e	
int rtp_sender_send_with_ts_wan(char *buf, int len, int tick,int vdtype);
int rtp_sender_send_with_ts_lan(char *buf, int len, int tick,int vdtype);
*/

static VtkRtpSendObj_T* new_one_rtp_send_session( char* ip_str, short ip_port)
{
	VtkRtpSendObj_T *ptr_rtp_send = malloc(sizeof(VtkRtpSendObj_T));
	if( ptr_rtp_send != NULL )
	{
		memset( ptr_rtp_send, 0, sizeof(VtkRtpSendObj_T) );
		if( start_video_rtp_sender( ptr_rtp_send, ip_str, ip_port, 0 ) != 0 )
		{
			free(ptr_rtp_send);
			printf("new_one_rtp_send_session[%s:%d] failed!\n",ip_str,ip_port);
			return NULL;
		}
		else
		{
			ptr_rtp_send->pkt_send_flag=1;
			printf("new_one_rtp_send_session[%s:%d] successfu!\n",ip_str,ip_port);
			return ptr_rtp_send;
		}
	}
	else
		return NULL;
}

static int free_one_rtp_send_session( VtkRtpSendObj_T* ptr_rtp_send )
{
	if( ptr_rtp_send != NULL )
	{
		if( stop_video_rtp_sender(ptr_rtp_send) == 0 )
		{
			printf("free_one_rtp_send_session[%s:%d] successfu!\n",ptr_rtp_send->vd_server,ptr_rtp_send->vd_port);
			free(ptr_rtp_send);
			return 0;
		}
		else
		{
			printf("free_one_rtp_send_session[%s:%d] failed!\n",ptr_rtp_send->vd_server,ptr_rtp_send->vd_port);
			free(ptr_rtp_send);
			return -1;
		}
	}
	else
		return -1;
}

static int send_one_rtp_buf( VtkRtpSendObj_T* ptr_rtp_send, char *buf, int len, int tick ,int vdtype )
{	
	return rtp_sender_send_with_ts(ptr_rtp_send, buf, len, tick , vdtype);
}
/**
 * @fn:		init_rtp_send_list
 *
 * @brief:	3路RTP发送列表初始化
 *
 * @param:	none
 *
 * @return: -1/err, 0/ok
 */
void init_rtp_send_list(void)
{
	for( int i = 0; i < 3; i++ )
	{
		pthread_mutex_init( &one_rtp_send_list[i].lock, 0);		
		INIT_LIST_HEAD(&one_rtp_send_list[i].send_list);
		one_rtp_send_list[i].send_num 	= 0;
		one_rtp_send_list[i].send_sn 	= 0;
		one_rtp_send_list[i].token 		= 0;
	}
}

/**
 * @fn:		rtp_check_send_node
 *
 * @brief:	检测指定列表中发送端口的有效性，避免重复添加
 *
 * @param:	plist		    - 发送节点列表
 * @param:	channel		    - 发送通道号
 * @param:	send_mode	    - 发送类型
 * @param:	send_addr	    - send ip str
 * @param:	send_port	    - send port
 *
 * @return: -1/err, 0/ok
 */
static int rtp_check_send_node( struct list_head *plist, int channel, int send_mode, char* send_addr, short send_port )
{
#if 1
	int ret = -1;
	struct list_head *p_node;
	list_for_each(p_node,plist)
	{
		VD_RTP_SEND_NODE_t* pnode = list_entry(p_node,VD_RTP_SEND_NODE_t,one_node);
		if( (pnode != NULL) && (pnode->channel == channel) && (pnode->target.send_mode == send_mode) && (pnode->target.send_sess != NULL) )
		{
			if( (strcmp( pnode->target.send_sess->vd_server, send_addr)== 0) && (pnode->target.send_sess->vd_port == send_port) )
			{
				ret = 0;
				break;
			}
		}
	}
	return ret;
#endif
}

/**
 * @fn:		rtp_add_one_send_node
 *
 * @brief:	RTP发送端口添加
 *
 * @param:	plist		    - 发送节点列表
 * @param:	channel		    - 发送通道号
 * @param:	send_mode	    - 发送类型
 * @param:	send_addr	    - send ip str
 * @param:	send_port	    - send port
 *
 * @return: -1/err, 0/ok
 */
static int rtp_add_one_send_node( struct list_head *plist, int channel, int send_mode, char* send_addr, short send_port )
{
	int vd_type;
	VD_RTP_SEND_NODE_t *new_node = (VD_RTP_SEND_NODE_t*)malloc( sizeof(VD_RTP_SEND_NODE_t) );
	if(new_node != NULL)
	{
		INIT_LIST_HEAD(&new_node->one_node);
		new_node->channel 			= channel;
		new_node->target.send_mode	= send_mode;
		api_ak_vi_get_channel_encoder(channel,NULL,&vd_type,NULL);
		new_node->target.send_sess	= new_one_rtp_send_session(send_addr,send_port);
		new_node->target.video_type = (vd_type==H264_ENC_TYPE?0:1); //vd_type;
        // add node process
		list_add_tail(&new_node->one_node, plist); 
        return 0;;
	}
    else
        return -1;
}

/**
 * @fn:		rtp_del_one_send_node
 *
 * @brief:	RTP发送端口释放
 *
 * @param:	plist		    - 发送节点列表
 * @param:	channel		    - 发送通道号
 * @param:	send_addr	    - send ip str
 * @param:	send_port	    - send port
 *
 * @return: -1/err, 0/ok
 */
static int rtp_del_one_send_node( struct list_head *plist, int channel, int send_mode, char* send_addr, short send_port )
{
#if 1
	int ret = -1;
	struct list_head *p_node,*p_node_temp;
	list_for_each_safe(p_node,p_node_temp, plist)
	{
		VD_RTP_SEND_NODE_t* pnode = list_entry(p_node,VD_RTP_SEND_NODE_t,one_node);
		if(  (pnode->channel == channel) && (pnode->target.send_mode == send_mode) && (pnode->target.send_sess != NULL)  )
		{
			if( (strcmp( pnode->target.send_sess->vd_server, send_addr)== 0) && (pnode->target.send_sess->vd_port == send_port) )
			{
				free_one_rtp_send_session(pnode->target.send_sess);
				pnode->target.send_sess = NULL;
				// release node process
				list_del_init(&pnode->one_node);
				free(pnode);
				ret = 0;
				break;
			}
		}
	}
	return ret;
#endif
}

/**
 * @fn:		rtp_del_all_send_node
 *
 * @brief:	RTP发送端口释放
 *
 * @param:	plist		    - 发送节点列表
 *
 * @return: -1/err, 0/ok
 */
static int rtp_del_all_send_node( struct list_head *plist )
{
	int ret = -1;
	struct list_head *p_node,*p_node_temp;
	list_for_each_safe(p_node,p_node_temp, plist)
	{
		VD_RTP_SEND_NODE_t* pnode = list_entry(p_node,VD_RTP_SEND_NODE_t,one_node);
		if( pnode != NULL )
		{
			free_one_rtp_send_session(pnode->target.send_sess);
			pnode->target.send_sess = NULL;
			// release node process
			list_del_init(&pnode->one_node);
			free(pnode);
		}
	}
	return 0;
}

/**
 * @fn:		rtp_get_one_send_node
 *
 * @brief:	获取指定的发送节点
 *
 * @param:	plist		    - 发送节点列表
 * @param:	channel		    - 发送通道号
 * @param:	send_addr	    - send ip str
 * @param:	send_port	    - send port
 *
 * @return: NULL/err, VD_RTP_SEND_NODE_t/ok
 */
static VD_RTP_SEND_NODE_t* rtp_get_one_send_node( struct list_head *plist, int channel, int send_mode, char* send_addr, short send_port )
{
#if 1
	struct list_head *p_node,*p_node_temp;
	list_for_each_safe(p_node,p_node_temp, plist)
	{
		VD_RTP_SEND_NODE_t* pnode = list_entry(p_node,VD_RTP_SEND_NODE_t,one_node);
		if(  (pnode->channel == channel) && (pnode->target.send_mode == send_mode) && (pnode->target.send_sess != NULL)  )
		{
			if( (strcmp( pnode->target.send_sess->vd_server, send_addr)== 0) && (pnode->target.send_sess->vd_port == send_port) )
				return pnode;
		}
	}
	return NULL;
#endif
}

/**
 * @fn:		send_rtp_enc_data
 *
 * @brief:	RTP发送回调函数
 *
 * @param:	channel		- 发送通道号
 * @param:	pbuf		- 发送数据指针
 * @param:	size		- 发送数据大小
 * @param:	extptr		- extend data ptr
 *
 * @return: none
 */
static void send_rtp_enc_data( int channel, char *pbuf, int size, void* extptr )
{
	int send_fragment_len;

	VD_RTP_SEND_LIST_T *ptr_rtp_send_list = &one_rtp_send_list[channel];

	pthread_mutex_lock(&one_rtp_send_list[channel].lock);

	struct list_head *p_node,*p_node_temp;
	list_for_each_safe(p_node,p_node_temp, &ptr_rtp_send_list->send_list)
	{
		VD_RTP_SEND_NODE_t* pnode = list_entry(p_node,VD_RTP_SEND_NODE_t,one_node);
		send_one_rtp_buf( pnode->target.send_sess, pbuf, size, 0, pnode->target.video_type );

		//printf("send_rtp_enc_data,channel[%d],len=[%d]\n",channel,size);

	}

	pthread_mutex_unlock(&one_rtp_send_list[channel].lock);
}

/**
 * @fn:		api_rtp_add_one_send_node
 *
 * @brief:	add one send node, if the channel list have no send node, subscriber one rtp token from encoder channel
 *
 * @param:	channel		    - channel index (0-2)
 * @param:	send_mode	    - send mode
 * @param:	send_ip	    	- send ip addr
 * @param:	send_port	    - send port
 *
 * @return: -1/err, x/total target
 */
int api_rtp_add_one_send_node( int channel, int send_mode, char* send_ip, short send_port )
{
	printf("rtp add node: channel=%d,send_mode=%d,send_ip=%s,send_port=%d\n",channel,send_mode,send_ip,send_port);

	pthread_mutex_lock(&one_rtp_send_list[channel].lock);

	if( rtp_check_send_node(&one_rtp_send_list[channel].send_list, channel, send_mode, send_ip, send_port ) != 0 )
	{
		if( !one_rtp_send_list[channel].send_num )
		{
			one_rtp_send_list[channel].token = api_ak_vi_trans_subscriber(channel, SUBSCRIBER_TYPE_RTP, send_rtp_enc_data);
			printf("token=%d\n",one_rtp_send_list[channel].token);
		}
		if( rtp_add_one_send_node( &one_rtp_send_list[channel].send_list, channel, send_mode, send_ip, send_port ) == 0 )
		{
			one_rtp_send_list[channel].send_num++;
			pthread_mutex_unlock(&one_rtp_send_list[channel].lock);
			printf("add: one_rtp_send_list[%d].send_num=%d\n",channel,one_rtp_send_list[channel].send_num);
			trigger_send_key_frame(channel);
			return one_rtp_send_list[channel].send_num;
		}
		else
		{
			printf("add err: trans desubscriber,token[%d]\n",one_rtp_send_list[channel].token);
			api_ak_vi_trans_desubscriber(channel, SUBSCRIBER_TYPE_RTP, one_rtp_send_list[channel].token);
			pthread_mutex_unlock(&one_rtp_send_list[channel].lock);
			return -1;
		}
	}
	else
	{
		printf("!!!!!! rtp_check_send_node is repeated !!!\n");
	}
	pthread_mutex_unlock(&one_rtp_send_list[channel].lock);	
	return -1;
}

/**
 * @fn:		api_rtp_del_one_send_node
 *
 * @brief:	delete one send node, if the channel list have no send node, desubscriber the rtp token from encoder channel
 *
 * @param:	channel		    - channel index (0-2)
 * @param:	send_mode	    - send mode
 * @param:	send_ip	    	- send ip addr
 * @param:	send_port	    - send port
 *
 * @return: -1/err, 0/ok
 */
int api_rtp_del_one_send_node( int channel, int send_mode, char* send_ip, short send_port )
{
	int ret;

	printf("rtp del node: channel=%d,send_mode=%d,send_ip=%s,send_port=%d\n",channel,send_mode,send_ip,send_port);

	pthread_mutex_lock(&one_rtp_send_list[channel].lock);
	if( one_rtp_send_list[channel].send_num )
	{
		if( rtp_del_one_send_node(&one_rtp_send_list[channel].send_list, channel, send_mode, send_ip, send_port ) == 0 )
		{
			one_rtp_send_list[channel].send_num--;
			if( !one_rtp_send_list[channel].send_num )
				ret = api_ak_vi_trans_desubscriber(channel, SUBSCRIBER_TYPE_RTP, one_rtp_send_list[channel].token);
			else
				ret = 0;
			pthread_mutex_unlock(&one_rtp_send_list[channel].lock);
			printf("del: one_rtp_send_list[%d].send_num=%d\n",channel,one_rtp_send_list[channel].send_num);
			return ret;
		}
		else
		{
			printf("del err: one_rtp_send_list[%d].send_num=%d\n",channel,one_rtp_send_list[channel].send_num);
			pthread_mutex_unlock(&one_rtp_send_list[channel].lock);
			return -1;
		}
	}
	else
	{
		printf("del none\n");
		pthread_mutex_unlock(&one_rtp_send_list[channel].lock);
		return -1;
	}
}

/**
 * @fn:		api_rtp_get_one_send_session
 *
 * @brief:	get one send node's session
 *
 * @param:	channel		    - channel index (0-2)
 * @param:	send_mode	    - send mode
 * @param:	send_ip	    	- send ip addr
 * @param:	send_port	    - send port
 *
 * @return: NULL/err, NOT NULL/ok
 */
void* api_rtp_get_one_send_session( int channel, int send_mode, char* send_ip, short send_port )
{
	VD_RTP_SEND_NODE_t* pnode;

	printf("get node: channel=%d,send_mode=%d,send_ip=%s,send_port=%d\n",channel,send_mode,send_ip,send_port);

	pthread_mutex_lock(&one_rtp_send_list[channel].lock);

	pnode = rtp_get_one_send_node(&one_rtp_send_list[channel].send_list, channel, send_mode, send_ip, send_port );
	if( pnode != NULL )
	{
		pthread_mutex_unlock(&one_rtp_send_list[channel].lock);
		return (void*)pnode->target.send_sess;
	}
	else
	{
		pthread_mutex_unlock(&one_rtp_send_list[channel].lock);
		return NULL;
	}
}

/**
 * @fn:		api_rtp_del_all_send_node
 *
 * @brief:	delete all send node
 *
 * @param:	channel		    - channel index (0-2)
 *
 * @return: -1/err, 0/ok
 */
int api_rtp_del_all_send_node( int channel )
{
	int ret;
	printf("del channel[%d] all nodes start\n",channel);
	pthread_mutex_lock(&one_rtp_send_list[channel].lock);
	if(one_rtp_send_list[channel].send_num==0)
	{
		pthread_mutex_unlock(&one_rtp_send_list[channel].lock);
		return 0;
	}
	rtp_del_all_send_node(&one_rtp_send_list[channel].send_list);
	one_rtp_send_list[channel].send_num=0;
	pthread_mutex_unlock(&one_rtp_send_list[channel].lock);
	api_ak_vi_trans_desubscriber(channel, SUBSCRIBER_TYPE_RTP, one_rtp_send_list[channel].token);
	printf("del channel[%d] all nodes doen\n",channel);
	return 0;
}
