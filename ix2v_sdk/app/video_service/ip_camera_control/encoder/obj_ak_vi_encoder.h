
#ifndef _OBJ_AK_VI_ENCODER_H_
#define	_OBJ_AK_VI_ENCODER_H_

#include "ak_common.h"
#include "ak_common_graphics.h"
#include "ak_vo.h"
#include "ak_vi.h"
#include "ak_mem.h"
#include "ak_log.h"
#include "ak_tde.h"
#include "ak_venc.h"
#include "ak_vpss.h"

void ak_encoder_reso_reset(int ch0_w, int ch0_h, int ch1_w, int ch1_h );

/**
 * @fn:		api_vi_ch_enc_open
 *
 * @brief:	打开编码设备
 *
 * @param:	channel - 编码通道号
 *
 * @return: -1/err, 0/ok
 */
int api_vi_ch_enc_open(int channel);

/**
 * @fn:		api_vi_ch_enc_get_handle
 *
 * @brief:	获取编码通道的句柄
 *
 * @param:	channel - 编码通道号
 *
 * @return: -1/err, 0/ok
 */
int api_vi_ch_enc_get_handle(int channel);

/**
 * @fn:		api_vi_ch_enc_close
 *
 * @brief:	关闭编码设备
 *
 * @param:	channel - 编码通道号
 *
 * @return: -1/err, 0/ok
 */
int api_vi_ch_enc_close(int channel);

/**
 * @fn:		api_vi_ch_enc_get
 *
 * @brief:	获取编码通道的配置参数
 *
 * @param:	channel - 编码通道号
 * @param:	w       - 编码分辨率宽度指针
 * @param:	h       - 编码分辨率高度指针
 * @param:	fps     - 编码帧率指针
 * @param:	enc_type- 编码264-265类型指针
 * @param:	profile - 编码profile类型指针
 *
 * @return: -1/err, 0/ok
 */
int api_vi_ch_enc_get(int channel, int *w, int *h, int *fps, int *enc_type, int *profile);

/**
 * @fn:		api_vi_ch_enc_set
 *
 * @brief:	设置编码通道的配置参数
 *
 * @param:	channel - 编码通道号
 * @param:	w       - 编码分辨率宽度
 * @param:	h       - 编码分辨率高度
 * @param:	fps     - 编码帧率
 * @param:	enc_type- 编码264-265类型
 * @param:	profile - 编码profile类型
 *
 * @return: -1/err, 0/ok
 */
int api_vi_ch_enc_set(int channel, int w, int h, int fps, int enc_type, int profile);

#endif



