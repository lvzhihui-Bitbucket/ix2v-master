
#ifndef _SENSOR_LOW_LUT_DETECT_H_
#define _SENSOR_LOW_LUT_DETECT_H_

typedef void (*cb_low_lut)(int);

/**
 * @fn:		sensor_low_lut_detect_start
 *
 * @brief:	启动夜视灯的定时检测，sensor驱动会300ms检测一次，检测结果会通过cb回调函数返回
 *
 * @param:	cb - 夜视状态更改时的消息通知回调函数，回调函数参数 - 0/day(白天)，1/night(夜晚)
 *
 * @return:     -1/err, 0/ok
 */
int sensor_low_lut_detect_start(cb_low_lut cb);


/**
 * @fn:		sensor_low_lut_detect_stop
 *
 * @brief:	停止夜视灯的定时检测
 *
 * @param:	none
 *
 * @return: -1/err, 0/ok
 */
int sensor_low_lut_detect_stop(void);

/**
 * @fn:		get_sensor_low_lut_status
 *
 * @brief:	获取sensor夜视灯状态
 *
 * @param:	none
 *
 * @return:     0/day, 0/night
 */
int get_sensor_low_lut_status(void);

#endif


