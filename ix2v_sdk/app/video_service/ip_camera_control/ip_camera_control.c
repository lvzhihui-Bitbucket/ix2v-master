/**
 * Description: ��������UDP����ͨ�������͹رշ���
 * Author: 		lvzhihui
 * Create: 		2012-01-23
 * 
 */

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>      /* basic system data types */
#include <error.h>

#include "ip_camera_control.h"

ip_camera_server_state	one_ip_camera_server;

/**
 * @fn:		ip_camera_control_init
 *
 * @brief:	��Ƶ�������˳�ʼ��
 *
 * @param:	none
 *
 * @return: -1/err, 0/ok
 */
int ip_camera_control_init(void)
{
	one_ip_camera_server.type 	= ip_camera_none;
	one_ip_camera_server.active	= ip_camera_idle;
	one_ip_camera_server.ip		= 0;
	one_ip_camera_server.dev_id	= 0;
	init_one_subscriber_server();
	// lzh_20240322_s
	read_io_sensor_pwr_off();
	// lzh_20240322_e
}

ip_video_trans_reso get_resolution_according_to_channel(int channel);

/**
 * @fn:		api_camera_server_turn_on
 *
 * @brief:	���������Ƶ����
 *
 * @param:		channel		- ����ͨ��
 * @param:		trans_type	- ��������
 * @param:  	client_ip	- �ͻ���IP��ַ
 * @param:  	period		- ����ʱ��
 *
 * @return: 	-1/err, 0/ok
 */
int api_camera_server_turn_on( int channel, ip_camera_type trans_type, int client_ip, int period )
{
	int 			user_cnt;
	int				ip_addr;
	unsigned short	ip_port;
	
	if( one_ip_camera_server.active == ip_camera_run )
	{
		vd_printf("send server request command to ip = 0x%08x, just busy!!!!!\n",client_ip);
		return -1;
	}

	vd_printf("send server request command to ip = 0x%08x, start...\n",client_ip);

	one_ip_camera_server.type 	= trans_type;
	one_ip_camera_server.active = ip_camera_run;

	switch( trans_type )
	{
		case ip_camera_local:
			api_ak_vi_ch_stream_preview_on(0,0);
			api_ak_vi_ch_stream_preview_on(1,1);
			api_ak_vi_ch_stream_preview_on(2,2);
			break;

		case ip_camera_multicast:
			printf("=====================muitiast 1========================\n");
			one_ip_camera_server.ip = client_ip;			
			get_server_trans_addr_port( client_ip, ip_video_multicast, get_resolution_according_to_channel(channel), &ip_addr, &ip_port );
			if( api_enable_video_server_transfer(channel, 0, ip_addr, ip_port) == 0 )
			{
				one_ip_camera_server.send_ch	= channel;
				one_ip_camera_server.send_ip 	= ip_addr;				
				one_ip_camera_server.send_port 	= ip_port;		
				// �Ǽǵ�user list
				vd_printf("activate_subscriber_list,channel=%d, type=%d, ip=0x%08x, time=%d\n",channel,0,ip_addr,period);
				user_cnt = activate_subscriber_list_ext(channel, 0, ip_addr, period);
				vd_printf("activate_subscriber_list,user total = %d\n",user_cnt);
			}
			else
			{
				vd_printf("api_enable_video_server_transfer failed!!!\n");
				one_ip_camera_server.active = ip_camera_idle;				
			}
			break;

		case ip_camera_unicast:
			printf("=====================unicast 1========================\n");
			one_ip_camera_server.ip = client_ip;			
			get_server_trans_addr_port( client_ip, ip_video_unicast, get_resolution_according_to_channel(channel), &ip_addr, &ip_port );
			if( api_enable_video_server_transfer(channel, 1, ip_addr, ip_port) == 0 )
			{
				one_ip_camera_server.send_ch	= channel;
				one_ip_camera_server.send_ip 	= ip_addr;				
				one_ip_camera_server.send_port 	= ip_port;				
				// �Ǽǵ�user list
				vd_printf("activate_subscriber_list,channel=%d, type=%d, ip=0x%08x, time=%d\n",channel,1,ip_addr,period);
				user_cnt = activate_subscriber_list_ext(channel, 1, ip_addr, period);
				vd_printf("activate_subscriber_list,user total = %d\n",user_cnt);
			}
			else
			{
				vd_printf("api_enable_video_server_transfer failed!!!\n");
				one_ip_camera_server.active = ip_camera_idle;				
			}
			break;
	}	
	return 0;	
}


/**
 * @fn:		api_camera_server_turn_off
 *
 * @brief:	�ر���Ƶ����
 *
 * @param:	none
 *
 * @return: 	-1/err, 0/ok
 */
int api_camera_server_turn_off( void )
{	
	int user_cnt,type;
	if( one_ip_camera_server.active == ip_camera_idle )
	{
		vd_printf("api_camera_server_turn_off is idle\n");
		return -1;
	}

	vd_printf("api_camera_server_turn_off to ip = 0x%08x\n",one_ip_camera_server.ip);

	one_ip_camera_server.active	= ip_camera_idle;

	switch( one_ip_camera_server.type )
	{
		case ip_camera_local:
			break;

		case ip_camera_unicast:
		case ip_camera_multicast:
			type = (one_ip_camera_server.type==ip_camera_multicast)?0:1;
			user_cnt = deactivate_subscriber_list_ext(one_ip_camera_server.send_ch, type, one_ip_camera_server.send_ip);	
			vd_printf("disactivate_subscriber_list,user total = %d\n",user_cnt);
			// �㲥���ͣ�ͬʱ��������, �鲥���ͣ��޶�����ر���Ƶ����
			if( type || (user_cnt <= 0) )
			{
				api_disable_video_server_transfer(one_ip_camera_server.send_ch, (one_ip_camera_server.type==ip_camera_multicast)?0:1, one_ip_camera_server.send_ip, one_ip_camera_server.send_port);
			}
			break;
	}
	return 0;		
}


//--------------------------------------
// test interface
//--------------------------------------

// �������ˣ�����������ƵԤ��
void api_local_preview_on( int channel )
{
	api_camera_server_turn_on(channel, ip_camera_local,0,0);
}
// �������ˣ��رձ�����ƵԤ��
void api_local_preview_off( int channel )
{
	api_camera_server_turn_off();
}

// �������ˣ�ѡ��һ��ͨ���������������͡���ָ�����ͻ��˵�ַ����ѡ�񡰷���ʱ�䡱
void api_capture_unicast_on(int channel, int client_ip, int period )
{
	api_camera_server_turn_on(channel, ip_camera_unicast,client_ip,period);
}
// �������ˣ��رշ���ͨ��
void api_capture_unicast_off(int channel)
{
	api_camera_server_turn_off();
}

// �������ˣ�ѡ��һ��ͨ���������鲥���͡���ָ�����ͻ��˵�ַ����ѡ�񡰷���ʱ�䡱
void api_capture_multicast_on(int channel, int client_ip, int period)
{
	api_camera_server_turn_on(channel, ip_camera_multicast,client_ip,period);
}

// �������ˣ��رշ���ͨ��
void api_capture_multicast_off(int channel)
{
	api_camera_server_turn_off();
}
