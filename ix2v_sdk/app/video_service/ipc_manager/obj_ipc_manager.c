#include <stdio.h>
#include "cJSON.h"
#include "obj_ipc_manager.h"
#include "define_file.h"
#include "obj_IoInterface.h"
#include "elog.h"
#include "obj_TableSurver.h"
/////////////////////////////////////////////////////////////////////////////////////////////////

int SetIpcChSelect(int* rec_ch, int* full_ch, int* quad_ch, int* app_ch,  cJSON* ch_dat)
{
	int ch_id = 0;
	int ch_limit = 0;
	cJSON* sub;
	*rec_ch = *full_ch = *quad_ch = *app_ch = -1;
	cJSON_ArrayForEach(sub, ch_dat)
	{
		cJSON *height = cJSON_GetObjectItemCaseSensitive(sub, "RES_H");
		if(cJSON_IsNumber(height))
		{
			if(height->valuedouble > 1152) //2K=2048*1152
			{
				ch_limit = 1;
			}
			else if(height->valuedouble == 1080 || height->valuedouble == 1152)
			{
				*rec_ch = *full_ch = ch_id;
			}
			else if(height->valuedouble == 720 || height->valuedouble == 960)
			{
				*quad_ch = *full_ch = ch_id;
			}
			else if(height->valuedouble == 480 || height->valuedouble == 576)
			{
				*quad_ch = *app_ch = ch_id;
			}
			ch_id ++;
		}
	}
	if(*rec_ch == -1)
	{
		*rec_ch = (ch_limit==1)? (ch_id - 1) : 0;
	}
	if(*full_ch == -1)
	{
		*full_ch = (ch_limit==1)? (ch_id - 1) : 0;
	}
	if(*quad_ch == -1)
	{
		*quad_ch = ch_id - 1;
	}
	if(*app_ch == -1)
	{
		*app_ch = ch_id - 1;
	}
}

int IpcAutoConfig(char* username, char* password, cJSON* result)
{
	int i,j;
	int ret;
	int rec_ch,full_ch,quad_ch,app_ch;
	onvif_login_info_t info;
	ipc_device_list_t  data;
	cJSON *device, *ch_dat, *ch;
	int cnt = 0;
	onvif_device_discovery(1000, &data);//search 1s 
	usleep(100000);
	for (i = 0; i < data.all; i++)
	{
		if(CheckIpcExist(data.dat[i].url) == 0)
		{
			continue; //已保存在表中的设备予以排除
		}
		memset(&info, 0, sizeof(onvif_login_info_t));
		ret = one_ipc_Login(data.dat[i].url, username, password, &info);
		if(ret != 0)
		{
			continue; //login错误
		}
		cnt++;
		device = cJSON_CreateObject();
		cJSON_AddStringToObject(device, "IP_ADDR", data.dat[i].url);
		cJSON_AddStringToObject(device, "IPC_NAME", data.dat[i].name);
		cJSON_AddStringToObject(device, "USR_NAME", username);
		cJSON_AddStringToObject(device, "USR_PWD", password);
		cJSON_AddItemToObject(device, "CH_DAT", ch = cJSON_CreateArray()); 
		for (j = 0; j < info.ch_cnt; j++)
		{
			info.dat[j].vd_type = Get_ffmpegshow_videoType(info.dat[j].rtsp_url, &info.dat[j].width, &info.dat[j].height);
			ch_dat = cJSON_CreateObject();
			cJSON_AddStringToObject(ch_dat, "RTSP_URL", info.dat[j].rtsp_url);
			cJSON_AddNumberToObject(ch_dat, "RES_W", info.dat[j].width);
			cJSON_AddNumberToObject(ch_dat, "RES_H", info.dat[j].height);
			cJSON_AddNumberToObject(ch_dat, "BPS", info.dat[j].bitrate);
			cJSON_AddNumberToObject(ch_dat, "FPS", info.dat[j].framerate);
			cJSON_AddNumberToObject(ch_dat, "CODEC", info.dat[j].vd_type);
			cJSON_AddItemToArray(ch, ch_dat);
		}
		SetIpcChSelect(&rec_ch, &full_ch, &quad_ch, &app_ch, ch);
		cJSON_AddNumberToObject(device, "CH_REC", rec_ch);
		cJSON_AddNumberToObject(device, "CH_FULL", full_ch);
		cJSON_AddNumberToObject(device, "CH_QUAD", quad_ch);
		cJSON_AddNumberToObject(device, "CH_APP", app_ch);
		cJSON_AddItemToArray(result, device);
	}

	if(data.all == 0 || cnt == 0)
	{
		return 2; //一个也没找到
	}
	else if(cnt >= 1)
	{
		IpcAutoToTb(result);
		//IpcAutoToBuPara();
		if(cnt < data.all)
		{
			return 3; //有添加失败
		}
		else
		{
			return 4; //找到且全部添加成功
		}
	}
}

int IpcAutoConfig2(cJSON* io_para)
{
	int i,j;
	int ret;
	int rec_ch,full_ch,quad_ch,app_ch;
	onvif_login_info_t info;
	cJSON *ipcsearch = NULL;
	cJSON *ipcsearch2 = NULL;
	cJSON *ipc_item;
	cJSON *device, *ch_dat, *ch;
	cJSON *result = NULL;
	char IpcUser[40];
	char IpcPwd[40];
	char ip_addr[40];
	char IpcName[40];
	int cnt = 0;
	long long cur_time;
	cur_time = time_since_last_call(-1);
	log_d("IpcAutoConfig start!!!\n");
	ipcsearch = cJSON_CreateArray();
	int ipcall = IpcDeviceDiscovery(1000, "LAN", ipcsearch);
	usleep(1000*1000);
	ipcsearch2 = cJSON_CreateArray();
	ipcall += IpcDeviceDiscovery(1000, "WLAN", ipcsearch2);
	cJSON_ArrayForEach(ipc_item, ipcsearch2)
	{
		API_TB_Add(ipcsearch, cJSON_Duplicate(ipc_item, 1));
	}
	result = cJSON_CreateArray();
	cJSON_ArrayForEach(ipc_item, ipcsearch)
	{
		GetJsonDataPro(ipc_item, "IP_ADDR", ip_addr);
		GetJsonDataPro(ipc_item, "IPC_NAME", IpcName);
		if(CheckIpcExist(ip_addr) == 0)
		{
			log_d("IpcAutoConfig CheckIpc[%s] Exist!!!\n", ip_addr);
			continue; //已保存在表中的设备予以排除
		}
		memset(&info, 0, sizeof(onvif_login_info_t));
		GetJsonDataPro(cJSON_GetArrayItem(io_para, 0), "IPC_USR_NAME", IpcUser);
        GetJsonDataPro(cJSON_GetArrayItem(io_para, 0), "IPC_USR_PWD", IpcPwd);	
		ret = one_ipc_Login(ip_addr, IpcUser, IpcPwd, &info);
		if(ret != 0)
		{
			GetJsonDataPro(cJSON_GetArrayItem(io_para, 1), "IPC_USR_NAME", IpcUser);
			GetJsonDataPro(cJSON_GetArrayItem(io_para, 1), "IPC_USR_PWD", IpcPwd);	
			ret = one_ipc_Login(ip_addr, IpcUser, IpcPwd, &info);
			if(ret != 0)
			{
				log_d("IpcAutoConfig Login[%s] error!!!\n", ip_addr);
				continue; //login错误
			}
		}
		cnt++;
		device = cJSON_CreateObject();
		cJSON_AddStringToObject(device, "IP_ADDR", ip_addr);
		cJSON_AddStringToObject(device, "IPC_NAME", IpcName);
		cJSON_AddStringToObject(device, "USR_NAME", IpcUser);
		cJSON_AddStringToObject(device, "USR_PWD", IpcPwd);
		cJSON_AddItemToObject(device, "CH_DAT", ch = cJSON_CreateArray()); 
		for (j = 0; j < info.ch_cnt; j++)
		{
			info.dat[j].vd_type = Get_ffmpegshow_videoType(info.dat[j].rtsp_url, &info.dat[j].width, &info.dat[j].height);
			ch_dat = cJSON_CreateObject();
			cJSON_AddStringToObject(ch_dat, "RTSP_URL", info.dat[j].rtsp_url);
			cJSON_AddNumberToObject(ch_dat, "RES_W", info.dat[j].width);
			cJSON_AddNumberToObject(ch_dat, "RES_H", info.dat[j].height);
			cJSON_AddNumberToObject(ch_dat, "BPS", info.dat[j].bitrate);
			cJSON_AddNumberToObject(ch_dat, "FPS", info.dat[j].framerate);
			cJSON_AddNumberToObject(ch_dat, "CODEC", info.dat[j].vd_type);
			cJSON_AddItemToArray(ch, ch_dat);
		}
		SetIpcChSelect(&rec_ch, &full_ch, &quad_ch, &app_ch, ch);
		cJSON_AddNumberToObject(device, "CH_REC", rec_ch);
		cJSON_AddNumberToObject(device, "CH_FULL", full_ch);
		cJSON_AddNumberToObject(device, "CH_QUAD", quad_ch);
		cJSON_AddNumberToObject(device, "CH_APP", app_ch);
		cJSON_AddItemToArray(result, device);
	}
	IpcTbCheckOnline();
	if(GetIpcNum())
	{
		if(cnt)
		{
			ret = 2;
		}
		else
		{
			ret = 3;
		}
	}
	else
	{
		if(cnt)
		{
			ret = 4;
		}
		else
		{
			ret = 5;
		}
	}
	if(cnt >= 1)
	{
		IpcAutoToTb(result);
	}


	log_d("IpcAutoConfig result: search cnt=%d save cnt=%d time=%d\n", ipcall, cnt, time_since_last_call(cur_time)/1000);
	cJSON_Delete(result);
	cJSON_Delete(ipcsearch);
	cJSON_Delete(ipcsearch2);
	return ret;	
}

int IpcDeviceDiscovery(int timeout, char* network, cJSON* result)
{	
	ipc_device_list_t  data;
	cJSON *ipc_device = NULL;
	int i;
	int ret = -1;
	if(network == NULL)
	{
		ret = 0;
	}
	else if(!strcmp(network, "LAN"))
	{
		ret = Api_NetRoute_EnterForce("LAN");
	}
	else if(!strcmp(network, "WLAN"))
	{
		ret = Api_NetRoute_EnterForce("WLAN");
	}
	else if(!strcmp(network, "4G"))
	{
		ret = Api_NetRoute_EnterForce("4G");
	}
	
	usleep(500*1000);
	if(ret == -1)
	{
		log_e("IpcDiscovery in net %s error!\n", network);
		return 0;
	}
	onvif_device_discovery(timeout, &data);
	for (i = 0; i < data.all; i++)
	{
		ipc_device = cJSON_CreateObject();
		cJSON_AddStringToObject(ipc_device, "IP_ADDR", data.dat[i].url);
		cJSON_AddStringToObject(ipc_device, "IPC_NAME", data.dat[i].name);
		cJSON_AddItemToArray(result, ipc_device);
	}
	if(network != NULL)
	{
		Api_NetRoute_ExitForce();
	}
	return data.all;
}

int IpcCheckOnline(const char* device_url)
{
	char name[100] = {0};
	return GetNameByIp(device_url, name);
}

int GetIpcNameByIp(const char* device_url, char* dev_name)
{

}

int IpcDeviceLogin(cJSON* jsonIn, cJSON* result)
{
	onvif_login_info_t info;
	char deviceUrl[100] = {0};
	char username[50] = {0};
	char password[50] = {0};
	cJSON *ch_dat = NULL;
	int i;

	GetJsonDataPro(jsonIn, "IP_ADDR", deviceUrl);
	GetJsonDataPro(jsonIn, "USR_NAME", username);
	GetJsonDataPro(jsonIn, "USR_PWD", password);

	memset(&info, 0, sizeof(onvif_login_info_t));
	int ret = one_ipc_Login(deviceUrl, username, password, &info);
	for (i = 0; i < info.ch_cnt; i++)
	{
		info.dat[i].vd_type = Get_ffmpegshow_videoType(info.dat[i].rtsp_url, &info.dat[i].width, &info.dat[i].height);
		ch_dat = cJSON_CreateObject();
		cJSON_AddStringToObject(ch_dat, "RTSP_URL", info.dat[i].rtsp_url);
		cJSON_AddNumberToObject(ch_dat, "RES_W", info.dat[i].width);
		cJSON_AddNumberToObject(ch_dat, "RES_H", info.dat[i].height);
		cJSON_AddNumberToObject(ch_dat, "BPS", info.dat[i].bitrate);
		cJSON_AddNumberToObject(ch_dat, "FPS", info.dat[i].framerate);
		cJSON_AddNumberToObject(ch_dat, "CODEC", info.dat[i].vd_type);
		cJSON_AddItemToArray(result, ch_dat);
	}
	return ret;
}


cJSON* CreateIpcSetupObject(IPC_ONE_DEVICE* record)
{
	cJSON *device = NULL;
	cJSON *ch = NULL;
	cJSON *ch_dat = NULL;
	int j;

    device = cJSON_CreateObject();
		
	cJSON_AddStringToObject(device, "NETWORK", (record->network == 0) ? "eth0" : "wlan0");
	cJSON_AddStringToObject(device, "IP_ADDR", record->IP);
	cJSON_AddStringToObject(device, "IPC_NAME", record->ID);
	cJSON_AddStringToObject(device, "NICK_NAME", record->NAME);
	cJSON_AddStringToObject(device, "USR_NAME", record->USER);
	cJSON_AddStringToObject(device, "USR_PWD", record->PWD);
	cJSON_AddNumberToObject(device, "CH_NUM", record->CH_ALL);
	cJSON_AddNumberToObject(device, "CH_REC", record->CH_REC);
	cJSON_AddNumberToObject(device, "CH_FULL", record->CH_FULL);
	cJSON_AddNumberToObject(device, "CH_QUAD", record->CH_QUAD);
	cJSON_AddNumberToObject(device, "CH_APP", record->CH_APP);
	
	cJSON_AddItemToObject(device, "IPC_CH", ch = cJSON_CreateArray()); 
	for (j = 0; j < record->CH_ALL; j++)
	{
		ch_dat = cJSON_CreateObject();
		cJSON_AddStringToObject(ch_dat, "RTSP_URL", record->CH_DAT[j].rtsp_url);
		cJSON_AddNumberToObject(ch_dat, "RES_W", record->CH_DAT[j].width);
		cJSON_AddNumberToObject(ch_dat, "RES_H", record->CH_DAT[j].height);
		cJSON_AddNumberToObject(ch_dat, "BPS", record->CH_DAT[j].bitrate);
		cJSON_AddNumberToObject(ch_dat, "FPS", record->CH_DAT[j].framerate);
		cJSON_AddNumberToObject(ch_dat, "CODEC", record->CH_DAT[j].vd_type);
		cJSON_AddItemToArray(ch, ch_dat);
	}

	return device;
}
void ParseIpcSetupObject(cJSON* view, IPC_ONE_DEVICE* record)
{
	cJSON *pSub = NULL;
	cJSON *ch_dat = NULL;
	cJSON *pSub2 = NULL;
	char name[10];
	int j;
	pSub = cJSON_GetArrayItem(view, 0);
	if(pSub != NULL)
	{
		GetJsonDataPro(pSub, "NETWORK", name);
		record->network = strcmp(name, "eth0") == 0? 0 : 1;
		GetJsonDataPro(pSub, "IP_ADDR", record->IP);
		GetJsonDataPro(pSub, "IPC_NAME", record->ID);
		GetJsonDataPro(pSub, "NICK_NAME", record->NAME);
		GetJsonDataPro(pSub, "USR_NAME", record->USER);
		GetJsonDataPro(pSub, "USR_PWD", record->PWD);
		GetJsonDataPro(pSub, "CH_NUM", &record->CH_ALL);
		GetJsonDataPro(pSub, "CH_REC", &record->CH_REC);
		GetJsonDataPro(pSub, "CH_FULL", &record->CH_FULL);
		GetJsonDataPro(pSub, "CH_QUAD", &record->CH_QUAD);
		GetJsonDataPro(pSub, "CH_APP", &record->CH_APP);
		ch_dat = cJSON_GetObjectItem( pSub, "IPC_CH");
		for(j=0; j<record->CH_ALL; j++)
		{
			pSub2 = cJSON_GetArrayItem(ch_dat, j);
			if(NULL == pSub2 ){ continue ; }
			GetJsonDataPro(pSub2, "RTSP_URL", record->CH_DAT[j].rtsp_url);
			GetJsonDataPro(pSub2, "RES_W", &record->CH_DAT[j].width);
			GetJsonDataPro(pSub2, "RES_H", &record->CH_DAT[j].height);
			GetJsonDataPro(pSub2, "BPS", &record->CH_DAT[j].bitrate);
			GetJsonDataPro(pSub2, "FPS", &record->CH_DAT[j].framerate);
			GetJsonDataPro(pSub2, "CODEC", &record->CH_DAT[j].vd_type);
		}		
	}
}

int GetIpcNum(void)
{
	cJSON* table = GetJsonFromFile(IPC_SETUP_FILE_NAME);
	int num = API_TB_Count(table, NULL);
	printf("GetIpcNum = %d\n", num);
	cJSON_Delete(table);
	return num;
}

int AddOrModifyOneIpc(IPC_ONE_DEVICE* record)
{
	cJSON* ret;
	cJSON* table = GetJsonFromFile(IPC_SETUP_FILE_NAME);
	cJSON* tempData = CreateIpcSetupObject(record);
	int count = 0;
	if(table == NULL)
	{
		ret = API_TB_Create(table, tempData);
		
		SetJsonToFile(IPC_SETUP_FILE_NAME, ret);
		cJSON_Delete(ret);
	}
	else
	{
		count = API_TB_Add(table, tempData);
		ShellCmdPrintf("API_TB_Add %d\n", count);
		if(count)
		{
			SetJsonToFile(IPC_SETUP_FILE_NAME, table);
		}
		cJSON_Delete(table);
	}
	cJSON_Delete(tempData);
}

int GetIpcRecord(char* network, cJSON* view)
{
	cJSON* table = GetJsonFromFile(IPC_SETUP_FILE_NAME);

	if(API_TB_SelectBySort(table, view, NULL, 0) > 0)
	{
		//API_TB_DropTemp(view);
		#if 0
		char* viewStr = cJSON_Print(view);
		printf("%s\n", viewStr);
		if(viewStr)
		{
			free(viewStr);
		}
		#endif
	}
	cJSON_Delete(table);

}

int GetIpcChdat(char* ip, cJSON* info)
{
	cJSON* table = GetJsonFromFile(IPC_SETUP_FILE_NAME);
	cJSON* where = cJSON_CreateObject();
	cJSON_AddStringToObject(where, "IP_ADDR", ip);

	if(API_TB_SelectBySort(table, info, where, 0) > 0)
	{
		//API_TB_DropTemp(info);
		#if 0
		char* viewStr = cJSON_Print(info);
		printf("GetIpcChdat = %s\n", viewStr);
		if(viewStr)
		{
			free(viewStr);
		}
		#endif	
	}
	cJSON_Delete(table);
	cJSON_Delete(where);
}

int GetOneIpcByIpOrName(char* ip, char* name, IPC_ONE_DEVICE* ipcRecord)
{
	cJSON* table = GetJsonFromFile(IPC_SETUP_FILE_NAME);
	cJSON* view = cJSON_CreateArray();
	cJSON* where = cJSON_CreateObject();
	if(ip)
	{
		cJSON_AddStringToObject(where, "IP_ADDR", ip);
	}
	if(name)
	{
		cJSON_AddStringToObject(where, "NICK_NAME", name);
	}	
	if(API_TB_SelectBySort(table, view, where, 0) > 0)
	{
		API_TB_DropTemp(view);
		ParseIpcSetupObject(view, ipcRecord);
		
	}
	cJSON_Delete(table);
	cJSON_Delete(view);
	cJSON_Delete(where);
}

int DeleteOneIpcByIpOrName(char* ip, char* name)
{
	cJSON* table = GetJsonFromFile(IPC_SETUP_FILE_NAME);
	int count = 0;
	cJSON* where = cJSON_CreateObject();
	if(ip)
	{
		cJSON_AddStringToObject(where, "IP_ADDR", ip);
	}
	if(name)
	{
		cJSON_AddStringToObject(where, "NICK_NAME", name);
	}	
	count = API_TB_Delete(table, where);
	ShellCmdPrintf("API_TB_Delete %d\n", count);
	if(count)
	{
		SetJsonToFile(IPC_SETUP_FILE_NAME, table);
	}
	cJSON_Delete(table);
	cJSON_Delete(where);
}

cJSON *IpcDeviceTb=NULL;
int ipc_tb_load(void)
{
	IpcDeviceTb=API_TB_LoadFromFile(IPC_SETUP_FILE_NAME,NULL,NULL,NULL,NULL,NULL);
	#if 1
	if(IpcDeviceTb==NULL)
	{
		cJSON *data=cJSON_CreateArray();
		IpcDeviceTb=API_TB_CreateNew(data,NULL,NULL,NULL,NULL,NULL);
		
		cJSON_Delete(data);
		if(IpcDeviceTb==NULL)
			return -1;
	}
	#endif
	
	return 0;
}

void ipc_tb_save(void)
{
	if(IpcDeviceTb!=NULL)
	{
		SetJsonToFile(IPC_SETUP_FILE_NAME,IpcDeviceTb);
	}
}


int AddOneIpcToTb(cJSON* record)
{
	cJSON* ret;
	cJSON* where;
	char ip[50] = {0};
	IpcDeviceTb = GetJsonFromFile(IPC_SETUP_FILE_NAME);
	if(IpcDeviceTb == NULL)
	{
		//printf("IpcDeviceTb == NULL\n");
		ret = API_TB_Create(IpcDeviceTb, record);
		
		SetJsonToFile(IPC_SETUP_FILE_NAME, ret);
		cJSON_Delete(ret);
	}
	else
	{
		where = cJSON_CreateObject();
		GetJsonDataPro(record, "IP_ADDR", ip);
		cJSON_AddStringToObject(where, "IP_ADDR", ip);
		if(API_TB_Update(IpcDeviceTb, where, record))
		{
			//printf("ModifyOneIpcToTb !!!\n");

		}
		else
		{
			//printf("AddOneIpcToTb !!!\n");
			API_TB_Add(IpcDeviceTb, record);
		}
		cJSON_Delete(where);
		ipc_tb_save();
	}
}

int CheckIpcExist(char* ip)
{
	cJSON* table = GetJsonFromFile(IPC_SETUP_FILE_NAME);
	int ret = -1;
	cJSON* where = cJSON_CreateObject();
	if(ip)
	{
		cJSON_AddStringToObject(where, "IP_ADDR", ip);
	}
	
	cJSON* view = cJSON_CreateArray();
	if(API_TB_SelectBySort(table, view, where, 0) > 0)
	{
		//printf("CheckIpcExist !!!\n");
		ret = 0;
	}
	cJSON_Delete(table);
	cJSON_Delete(view);
	cJSON_Delete(where);
	return ret;
}

void IpcTbCheckOnline(void)
{
	int i;
	cJSON *ipc_item;
	cJSON* where;
	char ip_addr[50];
	if(IpcDeviceTb!=NULL)
		cJSON_Delete(IpcDeviceTb);
	IpcDeviceTb = GetJsonFromFile(IPC_SETUP_FILE_NAME);
	for(i = cJSON_GetArraySize(IpcDeviceTb); i > 0; i--)
	{
		ipc_item = cJSON_GetArrayItem(IpcDeviceTb, i - 1);
		if(GetJsonDataPro(ipc_item, "IP_ADDR", ip_addr)==0)
			continue;
		if(IpcCheckOnline(ip_addr) != 0)
		{
			log_d("IpcTbCheckOnline delete ip[%s] !\n", ip_addr);
			where = cJSON_CreateObject();
			cJSON_AddStringToObject(where, "IP_ADDR", ip_addr);
			API_TB_Delete(IpcDeviceTb, where);
			cJSON_Delete(where);
		}	
	}
	ipc_tb_save();		
}

int IpcAutoToTb(cJSON* record)
{
	int cnt;
	cJSON* sub;
	if(IpcDeviceTb!=NULL)
		cJSON_Delete(IpcDeviceTb);
	IpcDeviceTb = GetJsonFromFile(IPC_SETUP_FILE_NAME);
	if(IpcDeviceTb == NULL)
	{
		//log_d("IpcAutoToTb create!!!\n");
		SetJsonToFile(IPC_SETUP_FILE_NAME, record);
	}
	else
	{
		cJSON_ArrayForEach(sub, record)
		{
			cnt = API_TB_Add(IpcDeviceTb, cJSON_Duplicate(sub, 1));
			//log_d("IpcAutoToTb add cnt=%d!!!\n", cnt);
		}
		
		ipc_tb_save();
	}
		
}
void IpcAutoToBuPara(void)
{	
	int ipc_num;
	cJSON *ipc_item;
	cJSON *quard_id_array;
	cJSON *app_id_array;
	cJSON *io_obj;
	char pip_bind_id[80]={0};
	int i;
	char ip_addr[80];
	cJSON *IPC_TB;
	
	IPC_TB = GetJsonFromFile(IPC_SETUP_FILE_NAME);
	//if(IPC_TB==NULL)
	{
		if(IPC_TB==NULL)
			ipc_num=0;
		else
			ipc_num=cJSON_GetArraySize(IPC_TB);
		io_obj=cJSON_CreateObject();
		quard_id_array=cJSON_AddArrayToObject(io_obj,IPC_Quad_List);
		app_id_array=cJSON_AddArrayToObject(io_obj,IPC_APP_List);
		if(ipc_num==0)
		{
			strcpy(pip_bind_id,"");
		}
		else
		{
			for(i=0;i<ipc_num;i++)
			{
				ipc_item=cJSON_GetArrayItem(IPC_TB,i);
				if(GetJsonDataPro(ipc_item, "IP_ADDR", ip_addr)==0)
					continue;
				if(i==0)
				{
					strcpy(pip_bind_id,ip_addr);
				}
				if(i<=2)
				{
					cJSON_AddItemToArray(quard_id_array,cJSON_CreateString(ip_addr));
				}
				cJSON_AddItemToArray(app_id_array,cJSON_CreateString(ip_addr));

			}
		}
	}
	API_Para_Write_String(IPC_PIP_BIND, pip_bind_id);
	API_Para_Write_Public(io_obj);
	
	cJSON_Delete(io_obj);
	cJSON_Delete(IPC_TB);
	Reload_IpcDeviceVtkTb();
}
cJSON *IpcDeviceVtkTb=NULL;
cJSON *Get_IPCChInfo_ByIP(const char *ip_addr,const char *ch_name)
{
	#if 0
	if(IpcDeviceVtkTb==NULL)
		IpcDeviceVtkTb=API_TB_LoadFromFile(IPC_SETUP_FILE_NAME,NULL,NULL,NULL,NULL,NULL);
	if(IpcDeviceVtkTb==NULL)
		return NULL;
	#endif
	cJSON* where = cJSON_CreateObject();
	cJSON* ipcs;
	cJSON* ipc_item;
	cJSON *ch_array;
	cJSON *info=NULL;
	int ch;
	cJSON_AddStringToObject(where, "IP_ADDR", ip_addr);
	ipcs=cJSON_CreateArray();
	if(API_TB_SelectBySortByName(TB_NAME_IPC_DEV_TB, ipcs, where, 0) > 0)
	{
		ipc_item=cJSON_GetArrayItem(ipcs,0);
		if(GetJsonDataPro(ipc_item,ch_name,&ch))
		{
			if(GetJsonDataPro(ipc_item,"CH_DAT",&ch_array))
			{
				if(cJSON_GetArraySize(ch_array)>ch)
					info=cJSON_Duplicate(cJSON_GetArrayItem(ch_array,ch),1);
			}
		}
		
	}
	
	cJSON_Delete(ipcs);
	cJSON_Delete(where);
	return info;
}
int Get_IPCChName_ByIP(const char *ip_addr,char *name)
{
	if(IpcDeviceVtkTb==NULL)
		IpcDeviceVtkTb=API_TB_LoadFromFile(IPC_SETUP_FILE_NAME,NULL,NULL,NULL,NULL,NULL);
	if(IpcDeviceVtkTb==NULL)
		return NULL;
	cJSON* where = cJSON_CreateObject();
	cJSON* ipcs;
	cJSON* ipc_item;
	//cJSON *ch_array;
	
	int ret=-1;
	cJSON_AddStringToObject(where, "IP_ADDR", ip_addr);
	ipcs=cJSON_CreateArray();
	if(API_TB_SelectBySort(IpcDeviceVtkTb, ipcs, where, 0) > 0)
	{
		ipc_item=cJSON_GetArrayItem(ipcs,0);
		
		if(GetJsonDataPro(ipc_item, "IPC_NAME", name))
			ret=0;
	}
	
	cJSON_Delete(ipcs);
	cJSON_Delete(where);
	return ret;
}
int Get_IPCChType_ByIP(const char *ip_addr,const char *ch_name)
{
	if(IpcDeviceVtkTb==NULL)
		IpcDeviceVtkTb=API_TB_LoadFromFile(IPC_SETUP_FILE_NAME,NULL,NULL,NULL,NULL,NULL);
	if(IpcDeviceVtkTb==NULL)
		return NULL;
	cJSON* where = cJSON_CreateObject();
	cJSON* ipcs;
	cJSON* ipc_item;
	cJSON *ch_array;
	//cJSON *info=NULL;
	int type;
	int ch;
	cJSON_AddStringToObject(where, "IP_ADDR", ip_addr);
	ipcs=cJSON_CreateArray();
	if(API_TB_SelectBySort(IpcDeviceVtkTb, ipcs, where, 0) > 0)
	{
		ipc_item=cJSON_GetArrayItem(ipcs,0);
		if(GetJsonDataPro(ipc_item,ch_name,&ch))
		{
			if(GetJsonDataPro(ipc_item,"CH_DAT",&ch_array))
			{
				if(cJSON_GetArraySize(ch_array)>ch)
					GetJsonDataPro(cJSON_GetArrayItem(ch_array,ch),"CODEC",&type);
			
			}
		}
		
	}
	
	cJSON_Delete(ipcs);
	cJSON_Delete(where);
	return type;
}
void Reload_IpcDeviceVtkTb(void)
{
	if(IpcDeviceVtkTb!=NULL)
		cJSON_Delete(IpcDeviceVtkTb);
	IpcDeviceVtkTb=API_TB_LoadFromFile(IPC_SETUP_FILE_NAME,NULL,NULL,NULL,NULL,NULL);
}