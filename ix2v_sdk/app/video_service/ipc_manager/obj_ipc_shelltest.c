#include <stdio.h>
#include <sys/sysinfo.h>
#include "task_Shell.h"
#include "obj_ipc_manager.h"


int ShellCmd_IpcManage(cJSON *cmd)
{
	int timeout = 0;
	int ins = 0;
	char* jsonstr;
	char* net;
	cJSON *target, *win;
	cJSON *search;
	cJSON *login;
	char* ctrlStr = GetShellCmdInputString(cmd, 1);
	if(ctrlStr == NULL)
	{
		ShellCmdPrintf("Input error!\n");
	}
	else if(!strcmp(ctrlStr, "search"))
	{
		net = GetShellCmdInputString(cmd, 2);
		jsonstr = GetShellCmdInputString(cmd, 3);
		if( jsonstr != NULL )
		{
			timeout = atoi(jsonstr);
		}
		ShellCmdPrintf("Ipc search start timeout[%d]\n", timeout);
		search = cJSON_CreateArray();
		IpcDeviceDiscovery(timeout, net, search);
		char* viewStr = cJSON_Print(search);
		ShellCmdPrintf("Ipc search result:%s\n", viewStr);
		if(viewStr)
		{
			free(viewStr);
		}
		cJSON_Delete(search);
	}
	else if(!strcmp(ctrlStr, "login"))
	{
		ShellCmdPrintf("Ipc login start!\n");
		jsonstr = GetShellCmdInputString(cmd, 2);
		if( jsonstr != NULL )
		{
			cJSON *cmd_json = cJSON_Parse(jsonstr);
			if( cmd_json != NULL )
			{
				login = cJSON_CreateArray();
				IpcDeviceLogin(cmd_json, login);
				char* viewStr = cJSON_Print(login);
				ShellCmdPrintf("Ipc login result:%s\n", viewStr);
				if(viewStr)
				{
					free(viewStr);
				}
				cJSON_Delete(login);
				cJSON_Delete(cmd_json);
			}
			else
			{
				ShellCmdPrintf("Input error!\n");
			}
		}
		else
		{
			ShellCmdPrintf("Input error!\n");
		}
	}
	else if(!strcmp(ctrlStr, "show"))
	{
		ShellCmdPrintf("Ipc show start!\n");
		jsonstr = GetShellCmdInputString(cmd, 2);
		if( jsonstr != NULL )
		{
			ins = (*jsonstr) - '0';	
			if(ins>3)
			{
				ShellCmdPrintf("ins must be <=3\n");
				return 1;
			}	
			jsonstr = GetShellCmdInputString(cmd, 3);
			if( jsonstr != NULL )
			{
				target = cJSON_Parse(jsonstr);
				if( target != NULL )
				{
					jsonstr = GetShellCmdInputString(cmd, 4);
					if( jsonstr != NULL )
					{
						win = cJSON_Parse(jsonstr);
					}
					else
					{
						win = cJSON_CreateObject();
					}
					#if	defined(PID_IX850)
					ipc_preview_on();
					#endif
					Start_OneIpc_Show(ins, target, win, 0);
					cJSON_Delete(win);
					cJSON_Delete(target);
				}
				else
				{
					ShellCmdPrintf("Input error!\n");
				}
			}
			else
			{
				ShellCmdPrintf("Input error!\n");
			}	
		}
		else
		{
			ShellCmdPrintf("Input error!\n");
		}
		
	}
	else if(!strcmp(ctrlStr, "close"))
	{
		ShellCmdPrintf("Ipc close!\n");
		jsonstr = GetShellCmdInputString(cmd, 2);
		if( jsonstr != NULL )
		{
			ins = (*jsonstr) - '0';			
		}
		if(ins>3)
		{
			ShellCmdPrintf("ins must be <=3\n");
			return 1;
		}	
		Stop_OneIpc_Show(ins);
	}
	else if(!strcmp(ctrlStr, "auto"))
	{
		IpcAutoConfig2(API_Para_Read_Public("IPC_USR_PARA"));
		IpcAutoToBuPara();
	}
	else if(!strcmp(ctrlStr, "show_file"))
	{
		jsonstr = GetShellCmdInputString(cmd, 2);
		api_start_ffmpegPlay(jsonstr,NULL);
	}
	else if(!strcmp(ctrlStr, "show_file_stop"))
	{
		api_stop_ffmpegPlay();
	}
	return 0;
}

