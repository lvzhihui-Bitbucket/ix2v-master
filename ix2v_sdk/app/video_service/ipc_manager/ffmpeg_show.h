
#ifndef _FFMPEG_SHOW_H_
#define _FFMPEG_SHOW_H_

#include "libavutil/common.h"
#include "libavformat/avformat.h"
#include "libavcodec/avcodec.h"
#include "one_tiny_task.h"

#define	FFMPEG_3_4_2_VERSION

typedef  int (*cb_ffmpeg_proc)(unsigned char* buf, int len);

typedef struct
{
	pthread_mutex_t 	ffmpegStateLock;
	int					state;		// 0:idle, 1:running
	AVFormatContext 	*ic;
	AVFormatContext 	*out_ic;
	AVInputFormat 		params;
		
	char				rtsp_url[240];
	int					width;
	int					height;
	int					fps;
	int					baudrate;
	int 				videostream;	
	int					videotype;		// 0:h264, 1:h265, 2:mpeg4
	int					videoCH;		// ͨ����
	int					showFlag;		// show
	int					recFlag;		// ¼��
	int					rtpFlag;		// rtp_trans
	cb_ffmpeg_proc		dat_process;
	one_tiny_task_t 	task_process;
	int 			rtp_send_list_ch;
	int 				debug;
	int				data_len;
	char *			data_buff;
	int				data_buff_len;
} ffmpegRun_t;


int api_start_ffmpegshow(int ffmpe_num, unsigned char* rtsp_url, int vdtype, cb_ffmpeg_proc proc );
int api_stop_ffmpegshow(int ffmpe_num);
int Get_ffmpegshow_videoType( char* rtsp_url, int* w, int* h);
#endif



