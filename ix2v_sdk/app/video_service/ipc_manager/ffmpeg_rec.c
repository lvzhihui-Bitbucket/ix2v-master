

#include "ffmpeg_rec.h"

extern int	bkgd_w;
extern int 	bkgd_h;

int task_ffmpeg_rec_init( void* arg )
{
	one_tiny_task_t *ptiny_task = (one_tiny_task_t*)arg;
	ffmpeg_rec_dat_t *pusr_dat = (ffmpeg_rec_dat_t*)ptiny_task->pusr_dat;	
	AVOutputFormat *ofmt = NULL;
	int ret = 0;
	int i;
	/* initialize libavcodec, and register all codecs and formats */
	if( !GetRegisterState() )
	{
		SetRegisterState(1);
		printf("\n=================begin to av_register_all...========\n");
		av_register_all();
		avcodec_register_all();
		avformat_network_init();
	}
	
	printf("\n=================begin to avformat_open_input: [%s]...========\n", pusr_dat->rtsp_url );
	pusr_dat->ifmt_ctx = avformat_alloc_context();
	AVDictionary* options = NULL;
	av_dict_set(&options, "buffer_size", 	"102400", 0); 	//���û����С��1080p�ɽ�ֵ����
	av_dict_set(&options, "rtsp_transport", "tcp", 0); 		//��udp��ʽ�򿪣������tcp��ʽ�򿪽�udp�滻Ϊtcp
	av_dict_set(&options, "stimeout", 		"2000000", 0); 	//���ó�ʱ�Ͽ�����ʱ�䣬��λ΢��
	av_dict_set(&options, "max_delay", 		"500000", 0); 	//�������ʱ��
	ret = avformat_open_input(&pusr_dat->ifmt_ctx, pusr_dat->rtsp_url, NULL, &options);
	if (ret < 0) 
	{
		printf("cannot open %s ret=%d\n", pusr_dat->rtsp_url, ret);
		//avformat_free_context(pusr_dat->ifmt_ctx);
		avformat_close_input(&pusr_dat->ifmt_ctx);
		return -1;
	}

	printf("\n=================begin to av_find_stream_info...========\n");
	pusr_dat->ifmt_ctx->probesize = 10 *1024;
	pusr_dat->ifmt_ctx->max_analyze_duration = 1 * AV_TIME_BASE;	
	ret = avformat_find_stream_info(pusr_dat->ifmt_ctx,NULL ); //&options);	 // ����Ĭ����Ҫ���ѽϳ���ʱ���������ʽ̽��, ��μ���̽��ʱ���ڣ� ����ͨ������AVFotmatContext��probesize��max_analyze_duration���Խ��е���
	if (ret < 0) 
	{
		printf( "avformat_find_stream_info error!\n");
		//avformat_free_context(pusr_dat->ifmt_ctx);
		avformat_close_input(&pusr_dat->ifmt_ctx);
		return -1;
	}

	for( i = 0; i < pusr_dat->ifmt_ctx->nb_streams; i++ )
	{
		// ���Ϊ��Ƶ����ý������
		if(pusr_dat->ifmt_ctx->streams[i]->codec->codec_type == AVMEDIA_TYPE_VIDEO)
		{
			pusr_dat->videostream = i;
			pusr_dat->width = pusr_dat->ifmt_ctx->streams[pusr_dat->videostream]->codecpar->width;	   
			pusr_dat->height = pusr_dat->ifmt_ctx->streams[pusr_dat->videostream]->codecpar->height;				
			pusr_dat->videotype = pusr_dat->ifmt_ctx->streams[pusr_dat->videostream]->codecpar->codec_id == AV_CODEC_ID_H265 ? 1 : 0;
			printf("videostream=%d Videotype=%d width=%d, height=%d\n", pusr_dat->videostream, pusr_dat->videotype, pusr_dat->width, pusr_dat->height);
			if( !pusr_dat->width || !pusr_dat->height )
			{
				pusr_dat->width = 1920;
				pusr_dat->height = 1088;
				pusr_dat->ifmt_ctx->streams[pusr_dat->videostream]->codecpar->width = 1920;
				pusr_dat->ifmt_ctx->streams[pusr_dat->videostream]->codecpar->height = 1080;
			}
			pusr_dat->height 	= pusr_dat->height==1080? 1088 : pusr_dat->height;
			break;
		}
	}

	// ����������
	ret = avformat_alloc_output_context2(&pusr_dat->ofmt_ctx, NULL, NULL, pusr_dat->out_file);
	if (ret < 0) 
	{
		printf( "Could not create output context\n");
		//avformat_free_context(pusr_dat->ifmt_ctx);
		avformat_close_input(&pusr_dat->ifmt_ctx);
		return -1;
    }
	pusr_dat->state			= 1;
	ofmt = pusr_dat->ofmt_ctx->oformat;
	// ������
	AVStream *in_stream = pusr_dat->ifmt_ctx->streams[pusr_dat->videostream];
	#if 0
	AVCodec *codec = avcodec_find_decoder(in_stream->codecpar->codec_id);
	AVStream *out_stream = avformat_new_stream(pusr_dat->ofmt_ctx, codec);
	if (!out_stream) 
	{
		printf("Failed allocating output stream\n");
		goto end;
	 
	}
	AVCodecContext *pCodecCtx = avcodec_alloc_context3(codec);
	ret = avcodec_parameters_to_context(pCodecCtx, in_stream->codecpar);
	if (ret < 0) 
	{
		printf("Failed to copy context input to output stream codec context\n");
		goto end;
	}
	#if 0
	ret = avcodec_open2(pCodecCtx, codec, NULL);
    if (ret < 0) 
	{
        printf("Failed to open decoder for stream 0\n");
		goto end;
    }
	#endif
	if (ofmt->flags & AVFMT_GLOBALHEADER) 
	{
		pCodecCtx->flags |= CODEC_FLAG_GLOBAL_HEADER;
	}
	pCodecCtx->codec_tag = 0;
	ret = avcodec_parameters_from_context(out_stream->codecpar, pCodecCtx);
	if (ret < 0) 
	{
		printf("Failed to copy context input to output stream codec context\n");
		goto end;
	}
	#endif
	AVStream *out_stream = avformat_new_stream(pusr_dat->ofmt_ctx, NULL);
	AVCodecParameters *in_codecpar = in_stream->codecpar;
	avcodec_parameters_copy(out_stream->codecpar, in_codecpar);
	out_stream->codecpar->codec_tag = 0;
	
	// ��AVIOContext������
	if (!(ofmt->flags & AVFMT_NOFILE)) 
	{
		ret = avio_open(&pusr_dat->ofmt_ctx->pb, pusr_dat->out_file, AVIO_FLAG_WRITE);
		if (ret < 0) 
		{
			printf("Could not open output file %s\n", pusr_dat->out_file);
			return -1;
		}
    }
	// д��ͷ����Ϣ
    ret = avformat_write_header(pusr_dat->ofmt_ctx, NULL);
	if (ret < 0) 
	{
        printf("Error occurred when opening output file\n");
		return -1;
	}
	
	#if 0
end:
	if (ret < 0) 
	{
		avformat_close_input(&pusr_dat->ifmt_ctx);
		//avcodec_free_context(&pCodecCtx);
		/* close output */
		if (pusr_dat->ofmt_ctx && !(ofmt->flags & AVFMT_NOFILE))
			avio_closep(&pusr_dat->ofmt_ctx->pb);
		avformat_free_context(pusr_dat->ifmt_ctx);
		avformat_free_context(pusr_dat->ofmt_ctx);
		return -1;
	}
	#endif
	return 0;
}
#if 0
int calfps(int deltaTime)
{
    static int fps = 0;
    static int timeLeft = 1000000; // ȡ�̶�ʱ����Ϊ1��
    static int frameCount = 0;

    ++frameCount;
    timeLeft -= deltaTime;
    if (timeLeft < 0)
    {
        fps = frameCount;
        frameCount = 0;
        timeLeft = 1000000;
    }
	printf("av_read_frame fps=%d\n", fps);
    return fps;
}
#endif
// �߳�ѭ������
int task_ffmpeg_rec_process( void* arg )
{
    int ret;
	one_tiny_task_t *ptiny_task = (one_tiny_task_t*)arg;
	ffmpeg_rec_dat_t *pusr_dat = (ffmpeg_rec_dat_t*)ptiny_task->pusr_dat;	
    AVPacket pkt;
	AVStream *in_stream, *out_stream;
	struct timeval start, end;
	int time_use;
	int m_frame_index = 0;
	int IDR = 0;
	int64_t pts_last;
	pusr_dat->state = 2;	// run flag;

	#if 0
	#include <sched.h>
	struct sched_param sh_param;
	sh_param.sched_priority = sched_get_priority_min(SCHED_FIFO);
	sched_setscheduler(0,SCHED_FIFO,&sh_param);
	#endif
    while( one_tiny_task_is_running(ptiny_task) )
	{		
        memset(&pkt, 0, sizeof(pkt));
        gettimeofday( &start, NULL );
        ret= av_read_frame(pusr_dat->ifmt_ctx, &pkt);
		gettimeofday( &end, NULL );
		time_use = (end.tv_sec - start.tv_sec) * 1000000 + (end.tv_usec - start.tv_usec);
		if(ret<0 || time_use > 2000000)
		{
			printf("task_ffmpeg_rec_process av_read_frame ret=%d time_use=%d\n", ret, time_use);
			av_packet_unref(&pkt);
			(pusr_dat->CallBack)(pusr_dat->videoCH);
		}
        else
		{
			if( pkt.stream_index == pusr_dat->videostream )
			{
				#if 0
				if( IDR == 0 && pkt.data[0] == 0 && pkt.data[1] == 0 && pkt.data[2] == 0 && pkt.data[3] == 1 )
				{
					if( ( (pkt.data[4]) & (0x1f) ) == 7)
					{
						IDR = 1;
						printf("API_Recording_mux H264 IDR OK\n");
					}
					else if( ( (pkt.data[4]) & (0x7e) ) == 0x40)
					{
						IDR = 1;
						printf("API_Recording_mux H265 IDR OK\n");
					}
				}
				#endif
				in_stream  = pusr_dat->ifmt_ctx->streams[pkt.stream_index];
				out_stream = pusr_dat->ofmt_ctx->streams[pkt.stream_index];
				if(!pusr_dat->recOnly)
					dvr_quad_full_buf(pkt.data, pkt.size);
				//if(IDR)
				{
					/* copy packet */
					//pkt.pts = av_rescale_q_rnd(pkt.pts, in_stream->time_base, out_stream->time_base, AV_ROUND_NEAR_INF|AV_ROUND_PASS_MINMAX);
					//pkt.dts = av_rescale_q_rnd(pkt.dts, in_stream->time_base, out_stream->time_base, AV_ROUND_NEAR_INF|AV_ROUND_PASS_MINMAX);
					//pkt.duration = av_rescale_q(pkt.duration, in_stream->time_base, out_stream->time_base);
					//av_packet_rescale_ts(&pkt, in_stream->time_base, out_stream->time_base);
					//�ļ��ְ���pts����
					if(pusr_dat->fileSplit)
					{
						pusr_dat->fileSplit = 0;
						pts_last = 0;
					}
					pkt.pts = 90000/pusr_dat->fps + pts_last;
					pkt.dts = pkt.pts;
					pts_last = pkt.pts;
					ret = av_interleaved_write_frame(pusr_dat->ofmt_ctx, &pkt);
					if (ret < 0) 
					{
						printf("write_frame ERR=%d\n", ret);
						(pusr_dat->CallBack)(pusr_dat->videoCH);
						break;

					}
					if(m_frame_index < pusr_dat->fps - 1)
					{
						m_frame_index++;
					}
					else
					{
						printf("m_frame_index=%d rec_time=%d\n",m_frame_index, pusr_dat->CurSec);
						m_frame_index = 0;
						pusr_dat->CurSec++;
					}
					if(pusr_dat->CurSec >= pusr_dat->TotalSec)
					{
						printf("task_ffmpeg_rec_process CurSec=%d\n", pusr_dat->CurSec);
						(pusr_dat->CallBack)(pusr_dat->videoCH);
						break;
					}
					if((pusr_dat->CurSec%600 == 0) && (m_frame_index==0)) 
					{
						pusr_dat->fileSplit = 1;
						//pusr_dat->fileSplitCnt++;
						ret = RecFileSplit(pusr_dat->videoCH);
						if (ret < 0) 
						{
							printf("RecFileSplit ERR!!!\n");
							(pusr_dat->CallBack)(pusr_dat->videoCH);
							break;
						}
					}
				}
			}
			av_packet_unref(&pkt);
        } 
		usleep(1*1000);
    }
	av_write_trailer(pusr_dat->ofmt_ctx);
	printf("-----end task_ffmpeg_rec_process !!!\n");
    return 0;
}

// �˳����̵߳Ĵ���: �ڴ����Դ�Ļ����ͷ�
int task_ffmpeg_rec_exit( void* arg )
{
	one_tiny_task_t *ptiny_task = (one_tiny_task_t*)arg;
	ffmpeg_rec_dat_t *pusr_dat = (ffmpeg_rec_dat_t*)ptiny_task->pusr_dat;	
	printf( "--task_ffmpeg_rec_exit state=%d--\n", pusr_dat->state); 
    AVOutputFormat *ofmt = NULL;
	if(pusr_dat->state)
	{
		pusr_dat->state = 0;
		avformat_close_input(&pusr_dat->ifmt_ctx);
		ofmt = pusr_dat->ofmt_ctx->oformat;
		if (pusr_dat->ofmt_ctx && !(ofmt->flags & AVFMT_NOFILE))
			avio_closep(&pusr_dat->ofmt_ctx->pb);
		avformat_free_context(pusr_dat->ifmt_ctx);
		avformat_free_context(pusr_dat->ofmt_ctx);
		printf("-----task_ffmpeg_rec_exit !!!\n");
	}
	return 0;
}

#if 0
void log_packet(const AVFormatContext *fmt_ctx, const AVPacket *pkt, const char *tag)
{
    AVRational *time_base = &fmt_ctx->streams[pkt->stream_index]->time_base;

    printf("%s: pts:%s pts_time:%s dts:%s dts_time:%s duration:%s duration_time:%s stream_index:%d\n",
           tag,
           av_ts2str(pkt->pts), av_ts2timestr(pkt->pts, time_base),
           av_ts2str(pkt->dts), av_ts2timestr(pkt->dts, time_base),
           av_ts2str(pkt->duration), av_ts2timestr(pkt->duration, time_base),
           pkt->stream_index);
}
#endif


ffmpeg_rec_dat_t		ffmpegRec[4];
int dvr_quad_to_full(int ffmpe_num)
{
	int ret;
	ffmpegRec[ffmpe_num].recOnly = 0;
	//ret = api_h264_show_start(4, ffmpegRec[ffmpe_num].width, ffmpegRec[ffmpe_num].height, 15, 512, 0, 0, bkgd_w, bkgd_h, ffmpegRec[ffmpe_num].videotype);
	printf("dvr_swich_to_full ret = %d\n", ret);
	return ret;
}
int dvr_full_to_quad(int ffmpe_num)
{
	int ret;
	ffmpegRec[ffmpe_num].recOnly = 1;
	printf("close dvr_full_show !!!\n");
	//ret = api_h264_show_stop(4);
	printf("close dvr_full_show ret = %d\n", ret);
	return ret;
}
	
int dvr_quad_full_buf( char* pdatbuf, int datlen )
{
	//return api_h264_show_dump_buf( 4, pdatbuf, datlen );
}

int RecFileSplit(int ffmpe_num)
{
	ffmpeg_rec_dat_t* pusr_dat = &ffmpegRec[ffmpe_num];
	av_write_trailer(pusr_dat->ofmt_ctx);
	avio_closep(&pusr_dat->ofmt_ctx->pb);
	avformat_free_context(pusr_dat->ofmt_ctx);

	char recfile[50];
	time_t t;
	struct tm *tblock; 	
	t = time(NULL); 
	tblock=localtime(&t);
	sprintf( recfile,"20%02d%02d%02d_%02d%02d%02d-CH%d.mp4",tblock->tm_year-100,tblock->tm_mon+1,tblock->tm_mday,tblock->tm_hour,tblock->tm_min,tblock->tm_sec,ffmpe_num+1);
	snprintf(pusr_dat->out_file,100,"/mnt/sdcard/video/%s",recfile);
	
	printf( "--RecFileSplit CurSec=%d-\n", pusr_dat->CurSec); 
	int ret;
	ret = avformat_alloc_output_context2(&pusr_dat->ofmt_ctx, NULL, NULL, pusr_dat->out_file);
	if (ret < 0) 
	{
		printf( "Could not create output context\n");
		return -1;
    }
	AVStream *in_stream = pusr_dat->ifmt_ctx->streams[pusr_dat->videostream];
	AVStream *out_stream = avformat_new_stream(pusr_dat->ofmt_ctx, NULL);
	avcodec_parameters_copy(out_stream->codecpar, in_stream->codecpar);
	ret = avio_open(&pusr_dat->ofmt_ctx->pb, pusr_dat->out_file, AVIO_FLAG_WRITE);
	if (ret < 0) 
	{
		printf("Could not open output file %s\n", pusr_dat->out_file);
		return -1;
	}
	ret = avformat_write_header(pusr_dat->ofmt_ctx, NULL);
	if (ret < 0) 
	{
        printf("Error when avformat_write_header\n");
		return -1;
	}
	//API_add_Inform_to_VideoMenu_queue(MSG_7_BRD_SUB_DVR_SPACE_CHECK);
	return 0;
}

int RecFileFlush(int ffmpe_num)
{
	printf("RecFileFlush ch=%d\n", ffmpe_num);
	ffmpeg_rec_dat_t* pusr_dat = &ffmpegRec[ffmpe_num];
	if (pusr_dat->ofmt_ctx->pb)
       avio_flush(pusr_dat->ofmt_ctx->pb);
	//av_freep(&pusr_dat->ofmt_ctx->priv_data);
	char cmd[100];
	strcpy(cmd,"echo 3 > /proc/sys/vm/drop_caches");
	system(cmd);
	sync();
}
int GetRecFileSize(int ffmpe_num)
{
	ffmpeg_rec_dat_t* pins = &ffmpegRec[ffmpe_num];
	//avio_seek(pins->ofmt_ctx->pb, 0, SEEK_END);
	int64_t filesize = avio_tell(pins->ofmt_ctx->pb);
	avio_seek(pins->ofmt_ctx->pb, filesize, SEEK_SET);
	filesize /= (1024*1024);
	printf("GetRecFileSize %d\n", filesize);
	return filesize;
}

int GetRecCurSec(int ffmpe_num)
{
	ffmpeg_rec_dat_t* pins = &ffmpegRec[ffmpe_num];
	return pins->CurSec;
}

int api_start_ffmpeg_rec(int ffmpe_num, unsigned char* rtsp_url, unsigned char*file, int min, int fps, ffmpegRecCallBack fun )
{
	ffmpeg_rec_dat_t* pins = &ffmpegRec[ffmpe_num];

	if( pins->state )
		return -1;
	pins->videoCH = ffmpe_num;
	pins->recOnly = 1;
	pins->fps = fps;
	pins->TotalSec = min*60;
	pins->CurSec = 0;
	pins->fileSplit = 0;
	pins->CallBack = fun;
	strcpy( pins->rtsp_url, rtsp_url );
	strcpy( pins->out_file, file );
	printf( "--api_start_ffmpeg_rec fps=%d---time=%d-\n", pins->fps, pins->TotalSec); 
	if( one_tiny_task_start2( &pins->task_process, task_ffmpeg_rec_init, task_ffmpeg_rec_process, task_ffmpeg_rec_exit, (void*)pins ) == 0 )
	{
		int wait_to = 0;
		while(1)
		{
			if( pins->state == 2 )
				return 0;
			else
			{
				usleep(100*1000);
				if( ++wait_to >= 50 )	// 3s
					return -2;
 			}
 		}		
 	}
	else
		return -1;
}

int api_stop_ffmpeg_rec(int ffmpe_num)
{
	ffmpeg_rec_dat_t* pins = &ffmpegRec[ffmpe_num];
	one_tiny_task_stop2( &pins->task_process );
	
	return 0;	
}



ffmpeg_rec_dat2		previewRec =
{
	.recStateLock 	= PTHREAD_MUTEX_INITIALIZER,
	.width 			= 1280,
	.height			= 720,
	.fps			= 15,
	.videotype		= 0,
};
static void api_setRecState( RECORD_STATE state )
{
	ffmpeg_rec_dat2* pins = &previewRec;
	pthread_mutex_lock(&pins->recStateLock);
	pins->state = state;
	pthread_mutex_unlock(&pins->recStateLock);
}

static RECORD_STATE api_getRecState( void )
{
	RECORD_STATE state;
	ffmpeg_rec_dat2* pins = &previewRec;
	pthread_mutex_lock(&pins->recStateLock);
	state = pins->state;
	pthread_mutex_unlock(&pins->recStateLock);
	return state;

}

// lzh_20230919_s
#if 1
#define EXTRADATA_ACC1_LEN	128	//( 0x2B + AV_INPUT_BUFFER_PADDING_SIZE )
unsigned char* extradata_avc1;
unsigned char extradata_avc2[EXTRADATA_ACC1_LEN]=
{
	// len + avcC
	0x00, 0x00, 0x00, 0x2B, 0x61, 0x76, 0x63, 0x43, 
	// sps: ver + profile + profile compat + level + 0xFF + 0xE1 + sps_size
	0x01, 0x4D, 0x40, 0x2A, 0xFF, 0xE1, 0x00, 0x14,
	// sps: data
	0x67, 0x4D, 0x40, 0x2A, 0xF4, 0x05, 0x01, 0x7D, 0x08, 0x00, 0x00, 0x03, 0x00, 0x08, 0x00, 0x00, 0x03, 0x00, 0xF0, 0x20, 
	// pps: num + pps_size
	0x01, 0x00, 0x04, 
	// pps: data
	0x68, 0xEE, 0x12, 0x88,
};
static int sps_size;
static unsigned char sps_data[128];
static int pps_size;
static unsigned char pps_data[128];
static int sps_data_ok = 0;
static int pps_data_ok = 0;
int sps_pps_init(unsigned char* pstream, int size)
{
	#if 1
	int head_cnt = 0;
	int size_cnt = 0;
	int sps_data_en = 0;
	int pps_data_en = 0;
	int i;
	printf("sps_pps_init size=%d\n", size);
	#if 0	
		printf("sps_pps_init data:");
		for( i = 0; i < 100; i++ )
		{
			printf(" %02x ",pstream[i]);	
		}
		printf("\n\r"); 
	#endif	
	do
	{
		if( pstream[head_cnt+0] == 0 && pstream[head_cnt+1] == 0 && pstream[head_cnt+2] == 0 && pstream[head_cnt+3] == 1 )
		{
			// sps
			if( !sps_data_en && (pstream[head_cnt+4]&0x1f) == 7 )
			{
				if( pps_data_en )
				{
					pps_data_en = 0;
					pps_data_ok = 1;
					pps_size = size_cnt;
				}
				sps_data_en = 1;
				size_cnt = 0;
				head_cnt += 3;	// jump head 4 bytes
			}
			// pps
			else if( !pps_data_en && (pstream[head_cnt+4]&0x1f) == 8 )
			{
				if( sps_data_en )
				{
					sps_data_en = 0;
					sps_data_ok = 1;
					sps_size = size_cnt;
				}
				pps_data_en = 1;
				size_cnt = 0;
				head_cnt += 3;	// jump head 4 bytes
			}
			else
			{
				if( sps_data_en )
				{
					sps_data_en = 0;
					sps_data_ok = 1;
					sps_size = size_cnt;
				}
				if( pps_data_en )
				{
					pps_data_en = 0;
					pps_data_ok = 1;
					pps_size = size_cnt;
				}
				if( sps_data_ok && pps_data_ok )
				{
					#if 1	
						printf("sps_data[%d]:\n",sps_size);
						for( i = 0; i < sps_size; i++ )
						{
							printf(" %02x ",sps_data[i]);	
						}
						printf("\n\r"); 
					#endif	
					#if 1	
						printf("pps_data[%d]:\n",pps_size);
						for( i = 0; i < pps_size; i++ )
						{
							printf(" %02x ",pps_data[i]);	
						}
						printf("\n\r"); 
					#endif	

					break;
				}
			}
		}
		else
		{
			if( sps_data_en ) 
			{
				sps_data[size_cnt++] = pstream[head_cnt];
			}
			if( pps_data_en ) 
			{
				pps_data[size_cnt++] = pstream[head_cnt];
			}
		}
	} while (head_cnt++ < size);	
	if( sps_data_en )
	{
		sps_data_en = 0;
		sps_data_ok = 1;
		sps_size = size_cnt;
	}
	if( pps_data_en )
	{
		pps_data_en = 0;
		pps_data_ok = 1;
		pps_size = size_cnt;
	}
	#else
	if( pstream[0] == 0 && pstream[1] == 0 && pstream[2] == 0 && pstream[3] == 1 )
	{
		// sps
		if( (pstream[4]&0x1f) == 7 )
		{
			sps_size = size;
			memcpy(sps_data, pstream+5, size);
			sps_data_ok = 1;
		}
		// pps
		else if( (pstream[4]&0x1f) == 8 )
		{
			pps_size = size;
			memcpy(sps_data, pstream+5, size);
			pps_data_ok = 1;
		}
	}
	#endif
	if( sps_data_ok && pps_data_ok )
		return 1;
	else
		return 0;
}
void sps_pps_deinit(void)
{
	sps_data_ok = 0;
	pps_data_ok = 0;
}

int extradata_init(AVCodecContext* pctx)
{
#if 0
	int extradata_size = sps_size + pps_size + 11;
	extradata_avc1 = (unsigned char*)av_malloc(extradata_size + AV_INPUT_BUFFER_PADDING_SIZE);
	extradata_avc1[0] = 0x01; 				//version
	extradata_avc1[1] = sps_data[1]; 		// profile
	extradata_avc1[2] = sps_data[2]; 		// profile compat
	extradata_avc1[3] = sps_data[3]; 		// level
	extradata_avc1[4] = 0xFC | (4 - 1); 		// reserved(6) + length size minus one(2)
	extradata_avc1[5] = 0xE0 | 1; 			// 1 个sps
	extradata_avc1[6] = sps_size >> 8; 		// sps size high
	extradata_avc1[7] = sps_size & 0xFF; 	// sps size low
	memcpy(extradata_avc1 + 8, sps_data, sps_size); // sps
	extradata_avc1[8 + sps_size] = 0x01; 	// number of PPS
	extradata_avc1[9 + sps_size] = pps_size >> 8; 	// pps size high
	extradata_avc1[10 + sps_size] = pps_size & 0xFF; //pps size low
	memcpy(extradata_avc1 + 11 + sps_size, pps_data, pps_size); //pps
#else
	int extradata_size = sps_size + pps_size + 8;
	extradata_avc1 = (unsigned char*)av_malloc(extradata_size);
	extradata_avc1[0] = 0x00;
	extradata_avc1[1] = 0x00;
	extradata_avc1[2] = 0x00;
	extradata_avc1[3] = 0x01;
	memcpy(extradata_avc1 + 4, sps_data, sps_size); // sps
	extradata_avc1[4 + sps_size] = 0x00;
	extradata_avc1[5 + sps_size] = 0x00;
	extradata_avc1[6 + sps_size] = 0x00;
	extradata_avc1[7 + sps_size] = 0x01;
	memcpy(extradata_avc1 + 8 + sps_size, pps_data, pps_size); //pps
#endif
	av_freep(&pctx->extradata);
	pctx->extradata = extradata_avc1;
	pctx->extradata_size = extradata_size;
	return 0;
}

#endif
// lzh_20230919_e

AVCodecContext  encodeCtx_s;
int Rec_init( ffmpeg_rec_dat2* pusr_dat  )
{
    int ret;
	ret = avformat_alloc_output_context2(&pusr_dat->ofmt_ctx, NULL, NULL, pusr_dat->out_file);
    if (ret < 0)  
	{
        printf( "Could not create output context\n");
        return -1;
    }
	AVStream *out_stream = avformat_new_stream(pusr_dat->ofmt_ctx, NULL);
	AVCodecContext *encodeCtx= &encodeCtx_s;//av_malloc(sizeof(AVCodecContext));
	encodeCtx->codec_id = pusr_dat->videotype == 0? AV_CODEC_ID_H264 : AV_CODEC_ID_H265;
	encodeCtx->codec_type = AVMEDIA_TYPE_VIDEO;
	encodeCtx->pix_fmt = AV_PIX_FMT_NV12;
	encodeCtx->width = pusr_dat->width;
	encodeCtx->height = pusr_dat->height;
	//encodeCtx->time_base = av_inv_q(pusr_dat->fps);
	encodeCtx->time_base.num = 1;  
	encodeCtx->time_base.den = pusr_dat->fps;
	extradata_init(encodeCtx);

	ret = avcodec_parameters_from_context(out_stream->codecpar, encodeCtx);
	if (ret < 0) 
	{
		printf( "Failed to copy encoder parameters to output stream\n");
		return ret;
	}
	out_stream->time_base = encodeCtx->time_base;
	pusr_dat->out_stream=out_stream;
	//av_free(encodeCtx);

	ret = avio_open(&pusr_dat->ofmt_ctx->pb, pusr_dat->out_file, AVIO_FLAG_WRITE);
	if (ret < 0) 
	{
		printf("Could not open output file %s\n", pusr_dat->out_file);
		return -1;
	}

    /* init muxer, write output file header */
    ret = avformat_write_header(pusr_dat->ofmt_ctx, NULL);
    if (ret < 0) 
	{
        printf("Error when avformat_write_header\n");
		return -1;
    }
	printf("-----ffmpeg_rec_init ok !!!\n");
	return 0;
}


int ffmpeg_rec_init(char* file_name, int period, int enc_type,  int fps, int width, int height,void *call_back)
{
	ffmpeg_rec_dat2* pins = &previewRec;
	int ret = 0;

	RECORD_STATE state = api_getRecState();
	//printf( "--ffmpeg_rec_init state[%d] file[%s] w[%d] h[%d] fps[%d] vdtype[%d]\n", state, file_name, width, height, fps, enc_type); 
	if( state == RECORD_IDLE )
	{
		strcpy( pins->out_file, file_name );
		pins->width = width;
		pins->height = height;
		pins->videotype = enc_type;
		pins->fps = fps;
		pins->TotalSec = fps * period ;
		pins->CurSec = 0;
		pins->CallBack=call_back;
		// lzh_20230919_s
		#if 0
		ret = Rec_init(pins);
		if( ret == -1 )
		{
			api_setRecState(RECORD_IDLE);
			return -1;
		}
		else
		{
			api_setRecState(RECORD_START);
			return 0;
		}
		#else
		return 0;
		#endif
		// lzh_20230919_e
	}
	else
	{
		printf( "------------ffmpeg_rec_init state fail----------------\n");	
		return -1;
	}

}
static int64_t pts_last = 0;
int ffmpeg_rec_proc( char* p_data, int size, unsigned long long pts, unsigned long seq_no, int p0i1)
{
	int ret = 0;
	ffmpeg_rec_dat2* pins = &previewRec;
    AVPacket pkt;
	static float one_tick;

	// lzh_20230919_s
	if( api_getRecState() == RECORD_IDLE )
	{
		//printf("-----11111111111111ffmpeg_rec_proc sps_pps_init size=%d !!!\n", size);
		if( sps_pps_init(p_data,size) )
		{
			//printf("-----2222222ffmpeg_rec_proc Rec_init!!!\n");
			if( Rec_init(pins) == 0 )
			{
				api_setRecState(RECORD_START);
			}
		}
		return 0;
	}
	// lzh_20230919_e

	if( api_getRecState() == RECORD_START )
	{
		pts_last = 0;
		one_tick = 1.0/av_q2d(pins->out_stream->time_base)/(float)pins->fps;
		api_setRecState(RECORD_RUN);
		//printf("-----3333333333333ffmpeg_rec_proc start !!!\n");
	}
	memset(&pkt, 0, sizeof(pkt));
	av_init_packet(&pkt);
	if( api_getRecState() == RECORD_RUN )
	{
		if(pins->CurSec==0&&p0i1!=1)
			return 0;
		//printf("ffmpeg_rec_proc len=[%d] pts=[%d],AV_TIME_BASE=%f,%f\n",size, pts_last,av_q2d(pins->out_stream->time_base),av_q2d(encodeCtx_s.time_base));
		pkt.size = size;
		void *save_pr=av_malloc(pkt.size);
		pkt.data =save_pr;
		memcpy( pkt.data, p_data, pkt.size );
		//pkt.pts = 90000/pins->fps + pts_last;;
		pkt.pts=pts_last;
		//pkt.pts = 12500/pins->fps + pts_last;;
		pkt.dts = pkt.pts;
		//pts_last = pkt.pts;
		pts_last= one_tick + pts_last;
		ret = av_interleaved_write_frame(pins->ofmt_ctx, &pkt);
		
		av_packet_unref(&pkt);
		av_free(save_pr);
		
		if (ret < 0) 
		{
			printf("write_frame ERR=%d\n", ret);
		}
		if( ++pins->CurSec >= pins->TotalSec)
		{
			ret = 1;
			//api_setRecState(RECORD_IDLE);
		}
		//printf("ffmpeg_rec_proc CurrSec=%d ret=%d\n", pins->CurSec, ret);
	}
	else
	{
		//ret = 1;
	}
	return ret;
}

int ffmpeg_rec_exit( void )
{
	int ret = 0;
	sps_pps_deinit();
	ffmpeg_rec_dat2* pusr_dat = &previewRec;
	if(api_getRecState() != RECORD_IDLE)
	{
		api_setRecState(RECORD_IDLE);
		av_write_trailer(pusr_dat->ofmt_ctx);
		if (pusr_dat->ofmt_ctx)
			avio_closep(&pusr_dat->ofmt_ctx->pb);
		avformat_free_context(pusr_dat->ofmt_ctx);
		printf("-----ffmpeg_rec_exit !!!---------%d\n",pusr_dat->CurSec);
		if(pusr_dat->CallBack!=NULL)
		{
			(*(pusr_dat->CallBack))(pusr_dat);
		}
	}
	return 0;
}