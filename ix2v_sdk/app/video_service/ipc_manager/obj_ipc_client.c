#include "obj_ipc_client.h"
#include "ffmpeg_show.h"
#include "elog_forcall.h"


int Start_OneIpc_Show(int ins_num, cJSON * target, cJSON * win, int time)
{
	int x,y,w,h;
	int width,height,vdtype;
	char rtsp[200] = {0};
	if(target==NULL)
		return -1;
	//printf("-----------111111111111111\n");
	GetJsonDataPro(target, "RTSP_URL", rtsp);
	//printf("-----------11112222222222 %s\n",rtsp);
	log_d("Start_OneIpc_Show = %s\n", rtsp);
	//printf("-----------11112222233333 %s\n",rtsp);
	if(GetJsonDataPro(target,"RES_W",&width)==0)
		width=1920;
	//printf("-----------1111333333333\n");
	if(GetJsonDataPro(target,"RES_H",&height)==0)
		height=1080;	
	//printf("-----------111144444444444\n");
	if((height > 1152) || (width > 2048)) //2K=2048*1152
		return -1;
	if(GetJsonDataPro(target,"CODEC",&vdtype)==0)
		vdtype=0;	
	//printf("-----------111122555555555\n");
	if(GetJsonDataPro(win,"X",&x)==0)
		x=0;
	//printf("-----------111126666666666\n");
	if(GetJsonDataPro(win,"Y",&y)==0)
		y=0;
	//printf("-----------11117777777777777\n");
	if(GetJsonDataPro(win,"W",&w)==0)
		w=1024;	
	//printf("-----------111188888888888\n");
	if(GetJsonDataPro(win,"H",&h)==0)
		h=600;	
	//printf("-----------11119999999999\n");
	Set_ds_show_pos(ins_num, x, y, w, h);
	//printf("-----------22222222222\n");
	if(api_ipc_show_one_stream(ins_num, rtsp, width, height, vdtype)!=0)
	{
		Clear_ds_show_layer(ins_num);
		return -1;
	}
	//printf("-----------33333333333\n");
	return 0;
}

int Stop_OneIpc_Show( int ins_num )
{
	return api_ipc_stop_mointor_one_stream(ins_num);
}

int api_OneIpc_Rtsp_Show(int ins_num, unsigned char* rtsp, int width ,int height, int vdtype, cJSON * win)
{
	int x,y,w,h;
	if((height > 1152) || (width > 2048)) //2K=2048*1152
		return -1;
	if(win == NULL)
	{
		x=0;
		y=0;
		w=1024;	
		h=600;
	}
	else
	{
		if(GetJsonDataPro(win,"X",&x)==0)
			x=0;
		if(GetJsonDataPro(win,"Y",&y)==0)
			y=0;
		if(GetJsonDataPro(win,"W",&w)==0)
			w=1024;	
		if(GetJsonDataPro(win,"H",&h)==0)
			h=600;	
	}
	
	Set_ds_show_pos(ins_num, x, y, w, h);
	return api_ipc_show_one_stream(ins_num, rtsp, width, height, vdtype);
}

int Start_OneIpc_Rtp(int ins_num, cJSON * target,int time,int send_list_ch)
{
	int x,y,w,h;
	int width,height,vdtype;
	char rtsp[200] = {0};

	GetJsonDataPro(target, "RTSP_URL", rtsp);
	log_d("Start_OneIpc_Show = %s\n", rtsp);
	if(GetJsonDataPro(target,"RES_W",&width)==0)
		width=1920;
	if(GetJsonDataPro(target,"RES_H",&height)==0)
		width=1080;	
	if(GetJsonDataPro(target,"CODEC",&vdtype)==0)
		vdtype=0;		
	
	return api_start_ffmpeg_rtp(ins_num,rtsp,vdtype,send_list_ch);
}

int Stop_OneIpc_Rtp( int ins_num )
{
	return api_stop_ffmpeg_rtp(ins_num);
}

