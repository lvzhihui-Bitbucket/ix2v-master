

#include "ffmpeg_play.h"
#include <libavutil/timestamp.h>
#define USE_H264BSF 1

extern int	bkgd_w;
extern int 	bkgd_h;

ffmpegPlay_dat_t		ffmpegPlay =
{
	.aviPlaybackStateLock 	= PTHREAD_MUTEX_INITIALIZER,
	.width 					= 1280,
	.height					= 720,
	.fps					= 15,
	.videotype				= 0,
	.CallBack				=NULL,
};

// �߳�����ǰ�ĳ�ʼ��
int task_ffmpegPlay_init( void* arg )
{
	one_tiny_task_t *ptiny_task = (one_tiny_task_t*)arg;
	ffmpegPlay_dat_t *pusr_dat = (ffmpegPlay_dat_t*)ptiny_task->pusr_dat;	
	int i;
	int ret;

	if( !GetRegisterState() )
	{
		SetRegisterState(1);
		printf("\n=================begin to av_register_all...========\n");
		av_register_all();
		avcodec_register_all();
		avformat_network_init();
	}
	printf("\n=================begin to avformat_open_input: [%s]...========\n", pusr_dat->filename );
	api_setPlayState(FFMPEG_PLAYBACK_START);
	ret = avformat_open_input(&pusr_dat->ic, pusr_dat->filename, NULL, NULL);
	if (ret < 0) 
	{
		printf("cannot open %s\n", pusr_dat->filename);
		//avformat_close_input(&pusr_dat->ic);
		return -1;
	}
	#if 1
	printf("\n=================begin to av_find_stream_info...========\n");
	ret = avformat_find_stream_info(pusr_dat->ic,NULL ); //&options);	 // ����Ĭ����Ҫ���ѽϳ���ʱ���������ʽ̽��?, ��μ���̽��ʱ���ڣ�? ����ͨ������AVFotmatContext��probesize��max_analyze_duration���Խ��е���
	if (ret < 0) 
	{
		printf( "could not find codec parameters\n");
		//avformat_close_input(&pusr_dat->ic);
		return -1;
	}
	printf("\n=================av_find_stream_info ok...========\n");
	for( i = 0; i < pusr_dat->ic->nb_streams; i++ )
	{
		// ����?��Ƶ����ý������
		if(pusr_dat->ic->streams[i]->codec->codec_type == AVMEDIA_TYPE_VIDEO)
		{
			pusr_dat->videostream = i;
			pusr_dat->width = pusr_dat->ic->streams[i]->codecpar->width;	   
			pusr_dat->height = pusr_dat->ic->streams[i]->codecpar->height;				
			pusr_dat->videotype = pusr_dat->ic->streams[i]->codecpar->codec_id == AV_CODEC_ID_H265 ? 1 : 0;
			printf("videostream=%d Videotype=%d width=%d, height=%d\n", pusr_dat->videostream, pusr_dat->videotype, pusr_dat->width, pusr_dat->height);
			if(pusr_dat->ic->streams[i]->avg_frame_rate.den != 0 && pusr_dat->ic->streams[i]->avg_frame_rate.num != 0)
			{
				float frame_rate = 1.0*pusr_dat->ic->streams[i]->avg_frame_rate.num/pusr_dat->ic->streams[i]->avg_frame_rate.den;
				pusr_dat->fps =(int) (frame_rate + 0.5);//ÿ������? 
				if(pusr_dat->fps == 0)
					pusr_dat->fps = 15;
				printf( "Video fps=%d framerate=%.2f \n", pusr_dat->fps, frame_rate); //pusr_dat->ic->streams[i]->duration * av_q2d(AV_TIME_BASE_Q));
			}
			#if 0
			AVRational frame_rate;
    		double rate;
			frame_rate = av_guess_frame_rate(pusr_dat->ic, pusr_dat->ic->streams[i], NULL);
            rate = (frame_rate.num && frame_rate.den ? av_q2d((AVRational){frame_rate.num, frame_rate.den}) : 15);
			pusr_dat->fps =(int) (rate + 0.5);//ÿ������? 
			printf( "Video rate=%.2f Video fps=%d\n", rate, pusr_dat->fps );
			#endif
			pusr_dat->TotalFrames = pusr_dat->ic->streams[i]->nb_frames;
			pusr_dat->TotalSec = pusr_dat->TotalFrames/pusr_dat->fps;
			printf("duration=%d TotalFrames=%d\n", pusr_dat->TotalSec, pusr_dat->TotalFrames);
			if(pusr_dat->CallBack)
				pusr_dat->CallBack(0, pusr_dat->TotalSec);
			break;
		}
	}
	if(!pusr_dat->TotalSec)
	{
		printf("pusr_dat->TotalSec is 0\n");
		return -1;
	}
	#else
	pusr_dat->width = 1280;
	pusr_dat->height = 720;
	pusr_dat->videotype = 0;
	pusr_dat->TotalFrames = 150;
	pusr_dat->fps = 15;
	pusr_dat->videostream = 0;
	pusr_dat->TotalSec = 10;
	if(pusr_dat->CallBack)
		pusr_dat->CallBack(0, pusr_dat->TotalSec);
	#endif
#if 1
#if	defined(PID_IX482)
	Set_ds_show_pos(0, 0, 0, 1024, 600);
#elif defined(PID_IX850)
	Set_ds_show_pos(0, 0, 0, 480, 360);
#else		
	Set_ds_show_pos(0, 0, 0, 720, 576);
#endif	
#endif
	api_monitor_show_start(0, pusr_dat->width, pusr_dat->height==1080? 1088 :pusr_dat->height, pusr_dat->videotype);
	//api_h264_show_start(0, pusr_dat->width, pusr_dat->height, pusr_dat->fps,512, 0, 0, bkgd_w, bkgd_h, pusr_dat->videotype);

	return 0;
}

#if USE_H264BSF
AVBitStreamFilterContext* h264bsfc = NULL;
#endif

// �߳�ѭ������
int task_ffmpegPlay_process( void* arg )
{
	int outbuf_flag = 0;
    int ret;
	one_tiny_task_t *ptiny_task = (one_tiny_task_t*)arg;

	ffmpegPlay_dat_t *pusr_dat = (ffmpegPlay_dat_t*)ptiny_task->pusr_dat;	

    AVPacket pkt;
	AVPacket new_pkt;
	int MicroSecPerFrame;
	int time_use;
	struct timeval start;
	struct timeval end;

	api_setPlayState(FFMPEG_PLAYBACK_RUN);
	//MicroSecPerFrame = AV_TIME_BASE/pusr_dat->fps - 10000;
	//uint8_t* outbuf = NULL;
	//int outlen = 0;
	av_init_packet(&pkt);
	pkt.data = NULL;
	pkt.size = 0;
#if USE_H264BSF
	av_init_packet(&new_pkt);
	new_pkt.data = NULL;
	new_pkt.size = 0;
	AVBitStreamFilterContext* h264bsfc = av_bitstream_filter_init(pusr_dat->videotype==0? "h264_mp4toannexb" : "hevc_mp4toannexb"); 
#endif
	
	replay:
	pusr_dat->play_frame = 0;
	pusr_dat->play_time = 0;
	pusr_dat->speedFF = 0;
	pusr_dat->play_rate = 1;
	while( one_tiny_task_is_running(ptiny_task) )
	{	
		if(pusr_dat->speedFF != 0)
		{
			pusr_dat->play_time = pusr_dat->ffTime;
			#if 0
			if(pusr_dat->ffTime != 0)
			{
				pusr_dat->play_time = pusr_dat->ffTime;
			}
			else
			{
				if(pusr_dat->speedFF == 1)
					pusr_dat->play_time = pusr_dat->play_time>=pusr_dat->TotalSec-5? (pusr_dat->TotalSec) : (pusr_dat->play_time+5);
				else
					pusr_dat->play_time = pusr_dat->play_time<=5? 0 : (pusr_dat->play_time-5);
			}
			#endif
			int64_t pts = pusr_dat->play_time / av_q2d(pusr_dat->ic->streams[pusr_dat->videostream]->time_base);
			ret = av_seek_frame(pusr_dat->ic, pusr_dat->videostream, pts, AVSEEK_FLAG_BACKWARD);
			//ret = av_seek_frame(pusr_dat->ic, -1, AV_TIME_BASE*pusr_dat->play_time, AVSEEK_FLAG_BACKWARD);
			//ret = avformat_seek_file(pusr_dat->ic, -1, INT64_MIN, AV_TIME_BASE*pusr_dat->play_time, INT64_MAX, 0);
			printf("av_seek_frame time=%ds ret=%d\n",pusr_dat->play_time, ret);
			pusr_dat->speedFF = 0;
		}
		if( api_getPlayState() == FFMPEG_PLAYBACK_RUN )
		{
			memset(&pkt, 0, sizeof(pkt));
			
			gettimeofday( &start, NULL );
			if(pusr_dat->play_frame + pusr_dat->fps*pusr_dat->play_time >= pusr_dat->TotalFrames)
			{
				if(pusr_dat->CallBack)
					pusr_dat->CallBack(pusr_dat->play_time, 0);
				#if	0	//def PID_IXG
				api_setPlayState(FFMPEG_PLAYBACK_REPLAY);
				#else
				api_setPlayState(FFMPEG_PLAYBACK_END);
				#endif
				continue;
			}
			ret= av_read_frame(pusr_dat->ic, &pkt);
			if(ret>=0)
			{
				//printf("size=%d,pts=[%08x],d0-d4=%02x %02x %02x %02x\n",pkt.size,pkt.pts,pkt.data[0],pkt.data[1],pkt.data[2],pkt.data[3]);
				if( pkt.stream_index == pusr_dat->videostream )
				{
#if USE_H264BSF
					if( av_bitstream_filter_filter(h264bsfc, pusr_dat->ic->streams[pusr_dat->videostream]->codec, NULL, &new_pkt.data, &new_pkt.size, pkt.data, pkt.size, pkt.flags & AV_PKT_FLAG_KEY) > 0 )
					{
						outbuf_flag = 1;
					}
					else
					{
						printf("out buffer NOT allocated...\n");
						outbuf_flag = 0;
					}
					if( new_pkt.data[0]==0x00 && new_pkt.data[1]==0x00 && new_pkt.data[2]==0x00 && new_pkt.data[3]==0x01 )
					{
						api_monitor_show_buf( 0, new_pkt.data, new_pkt.size );
					}
					else
					{
						printf("NOT h264 data format...\n");
					}
					//int64_t pts_time = av_rescale_q(new_pkt.pts, time_base, time_base_q);
					if (new_pkt.data != NULL)
					{
						av_freep(&new_pkt.data);
						av_packet_free_side_data(&new_pkt);
						av_free_packet(&new_pkt);
						new_pkt.data = NULL;
						new_pkt.size = 0;
					}
#else					
					if( pkt.data[0]==0x00 && pkt.data[1]==0x00 && pkt.data[2]==0x00 && pkt.data[3]==0x01 )
					{
						api_monitor_show_buf( 0, pkt.data, pkt.size );
					}
					else
					{
						printf("NOT h264 data format...\n");
					}
					//int64_t pts_time = av_rescale_q(pkt.pts, time_base, time_base_q);
#endif																					
					//AVRational *time_base = &pusr_dat->ic->streams[pusr_dat->videostream]->time_base;
					//AVRational time_base = pusr_dat->ic->streams[pusr_dat->videostream]->time_base;
					//AVRational time_base_q = { 1, AV_TIME_BASE };
					if(pusr_dat->play_frame < pusr_dat->fps - 1)
					{
						pusr_dat->play_frame++;
					}
					else
					{
						pusr_dat->play_frame = 0;
						pusr_dat->play_time++;
						if(pusr_dat->CallBack)
							pusr_dat->CallBack(pusr_dat->play_time, 0);
						//printf("play_time = %d\n", pusr_dat->play_time);
					}
					gettimeofday( &end, NULL );
						
					time_use = (end.tv_sec - start.tv_sec) * 1000000 + (end.tv_usec - start.tv_usec);
					MicroSecPerFrame = AV_TIME_BASE/(pusr_dat->fps*pusr_dat->play_rate);
					//printf("time_use = %d, MicroSecPerFrame = %d\n", time_use, MicroSecPerFrame);
					if( (MicroSecPerFrame - time_use ) > 0 )
					{
						usleep(  MicroSecPerFrame - time_use);
					} 
				}
				av_packet_unref(&pkt);
			} 
			else
			{
				usleep(200);
			}
		}
		else if( api_getPlayState() == FFMPEG_PLAYBACK_PAUSE || api_getPlayState() == FFMPEG_PLAYBACK_START || api_getPlayState() == FFMPEG_PLAYBACK_END)
		{
			usleep( 160 );
		}
		else if(api_getPlayState() == FFMPEG_PLAYBACK_REPLAY)
		{
			av_seek_frame(pusr_dat->ic, pusr_dat->videostream, 0, AVSEEK_FLAG_BACKWARD);
			usleep(100);
			api_setPlayState(FFMPEG_PLAYBACK_RUN);				
			goto replay;
		}
		else if(api_getPlayState() == FFMPEG_PLAYBACK_CH_FILE_START)
		{
			usleep(100);
			api_setPlayState(FFMPEG_PLAYBACK_CH_FILE_DOING);
		}
		else if(api_getPlayState() == FFMPEG_PLAYBACK_CH_FILE_OK)
		{
			usleep(100);
			api_setPlayState(FFMPEG_PLAYBACK_RUN);				
			goto replay;
		}
		else if(api_getPlayState() == FFMPEG_PLAYBACK_STOP)
		{
#if USE_H264BSF
			if( h264bsfc != NULL )
			{
				av_bitstream_filter_close(h264bsfc);  
				h264bsfc = NULL;
			}
#endif
			usleep(100);
			//break;
		}
		
    }	
    return 0;
}

// �˳����̵߳Ĵ���: �ڴ����Դ�Ļ����ͷ�?
int task_ffmpegPlay_exit( void* arg )
{
#if USE_H264BSF
	if( h264bsfc != NULL )
	{
		av_bitstream_filter_close(h264bsfc);  
		h264bsfc = NULL;
	}
#endif
	one_tiny_task_t *ptiny_task = (one_tiny_task_t*)arg;
	ffmpegPlay_dat_t *pusr_dat = (ffmpegPlay_dat_t*)ptiny_task->pusr_dat;	
	api_setPlayState(FFMPEG_PLAYBACK_IDLE);
	avformat_close_input(&pusr_dat->ic);
	printf("-----api_stop_ffmpegPlay ok !!!\n");
	return 0;
}
/////////////////////////////////////////////////////////////////////////////////////////////////
int api_start_ffmpegPlay(char* file, PlaybackCallBack fun )
{
	ffmpegPlay_dat_t* pins = &ffmpegPlay;
	int ret = 0;

	FFMPEG_PLAYBACK_STATE_t state = api_getPlayState();
	printf( "--api_start_ffmpegPlay state=%d---file=%s-\n", state, file); 
	if( state == FFMPEG_PLAYBACK_IDLE )
	{
		strcpy( pins->filename, file );
		if(fun)
			pins->CallBack 	= fun;
		ret = one_tiny_task_start2( &pins->task_process, task_ffmpegPlay_init, task_ffmpegPlay_process, task_ffmpegPlay_exit, (void*)pins );
		if( ret == -1 )
		{
			api_setPlayState(FFMPEG_PLAYBACK_IDLE);
			return -1;
		}
		else
		{
			return 0;
		}
	}
	else
	{
		printf( "------------playback_start state fail----------------\n");	
		return -1;
	}

}

static void* api_stop_ffmpegPlay_thread( void* arg )
{
	ffmpegPlay_dat_t* pins = (ffmpegPlay_dat_t*)arg;
	//if(api_getPlayState() != FFMPEG_PLAYBACK_STOP)
	{
		api_monitor_show_stop(0);
		one_tiny_task_stop( &pins->task_process );
	}
}

int api_stop_ffmpegPlay(void)
{
	ffmpegPlay_dat_t* pins = &ffmpegPlay;

	while( api_getPlayState() != FFMPEG_PLAYBACK_IDLE)
	{		
		// ��Ϊ�����׶Σ���ȴ��������
		if(api_getPlayState() != FFMPEG_PLAYBACK_START)
		{
			api_setPlayState(FFMPEG_PLAYBACK_STOP);
			break;
		}
		usleep(10000);
	}
	#if 0
	//create new thread process for lvgl menu calling
	pthread_t pid_stop;
	pthread_create( &pid_stop, NULL, api_stop_ffmpegPlay_thread, (void*)pins );
	pthread_detach(pid_stop);
	#endif
	one_tiny_task_stop2( &pins->task_process );
	api_monitor_show_stop(0);
	Clear_ds_show_layer(0);
	return 0;	
}

int api_ffmpegPlayFf(int        sec, int ff )
{
	ffmpegPlay_dat_t* pusr_dat = &ffmpegPlay;
	if( api_getPlayState() == FFMPEG_PLAYBACK_RUN || api_getPlayState() == FFMPEG_PLAYBACK_PAUSE || api_getPlayState() == FFMPEG_PLAYBACK_END)
	{
		printf("api_ffmpegPlayFf time=%d \n",sec);
		pusr_dat->speedFF = ff;
		pusr_dat->ffTime = sec;
		if(api_getPlayState() == FFMPEG_PLAYBACK_END)
		{
			api_setPlayState(FFMPEG_PLAYBACK_RUN);	
		}
	}
	return 0;	
}
int api_ffmpegPlaySpeed(int           speed )
{
	ffmpegPlay_dat_t* pusr_dat = &ffmpegPlay;
	if( api_getPlayState() == FFMPEG_PLAYBACK_RUN )
	{
		pusr_dat->play_rate = speed;
	}
	return 0;	
}

int api_ffmpegPlayPause( void )
{
	if( api_getPlayState() == FFMPEG_PLAYBACK_RUN )
		api_setPlayState(FFMPEG_PLAYBACK_PAUSE);
	else if( api_getPlayState() == FFMPEG_PLAYBACK_PAUSE )
		api_setPlayState(FFMPEG_PLAYBACK_RUN);
	return 0;
}

int api_ffmpegPlayContinue( void )
{
	if( api_getPlayState() == FFMPEG_PLAYBACK_PAUSE )
		api_setPlayState(FFMPEG_PLAYBACK_RUN);

	return 0;	
}

int api_ffmpegPlayReplay( void )
{
	if( api_getPlayState() == FFMPEG_PLAYBACK_END )
		api_setPlayState(FFMPEG_PLAYBACK_REPLAY);

	return 0;
}
int api_change_file_ffmpegPlay(char* file, PlaybackCallBack fun )
{
	int wait_cnt=0;
	int save_h;
	int save_w;
	int save_codec;
	int ret,i;
	ffmpegPlay_dat_t* pusr_dat = &ffmpegPlay;
	if(api_getPlayState() == FFMPEG_PLAYBACK_IDLE)
		return -1;
	if(api_getPlayState() == FFMPEG_PLAYBACK_START)
		usleep(100*1000);
	if(api_getPlayState() == FFMPEG_PLAYBACK_START)
		return -1;
	api_setPlayState(FFMPEG_PLAYBACK_CH_FILE_START);
	while(api_getPlayState() != FFMPEG_PLAYBACK_CH_FILE_DOING)
	{
		if(wait_cnt++>100)
			return -1;
		usleep(10*1000);	
	}
	save_h=pusr_dat->height;
	save_w=pusr_dat->width;
	save_codec=pusr_dat->videotype;
	avformat_close_input(&pusr_dat->ic);
	strcpy(pusr_dat->filename,file);
	if(fun)
		pusr_dat->CallBack=fun;
	ret = avformat_open_input(&pusr_dat->ic, pusr_dat->filename, NULL, NULL);
	if (ret < 0) 
	{
		printf("cannot open %s\n", pusr_dat->filename);
		//avformat_close_input(&pusr_dat->ic);
		return -1;
	}
	#if 1
	printf("\n=================begin to av_find_stream_info...========\n");
	ret = avformat_find_stream_info(pusr_dat->ic,NULL ); //&options);	 // ����Ĭ����Ҫ���ѽϳ���ʱ���������ʽ̽��?, ��μ���̽��ʱ���ڣ�? ����ͨ������AVFotmatContext��probesize��max_analyze_duration���Խ��е���
	if (ret < 0) 
	{
		printf( "could not find codec parameters\n");
		//avformat_close_input(&pusr_dat->ic);
		return -1;
	}
	printf("\n=================av_find_stream_info ok...========\n");
	for( i = 0; i < pusr_dat->ic->nb_streams; i++ )
	{
		// ����?��Ƶ����ý������
		if(pusr_dat->ic->streams[i]->codec->codec_type == AVMEDIA_TYPE_VIDEO)
		{
			pusr_dat->videostream = i;
			pusr_dat->width = pusr_dat->ic->streams[i]->codecpar->width;	   
			pusr_dat->height = pusr_dat->ic->streams[i]->codecpar->height;				
			pusr_dat->videotype = pusr_dat->ic->streams[i]->codecpar->codec_id == AV_CODEC_ID_H265 ? 1 : 0;
			printf("videostream=%d Videotype=%d width=%d, height=%d\n", pusr_dat->videostream, pusr_dat->videotype, pusr_dat->width, pusr_dat->height);
			if(pusr_dat->ic->streams[i]->avg_frame_rate.den != 0 && pusr_dat->ic->streams[i]->avg_frame_rate.num != 0)
			{
				float frame_rate = 1.0*pusr_dat->ic->streams[i]->avg_frame_rate.num/pusr_dat->ic->streams[i]->avg_frame_rate.den;
				pusr_dat->fps =(int) (frame_rate + 0.5);//ÿ������? 
				if(pusr_dat->fps == 0)
					pusr_dat->fps = 15;
				printf( "Video fps=%d framerate=%.2f \n", pusr_dat->fps, frame_rate); //pusr_dat->ic->streams[i]->duration * av_q2d(AV_TIME_BASE_Q));
			}
			#if 0
			AVRational frame_rate;
    		double rate;
			frame_rate = av_guess_frame_rate(pusr_dat->ic, pusr_dat->ic->streams[i], NULL);
            rate = (frame_rate.num && frame_rate.den ? av_q2d((AVRational){frame_rate.num, frame_rate.den}) : 15);
			pusr_dat->fps =(int) (rate + 0.5);//ÿ������? 
			printf( "Video rate=%.2f Video fps=%d\n", rate, pusr_dat->fps );
			#endif
			pusr_dat->TotalFrames = pusr_dat->ic->streams[i]->nb_frames;
			pusr_dat->TotalSec = pusr_dat->TotalFrames/pusr_dat->fps;
			printf("duration=%d TotalFrames=%d\n", pusr_dat->TotalSec, pusr_dat->TotalFrames);
			if(pusr_dat->CallBack)
				pusr_dat->CallBack(0, pusr_dat->TotalSec);
			break;
		}
	}
	if(!pusr_dat->TotalSec)
		return -1;
	#else
	pusr_dat->TotalFrames = 150;
	pusr_dat->fps = 15;
	pusr_dat->videostream = 0;
	pusr_dat->TotalSec = 10;
	if(pusr_dat->CallBack)
		pusr_dat->CallBack(0, pusr_dat->TotalSec);
	#endif	
	if(save_h!=pusr_dat->height||save_w!=pusr_dat->width||save_codec!=pusr_dat->videotype)
	{
		api_monitor_show_stop(0);
		api_monitor_show_start(0, pusr_dat->width, pusr_dat->height==1080? 1088 :pusr_dat->height, pusr_dat->videotype);
	}
	api_setPlayState(FFMPEG_PLAYBACK_CH_FILE_OK);
	wait_cnt=0;
	while(api_getPlayState() != FFMPEG_PLAYBACK_RUN)
	{
		if(wait_cnt++>50)
			return -1;
		usleep(10*1000);	
	}
	return 0;
}
void api_setPlayState( FFMPEG_PLAYBACK_STATE_t state )
{
	ffmpegPlay_dat_t* pins = &ffmpegPlay;
	pthread_mutex_lock(&pins->aviPlaybackStateLock);
	pins->state = state;
	pthread_mutex_unlock(&pins->aviPlaybackStateLock);
}

FFMPEG_PLAYBACK_STATE_t api_getPlayState( void )
{
	FFMPEG_PLAYBACK_STATE_t state;
	ffmpegPlay_dat_t* pins = &ffmpegPlay;
	pthread_mutex_lock(&pins->aviPlaybackStateLock);
	state = pins->state;
	pthread_mutex_unlock(&pins->aviPlaybackStateLock);
	return state;

}


