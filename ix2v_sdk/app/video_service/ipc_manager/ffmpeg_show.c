
#include "ffmpeg_show.h"
#include "elog_forcall.h"
int av_register_init  =0;
// 线程启动前的初始化
void ffmpeg_init(void)
{
	/* initialize libavcodec, and register all codecs and formats */
	printf("\n=================begin to av_register_all...========\n");
	av_register_init = 1;
	av_register_all();
	avcodec_register_all();
	avformat_network_init();
	avfilter_register_all();
}

ffmpegRun_t	ffmpegshow[8] =
{
	{
		.ffmpegStateLock = PTHREAD_MUTEX_INITIALIZER,
		.state 			 = 0,
		.data_buff		=NULL,
		.data_buff_len	=512*20,
	},
	{
		.ffmpegStateLock = PTHREAD_MUTEX_INITIALIZER,
		.state 			 = 0,
		.data_buff		=NULL,
		.data_buff_len	=512*20,
	},
	{
		.ffmpegStateLock = PTHREAD_MUTEX_INITIALIZER,
		.state 			 = 0,
		.data_buff		=NULL,
		.data_buff_len	=512*20,
	},
	{
		.ffmpegStateLock = PTHREAD_MUTEX_INITIALIZER,
		.state 			 = 0,
		.data_buff		=NULL,
		.data_buff_len	=512*20,
	},	
	{
		.ffmpegStateLock = PTHREAD_MUTEX_INITIALIZER,
		.state 			 = 0,
		.data_buff		=NULL,
		.data_buff_len	=512*20,
	},	
	{
		.ffmpegStateLock = PTHREAD_MUTEX_INITIALIZER,
		.state 			 = 0,
		.data_buff		=NULL,
		.data_buff_len	=512*20,
	},	
	{
		.ffmpegStateLock = PTHREAD_MUTEX_INITIALIZER,
		.state 			 = 0,
		.data_buff		=NULL,
		.data_buff_len	=512*20,
	},	
	{
		.ffmpegStateLock = PTHREAD_MUTEX_INITIALIZER,
		.state 			 = 0,
		.data_buff		=NULL,
		.data_buff_len	=512*20,
	},	
};

void set_ffmpeg_show_state(ffmpegRun_t* pusr_dat, int state)
{
	pthread_mutex_lock(&pusr_dat->ffmpegStateLock);
	pusr_dat->state = state;
	pthread_mutex_unlock(&pusr_dat->ffmpegStateLock);
	printf( "set_ffmpeg_show_state = %d\n", state) ;		
}

int get_ffmpeg_show_state(ffmpegRun_t* pusr_dat)
{
	int state;
	pthread_mutex_lock(&pusr_dat->ffmpegStateLock);
	state = pusr_dat->state;
	pthread_mutex_unlock(&pusr_dat->ffmpegStateLock);
	return state;
}


int task_ffmpegshow_init( void* arg )
{
	one_tiny_task_t *ptiny_task = (one_tiny_task_t*)arg;
	ffmpegRun_t *pusr_dat = (ffmpegRun_t*)ptiny_task->pusr_dat;	
	int ret;

	printf("\n=================begin to avformat_open_input: [%s]...====pusr_dat->videostream=%d\n", pusr_dat->rtsp_url,pusr_dat->videostream );

	//pusr_dat->ic = avformat_alloc_context();
	pusr_dat->ic=NULL;
	//pusr_dat
	#if 1
	AVDictionary* options = NULL;
	
	//if(options==NULL)
	{
		av_dict_set(&options, "buffer_size", 	"204800", 0); 	//设置缓存大小，1080p可将值调大
		av_dict_set(&options, "rtsp_transport", "tcp", 0); 		//以tcp方式打开
		av_dict_set(&options, "stimeout", 		"2000000", 0); 	//设置超时断开连接时间，单位微秒
		av_dict_set(&options, "max_delay", 		"500000", 0); 	//设置最大时延
	}
	#endif
	ret = avformat_open_input(&pusr_dat->ic, pusr_dat->rtsp_url, NULL, &options);
	if (ret < 0) 
	{
		printf("\n=================avformat_open_input ERR once========\n");
		pusr_dat->ic=NULL;
		ret = avformat_open_input(&pusr_dat->ic, pusr_dat->rtsp_url, NULL, &options);
		if (ret < 0) 
		{
			char data[1];
			data[0] = pusr_dat->videoCH;			
			printf("cannot open %s ret=%d\n", pusr_dat->rtsp_url, ret);
			api_mon_req_result(pusr_dat->videoCH, "open ipc rtsp err!");
			return -1;
		}
	}
	printf("\n=================avformat_open_input successful!!!========\n");

	#if 0

	pusr_dat->ic->probesize = 1 *1024;
	pusr_dat->ic->max_analyze_duration = 1 * AV_TIME_BASE;	

	printf("\n=================begin to av_find_stream_info...========\n");

	ret = avformat_find_stream_info(pusr_dat->ic,NULL ); //&options);	 // 函数默认需要花费较长的时间进行流格式探测, 如何减少探测时间内？ 可以通过设置AVFotmatContext的probesize和max_analyze_duration属性进行调节
	if (ret < 0) 
	{
		printf( "%s: could not find codec parameters\n", pusr_dat->filename);
		return -1;
	}

	///////////////////////////////////////////////////////
	// 查看有几路流媒体
	pusr_dat->videostream = -1;
	// 查看有几路流媒体
	printf(" total stream numbers=%d \n",pusr_dat->ic->nb_streams);
	int i;
	for( i = 0; i < pusr_dat->ic->nb_streams; i++ )
	{
		// 检测为视频的流媒体类型
		if(pusr_dat->ic->streams[i]->codec->codec_type == AVMEDIA_TYPE_VIDEO)
		{
			pusr_dat->videostream = i;
			break;
		}
	}
	printf(" video stream number=%d \n",pusr_dat->videostream);

	switch( pusr_dat->ic->streams[pusr_dat->videostream]->codecpar->codec_id )
	{
	  case AV_CODEC_ID_H264:
		  pusr_dat->videotype = 0;		 
		  pusr_dat->width = pusr_dat->ic->streams[pusr_dat->videostream]->codecpar->width;		 
		  pusr_dat->height = pusr_dat->ic->streams[pusr_dat->videostream]->codecpar->height;		 	  
		  printf("It is H264 Video width=%d, height=%d\n", pusr_dat->width, pusr_dat->height);
		  break;
	  case AV_CODEC_ID_H265:
		  pusr_dat->videotype = 1;		  
		  pusr_dat->width = pusr_dat->ic->streams[pusr_dat->videostream]->codecpar->width;		 
		  pusr_dat->height = pusr_dat->ic->streams[pusr_dat->videostream]->codecpar->height;		 	  
		  printf("It is H265 Video width=%d, height=%d\n", pusr_dat->width, pusr_dat->height);
		  break;
	  default:
		  printf("It is unknown Video\n");
		  pusr_dat->videotype = 0; // default as H264		  
		  break;
	}
	#endif
	return 0;
	
}

// 线程循环处理
//#define FRAME_BUFFER_LEN	(300*1024)
int task_ffmpegshow_process( void* arg )
{
    int i,k, ret;
    unsigned char *byte_buffer=NULL;
    int byte_buffer_size = 0;
	one_tiny_task_t *ptiny_task = (one_tiny_task_t*)arg;
	ffmpegRun_t *pusr_dat = (ffmpegRun_t*)ptiny_task->pusr_dat;	


	ret = 0;

    AVPacket pkt;
	int64_t pts_last;
	struct timeval start, end;
	int time_use;
	int unkown_frame=0;

	set_ffmpeg_show_state(pusr_dat, 2); // run flag;
	printf("\n=================task_ffmpegshow_process...========\n");
	if(pusr_dat->data_buff==NULL)
		pusr_dat->data_buff=malloc(pusr_dat->data_buff_len);
	av_init_packet(&pkt);
    while( one_tiny_task_is_running(ptiny_task) )
	{		
		if( get_ffmpeg_show_state(pusr_dat) == 2)
		{
			//memset(&pkt, 0, sizeof(pkt));
			gettimeofday( &start, NULL );
			pusr_dat->debug=0;
			ret= av_read_frame(pusr_dat->ic, &pkt);
			pusr_dat->debug=1;
			gettimeofday( &end, NULL );
			time_use = (end.tv_sec - start.tv_sec) * 1000000 + (end.tv_usec - start.tv_usec);
			if(ret<0 || time_use > 2000000)// 
			{
				api_mon_req_result(pusr_dat->videoCH, "ipc read offline!");
				pusr_dat->debug=2;
				printf("task_ffmpegshow_process av_read_frame ret=%d time_use=%d\n", ret, time_use);
				if( get_ffmpeg_show_state(pusr_dat) == 2)
					set_ffmpeg_show_state(pusr_dat, 3); // restart flag;
				pusr_dat->debug=3;
			}
			else
			{
			#if  1
				pusr_dat->debug=4;
				//if( pkt.stream_index == pusr_dat->videostream )
				if( pkt.stream_index == 0)
				{
					byte_buffer_size=0;
					if(pusr_dat->data_buff_len<(pkt.size+1))
					{
						free(pusr_dat->data_buff);
						pusr_dat->data_buff=NULL;
						while(pusr_dat->data_buff_len<(pkt.size+1))
						{
							pusr_dat->data_buff_len+=512;
						}
						pusr_dat->data_buff=malloc(pusr_dat->data_buff_len);
					}
					//byte_buffer = realloc(byte_buffer, pkt.size + 1);
					byte_buffer=pusr_dat->data_buff;
					// 若数据包开始为00 00 01则补充为00 00 00 01
					if( pkt.data[0]==0x00 && pkt.data[1]==0x00 && pkt.data[2]==0x01 )
					{
						byte_buffer[byte_buffer_size] = 0x00;
						byte_buffer_size++;
						//printf("pkt.data[0] filled 0...\n");						
					}
					memcpy( byte_buffer+byte_buffer_size, pkt.data, pkt.size );
					byte_buffer_size += pkt.size;
	
					//linphone 监视 IPC 
					if(pusr_dat->rtpFlag)
					{
					
						send_ipc_rtp_enc_data(pusr_dat ->rtp_send_list_ch,byte_buffer,byte_buffer_size,pusr_dat->videotype);
						//rtp_sender_send_with_ts(pkt.data, pkt.size);
						//rtp_sender_send_with_ts_wan( byte_buffer,byte_buffer_size,0,pusr_dat->videotype);
						//rtp_sender_send_with_ts_uni( byte_buffer,byte_buffer_size,0,pusr_dat->videotype);
						byte_buffer_size = 0;
						//printf("IPC rtp_sender_send_with_ts_wan\n");
					}
					if(pusr_dat->showFlag)
					{
						//printf("11111111111111pusr_dat->videotype=%d\n",pusr_dat->videotype);
						#if 1
						if(pusr_dat->videotype==0)
						{
							if(h264_data_check(byte_buffer,byte_buffer_size)!=0)
							{
								if(++unkown_frame<3)
									log_w("have h264 unkown_frame,len=%d",byte_buffer_size);
								continue;
								
							}
						}
						else
						{
							if(h265_data_check(byte_buffer,byte_buffer_size)!=0)
							{
								if(++unkown_frame<3)
									log_w("have h265 unkown_frame,len=%d",byte_buffer_size);
								continue;
								
							}
						}
						#endif
						pusr_dat->debug=5;
						pusr_dat->data_len=byte_buffer_size;
						(*pusr_dat->dat_process)(byte_buffer,byte_buffer_size);
						//API_Recording_mux( byte_buffer,byte_buffer_size, pusr_dat->videoCH, pusr_dat->videotype );
						byte_buffer_size = 0;
						pusr_dat->debug=6;
					}
	
					
					if(pusr_dat->recFlag)
					{
						pkt.pts = 90000/pusr_dat->fps + pts_last;
						pkt.dts = pkt.pts;
						pts_last = pkt.pts;
						ret = av_interleaved_write_frame(pusr_dat->out_ic, &pkt);
						//printf("av_interleaved_write_frame ret=%d\n", ret);
						if (ret < 0) 
						{
							printf("write_frame ERR=%d\n", ret);
	
						}
					}
				}
			#endif
			} 
			av_packet_unref(&pkt);
			//av_free(pkt.data);
		}
		else if( get_ffmpeg_show_state(pusr_dat) == 3)
		{
			api_restart_ipc_show(pusr_dat->videoCH);
		}
        
		usleep(1*1000);
    }
	#if 0
	if(byte_buffer != NULL)
	{
		free(byte_buffer);
		byte_buffer = NULL;
	}
	#endif
    return 0;
}

int api_start_ipc_rec(int ffmpe_num)
{
	ffmpegRun_t* pusr_dat = &ffmpegshow[ffmpe_num];

	char recfile[100];
	char temp[50];
	time_t t;
	struct tm *tblock; 	
	t = time(NULL); 
	tblock=localtime(&t);
	sprintf( temp,"20%02d%02d%02d_%02d%02d%02d-CH%d.mp4",tblock->tm_year-100,tblock->tm_mon+1,tblock->tm_mday,tblock->tm_hour,tblock->tm_min,tblock->tm_sec,ffmpe_num+1);
	snprintf(recfile,100,"/mnt/sdcard/video/%s",temp);
	
	int ret;
	ret = avformat_alloc_output_context2(&pusr_dat->out_ic, NULL, NULL, recfile);
	if (ret < 0) 
	{
		printf( "Could not create output context\n");
		return -1;
    }
	printf("videoch=%d Videotype=%d width=%d, height=%d\n", ffmpe_num, pusr_dat->videotype, pusr_dat->width, pusr_dat->height);
	AVStream *in_stream = pusr_dat->ic->streams[pusr_dat->videostream];
	AVStream *out_stream = avformat_new_stream(pusr_dat->out_ic, NULL);
	avcodec_parameters_copy(out_stream->codecpar, in_stream->codecpar);
	ret = avio_open(&pusr_dat->out_ic->pb, recfile, AVIO_FLAG_WRITE);
	if (ret < 0) 
	{
		printf("Could not open output file %s\n", recfile);
		return -1;
	}
	ret = avformat_write_header(pusr_dat->out_ic, NULL);
	if (ret < 0) 
	{
        printf("Error when avformat_write_header\n");
		return -1;
	}
	pusr_dat->recFlag = 1;
	pusr_dat->fps = 15;
	printf( "api_start_ipc_rec ch[%d] success!\n", ffmpe_num);
	return 0;
}

int api_stop_ipc_rec(int ffmpe_num)
{
	ffmpegRun_t* pusr_dat = &ffmpegshow[ffmpe_num];
	if(pusr_dat->recFlag)
	{
		av_write_trailer(pusr_dat->out_ic);
		avio_closep(&pusr_dat->out_ic->pb);
		avformat_free_context(pusr_dat->out_ic);
		pusr_dat->recFlag = 0;
		printf( "api_stop_ipc_rec ch[%d] !\n", ffmpe_num);
	}
	return 0;
}

int api_restart_ipc_show(int ffmpe_num)
{
	int ret;
	ffmpegRun_t* pusr_dat = &ffmpegshow[ffmpe_num];

	printf("==api_restart_ipc_show ins[%d] \n",pusr_dat->videoCH);	
	#if 1
    avformat_close_input(&pusr_dat->ic);
	pusr_dat->ic=NULL;
	//avformat_free_context(pusr_dat->ic);
	//pusr_dat->ic = avformat_alloc_context();
	AVDictionary* options = NULL;
	av_dict_set(&options, "buffer_size", 	"204800", 0); 	//设置缓存大小，1080p可将值调大
	av_dict_set(&options, "rtsp_transport", "tcp", 0); 		//以tcp方式打开
	av_dict_set(&options, "stimeout", 		"2000000", 0); 	//设置超时断开连接时间，单位微秒
	av_dict_set(&options, "max_delay", 		"500000", 0); 	//设置最大时延
	ret = avformat_open_input(&pusr_dat->ic, pusr_dat->rtsp_url, NULL, &options);
	if (ret < 0) 
	{
		ret = avformat_open_input(&pusr_dat->ic, pusr_dat->rtsp_url, NULL, &options);
		if (ret < 0) 
		{
			printf("cannot open %s ret=%d\n", pusr_dat->rtsp_url, ret);
			set_ffmpeg_show_state(pusr_dat, 1);
			api_mon_req_result(pusr_dat->videoCH, "open ipc restart err!");
			return -1;
		}
	}
	#endif
	set_ffmpeg_show_state(pusr_dat, 2);
	return 0;

}

// 退出该线程的处理: 内存和资源的回收释放
int task_ffmpegshow_exit( void* arg )
{
	one_tiny_task_t *ptiny_task = (one_tiny_task_t*)arg;
	ffmpegRun_t *pusr_dat = (ffmpegRun_t*)ptiny_task->pusr_dat;	
    avformat_close_input(&pusr_dat->ic);
	//avformat_free_context(pusr_dat->ic);
	if(pusr_dat->recFlag)
	{
		av_write_trailer(pusr_dat->out_ic);
		avio_closep(&pusr_dat->out_ic->pb);
		avformat_free_context(pusr_dat->out_ic);
		pusr_dat->recFlag = 0;
	}
	printf( "--task_ffmpegshow_exit ok!!!\n"); 
	return 0;
}
/////////////////////////////////////////////////////////////////////////////////////////////////
int api_start_ffmpegshow(int ffmpe_num, unsigned char* rtsp_url, int vdtype, cb_ffmpeg_proc proc )
{
	ffmpegRun_t* pins = &ffmpegshow[ffmpe_num];
	int state = get_ffmpeg_show_state(pins);
	if( state != 0)
	{
		printf( "------------api_start_ffmpegshow fail--state=%d--------------\n", state);	
		return -1;
	}
	pins->videoCH = ffmpe_num;
	pins->videotype = vdtype;
	pins->showFlag = 1;
	pins->recFlag = 0;
	pins->rtpFlag= 0;
	pins->dat_process 	= proc;
	strcpy( pins->rtsp_url, rtsp_url );
	set_ffmpeg_show_state(pins, 1);

	if( one_tiny_task_start2( &pins->task_process, task_ffmpegshow_init, task_ffmpegshow_process, task_ffmpegshow_exit, (void*)pins ) == 0 )
	{
		return 0;
 	}
	else
	{
		return -1;
	}
}

int api_stop_ffmpegshow(int ffmpe_num)
{
	ffmpegRun_t* pins = &ffmpegshow[ffmpe_num];
	int state = get_ffmpeg_show_state(pins);
	
	printf("api_stop_ffmpegshow ins=[%d] state=%d !!\n", ffmpe_num, state);
	if( state != 0)
	{
		one_tiny_task_stop2( &pins->task_process );

		set_ffmpeg_show_state(pins, 0);
	}
	return 0;	
}


// 线程启动前的初始化
int Get_ffmpegshow_videoType( char* rtsp_url, int* w, int* h)
{
	ffmpegRun_t ffmpeg_dat;
	ffmpegRun_t *pusr_dat = (ffmpegRun_t*)&ffmpeg_dat;	
	int ret;

	strcpy(ffmpeg_dat.rtsp_url, rtsp_url);
	AVDictionary* options = NULL;
	av_dict_set(&options, "buffer_size", 	"204800", 0); 	//设置缓存大小，1080p可将值调大
	av_dict_set(&options, "rtsp_transport", "tcp", 0); 		//以udp方式打开，如果以tcp方式打开将udp替换为tcp
	av_dict_set(&options, "stimeout", 		"2000000", 0); 	//设置超时断开连接时间，单位微秒
	av_dict_set(&options, "max_delay", 		"500000", 0); 	//设置最大时延
	
	pusr_dat->ic = avformat_alloc_context();
	
	printf("\n=================begin to avformat_open_input: [%s]========\n", pusr_dat->rtsp_url );
	ret = avformat_open_input(&pusr_dat->ic, pusr_dat->rtsp_url, NULL, &options);
	if (ret < 0) 
	{
		ret = avformat_open_input(&pusr_dat->ic, pusr_dat->rtsp_url, NULL, &options);
		if (ret < 0) 
		{
			printf("cannot open %s\n", pusr_dat->rtsp_url);
			return -1;
		}
	}
	
	printf("\n=================begin to av_find_stream_info...========\n");
	pusr_dat->ic->probesize = 10 *1024;
	pusr_dat->ic->max_analyze_duration = 1 * AV_TIME_BASE;	
	ret = avformat_find_stream_info(pusr_dat->ic,NULL ); //&options);	 // 函数默认需要花费较长的时间进行流格式探测, 如何减少探测时间内？ 可以通过设置AVFotmatContext的probesize和max_analyze_duration属性进行调节
	if (ret < 0) 
	{
		printf( "%s: could not find codec parameters\n", pusr_dat->rtsp_url);
		return -1;
	}

	///////////////////////////////////////////////////////

	int i;
	for( i = 0; i < pusr_dat->ic->nb_streams; i++ )
	{
		// 检测为视频的流媒体类型
		if(pusr_dat->ic->streams[i]->codec->codec_type == AVMEDIA_TYPE_VIDEO)
		{
			*w = pusr_dat->ic->streams[i]->codecpar->width; 	   
			*h = pusr_dat->ic->streams[i]->codecpar->height;			
			pusr_dat->videotype = pusr_dat->ic->streams[i]->codecpar->codec_id == AV_CODEC_ID_H265 ? 1 : 0;
			break;
		}
	}
	printf("Videotype=%d width=%d, height=%d\n", pusr_dat->videotype, *w, *h);
    avformat_close_input(&pusr_dat->ic);
	return pusr_dat->videotype;
}

int GetRegisterState(void)
{
	return av_register_init;
}
int SetRegisterState(int state)
{
	av_register_init = state;
}



int cb_show_buf1( char* pdatbuf, int datlen )
{
	return api_monitor_show_buf( 0, pdatbuf, datlen );
}
int cb_show_buf2( char* pdatbuf, int datlen )
{
	return api_monitor_show_buf( 1, pdatbuf, datlen );
}
int cb_show_buf3( char* pdatbuf, int datlen )
{
	return api_monitor_show_buf( 2, pdatbuf, datlen );
}
int cb_show_buf4( char* pdatbuf, int datlen )
{
	return api_monitor_show_buf( 3, pdatbuf, datlen );
}


cb_ffmpeg_proc ipc_cb[4] =
{
	cb_show_buf1,
	cb_show_buf2,
	cb_show_buf3,		
	cb_show_buf4,
};


int api_ipc_show_one_stream( int ins_num, unsigned char* rtsp, int width ,int height, int vdtype)
{
	//printf("++++++++++++111111111\n");
	if(api_monitor_show_start(ins_num, width, height==1080? 1088 :height, vdtype)!=0)
		return -1;
	//printf("++++++++++++22222222222222\n");
	return api_start_ffmpegshow(ins_num, rtsp, vdtype, ipc_cb[ins_num] );
	#if 0

	if( vdtype == -1 ) vdtype = 0;

	printf("api_ipc_show_one_stream: width[%d],height[%d],vdtype[%d]-----\n", width, height, vdtype);

	api_start_ffmpegshow(ins_num, rtsp, vdtype, ipc_cb[ins_num] );

	//return	api_h264_show_start(ins_num, width, height, 0, 0, 0, 0, 720, 480, vdtype);
	
	#endif		
}



// 3.2 应用接口：停止流媒体显示接口
int api_ipc_stop_mointor_one_stream(int ins_num)
{
	printf("1111111111111:%s:%d\n",__func__,__LINE__);
	api_stop_ffmpegshow(ins_num);
	printf("1111111111111:%s:%d\n",__func__,__LINE__);
	api_monitor_show_stop(ins_num);
	printf("1111111111111:%s:%d\n",__func__,__LINE__);
	Clear_ds_show_layer(ins_num);
	printf("1111111111111:%s:%d\n",__func__,__LINE__);
	return 0;
}
void ipc_show_debug(void)
{
	int i;
	for(i=0;i<5;i++)
	{
		ShellCmdPrintf("ipc[%d]_show_debug:state[%d],debug[%d],data_len[%d],rtp=%d,show=%d,rec=%d",i,ffmpegshow[i].state,ffmpegshow[i].debug,ffmpegshow[i].data_len,ffmpegshow[i].recFlag,ffmpegshow[i].showFlag,ffmpegshow[i].rtp_send_list_ch);
	}
}
////////////////////////////////
int api_start_ffmpeg_rtp(int rtp_num, unsigned char* rtsp_url, int vdtype, int rtp_send_list_ch )
{
	int ffmpe_num=4+rtp_num;
	ffmpegRun_t* pins = &ffmpegshow[ffmpe_num];
	int state = get_ffmpeg_show_state(pins);
	if( state != 0)
	{
		printf( "------------api_start_ffmpegshow fail--state=%d--------------\n", state);	
		return -1;
	}
	pins->videoCH = ffmpe_num;
	pins->videotype = vdtype;
	pins->showFlag = 0;
	pins->recFlag = 0;
	pins->rtpFlag= 1;
	pins->rtp_send_list_ch=rtp_send_list_ch;
	pins->dat_process = NULL;
	strcpy( pins->rtsp_url, rtsp_url );
	set_ffmpeg_show_state(pins, 1);

	if( one_tiny_task_start2( &pins->task_process, task_ffmpegshow_init, task_ffmpegshow_process, task_ffmpegshow_exit, (void*)pins ) == 0 )
	{
		return 0;
 	}
	else
	{
		return -1;
	}
}

int api_stop_ffmpeg_rtp(int rtp_num)
{
	int ffmpe_num=4+rtp_num;
	ffmpegRun_t* pins = &ffmpegshow[ffmpe_num];
	int state = get_ffmpeg_show_state(pins);
	
	printf("api_stop_ffmpegshow ins=[%d] state=%d !!\n", ffmpe_num, state);
	if( state != 0)
	{
		one_tiny_task_stop2( &pins->task_process );

		set_ffmpeg_show_state(pins, 0);
	}
	return 0;	
}