#ifndef _OBJ_IPC_CLIENT_H_
#define _OBJ_IPC_CLIENT_H_
#include "cJSON.h"


int Start_OneIpc_Show(int ins_num, cJSON * target, cJSON * win, int time);
int Stop_OneIpc_Show( int ins_num );
int api_OneIpc_Rtsp_Show(int ins_num, unsigned char* rtsp, int width ,int height, int vdtype, cJSON * win);


#endif
