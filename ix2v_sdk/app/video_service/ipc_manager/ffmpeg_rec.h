
#ifndef _FFMPEG_REC_H_
#define _FFMPEG_REC_H_

#include "one_tiny_task.h"
//#include "task_VideoMenu.h"

#include "libavutil/common.h"
#include "libavformat/avformat.h"
#include <libavutil/timestamp.h>
#include "libavutil/avutil.h"
#include "libswscale/swscale.h"
#include "libavcodec/avcodec.h"

typedef  void (*ffmpegRecCallBack)(void *pin);

typedef struct
{
	int					state;		// 0:idle, 1:running
	char				rtsp_url[240];
	char				out_file[100];
	// attribute
	int					width;
	int					height;
	int					videotype;		// 0:h264, 1:h265
	int					fps;
	int 				videostream;	
	int					videoCH;		// ͨ����
	int					recOnly;		// ��¼���־
	int 				TotalSec;
	int 				CurSec;
	int 				fileSplitCnt;
	int 				fileSplit;

	AVFormatContext 	*ifmt_ctx;
	AVFormatContext 	*ofmt_ctx;
	one_tiny_task_t 	task_process;	
	ffmpegRecCallBack 	CallBack;	
} ffmpeg_rec_dat_t;

typedef enum
{
	 RECORD_IDLE,
	 RECORD_START,
	 RECORD_RUN,
}
RECORD_STATE;

typedef struct
{
	RECORD_STATE		state;		// 0:idle, 1:running
	pthread_mutex_t 	recStateLock;
	char				out_file[100];
	int					width;
	int					height;
	int					videotype;		// 0:h264, 1:h265
	int					fps;
	int 				TotalSec;
	int 				CurSec;
	AVFormatContext 	*ofmt_ctx;
	ffmpegRecCallBack 	CallBack;	
	AVStream *out_stream;
} ffmpeg_rec_dat2;

#endif



