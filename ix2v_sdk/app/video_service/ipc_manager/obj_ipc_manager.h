#ifndef _OBJ_IPC_MANAGER_H_
#define _OBJ_IPC_MANAGER_H_

#include "cJSON.h"
#define	TOTAL_IPC_DEVICES		100

typedef struct
{
	char	url[100];	
	char	name[100];
} ipc_device_node;

typedef struct
{
	int		all;
	ipc_device_node	dat[TOTAL_IPC_DEVICES];
} ipc_device_list_t;

typedef struct
{
	char 	rtsp_url[200];
	int	 	width;
	int	 	height;
	int  	bitrate;	//码率
	int		framerate;	//帧率
	int 	vd_type;	//编码格式
}login_dat_t;

typedef struct 
{
	int	 	network;
	char	IP[40];
	char	ID[40];
	char	NAME[40];
	char	USER[40];
	char	PWD[40];
	int		CH_ALL;
	int		CH_REC;
	int		CH_FULL;
	int		CH_QUAD;
	int		CH_APP;
	login_dat_t	CH_DAT[4];	
} IPC_ONE_DEVICE;

typedef struct
{
	int		ch_cnt;	//通道数
	login_dat_t	dat[4];	
} onvif_login_info_t;


int IpcDeviceDiscovery(int timeout, char* network, cJSON* result);
int IpcCheckOnline(const char* device_url);
int IpcDeviceLogin(cJSON* jsonIn, cJSON* result);

int GetIpcNum(void);
int AddOrModifyOneIpc(IPC_ONE_DEVICE* record);
int CheckIpcExist(char* ip);
int GetIpcRecord(char* network, cJSON* view);
int GetIpcChdat(char* ip, cJSON* info);
int GetOneIpcByIpOrName(char* ip, char* name, IPC_ONE_DEVICE* ipcRecord);
int DeleteOneIpcByIpOrName(char* ip, char* name);

#endif
