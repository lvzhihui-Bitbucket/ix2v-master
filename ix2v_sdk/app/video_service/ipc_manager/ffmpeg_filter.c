#include <stdio.h>
#include "cJSON.h"
#include "libavcodec/avcodec.h"
#include "libavformat/avformat.h"
#include "libavfilter/avfilter.h"
#include "libavfilter/avfiltergraph.h"
#include "libavfilter/buffersink.h"
#include "libavfilter/buffersrc.h"
#include "libavutil/avutil.h"
#include "libswscale/swscale.h"


//const char *filter_descr = "lutyuv='u=128:v=128'";
//const char *filter_descr = "boxblur";
//const char *filter_descr = "hflip";
//const char *filter_descr = "hue='h=60:s=-3'";
//const char *filter_descr = "crop=in_w/2:in_h";
//const char *filter_descr = "drawbox=x=100:y=100:w=100:h=100:color=pink@0.5";
//const char *filter_descr = "drawtext=fontfile=arial.ttf:fontcolor=green:fontsize=30:text='Lei Xiaohua'";
//const char *filter_descr = "movie=my_logo.png[wm];[in][wm]overlay=5:5[out]";

static AVFormatContext *iFormatCtx=NULL;
static AVFormatContext *oFormatCtx=NULL;
static AVCodecContext *decodeCtx=NULL;
static AVCodecContext *encodeCtx=NULL;
AVFilterContext *buffersink_ctx;
AVFilterContext *buffersrc_ctx;
AVFilterGraph *filter_graph;
static int video_stream_index = -1;

static int open_input_file(const char *filename)
{
    int ret;
    AVCodec *dec;
	AVStream *stream;

	printf("begin to avformat_open_input: [%s]...\n", filename );
    if ((ret = avformat_open_input(&iFormatCtx, filename, NULL, NULL)) < 0) 
	{
        printf( "Cannot open input file\n");
        return ret;
    }

    if ((ret = avformat_find_stream_info(iFormatCtx, NULL)) < 0) 
	{
        printf( "Cannot find stream information\n");
        return ret;
    }
	#if 0
    /* select the video stream */
    ret = av_find_best_stream(iFormatCtx, AVMEDIA_TYPE_VIDEO, -1, -1, &dec, 0);
    if (ret < 0) 
	{
        printf( "Cannot find a video stream in the input file\n");
        return ret;
    }
    video_stream_index = ret;
    decodeCtx = iFormatCtx->streams[video_stream_index]->codec;
	#endif
	int i;
	for( i = 0; i < iFormatCtx->nb_streams; i++ )
	{
		// 检测为视频的流媒体类型
		if(iFormatCtx->streams[i]->codec->codec_type == AVMEDIA_TYPE_VIDEO)
		{
			video_stream_index = i;
			break;
		}
	}
	if (video_stream_index < 0) 
	{
        printf( "Cannot find a video stream in the input file\n");
        return -1;
    }
	stream = iFormatCtx->streams[video_stream_index];
	dec = avcodec_find_decoder(stream->codecpar->codec_id);
	decodeCtx = avcodec_alloc_context3(dec);
	if (!decodeCtx) 
	{
        printf( "Failed to allocate the decoder context\n");
		return -1;
	}
	ret = avcodec_parameters_to_context(decodeCtx, stream->codecpar);
	if (ret < 0) 
	{
        printf( "Failed to copy decoder parameters to input decoder context\n");
		return ret;
	}
	decodeCtx->framerate = av_guess_frame_rate(iFormatCtx, stream, NULL);

	ShellCmdPrintf("open_input_file video_size=%dx%d:pix_fmt=%d framerate=%d/%d\n", decodeCtx->width, decodeCtx->height, decodeCtx->pix_fmt, decodeCtx->framerate.num, decodeCtx->framerate.den);
    if (decodeCtx -> codec_id == AV_CODEC_ID_H265)
	{
		printf( "video_stream is h265!\n");
		//av_dict_set(&param, "preset", "slow", 0);
		//av_dict_set(&param, "tune", "zerolatency", 0);
	}
	else
	{
		printf( "video_stream is h264!\n");
		//av_opt_set(encodeCtx->priv_data, "preset", "slow", 0);
		//av_dict_set(&param, "preset", "slow", 0);
		//av_dict_set(&param, "tune", "zerolatency", 0);
	}	
    /* init the video decoder */
    if ((ret = avcodec_open2(decodeCtx, dec, NULL)) < 0) 
	{
        printf( "Cannot open video decoder\n");
        return ret;
    }

    return 0;
}

static int open_output_file(const char *filename)
{
    int ret;
    AVCodec *encoder;
	//AVDictionary *param = 0;
    ret = avformat_alloc_output_context2(&oFormatCtx, NULL, NULL, filename);
    if (ret < 0)  
	{
        printf( "Could not create output context\n");
        return -1;
    }
	AVStream *out_stream = avformat_new_stream(oFormatCtx, NULL);
	
	
	encoder = avcodec_find_encoder(AV_CODEC_ID_MPEG4);//decodeCtx->codec_id
	if (!encoder) 
	{
        printf( "Necessary encoder not found\n");
		return -1;
	}

	#if 1
	encodeCtx = avcodec_alloc_context3(encoder);
	if (!encodeCtx) 
	{
        printf("Could not allocate video codec context\n");
        return -1;
    }
	#endif
    //encodeCtx = iFormatCtx->streams[video_stream_index]->codec;
	#if 1
	encodeCtx->codec_id = AV_CODEC_ID_MPEG4;
	encodeCtx->codec_type = AVMEDIA_TYPE_VIDEO;
	encodeCtx->pix_fmt = AV_PIX_FMT_YUV420P;
	encodeCtx->width = decodeCtx->width/2;
	encodeCtx->height = decodeCtx->height;
	encodeCtx->sample_aspect_ratio = decodeCtx->sample_aspect_ratio;
	encodeCtx->time_base = av_inv_q(decodeCtx->framerate);
	//encodeCtx->time_base.num = 1;  
	//encodeCtx->time_base.den = 15;  
	//encodeCtx->bit_rate= 400000; 
	//encodeCtx->gop_size = 12;
	//encodeCtx->qmin = 10;
	//encodeCtx->qmax = 51;
	#endif
	ShellCmdPrintf("encodeCtx video_size=%dx%d:pix_fmt=%d time_base=%d/%d\n", encodeCtx->width, encodeCtx->height, encodeCtx->pix_fmt, encodeCtx->time_base.num, encodeCtx->time_base.den);

	
	if (oFormatCtx->oformat->flags & AVFMT_GLOBALHEADER)
        encodeCtx->flags |= AV_CODEC_FLAG_GLOBAL_HEADER;

	ret = avcodec_open2(encodeCtx, encoder, NULL);//&param
	if (ret < 0) 
	{
		printf( "Cannot open video encoder ret=%d\n", ret);
		return ret;
	}

	
	ret = avcodec_parameters_from_context(out_stream->codecpar, encodeCtx);
	if (ret < 0) 
	{
		printf( "Failed to copy encoder parameters to output stream\n");
		return ret;
	}
	out_stream->time_base = encodeCtx->time_base;

    if (!(oFormatCtx->oformat->flags & AVFMT_NOFILE)) {
        ret = avio_open(&oFormatCtx->pb, filename, AVIO_FLAG_WRITE);
        if (ret < 0) 
		{
            printf("Could not open output file %s\n", filename);
			return -1;
        }
    }

    /* init muxer, write output file header */
    ret = avformat_write_header(oFormatCtx, NULL);
    if (ret < 0) 
	{
        printf("Error occurred when opening output file\n");
		return -1;
    }

    return 0;
}

static int init_filters(const char *filters_descr, int w, int h)
{
	ShellCmdPrintf("init_filters=%s\n", filters_descr);
    char args[512];
    int ret;
    AVFilter *buffersrc  = avfilter_get_by_name("buffer");
    AVFilter *buffersink = avfilter_get_by_name("buffersink");
    AVFilterInOut *outputs = avfilter_inout_alloc();
    AVFilterInOut *inputs  = avfilter_inout_alloc();
    enum AVPixelFormat pix_fmts[] = { AV_PIX_FMT_YUV420P, AV_PIX_FMT_NONE };
    AVBufferSinkParams *buffersink_params;

    filter_graph = avfilter_graph_alloc();

    /* buffer video source: the decoded frames from the decoder will be inserted here. */
	if (decodeCtx)
	{
		snprintf(args, sizeof(args), "video_size=%dx%d:pix_fmt=%d:time_base=%d/%d:pixel_aspect=%d/%d",
			decodeCtx->width, decodeCtx->height, decodeCtx->pix_fmt,
			decodeCtx->time_base.num, decodeCtx->time_base.den,
			decodeCtx->sample_aspect_ratio.num, decodeCtx->sample_aspect_ratio.den);
	}
	else
	{
		snprintf(args, sizeof(args),"video_size=%dx%d:pix_fmt=%d:time_base=%d/%d:pixel_aspect=%d/%d",w,h,AV_PIX_FMT_YUV420P,1, 25,1,1);
	}

    ret = avfilter_graph_create_filter(&buffersrc_ctx, buffersrc, "in", args, NULL, filter_graph);
    if (ret < 0) 
	{
        printf("Cannot create buffer source\n");
        return ret;
    }

    /* buffer video sink: to terminate the filter chain. */
    buffersink_params = av_buffersink_params_alloc();
    buffersink_params->pixel_fmts = pix_fmts;
    ret = avfilter_graph_create_filter(&buffersink_ctx, buffersink, "out", NULL, buffersink_params, filter_graph);
    av_free(buffersink_params);
    if (ret < 0) 
	{
        printf("Cannot create buffer sink\n");
        return ret;
    }

    /* Endpoints for the filter graph. */
    outputs->name       = av_strdup("in");
    outputs->filter_ctx = buffersrc_ctx;
    outputs->pad_idx    = 0;
    outputs->next       = NULL;

    inputs->name       = av_strdup("out");
    inputs->filter_ctx = buffersink_ctx;
    inputs->pad_idx    = 0;
    inputs->next       = NULL;

    if ((ret = avfilter_graph_parse_ptr(filter_graph, filters_descr, &inputs, &outputs, NULL)) < 0)
        return ret;

    if ((ret = avfilter_graph_config(filter_graph, NULL)) < 0)
        return ret;
    return 0;
}

int ffmpeg_filter(char* infile, char* size_str, char* filter_str, char* outfile)
{
    int ret;
	char *ptr;
	char  buff[20];
	int in_width;
	int in_height;
	int yuvfile_in=0;
	int yuvfile_out=0;
	FILE *fp_in, *fp_out;

	if((ptr = strstr(size_str, "*")) ==NULL)
	{
		ShellCmdPrintf("Error size.\n");
		return -1;
	}
	memcpy(buff,size_str,ptr-size_str);
	buff[ptr-size_str]=0;
	in_width = atoi(buff);
	in_height = atoi(ptr+1);
	ShellCmdPrintf("input vd size w=%d h=%d\n", in_width, in_height);
	if(strstr(infile, ".yuv") != NULL)
	{
		yuvfile_in = 1;
		//Input YUV
		fp_in=fopen(infile,"rb+");
		if(fp_in==NULL)
		{
			ShellCmdPrintf("Error open input yuvfile.\n");
			return -1;
		}
	}
	else
	{
		if (open_input_file(infile) < 0)
		{
			ShellCmdPrintf("Error open input.\n");
			return -1;
		}
	}
	if(outfile)
	{
		if(strstr(outfile, ".yuv") != NULL)
		{
			yuvfile_out = 1;
			//Output YUV
			fp_out=fopen(outfile,"wb+");
			if(fp_out==NULL)
			{
				ShellCmdPrintf("Error open output file.\n");
				return -1;
			}
		}
	}
	
	if(init_filters(filter_str, in_width, in_height) < 0)
	{
		ShellCmdPrintf("Error init_filters.\n");
		goto end;
	}

    AVFrame *frame_in;
	AVFrame *frame_out;
	unsigned char *frame_buffer_in;
	unsigned char *frame_buffer_out;
	AVPacket packet;
	int got_frame;

	frame_in=av_frame_alloc();
	if(yuvfile_in)
	{
		frame_buffer_in=(unsigned char *)av_malloc(av_image_get_buffer_size(AV_PIX_FMT_YUV420P, in_width,in_height,1));
		av_image_fill_arrays(frame_in->data, frame_in->linesize,frame_buffer_in,AV_PIX_FMT_YUV420P,in_width, in_height,1);
		frame_in->width=in_width;
		frame_in->height=in_height;
		frame_in->format=AV_PIX_FMT_YUV420P;
	}

	frame_out=av_frame_alloc();
	if(yuvfile_out)
	{
		frame_buffer_out=(unsigned char *)av_malloc(av_image_get_buffer_size(AV_PIX_FMT_YUV420P, in_width,in_height,1));
		av_image_fill_arrays(frame_out->data, frame_out->linesize,frame_buffer_out,AV_PIX_FMT_YUV420P,in_width, in_height,1);
	}

	//monitor_menu_on();
	//Set_ds_show_pos(0,0,0,1024,600);

    while (1) 
	{
		if(yuvfile_in)
		{
			if(fread(frame_buffer_in, 1, in_width*in_height*3/2, fp_in)!= in_width*in_height*3/2)
			{
				break;
			}
			//show_one_frame_filter(in_width, in_height, frame_buffer_in);
			//input Y,U,V
			frame_in->data[0]=frame_buffer_in;
			frame_in->data[1]=frame_buffer_in+in_width*in_height;
			frame_in->data[2]=frame_buffer_in+in_width*in_height*5/4;

			if (av_buffersrc_add_frame(buffersrc_ctx, frame_in) < 0) 
			{
				ShellCmdPrintf( "Error while buffersrc add frame.\n");
				break;
			}

			/* pull filtered pictures from the filtergraph */
			ret = av_buffersink_get_frame(buffersink_ctx, frame_out);
			if (ret < 0)
			{
				ShellCmdPrintf( "Error while buffersink get frame.\n");
				break;
			}	

		}
		else
		{
			ret = av_read_frame(iFormatCtx, &packet);
			if (ret< 0)
            	break;
			if (packet.stream_index == video_stream_index)
			{
				 got_frame = 0;
				ret = avcodec_decode_video2(decodeCtx, frame_in, &got_frame, &packet);
				if (ret < 0) 
				{
					ShellCmdPrintf( "Error decoding video\n");
					break;
				}

				if (got_frame) 
				{
					ShellCmdPrintf("Get 1 frame!\n");
					frame_in->pts = av_frame_get_best_effort_timestamp(frame_in);
					if (av_buffersrc_add_frame(buffersrc_ctx, frame_in) < 0) 
					{
						ShellCmdPrintf( "Error while buffersrc add frame.\n");
						break;
					}	
					ret = av_buffersink_get_frame(buffersink_ctx, frame_out);
					if (ret < 0)
					{
						ShellCmdPrintf( "Error while buffersink get frame.\n");
						break;
					}	
					//show_one_frame_filter(in_width, in_height, frame_out->data[0]);
				}
			}
		}

        
		if(yuvfile_out)
		{
			//output Y,U,V
			if(frame_out->format==AV_PIX_FMT_YUV420P || frame_out->format==AV_PIX_FMT_YUVJ420P)
			{
				for(int i=0;i<frame_out->height;i++)
				{
					fwrite(frame_out->data[0]+frame_out->linesize[0]*i,1,frame_out->width,fp_out);
				}
				for(int i=0;i<frame_out->height/2;i++)
				{
					fwrite(frame_out->data[1]+frame_out->linesize[1]*i,1,frame_out->width/2,fp_out);
				}
				for(int i=0;i<frame_out->height/2;i++)
				{
					fwrite(frame_out->data[2]+frame_out->linesize[2]*i,1,frame_out->width/2,fp_out);
				}
				ShellCmdPrintf("Process 1 frame!\n");
			}
		}
		
		av_frame_unref(frame_out);
		if(!yuvfile_in)
		{
			av_frame_unref(frame_in);
			av_free_packet(&packet);
		}
			
    }
end:
	if(yuvfile_in)
		fclose(fp_in);
	if(yuvfile_out)
		fclose(fp_out);

	av_frame_free(&frame_in);
	av_frame_free(&frame_out);
    avfilter_graph_free(&filter_graph);
    if (decodeCtx)
        avcodec_close(decodeCtx);
	if (iFormatCtx)	
    	avformat_close_input(&iFormatCtx);

    return 0;
}

static int64_t pts_last = 0;
static int filter_encode_write_frame(AVFrame *frame_in, int fps)
{
	int ret;
    AVFrame *filt_frame;
	int got_frame;
    AVPacket enc_pkt;
	ShellCmdPrintf( "filter_encode_write_frame fps=%d\n", fps);
	if (av_buffersrc_add_frame(buffersrc_ctx, frame_in) < 0) 
	{
		ShellCmdPrintf( "Error while buffersrc add frame.\n");
		return -1;
	}	
	filt_frame = av_frame_alloc();
	if (!filt_frame) 
	{
		ret = -1;
		return -1;
	}
	ret = av_buffersink_get_frame(buffersink_ctx, filt_frame);
	if (ret < 0)
	{
		ShellCmdPrintf( "Error while buffersink get frame.\n");
		av_frame_free(&filt_frame);
		return -1;
	}		

	//filt_frame->pict_type = AV_PICTURE_TYPE_NONE;
	enc_pkt.data = NULL;
	enc_pkt.size = 0;
	av_init_packet(&enc_pkt);
	#if 0
	ret = avcodec_encode_video2(encodeCtx, &enc_pkt, filt_frame, &got_frame);
	if (ret < 0) 
	{
		ShellCmdPrintf( "Error encoding video ret=%d\n", ret);
		break;
	}
	#endif
	ret = avcodec_send_frame(encodeCtx, filt_frame);
	if (ret < 0) 
	{
		ShellCmdPrintf( "Error avcodec_send_frame ret=%d\n", ret);
		av_frame_free(&filt_frame);
		return -1;
	}
	ret = avcodec_receive_packet(encodeCtx, &enc_pkt);
	av_frame_free(&filt_frame);
	if (ret < 0) 
	{
		ShellCmdPrintf( "Error avcodec_receive_packet ret=%d\n", ret);
		return -1;
	}

	enc_pkt.stream_index = video_stream_index;
	av_packet_rescale_ts(&enc_pkt, encodeCtx->time_base, oFormatCtx->streams[video_stream_index]->time_base);
	//enc_pkt.pts = 90000/fps + pts_last;
	//enc_pkt.dts = enc_pkt.pts;
	//pts_last = enc_pkt.pts;
	ret = av_interleaved_write_frame(oFormatCtx, &enc_pkt);
	av_packet_unref(&enc_pkt);
	if (ret < 0) 
	{
		ShellCmdPrintf("write_frame ERR=%d\n", ret);
	}

    return ret;
}

int ffmpeg_rec(char* input, char* size_str, char* filter_str, char* outfile)
{
    int ret;
	char *ptr;
	char  buff[20];
	int fps;
	int sec;
	int m_frame_index = 0;
	int CurSec = 0;
	pts_last = 0;
	if((ptr = strstr(size_str, "@")) ==NULL)
	{
		ShellCmdPrintf("Error size.\n");
		return -1;
	}
	memcpy(buff,size_str,ptr-size_str);
	buff[ptr-size_str]=0;
	sec = atoi(buff);
	fps = atoi(ptr+1);
	if (open_input_file(input) < 0)
	{
		ShellCmdPrintf("Error open input.\n");
		return -1;
	}
	if(open_output_file(outfile) < 0)
	{
		ShellCmdPrintf("Error creat outfile.\n");
		goto end;
	}
	
	if(init_filters(filter_str, 0, 0) < 0)
	{
		ShellCmdPrintf("Error init_filters.\n");
		goto end;
	}

	AVPacket packet;
	int got_frame;
    AVFrame *frame_in = NULL;
	frame_in=av_frame_alloc();
    while (1) 
	{
		ret = av_read_frame(iFormatCtx, &packet);
		if (ret< 0)
			break;
		if (packet.stream_index == video_stream_index)
		{
			got_frame = 0;
			ret = avcodec_decode_video2(decodeCtx, frame_in, &got_frame, &packet);
			if (ret < 0) 
			{
				ShellCmdPrintf( "Error decoding video\n");
				break;
			}
			if (got_frame) 
			{
				ShellCmdPrintf("Get 1 frame!\n");
				frame_in->pts = frame_in->best_effort_timestamp;
                ret = filter_encode_write_frame(frame_in, fps);
				if (ret < 0) 
				{
					ShellCmdPrintf( "Error filter_encode_write_frame\n");
					break;
				}
				if(m_frame_index < fps - 1)
				{
					m_frame_index++;
				}
				else
				{
					ShellCmdPrintf("m_frame_index=%d rec_time=%d\n",m_frame_index, CurSec);
					m_frame_index = 0;
					CurSec++;
				}
				if(CurSec >= sec)
				{
					ShellCmdPrintf( "ffmpeg_rec CurSec=%d\n", CurSec);
					break;
				}
			}
		}
		av_free_packet(&packet);	
    }
	av_write_trailer(oFormatCtx);
end:

	av_frame_free(&frame_in);
    avfilter_graph_free(&filter_graph);
    if (decodeCtx)
        avcodec_free_context(&decodeCtx);
	if (encodeCtx)
        avcodec_free_context(&encodeCtx);	
	if (iFormatCtx)	
    	avformat_close_input(&iFormatCtx);
    if (oFormatCtx && !(oFormatCtx->oformat->flags & AVFMT_NOFILE))
        avio_closep(&oFormatCtx->pb);
    avformat_free_context(oFormatCtx);

    return 0;
}

int Crop(char* srcBuffer, int srcWidth, int srcHeight, char* dstBuffer, int dstWidth, int dstHeight, int offsetX, int offsetY) 
{
    // 创建AVFrame
    AVFrame* srcFrame = av_frame_alloc();
    srcFrame->width = srcWidth;
    srcFrame->height = srcHeight;
    srcFrame->format = AV_PIX_FMT_NV12;
    // 填充AVFrame
    av_image_fill_arrays(srcFrame->data, srcFrame->linesize, srcBuffer, AV_PIX_FMT_NV12, srcWidth, srcHeight, 1);
    AVFrame* dstFrame = av_frame_alloc();
    // 进行crop
    AVFilterGraph* filterGraph = avfilter_graph_alloc();
    char args[512] = "";
    snprintf(args, sizeof(args),
        "buffer=video_size=%dx%d:pix_fmt=%d:time_base=1/1:pixel_aspect=1/1[in];" // Parsed_buffer_0
        "[in]crop=x=%d:y=%d:out_w=%d:out_h=%d[out];"                             // Parsed_crop_1
        "[out]buffersink",                                                       // Parsed_buffersink_2
        srcWidth, srcHeight, AV_PIX_FMT_NV12,
        offsetX, offsetY, dstWidth, dstHeight);
 
    AVFilterInOut* inputs = NULL;
    AVFilterInOut* outputs = NULL;
    avfilter_graph_parse2(filterGraph, args, &inputs, &outputs);
    avfilter_graph_config(filterGraph, NULL);
    AVFilterContext* srcFilterCtx = avfilter_graph_get_filter(filterGraph, "Parsed_buffer_0");
    AVFilterContext* sinkFilterCtx = avfilter_graph_get_filter(filterGraph, "Parsed_buffersink_2");
 
    dstFrame = av_frame_clone(srcFrame);
    av_buffersrc_add_frame(srcFilterCtx, dstFrame);
    av_buffersink_get_frame(sinkFilterCtx, dstFrame);
    avfilter_graph_free(&filterGraph);
 
    // 获取crop完成后的数据
    avpicture_layout((AVPicture*)dstFrame, AV_PIX_FMT_NV12, dstWidth, dstHeight, dstBuffer, avpicture_get_size(AV_PIX_FMT_NV12, dstWidth, dstHeight));
 
    av_frame_free(&srcFrame);
    av_frame_free(&dstFrame);
    return 0;
}


int ShellCmd_FFmpeg(cJSON *cmd)
{
	char* ctrlStr = GetShellCmdInputString(cmd, 1);
	char* infile = GetShellCmdInputString(cmd, 2);
	char* size_str = GetShellCmdInputString(cmd, 3);
	char* filter_str = GetShellCmdInputString(cmd, 4);
	char* outfile = GetShellCmdInputString(cmd, 5);
	if(ctrlStr == NULL)
	{
		ShellCmdPrintf("Input error!!\n");
	}
	else if(!strcmp(ctrlStr, "crop"))
	{
		char *ptr;
		char  buff[20];
		int in_width;
		int in_height;
		int size;
		char* src_data;
    	char* dest_data;
		FILE *fp_in = fopen(infile, "rb");
		FILE *fp_out = fopen(outfile, "wb+");
		if(fp_in== NULL)
		{
			return -1;
		}
		if((ptr = strstr(size_str, "*")) ==NULL)
		{
			ShellCmdPrintf("Error size.\n");
			return -1;
		}
		memcpy(buff,size_str,ptr-size_str);
		buff[ptr-size_str]=0;
		in_width = atoi(buff);
		in_height = atoi(ptr+1);
		size = in_width * in_height * 3 / 2;
		src_data = (char*)malloc(size);
		dest_data = (char*)malloc(size/4);
		if (src_data == NULL || dest_data == NULL)
		{
			ShellCmdPrintf("Failed to allocate memory.\n");
			return -1;
		}
		memset(src_data, 0, size);
    	int ret = fread(src_data, 1, size, fp_in); 
		ShellCmdPrintf("read inputfile len=%d.\n", ret);
		Crop(src_data, in_width, in_height, dest_data, in_width / 2, in_height / 2, 0, 0);

		fwrite(dest_data, size/4, 1, fp_out);

		free(src_data);
		free(dest_data);
		fclose(fp_in);
		fclose(fp_out);
	}
	else if(!strcmp(ctrlStr, "filter"))
	{
		if(infile && size_str && filter_str)
			ffmpeg_filter(infile, size_str, filter_str, outfile);
	}
	else if(!strcmp(ctrlStr, "rec"))
	{
		//if(infile && size_str && filter_str && outfile)
		//	ffmpeg_rec(infile, size_str, filter_str, outfile);
		if(size_str!=NULL)
			api_rec_one_file_start(0,0,infile, atoi(size_str),NULL);
		else
			api_rec_one_file_start(0,0,infile, 3,NULL);
	}
	else if(!strcmp(ctrlStr, "play"))
	{
		if(infile)
		{
			#if defined(PID_IX850)
			PlaybackMenu_init();
			#endif
			api_start_ffmpegPlay(infile, NULL);
		}	
	}
	else if(!strcmp(ctrlStr, "stop"))
	{
		ffmpeg_rec_exit();
		api_stop_ffmpegPlay();
	}
	else
	{
		ShellCmdPrintf("Input error!!\n");
	}
}