#include <stdio.h>
#include <sys/sysinfo.h>
#include "task_Shell.h"
#include "task_survey.h"
#include "obj_UnitSR.h"
#include "define_Command.h"
#include "task_survey.h"
#include "stack212.h"
#include "obj_ThreadHeartBeatService.h"
#include "obj_IoInterface.h"

#define VDU_MSG_GoCmd			0
#define VDU_MSG_MonOnCmd			1
#define VDU_MSG_MonOffCmd			2
#define VDU_MSG_CallVideoOn		3
#define VDU_MSG_CallVideoOff		4
#define VDU_MSG_MemoForceOff	5
#define VDU_MSG_MonTalkCmd	6
#define VDU_MSG_ClearMonSr		7
#define VDU_MSG_RmReq			8
#define VDU_MSG_HeartBeat		9
#define VDU_MSG_RmCtrl			10
#define VDU_MSG_IXGRmReq			11
#define VDU_MSG_IXGRmKey			12
#define VDU_MSG_IXGRmClose			13

Loop_vdp_common_buffer	vdp_vbu_mesg_queue;
Loop_vdp_common_buffer	vdp_vbu_sync_queue;
vdp_task_t				task_vbu= {.task_name = "video_business"};

void vbu_mesg_data_process(char* msg_data, int len);
void* vbu_mesg_task( void* arg );

void vtk_TaskInit_vbu(void)
{
	init_vdp_common_queue(&vdp_vbu_mesg_queue, 1024, (msg_process)vbu_mesg_data_process, &task_vbu);
	init_vdp_common_queue(&vdp_vbu_sync_queue, 512, NULL, 								  &task_vbu);
	init_vdp_common_task(&task_vbu, MSG_ID_VideoBusiness, vbu_mesg_task, &vdp_vbu_mesg_queue, &vdp_vbu_sync_queue);
}


void* vbu_mesg_task( void* arg )
{
	vdp_task_t*	 ptask 			= (vdp_task_t*)arg;
	p_vdp_common_buffer pdb 	= 0;
	int	size;
		thread_log_add("%s",__func__);
	ptask->task_StartCompleted = 1;
	while( ptask->task_run_flag )
	{
		size = pop_vdp_common_queue(ptask->p_msg_buf, &pdb, 1000);
		if( size > 0 )
		{
			(*ptask->p_msg_buf->process)(pdb,size);
			purge_vdp_common_queue( ptask->p_msg_buf );
		}
		else
		{
			Memo_Playback_AutoClose();
			Rm_AutoClose();
		}
	}
	return 0;
}


void vbu_mesg_data_process(char* msg_data,int len)
{
	VDP_MSG_HEAD* pmsg = (VDP_MSG_HEAD*)msg_data;
	DTSTACK_MSG_TYPE *dt_cmd;
	//log_d();
	// �ж��Ƿ�Ϊϵͳ����
	switch(pmsg->msg_type)
	{
		case VDU_MSG_GoCmd:
			vbu_GoCmd_Process((DTSTACK_MSG_TYPE *)msg_data);
			break;
		case VDU_MSG_MonOnCmd:
			Video_MonOn_Request();
			break;
		case VDU_MSG_MonOffCmd:
			Video_MonOff_Request();
			break;
		case VDU_MSG_CallVideoOn:
			open_call_video();
			break;
		case VDU_MSG_CallVideoOff:
			close_call_video();
			Memo_Stop();
			break;
		case VDU_MSG_MemoForceOff:
			Memo_Stop();
			break;
		case VDU_MSG_MonTalkCmd:
			Video_MonTalk_Request();
			break;
		case VDU_MSG_ClearMonSr:
			ClearMonSrRequest();
			ClearPlaybackSrRequest();
			break;
		case VDU_MSG_RmReq:
			dt_cmd=(DTSTACK_MSG_TYPE *)msg_data;
			IpgRmReq_Process(dt_cmd->address,dt_cmd->data_buf[0]);
			break;
		case VDU_MSG_RmCtrl:
			dt_cmd=(DTSTACK_MSG_TYPE *)msg_data;
			RM_Ctrl_Process(dt_cmd->command_type,dt_cmd->address,dt_cmd->data_buf,dt_cmd->data_len);
			break;
		case VDU_MSG_HeartBeat:
			RecvThreadHeartbeatReply(VideoBu_Heartbeat_Mask);
			break;
	}
} 
void vbu_GoCmd_Process(DTSTACK_MSG_TYPE *Message_command)
{
	char re_buff[10];
	int video_id;
	int da = Message_command->target_address;
	int sa = Message_command->address;
	char *data = Message_command->data_buf;
	int len = Message_command->data_len;

	if(da==GA1_VIDEO_REQUEST)
	{	
		video_id=(data[0]&VIDEO_SERVICE_SERVER_ID);
		
		if(video_id>0)
		{
			re_buff[0]=(data[0]|VIDEO_SERVICE_ON);
			
		}
		else
		{
			re_buff[0]=0;
		}
		
		//AI_PushOneCmdIntoQueue(OS_Q_STACK_FRAME_TASK_TRS_APT_A, GA2_VIDEO_SERVICE, APCI_GROUP_VALUE_WRITE, 1, 1,re_buff);
		if(video_id==29)
		{
			uint8 video_request;
			uint16 client_time_limit;
			uint16 mem_address;
			char memo_re_buff[10];

			video_request = data[0];
				
			client_time_limit = data[1];
			client_time_limit = (client_time_limit<<8)+data[2];
			
			mem_address = data[3];
			mem_address = (mem_address<<8)+data[4];
			if(API_SR_Request(CT11_ST_MONITOR)!=0)
				return;
			Memo_Stop();
			
			//log_v("playback_req:video_request=%d,client_time_limit=%d,mem_address=%04x",video_request,client_time_limit,mem_address);
			if(Memo_Playback_Request(sa,memo_re_buff,client_time_limit)==1)
			{
				AI_PushOneCmdIntoQueue(OS_Q_STACK_FRAME_TASK_TRS_APT_A, GA2_VIDEO_SERVICE, APCI_GROUP_VALUE_WRITE, 1, 1,re_buff);
				SR_Routing_Create(CT11_ST_MONITOR);
			}
			usleep(100*1000);
			AI_PushOneCmdIntoQueue(OS_Q_STACK_FRAME_TASK_TRS_APT_A, GA4_MEM_REPORT, APCI_GROUP_VALUE_WRITE, 1, 6,memo_re_buff);
		}
		else if(video_id==0)
		{
			//if(API_SR_Request(CT11_ST_MONITOR)!=0)
			//	return;
			Video_Go_Request(video_id);
			Memo_Go_off();
			//AI_PushOneCmdIntoQueue(OS_Q_STACK_FRAME_TASK_TRS_APT_A, GA2_VIDEO_SERVICE, APCI_GROUP_VALUE_WRITE, 1, 1,re_buff);
		}
		else
		{
			
			//log_v("GA1_VIDEO_REQUEST:%02d",video_id);
			if(Video_Go_Request(video_id)>0)
				AI_PushOneCmdIntoQueue(OS_Q_STACK_FRAME_TASK_TRS_APT_A, GA2_VIDEO_SERVICE, APCI_GROUP_VALUE_WRITE, 1, 1,re_buff);
		}
	}
	else if(da==GA5_MEM_ORDER)
	{
	#if 1
		if(Message_command->data_buf[2] == MEMO_DELETE_BYROOM)	
		{
			Memo_Go_Request(sa,re_buff,1);
			AI_PushOneCmdIntoQueue(OS_Q_STACK_FRAME_TASK_TRS_APT_A, GA4_MEM_REPORT, APCI_GROUP_VALUE_WRITE, 1, 6,re_buff);
		}
		else
		{
			if(API_Para_Read_Int(DisablePIP)==0)
				Memo_Go_Request(sa,re_buff,0);
			//AI_PushOneCmdIntoQueue(OS_Q_STACK_FRAME_TASK_TRS_APT_A, GA4_MEM_REPORT, APCI_GROUP_VALUE_WRITE, 1, 6,re_buff);
		}
		//log_d("GA5_MEM_ORDER:%02x %02x %02x",data[0],data[1],data[2]);
		
		
		//log_d("GA5_MEM_STARTED");
	#endif
	}
	else if(da==GA3_MEM_PLAYBACK)
	{
		//log_v("GA3_MEM_PLAYBACK:%02d",data[0]);
		Memo_Playback_Ctrl(sa,data[0],re_buff);
	}
}
void RM_Ctrl_Process(int cmd,int device_sa,char *data,int len)
{
	int pointx,pointy;
	switch(cmd)
	{
		case APCI_RM_M_EXIT:
			IpgRmExit_Process(device_sa);
			break;
		case APCI_RM_M_OK:
			IpgRmTp_Process(device_sa,-1,-1);
			break;
		case APCI_RM_M_TP:
			pointx = data[0];
			pointx = (pointx<<8)|data[1];
			pointy = data[2];
			pointy = (pointy<<8)|data[3];
			IpgRmTp_Process(device_sa,pointx,pointy);
			break;
	}
}
void API_vbu_GoCmd(uint16 source_address , uint16 target_address, uint8* data, uint8 data_length)
{
	DTSTACK_MSG_TYPE	rec_msg_from_stack;
	
	rec_msg_from_stack.head.msg_source_id	= 0;
	rec_msg_from_stack.head.msg_target_id	= MSG_ID_VideoBusiness;
	rec_msg_from_stack.head.msg_type		= VDU_MSG_GoCmd;
	rec_msg_from_stack.head.msg_sub_type	= 0;
	
	rec_msg_from_stack.address			= source_address;	//???
	rec_msg_from_stack.target_address		= target_address;	//????
	rec_msg_from_stack.command_mode		= MODE_NEW_COM;		//?????
	rec_msg_from_stack.command_type		= 0;				//??
	if(data_length > 0 && data != NULL)
	{
		if(data_length <= DATA_MAX)
		{	
			rec_msg_from_stack.data_len	= data_length;		//????
		}
		else
		{
			rec_msg_from_stack.data_len	= DATA_MAX;			//????
		}
		memcpy(rec_msg_from_stack.data_buf,data,rec_msg_from_stack.data_len);
	}

	//API_add_message_to_suvey_queue((char*)&rec_msg_from_stack, sizeof(DTSTACK_MSG_TYPE) ); 
	push_vdp_common_queue(task_vbu.p_msg_buf, (char*)&rec_msg_from_stack, sizeof(DTSTACK_MSG_TYPE));
}
void API_vbu_CallVideoOn(void)
{
	VDP_MSG_HEAD msg;
	msg.msg_source_id=0;
	msg.msg_target_id=MSG_ID_VideoBusiness;
	msg.msg_type=VDU_MSG_CallVideoOn;
	msg.msg_sub_type=0;
	
	push_vdp_common_queue(task_vbu.p_msg_buf, (char*)&msg, sizeof(VDP_MSG_HEAD));
}

void API_vbu_CallVideoOff(void)
{
	VDP_MSG_HEAD msg;
	msg.msg_source_id=0;
	msg.msg_target_id=MSG_ID_VideoBusiness;
	msg.msg_type=VDU_MSG_CallVideoOff;
	msg.msg_sub_type=0;
	
	push_vdp_common_queue(task_vbu.p_msg_buf, (char*)&msg, sizeof(VDP_MSG_HEAD));
}

void API_vbu_MemoForceOff(void)
{
	VDP_MSG_HEAD msg;
	msg.msg_source_id=0;
	msg.msg_target_id=MSG_ID_VideoBusiness;
	msg.msg_type=VDU_MSG_MemoForceOff;
	msg.msg_sub_type=0;
	
	push_vdp_common_queue(task_vbu.p_msg_buf, (char*)&msg, sizeof(VDP_MSG_HEAD));
}


void API_vbu_MonOnCmd(int im_addr)
{
	VDP_MSG_HEAD msg;
	msg.msg_source_id=0;
	msg.msg_target_id=MSG_ID_VideoBusiness;
	msg.msg_type=VDU_MSG_MonOnCmd;
	msg.msg_sub_type=im_addr;
	
	push_vdp_common_queue(task_vbu.p_msg_buf, (char*)&msg, sizeof(VDP_MSG_HEAD));
}

void API_vbu_MonOffCmd(void)
{
	VDP_MSG_HEAD msg;
	msg.msg_source_id=0;
	msg.msg_target_id=MSG_ID_VideoBusiness;
	msg.msg_type=VDU_MSG_MonOffCmd;
	msg.msg_sub_type=0;
	
	push_vdp_common_queue(task_vbu.p_msg_buf, (char*)&msg, sizeof(VDP_MSG_HEAD));
}

void API_vbu_MonTalkCmd(void)
{
	VDP_MSG_HEAD msg;
	msg.msg_source_id=0;
	msg.msg_target_id=MSG_ID_VideoBusiness;
	msg.msg_type=VDU_MSG_MonTalkCmd;
	msg.msg_sub_type=0;
	
	push_vdp_common_queue(task_vbu.p_msg_buf, (char*)&msg, sizeof(VDP_MSG_HEAD));
}

void API_vbu_ClearMonSr(void)
{
	VDP_MSG_HEAD msg;
	msg.msg_source_id=0;
	msg.msg_target_id=MSG_ID_VideoBusiness;
	msg.msg_type=VDU_MSG_ClearMonSr;
	msg.msg_sub_type=0;
	
	push_vdp_common_queue(task_vbu.p_msg_buf, (char*)&msg, sizeof(VDP_MSG_HEAD));
}
void API_vbu_RmReq(uint16 source_address , uint16 target_address, uint8* data, uint8 data_length)
{
	DTSTACK_MSG_TYPE	rec_msg_from_stack;
	
	rec_msg_from_stack.head.msg_source_id	= 0;
	rec_msg_from_stack.head.msg_target_id	= MSG_ID_VideoBusiness;
	rec_msg_from_stack.head.msg_type		= VDU_MSG_RmReq;
	rec_msg_from_stack.head.msg_sub_type	= 0;
	
	rec_msg_from_stack.address			= source_address;	//???
	rec_msg_from_stack.target_address		= target_address;	//????
	rec_msg_from_stack.command_mode		= MODE_NEW_COM;		//?????
	rec_msg_from_stack.command_type		= 0;				//??
	if(data_length > 0 && data != NULL)
	{
		if(data_length <= DATA_MAX)
		{	
			rec_msg_from_stack.data_len	= data_length;		//????
		}
		else
		{
			rec_msg_from_stack.data_len	= DATA_MAX;			//????
		}
		memcpy(rec_msg_from_stack.data_buf,data,rec_msg_from_stack.data_len);
	}

	//API_add_message_to_suvey_queue((char*)&rec_msg_from_stack, sizeof(DTSTACK_MSG_TYPE) ); 
	push_vdp_common_queue(task_vbu.p_msg_buf, (char*)&rec_msg_from_stack, sizeof(DTSTACK_MSG_TYPE));
}

void API_vbu_RmCtrl(int cmd,uint16 source_address , uint16 target_address, uint8* data, uint8 data_length)
{
	DTSTACK_MSG_TYPE	rec_msg_from_stack;
	
	rec_msg_from_stack.head.msg_source_id	= 0;
	rec_msg_from_stack.head.msg_target_id	= MSG_ID_VideoBusiness;
	rec_msg_from_stack.head.msg_type		= VDU_MSG_RmCtrl;
	rec_msg_from_stack.head.msg_sub_type	= 0;
	
	rec_msg_from_stack.address			= source_address;	//???
	rec_msg_from_stack.target_address		= target_address;	//????
	rec_msg_from_stack.command_mode		= MODE_NEW_COM;		//?????
	rec_msg_from_stack.command_type		= cmd;				//??
	if(data_length > 0 && data != NULL)
	{
		if(data_length <= DATA_MAX)
		{	
			rec_msg_from_stack.data_len	= data_length;		//????
		}
		else
		{
			rec_msg_from_stack.data_len	= DATA_MAX;			//????
		}
		memcpy(rec_msg_from_stack.data_buf,data,rec_msg_from_stack.data_len);
	}

	//API_add_message_to_suvey_queue((char*)&rec_msg_from_stack, sizeof(DTSTACK_MSG_TYPE) ); 
	push_vdp_common_queue(task_vbu.p_msg_buf, (char*)&rec_msg_from_stack, sizeof(DTSTACK_MSG_TYPE));
}


void API_vbu_HeartBeatReq(void)
{
	VDP_MSG_HEAD msg;
	msg.msg_source_id=0;
	msg.msg_target_id=MSG_ID_VideoBusiness;
	msg.msg_type=VDU_MSG_HeartBeat;
	msg.msg_sub_type=0;
	
	push_vdp_common_queue(task_vbu.p_msg_buf, (char*)&msg, sizeof(VDP_MSG_HEAD));
}
typedef struct
{
	VDP_MSG_HEAD head;
	int source_addr;
	char data[10];
}IXGRM_MSG_S;
void API_vbu_IXGRMReq(int source_addr,char *data,int len)
{
	IXGRM_MSG_S msg;
	msg.head.msg_source_id=0;
	msg.head.msg_target_id=MSG_ID_VideoBusiness;
	msg.head.msg_type=VDU_MSG_IXGRmReq;
	msg.head.msg_sub_type=0;
	msg.source_addr=source_addr;
	memcpy(msg.data,data,len);
	push_vdp_common_queue(task_vbu.p_msg_buf, (char*)&msg, sizeof(IXGRM_MSG_S));
}

////////////////////////////////////
#include "task_survey.h"
#include "vtk_udp_stack_c5_ipc_cmd.h"
#include "obj_VtkUnicastCommandInterface.h"
typedef enum
{
	MON_SERVICE_ON,
	MON_SERVICE_NG,
	MON_SERVICE_PROXY,
} mon_link_rsp_state;

typedef enum
{
	MON_RESOURCE_OK,
	MON_RESOURCE_BUSY,	
	MON_RESOURCE_NG,
	MON_RESOURCE_PROXY,
} mon_resource_status;

typedef enum
{
	MON_LINK_NG_NONE,
	MON_LINK_NG_GATEWAY_BUSY,
	MON_LINK_NG_TARGET_BUSY,
	MON_LINK_NG_NO_TARGET,
} mon_link_rsp_ng_reason;
#pragma pack(1)
typedef struct
{
	mon_link_rsp_state 		state;			// 0-OK; 1-NG; 2-Proxy
	mon_link_rsp_ng_reason	reason;			// ��������ԭ�� : 1-Gateway_BUSY; 2-Target_BUSY; 3-NO_TARGET
	uint32					ipaddr;	
	uint32					gateway;	
	uint8					vres_id;
}mon_link_result;

typedef struct
{
	//ͨ��ͷ
	uint8 	mon_type;		// ��������
	uint8 	vres_id;		// �豸id
	//������������
	uint16 	apply_type;		// ��������
}mon_link_request;
typedef struct
{
	//ͨ��ͷ
	uint8 					mon_type;		// ��������
	uint8 					vres_id;		// �豸id
	//Ӧ����������
	mon_link_result			result;			// ���ؽ��?
}mon_link_response;
#pragma pack()
int ip_mon_link_rsp(int target_ip, int id, unsigned short apply_type, unsigned char vres_id_req,  mon_link_rsp_state state, mon_link_rsp_ng_reason reason, int gateway, int ipaddr,unsigned char vres_id_result )
{
	mon_link_response	MonLinkCmdRsp;

	MonLinkCmdRsp.mon_type			= GlMonMr;
	MonLinkCmdRsp.vres_id			= vres_id_req;	// ���󷽵�id
	MonLinkCmdRsp.result.state		= state;
	MonLinkCmdRsp.result.reason		= reason;
	MonLinkCmdRsp.result.gateway	= gateway;
	MonLinkCmdRsp.result.ipaddr		= ipaddr;
	MonLinkCmdRsp.result.vres_id	= vres_id_result;	// ��Ӧ��id

	if( api_udp_c5_ipc_send_rsp(target_ip,0x1081,id,(char*)&MonLinkCmdRsp,sizeof(mon_link_response) ) == -1)
	{
		bprintf("API_ip_mon_link_rsp fail\n");
		return -1;
	}
	else
	{
		bprintf("API_ip_mon_link_rsp ok\n");
		return 0;
	}
}

int recv_ip_mon_link_req( UDP_MSG_TYPE *pdata)		//czn_20181218
{
	
	mon_link_response 		MonLinkCmdRsp;
	mon_link_request*		pMonLinkCmdReq;
	mon_link_rsp_state		state;
	mon_link_rsp_ng_reason	reason;
	int						gateway;
	int						ipaddr;
	unsigned char				vres_id;
	int						vres_status;
	int						ret = 0;
	pMonLinkCmdReq=(mon_link_request*)pdata->pbuf;
	
	#if 0
	pthread_mutex_lock(&MonitorObj_Lock);
	
	
	printf("1111111111recv_ip_mon_link_req:%d\n",one_monitor.state);
	// �������д���״̬
	if( is_video_server_proxy_enable() )
	{
		state		= MON_SERVICE_PROXY;
		reason		= MON_LINK_NG_NONE;
		gateway 		= 0;
		ipaddr		= get_video_server_proxy_ip();
		vres_id		= 0;
	}
	// ����������״̬
	else if( is_video_server_available() )
	{
		if(one_monitor.state == MONITOR_IDLE)
		{
			if( api_get_video_cs_service_state() == VIDEO_CS_IDLE )
			{
				state		= MON_SERVICE_ON;
				reason		= MON_LINK_NG_NONE;
				gateway 		= 0;
				ipaddr		= 0;
				vres_id		= vres_id_req;

				one_monitor.state = MONITOR_SERVER;
				one_monitor.apply_type = apply_type;
				one_monitor.device_id = vres_id_req;
				one_monitor.target_ip = target_ip;
				one_monitor.talking = 0;			//czn_20190529
				#if 0
				if(psource->len < sizeof(mon_link_request))
				{
					one_monitor.limit_time = 30;
					one_monitor.source_dev_addr = 0;
				}
				else
				{
					one_monitor.limit_time = pMonLinkCmdReq->limit_time;
					one_monitor.source_dev_addr = pMonLinkCmdReq->source_addr;
				}
				#endif
			}
			// �ж��Ƿ�Ϊ����Ϊͬһ���豸id(����)��������?�Է�ϵͳæ
			else
			{
				if(api_get_video_cs_service_state() == VIDEO_CS_SERVER)
				{
					api_video_s_service_turn_off();
				}

				API_talk_off();
				#if 0
				if( api_get_video_s_service_dev_id() == pMonLinkCmdReq->vres_id)
				{
					state		= MON_SERVICE_ON;
					reason		= MON_LINK_NG_NONE;
					gateway 	= 0;
					ipaddr		= 0;
					vres_id		= 0;
				}
				else
				{
					state		= MON_SERVICE_NG;
					reason		= MON_LINK_NG_TARGET_BUSY;
					gateway 	= 0;
					ipaddr		= 0;
					vres_id		= 0;
				}
				#else
				state		= MON_SERVICE_NG;
				reason		= MON_LINK_NG_TARGET_BUSY;
				gateway 	= 0;
				ipaddr		= 0;
				vres_id		= 0;
				#endif
				ret = 1;
			}
		}
		else if(one_monitor.state == MONITOR_SERVER)
		{
			//if( api_get_video_s_service_dev_id() == vres_id_req)
			{
				state		= MON_SERVICE_ON;
				reason		= MON_LINK_NG_NONE;
				gateway 	= 0;
				ipaddr		= 0;
				vres_id		= vres_id_req;
				if(apply_type == REASON_CODE_CALL)
					one_monitor.apply_type = apply_type;
			}
			#if 0
			else
			{
				state		= MON_SERVICE_NG;
				reason		= MON_LINK_NG_TARGET_BUSY;
				gateway 	= 0;
				ipaddr		= 0;
				vres_id		= 0;
				bprintf("reason		= MON_LINK_NG_TARGET_BUSY curid= %d,reqid=%d\n",api_get_video_s_service_dev_id(),vres_id_req);

				ret = 1;				
			}
			#endif
		}
		else if(one_monitor.state == MONITOR_LINPHONE)
		{
			if(apply_type == REASON_CODE_CALL)
			{
				CloseLinphoneCall_ByMonlink();
				one_monitor.state = MONITOR_IDLE;
				#if 0
				state		= MON_SERVICE_ON;
				reason		= MON_LINK_NG_NONE;
				gateway 		= 0;
				ipaddr		= 0;
				vres_id		= vres_id_req;

				one_monitor.state = MONITOR_SERVER;
				one_monitor.apply_type = apply_type;
				one_monitor.device_id = vres_id_req;
				one_monitor.target_ip = target_ip;
				one_monitor.talking = 0;	
				#endif
			}
			//else
			{
				state		= MON_SERVICE_NG;
				reason		= MON_LINK_NG_TARGET_BUSY;
				gateway 	= 0;
				ipaddr		= 0;
				vres_id		= 0;

				ret = 1;
			}
		}
		else if(one_monitor.state == MONITOR_LINPHONEMON)
		{
			if(apply_type == REASON_CODE_CALL)
			{
				CloseLinphoneMon_ByMonlink();
				//API_VtkMediaTrans_StopJpgPush();
				//usleep(1000*1000);
				one_monitor.state = MONITOR_IDLE;
				state		= MON_SERVICE_ON;
				reason		= MON_LINK_NG_NONE;
				gateway 		= 0;
				ipaddr		= 0;
				vres_id		= vres_id_req;

				one_monitor.state = MONITOR_SERVER;
				one_monitor.apply_type = apply_type;
				one_monitor.device_id = vres_id_req;
				one_monitor.target_ip = target_ip;
				one_monitor.talking = 0;			//czn_20190529
			}
			else
			{
				state		= MON_SERVICE_NG;
				reason		= MON_LINK_NG_TARGET_BUSY;
				gateway 	= 0;
				ipaddr		= 0;
				vres_id		= 0;

				ret = 1;
			}
		}
		
		
	}
	// �������ر�״̬
	else
	{
		state		= MON_SERVICE_NG;
		reason		= MON_LINK_NG_NO_TARGET;
		gateway 	= 0;
		ipaddr		= 0;		
		vres_id		= 0;
	}

	pthread_mutex_unlock(&MonitorObj_Lock);
	#endif		
	state		= MON_SERVICE_ON;
	reason		= MON_LINK_NG_NONE;
	gateway 		= 0;
	ipaddr		= 0;
	vres_id		= pMonLinkCmdReq->vres_id;
	
	if( ip_mon_link_rsp( pdata->target_ip, pdata->id, pMonLinkCmdReq->apply_type, pMonLinkCmdReq->vres_id, state, reason, gateway, ipaddr,vres_id) == 0 )
	{
		ret = 0;		
	}
	else
	{
		ret = -1;
	}
	
	return ret;
}

// 0/ok, -1/err
int ip_mon_link_req(int target_ip,unsigned short apply_type, unsigned char vres_id,char* prcvbuf, int* plen )	//czn_20190610
{
	mon_link_request 	MonLinkCmdReq;
	
	MonLinkCmdReq.mon_type		= GlMonMr;		//  ���Ӻͺ��е�link��Ҫ����
	MonLinkCmdReq.vres_id		= vres_id;
	MonLinkCmdReq.apply_type		= apply_type;

	if( api_udp_c5_ipc_send_req2( target_ip,VTK_CMD_LINK_1001,(char*)&MonLinkCmdReq,sizeof(mon_link_request), prcvbuf, plen,2500) == -1 )
	{
		if(api_udp_c5_ipc_send_req2( target_ip,VTK_CMD_LINK_1001,(char*)&MonLinkCmdReq,sizeof(mon_link_request), prcvbuf, plen,2500) == -1)
		{
			bprintf("API_ip_mon_link_req fail\n");
			return -1;
		}
	}
	//else
	{
		bprintf("API_ip_mon_link_req ok\n");
		return 0;
	}
}
//return: 0/ok, -1/�豸�쳣��1/proxy ok, 2/�豸������
int send_ip_mon_link_req( int target_ip,unsigned short apply_type, unsigned char vres_id, int* premote_ip, unsigned char* pdev_id  )
{
	mon_link_response* presponse;
	char rev_buf[200];
	int  rec_len = 200;	//czn_20160516

	if( ip_mon_link_req(target_ip,apply_type,vres_id, rev_buf, &rec_len ) == 0 )
	{
		presponse = (mon_link_response*)rev_buf;
	
		// �ж�״̬
		if( presponse->result.state == MON_SERVICE_ON )
		{
			dprintf("mon link 0x%08x.%d rsp state ON!\n",target_ip,vres_id);
			if( premote_ip != NULL )
				*premote_ip 	= target_ip;
			if( pdev_id != NULL )
				*pdev_id		= vres_id;
			return 0;
		}
		else if( presponse->result.state == MON_SERVICE_PROXY )
		{
			dprintf("mon link 0x%08x.%d rsp state PROXY!\n",target_ip,vres_id);
			
			int proxy_ip = 0;
			
			if( presponse->result.ipaddr )
			{
				proxy_ip = presponse->result.ipaddr;
			}
			else if( presponse->result.gateway && !presponse->result.ipaddr )
			{
				proxy_ip = inet_addr("192.168.1.0");
				proxy_ip |= (presponse->result.gateway<<24);					
			}			
			if( proxy_ip )
			{
				if( ip_mon_link_req(proxy_ip, apply_type,presponse->result.vres_id, rev_buf, &rec_len) != -1 )
				{
					presponse = (mon_link_response*)rev_buf;
					if( presponse->result.state == MON_SERVICE_ON )
					{
						dprintf("mon link 0x%08x.%d rsp state ON!\n",proxy_ip,vres_id);	
						if( premote_ip != NULL )
							*premote_ip	= proxy_ip;
						if( pdev_id != NULL )
							*pdev_id 	= presponse->result.vres_id;						
						return 0;
					}
					else if( presponse->result.state == MON_SERVICE_NG )
					{
						dprintf("mon link 0x%08x.%d rsp state NG!\n",proxy_ip,vres_id);		
						return 2;
					}
					else
						return -1;
				}
				else						
					return -1;
			}
			else
				return -1;
		}
		else if( presponse->result.state == MON_SERVICE_NG )
		{
			dprintf("mon link 0x%08x.%d rsp state NG!\n",target_ip,vres_id);		
			return 2;
		}
		else
		{
			return -1;
		}
	}
	else
		return -1;
}
