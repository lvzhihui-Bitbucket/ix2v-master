#include <stdio.h>
#include <sys/sysinfo.h>
#include "task_Shell.h"
#include "task_survey.h"
#include "obj_GetIpByInput.h"
#include "ip_video_cs_control.h"
#include "obj_ak_decoder.h"
#include "obj_UnitSR.h"
#include "define_Command.h"
#include "stack212.h"
#include "obj_IoInterface.h"
#include "ffmpeg_rec.h"

#define RECORD_DAT_FILENAME		"racord.dat" 
//static pthread_mutex_t memo_lock = PTHREAD_MUTEX_INITIALIZER;
extern pthread_mutex_t momo_mon_lock;
int get_memo_data_byroomid(char *record_dir,int *unread ,int *total,int opt)
{
	char buff[100],*pstr;
	int unread_tmp,total_tmp;

	snprintf(buff,100,"%s/%s",record_dir,RECORD_DAT_FILENAME);

	FILE *pf = fopen(buff,"r+");

	if(pf == NULL)
	{
		pf = fopen(buff,"w");
		if(pf == NULL)
			return -1;
		
		if(opt)
		{
			unread_tmp = 1;
			total_tmp = 1;
		}
		else
		{
			unread_tmp = 0;
			total_tmp = 0;
		}
		
		snprintf(buff,100,"unread:%04d\n",unread_tmp);
		fputs(buff,pf);
		snprintf(buff,100,"total:%04d\n",total_tmp);
		fputs(buff,pf);
		
		//fclose(pf);
		//sync();
	}
	else
	{
		int seek_len = 0;
		while((pstr = fgets(buff,100,pf)) != NULL)
		{
			if(memcmp(buff,"unread:",strlen("unread:")) == 0)
			{
				pstr = buff + strlen("unread:");
				printf("+++++++++++++pstr unread strlen= %d+++++++++++++++\n",strlen(pstr));
				if(strlen(pstr) != 5)
				{
					fclose(pf);
					snprintf(buff,100,"%s/%s",record_dir,RECORD_DAT_FILENAME);
					remove(buff);
					//sync();
					return -1;
				}
				pstr[4] = 0;
				unread_tmp = atol(pstr);
				
				if(opt)
				{
					fseek(pf,seek_len,SEEK_SET);
					snprintf(buff,100,"unread:%04d\n",++unread_tmp);
					fputs(buff,pf);
				}

				seek_len = ftell(pf);

				while((pstr = fgets(buff,100,pf)) != NULL)
				{
					if(memcmp(buff,"total:",strlen("total:")) == 0)
					{
						pstr = buff + strlen("total:");
						printf("+++++++++++++pstr total strlen= %d+++++++++++++++\n",strlen(pstr));
						if(strlen(pstr) != 5)
						{
							fclose(pf);
							snprintf(buff,100,"%s/%s",record_dir,RECORD_DAT_FILENAME);
							remove(buff);
							//sync();
							return -1;
						}
						pstr[4] = 0;
						total_tmp = atol(pstr);
						
						if(opt)
						{
							fseek(pf,seek_len,SEEK_SET);
							snprintf(buff,100,"total:%04d\n",++total_tmp);
							fputs(buff,pf);
						}
						break;
					}
					seek_len = ftell(pf);
				}
				
				break;
			}
			seek_len = ftell(pf);
		}
		if(pstr == NULL)
		{
			fclose(pf);
			snprintf(buff,100,"%s/%s",record_dir,RECORD_DAT_FILENAME);
			remove(buff);
			//sync();
			return -1;
		}
	}

	
	
	fclose(pf);
	//sync();
	
	if(unread != NULL)
		*unread = unread_tmp;
	
	if(total!= NULL)
		*total = total_tmp;
	
	return 0;
}
int create_video_record_timelabel(char* label )
{
	#define VIDEO_RECORD_FILENAME_LEN	100
	int len = VIDEO_RECORD_FILENAME_LEN;
	char strtime[VIDEO_RECORD_FILENAME_LEN+1];
	time_t t;
	struct tm *tblock; 	
	t = time(NULL); 
	tblock=localtime(&t);

	
	//sprintf( strtime,"%02d%02d%02d_%02d%02d%02d.avi",tblock->tm_year-100,tblock->tm_mon+1,tblock->tm_mday,tblock->tm_hour,tblock->tm_min,tblock->tm_sec);

	printf("----strtime:   %s-----\n", strtime);


	
	sprintf( strtime,"%02d%02d%02d_%02d%02d%02d",(tblock->tm_year-100)>0?(tblock->tm_year-100):0,tblock->tm_mon+1,tblock->tm_mday,tblock->tm_hour,tblock->tm_min,tblock->tm_sec);
	


	
	//strtime[len-1]=0;
	strcpy( label, strtime); 
	return len;

}

static int total_record;
static int current_index ;

typedef struct			//lyx 20170713
{
	int				id;
	char                      filename[40];
	int				mtime;
	
    //pthread_mutex_t 	lock;	
} list_record_t;


list_record_t * 	 precord_list 		= NULL;

static int cur_room_unread;
static int cur_room_total;
static int cur_room_readed;
int set_memo_total_byroomid(char *record_dir,int total )
{
	char buff[100],*pstr;

	snprintf(buff,100,"%s/%s",record_dir,RECORD_DAT_FILENAME);

	FILE *pf = fopen(buff,"r+");

	if(pf == NULL)
		return -1;

	int seek_len = ftell(pf);

	while((pstr = fgets(buff,100,pf)) != NULL)
	{
		if(memcmp(buff,"total:",strlen("total:")) == 0)
		{
			pstr = buff + strlen("total:");
			if(strlen(pstr) != 5)
			{
				fclose(pf);
				snprintf(buff,100,"%s/%s",record_dir,RECORD_DAT_FILENAME);
				remove(buff);
				//sync();
				return -1;
			}
			
			
			fseek(pf,seek_len,SEEK_SET);
			snprintf(buff,100,"total:%04d\n",total);
			fputs(buff,pf);
			break;
		}
		seek_len = ftell(pf);
	}

	if(pstr == NULL)
	{
		fclose(pf);
		snprintf(buff,100,"%s/%s",record_dir,RECORD_DAT_FILENAME);
		remove(buff);
		//sync();
		return -1;
	}

	fclose(pf);
	//sync();

	return 0;
}
int set_memo_unread_byroomid(char *record_dir,int unread )
{
	char buff[100],*pstr;

	snprintf(buff,100,"%s/%s",record_dir,RECORD_DAT_FILENAME);

	FILE *pf = fopen(buff,"r+");

	if(pf == NULL)
		return -1;

	int seek_len = ftell(pf);

	while((pstr = fgets(buff,100,pf)) != NULL)
	{
		if(memcmp(buff,"unread:",strlen("unread:")) == 0)
		{
			pstr = buff + strlen("unread:");
			if(strlen(pstr) != 5)
			{
				fclose(pf);
				snprintf(buff,100,"%s/%s",record_dir,RECORD_DAT_FILENAME);
				remove(buff);
				//sync();
				return -1;
			}
			
			
			
			fseek(pf,seek_len,SEEK_SET);
			snprintf(buff,100,"unread:%04d\n",unread);
			fputs(buff,pf);
			break;
		}
		seek_len = ftell(pf);
	}

	if(pstr == NULL)
	{
		fclose(pf);
		snprintf(buff,100,"%s/%s",record_dir,RECORD_DAT_FILENAME);
		remove(buff);
		//sync();
		return -1;
	}

	fclose(pf);
	//sync();

	return 0;
}

int memo_release_recordlist(void)
{
	if(precord_list != NULL)
		free(precord_list);	

	precord_list = NULL;
	
}



int sort_record_by_motifytime(void)
{
	int i,j;
	list_record_t temp;
	
	for( i=1; i < total_record; i++)
	{
		for( j = total_record-1; j>=i; j--)
		{
			if(precord_list[j].mtime > precord_list[j-1].mtime )
			{
				temp = precord_list[j-1];
				precord_list[j-1] = precord_list[j];
				precord_list[j] = temp;
			}
		}

	}

	#if 0
	for (i =0;i <total_record; i++)
	{
		printf("%s\t mtime:%d \n",precord_list[i].filename, precord_list[i].mtime);  
	}
	#endif
}
int memo_get_recordlist(char* record_dir )
{
	//memo_release_recordlist();
	
	DIR *dir = NULL;
	struct dirent *entry;
	char fullname[100];
	
	

	/************   ͳ�Ƽ�¼����	***********************/
    	dir = opendir(record_dir);  
	if(dir == NULL)
	{
		printf("---open filedir failed!----\r\n");     
		return -1;
	}
	memo_release_recordlist();

	total_record = 0;
	
	int unread_temp = 0,total_temp = 0;
	
	get_memo_data_byroomid(record_dir,&unread_temp,&total_temp,0);
	
	if(total_temp > 0)
	{
		precord_list = malloc(sizeof(list_record_t)*total_temp);

		if(precord_list == NULL)
		{
			return -1;
		}
	}
	
	while(entry = readdir(dir))
	{

		  //������..���͡�.������Ŀ¼
		 if(strcmp(entry->d_name,".") == 0 || strcmp(entry->d_name,"..") == 0)
		 {
		   	continue;
		 }
		if(strstr(entry->d_name,RECORD_DAT_FILENAME) != NULL)
     		{
			continue;
     		}
		
		snprintf(fullname,100,"%s/%s",record_dir,entry->d_name);

		struct stat st; 
		
      		if ( stat( fullname, &st ) == -1 )
      		{
			printf(" get file stat error\n");     
      		}
    
           	if( S_ISDIR(st.st_mode) )      //������ļ��в�������?
           	{
                 	continue;				//��Ŀ¼����ѭ�� 
           	}     
	         else 
	         {	
	         	if(st.st_size<1024*10)
	         	{
				
				remove(fullname);
				continue;
			}
	         	if(total_record < total_temp)
	         	{
	         		strcpy( precord_list[total_record].filename, entry->d_name) ;		//���ļ��ͼ�1
					precord_list[total_record].mtime =  st.st_mtime;  
	         	}
				total_record++;		   
	         }

	}
	
	closedir(dir);
	
	if(total_record == 0)
	{
		
		if(precord_list != NULL)
		{
			free(precord_list);
			precord_list = NULL;
		}
		return -1;
	}

	/************   ��ȡ��¼�б�	***********************/
	
	
	if(total_record > total_temp)
	{
		if(precord_list != NULL)
		{
			free(precord_list);
			precord_list = NULL;
		}

		precord_list = malloc(sizeof(list_record_t)*total_record);

		if(precord_list == NULL)
		{
			return -1;
		}
		
		total_record = 0;
		
		 dir = opendir(record_dir);  
		if(dir == NULL)
		{
			printf("---open filedir failed!----\r\n");     
			return -1;
		}
		
		while(entry = readdir(dir))
		{

			  //������..���͡�.������Ŀ¼
			 if(strcmp(entry->d_name,".") == 0 || strcmp(entry->d_name,"..") == 0)
			 {
			   	continue;
			 }
			if(strstr(entry->d_name,RECORD_DAT_FILENAME) != NULL)
	     		{
				continue;
	     		}
			
			snprintf(fullname,100,"%s/%s",record_dir,entry->d_name);

			//printf("%s\n",fullname);

			struct stat st; 
			
	      		if ( stat( fullname, &st ) == -1 )
	      		{
				printf(" get file stat error\n");     
	      		}
	    
	           	if( S_ISDIR(st.st_mode) )      //������ļ��в�������?
	           	{
	                 	continue;				//��Ŀ¼����ѭ�� 
	           	}     
	                 else 
	                 {	
	                 		
							
	                       	strcpy( precord_list[total_record].filename, entry->d_name) ;		//���ļ��ͼ�1
				precord_list[total_record].mtime =  st.st_mtime;   
				total_record++;	
	                 }

		}

		if(total_record == 0)
		{
			free(precord_list);
			precord_list = NULL;
			return -1;
		}
	}
	sort_record_by_motifytime();
	//printf("total record:%d\n",total_record);
	
	return 0;	

}

#define LocalMemoDir		"/mnt/nand1-2/UserData/video"
#define SdMemoDir		"/mnt/sdcard/video"

#define Max_LocalMemo_Nums		15		//czn_20181113	100
#define MaxLocalMemoNum_PerRoom	10
#define MaxSDMemoNum_PerRoom		30
#define SaveMemoTime_Limit		3600*24*30

typedef struct _Local_Memo_File_Entry
{
	struct _Local_Memo_File_Entry *pre;
	struct _Local_Memo_File_Entry *next;
	int	 mtime;
	char file_path[81];
	char file_name[41];
}Local_Memo_File_Entry;
static Local_Memo_File_Entry *Local_Memo_List = NULL;
static int Local_Memo_List_Num = 0;

int AddOneEntryLocalMemoFileList(char *file_path,char *file_name)
{
	Local_Memo_File_Entry *pentry = malloc(sizeof(Local_Memo_File_Entry));

	if(pentry == NULL)
		return -1;

	char fullname[100];

	snprintf(fullname,100,"%s/%s",file_path,file_name);

	struct stat st; 
		
	if ( stat( fullname, &st ) == -1 )
	{
		free(pentry);
		return -1;
	}
	
	pentry->mtime =  st.st_mtime;
	
	strncpy(pentry->file_path,file_path,80);
	strncpy(pentry->file_name,file_name,40);
	
	pentry->pre = NULL;
	pentry->next = Local_Memo_List;

	if(Local_Memo_List != NULL)
	{
		Local_Memo_List->pre = pentry;
	}

	Local_Memo_List = pentry;

	Local_Memo_List_Num ++;

	return 0;
}

void AddOneDirLocalMemoFileList(char *file_dir)
{
	DIR *dir = NULL;
	struct dirent *entry;

	char fullname[100];
	
	dir = opendir(file_dir);

	if(dir == NULL)
		return;
	
	while(entry = readdir(dir))
	{
		if(strcmp(entry->d_name,".") == 0 || strcmp(entry->d_name,"..") == 0)
		{
			continue;
		}
		if(strstr(entry->d_name,RECORD_DAT_FILENAME) != NULL)
 		{
			continue;
 		}
		
		snprintf(fullname,100,"%s/%s",file_dir,entry->d_name);

		printf("%s\n",fullname);

		struct stat st; 
		
		if ( stat( fullname, &st ) == -1 )
		{
			printf(" get file stat error\n");     
		}
    
       	if( S_ISDIR(st.st_mode) )      //������ļ��в�������?
       	{
             continue;				//��Ŀ¼����ѭ�� 
       	}     
		else 
		{	
			AddOneEntryLocalMemoFileList(file_dir,entry->d_name);
		}
	}
	closedir(dir);
}

void MemoLocalNandListInit(void)
{	
	DIR *dir = NULL;
	struct dirent *entry;
	
	char fullname[100];
	
	dir = opendir(LocalMemoDir);

	if(dir == NULL)
		return;
	Local_Memo_List=NULL;
	Local_Memo_List_Num=0;
	while(entry = readdir(dir))
	{

		  //������..���͡�.������Ŀ¼
		 if(strcmp(entry->d_name,".") == 0 || strcmp(entry->d_name,"..") == 0)
		 {
		   	continue;
		 }
		
		snprintf(fullname,100,"%s/%s",LocalMemoDir,entry->d_name);


		struct stat st; 
	
  		if ( stat( fullname, &st ) == -1 )
  		{
			printf(" get file stat error\n");
			continue;
  		}

       	if( S_ISDIR(st.st_mode) )      //������ļ��в�������?
       	{
           	AddOneDirLocalMemoFileList(fullname);
       	}     
	}
	closedir(dir);
}
void DelOneEntryFromList(Local_Memo_File_Entry *p_entry)
{
	if(p_entry == NULL)
		return;
	
	if(p_entry->pre != NULL)
	{
		p_entry->pre->next = p_entry->next;
	}
	
	if(p_entry->next != NULL)
	{
		p_entry->next->pre = p_entry->pre;
	}
	
	if(Local_Memo_List == p_entry)
	{
		Local_Memo_List = p_entry->next;
	}
	free(p_entry);
	Local_Memo_List_Num -- ;
}

void DelLocalNandMemoOldFile(int nums)
{
	Local_Memo_File_Entry *p_entry,*p_oldest_entry;
	int i;
	
	while(nums > 0 && Local_Memo_List_Num > 0 && Local_Memo_List != NULL)
	{
		p_entry = Local_Memo_List->next;
		p_oldest_entry = Local_Memo_List;
		for(i = 0;i < Local_Memo_List_Num && p_entry != NULL;i++)
		{
			if(p_oldest_entry->mtime > p_entry->mtime)
			{
				p_oldest_entry = p_entry;
			}
			p_entry = p_entry->next;
		}
		
		char fullname[100];
		snprintf(fullname,100,"%s/%s",p_oldest_entry->file_path,p_oldest_entry->file_name);
		remove(fullname);
		#if 0
		if(p_oldest_entry->pre != NULL)
		{
			p_oldest_entry->pre->next = p_oldest_entry->next;
		}
		
		if(p_oldest_entry->next != NULL)
		{
			p_oldest_entry->next->pre = p_oldest_entry->pre;
		}
		
		if(Local_Memo_List == p_oldest_entry)
		{
			Local_Memo_List = p_oldest_entry->next;
		}
		free(p_oldest_entry);
		Local_Memo_List_Num -- ;
		#else
		DelOneEntryFromList(p_oldest_entry);
		#endif
		
		nums --;
	}
}

void DelLocalNandMemoOldFile2(void)//delete over30days local pic
{
	struct tm *tblock; 	
	time_t now_time = time(NULL); 
	tblock=localtime(&now_time);
	char fullname[100];
	Local_Memo_File_Entry *p1,*p2;
	
	
	if(Local_Memo_List_Num > 0 && Local_Memo_List != NULL && tblock->tm_year >= 115)
	{
		for(p1 = Local_Memo_List;p1 != NULL;p1 = p2)
		{
			p2 = p1->next;
			//if(p1->mtime > p_entry->mtime)
			if(abs(now_time - p1->mtime) > SaveMemoTime_Limit)
			{
				//p_oldest_entry = p_entry;
				
				snprintf(fullname,100,"%s/%s",p1->file_path,p1->file_name);
				remove(fullname);
				DelOneEntryFromList(p1);
			}
			
		}
	}
}

int AddMemoToLocalNand(char *file_path,char *file_name)
{
	printf("AddMemoToLocalNand Local_Memo_List_Num=%d,Max_LocalMemo_Nums = %d\n",Local_Memo_List_Num,Max_LocalMemo_Nums);
	DelLocalNandMemoOldFile2();
	if(Local_Memo_List_Num >= Max_LocalMemo_Nums)
	{
		DelLocalNandMemoOldFile(Local_Memo_List_Num - Max_LocalMemo_Nums + 1);

		if(Local_Memo_List_Num >= Max_LocalMemo_Nums)
			return - 1;
	}
	#if 0
	Local_Memo_File_Entry *pentry = malloc(sizeof(Local_Memo_File_Entry));

	if(pentry == NULL)
		return -1;

	char fullname[100];

	snprintf(fullname,100,"%s/%s",file_path,file_name);

	struct stat st; 
		
	if ( stat( fullname, &st ) == -1 )
	{
		free(pentry);
		return -1;
	}
	
	pentry->mtime =  st.st_mtime;
	
	strncpy(pentry->file_path,file_path,60);
	strncpy(pentry->file_name,file_name,40);
	
	pentry->pre = NULL;
	pentry->next = Local_Memo_List;

	if(Local_Memo_List != NULL)
	{
		Local_Memo_List->pre = pentry;
	}

	Local_Memo_List = pentry;

	Local_Memo_List_Num ++;
	
	return 0;
	#endif
	
	return AddOneEntryLocalMemoFileList(file_path,file_name);
}

void DelMemoToLocalNandEntry(char *file_path,char *file_name)
{
	int i;
	if(Local_Memo_List==NULL)
		return;
	Local_Memo_File_Entry *p_entry,*p_entry_temp;
	
	p_entry = Local_Memo_List;
	for(i = 0,p_entry = Local_Memo_List;i < Max_LocalMemo_Nums && Local_Memo_List_Num > 0 && p_entry != NULL;i++)
	{
		if(strcmp(p_entry->file_path,file_path) == 0)
		{
			if(file_name != NULL)
			{
				if(strcmp(p_entry->file_name,file_name) == 0)
				{
					DelOneEntryFromList(p_entry);
					break;
				}
				p_entry = p_entry->next;
			}
			else
			{
				p_entry_temp = p_entry;
				p_entry = p_entry->next;
				DelOneEntryFromList(p_entry_temp);
			}
		}
		else
		{
			p_entry = p_entry->next;
		}
	}
}
int MemoRoomSpaceControl(char *room_dir,int per_room_max)
{
	int i;
	char full_file_name[100];
	if(memo_get_recordlist(room_dir) != 0)
	{
		return 1;
	}
	
	int unread_temp = 0,total_temp = 0;
	
	
	total_temp = total_record;
	
	struct tm *tblock; 	
	time_t now_time = time(NULL); 
	tblock=localtime(&now_time);
	int re_load_flag=0;
	
	
	//if( || total_record >= per_room_max)	//err sys time 
	{
		if(tblock->tm_year >= 115)
		{
			for(i = 0;i < total_record;i ++)
			{
				if(abs(precord_list[i].mtime - now_time) > SaveMemoTime_Limit)
				{
					snprintf(full_file_name,100,"%s/%s",room_dir,precord_list[i].filename);
					remove(full_file_name);
					total_temp --;
					DelMemoToLocalNandEntry(room_dir,precord_list[i].filename);
					re_load_flag=1;
				}
			}
		}
		#if 0
		if(total_record != total_temp)
		{
			set_memo_total_byroomid(room_dir,total_temp);
			if(unread_temp > total_temp)
			{
				unread_temp = total_temp;
				set_memo_unread_byroomid(room_dir,total_temp);
			}
		}
		#endif
		if(total_temp > per_room_max)
		{
			if(re_load_flag)
			{
				memo_release_recordlist();
				if(memo_get_recordlist(room_dir) != 0)
				{
					return 1;
				}
			}
			total_temp = total_record;
			for(i = total_record - 1;i >= per_room_max;i --)
			{
				snprintf(full_file_name,100,"%s/%s",room_dir,precord_list[i].filename);
				remove(full_file_name);
				total_temp --;
				DelMemoToLocalNandEntry(room_dir,precord_list[i].filename);
				re_load_flag=1;
			}
			
		}
		total_record=total_temp;
		get_memo_data_byroomid(room_dir,&unread_temp,&total_temp,0);
		
		if(re_load_flag||total_record!=total_temp)
		{
			set_memo_total_byroomid(room_dir,total_record);
			if(unread_temp > total_record)
			{
				unread_temp = total_record;
				set_memo_unread_byroomid(room_dir,total_record);
			}
		}
	} 
	#if 0
	if(total_temp > per_room_max)
	{
		memo_release_recordlist();
		if(memo_get_recordlist(room_dir) != 0)
		{
			return;
		}
		total_temp = total_record;
		for(i = total_record - 1;i >= per_room_max;i --)
		{
			snprintf(full_file_name,100,"%s/%s",room_dir,precord_list[i].filename);
			remove(full_file_name);
			total_temp --;
			DelMemoToLocalNandEntry(room_dir,precord_list[i].filename);
		}
		set_memo_total_byroomid(room_dir,total_temp);
		if(unread_temp > total_temp)
		{
			unread_temp = total_temp;
			set_memo_unread_byroomid(room_dir,total_temp);
		}
	}
	#endif
	
	memo_release_recordlist();
	return re_load_flag;
	
}
#define MAX_SDSPACE_PER			0.9
#define RESET_SDSPACE_PER		0.6
#define RMFILE_LIMIT_INIT	(3600*24*180)
#define RMFILE_LIMIT_STEP	(3600*24*30)
int MemoRmFile(int act_mode,time_t limit)//act_mode = 0,limit mode,=1,remove 1/2
{
	DIR *dir = NULL;
	struct dirent *entry;
	char roomdir[100];
	char fullname[100];
	int i;
	time_t rm_end_time;
	
	dir = opendir(SdMemoDir);  
	if(dir == NULL)
	{
		printf("---open filedir failed!----\r\n");     
		return -1;
	}
	
	rm_end_time = time(NULL) - limit;

	
	
	while(entry = readdir(dir))
	{

		  //������..���͡�.������Ŀ¼
		 if(strcmp(entry->d_name,".") == 0 || strcmp(entry->d_name,"..") == 0)
		 {
		   	continue;
		 }
		
		snprintf(roomdir,100,"%s/%s",SdMemoDir,entry->d_name);

		struct stat st; 

		if ( stat( roomdir, &st ) == -1 )
		{
			continue;     
		}

		if( S_ISDIR(st.st_mode) )      //������ļ��в�������?
		{
			if(memo_get_recordlist(roomdir) == 0)
			{
				if(act_mode == 0)
				{
					for(i = 0;i < total_record;i ++)
					{
						if(precord_list[total_record - 1 - i].mtime > rm_end_time)
							break;
						snprintf(fullname,100,"%s/%s",roomdir,precord_list[total_record - 1 - i].filename);
						remove(fullname);
					}
				}
				else
				{
					for(i = 0;i < total_record/2;i ++)
					{
						snprintf(fullname,100,"%s/%s",roomdir,precord_list[total_record - 1 - i].filename);
						remove(fullname);
					}
				}
				memo_release_recordlist();
			}
		}     
	}

	return 0;
}
int MemoSdSpaceControl(void)
{
	int free_size,total_size;

	if(API_GetSDCardSize(&free_size,&total_size) != 0)
		return -1;
	
	float use_percent = (float)(total_size-free_size)/(float)total_size;
	
	printf("MemoSdSpaceControl : use_per = %f\n",use_percent);
	
	if(use_percent > MAX_SDSPACE_PER)
	{
		int limit_time = RMFILE_LIMIT_INIT;
		while(use_percent > RESET_SDSPACE_PER)
		{
			if(limit_time > 0)
			{
				if(MemoRmFile(0,limit_time) < 0)
					return -1;
				limit_time -= RMFILE_LIMIT_STEP; 
			}
			else
			{
				MemoRmFile(1,0);
				break;
			}
			
			if(API_GetSDCardSize(&free_size,&total_size) != 0)
				return -1;

			use_percent = (float)(total_size-free_size)/(float)total_size;
		}
	}

	return 0;
}
time_t Playback_StartTime;
static int Playback_Info_State = 1;
static int Playback_State = 0;
static int Playback_AutoClose_Timer = 0;
#define MEMO_INFO_ROOM_X			80
#define MEMO_INFO_ROOM_Y			10
#define MEMO_INFO_FILE_X			(MEMO_INFO_ROOM_X+100)
#define MEMO_INFO_FILE_Y			MEMO_INFO_ROOM_Y
#define MEMO_INFO_TIME_X			80//300
#define MEMO_INFO_TIME_Y			MEMO_INFO_ROOM_Y + 40//(480-40)
static char playback_info_save[2][40]={0};
	
void Memo_Playback_Info_Display(char *room_dir,int act)	// 0 ==start , 1 = recover
{
	char *p_dir;
	char disp[40] = {0};
	int i;
	int offset=0;
	
	//API_TimeLapseStart(COUNT_RUN_UP,0);
	if(precord_list == NULL || current_index >= total_record)
		return;
	
	//API_OsdStringClearExt(MEMO_INFO_ROOM_X,MEMO_INFO_ROOM_Y,600,40);
	p_dir = basename(room_dir);
	
	if(p_dir != NULL)
	{
		strcpy(playback_info_save[0],p_dir);
		if(Playback_Info_State)
		{
			//API_OsdStringDisplayExt(MEMO_INFO_ROOM_X, MEMO_INFO_ROOM_Y, COLOR_WHITE,p_dir, strlen(p_dir), 1,STR_UTF8,0);
			#if !defined(PID_IX850) && !defined(PID_IX611) && !defined(PID_IX622)&&!defined(PID_IX821)
			memo_info_display(0,MEMO_INFO_ROOM_X, MEMO_INFO_ROOM_Y,100, 0,p_dir, strlen(p_dir),1);
			#endif
		}
	}
	
	for(i = 0;i < 3;i++)
	{
	
		#if 0
		disp[i*3] = precord_list[current_index].filename[i*2];
		disp[i*3+1] = precord_list[current_index].filename[i*2+1];

		disp[9+i*3] = precord_list[current_index].filename[7+i*2];
		disp[9+i*3+1] = precord_list[current_index].filename[7+i*2+1];
		#endif
	}
	
	snprintf(disp,40,"[%d/%d]    %s",current_index+1,total_record,strstr(precord_list[current_index].filename,"CMR_")!=NULL?"    DS1":precord_list[current_index].filename);
	strcpy(playback_info_save[1],disp);
	if(Playback_Info_State)
	{
		//API_OsdStringDisplayExt(MEMO_INFO_FILE_X, MEMO_INFO_FILE_Y, COLOR_WHITE,disp, strlen(disp), 1,STR_UTF8,0);
		#if !defined(PID_IX850) && !defined(PID_IX611) && !defined(PID_IX622)&&!defined(PID_IX821)
		memo_info_display(1,MEMO_INFO_FILE_X, MEMO_INFO_FILE_Y,500, 0,disp, strlen(disp),1);
		#endif
	}
	
	if(strstr(precord_list[current_index].filename,"CMR_")!=NULL)
	{
		offset=4;
	}
	disp[0] = precord_list[current_index].filename[offset+0];
	disp[1] = precord_list[current_index].filename[offset+1];
	disp[2] = '\0';
	disp[3] = precord_list[current_index].filename[offset+2];
	disp[4] = precord_list[current_index].filename[offset+3];
	disp[5] = '\0';
	disp[6] = precord_list[current_index].filename[offset+4];
	disp[7] = precord_list[current_index].filename[offset+5];
	disp[8] = '\0';
	disp[9] = precord_list[current_index].filename[offset+7];
	disp[10] = precord_list[current_index].filename[offset+8];
	disp[11] = '\0';
	disp[12] = precord_list[current_index].filename[offset+9];
	disp[13] = precord_list[current_index].filename[offset+10];
	disp[14] = '\0';
	disp[15] = precord_list[current_index].filename[offset+11];
	disp[16] = precord_list[current_index].filename[offset+12];
	disp[17] = '\0';

	struct tm tm_temp;
	time_t time_temp;
	
	memset(&tm_temp,0,sizeof(struct tm));
	tm_temp.tm_year = atol(disp) + 100;
	tm_temp.tm_mon = atol(disp+3)-1;
	tm_temp.tm_mday = atol(disp+6);
	tm_temp.tm_hour = atol(disp+9);
	tm_temp.tm_min = atol(disp+12);
	tm_temp.tm_sec = atol(disp+15);
	
	Playback_StartTime = mktime(&tm_temp);
	if(act == 0)
	{
		DisplayPlaybackTiming(0);
	}
	else
	{
		DisplayPlaybackTiming(-1);
	}
}

void DisplayPlaybackTiming( int sec_cnt )
{
	char str[40];
	static time_t playback_time;

	if(sec_cnt >= 0)
	{
		playback_time = Playback_StartTime + sec_cnt;
	}
	struct tm tm_v;
	struct tm *p_tm;
	p_tm=&tm_v;
	localtime_r(&playback_time,p_tm);
	memset( str, 0, 10);
	
	#ifdef GMR_VER//ddmmyyyy
	snprintf( str, 40, "%02d/%02d/20%02d %02d:%02d:%02d",p_tm->tm_mday,p_tm->tm_mon+1, p_tm->tm_year-100,p_tm->tm_hour,p_tm->tm_min,p_tm->tm_sec);
	#else
	uint8 date_format=0;
	#if 0
	//API_io_server_read_local(GetMsgIDAccordingPid(pthread_self()),MEMO_DATE_FORMAT, (uint8*)&date_format);
	if(date_format == 1)	//DDMMYYYY
	{
		snprintf( str, 40, "%02d/%02d/20%02d %02d:%02d:%02d",p_tm->tm_mday,p_tm->tm_mon+1, p_tm->tm_year-100,p_tm->tm_hour,p_tm->tm_min,p_tm->tm_sec);
	}
	else if(date_format == 2)//mmddyyyy
	{
		snprintf( str, 40, "%02d/%02d/20%02d %02d:%02d:%02d",p_tm->tm_mon+1,p_tm->tm_mday, p_tm->tm_year-100,p_tm->tm_hour,p_tm->tm_min,p_tm->tm_sec);
	}
	else		//yymmdd
	{
		snprintf( str, 40, "%02d/%02d/%02d %02d:%02d:%02d", p_tm->tm_year-100, p_tm->tm_mon+1,p_tm->tm_mday,p_tm->tm_hour,p_tm->tm_min,p_tm->tm_sec);
	}
	#endif
	snprintf( str, 40, "20%02d-%02d-%02d %02d:%02d:%02d",p_tm->tm_year-100,p_tm->tm_mon+1,p_tm->tm_mday,p_tm->tm_hour,p_tm->tm_min,p_tm->tm_sec);
	
	#endif
	if(Playback_Info_State)
	{

	}
	printf("DisplayPlaybackTiming %s sec_cnt = %d\n",str,sec_cnt);
}

void Memo_Playback_Info_Clear(void)
{
	//API_OsdStringClearExt(MEMO_INFO_ROOM_X,MEMO_INFO_ROOM_Y,640,45);
	//API_OsdStringClearExt(MEMO_INFO_TIME_X,MEMO_INFO_TIME_Y,640,45);
	#if !defined(PID_IX850) && !defined(PID_IX611) && !defined(PID_IX622)&&!defined(PID_IX821)
	memo_info_display_off();
	#endif
}

void Memo_Playback_Info_On(char *filedir)
{
	Playback_Info_State = 1;

	Memo_Playback_Info_Display(filedir,0);
}

void Memo_Playback_Info_Off(void)
{
	Playback_Info_State = 0;

	Memo_Playback_Info_Clear();
}

void Memo_Playback_Info_Recover(char *filedir)
{
	Playback_Info_State = 1;

	Memo_Playback_Info_Display(filedir,1);
}
static char memo_rsp_buff_save[6];
static char memo_filepath_save[100];
static char memo_filedir_save[80];//get_memo_data_byroomid(filedir,&unread_tmp,&total_tmp,0);
void Memo_End_Callback(void *arg)
{	
	ffmpeg_rec_dat2* pin= (ffmpeg_rec_dat2*)arg;
	if(pin->CurSec>=15)
	{
		//AI_PushOneCmdIntoQueue(OS_Q_STACK_FRAME_TASK_TRS_APT_A, GA4_MEM_REPORT, APCI_GROUP_VALUE_WRITE, 1, 6,memo_rsp_buff_save);
		if(API_Para_Read_Int(DisablePIP)==1)
			AI_PushOneCmdIntoQueue(OS_Q_STACK_FRAME_TASK_TRS_APT_A, GA4_MEM_REPORT, APCI_GROUP_VALUE_WRITE, 1, 6,memo_rsp_buff_save);
	}
	else
	{
		int unread_tmp,total_tmp;
		
		//memo_rsp_buff_save[3]--;
		#if 0
		get_memo_data_byroomid(memo_filedir_save,&unread_tmp,&total_tmp,0);
		if(unread_tmp==0)
			total_tmp=0;
		memo_rsp_buff_save[2] = (unread_tmp>>8);
		memo_rsp_buff_save[3] = unread_tmp;
		memo_rsp_buff_save[4] = (total_tmp>>8);
		memo_rsp_buff_save[5] = total_tmp;
		//AI_PushOneCmdIntoQueue(OS_Q_STACK_FRAME_TASK_TRS_APT_A, GA4_MEM_REPORT, APCI_GROUP_VALUE_WRITE, 1, 6,memo_rsp_buff_save);
		#endif
		remove(memo_filepath_save);
	}
}
void Memo_Process_Callback(int rec_frame)
{
	if(rec_frame==15)
	{
		int unread_tmp,total_tmp;
		printf("Memo_Process_Callback++++\n");
		get_memo_data_byroomid(memo_filedir_save,&unread_tmp,&total_tmp,1);
		memo_rsp_buff_save[2] = (unread_tmp>>8);
		memo_rsp_buff_save[3] = unread_tmp;
		memo_rsp_buff_save[4] = (total_tmp>>8);
		memo_rsp_buff_save[5] = total_tmp;
		if(API_Para_Read_Int(DisablePIP)==0)
			AI_PushOneCmdIntoQueue(OS_Q_STACK_FRAME_TASK_TRS_APT_A, GA4_MEM_REPORT, APCI_GROUP_VALUE_WRITE, 1, 6,memo_rsp_buff_save);
	}
}
void Memo_Go_Request(int im_ph_addr,char *rsp_buff,int act_type)
{
	char filedir[100];
	char filepath[150];
	char timelabel[50];
	int im_addr;
	int sd_link;
	int unread_tmp,total_tmp;
	im_addr=(im_ph_addr&0x7f)>>2;
	if(im_addr==0)
		im_addr=32;
	pthread_mutex_lock(&momo_mon_lock);
	if(sd_link=Judge_SdCardLink())
	{
		sprintf(filedir,"%s/%04d",SdMemoDir,im_addr);
	}
	else
	{
		if(Local_Memo_List==NULL)
			MemoLocalNandListInit();
		sprintf(filedir,"%s/%04d",LocalMemoDir,im_addr);
	}
	create_multi_dir(filedir);
	
	//printf("111111111111111filedir:%s\n",filedir);
	
	//printf("111111111111111filepath:%s\n",filepath);
	if(act_type==0)
	{
		create_video_record_timelabel(timelabel);
		sprintf(filepath,"%s/CMR_%s.mp4",filedir,timelabel);
		strcpy(memo_filepath_save,filepath);
		get_memo_data_byroomid(filedir,&unread_tmp,&total_tmp,0);
		// log_d("GA5_MEM_START8888888888");
		rsp_buff[0] = im_ph_addr>>8;
		rsp_buff[1] = im_ph_addr;
		rsp_buff[2] = (unread_tmp>>8);
		rsp_buff[3] = unread_tmp;
		rsp_buff[4] = (total_tmp>>8);
		rsp_buff[5] = total_tmp;
		memcpy(memo_rsp_buff_save,rsp_buff,6);
		strcpy(memo_filedir_save,filedir);
		if(sd_link)
		{
			api_rec_one_file_start(0,0,filepath, API_Para_Read_Int(DisablePIP)?3:10,Memo_End_Callback);
			//MemoRoomSpaceControl(filedir,MaxSDMemoNum_PerRoom);
			MemoSdSpaceControl();
		}
		else
		{
			//log_d("GA5_MEM_START11111111");
			api_rec_one_file_start(0,0,filepath, 3,Memo_End_Callback);
			//log_d("GA5_MEM_START6666666666");
			sprintf(filepath,"CMR_%s.mp4",timelabel);
			AddMemoToLocalNand(filedir,filepath);
			//MemoRoomSpaceControl(filedir,MaxLocalMemoNum_PerRoom);
			//log_d("GA5_MEM_START7777777777");
		}
		
		//if(unread_tmp>0)
		//	unread_tmp--;
		//rsp_buff[2] = (unread_tmp>>8);
		//rsp_buff[3] = unread_tmp;
	}
	else
	{
		DelMemoToLocalNandEntry(filedir,NULL);
		sprintf(filepath,"rm -r %s",filedir);
		system(filepath);
		get_memo_data_byroomid(filedir,&unread_tmp,&total_tmp,0);
		// log_d("GA5_MEM_START8888888888");
		rsp_buff[0] = im_ph_addr>>8;
		rsp_buff[1] = im_ph_addr;
		rsp_buff[2] = 0;
		rsp_buff[3] = 0;
		rsp_buff[4] =0;
		rsp_buff[5] = 0;
	}
	
	pthread_mutex_unlock(&momo_mon_lock);
}
void Memo_Stop(void)
{
	pthread_mutex_lock(&momo_mon_lock);
	api_channel_rec_stop(0);
	pthread_mutex_unlock(&momo_mon_lock);
}
void Memo_Playback_CB(int sec, int max)
{
	//pthread_mutex_lock(&momo_mon_lock);
	DisplayPlaybackTiming(sec);
	//pthread_mutex_unlock(&momo_mon_lock);
}
int Memo_Playback_Request(int im_ph_addr,char *rsp_buff,int time_out)
{

	char filedir[100];
	char filepath[150];
	//char timelabel[50];
	int im_addr;
	int sd_link;
	//int unread_tmp,total_tmp;
	if(Playback_State==1)
		return 1;
	im_addr=(im_ph_addr&0x7f)>>2;
	if(im_addr==0)
		im_addr=32;
	if(sd_link=Judge_SdCardLink())
	{
		sprintf(filedir,"%s/%04d",SdMemoDir,im_addr);
		MemoRoomSpaceControl(filedir,MaxSDMemoNum_PerRoom);
	}
	else
	{
		sprintf(filedir,"%s/%04d",LocalMemoDir,im_addr);
		MemoRoomSpaceControl(filedir,MaxLocalMemoNum_PerRoom);
	}
	//create_multi_dir(filedir);
	pthread_mutex_lock(&momo_mon_lock);
	if(memo_get_recordlist(filedir) == 0)
	{
		Playback_State=1;
		Playback_AutoClose_Timer=0;
		#if 0
		if(req_playback_monlink() != 0)
	 	{
	 		memo_release_recordlist();
	 		send_memo_rsp_msg(memo_msg->msg_head.msg_type, memo_msg->msg_head.msg_sub_type, 1,0,0);
			return;
	 	}
		#endif
		current_index = 0;
		
		api_stop_ffmpegPlay();
		
		cur_room_readed = 0;
		get_memo_data_byroomid(filedir,&cur_room_unread,&cur_room_total,0);
		
		if(cur_room_total != total_record)
		{
			cur_room_total = total_record;
			set_memo_total_byroomid(filedir,cur_room_total);
			
			
		}
		if(cur_room_unread > cur_room_total)
		{
			cur_room_unread = cur_room_total;
			set_memo_unread_byroomid(filedir,cur_room_unread);
		}

		//send_memo_rsp_msg(memo_msg->msg_head.msg_type, memo_msg->msg_head.msg_sub_type, 0,cur_room_unread,cur_room_total); 
		sprintf(filepath,"%s/%s",filedir,precord_list[current_index].filename);
		#if !defined(PID_IX850) && !defined(PID_IX611) && !defined(PID_IX622)&&!defined(PID_IX821)
		video_turn_on_init();
		#endif
		Memo_Playback_Info_On(filedir);
		api_start_ffmpegPlay(filepath, Memo_Playback_CB);
		//Enter_RemoteMenu(dev_type,dev_addr,RMENU_001_MonSelect);
		//StartInitOneMenu(MENU_009_DsCalling,0,0);
		//API_Camera_On();
		//Memo_Playback_Info_On();//Memo_Playback_Info_Display();
		//API_OsdStringDisplay(50, 50, COLOR_BLUE,precord_list[current_index].filename);
		//if(cur_room_unread > 0)
			//cur_room_unread --;
		if(cur_room_unread > 0)
		{
			cur_room_readed = cur_room_unread;	//czn_20171006
			set_memo_unread_byroomid(filedir,cur_room_unread-cur_room_readed);
		}
		rsp_buff[0] = im_ph_addr>>8;
		rsp_buff[1] = im_ph_addr;
		rsp_buff[2] = (cur_room_unread>>8);
		rsp_buff[3] = cur_room_unread;
		rsp_buff[4] = (cur_room_total>>8);
		rsp_buff[5] = cur_room_total;
		
	}
	else
	{
		rsp_buff[0] = im_ph_addr>>8;
		rsp_buff[1] = im_ph_addr;
		rsp_buff[2] = 0;
		rsp_buff[3] = 0;
		rsp_buff[4] = 0;
		rsp_buff[5] = 0;
		//send_memo_rsp_msg(memo_msg->msg_head.msg_type, memo_msg->msg_head.msg_sub_type, 1,0,0); 
		//close_playback_monlink();
	}
	 pthread_mutex_unlock(&momo_mon_lock);
	return Playback_State;
}
void Memo_Playback_AutoClose(void)
{
	if(Playback_State)
	{
		if(Playback_AutoClose_Timer++>35)
		{
			Memo_Go_off();
		}
	}
}
void Memo_Playback_Ctrl(int im_ph_addr,int opt,char *rsp_buff)
{
	if(Playback_State==0||precord_list==NULL)
		return;
	char filedir[100];
	char filepath[150];
	//char timelabel[50];
	int im_addr;
	int sd_link;
	//int unread_tmp,total_tmp;
	im_addr=(im_ph_addr&0x7f)>>2;
	if(im_addr==0)
		im_addr=32;
	if(sd_link=Judge_SdCardLink())
	{
		sprintf(filedir,"%s/%04d",SdMemoDir,im_addr);
	}
	else
	{
		sprintf(filedir,"%s/%04d",LocalMemoDir,im_addr);
	}
	pthread_mutex_lock(&momo_mon_lock);
	Playback_AutoClose_Timer=0;
	if(opt == 0)  
	{
		//API_MEMO_PLY_LAST();
		current_index--;
				
		if( current_index < 0 )  
			current_index = cur_room_total - 1;
		//api_stop_ffmpegPlay();
		
		//printf("-----M_C_PLAYBACK_LAST------\r\n");
		sprintf(filepath,"%s/%s",filedir,precord_list[current_index].filename);
		//video_turn_on_init();
		//api_start_ffmpegPlay(filepath, NULL);
		if(api_change_file_ffmpegPlay(filepath, Memo_Playback_CB)==0)
			;//printf("111111111111chage[%s] ok\n",filepath);
		else
		{
			api_stop_ffmpegPlay();
			api_start_ffmpegPlay(filepath, Memo_Playback_CB);
			//printf("2222222222222chage[%s] fail\n",filepath);
		}
		Memo_Playback_Info_On(filedir);
	}
	else if(opt == 1)  
	{
	  	current_index++;
				
		if( current_index >= cur_room_total )  
			current_index = 0;
		
		//api_stop_ffmpegPlay();
		
		//printf("-----M_C_PLAYBACK_NEXT------\r\n");
		sprintf(filepath,"%s/%s",filedir,precord_list[current_index].filename);
		//video_turn_on_init();
		//api_start_ffmpegPlay(filepath, NULL);
		if(api_change_file_ffmpegPlay(filepath, Memo_Playback_CB)==0)
			;//printf("111111111111chage[%s] ok\n",filepath);
		else
		{
			api_stop_ffmpegPlay();
			api_start_ffmpegPlay(filepath, Memo_Playback_CB);
			//printf("2222222222222chage[%s] fail\n",filepath);
		}
		Memo_Playback_Info_On(filedir);
		//API_MEMO_PLY_NEXT();	
	}
	else if ( (opt == 2) || (opt == 5) )
	{
	  	if(total_record > 0)
		{
			cur_room_total = total_record - 1;
			set_memo_total_byroomid(filedir,cur_room_total);
			
			if(cur_room_readed > current_index)
			cur_room_readed--;	
			
			if(cur_room_unread > current_index)
			cur_room_unread--;	
			
			set_memo_unread_byroomid(filedir,cur_room_unread - cur_room_readed);
			
		
			//send_memo_rsp_msg(memo_msg->msg_head.msg_type, memo_msg->msg_head.msg_sub_type, 0,cur_room_unread-cur_room_readed,cur_room_total);
			api_stop_ffmpegPlay();
			usleep(200000);
			sprintf(filepath,"%s/%s",filedir,precord_list[current_index].filename);
			remove(filepath);
			DelMemoToLocalNandEntry(filedir,precord_list[current_index].filename);
			memo_release_recordlist();
			if(memo_get_recordlist(filedir) == 0)
			{
				//send_memo_rsp_msg(memo_msg->msg_head.msg_type, memo_msg->msg_head.msg_sub_type, 0,cur_room_unread-cur_room_readed,cur_room_total);

				if(current_index >= total_record)
					current_index = 0;

				//memo_video_playback_start( precord_list[current_index].filename, NULL); 
				//Memo_Playback_Info_Display(0);
				sprintf(filepath,"%s/%s",filedir,precord_list[current_index].filename);
				api_start_ffmpegPlay(filepath, Memo_Playback_CB);
				Memo_Playback_Info_On(filedir);
			}
			else
			{
				memo_release_recordlist();
				api_stop_ffmpegPlay();
				Playback_State=0;
				if(SR_State.in_use == TRUE)
				{
					SR_Routing_Close(CT11_ST_MONITOR);
				}
			}
			printf("-----M_C_PLAYBACK_DELETE------\r\n");
		}
		//API_MEMO_DELETE();
	}
	else if ( (opt == 3) || (opt== 6) )
	{
	  	//API_MEMO_DELETE_USER_ALL();//???(??: ???????????);	//MEM_???????????	//zfz_sc6
	  	
		memo_release_recordlist();
		api_stop_ffmpegPlay();
		Memo_Playback_Info_Off();
		DelMemoToLocalNandEntry(filedir,NULL);
		sprintf(filepath,"rm -r %s",filedir);
		system(filepath);
		Playback_State=0;
		if(SR_State.in_use == TRUE)
		{
			SR_Routing_Close(CT11_ST_MONITOR);
		}
	}
	else if(opt == 4)
	{
		//???(??: ???????????);	//MEM_???????????
		//API_MEMO_FORMAT();  
	}
	else if ( (opt== 8) )
	{
	  	//PI_MEMO_PLY_INFO_OFF();//???(??: ???????????);	//MEM_???????????	//zfz_sc6
	  	Memo_Playback_Info_Off();
	}
	else if ( (opt == 7) )
	{
		Memo_Playback_Info_Recover(filedir);
	  	//API_MEMO_PLY_INFO_ON();//???(??: ???????????);	//MEM_???????????	//zfz_sc6
	}
	pthread_mutex_unlock(&momo_mon_lock);
}
 void ClearPlaybackSrRequest(void)
 {
 	pthread_mutex_lock(&momo_mon_lock);
 	if(Playback_State==1)
 	{
 		memo_release_recordlist();
		api_stop_ffmpegPlay();
		Memo_Playback_Info_Off();
		set_menu_with_video_off();
		Playback_State=0;
 	}
	pthread_mutex_unlock(&momo_mon_lock);
 }
 void Memo_Go_off(void)
 {
 	pthread_mutex_lock(&momo_mon_lock);
	if(Playback_State==1)
 	{
 		
 		memo_release_recordlist();
		api_stop_ffmpegPlay();
		Memo_Playback_Info_Off();
		set_menu_with_video_off();
		Playback_State=0;
		if(SR_State.in_use == TRUE)
		{
			SR_Routing_Close(CT11_ST_MONITOR);
		}
 	}
	pthread_mutex_unlock(&momo_mon_lock);
 }
 int Get_Playback_State(void)
 {
 	return Playback_State;
 }


