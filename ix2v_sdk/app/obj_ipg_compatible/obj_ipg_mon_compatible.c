#include <stdio.h>
#include <sys/sysinfo.h>
#include "task_Shell.h"
#include "task_survey.h"
#include "obj_GetIpByInput.h"
#include "ip_video_cs_control.h"
#include "obj_ak_decoder.h"
#include "obj_UnitSR.h"
#include "define_Command.h"
#include "define_file.h"
#include "obj_IoInterface.h"
#include "obj_IXS_Proxy.h"

static int QuadMonState=0;
pthread_mutex_t momo_mon_lock = PTHREAD_MUTEX_INITIALIZER;
typedef struct
{
	int  monIndex;
	int  monState;		//open/close
	int  deviceType;	//ipc/ds
	int  vdType;		//264/265
	int  recState;
	char recFile[41];
	char deviceName[41];
}Mon_Quad_Menu_Para_t;

Mon_Quad_Menu_Para_t MonMenuWin[4]={0};

void Get_OneIpc_ShowPos(int win_id, int* x, int* y, int* width, int* height)
{
	int vdshowH;
	const int bkgd_w1=720;
	const int bkgd_h1=576;

	switch(win_id)
	{
		case 0:
			*x = 0;
			*y = 0;
			*width = bkgd_w1;
			*height = bkgd_h1;
			break;
		case 1:
			*x = 0;
			*y = 0;
			*width = bkgd_w1/2;
			*height = bkgd_h1/2;
			break;
		case 2:
			*x = bkgd_w1/2;
			*y = 0;
			*width = bkgd_w1/2;
			*height = bkgd_h1/2;
			break;
		case 3:
			*x = 0;
			*y = bkgd_h1/2;
			*width = bkgd_w1/2;
			*height = bkgd_h1/2;
			break;
		case 4:
			*x = bkgd_w1/2;
			*y = bkgd_h1/2;
			*width = bkgd_w1/2;
			*height = bkgd_h1/2;
			break;
		case 5:
			//608, 30, 320, 240,
			*x = 360;
			*y = 30;
			*width = 320;
			*height = 240;
			break;
	}
	
}
cJSON *Get_OneIpc_ShowPos_json(int win_id)
{
	int x,y,w,h;
	if(win_id>5)
		return NULL;
	Get_OneIpc_ShowPos(win_id,&x,&y,&w,&h);
	
	cJSON *win=cJSON_CreateObject();
	
	cJSON_AddNumberToObject(win,"X",x);
	cJSON_AddNumberToObject(win,"Y",y);
	cJSON_AddNumberToObject(win,"W",w);
	cJSON_AddNumberToObject(win,"H",h);

	return win;
}
int GetWinMonState(int win_id)
{
	return MonMenuWin[win_id].monState;
}
void SetWinMonState(int win_id, int state)
{
	MonMenuWin[win_id].monState = state;
}
int GetWinRecState(int win_id)
{
	return MonMenuWin[win_id].recState;
}
void SetWinRecState(int win_id, int state)
{
	MonMenuWin[win_id].recState = state;
}
void GetWinRecFile(int win_id, char* name)
{
	strcpy(name, MonMenuWin[win_id].recFile);
}
void SetWinRecFile(int win_id, char* name)
{
	strcpy(MonMenuWin[win_id].recFile, name);
}
void GetWinDeviceName(int win_id, char* name)
{
	strcpy(name, MonMenuWin[win_id].deviceName);
}
void SetWinDeviceName(int win_id, char* name)
{
	strcpy(MonMenuWin[win_id].deviceName, name);
}
int GetWinDeviceType(int win_id)
{
	return MonMenuWin[win_id].deviceType;
}
void SetWinDeviceType(int win_id, int type)
{
	MonMenuWin[win_id].deviceType = type;
}
int GetWinVdType(int win_id)
{
	return MonMenuWin[win_id].vdType;
}
void SetWinVdType(int win_id, int type)
{
	MonMenuWin[win_id].vdType = type;
}

//#define QuadIPCListFile 	"/mnt/nand1-2/UserRes/quadipclist.cfg"

//static cJSON *QuadIPCList=NULL;

int Get_QuadIpcNum(void)
{
	int num;
	cJSON *quad_list;
	quad_list=API_Para_Read_Public(IPC_Quad_List);
	num=cJSON_GetArraySize(quad_list);
	return (num>3?3:num);
}	

cJSON *Get_OneQuadIpcInfo(int index)
{
	///int quad_ch;
	//cJSON *ipc_item;
	//cJSON *ch_array;
	int num;
	cJSON *quad_list;
	
	quad_list=API_Para_Read_Public(IPC_Quad_List);
	num=cJSON_GetArraySize(quad_list);
	
	if(index>=num)
		return NULL;
	return Get_IPCChInfo_ByIP(cJSON_GetArrayItem(quad_list,index)->valuestring,"CH_QUAD");
}
int Get_OneQuadIpcName(int index,char *name)
{
	int num;
	cJSON *quad_list;
	
	quad_list=API_Para_Read_Public(IPC_Quad_List);
	num=cJSON_GetArraySize(quad_list);
	
	if(index>=num)
		return -1;
	return Get_IPCChName_ByIP(cJSON_GetArrayItem(quad_list,index)->valuestring,name);
}

cJSON *Get_OnePipIpcInfo(void)
{
	char *pip_bind=API_Para_Read_String2(IPC_PIP_BIND);
	if(pip_bind==NULL||strlen(pip_bind)==0)
		return NULL;
	return Get_IPCChInfo_ByIP(pip_bind,"CH_QUAD");
}

int Get_OnePipIpcName(char *name)
{
	char *pip_bind=API_Para_Read_String2(IPC_PIP_BIND);
	if(pip_bind==NULL||strlen(pip_bind)==0)
		return -1;
	return Get_IPCChName_ByIP(pip_bind,name);
}

int API_MonQuad_start( void )
{
	int i,select,x,y,w,h;
	char name_temp[41];
	//char bdRmMs[11];
	cJSON *target,*win;
	//LoadSpriteDisplay(1);
	//if(QuadMonState==1)
	//	return 0;
	if(Get_QuadIpcNum()==0)
	{
		//i=0;
		if(GetWinMonState(0))
		{
			return;
		}
		if(API_Para_Read_Int(DisablePIP))
			en_vi_cvbs();
		api_ak_vi_ch_stream_preview_on(0,0);
		SetWinMonState(0, 1);
		SetWinDeviceType(0, 3);
		//SetDxDs_Show_Win(i);
		SetWinDeviceName(0,"821-DX");
		for( i = 1; i < 4; i++ )
			SetWinMonState(i, 0);
	}
	else
	{
		i=Get_QuadIpcNum()>3?3:Get_QuadIpcNum();
		dis_vi_cvbs();
		if(GetWinMonState(i))
		{
			VdShowToQuadOne(i);
			//continue;
		}
		else
		{
			api_ak_vi_ch_stream_preview_on(0,i+1);
			SetWinMonState(i, 1);
			SetWinDeviceType(i, 3);
			//SetDxDs_Show_Win(i);
			SetWinDeviceName(i,"821-DX");
		}
		for( i = 0; i < 3; i++ )
		{
			if(i<Get_QuadIpcNum())
			{
				if(GetWinMonState(i))
				{
					VdShowToQuadOne(i);
					continue;
				}
				target=Get_OneQuadIpcInfo(i);
				win =Get_OneIpc_ShowPos_json(i+1);
				Start_OneIpc_Show(i+1,target,win,0);
				SetWinMonState(i, 1);
				SetWinDeviceType(i, 1);
				//SetDxDs_Show_Win(i);
				Get_OneQuadIpcName(i, name_temp);
				SetWinDeviceName(i,name_temp);
				cJSON_Delete(win);
				cJSON_Delete(target);
			}
			#if 0
			else
			{
				SetWinMonState(i, 0);
				break;
			}
			#endif
		}
		
	}
	//QuadMonState=1;
	return 0;
}

int API_MonPip_start( void )
{
	int i,select,x,y,w,h;
	char name_temp[41];
	//char bdRmMs[11];
	cJSON *target,*win;
	//LoadSpriteDisplay(1);
	//if(QuadMonState==1)
	//	return 0;
	if(GetWinMonState(Get_QuadIpcNum()>3?3:Get_QuadIpcNum())==0)
	{
		api_ak_vi_ch_stream_preview_on(0,0);
		SetWinMonState(Get_QuadIpcNum()>3?3:Get_QuadIpcNum(), 1);
		SetWinDeviceType(Get_QuadIpcNum()>3?3:Get_QuadIpcNum(), 3);
		//SetDxDs_Show_Win(i);
		SetWinDeviceName(Get_QuadIpcNum()>3?3:Get_QuadIpcNum(),"821-DX");
	}
	if(GetWinMonState(0)==0)
	{
		target=Get_OnePipIpcInfo();
		if(target!=NULL)
		{
			win =Get_OneIpc_ShowPos_json(5);
			Start_OneIpc_Show(1,target,win,0);
			SetWinMonState(0, 1);
			SetWinDeviceType(0, 1);
			//SetDxDs_Show_Win(i);
			Get_OnePipIpcName(name_temp);
			SetWinDeviceName(0,name_temp);
			cJSON_Delete(win);
			cJSON_Delete(target);
		}
	}
	
	
	
	
	//QuadMonState=1;
	return 0;
}
void VdShowHide(void)
{
	int i;
	//if(QuadMonState==0)
	//	return;
	for( i = 0; i < 4; i++ )
	{
		if(GetWinMonState(i) == 1)
		{
			if(GetWinDeviceType(i) == 1)
				Clear_ds_show_layer(i+1);
			else if(GetWinDeviceType(i) == 3)
			{
				api_ak_vi_ch_stream_preview_hide(0);
			}
			
		}
	}
}
void VdShowToFull(int win_id)
{
	//LoadSpriteDisplay(1);
	//if(QuadMonState==0)
	//	return;
	if(GetWinMonState(win_id)==0)
		return;
	if(GetWinDeviceType(win_id) == 1)
	{
		if(API_Para_Read_Int(DisablePIP))
			dis_vi_cvbs();
		Set_ds_show_pos(win_id+1, 0, 0, 720,576);
	}
	else if(GetWinDeviceType(win_id) == 3)
	{
		if(API_Para_Read_Int(DisablePIP))
			en_vi_cvbs();
		api_ak_vi_ch_stream_preview_change_win(0,0);
	}	
	
		
	//LoadSpriteDisplay(0);
}
void VdShowToQuad(void)
{
	int i,x,y,w,h;
	
	//LoadSpriteDisplay(1);
	//if(QuadMonState==0)
	//	return;
	for( i = 0; i < 4; i++ )
	{
		if(GetWinMonState(i) == 1)
		{
			Get_OneIpc_ShowPos(i+1,&x,&y,&w,&h);
			if(GetWinDeviceType(i) == 1)
				Set_ds_show_pos(i, x, y, w, h);
			else if(GetWinDeviceType(i) == 3)
			{
				api_ak_vi_ch_stream_preview_change_win(0,i+1);
			}
		}
	}
	//LoadSpriteDisplay(0);
}
void VdShowToQuadOne(int win)
{
	int x,y,w,h;
	if(GetWinMonState(win) == 1)
	{
		Get_OneIpc_ShowPos(win+1,&x,&y,&w,&h);
		if(GetWinDeviceType(win) == 1)
			Set_ds_show_pos(win-1, x, y, w, h);
		else if(GetWinDeviceType(win) == 3)
		{
			api_ak_vi_ch_stream_preview_change_win(0,win+1);
		}
	}
}
#if 0
void EnterQuadMenu(int win_id, char* deviceName, int deviceType)
{
	printf("VdShowToQuad win= %d\n", win_id);
	if(QuadMonState==0)
		return;
	if(GetWinDeviceType(win_id) == 1)
		Clear_ds_show_layer(win_id-1);
	else if(GetWinDeviceType(win_id) == 3)
	{
		api_ak_vi_ch_stream_preview_hide(0);
	}
	
	VdShowToQuad();
}
#endif

void EnterMonFull(int win_id)
{
	//if(QuadMonState==0)
	//	return;
	char name_temp[41];
	cJSON *target,*win;
	
	VdShowHide();
	
	if(GetWinMonState(win_id) == 0)
	{
		
		if(win_id==Get_QuadIpcNum()||win_id==3)
		{
			#if !defined(PID_IX850) && !defined(PID_IX611) && !defined(PID_IX622)&&!defined(PID_IX821)
			video_turn_on_init();
			#endif
			if(API_Para_Read_Int(DisablePIP))
				en_vi_cvbs();
			api_ak_vi_ch_stream_preview_on(0,0);
			SetWinMonState(win_id, 1);
			SetWinDeviceType(win_id, 3);
			//SetDxDs_Show_Win(i);
			SetWinDeviceName(win_id,"821-DX");
		}
		else if(win_id<Get_QuadIpcNum())
		{
			//printf("111111111112222222222222\n");
			#if !defined(PID_IX850) && !defined(PID_IX611) && !defined(PID_IX622)&&!defined(PID_IX821)
			video_turn_on_init();
			#endif
			if(API_Para_Read_Int(DisablePIP))
				dis_vi_cvbs();
			//printf("2222222222222222\n");
			target=Get_OneQuadIpcInfo(win_id);
			//printf("33333333333333333333\n");
			win =Get_OneIpc_ShowPos_json(0);
			//printf("44444444444444444444\n");
			Start_OneIpc_Show(win_id+1,target,win,0);
			//printf("5555555555555555\n");
			SetWinMonState(win_id, 1);
			SetWinDeviceType(win_id, 1);
			//SetDxDs_Show_Win(i);
			Get_OneQuadIpcName(win_id, name_temp);
			SetWinDeviceName(win_id,name_temp);
			cJSON_Delete(win);
			cJSON_Delete(target);
		}
	}
	else
		VdShowToFull(win_id);
	
}
#if 0
void EnterViMonFull(int win_id, int stack)
{
	if(GetWinMonState(win_id) == 0)
		return;
	//dsWinId = win_id;
	if(stack == 0)
	{
		VdShowHide();
	}
	//StartInitOneMenu(MENU_028_MONITOR2,0,stack);
	if(stack == 0)
	{
		VdShowToFull(win_id);
	}
	else
	{
		//SetWinMonState(dsWinId, 1);
		//SetWinDeviceType(dsWinId, 0);
	}
}
#endif
void ExitQuadMenu(void)
{
	int i;
	printf("ExitQuadMenu state=%d Type=%d \n", GetWinMonState(0), GetWinDeviceType(0));	
	//if(QuadMonState==0)
	//	return;
	set_menu_with_video_off();
	for( i = 0; i < 4; i++ )
	{
		if(GetWinMonState(i) == 1)
		{
			SetWinMonState(i, 0);
			if(GetWinDeviceType(i) == 1)
				Stop_OneIpc_Show(i+1);
			else if(GetWinDeviceType(i) == 3)
			{
				api_ak_vi_ch_stream_preview_off(0);
			}
		}
		//if(GetWinRecState(i) == 1)
		//	api_record_stop(i);
	}
	if(API_Para_Read_Int(DisablePIP))
		dis_vi_cvbs();
	//QuadMonState=0;
	//Quad_monitor_time = 0;
	//OS_StopTimer(&timer_Quad_monitor_stop);
	//AutoPowerOffReset();
	
	//API_Business_Close(Business_State_MonitorIpc);

}
 int Video_Go_Request(int video_id)
 {
 	pthread_mutex_lock(&momo_mon_lock);
	#if 1
	if(video_id>0&&QuadMonState==0)
	{
		if(API_SR_Request(CT11_ST_MONITOR)!=0)
		{
			pthread_mutex_unlock(&momo_mon_lock);
			return 0;
		}
	}
	#endif
	if(video_id==0&&QuadMonState==1)
	{
		if(SR_State.in_use == TRUE)
		{
			SR_Routing_Close(CT11_ST_MONITOR);
		}
		ExitQuadMenu();
		CloseDtTalk();
		QuadMonState=0;

	}
	if(video_id>=1&&video_id<=4&&QuadMonState==2)
	{
		EnterMonFull(Get_QuadIpcNum()>3?3:Get_QuadIpcNum());	
	}
	if(video_id>=5&&video_id<=7)
	{
		if(QuadMonState==0)
			QuadMonState=1;
		//printf("11111111111\n");
		EnterMonFull(video_id-5);
		if(SR_State.in_use == FALSE)
		{
			SR_Routing_Create(CT11_ST_MONITOR);
		}
	}
	pthread_mutex_unlock(&momo_mon_lock);
	return QuadMonState;
 }
 void Video_MonOn_Request(void)
 {
 		
 		pthread_mutex_lock(&momo_mon_lock);
 		if(QuadMonState==0)
 		{
 			if(API_SR_Request(CT11_ST_MONITOR)!=0)
 			{
 				pthread_mutex_unlock(&momo_mon_lock);
				return;
 			}
			QuadMonState=1;
			#if !defined(PID_IX850) && !defined(PID_IX611) && !defined(PID_IX622)&&!defined(PID_IX821)
			video_turn_on_init();
			#endif
			
			if(Get_QuadIpcNum()==1&&API_Para_Read_Int(DisablePIP)==0)
			{
				API_MonPip_start();
			}
			else
				API_MonQuad_start();
			#if !defined(PID_IX850) && !defined(PID_IX611) && !defined(PID_IX622)&&!defined(PID_IX821)
			video_turn_on_init();
			#endif
 		}
		else
		{
			EnterMonFull(Get_QuadIpcNum()>3?3:Get_QuadIpcNum());
		}
		if(SR_State.in_use == FALSE)
		{
			SR_Routing_Create(CT11_ST_MONITOR);
		}
		pthread_mutex_unlock(&momo_mon_lock);
 }
 void Video_MonOff_Request(void)
 {
 	pthread_mutex_lock(&momo_mon_lock);
 	if(QuadMonState==1)
 	{
 		
		ExitQuadMenu();	
		CloseDtTalk();
		QuadMonState=0;
		usleep(100*1000);
		if(SR_State.in_use == TRUE)
		{
			SR_Routing_Close(CT11_ST_MONITOR);
		}
 	}
	DebugStateOnReport();
	pthread_mutex_unlock(&momo_mon_lock);
 }
 void Video_MonTalk_Request(void)
 {
 	pthread_mutex_lock(&momo_mon_lock);
	if(QuadMonState==1)
	{
		EnterMonFull(Get_QuadIpcNum()>3?3:Get_QuadIpcNum());
		OpenDtTalk();
	}
	pthread_mutex_unlock(&momo_mon_lock);
 }
 void ClearMonSrRequest(void)
 {
 	pthread_mutex_lock(&momo_mon_lock);
 	if(QuadMonState==1)
 	{
		ExitQuadMenu();	
		CloseDtTalk();
		QuadMonState=0;
 	}
	pthread_mutex_unlock(&momo_mon_lock);
 }
 void open_call_video(void)
 {
 	
 	pthread_mutex_lock(&momo_mon_lock);

	#if !defined(PID_IX850) && !defined(PID_IX611) && !defined(PID_IX622)&&!defined(PID_IX821)
	
	
	if(QuadMonState)
	{
		ExitQuadMenu();	
		CloseDtTalk();
	}
	
	video_turn_on_init();
	#endif
	//API_MonQuad_start();
	if(API_Para_Read_Int(DisablePIP))
	{
		char temp[10];
		
		if(GetWinMonState(Get_QuadIpcNum()>3?3:Get_QuadIpcNum())==0)
		{
			if(API_Para_Read_Int(DisableAutoMemo)==0)
			{
				pthread_mutex_unlock(&momo_mon_lock);
				Memo_Go_Request(GetTargetDtAddr(),temp,0);
				pthread_mutex_lock(&momo_mon_lock);
			}
			en_vi_cvbs();
			api_ak_vi_ch_stream_preview_on(0,0);
			SetWinMonState(Get_QuadIpcNum()>3?3:Get_QuadIpcNum(), 1);
			SetWinDeviceType(Get_QuadIpcNum()>3?3:Get_QuadIpcNum(), 3);
			//SetDxDs_Show_Win(i);
			SetWinDeviceName(Get_QuadIpcNum()>3?3:Get_QuadIpcNum(),"821-DX");
		}
	}
	else
		API_MonPip_start();
	QuadMonState=2;
	pthread_mutex_unlock(&momo_mon_lock);
	//log_d("open_call_video ED");
 }
 void close_call_video(void)
 {
	pthread_mutex_lock(&momo_mon_lock);
	#if 1
	set_menu_with_video_off();
	//api_ak_vi_ch_stream_preview_off(0);
	ExitQuadMenu();
	QuadMonState=0;
	#endif
	pthread_mutex_unlock(&momo_mon_lock);
 }
 void PipShowSwitch(void)
{
	pthread_mutex_lock(&momo_mon_lock);
	int layer1 = get_ds_show_layer(0);
	int layer2 = get_ds_show_layer(1);
	
	printf("---PipShowSwitch layer1=%d layer2=%d\n",layer1, layer2);
	set_ds_show_layer(0, layer2);
	set_ds_show_layer(1, layer1);
	pthread_mutex_unlock(&momo_mon_lock);
	
}
 
 void PipShowReset(void)
{
	pthread_mutex_lock(&momo_mon_lock);
	//printf("---PipShowSwitch layer1=%d layer2=%d\n",layer1, layer2);
	set_ds_show_layer(0, 0);
	set_ds_show_layer(1, 1);
	pthread_mutex_unlock(&momo_mon_lock);
	
} 
void SetCallDsViToFull(void)
{
	pthread_mutex_lock(&momo_mon_lock);
	if(QuadMonState)
	{
			if(GetWinMonState(0) == 1)
			{
				SetWinMonState(0, 0);
				if(GetWinDeviceType(0) == 1)
					Stop_OneIpc_Show(0+1);
				#if 0
				else if(GetWinDeviceType(i) == 3)
				{
					api_ak_vi_ch_stream_preview_off(0);
				}
				#endif
			}
		EnterMonFull(Get_QuadIpcNum()>3?3:Get_QuadIpcNum());

	}

	pthread_mutex_unlock(&momo_mon_lock);
}

void MonitorToUnlock(int lock_id)
{
#if 0	
	log_d("DxMainCall:Unlock%d",lock_id);
	//if(QuadMonState!=1)
	//	return;
	cJSON *event;
	cJSON *target_array;
	char *event_str;
	event=cJSON_CreateObject();
	if(event!=NULL)
	{
		#if 0
		cJSON_AddStringToObject(event, "EventName","CB_CALL_UNLOCK_REQ");
		cJSON_AddNumberToObject(event, "LOCK_ID",lock_id);
		cJSON_AddStringToObject(event, "CALL_SCENE",callscene_str[CallServer_Run.call_type]);
		cJSON_AddStringToObject(event,"CALL_STATE",callserver_state_str[CallServer_Run.state]);
		target_array=cJSON_Duplicate(CallServer_Run.target_array,1);
		cJSON_AddItemToObject(event,"USER_INFO",target_array);
		#else
		//{"EventName":"EventLockReq","LOCK_NBR":"RL1","LOCK_PROCESS":true,"TIME":0}	
		cJSON_AddStringToObject(event, "EventName","EventLockReq");
		cJSON_AddStringToObject(event, "LOCK_NBR",lock_id==1?"RL1":"RL2");
		cJSON_AddTrueToObject(event, "LOCK_PROCESS");
		cJSON_AddNumberToObject(event, "TIME",0);
		cJSON_AddStringToObject(event, "SOURCE","Monitor");
		//cJSON_AddStringToObject(event, "CALL_SCENE",callscene_str[CallServer_Run.call_type]);
		//cJSON_AddStringToObject(event,"CALL_STATE",callserver_state_str[CallServer_Run.state]);
		//target_array=cJSON_Duplicate(CallServer_Run.target_array,1);
		//cJSON_AddItemToObject(event,"USER_INFO",target_array);
		#endif
		
		event_str=cJSON_Print(event);
		if(event_str)
		{
			API_Event(event_str);
			free(event_str);
		}
		cJSON_Delete(event);
	}
#else	
	//log_d("DxMainCall:Unlock%d",lock_id);
	if(QuadMonState!=1)
		return;

	API_Event_Unlock(lock_id==1?"RL1":"RL2", "Monitor");
#endif	
}

void SipMonitorToUnlock(int lock_id)
{
	API_Event_Unlock(lock_id==1?"RL1":"RL2", "SipMonitor");
}
void Get_QuadMonState(void)
{
	return QuadMonState;
}

