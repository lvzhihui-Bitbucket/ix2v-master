#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "cJSON.h"
#include "obj_APP_IPC_List.h"
//#include "obj_IPCTableSetting.h"
#include "define_file.h"
#include "obj_IoInterface.h"
/*
{
    "IPC":{
    	"ID": "CAM1", 
    	"NAME":"CAM1", 
    	"IP": "192.168.243.6", 
    	"CH_ALL": 3, 
    	"User": "admin",
    	"PWD": "admin",
    	"CH0":{
    		"rtsp_url": "rtsp://admin:admin@192.168.243.6:80/0",
    		"width": 1920,
			"height": 1080,
    		},
    	"CH1":{
    		"rtsp_url": "rtsp://admin:admin@192.168.243.6:80/1",
    		"width": 640,
			"height": 480,
    		},
    	"CH2":{
    		"rtsp_url": "rtsp://admin:admin@192.168.243.6:80/2",
    		"width": 320,
			"height": 240,
    		},
    	},

};
*/

static APP_IPC_DeviceTable appIpcListTable = {.ipc = NULL};

int DeleteOneAppIpcListRecordByIdAndName(char* id, char* name)
{
	int i;
	int appIpcNum, saveAppIpcNum;
	cJSON *ipcs, *ipc;
	int result = 0;

	ipcs = cJSON_GetObjectItem(appIpcListTable.ipc, APP_IPC_IPCS);
	appIpcNum = cJSON_GetArraySize(ipcs);
	saveAppIpcNum = appIpcNum;

	while(appIpcNum)
	{
		ipc = cJSON_GetArrayItem(ipcs, --appIpcNum);
		if(ipc != NULL)
		{
			if(id != NULL)
			{
				if(strcmp(id, cJSON_GetStringValue(cJSON_GetObjectItem(ipc, APP_IPC_ID))))
				{
					continue;
				}
			}

			if(name != NULL)
			{
				if(strcmp(name, cJSON_GetStringValue(cJSON_GetObjectItem(ipc, APP_IPC_NAME))))
				{
					continue;
				}
			}
				cJSON_DeleteItemFromArray(ipcs, appIpcNum);
				SaveAppIpcListToFile();
		}
	}

	if(saveAppIpcNum == cJSON_GetArraySize(ipcs))
	{
		result = -1;	//不需删除
	}
	else
	{
		SaveAppIpcListToFile();
		result = 0;	//已经存在，删除完成
	}

	return result;
}
#if 0
int AddOrModifyOneAppIpcListRecord(APP_IPC_ONE_DEVICE record, int syncFlag)
{
	int i;
	int appIpcNum;
	cJSON *ipcs, *ipc;
	int result = 0;

	ipcs = cJSON_GetObjectItem(appIpcListTable.ipc, APP_IPC_IPCS);
	appIpcNum = cJSON_GetArraySize(ipcs);
	
	for(i = 0; i < appIpcNum; i++)
	{
		ipc = cJSON_GetArrayItem(ipcs, i);
		if(ipc != NULL)
		{
			if(!strcmp(record.NAME, cJSON_GetStringValue(cJSON_GetObjectItem(ipc, APP_IPC_NAME))))
			{
				result = -1;
				if(strcmp(record.ID, cJSON_GetStringValue(cJSON_GetObjectItem(ipc, APP_IPC_ID))))
				{
					cJSON_ReplaceItemInObject(ipc, APP_IPC_ID, cJSON_CreateString(record.ID));
					result = 1;
				}

				if(strcmp(record.videoType, cJSON_GetStringValue(cJSON_GetObjectItem(ipc, APP_IPC_VIDEO_TYPE))))
				{
					cJSON_ReplaceItemInObject(ipc, APP_IPC_VIDEO_TYPE, cJSON_CreateString(record.videoType));
					result = 1;
				}

				if(result == 1 && syncFlag)
				{
					SaveAppIpcListToFile();
				}

				return result;
			}
		}
	}

	if(ipcs == NULL)
	{
		cJSON_AddItemToObject(appIpcListTable.ipc, APP_IPC_IPCS, ipcs = cJSON_CreateArray());	
	}

	ipc = cJSON_CreateObject();
	cJSON_AddStringToObject(ipc, APP_IPC_ID, record.ID);
	cJSON_AddStringToObject(ipc, APP_IPC_NAME, record.NAME);
	cJSON_AddStringToObject(ipc, APP_IPC_VIDEO_TYPE, record.videoType);
	cJSON_AddItemToArray(ipcs, ipc);

	if(syncFlag)
	{
		SaveAppIpcListToFile();
	}

	return result;	//添加成功
}

int AddOneAppIpcListFromIpcCacheTable(IPC_ONE_DEVICE record, int syncFlag)
{
	int videoType;
	
	APP_IPC_ONE_DEVICE appIpcRecord;

	strcpy(appIpcRecord.ID, record.ID);
	strcpy(appIpcRecord.NAME, record.NAME);
	strcpy(appIpcRecord.videoType, IPC_VIDEO_TYPE_H264);

	if(record.CH_DAT[record.CH_APP].vd_type == 1)
	{
		strcpy(appIpcRecord.videoType, IPC_VIDEO_TYPE_H265);
	}

	return AddOrModifyOneAppIpcListRecord(appIpcRecord, syncFlag);
}
#endif
cJSON *CreateAppIpcListByIo(void)
{
	cJSON *ipc_list_obj;
	cJSON *ipcs;
	cJSON *one_item;
	int i;
	int ipc_num;
	char ip_addr[100];
	char name[41];
	char type[10];
	cJSON *app_list;
	app_list=API_Para_Read_Public(IPC_APP_List);
	ipc_num=cJSON_GetArraySize(app_list);
	
	ipc_list_obj=cJSON_CreateObject();
	cJSON_AddItemToObject(ipc_list_obj, APP_IPC_IPCS, ipcs=cJSON_CreateArray());
	//ipc_num=Get_ALLIpcNum();
	for(i=0;i<ipc_num;i++)
	{
		char *id_str=cJSON_GetArrayItem(app_list,i)->valuestring;
		if(Get_IPCChName_ByIP(id_str,name)==0)
		{
			if(Get_IPCChType_ByIP(id_str, "CH_APP")==1)
				strcpy(type,"H265");
			else
				strcpy(type,"H264");
			one_item=cJSON_CreateObject();
			cJSON_AddStringToObject(one_item,"ID",id_str);
			cJSON_AddStringToObject(one_item,"NAME",name);
			cJSON_AddStringToObject(one_item,"VIDEO_TYPE",type);
			cJSON_AddItemToArray(ipcs,one_item);
		}
	}
	return ipc_list_obj;
}
char* CreateAppIpcListObject(void)
{
    cJSON *root = NULL;
	cJSON *devices = NULL;
	cJSON *device = NULL;
	char *string = NULL;
	int i,j;
	char name[20];

	if(appIpcListTable.ipc!=NULL)
		cJSON_Delete(appIpcListTable.ipc);
	appIpcListTable.ipc=CreateAppIpcListByIo();
	//appIpcListTable.ipc==NULL;
	if(appIpcListTable.ipc == NULL)
	{
		appIpcListTable.ipc = cJSON_CreateObject();
	}

	cJSON_GetObjectItem(appIpcListTable.ipc, APP_IPC_IPCS);
	if(cJSON_GetObjectItem(appIpcListTable.ipc, APP_IPC_IPCS) == NULL)
	{
		cJSON_AddItemToObject(appIpcListTable.ipc, APP_IPC_IPCS, cJSON_CreateArray());	
	}
	
	{
		string = cJSON_Print(appIpcListTable.ipc);
	}
	
	memset(appIpcListTable.md5Code, 0, 16);
	
	StringMd5_Calculate(string, appIpcListTable.md5Code);
	//if(md5!=NULL)
	//	memcpy(md5,appIpcListTable.md5Code,16);

	//cJSON_Delete(appIpcListTable.ipc);

	return string;
}



int ParseOneAppIpcListRecordObject(const char* json, APP_IPC_ONE_DEVICE *pOneIpcRecord)
{
	int status = 0;
	APP_IPC_ONE_DEVICE record;
	
	/* 创建一个用于解析的 cJSON 结构 */
	cJSON *root = cJSON_Parse(json);
	if (root == NULL)
	{
		const char *error_ptr = cJSON_GetErrorPtr();
		if (error_ptr != NULL)
		{
			fprintf(stderr, "Error before: %s\n", error_ptr);
		}
		status = 0;
		goto end;
	}


	GetJsonDataPro(root, APP_IPC_ID, record.ID);
	GetJsonDataPro(root, APP_IPC_NAME, record.NAME);
	GetJsonDataPro(root, APP_IPC_VIDEO_TYPE, record.videoType);

	*pOneIpcRecord = record;

	status = 1;

end:
	
    cJSON_Delete(root);
	
    return status;
}

void SaveAppIpcListToFile(void)
{
	SetJsonToFile(APP_IPC_LIST_FILE_NAME,appIpcListTable.ipc);
	
}

void GetAppIpcListFromFile(void)
{
	char* json;
	cJSON* ipcs;

	if(appIpcListTable.ipc != NULL)
	{
		cJSON_Delete(appIpcListTable.ipc);
		appIpcListTable.ipc = NULL;
	}

	
	appIpcListTable.ipc=GetJsonFromFile(APP_IPC_LIST_FILE_NAME);
	if(appIpcListTable.ipc == NULL)
	{
		appIpcListTable.ipc = cJSON_CreateObject();
	}

	ipcs = cJSON_GetObjectItem(appIpcListTable.ipc, APP_IPC_IPCS);
	if(ipcs == NULL)
	{
		cJSON_AddItemToObject(appIpcListTable.ipc, APP_IPC_IPCS, ipcs = cJSON_CreateArray());	
	}

	if(cJSON_GetArraySize(ipcs) == 0)
	{
		//AddAppIpcFromLanIpcOrWlanIpc();
	}
}

const char* GetAppIpcListMD5Code(void)
{
	char* temp;
	
	temp = CreateAppIpcListObject();

	if(temp != NULL)
	{
		free(temp);
	}
	
	return appIpcListTable.md5Code;
}
#if 0
cJSON *GetIpcRecordByNameOrId(char *name, char *id)
{
	char cmp_ip_addr[41];
	int i;
	int ipc_num;
	cJSON *ipc_info=NULL;
	ipc_num=Get_ALLIpcNum();
	for(i=0;i<ipc_num;i++)
	{
		Get_ALLIpcAppInfo(i,cmp_ip_addr,NULL,NULL);
		if(strcmp(cmp_ip_addr,id)==0||strcmp(cmp_ip_addr,name)==0)
		{
			ipc_info=cJSON_Duplicate(Get_OneAppIpcInfo(i),1);
		}
	}
	return ipc_info;
}
#endif