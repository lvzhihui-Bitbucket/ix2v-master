#include "stack212.h"
#include "obj_UnitSR.h"
#include "task_CallServer.h"


static int qr_menu_state=0;
static int qr_menu_timer=0;
static int qr_menu_im_addr=0;
static int IpgMode=1;

void Set_IpgCompMode(int val)
{
	IpgMode=val;
}

int Get_IpgCompMode(void)
{
	return IpgMode;
}
void IpgOnlineBrd_Send(void)
{
	if(Get_IpgCompMode()==0)
		return;
	unsigned char data[2];
	int ipc_num;
	
	//if(SR_State.in_use == FALSE)
	{
		//Get_Divert_Acc_new(1,NULL,NULL,NULL,NULL,(unsigned char*)&data);
		
		ipc_num=Get_QuadIpcNum();
		//sip_state = data[0];
		//ipc_num = data[1];
		data[0]=Get_SipSer_State()==1?0:1;
		data[1]=ipc_num;//ipc_num	
		//API_SendNewCmdWithoutAck(BROADCAST_ADDRESS,APCI_IPG_STATE,2,data);
		//IpgOnlineBrd_Age = AI_AppointFastLinkWithEventAck(0X34);
		//IpgOnlineBrd_Age = 0;
		AI_PushOneCmdIntoQueue(OS_Q_STACK_FRAME_TASK_TRS_APT_A, BROADCAST_ADDRESS, APCI_IPG_STATE, 1, 2, data );
	}
}



void IpgRmReq_Process(int im_ph_addr,int rm_id)
{
	if(Get_IpgCompMode()==0)
		return;
	int SR_apply;
	int im_addr;
	char sip_acc[60];
	char sip_ser[60];
	char master_acc[40];
	char sip_pwd[40];
	char addr_str[5];
	char data[2];
	int add_flag=0;
	int use_default_flag=0;
	char *qr_str;
	if(qr_menu_state==0)
	{
		SR_apply = API_SR_Request(CT11_ST_MONITOR); 
							//???,????
		if (SR_apply != SR_ENABLE_DIRECT)
			return;
	}
	im_addr=(im_ph_addr&0x7f)>>2;
	if(im_addr==0)
		im_addr=32;
	if(qr_menu_state==1&&qr_menu_im_addr==im_addr)
	{
		data[0] = rm_id;
		data[1]	= 1;
		qr_menu_timer=0;
		AI_PushOneCmdIntoQueue(OS_Q_STACK_FRAME_TASK_TRS_APT_A, im_ph_addr, APCI_RM_M_RSP, 1, 2, data );
		return;
	}
	master_acc[0]=0;
	get_sip_master_acc(master_acc);
	if(strlen(master_acc)<2)
		use_default_flag=1;
	
	if(get_sip_dt_im_acc_from_tb(im_addr,sip_acc,sip_pwd)<0)
	{
		if(get_default_sip_master_username(master_acc)!=1)
			return;
	
		//sprintf(sip_acc,"%s%04d",master_acc,im_addr);
		sprintf(sip_acc,"%s%04d",master_acc,im_addr);
	
		create_default_sip_pwd(sip_acc,sip_pwd);
		add_flag=1;
	}
	qr_str=create_sip_phone_qrcode(sip_acc,sip_pwd);
	if(qr_str==NULL)
		return;
	sprintf(addr_str,"%02d",im_addr);
	get_sip_master_ser(sip_ser);
	#if !defined(PID_IX850) && !defined(PID_IX611) && !defined(PID_IX622)&& !defined(PID_IX821)
	scr_qrcode(qr_str,addr_str,sip_ser,sip_acc,sip_pwd);
	#endif
	free(qr_str);
	data[0] = rm_id;
	data[1]	= 1;
	AI_PushOneCmdIntoQueue(OS_Q_STACK_FRAME_TASK_TRS_APT_A, im_ph_addr, APCI_RM_M_RSP, 1, 2, data );
	if(data[1] == 1)
	{
		//SR_Routing_Create(CT11_ST_MONITOR);
		qr_menu_state  = 1;
		qr_menu_timer=0;
		qr_menu_im_addr=im_addr;
		usleep(100*1000);
		SR_Routing_Create(CT11_ST_MONITOR);
	}
	if(add_flag==1)
	{
		add_sip_dt_im_acc(im_addr,sip_acc,sip_pwd);
	}
	if(use_default_flag==1)
	{
		sip_master_use_default();
	}
		
}
void IpgRmExit_Process(int im_ph_addr)
{
	if(Get_IpgCompMode()==0)
		return;
	
	AI_PushOneCmdIntoQueue(OS_Q_STACK_FRAME_TASK_TRS_APT_A, im_ph_addr, APCI_RM_M_QUIT, 1, 0,NULL );
	
	if(qr_menu_state == 1)
	{
		#if !defined(PID_IX850) && !defined(PID_IX611) && !defined(PID_IX622)&&!defined(PID_IX821)
		video_turn_on_init();
		#endif
		//API_POWER_VIDEORX_OFF();
		qr_menu_state=0;
		SR_Routing_Close(CT11_ST_MONITOR);
	}
		
}
void IpgRmTp_Process(int im_ph_addr,int pointx,int pointy)
{
	if(Get_IpgCompMode()==0)
		return;
	int im_addr;
	im_addr=(im_ph_addr&0x7f)>>2;
	if(im_addr==0)
		im_addr=32;
	if(qr_menu_state&&qr_menu_im_addr==im_addr)
	{
		qr_menu_timer=0;
		//printf("111111111111pointx=%d,pointy=%d\n",pointx,pointy);
		if((pointx==-1&&pointy==-1)||(pointx>520&&pointx<720&&pointy>280&&pointy<420))
		{
			AI_PushOneCmdIntoQueue(OS_Q_STACK_FRAME_TASK_TRS_APT_A, im_ph_addr, APCI_RM_ENTER_INPUT, 1, 0,NULL );

		}
	}
}
void IpgRmInput_Process(int im_ph_addr,char *data,int len)
{
	if(Get_IpgCompMode()==0)
		return;
	int im_addr;
	char sip_pwd[40]={0};
	char sip_acc[60];
	char sip_ser[60];
	char master_acc[40];
	char addr_str[5];
	char *qr_str;
	im_addr=(im_ph_addr&0x7f)>>2;
	if(im_addr==0)
		im_addr=32;
	if(qr_menu_state&&qr_menu_im_addr==im_addr)
	{
		qr_menu_timer=0;
		memcpy(sip_pwd,data,len);
		if(ch_sip_dt_im_phone_pwd(im_addr,sip_pwd)>=0)
		{
			get_sip_dt_im_acc_from_tb(im_addr, sip_acc,sip_pwd);
			qr_str=create_sip_phone_qrcode(sip_acc,sip_pwd);
			if(qr_str==NULL)
				return;
			sprintf(addr_str,"%02d",im_addr);
			get_sip_master_ser(sip_ser);
			#if !defined(PID_IX850) && !defined(PID_IX611) && !defined(PID_IX622)&&!defined(PID_IX821)
			scr_qrcode(qr_str,addr_str,sip_ser,sip_acc,sip_pwd);
			#endif
			free(qr_str);
		}
	}
}
void Rm_AutoClose(void)
{
	if(qr_menu_state)
	{
		if(qr_menu_timer++>40)
		{
			IpgRmExit_Process(qr_menu_im_addr);
		}
	}
}
void IpgDivertStart_Process(int im_ph_addr)
{
	if(Get_IpgCompMode()==0)
		return;
	cJSON *call_para,*target;
	int im_addr;
	char data[2];
	char sip_acc[60];
	
	im_addr=(im_ph_addr&0x7f)>>2;
	if(im_addr==0)
		im_addr=32;

	
	data[0] = im_addr;
	if(get_sip_dt_im_acc_from_tb(im_addr, sip_acc,NULL)>=0)
	{
		data[1] = 0;
		call_para=cJSON_CreateObject();
	
		target=cJSON_AddObjectToObject(call_para,CallPara_Divert);
		cJSON_AddStringToObject(target,CallTarget_SipAcc,sip_acc);
		API_CallServer_StartDivert(DxMainCall,call_para);
		cJSON_Delete(call_para);
	}
	else
	{
		data[1] = 1;
	}
	
				//API_SendNewCmd(Message_command->address,APCI_DIVERT_RSP_START,2,bcd_buf);
	AI_PushOneCmdIntoQueue(OS_Q_STACK_FRAME_TASK_TRS_APT_A, im_ph_addr, APCI_DIVERT_RSP_START, 1, 2,data);			
	//AI_PushOneCmdIntoQueue(im_ph_addr,APCI_DIVERT_RSP_START,2,data);
}

int GetOneXMLValue(const char *xml,const char *key,char *val)
{
	char buff[100];
	char *pos1,*pos2,*pos3;
	int ret=0;
	int len;
	sprintf(buff,"<%s",key);
	pos1 = strstr(xml, buff);
	if(pos1 != NULL)
	{
		pos2 = strchr(pos1, '>');
		if(pos2 != NULL)
		{
			pos3=strchr(pos2, '<');
			if(pos2 != NULL)
			{
				if(val!=NULL)
				{
					len = ((int)(pos3-pos2))-1;
					memcpy(val, pos2+1, len);
					val[len] = 0;
				}
				ret = 0;
			}
			else
			{
				ret = -1;
			}
		}
		else
		{
			ret = -1;
		}
	}
	else
	{
		ret = -1;
	}
	return ret;
}
sem_t			ixg_rm_sem;
char 			ixg_rm_ret[20];
void triger_rm_sem(char *ret)
{
	strcpy(ixg_rm_ret,ret);
	sem_post(&ixg_rm_sem);
}

void IXGRmReq_Process(int im_ph_addr,int rm_id)
{
	int SR_apply;
	int im_addr;
	char sip_acc[60];
	char sip_ser[60];
	char master_acc[40];
	char sip_pwd[40];
	char addr_str[5];
	char data[2];
	int add_flag=0;
	int use_default_flag=0;
	char map_ixaddr[11];
	char ixds_addr[11];
	char *qr_str;
	if(qr_menu_state==0)
	{
		SR_apply = API_SR_Request(CT11_ST_MONITOR); 
							//???,????
		if (SR_apply != SR_ENABLE_DIRECT)
			return;
	}
	im_addr=(im_ph_addr&0x7f)>>2;
	if(im_addr==0)
		im_addr=32;
	if(qr_menu_state==1&&qr_menu_im_addr==im_addr)
	{
		data[0] = rm_id;
		data[1]	= 1;
		qr_menu_timer=0;
		AI_PushOneCmdIntoQueue(OS_Q_STACK_FRAME_TASK_TRS_APT_A, im_ph_addr, APCI_RM_M_RSP, 1, 2, data );
		return;
	}
	
	
	///////////////////
	#if 0
		if(get_default_sip_master_username(master_acc)!=1)
			return;
	
		//sprintf(sip_acc,"%s%04d",master_acc,im_addr);
		sprintf(sip_acc,"%s%04d",master_acc,im_addr);
		create_default_sip_pwd(sip_acc,sip_pwd);
	#endif
		sprintf(ixds_addr,"%s000001",GetSysVerInfo_bd());
		IfIXGDtAddrToIxAddr(im_addr,map_ixaddr);
		qr_str=API_GetQR_Code(map_ixaddr,ixds_addr,map_ixaddr);
	
	
		//////////////
	if(qr_str==NULL)
		return;
	GetOneXMLValue(qr_str,"DOMAIN",sip_ser);
	GetOneXMLValue(qr_str,"USER",sip_acc);
	GetOneXMLValue(qr_str,"PSW",sip_pwd);
	sprintf(addr_str,"%02d",im_addr);
	get_sip_master_ser(sip_ser);
	#if !defined(PID_IX850) && !defined(PID_IX611) && !defined(PID_IX622)&&!defined(PID_IX821)
	scr_qrcode(qr_str,addr_str,sip_ser,sip_acc,sip_pwd);
	#endif
	API_Bin_ctrl("IXG_DT_VD_SW",1);
	free(qr_str);
	data[0] = rm_id;
	data[1]	= 1;
	AI_PushOneCmdIntoQueue(OS_Q_STACK_FRAME_TASK_TRS_APT_A, im_ph_addr, APCI_RM_M_RSP, 1, 2, data );
	if(data[1] == 1)
	{
		//SR_Routing_Create(CT11_ST_MONITOR);
		qr_menu_state  = 1;
		qr_menu_timer=0;
		qr_menu_im_addr=im_addr;
		usleep(100*1000);
		SR_Routing_Create(CT11_ST_MONITOR);
	}

		
}

void IXGDFQRExit_Process(void)
{
	if(qr_menu_state == 1)
	{
		//API_POWER_VIDEORX_OFF();
		qr_menu_state=0;
		//SR_Routing_Close(CT11_ST_MONITOR);
		//API_Bin_ctrl("IXG_DT_VD_SW",0);
	}
}
void IXGRmExit_Process(int im_ph_addr)
{
	AI_PushOneCmdIntoQueue(OS_Q_STACK_FRAME_TASK_TRS_APT_A, im_ph_addr, APCI_RM_M_QUIT, 1, 0,NULL );
	
	if(qr_menu_state == 1)
	{
		API_Bin_ctrl("IXG_DT_VD_SW",0);
		#if !defined(PID_IX850) && !defined(PID_IX611) && !defined(PID_IX622)&&!defined(PID_IX821)
		video_turn_on_init();
		#endif
		//API_POWER_VIDEORX_OFF();
		qr_menu_state=0;
		SR_Routing_Close(CT11_ST_MONITOR);
	}
		
}
void IXGRmTp_Process(int im_ph_addr,int pointx,int pointy)
{
	int im_addr;
	im_addr=(im_ph_addr&0x7f)>>2;
	if(im_addr==0)
		im_addr=32;
	if(qr_menu_state&&qr_menu_im_addr==im_addr)
	{
		qr_menu_timer=0;
		//printf("111111111111pointx=%d,pointy=%d\n",pointx,pointy);
		if((pointx==-1&&pointy==-1)||(pointx>520&&pointx<720&&pointy>280&&pointy<420))
		{
			AI_PushOneCmdIntoQueue(OS_Q_STACK_FRAME_TASK_TRS_APT_A, im_ph_addr, APCI_RM_ENTER_INPUT, 1, 0,NULL );
			
		}
	}
}
void IXGRmInput_Process(int im_ph_addr,char *data,int len)
{
#if 1
	int im_addr;
	char sip_pwd[40]={0};
	char sip_acc[60];
	char sip_ser[60];
	char master_acc[40];
	char addr_str[5];
	char *qr_str;
	char map_ixaddr[11];
	char ixds_addr[11];
	im_addr=(im_ph_addr&0x7f)>>2;
	if(im_addr==0)
		im_addr=32;
	if(qr_menu_state&&qr_menu_im_addr==im_addr)
	{
		qr_menu_timer=0;
		memcpy(sip_pwd,data,len);
		//if(ch_sip_dt_im_phone_pwd(im_addr,sip_pwd)>=0)
		{
			
		#if 0
			get_sip_dt_im_acc_from_tb(im_addr, sip_acc,sip_pwd);
			qr_str=create_sip_phone_qrcode(sip_acc,sip_pwd);
			if(qr_str==NULL)
				return;
			sprintf(addr_str,"%02d",im_addr);
			get_sip_master_ser(sip_ser);
			#if !defined(PID_IX850) && !defined(PID_IX611) && !defined(PID_IX622)
			scr_qrcode(qr_str,addr_str,sip_ser,sip_acc,sip_pwd);
			#endif
			free(qr_str);
			#endif
			sprintf(ixds_addr,"%s000001",GetSysVerInfo_bd());
			IfIXGDtAddrToIxAddr(im_addr,map_ixaddr);
			

			sem_init(&ixg_rm_sem,0,0);
			API_SetDivertState(map_ixaddr,ixds_addr,map_ixaddr,"RESET",sip_pwd);
			
			if(sem_wait_ex2(&ixg_rm_sem,5000) == 0 )
			{
				if(strcmp(ixg_rm_ret,"SUCC")==0)
				{
					qr_str=API_GetQR_Code(map_ixaddr,ixds_addr,map_ixaddr);

					if(qr_str==NULL)
						return;
					GetOneXMLValue(qr_str,"DOMAIN",sip_ser);
					GetOneXMLValue(qr_str,"USER",sip_acc);
					GetOneXMLValue(qr_str,"PSW",sip_pwd);
					sprintf(addr_str,"%02d",im_addr);
					get_sip_master_ser(sip_ser);
					#if !defined(PID_IX850) && !defined(PID_IX611) && !defined(PID_IX622)&&!defined(PID_IX821)
					scr_qrcode(qr_str,addr_str,sip_ser,sip_acc,sip_pwd);
					#endif
					free(qr_str);
				}
			}
			
			
		}
	}
#endif
}
void IXGDivertStart_Process(int im_ph_addr)
{
	if(Get_IpgCompMode()==0)
		return;
	cJSON *call_para,*target;
	int im_addr;
	char data[2];
	//char sip_acc[60];
	
	im_addr=(im_ph_addr&0x7f)>>2;
	if(im_addr==0)
		im_addr=32;

	
	data[0] = im_addr;
	#if 0
	if(get_sip_dt_im_acc_from_tb(im_addr, sip_acc,NULL)>=0)
	#endif	
	{
		data[1] = 0;
		call_para=cJSON_CreateObject();
	
		//target=cJSON_AddObjectToObject(call_para,CallPara_Divert);
		//cJSON_AddStringToObject(target,CallTarget_SipAcc,sip_acc);
		API_CallServer_StartDivert(IXGMainCall,call_para);
		cJSON_Delete(call_para);
	}
	
	
				//API_SendNewCmd(Message_command->address,APCI_DIVERT_RSP_START,2,bcd_buf);
	AI_PushOneCmdIntoQueue(OS_Q_STACK_FRAME_TASK_TRS_APT_A, im_ph_addr, APCI_DIVERT_RSP_START, 1, 2,data);			
	//AI_PushOneCmdIntoQueue(im_ph_addr,APCI_DIVERT_RSP_START,2,data);
}

//////////////////////////////////////////////////////////////////////
#define VSIP_VERSION			1	//VSIPЭ��İ汾
#define VSIP_VERSION_2			2	//VSIP2Э��İ汾		//czn_20170313
#define TRYING_100				0	//��ʾ���н��յ�INVITEָ��, ���ڴ���.
#define RINGING_180				1	//��ʾ���н��յ�INVITEָ��, �ҽ���RINGING״̬
#define OK_200					2	//��ʾ����ժ��,����ͨ��״̬
#define ACK						3	//��ʾ���н��յ�200/OK, ����ͨ��״̬
#define BYE_BECALLED			4	//��ʾ���йһ�
#define BYE_CALLER				5	//��ʾ����ȡ������
#define OK_201					6	//���н��յ�BYE_BECALLED֮Ӧ��
#define OK_202					7	//���н��յ�BYE_CALLER֮Ӧ��
#define VSIP_ERROR				8	//����Ӧ��(����������)
#define ROOM_ID					9	//�����з�Ϊ��IPG�ڷֻ���IPG���طֻ���ַ
//czn_20170411_s
#define VSIP_ERROR1_TB_SEARCH			1
#define VSIP_ERROR2_PARA_OVER			2
#define VSIP_ERROR3_LINK_IPDEV			3
#define VSIP_ERROR4_LINK_DTDEV			4
#define VSIP_ERROR5_NET_BUSY			5
#define VSIP_ERROR6_UNIT_BUSY			6
#define VSIP_ERROR7_TARGET_BUSY		7
#define VSIP_ERROR8_DAIL_IPDEV			8
#define VSIP_ERROR9_DAIL_DTDEV		9
#define VSIP_ERROR10_DAIL_LOCALIM		10
#define VSIP_ERROR11_DAIL_DEFAULT		11


void IpgIntercomCallInvite_Process(int im_ph_addr,uint8* data, uint8 data_length)
{
	if(Get_IpgCompMode()==0)
		return;
	uint8 ext_data[6];
	uint8 ext_len;
	
	
	
	ext_len			= 6;									//��չ������Ч����
	
	ext_data[0] 		= VSIP_VERSION;			//czn_20170318		//Invite_Dpt_Save.version;						//�汾
	ext_data[1] 		= (im_ph_addr>>8);			//Դ��ַ_H
	ext_data[2] 		= im_ph_addr;					//Դ��ַ_L
	
		
	ext_data[3] 		= VSIP_ERROR;
	
	ext_data[4] 		= 	VSIP_ERROR11_DAIL_DEFAULT;
	if(ext_data[4] == VSIP_ERROR10_DAIL_LOCALIM)
	{
		//ext_data[5]			= 	data6;
	}
	else
	{
		ext_data[5] = 0xff;
	}	
	AI_PushOneCmdIntoQueue(OS_Q_STACK_FRAME_TASK_TRS_APT_A, GA205_INTERCOMCALL_STATE, APCI_GROUP_VALUE_WRITE, 1, 6,ext_data);
	
}	
#define IXGOnlineBrd_Period	180
static int IXGOnlineBrd_TrigSend=0;
static int IXGOnlineBrd_Close=0;
void IXGOnlineBrd_Send(void)
{
	//AI_PushOneCmdIntoQueue(OS_Q_STACK_FRAME_TASK_TRS_APT_A, BROADCAST_ADDRESS, IXG_ON_LINE, 0, 0, 0 );
	API_Stack_BRD_With_ACK(IXG_ON_LINE);
}
int IXGOnlineBrd_Callback(int timing)
{	
	if(IXGOnlineBrd_Close)
		return 0;
	if(IXGOnlineBrd_TrigSend>0)
		IXGOnlineBrd_TrigSend++;
	int link_state=API_PublicInfo_Read_Bool(PB_DT_LINK_STATE);
	if((link_state==0&&timing>=IXGOnlineBrd_Period)||IXGOnlineBrd_TrigSend>=3)
	{
		IXGOnlineBrd_TrigSend=0;
		IXGOnlineBrd_Send();
		return 1;
	}
	if(link_state==0&&timing==25)
		IpgOnlineBrd_Send();
	return 0;
}

void IXGOnlineBrd_Trig(void)
{
	IXGOnlineBrd_TrigSend=1;
}
void SetOnlineBrd_Close(int val)
{
	IXGOnlineBrd_Close=val;
}
static int dfqr_menu_state=0;
int IXGDFRmReq_Process(int im_ph_addr)
{
	int SR_apply;
	int im_addr;
	char sip_acc[60];
	char sip_ser[60];
	char master_acc[40];
	char sip_pwd[40];
	char addr_str[5];
	char data[2];
	int add_flag=0;
	int use_default_flag=0;
	char map_ixaddr[11];
	char ixds_addr[11];
	char *qr_str;
	
	im_addr=(im_ph_addr&0x7f)>>2;
	if(im_addr==0)
		im_addr=32;
	
	
	
	
		sprintf(ixds_addr,"%s000001",GetSysVerInfo_bd());
		IfIXGDtAddrToIxAddr(im_addr,map_ixaddr);
		qr_str=API_GetQR_Code(map_ixaddr,ixds_addr,map_ixaddr);
	
	
		//////////////
	if(qr_str==NULL)
		return 0;
	GetOneXMLValue(qr_str,"DOMAIN",sip_ser);
	GetOneXMLValue(qr_str,"USER",sip_acc);
	GetOneXMLValue(qr_str,"PSW",sip_pwd);
	sprintf(addr_str,"%02d",im_addr);
	get_sip_master_ser(sip_ser);
	#if !defined(PID_IX850) && !defined(PID_IX611) && !defined(PID_IX622)&&!defined(PID_IX821)
	scr_qrcode(qr_str,addr_str,sip_ser,sip_acc,sip_pwd);
	#endif
	API_Bin_ctrl("IXG_DT_VD_SW",1);
	free(qr_str);
	
	
	dfqr_menu_state=1;

	return 1;
		
}

int get_dfqr_menu_state(void)
{
	return dfqr_menu_state;
}

void IXGDFRmReq_Close(void)
{
	if(dfqr_menu_state)
	{
		dfqr_menu_state=0;
		API_Bin_ctrl("IXG_DT_VD_SW",0);
		#if !defined(PID_IX850) && !defined(PID_IX611) && !defined(PID_IX622)&&!defined(PID_IX821)
		video_turn_on_init();
		#endif
	}
}