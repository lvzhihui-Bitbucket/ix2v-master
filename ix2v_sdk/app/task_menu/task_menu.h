
#ifndef _task_menu_H
#define  _task_menu_H

#include "task_survey.h"

#define INFORM_DATA_LEN		500

typedef struct
{
	VDP_MSG_HEAD	head;	
	char 			data[INFORM_DATA_LEN+1];
} InformMsg_T;

#define MSG_7_BRD_SUB_RES_TCP_DOWNLOAD_START				1
#define MSG_7_BRD_SUB_RES_TCP_DOWNLOAD_CANCEL				2

#endif


