
#include "task_menu.h"
#include "task_survey.h"
#include "obj_gpio.h"
#include "obj_IoInterface.h"

Loop_vdp_common_buffer	vdp_menu_mesg_queue;
Loop_vdp_common_buffer	vdp_menu_sync_queue;
vdp_task_t				task_menu;
static int menu_init_flag=0;
void vdp_menu_mesg_data_process(char* msg_data, int len);
void* vdp_menu_task( void* arg );

void vtk_TaskInit_menu(int priority)
{
	vtk_lvgl_lock();
	system("rmmod ak_fb");
	system("insmod /usr/modules/ak_fb.ko");
	usleep(1000*1000);
	
	 
	vtk_display_initial();
	
	// test 
	global_style_init();

#if defined( PID_IX850 )	
	//void test_menu_start(void);
	//test_menu_start();
	////api_local_preview_on(0);
	menu_initial();
	//scr_main();

	//ui_setting_init();
#endif
	
	menu_init_flag=1;
	vtk_lvgl_unlock(); 
	
	init_vdp_common_queue(&vdp_menu_mesg_queue, 1500,(msg_process)vdp_menu_mesg_data_process, &task_menu);
	init_vdp_common_queue(&vdp_menu_sync_queue, 1000, NULL,&task_menu);
	init_vdp_common_task(&task_menu, MSG_ID_Menu, vdp_menu_task, &vdp_menu_mesg_queue, &vdp_menu_sync_queue);
	
	dprintf("vdp_menu_task starting............\n");
}

void exit_vdp_menu_task(void)
{
	exit_vdp_common_queue(&vdp_menu_mesg_queue);
	exit_vdp_common_queue(&vdp_menu_sync_queue);
	exit_vdp_common_task(&task_menu);	
}

void* vdp_menu_task( void* arg )
{
	vdp_task_t*	 ptask 			= (vdp_task_t*)arg;
	p_vdp_common_buffer pdb 	= 0;
	int	size;
	//sleep(1);

	thread_log_add(__func__);
	#if 0
	system("rmmod ak_fb");
	system("insmod /usr/modules/ak_fb.ko");
	sleep(1);
	thread_log_add(__func__);
	vtk_lvgl_lock(); 
	vtk_display_initial();
	
	// test 
	global_style_init();

#if defined( PID_IX850 )	
	//void test_menu_start(void);
	//test_menu_start();
	////api_local_preview_on(0);
	//menu_initial();
	//scr_main();

	//ui_setting_init();
#endif
	
	menu_init_flag=1;
	vtk_lvgl_unlock(); 
	#endif
	ptask->task_StartCompleted = 1;
	while( ptask->task_run_flag )
	{
		size = pop_vdp_common_queue( ptask->p_msg_buf, &pdb, VDP_QUEUE_POLLING_TIME);
		if( size > 0 )
		{
			(*ptask->p_msg_buf->process)(pdb,size);
			purge_vdp_common_queue( ptask->p_msg_buf );
		}
	}
	return 0;
}

void vdp_menu_mesg_data_process(char* msg_data,int len)
{
	InformMsg_T* 	pMsg 	= (InformMsg_T*)msg_data;

	switch( pMsg->head.msg_type )
	{
		
	}
}



static int	API_add_message_to_menu_queue( char* pdata, int len )
{
	if(task_menu.task_run_flag == 0)
	{
		return -1;
	}

	return push_vdp_common_queue(task_menu.p_msg_buf, (char*)pdata, len);
}

int API_add_inform(int inform)
{
	InformMsg_T msg;
	int len;
	
	msg.head.msg_source_id	= 0;
	msg.head.msg_target_id	= 0;
	//msg.head.msg_type		= HAL_TYPE_BRD;
	msg.head.msg_sub_type	= inform;
	
	len = sizeof(InformMsg_T) - INFORM_DATA_LEN;
	
	return API_add_message_to_menu_queue( (char*)&msg, len);
}

int API_add_inform_with_data(int inform, char* data, int len)
{
	InformMsg_T msg;
	
	if(len > INFORM_DATA_LEN)
	{
		return -1;
	}
	
	msg.head.msg_source_id	= 0;
	msg.head.msg_target_id	= 0;
	//msg.head.msg_type		= HAL_TYPE_BRD_WITH_DATA;
	msg.head.msg_sub_type	= inform;
	
	memcpy(msg.data, data, len);

	len = sizeof(InformMsg_T) - INFORM_DATA_LEN +len;
	
	return API_add_message_to_menu_queue((char*)&msg, len);
}


int IfMenuInit(void)
{
	return menu_init_flag;
}
