
#ifndef _GUI_LIB_H
#define _GUI_LIB_H

#include "ak_common.h"
#include "ak_log.h" 
#include "ak_vdec.h"
#include "ak_vo.h"
#include "ak_thread.h"
#include "ak_mem.h"
#include "ak_tde.h"

#ifdef PID_IX850
#define MY_DISP_HOR_RES 480
#define MY_DISP_VER_RES 800
#else
#define MY_DISP_HOR_RES 1024
#define MY_DISP_VER_RES 600
#endif

#define OSD_ROTATE_NONE		0
#define OSD_ROTATE_LEFT		1
#define OSD_ROTATE_RIGHT	2
#define OSD_ROTATE_MODE		OSD_ROTATE_NONE

#if 0

#define	AK_VO_CREATE_USER_LAYER		ak_vo_create_bg_layer
#define	USE_GUI_GROUP_1				AK_VO_REFRESH_BG_GROUP
#define	USE_GUI_LAYER_1				AK_VO_LAYER_BG_1
#define	USE_GUI_GROUP_2				AK_VO_REFRESH_BG_GROUP
#define	USE_GUI_LAYER_2				AK_VO_LAYER_BG_2
#define	USE_GUI_GROUP_3				AK_VO_REFRESH_BG_GROUP
#define	USE_GUI_LAYER_3				AK_VO_LAYER_BG_3
#define	USE_GUI_GROUP_4				AK_VO_REFRESH_BG_GROUP
#define	USE_GUI_LAYER_4				AK_VO_LAYER_BG_4
#define	USE_GUI_GROUP_5				AK_VO_REFRESH_BG_GROUP
#define	USE_GUI_LAYER_5				AK_VO_LAYER_BG_5

#define REFRESH_DEFAULT				(USE_GUI_GROUP_1&(1<<USE_GUI_LAYER_1))

#else

#define	AK_VO_CREATE_USER_LAYER		ak_vo_create_gui_layer
#define	USE_GUI_GROUP_1				AK_VO_REFRESH_GUI_GROUP
#define	USE_GUI_LAYER_1				AK_VO_LAYER_GUI_1
#define	USE_GUI_GROUP_2				AK_VO_REFRESH_GUI_GROUP
#define	USE_GUI_LAYER_2				AK_VO_LAYER_GUI_2
#define	USE_GUI_GROUP_3				AK_VO_REFRESH_GUI_GROUP
#define	USE_GUI_LAYER_3				AK_VO_LAYER_GUI_3
#define	USE_GUI_GROUP_4				AK_VO_REFRESH_GUI_GROUP
#define	USE_GUI_LAYER_4				AK_VO_LAYER_GUI_4
#define	USE_GUI_GROUP_5				AK_VO_REFRESH_GUI_GROUP
#define	USE_GUI_LAYER_5				AK_VO_LAYER_GUI_5

#endif

#define REFRESH_LAYER1			(USE_GUI_GROUP_1&(1<<USE_GUI_LAYER_1))
#define REFRESH_LAYER2			(USE_GUI_GROUP_2&(1<<USE_GUI_LAYER_2))
#define REFRESH_LAYER3			(USE_GUI_GROUP_3&(1<<USE_GUI_LAYER_3))
#define REFRESH_LAYER4			(USE_GUI_GROUP_4&(1<<USE_GUI_LAYER_4))
#define REFRESH_LAYER5			(USE_GUI_GROUP_5&(1<<USE_GUI_LAYER_5))

#define BYTEH1(x)	((x>>24)&0xff)
#define BYTEH2(x)	((x>>16)&0xff)
#define BYTEH3(x)	((x>>8)&0xff)
#define BYTEH4(x)	((x&0xff)
#define SWAPINT(x)  (x=((x&0xFF)<<24)|((x&0xFF00)<<8)|((x&0xFF0000)>>8)|((x&0xFF000000)>>24))

//#define TDE_DISP_SPRITE

//#define VO_MULTI_LAYERS


#define COLOR_FORMAT_RGB565
#define GP_OUTPUT_RGBXXX		GP_FORMAT_RGB565

#ifndef VO_MULTI_LAYERS
#define REFRESH_DEFAULT				(REFRESH_LAYER1)
#else
#define REFRESH_DEFAULT				(REFRESH_LAYER1|REFRESH_LAYER2|REFRESH_LAYER3|REFRESH_LAYER4|REFRESH_LAYER5)
#endif

#define REFRESH_VIDEO				(AK_VO_REFRESH_VIDEO_GROUP&(1<<AK_VO_LAYER_VIDEO_1))

#if defined(COLOR_FORMAT_RGB565)
#define GP_FORMAT_RGBXXX		GP_FORMAT_RGB565
#elif defined(COLOR_FORMAT_ARGB8888)
#define GP_FORMAT_RGBXXX		GP_FORMAT_RGB565	//GP_FORMAT_ARGB8888
#else
#define GP_FORMAT_RGBXXX		GP_FORMAT_RGB888
#endif

#define LEN_OFFSET_V                4                                           //yuv��uֵƫ���� y=0 u=h*w v=h*w+h*w/4
#define YUV_TEST_PRINT              100
#define DEV_GUI                     "/dev/fb0"                                  //gui lcdʹ�õ��豸����
#define SIZE_ERROR                  0xffffffff
#define USEC2SEC                    1000000

#if defined(COLOR_FORMAT_RGB565)
#define BITS_PER_PIXEL              16
#define LEN_COLOR_R                  5
#define LEN_COLOR_G                  6
#define LEN_COLOR_B                  5
#elif defined(COLOR_FORMAT_ARGB8888)
#define BITS_PER_PIXEL              24
#define LEN_COLOR_R                  8
#define LEN_COLOR_G                  8
#define LEN_COLOR_B                  8
#else
#define BITS_PER_PIXEL              24
#define LEN_COLOR_R                  8
#define LEN_COLOR_G                  8
#define LEN_COLOR_B                  8
#endif

#define DUAL_FB_FIX                 fb_fix_screeninfo_param.reserved[ 0 ]
#define DUAL_FB_VAR                 fb_var_screeninfo_param.reserved[ 0 ]

#ifndef AK_SUCCESS
#define AK_SUCCESS 0
#endif

#ifndef AK_FAILED
#define AK_FAILED -1
#endif

#if defined(COLOR_FORMAT_RGB565)
enum color_offset {
	OFFSET_RED = 11,
	OFFSET_GREEN = 5,
	OFFSET_BLUE = 0,
};
#elif defined(COLOR_FORMAT_ARGB8888)
enum color_offset {
	OFFSET_RED = 16,
	OFFSET_GREEN = 8,
	OFFSET_BLUE = 0,
};
#else
enum color_offset {
	OFFSET_RED = 16,
	OFFSET_GREEN = 8,
	OFFSET_BLUE = 0,
};
#endif
typedef struct _gui_base_attr_
{
	int 			fd;				// fb device file descripter
	int 			max_w;			// fb disp max width
	int 			max_h;			// fb disp max height	
	int 			bpp_len;		// fb disp bpp bytes
	char*			pframebuf;
	unsigned long 	max_size;		// fb buf size
	int				fix_smem_len;	
} gui_base_attr_t;

typedef struct _OSD_LAYER_
{
	int		width;
	int		height;
	int		bpp_len;
	int		buf_size;
	char* 	pbuf;
	struct 	ak_vo_layer_in	layer;	
	struct 	ak_vo_layer_out	vo_out_info;	
	struct 	ak_vo_obj 		vo_obj;
	int  	ak_vo_group;
	int		ak_vo_layer;	
}OSD_LAYER;

int ak_gui_dev_initial(void);
void ak_gui_dev_deinitial(void);

void ak_gui_area_update( int x, int y, int sx, int sy, unsigned short* pdat);

void ak_vo_refresh_cmd_set(int layer);

#define IX481H 		0
#define IX481V 		1
#define IX482  		2
#define IX850		3
#define IX47		4
#define IXG			5
#define IXTYPE_MAX	6

extern int	bkgd_w;
extern int 	bkgd_h;
int get_pane_type(void);
int set_pane_type(int type);

void vtk_display_initial(void);
void vtk_display_deinitial(void);

#endif
