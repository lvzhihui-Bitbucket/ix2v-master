
#include "utility.h"
#include "one_tiny_task.h"
#include "lvgl.h"
#include "lv_examples.h"

#include "virt_io.h"
#include "task_Shell.h"

#define MAX_KEY_ARRAY	10

static KEY_DATA         KEY_ARRAY[MAX_KEY_ARRAY];
static int              uKeyIndexIn  = 0;
static int              uKeyIndexOut = 0;
static int              uKeyIndexAdd = 0;
static pthread_mutex_t  lock = PTHREAD_MUTEX_INITIALIZER;

static void ResetKeySendBuffer(void)
{
	uKeyIndexOut    = 0;
	uKeyIndexIn     = 0;
	uKeyIndexAdd    = 0;
}

int PushInKeyArray(const KEY_DATA uKeyMsg)
{
	int uIndexTmp,size;
    pthread_mutex_lock(&lock);
	uIndexTmp = uKeyIndexIn + 1;
	if (uIndexTmp >= MAX_KEY_ARRAY)
		uIndexTmp = 0;
	if (uIndexTmp == uKeyIndexOut)
        uKeyIndexOut++;
    KEY_ARRAY[uKeyIndexIn]  = uKeyMsg;
    uKeyIndexIn             = uIndexTmp;
	size = uKeyIndexAdd++;
    pthread_mutex_unlock(&lock);
    return size;
}

int PopOutKeyArray(KEY_DATA* uKeyMsg)
{
    pthread_mutex_lock(&lock);
    if (uKeyIndexOut == uKeyIndexIn)
    {
        pthread_mutex_unlock(&lock);
        return 0;
    }
    else
    {
        *uKeyMsg = KEY_ARRAY[uKeyIndexOut];
        if (++uKeyIndexOut >= MAX_KEY_ARRAY)
            uKeyIndexOut = 0;
        uKeyIndexAdd--;
        pthread_mutex_unlock(&lock);
        return 1;
    }
}

int PopOutKeyArrayByType(KEY_DATA* uKeyMsg,int type)
{
    pthread_mutex_lock(&lock);
    if (uKeyIndexOut == uKeyIndexIn)
    {
        pthread_mutex_unlock(&lock);
        return 0;
    }
    else
    {
    	if(KEY_ARRAY[uKeyIndexOut].keyType!=type)
    	{
		 pthread_mutex_unlock(&lock);
        	return 0;
	}
        *uKeyMsg = KEY_ARRAY[uKeyIndexOut];
        if (++uKeyIndexOut >= MAX_KEY_ARRAY)
            uKeyIndexOut = 0;
        uKeyIndexAdd--;
        pthread_mutex_unlock(&lock);
        return 1;
    }
}

int GetKeyArraySize(void)
{
    int size;
    pthread_mutex_lock(&lock);
    size = uKeyIndexAdd;
    pthread_mutex_unlock(&lock);
    return size;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
// usage: tp press/release x y
// eg:  tp p 10 10  // 按下
// eg:  tp r 10 10  // 释放
// eg:  tp t 10 10  // 模拟一次短按
int ShellCmd_touchpad_io(cJSON *cmd)
{
    KEY_DATA keydat;
    char* str_o = GetShellCmdInputString(cmd, 1);
    char* str_x = GetShellCmdInputString(cmd, 2);
    char* str_y = GetShellCmdInputString(cmd, 3);

    printf("tp[%s](%s,%s)\n",str_o,str_x,str_y);

    if( strcmp(str_o,"t") == 0 )
    {
        keydat.keyStatus = KEY_STATUS_PRESS;
        keydat.TpKey.x  = atoi(str_x);
        keydat.TpKey.y  = atoi(str_y);
        keydat.keyType  = KEY_TYPE_TP;
        PushInKeyArray(keydat);
        keydat.keyStatus = KEY_STATUS_RELEASE;
        PushInKeyArray(keydat);
        printf("totol:%d\n",GetKeyArraySize());
    }
    else
    {
        if( strcmp(str_o,"p") == 0 )
            keydat.keyStatus = KEY_STATUS_PRESS;
        else
            keydat.keyStatus = KEY_STATUS_RELEASE;
        keydat.TpKey.x  = atoi(str_x);
        keydat.TpKey.y  = atoi(str_y);
        keydat.keyType  = KEY_TYPE_TP;
        PushInKeyArray(keydat);
        printf("totol:%d\n",GetKeyArraySize());
    }
}

void uart_touchpad_io(int x, int y)
{
    printf("uart_touchpad(%d,%d)\n",x,y);
    KEY_DATA keydat;
    float diff;
    keydat.keyStatus = KEY_STATUS_PRESS;
    #if 0
    //dt47mg  tp
    diff = 0.85*x;
    keydat.TpKey.x  = 5 + diff;
    diff = 1.14*y;
    keydat.TpKey.y  = 5 + diff;
    #endif
    //dt471 tp
    diff = 0.87*x;
    keydat.TpKey.x  = 8 + diff;
    diff = 1.2*y;
    keydat.TpKey.y  = 8 + diff;
    keydat.keyType  = KEY_TYPE_TP;
    printf("uart_touchpad2(%d,%d)\n",keydat.TpKey.x, keydat.TpKey.y);
    PushInKeyArray(keydat);
    usleep(100*1000);
    keydat.keyStatus = KEY_STATUS_RELEASE;
    PushInKeyArray(keydat);
}

int ShellCmd_touchkey_io(cJSON *cmd)
{
    KEY_DATA keydat;
    char* key = GetShellCmdInputString(cmd, 1);
    if( key != NULL )
    {
        keydat.keyType  = KEY_TYPE_TS;
        keydat.keyStatus = KEY_STATUS_PRESS;
        keydat.keyData = (*key) - '0';
        PushInKeyArray(keydat);
        usleep(100*1000);
        keydat.keyStatus = KEY_STATUS_RELEASE;
        PushInKeyArray(keydat);
    }
    else
    {
        ShellCmdPrintf("ShellCmd_touchkey_io para err!\n");
    }
}

void uart_touchkey_io(char key)
{
    printf("uart_touchkey: %02x\n",key);
    KEY_DATA keydat;
    keydat.keyType  = KEY_TYPE_TS;
    keydat.keyStatus = KEY_STATUS_PRESS;
    keydat.keyData = key - 0x82;
    PushInKeyArray(keydat);
    usleep(100*1000);
    keydat.keyStatus = KEY_STATUS_RELEASE;
    PushInKeyArray(keydat);
}

void cap1203_touchkey_io(char key, int status)
{
    KEY_DATA keydat;
    keydat.keyType  = KEY_TYPE_TS;
    if( !status )
        keydat.keyStatus = KEY_STATUS_RELEASE;
    else
        keydat.keyStatus = KEY_STATUS_PRESS;
    keydat.keyData = key;
    PushInKeyArray(keydat);
}

void matrix_key_io(char key, int status)
{
    KEY_DATA keydat;
    keydat.keyType  = KEY_TYPE_TS;
    if( !status )
        keydat.keyStatus = KEY_STATUS_RELEASE;
    else
        keydat.keyStatus = KEY_STATUS_PRESS;
    keydat.keyData = key;
    PushInKeyArray(keydat);
}
