
#include "utility.h"
#include "one_tiny_task.h"
#include "lvgl.h"
#include "lv_examples.h"

static 	pthread_t 	tast_lvgl_pid = 0;
static pthread_mutex_t  task_lvgl_lock = PTHREAD_MUTEX_INITIALIZER;

one_tiny_task_t		task_lvgl_tick_inc;
one_tiny_task_t		task_lvgl_timer_handle;
static int lvgl_lock_cnt=0;
static pthread_t lvgl_lock_pid=0;
void vtk_lvgl_lock(void)
{
	int cnt=0;
	pthread_t 			self_pid=pthread_self();	
	#if 1
	if(task_lvgl_timer_handle.pid!=self_pid&&task_lvgl_tick_inc.pid!=self_pid)
	#else
	if( tast_lvgl_pid != pthread_self() )
	#endif
	{
		//printf("lock,pid=[%d]\n",pthread_self());
		if(lvgl_lock_cnt==0)
		{
			pthread_mutex_lock(&task_lvgl_lock);
			lvgl_lock_pid=self_pid;
			lvgl_lock_cnt++;
		}
		else if(lvgl_lock_pid==self_pid)
		{
			lvgl_lock_cnt++;
		}
		else
		{
			pthread_mutex_lock(&task_lvgl_lock);
			lvgl_lock_pid=self_pid;
			lvgl_lock_cnt++;
		}
	}
	//else
	//	printf("not lock\n");
}
void vtk_lvgl_unlock(void)
{
	pthread_t 			self_pid=pthread_self();
	#if 1
	if(task_lvgl_timer_handle.pid!=self_pid&&task_lvgl_tick_inc.pid!=self_pid)
	#else
	if( tast_lvgl_pid != pthread_self() )
	#endif
	{
		//printf("unlock,pid=[%d]\n",pthread_self());
		lvgl_lock_cnt--;
		if(lvgl_lock_cnt<=0)
		{
			lvgl_lock_cnt=0;
			lvgl_lock_pid=0;
			pthread_mutex_unlock(&task_lvgl_lock);
		}
	}
	//else
	//	printf("not unlock\n");
}

// tick inc
int task_lvgl_tick_inc_init( void* arg )
{
	//one_tiny_task_t *ptiny_task = (one_tiny_task_t*)arg;

	return 0;
}
int ifinvtklvgl_thread(void)
{
	//if(task_lvgl_timer_handle.pid!=pthread_self()&&task_lvgl_tick_inc.pid!=pthread_self())
	//	return 0;
	return -1;
}
#define ONE_THREAD_RUN

/**********************************************************************
 * 函数名称： custom_tick_get
 * 功能描述： 以毫秒为单位计算当前系统时间
 * 输入参数：
 * 输出参数： 系统运行了多少毫秒
 * 返 回 值：
 ***********************************************************************/
uint32_t custom_tick_get(void)
{
	static uint64_t start_ms = 0;
	if(start_ms == 0)
	{
		struct timeval tv_start;
		gettimeofday(&tv_start, NULL);
		start_ms = (tv_start.tv_sec * 1000000 + tv_start.tv_usec) / 1000;
	}
    struct timeval tv_now;
    gettimeofday(&tv_now, NULL);
    uint64_t now_ms;
    now_ms = (tv_now.tv_sec * 1000000 + tv_now.tv_usec) / 1000;
    uint32_t time_ms = now_ms - start_ms;
    return time_ms;
}

int task_lvgl_tick_inc_process( void* arg )
{
	one_tiny_task_t *ptiny_task = (one_tiny_task_t*)arg;

	struct timespec ts;	
	uint64_t orig;
	uint64_t tick_time;
	uint64_t curtime;
	uint64_t realtime;
	int64_t  diff;	

	printf("lvgl_tick_inc_process...\n");

#if !defined( LVGL_SUPPORT_TDE )
	//rtp_process_set_high_prio();
#endif
	clock_gettime(CLOCK_MONOTONIC,&ts);
	orig = (ts.tv_sec*1000LL) + (ts.tv_nsec+500000LL)/1000000LL;
	tick_time = 0;
	thread_log_add(__func__);
	while( one_tiny_task_is_running(ptiny_task) )
	{	
#if defined( LVGL_SUPPORT_TDE )
		//lv_tick_inc(5);
		#ifdef ONE_THREAD_RUN
#if 0
		pthread_mutex_lock(&task_lvgl_lock);
		lv_task_handler();
		pthread_mutex_unlock(&task_lvgl_lock);
#else
		//vtk_lvgl_lock();
		//lv_task_handler();
		//vtk_lvgl_unlock();
#endif
		#endif
        //ak_sleep_ms(10);
        sleep(10);
#else
		//vtk_lvgl_lock();
		tick_time += 10;
		//lv_tick_inc(10);
		#ifdef ONE_THREAD_RUN
		pthread_mutex_lock(&task_lvgl_lock);
		lv_timer_handler();
		pthread_mutex_unlock(&task_lvgl_lock);
		#endif
		while(1)
		{
			clock_gettime(CLOCK_MONOTONIC,&ts);
			curtime = (ts.tv_sec*1000LL) + (ts.tv_nsec+500000LL)/1000000LL;
			realtime = curtime-orig;		
			diff = tick_time-realtime;
			if( diff <= 0 )
				break;
			ts.tv_sec=0;
			ts.tv_nsec=(int)diff*1000000LL;
			nanosleep(&ts,NULL);
		}
#endif		
		//vtk_lvgl_unlock();
	}

	return 0;
}

int task_lvgl_tick_inc_exit( void* arg )
{
	//one_tiny_task_t *ptiny_task = (one_tiny_task_t*)arg;

	return 0;
}

// timer handle
int task_lvgl_timer_handle_init( void* arg )
{
	//one_tiny_task_t *ptiny_task = (one_tiny_task_t*)arg;

	return 0;
}

int task_lvgl_timer_handle_process( void* arg )
{
	one_tiny_task_t *ptiny_task = (one_tiny_task_t*)arg;
	struct timespec ts;	
	uint64_t orig;
	uint64_t tick_time;
	uint64_t curtime;
	uint64_t realtime;
	int64_t  diff;	
	printf("lvgl_timer_handle_inc_process...\n");
	
	sleep(5);
	thread_log_add(__func__);
	
	clock_gettime(CLOCK_MONOTONIC,&ts);
	orig = (ts.tv_sec*1000LL) + (ts.tv_nsec+500000LL)/1000000LL;
	tick_time = 0;
	//rtp_process_set_high_prio();
	
	while( one_tiny_task_is_running(ptiny_task) )
	{
#if defined( LVGL_SUPPORT_TDE )
		pthread_mutex_lock(&task_lvgl_lock);
		lv_task_handler();
		pthread_mutex_unlock(&task_lvgl_lock);

		#if 0
		clock_gettime(CLOCK_MONOTONIC,&ts);
			curtime = (ts.tv_sec*1000LL) + (ts.tv_nsec+500000LL)/1000000LL;
			diff = curtime-orig;	
			orig=curtime;
			if( diff <= 5 )
				continue;
			if(diff>15)
				diff=15;
			ts.tv_sec=0;
			ts.tv_nsec=(int)(diff-5)*1000000LL;
			nanosleep(&ts,NULL);
		#endif
#if 1
        ak_sleep_ms(2);
#else
        ak_sleep_ms(10);
#endif
#else
		#ifdef ONE_THREAD_RUN
		sleep(10);
		#else
		vtk_lvgl_lock();
		lv_timer_handler();
		vtk_lvgl_unlock();
		usleep(15*1000);
		#endif
#endif
	}
	
	return 0;
}

int task_lvgl_timer_handle_exit( void* arg )
{
	//one_tiny_task_t *ptiny_task = (one_tiny_task_t*)arg;
	return 0;
}



int api_start_lvgl_task(void)
{
	// start tick inc
	one_tiny_task_start( &task_lvgl_tick_inc, task_lvgl_tick_inc_init, task_lvgl_tick_inc_process, task_lvgl_tick_inc_exit, &task_lvgl_tick_inc);
	// start timer handle
	one_tiny_task_start( &task_lvgl_timer_handle, task_lvgl_timer_handle_init, task_lvgl_timer_handle_process, task_lvgl_timer_handle_exit, &task_lvgl_timer_handle);
	tast_lvgl_pid = task_lvgl_timer_handle.pid;
	return 0;	
}

int api_stop_lvgl_task(void)
{
	// stop timer handle
	one_tiny_task_stop( &task_lvgl_timer_handle );
	// stop tick inc
	one_tiny_task_stop( &task_lvgl_tick_inc );
	return 0;
}

