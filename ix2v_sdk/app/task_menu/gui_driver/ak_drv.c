

#include <sys/types.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <error.h>
#include <signal.h>
#include <pthread.h>
#include <semaphore.h>
#include <sched.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <errno.h>

#include <sys/mman.h>
#include <linux/fb.h>

#include "gui_lib.h"

int set_pane_type(int type);

OSD_LAYER					osd_layer;
gui_base_attr_t				gui_attr;
struct fb_fix_screeninfo 	fb_fix_screeninfo_param;  //屏幕固定参数
struct fb_var_screeninfo 	fb_var_screeninfo_param;  //屏幕可配置参数
static pthread_mutex_t		refresh_mutex;

static int menu_with_video_on = 0;

void set_menu_with_video_on(void)
{
#if !defined( LVGL_SUPPORT_TDE )
	struct ak_gp_colorkey colorkey;
	colorkey.coloract  	= COLOR_DELETE;
	colorkey.color_min 	= 0; 	//0; 	// 0 just transparent color
	colorkey.color_max 	= 0; 	//0;
	ak_vo_set_layer_colorkey(AK_VO_LAYER_GUI_1,colorkey);
	pthread_mutex_lock( &refresh_mutex );
	menu_with_video_on = 1;
	pthread_mutex_unlock( &refresh_mutex );
#else
	menu_with_video_on = 1;
#endif	
}

void set_menu_with_video_off(void)
{
#if !defined( LVGL_SUPPORT_TDE )
	struct ak_gp_colorkey colorkey;
	colorkey.coloract  	= COLOR_KEEP;
	colorkey.color_min 	= 0;  			// 0 just transparent color
	colorkey.color_max 	= 0xffffff;
	ak_vo_set_layer_colorkey(AK_VO_LAYER_GUI_1,colorkey);
	pthread_mutex_lock( &refresh_mutex );
	menu_with_video_on = 0;
	pthread_mutex_unlock( &refresh_mutex );
#else
	menu_with_video_on = 0;
#endif	
}

int ak_layer_in_create( OSD_LAYER* player, int ak_vo_group, int ak_vo_layer, int w, int h ) 
{
	int ret;

	player->ak_vo_group					= ak_vo_group;
	player->ak_vo_layer					= ak_vo_layer;
	player->layer.create_layer.height 	= h;
	player->layer.create_layer.width  	= w;
    player->layer.create_layer.left  	= 0;
    player->layer.create_layer.top   	= 0;
    player->layer.format             	= GP_FORMAT_RGBXXX;

	player->layer.layer_opt          	= GP_OPT_COLORKEY;
	player->layer.alpha			 		= 0;
	#if 1
	player->layer.colorkey.coloract  	= COLOR_DELETE; //COLOR_KEEP;
	player->layer.colorkey.color_min 	= 0; 	//0; //1;  			// 0 just transparent color
	player->layer.colorkey.color_max 	= 0; 	//0; //0xffffff;
	#else
	player->layer.colorkey.coloract  	= COLOR_KEEP;
	player->layer.colorkey.color_min 	= 0;  			// 0 just transparent color
	player->layer.colorkey.color_max 	= 0xffffff;
	#endif
	/*
	player->layer.layer_opt          	= GP_OPT_BLIT; //|GP_OPT_COLORKEY;
	//player->layer.alpha			 	= 0;
	
	player->layer.colorkey.coloract  	= COLOR_DELETE;
	player->layer.colorkey.color_min 	= 0; //1;  			// 0 just transparent color
	player->layer.colorkey.color_max 	= 0; //0xffffff;
	*/

	printf("---w[%d],h[%d]---\n",player->layer.create_layer.width,player->layer.create_layer.height); 

    ret = ak_vo_create_gui_layer(&player->layer, ak_vo_layer, &player->vo_out_info);

    if(ret != 0)
    {
        ak_print_error_ex(MODULE_ID_VO, "ak_vo_create_gui_layer failed![%d]\n",ret);
        ak_vo_destroy_layer(ak_vo_layer);
        return AK_FAILED;	
    }
	printf("---------------ak_layer_create layer[%d] ok!------------\n", ak_vo_layer); 
	return 0;
}

static int					refresh_state;
static int 					refresh_cmd;

static ak_pthread_t			refresh_id;
static ak_sem_t				refresh_sem;
#define REFRESH_TIMER		20 	// 20 ms

// 同一层的刷新需要立即执行，不漏显示
// 不同层的刷新可以合并执行，节省时间

void ak_ui_refresh_cmd_set(int layer)
{	
	pthread_mutex_lock( &refresh_mutex );
	if( !menu_with_video_on )
	{
		ak_vo_refresh_screen(layer);
	}
	pthread_mutex_unlock( &refresh_mutex );
}

void ak_vo_refresh_cmd_set(int layer)
{
	pthread_mutex_lock( &refresh_mutex );
	refresh_cmd |= layer;
	//ak_thread_sem_post(&refresh_sem);
	pthread_mutex_unlock( &refresh_mutex );
}

void* ak_vo_refresh_process(void)
{
	ak_mutexattr_t	refresh_attr;
	refresh_cmd 	= 0;
	refresh_state 	= 1;
 	unsigned long nsec;
	struct timespec	wait_time;
	while(1)
	{	
		clock_gettime(CLOCK_REALTIME, &wait_time);
		nsec = wait_time.tv_nsec + REFRESH_TIMER*1000000;
		wait_time.tv_nsec = nsec%1000000000;
		wait_time.tv_sec += nsec/1000000000;
		ak_thread_sem_timedwait(&refresh_sem,&wait_time);
		//ak_thread_sem_wait(&refresh_sem);
		
		pthread_mutex_lock( &refresh_mutex );

		if( menu_with_video_on )
		{
			refresh_cmd |= REFRESH_DEFAULT;
			ak_vo_refresh_screen(refresh_cmd);		
		}

		pthread_mutex_unlock( &refresh_mutex );
	}
	return NULL;
}

void ak_vo_refresh_start(void)
{
	ak_thread_sem_init(&refresh_sem,0);
	pthread_mutex_init(&refresh_mutex,0);
	refresh_state = 0;
	ak_thread_create(&refresh_id, ak_vo_refresh_process, NULL, ANYKA_THREAD_MIN_STACK_SIZE, -1);	
	printf("ak_vo_refresh_start ok!\n");	
}

void ak_vo_refresh_stop(void)
{
	if( refresh_state )
	{
		refresh_state = 0;
		ak_thread_cancel(refresh_id);
		ak_thread_join(refresh_id);
		ak_thread_mutex_destroy(&refresh_mutex);
	}
	printf("ak_vo_refresh_stop ok!\n");	
}

int InitOsdLayer( OSD_LAYER* pOsdLayer, int ak_vo_group, int ak_vo_layer, int w, int h, int bpp_len )
{
	pOsdLayer->width	= w;
	pOsdLayer->height	= h;
	pOsdLayer->bpp_len	= bpp_len;
	pOsdLayer->buf_size	= w*h*bpp_len;	
	pOsdLayer->pbuf 	= ak_mem_dma_alloc(MODULE_ID_VO,pOsdLayer->buf_size);	
	if( pOsdLayer->pbuf == NULL )
	{
		printf("osd_layer init fail!\n"); 
	}
	else
	{
		printf("osd_layer init ok!\n"); 	
		ak_layer_in_create(pOsdLayer,ak_vo_group,ak_vo_layer,w,h);	
	}
	return 0;
}


int ak_gui_dev_initial(void)
{
    gui_attr.fd = open( DEV_GUI, O_RDWR );
	if( gui_attr.fd < 0 ) 
	{
		printf("Can not open fb0!\n");
		return -1;
	}
	
	if (ioctl(gui_attr.fd, FBIOGET_FSCREENINFO, &fb_fix_screeninfo_param) < 0) 
	{
		perror("ioctl FBIOGET_FSCREENINFO");
		close(gui_attr.fd);
		return -1;
	}

	if( ioctl(gui_attr.fd, FBIOGET_VSCREENINFO, &fb_var_screeninfo_param) < 0) 
	{
		perror("ioctl FBIOGET_VSCREENINFO");
		close(gui_attr.fd);
		return -1;
	}

	gui_attr.pframebuf = mmap(NULL, fb_fix_screeninfo_param.smem_len, PROT_READ|PROT_WRITE, MAP_SHARED, gui_attr.fd, 0);
	if( gui_attr.pframebuf == MAP_FAILED )
	{
		printf("LCD Video Map failed!\n");
	}
	memset( gui_attr.pframebuf, 0 , fb_fix_screeninfo_param.smem_len );	

	fb_var_screeninfo_param.xres 			= fb_var_screeninfo_param.xres_virtual;
	fb_var_screeninfo_param.yres 			= fb_var_screeninfo_param.yres_virtual;
	fb_var_screeninfo_param.bits_per_pixel 	= BITS_PER_PIXEL;
	fb_var_screeninfo_param.red.offset 		= OFFSET_RED;
	fb_var_screeninfo_param.red.length 		= LEN_COLOR_R ;
	fb_var_screeninfo_param.green.offset 	= OFFSET_GREEN;
	fb_var_screeninfo_param.green.length 	= LEN_COLOR_G ;
	fb_var_screeninfo_param.blue.offset 	= OFFSET_BLUE;
	fb_var_screeninfo_param.blue.length 	= LEN_COLOR_B ;

	if (ioctl(gui_attr.fd, FBIOPUT_VSCREENINFO, &fb_var_screeninfo_param) < 0) 
	{
		close( gui_attr.fd );
		return -1;
	}

    if ( ( DUAL_FB_FIX == AK_TRUE ) && ( DUAL_FB_VAR != 0 ) ) 
	{
        DUAL_FB_VAR = 0;
        ioctl( gui_attr.fd, FBIOPUT_VSCREENINFO, &fb_var_screeninfo_param ) ;
    }
	
	int fb_xres = fb_var_screeninfo_param.xres;
	int fb_yres = fb_var_screeninfo_param.yres;
	int fb_pixel= fb_var_screeninfo_param.bits_per_pixel;

	gui_attr.max_size 			= fb_xres * fb_yres * fb_pixel / 8;	

#ifdef PID_IX850
	gui_attr.max_w				= fb_yres;		// notice here，just yres
	gui_attr.max_h				= fb_xres;		// notice here，just xres
#else
	gui_attr.max_w				= fb_xres;
	gui_attr.max_h				= fb_yres;
#endif
	gui_attr.bpp_len			= fb_pixel/8;
	gui_attr.fix_smem_len		= fb_fix_screeninfo_param.smem_len;
	printf("var.bits_per_pixel = %d,var.xres = %d,var.yres = %d\n", fb_pixel,fb_xres,fb_yres);
	printf("fix.smem_len = 0x%x\n", fb_fix_screeninfo_param.smem_len);

	int ret;

    struct ak_vo_param	param;

    /* fill the struct ready to open */
	param.width  = gui_attr.max_w;
	param.height = gui_attr.max_h;
    param.format = GP_OUTPUT_RGBXXX;	//GP_FORMAT_RGBXXX;  		//format to output
#ifdef PID_IX850
	param.rotate = AK_GP_ROTATE_90;    	//270 rotate value
#else	
	param.rotate = AK_GP_ROTATE_NONE; 
#endif	
    /* open vo */
    ret = ak_vo_open(&param, DEV_NUM);  //open vo

    if(ret != 0)
    {
        /* open failed return -1 */
        ak_print_error_ex(MODULE_ID_VO, "ak_vo_open failed![%d]\n",ret);
        return AK_FAILED;	
    }

    /* use the double buff mode */
    //ak_vo_set_fbuffer_mode(AK_VO_BUFF_SINGLE);
    ak_vo_set_fbuffer_mode(AK_VO_BUFF_DOUBLE);
	//ak_vo_set_layer_alpha(USE_GUI_LAYER_1,15);

	InitOsdLayer(&osd_layer, AK_VO_REFRESH_GUI_GROUP, AK_VO_LAYER_GUI_1, gui_attr.max_w, gui_attr.max_h, gui_attr.bpp_len);

	set_menu_with_video_off();
	
#if !defined( LVGL_SUPPORT_TDE )
	ak_vo_refresh_start();	
#endif

#if defined(PID_IX850)
	set_pane_type(3);
#endif

	return 0;
}

void ak_gui_dev_deinitial(void)
{

}

void ak_gui_area_update( int x, int y, int sx, int sy, unsigned short* pdat)
{
	int i;
	int location;

	if( x < 0 ) return;
	if( y < 0 ) return;
	if( x > osd_layer.width  ) return;
	if( y > osd_layer.height ) return;
	if( sx > osd_layer.width ) 	return;
	if( sy > osd_layer.height) 	return;	
	if( (x+sx) > osd_layer.width  ) return;
	if( (y+sy) > osd_layer.height ) return;	
#if 1
	memcpy( osd_layer.pbuf, pdat, sx*sy*2);
	/* set obj src info*/	
	osd_layer.vo_obj.format 					= GP_FORMAT_RGBXXX;
	osd_layer.vo_obj.alpha 						= 10;
	osd_layer.vo_obj.cmd 						= GP_OPT_BLIT;
	//osd_layer.vo_obj.cmd 						= GP_OPT_BLIT|GP_OPT_COLORKEY|GP_OPT_TRANSPARENT;
	osd_layer.vo_obj.vo_layer.width 			= sx;
	osd_layer.vo_obj.vo_layer.height 			= sy;
	osd_layer.vo_obj.vo_layer.clip_pos.top 		= 0;
	osd_layer.vo_obj.vo_layer.clip_pos.left 	= 0;
	osd_layer.vo_obj.vo_layer.clip_pos.width 	= sx;
	osd_layer.vo_obj.vo_layer.clip_pos.height 	= sy;
	ak_mem_dma_vaddr2paddr(osd_layer.pbuf, &(osd_layer.vo_obj.vo_layer.dma_addr));	
	// set dst_layer 1 info
	osd_layer.vo_obj.dst_layer.top 				= y;
	osd_layer.vo_obj.dst_layer.left 			= x;
	osd_layer.vo_obj.dst_layer.width 			= sx;
	osd_layer.vo_obj.dst_layer.height 			= sy;
	ak_vo_add_obj(&osd_layer.vo_obj, AK_VO_LAYER_GUI_1);
	// display update
	ak_ui_refresh_cmd_set(REFRESH_DEFAULT);
#else
	unsigned short* fbp16 = (unsigned short*)osd_layer.vo_out_info.vir_addr;
	for(i = 0; i < sy; i++) 
	{
		location = x + (i+y)*osd_layer.width;
		memcpy(&fbp16[location], pdat, sx*2);
		pdat += sx;
	}
	ak_vo_refresh_screen(REFRESH_DEFAULT);
#endif	
}


int m_PANEL_TYPE = IX850;
int bkgd_w 	= 0;
int bkgd_h	= 0;

const int PANEL_RESOLUTION_TAB[IXTYPE_MAX*2]=
{
// 	width, 	height
	1280,	800,		// IX481H
	800,	1280,		// IX481V
	1024,	600,		// IX482
	480,	800,		// IX850
	1024,	600,		// IX47
	720,	576,		// IXG
};

int get_pane_type(void)
{
	return m_PANEL_TYPE;
}

int set_pane_type(int type)
{
	printf("set_pane_type = %d\n", type);
	m_PANEL_TYPE = type;
	bkgd_w = PANEL_RESOLUTION_TAB[m_PANEL_TYPE*2+0];
	bkgd_h = PANEL_RESOLUTION_TAB[m_PANEL_TYPE*2+1];	
	return 0;
}
