
#ifndef _VIRT_IO_H_
#define _VIRT_IO_H_

typedef struct TP_POINT_
{
	short x;
	short y;
} TP_POINT;

typedef struct
{
	TP_POINT        TpKey;			//touch panel coord
	unsigned char   keyType;		//key type
	unsigned char   keyData;		//touch key data
	unsigned char   keyStatus;	    //public key status
	unsigned char   keyStatus2;	    //public key status		
} KEY_DATA;

#define KEY_TYPE_PH         		0 // physical key
#define KEY_TYPE_TS         		1 // touch sense key
#define KEY_TYPE_HK         		2 // hook key
#define KEY_TYPE_DB         		3 // door bell
#define KEY_TYPE_TP         		4 // touch pad key
#define KEY_TYPE_DIP 		        5 //dip

#define KEY_STATUS_PRESS    		0
#define KEY_STATUS_RELEASE  		1
#define KEY_STATUS_3SECOND			2

// touch key
//#define KEY_OK                  0x01       
//#define KEY_UP                  0x02
//#define KEY_DOWN                0x03
//#define KEY_BACK                0x04


int PushInKeyArray(const KEY_DATA uKeyMsg);
int PopOutKeyArray(KEY_DATA* uKeyMsg);
int GetKeyArraySize(void);

void cap1203_touchkey_io(char key, int status);
void matrix_key_io(char key, int status);

#endif
