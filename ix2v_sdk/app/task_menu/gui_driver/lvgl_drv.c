
#include <sys/types.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <error.h>
#include <sys/mman.h>
#include <linux/fb.h>
#include <termios.h>
#include <sys/time.h>
#include <fcntl.h>
#include <linux/input.h>

#include "lvgl.h"
#include "fbdev.h"
#include "evdev.h"
#include "one_lvgl_task.h"

#include "gui_lib.h"
#include "virt_io.h"
#include "task_Beeper.h"

#if defined( LVGL_SUPPORT_TDE )

#ifdef PID_IX850
#define GUI_ROTATE_DEGREE		AK_GP_ROTATE_90
#else
#define GUI_ROTATE_DEGREE		AK_GP_ROTATE_NONE
#endif

#include "src/draw/sw/lv_draw_sw.h"
void draw_ctx_init(lv_disp_drv_t * drv, lv_draw_ctx_t * draw_ctx);
void draw_ctx_deinit(lv_disp_drv_t * drv, lv_draw_ctx_t * draw_ctx);
void draw_ctx_wait_for_finish(lv_draw_ctx_t * draw_ctx);
void draw_ctx_blend(lv_draw_ctx_t * draw_ctx, const lv_draw_sw_blend_dsc_t * dsc);

#define PFINTF_FPS                  0               // 串口打印帧率开关，1 为打开，0 为关闭
#define SUPPORT_HARDWARE_RENDERER   0               // 硬件绘制图形开关，暂不支持，只能设置为 0

extern struct fb_fix_screeninfo 	fb_fix_screeninfo_param;
extern struct fb_var_screeninfo 	fb_var_screeninfo_param;
extern gui_base_attr_t				gui_attr;

static unsigned long draw_buf_paddr_1 = 0;         // lvgl 绘图缓冲区对应的物理地址
static void* draw_buf_p_1 = NULL;
#if PFINTF_FPS
static int i_times = 0;
static unsigned long long i_us ;
static struct timeval ak_timeval_begin_fps, ak_timeval_end_fps;
/**********************************************************************
 * 函数名称： timeval_mark
 * 功能描述： 对一个 timeval 结构体时间赋予当前时间戳
 * 输入参数： 
 * 输出参数： 
 * 返 回 值： struct timeval
 ***********************************************************************/
static struct timeval *timeval_mark(struct timeval *p_timeval)
{
    gettimeofday(p_timeval, 0);
    return p_timeval;
}

/**********************************************************************
 * 函数名称： timeval_count
 * 功能描述： 对两个 timeval 之间的时间差进行统计
 * 输入参数： 
 * 输出参数： 
 * 返 回 值： long long
 ***********************************************************************/
static long long timeval_count(struct timeval *p_timeval_begin , struct timeval *p_timeval_end)
{
    long long i_time = (p_timeval_end->tv_sec - p_timeval_begin->tv_sec )*1000000 + ( p_timeval_end->tv_usec - p_timeval_begin->tv_usec);
    if (i_time > 0)
        return i_time;
    else
        return 0 ;
}
#endif /* PFINTF_FPS */

/**********************************************************************
 * 函数名称： set_tde_layer
 * 功能描述： 设置图层描述结构
 * 输入参数： 
 * 输出参数： 
 * 返 回 值： void
 ***********************************************************************/
static inline void set_tde_layer( struct ak_tde_layer *p_tde_layer, unsigned short lw, unsigned short lh,
                                    unsigned short x, unsigned short y, unsigned short w, unsigned short h,
                                    unsigned int p, enum ak_gp_format f )
{
    if( p_tde_layer != NULL )
    {
        p_tde_layer->width = lw;
        p_tde_layer->height = lh;
        p_tde_layer->pos_left = x;
        p_tde_layer->pos_top = y;
        p_tde_layer->pos_width = w;
        p_tde_layer->pos_height = h;
        p_tde_layer->phyaddr = p;
        p_tde_layer->format_param = f;
    }

    return;
}

/**********************************************************************
 * 函数名称： draw_ctx_init
 * 功能描述： 自定义绘图接口初始化
 * 输入参数： 
 * 输出参数： 
 * 返 回 值： void
 ***********************************************************************/
void draw_ctx_init(lv_disp_drv_t * drv, lv_draw_ctx_t * draw_ctx)
{
    lv_draw_sw_init_ctx(drv, draw_ctx);

    lv_draw_sw_ctx_t * sw_draw_ctx = (lv_draw_sw_ctx_t *)draw_ctx;

    sw_draw_ctx->blend = draw_ctx_blend;
    sw_draw_ctx->base_draw.wait_for_finish = draw_ctx_wait_for_finish;
}

/**********************************************************************
 * 函数名称： draw_ctx_init
 * 功能描述： 自定义绘图接口反初始化
 * 输入参数： 
 * 输出参数： 
 * 返 回 值： void
 ***********************************************************************/
void draw_ctx_deinit(lv_disp_drv_t * drv, lv_draw_ctx_t * draw_ctx)
{
    LV_UNUSED(drv);

    lv_draw_sw_ctx_t * draw_sw_ctx = (lv_draw_sw_ctx_t *) draw_ctx;
    lv_memset_00(draw_sw_ctx, sizeof(lv_draw_sw_ctx_t));
}

/**********************************************************************
 * 函数名称： draw_ctx_wait_for_finish
 * 功能描述： 自定义绘图接口等待完成
 * 输入参数： 
 * 输出参数： 
 * 返 回 值： void
 ***********************************************************************/
void draw_ctx_wait_for_finish(lv_draw_ctx_t * draw_ctx)
{
    LV_UNUSED(draw_ctx);
    /*Nothing to wait for*/
}

/**********************************************************************
 * 函数名称： draw_ctx_blend
 * 功能描述： 自定义绘图填充接口
 * 输入参数： 
 * 输出参数： 
 * 返 回 值： void
 ***********************************************************************/
void draw_ctx_blend(lv_draw_ctx_t * draw_ctx, const lv_draw_sw_blend_dsc_t * dsc)
{
    int ret = -1;

    lv_area_t blend_area;
    if (!_lv_area_intersect(&blend_area, dsc->blend_area, draw_ctx->clip_area)) return;

    const lv_opa_t * mask;
    if(dsc->mask_buf == NULL) mask = NULL;
    if(dsc->mask_buf && dsc->mask_res == LV_DRAW_MASK_RES_TRANSP) return;
    else if(dsc->mask_res == LV_DRAW_MASK_RES_FULL_COVER) mask = NULL;
    else mask = dsc->mask_buf;

    if (NULL != mask)
	{
        goto EXIT;
	}

    if ((dsc->mask_buf == NULL) && ((dsc->blend_mode == LV_BLEND_MODE_NORMAL) || (dsc->blend_mode == LV_BLEND_MODE_REPLACE)))
    {
        struct ak_tde_layer tde_layer_src;
        struct ak_tde_layer tde_layer_dst;

        lv_coord_t dst_w = lv_area_get_width(draw_ctx->buf_area);
        lv_coord_t dst_h = lv_area_get_height(draw_ctx->buf_area);

        lv_coord_t clip_w = lv_area_get_width(&blend_area);
        lv_coord_t clip_h = lv_area_get_height(&blend_area);
        lv_coord_t clip_x = (blend_area.x1 - draw_ctx->buf_area->x1);
        lv_coord_t clip_y = (blend_area.y1 - draw_ctx->buf_area->y1);

        ak_mem_dma_vaddr2paddr((void*)(draw_ctx->buf), (unsigned long *)&tde_layer_dst.phyaddr);

        set_tde_layer(&tde_layer_dst, dst_w, dst_h, clip_x, clip_y, clip_w, clip_h, tde_layer_dst.phyaddr , GP_OUTPUT_RGBXXX);

        if ((tde_layer_dst.width == 0) || (tde_layer_dst.height == 0) || (tde_layer_dst.pos_width < 18) || (tde_layer_dst.pos_height < 18))
        {
            ret = -1;
            goto EXIT;
        }

        if (NULL == (dsc->src_buf))
        {
            if ((dsc->opa >= LV_OPA_MAX) && (MY_DISP_HOR_RES == clip_w))
            {
                unsigned short color_565 = *(unsigned short *)&(dsc->color);
                unsigned int color_888 = (color_565 & 0xF800) << 8 | (color_565 & 0x7E0) << 5 | (color_565 & 0x1F) << 3;

                /* 根据 tde_layer 指定的图层区域进行单色填充 */
                if(ak_tde_opt_fillrect(&tde_layer_dst, color_888))
                    ak_print_error_ex(MODULE_ID_APP, "<ak_tde_opt_fillrect> error.\n");
            }
            goto EXIT;
        }

        tde_layer_src.phyaddr = 0;
        ak_mem_dma_vaddr2paddr((void*)(dsc->src_buf), (unsigned long *)&tde_layer_src.phyaddr);

        if (0 == (tde_layer_src.phyaddr))
		{
            goto EXIT;
		}

        lv_coord_t src_w = lv_area_get_width(dsc->blend_area);
        lv_coord_t src_h = lv_area_get_height(dsc->blend_area);
        lv_coord_t blend_w = lv_area_get_width(&blend_area);
        lv_coord_t blend_h = lv_area_get_height(&blend_area);
        lv_coord_t blend_x = (blend_area.x1 - dsc->blend_area->x1);
        lv_coord_t blend_y = (blend_area.y1 - dsc->blend_area->y1);

        set_tde_layer(&tde_layer_src, src_w, src_h, blend_x, blend_y, blend_w, blend_h, tde_layer_src.phyaddr, GP_FORMAT_RGBXXX);

        if ((tde_layer_src.width == 0) || (tde_layer_src.height == 0) || (tde_layer_src.pos_width < 18) || (tde_layer_src.pos_height < 18))
            {
                ret = -1;
				ak_print_error_ex(MODULE_ID_APP, "exit 1...\n");
                goto EXIT;
            }

        if (dsc->opa >= LV_OPA_MAX)
        {
            /* 拷贝图层 */
			if( ak_tde_opt_blit(&tde_layer_src, &tde_layer_dst))
				ak_print_error_ex(MODULE_ID_APP, "<ak_tde_opt_blit> error.\n");
			goto EXIT;
        }
        else
        {
 /*         将lvgl opa 的 11 个等级 除以 17，转换为 ak_tde alpha 相应的 15 个等级：
 *          LV_OPA_TRANSP = 0,     ----- /17 == 0
 *          LV_OPA_0      = 0,     ----- /17 == 0
 *          LV_OPA_10     = 25,    ----- /17 == 1
 *          LV_OPA_20     = 51,    ----- /17 == 3
 *          LV_OPA_30     = 76,    ----- /17 == 4
 *          LV_OPA_40     = 102,   ----- /17 == 6
 *          LV_OPA_50     = 127,   ----- /17 == 7
 *          LV_OPA_60     = 153,   ----- /17 == 9
 *          LV_OPA_70     = 178,   ----- /17 == 10
 *          LV_OPA_80     = 204,   ----- /17 == 12
 *          LV_OPA_90     = 229,   ----- /17 == 13
 *          LV_OPA_100    = 255,   ----- /17 == 15
 *          LV_OPA_COVER  = 255,   ----- /17 == 15          */
            int alpha = (dsc->opa) / 17;
            /* 将源图层进行透明度处理，再贴到目标图层 */
            if(ak_tde_opt_transparent( &tde_layer_src, &tde_layer_dst, alpha))
                ak_print_error_ex(MODULE_ID_APP, "<ak_tde_opt_transparent> error.\n");

    		printf("alpha:w= %d, h= %d, w1 = %d  h1 = %d\n",tde_layer_src.width,tde_layer_src.height,tde_layer_src.pos_width,tde_layer_src.pos_height);
			ak_print_error_ex(MODULE_ID_APP, "exit 3...\n");
            goto EXIT;
        }
    }

EXIT:
    if (ret)	
        lv_draw_sw_blend_basic(draw_ctx, dsc);

    return;
}

///////////////////////////////////////////////////////////
static struct ak_timeval page_sw_begin, page_sw_end;
static int page_switch_flag = 0;
static int page_switch_time = 0;
static char page_switch_name[200] = {0};
void page_switch_start(const char* pagename)
{
    ak_get_ostime(&page_sw_begin);
    page_switch_flag = 1;
    strcpy( page_switch_name, pagename );
}
void page_switch_end(void)
{
    ak_get_ostime(&page_sw_end);
    if( page_switch_flag )
    {
        page_switch_flag = 0;
        page_switch_time = ak_diff_ms_time( &page_sw_end, &page_sw_begin);       
        API_PublicInfo_Write_Int("switch_time",page_switch_time);
        printf("--------page[%s] switch time %d ms-------\n",page_switch_name,page_switch_time);
    }
}

/**********************************************************************
 * 函数名称： fbdev_flush
 * 功能描述： Flush the content of the internal buffer the specific area on the display
 * 输入参数： 
 * 输出参数： 
 * 返 回 值： void
 ***********************************************************************/
static void tde_fbdev_flush(lv_disp_drv_t * disp_drv, const lv_area_t * area, lv_color_t * color_p)
{
    //int i;
    //printf("flush:w= %d, h= %d x1 = %d  y1 = %d ifinvtklvgl_thread()=%d\n",lv_area_get_width(area), lv_area_get_height(area), area->x1, area->y1,ifinvtklvgl_thread());
    //if( color_p[0].full != 0 ) printf("[%08x]\n",color_p[0].full);

    struct ak_tde_layer tde_layer_src;        // 图层描述结构1,绑定lvgl绘图物理地址
    struct ak_tde_layer tde_layer_dst;        // 图层描述结构2,绑定fb的物理地址

    if(fb_var_screeninfo_param.reserved [ 0 ] == 0)
    {
        fb_var_screeninfo_param.reserved [ 0 ] = 1;
        set_tde_layer( &tde_layer_dst, fb_var_screeninfo_param.xres, fb_var_screeninfo_param.yres, 0, 0, fb_var_screeninfo_param.xres, fb_var_screeninfo_param.yres, fb_fix_screeninfo_param.smem_start + fb_fix_screeninfo_param.smem_len / 2, GP_OUTPUT_RGBXXX );
    }
    else
    {
        fb_var_screeninfo_param.reserved [ 0 ] = 0;
        set_tde_layer( &tde_layer_dst, fb_var_screeninfo_param.xres, fb_var_screeninfo_param.yres, 0, 0, fb_var_screeninfo_param.xres, fb_var_screeninfo_param.yres, fb_fix_screeninfo_param.smem_start, GP_OUTPUT_RGBXXX );
    }
    set_tde_layer( &tde_layer_src, gui_attr.max_w, gui_attr.max_h, 0, 0, gui_attr.max_w, gui_attr.max_h, draw_buf_paddr_1, GP_FORMAT_RGBXXX );
    ak_tde_opt_rotate(&tde_layer_src, &tde_layer_dst,GUI_ROTATE_DEGREE);
    int m;
    char c_get_time = AK_FALSE;
    struct ak_timeval ak_timeval_begin, ak_timeval_end;
    int lcdc_status;
    unsigned long lcdc_is_busy;
    
    /* 轮询等待lcdc控制器可用, 这里设置查询次数为 5 */
    for(m = 0; m < 5; m++)
    {
        if(c_get_time == AK_TRUE)
        {
            ak_get_ostime(&ak_timeval_end);
            /* 在循环时间超过 20 ms 则直接退出循环 */
            if (ak_diff_ms_time( &ak_timeval_end, &ak_timeval_begin) > 20) 
                break;
        }

        /* 0x4680,should match the cmd defined in ak_fb.c */
        if(ioctl( gui_attr.fd, 0x4680, &lcdc_is_busy) < 0) 
            lcdc_status = AK_FAILED;
        else
            lcdc_status = AK_SUCCESS;
        if ((lcdc_status == AK_SUCCESS) && (lcdc_is_busy == AK_FALSE))
            break;
        if(c_get_time == AK_FALSE)
        {
            ak_get_ostime(&ak_timeval_begin);
            c_get_time = AK_TRUE;
        }
        ak_sleep_ms(1);
    }

    /* May be some direct update command is required */
    if (fb_fix_screeninfo_param.reserved[ 0 ] == AK_TRUE)
    {
		if(ioctl(gui_attr.fd, FBIOPUT_VSCREENINFO, &fb_var_screeninfo_param) < 0)
            ak_print_error_ex(MODULE_ID_APP, "FBIOPUT_VSCREENINFO failed!\n");
    }

#if PFINTF_FPS
    i_times ++;
    
    /* 每隔 25 帧打印一次 */
    if( i_times == 25 )
    {
        timeval_mark( &ak_timeval_end_fps ) ;
        i_us = timeval_count( &ak_timeval_begin_fps , &ak_timeval_end_fps );
        //ak_print_normal(MODULE_ID_APP, "fps = %f\n", ( double )i_times / i_us * 1000000);
        ak_timeval_begin_fps = ak_timeval_end_fps;
        i_times = 0;
    }
#endif  /* PFINTF_FPS */

    /* lv_disp_flush_ready() has to be called when finished */
    lv_disp_flush_ready(disp_drv);
    // 
    page_switch_end();
}

#endif

static lv_disp_t * disp;
static lv_disp_draw_buf_t disp_buf;
static lv_color_t buf_1[MY_DISP_HOR_RES*MY_DISP_VER_RES];
static lv_color_t buf_2[MY_DISP_HOR_RES*MY_DISP_VER_RES];
static lv_disp_drv_t disp_drv;
static lv_indev_drv_t indev_drv;
static lv_indev_drv_t tskey_drv;
static lv_indev_drv_t cap1203_drv;
static lv_group_t *g;

void vtk_fbdev_flush(lv_disp_drv_t * drv, const lv_area_t * area, lv_color_t * color_p)
{
#if !defined( LVGL_SUPPORT_TDE )
	//printf("vtk_fbdev_flush, area->x1[%d],area->y1[%d],area->x2[%d],area->y1[%d]\n",area->x1,area->y1,area->x2-area->x1+1,area->y2-area->y1+1);
	ak_gui_area_update(area->x1,area->y1,area->x2-area->x1+1,area->y2-area->y1+1,(unsigned short*)color_p);
    lv_disp_flush_ready(drv);
#else
	tde_fbdev_flush(drv,area,color_p);
#endif	
}

static KEY_DATA keydat = {0};
static void virt_io_read(lv_indev_drv_t* indev_drv, lv_indev_data_t * data)
{
	#if 0
	if(PopOutKeyArray(&keydat)==0)
		return 0;
	#endif
	if(PopOutKeyArrayByType(&keydat,KEY_TYPE_TP)==0)
		return;
	//printf("@@@@@@111111111111111\n");
	switch( keydat.keyType )
	{
		case KEY_TYPE_TP:
			data->state 	= (keydat.keyStatus==KEY_STATUS_PRESS)?LV_INDEV_STATE_PR:LV_INDEV_STATE_REL;
			data->point.x 	= keydat.TpKey.x;
			data->point.y 	= keydat.TpKey.y;
			if( GetKeyArraySize() )
				data->continue_reading = true;
			else
				data->continue_reading = false;
			break;
		default:
			break;
	}
	//printf("##########111111111111111\n");
}
// type of lv_event_code_t;
const char* EVENT_STR[]=
{
    "LV_EVENT_ALL = 0",
    "LV_EVENT_PRESSED",
    "LV_EVENT_PRESSING",
    "LV_EVENT_PRESS_LOST",
    "LV_EVENT_SHORT_CLICKED",
    "LV_EVENT_LONG_PRESSED",
    "LV_EVENT_LONG_PRESSED_REPEAT",
    "LV_EVENT_CLICKED",
    "LV_EVENT_RELEASED",
    "LV_EVENT_SCROLL_BEGIN",
    "LV_EVENT_SCROLL_END",
    "LV_EVENT_SCROLL",
    "LV_EVENT_GESTURE",
    "LV_EVENT_KEY",
    "LV_EVENT_FOCUSED",
    "LV_EVENT_DEFOCUSED",
    "LV_EVENT_LEAVE",
    "LV_EVENT_HIT_TEST",
     /** Drawing events*/
    "LV_EVENT_COVER_CHECK",        /**< Check if the object fully covers an area. The event parameter is `lv_cover_check_info_t *`.*/
    "LV_EVENT_REFR_EXT_DRAW_SIZE", /**< Get the required extra draw area around the object (e.g. for shadow). The event parameter is `lv_coord_t *` to store the size.*/
    "LV_EVENT_DRAW_MAIN_BEGIN",    /**< Starting the main drawing phase*/
    "LV_EVENT_DRAW_MAIN",          /**< Perform the main drawing*/
    "LV_EVENT_DRAW_MAIN_END",      /**< Finishing the main drawing phase*/
    "LV_EVENT_DRAW_POST_BEGIN",    /**< Starting the post draw phase (when all children are drawn)*/
    "LV_EVENT_DRAW_POST",          /**< Perform the post draw phase (when all children are drawn)*/
    "LV_EVENT_DRAW_POST_END",      /**< Finishing the post draw phase (when all children are drawn)*/
    "LV_EVENT_DRAW_PART_BEGIN",    /**< Starting to draw a part. The event parameter is `lv_obj_draw_dsc_t *`. */
    "LV_EVENT_DRAW_PART_END",      /**< Finishing to draw a part. The event parameter is `lv_obj_draw_dsc_t *`. */

    /** Special events*/
    "LV_EVENT_VALUE_CHANGED",       /**< The object's value has changed (i.e. slider moved)*/
    "LV_EVENT_INSERT",              /**< A text is inserted to the object. The event data is `char *` being inserted.*/
    "LV_EVENT_REFRESH",             /**< Notify the object to refresh something on it (for the user)*/
    "LV_EVENT_READY",               /**< A process has finished*/
    "LV_EVENT_CANCEL",              /**< A process has been cancelled */

    /** Other events*/
    "LV_EVENT_DELETE",              /**< Object is being deleted*/
    "LV_EVENT_CHILD_CHANGED",       /**< Child was removed, added, or its size, position were changed */
    "LV_EVENT_CHILD_CREATED",       /**< Child was created, always bubbles up to all parents*/
    "LV_EVENT_CHILD_DELETED",       /**< Child was deleted, always bubbles up to all parents*/
    "LV_EVENT_SCREEN_UNLOAD_START", /**< A screen unload started, fired immediately when scr_load is called*/
    "LV_EVENT_SCREEN_LOAD_START",   /**< A screen load started, fired when the screen change delay is expired*/
    "LV_EVENT_SCREEN_LOADED",       /**< A screen was loaded*/
    "LV_EVENT_SCREEN_UNLOADED",     /**< A screen was unloaded*/
    "LV_EVENT_SIZE_CHANGED",        /**< Object coordinates/size have changed*/
    "LV_EVENT_STYLE_CHANGED",       /**< Object's style has changed*/
    "LV_EVENT_LAYOUT_CHANGED",      /**< The children position has changed due to a layout recalculation*/
    "LV_EVENT_GET_SELF_SIZE",       /**< Get the internal size of a widget*/

    "_LV_EVENT_LAST",               /** Number of default events*/

};


static void tp_io_event(lv_indev_drv_t* indev_drv , uint8_t event_code)
{
	#if defined(PID_IX850)
    //if( event_code == LV_EVENT_PRESSED ) 
	//BEEP_KEY();
	 if( event_code == LV_EVENT_PRESSED ||event_code ==LV_EVENT_SCROLL_BEGIN)
	 {
	 	MainMenu_Reset_Time();
		ScrBr_Reset_Time();
		//if(event_code == LV_EVENT_PRESSED)
			//BEEP_KEY();
	 }
	 #endif
	//printf("tp_io_event[%s] type[%d]\n",EVENT_STR[event_code], indev_drv?indev_drv->type:-1);
	//printf("tp_io_event[%s] type[%d]\n",(event_code>=0&&event_code<17)?EVENT_STR[event_code]:"unkown", event_code);
}

static void ts_io_event(lv_indev_drv_t* indev_drv , uint8_t event_code)
{
	//printf("ts_io_event[%s] type[%d]\n",EVENT_STR[event_code], indev_drv->type);
}

static int evdev_button = LV_INDEV_STATE_REL;
static int evdev_key_val = 0;
static int tp_key;
static void keypad_read(lv_indev_drv_t* indev_drv, lv_indev_data_t * data)
{	
	if(PopOutKeyArrayByType(&keydat,KEY_TYPE_TS)==0)
	{
        /* No data retrieved */
        data->key = evdev_key_val;
        data->state = evdev_button; 
		return;
	}
	switch( keydat.keyType )
	{
		case KEY_TYPE_TS:
            data->state = (keydat.keyStatus==KEY_STATUS_PRESS)?LV_INDEV_STATE_PR:LV_INDEV_STATE_REL;
			switch(keydat.keyData) 
			{
				case 1:
					data->key = LV_KEY_UP;
					break;
				case 2:
					data->key = LV_KEY_DOWN;
					break;
				case 3:
					data->key = LV_KEY_RIGHT;
					break;
				case 4:
					data->key = LV_KEY_LEFT;
					break;
				case 5:
					data->key = LV_KEY_ESC;
					break;
				case 6:
					data->key = LV_KEY_DEL;
					break;
				case 7:
					data->key = LV_KEY_BACKSPACE;
					break;
				case 8:
					data->key = LV_KEY_ENTER;
					break;
				case 9:
					data->key = LV_KEY_NEXT;
					break;
				case 10:
					data->key = LV_KEY_PREV;
					break;	
				case 11:
					data->key = LV_KEY_HOME;
					break;	
				case 12:
					data->key = LV_KEY_END;
					break;			
				default:
					data->key = 0;
					break;		
			}
			if( GetKeyArraySize() )
				data->continue_reading = true;
			else
				data->continue_reading = false;

            evdev_key_val = data->key;
            evdev_button = data->state;
			break;
	}
}

void vtk_display_initial(void)
{
	if( ak_gui_dev_initial() == -1 )
        return;

	lv_init();
	vtk_lv_bmp_init();
	//lv_port_fs_init();

#if defined( LVGL_SUPPORT_TDE )
#if PFINTF_FPS
    timeval_mark(&ak_timeval_begin_fps);
#endif  /* PFINTF_FPS */

    draw_buf_p_1 = ak_mem_dma_alloc(MODULE_ID_APP, gui_attr.max_size);
    if(draw_buf_p_1 == NULL)
    {
        ak_print_error_ex(MODULE_ID_APP, "Can't malloc DMA memory!\n");
        return;
    }

	/*Initialize `disp_buf` with the buffer(s). With only one buffer use NULL instead buf_2 */
    ak_mem_dma_vaddr2paddr(draw_buf_p_1, &draw_buf_paddr_1);

	lv_disp_draw_buf_init(&disp_buf, draw_buf_p_1, draw_buf_p_1, MY_DISP_HOR_RES*MY_DISP_VER_RES);
#else
	/*Initialize `disp_buf` with the buffer(s). With only one buffer use NULL instead buf_2 */
	lv_disp_draw_buf_init(&disp_buf, buf_1, buf_2, MY_DISP_HOR_RES*MY_DISP_VER_RES);

#endif

	lv_disp_drv_init(&disp_drv);            /*Basic initialization*/
	disp_drv.draw_buf = &disp_buf;          /*Set an initialized buffer*/
	disp_drv.flush_cb = vtk_fbdev_flush;        /*Set a flush callback to draw to the display*/
	disp_drv.hor_res = MY_DISP_HOR_RES;     /*Set the horizontal resolution in pixels*/
	disp_drv.ver_res = MY_DISP_VER_RES;     /*Set the vertical resolution in pixels*/
	//disp_drv.rotated = LV_DISP_ROT_180;     /*Set the vertical resolution in pixels*/
	//disp_drv.sw_rotate = 1;
#if defined( LVGL_SUPPORT_TDE )
    /* Fill a memory array with a color if you have GPU.
     * Note that, in lv_conf.h you can enable GPUs that has built-in support in LVGL.
     * But if you have a different GPU you can use with this callback.*/
    disp_drv.draw_ctx_init = draw_ctx_init;
    disp_drv.draw_ctx_deinit = draw_ctx_deinit;
    disp_drv.draw_ctx_size = sizeof(lv_draw_sw_ctx_t);
    /*配置直接绘制模式*/
    disp_drv.direct_mode = 1;
#endif	
	disp = lv_disp_drv_register(&disp_drv); /*Register the driver and save the created display objects*/

	evdev_init();
	lv_indev_drv_init(&indev_drv);      /*Basic initialization*/

	indev_drv.type = LV_INDEV_TYPE_POINTER;
#if defined(PID_IX850)
	indev_drv.read_cb = NULL;//evdev_read;   //lv_gesture_dir_t lv_indev_get_gesture_dir(const lv_indev_t * indev)
	indev_drv.feedback_cb	= tp_io_event;
#else
	indev_drv.read_cb 		= virt_io_read;   //lv_gesture_dir_t lv_indev_get_gesture_dir(const lv_indev_t * indev)
	indev_drv.feedback_cb	= tp_io_event;
#endif	
	lv_indev_t * my_indev = lv_indev_drv_register(&indev_drv);
#if 0
	lv_indev_drv_init(&tskey_drv);
    tskey_drv.type = LV_INDEV_TYPE_KEYPAD;
    tskey_drv.read_cb = keypad_read;
	tskey_drv.feedback_cb	= ts_io_event;
    lv_indev_t *indev_keypad = lv_indev_drv_register(&tskey_drv);
    g = lv_group_create();	//创建组
	lv_group_set_default(g);
    lv_indev_set_group(indev_keypad, g);	//将组绑定到输入设备
#endif
	api_start_lvgl_task();
	usleep(50*1000);	
	printf("start lvgl task....\n");
}

void vtk_display_deinitial()
{
	api_stop_lvgl_task();
	api_stop_akvi_task();
}
#ifdef PID_IX850
void vtk_font_reinit(void)
{
	vtk_lvgl_lock();
	bprintf("11111111111111111\n");
	//main_menu_release();
	bprintf("222222222222\n");
	font_deinit();
	bprintf("33333333333\n");
	lv_freetype_destroy();
#if LV_FREETYPE_CACHE_SIZE >= 0
	    lv_freetype_init(LV_FREETYPE_CACHE_FT_FACES, LV_FREETYPE_CACHE_FT_SIZES, LV_FREETYPE_CACHE_SIZE);
#else
	    lv_freetype_init(0, 0, 0);
#endif
	bprintf("44444444444\n");
	font_init();
	bprintf("555555555555\n");
   	 //MainMenu_Init();
   	 API_PublicInfo_Write_Bool("OnInstaller", 0);
   	 MainMenuLeave_InstallerMode();
	 bprintf("6666666666\n");
	vtk_lvgl_unlock();
}
void set_lv_read_cb(void)
{
	if(indev_drv.read_cb==NULL)
		indev_drv.read_cb = evdev_read;
}
void vtk_lvgl_reinit(void)
{
#if 0
	vtk_lvgl_lock();
	bprintf("11111111111111111\n");
	main_menu_release();
	bprintf("2222222222222222222\n");
	font_deinit();
	bprintf("333333333333333333333\n");
	lv_deinit();
	bprintf("444444444444444444444\n");
	lv_init();
	bprintf("55555555555555555555\n");
	vtk_lv_bmp_init();
	bprintf("66666666666666666\n");
	#if defined( LVGL_SUPPORT_TDE )
#if PFINTF_FPS
    timeval_mark(&ak_timeval_begin_fps);
#endif  /* PFINTF_FPS */
#if 0
    draw_buf_p_1 = ak_mem_dma_alloc(MODULE_ID_APP, gui_attr.max_size);
    if(draw_buf_p_1 == NULL)
    {
        ak_print_error_ex(MODULE_ID_APP, "Can't malloc DMA memory!\n");
        return;
    }

	/*Initialize `disp_buf` with the buffer(s). With only one buffer use NULL instead buf_2 */
    ak_mem_dma_vaddr2paddr(draw_buf_p_1, &draw_buf_paddr_1);
#endif
	lv_disp_draw_buf_init(&disp_buf, draw_buf_p_1, draw_buf_p_1, MY_DISP_HOR_RES*MY_DISP_VER_RES);
#else
	/*Initialize `disp_buf` with the buffer(s). With only one buffer use NULL instead buf_2 */
	lv_disp_draw_buf_init(&disp_buf, buf_1, buf_2, MY_DISP_HOR_RES*MY_DISP_VER_RES);

#endif

	lv_disp_drv_init(&disp_drv);            /*Basic initialization*/
	disp_drv.draw_buf = &disp_buf;          /*Set an initialized buffer*/
	disp_drv.flush_cb = vtk_fbdev_flush;        /*Set a flush callback to draw to the display*/
	disp_drv.hor_res = MY_DISP_HOR_RES;     /*Set the horizontal resolution in pixels*/
	disp_drv.ver_res = MY_DISP_VER_RES;     /*Set the vertical resolution in pixels*/
	//disp_drv.rotated = LV_DISP_ROT_180;     /*Set the vertical resolution in pixels*/
	//disp_drv.sw_rotate = 1;
#if defined( LVGL_SUPPORT_TDE )
    /* Fill a memory array with a color if you have GPU.
     * Note that, in lv_conf.h you can enable GPUs that has built-in support in LVGL.
     * But if you have a different GPU you can use with this callback.*/
    disp_drv.draw_ctx_init = draw_ctx_init;
    disp_drv.draw_ctx_deinit = draw_ctx_deinit;
    disp_drv.draw_ctx_size = sizeof(lv_draw_sw_ctx_t);
    /*配置直接绘制模式*/
    disp_drv.direct_mode = 1;
#endif	
	disp = lv_disp_drv_register(&disp_drv); /*Register the driver and save the created display objects*/

	//evdev_init();
	lv_indev_drv_init(&indev_drv);      /*Basic initialization*/

	indev_drv.type = LV_INDEV_TYPE_POINTER;
#if defined(PID_IX850)
	indev_drv.read_cb = evdev_read;   //lv_gesture_dir_t lv_indev_get_gesture_dir(const lv_indev_t * indev)
	indev_drv.feedback_cb	= tp_io_event;
#else
	indev_drv.read_cb 		= virt_io_read;   //lv_gesture_dir_t lv_indev_get_gesture_dir(const lv_indev_t * indev)
	indev_drv.feedback_cb	= tp_io_event;
#endif	
	lv_indev_t * my_indev = lv_indev_drv_register(&indev_drv);
	bprintf("777777777777\n");
	global_style_init(); 
	bprintf("8888888888888888\n");
	menu_initial();
	bprintf("999999999999\n");
	vtk_lvgl_unlock();
#endif
}
#endif
lv_group_t *Get_group(void)
{
    return g;
}
