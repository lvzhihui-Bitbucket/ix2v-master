﻿#include "obj_TableSurver.h"
#include <stdlib.h>
#include <stdio.h>
#include "define_string.h"
#include "define_file.h"
#include "obj_PublicInformation.h"
#include "obj_IoInterface.h"
#include "utility.h"
#include "elog.h"
#include "task_Beeper.h"

static int MeetTheConditions(const cJSON* record, cJSON* Where);

static void TB_Tips(char* msg)
{
//#if	defined(PID_IX850)
#if 0
    void* call_tips = API_TIPS_Ext(msg);
    sleep(3);
    API_TIPS_Close_Ext(call_tips);
#endif    
}
//判断表类型
static TableType TB_Check(const cJSON* table)
{
    TableType ret = ERROR_TB;

    if(cJSON_IsArray(table))
    {
        ret = SIMPLE_TB;
    }
    else if(cJSON_IsObject(table))
    {
        if( cJSON_GetObjectItemCaseSensitive(table, TB_NAME) &&
            cJSON_GetObjectItemCaseSensitive(table, TB_DESC) &&
            cJSON_GetObjectItemCaseSensitive(table, TB_TPL) &&
            cJSON_GetObjectItemCaseSensitive(table, TB_KEY) &&
            cJSON_GetObjectItemCaseSensitive(table, TB_DAT))
        {
            ret = STANDARD_TB;
        }
    }

    return ret;
}

//判断记录值范围和字段有没有少，返回：1校验通过，0校验不通过
static int RecordRuleCheck(const cJSON* record, const cJSON* rule)
{
    int ret = 1;
    cJSON* ruleElement;
    cJSON* recordElement;

    if(cJSON_IsObject(rule))
    {
        if(cJSON_IsObject(record))
        {
            cJSON_ArrayForEach(ruleElement, rule)
            {
                recordElement = cJSON_GetObjectItemCaseSensitive(record, ruleElement->string);
                if(!recordElement)
                {
                    log_e("Record check error:%s is not exist.", ruleElement->string);
                    ret = 0;
                    break;
                }
                else
                {
                    //检测规则是正则表达式
                    if(cJSON_IsString(ruleElement))
                    {
                        if(cJSON_IsString(recordElement))
                        {
                            //正则表达式校验失败
                            if(!RegexCheck(cJSON_GetStringValue(ruleElement), cJSON_GetStringValue(recordElement)))
                            {
                                ret = 0;
                                log_e("Record check regex error:record=%s, rule=%s", cJSON_GetStringValue(recordElement), cJSON_GetStringValue(ruleElement));
                                break;
                            }
                        }
                        else
                        {
                            log_e("Record check error:record->type=%d, rule=%s", recordElement->type, cJSON_GetStringValue(ruleElement));
                            ret = 0;
                            break;
                        }
                    }
                    //检测规则是Number最大最小值或者false/true
                    else if(cJSON_IsArray(ruleElement))
                    {
                        int ruleArraySize = cJSON_GetArraySize(ruleElement);
                        if(ruleArraySize > 0)
                        {
                            //两个整数范围之内
                            if(ruleArraySize == 2 &&  cJSON_IsNumber(cJSON_GetArrayItem(ruleElement, 0)) && cJSON_IsNumber(cJSON_GetArrayItem(ruleElement, 1)))
                            {

                                if(!cJSON_IsNumber(recordElement))
                                {
                                    ret = 0;
                                    log_e("Record check type error:record->type=%d, rule=[%d, %d]", recordElement->type, cJSON_GetArrayItem(ruleElement, 0)->valueint, cJSON_GetArrayItem(ruleElement, 1)->valueint);
                                    break;
                                }
                                else
                                {
                                    if(recordElement->valueint < cJSON_GetArrayItem(ruleElement, 0)->valueint || recordElement->valueint > cJSON_GetArrayItem(ruleElement, 1)->valueint)
                                    {
                                        ret = 0;
                                        log_e("Record check value error:record=%d, rule=[%d, %d]", recordElement->valueint, cJSON_GetArrayItem(ruleElement, 0)->valueint, cJSON_GetArrayItem(ruleElement, 1)->valueint);
                                        break;
                                    }
                                }
                            }
                            //数组元素枚举
                            else
                            {
                                ret = 0;
                                cJSON* tempElement;
                                cJSON_ArrayForEach(tempElement, ruleElement)
                                {
                                    if(cJSON_Compare(tempElement, recordElement, 1))
                                    {
                                        ret = 1;
                                        break;
                                    }
                                }

                                if(ret == 0)
                                {
                                    char* ruleString = cJSON_PrintUnformatted(ruleElement);
                                    char* recordString = cJSON_PrintUnformatted(recordElement);
                                    log_e("Record check enumeration error:record=%s, rule=%s", recordString, ruleString);
                                    if(ruleString)
                                    {
                                        free(ruleString);
                                    }
                                    if(recordString)
                                    {
                                        free(recordString);
                                    }
                                    break;
                                }
                            }
                        }
                        //检测格式是Array
                        else
                        {
                            if(!cJSON_IsArray(recordElement))
                            {
                                log_e("Record check type error:record->type=%d, rule=[]", recordElement->type);
                                ret = 0;
                                break;
                            }
                        }
                    }
                    //检测格式是object
                    else if(cJSON_IsObject(ruleElement))
                    {
                        if(!cJSON_IsObject(recordElement))
                        {
                            log_e("Record check type error:record->type=%d, rule={}", recordElement->type);
                            ret = 0;
                            break;
                        }
                    }
                }
            }
        }
        else
        {
            log_e("Record check error:record is not object.");
            ret = 0;
        }
    }
    return ret;
}

/*************************************判断Key字段是否可用******************************
table--表，
keyValue--KEY值；
返回：NULL--校验通过；非空--Key相同那条记录指针校验不通过
**************************************************************************************/
static const cJSON* CheckKeyValue(const cJSON* table, const cJSON* keyValue)
{
    cJSON* ret = NULL;
    cJSON* tableData;
    char* keyString;
    cJSON* record;

    if(keyValue)
    {
        switch (TB_Check(table))
        {
            case ERROR_TB:
            case SIMPLE_TB:
                return ret;

            case STANDARD_TB:
                tableData = cJSON_GetObjectItemCaseSensitive(table, TB_DAT);
                keyString = cJSON_GetStringValue(cJSON_GetObjectItemCaseSensitive(table, TB_KEY));
                if(keyString && keyString[0])
                {
                    cJSON_ArrayForEach(record, tableData)
                    {
                        //KEY值相等，校验不通过
                        if(cJSON_Compare(keyValue, cJSON_GetObjectItemCaseSensitive(record, keyString), 1))
                        {
                            ret = record;
                            break;
                        }
                    }
                }
                break;
        }
    }

    return ret;
}

int CheckTableKeyValue(const cJSON* table)
{
    int ret = 0;
    char* keyString;

    switch (TB_Check(table))
    {
        case ERROR_TB:
        case SIMPLE_TB:
            return ret;

        case STANDARD_TB:
            keyString = cJSON_GetStringValue(cJSON_GetObjectItemCaseSensitive(table, TB_KEY));
            if(keyString && keyString[0])
            {
                cJSON* key1;
                cJSON* key2;
                int i, k;
                cJSON* tableData = cJSON_GetObjectItemCaseSensitive(table, TB_DAT);
                int recordNum = cJSON_GetArraySize(tableData);

                for(i = 0; i < recordNum - 1; i++)
                {
                    key1 = cJSON_GetObjectItemCaseSensitive(cJSON_GetArrayItem(tableData, i), keyString);
                    for(k = i+1; k < recordNum; k++)
                    {
                        key2 = cJSON_GetObjectItemCaseSensitive(cJSON_GetArrayItem(tableData, k), keyString);
                        if(cJSON_Compare(key1, key2, 1))
                        {
                            ret = 1;
                            return ret;
                        }
                    }
                }
            }
            break;
    }

    return ret;
}


int API_TB_IsTableHasTemplate(const cJSON* table)
{
    return 0;
}

/**
 * @brief 通过where条件查找符合的数量
 * @param json_tb 表对象
 * @param filter  where条件，当为NULL时返回全部
 * 
 * @return        减掉模板以后的数量
*/
int API_TB_Count(const cJSON* table, cJSON* Where)
{
    int recordNum = 0;
    cJSON* tableData;
    cJSON* element;

    switch (TB_Check(table))
    {
        case ERROR_TB:
            tableData = NULL;
            break;

        case SIMPLE_TB:
            tableData = table;
            break;

        case STANDARD_TB:
            tableData = cJSON_GetObjectItemCaseSensitive(table, TB_DAT);
            break;
    }

    cJSON_ArrayForEach(element, tableData)
    {
        if(MeetTheConditions(element, Where))
        {
            recordNum++;
        }
    }
    
    return recordNum;
}

/**
 * @brief 给表添加一个字段
 * @param table 表对象
 * @param data       完整的字段
 * @return         返回值int=0：添加失败
                   返回值int=N：添加成功，返回该表的记录数
*/
int API_TB_Add(const cJSON* table, cJSON* data)
{
    cJSON* tableData;
    cJSON* Template;
    cJSON* dataElement;
    cJSON* rule;
    int recordNum = 0;
    char* keyString;

    if (cJSON_IsObject(data) && cJSON_GetArraySize(data))
    {
        switch(TB_Check(table))
        {
            case ERROR_TB:
                recordNum = 0;
                break;

            case SIMPLE_TB:
                tableData = table;
                cJSON_AddItemToArray(tableData, cJSON_Duplicate(data, 1));
                recordNum = cJSON_GetArraySize(tableData);
                break;

            case STANDARD_TB:
                tableData = cJSON_GetObjectItemCaseSensitive(table, TB_DAT);
                Template = cJSON_Duplicate(cJSON_GetObjectItemCaseSensitive(table, TB_TPL), 1);
                rule = cJSON_GetObjectItemCaseSensitive(table, TB_RULE);
                keyString = cJSON_GetStringValue(cJSON_GetObjectItemCaseSensitive(table, TB_KEY));
                cJSON_ArrayForEach(dataElement, data)
                {
                    if(cJSON_GetObjectItemCaseSensitive(Template, dataElement->string))
                    {
                        cJSON* recordItem = cJSON_Duplicate(dataElement, 1);
                        if(recordItem)
                        {
                            cJSON_ReplaceItemInObjectCaseSensitive(Template, dataElement->string, recordItem);
                        }
                        else
                        {
                            dprintf("TB cJSON_Duplicate: error.");
                        }
                    }
                }
                
                //检查记录成功
                if(RecordRuleCheck(Template, rule))
                {
                    //检查KEY值重复
                    if(!CheckKeyValue(table, cJSON_GetObjectItemCaseSensitive(Template, keyString)))
                    {
                        cJSON_AddItemToArray(tableData, Template);
                        recordNum = cJSON_GetArraySize(tableData);
                        break;
                    }
                    else
                    {
                        //log_e("API_TB_Add CheckKeyValue Duplicate key values.\n");
                       // TB_Tips("TB add: Duplicate key values");
                    }
                }
                else
                {
                    dprintf("TB add: rule check error.");
                    TB_Tips("TB add: rule check error.");
                }
                cJSON_Delete(Template);
                break;
        }
    }

    return recordNum;
}

static int MeetTheConditions(const cJSON* record, cJSON* Where)
{
    cJSON* value;
    cJSON* elementWhere;

    cJSON_ArrayForEach(elementWhere, Where)
    {
        value = cJSON_GetObjectItemCaseSensitive(record, elementWhere->string);
        if (cJSON_IsString(value) && cJSON_IsString(elementWhere))
        {
            if (!strstr(value->valuestring, elementWhere->valuestring))
            {
                return 0;
            }
        }
        else if(!cJSON_Compare(value, elementWhere, 1))
        {
            return 0;
        }
    }
    
    return 1;
}


/**
 * @brief 通过where条件删除表字段
 * @param json_tb 表对象
 * @param filter  where条件，当为NULL时删掉模板以外全部数据
 * @return         返回值int=0：删除失败，无符合记录或者表为空
                   返回值int=N：删除记录的记录数量
*/
int API_TB_Delete(const cJSON* table, cJSON* Where)
{
    cJSON* record;
    cJSON* tableData;
    int deleteRecordNum = 0;
    int size;

    switch(TB_Check(table))
    {
        case ERROR_TB:
            return 0;

        case SIMPLE_TB:
            tableData = table;
            break;

        case STANDARD_TB:
            tableData = cJSON_GetObjectItemCaseSensitive(table, TB_DAT);
            break;
    }


    for(size = cJSON_GetArraySize(tableData); size > 0; size--)
    {
        record = cJSON_GetArrayItem(tableData, size - 1);
        if(MeetTheConditions(record, Where))
        {
            cJSON_DeleteItemFromArray(tableData, size - 1);
            deleteRecordNum++;
        }
    }
    
    return deleteRecordNum;
}

/**
 * @brief 通过index删除表记录
 * @param json_tb 表对象
 * @return         返回值int=0：删除失败，无符合记录或者表为空
                   返回值int=N：删除记录的记录数量
*/
int API_TB_DeleteByIndex(const cJSON* table, int index)
{
    cJSON* record;
    cJSON* tableData;
    int deleteRecordNum = 0;
    int size;

    switch(TB_Check(table))
    {
        case ERROR_TB:
            return 0;

        case SIMPLE_TB:
            tableData = table;
            break;

        case STANDARD_TB:
            tableData = cJSON_GetObjectItemCaseSensitive(table, TB_DAT);
            break;
    }


    if(cJSON_GetArraySize(tableData) > index)
    {
        cJSON_DeleteItemFromArray(tableData, index);
        deleteRecordNum++;
    }
    
    return deleteRecordNum;
}

/**
 * @brief 删除index前面的表记录
 * @param json_tb 表对象
 * @return         返回值int=0：删除失败，无符合记录或者表为空
                   返回值int=N：删除记录的记录数量
*/
int API_TB_DeleteBeforeIndex(const cJSON* table, int index)
{
    cJSON* record;
    cJSON* tableData;
    int deleteRecordNum = 0;
    int size;

    switch(TB_Check(table))
    {
        case ERROR_TB:
            return 0;

        case SIMPLE_TB:
            tableData = table;
            break;

        case STANDARD_TB:
            tableData = cJSON_GetObjectItemCaseSensitive(table, TB_DAT);
            break;
    }

    size = cJSON_GetArraySize(tableData);

    while((deleteRecordNum < index) && (deleteRecordNum < size))
    {
        cJSON_DeleteItemFromArray(tableData, 0);
        deleteRecordNum++;
    }
    
    return deleteRecordNum;
}

/**
 * @brief 截取index前面的部分表
 * @param table 表对象
 * @return          返回值null：无
                    返回值非null：新的JSON表
*/
cJSON* API_TB_CopyBeforeIndex(const cJSON* table, int index)
{
    cJSON* retTable = NULL;
    cJSON* tableData;
    cJSON* retTableData = NULL;
    cJSON* record;
    int recordNum = 0;

    switch(TB_Check(table))
    {
        case ERROR_TB:
            return retTable;

        case SIMPLE_TB:
            tableData = table;
            retTableData = cJSON_CreateArray();
            retTable = retTableData;
            break;

        case STANDARD_TB:
            tableData = cJSON_GetObjectItemCaseSensitive(table, TB_DAT);
            if(tableData)
            {
                retTable = cJSON_Duplicate(table, 1);
                retTableData = cJSON_CreateArray();
                cJSON_ReplaceItemInObjectCaseSensitive(retTable, TB_DAT, retTableData);
            }
            break;

        default:
            return retTable;
    }

    cJSON_ArrayForEach(record, tableData)
    {
        if(recordNum++ < index)
        {
            cJSON_AddItemToArray(retTableData, cJSON_Duplicate(record, 1));
        }
        else
        {
            break;
        }
    }

    return retTable;
}

/**
 * @brief 通过where条件更新表字段
 * @param json_tb 表对象
 * @param filter       where条件
 * @param newitem       新的字段
 * @return         返回值int=0：更新失败，无符合记录或者表为空
                   返回值int=N：更新记录的记录数量
*/
int API_TB_Update(const cJSON* table, cJSON* Where, cJSON* newitem)
{
    cJSON* element;
    cJSON* elementNewitem;
    cJSON* tableData;
    cJSON* record;
    int updateRecordNum = 0;

    switch(TB_Check(table))
    {
        case ERROR_TB:
            return 0;

        case SIMPLE_TB:
            tableData = table;
            break;

        case STANDARD_TB:
            tableData = cJSON_GetObjectItemCaseSensitive(table, TB_DAT);
            break;
    }

    cJSON_ArrayForEach(element, tableData)
    {
        if(MeetTheConditions(element, Where))
        {
            cJSON* keyValue = NULL;
            cJSON* Template = cJSON_Duplicate(element, 1);
            cJSON* rule = cJSON_GetObjectItemCaseSensitive(table, TB_RULE);
            char* keyString = cJSON_GetStringValue(cJSON_GetObjectItemCaseSensitive(table, TB_KEY));
            cJSON_ArrayForEach(elementNewitem, newitem)
            {
                if(cJSON_GetObjectItemCaseSensitive(Template, elementNewitem->string))
                {
                    cJSON_ReplaceItemInObjectCaseSensitive(Template, elementNewitem->string, cJSON_Duplicate(elementNewitem, 1));
                }
            }

            //检查记录成功
            if(RecordRuleCheck(Template, rule))
            {
                //检查Key值
                record = CheckKeyValue(table, cJSON_GetObjectItemCaseSensitive(Template, keyString));
                if(record && record != element)
                {
                    log_e("API_TB_Update CheckKeyValue Duplicate key values.\n");
                    //TB_Tips("TB update: Duplicate key values");
                }
                else
                {
                    cJSON_ArrayForEach(elementNewitem, newitem)
                    {
                        cJSON_ReplaceItemInObject(element, elementNewitem->string, cJSON_Duplicate(elementNewitem, 1));
                    }
                    updateRecordNum++;
                }
            }
            else
            {
                TB_Tips("TB update: rule check error.");
            }
            cJSON_Delete(Template);
        }
    }
    
    return updateRecordNum;
}

int SortTableView(cJSON* View, char* keyString, int Sort)
{
    int i, k;
    int recordNum = cJSON_GetArraySize(View);
    int exchangeFlag;
    cJSON* key1;
    cJSON* key2;
	char str1[100];
	char str2[100];
	int len1;
	int len2;
    for(i = 0; i < recordNum - 1; i++)
    {
        for(k = 0; k < recordNum - i - 1; k++)
        {
            exchangeFlag = 0;
            key1 = cJSON_GetObjectItemCaseSensitive(cJSON_GetArrayItem(View, k), keyString);
            key2 = cJSON_GetObjectItemCaseSensitive(cJSON_GetArrayItem(View, k+1), keyString);
		if(key1==NULL||key2==NULL)
			continue;

		 if(cJSON_IsString(key1))	
		 {
		 	int j;
			len1=strlen(key1->valuestring);
			for(j = 0;j < len1&&j<99;j++)
			{
				str1[j] = toupper(key1->valuestring[j]);
			}
			str1[j]=0;
			len2=strlen(key2->valuestring);
			for(j = 0;j < len2&&j<99;j++)
			{
				str2[j] = toupper(key2->valuestring[j]);
			}
			str2[j]=0;
		 }
            if(Sort)
            {
                if(cJSON_IsNumber(key1))
                {
                    if(key1->valueint < key2->valueint)
                    {
                        exchangeFlag = 1;
                    }
                }
                else if(cJSON_IsString(key1))
                {
                    if(strcmp(str1, str2) < 0)
                    {
                        exchangeFlag = 1;
                    }
                }
            }
            else
            {
                if(cJSON_IsNumber(key1))
                {
                    if(key1->valueint > key2->valueint)
                    {
                        exchangeFlag = 1;
                    }
                }
                else if(cJSON_IsString(key1))
                {
                    if(strcmp(str1, str2) > 0)
                    {
                        exchangeFlag = 1;
                    }
                }
            }


            if(exchangeFlag)
            {
                cJSON_InsertItemInArray(View, k+1, cJSON_DetachItemFromArray(View, k));
            }
        }
    }
 
    return 1;
}

/**
 * @brief 通过where条件查找表并按条件显示
 * @param json_tb 表对象
 * @param filter       where条件, 格式{"abc":"", "123":[]}
 *                     View格式[{"abc":"", "123":564}]
 * @param sort      排序的顺序
 * @return          返回值int=0：失败，无符合记录或者表为空
                    返回值int=N：符合条件的记录数量
*/
int API_TB_SelectBySort(const cJSON* table, cJSON* View, cJSON* Where, int Sort)
{
    if(!cJSON_IsArray(View))
    {
        return 0;
    }

    cJSON* tableData;
    cJSON* element;
    cJSON* viewRecord;
    cJSON* viewRecordField;
    char* keyString = NULL;
    int viewRecordNum = 0;
    int viewIndex = 0;

    switch(TB_Check(table))
    {
        case ERROR_TB:
            return 0;

        case SIMPLE_TB:
            tableData = table;
            break;

        case STANDARD_TB:
            tableData = cJSON_GetObjectItemCaseSensitive(table, TB_DAT);
            keyString = cJSON_GetStringValue(cJSON_GetObjectItemCaseSensitive(table, TB_KEY));
            break;
    }

    cJSON* viewtemplate = cJSON_DetachItemFromArray(View, 0);

    cJSON_ArrayForEach(element, tableData)
    {
        if(!MeetTheConditions(element, Where))
        {
            continue;
        }
       

        if(viewtemplate != NULL && cJSON_IsObject(viewtemplate))
        {
            viewRecord = cJSON_CreateObject();
            cJSON_ArrayForEach(viewRecordField, viewtemplate)
            {
                cJSON_AddItemToObject(viewRecord, viewRecordField->string, cJSON_Duplicate(cJSON_GetObjectItemCaseSensitive(element, viewRecordField->string), 1));
            }
        }
        else
        {
            viewRecord = cJSON_Duplicate(element, 1);
        }

        viewRecordNum++;


        cJSON_AddItemToArray(View, viewRecord);
    }

    if(Sort)
    {
        SortTableView(View, keyString, 0);
    }

    if(viewtemplate)
    {
        cJSON_InsertItemInArray(View, 0, viewtemplate);
    }

    return viewRecordNum;
}

/**
 * @brief 获取表的KEY字段
 * @param table 表对象
 * @return          返回值null：无
                    返回值非null：KEY字段
*/
const cJSON* API_TB_GetKey(const cJSON* table)
{
    cJSON* key;

    switch(TB_Check(table))
    {
        case ERROR_TB:
        case SIMPLE_TB:
            key =  NULL;
            break;

        case STANDARD_TB:
            key = cJSON_GetObjectItemCaseSensitive(table, TB_KEY);
            break;
    }

    return key;
}

/**
 * @brief 获取表的一条新记录
 * @param table 表对象
 * @return          返回值null：无
                    返回值非null：一个带Key值的记录
*/
cJSON* API_TB_CreakNewRecord(const cJSON* table)
{
    cJSON* record = NULL;
    cJSON* recordKey;
    cJSON* recordTemp;
    cJSON* recordTempKey;
    cJSON* tbData;
    char* keyString;
    int recordNum, keyValue, i;

    switch(TB_Check(table))
    {
        case ERROR_TB:
            record = NULL;
            break;

        case SIMPLE_TB:
            record = cJSON_Duplicate(cJSON_GetArrayItem(table, 0), 1);
            break;

        case STANDARD_TB:
            tbData = cJSON_GetObjectItemCaseSensitive(table, TB_DAT);
            recordNum = cJSON_GetArraySize(tbData);
            keyString = cJSON_GetStringValue(cJSON_GetObjectItemCaseSensitive(table, TB_KEY));
            if(keyString)
            {
                if(recordNum)
                {
                    record = cJSON_Duplicate(cJSON_GetArrayItem(tbData, 0), 1);
                }
                else
                {
                    record = cJSON_Duplicate(cJSON_GetObjectItemCaseSensitive(table, TB_TPL), 1);
                }
                //目前只处理KEY值类型为整数的
                recordKey = cJSON_GetObjectItemCaseSensitive(record, keyString);
                if(cJSON_IsNumber(recordKey))
                {
                    for(keyValue = 0; 1; keyValue++)
                    {
                        for (i = 0; i < recordNum; i++)
                        {
                            recordTemp = cJSON_GetArrayItem(tbData, i);
                            recordTempKey = cJSON_GetObjectItemCaseSensitive(recordTemp, keyString);
                            if(recordTempKey && (recordTempKey->valueint == keyValue))
                            {
                                break;
                            }
                        }

                        //搜索完毕没有KEY值相同的记录存在
                        if(i == recordNum)
                        {
                            break;
                        }
                    }
                    cJSON_ReplaceItemInObjectCaseSensitive(record, keyString, cJSON_CreateNumber(keyValue));
                }
            }
            break;
    }
    return record;
}


// 创建表，从内存表对象复制, 判断来源表是否有模板行：如无，自动拷贝记录1，增加到模板行
cJSON* API_TB_Create(const cJSON* table, const cJSON* tempData)
{
    cJSON* ret = NULL;
    cJSON* record = NULL;
    cJSON* template = NULL;

    if(tempData)
    {
        if(!cJSON_IsObject(tempData))
        {
            return ret;
        }
        template = cJSON_Duplicate(tempData, 1);
    }
    else
    {
        if(cJSON_GetArraySize(table) == 0)
        {
            return ret;
        }
        else
        {
            template = cJSON_Duplicate(cJSON_GetArrayItem(table, 0), 1);
        }
    }

    if(!cJSON_GetObjectItemCaseSensitive(template, IX2V_TABLE))
    {
        cJSON_AddItemToObject(template, IX2V_TABLE, cJSON_CreateTrue());
    }

    if(table == NULL)
    {
        ret = cJSON_CreateArray();
        cJSON_AddItemToArray(ret, template);
    }
    else
    {
        ret = cJSON_Duplicate(table, 1);
        //有模版，则修改模版
        if(API_TB_IsTableHasTemplate(table))
        {
            cJSON_DeleteItemFromArray(ret, 0);
            cJSON_InsertItemInArray(ret, 0, template);
        }
        //没有模版，则添加模版
        else
        {
            cJSON_InsertItemInArray(ret, 0, template);
        }
    }

    return ret;
}

// 创建表，从内存表对象复制, 判断来源表是否有模板行：如无，自动拷贝记录1，增加到模板行
cJSON* API_TB_CreateNew(const cJSON* table, const char* name, const char* desc, const cJSON* tpl, const char* key, const cJSON* rule)
{
    cJSON* newTable = NULL;
    cJSON* newTableData;
    cJSON* newRecord;
    cJSON* newRecordElement;
    cJSON* record = NULL;
    cJSON* tplElement = NULL;
    cJSON* tableData;
    int recordNum;
    cJSON* jsonDesc = NULL;

    switch(TB_Check(table))
    {
        case ERROR_TB:
            return newTable;

        case SIMPLE_TB:
            tableData = table;
            newTable = cJSON_CreateObject();
            cJSON_AddStringToObject(newTable, TB_NAME, (name ? name : ""));
            jsonDesc = cJSON_Parse(desc);
            if(!jsonDesc)
            {
                jsonDesc = cJSON_CreateString(desc);
            }
            cJSON_AddItemToObject(newTable, TB_DESC, (jsonDesc ? jsonDesc : cJSON_CreateObject()));
            cJSON_AddItemToObject(newTable, TB_TPL, cJSON_Duplicate(tpl ? tpl : cJSON_GetArrayItem(tableData, 0), 1));
            cJSON_AddItemToObject(newTable, TB_RULE, rule ? cJSON_Duplicate(rule, 1) : cJSON_CreateObject());
            cJSON_AddStringToObject(newTable, TB_KEY, (key ? key : ""));
            cJSON_AddItemToObject(newTable, TB_DAT, newTableData = cJSON_CreateArray());

            tpl = cJSON_GetObjectItemCaseSensitive(newTable, TB_TPL);

            cJSON_ArrayForEach(record, tableData)
            {
                newRecord = cJSON_CreateObject();
                cJSON_ArrayForEach(tplElement, tpl)
                {
                    newRecordElement = cJSON_GetObjectItemCaseSensitive(record, tplElement->string);
                    if(newRecordElement == NULL)
                    {
                        newRecordElement = tplElement;
                    }
                    cJSON_AddItemToObject(newRecord, tplElement->string, cJSON_Duplicate(newRecordElement, 1));
                }
                cJSON_AddItemToArray(newTableData, newRecord);
            }
            break;

        case STANDARD_TB:
            tableData = cJSON_GetObjectItemCaseSensitive(table, TB_DAT);
            newTable = cJSON_CreateObject();
            cJSON_AddItemToObject(newTable, TB_NAME, (name ? cJSON_CreateString(name) : cJSON_Duplicate(cJSON_GetObjectItemCaseSensitive(table, TB_NAME), 1)));
            if(desc)
            {
                jsonDesc = cJSON_Parse(desc);
                if(!jsonDesc)
                {
                    jsonDesc = cJSON_CreateString(desc);
                }
            }
            else
            {
                jsonDesc = cJSON_Duplicate(cJSON_GetObjectItemCaseSensitive(table, TB_DESC), 1);
            }

            cJSON_AddItemToObject(newTable, TB_DESC, (jsonDesc ? jsonDesc : cJSON_CreateObject()));
            cJSON_AddItemToObject(newTable, TB_TPL, cJSON_Duplicate(tpl ? tpl : cJSON_GetObjectItemCaseSensitive(table, TB_TPL), 1));
            cJSON_AddItemToObject(newTable, TB_RULE, cJSON_Duplicate(rule ? rule : cJSON_GetObjectItemCaseSensitive(table, TB_RULE), 1));
            cJSON_AddItemToObject(newTable, TB_KEY, (key ? cJSON_CreateString(key) : cJSON_Duplicate(cJSON_GetObjectItemCaseSensitive(table, TB_KEY), 1)));
            cJSON_AddItemToObject(newTable, TB_DAT, newTableData = cJSON_CreateArray());

            rule = cJSON_GetObjectItemCaseSensitive(newTable, TB_RULE); 
            tpl = cJSON_GetObjectItemCaseSensitive(newTable, TB_TPL);

            cJSON_ArrayForEach(record, tableData)
            {
                newRecord = cJSON_CreateObject();
                cJSON_ArrayForEach(tplElement, tpl)
                {
                    newRecordElement = cJSON_GetObjectItemCaseSensitive(record, tplElement->string);
                    if(newRecordElement == NULL)
                    {
                        newRecordElement = tplElement;
                    }
                    cJSON_AddItemToObject(newRecord, tplElement->string, cJSON_Duplicate(newRecordElement, 1));
                }

                if(RecordRuleCheck(newRecord, rule))
                {
                    cJSON* keyValue = cJSON_GetObjectItemCaseSensitive(newRecord, cJSON_GetStringValue(cJSON_GetObjectItemCaseSensitive(newTable, TB_KEY)));
                    //检查KEY值重复
                    if(!CheckKeyValue(newTable, keyValue))
                    {
                        cJSON_AddItemToArray(newTableData, newRecord);
                    }
                    else
                    {
                        cJSON_Delete(newRecord);
                        MyPrintJson("API_TB_CreateNew Duplicate key values", keyValue);
                        TB_Tips("TB create: Duplicate key values");
                    }
                }
                else
                {
                    cJSON_Delete(newRecord);
                    TB_Tips("TB create: rule check error.");
                }
            }
            break;
    }

    return newTable;
}

const cJSON* API_TB_ReplaceRule(const cJSON* table, const cJSON* rule)
{
    cJSON* ret = NULL;

    switch(TB_Check(table))
    {
        case ERROR_TB:
        case SIMPLE_TB:
            break;

        case STANDARD_TB:
            if(cJSON_GetObjectItemCaseSensitive(table, TB_RULE))
            {
                cJSON_ReplaceItemInObjectCaseSensitive(table, TB_RULE, cJSON_Duplicate(rule, 1));
            }
            else
            {
                cJSON_AddItemToObject(table, TB_RULE, cJSON_Duplicate(rule, 1));
            }
            ret = cJSON_GetObjectItemCaseSensitive(table, TB_RULE);
            break;
    }

    return ret;
}

const cJSON* API_TB_GetRule(const cJSON* table)
{
    cJSON* ret = NULL;

    switch(TB_Check(table))
    {
        case ERROR_TB:
        case SIMPLE_TB:
            break;

        case STANDARD_TB:
            ret = cJSON_GetObjectItemCaseSensitive(table, TB_RULE);
            //MyPrintJson("API_TB_GetRule", ret);
            break;
    }

    return ret;
}

const cJSON* API_TB_ReplaceDesc(const cJSON* table, const cJSON* desc)
{
    cJSON* ret = NULL;

    switch(TB_Check(table))
    {
        case ERROR_TB:
        case SIMPLE_TB:
            break;

        case STANDARD_TB:
            if(cJSON_GetObjectItemCaseSensitive(table, TB_DESC))
            {
                cJSON_ReplaceItemInObjectCaseSensitive(table, TB_DESC, cJSON_Duplicate(desc, 1));
            }
            else
            {
                cJSON_AddItemToObject(table, TB_DESC, cJSON_Duplicate(desc, 1));
            }
            ret = cJSON_GetObjectItemCaseSensitive(table, TB_DESC);
            break;
    }

    return ret;
}

const cJSON* API_TB_GetDesc(const cJSON* table)
{
    cJSON* ret = NULL;

    switch(TB_Check(table))
    {
        case ERROR_TB:
        case SIMPLE_TB:
            break;

        case STANDARD_TB:
            ret = cJSON_GetObjectItemCaseSensitive(table, TB_DESC);
            break;
    }

    return ret;
}

// 创建表，从表文件或者字符串加载
cJSON* API_TB_LoadFromFile(const char *filename, const char* name, const char* desc, const cJSON* tpl, const char* key, const cJSON* rule)
{
    cJSON* table;
    cJSON* ret;

    table = GetJsonFromFile(filename);
    if(table == NULL)
    {
        table = cJSON_Parse(filename);
    }
    ret = API_TB_CreateNew(table, name, desc, tpl, key, rule);
    cJSON_Delete(table);

    return ret;
}

// 删除表模板
int API_TB_DropTemp(cJSON* table)
{
    cJSON* tableData;
    cJSON* element;

    switch(TB_Check(table))
    {
        case ERROR_TB:
        case SIMPLE_TB:
            break;

        case STANDARD_TB:
            cJSON_DeleteItemFromObjectCaseSensitive(table, TB_NAME);
            cJSON_DeleteItemFromObjectCaseSensitive(table, TB_DESC);
            cJSON_DeleteItemFromObjectCaseSensitive(table, TB_TPL);
            cJSON_DeleteItemFromObjectCaseSensitive(table, TB_RULE);
            cJSON_DeleteItemFromObjectCaseSensitive(table, TB_KEY);
            cJSON_DeleteItemFromObjectCaseSensitive(table, TB_MAX_CNT);
            tableData = cJSON_DetachItemFromObjectCaseSensitive(table, TB_DAT);
            table->type = cJSON_Array;
            cJSON_ArrayForEach(element, tableData)
            {
                cJSON_AddItemToArray(table, cJSON_Duplicate(element, 1));
            }
            cJSON_Delete(tableData);
            break;
    }

    return 1;
}

#define IM_DIVERT_TB_TPL        "{\"DT_ADDR\":\"\",\"PWD\":\"\",\"Phone_Acc\":\"\",\"Phone_Pwd\":\"\",\"Divert_Option\":\"-\",\"Remark\":\"\"}"
#define IMCallRecord_TPL        "{\"Time\":\"\",\"Call_ID\":\"\",\"Call_Type\":\"\",\"Call_Source\":null,\"Call_Target\":null,\"Rec_File\":\"\",\"Unread\":null,\"Miss\":null,\"Delete\":null,\"Remark\":\"\"}"
#define EventRecord_TPL         "{\"Time\":\"\",\"Type\":\"\",\"Recorder\":null,\"Detail\":null,\"Rec_File\":\"\",\"Unread\":null}"
#define PRIVATE_PWD_TPL         "{\"IX_ADDR\":\"\",\"ID\":0,\"CODE\":\"\",\"NOTE\":\"\"}"
#define DSUnlockRecord_TPL      "{\"UNLOCK_TIME\":\"\",\"LOCK_NBR\":\"\",\"SOURCE\":\"\"}"
#define LOG_TB_TPL              "{\"MY_TIME\":\"2022-08-06 05:25:22\",\"TIME\":\"2022-08-06 05:25:22\",\"MFG_SN\":\"64000001fee7\",\"IX_ADDR\":\"0099000158\",\"TYPE\":\"Unlock\",\"EventName\":\"EventLockReq\",\"EventData\":{\"EventName\":\"EventLockReq\",\"LOCK_NBR\":\"RL1\",\"LOCK_PROCESS\":true,\"TIME\":0,\"SOURCE\":\"PWD\"}}"
#define DSCallRecord_TPL        "{\"Time\":\"\",\"Call_ID\":\"\",\"Call_Type\":\"IxMainCall\",\"Call_Source\":{},\"Call_Target\":{},\"Rec_File\":\"\",\"Unread\":false,\"Miss\":false,\"Delete\":false,\"Remark\":\"\"}"
#define DivertAcc_TB_TPL        "{\"Type\":\"\",\"ADDR\":\"\",\"PWD\":\"\",\"Phone_Acc\":\"\",\"Phone_Pwd\":\"\",\"Divert_Option\":\"-\",\"Remark\":\"\"}"
#define IXG_LIST_TPL            "{\"IXG_ID\":1,\"GW_ID\":1,\"IX_ADDR\":\"\",\"MFG_SN\":\"\",\"IP_ADDR\":\"\",\"IX_TYPE\":\"\",\"Platform\":\"\",\"UpTime\":\"\",\"DT_IM\":[]}"
#define DT_IM_LIST_TPL          "{\"DT_IM\":1}"
#define R8001_TPL               "{\"IX_ADDR\":\"\",\"IXG_ID\":1,\"IXG_IM_ID\":1,\"G_NBR\":\"-\",\"L_NBR\":\"-\",\"IX_NAME\":\"IXG1-IM01\",\"MFG_SN\":\"-\",\"IP_ADDR\":\"-\",\"IP_MASK\":\"-\",\"IP_GATEWAY\":\"-\",\"IX_TYPE\":\"IM\",\"IX_Model\":\"IXG-IM\",\"DevModel\":\"IXG-IM\",\"FW_VER\":\"-\",\"HW_VER\":\"-\"}"
#define CardTable_TPL		    "{\"CARD_NUM\":\"\",\"ROOM\":\"\",\"DATE\":\"\",\"NAME\":\"\",\"PROPERTY\":\"\"}"
#define IpcTable_TPL		    "{\"IP_ADDR\":\"\",\"IPC_NAME\":\"\",\"USR_NAME\":\"\",\"USR_PWD\":\"\",\"CH_DAT\":[],\"CH_REC\":0,\"CH_FULL\":0,\"CH_QUAD\":1,\"CH_APP\":1}"
#define CERT_LOG_TPL		    "{\"DATE\":\"\",\"TYPE\":\"\",\"INFO\":\"\"}"

#define IXGDFRMKeyFunc_TPL		"{\"MENU_ID\":\"\",\"RM_KEY_UP\":\"\",\"RM_KEY_DN\":\"\",\"RM_KEY_DN\":\"\",\"RM_KEY_LOCK\":\"\",\"RM_KEY_BACK\":\"\",\"RM_KEY_DIVERT\":\"\",\"RM_KEY_MUTE\":\"\"}"
#define IXGMonRes_TPL		    "{\"TYPE\":\"\",\"ID\":\"\",\"NAME\":\"\"}"

#define NDM_Register_TPL		"{\"IP_ADDR\":\"\",\"MFG_SN\":\"\",\"IX_ADDR\":\"\",\"ENABLE\":\"\",\"NDM_State\":\"\"}"
#define NDM_Log_TPL		        "{\"IP_ADDR\":\"\",\"MFG_SN\":\"\",\"IX_ADDR\":\"\",\"DATE\":\"\",\"INFO\":\"\",\"NDM_RESULT\":{}}"
#define NDM_S_Log_TPL		    "{\"DATE\":\"\",\"IP_ADDR\":\"\",\"MFG_SN\":\"\",\"NDM_CONN\":\"\",\"NDM_ITEM\":null}"
#define PRIVATE_PWD_BY_L_NBR_TPL		    "{\"IX_ADDR\":\"\",\"L_NBR\":\"\",\"CODE\":\"\",\"NOTE\":\"\"}"
#define ELOG_TPL		        "{\"LEVEL\":\"\",\"TIME\":\"\",\"TID\":null,\"INFO\":\"\"}"
#define IXRList_TPL		    "{\"IX_ADDR\":\"\",\"IX_NAME\":\"\",\"G_NBR\":\"\",\"L_NBR\":\"\"}"


#define TB_Sub_SIP_ACC_TPL		"{\"IX_ADDR\":\"\",\"PWD\":\"\",\"Phone_Acc\":\"\",\"Phone_Pwd\":\"\"}"
#define TB_Divert_On_TPL		"{\"IX_ADDR\":\"\"}"
#define VideoSerErr_TPL		        "{\"IM_IP\":\"\",\"TIME\":\"\",\"TYPE\":0}"
#define TB_KEY_DEV_ADDR		    "IX_ADDR"

#define PhoneQR_Temp	            "<TYPE>IX</TYPE><VER>2.0</VER><USER>#PHONE_ID#</USER><PSW>#PHONE_PWD#</PSW><DOMAIN>47.91.88.33:5068</DOMAIN><HOME ID>#HOME_ID#</HOME ID><PSW2>63088</PSW2><CALL CODE>1000</CALL CODE><string-array name=\"List\"><item>1,DS1,#HOME_ID#,11,1028</item></string-array>"

#define DSCallRecord_RULE           "{\"Time\":\".*\",\"Call_ID\":\".*\",\"Call_Type\":[\"IxMainCall\"],\"Call_Source\":{},\"Call_Target\":{},\"Rec_File\":\".*\",\"Unread\":[true,false],\"Miss\":[true,false],\"Delete\":[true,false],\"Remark\":\".*\"}"
#define IXG_LIST_RULE               "{\"IXG_ID\":[1, 63]}"
#define DT_IM_LIST_RULE             "{\"DT_IM\":[1, 832]}"
#define CardTable_RULE		        "{\"CARD_NUM\":\"^[0-9]{1,10}$\",\"ROOM\":[1,9999],\"DATE\":\"^[0-9]{2,2}[0-9]{2,2}[0-9]{2,2}$\",\"NAME\":\"^.{1,20}$\"}"

#define SipCallLog_TPL			"{\"TIME\":\"\",\"IX_ADDR\":\"-\",\"TAR_ACC\":\"\",\"RESULT\":\"\"}"
#define IP_CONFIG_TPL			"{\"IX_ADDR\":\"\",\"IP_STATIC\":0,\"IP_ADDR\":\"\",\"IP_MASK\":\"\",\"IP_GATEWAY\":\"\"}"

#define IXRList_RULE		    "{\"IX_ADDR\":\"^[0-9]{10}$\",\"IX_NAME\":^.{1,20}$,\"G_NBR\":\"^[0-9]{1,10}$\",\"L_NBR\":\"^[0-9]{1,10}$\"}"
#define DEV_TB_TPL			    "{\"Platform\":\"\",\"MFG_SN\":\"\",\"IP_ADDR\":\"\",\"IX_ADDR\":\"\",\"G_NBR\":\"\",\"L_NBR\":\"\",\"IX_NAME\":\"\",\"IX_TYPE\":\"\",\"IX_Model\":\"\",\"DevModel\":\"\",\"FW_VER\":\"\",\"HW_VER\":\"\"}"

#define HYBRID_MAT_TPL			"{\"IX_ADDR\":\"\",\"GW_ADDR\":\"\",\"IX_TYPE\":\"\",\"CONNECT\":\"\",\"GW_ID\":0,\"IM_ID\":1,\"G_NBR\":\"\",\"L_NBR\":\"\",\"IX_NAME\":\"\"}"
#define NEW_DT_IM_TPL			"{\"IX_ADDR\":\"\",\"GW_ADDR\":\"\",\"IX_TYPE\":\"IM\",\"CONNECT\":\"DT\",\"GW_ID\":0,\"IM_ID\":1}"
#define DT_IM_CHECK_TPL			"{\"IX_ADDR\":\"\",\"GW_ADDR\":\"\",\"IX_TYPE\":\"\",\"CONNECT\":\"\",\"GW_ID\":0,\"IM_ID\":1,\"G_NBR\":\"\",\"L_NBR\":\"\",\"IX_NAME\":\"\",\"STATE\":\"OFFLINE\"}"
#define SAT_DH_HYBRID_TPL		"{\"IX_ADDR\":\"\",\"SYS_TYPE\":\"UNKNOWN\",\"IX_TYPE\":\"\",\"DEV_ID\":1,\"ASSO\":[],\"G_NBR\":\"\",\"L_NBR\":\"\",\"IX_NAME\":\"\"}"
#define SAT_DH_HYBRID_CHECK_TPL		"{\"IX_ADDR\":\"\",\"IX_TYPE\":\"\",\"DEV_ID\":1,\"STATE\":\"OFFLINE\",\"PARA_CMP\":\"different\",\"SYS_TYPE_CUR\":\"UNKNOWN\",\"ASSO_CUR\":[],\"G_NBR_CUR\":\"\",\"L_NBR_CUR\":\"\",\"IX_NAME_CUR\":\"\",\"SYS_TYPE\":\"UNKNOWN\",\"ASSO\":[],\"G_NBR\":\"\",\"L_NBR\":\"\",\"IX_NAME\":\"\"}"

#define KEYID_MAPPING_TPL			    "{\"DS_ID\":0,\"KEY_ID\":\"\",\"IX_ADDR\":\"\"}"

#define APT_MAT_TPL			    "{\"IM_ID\":1,\"SYS_TYPE\":\"\",\"IX_ADDR\":\"\",\"IX_TYPE\":\"\",\"CONNECT\":\"\",\"G_NBR\":\"\",\"L_NBR\":\"\",\"IX_NAME\":\"\"}"
#define APT_MAT_CHECK_TPL		"{\"IM_ID\":1,\"IX_ADDR\":\"\",\"STATE\":\"OFFLINE\",\"PARA_CMP\":\"different\",\"SYS_TYPE_CUR\":\"UNKNOWN\",\"G_NBR_CUR\":\"\",\"L_NBR_CUR\":\"\",\"IX_NAME_CUR\":\"\",\"SYS_TYPE\":\"UNKNOWN\",\"G_NBR\":\"\",\"L_NBR\":\"\",\"IX_NAME\":\"\"}"
#define VM_MAT_TPL			    "{\"VM_ID\":1,\"SYS_TYPE\":\"\",\"IX_ADDR\":\"\",\"IX_TYPE\":\"\",\"G_NBR\":\"\",\"L_NBR\":\"\",\"IX_NAME\":\"\",\"VM_ACC\":\"\",\"VM_PWD\":\"\",\"VM_UserPWD\":\"\"}"


static TB_Define_S tbDef[] = {

    {TB_NAME_DEV_TB,              UserDataFolder DeviceTableFileName, NULL, DEV_TB_TPL, IX2V_MFG_SN, NULL, 1000},
    {TB_NAME_IM_DIVERT_TB,        IMDivertAccTbPath, NULL, IM_DIVERT_TB_TPL, "DT_ADDR", NULL, 1000},
    {TB_NAME_IPC_DEV_TB,          IPC_SETUP_FILE_NAME, NULL, IpcTable_TPL, NULL, NULL, 1000},
    {TB_NAME_IMCallRecord,        UserDataFolder ImCallRecordFileName, NULL,IMCallRecord_TPL,NULL, NULL, 1000},
    {TB_NAME_PRIVATE_PWD,         SettingFolder MY_DEV_NAME PrivatePwd_FILE_NAME, NULL, PRIVATE_PWD_TPL, "ID", NULL, 1000},
    {TB_NAME_UNLOCK_RECORD,       UserDataFolder DSUnlockRecordFileName, NULL,DSUnlockRecord_TPL, NULL, NULL, 1000},
	{TB_NAME_SipCallLog,		  LOG_DIR SipCallLogName,	NULL,SipCallLog_TPL,NULL,NULL,1000},										
    {TB_NAME_IXG_LIST,            UserDataFolder IXG_LIST_FileName, NULL, IXG_LIST_TPL, "IXG_ID", IXG_LIST_RULE, 1000},

#if defined(PID_IX850)
    {TB_NAME_IXRLIST,             TB_IXRList_PATH, NULL, IXRList_TPL, TB_KEY_DEV_ADDR, IXRList_TPL, 1000},
#endif

#if defined(PID_IX622) || defined(PID_IX850) || defined(PID_IX821)
    {TB_NAME_CardTable,           TB_CardTable_PATH, NULL, CardTable_TPL, "CARD_NUM", CardTable_RULE, 1100},
#endif

	{TB_NAME_DSCallRecord,        DSCallRecordFilePath, NULL, DSCallRecord_TPL, "Call_ID", DSCallRecord_RULE, 1000},
	{TB_NAME_DivertAcc_TB,        DivertAcc_TBFilePath, NULL,DivertAcc_TB_TPL,NULL, NULL, 1000},
	{TB_NAME_TB_Sub_SIP_ACC,      TB_Sub_SIP_ACC_PATH, PhoneQR_Temp,TB_Sub_SIP_ACC_TPL,TB_KEY_DEV_ADDR, NULL, 1000},
	{TB_NAME_TB_Divert_On,        TB_Divert_On_PATH, NULL,TB_Divert_On_TPL,TB_KEY_DEV_ADDR, NULL, 1000},
    {TB_NAME_LOG_TB,              LOG_TB_PATH, NULL, LOG_TB_TPL, NULL, NULL, 1000},
    {TB_NAME_R8001,               IX2V_R8001_TABLE_NAME, NULL, R8001_TPL, TB_KEY_DEV_ADDR, NULL, 1000},
    {TB_NAME_EventRecord,         UserDataFolder EventRecordFileName, NULL,EventRecord_TPL,NULL, NULL, 1000},
    {TB_NAME_CertLog,             LOG_DIR CERT_LOG_FileName, NULL,CERT_LOG_TPL,NULL, NULL, 1000},
    {TB_NAME_NDM_Register,        UserDataFolder NDM_RegisterFileName, NULL,NDM_Register_TPL,"MFG_SN", NULL, 1000},
    {TB_NAME_NDM_Log,             LOG_DIR NDM_LogFileName, NULL,NDM_Log_TPL,NULL, NULL, 1000},
    {TB_NAME_NDM_S_Log,           LOG_DIR NDM_S_LogFileName, NULL,NDM_S_Log_TPL,NULL, NULL, 1000},
    {TB_NAME_PRIVATE_PWD_BY_L_NBR,      IX2V_PRIVATE_PWD_BY_L_NBR_TABLE_NAME, NULL, PRIVATE_PWD_BY_L_NBR_TPL, "L_NBR", NULL, 1000},
    {TB_NAME_Elog,                  LOG_DIR ElogJsonFileName, NULL, ELOG_TPL, NULL, NULL, 1000},

    {TB_NAME_VideoSerError,                  LOG_DIR VideoSerErrFileName, NULL, VideoSerErr_TPL, NULL, NULL, 1000},

	{TB_NAME_IP_CONFIG,			    UserDataFolder IP_CONFIG_FileName,	NULL, IP_CONFIG_TPL, TB_KEY_DEV_ADDR,NULL,1000},	
	{TB_NAME_HYBRID_MAT,			TB_HYBRID_MAT_FILE_NAME,	NULL, HYBRID_MAT_TPL, TB_KEY_DEV_ADDR, NULL,1000},	
	{TB_NAME_NEW_DT_IM,			    TB_NEW_DT_IM_FILE_NAME,	NULL, NEW_DT_IM_TPL, TB_KEY_DEV_ADDR, NULL,1000},	
	{TB_NAME_DT_IM_CHECK,           UserDataFolder DT_IM_CHECK_RESULT_FileName,	NULL, DT_IM_CHECK_TPL, TB_KEY_DEV_ADDR, NULL,1000},	
	{TB_NAME_HYBRID_SAT,			TB_HYBRID_SAT_FILE_NAME,	NULL, SAT_DH_HYBRID_TPL, TB_KEY_DEV_ADDR, NULL,1000},	
	{TB_NAME_HYBRID_SAT_CHECK,	    TB_HYBRID_SAT_CHECK_FILE_NAME,	NULL, SAT_DH_HYBRID_CHECK_TPL, TB_KEY_DEV_ADDR, NULL,1000},	
	{TB_NAME_APT_MAT,			    TB_APT_MAT_FILE_NAME,	NULL, APT_MAT_TPL, TB_KEY_DEV_ADDR, NULL,1000},	

	#ifdef PID_IX821
	{TB_NAME_KEYID_MAPPING,	TB_KEYID_MAPPING_FILE_NAME,	NULL, KEYID_MAPPING_TPL, NULL, NULL,1000},	
	#endif

	{TB_NAME_APT_MAT_CHECK,	        TB_APT_MAT_CHECK_FILE_NAME,	NULL, APT_MAT_CHECK_TPL, TB_KEY_DEV_ADDR, NULL,1000},	
	{TB_NAME_VM_MAT,			    TB_VM_MAT_FILE_NAME,	NULL, VM_MAT_TPL, TB_KEY_DEV_ADDR, NULL,1000},	

};

static const int TB_CNT = sizeof(tbDef)/sizeof(tbDef[0]);
static cJSON* tbRun = NULL;
static int checkRecordCntTimer;



//ifChange：表是否有变化， def：定义属性
static cJSON* LoadTableByDefine(TB_Define_S* def, int* ifChange)
{
    cJSON* newTable = NULL;

    if(!def)
    {
        return newTable;
    }
    cJSON* tpl = cJSON_Parse(def->tpl);
    cJSON* rule = cJSON_Parse(def->rule);
    cJSON* tbData = NULL;
    cJSON* defaultTable;

    defaultTable = GetJsonFromFile(def->path);
    if(!defaultTable)
    {
		defaultTable = cJSON_CreateArray();
    }

	newTable = API_TB_CreateNew(defaultTable, def->name, def->desc, tpl, def->key, rule);
    cJSON_Delete(tpl);
    cJSON_Delete(rule);

    //为了排列好看，先剥离"TB_DAT"字段
    tbData = cJSON_DetachItemFromObjectCaseSensitive(newTable, TB_DAT);

    //表文件中缺少TB_MAX_CNT字段，增加TB_MAX_CNT字段
    if(!cJSON_GetObjectItemCaseSensitive(newTable, TB_MAX_CNT))
    {
        cJSON* defaultMaxRecordCnt = cJSON_GetObjectItemCaseSensitive(defaultTable, TB_MAX_CNT);
        if(defaultMaxRecordCnt)
        {
            def->maxRecordCnt = defaultMaxRecordCnt->valueint;
        }
        cJSON_AddNumberToObject(newTable, TB_MAX_CNT, def->maxRecordCnt);
    }

    //添加TB_DAT字段
    cJSON_AddItemToObject(newTable, TB_DAT, tbData);

    //表有变化,置位标志
    if(ifChange)
    {
        *ifChange = (cJSON_Compare(defaultTable, newTable, 1) ? 0 : 1);
    }
    
    cJSON_Delete(defaultTable);

    char pbName[100];
    snprintf(pbName, 100, "%s_%s", TB_DESC, def->name);
    API_PublicInfo_Write(pbName, API_TB_GetDesc(newTable));

    //MyPrintJson(def->name, newTable);

    return newTable;
}

static const char *GetTime(void)
{
    static char cur_system_time[24] = { 0 };
    time_t cur_t;
    struct tm cur_tm;

    time(&cur_t);
    localtime_r(&cur_t, &cur_tm);

    strftime(cur_system_time, sizeof(cur_system_time), "%Y%m%d_%I%M%S", &cur_tm);

    return cur_system_time;
}

static int SaveTableToFile(int timing)
{
    TB_Property_S* tb;
    cJSON* element;
    int checkTime;

	if(timing >= 60)
	{
        //每分钟检测表是否需要保存
        cJSON_ArrayForEach(element, tbRun)
        {
            tb = element->valueint;
            if(tb)
            {
                pthread_mutex_lock(&(tb->lock));
                if(tb->saveFlag && tb->tb)
                {
		            SetJsonToFile(tb->path,tb->tb);
                    tb->saveFlag = 0;
                }
                pthread_mutex_unlock(&(tb->lock));
            }
        }
   
        //每CHECK_TB_TIME分钟检测表是否超长，超长删剩下一半
        checkTime = API_Para_Read_Int(CHECK_TB_TIME);
        checkTime = (checkTime ? checkTime : 10);
        if(checkRecordCntTimer++ >=  checkTime)
        {
            int count;
            checkRecordCntTimer = 0;
            cJSON_ArrayForEach(element, tbRun)
            {
                tb = element->valueint;
                if(tb)
                {
                    pthread_mutex_lock(&(tb->lock));
                    if(tb->maxRecordCnt > 0)
                    {
                        count = API_TB_Count(tb->tb, NULL);
                        if(count >= tb->maxRecordCnt)
                        {
                            count = (count - tb->maxRecordCnt/2);
                            char* transferDir = API_Para_Read_String2(TB_TRANSFER_DIR);
                            //路径不为空的时候才允许转存
                            if(transferDir && transferDir[0])
                            {
                                if(MakeDir(transferDir) == 0)
                                {
                                    //空间足够，则保存文件
                                    if(GetDirFreeSizeInMByte(transferDir))
                                    {
                                        char path[500];
                                        snprintf(path, 500, "%s/%s_%s.json", transferDir, tb->name, get_time_string());
                                        //从内存转存一半表到指定目录文件
                                        cJSON* saveTb = API_TB_CopyBeforeIndex(tb->tb, count);
                                        if(saveTb)
                                        {
                                            SetJsonToFile(path, saveTb);
                                            cJSON_Delete(saveTb);
                                        }
                                    }
                                }
                            }

                            //删除一半表，并保存到文件
                            if(API_TB_DeleteBeforeIndex(tb->tb, count))
                            {
                                SetJsonToFile(tb->path,tb->tb);
                            }
                        }
                    }
                    pthread_mutex_unlock(&(tb->lock));
                }
            }
        }
		return 1;
	}

	return 0;
}


void TableSurverInit(void)
{
    TB_Property_S* tb;
    cJSON* element;

    //删除所有的表
    if(tbRun)
    {
        cJSON_ArrayForEach(element, tbRun)
        {
            tb = element->valueint;
            if(tb)
            {
                pthread_mutex_lock(&(tb->lock));
                cJSON_Delete(tb->tb);
                pthread_mutex_unlock(&(tb->lock));
                free(tb);
            }
        }
        cJSON_Delete(tbRun);
    }

    tbRun = cJSON_CreateArray();

    //从文件导入表
    for(int i = 0; i < TB_CNT; i++)
    {
        tb = malloc(sizeof(TB_Property_S));
        if(tb)
        {
            tb->name = tbDef[i].name;
            tb->path = tbDef[i].path;
            pthread_mutex_init(&(tb->lock), 0);

            pthread_mutex_lock(&(tb->lock));
            tb->tb = LoadTableByDefine(&(tbDef[i]), &(tb->saveFlag));
            tb->maxRecordCnt = tbDef[i].maxRecordCnt;
            pthread_mutex_unlock(&(tb->lock));            
            cJSON_AddItemToArray(tbRun, cJSON_CreateNumber((int)tb));
        }
    }

    //定时每分钟保存表
    if(cJSON_GetArraySize(tbRun))
    {
        StartTimingSaveTable();
    }
}


void StopTimingSaveTable(void)
{
     API_Del_TimingCheck(SaveTableToFile);
}

void StartTimingSaveTable(void)
{
    checkRecordCntTimer = 0;
    SaveTableToFile(60);
    API_Add_TimingCheck(SaveTableToFile, 60);
}


static TB_Property_S* GetTbPropertyByName(const char* tbName)
{
    cJSON* element;
    TB_Property_S* tb;

    cJSON_ArrayForEach(element, tbRun)
    {
        tb = element->valueint;
        if(tb && tb->name && tbName && !strcmp(tb->name, tbName))
        {
            return tb;
        }
    }

	return NULL;
}

/*重新导入表，
    filePath文件路径为NULL时，使用默认路径，
    返回1导入成功，
    返回0，导入失败
*/
int API_TB_ReloadByName(const char* name, const char* filePath)
{
    int ret = 0;
    TB_Property_S* tb = GetTbPropertyByName(name);
    if(tb)
    {
        pthread_mutex_lock(&(tb->lock));
        API_Add_TimingCheck(SaveTableToFile, 60);
        cJSON_Delete(tb->tb);
        for(int i = 0; i < TB_CNT; i++)
        {
            if(!strcmp(name, tbDef[i].name))
            {
                if(filePath)
                {
                    tbDef[i].path = filePath;
                }
                tb->tb = LoadTableByDefine(&(tbDef[i]), &(tb->saveFlag));
                break;
            }
        }
        pthread_mutex_unlock(&(tb->lock));
        ret = 1;
    }
    return ret;
}


int API_TB_CountByName(const char* name, cJSON* Where)
{
    int ret = 0;
    TB_Property_S* tb = GetTbPropertyByName(name);
    if(tb)
    {
        pthread_mutex_lock(&(tb->lock));
        ret = API_TB_Count(tb->tb, Where);
        pthread_mutex_unlock(&(tb->lock));
    }
    return ret;
}

int API_TB_AddByName(const char* name, cJSON* record)
{
    int ret = 0;
    TB_Property_S* tb = GetTbPropertyByName(name);
    if(tb)
    {
        pthread_mutex_lock(&(tb->lock));
        ret = API_TB_Add(tb->tb, record);
        if(ret)
        {
            tb->saveFlag = 1;
        }
        pthread_mutex_unlock(&(tb->lock));
    }
    return ret;
}

int API_TB_DeleteByName(const char* name, cJSON* Where)
{
    int ret = 0;
    TB_Property_S* tb = GetTbPropertyByName(name);
    if(tb)
    {
        pthread_mutex_lock(&(tb->lock));
        ret = API_TB_Delete(tb->tb, Where);
        if(ret)
        {
            tb->saveFlag = 1;
        }
        pthread_mutex_unlock(&(tb->lock));
    }
    return ret;
}

int API_TB_DeleteByNameByIndex(const char* name, int index)
{
    int ret = 0;
    TB_Property_S* tb = GetTbPropertyByName(name);
    if(tb)
    {
        pthread_mutex_lock(&(tb->lock));
        ret = API_TB_DeleteByIndex(tb->tb, index);
        if(ret)
        {
            tb->saveFlag = 1;
        }
        pthread_mutex_unlock(&(tb->lock));
    }
    return ret;
}

int API_TB_UpdateByName(const char* name, cJSON* Where, cJSON* newitem)
{
    int ret = 0;
    TB_Property_S* tb = GetTbPropertyByName(name);
    if(tb)
    {
        pthread_mutex_lock(&(tb->lock));
        ret = API_TB_Update(tb->tb, Where, newitem);
        if(ret)
        {
            tb->saveFlag = 1;
        }
        pthread_mutex_unlock(&(tb->lock));
    }
    return ret;
}

int API_TB_SelectBySortByName(const char* name, cJSON* View, cJSON* Where, int Sort)
{
    int ret = 0;
    TB_Property_S* tb = GetTbPropertyByName(name);
    if(tb)
    {
        pthread_mutex_lock(&(tb->lock));
        ret = API_TB_SelectBySort(tb->tb, View, Where, Sort);
        pthread_mutex_unlock(&(tb->lock));
    }
    return ret;
}

const cJSON* API_TB_GetKeyByName(const char* name)
{
    cJSON* ret = NULL;
    TB_Property_S* tb = GetTbPropertyByName(name);
    if(tb)
    {
        pthread_mutex_lock(&(tb->lock));
        ret = API_TB_GetKey(tb->tb);
        pthread_mutex_unlock(&(tb->lock));
    }
    return ret;
}

cJSON* API_TB_CreakNewRecordByName(const char* name)
{
    cJSON* ret = NULL;
    TB_Property_S* tb = GetTbPropertyByName(name);
    if(tb)
    {
        pthread_mutex_lock(&(tb->lock));
        ret = API_TB_CreakNewRecord(tb->tb);
        pthread_mutex_unlock(&(tb->lock));
    }
    return ret;
}

cJSON* API_TB_CopyNewTableByName(const char* name)
{
    cJSON* ret = NULL;
    TB_Property_S* tb = GetTbPropertyByName(name);
    if(tb)
    {
        pthread_mutex_lock(&(tb->lock));
        ret = API_TB_CreateNew(tb->tb, NULL, NULL, NULL, NULL, NULL);
        pthread_mutex_unlock(&(tb->lock));
    }
    return ret;
}

const cJSON* API_TB_ReplaceRuleByName(const char* name, const cJSON* rule)
{
    cJSON* ret = NULL;
    TB_Property_S* tb = GetTbPropertyByName(name);
    if(tb)
    {
        pthread_mutex_lock(&(tb->lock));
        ret = API_TB_ReplaceRule(tb->tb, rule);
        if(ret)
        {
            tb->saveFlag = 1;
        }
        pthread_mutex_unlock(&(tb->lock));
    }
    return ret;
}

const cJSON* API_TB_GetRuleByName(const char* name)
{
    cJSON* ret = NULL;
    TB_Property_S* tb = GetTbPropertyByName(name);
    if(tb)
    {
        pthread_mutex_lock(&(tb->lock));
        ret = API_TB_GetRule(tb->tb);;
        pthread_mutex_unlock(&(tb->lock));
    }
    return ret;
}

void API_TB_SaveByName(const char* name)
{
    TB_Property_S* tb = GetTbPropertyByName(name);
    if(tb)
    {
        pthread_mutex_lock(&(tb->lock));
        if(tb->saveFlag && tb->tb)
        {
            SetJsonToFile(tb->path,tb->tb);
            tb->saveFlag = 0;
        }
        pthread_mutex_unlock(&(tb->lock));
    }
}

const cJSON* API_TB_ReplaceDescByName(const char* name, const cJSON* desc)
{
    char pbName[100];
    cJSON* ret = NULL;
    TB_Property_S* tb = GetTbPropertyByName(name);
    if(tb)
    {
        pthread_mutex_lock(&(tb->lock));
        ret = API_TB_ReplaceDesc(tb->tb, desc);
        if(ret)
        {
            tb->saveFlag = 1;
            snprintf(pbName, 100, "%s_%s", TB_DESC, name);
            API_PublicInfo_Write(pbName, API_TB_GetDesc(tb->tb));
        }
        pthread_mutex_unlock(&(tb->lock));
    }
    return ret;
}

const cJSON* API_TB_GetDescByName(const char* name)
{
    cJSON* ret = NULL;
    TB_Property_S* tb = GetTbPropertyByName(name);
    if(tb)
    {
        pthread_mutex_lock(&(tb->lock));
        ret = API_TB_GetDesc(tb->tb);
        pthread_mutex_unlock(&(tb->lock));
    }
    return ret;
}
int ResTbRestore(char *update_dir)
{
	cJSON* element;
	TB_Property_S* tb;
	char filename[200];
	char cmd_line[400];
	int succ_flag=0;
  	cJSON_ArrayForEach(element, tbRun)
    {
        tb = element->valueint;
        if(tb && tb->name )
        {
           	snprintf(filename,200,"%s/%s",update_dir,tb->name);
		if(access(filename, F_OK) != 0)
		{
			snprintf(filename,200,"%s/%s.json",update_dir,tb->name);
			if(access(filename, F_OK) != 0)
			{
				snprintf(filename,200,"%s/%s.txt",update_dir,tb->name);
				if(access(filename, F_OK) != 0)
				{
					continue;
				}
			}
		}
		pthread_mutex_lock(&(tb->lock));
		snprintf(cmd_line,400,"cp %s %s",filename,tb->path);
		system(cmd_line);
		pthread_mutex_unlock(&(tb->lock));
		sync();
		API_TB_ReloadByName(tb->name,NULL);
		succ_flag++;
        }
    }
	return succ_flag;
}

int ResTbBackup(cJSON *backup_list,char *backup_file_path)
{
	cJSON* element;
	TB_Property_S* tb;
	char cmd[400];
	if( access(backup_file_path, F_OK ) == 0 )
	{
		snprintf(cmd, 200, "rm -r %s", backup_file_path);
		system(cmd);
	}

	//���������ļ���
	if( mkdir( backup_file_path, 777 ) < 0 )
	{
		return -1;
	}
	cJSON_ArrayForEach(element, backup_list)
	{
		if(cJSON_IsString(element))
		{
			tb=GetTbPropertyByName(element->valuestring);
			if(tb!=NULL)
			{
				if(tb->saveFlag)
					API_TB_SaveByName(element->valuestring);
				if(access(tb->path, F_OK) == 0)
				{
					pthread_mutex_lock(&(tb->lock));
					snprintf(cmd,400,"cp %s %s/%s.json",tb->path,backup_file_path,element->valuestring);
					system(cmd);
					pthread_mutex_unlock(&(tb->lock));
				}	
			}
		}

	}
	sync();
	return 0;
	//snprintf(cmd, 200, "tar -zcvf %s/BAK.%s.tar.gz -C %s %s/", backup_file_path, "821", backupRestoreDir, BACKUP_RESTORE_DIR_NAME);
}

int UpdateResFromSD_Process(cJSON *cmd)
{
	cJSON* element;
    TB_Property_S* tb;
	char filename[200];
	char cmd_line[400];
	int succ_flag=0;
	DIR *dir = NULL;
	if(ifExitKeyLongPress())
	{
		XDRemoteDisconnect();
    		API_Event_By_Name("EventFactoryDefault");
		return;
	}
	
	sprintf(filename,"%s/DH821app",SdUpgradePath);
	if(access(filename, F_OK) == 0)
	{
		API_Event_UpdateStart("SD_Card","DH821app",1,"DIP");
		return 0;
	}
	cJSON* fileList = cJSON_CreateArray();
	cJSON *fileRecord=NULL;
	if(GetFileAndDirList(SDCARD_Backup_Path, fileList, 0)==0&&cJSON_GetArraySize(fileList))
	{
		
	    cJSON_ArrayForEach(fileRecord, fileList)
	    {
	        cJSON* fileName = cJSON_GetArrayItem(fileRecord, 3);
	        if(cJSON_IsString(fileName) && strcmp(fileName->valuestring, "BAK.TB.tar")==0)
	        {
	            char temp[200];
			snprintf(temp,200,"%s%s",SDCARD_Backup_Path,fileName->valuestring);
			Api_Restore(temp);
			//cJSON_Delete(fileList);
			//return 0;
			break;
	        }
	    }
	}
	if(fileRecord)
		sleep(2);
	cJSON_Delete(fileList);
	dir = opendir(SDCARD_TbUpdate_PATH); 
	if(dir == NULL)
	{
		//BEEP_ERROR();
		return 0;
	}
	closedir(dir);
	
  	succ_flag=ResTbRestore(SDCARD_TbUpdate_PATH);
	
	if(succ_flag)
		API_Beep(BEEP_TYPE_DL2)	;//BEEP_CONFIRM();
	//else
	//	BEEP_ERROR();
	return 0;
}
