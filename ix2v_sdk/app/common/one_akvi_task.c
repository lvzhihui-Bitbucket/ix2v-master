
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <getopt.h>
#include <stdio.h>
#include <errno.h>
#include <signal.h>
#include <ctype.h>
#include <stdlib.h>
#include <dirent.h>
#include <sys/stat.h>

#include "ak_common.h"
#include "ak_common_graphics.h"
#include "ak_vo.h"
#include "ak_vi.h"
#include "ak_mem.h"
#include "ak_log.h"
#include "ak_tde.h"

#include "utility.h"
#include "one_tiny_task.h"

typedef struct
{
    int                 state;
    int                 chn1;
    int                 chn2;
	VI_DEV_ATTR	        dev_attr;
	VI_CHN_ATTR         m_chn_attr;
	VI_CHN_ATTR         s_chn_attr;

    one_tiny_task_t		task_akvi_handle;
} AK_VI_T;

AK_VI_T one_ak_vi=
{
    .state  = 0,
    .chn1   = 0,
    .chn2   = 0,
};

int ak_vi_initial(char* cfg_file, int m_w, int m_h, int s_w, int s_h)
{
	int ret;

	ret = ak_vi_open(VIDEO_DEV0);
	if (AK_SUCCESS != ret) 
	{
		ak_print_error_ex(MODULE_ID_VI, "vi device open failed\n");	
		return -1;
	}

	ret = ak_vi_load_sensor_cfg(VIDEO_DEV0, cfg_file);
	if (AK_SUCCESS != ret) 
	{
		ak_print_error_ex(MODULE_ID_VI, "vi device load cfg [%s] failed!\n", cfg_file);	
		ak_vi_close(VIDEO_DEV0);
		return -1;
	}

	RECTANGLE_S res;
	one_ak_vi.dev_attr.interf_mode 		= 0;
	one_ak_vi.dev_attr.dev_id 			= VIDEO_DEV0;
	one_ak_vi.dev_attr.data_path 		= 0;
	one_ak_vi.dev_attr.data_type 		= VI_DATA_TYPE_YUV420SP;
	one_ak_vi.dev_attr.crop.left 		= 0;
	one_ak_vi.dev_attr.crop.top 		= 0;
	one_ak_vi.dev_attr.crop.width 		= m_w;
	one_ak_vi.dev_attr.crop.height 		= m_h;
	one_ak_vi.dev_attr.max_width 		= m_w;
	one_ak_vi.dev_attr.max_height 		= m_h;
	one_ak_vi.dev_attr.sub_max_width 	= s_w;
	one_ak_vi.dev_attr.sub_max_height 	= s_h;
	ret = ak_vi_get_sensor_resolution(VIDEO_DEV0, &res);
	if( ret )
	{
		ak_print_error_ex(MODULE_ID_VI, "Can't get dev[%d]resolution\n",VIDEO_DEV0);
		ak_vi_close(VIDEO_DEV0);
		return -1;
	} 
	else 
	{
		ak_print_normal_ex(MODULE_ID_VI, "get dev res w:[%d]h:[%d]\n",res.width, res.height);
		one_ak_vi.dev_attr.crop.width 	= m_w;
		one_ak_vi.dev_attr.crop.height 	= m_h;
	}

	//default parameters: 25fps, day mode, auto frame-control
	ret = ak_vi_set_dev_attr(VIDEO_DEV0, &one_ak_vi.dev_attr);
	if (ret) 
	{
		ak_print_error_ex(MODULE_ID_VI, "vi device set device attribute failed!\n");
		ak_vi_close(VIDEO_DEV0);
		return -1;
	}
    one_ak_vi.chn1 = 1;

	// set main channel attribute
	one_ak_vi.m_chn_attr.chn_id 	= VIDEO_CHN0;
	one_ak_vi.m_chn_attr.res.width 	= m_w;
	one_ak_vi.m_chn_attr.res.height = m_h;
	one_ak_vi.m_chn_attr.frame_depth = 3;
	ret = ak_vi_set_chn_attr(VIDEO_CHN0, &one_ak_vi.m_chn_attr);
	if (ret) 
	{
		ak_print_error_ex(MODULE_ID_VI, "vi device set channel [%d] attribute failed!\n", VIDEO_CHN0);
		ak_vi_close(VIDEO_DEV0);
		return -1;
	}
	// set sub channel attribute
	one_ak_vi.s_chn_attr.chn_id = VIDEO_CHN1;
	one_ak_vi.s_chn_attr.res.width = s_w;
	one_ak_vi.s_chn_attr.res.height = s_h;
	one_ak_vi.s_chn_attr.frame_depth = 3;
	ret = ak_vi_set_chn_attr(VIDEO_CHN1, &one_ak_vi.s_chn_attr);
	if (ret) 
	{
		ak_print_error_ex(MODULE_ID_VI, "vi device set channel [%d] attribute failed!\n", VIDEO_CHN1);
		ak_vi_close(VIDEO_DEV0);
		return -1;
	}
    one_ak_vi.chn2 = 1;

	// start capture frames
	ret = ak_vi_enable_dev(VIDEO_DEV0);
	if (ret) 
	{
		ak_print_error_ex(MODULE_ID_VI, "vi device enable device  failed!\n");
		ak_vi_close(VIDEO_DEV0);
		return -1;
	}
	ret = ak_vi_enable_chn(VIDEO_CHN0);
	if(ret)
	{
		ak_print_error_ex(MODULE_ID_VI, "vi channel[%d] enable failed!\n",VIDEO_CHN0);
		ak_vi_close(VIDEO_DEV0);
		return -1;
	}
	ret = ak_vi_enable_chn(VIDEO_CHN1);
	if(ret)
	{
		ak_print_error_ex(MODULE_ID_VI, "vi channel[%d] enable failed!\n",VIDEO_CHN1);
		ak_vi_close(VIDEO_DEV0);
		return -1;
	}
	return 0;
}

int ak_vi_deinitial(void)
{
    int ret;
	// disable both channel 
	ak_vi_disable_chn(VIDEO_CHN0);
	ak_vi_disable_chn(VIDEO_CHN1);

	// disable video device 
	ak_vi_disable_dev(VIDEO_DEV0);

	// close vi device 
	ret = ak_vi_close(VIDEO_DEV0);

    one_ak_vi.state= 0;
    one_ak_vi.chn1 = 0;
    one_ak_vi.chn2 = 0;
}

int task_akvi_handle_init( void* arg )
{
        //one_tiny_task_t *ptiny_task = (one_tiny_task_t*)arg;

        return 0;
}

#define REFRESH_VIDEO1			(AK_VO_REFRESH_VIDEO_GROUP&(1<<AK_VO_LAYER_VIDEO_1))

int task_akvi_handle_process( void* arg )
{
    int ret;
    one_tiny_task_t *ptiny_task = (one_tiny_task_t*)arg;

	struct video_input_frame frame;
	struct video_input_frame frame_sub;

	// create the video layer
	struct ak_vo_layer_in video_layer;
	video_layer.create_layer.width  = 480;
	video_layer.create_layer.height = 800;
	video_layer.create_layer.left  = 0;
	video_layer.create_layer.top   = 0;
	video_layer.layer_opt          = 0;
    // create the video_1 */
	ak_vo_create_video_layer(&video_layer, AK_VO_LAYER_VIDEO_1);

	ak_print_normal(MODULE_ID_VI, "capture start\n");

    while( one_tiny_task_is_running(ptiny_task) )
    {        
		memset(&frame, 0x00, sizeof(frame));
		// get main channel frame */
		ret = ak_vi_get_frame(VIDEO_CHN0, &frame);
		if( !ret ) 
		{
            struct ak_vo_obj    obj;
            memset(&obj, 0, sizeof(struct ak_vo_obj));

            obj.format = GP_FORMAT_YUV420SP;
            obj.vo_layer.width 	= one_ak_vi.m_chn_attr.res.width;   	// real width of vdec output data
            obj.vo_layer.height = one_ak_vi.m_chn_attr.res.height; 	// real height of vdec output data
            obj.cmd = GP_OPT_SCALE;                             		// SCALE opt

            // set the pos and range to show
            obj.vo_layer.clip_pos.top 	= 0;
            obj.vo_layer.clip_pos.left 	= 0;
            obj.vo_layer.clip_pos.width =  one_ak_vi.m_chn_attr.res.width;
            obj.vo_layer.clip_pos.height = one_ak_vi.m_chn_attr.res.height;
            obj.vo_layer.dma_addr       = frame.phyaddr;

            // set the param */
            obj.dst_layer.top 		= 480;
            obj.dst_layer.left 		= 0;
            obj.dst_layer.width 	= 480; //320; //dst_width;
            obj.dst_layer.height 	= 320; //240; //dst_height;
			// add main channel image to layer
            ak_vo_add_obj(&obj, AK_VO_LAYER_VIDEO_1);
			ak_vi_release_frame(VIDEO_CHN0, &frame);

			// add sub channel
			if(0) // one_ak_vi.chn2 )
			{
				memset(&frame_sub, 0x00, sizeof(frame_sub));
				ret = ak_vi_get_frame(VIDEO_CHN1, &frame_sub);
				if( !ret )
				{
					obj.format 				= GP_FORMAT_YUV420SP;
 					obj.vo_layer.width 		= one_ak_vi.s_chn_attr.res.width;  // real width of vdec output data
					obj.vo_layer.height 	= one_ak_vi.s_chn_attr.res.height; // real height of vdec output data 
					obj.cmd 				= GP_OPT_SCALE;                     // SCALE opt

					// set the pos and range to show 
					obj.vo_layer.clip_pos.top 	= 0;
					obj.vo_layer.clip_pos.left 	= 0;
					obj.vo_layer.clip_pos.width = one_ak_vi.s_chn_attr.res.width;
					obj.vo_layer.clip_pos.height = one_ak_vi.s_chn_attr.res.height;
					obj.vo_layer.dma_addr 		= frame_sub.phyaddr;

					obj.dst_layer.top 			= 400;
					obj.dst_layer.left 			= 0;
					obj.dst_layer.width 		= 240;
					obj.dst_layer.height 		= 160;

					ak_vo_add_obj(&obj, AK_VO_LAYER_VIDEO_1);
					ak_vi_release_frame(VIDEO_CHN1, &frame_sub);
				}
				else
					ak_print_normal_ex(MODULE_ID_VI, "sub chn get frmae failed!\n");
			}
			ak_vo_refresh_cmd_set(AK_VO_REFRESH_VIDEO_GROUP&(1<<AK_VO_LAYER_VIDEO_1));			
		} 
		else 
		{
			ak_sleep_ms(10);
			ak_print_normal_ex(MODULE_ID_VI, "empty!\n");
		}
    }    
    return 0;
}

int task_akvi_handle_exit( void* arg )
{
    // AK_VI_T *ptiny_task = (AK_VI_T*)arg;
    return 0;
}

int api_start_akvi_task(void)
{
	#if 0
    if( one_ak_vi.state )
        return -1;
	set_menu_with_video_on();
    one_ak_vi.state = 1;
    if( ak_vi_initial("/mnt/nand1-1/App/share/isp_dc7120_dvp-H1V1P1.conf",1280,720,640,480) == 0 )
    {
        one_tiny_task_start( &one_ak_vi.task_akvi_handle, ffask_akvi_handle_init, task_akvi_handle_process, task_akvi_handle_exit, &one_ak_vi.task_akvi_handle);
        printf("api_start_akvi_task ok!\n");
        return 0;	
    }
    else
    {
        printf("api_start_akvi_task fail!\n");
        return -1;
    }
	#else
	set_menu_with_video_on();
	return 0;
	#endif
}

int api_stop_akvi_task(void)
{
    if( !one_ak_vi.state )
        return -1;		
    one_ak_vi.state = 0;

	#if 0
	one_tiny_task_stop( &one_ak_vi.task_akvi_handle );
    ak_vi_deinitial();
	#else
	api_local_preview_off(0);
	#endif
	return 0;
}

