#ifndef _obj_FileManageUtility_H_
#define _obj_FileManageUtility_H_

#include "cJSON.h"
#define	IF_TRUE(mode, mask)	((mode) & (mask))

void SetJsonToFile(const char* filePath, const cJSON *json);
CJSON_PUBLIC(cJSON *) GetJsonFromFile(const char* filePath);
int DeleteFileProcess(char* dir, char* fileName);
int GetDirFreeSize(char *path);
int MakeDir(char* path);
int IsFileExist(char* path);
int FileMd5_CalculateString(char *file_path, char *file_md5_string);
int MoveFile(const char* srcFile, const char* tagFile);
int CopyFile(const char* srcFile, const char* tagFile);
int FileChmod(const char* srcFile, const char* mode);

#endif


