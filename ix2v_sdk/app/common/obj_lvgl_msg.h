/**
  ******************************************************************************
  * @file    obj_lvgl_msg.h
  * @author  jia
  * @version V1.0.0
  * @date    2012.06.01
  * @brief   This file contains the define of GPIO use.
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2012 V-Tec</center></h2>
  ******************************************************************************
  */ 

#ifndef _obj_lvgl_msg_H_
#define _obj_lvgl_msg_H_

#include "lvgl.h"

#define MSG_DT_CALL_REQ                     3
#define MSG_DT_CALL_RESULT                  4
//#define MSG_SETTING_SIP_ROUTE                  		1001
//#define MSG_SETTING_SIP_ROUTE_RSP                 	1002

#define MSG_DT_CHECK_ONLINE_RESULT          2001
#define MSG_DT_CHECK_ONLINE_REQ             2002

#define MSG_TP_KEY_PROCESS                  2003

#define MSG_LOCK          1
#define MSG_UNLOCK        2
#define MSG_LANCONFIG_CONFIM       11
#define MSG_LANCONFIG_RESULT       12
#define MSG_WLANCONFIG_SEARCH      21
#define MSG_WLANCONFIG_RESULT      22
#define MSG_WLAN_CONNECT           23
#define MSG_WLAN_RESULT            24
#define MSG_WLAN_OPEN              25
#define MSG_WLAN_CLOSE             26
#define MSG_SIP_REGISTER           31
#define MSG_SIP_DEFAULT            32

#define MSG_IPC_SEARCH_REQ          100
#define MSG_IPC_SEARCH_RESULT       101
#define MSG_IPC_LOGIN_REQ           102
#define MSG_IPC_LOGIN_OK            103
#define MSG_IPC_LOGIN_ERR           104
#define MSG_IPC_PREVIEW             105
#define MSG_IPC_Add                 106
#define MSG_IPC_Del                 107
#define MSG_IPC_AUTO_CONFIG         108
#define MSG_IPC_AUTO_RESULT         109

#define IX850_MSG_CALL_RESULT        	2302171094
#define IX850_MSG_CALL_INFO   		    2302286601
#define IX850_MSG_Namelist_Reload   	2303016601
#define IX850_MSG_UNCALL              2302171093
#define IX850_MSG_LOCK                2302171091
#define IX850_MSG_CALL                2302171092
#define IX850_MSG_DIVERT_INFO        	2305196601

#define SETTING_MSG_SettingReplacePage  230721900
#define SETTING_MSG_SettingChangePage   230721901
#define SETTING_MSG_SettingClose        230721902
#define SETTING_MSG_SettingBtn 		  	  230721903
#define SETTING_MSG_SettingReturn 		  230721904
#define SETTING_MSG_SettingTextEdit 		  	231123601
#define SETTING_MSG_NormalTip 		  		231218601
#define SETTING_MSG_SuccTip 		  		231218602
#define SETTING_MSG_ErrorTip 		  		231218603
#define SETTING_MSG_LongTip 		  		231222601
#define SETTING_MSG_ValueFresh 		  		231221601

#define MAINMENU_NL_BACK 		  		230727601
#define MAINMENU_NL_SEARCH 		  		230727602
#define MAINMENU_NL_JUMP 		  		230727603

#define MAINMENU_RL_BACK 		  		230727604
#define MAINMENU_RL_SEARCH 		  	230727605
#define MAINMENU_RL_JUMP 		  		230727606

#define MAINMENU_MAIN_NAMELIST 		  230727607
#define MAINMENU_MAIN_ROOMLIST 		 230727608
#define MAINMENU_MAIN_CODEKP	  		230727609
#define MAINMENU_MAIN_DIALKP			230727610
#define MAINMENU_MAIN_HELP			230727611

#define MAINMENU_MAIN_StartCall			230801601
#define MAINMENU_MAIN_ToCall			230801602
#define MAINMENU_MAIN_CallReturn		230801603
#define MAINMENU_MAIN_CallError			230801604
#define MAINMENU_Return_MainPage		230801605

#define MAINMENU_MAIN_ToSetting			230803601
#define MAINMENU_Reload			230803602

#define IXG_MSG_ENTER_IM			230823901
#define IXG_MSG_ENTER_DM			230823902
#define IXG_MSG_CHANGE_STATE			230824901
#define IXG_MANAGE_REFRESH_SEND	  230828901
#define IM_MANAGE_REFRESH_SEND		230828902
#define IXG_MANAGE_REFRESH_RECEIVE	  230831901
#define IM_MANAGE_REFRESH_RECEIVE		230831902
#define IXG_MSG_REBOOT	          230901901
#define IXG_MSG_RESTORE		        230901902
#define FWUPGRADE_SERVER          230906901
#define FWUPGRADE_DLCODE          230906902
#define FWUPGRADE_OPERATION       230906903
#define FWUPGRADE_RECEIVE         230906904
#define FWUPGRADE_INIT            230906905
#define FWUPGRADE_EXIT            230927601
#define ENTER_FW_UPGRADE          230906906
#define ENTER_IXG_ABOUT           230906907
#define IXG_MANAGE_EXIT           230911901
#define IM_MANAGE_EXIT            230911902
#define MENU_PASSWORD_EXIT        230921901
#define IXG_LIST_REFRESH          231009901

#define MAINMENU_COMMON_CLICK 240513601

#define MENU_CLICK 240709901

void CB_DT_CheckOnline(void * s, lv_msg_t * m);
void CB_DT_CheckOnlineResult(void * s, lv_msg_t * m);
void CB_DT_Call(void * s, lv_msg_t * m);
void CB_DT_CallResult(void * s, lv_msg_t * m);
void CB_SET_SipRoute(char *para);
void CB_SET_SipRouteRespond(char *para);

void lock_control(void* s, lv_msg_t* m);
void lv_msg_NamelistReload_Callback(void* s, lv_msg_t* m);

void lv_msg_setting_handler(void* s, lv_msg_t* m);
void btn_Pressed_handle(void* s, lv_msg_t* m);

#endif

