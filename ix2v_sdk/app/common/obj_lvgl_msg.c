/**
 ******************************************************************************
 * @file    obj_lvgl_msg.c
 * @author  jia
 * @version V1.0.0
 * @date    2012.06.01
 * @brief   This file contains the define of GPIO use.
 ******************************************************************************
 * @attention
 *
 *
 * <h2><center>&copy; COPYRIGHT 2012 V-Tec</center></h2>
 ******************************************************************************
 */

#include "obj_lvgl_msg.h"

void lvgl_msg_init(void)
{

#if defined(PID_IX611) || defined(PID_IX622)|| defined(PID_IX821)
     
#else
    lv_msg_subsribe(MSG_DT_CHECK_ONLINE_REQ, CB_DT_CheckOnline, NULL);
    lv_msg_subsribe(MSG_DT_CHECK_ONLINE_RESULT, CB_DT_CheckOnlineResult, NULL);
    lv_msg_subsribe(MSG_DT_CALL_REQ, CB_DT_Call, NULL);
    lv_msg_subsribe(MSG_DT_CALL_RESULT, CB_DT_CallResult, NULL);    
    //lv_msg_subsribe(MSG_SETTING_SIP_ROUTE, CB_SET_SipRoute, NULL); 
   // lv_msg_subsribe(MSG_SETTING_SIP_ROUTE_RSP, CB_SET_SipRouteRespond, NULL);

//call
    lv_msg_subsribe(IX850_MSG_LOCK, lock_control, NULL);
    lv_msg_subsribe(IX850_MSG_CALL, lock_control, NULL);
    lv_msg_subsribe(IX850_MSG_UNCALL, lock_control, NULL);
   
//namelist
    lv_msg_subsribe(IX850_MSG_Namelist_Reload, lv_msg_NamelistReload_Callback, NULL);

//setting菜单
    lv_msg_subsribe(SETTING_MSG_SettingChangePage, lv_msg_setting_handler, NULL);	
    lv_msg_subsribe(SETTING_MSG_SettingClose, lv_msg_setting_handler, NULL);
	lv_msg_subsribe(SETTING_MSG_SettingBtn, lv_msg_setting_handler, NULL);
	lv_msg_subsribe(SETTING_MSG_SettingTextEdit, lv_msg_setting_handler, NULL);
    lv_msg_subsribe(SETTING_MSG_SettingReturn, lv_msg_setting_handler, NULL);
	lv_msg_subsribe(SETTING_MSG_NormalTip, lv_msg_setting_handler, NULL);
	lv_msg_subsribe(SETTING_MSG_SuccTip, lv_msg_setting_handler, NULL);
	lv_msg_subsribe(SETTING_MSG_ErrorTip, lv_msg_setting_handler, NULL);
	lv_msg_subsribe(SETTING_MSG_ValueFresh, lv_msg_setting_handler, NULL);
	lv_msg_subsribe(SETTING_MSG_LongTip, lv_msg_setting_handler, NULL);

    lv_msg_subsribe(MENU_CLICK, btn_Pressed_handle, NULL);
#endif
}
