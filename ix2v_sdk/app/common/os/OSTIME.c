
#include "OSTIME.h"

/*********************************************************************
*
*       OS_CreateTimer
*/
void OS_CreateTimer( OS_TIMER* pTimer, OS_TIMERROUTINE* Hook, OS_TIME Period )
{	
#ifndef MULTI_TIMER
	memset( pTimer, 0, sizeof(OS_TIMER) );
	
	pTimer->Period = Period;

	pTimer->Evp.sigev_value.sival_ptr	= &pTimer->TimerID;
	pTimer->Evp.sigev_notify			= SIGEV_SIGNAL;//SIGEV_THREAD;//SIGEV_SIGNAL;
//	pTimer->Evp.sigev_notify_function 	= &Hook;
//	pTimer->Evp.sigev_notify_attributes = NULL;
	pTimer->Evp.sigev_signo			= SIGUSR2;

//	pTimer->Act.sa_handler 			= Hook;
//	pTimer->Act.sa_flags 			= 0;

	//sigemptyset( &pTimer->Act.sa_mask );

	//if( sigaction(SIGUSR2, &pTimer->Act, NULL) == -1 )
	{
	//	perror("fail to sigaction");
	}
	
	signal( SIGUSR2, Hook );

	if( timer_create(CLOCK_REALTIME, &pTimer->Evp, &pTimer->TimerID) == -1 )
	{
		perror(" OS_CreateTimer fail to timer_create");
	}
#else
	add_a_timer(&(pTimer->timer),(timer_proc)Hook,(int)Period);
#endif
}

/*********************************************************************
*
*       OS_RetriggerTimer
*/
void OS_RetriggerTimer( OS_TIMER* pTimer ) 
{
#ifndef MULTI_TIMER
	if( !pTimer->Active ) 
	{
		pTimer->Active					= 1;
		pTimer->It.it_interval.tv_sec	= 0;
		pTimer->It.it_interval.tv_nsec 	= pTimer->Period * 1000000;
		pTimer->It.it_value.tv_sec		= 0;
		pTimer->It.it_value.tv_nsec 	= pTimer->Period * 1000000;
		
		if( timer_settime(pTimer->TimerID, 0, &pTimer->It, NULL) == -1 )
		{
			perror("beep_timer_settime fail to timer_settime");
		}
	}
#else
	start_a_timer(&pTimer->timer);
#endif
}

/*********************************************************************
*
*       OS_StartTimer
*
*       Always ends with a call of OS_RESTORE_I().
*       OS_RetriggerTimer() relies on this call !
*/
 void OS_StartTimer( OS_TIMER* pTimer ) 
 {
#ifndef MULTI_TIMER 
	OS_RetriggerTimer( pTimer );
#else
	OS_RetriggerTimer( pTimer );
#endif
}

/*********************************************************************
*
*       OS_StopTimer()
*/
void OS_StopTimer( OS_TIMER* pTimer ) 
{
#ifndef MULTI_TIMER
	if( pTimer->Active ) 
	{
		pTimer->Active					= 0;
		pTimer->It.it_interval.tv_sec	= 0;
		pTimer->It.it_interval.tv_nsec 	= 0;
		pTimer->It.it_value.tv_sec		= 0;
		pTimer->It.it_value.tv_nsec 	= 0;
		
		if( timer_settime(pTimer->TimerID, 0, &pTimer->It, NULL) == -1 )
		{
			perror("beep_timer_settime fail to timer_settime");
		}
	}
#else
	stop_a_timer(&pTimer->timer);
#endif
}

/*********************************************************************
*
*       OS_SetTimerPeriod
*
**********************************************************************
*/
void OS_SetTimerPeriod( OS_TIMER* pTimer, OS_TIME Period )
{
#ifndef MULTI_TIMER
	pTimer->Period = Period;
#else
	set_a_timer_period(&pTimer->timer,Period);
#endif
}

/*********************************************************************
*
*       Global functions
*
**********************************************************************
*/
void OS_DeleteTimer( OS_TIMER* pTimer )
{
#ifndef MULTI_TIMER
	timer_delete( &pTimer->TimerID );
#else
	del_a_timer(&pTimer->timer);
#endif
}


/*********************************************************************
*
*       OS_CreateTimer
*/
#include <stdio.h>  
#include <errno.h>  
#include <unistd.h>  
#include <string.h>  
#include <time.h>  
#include <sys/time.h>  
#include <netinet/in.h>  
#include <sys/socket.h>  
#include <arpa/inet.h>  
#include <netdb.h>  

#include "sntp_c.h"  

#include "unix_socket.h"
#include "obj_PublicInformation.h" 

static int TimerOneThread_task_process( timer_info_t* ptimer)	
{
	sem_t				sem;
	sem_init(&sem,0,0);
	while(1)
	{
		sem_wait_ex2(&sem, 20);
		pthread_mutex_lock( &ptimer->lock );
				
		if( ptimer->state )
		{
			ptimer->counter++;
			if( ptimer->counter >= ptimer->interval )
			{
				ptimer->counter 	= 0;
				ptimer->state 	= 0;		//cao20151118
				pthread_mutex_unlock( &ptimer->lock );
				(*ptimer->callback)();
				continue;
			}
			else
			{
				//pthread_mutex_unlock( &ptimer->lock );				
			}
		}
		pthread_mutex_unlock( &ptimer->lock );
	}
	
	return 0;	
}

void OS_CreateTimerOneThread( OS_TIMER* pTimer, OS_TIMERROUTINE* Hook, OS_TIME Period )
{	
	pthread_t 			pid;			// 任务id
	pthread_attr_t 		pattr;
	
	 pthread_mutex_init( &pTimer->timer.lock, 0 );

	pTimer->timer.state= 0;
	pTimer->timer.counter		= 0;
	pTimer->timer.interval	= Period;
	pTimer->timer.callback	= Hook;
	pTimer->timer.handle		= 0;
	pthread_attr_init( &pattr );
	if( pthread_create(&pid,&pattr,(void*)TimerOneThread_task_process, &pTimer->timer ) != 0 )
	{
		printf( "create one_task_startd error! \n" );
		return -1;
	}	
}



