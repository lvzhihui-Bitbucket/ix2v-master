

#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "one_tiny_task.h"
#include "elog_forcall.h"

static int tiny_task_exit( one_tiny_task_t *pins )	
{
	pins->cb_exit(pins);		
	printf( "%s(%d)-<%s>\n", __FILE__, __LINE__, __FUNCTION__ );		
	pthread_mutex_lock( &pins->lock );	
	pins->flag	= -1;	
	pthread_mutex_unlock( &pins->lock );
	return 0;	
}

static int tiny_task_process( one_tiny_task_t *pins )	
{
	pthread_cleanup_push( tiny_task_exit, pins  );

	if(pins->cb_init(pins) != -1)
	{
		printf( "%s(%d)-<%s>\n", __FILE__, __LINE__, __FUNCTION__ );		

		pthread_mutex_lock( &pins->lock );	
		pins->flag	= 1;	
		pthread_mutex_unlock( &pins->lock );

		pins->cb_proc(pins);
		

		printf( "%s(%d)-<%s>\n", __FILE__, __LINE__, __FUNCTION__ );		
	}

	pthread_cleanup_pop(1);
	
	return 0;	
}
static int tiny_task_process2(one_tiny_task_t *pins )	
{
	pthread_cleanup_push( tiny_task_exit, pins  );

	if(pins->cb_init(pins) != -1)
	{
		printf( "%s(%d)-<%s>\n", __FILE__, __LINE__, __FUNCTION__ );		

		pthread_mutex_lock( &pins->lock );	
		//pins->flag	= 1;	
		pthread_mutex_unlock( &pins->lock );

		pins->cb_proc(pins);
		

		printf( "%s(%d)-<%s>\n", __FILE__, __LINE__, __FUNCTION__ );		
	}
	else
		pins->flag	= 0;	

	pthread_cleanup_pop(1);
	
	return 0;	
}

// init one tiny task and run
int one_tiny_task_start( one_tiny_task_t *pins, callback_init cb_init, callback_proc cb_proc, callback_exit cb_exit, void* pusr_dat )
{
	pthread_mutex_init( &pins->lock, 0);
	pins->cb_init	= cb_init;
	pins->cb_proc	= cb_proc;
	pins->cb_exit	= cb_exit;	
	pins->pusr_dat 	= pusr_dat;
	pins->flag		= 0;
	
	pthread_attr_init( &pins->pattr );
	size_t stacksize;
	pthread_attr_getstacksize (&pins->pattr, &stacksize); 
	stacksize *=4;
	//stacksize=PTHREAD_STACK_MAX;
	pthread_attr_setstacksize (&pins->pattr, stacksize); 
	// lzh_20181016_s
#ifdef USE_THREAD_DETACHED_MODE
	pthread_attr_setdetachstate(&pins->pattr,PTHREAD_CREATE_DETACHED);  // 需要和主线程分离
#endif
	// lzh_20181016_e	
	if( pthread_create(&pins->pid,&pins->pattr,(void*)tiny_task_process, pins ) != 0 )
	{
		printf( "create one_task_startd error! \n" );
		return -1;
	}	
	
	//printf( "%s(%d)-<%s>\n", __FILE__, __LINE__, __FUNCTION__ );		
	
	return 0;	
}
#define TINY2_USE_THREAD2_DETACHED_MODE
int one_tiny_task_start2( one_tiny_task_t *pins, callback_init cb_init, callback_proc cb_proc, callback_exit cb_exit, void* pusr_dat )
{
	pthread_mutex_init( &pins->lock, 0);
	pins->cb_init	= cb_init;
	pins->cb_proc	= cb_proc;
	pins->cb_exit	= cb_exit;	
	pins->pusr_dat 	= pusr_dat;
	pins->flag		= 1;
	size_t stacksize;
	pthread_attr_init( &pins->pattr );
	//pthread_attr_getstacksize (&pins->pattr, &stacksize); 
	//stacksize *=2;
	//pthread_attr_setstacksize (&pins->pattr, &stacksize); 
	// lzh_20181016_s
#ifdef TINY2_USE_THREAD2_DETACHED_MODE
	pthread_attr_setdetachstate(&pins->pattr,PTHREAD_CREATE_DETACHED);  // 需要和主线程分离
#endif
	// lzh_20181016_e	
	if( pthread_create(&pins->pid,&pins->pattr,(void*)tiny_task_process2, pins ) != 0 )
	{
		pins->flag		= 0;
		printf( "create one_task_startd error! \n" );
		return -1;
	}	
	
	//printf( "%s(%d)-<%s>\n", __FILE__, __LINE__, __FUNCTION__ );		
	
	return 0;	
}

// check one tiny task just running
int one_tiny_task_is_running( one_tiny_task_t *pins )
{
	int ret;
	pthread_mutex_lock( &pins->lock );	
	ret = (pins->flag == 1? 1 : 0);
	pthread_mutex_unlock( &pins->lock );
	return ret;
}

// stop one tiny task
int one_tiny_task_stop( one_tiny_task_t *pins )
{
#ifdef USE_THREAD_DETACHED_MODE
	if(pins->flag	!= 1)
		return 0;
		
	pthread_mutex_lock( &pins->lock );	
	pins->flag	= 0;	
	pthread_mutex_unlock( &pins->lock );
	int cnt=0;
	while(pins->flag!=-1&&cnt++<100)
	{
		usleep(100*1000);
	}
	if(pins->flag!=-1)
		log_e("one_tiny_task_stop timeout");
	pthread_mutex_lock( &pins->lock );	
	pins->flag	= 0;	
	pthread_mutex_unlock( &pins->lock );
#else
/*
	if(pins->flag)
	{
		if(pins->pid != pthread_self())
		{
			pthread_mutex_lock( &pins->lock );	
			pins->flag	= 0;	
			pthread_mutex_unlock( &pins->lock );
			pthread_cancel( pins->pid );
			pthread_join( pins->pid, NULL );
		}
		else
		{
			pthread_mutex_lock( &pins->lock );	
			pins->flag	= 0;	
			pthread_mutex_unlock( &pins->lock );
		}
	}
	*/
	pthread_cancel( pins->pid ) ;
	pthread_join( pins->pid, NULL );
#endif

	printf( "%s(%d)-<%s> \n", __FILE__, __LINE__, __FUNCTION__ );
	
	return 0;	
}

int one_tiny_task_stop2( one_tiny_task_t *pins )
{
#ifdef TINY2_USE_THREAD2_DETACHED_MODE
	if(pins->flag	!= 1)
		return 0;
		
	pthread_mutex_lock( &pins->lock );	
	pins->flag	= 0;	
	pthread_mutex_unlock( &pins->lock );
	int cnt=0;
	while(pins->flag!=-1&&cnt++<100)
	{
		usleep(100*1000);
	}
	if(pins->flag!=-1)
		log_e("one_tiny_task_stop timeout");
	pthread_mutex_lock( &pins->lock );	
	pins->flag	= 0;	
	pthread_mutex_unlock( &pins->lock );
#else
	if(pins->flag)
	{
		if(pins->pid != pthread_self())
		{
			pthread_mutex_lock( &pins->lock );	
			pins->flag	= 0;	
			pthread_mutex_unlock( &pins->lock );
			pthread_cancel( pins->pid );
			pthread_join( pins->pid, NULL );
		}
		else
		{
			pthread_mutex_lock( &pins->lock );	
			pins->flag	= 0;	
			pthread_mutex_unlock( &pins->lock );
		}
	}
#endif

	printf( "%s(%d)-<%s> \n", __FILE__, __LINE__, __FUNCTION__ );
	
	return 0;	
}

