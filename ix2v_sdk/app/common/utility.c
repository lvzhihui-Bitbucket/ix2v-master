#include "utility.h"
#include "task_survey.h"	
#include "sys_msg_process.h"
#include "cJSON.h"
#include "one_tiny_task.h"
#include "elog_forcall.h"
#include "define_file.h"

/****************************************************************************************************************************
 * @fn:		init_vdp_common_queue
 *
 * @brief:	��ʼ��vdpͨ�����ݶ���
 *
 * @param:  	pobj 			- ���ж���
 * @param:  	qsize 			- ���д�С
 * @param:  	powner 			- ���е�ӵ����
 * @param:	   	process		   	- ���е����ݴ�������
 				
 *
 * @return: 	0/ok, -1/err
****************************************************************************************************************************/
int init_vdp_common_queue(p_Loop_vdp_common_buffer pobj, int qsize, msg_process process, void* powner )
{
	// create embOS queue
	pobj->QSize		= qsize;
	pobj->pQBuf		= (unsigned char*)malloc(qsize);
	OS_Q_Create( &pobj->embosQ, pobj->pQBuf, qsize );

	pobj->process	= process;
	pobj->powner 	= powner;

	if( pobj->pQBuf == NULL ) printf("malloc err!\n"); //else printf("malloc addr = 0x%08x\n",pobj->pQBuf);
		
	return 0;
}

/****************************************************************************************************************************
 * @fn:		exit_vdp_common_queue
 *
 * @brief:	�ͷ�һ������
 *
 * @param:  	pobj 			- ���ж��� 
 *
 * @return: 	0/ok, -1/err
****************************************************************************************************************************/
int exit_vdp_common_queue( p_Loop_vdp_common_buffer pobj )
{
	if(pobj && pobj->pQBuf)
	{
		free( pobj->pQBuf );
		pobj->pQBuf = NULL;
	}
	return 0;
}

/****************************************************************************************************************************
 * @fn:		push_vdp_common_queue
 *
 * @brief:	��vdpͨ�ö����м�����Ϣ����
 *
 * @param:  	pobj 	- ���ж���
 * @param:  	data 	- ����ָ��
 * @length:  	length 	- ���ݳ���
 *
 * @return: 	0/full, !0/current data pointer
****************************************************************************************************************************/
int push_vdp_common_queue( p_Loop_vdp_common_buffer pobj, char *data, unsigned int length)		//czn_20170712
{
	return OS_Q_Put (&pobj->embosQ, data, length);	//0: ok, 1: full
}

/****************************************************************************************************************************
 * @fn:		pop_vdp_common_queue
 *
 * @brief:	�Ӷ����еõ�����ָ��
 *
 * @param:  	pobj 		- ���ж���
 * @param:  	pdb 		- ����ָ��
 * @param:  	timeout 	- ��ʱʱ�䣬msΪ��λ
 *
 * @return: 	size of data
****************************************************************************************************************************/
int pop_vdp_common_queue(p_Loop_vdp_common_buffer pobj, p_vdp_common_buffer* pdb, int timeout)
{
	if( !timeout )
		return OS_Q_GetPtr( &pobj->embosQ,  (void*)pdb );
	else
		return OS_Q_GetPtrTimed( &pobj->embosQ, (void*)pdb, timeout );
}

/****************************************************************************************************************************
 * @fn:		purge_vdp_common_queue
 *
 * @brief:	�������Ķ���
 *
 * @param:  	pobj 		- ���ж���
 *
 * @return: 	size of data
****************************************************************************************************************************/
int purge_vdp_common_queue(p_Loop_vdp_common_buffer pobj)
{
	OS_Q_Purge( &pobj->embosQ );
	return 0;
}

void* vdp_public_task( void* arg )
{
	vdp_task_t*	 ptask 		= (vdp_task_t*)arg;
	p_vdp_common_buffer pdb 	= 0;
	int	size;
	ptask->task_StartCompleted = 1;
	thread_log_add("%s",__func__);
	while( ptask->task_run_flag )
	{
		size = pop_vdp_common_queue( ptask->p_msg_buf, &pdb, VDP_QUEUE_POLLING_TIME);
		if( size > 0 )
		{
			(*ptask->p_msg_buf->process)(pdb,size);
			purge_vdp_common_queue( ptask->p_msg_buf );
		}
	}
	return 0;
}

/****************************************************************************************************************************
 * @fn:		init_vdp_common_task
 *
 * @brief:	��ʼ��һ������
 *
 * @param:  	ptask 			- �������
 * @param:  	task_id			- ����ID
 * @param:  	pthread			- ��������̷߳�����
 * @param:	   	msg_buf		   	- �������Ϣ���� - �������첽��Ϣ
 * @param:	   	syc_buf		   	- �����ͬ������ - ������ͬ����Ϣ
 *
 * @return: 	0/ok, -1/err
****************************************************************************************************************************/
int init_vdp_common_task( vdp_task_t* ptask, int task_id, void* (*pthread)(void*), p_Loop_vdp_common_buffer msg_buf, p_Loop_vdp_common_buffer syc_buf )
{
	ptask->task_StartCompleted 	= 0;
	ptask->task_id 				= task_id;
	ptask->p_msg_buf			= msg_buf;
	ptask->p_syc_buf			= syc_buf;
	ptask->task_pid				= 0;
	ptask->task_run_flag 		= 1;
	if( pthread  != NULL )
	{
		if( pthread_create(&ptask->task_pid, 0, pthread, ptask) != 0 )
		{
			eprintf("Create task thread Failure,%s\n", strerror(errno));
		}
		else
		{
			dprintf("Create task thread pid = %lu\n", ptask->task_pid);	
		}
	}	
	return 0;
}

/****************************************************************************************************************************
 * @fn:		exit_vdp_common_task
 *
 * @brief:	����һ������
 *
 * @param:  	ptask 			- �������
 *
 * @return: 	0/ok, -1/err
****************************************************************************************************************************/
int exit_vdp_common_task( vdp_task_t* ptask )
{
#ifdef USE_THREAD_DETACHED_MODE
	ptask->task_run_flag = 0;
#else
	if(	ptask->task_run_flag)
	{
		ptask->task_run_flag = 0;
		pthread_cancel(ptask->task_pid) ;
		pthread_join( ptask->task_pid, NULL );
	}
#endif
	return 0;	
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
int select_ex(int nfds, fd_set *readfds, fd_set *writefds, fd_set *exceptfds, struct timeval *timeout)
{
	int r;
	do
	{
		r = select(nfds, readfds, writefds, exceptfds, timeout);
	}while((-1 == r) && (EINTR == errno));
	return r;
}

int ioctlex(int fd, int request, void * arg)
{
	int r;
	do
	{
		r = ioctl(fd, request, arg);
	}while((-1 == r) && (EINTR == errno));
	return r;
}

int sem_wait_ex(sem_t *p_sem, int semto)
{
	struct timeval tv;
	struct timespec ts;
	int ret;
	
	if(semto > 0)
	{
		if(gettimeofday(&tv, 0) == -1)
		{
			printf("clock_gettime call failure!msg:%s\n", strerror(errno));
		}
		ts.tv_sec = (tv.tv_sec + (semto / 1000 ));
		ts.tv_nsec = (tv.tv_usec  + ((semto % 1000) * 1000)) * 1000;
		ts.tv_sec += ts.tv_nsec / (1000 * 1000 * 1000);
		ts.tv_nsec %= 1000 * 1000 * 1000;
		
		while(((ret = sem_timedwait(p_sem, &ts)) != 0) && (errno == EINTR));
		if(errno == ETIMEDOUT)
		{
			ret = 0;
		}
	}
	else
	{
		while(((ret = sem_wait(p_sem)) != 0) && (errno == EINTR));
	}	
	return ret;
}

int sem_wait_ex2(sem_t *p_sem, int semto)
{
	struct timeval tv;
	struct timespec ts;
	int ret;
	
	if(semto > 0)
	{
		if(gettimeofday(&tv, 0) == -1)
		{
			printf("clock_gettime call failure!msg:%s\n", strerror(errno));
		}
		ts.tv_sec = (tv.tv_sec + (semto / 1000 ));
		ts.tv_nsec = (tv.tv_usec  + ((semto % 1000) * 1000)) * 1000;
		ts.tv_sec += ts.tv_nsec / (1000 * 1000 * 1000);
		ts.tv_nsec %= 1000 * 1000 * 1000;
		
		ret = sem_timedwait(p_sem, &ts);
	}
	else
	{
		while(((ret = sem_wait(p_sem)) != 0) && (errno == EINTR));
	}	
	return ret;
}

int set_block_io(int fd, int is_block)
{
	int socket_flags;

	socket_flags = fcntl(fd, F_GETFL, 0);
	if(socket_flags < 0)
	{
		printf("Cant Not Get Socket Flags!\n");
		return -1;
	}
	if(is_block)
	{
		if(fcntl(fd, F_SETFL, (socket_flags & ~O_NONBLOCK)) < 0)
		{
			printf("Cant Not Set Socket Flags Block!\n");
			return -1;
		}
	}
	else
	{
		if(fcntl(fd, F_SETFL, (socket_flags | O_NONBLOCK)) < 0)
		{
			printf("Cant Not Set Socket Flags Non_Block!\n");
			return -1;
		}
	}
	return 0;
}

int get_format_time(char *tstr)
{
	int len;
	time_t t;
	t = time(NULL);	
	strcpy(tstr, ctime(&t));
	len = strlen(tstr);
	tstr[len-1] = '\0';
	return len;
}
      
#define MAC_BCAST_ADDR      (unsigned char *) "\xff\xff\xff\xff\xff\xff"   
#define ETH_INTERFACE       "eth0"   
  
struct arpMsg 
{  
	struct ethhdr ethhdr;       		/* Ethernet header */  
	unsigned short htype;              	/* hardware type (must be ARPHRD_ETHER) */  
	unsigned short ptype;              	/* protocol type (must be ETH_P_IP) */  
	unsigned char  hlen;               	/* hardware address length (must be 6) */  
	unsigned char  plen;               	/* protocol address length (must be 4) */  
	unsigned short operation;          /* ARP opcode */  
	unsigned char  sHaddr[6];          /* sender's hardware address */  
	unsigned char  sInaddr[4];         /* sender's IP address */  
	unsigned char  tHaddr[6];          	/* target's hardware address */  
	unsigned char  tInaddr[4];         /* target's IP address */  
	unsigned char  pad[18];            	/* pad for min. Ethernet payload (60 bytes) */  
};  
  
struct server_config_t 
{  
	unsigned int server;       		/* Our IP, in network order */  
	unsigned int start;        		/* Start address of leases, network order */  
	unsigned int end;          		/* End of leases, network order */  
	struct option_set *options; 	/* List of DHCP options loaded from the config file */  
	char *interface;       	 		/* The name of the interface to use */  
	int ifindex;            			/* Index number of the interface to use */  
	unsigned char arp[6];       		/* Our arp address */  
	unsigned long lease;        		/* lease time in seconds (host order) */  
	unsigned long max_leases;   	/* maximum number of leases (including reserved address) */  
	char remaining;         			/* should the lease file be interpreted as lease time remaining, or * as the time the lease expires */  
	unsigned long auto_time;    	/* how long should udhcpd wait before writing a config file.  * if this is zero, it will only write one on SIGUSR1 */  
	unsigned long decline_time;     	/* how long an address is reserved if a client returns a * decline message */  
	unsigned long conflict_time;    	/* how long an arp conflict offender is leased for */  
	unsigned long offer_time;   	/* how long an offered address is reserved */  
	unsigned long min_lease;    	/* minimum lease a client can request*/  
	char *lease_file;  
	char *pidfile;  
	char *notify_file;      			/* What to run whenever leases are written */  
	unsigned int siaddr;       		/* next server bootp option */  
	char *sname;            			/* bootp server name */  
	char *boot_file;        			/* bootp boot file option */  
};    
  
struct server_config_t server_config;
  
  
/*
interface 	- �����豸���� �ӿ�
ifindex	- �������� 
addr		- ����IP��ַ 
arp		- ����arp��ַ
*/  
int read_interface(char *interface, int *ifindex, u_int32_t *addr, unsigned char *arp)  
{  
	int fd;  
	/*ifreq�ṹ������/usr/include/net/if.h����������ip��ַ������ӿڣ�����MTU�Ƚӿ���Ϣ�ġ� 
	���а�����һ���ӿڵ����ֺ;������ݡ������Ǹ������壬�п�����IP��ַ���㲥��ַ���������룬MAC�ţ�MTU���������ݣ��� 
	ifreq������ifconf�ṹ�С���ifconf�ṹͨ���������������нӿڵ���Ϣ�ġ� 
	*/  
	struct ifreq ifr;  
	struct sockaddr_in *our_ip;  

	memset(&ifr, 0, sizeof(struct ifreq));  
	
	/*����һ��socket������SOCK_RAW��Ϊ�˻�ȡ������������IP�����ݣ�      IPPROTO_RAW�ṩӦ�ó�������ָ��IPͷ���Ĺ��ܡ�    */  
	if((fd = socket(AF_INET, SOCK_RAW, IPPROTO_RAW)) >= 0) 
	{  
		ifr.ifr_addr.sa_family = AF_INET;  
		
		/*���������͸�ֵ��ifr_name*/  
		strcpy(ifr.ifr_name, interface);  
  
		if (addr) 
		{  
			/*SIOCGIFADDR���ڼ����ӿڵ�ַ*/  
			if (ioctl(fd, SIOCGIFADDR, &ifr) == 0) 
			{
				/*��ȡ����IP��ַ��addr��һ��ָ��õ�ַ��ָ��*/  
				our_ip = (struct sockaddr_in *) &ifr.ifr_addr;  
				*addr = our_ip->sin_addr.s_addr;  
				printf("%s (our ip) = %s\n", ifr.ifr_name, inet_ntoa(our_ip->sin_addr));  
			} 
			else 
			{  
				printf("SIOCGIFADDR failed, is the interface up and configured?: %s\n",  strerror(errno));  
				return -1;  
            		}  
		}
  
		/*SIOCGIFINDEX���ڼ����ӿ�����*/  
		if (ioctl(fd, SIOCGIFINDEX, &ifr) == 0) 
		{  
			printf("adapter index %d\n", ifr.ifr_ifindex);  
			/*ָ��ifindex ��ȡ����*/  
			*ifindex = ifr.ifr_ifindex;  
		} 
		else 
		{  
			printf("SIOCGIFINDEX failed!: %s\n", strerror(errno));  
			return -1;  
		}  
		
		/*SIOCGIFHWADDR���ڼ���Ӳ����ַ*/  
		if (ioctl(fd, SIOCGIFHWADDR, &ifr) == 0) 
		{  
			/*����ȡ��Ӳ����ַ���Ƶ��ṹserver_config������arp[6]������*/  
			memcpy(arp, ifr.ifr_hwaddr.sa_data, 6);  
			printf("adapter hardware address %02x:%02x:%02x:%02x:%02x:%02x\n",  arp[0], arp[1], arp[2], arp[3], arp[4], arp[5]);  
		} 
		else 
		{  
			printf("SIOCGIFHWADDR failed!: %s\n", strerror(errno));  
			return -1;  
		}  
	}  
	else 
	{  
		printf("socket failed!: %s\n", strerror(errno));  
		return -1;  
	}  
	close(fd);  
	return 0;  
}  
  
  
/*
yiaddr 	- Ŀ��IP��ַ
ip		- ����IP��ַ
mac		- ����mac��ַ
interface	- ��������
*/  
int arpping(u_int32_t yiaddr, u_int32_t ip, unsigned char *mac, char *interface)  
{  
	int timeout 	= 2;  
	int optval 	= 1;  
	int s;                      		/* socket */  
	int rv 		= 0;                 		/* return value */  
	struct sockaddr 	addr; 	/* for interface name */  
	struct arpMsg 	arp;  
	fd_set 			fdset;  
	struct timeval 	tm;  
	time_t 			prevTime;
  
	/*socket����һ��arp��*/  
	if ((s = socket (PF_PACKET, SOCK_PACKET, htons(ETH_P_ARP))) == -1) 
	{  
		eprintf("Could not open raw socket\n");  
		return -1;  
	}  
      
	/*�����׽ӿ�����Ϊ�㲥�������arp���ǹ㲥�����������*/  
	if (setsockopt(s, SOL_SOCKET, SO_BROADCAST, &optval, sizeof(optval)) == -1) 
	{  
		eprintf("Could not setsocketopt on raw socket\n");  
		close(s);  
		return -1;  
	}
  
	/* ��arp���ã����ﰴ��arp���ķ�װ��ʽ��ֵ���ɣ����http://blog.csdn.net/wanxiao009/archive/2010/05/21/5613581.aspx */  
	memset(&arp, 0, sizeof(arp));  
	
	memcpy(arp.ethhdr.h_dest, MAC_BCAST_ADDR, 6);  	/* MAC DA */  
	memcpy(arp.ethhdr.h_source, mac, 6);        			/* MAC SA */  
	
	arp.ethhdr.h_proto 	= htons(ETH_P_ARP);     		/* protocol type (Ethernet) */  
	arp.htype 			= htons(ARPHRD_ETHER);        	/* hardware type */  
	arp.ptype 			= htons(ETH_P_IP);            		/* protocol type (ARP message) */
	arp.hlen 				= 6;                   				/* hardware address length */  
	arp.plen 				= 4;                   				/* protocol address length */  
	arp.operation 			= htons(ARPOP_REQUEST);       	/* ARP op code */  
	
	*((u_int *) arp.sInaddr) = ip;          					/* source IP address */  
	memcpy(arp.sHaddr, mac, 6);         					/* source hardware address */  
	*((u_int *) arp.tInaddr) = yiaddr;      					/* target IP address */  
  
	memset(&addr, 0, sizeof(addr));  
	strcpy(addr.sa_data, interface);  
	
	/*����arp����*/  
	if( sendto(s, &arp, sizeof(arp), 0, &addr, sizeof(addr)) < 0 ) 
	{
		eprintf("arp send error: %s\n", strerror(errno));  
		rv = 0;  
	}
	/* ����select�������ж�·�ȴ�*/  
	tm.tv_usec = 0;
	time(&prevTime);
	while( timeout > 0 ) 
	{
		FD_ZERO(&fdset);
		FD_SET(s, &fdset);		
		tm.tv_sec = timeout;  
		
		if( select(s + 1, &fdset, (fd_set *) NULL, (fd_set *) NULL, &tm) < 0 ) 
		{  
			eprintf("Error on ARPING request: %s\n", strerror(errno));  
			if (errno != EINTR) rv = 0;  
		} 
		else if( FD_ISSET(s, &fdset) ) 
		{  
			if( recv(s, &arp, sizeof(arp), 0) < 0 )
			{
				dprintf("received one reply err.....\n");  
			}
			else			
			{
				dprintf("received one reply ok.....\n");  
			}
			/*������� htons(ARPOP_REPLY) bcmp(arp.tHaddr, mac, 6) == 0 *((u_int *) arp.sInaddr) == yiaddr ���߶�Ϊ�棬��ARPӦ����Ч,˵�������ַ���ѽ����ڵ�*/  
			if( (arp.operation == htons(ARPOP_REPLY)) &&  (bcmp(arp.tHaddr, mac, 6) == 0)  &&  (*((u_int *) arp.sInaddr) == yiaddr) ) 
			{  
				dprintf("Valid arp reply receved for this address\n");  
				rv = 1;  
				break;  
			}  
		}  
		timeout -= (time(NULL) - prevTime);
		time(&prevTime);  
	}  
	close(s);  
	return rv;  
}  
  
  
int check_ip(u_int32_t addr)  
{  
	struct in_addr temp;  
  
	if( arpping(addr, server_config.server, server_config.arp, ETH_INTERFACE) > 0 )  
	{  
		temp.s_addr = addr;  
		dprintf("%s belongs to someone, reserving it for %ld seconds\n",  inet_ntoa(temp), server_config.conflict_time);  
		return 1;  
	}  
	else  
	    return 0;  
}  
  
  
int check_ip_repeat( int ip )  
{         
	int ret = 0;

	/*����̫���ӿں�������ȡһЩ������Ϣ*/  
	if (read_interface(ETH_INTERFACE, &server_config.ifindex,  &server_config.server, server_config.arp) < 0)  
	{  
		exit(0);  
	}  
	  
	/*IP��⺯��*/  
	ret = check_ip(ip);
	if( ret <= 0)  
	{  
		dprintf("IP:%08x can use\n", ip);   
	}  
	else  
	{  
		dprintf("IP:%08x conflict\n", ip);  
	} 	  
	return ret;  
}  


char* my_inet_ntoa(int ip_n, char* ip_a)
{
	struct in_addr temp_addr;
	
	temp_addr.s_addr = ip_n;

	if(ip_a != NULL)
	{
		strcpy(ip_a, inet_ntoa(temp_addr));
	}
	
	return ip_a;
}

char* my_inet_ntoa2(int ip_n)
{
	struct in_addr temp_addr;
	static char tempIP[20];
	
	temp_addr.s_addr = ip_n;

	snprintf(tempIP, 20, "%s", inet_ntoa(temp_addr));
	
	return tempIP;
}


int ConvertIpStr2IpInt( const char* ipstr, int* ipint )
{
	struct in_addr s;  						// IPv4��ַ�ṹ��
	inet_pton(AF_INET, ipstr, (void *)&s);
	*ipint = s.s_addr;
	return 0;
}

int ConvertIpInt2IpStr( int ipint, char* ipstr  )
{
	struct in_addr s;  						// IPv4��ַ�ṹ��

	s.s_addr = ipint;

	inet_ntop(AF_INET, (void *)&s, ipstr, 16);
		
	return 0;
}

int socket_bind_to_device(int sockfd, char *net_dev_name)
{
	int m_loop;
	struct ifreq ifr;

	if(net_dev_name == NULL)
	{
		return -1;
	}
	
	m_loop = 1;
	if( setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &m_loop, sizeof(m_loop)) == -1 )
	{
		eprintf("sockfd =%d, Setsockopt SOL_SOCKET::SO_REUSEADDR Failure!\n", sockfd);
		return -1;
	}
	
	memset(ifr.ifr_name, 0, IFNAMSIZ);
	sprintf(ifr.ifr_name, "%s", net_dev_name);
	if(setsockopt(sockfd, SOL_SOCKET, SO_BINDTODEVICE, (char *)&ifr, sizeof(struct ifreq)))
	{
		eprintf("sockfd =%d, Setsockopt SOL_SOCKET::SO_BINDTODEVICE Failure!\n", sockfd);
		return -1;
	}
	
	return 0;
}
int create_trs_udp_socket(char *net_dev_name)
{
	int m_loop;
	int sockfd = -1;
	struct ifreq ifr;
	struct sockaddr_in self_addr;

	sockfd = socket(AF_INET, SOCK_DGRAM, 0);

	m_loop = 1;
	if( setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &m_loop, sizeof(m_loop)) == -1 )
	{
		eprintf("Setsockopt SOL_SOCKET::SO_REUSEADDR Failure!\n");
		goto create_udp_socket_error;
	}
	
	memset(ifr.ifr_name, 0, IFNAMSIZ);
	sprintf(ifr.ifr_name, "%s", net_dev_name);
	if(setsockopt(sockfd, SOL_SOCKET, SO_BINDTODEVICE, (char *)&ifr, sizeof(struct ifreq)))
	{
		eprintf("Setsockopt SOL_SOCKET::SO_BINDTODEVICE Failure!\n");
		goto create_udp_socket_error;
	}
	
	//Bind Self Address Port
	memset(&self_addr, 0, sizeof(struct sockaddr_in)); 
	self_addr.sin_family = AF_INET; 
	self_addr.sin_addr.s_addr = INADDR_ANY;
	self_addr.sin_port = 0; 
	if(bind(sockfd, (struct sockaddr *)&self_addr, sizeof(struct sockaddr_in)) == -1)
	{     
		eprintf("bind call failure!msg:%s\n", strerror(errno));
		goto create_udp_socket_error;
	}

	return sockfd;
	
	create_udp_socket_error:
	if(sockfd != -1)
	{
		close(sockfd);
	}

	
	return -1;	
}


int create_rcv_udp_socket(char *net_dev_name, unsigned short prot, int block )
{
	int m_loop;
	int sockfd = -1;
	struct ifreq ifr;
	struct sockaddr_in self_addr;

	sockfd = socket(AF_INET, SOCK_DGRAM, 0);

	m_loop = 1;
	if( setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &m_loop, sizeof(m_loop)) == -1 )
	{
		eprintf("Setsockopt SOL_SOCKET::SO_REUSEADDR Failure!\n");
		goto create_udp_socket_error;
	}
	
	memset(ifr.ifr_name, 0, IFNAMSIZ);
	sprintf(ifr.ifr_name, "%s", net_dev_name);
	if(setsockopt(sockfd, SOL_SOCKET, SO_BINDTODEVICE, (char *)&ifr, sizeof(struct ifreq)))
	{
		eprintf("Setsockopt SOL_SOCKET::SO_BINDTODEVICE Failure!\n");
		goto create_udp_socket_error;
	}
	
	//Set Asynchronous IO
	if(set_block_io(sockfd, block))
	{
		eprintf("Set Asynchronous IO Failure!\n");
		goto create_udp_socket_error;
	}
	//Bind Self Address Port
	memset(&self_addr, 0, sizeof(struct sockaddr_in)); 
	self_addr.sin_family = AF_INET; 
	self_addr.sin_addr.s_addr = INADDR_ANY;
	self_addr.sin_port = htons(prot); 
	if(bind(sockfd, (struct sockaddr *)&self_addr, sizeof(struct sockaddr_in)) == -1)
	{     
		eprintf("bind call failure!msg:%s\n", strerror(errno));
		goto create_udp_socket_error;
	}
	
	//Dont Recv Loop Data
	#if 1
	m_loop = 1;
	if(setsockopt(sockfd, IPPROTO_IP, IP_MULTICAST_LOOP, &m_loop, sizeof(int)) == -1)
	{
		printf("Setsockopt IPPROTO_IP::IP_MULTICAST_LOOP Failure!\n");
		goto create_udp_socket_error;
	}
	#endif
	
 	return sockfd;
 	
create_udp_socket_error:
	if(sockfd != -1)
	{
		close(sockfd);
	}
	return -1;
}

int join_multicast_group(char *net_dev_name, int socket_fd, int mcg_addr)
{
	struct ifreq ifr;
	struct ip_mreq mreq;

	//Get Loacl IP Addr
	memset(ifr.ifr_name, 0, IFNAMSIZ);	
	sprintf(ifr.ifr_name,"%s", net_dev_name);
	if(ioctl(socket_fd, SIOCGIFADDR, &ifr) == -1)//ʹ�� SIOCGIFADDR ��ȡ�ӿڵ�ַ
	{
		printf("ioctl call failure!msg:%s\n", strerror(errno));
		goto add_multicast_group_error;
	}
	
	//Added The Multicast Group
	memset(&mreq, 0, sizeof(struct ip_mreq));
	mreq.imr_multiaddr.s_addr = mcg_addr; //inet_addr(mcg_addr);
	mreq.imr_interface.s_addr = htonl(inet_network(inet_ntoa(((struct sockaddr_in *)&ifr.ifr_addr)->sin_addr)));	// ������ַ����mcg_addr�鲥��
	if(setsockopt(socket_fd, IPPROTO_IP, IP_ADD_MEMBERSHIP, &mreq, sizeof(struct ip_mreq)) == -1)
	{
		printf("=============================Setsockopt IPPROTO_IP::IP_ADD_MEMBERSHIP Failure!==================================\n");
		goto add_multicast_group_error;
	}	
	//printf("============================Setsockopt IPPROTO_IP::IP_ADD_MEMBERSHIP Successful!====================================\n");
	
	return 0;
add_multicast_group_error:
	return -1;
}

int join_multicast_group_ip(int socket_fd, int mcg_addr, int addr)
{
	struct ip_mreq mreq;

	//Added The Multicast Group
	memset(&mreq, 0, sizeof(struct ip_mreq));
	mreq.imr_multiaddr.s_addr = mcg_addr; 	//inet_addr(mcg_addr);
	mreq.imr_interface.s_addr = addr;		// inet_addr(addr);
	if(setsockopt(socket_fd, IPPROTO_IP, IP_ADD_MEMBERSHIP, &mreq, sizeof(struct ip_mreq)) == -1)
	{
		printf("Setsockopt IPPROTO_IP::IP_ADD_MEMBERSHIP Failure!\n");
		return -1;
	}	
	return 0;
}

int leave_multicast_group(char *net_dev_name, int socket_fd, int mcg_addr)
{
	struct ifreq ifr;
	struct ip_mreq mreq;

	//Get Loacl IP Addr
	sprintf(ifr.ifr_name,"%s", net_dev_name);
	if(ioctl(socket_fd, SIOCGIFADDR, &ifr) == -1)
	{
		printf("ioctl call failure!msg:%s\n", strerror(errno));
		goto leave_multicast_group_error;
	}
	//Leave The Multicast Group
	memset(&mreq, 0, sizeof(struct ip_mreq));
	mreq.imr_multiaddr.s_addr = mcg_addr; //inet_addr(mcg_addr);
	mreq.imr_interface.s_addr = htonl(inet_network(inet_ntoa(((struct sockaddr_in *)&ifr.ifr_addr)->sin_addr)));
	if(setsockopt(socket_fd, IPPROTO_IP, IP_DROP_MEMBERSHIP, &mreq, sizeof(struct ip_mreq)) == -1)		// ������ַ�뿪mcg_addr�鲥��
	{
		printf("Setsockopt IPPROTO_IP::IP_DROP_MEMBERSHIP Failure!\n");
		goto leave_multicast_group_error;
	}
	//printf("Setsockopt IPPROTO_IP::IP_DROP_MEMBERSHIP ok!\n");
	
	return 0;
leave_multicast_group_error:
	return -1;
}

int send_comm_udp_data( int sock_fd, struct sockaddr_in sock_target_addr, char *data, int length)
{
	int ret;

	ret = sendto(sock_fd, data, length, 0, (struct sockaddr*)&sock_target_addr,sizeof(struct sockaddr_in));
	
	if( ret == -1 )
	{
		eprintf("can not send data from socket! errno:%d,means:%s\n",errno,strerror(errno));
		return -1;
	}
	
	printf("send one package: sock = %d, ip = 0x%x, port = %d....\n",sock_fd,htonl(sock_target_addr.sin_addr.s_addr),htons(sock_target_addr.sin_port));
	
	return ret;
}

// lzh_20210804_s
static msg_process g_fw_uart_call_back = NULL;
void register_fw_uart_callback(msg_process process)
{
	g_fw_uart_call_back = process;
}
// lzh_20210804_e



//
#include "md5.h"  

int FileMd5_Calculate(char *file_path,unsigned char *file_md5)
{
	MD5_CTX md5;  
	int file_length,i,read_len;
	unsigned char buff[512];
	
	FILE *pf = fopen(file_path,"rb");
	
	if(pf == NULL)
	{
		return -1;
	}
	
	fseek(pf,0,SEEK_END);
	file_length = ftell(pf);
	fseek(pf,0,SEEK_SET);
	
	MD5Init(&md5);
	
	i = 0;
	while(i<file_length)
	{
		read_len = fread(buff,1,sizeof(buff),pf);
		if(read_len <= 0 || read_len>sizeof(buff))
		{
			fclose(pf);
			return -1;
		}	

		MD5Update(&md5,buff,read_len);  
		i += read_len;
	}
	fclose(pf);
    	                
    	MD5Final(&md5,file_md5); 
		
	return 0;
}

int StringMd5_Calculate(char *str,unsigned char *file_md5)	//czn_20170922
{
	MD5_CTX md5;  
	int str_length,i,read_len;
	//unsigned char buff[512];
	
	if(str == NULL)
	{
		return -1;
	}

	str_length = strlen(str);
	
	
	MD5Init(&md5);
	
	i = 0;
	
	while(i<str_length)
	{
		read_len = (str_length - i) < 512 ? (str_length - i) : 512;
		MD5Update(&md5,str+i,read_len);  
		i += read_len;
	}
    	                
    MD5Final(&md5,file_md5); 
		
	return 0;
}
//czn_20161008_e

int PrintCurrentTime(int num)
{	
	struct timeval tv;	
	if( gettimeofday(&tv, 0) == -1 )		
		printf("clock_gettime call failure!msg:%s\n\r", strerror(errno));	
	else		
		printf("::cur time %d=%d.%d\n\r",num,tv.tv_sec,tv.tv_usec);	
	return 0;
}

unsigned long long time_since_last_call(unsigned long long last)
{	
	unsigned long long current = 0;
	struct timeval tv;	
	gettimeofday(&tv, 0);
	current = tv.tv_sec*1000000 + tv.tv_usec;	
	if( last == -1 )
		return current;
	else
		return (current-last);
}

int rtp_process_set_high_prio(void)
{
	struct sched_param param;
	int policy=SCHED_RR;
	memset(&param,0,sizeof(param));
	int result=0;
	int min_prio, max_prio;

	min_prio = sched_get_priority_min(policy);
	max_prio = sched_get_priority_max(policy);
	param.sched_priority=max_prio;
	if((result=pthread_setschedparam(pthread_self(),policy, &param))) 
	{
		if (result==EPERM)
		{
			/*
				The linux kernel has 
				sched_get_priority_max(SCHED_OTHER)=sched_get_priority_max(SCHED_OTHER)=0.
				As long as we can't use SCHED_RR or SCHED_FIFO, the only way to increase priority of a calling thread
				is to use setpriority().
			*/
			if (setpriority(PRIO_PROCESS,0,-20)==-1)
			{
				printf("setpriority() failed: %s, nevermind.",strerror(errno));
				result = -1;
			}
			else
			{
				printf("priority increased to maximum.");
			}
		}
		else
		{
			printf("Set pthread_setschedparam failed: %s",strerror(result));
			result = -1;
		}
	} 
	else 
	{
		printf("priority set to %s and value (%i)",policy==SCHED_FIFO ? "SCHED_FIFO" : "SCHED_RR", param.sched_priority);
	}
	return result;
}

int GetLocalMacByDevice(char* net_device_name, char* pMac ) 
{  
	int sock_get_mac;        
	struct ifreq ifr_mac;
	if( (sock_get_mac=socket(AF_INET, SOCK_STREAM, 0)) == -1)
	{
		return -1;  
	}      
	memset(&ifr_mac,0,sizeof(ifr_mac));     
	strncpy( ifr_mac.ifr_name, net_device_name, sizeof(ifr_mac.ifr_name)-1);   
	if( ioctl( sock_get_mac, SIOCGIFHWADDR, &ifr_mac) < 0) 
	{  
		close(sock_get_mac);
	    return -1;
	}  	 
	pMac[0] = (char)ifr_mac.ifr_hwaddr.sa_data[0];  
	pMac[1] = (char)ifr_mac.ifr_hwaddr.sa_data[1];  
	pMac[2] = (char)ifr_mac.ifr_hwaddr.sa_data[2];  
	pMac[3] = (char)ifr_mac.ifr_hwaddr.sa_data[3];  
	pMac[4] = (char)ifr_mac.ifr_hwaddr.sa_data[4];  
	pMac[5] = (char)ifr_mac.ifr_hwaddr.sa_data[5];  

	close(sock_get_mac);
	return 0;
}  

int mypopen2(const char* type, FILE* fp[2], cJSON* cmd)
{
	//mypopen ��������ֵ����0����shell����Ľ���ID,���ظ�ֵ�������ִ���
	int i;
	int pfd[2];
	int pfderr[2];
	pid_t pid;
	int Ret;
	int arraySize = cJSON_GetArraySize(cmd);

	char** arg;

	arg = malloc((arraySize + 1) * sizeof(char*));

	//��׼��������ܵ��ļ�����
	if(pipe(pfd)<0) return -1;

	if(pipe(pfderr)<0)
	{
		close(pfd[0]);
		close(pfd[1]);
		return -1;
	}

	if((pid = fork()) < 0)
	{
		return -1;
	}
	else if(pid == 0)
	{
		if(*type == 'r')
		{
			if(pfd[1] != STDOUT_FILENO)
			{
				Ret = dup2(pfd[1], STDOUT_FILENO);
			}

			if(pfderr[1] != STDERR_FILENO)
			{
				Ret = dup2(pfderr[1], STDERR_FILENO);
			}
			
		}
		else
		{
			if(pfd[0] != STDIN_FILENO)
			{
				Ret = dup2(pfd[0], STDIN_FILENO);
			}

			if(pfderr[0] != STDERR_FILENO)
			{
				Ret = dup2(pfderr[0], STDERR_FILENO);
			}
		}

		for(i = 0; i < arraySize; i++)
		{
			arg[i] = cJSON_GetStringValue(cJSON_GetArrayItem(cmd, i));
		}
		arg[i] = NULL;

		execvp(arg[0], arg);
		free(arg);

		_exit(0);
	}
	else
	{
		if(*type == 'r')
		{
			close(pfd[1]);
			close(pfderr[1]);
			if((fp[0] = fdopen(pfd[0], type)) == NULL)
			{
				close(pfd[0]);
				close(pfderr[0]);
				return -1;
			}

			if((fp[1] = fdopen(pfderr[0], type)) == NULL)
			{
				close(fp[0]);
				close(pfd[0]);
				close(pfderr[0]);
				return -1;
			}
		}
		else
		{
			close(pfd[0]);
			close(pfderr[0]);
			if((fp[0] = fdopen(pfd[1], type)) == NULL)
			{
				close(pfd[1]);
				close(pfderr[1]);
				return -1;
			}

			if((fp[1] = fdopen(pfderr[1], type)) == NULL)
			{
				close(fp[0]);
				close(pfd[1]);
				close(pfderr[1]);
				return -1;
			}
		}
	}

	return pid;
}

int mypopen(const char* type, FILE* fp[2], char * execPro, ... )
{
	#define OPT_MAX 	10
	int i, ret;
	va_list IpArgs;
	char* arg[OPT_MAX+1] = {NULL};
    cJSON * cmdJson = cJSON_CreateArray();

	va_start(IpArgs, execPro);
	for(i = 0; i < OPT_MAX+1; i++)
	{
		if(i == 0)
		{
			arg[i] = execPro;
		}
		else
		{
			arg[i] = va_arg(IpArgs, char*);
		}
		
		if(arg[i] == NULL)
		{
			break;
		}
		cJSON_AddItemToArray(cmdJson, cJSON_CreateString(arg[i]));
	}
	va_end(IpArgs);

	ret = mypopen2(type, fp, cmdJson);

	cJSON_Delete(cmdJson);

	return ret;
}


int myfread(FILE* fp[2], void* RetVal, int RetSize, int TimeOut)
{
	int size, Ret, RecvSize, MaxFD, RecvSize1;
	int Flag[2] ={0, 0};
	struct timeval tv;
	fd_set fdR;

	MaxFD = (fileno(fp[0]) > fileno(fp[1]) ? fileno(fp[0]) : fileno(fp[1]));
	int i;
	size = 0;
	while(size < RetSize)
	{
		//bprintf("111111 size=%d\n",size);
		memset(&tv, 0, sizeof(tv));
		tv.tv_sec = TimeOut;
		tv.tv_usec = 0;
		FD_ZERO(&fdR);
		FD_SET(fileno(fp[0]), &fdR);
		FD_SET(fileno(fp[1]), &fdR);
		
		Ret = select(MaxFD + 1, &fdR, NULL, NULL, &tv);
		if(Ret <= 0)
		{
			if(size)
			{
				//bprintf("22222 size=%d,%s\n",size,(char*)RetVal);
				break;
			}
			else
			{
				return -1;
			}
		}
		else if(Ret > 0)
		{
			if(FD_ISSET(fileno(fp[0]), &fdR))
			{
				RecvSize = read(fileno(fp[0]), RetVal + size, RetSize - size);
				if(RecvSize > 0)
				{
					size += RecvSize;
				}
				else if(RecvSize < 0)
				{
					return -1;
				}
			}

			if(FD_ISSET(fileno(fp[1]), &fdR))
			{
				RecvSize1 = read(fileno(fp[1]), RetVal + size, RetSize - size);
				if(RecvSize1 > 0)
				{
					size += RecvSize1;
				}
				else if(RecvSize1 < 0)
				{
					return -1;
				}
			}
			
			if(!RecvSize && !RecvSize1)
			{
				break;
			}
		}
	}

	return size;
}

int myfread_oneline(FILE* fp[2], void* RetVal, int RetSize, int TimeOut)
{
	int size, Ret, RecvSize, MaxFD, RecvSize1;
	int Flag[2] ={0, 0};
	struct timeval tv;
	fd_set fdR;

	MaxFD = (fileno(fp[0]) > fileno(fp[1]) ? fileno(fp[0]) : fileno(fp[1]));
	int i;
	size = 0;
	while(size < RetSize)
	{
		//bprintf("111111 size=%d\n",size);
		memset(&tv, 0, sizeof(tv));
		tv.tv_sec = TimeOut;
		tv.tv_usec = 0;
		FD_ZERO(&fdR);
		FD_SET(fileno(fp[0]), &fdR);
		FD_SET(fileno(fp[1]), &fdR);
		
		Ret = select(MaxFD + 1, &fdR, NULL, NULL, &tv);
		if(Ret <= 0)
		{
			if(size)
			{
				//bprintf("22222 size=%d,%s\n",size,(char*)RetVal);
				break;
			}
			else
			{
				return -1;
			}
		}
		else if(Ret > 0)
		{
			if(FD_ISSET(fileno(fp[0]), &fdR))
			{
				RecvSize = read(fileno(fp[0]), RetVal + size, RetSize - size);
				if(RecvSize > 0)
				{
					size += RecvSize;
				}
				else if(RecvSize < 0)
				{
					return -1;
				}
			}

			if(FD_ISSET(fileno(fp[1]), &fdR))
			{
				RecvSize1 = read(fileno(fp[1]), RetVal + size, RetSize - size);
				if(RecvSize1 > 0)
				{
					size += RecvSize1;
				}
				else if(RecvSize1 < 0)
				{
					return -1;
				}
			}
			
			if(!RecvSize && !RecvSize1)
			{
				break;
			}
			if(strstr((char*)RetVal,"\n"))
				break;
		}
	}

	return size;
}
int mypclose(FILE* fp[2], int* processPid)
{
	int states;
	pid_t pid;
	pid_t pid_tmp;
	int ret;

	if(fclose(fp[0]) == EOF) return -1;
	if(fclose(fp[1]) == EOF) return -1;
	fp[0] = NULL;
	fp[1] = NULL;

	if((pid = *processPid) == 0) return -1;
	ret = waitpid(pid, &states, WNOHANG);
	if(ret == 0)
	{
		kill(pid, SIGKILL);
		do
		{
			pid_tmp = waitpid(pid, &states, 0);
		}while(pid_tmp == -1 && errno == EINTR);
	}

	*processPid = 0;

	return states;
}

int GetLocalIpByDevice(char* net_device_name)
{  
	char* pIP;
	int sock_get_ip;  
	int ip;
	struct   ifreq ifr_ip;   
	
	if(!CheckNetDevcieExist(net_device_name))
	{
		return -1; 
	}
	
	if ((sock_get_ip=socket(AF_INET, SOCK_STREAM, 0)) == -1) 
	{  
		eprintf("socket create failse...GetLocalIpByDevice,net_device_name=%s\n", net_device_name);  
		return -1;  
	}  
	memset(&ifr_ip, 0, sizeof(ifr_ip));
	strncpy(ifr_ip.ifr_name, net_device_name, sizeof(ifr_ip.ifr_name) - 1);     
	if( ioctl( sock_get_ip, SIOCGIFADDR, &ifr_ip) < 0 )     
	{     
		close( sock_get_ip ); 
		return -1;
	}       

	// �õ�ip��ַ���ַ���
	pIP = inet_ntoa(((struct sockaddr_in *)&ifr_ip.ifr_addr)->sin_addr);

	// �õ������ֽ���IP ��ַ
	ip = htonl(inet_network(pIP) );

	close( sock_get_ip ); 
	
	return ip; 
}  

#if 1
int GetLocalGatewayByDevice(char* net_device_name)
{
	int gw = -1;
	char cmd[200] = {0};
	char linestr[100] = {0};

	if(!net_device_name)
	{
		return gw;
	}
	
	FILE *pf;
	snprintf(cmd, 200, "route -n | grep 'UG' | grep '%s' | awk '{print $2}'", net_device_name);

	if(pf = popen(cmd, "r"))
	{
		while(fgets(linestr, 100, pf))
		{
			gw = inet_addr(linestr);
		}
		pclose(pf);
	}

	return gw;
}

int DelDefaultGatewayByDevice(char* net_device_name)
{
	FILE *pf;
	char cmd[200] = {0};
	char linestr[100] = {0};
	int ret = -1;
	int intGw;

	if(!net_device_name)
	{
		return ret;
	}

	if((intGw = GetLocalGatewayByDevice(net_device_name)) != -1)
	{
		snprintf(cmd, 200, "route del default gw %s", my_inet_ntoa2(intGw));
		if(pf = popen(cmd, "r"))
		{
			pclose(pf);
		}

		if(pf = popen(cmd, "r"))
		{
			while(fgets(linestr, 100, pf))
			{
				if(strstr(linestr, "No such process"))
				{
					ret = 0;
					break;
				}
			}
			pclose(pf);
		}
	}

	return ret;
}

int SetDefaultGatewayByDevice(char* net_device_name, char* gw)
{
	FILE *pf;
	char linestr[100] = {0};
	char cmd[200] = {0};
	int ret = -1;
	int intGw;

	if(!net_device_name || !gw)
	{
		return ret;
	}

	snprintf(cmd, 200, "route add default gw %s %s", gw, net_device_name);
	if(pf = popen(cmd, "r"))
	{
		pclose(pf);
	}

	if(pf = popen(cmd, "r"))
	{
		while(fgets(linestr, 100, pf))
		{
			if(strstr(linestr, "File exists"))
			{
				ret = 0;
				break;
			}
		}
		pclose(pf);
	}

	return ret;
}

/*
int MyKillTask(char* task_name)
{
	int account, timeCnt;
	int pid;
	int states;

	for(account = 0, timeCnt = 0; timeCnt < 3; timeCnt++)
	{
		pid = getpid_bycmdline(task_name);
		if(pid > 0)
		{
			if(waitpid(pid, &states, WNOHANG) == 0)
			{
				if(kill(pid, kill(pid, SIGTERM)) == 0)
				{
					dprintf("waiting killed %d\n", pid);
					while(waitpid(pid, &states, 0) == -1 && errno == EINTR);
					account++;
					dprintf("kill %d\n", pid);
				}
			}
		}
		else if(pid == 0)
		{
			usleep(10*1000);
		}
		else if(pid < 0)
		{
			break;
		}
	}
	
	return account;
}
*/
/*
����˵����
ping -c 3 -p 1 -s 8 -i 0.2 -W 1 163.177.151.109

-c 3: ����3��ping����

-p 1: ����TTLֵΪ1��

-s 8: ���Ͱ��Ĵ�С����Ϊ8�ֽڣ��Լ��������豸�������ӵĶ��⿪����

-i 0.2: ����ping����ļ��Ϊ0.2�롣

-W 1: ���ó�ʱʱ��Ϊ1�롣
*/
/*
int PingNetwork2(char* ipString, float *pingtime)
{
	FILE *pf;
	char linestr[500] = {0};
	char cmd[200] = {0};
	int ret = -1;

	if(!ipString)
	{
		return ret;
	}
	
	snprintf(cmd, 200, "ping -c 1 -s 8 -i 0.2 -W 1 %s > %s/%s.txt\n", ipString, TEMP_Folder, ipString);
	dprintf("ping %s result=%d\n", ipString, ret);
	system(cmd);
	dprintf("ping %s result=%d\n", ipString, ret);
	snprintf(cmd, 200, "%s/%s.txt", TEMP_Folder, ipString);
	if(pf = fopen(cmd,"r"))
	{
		while(fgets(linestr, 500, pf))
		{
			char *pch1,*pch2;
			if(pch1 = strstr(linestr, "time="))
			{
				ret = 0;
				if(pingtime)
				{
					pch1 += strlen("time=");
					if(pch2 = strstr(pch1, " ms"))
					{
						pch2[0] = 0;
						*pingtime = atof(pch1);
					}
				}
				break;
			}
		}
		fclose(pf);
	}
	FileChmod(cmd, "777");
	DeleteFileProcess(NULL, cmd);
	//snprintf(cmd, 200, "ping -c 1 -s 8 -i 0.2 -W 1 %s", ipString);
	//MyKillTask(cmd);
	dprintf("ping %s result=%d\n", ipString, ret);
	return ret;
}

int PingNetwork(char* ipString)
{
	return PingNetwork2(ipString, NULL);
}
*/

#else
int GetLocalGatewayByDevice(char* net_device_name)
{
	FILE* stream[2];
	int gw = -1;
	char readBuffer[1024] = {0};
	char temp2[128];
	char gwChar[100];
	char* pos1;
	int pid;

	pid = mypopen("r", stream, "route", "-n", NULL);
	if(pid > 0)
	{
		if(myfread(stream, readBuffer, 1023, 1) == -1)
		{
			//log_e("%s:myfread error!\n",__func__);
		}
		else
		{
			strtok(readBuffer,"\n");
			while(pos1 = strtok(NULL,"\n"))
			{
				if(strstr(pos1, "UG") && strstr(pos1, net_device_name))
				{
					sscanf(pos1, "%*s%s", temp2);
					sscanf(temp2, "%[^ ]", gwChar);
					gw = inet_addr(gwChar);
					break;
				}
			}
		}
	}
	mypclose(stream, &pid);

	return gw;
}

int DelDefaultGatewayByDevice(char* net_device_name)
{
	FILE* stream[2];
	char readBuffer[1024] = {0};
	int pid;
	int ret = -1;
	char gw[20] = {0};
	int intGw;

	if((intGw = GetLocalGatewayByDevice(net_device_name)) != -1)
	{
		my_inet_ntoa(intGw, gw);
		
		pid = mypopen("r", stream, "route", "del", "default", "gw", gw, NULL);
		mypclose(stream, &pid);

		pid = mypopen("r", stream, "route", "del", "default", "gw", gw, NULL);
		if(pid > 0)
		{
			if(myfread(stream, readBuffer, 1023, 1) == -1)
			{
				//log_e("%s:myfread error!\n",__func__);
			}
			else
			{
				if(strstr(readBuffer, "No such process"))
				{
					ret = 0;
				}
			}
		}
		mypclose(stream, &pid);
	}

	return ret;
}

int SetDefaultGatewayByDevice(char* net_device_name, char* gw)
{
	FILE* stream[2];
	char readBuffer[1024] = {0};
	int pid;
	int ret = -1;
	
	pid = mypopen("r", stream, "route", "add", "default", "gw", gw, net_device_name, NULL);
	
	mypclose(stream, &pid);

	pid = mypopen("r", stream, "route", "add", "default", "gw", gw, net_device_name, NULL);
	if(pid > 0)
	{
		if(myfread(stream, readBuffer, 1023, 1) == -1)
		{
			//log_e("%s:myfread error!\n",__func__);
		}
		else
		{
			if(strstr(readBuffer, "File exists"))
			{
				ret = 0;
			}
		}
	}
	mypclose(stream, &pid);

	return ret;
}
#endif

int PingNetwork2(char* ipString,float *pingtime)	//20181017
{
	FILE* stream[2];
	int ret = -1;
	char readBuffer[1024] = {0};
	char temp2[128];
	char gwChar[100];
	char* pos1;
	int pid;

	if(!ipString)
	{
		return ret;
	}

	pid = mypopen("r", stream, "ping", "-c", "2", "-s", "8", "-i", "0.2", "-W", "1", ipString, NULL);
	if(pid > 0)
	{
		if(myfread(stream, readBuffer, 1023, 1) == -1)
		{
			//log_e("%s:myfread error!\n",__func__);
		}
		else
		{
			char *pch1,*pch2;
			if(pch1 = strstr(readBuffer, "time="))
			{
				ret = 0;
				if(pingtime)
				{
					pch1  += strlen("time=");
					if(pch2 = strstr(pch1, " ms"))
					{
						pch2[0] = 0;
						*pingtime = atof(pch1);
					}
				}
			}
		}
	}
	mypclose(stream, &pid);
	
	//dprintf("ret = %d, readBuffer = %s\n", ret, readBuffer);

	return ret;
}

int PingNetwork(char* ipString)
{
	return PingNetwork2(ipString, NULL);
}

int GetLocalMaskAddrByDevice(char* net_device_name)
{
	char* pIP;
	int sock_get_ip;  
	int ip = -1;
	struct   ifreq ifr_ip;     	
	if ((sock_get_ip=socket(AF_INET, SOCK_STREAM, 0)) == -1) 
	{  
		log_e("socket create failse...GetLocalMaskAddrByDevice, net_device_name=%s\n", net_device_name);  
		return -1;  
	}  
	memset(&ifr_ip, 0, sizeof(ifr_ip));
	strncpy(ifr_ip.ifr_name, net_device_name, sizeof(ifr_ip.ifr_name) - 1);     
	if( ioctl( sock_get_ip, SIOCGIFNETMASK, &ifr_ip) < 0 )     
	{     
		close( sock_get_ip ); 
		return -1;
	}       

	// �õ�ip��ַ���ַ���
	pIP = inet_ntoa(((struct sockaddr_in *)&ifr_ip.ifr_addr)->sin_addr);

	// �õ������ֽ���IP ��ַ
	ip = htonl(inet_network(pIP) );

	close( sock_get_ip ); 
	
	return ip; 
}

int getpid_bycmdline(char* cmdline)
{
	char cmd[200];
	char pid[50];
	char user[50];
	int ret = -1;

	snprintf(cmd,200,"ps |grep '%s' |grep -v 'grep'",cmdline);
	FILE *pgetpid = popen(cmd,"r");
	if(pgetpid == NULL)
		return ret;
	if(fgets(cmd,200,pgetpid) == NULL)
	{
		pclose(pgetpid);
		return ret;
	}
	pclose(pgetpid);
	dprintf("1111:%s\n", cmd);
	sscanf(cmd, "%s %s", pid, user);
	if(!strcmp(user, "root"))
	{
		ret = atol(pid);
	}
	else if(!strcmp(user, "Done"))
	{
		ret = 0;
	}

	dprintf("getpid_bycmdline pid:%d\n", ret);
	return ret;
}

float CPU_Ratio=0.0;
void CpuRatioDisplay(void);
void CPU_Ratio_Measure(void)
{
	static uint32 last_total=0;
	static uint32 last_user=0;
	uint32 cur_total=0;
	uint32 cur_user=0;
	static uint32 timer=0;
	float total_diff;
	float user_diff;
	uint32 cpu_time[20];
	int time_cnt=0;
	char *strok_buff;
	char *pos1;
	char *pos2;
	//if(++timer<=10)
	//	return;
	//timer=0;
	char buff[500];
	FILE *pf=fopen("/proc/stat","r");
	if(pf==NULL)
	{
		printf("can't open stat file\n");
		return;
	}
	//GetMemAvailable();
	while(fgets(buff,500,pf)!=NULL)
	{
		if((pos1=strstr(buff,"cpu"))!=NULL)
		{
			pos2 = strtok_r(pos1+4," \t\n\r",&strok_buff);
			while(pos2!=NULL)
			{
				if(strcmp(pos2," \t\n\r")!=0)
				{
					cpu_time[time_cnt]=atoi(pos2);
					//printf("time %d=%d\n",time_cnt,cpu_time[time_cnt]);
					cur_total+=cpu_time[time_cnt];
					if(time_cnt<3)
						cur_user+=cpu_time[time_cnt];
					time_cnt++;
				}
				pos2 = strtok_r(NULL," \t\n\r",&strok_buff);
			}
			
			break;
		}
	}
	fclose(pf);
	if(time_cnt>3)
	{
		total_diff=(float)(cur_total-last_total);
		user_diff=(float)(cur_user-last_user);
		CPU_Ratio=user_diff/total_diff;
		printf("CPU_Ratio=%.02f\n",CPU_Ratio*100.0);
		last_total=cur_total;
		last_user=cur_user;
	}
	//CpuRatioDisplay();
}
float GetCPURatio(void)
{
	return CPU_Ratio*100.0;
}
int GetMemAvailable(void)
{
	char buff[500];
	char *strok_buff;
	char *pos1;
	char *pos2;
	int MemAvailable;
	FILE *pf=fopen("/proc/meminfo","r");
	if(pf==NULL)
	{
		log_e("can't open memifo file\n");
		return;
	}
	while(fgets(buff,500,pf)!=NULL)
	{
		if((pos1=strstr(buff,"MemAvailable:"))!=NULL)
		{
			pos2 = strtok_r(pos1+strlen("MemAvailable:")," \t\n\r",&strok_buff);
			if(pos2!=NULL)
			{
				MemAvailable=atoi(pos2);
				//printf("MemAvailable:%d kb\n",MemAvailable);
			}
			
			break;
		}
	}
	fclose(pf);

	if(MemAvailable<13000)
		log_w("MemAvailable only:%d kb",MemAvailable);
	return MemAvailable;
}

 int create_multi_dir(const char *path)            //lyx 20170714
{
         int i, len;

	char dir_path[200];
       len = strlen(path);
		 
	memcpy(dir_path, path,len);
	
	if(path[len-1]!='/')
	{
       	  dir_path[len+1] = '\0';
		dir_path[len]='/';
		len++;
	}
	else
		dir_path[len] = '\0';
	
         

         for (i=0; i<len; i++)
         {
                 if (dir_path[i] == '/' && i > 0)
                 {
                         dir_path[i]='\0';
                         if (access(dir_path, F_OK) < 0)
                         {
                                 if (mkdir(dir_path, 750) < 0)
                                 {
                                         printf("mkdir=%s:msg=%s\n", dir_path, strerror(errno));
                                         return -1;
                                 }
                         }
                         dir_path[i]='/';
                 }
         }

         return 0;
}

NetWork_T GetNetworkDevcies(void)
{
	FILE *pf = NULL;
	char linestr[100];

	NetWork_T network;

	network.devCnt = 0;
	if(pf=popen("/sbin/ifconfig | grep 'encap:Ethernet' | awk '{print $1}'", "r"))
	{
		while(fgets( linestr, 100, pf))
		{
			if(network.devCnt < 5)
			{
				linestr[strcspn(linestr, "\n")] = 0;
				snprintf(network.devName[network.devCnt++], 10, "%s", linestr);
			}
			else
			{
				break;
			}
		}
		pclose(pf);
	}
	return network;
}


char* GetCurrentTime(char* buffer, int len, const char* formate)
{
    time_t rawtime;
    struct tm* info;

    time(&rawtime);

    info = localtime(&rawtime);

    strftime(buffer, len, formate, info);

	//dprintf("buffer = %s\n", buffer);

    return buffer;
}

#include <sys/statfs.h>
int getDiscSize( char *path, int *freeSize, int *totalSize ) 
{
	 struct statfs myStatfs;
	 if( statfs(path, &myStatfs) == -1 ) 
		  return -1;
	 
	 *freeSize = (int)((((long long)myStatfs.f_bsize * (long long)myStatfs.f_bfree) / (long long) (1024*1024)));
	 *totalSize = (int)((((long long)myStatfs.f_bsize * (long long)myStatfs.f_blocks) /(long long) (1024*1024)));

	 return 0;
}
int getDiscFreeSize( char *path )
{
	int freeSize_ = 0;
	int totalSize_ = 0;

	if( getDiscSize(path, &freeSize_, &totalSize_ ) != 0 )
		return -1;

	//dprintf(" totalSize_ : %d    freeSize_: %d \n", totalSize_, freeSize_ );
		
	return freeSize_;
}

//IX_ADDRתGW_ID
int IX_ADDR_TO_GW_ID(char* ixAddr)
{
	int ret = 0;
	char temp[3] = {0};

	if(ixAddr && (strlen(ixAddr) == 10 || strlen(ixAddr) == 8))
	{
		memcpy(temp, ixAddr+4, 2);
		temp[3] = 0;
		ret = atoi(temp);
	}

	return ret;
}

//GW_IDתGW_ADDR
const char* GW_ID_TO_GW_ADDR(int gwId)
{
	static char gwAddr[11] = {0};
	if(gwId > 99 || gwId < 0) gwId = 0;

	snprintf(gwAddr, 11, "0099%02d0001", gwId);
	return gwAddr;
}

//GW_ID��IM_IDתIX_ADDR
const char* GW_ID_AND_IM_ID_TO_IX_ADDR(int gwId, int imId)
{
	static char ixAddr[11] = {0};
	if(gwId > 99 || gwId < 0) gwId = 0;
	if(imId > 99 || imId < 0) imId = 0;

	snprintf(ixAddr, 11, "0099%02d%02d01", gwId, imId);
	return ixAddr;
}