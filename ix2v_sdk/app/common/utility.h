
#ifndef _UTILITY_H_
#define _UTILITY_H_

#include <sys/types.h>
#include <sys/time.h>
#include <sys/ioctl.h>
#include <sys/stat.h>
#include <sys/socket.h>   
#include <sys/un.h>

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <unistd.h>
#include <error.h>
#include <signal.h>
#include <pthread.h>
#include <semaphore.h>
#include <sched.h>
#include <fcntl.h>
#include <errno.h>
#include <time.h> 
#include <netinet/in.h>    
#include <netinet/if_ether.h>   
#include <net/if.h>   
#include <net/if_arp.h>   
#include <arpa/inet.h>     

#include <assert.h>

#include <sys/timeb.h>
#include <sys/mman.h>
#include <sys/ipc.h>
#include <sys/resource.h>
#include <unistd.h>
#include <dirent.h>

//#include <execinfo.h>

#include "RTOS.h"
#include "OSQ.h"
#include "OSTIME.h"
#include "define_string.h"

//#define VDP_PRINTF_ERROR

//#define	VDP_PRINTF_DEBUG
//#define VDP_PRINTF_BUSINESS
//#define VDP_PRINTF_LOGFILE

#ifdef	VDP_PRINTF_ERROR
#define	eprintf(fmt,...)	printf("[E]-["__FILE__"]-[%04d]-"fmt"",__LINE__,##__VA_ARGS__)
#else
#define	eprintf(fmt,...)
#endif

#ifdef	VDP_PRINTF_DEBUG
#define	dprintf(fmt,...)	printf("[D]-["__FILE__"]-[%04d]-"fmt"",__LINE__,##__VA_ARGS__)
#else
#define	dprintf(fmt,...)
#endif

#ifdef	VDP_PRINTF_BUSINESS
#ifdef 	VDP_PRINTF_LOGFILE
#define	bprintf(fmt,...)	printf("[B]-["__FILE__"]-[%04d]-"fmt"",__LINE__,##__VA_ARGS__)  //PushBusinessMessage("[B]-["__FILE__"]-[%04d]-"fmt"",__LINE__,##__VA_ARGS__)
#else
#define	bprintf(fmt,...)	printf("[B]-["__FILE__"]-[%04d]-"fmt"",__LINE__,##__VA_ARGS__)
#endif
#else
#define	bprintf(fmt,...)
#endif

#define CREATE_IP_ADDR(IP)					( ((int)IP_ADD0<<24)|((int)IP_ADD1<<16)|((int)IP_ADD2<<8)|IP )
#define COMBINING_IP_ADDR(IP1,IP2,IP3,IP4)	( ((int)IP1<<24)|((int)IP2<<16)|((int)IP3<<8)|IP4 )

//czn_20160827_s
#define RID0000_FIRMWIRE					0

#define RID1000_IO_PARAMETER				1000
#define RID1001_TB_ROOM					1001
#define RID1002_TB_GATEWAY				1002
#define RID1003_DS1_BTNMAP				1003
#define RID1004_DS2_BTNMAP				1004
#define RID1005_DS3_BTNMAP				1005
#define RID1006_DS4_BTNMAP				1006
#define RID1007_DS1_NAMELIST				1007
#define RID1008_DS2_NAMELIST				1008
#define RID1009_DS3_NAMELIST				1009
#define RID1010_DS4_NAMELIST				1010
#define RID1011_DSA_NAMELIST				1011
#define RID1012_IM_NAMELIST				1012
#define RID1013_IM_GL_MAP					1013
#define RID1014_MON_VRES_MAP				1014		//czn_20161216

//czn_20160827_e

//czn_20160708_s

typedef enum
{
	UpgradeIniKey_Unkown = 0,
	UpgradeIniKey_DeviceType,
	UpgradeIniKey_FwVer,
	UpgradeIniKey_FwParser,
	UpgradeIniKey_FwReset,
	UpgradeIniKey_Stm32,
	UpgradeIniKey_End,
}UpgradeIniKey_Type;

typedef struct
{
	char		fwname[40];
	char		update_path[40*2];
}N329fw_Stru;

typedef struct
{
	char	devName[5][10];
	int		devCnt;
}NetWork_T;

typedef struct
{
	char			device_type[20];
	char			fw_ver[20];
	int				n329fw_cnt;
	N329fw_Stru	*	n329fw_Items;
	int				reset_enble;
	int				have_stm32fw;
	char			stm32fwname[40];
	int				stm32fw_update_enble;
}UpgradeIni_Parser_Stru;

//czn_20160708_e

/*******************************************************************************************
ͨ�ö������ݽṹ
*******************************************************************************************/
typedef void (*msg_process)(void*,int);

typedef unsigned char* p_vdp_common_buffer;

#define COMMON_RESPONSE_BIT		(0x80)

typedef struct Loop_vdp_common_buffer_tag
{
	OS_Q 					embosQ;
	int 					QSize;
	unsigned char*			pQBuf;
	msg_process 			process;					// ���ݴ�������
	void*					powner;
} Loop_vdp_common_buffer, *p_Loop_vdp_common_buffer;

int init_vdp_common_queue(p_Loop_vdp_common_buffer pobj, int qsize, msg_process process, void* powner );
int exit_vdp_common_queue( p_Loop_vdp_common_buffer pobj );
int push_vdp_common_queue( p_Loop_vdp_common_buffer pobj, char *data, unsigned int length);		//czn_20170712
int pop_vdp_common_queue(p_Loop_vdp_common_buffer pobj, p_vdp_common_buffer* pdb, int timeout);
int purge_vdp_common_queue(p_Loop_vdp_common_buffer pobj);

typedef struct
{
	int								task_id;
	char*							task_name;
	p_Loop_vdp_common_buffer		p_msg_buf;
	p_Loop_vdp_common_buffer		p_syc_buf;	
	int 							task_run_flag;		// �����߳����б�־
	pthread_t						task_pid;			// �����߳�����id	
	int 							task_StartCompleted;		//任务启动完成
} vdp_task_t;

int init_vdp_common_task( vdp_task_t* ptask, int task_id, void* (*pthread)(void*), p_Loop_vdp_common_buffer msg_buf, p_Loop_vdp_common_buffer syc_buf );
int exit_vdp_common_task( vdp_task_t* ptask );
void* vdp_public_task( void* arg );

/////////////////////////////////////////////////////////////////////////////////////////////////////////
int select_ex(int, fd_set *, fd_set *, fd_set *, struct timeval *);
int ioctlex(int, int, void *);
int sem_wait_ex(sem_t *p_sem, int semto);
int sem_wait_ex2(sem_t *p_sem, int semto);
int set_block_io(int fd, int is_block);

int PrintCurrentTime(int num);
unsigned long long time_since_last_call(unsigned long long last);

// common 
int get_format_time(char *tstr);
int check_ip_repeat( int ip );
int ConvertIpStr2IpInt( const char* ipstr, int* ipint );
int ConvertIpInt2IpStr( int ipint, char* ipstr	);

// socket serial

int create_trs_udp_socket(char *net_dev_name);
int create_rcv_udp_socket(char *net_dev_name, unsigned short prot, int block );

int join_multicast_group(char *net_dev_name, int socket_fd, int mcg_addr);
int join_multicast_group_ip(int socket_fd, int mcg_addr, int addr);
int leave_multicast_group(char *net_dev_name, int socket_fd, int mcg_addr);


int send_comm_udp_data( int sock_fd, struct sockaddr_in sock_target_addr, char *data, int length);

// lzh_20210804_s
void register_fw_uart_callback(msg_process process);
// lzh_20210804_e

int rtp_process_set_high_prio(void);

int GetLocalIpByDevice(char* net_device_name);

NetWork_T GetNetworkDevcies(void);
char* my_inet_ntoa2(int ip_n);
char* my_inet_ntoa(int ip_n, char* ip_a);
char* GetCurrentTime(char* buffer, int len, const char* formate);
//IX_ADDR转GW_ID
int IX_ADDR_TO_GW_ID(char* ixAddr);
//GW_ID转IX_ADDR
const char* GW_ID_TO_GW_ADDR(int gwId);
//GW_ID和IM_ID转IX_ADDR
const char* GW_ID_AND_IM_ID_TO_IX_ADDR(int gwId, int imId);

#endif


