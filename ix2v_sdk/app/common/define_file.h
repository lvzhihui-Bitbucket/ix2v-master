/**
  ******************************************************************************
  * @file    define_file.h
  * @author  jia
  * @version V1.0.0
  * @date    2012.06.01
  * @brief   This file contains the define of GPIO use.
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2012 V-Tec</center></h2>
  ******************************************************************************
  */ 

#ifndef _define_file_H_
#define _define_file_H_

#define LOCAL_AUTOIP_SERVICE_FILE 						"/tmp/autoip_server_socket"

#define RING_Folder   									"/usr/rings/"
#define RING_DEFAULT_CFG_FILE   						"/usr/rings/default.txt"
#define WIFI_PATH 									    "/usr/wifi/"
#define MCU_SIMULATE_FLASH_PATH 				"/usr/MCU_SimulateFlash.bin"

#define INSTALLER_BAK_CONFIG_FILE						"/mnt/nand1-1/App/res/backup0.cfg"
#define UPDATE_BAK_CONFIG_FILE							"/mnt/nand1-1/App/res/updatebackup.cfg"
#define IO_DATA_DEFAULT_FILE_NAME						"/mnt/nand1-1/App/res/io_data_default_table.csv"
#define EXT_MODULE_FILE_NAME							"/mnt/nand1-1/App/res/ExtModule.cfg"
#define PARA_MENU_FILE_NAME								"/mnt/nand1-1/App/res/ParaMenu_table.json"
#define SIP_CONFIG_DEFAULT_FILE_NAME					"/mnt/nand1-1/App/res/sipcfg_default.cfg"
#define	SwUpgradeIni									"/mnt/nand1-1/App/res/SwUpgrade.ini"
#define 	IXGIMEditCheckFile								"/mnt/nand1-1/App/res/IXGIM_Edit_Check.json"
#define 	IXGIMEditTransFile								"/mnt/nand1-1/App/res/IXGIM_Edit_Translate.json"
#define 	IXGDFRMKeyFuncTbPath						"/mnt/nand1-1/App/res/IXGDFRMKeyFuncTb.json"
#define   ResLanguageNandFolder					  "/mnt/nand1-1/App/res/language/"
#define   KEYBORD_LANGUAGE_PATH           "/mnt/nand1-1/App/res/KeyboardLanguages"

#define ROUTE_TEMP										"/tmp/route.sh"
#define AUTO_IP_DHCP_CLIENT_PATH						"/mnt/nand1-1/App/dhcpclient"
#define FTP_CLIENT_PATH									"/mnt/nand1-1/App/ftpclient"

#define MENU_FILE_PATH									"/mnt/nand1-1/Drv/res/"
#define MENU_V_FILE_PATH								"/mnt/nand1-1/Drv/resV/"
#define EXTEND_FONT1_NAME								"/mnt/nand1-1/Drv/res/unicode_font1.ttf"
#define INNER_FONT_NAME									"/mnt/nand1-1/Drv/res/wryh1624.bin"
#define JPEG_BACKGROUND									"/mnt/nand1-1/Drv/res/background.jpg"
#define MENU_LANG_FILE_PATH								"/mnt/nand1-1/Drv/res/"
#define MENU_LANG_MESG_FILE								"/mnt/nand1-1/Drv/res/menu_lang_mesg.def"

#define CustomerFileDir									"/mnt/nand1-2/Customerized/"
#define CustomerizedPromtDir								"/mnt/nand1-2/Customerized/prompt/"
#define CustomerizedLanguageNandFolder					"/mnt/nand1-2/Customerized/language/"
#define INSTALLER_BAK_Customerized_FILE					"/mnt/nand1-2/Customerized/Installer/backup0.cfg"
#define CUSTOMERIAED_IO_TABLE							"/mnt/nand1-2/Customerized/para/customerized_io_data_table.csv"
#define CUS_SETTING_PAGE_DIR		            "/mnt/nand1-2/Customerized/SETTING-PAGE/"
// lzh_20210909_s
#define MENU_FILE_PATH_CUSTOM							"/mnt/nand1-2/Customerized/UI/CustomizedOutput/"
#define MENU_V_FILE_PATH_CUSTOM							"/mnt/nand1-2/Customerized/UI/CustomizedOutputV/"
// lzh_20210909_e
#define CustomerizedSoundDir							"/mnt/nand1-2/Customerized/sound/"

#define INSTALLER_BAK_PATH								"/mnt/nand1-2/Backup/"
#define INSTALLER_FILE_BAK1								"/mnt/nand1-2/Backup/backup1.bak"
#define INSTALLER_FILE_BAK2								"/mnt/nand1-2/Backup/backup2.bak"
#define Backup_HYBRID_MAT_Folder				  "/mnt/nand1-2/Backup/HYBRID_MAT/"
#define Backup_HYBRID_SAT_Folder				  "/mnt/nand1-2/Backup/HYBRID_SAT/"
#define Backup_APT_MAT_Folder				      "/mnt/nand1-2/Backup/APT_MAT/"
#define NAND_Backup_Path				      "/mnt/nand1-2/Backup/"
#define Backup_VM_MAT_Folder				      "/mnt/nand1-2/Backup/VM_MAT/"
	

#define SettingFolder									"/mnt/nand1-2/Settings/"

#define PublicInfo_File_Name						"/mnt/nand1-1/App/res/PublicInformation.json"
#define SettingCode_File_Name						"/mnt/nand1-1/App/res/SettingCode.json"

#define IO_PARA_Path				            "/mnt/nand1-1/App/res/"
#define IO_PARA_Public_File_Name				"/mnt/nand1-1/App/res/IO_PARA_Public.json"
#define IO_Trim_FILE_NAME								"_PARA_Trim.json"
#define IO_ID_FILE_NAME					        "_PARA_ID.json"
#define IO_Check_FILE_NAME							"_PARA_Check.json"
#define IO_OLD_ID_TO_VALUE_FILE_NAME		"_OldIDtoValue.json"
#define SETTING_PAGE_DIR		            "/mnt/nand1-1/App/res/SETTING-PAGE/"
#define Factory_HYBRID_MAT_Folder				"/mnt/nand1-1/App/res/HYBRID_MAT/"
#define Factory_HYBRID_SAT_Folder		    "/mnt/nand1-1/App/res/HYBRID_SAT/"
#define Factory_APT_MAT_Folder					"/mnt/nand1-1/App/res/APT_MAT/"
#define Factory_VM_MAT_Folder					"/mnt/nand1-1/App/res/VM_MAT/"

#define IO_Cus_Path							        "/mnt/nand1-2/Customerized/para/"
#define IO_Cus_FILE_NAME								"_PARA_Cus.json"
#define PrivatePwd_FILE_NAME		        "_PrivatePassword.json"
#define Customerized_HYBRID_MAT_Folder				    "/mnt/nand1-2/Customerized/HYBRID_MAT/"
#define Customerized_HYBRID_SAT_Folder				    "/mnt/nand1-2/Customerized/HYBRID_SAT/"
#define Customerized_APT_MAT_Folder				        "/mnt/nand1-2/Customerized/APT_MAT/"
#define Customerized_VM_MAT_Folder				        "/mnt/nand1-2/Customerized/VM_MAT/"
#define IO_Factory_FILE_NAME								"/mnt/nand1-2/Customerized/para/IO_PARA_Factory.json"

#define IO_DATA_VALUE_NAME								"/mnt/nand1-2/Settings/io_data_value.json"
#define SIP_CONFIG_FILE_NAME							"/mnt/nand1-2/Settings/sipcfg.cfg"
#define	WIFI_SSID_FILE_NAME								"/mnt/nand1-2/Settings/wifi_ssid.cfg"

#define MS_LIST_TABLE_NAME								"/mnt/nand1-2/Settings/MS_ListTable.csv"
#define IM_NAME_LIST_TABLE_NAME							"/mnt/nand1-2/Settings/ImNameListTable.csv"
#define DEVICE_REGISTER_TABLE_NAME						"/mnt/nand1-2/Settings/DeviceRegisterTable.csv"
#define CALL_CONTROL_MAP_TABLE_NAME						"/mnt/nand1-2/Settings/CallControlTable.csv"
#define DEVICE_MAP_TABLE_NAME							"/mnt/nand1-2/Settings/NewDeviceMapTable.csv"
#define WLAN_IPC_MON_LIST_FILE_NAME						"/mnt/nand1-2/Settings/WLAN_IPC_MonRes_table.csv"
#define EXT_MODULE_KEY_TABLE_FILE_NAME					"/mnt/nand1-2/Settings/ExtModuleKeyTable.csv"
#define VIDEO_PROXY_FILE_NAME							"/mnt/nand1-2/Settings/VideoProxy.cfg"
#define TlogEventList_FILE_NAME					  "/mnt/nand1-2/Settings/TlogEventList.json"

#define UserDataFolder									"/mnt/nand1-2/UserData/"

#define REBOOT_FLAG_PATH								"/mnt/nand1-2/UserData/reboot.temp"
#define JPEG_STORE_DIR									"/mnt/nand1-2/UserData/Photo/"
#define CALL_RECORD_FILE_NAME							"/mnt/nand1-2/UserData/call_record_table.txt"
#define	SearchResult_FILE								"/mnt/nand1-2/UserData/searchResult.txt"
#define	ImCallRecordFileName						"ImCallRecord.json"
#define	EventRecordFileName						"EventRecord.json"
#define	DSUnlockRecordFileName					"DS_UnlockRecord.json"
#define	DSCallRecordFilePath						"/mnt/nand1-2/UserData/DSCallRecord.json"
#define	DivertAcc_TBFilePath						"/mnt/nand1-2/UserRes/DivertAccTable.json"
#define	IXG_LIST_FileName					      "IXG_List.json"
#define	DT_IM_LIST_FileName					    "DT_IM_List.json"
#define	NDM_RegisterFileName					  "NDM_Register.json"
#define	IP_CONFIG_FileName					    "IP_CONFIG.json"
#define	DT_IM_CHECK_RESULT_FileName			"DT_IM_CHECK_RESULT.json"

#define LOG_DIR									        "/mnt/nand1-2/UserData/log/"
#define	CERT_LOG_FileName					      "CertLog.json"
#define	NDM_LogFileName					        "NDM_Log.json"
#define	NDM_S_LogFileName					      "NDM_S_Log.json"
#define	Easylogger_FILE									"/mnt/nand1-2/UserData/log/elog_file.log"
#define BUSINESS_LOG_FILE								"/mnt/nand1-2/UserData/log/business.log"
#define	ElogJsonFileName								"Elog.json"
#define 	SipCallLogName								"SipCallLog.json"	
#define	VideoSerErrFileName								"VideoSerErr.json"


#define UserResFolder									"/mnt/nand1-2/UserRes/"
#define MON_LIST_FILE_NAME						"/mnt/nand1-2/UserRes/rid1014_VideoResource.csv"
#define NAME_LIST_FILE_NAME								"/mnt/nand1-2/UserRes/rid1012_IMNameListTable.csv"
#define NAME_LIST_BAK_FILE_NAME							"/mnt/nand1-2/UserRes/rid1012_IMNameListTable.csv.bak"
#define IPC_MON_LIST_FILE_NAME							"/mnt/nand1-2/UserRes/IPC_MonRes_table.csv"
#define R8001_TABLE_NAME								"/mnt/nand1-2/UserRes/R8001.csv"
#define IM_COOKIE_TABLE_NAME							"/mnt/nand1-2/UserRes/R8018.csv"
#define MON_COOKIE_TABLE_NAME							"/mnt/nand1-2/UserRes/R8019.csv"
#define RESOURCE_TABLE_NAME								"/mnt/nand1-2/UserRes/ResourceTable.csv"
#define IPC_SETUP_FILE_NAME								"/mnt/nand1-2/UserRes/IPC_Setup.cfg"
#define IMDivertAccTbPath	                "/mnt/nand1-2/UserRes/IM_DivertAcc_Table.json"
#define IPC_WLAN_SETUP_FILE_NAME						"/mnt/nand1-2/UserRes/IPC_WLAN_Setup.cfg"
#define APP_IPC_LIST_FILE_NAME			    		"/mnt/nand1-2/UserRes/APP_IPC_List.cfg"
#define PublicResFileDir								"/mnt/nand1-2/UserRes/res_public/"
#define	RSFILE_FOLDER									"/mnt/nand1-2/UserRes/res_file/"
#define ResFileDir										"/mnt/nand1-2/UserRes/res_file/"
#define ResFileParentDir								"/mnt/nand1-2/UserRes/"
#define DeviceTableFileName							"TB_DeviceTable.txt"
#define DeviceTableTftpFileName					"DeviceTable.txt"
#define TB_Sub_SIP_ACC_PATH						"/mnt/nand1-2/UserRes/TB_Sub_SIP_ACC.json"
#define TB_Divert_On_PATH							"/mnt/nand1-2/UserRes/TB_Divert_On.json"
#define TB_CardTable_PATH							"/mnt/nand1-2/UserRes/CardTable.json"
#define TB_IXGMonRes								"IXGMonRes.json"
#define TB_IXRList_PATH							"/mnt/nand1-2/UserRes/IXRList.json"
#define TB_NEW_DT_IM_FILE_NAME			"/mnt/nand1-2/UserRes/NEW_DT_IM.json"
#define TB_HYBRID_MAT_FILE_NAME			"/mnt/nand1-2/UserRes/HYBRID_MAT.json"
#define TB_HYBRID_SAT_FILE_NAME			  "/mnt/nand1-2/UserRes/HYBRID_SAT.json"
#define TB_HYBRID_SAT_CHECK_FILE_NAME			"/mnt/nand1-2/UserRes/HYBRID_SAT_CHECK.json"
#define TB_APT_MAT_FILE_NAME			        "/mnt/nand1-2/UserRes/APT_MAT.json"
#define TB_KEYID_MAPPING_FILE_NAME			        "/mnt/nand1-2/UserRes/KEYID_MAPPING.json"
#define TB_APT_MAT_CHECK_FILE_NAME			  "/mnt/nand1-2/UserRes/APT_MAT_CHECK.json"
#define TB_VM_MAT_FILE_NAME			        "/mnt/nand1-2/UserRes/VM_MAT.json"
#define VM_ACC_FILE_NAME			        "/mnt/nand1-2/UserRes/VM_ACC.json"
#define VM_ACC2_FILE_NAME			        "/mnt/nand1-2/UserRes/VM_ACC2.json"


#define CERT_DIR							        "/mnt/nand1-2/UserRes/CERT/"
#define CERT_DCFG_SELF_NAME				    "dcfg_self.json"

#define CERT_FILE				              "/mnt/nand1-2/UserData/cert.json"


#define TEMP_Folder   									"/mnt/nand1-2/temp/"

#define CERT_TEMP_FILE				          "/mnt/nand1-2/temp/certTemp.json"
#define TIME_TEMP_FILE   								"/mnt/nand1-2/temp/timeTemp.txt"
#define IxDeviceFileDir									"/mnt/nand1-2/temp/ix_device/"
#define IxDeviceTempFileDir								"/mnt/nand1-2/temp/ix_device/temp/"
#define	RSWR_TEMPFOLDER									"/mnt/nand1-2/temp/rswr_temp/"
#define	RSRD_TEMPFOLDER									"/mnt/nand1-2/temp/rsrd_temp/"
#define REBOOT_FLAG_PATH_OLD							"/mnt/nand1-2/temp/reboot.temp"

#define NAND_DOWNLOAD_PATH								"/mnt/nand1-2/vtkDownload"
#define SHELL_PATH   									    "/mnt/nand1-2/shell/"

#define IX2V_FONT_22									"/mnt/nand1-1/App/res/verdana_ls_22.bin"
#define IX2V_R8001_TABLE_NAME					"/mnt/nand1-2/UserRes/R8001.json"
#define IX2V_PRIVATE_PWD_BY_L_NBR_TABLE_NAME					"/mnt/nand1-2/UserRes/PrivatePasswordByL_NBR.json"

#define IX2V_UI_DOCS_DEFAULT					"/mnt/nand1-1/App/res/UI-DOCS"
#define IX2V_UI_DOCS_CUS					    "/mnt/nand1-2/Customerized/UI-DOCS"


#define SDCARD_DEVICE_FILE								    "/dev/mmcblk0p1"    //不能修改，修改程序会错
#define DISK_SDCARD										        "/mnt/sdcard"       //不能修改，修改程序会错

#define SDCARD_DOWNLOAD_PATH							"/mnt/sdcard/vtkDownload"
#define SDCARD_RES_PATH									  "/mnt/sdcard/RES/"
#define SDCARD_TbUpdate_PATH									  "/mnt/sdcard/TbUpdate"
#define VIDEO_STORE_DIR									  "/mnt/sdcard/video/"
#define SdUpgradePath									    "/mnt/sdcard/SdUpgrade"
#define LOG_TB_PATH							          "/mnt/sdcard/log_tb.txt"
#define SDCARD_HYBRID_MAT_Folder				  "/mnt/sdcard/HYBRID_MAT/"
#define SDCARD_HYBRID_SAT_Folder				  "/mnt/sdcard/HYBRID_SAT/"
#define SDCARD_APT_MAT_Folder				      "/mnt/sdcard/APT_MAT/"
#define SDCARD_Backup_Path									"/mnt/sdcard/Backup/"
#define SDCARD_VM_MAT_Folder				      "/mnt/sdcard/VM_MAT/"

#define CustomerizedLanguageSDCoardFolder				"/mnt/sdcard/Customerized/language/"

#define IXG_Update_Folder   									"/mnt/nand1-2/temp/update/"
#endif

