

#ifndef _sntp_c_h  
#define _sntp_c_h  

#define DEFAULT_SNTP_SERVER	"192.168.1.254" 

#define SNTP_CTRL_UDP_SYNC_TIME_REQ				0x01
#define SNTP_CTRL_UDP_SYNC_TIME_RSP				0x81

#define SNTP_CTRL_DAT_MAX		(136)

typedef struct
{
	int		cmd;
	int		target_ip;
	int		timezone;
	int		reserve1;
	int		reserve2;
}SNTP_CTRL_T;

// lzh_20181030_s
typedef struct    
{  
    unsigned char LiVnMode;  
    unsigned char Stratum;  
    unsigned char Poll;  
    unsigned char Precision;  
    long int RootDelay;  
    long int RootDispersion;  
    char RefID[4];  
    long int RefTimeInt;  
    long int RefTimeFraction;  
    long int OriTimeInt;  
    long int OriTimeFraction;  
    long int RecvTimeInt;  
    long int RecvTimeFraction;  
    long int TranTimeInt;  
    long int TranTimeFraction;  
}STNP_Header;  

typedef struct
{
	int			cmd;	
	int			ntp_server_ip;
	STNP_Header ntp_data;
}SNTP_SERVER_PACK_T;
// lzh_20181030_e	

int sync_time_from_sntp_server( int sntp_server_ip, int timezone );


#endif  

