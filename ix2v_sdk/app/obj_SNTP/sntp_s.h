

#ifndef __sntp_time__
#define __sntp_time__

#define JAN_1ST_1900  2415021 //��Ԫ�굽1900��1��1�յ�����ֵ.

//Representation of an NTP timestamp
struct NtpTimePacket
{
    unsigned int m_dwInteger;
	unsigned int m_dwFractional;
};



//The mandatory part of an NTP packet
struct NtpBasicInfo
{
	char m_LiVnMode;
	char m_Stratum;
	char m_Poll;
	char m_Precision;
	int m_RootDelay;
	int m_RootDispersion;
	char m_ReferenceID[4];
	//ϵͳʱ�����һ�α��趨����µ�ʱ�䡣
	struct NtpTimePacket m_ReferenceTimestamp; 
	//NTP�������뿪���Ͷ�ʱ���Ͷ˵ı���ʱ�䡣
	struct NtpTimePacket m_OriginateTimestamp; 
	//NTP�����ĵ�����ն�ʱ���ն˵ı���ʱ�䡣
	struct NtpTimePacket m_ReceiveTimestamp; 
	//Ӧ�����뿪Ӧ����ʱӦ���ߵı���ʱ�䡣
	struct NtpTimePacket m_TransmitTimestamp; 
};




//The optional part of an NTP packet
struct NtpAuthenticationInfo
{
	unsigned int m_KeyID;
	char m_MessageDigest[16];
};



//The Full NTP packet
struct NtpFullPacket
{
	struct NtpBasicInfo          m_Basic;
	struct NtpAuthenticationInfo m_Auth;
};





/*******************************
discription:
  ����ʱ��ת����ʱ�䡣
parameter:
  pTvSec--���롢�����ת��ǰ���ʱ����ֵ��
*******************************/
static int local_time_to_utc(void *pTvSec);




/*******************************************
description:
  �Ѹ���ʱ��mTimeת����utcʱ�䡣
parameter:
  tmTime--���룬����ʱ�䡣
  puiSeconds--����������ntpʱ�䣬��ֵ��
*******************************************/
static int create_ntp_time(const struct tm tmTime, unsigned int *puiSeconds);




/*************************************************
discription:
  ���㵱ǰ��1900��1��1�յ�ָ������һ���ж����졣
parameter:
  iYear--���룬�ꡣ
  iMonth--���룬�¡�
  iDay--���룬�ա�
return:
  1900��1��1�յ�ָ������һ���ж����졣
*************************************************/
static unsigned int get_julian_day(int iYear, int iMonth, int iDay);




/***************************************
description:
  ��ȡϵͳ��ǰ����ʱ�䡣
parameter:
  puiSec--�������ǰϵͳntpʱ����ֵ��
***************************************/
int get_current_ntp_time(unsigned int *puiSec);



#if 0
/********************************************
description:
  ����ϵͳʱ����
parameter:
  iTZMinutesWest--���룬��ǰʱ����Ը���ʱ���ʱ�
********************************************/
int set_timezone(int iTZMinutesWest);
#endif

/*********************************************************************
description:  ����ϵͳʱ����������0��ʾΪ��ʱ����С�ڵ���0Ϊ��ʱ��
parameter:  	timezone--���룬��ǰʱ����Ը���ʱ���ʱ��
return:		0
*********************************************************************/
int set_timezone(int timezone );

#endif


