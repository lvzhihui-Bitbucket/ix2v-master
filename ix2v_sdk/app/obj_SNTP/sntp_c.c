

#include <stdio.h>  
#include <errno.h>  
#include <unistd.h>  
#include <string.h>  
#include <time.h>  
#include <sys/time.h>  
#include <netinet/in.h>  
#include <sys/socket.h>  
#include <arpa/inet.h>  
#include <netdb.h>  

#include "sntp_c.h"  

#include "unix_socket.h"
#include "obj_PublicInformation.h"

#define LOCAL_SNTP_SERVICE_FILE 	"/tmp/sntp_server_socket"
#define JAN_1970    		2208988800UL        /* 1970 - 1900 in seconds */  

static unix_socket_t		sntp_cliet_socket;
static pthread_mutex_t 		sntp_client_lock;
static sem_t				sntp_client_sem;
static int					sync_time_result = -1;
static int					sync_time_serverip = 0;
static SNTP_SERVER_PACK_T 	sntp_server_response;

// lzh_20181030_s
static int sync_time_rsp_done( STNP_Header *pstnp, int ntp_server_ip )  
{
    struct timeval 	tv;  
    struct timezone tzTimeZone;
    time_t t1,t2,t3,t4,dis;  

	t1 = ntohl(pstnp->OriTimeInt);
    t2 = ntohl(pstnp->RecvTimeInt);  
    t3 = ntohl(pstnp->TranTimeInt);  
    time(&t4);  
    t4+=JAN_1970;  
      
    dis = ( (t2-t1)+(t3-t4) )/2;  
      
    gettimeofday(&tv, &tzTimeZone);  

    tv.tv_sec+=dis;  

    //printf("tv.tv_sec=%d,t1=%d,t2=%d,t3=%d,t4=%d,dis=%d\n", tv.tv_sec,t1,t2,t3,t4,dis); 
	//printf("0000000000 get sync time: %s:%d\n", ctime(&tv.tv_sec),tzTimeZone.tz_minuteswest); 
	// 北京时间比格林尼治时间早8小时，接收到的utc时间需要加上8*60*60秒
	tv.tv_sec += (tzTimeZone.tz_minuteswest*60);

	struct in_addr temp;
	temp.s_addr = ntp_server_ip;
    //printf("From %s get sync time: %s", inet_ntoa(temp), ctime(&tv.tv_sec)); 
     #if 1 
    if(settimeofday(&tv, NULL)== -1)
	{
	    printf("settimeofday error:%s\n", strerror(errno)); 
		return -1;
	}
	#endif
	gettimeofday(&tv, &tzTimeZone);  
	//printf("1111111 get sync time: %s:%d\n", ctime(&tv.tv_sec),tzTimeZone.tz_minuteswest); 
	FILE* fd   = NULL;
	if( (fd = popen( "hwclock -w", "r" )) == NULL )
	{
		//printf( "set_system_date_time error:%s\n",strerror(errno) );
		return -1;
	}
	pclose( fd );
  
    return 0;  	
}

// lzh_20181030_e

//czn_20161014_s
static void sntp_client_socket_recv_data(char* pbuf, int len)
{
	SNTP_SERVER_PACK_T* ptr_sntp_server = (SNTP_SERVER_PACK_T*)pbuf;
	switch( ptr_sntp_server->cmd )
	{
		case SNTP_CTRL_UDP_SYNC_TIME_RSP:
			//printf("rcv SNTP_CTRL_UDP_SYNC_TIME_RSP rsp\n");
			sntp_server_response = *ptr_sntp_server;
			//printf("sntp_server_response.ntp_server_ip = 0x%08x, sync_time_serverip = 0x%08x\n",sntp_server_response.ntp_server_ip, sync_time_serverip);
			if( sntp_server_response.ntp_server_ip == sync_time_serverip )
				sync_time_result = 0;
			else
				sync_time_result = 1;
			sem_post(&sntp_client_sem);
			break;	

	}	
}

static int init_sntp_client( void )
{
	static int init_flag=0;
	if(init_flag)
		return 0;
	init_flag=1;
	pthread_mutex_init(&sntp_client_lock, 0);
	init_unix_socket(&sntp_cliet_socket,0,LOCAL_SNTP_SERVICE_FILE,sntp_client_socket_recv_data);		// 服务器端
	create_unix_socket_create(&sntp_cliet_socket);
	return 0;
}

static int deinit_sntp_client(void)
{
	deinit_unix_socket(&sntp_cliet_socket);
	return 0;
}
static unsigned char year[5] = {0};
static unsigned char mon[3] = {0};
static unsigned char day[3] = {0};
static unsigned char hour[3] = {0};
static unsigned char min[3] = {0};
static unsigned char second[3] = {0};
static unsigned char inputSet[9] = {0};

int sync_time_from_sntp_server( int sntp_server_ip, int timezone )
{
	int				ret;
	SNTP_CTRL_T 	temp;

	init_sntp_client();

	pthread_mutex_lock(&sntp_client_lock);
	
	temp.cmd		= SNTP_CTRL_UDP_SYNC_TIME_REQ;
	temp.target_ip	= sntp_server_ip;
	temp.timezone	= timezone;

	

	sync_time_serverip = sntp_server_ip;
	sem_init(&sntp_client_sem,0,0);
	
	unix_socket_send_data( &sntp_cliet_socket, (char*)&temp, sizeof(SNTP_CTRL_T));
	
	if( sem_wait_ex2(&sntp_client_sem, 5000) != 0 )
	{
		ret = -1;
	}
	else
		ret = sync_time_result;
	
	// lzh_20181030_s
	if( ret == 0 )
	{
		//printf("----sync_time_from_sntp_server ok!-----\n");
		sync_time_rsp_done(&sntp_server_response.ntp_data,sntp_server_response.ntp_server_ip);
	}
	else
		printf("----sync_time_from_sntp_server error!-----\n");
	// lzh_20181030_e

	pthread_mutex_unlock(&sntp_client_lock);

	//deinit_sntp_client();

	return ret;
}
int	ReadDateTime(uint8 dateOrTime)	
{
//#define RTC_TIME

	unsigned char h;
	unsigned char mi;
	unsigned char s;
	unsigned char y;
	unsigned char mo;
	unsigned char d;

	// Get the current date time
	if(dateOrTime)
	{
#if 0
		RTC_DateTypeDef date;
		date	= RtcGetDate();
		y 	= BcdToBin(date.RTC_Year);
		mo 	= BcdToBin(date.RTC_Month);
		d 	= BcdToBin(date.RTC_Date);
#else
		time_t t;
		struct tm *tblock;	
		t = time(NULL); 
		tblock=localtime(&t);

		//printf("get localtime %d-%d-%d \n", tblock->tm_year, tblock->tm_mon, tblock->tm_mday);
		
		y = ((tblock->tm_year > 100) ? tblock->tm_year -100 : 0);
		mo = tblock->tm_mon+1;
		d = tblock->tm_mday;
		h = tblock->tm_hour;
		mi = tblock->tm_min;
		s = tblock->tm_sec;
#endif
		// init year
		snprintf(year,5,"20%02d",y);	
		// init month
		snprintf(mon,3,"%02d",mo);	
		// init day
		snprintf(day,3,"%02d",d);	
	}
	else
	{
#if 0
		RTC_TimeTypeDef time;
		time	= RtcGetTime();
		h	= BcdToBin(time.RTC_Hours);
		mi	= BcdToBin(time.RTC_Minutes);
		s	= BcdToBin(time.RTC_Seconds);
#else
		time_t t;
		struct tm *tblock;	
		t = time(NULL); 
		tblock=localtime(&t);
		
		y = tblock->tm_year-100;
		mo = tblock->tm_mon+1;
		d = tblock->tm_mday;
		h = tblock->tm_hour;
		mi = tblock->tm_min;
		s = tblock->tm_sec;
#endif
		// init hour
		snprintf(hour,3,"%02d",h);	
		// init minute
		snprintf(min,3,"%02d",mi);	
		// init minute
		snprintf(second,3,"%02d",s);	
	}
}
int set_system_date_time(uint8 dateOrTime)
{
	FILE* fd   = NULL;
	char buf[100] = {0};
	
	if(dateOrTime)
	{
		memcpy(year, inputSet, 4);
		memcpy(mon, inputSet+4, 2);
		memcpy(day, inputSet+6, 2);
		ReadDateTime(0);
	}
	else
	{
		memcpy(hour, inputSet, 2);
		memcpy(min, inputSet+2, 2);
		memcpy(second, "00", 2);
		ReadDateTime(1);
	}
	snprintf( buf, sizeof(buf),  "%s\"%s-%s-%s %s:%s:%s\"\n", "date -s ", year,mon,day,hour,min,second);

	

	//dprintf( "%s\n",buf);
	
	if( (fd = popen( buf, "r" )) == NULL )
	{
		dprintf( "set_system_date_time error:%s\n",strerror(errno) );
		return -1;
	}		
	pclose( fd );

	if( (fd = popen( "hwclock -w", "r" )) == NULL )
	{
		dprintf( "set_system_date_time error:%s\n",strerror(errno) );
		return -1;
	}
	pclose( fd );
	
	//dprintf( "set_system_date_time ok!\n" );

	//cao_201081020_s
	//SetTimeUpdateFlag(1);
	//SaveDateAndTimeToFile();
	//cao_201081020_e
	
	return 0;
}

int RtcSet_ParaCheckDate(void)
{
	int para_len,para_value;
	
	memcpy(year, inputSet, 4);
	memcpy(mon, inputSet+4, 2);
	memcpy(day, inputSet+6, 2);

	if(strlen(inputSet) != 8)
	{
		return -1;
	}
	//printf("11111111%s:%d:%s\n",__func__,__LINE__,inputSet);
	para_len = strlen(year);
	if(para_len != 4)
	{
		return -1;
	}
	para_value = atol(year);
	//if(para_value > 2037 || para_value < 2005)
	if(para_value < 2000)
	{
		return -1;
	}

	para_len = strlen(mon);
	if(para_len != 1 && para_len != 2)
	{
		return -1;
	}
	para_value = atol(mon);
	if(para_value > 12)
	{
		return -1;
	}

	para_len = strlen(day);
	if(para_len != 1 && para_len != 2)
	{
		return -1;
	}
	para_value = atol(day);
	if(para_value > 31)
	{
		return -1;
	}
	//printf("11111111%s:%d:%s\n",__func__,__LINE__,inputSet);
	return 0;
}

int RtcSet_ParaCheckTime(void)
{
	int para_len,para_value;
	
	memcpy(hour, inputSet, 2);
	memcpy(min, inputSet+2, 2);
	//printf("11111111%s:%d:%s\n",__func__,__LINE__,inputSet);
	if(strlen(inputSet) != 4)
	{
		return -1;
	}

	para_len = strlen(hour);
	if(para_len != 1 && para_len != 2)
	{
		return -1;
	}
	para_value = atol(hour);
	if(para_value > 23)
	{
		return -1;
	}

	para_len = strlen(min);
	if(para_len != 1 && para_len != 2)
	{
		return -1;
	}
	para_value = atol(min);
	if(para_value > 59)
	{
		return -1;
	}
	#if 0
	printf("11111111%s:%d:%s\n",__func__,__LINE__,inputSet);
	para_len = strlen(second);
	if(para_len != 1 && para_len != 2)
	{
		return -1;
	}
	para_value = atol(second);
	if(para_value > 59)
	{
		return -1;
	}
	#endif
	return 0;
}
int DateSave(const char* date)
{
	//strcpy(inputSet, date);
	memcpy(inputSet,date,4);
	memcpy(inputSet+4,date+4,2);
	memcpy(inputSet+6,date+6,2);
	inputSet[8]=0;
	//printf("11111111%s:%d:%s\n",__func__,__LINE__,inputSet);
	if(!RtcSet_ParaCheckDate())
	{
		//printf("11111111%s:%d:%s\n",__func__,__LINE__,inputSet);
		set_system_date_time(1);
		
		return 1;
	}
	else
	{
		return 0;
	}
}

int TimeSave(const char* time)
{
	//strcpy(inputSet, time);
	memcpy(inputSet,time,2);
	memcpy(inputSet+2,time+2,2);
	inputSet[4]=0;
	//printf("11111111%s:%d:%s\n",__func__,__LINE__,inputSet);
	if(!RtcSet_ParaCheckTime())
	{
		//printf("11111111%s:%d:%s\n",__func__,__LINE__,inputSet);
		set_system_date_time(0);
		return 1;
	}
	else
	{
		return 0;
	}
}

