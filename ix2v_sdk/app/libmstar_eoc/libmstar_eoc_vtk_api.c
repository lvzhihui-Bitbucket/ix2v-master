#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "libmstar_eoc.h"
#include "parson.h"
#include "cJSON.h"
#define MME_VERSION "2020.11.26"

#if 0
void print_usage ()
{
    ShellCmdPrintf ("mme : usage to show and operation eoc version %s\n", MME_VERSION);
    ShellCmdPrintf ("mme interface dst_mac version\n");
    ShellCmdPrintf ("mme interface dst_mac multiversion\n");
    ShellCmdPrintf ("\t\t all of the device that recvice this packet will reponse.\n");
    ShellCmdPrintf ("mme interface dst_mac get_all_cco\n");
    ShellCmdPrintf ("\t\t get all cco device in the multile network.\n");
    ShellCmdPrintf ("mme interface dst_mac file      read      file_name\n");
    ShellCmdPrintf ("mme interface dst_mac file      save      file_name  file_path\n");
    ShellCmdPrintf ("mme interface dst_mac file      write     file_name  file_path\n");
    ShellCmdPrintf ("mme interface dst_mac file      upgrade   file_name  file_path\n");
    ShellCmdPrintf ("mme interface dst_mac parameter read      file_name  group    variable\n");
    ShellCmdPrintf ("mme interface dst_mac parameter write     file_name  group    variable    type   value\n"
            "\t\t type 2 means string, 3 means num, 4 means object, 5 means array, 6 means bool\n");
    ShellCmdPrintf ("mme interface dst_mac parameter write     file_name  group    variable    type(array)   array_data_type  array_index   value\n");
    ShellCmdPrintf ("mme interface dst_mac reset\n");
    ShellCmdPrintf ("mme interface dst_mac get_tonemask peer_mac\n");
    ShellCmdPrintf ("mme interface dst_mac get_eth_phy\n");
    ShellCmdPrintf ("mme interface dst_mac eth_stats\n");
    ShellCmdPrintf ("mme interface dst_mac get_status\n");
    ShellCmdPrintf ("mme interface dst_mac get_tonemap sta_mac dir(0:tx, 1:rx)\n");
    ShellCmdPrintf ("mme interface dst_mac get_snr sta_mac -- not ok now\n");
    ShellCmdPrintf ("mme interface dst_mac get_link_stats ReqType ReqID LID TLFlag Mgmt_Flag DA/SA\n"
            "\t\t\t ReqType 0 means reset statistics, 1 means get, 2 means get and reset\n"
            "\t\t\t ReqID Not Use, Give 0\n"
            "\t\t\t LID     0-3 means cap0-cap3\n"
            "\t\t\t txflag  0   means tx, 1 means rx\n"
            "\t\t\t Mgnt_Flag Not use, Give 0\n"
            );
    ShellCmdPrintf ("mme interface dst_mac nw_system_info\n");
    ShellCmdPrintf ("mme interface dst_mac set_mac mac\n");
    ShellCmdPrintf ("mme interface dst_mac diag_debug subtype action other_params\n"
            "\t\t\t subtype 0 means assert, 1 means sniffer\n"
            "\t\t\t action 1 open sniffer 0 clonde sniffer, only sniffer need set\n"
            "\t\t\t other_params only sniffer need set\n"
            );
}
#else
void print_usage ()
{
	char strbuff[2000]={0};
    snprintf (strbuff+strlen(strbuff),2000-strlen(strbuff),"mme : usage to show and operation eoc version %s\n", MME_VERSION);
    snprintf (strbuff+strlen(strbuff),2000-strlen(strbuff),"mme interface dst_mac version\n");
    snprintf (strbuff+strlen(strbuff),2000-strlen(strbuff),"mme interface dst_mac multiversion\n");
    snprintf (strbuff+strlen(strbuff),2000-strlen(strbuff),"\t\t all of the device that recvice this packet will reponse.\n");
    snprintf (strbuff+strlen(strbuff),2000-strlen(strbuff),"mme interface dst_mac get_all_cco\n");
    snprintf (strbuff+strlen(strbuff),2000-strlen(strbuff),"\t\t get all cco device in the multile network.\n");
    snprintf (strbuff+strlen(strbuff),2000-strlen(strbuff),"mme interface dst_mac file      read      file_name\n");
    snprintf (strbuff+strlen(strbuff),2000-strlen(strbuff),"mme interface dst_mac file      save      file_name  file_path\n");
    snprintf (strbuff+strlen(strbuff),2000-strlen(strbuff),"mme interface dst_mac file      write     file_name  file_path\n");
    snprintf (strbuff+strlen(strbuff),2000-strlen(strbuff),"mme interface dst_mac file      upgrade   file_name  file_path\n");
    snprintf (strbuff+strlen(strbuff),2000-strlen(strbuff),"mme interface dst_mac parameter read      file_name  group    variable\n");
    snprintf (strbuff+strlen(strbuff),2000-strlen(strbuff),"mme interface dst_mac parameter write     file_name  group    variable    type   value\n"
            "\t\t type 2 means string, 3 means num, 4 means object, 5 means array, 6 means bool\n");
    snprintf (strbuff+strlen(strbuff),2000-strlen(strbuff),"mme interface dst_mac parameter write     file_name  group    variable    type(array)   array_data_type  array_index   value\n");
    snprintf (strbuff+strlen(strbuff),2000-strlen(strbuff),"mme interface dst_mac reset\n");
    snprintf (strbuff+strlen(strbuff),2000-strlen(strbuff),"mme interface dst_mac get_tonemask peer_mac\n");
    snprintf (strbuff+strlen(strbuff),2000-strlen(strbuff),"mme interface dst_mac get_eth_phy\n");
    snprintf (strbuff+strlen(strbuff),2000-strlen(strbuff),"mme interface dst_mac eth_stats\n");
    snprintf (strbuff+strlen(strbuff),2000-strlen(strbuff),"mme interface dst_mac get_status\n");
    snprintf (strbuff+strlen(strbuff),2000-strlen(strbuff),"mme interface dst_mac get_tonemap sta_mac dir(0:tx, 1:rx)\n");
    snprintf (strbuff+strlen(strbuff),2000-strlen(strbuff),"mme interface dst_mac get_snr sta_mac -- not ok now\n");
    snprintf (strbuff+strlen(strbuff),2000-strlen(strbuff),"mme interface dst_mac get_link_stats ReqType ReqID LID TLFlag Mgmt_Flag DA/SA\n"
            "\t\t\t ReqType 0 means reset statistics, 1 means get, 2 means get and reset\n"
            "\t\t\t ReqID Not Use, Give 0\n"
            "\t\t\t LID     0-3 means cap0-cap3\n"
            "\t\t\t txflag  0   means tx, 1 means rx\n"
            "\t\t\t Mgnt_Flag Not use, Give 0\n"
            );
    snprintf (strbuff+strlen(strbuff),2000-strlen(strbuff),"mme interface dst_mac nw_system_info\n");
    snprintf (strbuff+strlen(strbuff),2000-strlen(strbuff),"mme interface dst_mac set_mac mac\n");
    snprintf (strbuff+strlen(strbuff),2000-strlen(strbuff),"mme interface dst_mac diag_debug subtype action other_params\n"
            "\t\t\t subtype 0 means assert, 1 means sniffer\n"
            "\t\t\t action 1 open sniffer 0 clonde sniffer, only sniffer need set\n"
            "\t\t\t other_params only sniffer need set\n"
            );
	ShellCmdPrintf(strbuff);
	
}
#endif
#if 1

int API_VTK_CMP_Mac(const char *mac1,const char *mac2)
{
	
	char mac_bin1[6];
	char mac_bin2[6];
	//libmstar_vs_product_req_t product_req = { 0 };
        //libmstar_vs_product_cnf_t product_cnf = { 0 };
        
      
        libmstar_mac_str_to_bin(mac1,mac_bin1);
	libmstar_mac_str_to_bin(mac2,mac_bin2);
	if(memcmp(mac_bin1,mac_bin2,6)==0)
		return 0;

	
      return 1;
}
int API_VTK_Get_Eoc_Mac(char *net_interface,char *mac)
{
	int ret=-1;
	char version[128] = { 0 };
	char dest_mac[18] = { 0 };
	strcpy(dest_mac,"00:b0:52:00:00:01");//strcpy(dest_mac,"ff:ff:ff:ff:ff:ff");
	if(net_interface==NULL)
		net_interface="eth0";
	ret = libmstar_vs_get_mst_version (net_interface, dest_mac, version);
	if(ret==0&&mac)
		strcpy(mac,dest_mac);
	return ret;
}
int API_VTK_Get_Eoc_Mac_Ver(char *net_interface,char *mac,char*ver)
{
	int ret=-1;
	char version[128] = { 0 };
	char dest_mac[18] = { 0 };
	strcpy(dest_mac,"00:b0:52:00:00:01");//strcpy(dest_mac,"ff:ff:ff:ff:ff:ff");
	if(net_interface==NULL)
		net_interface="eth0";
	ret = libmstar_vs_get_mst_version (net_interface, dest_mac, version);
	
	if(ret==0&&mac)
		strcpy(mac,dest_mac);
	if(ret==0&&ver)
		strcpy(ver,version);
	if(strcmp(dest_mac,"ff:ff:ff:ff:ff:ff")==0||strcmp(dest_mac,"FF:FF:FF:FF:FF:FF")==0)
		return -1;
	return ret;
}
int API_VTK_Set_Eoc_Mac(char *net_interface,const char *mac)
{
	char dest_mac[18] = { 0 };
	int try_cnt=0;
	char mac_bin[6];
	libmstar_vs_product_req_t product_req = { 0 };
        libmstar_vs_product_cnf_t product_cnf = { 0 };
	 if (LIBMSTAR_SUCCESS != check_mac_format(mac))
        {
            //ShellCmdPrintf("please check mac format!\n");
            return -1;
        }

       try_cnt=15; 
	while(--try_cnt>0)
	{
		if(API_VTK_Get_Eoc_Mac(net_interface,dest_mac)==0)
			break;
		sleep(1);
	}
	
	if(try_cnt<=0)
	  	return -1;
	if(strcmp(dest_mac,"ff:ff:ff:ff:ff:ff")==0||strcmp(dest_mac,"FF:FF:FF:FF:FF:FF")==0)
		return -1;
	
        product_req.moduleid = 1 << MODULE_SET_MAC | 1 << MODULE_REBOOT;
        
      
        libmstar_mac_str_to_bin(mac, product_req.data);
	libmstar_mac_str_to_bin(dest_mac,mac_bin);
	if(memcmp( product_req.data,mac_bin,6)==0)
		return 1;
        if(LIBMSTAR_SUCCESS == libmstar_vs_product (net_interface, dest_mac, &product_req, &product_cnf))
        {
            if (product_cnf.mstatus == LIBMSTAR_VS_PRODUCT_CNF_STATUS_SUCCESS)
            {
                	//ShellCmdPrintf ("set_mac %s success!\n", dest_mac);
			return 0;
            }
            else
            {
                	ShellCmdPrintf("11111set_mac %s fail!\n", dest_mac);
			return -1;	
            }
            
        }
        else
        {
        	try_cnt=30;
        	while(try_cnt--)
        	{
        		if(API_VTK_Get_Eoc_Mac(net_interface,dest_mac)!=0)
        		{
        			sleep(1);
				continue;
			}
			//ShellCmdPrintf ("000set_mac %s success:%d!\n", dest_mac,try_cnt);	
			return 0;
        	}
            	ShellCmdPrintf("222222set_mac %s fail!\n", dest_mac);
		return -1;	
        }
}
#endif

int API_VTK_Get_Eoc_Link_State(char *net_interface,char *eoc_mac)
{
	char dest_mac[18] = { 0 };
	if(eoc_mac==NULL||eoc_mac[0]==0)
	{
		if(API_VTK_Get_Eoc_Mac(net_interface,dest_mac)!=0)
		{
			return -1;
		}
		
		if(eoc_mac!=NULL)
			strcpy(eoc_mac,dest_mac);
		//printf("111111:%s:%s\n",dest_mac,eoc_mac?eoc_mac:"null");
	}
	else
		strcpy(dest_mac,eoc_mac);
	libmstar_vs_get_nw_info_cnf_t get_nw_info_cnf;
        //libmstar_vs_diag_debug_req_t diag_debug_req;
       // libmstar_vs_diag_debug_cnf_t diag_debug_cnf;
        

        if (LIBMSTAR_SUCCESS == libmstar_vs_get_nw_info (net_interface, dest_mac, &get_nw_info_cnf))
        {
		  if (0 < get_nw_info_cnf.num_stas)
            {
                if (1 == get_nw_info_cnf.sta_info[0].sta_tei)
			return 1;
		else
			return 2;
		}
		  return 0;
	}
	return -1;	
}
int API_VTK_Get_Eoc_Link_State2(char *net_interface,char *eoc_mac,char *ver)
{
	char dest_mac[18] = { 0 };
	char version[128] = { 0 };
	if(eoc_mac==NULL||eoc_mac[0]==0)
	{
		if(API_VTK_Get_Eoc_Mac_Ver(net_interface,dest_mac,version)!=0)
		{
			return -1;
		}
		
		if(eoc_mac!=NULL)
			strcpy(eoc_mac,dest_mac);
		if(ver!=NULL)
			strcpy(ver,version);
		printf("111111:%s:%s,ver=%s\n",dest_mac,eoc_mac?eoc_mac:"null",version);
	}
	else
	{
		strcpy(dest_mac,eoc_mac);
		if(ver)
		{
			if(libmstar_vs_get_mst_version (net_interface, dest_mac, version)==0)
				strcpy(ver,version);
		}
	}
	libmstar_vs_get_nw_info_cnf_t get_nw_info_cnf;
        //libmstar_vs_diag_debug_req_t diag_debug_req;
       // libmstar_vs_diag_debug_cnf_t diag_debug_cnf;
        

        if (LIBMSTAR_SUCCESS == libmstar_vs_get_nw_info (net_interface, dest_mac, &get_nw_info_cnf))
        {
		  if (0 < get_nw_info_cnf.num_stas)
            {
                if (1 == get_nw_info_cnf.sta_info[0].sta_tei)
			return 1;
		else
			return 2;
		}
		  return 0;
	}
	return -1;	
}
#include "cJSON.h"
cJSON *API_VTK_Get_Eoc_Diag_Debug(char *net_interface,char *eoc_mac)
{
	libmstar_vs_diag_debug_req_t diag_debug_req;
        libmstar_vs_diag_debug_cnf_t diag_debug_cnf;
	   diag_debug_req.command = LIBMSTAR_VS_DIAG_DEBUG_REQ_SUBTYPE_GET_SYS_INFO;

         if (LIBMSTAR_SUCCESS ==libmstar_vs_diag_debug (net_interface, eoc_mac, &diag_debug_req,&diag_debug_cnf))
	{
		cJSON *item=cJSON_CreateObject();
		cJSON_AddNumberToObject(item,"EOC_auth_times",diag_debug_cnf.authenticated_times);
		cJSON_AddNumberToObject(item,"EOC_online_time",diag_debug_cnf.auth_time);
		return item;
	}
	return NULL;
}
int API_VTK_Get_Eoc_File(char *net_interface,char *dest_mac,char *eoc_file_path,char *vtk_file_path)
{
	vs_file_access_cmd_t cmd;
        memset (&cmd, 0x0, sizeof (cmd));
        cmd.file_type = VS_FILE_ACCESS_REQ_FILE_TYPE_GENERAL_FILE;

        cmd.op = VS_FILE_ACCESS_REQ_OP_SAVE;
        cmd.file_type = VS_FILE_ACCESS_REQ_FILE_TYPE_GENERAL_FILE;
        strcpy (cmd.file_name, vtk_file_path);
      
        strcpy (cmd.parameter, eoc_file_path);

        return libmstar_vs_file_access_req (net_interface, dest_mac, &cmd);
}

int API_VTK_Set_Eoc_File(char *net_interface,char *dest_mac,char *eoc_file_path,char *vtk_file_path)
{
	vs_file_access_cmd_t cmd;
        memset (&cmd, 0x0, sizeof (cmd));
        cmd.file_type = VS_FILE_ACCESS_REQ_FILE_TYPE_GENERAL_FILE;

        cmd.op = VS_FILE_ACCESS_REQ_OP_WRITE;
        //cmd.file_type = VS_FILE_ACCESS_REQ_FILE_TYPE_GENERAL_FILE;
        strcpy (cmd.file_name, vtk_file_path);
      
        strcpy (cmd.parameter, eoc_file_path);

        return libmstar_vs_file_access_req (net_interface, dest_mac, &cmd);
}
int API_VTK_Get_Eoc_Parameter(char *net_interface,char *dest_mac,char *eoc_file_path,char *group_name,char *para_name,void *data)
{
	vs_file_access_cmd_t cmd;
        memset (&cmd, 0x0, sizeof (cmd));
        cmd.file_type = VS_FILE_ACCESS_REQ_FILE_TYPE_GENERAL_FILE;

        cmd.op = VS_FILE_ACCESS_REQ_OP_READ;
        //cmd.file_type = VS_FILE_ACCESS_REQ_FILE_TYPE_GENERAL_FILE;
      //  strcpy (cmd.file_name, vtk_file_path);
      
        strcpy (cmd.parameter, eoc_file_path);

       if(libmstar_vs_file_access_req (net_interface, dest_mac, &cmd)!=0)
       {
       	printf("1111 %s,%d\n",__func__,__LINE__);
	   	return -1;
       }
	cJSON *para_json= cJSON_Parse(cmd.file_buf);
	if(para_json==NULL)
	{
		printf("1111 %s,%d\n",__func__,__LINE__);
		return -1;
	}
	cJSON *group=NULL;
	if(group_name)
	{
		if(GetJsonDataPro(para_json, group_name, &group)==0)
		{
			printf("1111 %s,%d\n",__func__,__LINE__);
			cJSON_Delete(para_json);
			return -1;
		}
	}
	else
		group=para_json;
	if(GetJsonDataPro(group, para_name, data)==0)
	{
		printf("1111 %s,%d\n",__func__,__LINE__);
		cJSON_Delete(para_json);
		return -1;
	}
	cJSON_Delete(para_json);
	return 0;
}
int API_VTK_SoftReset_Eoc(char *net_interface,char *dest_mac)
{
	 libmstar_vs_reset_cnf_t reset_cnf;
	 printf("%s:start!!!!!!!\n",__func__);
	 if (LIBMSTAR_SUCCESS == libmstar_vs_reset (net_interface, dest_mac, &reset_cnf)
            && LIBMSTAR_VS_RESET_CNF_RESULT_SUCCESS == reset_cnf.result)
	 {	
	 	int trycnt=60;
	 	while(trycnt--)
	 	{
	 		sleep(1);
			if(API_VTK_Get_Eoc_Link_State(net_interface, dest_mac)>=0)
				break;
	 	}
		if(trycnt>0)
		{
			printf("%s:succ:%d!!!!!!!\n",__func__,trycnt);
			return 0;
		}
	 }
	 printf("%s:fail!!!!!!!\n",__func__);
	 return -1;
}
int API_VTK_Update_Eoc_Parameter(char *net_interface,char *dest_mac,char *eoc_file_path,char *group_name,char *para_name,int para_type,char *value)
{
	 cJSON* node=NULL;
	 if(para_type!=3&&para_type!=6)
	 	return -1;
	
	 if(API_VTK_Get_Eoc_File(net_interface, dest_mac,eoc_file_path,"/tmp/eoc_para.tmp")!=0)
	 {
       	printf("1111 %s,%d\n",__func__,__LINE__);
	   	//return -1;
       }
	cJSON *para_json= GetJsonFromFile("/tmp/eoc_para.tmp");//cJSON_Parse(cmd.file_buf);
	system("rm /tmp/eoc_para.tmp");
	if(para_json==NULL)
	{
		//printf("1111 %s,%d\n",__func__,__LINE__);
		//return -1;
		para_json=cJSON_CreateObject();
	}
	//printf("1111 %s,%d:\n%s\n",__func__,__LINE__,cmd.file_buf);
	
	
	cJSON *group=NULL;
	if(group_name)
	{
		if(GetJsonDataPro(para_json, group_name, &group)==0)
		{
			//printf("1111 %s,%d\n",__func__,__LINE__);
			//cJSON_Delete(para_json);
			//return -1;
			group=cJSON_AddObjectToObject(para_json,group_name);
		}
	}
	else
		group=para_json;
	if(para_type==3||para_type==6)
	{
		
		if(para_type==3)
			node=cJSON_CreateNumber(atoi(value));
		else 
			node=cJSON_CreateBool(atoi(value));
		int data;
		if(GetJsonDataPro(group, para_name, &data)==0)
		{
			//printf("1111 %s,%d\n",__func__,__LINE__);
			//cJSON_Delete(para_json);
			//return -1;
			cJSON_AddItemToObject(group,para_name,node);
		}
		else
		{
			if(data!=atoi(value))
				cJSON_ReplaceItemInObjectCaseSensitive(group,para_name,node);
			else
			{
				cJSON_Delete(node);
				cJSON_Delete(para_json);
				return 0;
			}
		}
		
	}
	
	
	SetJsonToFileUnformatted("/tmp/eoc_para.tmp",para_json);
	cJSON_Delete(para_json);
	printf("1111 %s,%d\n",__func__,__LINE__);
	if(API_VTK_Set_Eoc_File(net_interface, dest_mac,eoc_file_path,"/tmp/eoc_para.tmp")!=0)
	{
		system("rm /tmp/eoc_para.tmp");
		return -1;
	}
	system("rm /tmp/eoc_para.tmp");
	return 1;
}

int Eoc_Libmstar_Shell_Test (cJSON *cmd)
{
    //char slave_mac[MAC_STR_MAX_LEN] = EOC_DEFAULT_MAC;
    char interface[32] = "br0";
    char dest_mac[18] = { 0 };
    int ret = 0;
	int i;
	int argc=cJSON_GetArraySize(cmd);
	char *argv[20];
	char strbuff[2000]={0};
	if(argc>20)
	{
		ShellCmdPrintf("error:arg num max is 20\n");
		return ret;
	}
	else
	{
		for(i=0;i<argc;i++)
			argv[i]=GetShellCmdInputString(cmd,i);
	}
	if(!strcmp(argv[1], "test"))
	{
		GetPhMFGSnInit();
		return 0;
	}
	if (argc>2&&!strcmp(argv[2], "vtk_get_mac"))
	 {
		if(API_VTK_Get_Eoc_Mac(argv[1],dest_mac)==0)
			 ShellCmdPrintf ("vtk_get_mac succ:%s\n",dest_mac);
		else
			 ShellCmdPrintf ("vtk_get_mac fail\n");
			
	 }
	else if(argc>2&&!strcmp(argv[2], "vtk_get_state"))
	{
		int link_state=API_VTK_Get_Eoc_Link_State(argv[1],argc>3?argv[3]:NULL);
		if(link_state<0)
			ShellCmdPrintf ("unlink\n");
		if(link_state==0)
			ShellCmdPrintf ("unauth\n");
		if(link_state==1)
			ShellCmdPrintf ("slave\n");
		if(link_state==2)
			ShellCmdPrintf ("master\n");
	}
	  else if (argc>3&&!strcmp(argv[2], "vtk_set_mac"))
	 {
	 
		if((ret=API_VTK_Set_Eoc_Mac(argv[1],argv[3]))==0)
			 ShellCmdPrintf ("vtk_Set_mac succ:%s\n",argv[3]);
		else if(ret==1)
			ShellCmdPrintf ("vtk_Set_mac repeat:%s\n",argv[3]);
		else
			 ShellCmdPrintf ("vtk_Set_mac fail\n");
			
	 }	

  else if (argc < 4)
    {
        
	  	print_usage ();
    }
    else if (!strcmp (argv[3], "version"))
    {
        strcpy (interface, argv[1]);
        strcpy (dest_mac, argv[2]);
        char version[128] = { 0 };
        ret = libmstar_vs_get_mst_version (interface, dest_mac, version);
        if(ret == 0)
            ShellCmdPrintf ("Get mac %s version:%s ret=%d\n", dest_mac, version, ret);
        else
            ShellCmdPrintf("error! ret = %d\n", ret);
        
    }
    else if (!strcmp (argv[3], "multiversion"))
    {
        libmstar_vs_get_version_cnf_t *version_cnf = NULL;
        libmstar_vs_get_version_cnf_t *node = NULL;
        int num = 0;
        char mac[20];

        strcpy (interface, argv[1]);
        strcpy (dest_mac, argv[2]);

        ret = libmstar_vs_get_mst_version_with_multi_response (interface, dest_mac, &version_cnf);
        if (ret == LIBMSTAR_SUCCESS)
        {
            node = version_cnf;
            while (node)
            {
                libmstar_mac_bin_to_str(node->mac, mac);
                ShellCmdPrintf("No.%d: mac=%s version=%s\n", ++num , mac, node->av_stack_version);
                node = node->next;
            }
            free_vs_get_version_cnf (version_cnf);
            return 0;
        }else
        {
            ShellCmdPrintf("error ! ret = %d\n", ret);
        }
    }
    else if (!strcmp (argv[3], "get_all_cco"))
    {
        libmstar_vs_get_cco_dev_cnf_t *cco_dev_list_head = NULL;
        libmstar_vs_get_cco_dev_cnf_t *node = NULL;
        int num = 0;
        char mac[20];
        strcpy (interface, argv[1]);
        strcpy (dest_mac, argv[2]);

        ret = libmstar_vs_get_cco_dev_with_multi_response (interface, dest_mac, &cco_dev_list_head);
        if (ret == LIBMSTAR_SUCCESS)
        {
            node = cco_dev_list_head;
            while (node)
            {
                libmstar_mac_bin_to_str(node->mac, mac);
                ShellCmdPrintf("No.%d: mac=%s \n", ++num , mac);
                node = node->next;
            }
            free_vs_cco_dev_cnf (cco_dev_list_head);
            return 0;
        }else
        {
            ShellCmdPrintf("error ! ret = %d\n", ret);
        }
    }
    else if (!strcmp (argv[3], "file"))
    {
        //char op_str[18] = { 0 };
        vs_file_access_cmd_t cmd;
        memset (&cmd, 0x0, sizeof (cmd));
        cmd.file_type = VS_FILE_ACCESS_REQ_FILE_TYPE_GENERAL_FILE;

        strcpy (interface, argv[1]);
        strcpy (dest_mac, argv[2]);

        if (!strcmp (argv[4], "read"))
        {
            if (!strcmp (argv[5], "/"))
                cmd.op = VS_FILE_ACCESS_REQ_OP_LIST_DIR;
            else
                cmd.op = VS_FILE_ACCESS_REQ_OP_READ;
        }
        else if (!strcmp (argv[4], "save"))
        {
            cmd.op = VS_FILE_ACCESS_REQ_OP_SAVE;
            cmd.file_type = VS_FILE_ACCESS_REQ_FILE_TYPE_GENERAL_FILE;
            strcpy (cmd.file_name, argv[6]);
        }
        else if (!strcmp (argv[4], "write"))
        {
            cmd.op = VS_FILE_ACCESS_REQ_OP_WRITE;
            strcpy (cmd.file_name, argv[6]);
        }
        else if (!strcmp (argv[4], "upgrade"))
        {
            cmd.op = VS_FILE_ACCESS_REQ_OP_WRITE;
            cmd.file_type = VS_FILE_ACCESS_REQ_FILE_TYPE_SIMAGE;
            strcpy (cmd.file_name, argv[6]);
        }
        strcpy (cmd.parameter, argv[5]);

        ret = libmstar_vs_file_access_req (interface, dest_mac, &cmd);
        if (ret != LIBMSTAR_SUCCESS)
        {
            ShellCmdPrintf ("%s %s fail ret = %d\n", argv[4], cmd.parameter, ret);
            return 0;
        }
        ShellCmdPrintf ("%s %s success:\n%s\n", argv[4], cmd.parameter, cmd.file_buf);
    }
    else if (!strcmp (argv[3], "parameter"))
    {
        libmstar_patameter_info_t patameter_info;

        if (argc < 8)
        {
            print_usage ();
        }

        strcpy (patameter_info.filename, argv[5]);
        strcpy (patameter_info.groupname, argv[6]);
        strcpy (patameter_info.parametername, argv[7]);

        if(!strcmp (argv[4], "write"))
        {
            patameter_info.parametertype = atoi (argv[8]);
            if (JSONArray == patameter_info.parametertype)
            {
                patameter_info.array_data_type = atoi (argv[9]);
                patameter_info.array_index     = atoi (argv[10]);
                strcpy (patameter_info.value, argv[11]);
            }
            else
            {
                strcpy (patameter_info.value, argv[9]);
            }
        }

        if (!strcmp (argv[4], "read"))
        {
            if (NULL == libmstar_conf_file_patameter_read (argv[1], argv[2],
                                                           argv[5], argv[6], argv[7]))
            {
                ShellCmdPrintf ("read fail\n");
            }
        }
        else if (!strcmp (argv[4], "write"))
        {
             if (0 != libmstar_conf_file_patameter_write (argv[1], argv[2],
			 	                                          &patameter_info))
            {
                ShellCmdPrintf ("write fail\n");
            }
        }
    }
    else if (!strcmp (argv[3], "reset"))
    {
        libmstar_vs_reset_cnf_t reset_cnf;
        strcpy (interface, argv[1]);
        strcpy (dest_mac, argv[2]);

        if (LIBMSTAR_SUCCESS == libmstar_vs_reset (interface, dest_mac, &reset_cnf)
            && LIBMSTAR_VS_RESET_CNF_RESULT_SUCCESS == reset_cnf.result)
        {
            ShellCmdPrintf ("reset %s success\n", dest_mac);
        }
        else
        {
            ShellCmdPrintf ("reset %s fail\n", dest_mac);
        }
    }
    else if (!strcmp (argv[3], "get_tonemask"))
    {
        libmstar_vs_get_tonemask_cnf_t tonemask_cnf;
        strcpy (interface, argv[1]);
        strcpy (dest_mac, argv[2]);
        int i = 0;

        if (LIBMSTAR_SUCCESS == libmstar_vs_get_tonemask (interface, dest_mac, argv[4], &tonemask_cnf)
            && LIBMSTAR_VS_GET_TONEMASK_CNF_RESULT_SUCCESS == tonemask_cnf.result)
        {
            ShellCmdPrintf ("get  %s  tonemask success\n", dest_mac);
            int l_len = 32;
            for (i = 0; i < 512; i++)
            {
                if (!(i % l_len))
                    ShellCmdPrintf ("%03d-%03d: ", i,
                            (i +  l_len- 1) > 512 ? 512 : i + l_len - 1);
                ShellCmdPrintf ("%02x ", tonemask_cnf.tonemask[i]);
                if ((i+1) % l_len == 0)
                    ShellCmdPrintf ("\n");
            }
        }
        else
        {
            ShellCmdPrintf ("get  %s  tonemask fail\n", dest_mac);
        }
    }
    else if (!strcmp (argv[3], "get_eth_phy"))
    {
        libmstar_vs_get_eth_phy_cnf_t eth_phy_info;
        strcpy (interface, argv[1]);
        strcpy (dest_mac, argv[2]);

        if (LIBMSTAR_SUCCESS == libmstar_vs_get_eth_phy (interface, dest_mac, &eth_phy_info)
            && LIBMSTAR_VS_GET_ETH_PHY_CNF_RESULT_SUCCESS == eth_phy_info.result)
        {
            ShellCmdPrintf ("get  %s  eth phy info success\n", dest_mac);
            if (eth_phy_info.link == 1)
            {
                ShellCmdPrintf ("eth phy link success\n");
            }
            else
            {
                ShellCmdPrintf ("eth phy link fail\n");
            }

            if (eth_phy_info.speed== 0)
            {
                ShellCmdPrintf ("eth phy link 10M\n");
            }
            else if (eth_phy_info.speed== 1)
            {
                ShellCmdPrintf ("eth phy link 100M\n");
            }
            else if (eth_phy_info.speed== 2)
            {
                ShellCmdPrintf ("eth phy link 1000M\n");
            }

            if (eth_phy_info.duplex == 1)
            {
                ShellCmdPrintf ("eth phy full duplex\n");
            }
            else
            {
                ShellCmdPrintf ("eth phy half duplex\n");
            }
        }
        else
        {
            ShellCmdPrintf ("get  %s  eth phy info fail\n", dest_mac);
        }
    }
    else if (!strcmp (argv[3], "eth_stats"))
    {
        libmstar_vs_eth_stats_cnf_t eth_stats;
        strcpy (interface, argv[1]);
        strcpy (dest_mac, argv[2]);
        u8 command = LIBMSTAR_VS_ETH_STATS_REQ_REQTYPE_GET;

        if (LIBMSTAR_SUCCESS == libmstar_vs_eth_stats (interface, dest_mac, command, &eth_stats)
            && LIBMSTAR_VS_ETH_STATS_CNF_RESULT_SUCCESS == eth_stats.result)
        {
            ShellCmdPrintf ("get  %s  eth stats success\n", dest_mac);
            ShellCmdPrintf ("get rx_packets = %x\n", eth_stats.rx_packets);
            ShellCmdPrintf ("get rx_good_packets = %x\n", eth_stats.rx_good_packets);
            ShellCmdPrintf ("get rx_good_unitcast_packets = %x\n", eth_stats.rx_good_unitcast_packets);
            ShellCmdPrintf ("get rx_good_multicast_packets = %x\n", eth_stats.rx_good_multicast_packets);
            ShellCmdPrintf ("get rx_good_broadcast_packets = %x\n", eth_stats.rx_good_broadcast_packets);
            ShellCmdPrintf ("get rx_error_packets = %x\n", eth_stats.rx_error_packets);
            ShellCmdPrintf ("get rx_fifo_overflow = %x\n", eth_stats.rx_fifo_overflow);
            ShellCmdPrintf ("get tx_packets = %x\n", eth_stats.tx_packets);
            ShellCmdPrintf ("get tx_good_packets = %x\n", eth_stats.tx_good_packets);
            ShellCmdPrintf ("get tx_good_unitcast_packets = %x\n", eth_stats.tx_good_unitcast_packets);
            ShellCmdPrintf ("get tx_good_multicast_packets = %x\n", eth_stats.tx_good_multicast_packets);
            ShellCmdPrintf ("get tx_good_broadcast_packets = %x\n", eth_stats.tx_good_broadcast_packets);
            ShellCmdPrintf ("get tx_error_packets = %x\n", eth_stats.tx_error_packets);
            ShellCmdPrintf ("get tx_fifo_underflow = %x\n", eth_stats.tx_fifo_underflow);
            ShellCmdPrintf ("get tx_collision = %x\n", eth_stats.tx_collision);
            ShellCmdPrintf ("get tx_carrier_error = %x\n", eth_stats.tx_carrier_error);
        }
        else
        {
            ShellCmdPrintf ("get  %s  eth stats fail\n", dest_mac);
        }
    }
    else if (!strcmp (argv[3], "get_status"))
    {
        libmstar_vs_get_status_cnf_t get_status;
        strcpy (interface, argv[1]);
        strcpy (dest_mac, argv[2]);

        if (LIBMSTAR_SUCCESS == libmstar_vs_get_status (interface, dest_mac, &get_status)
            && LIBMSTAR_VS_GET_STATUS_CNF_RESULT_SUCCESS == get_status.result)
        {
            ShellCmdPrintf ("get  %s  get status success\n", dest_mac);
            ShellCmdPrintf ("get status = %x\n", get_status.status);
            ShellCmdPrintf ("get cco = %x\n", get_status.cco);
            ShellCmdPrintf ("get preferred_cco = %x\n", get_status.preferred_cco);
            ShellCmdPrintf ("get backup_cco = %x\n", get_status.backup_cco);
            ShellCmdPrintf ("get proxy_cco = %x\n", get_status.proxy_cco);
            ShellCmdPrintf ("get simple_connect = %x\n", get_status.simple_connect);
            ShellCmdPrintf ("get link_state = %x\n", get_status.link_state);
            ShellCmdPrintf ("get ready_operation = %x\n", get_status.ready_operation);
            ShellCmdPrintf ("get freq_error = %lld\n", get_status.freq_error);
            ShellCmdPrintf ("get freq_offset = %lld\n", get_status.freq_offset);
            ShellCmdPrintf ("get uptime = %lld (s)\n", get_status.uptime);
            ShellCmdPrintf ("get authenticated_time = %lld (s)\n", get_status.authenticated_time);
            ShellCmdPrintf ("get authenticated_count = %d (times)\n", get_status.authenticated_count);
        }
        else
        {
            ShellCmdPrintf ("get  %s  get status fail\n", dest_mac);
        }
    }
    else if (!strcmp (argv[3], "get_tonemap"))
    {
        libmstar_vs_get_tonemap_req_t get_tonemap_req;
        libmstar_vs_get_tonemap_cnf_t get_tonemap_cnf;
        libmstar_vs_get_tonemap_cnf_t get_tonemap_cnf_tmp;
        strcpy (interface, argv[1]);
        strcpy (dest_mac, argv[2]);
        u8 carrier_list[4096] = { 0 };
        int i, j;
        int modulation_num;
        int carrier_star_index;
        int carrier_last_index;

        libmstar_mac_str_to_bin (argv[4], get_tonemap_req.Station_address);
        get_tonemap_req.tmi = 0xff;
        get_tonemap_req.int_id = 0xff;
        if (!strcmp (argv[5], "0") || !strcmp (argv[5], "tx"))
            get_tonemap_req.dir = 0;
        else
            get_tonemap_req.dir = 1;
        get_tonemap_req.carrier_group = 0xff;
        memset (&get_tonemap_cnf, 0x0, sizeof (libmstar_vs_get_tonemap_cnf_t));
        memset (&get_tonemap_cnf_tmp, 0x0, sizeof (libmstar_vs_get_tonemap_cnf_t));

        if (LIBMSTAR_SUCCESS == libmstar_vs_get_tonemap (interface, dest_mac,
                                                         &get_tonemap_req, &get_tonemap_cnf)
            && (0 < get_tonemap_cnf.tmi_length))
        {
            /* get Even numbers carrier */
            get_tonemap_req.tmi = get_tonemap_cnf.tmi_list[0];
            get_tonemap_req.int_id = get_tonemap_cnf.int_id;
            get_tonemap_req.carrier_group = 0;

            if (LIBMSTAR_SUCCESS
				== libmstar_vs_get_tonemap (interface, dest_mac,
                                            &get_tonemap_req, &get_tonemap_cnf_tmp)
                && LIBMSTAR_VS_GET_TONEMAP_CNF_RESULT_SUCCESS == get_tonemap_cnf_tmp.result)
            {
                if (get_tonemap_req.dir == 1)
                    ShellCmdPrintf ("att1 = %d\n", get_tonemap_cnf_tmp.tm_rx_gain + 20);

                modulation_num = get_tonemap_cnf_tmp.modulation_list_length / 8;
                if ((get_tonemap_cnf_tmp.modulation_list_length % 8) > 0)
                {
                   modulation_num = modulation_num + 1;
                }

                i = 0;
                j = 0;
                for (i = 0; i < modulation_num; i++)
                {
                    carrier_list[j * 2] = get_tonemap_cnf_tmp.modulation_list[i] & 0x0f;
                    carrier_list[(j + 1) * 2] = (get_tonemap_cnf_tmp.modulation_list[i] & 0xf0) >> 4;
                    j = j + 2;
                }
            }
            else
            {
                ShellCmdPrintf ("get tonemap fail\n");
                return -1;
            }

            /* get Odd number  carrier */
            get_tonemap_req.carrier_group = 1;
            memset (&get_tonemap_cnf_tmp, 0x0, sizeof (libmstar_vs_get_tonemap_cnf_t));

            if (LIBMSTAR_SUCCESS
				== libmstar_vs_get_tonemap (interface, dest_mac, &get_tonemap_req, &get_tonemap_cnf_tmp)
                && LIBMSTAR_VS_GET_TONEMAP_CNF_RESULT_SUCCESS == get_tonemap_cnf_tmp.result)
            {
                if (get_tonemap_req.dir == 1)
                    ShellCmdPrintf ("att2 = %d\n", get_tonemap_cnf_tmp.tm_rx_gain + 20);

                modulation_num = get_tonemap_cnf_tmp.modulation_list_length / 8;
                if ((get_tonemap_cnf_tmp.modulation_list_length % 8) > 0)
                {
                   modulation_num = modulation_num + 1;
                }
                i = 0;
                j = 0;
                for (i = 0; i < modulation_num; i++)
                {
                    carrier_list[(j * 2) + 1] = get_tonemap_cnf_tmp.modulation_list[i] & 0x0f;
                    carrier_list[((j + 1) * 2) + 1] = (get_tonemap_cnf_tmp.modulation_list[i] & 0xf0) >> 4;
                    j = j + 2;
                }
            }
            else
            {
                ShellCmdPrintf ("get tonemap fail\n");
                return -1;
            }

            for (i = 0; i < 4096; i++)
            {
                if (carrier_list[i] != 0)
                {
                    carrier_star_index = i;
                    break;
                }
            }

            for (i = 4095; i >=0 ; i--)
            {
                if (carrier_list[i] != 0)
                {
                    carrier_last_index = i;
                    break;
                }
            }

            ShellCmdPrintf ("carrier_star_index = %d\n", carrier_star_index);
            ShellCmdPrintf ("carrier_end_index = %d\n", carrier_last_index);

            int l_len = 32;
            for (i = 0; i < carrier_last_index; i++)
            {
                if (!(i % l_len))
                    ShellCmdPrintf ("%04d-%04d: ", i,
                            (i + l_len - 1) > carrier_last_index ? carrier_last_index : i + l_len - 1);
                ShellCmdPrintf ("%d ", carrier_list[i]);

                if (!((i + 1) % l_len)) ShellCmdPrintf ("\n");
            }
            ShellCmdPrintf ("\n");
        }
        else
        {
            ShellCmdPrintf ("get tonemap fail\n");
            return -1;
        }
    }
    else if (!strcmp (argv[3], "get_snr"))
    {
        libmstar_vs_get_snr_cnf_t get_snr_cnf;
        libmstar_vs_get_snr_req_t get_snr_req;
        strcpy (interface, argv[1]);
        strcpy (dest_mac, argv[2]);
        int i = 0;

        libmstar_mac_str_to_bin (argv[4], get_snr_req.Station_address);
        get_snr_req.int_index = atoi (argv[5]);
        get_snr_req.int_id = atoi (argv[6]);
        get_snr_req.carrier_group = atoi (argv[7]);

        ShellCmdPrintf ("ret %d\n", ret);
        ShellCmdPrintf ("get_snr_cnf.result %d\n", get_snr_cnf.result);
        if (LIBMSTAR_SUCCESS == libmstar_vs_get_snr (interface, dest_mac, &get_snr_req, &get_snr_cnf)
            && LIBMSTAR_VS_GET_STATUS_CNF_RESULT_SUCCESS == get_snr_cnf.result)
        {
            ShellCmdPrintf ("get  %s  snr success\n", dest_mac);
            ShellCmdPrintf ("get int_id = %x\n", get_snr_cnf.int_id);
            ShellCmdPrintf ("get intervals_nb = %x\n", get_snr_cnf.intervals_nb);
            for (i = 0; i < get_snr_cnf.intervals_nb; i++)
            {
                ShellCmdPrintf ("get int_et%d = %x\n", i, get_snr_cnf.int_et[i]);
            }

            ShellCmdPrintf ("get tm_ber = %x\n", get_snr_cnf.tm_ber);
            ShellCmdPrintf ("get carrier_gr = %x\n", get_snr_cnf.carrier_gr);
            for (i = 0; i < 1024; i++)
            {
                ShellCmdPrintf ("%x ", get_snr_cnf.snr[i]);
                if (i % 32 == 0)
                    ShellCmdPrintf ("\n");
            }

        }
        else
        {
            ShellCmdPrintf ("get  %s  snr fail\n", dest_mac);
        }
    }
    else if (!strcmp (argv[3], "get_link_stats"))
    {
        libmstar_vs_get_link_stats_cnf_t get_link_stats_cnf;
        libmstar_vs_get_link_stats_req_t get_link_stats_req;
        strcpy (interface, argv[1]);
        strcpy (dest_mac, argv[2]);

        get_link_stats_req.ReqType = atoi (argv[4]);
        get_link_stats_req.ReqID = atoi (argv[5]);
        get_link_stats_req.lid = atoi (argv[6]);
        get_link_stats_req.TLFlag = atoi (argv[7]);
        get_link_stats_req.Mgmt_Flag = atoi (argv[8]);
        libmstar_mac_str_to_bin (argv[9], get_link_stats_req.dasa);

        ShellCmdPrintf ("ret  %d\n", ret);
        ShellCmdPrintf ("get_link_stats_cnf.result %d\n", get_link_stats_cnf.result);
        ShellCmdPrintf ("libmstar_vs_get_link_stats_req_t %lu\n", sizeof (libmstar_vs_get_link_stats_req_t));

        if (LIBMSTAR_SUCCESS
            == libmstar_vs_get_link_stats (interface, dest_mac, &get_link_stats_req, &get_link_stats_cnf)
            && LIBMSTAR_VS_GET_LINK_STATS_CNF_RESULT_SUCCESS == get_link_stats_cnf.result)
        {
            ShellCmdPrintf ("get  %s  link stats success\n", dest_mac);
            ShellCmdPrintf ("get ReqID = %x\n", get_link_stats_cnf.ReqID);

            if (LIBMSTAR_VS_GET_LINK_STATS_REQ_REQTYPE_GET_STAT == get_link_stats_req.ReqType
                || LIBMSTAR_VS_GET_LINK_STATS_REQ_REQTYPE_GET_RESET_STAT == get_link_stats_req.ReqType)
            {
                if (LIBMSTAR_VS_GET_LINK_STATS_REQ_TLFLAG_TX_LINK == get_link_stats_req.TLFlag)
                {
                    ShellCmdPrintf ("get statistics_start_date = %llx\n", get_link_stats_cnf.mfs_tx.statistics_start_date);
                    ShellCmdPrintf ("get num_segs_suc = %x\n", get_link_stats_cnf.mfs_tx.num_segs_suc);
                    ShellCmdPrintf ("get num_segs_dropped = %x\n", get_link_stats_cnf.mfs_tx.num_segs_dropped);
                    ShellCmdPrintf ("get num_pbs = %x\n", get_link_stats_cnf.mfs_tx.num_pbs);
                    ShellCmdPrintf ("get num_mpdus = %x\n", get_link_stats_cnf.mfs_tx.num_mpdus);
                    ShellCmdPrintf ("get num_bursts = %x\n", get_link_stats_cnf.mfs_tx.num_bursts);
                    ShellCmdPrintf ("get num_sacks = %x\n", get_link_stats_cnf.mfs_tx.num_sacks);
                    ShellCmdPrintf ("get num_bad_pbs_crc = %x\n", get_link_stats_cnf.mfs_tx.num_bad_pbs_crc);
                    ShellCmdPrintf ("get num_mpdus_coll = %x\n", get_link_stats_cnf.mfs_tx.num_mpdus_coll);
                    ShellCmdPrintf ("get num_mpdus_fail = %x\n", get_link_stats_cnf.mfs_tx.num_mpdus_fail);
                    ShellCmdPrintf ("get num_total_seg = %llx\n", get_link_stats_cnf.mfs_tx.num_total_seg);
                    ShellCmdPrintf ("get num_tx_buf_shortage_drop = %x\n", get_link_stats_cnf.mfs_tx.num_tx_buf_shortage_drop);
                }
                else if (LIBMSTAR_VS_GET_LINK_STATS_REQ_TLFLAG_RX_LINK == get_link_stats_req.TLFlag)
                {
                    ShellCmdPrintf ("get statistics_start_date = %llx\n", get_link_stats_cnf.mfs_rx.statistics_start_date);
                    ShellCmdPrintf ("get num_segs_suc = %x\n", get_link_stats_cnf.mfs_rx.num_segs_suc);
                    ShellCmdPrintf ("get num_pbs = %x\n", get_link_stats_cnf.mfs_rx.num_pbs);
                    ShellCmdPrintf ("get num_mpdus = %x\n", get_link_stats_cnf.mfs_rx.num_mpdus);
                    ShellCmdPrintf ("get num_bursts = %x\n", get_link_stats_cnf.mfs_rx.num_bursts);
                    ShellCmdPrintf ("get num_bad_pbs_crc = %x\n", get_link_stats_cnf.mfs_rx.num_bad_pbs_crc);
                    ShellCmdPrintf ("get num_sacks = %x\n", get_link_stats_cnf.mfs_rx.num_sacks);
                    ShellCmdPrintf ("get num_ssn_under_min = %x\n", get_link_stats_cnf.mfs_rx.num_ssn_under_min);
                    ShellCmdPrintf ("get num_ssn_over_max = %x\n", get_link_stats_cnf.mfs_rx.num_ssn_over_max);
                }
            }
        }
        else
        {
            ShellCmdPrintf ("get  %s  link stats fail\n", dest_mac);
        }
    }
    else if (!strcmp (argv[3], "nw_system_info"))
    {
        libmstar_vs_get_nw_info_cnf_t get_nw_info_cnf;
        libmstar_vs_diag_debug_req_t diag_debug_req;
        libmstar_vs_diag_debug_cnf_t diag_debug_cnf;
        strcpy (interface, argv[1]);
        strcpy (dest_mac, argv[2]);
        int i = 0;
        char tmp_mac[18];

        if (LIBMSTAR_SUCCESS == libmstar_vs_get_nw_info (interface, dest_mac, &get_nw_info_cnf))
        {
            ShellCmdPrintf ("get  %s  nw info success\n", dest_mac);

            ShellCmdPrintf ("get num_stas %x\n", get_nw_info_cnf.num_stas);
            ShellCmdPrintf ("No tei     mac                    PHY TX(C/R)       PHY RX(C/R)     AGC gain\n");
            for (i = 0; i < get_nw_info_cnf.num_stas; i++)
            {
                libmstar_mac_bin_to_str(get_nw_info_cnf.sta_info[i].sta_mac,
                                        tmp_mac);
                ShellCmdPrintf ("%d  %d       %s      %d/%d            %d/%d         %d\n",
                        i + 1,
                        get_nw_info_cnf.sta_info[i].sta_tei,
                        tmp_mac,
                        get_nw_info_cnf.sta_info[i].phy_tx_coded,
                        get_nw_info_cnf.sta_info[i].phy_tx_raw,
                        get_nw_info_cnf.sta_info[i].phy_rx_coded,
                        get_nw_info_cnf.sta_info[i].phy_rx_raw,
                        get_nw_info_cnf.sta_info[i].agc_gain);
            }

            ShellCmdPrintf ("\n");
            ShellCmdPrintf ("Get Device Time\n");

            ShellCmdPrintf ("No tei     mac                 system time       auth times     online time    unauth time\n");


            if (0 < get_nw_info_cnf.num_stas)
            {
                libmstar_mac_bin_to_str (get_nw_info_cnf.cco_mac, tmp_mac);
                diag_debug_req.command = LIBMSTAR_VS_DIAG_DEBUG_REQ_SUBTYPE_GET_SYS_INFO;

                ret = libmstar_vs_diag_debug (interface, tmp_mac, &diag_debug_req,
                                              &diag_debug_cnf);

                ShellCmdPrintf ("%d  %d       %s    %6d            %4d           %6d         %6d\n",
                        1, get_nw_info_cnf.cco_tei, tmp_mac,
                        diag_debug_cnf.systime, diag_debug_cnf.authenticated_times,
                        diag_debug_cnf.auth_time, diag_debug_cnf.unauth_time);

                /*  if dest_mac is slave */
                if (1 == get_nw_info_cnf.sta_info[0].sta_tei)
                {
                    diag_debug_req.command = LIBMSTAR_VS_DIAG_DEBUG_REQ_SUBTYPE_GET_SYS_INFO;

                    ret = libmstar_vs_diag_debug (interface, dest_mac, &diag_debug_req,
                                                  &diag_debug_cnf);

                    ShellCmdPrintf ("%d  %d       %s    %6d            %4d           %6d         %6d\n",
                            2, get_nw_info_cnf.cco_tei, dest_mac,
                            diag_debug_cnf.systime, diag_debug_cnf.authenticated_times,
                            diag_debug_cnf.auth_time, diag_debug_cnf.unauth_time);
			ShellCmdPrintf("111111dest_mac is slave\n");
                }
                else
                {
                    for (i = 0; i < get_nw_info_cnf.num_stas; i++)
                    {
                        libmstar_mac_bin_to_str (get_nw_info_cnf.sta_info[i].sta_mac, tmp_mac);
                        diag_debug_req.command = LIBMSTAR_VS_DIAG_DEBUG_REQ_SUBTYPE_GET_SYS_INFO;

                        ret = libmstar_vs_diag_debug (interface, tmp_mac, &diag_debug_req,
                                                      &diag_debug_cnf);

                        ShellCmdPrintf ("%d  %d       %s    %6d            %4d           %6d         %6d\n",
                                i + 2, get_nw_info_cnf.cco_tei, tmp_mac,
                                diag_debug_cnf.systime, diag_debug_cnf.authenticated_times,
                                diag_debug_cnf.auth_time, diag_debug_cnf.unauth_time);
                    }
			ShellCmdPrintf("0000000dest_mac is master\n");		
                }
            }
            else
            {
                diag_debug_req.command = LIBMSTAR_VS_DIAG_DEBUG_REQ_SUBTYPE_GET_SYS_INFO;

                ret = libmstar_vs_diag_debug (interface, dest_mac, &diag_debug_req,
                                              &diag_debug_cnf);

                ShellCmdPrintf ("%d  %d       %s    %6d            %4d           %6d         %6d\n",
                        1, get_nw_info_cnf.cco_tei, dest_mac,
                        diag_debug_cnf.systime, diag_debug_cnf.authenticated_times,
                        diag_debug_cnf.auth_time, diag_debug_cnf.unauth_time);
			ShellCmdPrintf("0000000get_nw_info_cnf.num_stas=0\n");	
            }
        }
        else
        {
            ShellCmdPrintf ("get  %s  get_nw_info fail\n", dest_mac);
        }
    }
    else if (!strcmp(argv[3], "set_mac"))
    {
        libmstar_vs_product_req_t product_req = { 0 };
        libmstar_vs_product_cnf_t product_cnf = { 0 };

        strcpy (interface, argv[1]);
        strcpy (dest_mac, argv[2]);

        product_req.moduleid = 1 << MODULE_SET_MAC | 1 << MODULE_REBOOT;
        
        if (LIBMSTAR_SUCCESS != check_mac_format(argv[4]))
        {
            ShellCmdPrintf("please check mac format!\n");
            return 0;
        }
        libmstar_mac_str_to_bin(argv[4], product_req.data);
        if(LIBMSTAR_SUCCESS == libmstar_vs_product (interface, dest_mac, &product_req, &product_cnf))
        {
            if (product_cnf.mstatus == LIBMSTAR_VS_PRODUCT_CNF_STATUS_SUCCESS)
            {
                ShellCmdPrintf ("set_mac %s success!\n", dest_mac);
            }
            else
            {
                ShellCmdPrintf("set_mac %s fail!\n", dest_mac);
            }
            
        }
        else
        {
            ShellCmdPrintf("set_mac %s fail!\n", dest_mac);
        }
        
    }
    else if (!strcmp(argv[3], "diag_debug"))
    {
        libmstar_vs_diag_debug_req_t diag_debug_req;
        libmstar_vs_diag_debug_cnf_t diag_debug_cnf;
        strcpy (interface, argv[1]);
        strcpy (dest_mac, argv[2]);
        u16 subtype = atoi(argv[4]);
        if (subtype > LIBMSTAR_VS_DIAG_DEBUG_REQ_SUBTYPE_NB )
            ShellCmdPrintf("subtype not support!\n");
        diag_debug_req.command = subtype;

        if (subtype == LIBMSTAR_VS_DIAG_DEBUG_REQ_SUBTYPE_SNIFFER_SET)
        {
            u8 action = atoi(argv[5]);
            if (LIBMSTAR_SUCCESS != check_mac_format(argv[6]) || action > LIBMSTAR_VS_DIAG_DEBUG_REQ_ACTION_NB)
            {
                ShellCmdPrintf("please check mac format or action!\n");
                return 0;
            }
            libmstar_mac_str_to_bin (argv[6], diag_debug_req.sniffer_mac);
            diag_debug_req.action = action;
        }

        int ret = libmstar_vs_diag_debug (interface, dest_mac, &diag_debug_req, &diag_debug_cnf);
        if (ret == LIBMSTAR_SUCCESS && diag_debug_cnf.result == LIBMSTAR_VS_DIAG_DEBUG_CNF_RESULT_SUCCESS)
            ShellCmdPrintf ("operation success!\n");
        else
        {
            ShellCmdPrintf ("operation failed!\n");
        }
    }
	
    else
    {
        ShellCmdPrintf ("Not Support %s\n", argv[3]);
    }

    return 0;
}
