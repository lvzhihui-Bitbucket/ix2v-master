#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "libmstar_eoc.h"
#include "endian.h"
#include "parson.h"
#include "libmme.h"

char mac_zero_str[MAC_STR_MAX_LEN] = "00:00:00:00:00:00";

int calc_xor_checksum (unsigned char *buf, unsigned int size,
                            unsigned int *chksum)
{
    unsigned char res[4] = {0};
    unsigned char cur[4];
    unsigned int offset = 0;
    if (size > 4)
    {
        size = size / 4 * 4;
    }

    if (size & 3)
    {
        *chksum = 0xffffffff;
        return -1;
    }

	if (*chksum != 0)
	{
		memcpy (res, (u8 *)chksum, sizeof(u32));
		res[0] = ~res[0];
		res[1] = ~res[1];
		res[2] = ~res[2];
		res[3] = ~res[3];
	}

    while (size)
    {
        memcpy (cur, buf + offset, 4);
        res[0] ^= cur[0];
        res[1] ^= cur[1];
        res[2] ^= cur[2];
        res[3] ^= cur[3];
        offset += 4;
        size -= 4;
    }
    res[0] = ~res[0];
    res[1] = ~res[1];
    res[2] = ~res[2];
    res[3] = ~res[3];
    memcpy (chksum, res, 4);

    return 0;
}

libmstar_error_t
check_mac_format (const char *mac_addr)
{
    int i;

    if (mac_addr == NULL)
       return LIBMSTAR_ERROR_PARAM;

    for (i=0; i < MAC_STR_MAX_LEN - 1; i++)
    {
        if (!((mac_addr[i] >= '0' && mac_addr[i] <= '9') ||
              (mac_addr[i] >= 'A' && mac_addr[i] <= 'F') ||
              (mac_addr[i] >= 'a' && mac_addr[i] <= 'f') || mac_addr[i] == ':'))
            return LIBMSTAR_ERROR_MAC;

        if ((i % 3) == 2 && mac_addr[i] != ':')
            return LIBMSTAR_ERROR_MAC;
    }
    if (!strcmp (mac_addr, mac_zero_str))
        return LIBMSTAR_ERROR_MAC;

    return LIBMSTAR_SUCCESS;
}
/**
 * Convert a MAC address in string format aa:bb:cc:dd:ee:ff into a
 * MAC address in binary format (6 bytes).<br>
 * The array to store the binary MAC address needs to be provided.<br>
 * Example : "00:23:54:ae:f5:24" ---> { 0x00, 0x23, 0x54, 0xae, 0xf5, 0x24 }
 *
 * \param str MAC address string
 * \param bin MAC address returned in binary format
 * \return error type (LIBMSTAR_SUCCESS if success)
 * \return LIBMSTAR_ERROR_PARAM: bad input parameters
 */

libmstar_error_t
libmstar_mac_str_to_bin (const char *str, unsigned char *bin)
{
    int i;
    char *s, *e;

    if ((NULL == bin) || (NULL == str))
    {
        return LIBMSTAR_ERROR_PARAM;
    }

    s = (char *) str;
    for (i = 0; i < 6; ++i)
    {
        bin[i] = s ? strtoul (s, &e, 16) : 0;
        if (s)
            s = (*e) ? e + 1 : e;
    }

    return LIBMSTAR_SUCCESS;
}

/**
 * Convert a MAC address in binary format (6 bytes) into a MAC address
 * in string format aa:bb:cc:dd:ee:ff.<br>
 * The array to store the string MAC address needs to be provided with minimal
 * size of 18 bytes (LIBMSTAR_MAC_STR_LEN).<br>
 * Example : { 0x00, 0x23, 0x54, 0xae, 0xf5, 0x24 } ---> "00:23:54:ae:f5:24"
 *
 * \param bin MAC address returned in binary format
 * \param str MAC address string
 * \return error type (LIBMSTAR_SUCCESS if success)
 * \return LIBMSTAR_ERROR_PARAM: bad input parameters
 */

libmstar_error_t
libmstar_mac_bin_to_str (const unsigned char *bin, char *str)
{
    if ((NULL == bin) || (NULL == str))
    {
        return LIBMSTAR_ERROR_PARAM;
    }

    sprintf (str, "%2.2x:%2.2x:%2.2x:%2.2x:%2.2x:%2.2x",
             bin[0], bin[1], bin[2], bin[3], bin[4], bin[5]);

    return LIBMSTAR_SUCCESS;
}

libmstar_error_t
libmstar_vs_get_mst_version (char *interface, char *dest_mac, char *version)
{
    unsigned int result_len = 0;
    char sw_ver[128] = { 0 };
    unsigned char tmp[100] = { 0 };
    unsigned char dest_bin[MAC_BIN_LEN] = { 0 };
    unsigned char snd_buffer[ETH_DATA_LEN] = { 0 };
    mme_ctx_t request_ctx, confirm_ctx;

    if (NULL == dest_mac)
    {
		return LIBMSTAR_ERROR_PARAM;
    }

    libmstar_mac_str_to_bin (dest_mac, dest_bin);

    /* initialize indication MME for VS_GET_VERSION_REG/CNF */
    if (MME_SUCCESS != mme_init (&request_ctx, VS_GET_VERSION | MME_REQ,
                                 snd_buffer, (unsigned int)ETH_DATA_LEN))
    {
        return LIBMSTAR_ERROR_MME;
    }

    if (MME_SUCCESS  !=  mme_init (&confirm_ctx, VS_GET_VERSION | MME_CNF,
                                   snd_buffer, (unsigned int)ETH_DATA_LEN))
    {
		return LIBMSTAR_ERROR_MME;
    }

    /* send indication MME */
    if (MME_SUCCESS != mme_send (&request_ctx, MME_SEND_REQ_CNF, interface,
                                 dest_bin, &confirm_ctx, MME_MMV1, SPIDCOM_OUI))
    {
        return LIBMSTAR_ERROR_MME;
    }

    /* get cnf parameter data */
    if (MME_SUCCESS != mme_pull (&confirm_ctx, tmp, 1, &result_len))
    {
        return LIBMSTAR_ERROR_MME;
    }

    if (MME_SUCCESS != mme_pull (&confirm_ctx, tmp, 19, &result_len))
    {
        return LIBMSTAR_ERROR_MME;
    }

    if (MME_SUCCESS != mme_pull (&confirm_ctx, sw_ver, 64, &result_len))
    {
        return LIBMSTAR_ERROR_MME;
    }

    libmstar_mac_bin_to_str (dest_bin, dest_mac);

    strcpy (version, sw_ver);

    return LIBMSTAR_SUCCESS;
}

libmstar_error_t
libmstar_vs_reset (char *interface, char *dest_mac,
                        libmstar_vs_reset_cnf_t *reset_cnf)
{
    unsigned int result_len = 0;
    unsigned char dest_bin[MAC_BIN_LEN] = { 0 };
    unsigned char snd_buffer[ETH_DATA_LEN] = { 0 };
    mme_ctx_t request_ctx, confirm_ctx;

    if (NULL == dest_mac)
    {
		return LIBMSTAR_ERROR_PARAM;
    }

    libmstar_mac_str_to_bin (dest_mac, dest_bin);

    /* initialize indication MME for VS_GET_VERSION_REG/CNF */
    if (MME_SUCCESS != mme_init (&request_ctx, VS_RESET | MME_REQ,
                                 snd_buffer, (unsigned int)ETH_DATA_LEN))
    {
        return LIBMSTAR_ERROR_MME;
    }

    if (MME_SUCCESS  !=  mme_init (&confirm_ctx, VS_RESET | MME_CNF,
                                   snd_buffer, (unsigned int)ETH_DATA_LEN))
    {
		return LIBMSTAR_ERROR_MME;
    }

    /* send indication MME */
    if (MME_SUCCESS != mme_send (&request_ctx, MME_SEND_REQ_CNF, interface,
                                 dest_bin, &confirm_ctx, MME_MMV1, SPIDCOM_OUI))
    {
        return LIBMSTAR_ERROR_MME;
    }

    /* get cnf parameter data */
    if (MME_SUCCESS != mme_pull (&confirm_ctx, reset_cnf,
                                 sizeof (libmstar_vs_reset_cnf_t), &result_len))
    {
        return LIBMSTAR_ERROR_MME;
    }

    return LIBMSTAR_SUCCESS;
}

libmstar_error_t
libmstar_vs_get_tonemask (char *interface, char *dest_mac, char *peer_mac,
                                 libmstar_vs_get_tonemask_cnf_t *tonemask)
{
    unsigned int result_len = 0;
    unsigned char dest_bin[MAC_BIN_LEN] = { 0 };
    unsigned char peer_bin[MAC_BIN_LEN] = { 0 };
    unsigned char snd_buffer[ETH_DATA_LEN] = { 0 };
    mme_ctx_t request_ctx, confirm_ctx;

    if (NULL == dest_mac || NULL == peer_mac)
    {
        return LIBMSTAR_ERROR_PARAM;
    }

    libmstar_mac_str_to_bin (dest_mac, dest_bin);
    libmstar_mac_str_to_bin (peer_mac, peer_bin);

    /* initialize indication MME for VS_GET_VERSION_REG */
    if (MME_SUCCESS != mme_init (&request_ctx, VS_GET_TONEMASK | MME_REQ,
                                 snd_buffer, (unsigned int)ETH_DATA_LEN))
    {
        return LIBMSTAR_ERROR_PARAM;
    }

    if (MME_SUCCESS != mme_init (&confirm_ctx, VS_GET_TONEMASK | MME_CNF,
                                 snd_buffer, (unsigned int)ETH_DATA_LEN))
    {
        return LIBMSTAR_ERROR_PARAM;
    }

    if (MME_SUCCESS != mme_put (&request_ctx, peer_bin, 6, &result_len))
    {
        return LIBMSTAR_ERROR_PARAM;
    }

    /* send indication MME */
    if (MME_SUCCESS != mme_send (&request_ctx, MME_SEND_REQ_CNF, interface,
                                 dest_bin, &confirm_ctx, MME_MMV1, SPIDCOM_OUI))
    {
        return LIBMSTAR_ERROR_PARAM;
    }

    if (MME_SUCCESS != mme_pull (&confirm_ctx, tonemask,
                                 sizeof (libmstar_vs_get_tonemask_cnf_t), &result_len))
    {
        return LIBMSTAR_ERROR_PARAM;
    }

    return LIBMSTAR_SUCCESS;
}

libmstar_error_t
libmstar_vs_get_eth_phy (char *interface, char *dest_mac,
                                libmstar_vs_get_eth_phy_cnf_t *eth_phy)
{
    unsigned int result_len = 0;
    unsigned char dest_bin[MAC_BIN_LEN] = { 0 };
    mme_ctx_t request_ctx, confirm_ctx;
    unsigned char snd_buffer[ETH_DATA_LEN] = { 0 };

    if (NULL == dest_mac || NULL == eth_phy)
    {
        return LIBMSTAR_ERROR_PARAM;
    }

    libmstar_mac_str_to_bin (dest_mac, dest_bin);

    /* initialize indication MME for VS_GET_VERSION_REG */
    if (MME_SUCCESS != mme_init (&request_ctx, VS_GET_ETH_PHY | MME_REQ,
                                 snd_buffer, (unsigned int)ETH_DATA_LEN))
    {
        return LIBMSTAR_ERROR_PARAM;
    }

    if (MME_SUCCESS != mme_init (&confirm_ctx, VS_GET_ETH_PHY | MME_CNF,
                                 snd_buffer, (unsigned int)ETH_DATA_LEN))
    {
        return LIBMSTAR_ERROR_PARAM;
    }

    /* send indication MME */
    if (MME_SUCCESS != mme_send (&request_ctx, MME_SEND_REQ_CNF, interface,
                                 dest_bin, &confirm_ctx, MME_MMV1, SPIDCOM_OUI))
    {
        return LIBMSTAR_ERROR_PARAM;
    }

	if (MME_SUCCESS != mme_pull (&confirm_ctx, eth_phy,
                                 sizeof (libmstar_vs_get_eth_phy_cnf_t), &result_len))
	{
	    return LIBMSTAR_ERROR_PARAM;
	}

    return LIBMSTAR_SUCCESS;
}

libmstar_error_t
libmstar_vs_eth_stats (char *interface, char *dest_mac,
                       u8 command, libmstar_vs_eth_stats_cnf_t *eth_stats)
{
    unsigned int result_len = 0;
    unsigned char dest_bin[MAC_BIN_LEN] = { 0 };
    mme_ctx_t request_ctx, confirm_ctx;
    unsigned char snd_buffer[ETH_DATA_LEN] = { 0 };

    if (NULL == dest_mac)
    {
        return LIBMSTAR_ERROR_PARAM;
    }

    libmstar_mac_str_to_bin (dest_mac, dest_bin);

    /* initialize indication MME for VS_GET_VERSION_REG */
    if (MME_SUCCESS != mme_init (&request_ctx, VS_ETH_STATS | MME_REQ,
                                 snd_buffer, (unsigned int)ETH_DATA_LEN))
    {
        return LIBMSTAR_ERROR_PARAM;
    }

    if (MME_SUCCESS != mme_init (&confirm_ctx, VS_ETH_STATS | MME_CNF,
                                 snd_buffer, (unsigned int)ETH_DATA_LEN))
    {
        return LIBMSTAR_ERROR_PARAM;
    }

    if (MME_SUCCESS != mme_put (&request_ctx, &command, 1, &result_len))
	{
	    return LIBMSTAR_ERROR_PARAM;
	}

    /* send indication MME */
    if (MME_SUCCESS != mme_send (&request_ctx, MME_SEND_REQ_CNF, interface,
                                 dest_bin, &confirm_ctx, MME_MMV1, SPIDCOM_OUI))
    {
        return LIBMSTAR_ERROR_PARAM;
    }

    if (MME_SUCCESS != mme_pull (&confirm_ctx, eth_stats,
                                 sizeof (libmstar_vs_eth_stats_cnf_t), &result_len))
    {
        return LIBMSTAR_ERROR_PARAM;
    }

    eth_stats->rx_packets = LE32TOH(eth_stats->rx_packets);
    eth_stats->rx_good_packets = LE32TOH(eth_stats->rx_good_packets);
    eth_stats->rx_good_unitcast_packets = LE32TOH(eth_stats->rx_good_unitcast_packets);
    eth_stats->rx_good_multicast_packets = LE32TOH(eth_stats->rx_good_multicast_packets);
    eth_stats->rx_good_broadcast_packets = LE32TOH(eth_stats->rx_good_broadcast_packets);
    eth_stats->rx_error_packets = LE32TOH(eth_stats->rx_error_packets);
    eth_stats->rx_fifo_overflow = LE32TOH(eth_stats->rx_fifo_overflow);
    eth_stats->tx_packets = LE32TOH(eth_stats->tx_packets);
    eth_stats->tx_good_packets = LE32TOH(eth_stats->tx_good_packets);
    eth_stats->tx_good_unitcast_packets = LE32TOH(eth_stats->tx_good_unitcast_packets);
    eth_stats->tx_good_multicast_packets = LE32TOH(eth_stats->tx_good_multicast_packets);
    eth_stats->tx_good_broadcast_packets = LE32TOH(eth_stats->tx_good_broadcast_packets);
    eth_stats->tx_error_packets = LE32TOH(eth_stats->tx_error_packets);
    eth_stats->tx_fifo_underflow = LE32TOH(eth_stats->tx_fifo_underflow);
    eth_stats->tx_collision = LE32TOH(eth_stats->tx_collision);
    eth_stats->tx_carrier_error = LE32TOH(eth_stats->tx_carrier_error);

    return LIBMSTAR_SUCCESS;
}

libmstar_error_t
libmstar_vs_get_status (char *interface, char *dest_mac,
                        libmstar_vs_get_status_cnf_t *status)
{
    unsigned int result_len = 0;
    unsigned char dest_bin[MAC_BIN_LEN] = { 0 };
    mme_ctx_t request_ctx, confirm_ctx;
    unsigned char snd_buffer[ETH_DATA_LEN] = { 0 };

    if (NULL == dest_mac || NULL == status)
    {
        return LIBMSTAR_ERROR_PARAM;
    }

    libmstar_mac_str_to_bin (dest_mac, dest_bin);

    /* initialize indication MME for VS_GET_VERSION_REG */
    if (MME_SUCCESS != mme_init (&request_ctx, VS_GET_STATUS | MME_REQ,
                                 snd_buffer, (unsigned int)ETH_DATA_LEN))
    {
        return LIBMSTAR_ERROR_PARAM;
    }

    if (MME_SUCCESS != mme_init (&confirm_ctx, VS_GET_STATUS | MME_CNF,
                                 snd_buffer, (unsigned int)ETH_DATA_LEN))
    {
        return LIBMSTAR_ERROR_PARAM;
    }

    /* send indication MME */
    if (MME_SUCCESS != mme_send (&request_ctx, MME_SEND_REQ_CNF, interface,
                                 dest_bin, &confirm_ctx, MME_MMV1, SPIDCOM_OUI))
    {
        return LIBMSTAR_ERROR_PARAM;
    }

    if (MME_SUCCESS != mme_pull (&confirm_ctx, status,
                                 sizeof (libmstar_vs_get_status_cnf_t), &result_len))
    {
        return LIBMSTAR_ERROR_PARAM;
    }

    status->freq_error = LE64TOH(status->freq_error);
    status->freq_offset = LE64TOH(status->freq_offset);
    status->uptime = LE64TOH(status->uptime);
    status->authenticated_time = LE64TOH(status->authenticated_time);
    status->authenticated_count = LE16TOH(status->authenticated_count);

    return LIBMSTAR_SUCCESS;
}

libmstar_error_t
libmstar_vs_get_tonemap (char *interface, char *dest_mac,
                         libmstar_vs_get_tonemap_req_t *tonemap_req,
                         libmstar_vs_get_tonemap_cnf_t *tonemap_cnf)
{
    int i, ret = LIBMSTAR_SUCCESS;
    unsigned int result_len = 0;
    unsigned char dest_bin[MAC_BIN_LEN] = { 0 };
    unsigned int  modulation_num;
    mme_ctx_t request_ctx, confirm_ctx;
    unsigned char snd_buffer[ETH_DATA_LEN] = { 0 };

    if (NULL == dest_mac || NULL == tonemap_req || NULL == tonemap_cnf)
    {
        return LIBMSTAR_ERROR_PARAM;
    }

    libmstar_mac_str_to_bin (dest_mac, dest_bin);

    /* initialize indication MME for VS_GET_VERSION_REG */
    if (MME_SUCCESS != mme_init (&request_ctx, VS_GET_TONEMAP | MME_REQ,
                                 snd_buffer, (unsigned int)ETH_DATA_LEN))
    {
        return LIBMSTAR_ERROR_PARAM;
    }

    if (MME_SUCCESS != mme_init (&confirm_ctx, VS_GET_TONEMAP | MME_CNF,
                                 snd_buffer, (unsigned int)ETH_DATA_LEN))
    {
        return LIBMSTAR_ERROR_PARAM;
    }

    if (MME_SUCCESS != mme_put (&request_ctx, tonemap_req,
                                sizeof (libmstar_vs_get_tonemap_req_t), &result_len))
    {
        return LIBMSTAR_ERROR_PARAM;
    }

    /* send indication MME */
    if (MME_SUCCESS != mme_send (&request_ctx, MME_SEND_REQ_CNF, interface,
                                 dest_bin, &confirm_ctx, MME_MMV1, SPIDCOM_OUI))
    {
        return LIBMSTAR_ERROR_PARAM;
    }

    if (MME_SUCCESS != mme_pull (&confirm_ctx, &tonemap_cnf->result, 1, &result_len))
    {
        return LIBMSTAR_ERROR_PARAM;
    }

    if (MME_SUCCESS != mme_pull (&confirm_ctx, tonemap_cnf->Reserved, 4, &result_len))
    {
        return LIBMSTAR_ERROR_PARAM;
    }

    if (MME_SUCCESS != mme_pull (&confirm_ctx, &tonemap_cnf->int_id, 1, &result_len))
    {
        return LIBMSTAR_ERROR_PARAM;
    }

    if (MME_SUCCESS != mme_pull (&confirm_ctx, &tonemap_cnf->tmi_default, 1, &result_len))
    {
        return LIBMSTAR_ERROR_PARAM;
    }

    if (MME_SUCCESS != mme_pull (&confirm_ctx, &tonemap_cnf->tmi_length, 1, &result_len))
    {
        return LIBMSTAR_ERROR_PARAM;
    }

    if (tonemap_cnf->tmi_length > 0)
    {
        ret = mme_pull (&confirm_ctx, tonemap_cnf->tmi_list, tonemap_cnf->tmi_length, &result_len);
    }

    ret = mme_pull (&confirm_ctx, &tonemap_cnf->int_length, 1, &result_len);
    for (i = 0; i < tonemap_cnf->int_length; i++)
    {
        ret = mme_pull (&confirm_ctx, &tonemap_cnf->int_info[i].int_et, 2, &result_len);
        tonemap_cnf->int_info[i].int_et = LE16TOH(tonemap_cnf->int_info[i].int_et);
        ret = mme_pull (&confirm_ctx, &tonemap_cnf->int_info[i].int_tmi, 1, &result_len);
        ret = mme_pull (&confirm_ctx, &tonemap_cnf->int_info[i].int_rx_gain, 1, &result_len);
        ret = mme_pull (&confirm_ctx, &tonemap_cnf->int_info[i].int_fec, 1, &result_len);
        ret = mme_pull (&confirm_ctx, &tonemap_cnf->int_info[i].int_gi, 1, &result_len);
        ret = mme_pull (&confirm_ctx, &tonemap_cnf->int_info[i].int_phy_rate, 4, &result_len);
        tonemap_cnf->int_info[i].int_phy_rate = LE32TOH(tonemap_cnf->int_info[i].int_phy_rate);
    }
    ret = mme_pull (&confirm_ctx, &tonemap_cnf->tmi, 1, &result_len);
    ret = mme_pull (&confirm_ctx, &tonemap_cnf->tm_rx_gain, 1, &result_len);
    ret = mme_pull (&confirm_ctx, &tonemap_cnf->tm_fec, 1, &result_len);
    ret = mme_pull (&confirm_ctx, &tonemap_cnf->tm_gi, 1, &result_len);
    ret = mme_pull (&confirm_ctx, &tonemap_cnf->tm_phy_rate, 4, &result_len);
    tonemap_cnf->tm_phy_rate = LE32TOH(tonemap_cnf->tm_phy_rate);
    ret = mme_pull (&confirm_ctx, &tonemap_cnf->carrier_group, 1, &result_len);
    ret = mme_pull (&confirm_ctx, &tonemap_cnf->modulation_list_length, 2, &result_len);
    tonemap_cnf->modulation_list_length = LE16TOH(tonemap_cnf->modulation_list_length);
    /* bit conversion byte */
    modulation_num = tonemap_cnf->modulation_list_length/8;

    if ((tonemap_cnf->modulation_list_length % 8) > 0)
    {
       modulation_num = modulation_num + 1;
    }

    if (tonemap_cnf->modulation_list_length > 0)
    {
        ret = mme_pull (&confirm_ctx, &tonemap_cnf->modulation_list, modulation_num, &result_len);
    }

    return ret;
}

libmstar_error_t
libmstar_vs_get_snr (char *interface, char *dest_mac,
                          libmstar_vs_get_snr_req_t *snr_req,
                          libmstar_vs_get_snr_cnf_t *snr_cnf)
{


    mme_ctx_t request_ctx, confirm_ctx;
    unsigned char snd_buffer[ETH_DATA_LEN] = { 0 };
    unsigned int result_len = 0;
    unsigned char dest_bin[MAC_BIN_LEN] = { 0 };
    u8 i = 0;

    if (NULL == dest_mac || NULL == snr_req || NULL == snr_cnf)
    {
        return LIBMSTAR_VS_GET_SNR_CNF_RESULT_FAILURE;
    }

    libmstar_mac_str_to_bin (dest_mac, dest_bin);

    /* initialize indication MME for VS_GET_VERSION_REG */
    if (MME_SUCCESS != mme_init (&request_ctx, VS_GET_SNR | MME_REQ,
                                 snd_buffer, (unsigned int)ETH_DATA_LEN))
    {
        return LIBMSTAR_ERROR_PARAM;
    }

    if (MME_SUCCESS != mme_init (&confirm_ctx, VS_GET_SNR | MME_CNF,
                                 snd_buffer, (unsigned int)ETH_DATA_LEN))
    {
        return LIBMSTAR_ERROR_PARAM;
    }

    if (MME_SUCCESS != mme_put (&request_ctx, snr_req,
                                sizeof (libmstar_vs_get_snr_req_t), &result_len))
    {
        return LIBMSTAR_ERROR_PARAM;
    }

    /* send indication MME */
    if (MME_SUCCESS != mme_send (&request_ctx, MME_SEND_REQ_CNF, interface,
                                 dest_bin, &confirm_ctx, MME_MMV1, SPIDCOM_OUI))
    {
        return LIBMSTAR_ERROR_PARAM;
    }

    if (MME_SUCCESS != mme_pull (&confirm_ctx, &snr_cnf->result, 1, &result_len))
    {
        return LIBMSTAR_ERROR_PARAM;
    }

    if (MME_SUCCESS != mme_pull (&confirm_ctx, &snr_cnf->int_id, 1, &result_len))
    {
        return LIBMSTAR_ERROR_PARAM;
    }

    if (MME_SUCCESS != mme_pull (&confirm_ctx, &snr_cnf->intervals_nb, 1, &result_len))
    {
        return LIBMSTAR_ERROR_PARAM;
    }

    if (MME_SUCCESS != mme_pull (&confirm_ctx, snr_cnf->int_et, snr_cnf->intervals_nb * 2, &result_len))
    {
        return LIBMSTAR_ERROR_PARAM;
    }

    if (MME_SUCCESS != mme_pull (&confirm_ctx, &snr_cnf->tm_ber, 2, &result_len))
    {
        return LIBMSTAR_ERROR_PARAM;
    }

    if (MME_SUCCESS != mme_pull (&confirm_ctx, &snr_cnf->carrier_gr, 1, &result_len))
    {
        return LIBMSTAR_ERROR_PARAM;
    }

    if (MME_SUCCESS != mme_pull (&confirm_ctx, snr_cnf->snr, 1024, &result_len))
    {
        return LIBMSTAR_ERROR_PARAM;
    }

    for (i = 0; i < INT_LENGTH; i++)
    {
        snr_cnf->int_et[i] = LE16TOH(snr_cnf->int_et[i]);
    }
    snr_cnf->tm_ber = LE16TOH(snr_cnf->tm_ber);
    for (i = 0; i < LIBMSTAR_SNR_IN_MSG_NB; i++)
    {
        snr_cnf->snr[i] = LE32TOH(snr_cnf->snr[i]);
    }

    return LIBMSTAR_SUCCESS;
}

libmstar_error_t
libmstar_vs_get_link_stats (char *interface, char *dest_mac,
                                    libmstar_vs_get_link_stats_req_t *link_stats_req,
                                    libmstar_vs_get_link_stats_cnf_t *link_stats_cnf)
{
    unsigned int result_len = 0;
    unsigned char dest_bin[MAC_BIN_LEN] = { 0 };
    mme_ctx_t request_ctx, confirm_ctx;
    unsigned char snd_buffer[ETH_DATA_LEN] = { 0 };

    if (NULL == dest_mac || NULL == link_stats_req || NULL == link_stats_cnf)
    {
        return LIBMSTAR_ERROR_PARAM;
    }

    libmstar_mac_str_to_bin (dest_mac, dest_bin);

    /* initialize indication MME for VS_GET_VERSION_REG */
    if (MME_SUCCESS != mme_init (&request_ctx, VS_GET_LINK_STATS | MME_REQ,
                                 snd_buffer, (unsigned int)ETH_DATA_LEN))
    {
        return LIBMSTAR_ERROR_PARAM;
    }

    if (MME_SUCCESS != mme_init (&confirm_ctx, VS_GET_LINK_STATS | MME_CNF,
                    snd_buffer, (unsigned int)ETH_DATA_LEN))
    {
        return LIBMSTAR_ERROR_PARAM;
    }

    if (MME_SUCCESS != mme_put (&request_ctx, link_stats_req,
                                sizeof (libmstar_vs_get_link_stats_req_t), &result_len))
    {
        return LIBMSTAR_ERROR_PARAM;
    }

    /* send indication MME */
    if (MME_SUCCESS != mme_send (&request_ctx, MME_SEND_REQ_CNF, interface,
                                 dest_bin, &confirm_ctx, MME_MMV1, SPIDCOM_OUI))
    {
        return LIBMSTAR_ERROR_PARAM;
    }

    if (MME_SUCCESS != mme_pull (&confirm_ctx, &link_stats_cnf->ReqID, 1, &result_len))
    {
        return LIBMSTAR_ERROR_PARAM;
    }

    if (MME_SUCCESS != mme_pull (&confirm_ctx, &link_stats_cnf->result, 1, &result_len))
    {
        return LIBMSTAR_ERROR_PARAM;
    }

    if ((link_stats_req->ReqType == LIBMSTAR_VS_GET_LINK_STATS_REQ_REQTYPE_GET_STAT
         || link_stats_req->ReqType == LIBMSTAR_VS_GET_LINK_STATS_REQ_REQTYPE_GET_RESET_STAT)
        && link_stats_cnf->result == LIBMSTAR_VS_GET_LINK_STATS_CNF_RESULT_SUCCESS)
    {
        if (link_stats_req->TLFlag == LIBMSTAR_VS_GET_LINK_STATS_REQ_TLFLAG_TX_LINK)
        {
	        if (MME_SUCCESS != mme_pull (&confirm_ctx, &link_stats_cnf->mfs_tx,
                                         sizeof (link_stats_tx_t), &result_len))
	        {
	            return LIBMSTAR_ERROR_PARAM;
	        }

            link_stats_cnf->mfs_tx.statistics_start_date =
                LE64TOH(link_stats_cnf->mfs_tx.statistics_start_date);
            link_stats_cnf->mfs_tx.num_segs_suc =
                LE32TOH(link_stats_cnf->mfs_tx.num_segs_suc);
            link_stats_cnf->mfs_tx.num_segs_dropped =
                LE32TOH(link_stats_cnf->mfs_tx.num_segs_dropped);
            link_stats_cnf->mfs_tx.num_pbs =
                LE32TOH(link_stats_cnf->mfs_tx.num_pbs);
            link_stats_cnf->mfs_tx.num_mpdus =
                LE32TOH(link_stats_cnf->mfs_tx.num_mpdus);
            link_stats_cnf->mfs_tx.num_bursts =
                LE32TOH(link_stats_cnf->mfs_tx.num_bursts);
            link_stats_cnf->mfs_tx.num_sacks =
                LE32TOH(link_stats_cnf->mfs_tx.num_sacks);
            link_stats_cnf->mfs_tx.num_bad_pbs_crc =
                LE32TOH(link_stats_cnf->mfs_tx.num_bad_pbs_crc);
            link_stats_cnf->mfs_tx.num_mpdus_coll =
                LE32TOH(link_stats_cnf->mfs_tx.num_mpdus_coll);
            link_stats_cnf->mfs_tx.num_mpdus_fail =
                LE32TOH(link_stats_cnf->mfs_tx.num_mpdus_fail);
            link_stats_cnf->mfs_tx.num_total_seg =
                LE64TOH(link_stats_cnf->mfs_tx.num_total_seg);
            link_stats_cnf->mfs_tx.num_tx_buf_shortage_drop =
                LE32TOH(link_stats_cnf->mfs_tx.num_tx_buf_shortage_drop);
        }
        else if (link_stats_req->TLFlag == LIBMSTAR_VS_GET_LINK_STATS_REQ_TLFLAG_RX_LINK)
        {
	        if (MME_SUCCESS != mme_pull (&confirm_ctx, &link_stats_cnf->mfs_rx,
                                         sizeof (link_stats_rx_t), &result_len))
	        {
	            return LIBMSTAR_ERROR_PARAM;
	        }

            link_stats_cnf->mfs_rx.statistics_start_date =
                LE64TOH(link_stats_cnf->mfs_rx.statistics_start_date);
            link_stats_cnf->mfs_rx.num_segs_suc =
                LE32TOH(link_stats_cnf->mfs_rx.num_segs_suc);
            link_stats_cnf->mfs_rx.num_pbs =
                LE32TOH(link_stats_cnf->mfs_rx.num_pbs);
            link_stats_cnf->mfs_rx.num_mpdus =
                LE32TOH(link_stats_cnf->mfs_rx.num_mpdus);
            link_stats_cnf->mfs_rx.num_bursts =
                LE32TOH(link_stats_cnf->mfs_rx.num_bursts);
            link_stats_cnf->mfs_rx.num_bad_pbs_crc =
                LE32TOH(link_stats_cnf->mfs_rx.num_bad_pbs_crc);
            link_stats_cnf->mfs_rx.num_sacks =
                LE32TOH(link_stats_cnf->mfs_rx.num_sacks);
            link_stats_cnf->mfs_rx.num_ssn_under_min =
                LE32TOH(link_stats_cnf->mfs_rx.num_ssn_under_min);
            link_stats_cnf->mfs_rx.num_ssn_over_max =
                LE32TOH(link_stats_cnf->mfs_rx.num_ssn_over_max);
        }
    }

    return LIBMSTAR_SUCCESS;
}

libmstar_error_t
libmstar_vs_get_nw_info (char *interface, char *dest_mac,
                         libmstar_vs_get_nw_info_cnf_t *nw_info_cnf)
{
    int i;
    unsigned int result_len = 0;
    unsigned char dest_bin[MAC_BIN_LEN] = { 0 };
    mme_ctx_t request_ctx, confirm_ctx;
    unsigned char snd_buffer[ETH_DATA_LEN] = { 0 };

    if (NULL == dest_mac || NULL == nw_info_cnf)
    {
        return LIBMSTAR_ERROR_PARAM;
    }

    libmstar_mac_str_to_bin (dest_mac, dest_bin);

    /* initialize indication MME for VS_GET_VERSION_REG */
    if (MME_SUCCESS != mme_init (&request_ctx, VS_GET_NW_INFO | MME_REQ,
                                 snd_buffer, (unsigned int)ETH_DATA_LEN))
    {
        return LIBMSTAR_ERROR_PARAM;
    }

    if (MME_SUCCESS != mme_init (&confirm_ctx, VS_GET_NW_INFO | MME_CNF,
                                 snd_buffer, (unsigned int)ETH_DATA_LEN))
    {
        return LIBMSTAR_ERROR_PARAM;
    }

    /* send indication MME */
    if (MME_SUCCESS != mme_send (&request_ctx, MME_SEND_REQ_CNF, interface,
                                 dest_bin, &confirm_ctx, MME_MMV1, SPIDCOM_OUI))
    {
        return LIBMSTAR_ERROR_PARAM;
    }

    if (MME_SUCCESS != mme_pull (&confirm_ctx, &nw_info_cnf->nid, 7, &result_len))
    {
        return LIBMSTAR_ERROR_PARAM;
    }

    if (MME_SUCCESS != mme_pull (&confirm_ctx, &nw_info_cnf->snid, 1, &result_len))
    {
        return LIBMSTAR_ERROR_PARAM;
    }

    if (MME_SUCCESS != mme_pull (&confirm_ctx, &nw_info_cnf->cco_tei, 1, &result_len))
    {
        return LIBMSTAR_ERROR_PARAM;
    }

    if (MME_SUCCESS != mme_pull (&confirm_ctx, &nw_info_cnf->cco_mac, 6, &result_len))
    {
        return LIBMSTAR_ERROR_PARAM;
    }

    if (MME_SUCCESS != mme_pull (&confirm_ctx, &nw_info_cnf->num_stas, 1, &result_len))
    {
        return LIBMSTAR_ERROR_PARAM;
    }

    for (i = 0; i < nw_info_cnf->num_stas; i++)
    {
		if (MME_SUCCESS != mme_pull (&confirm_ctx, &nw_info_cnf->sta_info[i],
                                     sizeof (sta_info_t), &result_len))
		{
			return LIBMSTAR_ERROR_PARAM;
		}


        nw_info_cnf->sta_info[i].phy_tx_coded =
            LE16TOH(nw_info_cnf->sta_info[i].phy_tx_coded);
        nw_info_cnf->sta_info[i].phy_tx_raw =
            LE16TOH(nw_info_cnf->sta_info[i].phy_tx_raw);
        nw_info_cnf->sta_info[i].phy_rx_coded =
            LE16TOH(nw_info_cnf->sta_info[i].phy_rx_coded);
        nw_info_cnf->sta_info[i].phy_rx_raw =
            LE16TOH(nw_info_cnf->sta_info[i].phy_rx_raw);
    }

    return LIBMSTAR_SUCCESS;
}

libmstar_error_t
libmstar_vs_set_capture_state (char *interface, char *dest_mac,
                                       libmstar_vs_set_capture_state_req_t *capture_state_req,
                                       libmstar_vs_set_capture_state_cnf_result_t *capture_state_result)
{
    unsigned int result_len = 0;
    unsigned char dest_bin[MAC_BIN_LEN] = { 0 };
    mme_ctx_t request_ctx, confirm_ctx;
    unsigned char snd_buffer[ETH_DATA_LEN] = { 0 };

    if (NULL == dest_mac || NULL == capture_state_req
        || NULL == capture_state_result)
    {
        return LIBMSTAR_ERROR_PARAM;
    }

    libmstar_mac_str_to_bin (dest_mac, dest_bin);

    /* initialize indication MME for VS_GET_VERSION_REG */
    if (MME_SUCCESS != mme_init (&request_ctx, VS_SET_CAPTURE_STATE | MME_REQ,
                                 snd_buffer, (unsigned int)ETH_DATA_LEN))
    {
        return LIBMSTAR_ERROR_PARAM;
    }


    if (MME_SUCCESS != mme_init (&confirm_ctx, VS_SET_CAPTURE_STATE | MME_CNF,
                                 snd_buffer, (unsigned int)ETH_DATA_LEN))
    {
        return LIBMSTAR_ERROR_PARAM;
    }

    if (MME_SUCCESS != mme_put (&request_ctx, capture_state_req,
                                sizeof (libmstar_vs_set_capture_state_req_t), &result_len))
    {
        return LIBMSTAR_ERROR_PARAM;
    }

    /* send indication MME */
    if (MME_SUCCESS != mme_send (&request_ctx, MME_SEND_REQ_CNF, interface,
                                 dest_bin, &confirm_ctx, MME_MMV1, SPIDCOM_OUI))
    {
        return LIBMSTAR_ERROR_PARAM;
    }

    if (MME_SUCCESS != mme_pull (&confirm_ctx, capture_state_result, 1, &result_len))
    {
        return LIBMSTAR_ERROR_PARAM;
    }

    return LIBMSTAR_SUCCESS;
}

libmstar_error_t
libmstar_vs_set_nvram (char *interface, char *dest_mac,
                             libmstar_vs_set_nvram_req_t *nvram_req,
                             libmstar_vs_set_nvram_cnf_t *nvram_cnf)
{
    unsigned int result_len = 0;
    unsigned char dest_bin[MAC_BIN_LEN] = { 0 };
    mme_ctx_t request_ctx, confirm_ctx;
    unsigned char snd_buffer[ETH_DATA_LEN] = { 0 };

    if (NULL == dest_mac || NULL == nvram_req)
    {
        return LIBMSTAR_ERROR_PARAM;
    }

    libmstar_mac_str_to_bin (dest_mac, dest_bin);

    /* initialize indication MME for VS_GET_VERSION_REG */
    if (MME_SUCCESS != mme_init (&request_ctx, VS_SET_NVRAM | MME_REQ,
                                 snd_buffer, (unsigned int)ETH_DATA_LEN))
    {
        return LIBMSTAR_ERROR_PARAM;
    }

    if (MME_SUCCESS != mme_init (&confirm_ctx, VS_SET_NVRAM | MME_CNF,
                                snd_buffer, (unsigned int)ETH_DATA_LEN))
    {
        return LIBMSTAR_ERROR_PARAM;
    }

    nvram_req->nvram_read_size = LE16TOH(nvram_req->nvram_read_size);
    nvram_req->nvram_read_size = LE32TOH(nvram_req->nvram_read_size);
    nvram_req->checksum = LE32TOH(nvram_req->checksum);

    if (MME_SUCCESS != mme_put (&request_ctx, nvram_req,
                                sizeof (libmstar_vs_set_nvram_req_t), &result_len))
    {
        return LIBMSTAR_ERROR_PARAM;
    }

    /* send indication MME */
    if (MME_SUCCESS != mme_send (&request_ctx, MME_SEND_REQ_CNF, interface,
                                 dest_bin, &confirm_ctx, MME_MMV1, SPIDCOM_OUI))
    {
        return LIBMSTAR_ERROR_PARAM;
    }

    if (MME_SUCCESS != mme_pull (&confirm_ctx, nvram_cnf, 1, &result_len))
	{
	    return LIBMSTAR_ERROR_PARAM;
	}

    return LIBMSTAR_SUCCESS;
}

libmstar_error_t
libmstar_vs_get_nvram (char *interface, char *dest_mac,
                             libmstar_vs_get_nvram_req_t *get_nvram_req,
                             libmstar_vs_get_nvram_cnf_t *get_nvram_cnf)
{
    unsigned int result_len = 0;
    unsigned char dest_bin[MAC_BIN_LEN] = { 0 };
    mme_ctx_t request_ctx, confirm_ctx;
    unsigned char snd_buffer[ETH_DATA_LEN] = { 0 };

    if (NULL == dest_mac || NULL == get_nvram_req
        || NULL == get_nvram_cnf)
    {
        return LIBMSTAR_ERROR_PARAM;
    }

    libmstar_mac_str_to_bin (dest_mac, dest_bin);

    /* initialize indication MME for VS_GET_VERSION_REG */
    if (MME_SUCCESS != mme_init (&request_ctx, VS_GET_NVRAM | MME_REQ,
                                 snd_buffer, (unsigned int)ETH_DATA_LEN))
    {
        return LIBMSTAR_ERROR_PARAM;
    }

    if (MME_SUCCESS != mme_init (&confirm_ctx, VS_GET_NVRAM | MME_CNF,
                                  snd_buffer, (unsigned int)ETH_DATA_LEN))
    {
        return LIBMSTAR_ERROR_PARAM;
    }

	if (MME_SUCCESS != mme_put (&request_ctx, get_nvram_req,
                                sizeof (libmstar_vs_get_nvram_req_t), &result_len))
	{
	    return LIBMSTAR_ERROR_PARAM;
	}

    /* send indication MME */
    if (MME_SUCCESS != mme_send (&request_ctx, MME_SEND_REQ_CNF, interface,
                                 dest_bin, &confirm_ctx, MME_MMV1, SPIDCOM_OUI))
    {
        return LIBMSTAR_ERROR_PARAM;
    }

    if (MME_SUCCESS != mme_pull (&confirm_ctx, get_nvram_cnf,
                                 sizeof (libmstar_vs_get_nvram_cnf_t), &result_len))
	{
	    return LIBMSTAR_ERROR_PARAM;
	}

    get_nvram_cnf->nvram_size = LE16TOH(get_nvram_cnf->nvram_size);

    return LIBMSTAR_SUCCESS;
}

libmstar_error_t
libmstar_vs_get_pwm_stats (char *interface, char *dest_mac,
                                  libmstar_vs_get_pwm_stats_cnf_t *get_pwm_stats_cnf)
{
    unsigned int result_len = 0;
    unsigned char dest_bin[MAC_BIN_LEN] = { 0 };
    mme_ctx_t request_ctx, confirm_ctx;
    unsigned char snd_buffer[ETH_DATA_LEN] = { 0 };

    if (NULL == dest_mac || NULL == get_pwm_stats_cnf)
    {
        return LIBMSTAR_ERROR_PARAM;
    }

    libmstar_mac_str_to_bin (dest_mac, dest_bin);

    /* initialize indication MME for VS_GET_VERSION_REG */
    if (MME_SUCCESS != mme_init (&request_ctx, VS_GET_PWM_STATS | MME_REQ,
                                 snd_buffer, (unsigned int)ETH_DATA_LEN))
    {
        return LIBMSTAR_ERROR_PARAM;
    }

    if (MME_SUCCESS != mme_init (&confirm_ctx, VS_GET_PWM_STATS | MME_CNF,
                                 snd_buffer, (unsigned int)ETH_DATA_LEN))
    {
        return LIBMSTAR_ERROR_PARAM;
    }


    /* send indication MME */
    if (MME_SUCCESS != mme_send (&request_ctx, MME_SEND_REQ_CNF, interface,
                                 dest_bin, &confirm_ctx, MME_MMV1, SPIDCOM_OUI))
    {
        return LIBMSTAR_ERROR_PARAM;
    }

	if (MME_SUCCESS != mme_pull (&confirm_ctx, get_pwm_stats_cnf,
                                 sizeof (libmstar_vs_get_pwm_stats_cnf_t), &result_len))
	{
	    return LIBMSTAR_ERROR_PARAM;
	}

    get_pwm_stats_cnf->freq = LE16TOH(get_pwm_stats_cnf->freq);
    get_pwm_stats_cnf->duty_cycle = LE16TOH(get_pwm_stats_cnf->duty_cycle);
    get_pwm_stats_cnf->millvolt = LE16TOH(get_pwm_stats_cnf->millvolt);

    return LIBMSTAR_SUCCESS;
}

libmstar_error_t
libmstar_vs_diag_debug (char *interface, char *dest_mac,
                        libmstar_vs_diag_debug_req_t *diag_debug_req,
                        libmstar_vs_diag_debug_cnf_t *diag_debug_cnf)
{
    u16 tmp_command;
    unsigned int result_len = 0;
    unsigned char dest_bin[MAC_BIN_LEN] = { 0 };
    mme_ctx_t request_ctx, confirm_ctx;
    unsigned char snd_buffer[ETH_DATA_LEN] = { 0 };

    if (NULL == dest_mac || NULL == diag_debug_req
        || NULL == diag_debug_cnf)
    {
        return LIBMSTAR_ERROR_PARAM;
    }

    libmstar_mac_str_to_bin (dest_mac, dest_bin);

    /* initialize indication MME for VS_GET_VERSION_REG */
    if (MME_SUCCESS != mme_init (&request_ctx, VS_DIAG_DEBUG | MME_REQ,
                                 snd_buffer, (unsigned int)ETH_DATA_LEN))
    {
        return LIBMSTAR_ERROR_PARAM;
    }

    if (MME_SUCCESS != mme_init (&confirm_ctx, VS_DIAG_DEBUG | MME_CNF,
                                 snd_buffer, (unsigned int)ETH_DATA_LEN))
    {
        return LIBMSTAR_ERROR_PARAM;
    }

    tmp_command = diag_debug_req->command;

    if (diag_debug_req->command == LIBMSTAR_VS_DIAG_DEBUG_REQ_SUBTYPE_SNIFFER_SET)
    {
        diag_debug_req->command = LE16TOH(diag_debug_req->command);
		if (MME_SUCCESS != mme_put (&request_ctx, &diag_debug_req->command, 2, &result_len))
		{
			return LIBMSTAR_ERROR_PARAM;
		}

		if (MME_SUCCESS != mme_put (&request_ctx, &diag_debug_req->action,  1, &result_len))
		{
			return LIBMSTAR_ERROR_PARAM;
		}
        if (MME_SUCCESS != mme_put (&request_ctx, diag_debug_req->sniffer_mac, 6, &result_len))
        {
            return LIBMSTAR_ERROR_PARAM;
        }
    }
    else
    {
        diag_debug_req->command = LE16TOH(diag_debug_req->command);
		if (MME_SUCCESS != mme_put (&request_ctx, &diag_debug_req->command, 2, &result_len))
		{
			return LIBMSTAR_ERROR_PARAM;
		}
    }

    /* send indication MME */
    if (MME_SUCCESS != mme_send (&request_ctx, MME_SEND_REQ_CNF, interface,
                                  dest_bin, &confirm_ctx, MME_MMV1, SPIDCOM_OUI))
    {
        return LIBMSTAR_ERROR_PARAM;
    }

	if (MME_SUCCESS != mme_pull (&confirm_ctx, &diag_debug_cnf->result, 1, &result_len))
	{
	    return LIBMSTAR_ERROR_PARAM;
	}

    if (tmp_command == LIBMSTAR_VS_DIAG_DEBUG_REQ_SUBTYPE_GET_SYS_INFO)
    {
		if (MME_SUCCESS != mme_pull (&confirm_ctx, &diag_debug_cnf->systime, 4, &result_len))
		{
			return LIBMSTAR_ERROR_PARAM;
		}

		if (MME_SUCCESS != mme_pull (&confirm_ctx, &diag_debug_cnf->authenticated_times, 1, &result_len))
		{
			return LIBMSTAR_ERROR_PARAM;
		}

		if (MME_SUCCESS != mme_pull (&confirm_ctx, &diag_debug_cnf->auth_time, 4, &result_len))
		{
			return LIBMSTAR_ERROR_PARAM;
		}

		if (MME_SUCCESS != mme_pull (&confirm_ctx, &diag_debug_cnf->unauth_time, 4, &result_len))
		{
			return LIBMSTAR_ERROR_PARAM;
		}

        diag_debug_cnf->systime = LE32TOH(diag_debug_cnf->systime);
        diag_debug_cnf->auth_time = LE32TOH(diag_debug_cnf->auth_time);
        diag_debug_cnf->unauth_time = LE32TOH(diag_debug_cnf->unauth_time);
    }

    return LIBMSTAR_SUCCESS;
}
libmstar_error_t
libmstar_vs_file_access_send_req (char *interface, char *dest_mac,
                    vs_file_access_req_t *file_access_req,
                    vs_file_access_cnf_t *file_access_cnf,
                    unsigned char *cnf_data)
{
    unsigned int result_len = 0;
    //char sw_ver[128] = { 0 };
    //unsigned char tmp[100] = { 0 };
    unsigned char dest_bin[MAC_BIN_LEN] = { 0 };
    mme_ctx_t request_ctx, confirm_ctx;
    unsigned char snd_buffer[ETH_DATA_LEN] = { 0 };

    if (dest_mac == NULL || file_access_req == NULL
        || NULL == file_access_cnf || cnf_data == NULL)
    {
        printf ("%s param error\n", __func__);
        return LIBMSTAR_ERROR_PARAM;
    }

    libmstar_mac_str_to_bin (dest_mac, dest_bin);

    /* initialize indication MME for VS_FILE_ACCESS */
    if (MME_SUCCESS != mme_init (&request_ctx, VS_FILE_ACCESS | MME_REQ,
                                 snd_buffer, (unsigned int)ETH_DATA_LEN))
    {
        return LIBMSTAR_ERROR_MME;
    }

    if (MME_SUCCESS != mme_init (&confirm_ctx, VS_FILE_ACCESS | MME_CNF,
                                 snd_buffer, (unsigned int)ETH_DATA_LEN))
    {
        return LIBMSTAR_ERROR_MME;
    }

    file_access_req->total_fragments = LE16TOH(file_access_req->total_fragments);
    file_access_req->fragment_number = LE16TOH(file_access_req->fragment_number);
    file_access_req->offset = LE32TOH(file_access_req->offset);
    file_access_req->checksum = LE32TOH(file_access_req->checksum);
    file_access_req->length = LE16TOH(file_access_req->length);

    if (MME_SUCCESS != mme_put (&request_ctx, file_access_req,
		                        sizeof (vs_file_access_req_t), &result_len))
    {
        return LIBMSTAR_ERROR_MME;
    }

    /* send indication MME */
    if (MME_SUCCESS != mme_send (&request_ctx, MME_SEND_REQ_CNF, interface,
                                 dest_bin, &confirm_ctx, MME_MMV1, SPIDCOM_OUI))
    {
        return LIBMSTAR_ERROR_MME;
    }

	if (MME_SUCCESS != mme_pull (&confirm_ctx, file_access_cnf,
                                 sizeof (vs_file_access_cnf_t), &result_len))
	{
		return LIBMSTAR_ERROR_MME;
	}

    if (result_len != sizeof (vs_file_access_cnf_t))
    {
        printf ("%s %d mme_pull result_len %d not right\n", __func__, __LINE__,
                result_len);
        return LIBMSTAR_ERROR_MME;
    }

    file_access_cnf->total_fragments = LE16TOH(file_access_cnf->total_fragments);
    file_access_cnf->fragment_number = LE16TOH(file_access_cnf->fragment_number);
    file_access_cnf->offset = LE32TOH(file_access_cnf->offset);
    file_access_cnf->length = LE16TOH(file_access_cnf->length);

	if (MME_SUCCESS != mme_pull (&confirm_ctx, cnf_data,
                                 file_access_cnf->length, &result_len))
	{
	    return LIBMSTAR_ERROR_MME;
	}

    if (result_len != file_access_cnf->length)
    {
        printf ("%s %d mme_pull result_len %d != %d\n", __func__, __LINE__,
                result_len, file_access_cnf->length);
        return LIBMSTAR_ERROR_MME;
    }

    return LIBMSTAR_SUCCESS;
}

libmstar_error_t
libmstar_vs_file_access_req (char *interface, char *dest_mac, vs_file_access_cmd_t *cmd)
{
    FILE *fp = NULL;
    int n = 0, result = 0;
    int remain_len = 0;
    int file_size = 0, size = 0, ret = 0;
    unsigned char cnf_data[VS_FILE_ACCESS_CNF_DATA_MAX_LEN] = { 0 };
    unsigned char dest_bin[MAC_BIN_LEN] = { 0 };
    unsigned char *buf = NULL;
    vs_file_access_req_t mme_sent;
    vs_file_access_cnf_t mme_cnf;

    if (NULL == dest_mac || NULL == cmd)
    {
        return LIBMSTAR_ERROR_PARAM;
    }

    libmstar_mac_str_to_bin (dest_mac, dest_bin);

    memset (&mme_sent, 0x0, sizeof (mme_sent));
    memset (&mme_cnf, 0x0, sizeof (mme_cnf));
    mme_sent.op = cmd->op;
    mme_sent.file_type = cmd->file_type;
    if (0 != strlen (cmd->parameter))
    {
        strcpy (mme_sent.parameter, cmd->parameter);
    }
    if (VS_FILE_ACCESS_REQ_OP_WRITE == cmd->op)
    {
        fp = fopen (cmd->file_name, "rb");
        if (NULL == fp)
        {
            printf ("%d, Can't open the file - %s\n", __LINE__, cmd->file_name);
            return -1;
        }

        fseek (fp, 0, SEEK_END);
        file_size = ftell (fp);
        rewind (fp);

        buf = (unsigned char *)malloc (file_size);
        fread (buf, 1, file_size, fp);
        fclose (fp);

        mme_sent.total_fragments = (file_size + VS_FILE_ACCESS_REQ_DATA_MAX_LEN - 1)
            / (VS_FILE_ACCESS_REQ_DATA_MAX_LEN);
    }

    printf ("cmd->file_name:[%s], \ncmd->parameter:[%s]\n",cmd->file_name, cmd->parameter);
    printf ("Sending MStar VS_FILE_ACCESS.REQ %s\n", cmd->name);
    do
    {
        if (VS_FILE_ACCESS_REQ_OP_WRITE == cmd->op)
        {
            remain_len = file_size -
                (VS_FILE_ACCESS_REQ_DATA_MAX_LEN * mme_sent.fragment_number);
            size = (remain_len > VS_FILE_ACCESS_REQ_DATA_MAX_LEN) ?
                VS_FILE_ACCESS_REQ_DATA_MAX_LEN:remain_len;
            memcpy (mme_sent.data, buf +
                (VS_FILE_ACCESS_REQ_DATA_MAX_LEN * (mme_sent.fragment_number)), size);
            mme_sent.length = size;
            mme_sent.offset = VS_FILE_ACCESS_REQ_DATA_MAX_LEN * (mme_sent.fragment_number);

            if (VS_FILE_ACCESS_REQ_FILE_TYPE_GENERAL_FILE != cmd->file_type)
            {
                mme_sent.checksum = 0;
                calc_xor_checksum (buf +
                    (VS_FILE_ACCESS_REQ_DATA_MAX_LEN * (mme_sent.fragment_number)),
                    mme_sent.length, &mme_sent.checksum);
            }
        }
        result = 0;
        ret = libmstar_vs_file_access_send_req (interface, dest_mac,
                    &mme_sent, &mme_cnf, cnf_data);
        if (ret != LIBMSTAR_SUCCESS)
        {
            printf ("%s %d libmstar_vs_file_access_send_req ret %d\n",
                    __func__, __LINE__, ret);
            result = -1;
            break;
        }

        if (VS_FILE_ACCESS_REQ_OP_SAVE == cmd->op)
        {
            if (0 == mme_cnf.fragment_number)
                fp = fopen (cmd->file_name, "wb");
            else
                fp = fopen (cmd->file_name, "a+b");
            if (fp)
            {
                fwrite (cnf_data, 1, mme_cnf.length, fp);
                fclose (fp);
            }
        }
        else
        {
            /* Judge op */
            if (0 == mme_cnf.fragment_number)
            {
                cmd->file_len = mme_cnf.length;
                memset (cmd->file_buf, 0, 10240);
                memcpy (cmd->file_buf, cnf_data, mme_cnf.length);
            }
            else
            {
                memcpy (cmd->file_buf + cmd->file_len, cnf_data, mme_cnf.length);
                cmd->file_len += mme_cnf.length;
            }
        }
        mme_sent.total_fragments = mme_cnf.total_fragments;
        mme_sent.fragment_number = mme_cnf.fragment_number + 1;
        n++;
    } while ((LIBMSTAR_SUCCESS == result)
             && (mme_sent.fragment_number < mme_sent.total_fragments));


    return result;
}

JSON_Value * libmstar_json_object_get_patameter_value (const char *filename, char *groupname,
                                                       char *parametername)
{
    int i = 0;
    int count = 0;
    JSON_Value *root_value = NULL;
    JSON_Value *root_value_tmp = NULL;
    JSON_Value *root_value_tmp_2 = NULL;
    JSON_Object *root_object = NULL;
    JSON_Array *array = NULL;
	printf("111111 %s:%d\n",__func__,__LINE__);
    root_value = json_parse_file(filename);
	printf("111111 %s:%d\n",__func__,__LINE__);
    root_object = json_value_get_object(root_value);
	printf("111111 %s:%d\n",__func__,__LINE__);
    if (!strcmp (groupname, ""))
    {
    	printf("111111 %s:%d\n",__func__,__LINE__);
        root_value_tmp = json_object_get_value (root_object, parametername);
        if (NULL == root_value_tmp)
        {
            printf ("find fail\n");
            json_value_free(root_value);
            return NULL;
        }
        else
        {
        	printf("111111 %s:%d\n",__func__,__LINE__);
            switch (root_value_tmp->type)
            {
                case JSONNull:
                    break;

                case JSONString:
                    printf ("%s-%s = %s\n", groupname, parametername, root_value_tmp->value.string);
                    break;

                case JSONNumber:
                    printf ("%s-%s = %f\n", groupname, parametername, root_value_tmp->value.number);
                    break;

                case JSONObject:
                    break;

                case JSONArray:
                    array = json_value_get_array(root_value_tmp);
                    count = json_array_get_count(array);
                    for (i = 0; i < count; i++)
                    {
                        printf ("%s-%s = %f\n", groupname, parametername, array->items[i]->value.number);
                    }
                    break;

                case JSONBoolean:
                    if (0 == root_value_tmp->value.boolean)
                    {
                        printf ("%s-%s = %s\n", groupname, parametername, "false");
                    }
                    else
                    {
                        printf ("%s-%s = %s\n", groupname, parametername, "true");
                    }

                    break;

                default:
                    break;
            }
            json_value_free(root_value);
            return root_value_tmp;
        }
    }
    else
    {
    	printf("111111 %s:%d\n",__func__,__LINE__);
        root_value_tmp = json_object_get_value (root_object, groupname);
	if(NULL==root_value_tmp)
	{
		 printf ("find group fail\n");
            json_value_free(root_value);
            return NULL;
	}
        root_value_tmp_2 = json_object_get_value (root_value_tmp->value.object, parametername);

        if (NULL == root_value_tmp_2)
        {
            printf ("find fail\n");
            json_value_free(root_value);
            return NULL;
        }
        else
        {
            printf ("value typr %d\n", root_value_tmp_2->type);
            switch (root_value_tmp_2->type)
            {
                case JSONNull:
                    break;

                case JSONString:
                    printf ("%s-%s = %s\n", groupname, parametername, root_value_tmp_2->value.string);
                    break;

                case JSONNumber:
                    printf ("%s-%s = %f\n", groupname, parametername, root_value_tmp_2->value.number);
                    break;

                case JSONObject:
                    break;

                case JSONArray:
                    array = json_value_get_array(root_value_tmp_2);
                    count = json_array_get_count(array);
                    for (i = 0; i < count; i++)
                    {
                        printf ("%s-%s = %f\n", groupname, parametername, array->items[i]->value.number);
                    }
                    break;

                case JSONBoolean:
                    if (0 == root_value_tmp_2->value.boolean)
                    {
                        printf ("%s-%s = %s\n", groupname, parametername, "false");
                    }
                    else
                    {
                        printf ("%s-%s = %s\n", groupname, parametername, "true");
                    }

                    break;

                default:
                    break;
            }
            json_value_free(root_value);
            return root_value_tmp_2;
        }
    }
}

JSON_Value * libmstar_conf_file_patameter_read (char *interface_tmp, char *dest_mac,
                                                          char *filename, char *groupname,
                                                          char *parametername)
{
    vs_file_access_cmd_t cmd;

    memset (&cmd, 0x0, sizeof (cmd));
    cmd.file_type = VS_FILE_ACCESS_REQ_FILE_TYPE_GENERAL_FILE;
    strcpy (cmd.parameter, filename);
    sprintf (cmd.file_name, "/tmp%s", filename);

    cmd.op = VS_FILE_ACCESS_REQ_OP_SAVE;
    if (LIBMSTAR_SUCCESS != libmstar_vs_file_access_req (interface_tmp, dest_mac, &cmd))
    {
        printf ("parameter read %s fail\n", cmd.parameter);
        return NULL;
    }

    return libmstar_json_object_get_patameter_value (cmd.file_name, groupname,
                                                     parametername);
}

int libmstar_json_object_set_patameter_value (libmstar_patameter_info_t *patameter_info)
{
    //int i = 0;
    //int count = 0;
    JSON_Value *root_value = NULL;
    JSON_Value *root_value_tmp = NULL;
    JSON_Value *root_value_tmp_2 = NULL;
    JSON_Object *root_object = NULL;
    JSON_Array *array = NULL;
	printf("111111 %s:%d\n",__func__,__LINE__);
    root_value = json_parse_file(patameter_info->filename);
	printf("111111 %s:%d\n",__func__,__LINE__);
    root_object = json_value_get_object(root_value);
	
    if (!strcmp (patameter_info->groupname, ""))
    {
        root_value_tmp = json_object_get_value (root_object, patameter_info->parametername);

        /* patameter exist */
        if (NULL != root_value_tmp)
        {
            if (root_value_tmp->type != patameter_info->parametertype)
            {
                json_value_free(root_value);
                return -1;
            }
        }
        else
        {
            /* patameter not exist, array not add */
            if (patameter_info->parametertype == JSONArray)
            {
                json_value_free(root_value);
                return -1;
            }
        }
        switch (patameter_info->parametertype)
        {
            case JSONNull:
                break;

            case JSONString:
                if (0 != json_object_set_string (root_object,
                                                 patameter_info->parametername,
                                                 patameter_info->value))
                {
                    json_value_free(root_value);
                    return -1;
                }
                json_serialize_to_file_pretty (root_value, patameter_info->filename);
                break;

            case JSONNumber:

                if (0 != json_object_set_number (root_object,
                                                 patameter_info->parametername,
                                                 atoi(patameter_info->value)))
                {
                    json_value_free(root_value);
                    return -1;
                }
                json_serialize_to_file_pretty (root_value, patameter_info->filename);
                break;

            case JSONObject:
                break;

            case JSONArray:
                array = json_value_get_array(root_value_tmp);
                if (NULL != root_value_tmp)
                {
                    if (JSONNumber == patameter_info->array_data_type)
                    {
                        if (0 != json_array_replace_number (array,
                                                            patameter_info->array_index,
                                                            atoi(patameter_info->value)))
                        {
                            json_value_free(root_value);
                            return -1;
                        }
                    }
                    else if (JSONString == patameter_info->array_data_type)
                    {
                        if (0 != json_array_replace_string (array,
                                                            patameter_info->array_index,
                                                            patameter_info->value))
                        {
                            json_value_free(root_value);
                            return -1;
                        }
                    }

                    json_serialize_to_file_pretty (root_value, patameter_info->filename);
                }
                break;

            case JSONBoolean:
                if (0 != json_object_set_boolean (root_object,
                                                  patameter_info->parametername,
                                                  atoi (patameter_info->value)))
                {
                    json_value_free(root_value);
                    return -1;
                }
                json_serialize_to_file_pretty (root_value, patameter_info->filename);
                break;

            default:
                break;
        }
        json_value_free(root_value);
        return 0;
    }
    else
    {
        root_value_tmp = json_object_get_value (root_object, patameter_info->groupname);

        if (NULL == root_value_tmp)
        {
            json_value_free(root_value);
            return -1;
        }

        root_value_tmp_2 = json_object_get_value (root_value_tmp->value.object, patameter_info->parametername);

        /* patameter exist */
        if (NULL != root_value_tmp_2)
        {
            if (root_value_tmp_2->type != patameter_info->parametertype)
            {
                json_value_free(root_value);
                return -1;
            }
        }
        else
        {
            /* patameter not exist, array not add */
            if (patameter_info->parametertype == JSONArray)
            {
                json_value_free(root_value);
                return -1;
            }
        }

        switch (patameter_info->parametertype)
        {
            case JSONNull:
                break;

            case JSONString:
                if (0 != json_object_set_string (root_value_tmp->value.object,
                                                 patameter_info->parametername,
                                                 patameter_info->value))
                {
                    json_value_free(root_value);
                    return -1;
                }
                json_serialize_to_file_pretty (root_value, patameter_info->filename);
                break;

            case JSONNumber:

                if (0 != json_object_set_number (root_value_tmp->value.object,
                                                 patameter_info->parametername,
                                                 atoi(patameter_info->value)))
                {
                    json_value_free(root_value);
                    return -1;
                }
                json_serialize_to_file_pretty (root_value, patameter_info->filename);
                break;

            case JSONObject:
                break;

            case JSONArray:
                array = json_value_get_array(root_value_tmp_2);
                if (NULL != root_value_tmp_2)
                {
                    if (JSONNumber == patameter_info->array_data_type)
                    {
                        if (0 != json_array_replace_number (array,
                                                            patameter_info->array_index,
                                                            atoi(patameter_info->value)))
                        {
                            json_value_free(root_value);
                            return -1;
                        }
                    }
                    else if (JSONString == patameter_info->array_data_type)
                    {
                        if (0 != json_array_replace_string (array,
                                                            patameter_info->array_index,
                                                            patameter_info->value))
                        {
                            json_value_free(root_value);
                            return -1;
                        }
                    }

                    json_serialize_to_file_pretty (root_value, patameter_info->filename);
                }
                break;

            case JSONBoolean:
                if (0 != json_object_set_boolean (root_value_tmp->value.object,
                                                  patameter_info->parametername,
                                                  atoi (patameter_info->value)))
                {
                    json_value_free(root_value);
                    return -1;
                }
                json_serialize_to_file_pretty (root_value, patameter_info->filename);
                break;

            default:
                break;
        }
        json_value_free(root_value);
        return 0;
    }
}

int
libmstar_conf_file_patameter_write (char *interface_tmp, char *dest_mac,
                                    libmstar_patameter_info_t *patameter_info)
{
    vs_file_access_cmd_t cmd;

    memset (&cmd, 0x0, sizeof (cmd));
    cmd.file_type = VS_FILE_ACCESS_REQ_FILE_TYPE_GENERAL_FILE;
    strcpy (cmd.parameter, patameter_info->filename);
    sprintf (cmd.file_name, "/tmp%s", patameter_info->filename);

    cmd.op = VS_FILE_ACCESS_REQ_OP_SAVE;
    if (LIBMSTAR_SUCCESS != libmstar_vs_file_access_req (interface_tmp, dest_mac, &cmd))
    {
        printf ("write parameter read %s fail\n", cmd.parameter);
        return -1;
    }
    strcpy (patameter_info->filename, cmd.file_name);
    libmstar_json_object_set_patameter_value (patameter_info);

    cmd.op = VS_FILE_ACCESS_REQ_OP_WRITE;
    if (LIBMSTAR_SUCCESS != libmstar_vs_file_access_req (interface_tmp, dest_mac, &cmd))
    {
        printf ("write parameter read %s fail\n", cmd.parameter);
        return -1;
    }

    return 0;
}

libmstar_error_t
libmstar_vs_product (char *interface, char *dest_mac,
                           libmstar_vs_product_req_t *product_req,
                           libmstar_vs_product_cnf_t *product_cnf)
{								
    unsigned int result_len = 0;
    unsigned char dest_bin[MAC_BIN_LEN] = { 0 };
    mme_ctx_t request_ctx, confirm_ctx;
    unsigned char snd_buffer[ETH_DATA_LEN] = { 0 };

    if (NULL == dest_mac || NULL == product_req || NULL == product_cnf)
    {
        return LIBMSTAR_ERROR_PARAM;
    }

    libmstar_mac_str_to_bin (dest_mac, dest_bin);
									
    /* initialize indication MME for VS_PRODUCT REG/CNF */
    if (MME_SUCCESS != mme_init (&request_ctx, VS_PRODUCT | MME_REQ,
		                         snd_buffer, (unsigned int)ETH_DATA_LEN))
    {
		return LIBMSTAR_ERROR_PARAM;
    }

    if (MME_SUCCESS != mme_init (&confirm_ctx, VS_PRODUCT | MME_CNF,
                                  snd_buffer, (unsigned int)ETH_DATA_LEN))
    {
        return LIBMSTAR_ERROR_PARAM;
    }

    /* BIG ENDIAN*/
    product_req->moduleid = HTOBE16(product_req->moduleid);
    /* prepare req data */
    if (MME_SUCCESS != mme_put (&request_ctx, product_req,
		                        sizeof (libmstar_vs_product_req_t), &result_len))
    {
        return LIBMSTAR_ERROR_PARAM;
    }

    /* send indication MME */
    if (MME_SUCCESS != mme_send (&request_ctx, MME_SEND_REQ_CNF, interface,
                                 dest_bin, &confirm_ctx, MME_MMV1, SPIDCOM_OUI))
    {
        return LIBMSTAR_ERROR_PARAM;
    }

    /* get cnf parameter data */
    if (MME_SUCCESS != mme_pull (&confirm_ctx, product_cnf,
                                 sizeof (libmstar_vs_product_cnf_t), &result_len))
    {
        return LIBMSTAR_ERROR_PARAM;
    }

    return LIBMSTAR_SUCCESS;
}


libmstar_error_t
libmstar_vs_get_mst_version_with_multi_response (char *interface, char *dest_mac, libmstar_vs_get_version_cnf_t **version_cnf_list)
{
    unsigned int result_len = 0;
    unsigned char dest_bin[MAC_BIN_LEN] = { 0 };
    unsigned char snd_buffer[ETH_DATA_LEN] = { 0 };
    mme_ctx_t request_ctx;
    mme_ctx_t *confirm_ctx_list = NULL;
    mme_ctx_t *node_mme = NULL;
    int ret  = LIBMSTAR_SUCCESS;

    if (interface == NULL || dest_mac == NULL || version_cnf_list == NULL)
        return LIBMSTAR_ERROR_PARAM;
    
    libmstar_mac_str_to_bin (dest_mac, dest_bin);

    /* initialize indication MME for VS_GET_VERSION_REG/CNF */
    if (MME_SUCCESS != mme_init (&request_ctx, VS_GET_VERSION | MME_REQ,
                                 snd_buffer, (unsigned int)ETH_DATA_LEN))
    {
        return LIBMSTAR_ERROR_MME;
    }

    /* send indication MME */
    ret = mme_send_with_multi_response (&request_ctx, MME_SEND_REQ_CNF, interface, dest_bin, &confirm_ctx_list, MME_MMV1, SPIDCOM_OUI);
    if (ret < MME_SUCCESS)
    {
        ret = LIBMSTAR_ERROR_MME;
        goto err;
    }

    node_mme = confirm_ctx_list;
    while (node_mme)
    {
        libmstar_vs_get_version_cnf_t *node_dev = malloc(sizeof(libmstar_vs_get_version_cnf_t)); 
        if (!node_dev)
        {
            ret = LIBMSTAR_ERROR_NO_SPACE;
            goto err;
        }
            
        memset (node_dev, 0, sizeof(libmstar_vs_get_version_cnf_t));
        if (MME_SUCCESS != mme_pull (node_mme, &node_dev->result, 1, &result_len) )
        {
            ret = LIBMSTAR_ERROR_MME;
            goto err;
        }

        if (MME_SUCCESS != mme_pull (node_mme, &node_dev->device_id, 2, &result_len) )
        {
            ret = LIBMSTAR_ERROR_MME;
            goto err;
        }

        if (MME_SUCCESS != mme_pull (node_mme, &node_dev->current_image_index, 1, &result_len) )
        {
            ret = LIBMSTAR_ERROR_MME;
            goto err;
        }

        if (MME_SUCCESS != mme_pull (node_mme, node_dev->applicative_version, 16, &result_len) )
        {
            ret = LIBMSTAR_ERROR_MME;
            goto err;
        }

        if (MME_SUCCESS != mme_pull (node_mme, node_dev->av_stack_version, 64, &result_len) )
        {
            ret = LIBMSTAR_ERROR_MME;
            goto err;
        }

        if (MME_SUCCESS != mme_pull (node_mme, node_dev->applicative_alternate, 16, &result_len) )
        {
            ret = LIBMSTAR_ERROR_MME;
            goto err;
        }

        if (MME_SUCCESS != mme_pull (node_mme, node_dev->bootloader_version, 64, &result_len) )
        {
            ret = LIBMSTAR_ERROR_MME;
            goto err;
        }

        memcpy (node_dev->mac, node_mme->mac, 6);

        /* ADD this new node to list head */
        if (*version_cnf_list)
            node_dev->next = (*version_cnf_list);
        (*version_cnf_list) = node_dev;

        node_mme = node_mme->next;
    }
err:
    if (confirm_ctx_list)
        free_mme_ctx (confirm_ctx_list);

    return LIBMSTAR_SUCCESS;
}


libmstar_error_t
libmstar_vs_get_cco_dev_with_multi_response (char *interface, char *dest_mac, libmstar_vs_get_cco_dev_cnf_t **devcnf_head)
{
    unsigned int result_len = 0;
    unsigned char dest_bin[MAC_BIN_LEN] = { 0 };
    unsigned char snd_buffer[ETH_DATA_LEN] = { 0 };
    mme_ctx_t request_ctx;
    mme_ctx_t *confirm_ctx_list_head = NULL;
    mme_ctx_t *node_mme = NULL;
    int ret  = LIBMSTAR_SUCCESS;

    if (interface == NULL || dest_mac == NULL || devcnf_head == NULL)
        return LIBMSTAR_ERROR_PARAM;
    *devcnf_head = NULL;
    libmstar_mac_str_to_bin (dest_mac, dest_bin);

    /* initialize indication MME for VS_GET_VERSION_REG/CNF */
    if (MME_SUCCESS != mme_init (&request_ctx, VS_GET_CCO_DEV | MME_REQ,
                                 snd_buffer, (unsigned int)ETH_DATA_LEN))
    {
        return LIBMSTAR_ERROR_MME;
    }

    /* send indication MME */
    ret = mme_send_with_multi_response (&request_ctx, MME_SEND_REQ_CNF, interface,
                                 dest_bin, &confirm_ctx_list_head, MME_MMV1, SPIDCOM_OUI);
    if (ret != MME_SUCCESS)
    {
        ret = LIBMSTAR_ERROR_MME;
        goto err;
    }

    node_mme = confirm_ctx_list_head;
    while (node_mme)
    {
        libmstar_vs_get_cco_dev_cnf_t *node_dev = malloc(sizeof(libmstar_vs_get_cco_dev_cnf_t)); 
        if (!node_dev)
        {
            ret = LIBMSTAR_ERROR_NO_SPACE;
            goto err;
        }
            
        memset (node_dev, 0, sizeof(libmstar_vs_get_cco_dev_cnf_t));
        if (MME_SUCCESS != mme_pull (node_mme, &node_dev->result, 1, &result_len) )
        {
            ret = LIBMSTAR_ERROR_MME;
            goto err;
        }

        if (MME_SUCCESS != mme_pull (node_mme, node_dev->mac, 6, &result_len))
        {
            ret = LIBMSTAR_ERROR_MME;
            goto err;
        }
        
        /* ADD this new node to list head */
        if (*devcnf_head)
            node_dev->next = (*devcnf_head);
        (*devcnf_head) = node_dev;

        node_mme = node_mme->next;
    }
err:
    if (confirm_ctx_list_head)
        free_mme_ctx (confirm_ctx_list_head);

    return LIBMSTAR_SUCCESS;

}

/* free libmstar_vs_get_cco_dev_cnf_t. only call after with multi response.*/
void free_vs_cco_dev_cnf (libmstar_vs_get_cco_dev_cnf_t *ptr)
{
    libmstar_vs_get_cco_dev_cnf_t *node = ptr;
    libmstar_vs_get_cco_dev_cnf_t *tmp = ptr;
    while (node)
    {
        tmp = node;
        node = node->next;
        free (tmp);
    }
    ptr =NULL;
}

/* free libmstar_vs_get_version_cnf_t. only call after with multi response.*/
void free_vs_get_version_cnf (libmstar_vs_get_version_cnf_t *ptr)
{
    libmstar_vs_get_version_cnf_t *node = ptr;
    libmstar_vs_get_version_cnf_t *tmp = ptr;
    while (node)
    {
        tmp = node;
        node = node->next;
        free (tmp);
    }
    ptr = NULL;
}