/* WIFI bundle {{{
 *
 * Copyright (C) 2013 Mstar
 *
 * <<<Licence>>>
 *
 * }}} */
/**
 * \file    lib/libmme/mme.c
 * \brief   MME management library
 * \ingroup libmme
 *
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <unistd.h>
#include <errno.h>
#include <linux/if_packet.h>
#include <linux/if_ether.h>
#include <sys/ioctl.h>
#include <net/if.h>
#include <net/ethernet.h>
#include <inttypes.h>
#include <syslog.h>

#include "libmme.h"
#include "platform_endian.h"

static void
mme_check_oui (mme_ctx_t *ctx, MME_t *mh, unsigned char *pkt, int oui_type)
{
    int oui_offset = sizeof (MME_t);
    if (mh->mmv == 0) oui_offset -= 2;

    /* Fill OUI information */
    if (MMTYPE_IS_VS(mh->mmtype))
    {
        /* Set MME_OUI_SPIDCOM only if it was not set before */
        if ((memcmp (OUI_SPIDCOM, pkt + oui_offset, 3) == 0)
            && ((ctx->oui == MME_OUI_NOT_PRESENT) || (ctx->oui == MME_OUI_SPIDCOM)))
        {
            ctx->oui = MME_OUI_SPIDCOM;
        }
        else if ((memcmp (OUI_QCALCOM, pkt + oui_offset, 3) == 0)
                 && ((ctx->oui == MME_OUI_NOT_PRESENT) || (ctx->oui == MME_OUI_QCA)))
        {
            ctx->oui = MME_OUI_QCA;
        }
        else /* the OUI check failed */
        {
            /* One fragment of the MME (if fmi_fmsn != 0) did not
            ** contain SPiDCOM OUI, so the whole MME will be
            ** marked as a foreign VS MME */
            ctx->oui = MME_OUI_NOT_SPIDCOM;
        }
    }
}

/**
 * Construct the FMI field of the MME header.
 * \param  nf_mi  number of fragments
 * \param  fn_mi  fragment number
 * \param  fmsn  fragmentation message sequence number
 * \return  the FMI field.
 */
static inline unsigned short
mme_fmi_set (uint nf_mi, uint fn_mi, uint fmsn)
{
    return (fmsn << 8) | (nf_mi << 4) | fn_mi;
}

/**
 * Get the FMI field uncompressed from the MME header.
 * \param  fmi  the compressed FMI
 * \param  nf_mi  number of fragments
 * \param  fn_mi  fragment number
 * \param  fmsn  fragmentation message sequence number
 */
static inline void
mme_fmi_get (const unsigned short fmi, uint *nf_mi,
             uint *fn_mi, uint *fmsn)
{
    *fn_mi = fmi & 0xF;
    *nf_mi = (fmi >> 4) & 0xF;
    *fmsn = (fmi >> 8) & 0xFF;
}

/**
 * Create a MME message context. This context is used to build the MME message step by step.<br>
 * The provided buffer must be enough large to contain all the final payload.<br>
 * The MME payload can be bigger than an ethernet payload, as the fragmentation is managed by the 'send' function.
 *
 * \param  ctx MME context to fill with init value
 * \param  mmtype type of MME message (must be in official type list)
 * \param  buffer the buffer to put the payload
 * \param  length  the buffer length
 * \return error type (MME_SUCCESS if success)
 */

mme_error_t
mme_init (mme_ctx_t *ctx, const mmtype_t mmtype, unsigned char *buffer, const unsigned int length)
{
    /* protect from null pointers */
    if (ctx == NULL || buffer == NULL)
        return MME_ERROR_GEN;

    ctx->buffer     =   buffer;
    ctx->mmtype     =   mmtype;
    ctx->length     =   length;
    ctx->head       =   0;
    ctx->tail       =   0;

    /* By default the MME is not VS */
    ctx->oui        =   MME_OUI_NOT_PRESENT;
    ctx->status     =   MME_STATUS_OK;

    ctx->next       =   NULL;

    return MME_SUCCESS;
}

/** Get the current MME payload length
 *
 * \param ctx MME context to get the payload length
 * \param length the returned length
 * \return error type (MME_SUCCESS if success)
 * \return MME_ERROR_NOT_INIT: context not initialized
 */

mme_error_t
mme_get_length (mme_ctx_t *ctx, unsigned int *length)
{
    /* protect from null pointers */
    if (ctx == NULL || length == NULL)
        return MME_ERROR_GEN;
    /* check if ctx has been inititalized */
    if (ctx->status == MME_STATUS_INIT)
        return MME_ERROR_NOT_INIT;

    *length = ctx->tail - ctx->head;
    return MME_SUCCESS;
}

/** Get the remaining free space to add payload
 *
 * \param ctx MME context to get the remaining space
 * \param length the remaining space
 * \return error type (MME_SUCCESS if success)
 * \return MME_ERROR_NOT_INIT: context not initialized
 */

mme_error_t
mme_get_free_space (mme_ctx_t *ctx, unsigned int *length)
{
    /* protect from null pointers */
    if (ctx == NULL || length == NULL)
        return MME_ERROR_GEN;
    /* check if ctx has been inititalized */
    if (ctx->status == MME_STATUS_INIT)
        return MME_ERROR_NOT_INIT;

    *length = ctx->length - (ctx->tail - ctx->head);
    return MME_SUCCESS;
}


/**
 * Push data at the beginning of the MME payload. Data are inserted between the start of buffer and the current payload data.<br>
 * MME data tail and length are updated.<br>
 * If there is not enough free place to push data, an error is returned and the remaining free space length is returned.
 *
 * \param  ctx MME context where to push data
 * \param  data data to be pushed into payload
 * \param  length length of data to push
 * \param  result_length length of data really pushed
 * \return error type (MME_SUCCESS if success)
 * \return MME_ERROR_NOT_INIT: context not initialized
 * \return MME_ERROR_SPACE: not enough available space
 */

mme_error_t
mme_push (mme_ctx_t *ctx, const void *data, unsigned int length, unsigned int *result_length)
{
    unsigned int free = 0;
    int delta = 0;

    /* protect from null pointers */
    if (ctx == NULL || data == NULL || result_length == NULL)
        return MME_ERROR_GEN;
    /* check if ctx has been inititalized */
    if (ctx->status == MME_STATUS_INIT)
        return MME_ERROR_NOT_INIT;

    free = ctx->length - (ctx->tail - ctx->head);
    if (length > free)
    {
        *result_length = free;
        return MME_ERROR_SPACE;
    }

    *result_length = length;

    /* make place in front, if needed */
    if (length > ctx->head)
    {
        /*
         *             *length
         *  .-----------^-----------.
         *  |---------|xxxxxxxxxxxxx|--------------------|---------------------|
         * buff      head                              tail                 ctx->length
         *             \_______________  _______________/
         *                             \/
         *                           pyload
         *
         * we have to shift right our payload for this difference delta marked with 'x'
         * in order for *length bytes to fit in from beginning of the buffer
         */
        delta = length - ctx->head;
        memmove (ctx->buffer + ctx->head + delta, ctx->buffer + ctx->head, ctx->tail - ctx->head);

        /* update head and tail pointers (offsets) */
        ctx->head += delta;
        ctx->tail += delta;
    }

    ctx->head -= length;    /* place head pointer to where copy chould start from */
    memcpy (ctx->buffer + ctx->head, data, length);

    return MME_SUCCESS;
}

/**
 * Put data at the end of MME payload. MME data tail and length are updated<br>
 * If there is not enough free place to put data, an error is returned and the remaining free space length is returned.
 *
 * \param  ctx MME context where to put data
 * \param  data data to put at the end of MME payload
 * \param  length length of data to put
 * \param  result_length length of data really put
 * \return error type (MME_SUCCESS if success)
 * \return MME_ERROR_NOT_INIT: context not initialized
 * \return MME_ERROR_SPACE: not enough available space
 */

mme_error_t
mme_put (mme_ctx_t *ctx, const void *data, unsigned int length, unsigned int *result_length)
{
    unsigned int free = 0;
    int delta = 0;

    /* protect from null pointers */
    if (ctx == NULL || data == NULL || result_length == NULL)
        return MME_ERROR_GEN;
    /* check if ctx has been inititalized */
    if (ctx->status == MME_STATUS_INIT)
        return MME_ERROR_NOT_INIT;

    free = ctx->length - (ctx->tail - ctx->head);
    if (length > free)
    {
        *result_length = free;
        return MME_ERROR_SPACE;
    }

    *result_length = length;

    /* make place after payload, if needed */
    if (length > ctx->length - ctx->tail)
    {
        /*
         *                                       *length
         *                           .---------------^-----------------.
         *  |-----------|------------|xxxxxxx|-------------------------|
         * buff        head                 tail                    ctx->length
         *               \________  ________/
         *                        \/
         *                      payload
         *
         * we have to shift left our payload for this difference delta marked with 'x'
         * in order for *length bytes to fit in from beginning of the buffer
         */
        delta = length - (ctx->length - ctx->tail);
        memmove ((unsigned char *) (ctx->buffer + ctx->head - delta), (unsigned char *) (ctx->buffer + ctx->head), ctx->tail - ctx->head);

        /* update head and tail pointers (offsets) */
        ctx->head -= delta;
        ctx->tail -= delta;
    }

    memcpy ((unsigned char *) (ctx->buffer + ctx->tail), (unsigned char *) data, length);
    ctx->tail += length;

    return MME_SUCCESS;
}

/**
 * Pop (get) data from the end of MME payload. MME data tail and length are updated<br>
 * If there is not enough data to pull, an error is returned and the remaining payload length is returned.
 *
 * \param  ctx MME context where to get data
 * \param  data buffer where to get data from the end of MME payload
 * \param  length length of data to pull
 * \param result_length length of data gotten; if there is not enough data into the MME to fit the length, the remaining data length is returned
 * \return error type (MME_SUCCESS if success)
 * \return MME_ERROR_NOT_INIT: context not initialized
 * \return MME_ERROR_SPACE: not enough available space
 */

mme_error_t
mme_pop (mme_ctx_t *ctx, void *data, unsigned int length, unsigned int *result_length)
{
    /* protect from null pointers */
    if (ctx == NULL || data == NULL || result_length == NULL)
        return MME_ERROR_GEN;
    /* check if ctx has been inititalized */
    if (ctx->status == MME_STATUS_INIT)
        return MME_ERROR_NOT_INIT;

    /* check if it is demanded more data than we have in payload */
    if (length > ctx->tail - ctx->head)
    {
        *result_length = ctx->tail - ctx->head;
        return MME_ERROR_ENOUGH;
    }

    *result_length = length;

    memcpy (data, (unsigned char *) (ctx->buffer + ctx->tail - length), length);
    ctx->tail -= length;

    return MME_SUCCESS;
}

static int
_get_iface_mac_addr (int sock, char *iface, unsigned char *mac_addr)
{
    struct ifreq ifr;

    memset (&ifr, 0x0, sizeof (ifr));
    /* Get the interface Index  */
    strncpy (ifr.ifr_name, iface, IFNAMSIZ);
    if ((ioctl (sock, SIOCGIFINDEX, &ifr)) < 0)
    {
        return -1;
    }
    if (ioctl (sock, SIOCGIFHWADDR, &ifr) < 0)
    {
        return -1;
    }
    memcpy (mac_addr, ifr.ifr_hwaddr.sa_data, ETH_ALEN);
    return 0;
}

/**
* Build the MME header. It calculates the number of packet and returns it.
* \param mh MME header, filled by this function
* \param ctx MME context to send
* \param src_mac MAC Address of the source
* \param dest MAC Address of the destination
* \return int The number of packets required to send the MME
*/

int
mme_header_prepare (MME_t *mh, mme_ctx_t *ctx, unsigned char *src_mac,
                    unsigned char *dest, int mmv)
{
    int   nbpkt;
    uint  fmi_nf_mi = 0; /* Number of fragments */
    uint  fmi_fmsn = 0; /* SSN of the fragmented mme */
    uint  payload_size;
    /*
     * --- Create MME header ---
     */

    /* copy the Src mac addr */
    memcpy (mh->mme_src, (void *)src_mac, 6);
    /* copy the Dst mac addr */
    memcpy (mh->mme_dest, (void *)dest, 6);
    /* copy the protocol */
    mh->mtype = htons (MME_TYPE);
    /* set default mme version */
    mh->mmv = 0;
    mh->mmtype =  LE16TOH (ctx->mmtype);

    /* Set the max payload size */
    SET_MME_PAYLOAD_MAX_SIZE(mh->mmtype, payload_size);

    if (mmv == 1)
    {
        /* calculate number of mme packets needed */
        nbpkt = (ctx->tail - ctx->head) / payload_size;
        if (nbpkt == 0 || ((ctx->tail - ctx->head) % payload_size) > 0)
        {
            nbpkt++;
        }

        mh->mmv = 1;
        fmi_nf_mi  = nbpkt - 1;
        /* Set the sequence number to 1 if the message is fragmented.  This number
         * correspond to a session number, all fragmented MME of this session must
         * have the same fmsn number. */
        fmi_fmsn = fmi_nf_mi ? 1 : 0;
        mh->fmi = mme_fmi_set (fmi_nf_mi, 0, fmi_fmsn);
    }
    else
        return 1;

    return nbpkt;
}

/**
 * Pull (get) data from beginning of MME payload. MME data head and length are updated<br>
 * If there is not enough data to pull, an error is returned and the remaining payload length is returned.
 *
 * \param  ctx MME context where to get data
 * \param  data buffer where to get data from the beginning of MME payload
 * \param  length length of data to pull
 * \param result_length length of data pulled; if there is not enough data into the MME to fit the length, the remaining data length is returned
 * \return error type (MME_SUCCESS if success)
 * \return MME_ERROR_NOT_INIT: context not initialized
 */

mme_error_t
mme_pull (mme_ctx_t *ctx, void *data, unsigned int length, unsigned int *result_length)
{
    /* protect from null pointers */
    if (ctx == NULL || data == NULL || result_length == NULL)
        return MME_ERROR_GEN;
    /* check if ctx has been inititalized */
    if (ctx->status == MME_STATUS_INIT)
        return MME_ERROR_NOT_INIT;

    /* check if it is demanded more data than we have in payload */
    if (length > ctx->tail - ctx->head)
    {
        *result_length = ctx->tail - ctx->head;
        memcpy (data, (unsigned char *) (ctx->buffer + ctx->head), *result_length);
        return MME_ERROR_ENOUGH;
    }

    *result_length = length;

    memcpy (data, (unsigned char *) (ctx->buffer + ctx->head), length);
    ctx->head += length;

    return MME_SUCCESS;
}

/**
* Build a packet from the context. This function will be called by mme_send
* before sending each fragment of the packet. The function will update the
* fragment offset.
* This function is for internal use, and should not be used outside the
* library.
* \param mh MME header, will be updated with the correct fragment number
* \param ctx MME context to send
* \param fo Fragment offset, from which data must be sent
* \param index Fragment number
* \param pkt Pointer to the head of the data buffer
* \return int Size of the packet (including header)
*/

int
mme_prepare (MME_t *mh, mme_ctx_t *ctx, int *fo, int index, unsigned char *pkt, int oui_type)
{
    uint  fs = 0;
    uint  fmi_nf_mi = 0; /* Number of fragments */
    uint  fmi_fn_mi = 0; /* Fragment number */
    uint  fmi_fmsn = 0; /* SSN of the fragmented mme */
    uint  payload_size, payload_min_size;

    SET_MME_PAYLOAD_MAX_SIZE(mh->mmtype, payload_size);
    SET_MME_PAYLOAD_MIN_SIZE(mh->mmtype, payload_min_size);

    /*
     * --- Set per-message fragmentation info, current fragment number ---
     */
    mme_fmi_get (mh->fmi, &fmi_nf_mi, &fmi_fn_mi, &fmi_fmsn);
    mh->fmi = mme_fmi_set (fmi_nf_mi, index, fmi_fmsn);

    /*
     * --- Append payload ---
     */

    /* In case of VS MME, append the OUI first */
    if (MMTYPE_IS_VS(mh->mmtype))
    {
        if (oui_type == QCA_OUI)
        {
            memcpy(pkt, OUI_QCALCOM, QCA_OUI_SIZE);
            pkt += QCA_OUI_SIZE;
        }
        else
        {
            memcpy(pkt, OUI_SPIDCOM, SPIDCOM_OUI_SIZE);
            pkt += SPIDCOM_OUI_SIZE;
        }
    }

    /* calculate the size of the current fragment */
    fs = ((ctx->tail - *fo) < payload_size) ? (ctx->tail - *fo) : payload_size;
    /* copy the content into packet buffer */
    memcpy (pkt, ctx->buffer + *fo, fs);
    /* update fo */
    *fo += fs;

    /* zero-pad the rest if last packet fs < MME_MIN_SIZE, which is minimum size for mme packet */
    if (fs < payload_min_size)
    {
        memset (pkt + fs, 0x0, payload_min_size - fs);
        fs = payload_min_size;
    }

    /* In case of VS MME, add SPIDCOM_OUI_SIZE to the payload */
    if (MMTYPE_IS_VS(mh->mmtype))
    {
        fs += SPIDCOM_OUI_SIZE;
    }

    return (fs + sizeof (MME_t));
}

/**
 * Send MME to network interface and wait for confirm MME; this is a synchronous transmit and receive transaction
 * (function waits for answer MME before returning). If no answer packet is received before MME_CONFIRM_TIMEOUT,
 * error is returned.<br>
 * As MME are level 2 and IP routing does not apply, the network interface where to send data packet must be selected inside
 * iface paramater. For instance : "eth0", "br0", ...<br>
 * If an MME needs to be fragmented, the sending process is in charge to manage the fragmentation. The same as reassembling
 * the fragmented MME confirm.<br>
 * 3 types of communication can be done :
 * <ul><li>MME_SEND_REQ_CNF : request and confirm transaction
 * <li> MME_SEND_CNF : confirm only (no answer expected)
 * <li>MME_SEND_IND_RSP : indicate and response transaction
 * <li>MME_SEND_IND : indicate only (no answer expected)
 * </ul>
 *
 * \param  ctx MME context to send
 * \param  type transaction type
 * \param  iface selected communication interface; if NULL, "br0" if remote destination, "lo" if local
 * \param  dest destination MAC address in binary format (6 bytes)
 * \param  confirm_ctx MME context given to put the received MME answer packet. A adapted buffer must have been provided.
 * \return error type (MME_SUCCESS if success)
 * \return MME_ERROR_NOT_INIT: context not initialized
 */

mme_error_t
mme_send (mme_ctx_t *ctx, mme_send_type_t type, char *iface, unsigned char *dest, mme_ctx_t *confirm_ctx, int mmv, int oui_type)
{
    int raw;
    struct sockaddr_ll sll;
    struct ifreq ifr;
    MME_t *mh;
    unsigned char src_mac[6] = {0};     /* MAC of chosen interface */
    unsigned char *pkt;
    unsigned char pkt_holder[ETH_PACKET_MAX_NOVLAN_SIZE] = {0};
    int pkt_len;
    int sent;
    unsigned short nbpkt = 0;
    fd_set fds;
    uint mme_h_size; /* Size of MME header */
    int fo;      /* current fragment offset */
    struct sockaddr_ll pkt_info;
    int last_pkt = 0;
    uint fmi_nf_mi = 0; /* Number of fragments */
    uint fmi_fn_mi = 0; /* Fragment number */
    uint fmi_fmsn = 0; /* SSN of the fragmented mme */
    struct timeval timeout; /* When we should time out */
    struct timeval now;
    struct timeval waittime;
    int res;
    int len;
    unsigned int result_len;
    int i;
    int pkt_nfo_sz = sizeof (pkt_info);
    mme_error_t ret;
    uint pkt_cnt = 0;
    char *iface_real;

    /* protect from null pointers */
    if (ctx == NULL || confirm_ctx == NULL || dest == NULL)
        return MME_ERROR_GEN;
    /* check if ctx has been inititalized */
    if (ctx->status == MME_STATUS_INIT)
        return MME_ERROR_NOT_INIT;

    /*
     * #####################
     * #    SEND
     * #####################
     */

    /*
     * --- Create the raw socket ---
     */
    if ((raw = socket (PF_PACKET, SOCK_RAW, htons (ETH_P_HPAV))) == -1)
    {
        perror ("Error creating raw socket: ");
        return MME_ERROR_GEN;
    }

    memset (&sll, 0x0, sizeof (sll));
    memset (&ifr, 0x0, sizeof (ifr));

    /* keep the same interface */
    iface_real = iface;

    /* get the bridge mac-address */
    _get_iface_mac_addr (raw, iface_real, src_mac);

    /* Get index of needed interface  */
    strncpy (ifr.ifr_name, iface_real, IFNAMSIZ);
    if ((ioctl (raw, SIOCGIFINDEX, &ifr)) == -1)
    {
        printf ("%s: Error getting %s index !\n", __FUNCTION__, iface_real);
        close (raw);
        return MME_ERROR_GEN;
    }

    /* Bind our raw socket to this interface */
    sll.sll_family = AF_PACKET;
    sll.sll_ifindex = ifr.ifr_ifindex;
    sll.sll_protocol = htons (ETH_P_HPAV);
    if ((bind (raw, (struct sockaddr *)&sll, sizeof (sll)))== -1)
    {
        perror ("Error binding raw socket to interface\n");
        close (raw);
        return MME_ERROR_GEN;
    }

    mh = (MME_t *)pkt_holder;

    nbpkt = mme_header_prepare (mh, ctx, src_mac, dest, mmv);

    if (mmv == 1)
    {
        mme_fmi_get (mh->fmi, &fmi_nf_mi, &fmi_fn_mi, &fmi_fmsn);

        /* Security check, if nbpkt is more than 15 don't send anything and inform
         * the user. */
        if (fmi_nf_mi > 0xf)
        {
            printf("The MME to send is too long and cannot be fragmented\n");
            return MME_ERROR_SPACE;
        }
    }

    /* initialize fragment offset to the begining of the payload*/
    fo = ctx->head;

    /* prepare and send out packets */
    for (i=0; i<nbpkt; i++)
    {
        /*
         * --- Append payload ---
         */
        /* skip the MME header */
        pkt = pkt_holder + sizeof (MME_t);

        /* mme version 0 without fmi */
        if (mmv == 0)   pkt -= 2;

        /* determine the size of whole packet (header + data) */
        pkt_len = mme_prepare (mh, ctx, &fo, i, pkt, oui_type);
        if (mmv == 0)   pkt_len -= 2;

        /*
         * --- Send raw packet ---
         */
        /* initialize pkt pointer to the start of the send buffer */
        pkt = pkt_holder;

        if ((sent = write (raw, pkt, pkt_len)) != pkt_len)
        {
            perror ("string error\n");
            /* Error */
            syslog (LOG_WARNING, "%s: Could only send %d bytes of packet of length %d", __FUNCTION__, sent, pkt_len);
            close (raw);
            return MME_ERROR_GEN;
        }
    } /* for */

    /* we should exit here if we do not have to wait on response */
    if (type == MME_SEND_CNF || type == MME_SEND_IND)
    {
        /* if we had to send a .CNF or .IND only we have finished,
         * beacuse these MME messages does not require response */
        close (raw);
        return MME_SUCCESS;
    }

    /*
     * #####################
     * #    RECEIVE
     * #####################
     */

    /* Set the MME Header size */
    SET_MME_HEADER_SIZE(ctx->mmtype, mme_h_size);
    if (mmv == 0) mme_h_size -= 2;

    /* Calculate into 'timeout' when we should time out */
    gettimeofday (&timeout, 0);
    if (ctx->mmtype == VS_GET_VERSION)
    {
        /* Get version timeout 100ms */
        timeout.tv_usec += 100 * 1000;
    }
    else if (ctx->mmtype == QCA_FAC_DEFAULT)
    {
        /* Fac default timeout 1s */
        timeout.tv_sec += 1;
    }
    else
    {
        timeout.tv_sec += MME_TOUT_MS / 1000;
        timeout.tv_usec += (MME_TOUT_MS & 1000) * 100;
    }

    /* initialize packet counter */
    pkt_cnt = 0;

    do  /* receive the payload, packet by packet */
    {
        /* rewind pkt pointer to begining of the packet holder */
        pkt = pkt_holder;

        for (;;)  /* try to receive until we get response from our proper server */
        {
            /* Force timeout if we ran out of time */
            gettimeofday (&now, 0);
            if (timercmp (&now, &timeout, >) != 0)
            {
                printf ("Server response timeout.\n");
                close (raw);
                return MME_ERROR_TIMEOUT;
            }

            /* Calculate into 'waittime' how long to wait */
            timersub (&timeout, &now, &waittime); /* wait = timeout - now */
            
            /* Init fds with 'sock' as the only fd */
            FD_ZERO (&fds);
            FD_SET (raw, &fds);

            /* fds fd_set is containing only our socket, so no need to check with FD_ISSET() */
            res = select (raw + 1, &fds, NULL, NULL, &waittime);

            if (res < 0)
            {
                perror ("select() failed");
                close (raw);
                return MME_ERROR_GEN;
            }
            else if (res == 0)  /* timeout */
            {
                printf ("%02x:%02x:%02x:%02x:%02x:%02x MMTYPE %04x MMV %d EoC OUI:%d response timeout %ld seconds\n",
                        dest[0], dest[1], dest[2], dest[3], dest[4], dest[5], ctx->mmtype, mmv, oui_type, timeout.tv_sec);
                close (raw);
                return MME_ERROR_TIMEOUT;
            }

            if ((len = recvfrom (raw, pkt, ETH_PACKET_MAX_NOVLAN_SIZE, 0, (struct sockaddr*)&pkt_info, (socklen_t *)&pkt_nfo_sz)) < 0)
            {
                perror ("Recv from returned error: ");
                close (raw);
                return MME_ERROR_TXRX;
            }
            //unsigned char *tmp = pkt_info.sll_addr;
            //printf ("recv %d from %02x:%02x:%02x:%02x:%02x:%02x\n", len,
            //           tmp[0], tmp[1], tmp[2], tmp[3], tmp[4], tmp[5]);

            mh =  (MME_t *)pkt;
            mme_fmi_get (mh->fmi, &fmi_nf_mi, &fmi_fn_mi, &fmi_fmsn);


            /* check if response came from the our proper server, and if it is,
             * check if correspondent response was sent (REQ/CNF or IND/RSP);
             * by HP AV standard difference is in LSB, i.e. if REQ LSBs are ...00,
             * CNF will be ...01, IND ...10 and RSP ...11 */
            if ((mh->mmtype == (VS_GET_VERSION | MME_CNF)
                 || memcmp (pkt_info.sll_addr, dest, 6) == 0 /* we received packet from correct source */)
                && mh->mmtype == confirm_ctx->mmtype /* we received type that we expect */
                && fmi_fn_mi == pkt_cnt ) /* packets are coming in the right order */

            {
                mme_check_oui (confirm_ctx, mh, pkt, oui_type);
                break;  /* good answer - go out and process the package */
            }
        } /* for */

        /* check if the packet we received is the last one */
        if (fmi_fn_mi == fmi_nf_mi)
            last_pkt = 1;

        /* skip the MME header */
        pkt += mme_h_size;
        len -= mme_h_size;

        /* copy the packet into the receive buffer */
        ret = mme_put (confirm_ctx, pkt, len, &result_len);
        if (ret != MME_SUCCESS)
        {
            close (raw);
            return ret;
        }

        pkt_cnt++;

    } while (!last_pkt);

    memcpy(dest, pkt_info.sll_addr, 6);
    close (raw);
    return MME_SUCCESS;
}


int
mme_send_with_multi_response (mme_ctx_t *ctx, mme_send_type_t type, char *iface, unsigned char *dest, 
    mme_ctx_t **confirm_ctx_res, int mmv, int oui_type)
{
    int raw;
    struct sockaddr_ll sll;
    struct ifreq ifr;
    MME_t *mh;
    unsigned char src_mac[6] = {0};     /* MAC of chosen interface */
    unsigned char *pkt;
    unsigned char pkt_holder[ETH_PACKET_MAX_NOVLAN_SIZE] = {0};
    int pkt_len;
    int sent;
    unsigned short nbpkt = 0;
    fd_set fds;
    uint mme_h_size; /* Size of MME header */
    int fo;      /* current fragment offset */
    struct sockaddr_ll pkt_info;
    //int last_pkt = 0;
    uint fmi_nf_mi = 0; /* Number of fragments */
    uint fmi_fn_mi = 0; /* Fragment number */
    uint fmi_fmsn = 0; /* SSN of the fragmented mme */
    struct timeval timeout; /* When we should time out */
    //struct timeval now;
    struct timeval waittime;
    int res;
    int len;
    unsigned int result_len;
    int i;
    int pkt_nfo_sz = sizeof (pkt_info);
    //mme_error_t ret;
    //uint pkt_cnt = 0;
    char *iface_real;
    unsigned char *buf;
    mme_ctx_t *node;
    /* protect from null pointers */
    if (ctx == NULL || confirm_ctx_res == NULL || dest == NULL)
        return MME_ERROR_GEN;
    /* check if ctx has been inititalized */
    if (ctx->status == MME_STATUS_INIT)
        return MME_ERROR_NOT_INIT;

    *confirm_ctx_res = NULL;
    /*
     * #####################
     * #    SEND
     * #####################
     */

    /*
     * --- Create the raw socket ---
     */
    if ((raw = socket (PF_PACKET, SOCK_RAW, htons (ETH_P_HPAV))) == -1)
    {
        perror ("Error creating raw socket: ");
        return MME_ERROR_GEN;
    }

    memset (&sll, 0x0, sizeof (sll));
    memset (&ifr, 0x0, sizeof (ifr));

    /* keep the same interface */
    iface_real = iface;

    /* get the bridge mac-address */
    _get_iface_mac_addr (raw, iface_real, src_mac);

    /* Get index of needed interface  */
    strncpy (ifr.ifr_name, iface_real, IFNAMSIZ);
    if ((ioctl (raw, SIOCGIFINDEX, &ifr)) == -1)
    {
        printf ("%s: Error getting %s index !\n", __FUNCTION__, iface_real);
        close (raw);
        return MME_ERROR_GEN;
    }

    /* Bind our raw socket to this interface */
    sll.sll_family = AF_PACKET;
    sll.sll_ifindex = ifr.ifr_ifindex;
    sll.sll_protocol = htons (ETH_P_HPAV);
    if ((bind (raw, (struct sockaddr *)&sll, sizeof (sll)))== -1)
    {
        perror ("Error binding raw socket to interface\n");
        close (raw);
        return MME_ERROR_GEN;
    }

    mh = (MME_t *)pkt_holder;

    nbpkt = mme_header_prepare (mh, ctx, src_mac, dest, mmv);

    if (mmv == 1)
    {
        mme_fmi_get (mh->fmi, &fmi_nf_mi, &fmi_fn_mi, &fmi_fmsn);

        /* Security check, if nbpkt is more than 15 don't send anything and inform
         * the user. */
        if (fmi_nf_mi > 0xf)
        {
            printf("The MME to send is too long and cannot be fragmented\n");
            return MME_ERROR_SPACE;
        }
    }

    /* initialize fragment offset to the begining of the payload*/
    fo = ctx->head;

    /* prepare and send out packets */
    for (i=0; i<nbpkt; i++)
    {
        /*
         * --- Append payload ---
         */
        /* skip the MME header */
        pkt = pkt_holder + sizeof (MME_t);

        /* mme version 0 without fmi */
        if (mmv == 0)   pkt -= 2;

        /* determine the size of whole packet (header + data) */
        pkt_len = mme_prepare (mh, ctx, &fo, i, pkt, oui_type);
        if (mmv == 0)   pkt_len -= 2;

        /*
         * --- Send raw packet ---
         */
        /* initialize pkt pointer to the start of the send buffer */
        pkt = pkt_holder;

        if ((sent = write (raw, pkt, pkt_len)) != pkt_len)
        {
            perror ("string error\n");
            /* Error */
            syslog (LOG_WARNING, "%s: Could only send %d bytes of packet of length %d", __FUNCTION__, sent, pkt_len);
            close (raw);
            return MME_ERROR_GEN;
        }
    } /* for */

    /* we should exit here if we do not have to wait on response */
    if (type == MME_SEND_CNF || type == MME_SEND_IND)
    {
        /* if we had to send a .CNF or .IND only we have finished,
         * beacuse these MME messages does not require response */
        close (raw);
        return MME_SUCCESS;
    }

    /*
     * #####################
     * #    RECEIVE
     * #####################
     */

    /* Set the MME Header size */
    SET_MME_HEADER_SIZE(ctx->mmtype, mme_h_size);
    if (mmv == 0) mme_h_size -= 2;

#if 0
    /* Calculate into 'timeout' when we should time out */
    gettimeofday (&timeout, 0);
    if (ctx->mmtype == VS_GET_VERSION)
    {
        /* Get version timeout 100ms */
        //timeout.tv_usec += 100 * 1000;
        timeout.tv_sec += 1;
    }
    else if (ctx->mmtype == QCA_FAC_DEFAULT)
    {
        /* Fac default timeout 1s */
        timeout.tv_sec += 1;
    }
    else
    {
        timeout.tv_sec += MME_TOUT_MS / 1000;
        timeout.tv_usec += (MME_TOUT_MS & 1000) * 1000;
    }
#endif
    /* initialize packet counter */
    while (1)  /* receive the payload, packet by packet */
    {
        /* rewind pkt pointer to begining of the packet holder */
        pkt = pkt_holder;
         /* Init fds with 'sock' as the only fd */
        FD_ZERO (&fds);
        FD_SET (raw, &fds);
#if 0
        /* Force timeout if we ran out of time */
        gettimeofday (&now, 0);

        if (timercmp (&now, &timeout, >) != 0)
        {
            printf ("Server response timeout.\n");
            close (raw);
            return -MME_ERROR_TIMEOUT;
        }
#endif
        waittime.tv_sec = 1;
        waittime.tv_usec = 0;
        //waittime.tv_usec = 500 * 1000;
        /* Calculate into 'waittime' how long to wait */
#if 0
        timersub (&timeout, &now, &waittime); /* wait = timeout - now */
#endif
        /* fds fd_set is containing only our socket, so no need to check with FD_ISSET() */
        res = select (raw + 1, &fds, NULL, NULL, &waittime);

        if (res < 0)
        {
            perror ("select() failed");
            close (raw);
            return MME_ERROR_GEN;
        }
        else if (res == 0)  /* timeout */
        {
            close (raw);
            if (!(*confirm_ctx_res))
                {
                printf ("%02x:%02x:%02x:%02x:%02x:%02x MMTYPE %04x MMV %d EoC OUI:%d response timeout %lds\n",
                    dest[0], dest[1], dest[2], dest[3], dest[4], dest[5], ctx->mmtype, mmv, oui_type, timeout.tv_sec);
                return MME_ERROR_TIMEOUT;
            }
            else
                return MME_SUCCESS;
        }

        if ((len = recvfrom (raw, pkt, ETH_PACKET_MAX_NOVLAN_SIZE, 0, (struct sockaddr*)&pkt_info, (socklen_t *)&pkt_nfo_sz)) < 0)
        {
            perror ("Recv from returned error: ");
            close (raw);
            return MME_ERROR_TXRX;
        }
        //unsigned char *tmp = pkt_info.sll_addr;
        //printf ("recv %d from %02x:%02x:%02x:%02x:%02x:%02x\n", len,
        //               tmp[0], tmp[1], tmp[2], tmp[3], tmp[4], tmp[5]);
        mh =  (MME_t *)pkt;
        mme_fmi_get (mh->fmi, &fmi_nf_mi, &fmi_fn_mi, &fmi_fmsn);
     
        if (mh->mmtype != ctx->mmtype + 1)
            continue;

        /* skip the MME header */
        pkt += mme_h_size;
        len -= mme_h_size;
        /* copy the packet into the receive buffer */
        node = malloc(sizeof(mme_ctx_t));
        buf = malloc(len + 1);
        if (!node || !buf)
            return MME_ERROR_SPACE;
        mme_init(node, ctx->mmtype + 1, buf, len + 1);
        mme_put (node, pkt, len, &result_len);
        memcpy(node->mac, pkt_info.sll_addr, 6);

        /* add the node to head->next */
        if (*confirm_ctx_res)
            node->next = (*confirm_ctx_res);  
        *confirm_ctx_res = node;
            
        memset(pkt_holder, 0, sizeof(pkt_holder));
    }

    close (raw);
    return MME_SUCCESS;
}

/* free mme_ctx_t. only call after function wtich multi response.*/
void free_mme_ctx (mme_ctx_t *ptr)
{
    mme_ctx_t *node = ptr;
    mme_ctx_t *tmp = ptr;
    while (node)
    {
        free (node->buffer);
        tmp = node;
        node = node->next;
        free (tmp);
    }
}