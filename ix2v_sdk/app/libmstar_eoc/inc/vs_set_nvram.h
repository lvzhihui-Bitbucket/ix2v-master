#ifndef libmstar_inc_vs_set_nvram_h
#define libmstar_inc_vs_set_nvram_h
/* Maya project {{{
 *
 * Copyright (C) 2018 Mstar
 *
 * <<<Licence>>>
 *
 * }}} */
/**
 * \file    cp/msg/inc/vs_set_nvram.h
 * \brief   VS_SET_NVRAM Structures.
 * \ingroup libmstar
 */

/** VS_SET_NVRAM.REQ ReqType. */
typedef struct libmstar_vs_set_nvram_req_t
{
    u8 block_index;
    u8 block_index_max;
    u16 nvram_size;
    u32 nvram_read_size;
    u32 checksum;
    u8 data[1024];
}__attribute__ ((__packed__)) libmstar_vs_set_nvram_req_t;

/** VS_SET_NVRAM.CNF result. */
enum libmstar_vs_set_nvram_cnf_result_t
{
   LIBMSTAR_VS_SET_NVRAM_CNF_RESULT_SUCCESS,
   LIBMSTAR_VS_SET_NVRAM_CNF_RESULT_REQ_FAILURE,
   LIBMSTAR_VS_SET_NVRAM_CNF_RESULT_CHECKSUM_FAILURE,
   LIBMSTAR_VS_SET_NVRAM_CNF_RESULT_WRITE_TEMP_FILE_FAILURE,
   LIBMSTAR_VS_SET_NVRAM_CNF_RESULT_WRITE_FLASH_FAILURE,
   LIBMSTAR_VS_SET_NVRAM_CNF_RESULT_READ_TEMP_FILE_FAILURE,
   LIBMSTAR_VS_SET_NVRAM_CNF_RESULT_NB
};

typedef enum libmstar_vs_set_nvram_cnf_result_t
libmstar_vs_set_nvram_cnf_result_t;

typedef struct libmstar_vs_set_nvram_cnf_t
{
    u8 result;
}__attribute__ ((__packed__)) libmstar_vs_set_nvram_cnf_t;

#endif /* libmstar_inc_vs_set_nvram_h */
