#ifndef libmstar_inc_vs_get_tonemask_h
#define libmstar_inc_vs_get_tonemask_h
/* Inka project {{{
 *
 * Copyright (C) 2018 Mstar Semiconductor
 *
 * <<<Licence>>>
 *
 * }}} */

/**
 * \file    cp/msg/inc/vs_get_tonemask.h
 * \brief   VS_GET_TONEMASK Structures.
 * \ingroup cp_msg
 */

/** VS_GET_TONEMASK.CNF result. */
enum libmstar_vs_get_tonemask_cnf_result_t
{
    LIBMSTAR_VS_GET_TONEMASK_CNF_RESULT_SUCCESS,
    LIBMSTAR_VS_GET_TONEMASK_CNF_RESULT_FAILURE,
    LIBMSTAR_VS_GET_TONEMASK_CNF_RESULT_NB
};

typedef enum libmstar_vs_get_tonemask_cnf_result_t
libmstar_vs_get_tonemask_cnf_result_t;

/** VS_GET_TONEMASK.CNF ReqType. */
typedef struct libmstar_vs_get_tonemask_req_t
{
	unsigned char peer_mac[MAC_BIN_LEN];
    u8 tmi;
	u8 int_id;
	u8 direction;
	u8 carrier_group;
}__attribute__ ((__packed__)) libmstar_vs_get_tonemask_req_t;

/** VS_GET_TONEMASK.CNF ReqType. */
typedef struct libmstar_vs_get_tonemask_cnf_t
{
    u8 result;
    u8 tonemask[512];
}__attribute__ ((__packed__)) libmstar_vs_get_tonemask_cnf_t;

 #endif /* vs_get_tonemask_h */
