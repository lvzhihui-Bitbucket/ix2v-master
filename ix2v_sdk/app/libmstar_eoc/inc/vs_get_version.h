#ifndef libmstar_inc_vs_get_version_h
#define libmstar_inc_vs_get_version_h
/* Maya project {{{
 *
 * Copyright (C) 2015 MStar Semiconductor
 *
 * <<<Licence>>>
 *
 * }}} */

#define LIBMSTAR_VS_SW_VERSION_LEN 128
#define LIBMSTAR_VS_GET_VERSION_RES_LEN 5
#define LIBMSTAR_VS_OAS_CLIENT_VERSION_LEN 64

/**
 * \file    cp/msg/inc/vs_get_version.h
 * \brief   VS_GET_VERSION Structures.
 * \ingroup LIBMSTAR
 */
/** VS_GET_VERSION.CNF result. */
enum libmstar_vs_get_version_cnf_result_t
{
    LIBMSTAR_VS_GET_VERSION_CNF_RESULT_SUCCESS,
    LIBMSTAR_VS_GET_VERSION_CNF_RESULT_FAILURE,
    LIBMSTAR_VS_GET_VERSION_CNF_RESULT_NB
};

typedef enum libmstar_vs_get_version_cnf_result_t
libmstar_vs_get_version_cnf_result_t;

typedef struct libmstar_vs_get_version_cnf_t
{
    u8 result;
    u16 device_id;
    u8 current_image_index;
    char applicative_version[16];
    char av_stack_version[64];
    char applicative_alternate[16];
    char bootloader_version[64];

    /** used by multi response */
    struct libmstar_vs_get_version_cnf_t *next;
    unsigned char mac[6];
}__attribute__ ((__packed__)) libmstar_vs_get_version_cnf_t;

#endif /* LIBMSTAR_inc_vs_get_version_h */
