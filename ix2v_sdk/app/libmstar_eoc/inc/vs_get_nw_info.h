#ifndef libmstar_inc_vs_get_nw_info_h
#define libmstar_inc_vs_get_nw_info_h
/* Cesar project {{{
 *
 * Copyright (C) 2008 Spidcom
 *
 * <<<Licence>>>
 *
 * }}} */
/**
 * \file    cp/msg/inc/vs_get_nw_info.h
 * \brief   VS_GET_NW_INFO Structures.
 * \ingroup libmstar
 */

/** VS_GET_NW_INFO.CNF result. */
enum libmstar_vs_get_nw_info_cnf_result_t
{
    LIBMSTAR_VS_GET_NW_INFO_CNF_RESULT_SUCCESS,
    LIBMSTAR_VS_GET_NW_INFO_CNF_RESULT_FAILURE,
    LIBMSTAR_VS_GET_NW_INFO_CNF_RESULT_NB
};

typedef enum libmstar_vs_get_nw_info_cnf_result_t
libmstar_vs_get_nw_info_cnf_result_t;

/** sta info. */
typedef struct sta_info_t
{
    /** sta tei. */
    u8 sta_tei;
    /** sta_mac */
    u8 sta_mac[6];
    /** phy tx coded. */
    u16 phy_tx_coded;
    /** phy tx raw. */
    u16 phy_tx_raw;
    /** phy rx coded. */
    u16 phy_rx_coded;
    /** phy rx raw. */
    u16 phy_rx_raw;
    /** agc gain. */
    s8 agc_gain;

}__attribute__ ((__packed__)) sta_info_t;

/** Data structure for VS_GET_NW_INFO.CNF message. */
typedef struct libmstar_vs_get_nw_info_cnf_t
{
    /** nid */
    u8 nid[7];
    /** snid */
    u8 snid;
    /** cco tei */
    u8 cco_tei;
    /** cco mac */
    u8 cco_mac[6];
    /** num stas. */
    u8 num_stas;
    /** sta info. */
    sta_info_t sta_info[64];

}__attribute__ ((__packed__)) libmstar_vs_get_nw_info_cnf_t;

#endif /* vs_get_nw_info_h */
