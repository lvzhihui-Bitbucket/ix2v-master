#ifndef inc_vs_file_access_h
#define inc_vs_file_access_h
/* Cesar project {{{
 *
 * Copyright (C) 2013 Mstar
 *
 * <<<Licence>>>
 *
 * }}} */
/**
 * \file    vs_file_access.h
 * \brief   VS_FILE_ACCESS Structures.
 * \ingroup cp_msg
 */

#define VS_FILE_ACCESS_REQ_DATA_MAX_LEN 1024
#define VS_FILE_ACCESS_CNF_DATA_MAX_LEN 1024
#define VS_FILE_ACCESS_PARAMETER_MAX_LEN 32

#define BOOTLOADER_TEMP_PATH "/bootloader.tmp"

#define VS_FILE_ACCESS_ERR_MSG_CHECKSUM "checksum error"
#define VS_FILE_ACCESS_ERR_MSG_WRITE_BOOTLOADER "can't write bootloader"
#define VS_FILE_ACCESS_ERR_MSG_WRITE_SIMAGE "can't write simage"
#define VS_FILE_ACCESS_ERR_MSG_WRITE_SIMAGE_CONF "can't write simage configuration"
#define VS_FILE_ACCESS_ERR_MSG_WRITE_SIMAGE_CHKSUM "can't upgrade simage due to checksum error"
#define VS_FILE_ACCESS_ERR_MSG_OPEN_FILE "can't open file"
#define VS_FILE_ACCESS_ERR_MSG_REMOVE_FILE "can't remove file"
#define VS_FILE_ACCESS_ERR_MSG_WRITE_FILE "can't write file"
#define VS_FILE_ACCESS_ERR_MSG_READ_FILE "can't read file"
#define VS_FILE_ACCESS_ERR_MSG_READ_FORMAT_NOT_SUPPORT "this format not support"
#define VS_FILE_ACCESS_ERR_MSG_OPEN_DIR "can't open directory"
#define VS_FILE_ACCESS_ERR_MSG_DIR_EXIST "directory exists"
#define VS_FILE_ACCESS_ERR_MSG_MAKE_DIR "can't make directory"
#define VS_FILE_ACCESS_ERR_MSG_REMOVE_DIR "can't remove directory"
#define VS_FILE_ACCESS_ERR_MSG_FORMAT_FLASH "can't format flash"
#define VS_FILE_ACCESS_ERR_MSG_UNKNOWN_OP "unknown operation"


/** VS_FILE_ACCESS.CNF MSTATUS. */
enum vs_file_access_cnf_mstatus_t
{
    VS_FILE_ACCESS_CNF_MSTATUS_SUCCESS = 0x00,
    VS_FILE_ACCESS_CNF_MSTATUS_FAIL = 0x01,
};
typedef enum vs_file_access_cnf_mstatus_t
vs_file_access_cnf_mstatus_t;

/** VS_FILE_ACCESS.REQ OP. */
enum vs_file_access_req_op_t
{
    VS_FILE_ACCESS_REQ_OP_WRITE = 0x00,
    VS_FILE_ACCESS_REQ_OP_READ  = 0x01,
    VS_FILE_ACCESS_REQ_OP_DELETE = 0x02,
    VS_FILE_ACCESS_REQ_OP_LIST_DIR = 0x03,
    VS_FILE_ACCESS_REQ_OP_MAKE_DIR = 0x04,
    VS_FILE_ACCESS_REQ_OP_DELETE_DIR = 0x05,
    VS_FILE_ACCESS_REQ_OP_FORMAT_FLASH = 0x06,
    VS_FILE_ACCESS_REQ_OP_SAVE  = 0x07,
    VS_FILE_ACCESS_REQ_OP_SCAN_STA  = 0x08,
};
typedef enum vs_file_access_req_op_t
vs_file_access_req_op_t;

/** VS_FILE_ACCESS.REQ OP. */
enum vs_file_access_req_file_type_t
{
    VS_FILE_ACCESS_REQ_FILE_TYPE_BOOTLOADER = 0x00,
    VS_FILE_ACCESS_REQ_FILE_TYPE_SIMAGE = 0x01,
    VS_FILE_ACCESS_REQ_FILE_TYPE_GENERAL_FILE = 0x02,
};
typedef enum vs_file_access_req_file_type_t
vs_file_access_req_file_type_t;

typedef struct vs_file_access_req_t
{
    unsigned char op;
    unsigned char file_type;
    char parameter[VS_FILE_ACCESS_PARAMETER_MAX_LEN];
    unsigned short total_fragments;
    unsigned short fragment_number;
    unsigned int offset;
    unsigned int checksum;
    unsigned short length;
    unsigned char data[VS_FILE_ACCESS_REQ_DATA_MAX_LEN];
}__attribute__ ((__packed__)) vs_file_access_req_t;

typedef struct vs_file_access_cnf_t
{
    unsigned char mstatus;
    unsigned char op;
    unsigned char file_type;
    char parameter[VS_FILE_ACCESS_PARAMETER_MAX_LEN];
    unsigned short total_fragments;
    unsigned short fragment_number;
    unsigned int offset;
    unsigned short length;
}__attribute__ ((__packed__)) vs_file_access_cnf_t;

typedef struct
{
    char name[32];
    unsigned char op;
    unsigned char file_type;
    unsigned char parse_next;
    char input_output_file;
    char parameter[32];
    char file_name[128];
    int file_len;
    char file_buf[10240];
} vs_file_access_cmd_t;

#define PARSE_NEXT_YES 1
#define PARSE_NEXT_NO 0
#endif /* inc_vs_file_access_h */
