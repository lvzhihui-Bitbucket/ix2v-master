#ifndef libmstar_inc_vs_get_pwm_stats_h
#define libmstar_inc_vs_get_pwm_stats_h
/* Maya project {{{
 *
 * Copyright (C) 2018 MStar Semiconductor
 *
 * <<<Licence>>>
 *
 * }}} */
/**
 * \file    cp/msg/inc/vs_get_pwm_stats.h
 * \brief   VS_GET_PWM_STATS Structures.
 * \ingroup libmstar
 */

#define PWM_PERIOD_MIN 100
#define PWM_PERIOD_MAX 1000
/** Convert SAR ADC output data into millvoltage.
 *  SAR unit to mV: 3.3042777/1023 = 3.229988 --> 3.229988 * 4(OPA) = 12.919952
 */
#define SARADC_UNIT 12920
#define SARADC_UNIT_MV SARADC_UNIT/1000
/** We need to compensate 100k ohm cause voltage degrade and
 *  this value measure by CAE.
 */
#define RESISTOR_COMPENSATE_MV 300

/** VS_GET_PWM_STATS.CNF result. */
enum libmstar_vs_get_pwm_stats_cnf_result_t
{
    LIBMSTAR_VS_GET_PWM_STATS_CNF_RESULT_SUCCESS,
    LIBMSTAR_VS_GET_PWM_STATS_CNF_RESULT_DISABLE,
    LIBMSTAR_VS_GET_PWM_STATS_CNF_RESULT_NO_SIGNAL,
    LIBMSTAR_VS_GET_PWM_STATS_CNF_RESULT_NB
};

typedef enum libmstar_vs_get_pwm_stats_cnf_result_t
libmstar_vs_get_pwm_stats_cnf_result_t;

/** VS_GET_PWM_STATS.IND result. */
enum libmstar_vs_get_pwm_stats_ind_result_t
{
    LIBMSTAR_VS_GET_PWM_STATS_IND_RESULT_INIT,
    LIBMSTAR_VS_GET_PWM_STATS_IND_RESULT_FREQ_OUT_OF_THR,
    LIBMSTAR_VS_GET_PWM_STATS_IND_RESULT_DC_OUT_OF_THR,
    LIBMSTAR_VS_GET_PWM_STATS_IND_RESULT_FREQ_DC_OUT_OF_THR,
    LIBMSTAR_VS_GET_PWM_STATS_IND_RESULT_VOL_OUT_OF_THR,
    LIBMSTAR_VS_GET_PWM_STATS_IND_RESULT_FREQ_VOL_OUT_OF_THR,
    LIBMSTAR_VS_GET_PWM_STATS_IND_RESULT_DC_VOL_OUT_OF_THR,
    LIBMSTAR_VS_GET_PWM_STATS_IND_RESULT_FREQ_DC_VOL_OUT_OF_THR,
    LIBMSTAR_VS_GET_PWM_STATS_IND_RESULT_NB
};

typedef enum libmstar_vs_get_pwm_stats_ind_result_t
libmstar_vs_get_pwm_stats_ind_result_t;

/** VS_GET_PWM_STATS over threshold (OT) mask. */
enum libmstar_vs_get_pwm_stats_ot_mask_t
{
    LIBMSTAR_VS_GET_PWM_STATS_OT_FREQ = 1,
    LIBMSTAR_VS_GET_PWM_STATS_OT_DUTY_CYCLE = 2,
    LIBMSTAR_VS_GET_PWM_STATS_OT_VOL = 4,
    LIBMSTAR_VS_GET_PWM_STATS_OT_NB
};

typedef enum libmstar_vs_get_pwm_stats_ot_mask_t
libmstar_vs_get_pwm_stats_ot_mask_t;

typedef struct libmstar_vs_get_pwm_stats_cnf_t
{
    u8 result;
    u16 freq;
    u16 duty_cycle;
    u16 millvolt;
}__attribute__ ((__packed__)) libmstar_vs_get_pwm_stats_cnf_t;

#endif /* libmstar_inc_vs_get_pwm_stats_h */
