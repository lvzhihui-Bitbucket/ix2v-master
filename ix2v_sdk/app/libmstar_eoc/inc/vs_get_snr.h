#ifndef libmstar_inc_vs_get_snr_h
#define libmstar_inc_vs_get_snr_h
/* Cesar project {{{
 *
 * Copyright (C) 2008 Spidcom
 *
 * <<<Licence>>>
 *
 * }}} */
/**
 * \file    cp/msg/inc/vs_get_snr.h
 * \brief   VS_GET_SNR Structures.
 * \ingroup libmstar
 */

/** VS_GET_SNR.REQ INT. */
enum libmstar_vs_get_snr_req_int_t
{
    LIBMSTAR_VS_GET_SNR_REQ_INT_MIN,
    LIBMSTAR_VS_GET_SNR_REQ_INT_MAX = 0x1F,
    LIBMSTAR_VS_GET_SNR_REQ_INT_RSVD_MIN,
    LIBMSTAR_VS_GET_SNR_REQ_INT_RSVD_MAX = 0xFE,
    LIBMSTAR_VS_GET_SNR_REQ_INT_LIST_ONLY,
    LIBMSTAR_VS_GET_SNR_REQ_INT_NB
};

typedef enum libmstar_vs_get_snr_req_int_t libmstar_vs_get_snr_req_int_t;

#define TM_INT_I_IS_VALID(TmInti)                       \
    (((TmInti) < LIBMSTAR_VS_GET_SNR_REQ_INT_RSVD_MIN)\
     || ((TmInti) == LIBMSTAR_VS_GET_SNR_REQ_INT_LIST_ONLY))

/** VS_GET_SNR.REQ Carrier group. */
#define LIBMSTAR_VS_GET_SNR_REQ_CARRIER_GR_NB 8
/** Total number of OFDM carrier, also counting unusable ones. */
#define PHY_ALL_CARRIER_NB 4096

#define INT_LENGTH 32

#define LIBMSTAR_SNR_IN_MSG_NB (PHY_ALL_CARRIER_NB / \
        LIBMSTAR_VS_GET_SNR_REQ_CARRIER_GR_NB)

/** VS_GET_SNR.CNF result. */
enum libmstar_vs_get_snr_cnf_result_t
{
    LIBMSTAR_VS_GET_SNR_CNF_RESULT_SUCCESS,
    LIBMSTAR_VS_GET_SNR_CNF_RESULT_FAILURE,
    LIBMSTAR_VS_GET_SNR_CNF_RESULT_BAD_TMP_INT_LIST_ID,
    LIBMSTAR_VS_GET_SNR_CNF_RESULT_NB
};

typedef enum libmstar_vs_get_snr_cnf_result_t libmstar_vs_get_snr_cnf_result_t;

/** Data associated with a VS_GET_SNR.CNF message. */
typedef struct libmstar_vs_get_snr_cnf_t
{
    /** Result. */
    u8 result;
    /** Current tonemap interval list identifier. */
    u8 int_id;
    /** Number of intervals, 0-31. */
    u8 intervals_nb;
    /** Intervals et. */
    u16 int_et[INT_LENGTH];
    /** Average Bit Error Rate. */
    u16 tm_ber;
    /** Carrier group. */
    u8 carrier_gr;
    /** snr array. */
    u32 snr[LIBMSTAR_SNR_IN_MSG_NB];
}__attribute__ ((__packed__)) libmstar_vs_get_snr_cnf_t;

typedef struct libmstar_vs_get_snr_req_t
{
    u8 Station_address[MAC_BIN_LEN];
    u8 int_index;
    u8 int_id;
    unsigned char carrier_group;
}__attribute__ ((__packed__)) libmstar_vs_get_snr_req_t;

#endif /* vs_get_snr_h */
