#ifndef libmstar_inc_vs_get_eth_phy_h
#define libmstar_inc_vs_get_eth_phy_h
/* Cesar project {{{
 *
 * Copyright (C) 2018 Mstar
 *
 * <<<Licence>>>
 *
 * }}} */
/**
 * \file    cp/msg/inc/vs_get_eth_phy.h
 * \brief   VS_GET_ETH_PHY Structures.
 * \ingroup cp_msg
 */

/** VS_GET_ETH_PHY.CNF result. */
enum libmstar_vs_get_eth_phy_cnf_result_t
{
    LIBMSTAR_VS_GET_ETH_PHY_CNF_RESULT_SUCCESS,
    LIBMSTAR_VS_GET_ETH_PHY_CNF_RESULT_FAILURE,
    LIBMSTAR_VS_GET_ETH_PHY_CNF_RESULT_NB
};

typedef enum libmstar_vs_get_eth_phy_cnf_result_t
libmstar_vs_get_eth_phy_cnf_result_t;

typedef struct libmstar_vs_get_eth_phy_cnf_t
{
    u8 result;
    u8 link;
    u8 speed;
    u8 duplex;
}__attribute__ ((__packed__)) libmstar_vs_get_eth_phy_cnf_t;

#endif /* vs_get_eth_phy_h */
