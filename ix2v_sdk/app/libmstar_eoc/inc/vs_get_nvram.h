#ifndef libmstar_inc_vs_get_nvram_h
#define libmstar_inc_vs_get_nvram_h
/* Maya project {{{
 *
 * Copyright (C) 2018 MStar Semiconductor
 *
 * <<<Licence>>>
 *
 * }}} */
/**
 * \file    cp/msg/inc/vs_get_nvram_h.
 * \brief   VS_GET_NVRAM Structures.
 * \ingroup libmstar
 */

typedef struct libmstar_vs_get_nvram_req_t
{
    u8 block_index;
}__attribute__ ((__packed__)) libmstar_vs_get_nvram_req_t;

/** VS_GET_NVRAM.CNF result. */
enum libmstar_vs_get_nvram_cnf_result_t
{
    LIBMSTAR_VS_GET_NVRAM_CNF_RESULT_SUCCESS,
    LIBMSTAR_VS_GET_NVRAM_CNF_RESULT_FAILURE,
    LIBMSTAR_VS_GET_NVRAM_CNF_RESULT_BAD_INDEX,
    LIBMSTAR_VS_GET_NVRAM_CNF_RESULT_NVRAM_INVALID,
    LIBMSTAR_VS_GET_NVRAM_CNF_RESULT_NB
};

typedef enum libmstar_vs_get_nvram_cnf_result_t
libmstar_vs_get_nvram_cnf_result_t;

typedef struct libmstar_vs_get_nvram_cnf_t
{
    u8 result;
    u8 request_block_index;
    u16 nvram_size;
    u8 data[1024];
}__attribute__ ((__packed__)) libmstar_vs_get_nvram_cnf_t;

#endif /* libmstar_inc_vs_get_nvram_h */
