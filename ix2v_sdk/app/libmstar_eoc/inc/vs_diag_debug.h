#ifndef libmstar_inc_vs_diag_debug_h
#define libmstar_inc_vs_diag_debug_h

/** VS_GET_VERSION.CNF result. */
enum libmstar_vs_diag_debug_cnf_mstatus_t
{
    LIBMSTAR_VS_DIAG_DEBUG_CNF_RESULT_SUCCESS,
    LIBMSTAR_VS_DIAG_DEBUG_CNF_RESULT_FAILURE,
    LIBMSTAR_VS_DIAG_DEBUG_CNF_RESULT_NB
};
typedef enum libmstar_vs_diag_debug_cnf_mstatus_t
    libmstar_vs_diag_debug_cnf_mstatus_t;

enum libmstar_vs_diag_debug_req_subtype_t
{
    LIBMSTAR_VS_DIAG_DEBUG_REQ_SUBTYPE_DBG_DUMP = 0,
    LIBMSTAR_VS_DIAG_DEBUG_REQ_SUBTYPE_SNIFFER_SET,
    LIBMSTAR_VS_DIAG_DEBUG_REQ_SUBTYPE_GET_SYS_INFO,
    LIBMSTAR_VS_DIAG_DEBUG_REQ_SUBTYPE_SET_CABLE_VLAN,
    LIBMSTAR_VS_DIAG_DEBUG_REQ_SUBTYPE_NB
};

enum libmstar_vs_diag_debug_req_action_t
{
    LIBMSTAR_VS_DIAG_DEBUG_REQ_ACTION_SUCCESS,
    LIBMSTAR_VS_DIAG_DEBUG_REQ_ACTION_FAILURE,
    LIBMSTAR_VS_DIAG_DEBUG_REQ_ACTION_NB
};

typedef enum libmstar_vs_diag_debug_req_subtype_t
    libmstar_vs_diag_debug_req_subtype_t;

typedef struct libmstar_vs_diag_debug_req_t
{
    u16 command;
    u8 action;
    u8 sniffer_mac[6];
}__attribute__ ((__packed__)) libmstar_vs_diag_debug_req_t;

typedef struct libmstar_vs_diag_debug_cnf_t
{
    u8 result;
    u32 systime;
    u8 authenticated_times;
    u32 auth_time;
    u32 unauth_time;
}__attribute__ ((__packed__)) libmstar_vs_diag_debug_cnf_t;
#endif
