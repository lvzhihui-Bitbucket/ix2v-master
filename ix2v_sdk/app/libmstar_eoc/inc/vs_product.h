#ifndef libmstar_inc_vs_product_h
#define libmstar_inc_vs_product_h
/* Maya project {{{
 *
 * Copyright (C) 2018 Mstar
 *
 * <<<Licence>>>
 *
 * }}} */
/**
 * \file    cp/msg/inc/vs_product.h
 * \brief   VS_PRODUCT Structures.
 * \ingroup libmstar
 */

#define     MODULE_SET_MAC      0
#define     MODULE_SET_DPW      1
#define     MODULE_SET_HFID     2
#define     MODULE_SET_NMK      3
#define     MODULE_START_SC     8
#define     MODULE_RANDOM_NMK   9
#define     MODULE_FACTORY      10
#define     MODULE_GET_INFO     11
#define     MODULE_REBOOT       15

#define     MODULE_MAC_ADDRESS_LEN     6
#define     MODULE_DPW_LEN             64
#define     MODULE_HFID                64
#define     MODULE_NMK                 16

/** VS_PRODUCT.REQ ReqType. */
typedef struct libmstar_vs_product_req_t
{
    u16 moduleid;
    u8 data[150];
}__attribute__ ((__packed__)) libmstar_vs_product_req_t;


/** VS_GET_VERSION.CNF result. */
enum libmstar_vs_product_cnf_status_t
{
    LIBMSTAR_VS_PRODUCT_CNF_STATUS_SUCCESS,
    LIBMSTAR_VS_PRODUCT_CNF_STATUS_FAILURE,
    LIBMSTAR_VS_PRODUCT_CNF_STATUS_NB
};

/** VS_PRODUCT.CNF result. */
typedef struct libmstar_vs_product_cnf_t
{
    u8 mstatus;
    u16 moduleid;
    u8 data[150];
}__attribute__ ((__packed__)) libmstar_vs_product_cnf_t;

#endif /* libmstar_inc_vs_product_h */
