#ifndef libmstar_inc_vs_get_cco_dev_h
#define libmstar_inc_vs_get_cco_dev_h
/* Maya project {{{
 *
 * Copyright (C) 2015 MStar Semiconductor
 *
 * <<<Licence>>>
 *
 * }}} */

#define LIBMSTAR_VS_GET_CCO_DEV_MAC_LEN 6


enum libmstar_vs_get_cco_dev_cnf_result_t
{
    LIBMSTAR_VS_GET_CCO_DEV_CNF_RESULT_SUCCESS,
    LIBMSTAR_VS_GET_CCO_DEV_CNF_RESULT_FAILURE,
    LIBMSTAR_VS_GET_CCO_DEV_CNF_RESULT_NB
};

typedef enum libmstar_vs_get_cco_dev_cnf_result_t
libmstar_vs_get_cco_dev_cnf_result_t;

typedef struct libmstar_vs_get_cco_dev_cnf_t
{
    unsigned char result;
    unsigned char mac[6];

    /** used by multi response */
    struct libmstar_vs_get_cco_dev_cnf_t *next;
}__attribute__ ((__packed__)) libmstar_vs_get_cco_dev_cnf_t;

#endif /* libmstar_inc_vs_get_cco_dev_h */