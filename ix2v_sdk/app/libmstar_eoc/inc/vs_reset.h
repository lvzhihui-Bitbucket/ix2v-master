#ifndef libmstar_inc_vs_reset_h
#define libmstar_inc_vs_reset_h
/* Maya project {{{
 *
 * Copyright (C) 2015 MStar Semiconductor
 *
 * <<<Licence>>>
 *
 * }}} */
/**
 * \file    cp/msg/inc/vs_reset.h
 * \brief   VS_RESET Structures.
 * \ingroup cp_msg
 */

/** VS_RESET.CNF result. */
enum libmstar_vs_reset_cnf_result_t
{
    LIBMSTAR_VS_RESET_CNF_RESULT_SUCCESS,
    LIBMSTAR_VS_RESET_CNF_RESULT_FAILURE,
    LIBMSTAR_VS_RESET_CNF_RESULT_NB
};

typedef enum libmstar_vs_reset_cnf_result_t
libmstar_vs_reset_cnf_result_t;

/** VS_RESET.CNF ReqType. */
typedef struct libmstar_vs_reset_cnf_t
{
    u8 result;
}__attribute__ ((__packed__)) libmstar_vs_reset_cnf_t;

#endif /* vs_reset_h */
