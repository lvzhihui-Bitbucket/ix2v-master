#ifndef libmstar_inc_vs_get_link_stat_h
#define libmstar_inc_vs_get_link_stat_h
/* Cesar project {{{
 *
 * Copyright (C) 2008 Spidcom
 *
 * <<<Licence>>>
 *
 * }}} */
/**
 * \file    cp/msg/inc/vs_get_link_stat.h
 * \brief   VS_GET_LINK_STATS Structures.
 * \ingroup libmstar
 */

/** VS_GET_LINK_STATS.REQ ReqType. */
enum libmstar_vs_get_link_stats_req_reqtype_t
{
    LIBMSTAR_VS_GET_LINK_STATS_REQ_REQTYPE_RESET_STAT,
    LIBMSTAR_VS_GET_LINK_STATS_REQ_REQTYPE_GET_STAT,
    LIBMSTAR_VS_GET_LINK_STATS_REQ_REQTYPE_GET_RESET_STAT,
    LIBMSTAR_VS_GET_LINK_STATS_REQ_REQTYPE_NB
};

typedef enum libmstar_vs_get_link_stats_req_reqtype_t
libmstar_vs_get_link_stats_req_reqtype_t;

/** VS_GET_LINK_STATS.REQ TLFlag. */
enum libmstar_vs_get_link_stats_req_tlflag_t
{
    LIBMSTAR_VS_GET_LINK_STATS_REQ_TLFLAG_TX_LINK,
    LIBMSTAR_VS_GET_LINK_STATS_REQ_TLFLAG_RX_LINK,
    LIBMSTAR_VS_GET_LINK_STATS_REQ_TLFLAG_NB
};

typedef enum libmstar_vs_get_link_stats_req_tlflag_t
libmstar_vs_get_link_stats_req_tlflag_t;

/** VS_GET_LINK_STATS.REQ Mgmt_Flag. */
enum libmstar_vs_get_link_stats_req_mgmtflag_t
{
    LIBMSTAR_VS_GET_LINK_STATS_REQ_MGMTFLAG_NOT_MGMT_LINK,
    LIBMSTAR_VS_GET_LINK_STATS_REQ_MGMTFLAG_MGMT_LINK,
    LIBMSTAR_VS_GET_LINK_STATS_REQ_MGMTFLAG_NB
};

typedef enum libmstar_vs_get_link_stats_req_mgmtflag_t
libmstar_vs_get_link_stats_req_mgmtflag_t;

/** Data associated with a VS_GET_LINK_STATS.REQ message. */
typedef struct libmstar_vs_get_link_stats_req_t
{
    /** Request Type. */
    u8 ReqType;
    /** Request Identifier. */
    u8 ReqID;
    /** Link Identifier. */
    u8 lid;
    /** Transmit Link Flag. */
    u8 TLFlag;
    /** Management Link. */
    u8 Mgmt_Flag;
    /** Destination MAC Address/Source Mac Address depending on TLFlag 0x00/0x01. */
    u8 dasa[MAC_BIN_LEN];
}__attribute__ ((__packed__)) libmstar_vs_get_link_stats_req_t;

/** VS_GET_LINK_STATS.CNF result. */
enum libmstar_vs_get_link_stats_cnf_result_t
{
    LIBMSTAR_VS_GET_LINK_STATS_CNF_RESULT_SUCCESS,
    LIBMSTAR_VS_GET_LINK_STATS_CNF_RESULT_FAILURE,
    LIBMSTAR_VS_GET_LINK_STATS_CNF_RESULT_NB
};

typedef enum libmstar_vs_get_link_stats_cnf_result_t
libmstar_vs_get_link_stats_cnf_result_t;

/** TX link stats. */
typedef struct link_stats_tx_t
{
    /** Date of the start of statistics collection. */
    u64 statistics_start_date;
    /** Number of segments that were successfully delivered. */
    u32 num_segs_suc;
    /** Number of segments that were dropped. */
    u32 num_segs_dropped;
    /** Number of PBs handed over to the PHY for transmission. */
    u32 num_pbs;
    /** Number of MPDUs that were transmitted. */
    u32 num_mpdus;
    /** Number of bursts that were transmitted. */
    u32 num_bursts;
    /** Number of MPDUs that were successfully acknowledged. */
    u32 num_sacks;
    /** Number of bad CRCs from transmitted pbs. */
    u32 num_bad_pbs_crc;
    /** Number of MPDUs that were transmitted and collied. */
    u32 num_mpdus_coll;
    /** Number of MPDUs that were transmitted and fail. */
    u32 num_mpdus_fail;
    /** Number of total segments that were transmitted from mfs. */
    u64 num_total_seg;
    /** Number of Tx buffer shortage drop. (Reserve for future use) */
    u32 num_tx_buf_shortage_drop;
}__attribute__ ((__packed__)) link_stats_tx_t;

/** RX link stats. */
typedef struct link_stats_rx_t
{
    /** Date of the start of statistics collection. */
    u64 statistics_start_date;
    /** Number of segments that were successfully received. */
    u32 num_segs_suc;
    /** Number of PBs handed over from the PHY to the MAC. */
    u32 num_pbs;
    /** Number of MPDUs that were received. */
    u32 num_mpdus;
    /** Number of bursts that were received. */
    u32 num_bursts;
    /** Number of bad CRCs from received pbs. */
    u32 num_bad_pbs_crc;
    /** Number of MPDUs that were successfully acknowledged. */
    u32 num_sacks;
    /** Number of SSN under min. (Reserve for future use) */
    u32 num_ssn_under_min;
    /** Number of SSN over max. (Reserve for future use) */
    u32 num_ssn_over_max;
}__attribute__ ((__packed__)) link_stats_rx_t;

/** Data structure for VS_GET_LINK_STATS.CNF message. */
typedef struct libmstar_vs_get_link_stats_cnf_t
{
    /** Request Identifier. */
    u8 ReqID;
    /** Request result. */
    u8 result;
    /** Request Tx or Rx. */
    /** Tx MFS. */
    link_stats_tx_t mfs_tx;
    /** Rx MFS. */
    link_stats_rx_t mfs_rx;

}__attribute__ ((__packed__)) libmstar_vs_get_link_stats_cnf_t;

#endif /* vs_get_link_stat_h */
