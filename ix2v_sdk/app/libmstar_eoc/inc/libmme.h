/* WIFI bundle {{{
 *
 * Copyright (C) 2013 Mstar
 *
 * <<<Licence>>>
 *
 * }}} */
/**
 * \file    lib/libmme/libmme.h
 * \brief   structures and function prototypes of libmme
 * \defgroup libmme libmme : MME management library
 *
 */

#ifndef LIBMME_H
#define LIBMME_H

#include <linux/if_ether.h>
/* Include the auto-generated file from mmtcgen. */
#include "mmtypes.h"

#define MME_TYPE        0x88e1      /* MME Ethertype (SPiDCOM MME Protocol)   */
#define MME_VERSION     0x01
#define MME_HEADER_SIZE 5 /* MMV - MMTYPE - FMI */

#define ETH_P_HPAV  0x88E1      /* HomePlug AV packet       */
/** Ethernet Mac Address size. */
#define ETH_MAC_ADDRESS_SIZE 6
/** Define the ethernet type offset (=12). */
#define ETH_TYPE_OFFSET ((ETH_MAC_ADDRESS_SIZE)*2)
/** Define the MTYPE size. */
#define ETH_TYPE_SIZE 2
/** Ethernet packet maximum payload size. */
#define ETH_PACKET_MAX_PAYLOAD_SIZE 1500
/** Ethernet packet minimum payload size. */
#define ETH_PACKET_MIN_PAYLOAD_SIZE 50
/** Ethernet packet maximum size without vlan (=1514). */
#define ETH_PACKET_MAX_NOVLAN_SIZE (ETH_PACKET_MIN_SIZE_ALLOWED \
                                + ETH_PACKET_MAX_PAYLOAD_SIZE)
/** Ethernet minimum size allowed (=14). */
#define ETH_PACKET_MIN_SIZE_ALLOWED (ETH_TYPE_OFFSET + ETH_TYPE_SIZE)

#define MME_MAX_SIZE    (ETH_PACKET_MAX_PAYLOAD_SIZE - MME_HEADER_SIZE)        /* maximum size of MME payload */
#define MME_MIN_SIZE    (ETH_PACKET_MIN_PAYLOAD_SIZE - MME_HEADER_SIZE)       /* minimum size of MME payload */
//#define MME_TOUT        1       /* communication timeout in seconds */
#define MME_TOUT_MS     1000
#define MME_TOUT_SHORT  1

#define OUI_SPIDCOM     "\x00\x13\xd7"
#define SPIDCOM_OUI_SIZE 3

#define SET_MME_PAYLOAD_MAX_SIZE(type, val) val = MMTYPE_IS_VS(type) ? MME_MAX_SIZE - SPIDCOM_OUI_SIZE : MME_MAX_SIZE
#define SET_MME_PAYLOAD_MIN_SIZE(type, val) val = MMTYPE_IS_VS(type) ? MME_MIN_SIZE - SPIDCOM_OUI_SIZE : MME_MIN_SIZE
#define SET_MME_HEADER_SIZE(type, val) val = MMTYPE_IS_VS(type) ? sizeof(MME_t) + SPIDCOM_OUI_SIZE : sizeof(MME_t)

#define MME_TYPE_MASK 0x0003

#define OUI_QCALCOM     "\x00\xb0\x52"
#define QCA_OUI_SIZE    3

typedef struct
{
    unsigned char   mme_dest[ETH_ALEN];     /* Destination node                         */
    unsigned char   mme_src[ETH_ALEN];      /* Source node                              */
    /* unsigned int        vlan_tag; */     /* ieee 802.1q VLAN tag (optional) [0x8100] */
    unsigned short      mtype;              /* 0x88e1 (iee assigned Ethertype)          */
    unsigned char       mmv;                /* Management Message Version               */
    unsigned short      mmtype;             /* Management Message Type                  */
    unsigned short      fmi;                /* Fragmentation Management Info            */
} __attribute__ ((__packed__)) MME_t;

typedef struct
{
    unsigned char       mme_dest[ETH_ALEN]; /* Destination node                         */
    unsigned char       mme_src[ETH_ALEN];  /* Source node                              */
    unsigned int        vlan_tag;           /* ieee 802.1q VLAN tag (optional) [0x8100]	*/
    unsigned short      mtype;              /* 0x88e1 (iee assigned Ethertype)          */
    unsigned char       mmv;                /* Management Message Version               */
    unsigned short      mmtype;             /* Management Message Type                  */
    unsigned short      fmi;                /* Fragmentation Management Info            */
} __attribute__ ((__packed__)) MME_vlan_t;

typedef enum {
    SPIDCOM_OUI = 0,
    QCA_OUI
} oui_type_t;

/**
 * list of errors returned by libmme functions
 */
typedef enum {
    MME_SUCCESS = 0,
    /** context not initialized */
    MME_ERROR_NOT_INIT,
    /** not enough available space */
    MME_ERROR_SPACE,
    /** not enough available data */
    MME_ERROR_ENOUGH,
    /** timeout on waiting on response */
    MME_ERROR_TIMEOUT,
    /** socket send/receive error */
    MME_ERROR_TXRX,
    /** general error */
    MME_ERROR_GEN
} mme_error_t;

/**
 * status of MME context for initialization
 */
typedef enum {
    MME_STATUS_INIT = 0,
    MME_STATUS_OK = 0xabcdefab,
} mme_status_t;

/** MME transaction type */
typedef enum {
    /** send a .REQ and wait for a .CNF */
    MME_SEND_REQ_CNF = 0,
    /** send a .CNF only */
    MME_SEND_CNF,
    /** send a .IND and wait for a .RSP */
    MME_SEND_IND_RSP,
    /** send a .IND only */
    MME_SEND_IND
} mme_send_type_t;


/** Type of OUI */
typedef enum {
    MME_OUI_NOT_PRESENT = 0,
    MME_OUI_SPIDCOM,
    MME_OUI_JIANGSU,
    MME_OUI_NOT_SPIDCOM,
    MME_OUI_QCA,
} mme_oui_type_t;
/**
 * Context structure of an MME message
 * \struct mme_ctx_t
 */

typedef struct mme_ctx_t{
    /** context current status */
    mme_status_t status;
    /** management message type */
    unsigned short mmtype;
    /** buffer  where to store MME data */
    unsigned char *buffer;
    /** total length of buffer */
    unsigned int length;
    /** start of MME payload */
    unsigned int head;
    /** end of MME payload */
    unsigned int tail;
    /** OUI retrieved from MME */
    mme_oui_type_t oui;
    /** resotre confirm or response mac address */
    unsigned char mac[6];
    /** use for next mme_ctx_t because of multi response. */
    struct mme_ctx_t *next;
} mme_ctx_t;

/**
 * Callback use give the received MME after a mme_listen()
 *
 * \param  ctx context containing the received packet
 * \param  iface the network interface where packet has been received
 * \param  source source MAC address of received packet
 * \return error type (MME_SUCCESS if success)
 */
typedef mme_error_t (*mme_listen_cb_t) (mme_ctx_t *ctx, char *iface, unsigned char *source);

/* function prototypes */
mme_error_t mme_init (mme_ctx_t *ctx, const mmtype_t mmtype, unsigned char *buffer, const unsigned int length);
mme_error_t mme_get_length (mme_ctx_t *ctx, unsigned int *length);
mme_error_t mme_get_free_space (mme_ctx_t *ctx, unsigned int *length);
mme_error_t mme_push (mme_ctx_t *ctx, const void *data, unsigned int length, unsigned int *result_length);
mme_error_t mme_put (mme_ctx_t *ctx, const void *data, unsigned int length, unsigned int *result_length);
mme_error_t mme_pull (mme_ctx_t *ctx, void *data, unsigned int length, unsigned int *result_length);
mme_error_t mme_pop (mme_ctx_t *ctx, void *data, unsigned int length, unsigned int *result_length);
int         mme_header_prepare (MME_t *mh, mme_ctx_t *ctx, unsigned char *src_mac, unsigned char *dest, int mmv);
mme_error_t mme_send (mme_ctx_t *ctx, mme_send_type_t type, char *iface, unsigned char *dest, mme_ctx_t *confirm_ctx, int mmv, int oui_type);
int mme_prepare (MME_t *mh, mme_ctx_t *ctx, int *fo, int index, unsigned char *pkt, int oui_type);
int mme_send_with_multi_response (mme_ctx_t *ctx, mme_send_type_t type, char *iface, unsigned char *dest, mme_ctx_t **confirm_ctx_res, int mmv, int oui_type);

/* free mme_ctx_t. only call after function wtich multi response.*/
void free_mme_ctx (mme_ctx_t *ptr);

#endif /* LIBMME_H */
