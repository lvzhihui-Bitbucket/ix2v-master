#ifndef libmstar_inc_vs_set_capture_state_h
#define libmstar_inc_vs_set_capture_state_h
/* Maya project {{{
 *
 * Copyright (C) 2018 Mstar Semiconductor
 *
 * <<<Licence>>>
 *
 * }}} */
/**
 * \file    cp/msg/inc/vs_set_capture_state.h
 * \brief   VS_SET_CAPTURE_STATE Structures.
 * \ingroup libmstar
 */

/** VS_SET_CAPTURE_STATE.CNF result. */
enum libmstar_vs_set_capture_state_cnf_result_t
{
    LIBMSTAR_VS_SET_CAPTURE_STATE_CNF_RESULT_SUCCESS,
    LIBMSTAR_VS_SET_CAPTURE_STATE_CNF_RESULT_RESOURCE_OCCUPIED,
    LIBMSTAR_VS_SET_CAPTURE_STATE_CNF_RESULT_NO_PEER_STATION,
    LIBMSTAR_VS_SET_CAPTURE_STATE_CNF_RESULT_WRONG_STATE,
    LIBMSTAR_VS_SET_CAPTURE_STATE_CNF_RESULT_NOT_SUPPORTED_CAPTURED,
    LIBMSTAR_VS_SET_CAPTURE_STATE_CNF_RESULT_NOT_SUPPORTED_CAPTURED_SOURCE,
    LIBMSTAR_VS_SET_CAPTURE_STATE_CNF_RESULT_NB
};

typedef enum libmstar_vs_set_capture_state_cnf_result_t
libmstar_vs_set_capture_state_cnf_result_t;

/** VS_SET_CAPTURE_STATE.REQ state. */
enum libmstar_vs_set_capture_state_req_state_t
{
    LIBMSTAR_VS_SET_CAPTURE_STATE_REQ_STATE_STOP,
    LIBMSTAR_VS_SET_CAPTURE_STATE_REQ_STATE_START,
    LIBMSTAR_VS_SET_CAPTURE_STATE_REQ_STATE_NB
};

typedef enum libmstar_vs_set_capture_state_req_state_t
libmstar_vs_set_capture_state_req_state_t;

#define SET_CAPTURE_STATE_I_IS_VALID(ScState) \
    ((ScState) < LIBMSTAR_VS_SET_CAPTURE_STATE_REQ_STATE_NB)

/** VS_SET_CAPTURE_STATE.REQ captured. */
enum libmstar_vs_set_capture_state_req_captured_t
{
    LIBMSTAR_VS_SET_CAPTURE_STATE_REQ_CAPTURED_SNR,
    LIBMSTAR_VS_SET_CAPTURE_STATE_REQ_CAPTURED_SPECTRUM,
    LIBMSTAR_VS_SET_CAPTURE_STATE_REQ_CAPTURED_NB
};

typedef enum libmstar_vs_set_capture_state_req_captured_t
libmstar_vs_set_capture_state_req_captured_t;

#define SET_CAPTURE_CAPTURED_I_IS_VALID(ScCaptured) \
    ((ScCaptured) < LIBMSTAR_VS_SET_CAPTURE_STATE_REQ_CAPTURED_NB)

/** VS_SET_CAPTURE_STATE.REQ captured_source. */
enum libmstar_vs_set_capture_state_req_captured_source_t
{
    LIBMSTAR_VS_SET_CAPTURE_STATE_REQ_CAPTURED_SOURCE_MME,
    LIBMSTAR_VS_SET_CAPTURE_STATE_REQ_CAPTURED_SOURCE_DATA,
    LIBMSTAR_VS_SET_CAPTURE_STATE_REQ_CAPTURED_SOURCE_ALL,
    LIBMSTAR_VS_SET_CAPTURE_STATE_REQ_CAPTURED_SOURCE_NB
};

typedef enum libmstar_vs_set_capture_state_req_captured_source_t
libmstar_vs_set_capture_state_req_captured_source_t;

typedef struct libmstar_vs_set_capture_state_req_t
{
    u8 Station_address[MAC_BIN_LEN];
    u8 state;
    u8 captured;
    u8 captured_source;
}__attribute__ ((__packed__)) libmstar_vs_set_capture_state_req_t;

#define SET_CAPTURE_CAPTURED_SOURCE_I_IS_VALID(ScCapturedSource) \
    ((ScCapturedSource) < LIBMSTAR_VS_SET_CAPTURE_STATE_REQ_CAPTURED_SOURCE_NB)

#endif /* vs_set_capture_state_h */
