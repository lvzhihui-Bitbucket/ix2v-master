#ifndef libmstar_inc_vs_get_status_h
#define libmstar_inc_vs_get_status_h
/* Cesar project {{{
 *
 * Copyright (C) 2017 Mstar
 *
 * <<<Licence>>>
 *
 * }}} */
/**
 * \file    cp/msg/inc/vs_get_status.h
 * \brief   VS_GET_STATUS Structures.
 * \ingroup libmstar
 */

typedef enum libmstar_vs_get_status_req_reqtype_t
libmstar_vs_get_status_req_reqtype_t;

/** VS_GET_STATUS.CNF Result. */
enum libmstar_vs_get_status_cnf_result_t
{
    LIBMSTAR_VS_GET_STATUS_CNF_RESULT_SUCCESS,
    LIBMSTAR_VS_GET_STATUS_CNF_RESULT_FAILURE,
    LIBMSTAR_VS_GET_STATUS_CNF_RESULT_NB
};

/** VS_GET_STATUS.CNF Status. */
enum libmstar_vs_get_status_cnf_status_t
{
    LIBMSTAR_VS_GET_STATUS_CNF_STATUS_UNASSOCIATED,
    LIBMSTAR_VS_GET_STATUS_CNF_STATUS_ASSOCIATED,
    LIBMSTAR_VS_GET_STATUS_CNF_STATUS_AUTHENTICATED,
    LIBMSTAR_VS_GET_STATUS_CNF_STATUS_NB
};

/** VS_GET_STATUS.CNF CCo. */
enum libmstar_vs_get_status_cnf_cco_t
{
    LIBMSTAR_VS_GET_STATUS_CNF_CCO_IS_NOT_CCO,
    LIBMSTAR_VS_GET_STATUS_CNF_CCO_IS_CCO,
    LIBMSTAR_VS_GET_STATUS_CNF_CCO_NB
};

/** VS_GET_STATUS.CNF Preferred CCo. */
enum libmstar_vs_get_status_cnf_preferred_cco_t
{
    LIBMSTAR_VS_GET_STATUS_CNF_PREFERRED_CCO_IS_NOT_CCO_PREFERRED,
    LIBMSTAR_VS_GET_STATUS_CNF_PREFERRED_CCO_IS_CCO_PREFERRED,
    LIBMSTAR_VS_GET_STATUS_CNF_PREFERRED_CCO_NB
};

/** VS_GET_STATUS.CNF Backup CCo. */
enum libmstar_vs_get_status_cnf_backup_cco_t
{
    LIBMSTAR_VS_GET_STATUS_CNF_BACKUP_CCO_IS_NOT_BACKUP_CCO,
    LIBMSTAR_VS_GET_STATUS_CNF_BACKUP_CCO_IS_BACKUP_CCO,
    LIBMSTAR_VS_GET_STATUS_CNF_BACKUP_CCO_NB
};

/** VS_GET_STATUS.CNF Proxy CCo. */
enum libmstar_vs_get_status_cnf_proxy_cco_t
{
    LIBMSTAR_VS_GET_STATUS_CNF_PROXY_CCO_IS_NOT_PROXY_CCO,
    LIBMSTAR_VS_GET_STATUS_CNF_PROXY_CCO_IS_PROXY_CCO,
    LIBMSTAR_VS_GET_STATUS_CNF_PROXY_CCO_NB
};

/** VS_GET_STATUS.CNF Simple Connect. */
enum libmstar_vs_get_status_cnf_simple_connect_t
{
    LIBMSTAR_VS_GET_STATUS_CNF_SIMOLE_CONNECT_IS_NOT_SC,
    LIBMSTAR_VS_GET_STATUS_CNF_SIMOLE_CONNECT_IS_SC,
    LIBMSTAR_VS_GET_STATUS_CNF_SIMOLE_CONNECT_NB
};

/** VS_GET_STATUS.CNF link_stat. */
enum libmstar_vs_get_status_cnf_link_stat_t
{
    LIBMSTAR_VS_GET_STATUS_CNF_LINK_STATE_DISCONNECTED,
    LIBMSTAR_VS_GET_STATUS_CNF_LINK_STATE_CONNECTED,
    LIBMSTAR_VS_GET_STATUS_CNF_LINK_STATE_NB
};

/** VS_GET_STATUS.CNF ready for operation. */
enum libmstar_vs_get_status_cnf_ready_operation_t
{
    LIBMSTAR_VS_GET_STATUS_CNF_READY_OPERATION_NOT_READY,
    LIBMSTAR_VS_GET_STATUS_CNF_READY_OPERATION_READY,
    LIBMSTAR_VS_GET_STATUS_CNF_READY_OPERATION_NB
};

typedef struct libmstar_vs_get_status_cnf_t
{
    u8 result;
    u8 status;
    u8 cco;
    u8 preferred_cco;
    u8 backup_cco;
    u8 proxy_cco;
    u8 simple_connect;
    u8 link_state;
    u8 ready_operation;
    s64 freq_error;
    s64 freq_offset;
    u64 uptime;
    u64 authenticated_time;
    u16 authenticated_count;
}__attribute__ ((__packed__)) libmstar_vs_get_status_cnf_t;

#endif /* libmstar_inc_vs_get_status_h */
