#ifndef LIBMSTAR_EOC_H
#define LIBMSTAR_EOC_H

typedef unsigned long long int u64;
typedef unsigned int u32;
typedef unsigned short u16;
typedef unsigned char u8;
typedef unsigned char uint8_t;
typedef signed long long s64;
typedef signed int s32;
typedef signed short s16;
typedef signed char s8;

#define MAC_STR_MAX_LEN 18
#define MAC_BIN_LEN 6
#define LIBMSTAR_SW_VER_MAX_LEN 128
#define LIBMSTAR_NAME_MAX_LENGTH 128
#define LIBMSTAR_PATAMETER_MAX_LENGTH 512

#define EOC_DEFAULT_MAC "00:b0:52:00:00:01"
#define MME_DEFAULT_MMV 0
#define MME_MMV1        1
#define MME_MMV0        0


#define ETH_P_HPAV  0x88E1      /* HomePlug AV packet       */

#include "vs_eth_stats.h"
#include "vs_file_access.h"
#include "vs_get_eth_phy.h"
#include "vs_get_link_stat.h"
#include "vs_get_nvram.h"
#include "vs_get_nw_info.h"
#include "vs_get_pwm_stats.h"
#include "vs_get_snr.h"
#include "vs_get_status.h"
#include "vs_get_tonemap.h"
#include "vs_get_tonemask.h"
#include "vs_get_version.h"
#include "vs_reset.h"
#include "vs_set_capture_state.h"
#include "vs_set_nvram.h"
#include "vs_diag_debug.h"
#include "vs_product.h"
#include "vs_get_cco_dev.h"
#include "parson.h"
#include "platform_endian.h"


typedef enum {
    LIBMSTAR_SUCCESS = 0,
    /** bad input parameters */
    LIBMSTAR_ERROR_PARAM,
    /** not enough available space */
    LIBMSTAR_ERROR_NO_SPACE,
    /** item / data not found */
    LIBMSTAR_ERROR_NOT_FOUND,
    /** system error, see errno for more details */
    LIBMSTAR_ERROR_SYSTEM,
    /** mme function failed */
    LIBMSTAR_ERROR_MME,
    /** general error */
    LIBMSTAR_ERROR_GEN,
    /** an error specific to the processing done by the function.
     * More details may be provided in an other status returned by the
     * function. */
    LIBMSTAR_ERROR_PROCESSING,
    /** return value if autoconfiguration exist */
    LIBMSTAR_ERROR_AUTOCONF_EXIST,
    /** MAC address error format **/
    LIBMSTAR_ERROR_MAC,
    /** error check utility **/
    LIBMSTAR_ERROR_CHECK,
    /** slave with MAC address is not allowed */
    LIBMSTAR_ERROR_AUTH,
} libmstar_error_t;

typedef struct libmstar_patameter_info_t
{
    char filename[LIBMSTAR_NAME_MAX_LENGTH];
    char groupname[LIBMSTAR_NAME_MAX_LENGTH];
    char parametername[LIBMSTAR_NAME_MAX_LENGTH];
    int  parametertype;
    int  array_data_type;
    int  array_index;
    char value[LIBMSTAR_PATAMETER_MAX_LENGTH];
}__attribute__ ((__packed__)) libmstar_patameter_info_t;

libmstar_error_t
libmstar_mac_bin_to_str (const unsigned char *bin, char *str);
libmstar_error_t
libmstar_mac_str_to_bin (const char *str, unsigned char *bin);


int
calc_xor_checksum (unsigned char *buf, unsigned int size, unsigned int *chksum);

libmstar_error_t
check_mac_format (const char *mac_addr);

libmstar_error_t
libmstar_vs_get_mst_version (char *interface, char *dest_mac, char *version);

libmstar_error_t
libmstar_vs_reset (char *interface, char *dest_mac,
                        libmstar_vs_reset_cnf_t *reset_cnf);

libmstar_error_t
libmstar_vs_get_tonemask (char *interface, char *dest_mac, char *peer_mac,
                                 libmstar_vs_get_tonemask_cnf_t *tonemask);

libmstar_error_t
libmstar_vs_get_eth_phy (char *interface, char *dest_mac,
                                libmstar_vs_get_eth_phy_cnf_t *eth_phy);

libmstar_error_t
libmstar_vs_eth_stats (char *interface, char *dest_mac,
                       u8 command, libmstar_vs_eth_stats_cnf_t *eth_stats);

libmstar_error_t
libmstar_vs_get_status (char *interface, char *dest_mac,
                        libmstar_vs_get_status_cnf_t *status);

libmstar_error_t
libmstar_vs_get_tonemap (char *interface, char *dest_mac,
                         libmstar_vs_get_tonemap_req_t *tonemap_req,
                         libmstar_vs_get_tonemap_cnf_t *tonemap_cnf);

libmstar_error_t
libmstar_vs_get_snr (char *interface, char *dest_mac,
                          libmstar_vs_get_snr_req_t *snr_req,
                          libmstar_vs_get_snr_cnf_t *snr_cnf);

libmstar_error_t
libmstar_vs_get_link_stats (char *interface, char *dest_mac,
                                    libmstar_vs_get_link_stats_req_t *link_stats_req,
                                    libmstar_vs_get_link_stats_cnf_t *link_stats_cnf);

libmstar_error_t
libmstar_vs_get_nw_info (char *interface, char *dest_mac,
                         libmstar_vs_get_nw_info_cnf_t *nw_info_cnf);

libmstar_error_t
libmstar_vs_set_capture_state (char *interface, char *dest_mac,
                                       libmstar_vs_set_capture_state_req_t *capture_state_req,
                                       libmstar_vs_set_capture_state_cnf_result_t *capture_state_result);

libmstar_error_t
libmstar_vs_set_nvram (char *interface, char *dest_mac,
                             libmstar_vs_set_nvram_req_t *nvram_req,
                             libmstar_vs_set_nvram_cnf_t *nvram_cnf);

libmstar_error_t
libmstar_vs_get_nvram (char *interface, char *dest_mac,
                             libmstar_vs_get_nvram_req_t *get_nvram_req,
                             libmstar_vs_get_nvram_cnf_t *get_nvram_cnf);

libmstar_error_t
libmstar_vs_get_pwm_stats (char *interface, char *dest_mac,
                                  libmstar_vs_get_pwm_stats_cnf_t *get_pwm_stats_cnf);

libmstar_error_t
libmstar_vs_diag_debug (char *interface, char *dest_mac,
                        libmstar_vs_diag_debug_req_t *diag_debug_req,
                        libmstar_vs_diag_debug_cnf_t *diag_debug_cnf);

libmstar_error_t
libmstar_vs_file_access_send_req (char *interface, char *dest_mac,
                    vs_file_access_req_t *file_access_req,
                    vs_file_access_cnf_t *file_access_cnf,
                    unsigned char *cnf_data);

libmstar_error_t
libmstar_vs_file_access_req (char *interface, char *dest_mac, vs_file_access_cmd_t *cmd);

int
libmstar_json_object_set_patameter_value (libmstar_patameter_info_t *patameter_info);

int
libmstar_conf_file_patameter_write (char *interface_tmp, char *dest_mac,
                                    libmstar_patameter_info_t *patameter_info);

libmstar_error_t
libmstar_vs_product (char *interface, char *dest_mac,
                           libmstar_vs_product_req_t *product_req,
                           libmstar_vs_product_cnf_t *product_cnf);

libmstar_error_t
libmstar_vs_get_mst_version_with_multi_response (char *interface, char *dest_mac, libmstar_vs_get_version_cnf_t **version_cnf_list);

libmstar_error_t
libmstar_vs_get_cco_dev_with_multi_response (char *interface, char *dest_mac, libmstar_vs_get_cco_dev_cnf_t **devcnf_head);

JSON_Value *
libmstar_conf_file_patameter_read (char *interface_tmp, char *dest_mac,
                                                          char *filename, char *groupname,
                                                          char *parametername);

/* free libmstar_vs_get_cco_dev_cnf_t. only call after with multi response.*/
void free_vs_cco_dev_cnf (libmstar_vs_get_cco_dev_cnf_t *ptr);

/* free libmstar_vs_get_cco_dev_cnf_t. only call after with multi response.*/
void free_vs_get_version_cnf (libmstar_vs_get_version_cnf_t *ptr);

#endif
