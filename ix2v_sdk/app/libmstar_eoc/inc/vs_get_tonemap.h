#ifndef libmstar_inc_vs_get_tonemap_h
#define libmstar_inc_vs_get_tonemap_h
/* Inka project {{{
 *
 * Copyright (C) 2018 MStar Semiconductor
 *
 * <<<Licence>>>
 *
 * }}} */
/**
 * \file    cp/msg/inc/vs_get_tonemap.h
 * \brief   VS_GET_TONEMAP Structures.
 * \ingroup libmstar
 */
#define TMI_MAX_LENGTH    7
#define INT_MAX_LENGTH    32

/** VS_GET_TONEMAP TMI. */
enum libmstar_vs_get_tonemap_tmi_t
{
    LIBMSTAR_VS_GET_TONEMAP_TMI_ROBO,
    LIBMSTAR_VS_GET_TONEMAP_TMI_HS_ROBO,
    LIBMSTAR_VS_GET_TONEMAP_TMI_MINI_ROBO = 0x2,
    LIBMSTAR_VS_GET_TONEMAP_TMI_RSVD_MIN,
    LIBMSTAR_VS_GET_TONEMAP_TMI_AV1_MIN = 0x4,
    LIBMSTAR_VS_GET_TONEMAP_TMI_AV2_MIN = 0x7,
    LIBMSTAR_VS_GET_TONEMAP_TMI_MAX = 0x1F,
    LIBMSTAR_VS_GET_TONEMAP_TMI_RSVD_MAX = 0xFE,
    LIBMSTAR_VS_GET_TONEMAP_TMI_AND_INT_ONLY,
    LIBMSTAR_VS_GET_TONEMAP_TMI_NB
};

typedef enum libmstar_vs_get_tonemap_tmi_t libmstar_vs_get_tonemap_tmi_t;

#define TMI_IS_VALID(tmi) ((((tmi) <= LIBMSTAR_VS_GET_TONEMAP_TMI_MAX)  \
                            && ((tmi) >= LIBMSTAR_VS_GET_TONEMAP_TMI_AV1_MIN))\
                           || ((tmi) ==                                      \
                                     LIBMSTAR_VS_GET_TONEMAP_TMI_AND_INT_ONLY))

/** VS_GET_TONEMAP.REQ direction. */
enum libmstar_vs_get_tonemap_req_dir_t
{
    LIBMSTAR_VS_GET_TONEMAP_REQ_DIRECTION_TX,
    LIBMSTAR_VS_GET_TONEMAP_REQ_DIRECTION_RX,
    LIBMSTAR_VS_GET_TONEMAP_REQ_DIRECTION_NB
};

typedef enum libmstar_vs_get_tonemap_req_dir_t libmstar_vs_get_tonemap_req_dir_t;

/** VS_GET_TONEMAP.CNF result. */
enum libmstar_vs_get_tonemap_cnf_result_t
{
    LIBMSTAR_VS_GET_TONEMAP_CNF_RESULT_SUCCESS,
    LIBMSTAR_VS_GET_TONEMAP_CNF_RESULT_FAILURE,
    LIBMSTAR_VS_GET_TONEMAP_CNF_RESULT_BAD_TMP_INT_LIST_ID,
    LIBMSTAR_VS_GET_TONEMAP_CNF_RESULT_NOT_SUPPORTED,
    LIBMSTAR_VS_GET_TONEMAP_CNF_RESULT_NB
};

typedef enum libmstar_vs_get_tonemap_cnf_result_t
libmstar_vs_get_tonemap_cnf_result_t;


typedef struct libmstar_vs_get_tonemap_req_t
{
    u8 Station_address[MAC_BIN_LEN];
    u8 tmi;
    u8 int_id;
    u8 dir;
    u8 carrier_group;
}__attribute__ ((__packed__)) libmstar_vs_get_tonemap_req_t;

typedef struct int_info_t
{
    unsigned short int_et;
    unsigned char  int_tmi;
    unsigned char  int_rx_gain;
    unsigned char  int_fec;
    unsigned char  int_gi;
    u32  int_phy_rate;
}__attribute__ ((__packed__)) int_info_t;

typedef struct libmstar_vs_get_tonemap_cnf_t
{
    u8 result;
    u8 Reserved[4];
    u8 int_id;
    u8 tmi_default;
    u8 tmi_length;
    u8 tmi_list[TMI_MAX_LENGTH];
    u8 int_length;
    int_info_t int_info[INT_MAX_LENGTH];
    u8 tmi;
    s8 tm_rx_gain;
    u8 tm_fec;
    u8 tm_gi;
    u32 tm_phy_rate;
    u8 carrier_group;
    u16 modulation_list_length;
    u8 modulation_list[1024];
}__attribute__ ((__packed__)) libmstar_vs_get_tonemap_cnf_t;

#endif /* libmstar_inc_vs_get_tonemap_h */
