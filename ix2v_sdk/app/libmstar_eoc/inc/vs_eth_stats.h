#ifndef libmstar_inc_vs_eth_stats_h
#define libmstar_inc_vs_eth_stats_h
/* Cesar project {{{
 *
 * Copyright (C) 2018 Mstar
 *
 * <<<Licence>>>
 *
 * }}} */
/**
 * \file    cp/msg/inc/vs_eth_stats.h
 * \brief   VS_ETH_STATS Structures.
 * \ingroup cp_msg
 */

/** VS_ETH_STATS.REQ ReqType. */
enum libmstar_vs_eth_stats_req_reqtype_t
{
    LIBMSTAR_VS_ETH_STATS_REQ_REQTYPE_GET,
    LIBMSTAR_VS_ETH_STATS_REQ_REQTYPE_RESET,
    LIBMSTAR_VS_ETH_STATS_REQ_REQTYPE_NB
};

typedef enum libmstar_vs_eth_stats_req_reqtype_t
libmstar_vs_eth_stats_req_reqtype_t;

/** VS_ETH_STATS.CNF result. */
enum libmstar_vs_eth_stats_cnf_result_t
{
    LIBMSTAR_VS_ETH_STATS_CNF_RESULT_SUCCESS,
    LIBMSTAR_VS_ETH_STATS_CNF_RESULT_FAILURE,
    LIBMSTAR_VS_ETH_STATS_CNF_RESULT_NB
};

typedef enum libmstar_vs_eth_stats_cnf_result_t
libmstar_vs_eth_stats_cnf_result_t;

typedef struct libmstar_vs_eth_stats_cnf_t
{
    u8 result;
    u32 rx_packets;
    u32 rx_good_packets;
    u32 rx_good_unitcast_packets;
    u32 rx_good_multicast_packets;
    u32 rx_good_broadcast_packets;
    u32 rx_error_packets;
    u32 rx_fifo_overflow;
    u32 tx_packets;
    u32 tx_good_packets;
    u32 tx_good_unitcast_packets;
    u32 tx_good_multicast_packets;
    u32 tx_good_broadcast_packets;
    u32 tx_error_packets;
    u32 tx_fifo_underflow;
    u32 tx_collision;
    u32 tx_carrier_error;
}__attribute__ ((__packed__)) libmstar_vs_eth_stats_cnf_t;

#endif /* vs_eth_stats_h */
