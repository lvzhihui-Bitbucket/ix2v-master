/**
  ******************************************************************************
  * @file    obj_RequestSn.c
  * @author  czb
  * @version V00.01.00
  * @date    2016.5.31
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 

#include <sys/wait.h>
#include "vdp_uart.h"
#include "obj_Get_Ph_MFG_SN.h"
#include "one_tiny_task.h"
#include "tcp_Client_process.h"
#include "obj_PublicInformation.h"
#include "obj_IoInterface.h"
#include "task_Beeper.h"
#include "define_file.h"
#include "task_Event.h"

#ifdef SN_SERVER
#undef SN_SERVER
#endif
#define SN_SERVER			"47.106.104.38"

#ifdef SN_PORT
#undef SN_PORT
#endif
#define SN_PORT				8862

#ifdef NET_ETH0
#undef NET_ETH0
#endif
#define NET_ETH0			"eth0"   

#ifdef NET_WLAN0
#undef NET_WLAN0
#endif
#define NET_WLAN0			"wlan0"   

static MY_SN mySn = {.type = SN_Type_NONE};

static char* SixByteSnToString(char* sixByteSn, char* snString,int order)
{
	int i;
	char sn[6];

	for(i = 0; i < 6; i++)
	{

		if(order==1)
		{
			if(i == 0)
			{
				sn[i] = (sixByteSn[5-i]<<4)+4;;
			}
			else
			{
				sn[i] = sixByteSn[5-i];
			}
		}
		else
		{
			if(i == 0)
			{
				sn[i] = (sixByteSn[i]<<4)+4;;
			}
			else
			{
				sn[i] = sixByteSn[i];
			}
		}
	
	}
	snprintf(snString, 20, "%02x%02x%02x%02x%02x%02x", sn[0], sn[1], sn[2], sn[3], sn[4], sn[5]);

	return snString;
}

static int ReadDs2411Sn(char* snString)
{
	int i;
	DS2411_SN ds2411_sn;
	extern FILE *hal_fd;
	int ret = 0;

	for(i = 0; i < 5; i++)
	{
		memset(&ds2411_sn, 0, sizeof(DS2411_SN));
		ret = ioctl(hal_fd, _IOR('G', 28,  DS2411_SN), &ds2411_sn);
		if(ret == 0)
		{
			break;
		}
		dprintf("ReadDs2411 Error %d times\n", i+1);
		usleep(20*1000);
	}

	if(ret == 0)
	{
		SixByteSnToString(ds2411_sn.serial_number, snString,1);
		dprintf("ReadDs2411Sn: %s\n", snString);
	}

	return ret;
}

static int ReadRandSn(char* snString)
{
	char mac[6];
	char sn[6];
	int i;

	GetLocalMacByDevice(NET_ETH0, mac);	
	for(i = 0; i < 6; i++)
	{
		srand(mac[i]+time(NULL));
		sn[i] = rand()%256;
	}

	SixByteSnToString(sn, snString,0);
	dprintf("ReadRandSn: %s\n", snString);

	return 0;
}


static int ReadTiny1616Sn(char* snString)
{
	int ret = -1;
	int len;
	char tempSnString[20];
	char tempMd5[16];
	char md5[16];

	if(API_IO_ReadMcu(IO_MFG_SN, tempSnString))
	{
		StringMd5_Calculate(tempSnString, md5);
		if(API_IO_ReadMcu(IO_MFG_SN_CHECK, tempMd5) && !memcmp(md5, tempMd5, 16))
		{
			ret = 0;
			strcpy(snString, tempSnString);
		}
	}

	return ret;
}

static int SetTiny1616Sn(char* snString)
{
	int ret;
	char md5[16];
	char eoc_mac[18]={0};
	
	ret = API_IO_WriteMcu(IO_MFG_SN, snString);
	if(ret)
	{
		StringMd5_Calculate(snString, md5);
		ret = API_IO_WriteMcu(IO_MFG_SN_CHECK, md5);
		if(ret)
		{
			//BEEP_T6();
			//HardwareRestart("SetTiny1616Sn");
			dprintf("SetTiny1616Sn :%s\n", snString);

			GetPhMFGSnInit();
			//api_create_default_sip_info();
			sleep(2);
			//SetEocMacBySn();
			if(IfHasSn())
			{
				if(API_Para_Read_String(IO_EOC_MAC,eoc_mac)==1)
				{
					API_VTK_Set_Eoc_Mac("eth0",eoc_mac);
					#ifndef IX_LINK
					if(eoc_mac[0]!=0)
						WriteEocSimulateFlashBackupFile(eoc_mac);
					#endif
				}
				API_Set_Lan_Reload();
				///BEEP_CONFIRM();
			}
			//else
			//	BEEP_ERROR();
			//CreateEocMacBySn(snString);
			//if(snString[0]!=0)
			//	WriteEocSimulateFlashBackupFile(snString);
			//api_Lan_Reload();
		}
	}
	
	return ret;
}

int IfHasSn(void)
{
	SN_Type_e type;

	if(mySn.type == SN_Type_Rand || mySn.type == SN_Type_NONE)
	{
		return 0;
	}
	else
	{
		return 1;
	}
}
static int Tcp_GetSn(char* net_dev_name, char *ip_str, int port, char* snString)
{
	int  err;
	int fd = 0;
	int ret = -1;
	MFG_SN_REQ recv_pkt;
	int recv_len;

	srand(time(NULL));
	
	// connect
	int retry_cnt = 4;

	for(retry_cnt = 4, err = -1; retry_cnt > 0 && err != 0; retry_cnt--)
	{
		err = tcp_client_connect_with_timeout(net_dev_name, ip_str, port, 5, &fd);
	}
	
	if(err == 0)
	{
		int setflag = 1;
		int flags = fcntl(fd, F_GETFL, 0);
		if (flags < 0) 
		{
			dprintf("Get flags error:%s\n", strerror(errno));
			goto connect_error;
		}
		flags &= ~O_NONBLOCK;
		if (fcntl(fd, F_SETFL, flags) < 0) 
		{
			dprintf("Set flags error:%s\n", strerror(errno));
			goto connect_error;
		}
		
		/*
 		struct timeval tv_out;
    	tv_out.tv_sec = 3;
    	tv_out.tv_usec = 0;
    	setsockopt(fd, SOL_SOCKET, SO_RCVTIMEO, &tv_out, sizeof(tv_out));
		*/

		dprintf("connect ok! [server:%s]\n",ip_str);
	
		// send
		memcpy(recv_pkt.head.head, IX_DEVICE_REMOTE_CMD_HEAD, 4);
		recv_pkt.head.cmd_type = MFG_SN_CLIENT_REQ;
		recv_pkt.head.cmd_id = 1;
		recv_pkt.head.dev_type = 0;		//0分机，1主机
		recv_pkt.head.data_len = 0;
		Api_tcp_client_send(fd, &recv_pkt, sizeof(recv_pkt.head), 1);
		
		// recv
		recv_len = sizeof(recv_pkt);
		memset(&recv_pkt, 0, recv_len);
		if (Api_tcp_client_recv(fd, (char*)&recv_pkt, recv_len, 3) > 0) // success
		{
			dprintf("recv_pkt.head.cmd_type=0x%04x, recv_pkt.result=%d\n", recv_pkt.head.cmd_type, recv_pkt.result);	
			if(!memcmp(recv_pkt.head.head, IX_DEVICE_REMOTE_CMD_HEAD, 4) && recv_pkt.head.cmd_type == MFG_SN_SERVER_RSP && recv_pkt.result == 0)
			{
				memcpy(recv_pkt.head.head, IX_DEVICE_REMOTE_CMD_HEAD, 4);
				recv_pkt.head.cmd_type = MFG_SN_CLIENT_ACK;
				recv_pkt.head.cmd_id = 2;
				recv_pkt.head.dev_type = 0;		//0分机，1主机
				recv_pkt.head.data_len = 8;
				Api_tcp_client_send(fd, &recv_pkt, sizeof(recv_pkt), 1);

				SixByteSnToString(recv_pkt.sn, snString,0);
				ret = 0;
			}
		}
	}

	connect_error:

	if(fd > 0)
	{
		shutdown(fd, SHUT_RDWR);
		close(fd);
	}
	return ret;
}


int Api_Network_GetSn(char* netWorkDevice)
{
	if(!IfHasSn())
	{
		char snString[20];
		if(Tcp_GetSn(netWorkDevice, SN_SERVER, SN_PORT, snString) == 0)
		{
			//SetTiny1616Sn(snString);
			if(!SetTiny1616Sn(snString))
				return -2;
			return 0;
		}
		else
			return -1;
	}
	return 1;
}


static const char* SourceTypeToString(SN_Type_e type)
{
	char* ret = SN_SourceType_NONE;

	switch (type)
	{
		case SN_Type_DS2411:
			ret = SN_SourceType_DS2411;
			break;
		case SN_Type_Tiny1616:
			ret = SN_SourceType_MCU;
			break;
		case SN_Type_Rand:
			ret = SN_SourceType_Rand;
			break;
	}

	return ret;
}

SN_Type_e SourceStringToType(char* typeString)
{
	SN_Type_e ret = SN_Type_NONE;

	if(typeString)
	{
		if(!strcmp(SN_SourceType_DS2411, typeString))
		{
			ret = SN_Type_DS2411;
		}
		else if(!strcmp(SN_SourceType_MCU, typeString))
		{
			ret = SN_Type_Tiny1616;
		}
		else if(!strcmp(SN_SourceType_Rand, typeString))
		{
			ret = SN_Type_Rand;
		}
	}

	return ret;
}

void GetPhMFGSnInit(void)
{
	char snString[20];
	char eoc_mac[18]={0};
	int i;

	SN_Type_e type = SourceStringToType(cJSON_GetStringValue(API_Para_Read_Public(IO_MFG_SN_Source)));

	if(type != SN_Type_DS2411 && type != SN_Type_Tiny1616)
	{
		if(ReadDs2411Sn(snString) == 0)
		{
			strcpy(mySn.serial_number, snString);
			mySn.type = SN_Type_DS2411;
		}
		else if (ReadTiny1616Sn(snString) == 0)
		{
			strcpy(mySn.serial_number, snString);
			mySn.type = SN_Type_Tiny1616;
		}
		else if(type == SN_Type_Rand)
		{
			API_Para_Read_String(IO_MFG_SN, mySn.serial_number);
			mySn.type = SN_Type_Rand;
		}
		else if(type == SN_Type_NONE)
		{
			ReadRandSn(snString);
			strcpy(mySn.serial_number, snString);
			mySn.type = SN_Type_Rand;
		}
		API_Para_Write_String(IO_MFG_SN_Source, SourceTypeToString(mySn.type));
		API_Para_Write_String(IO_MFG_SN, mySn.serial_number);
		
		
		
	}
	else
	{
		API_Para_Read_String(IO_MFG_SN, mySn.serial_number);
		mySn.type = type;
	}

	API_PublicInfo_Write_String(PB_MFG_SN, API_GetLocalSnString());
	API_PublicInfo_Write_String(PB_MFG_SN_SOURCE, SourceTypeToString(mySn.type));
	API_IO_UpdateMcu();
	//ShellCmdPrintf("1111111111%d:%s\n",mySn.type,mySn.serial_number);
	if(mySn.type == SN_Type_DS2411 || mySn.type == SN_Type_Tiny1616)
	{
		for(i=0;i<6;i++)
		{
			eoc_mac[i*3]=mySn.serial_number[i*2];
			eoc_mac[i*3+1]=mySn.serial_number[i*2+1];
			eoc_mac[i*3+2]=':';
		}
		eoc_mac[1]='e';
		eoc_mac[17]=0;
		//ShellCmdPrintf("1111111111%d:%s\n",mySn.type,eoc_mac);
		if(API_Para_Read_String2(IO_EOC_MAC))
		{
			if(strcmp(eoc_mac,API_Para_Read_String2(IO_EOC_MAC))!=0)
			{
				API_Para_Write_String(IO_EOC_MAC,eoc_mac);
			}
		}
	}
}


char* API_GetLocalSnString(void)
{
	while(mySn.type == SN_Type_NONE)
	{
		dprintf("Wait for getting sn !!!\n");
		sleep(1);
	}

	return mySn.serial_number;
}


int API_GetLocalSn(char* pSixByeSn)
{
	int sn[6];
	int i;
    sscanf(API_GetLocalSnString(), "%02x%02x%02x%02x%02x%02x", &sn[0], &sn[1], &sn[2], &sn[3], &sn[4], &sn[5]);
	for(i=0;i<6;i++)
	{
		pSixByeSn[i]=sn[i];
	}
	return 0;
}

int ReqSNFromServer_Callback(cJSON *cmd)
{
	bprintf("1111\n");
	char *netdev=GetShellCmdJsonString(cmd,"NetInterface");
	if(netdev==NULL)
		netdev=NET_ETH0;
	#ifdef PID_IX850
	void *tip=API_TIPS_Ext_Time("Request sn from sever...", 40);
	#endif
	int rev=Api_Network_GetSn(netdev);
	if(rev==0)
	{
		BEEP_CONFIRM();
		#ifdef PID_IX850
		API_TIPS_Close_Ext(tip);
		tip=API_TIPS_Ext_Time("Request ok", 3);
		#endif
	}
	else if(rev==-1)
	{
		BEEP_ERROR();
		#ifdef PID_IX850
		API_TIPS_Close_Ext(tip);
		tip=API_TIPS_Ext_Time("Link sever error", 3);
		sleep(2);
		//menu_sn_eoc_err_disp();
		#endif
		API_Event_By_Name(EventSnErrDisp);
	}
	else if(rev==-2)
	{
		BEEP_ERROR();
		#ifdef PID_IX850
		API_TIPS_Close_Ext(tip);
		tip=API_TIPS_Ext_Time("Save sn error", 3);
		sleep(2);
		//menu_sn_eoc_err_disp();
		#endif
		API_Event_By_Name(EventSnErrDisp);
	}
	
	return 0;
}

int SetEocMacCallback(cJSON *cmd)
{
	char eoc_mac[18]={0};
	
	if(IfHasSn())
	{
		if(API_Para_Read_String(IO_EOC_MAC,eoc_mac)==1)
		{
			API_VTK_Set_Eoc_Mac("eth0",eoc_mac);
		}
		API_Set_Lan_Reload();
		BEEP_CONFIRM();
	}
	
	return 0;
}

void CreateEocMacBySn(char *eoc_mac)
{
	//SYS_VER_INFO_T sysInfo;
	int i;
	if(mySn.type == SN_Type_DS2411 || mySn.type == SN_Type_Tiny1616)
	{
		API_Para_Read_String(IO_EOC_MAC,eoc_mac);
		
	}
	else
		eoc_mac[0]=0;
	
}
void CheckEocMacBySn(char *check_mac)
{
	char eoc_mac[18]={0};
	//SYS_VER_INFO_T sysInfo;
	int i;
	
	if(mySn.type == SN_Type_DS2411 || mySn.type == SN_Type_Tiny1616)
	{
		API_Para_Read_String(IO_EOC_MAC,eoc_mac);
		
		if(API_VTK_CMP_Mac(check_mac,eoc_mac)==0)
			API_PublicInfo_Write_String("EOC_Mac_State","Ok");
		else
			API_PublicInfo_Write_String("EOC_Mac_State","Error");
		
	}
	else
		API_PublicInfo_Write_String("EOC_Mac_State","Error");

}

#define EOC_SIMULATE_FLASH_BUCKUP 				"/VtkSimFlash.bak"

void CheckEocSimulateFlashBackupFile(char *eoc_mac)
{
	#ifdef NOT_WITH_MCU 
	if(IfHasSn())
	{
		if(API_VTK_Get_Eoc_File("eth0",eoc_mac,EOC_SIMULATE_FLASH_BUCKUP,"/tmp/eoc_file.tmp")==0)
		{
			char md51[16],md52[16];
			FileMd5_Calculate("/tmp/eoc_file.tmp",md51);
			FileMd5_Calculate(MCU_SIMULATE_FLASH_PATH,md52);
			system("rm /tmp/eoc_file.tmp");
			if(memcmp(md52,md51,16)==0)
			{
				API_PublicInfo_Write_String("EOC_SN_Bak","Ok");
				bprintf("!!!!!CheckEocMacBackupFile\n");
				return;
			}
			
		}
		
		if(API_VTK_Set_Eoc_File("eth0",eoc_mac,EOC_SIMULATE_FLASH_BUCKUP,MCU_SIMULATE_FLASH_PATH)==0)
		{
			API_PublicInfo_Write_String("EOC_SN_Bak","Ok");
			bprintf("!!!!!CheckEocMacBackupFile\n");
		}
		else
		{
			API_PublicInfo_Write_String("EOC_SN_Bak","Fail");
			bprintf("!!!!!CheckEocMacBackupFile\n");
		}
		
	}
	else
	{	
		char dest_mac[18]={0};
		if(API_VTK_Get_Eoc_Mac_Ver("eth0",dest_mac,NULL)!=0)
		{
			API_PublicInfo_Write_String("EOC_SN_Bak","LinkFail");
			bprintf("!!!!!CheckEocMacBackupFile\n");
			return;
		}
		if(API_VTK_Get_Eoc_File("eth0",dest_mac,EOC_SIMULATE_FLASH_BUCKUP,MCU_SIMULATE_FLASH_PATH)==0)
		{
			API_PublicInfo_Write_String("EOC_SN_Bak","Ok");
			MCU_SimulateFlashInit();
			GetPhMFGSnInit();
			bprintf("!!!!!CheckEocMacBackupFile\n");
			UpateEocState(dest_mac);
			if(IfHasSn())
			{
				API_Set_Lan_Reload();
				CmdRingReq("Recover SN from EOC");
			}
			return;
		}
		else
			API_PublicInfo_Write_String("EOC_SN_Bak","NoFile");
	}
	#endif
}
void WriteEocSimulateFlashBackupFile(char *eoc_mac)
{
	#ifdef NOT_WITH_MCU 
	if(API_VTK_Set_Eoc_File("eth0",eoc_mac,EOC_SIMULATE_FLASH_BUCKUP,MCU_SIMULATE_FLASH_PATH)==0)
	{
		API_PublicInfo_Write_String("EOC_SN_Bak","Ok");
		bprintf("!!!!!CheckEocMacBackupFile\n");
	}
	else
	{
		API_PublicInfo_Write_String("EOC_SN_Bak","Fail");
		bprintf("!!!!!CheckEocMacBackupFile\n");
	}
	#endif
}
void UpdateEocParameter(char *eoc_mac)
{
	char para_str[100];
	int para_int;
	int para_type;
	char *para_value;
	char *para_file;
	char *para_group;
	char *para_name;
	char* pbEocMacState = API_PublicInfo_Read_String("EOC_Mac_State");
	 if(pbEocMacState==NULL||strcmp(pbEocMacState, "Ok")!=0)
	 	return;
	  
	cJSON *Eoc_Para_Update=API_Para_Read_Public("Eoc_Para_Update");
	if(Eoc_Para_Update)
	{
		cJSON *node;
		int reset_flag=0;
		cJSON_ArrayForEach(node, Eoc_Para_Update)
		{
			para_type=GetEventItemInt(node, "Type");
			para_value=GetEventItemString(node, "Value");
			para_file=GetEventItemString(node, "File");
			para_group=GetEventItemString(node, "Group");
			para_name=GetEventItemString(node, "Name");
			if(para_type==3||para_type==6)
			{
				#if 1
				if(API_VTK_Get_Eoc_Parameter("eth0",eoc_mac,"/inka_update.conf",para_group,para_name,&para_int)==0)
				{
					if(para_int!=atoi(para_value))
					{
						printf("%s:%s\n",__func__,para_name);
						if(API_VTK_Update_Eoc_Parameter("eth0",eoc_mac,"/inka_update.conf",para_group,para_name, para_type,para_value)>0)
							reset_flag=1;
					}
				}
				else if(API_VTK_Get_Eoc_Parameter("eth0",eoc_mac,para_file,para_group,para_name,&para_int)==0)
				{
					if(para_int!=atoi(para_value))
					{
						printf("%s:%s\n",__func__,para_name);
						if(API_VTK_Update_Eoc_Parameter("eth0",eoc_mac,"/inka_update.conf",para_group,para_name, para_type,para_value)>0)
							reset_flag=1;
					}
				}
				#endif
				
			}
			if(para_type==2)
			{
				#if 0
				if(API_VTK_Get_Eoc_Parameter("eth0",eoc_mac,para_file,para_group,para_name,para_str)==0)
				{
					if(strcmp(para_str,para_value)!=0)
					{
						printf("%s:%s\n",__func__,para_name);
						API_VTK_Set_Eoc_Parameter("eth0",eoc_mac,para_file,para_group,para_name, para_type,para_value);
					}
				}
				#endif
			}	
		}
		if(reset_flag)
		{
			API_VTK_SoftReset_Eoc("eth0",eoc_mac);
		}
		
	}
}
/*********************************************************************************************************
**  End Of File
*********************************************************************************************************/

