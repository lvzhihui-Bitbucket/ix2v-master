#ifndef _obj_R8001Table_h
#define _obj_R8001Table_h

#define R8001TablePath	        "/mnt/nand1-2/UserRes/R8001.json"
#define R8001CheckPath	        "/mnt/nand1-2/UserRes/C8001.json"
#define R8001_KEY_BD_RM_MS		"BD_RM_MS"
#define R8001_KEY_NAME1			"NAME"
#define R8001_KEY_LOCAL			"LOCAL"
#define R8001_KEY_GLOBAL		"GLOBAL"
#define R8001_KEY_DEVICE_TYPE	"DEVICE_TYPE"
#define R8001_KEY_MFG_SN		"MFG_SN"
#define R8001_KEY_NAME2			"NAME2_UTF8"
#define R8001_KEY_IP_STATIC		"IP_STATIC"
#define R8001_KEY_IP_ADDR		"IP_ADDR"
#define R8001_KEY_IP_MASK		"IP_MASK"
#define R8001_KEY_IP_GATEWAY	"IP_GATEWAY"
#define R8001_KEY_CHECK_RESULT	"CHECK_RESULT"

#endif
