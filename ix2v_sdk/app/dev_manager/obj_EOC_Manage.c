/**
  ******************************************************************************
  * @file    obj_GP_OutProxy.c
  * @author  czb
  * @version V00.01.00
  * @date    2022.06.15
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 

#include "obj_IoInterface.h"
#include "obj_PublicInformation.h" 
#include <pthread.h>
#include "elog.h"
#include "utility.h"
#include "obj_gpio.h"

static int eocResteCnt = 0;

int EOC_CheckTimerCallback(int timing)
{
	static int eocUnlinkTime = 0;
	static int eocResteTime = 0;

	if(CheckDeviceType("DH"))
	{
		char* eocLinkState = API_PublicInfo_Read_String(PB_EOC_LINK_STATE);
		char* eocWorkMode = API_Para_Read_String2(EOC_WORK_MODE);
		if(eocLinkState && eocWorkMode)
		{
			if(!strcmp(eocLinkState, "Linked"))
			{
				int link_state= API_VTK_Get_Eoc_Link_State("eth0", NULL);
				//工作在从模式
				if(link_state == 1)
				{
					eocUnlinkTime = 0;
					if(!strcmp(eocWorkMode, "master"))
					{
						eocResteCnt++;
						Set_EOC_WorkMode(1);
					}
				}
				//工作在主模式
				else if(link_state == 2)
				{
					eocUnlinkTime = 0;
					if(strcmp(eocWorkMode, "master"))
					{
						eocResteCnt++;
						Set_EOC_WorkMode(0);
					}
				}
				//没组网
				else
				{
					eocUnlinkTime++;
				}
			}
			else
			{
				eocUnlinkTime++;
			}

			if(eocResteCnt)
			{
				eocResteTime = 16;
			}
			else
			{
				eocResteTime = 1;
			}

			if(eocUnlinkTime >= eocResteTime)
			{
				eocResteCnt++;
				eocUnlinkTime = 0;
				if(!strcmp(eocWorkMode, "master"))
				{
					Set_EOC_WorkMode(1);
				}
				else
				{
					Set_EOC_WorkMode(0);
				}
			}
		}
	}

	return 1;
}
void EOC_WorkModeInit(void)
{
	if(CheckDeviceType("DH"))
	{
		char* eocWorkMode = API_Para_Read_String2(EOC_WORK_MODE);
		if(eocWorkMode && !strcmp(eocWorkMode, "slave"))
		{
			eocResteCnt++;
			Set_EOC_WorkMode(0);
		}
	}
}


void IO_SetEOC_WorkMode(cJSON *old, cJSON *new)
{
    if(!cJSON_Compare(old, new, 1))
    {
		eocResteCnt++;
		if(CheckDeviceType("DH"))
		{
			if(cJSON_IsString(new) && !strcmp(new->valuestring, "master"))
			{
				Set_EOC_WorkMode(1);
			}
			else
			{
				Set_EOC_WorkMode(0);
			}
		}
    }
}

/*********************************************************************************************************
**  End Of File
*********************************************************************************************************/

