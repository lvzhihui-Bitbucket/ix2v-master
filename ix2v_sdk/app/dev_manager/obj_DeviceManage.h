/**
  ******************************************************************************
  * @file    obj_DeviceManage.h
  * @author  cao
  * @version V00.01.00
  * @date    2016.5.31
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 
	  //cao_20170304  整个文件

#ifndef _obj_DeviceManage_H
#define _obj_DeviceManage_H

#include "task_survey.h"

#define SUB_DEVICE_MAX_NUM		32

// Define Object Property-------------------------------------------------------
#pragma pack(1)  //指定按1字节对齐
typedef struct
{	
	int			IpAddr;				//Net device ip（4）
	uint16		subAddr;			//Sub device addr（2）
}DEVICE_REBOOT_REQ_T;

typedef struct
{	
	int			IpAddr;				//Net device ip（4）
	uint16		subAddr;			//Sub device addr（2）
	uint8		result;				//Result（1）
	uint16 		time;				//Restart time（2）预计重启的时间（s为单位）
}DEVICE_REBOOT_RSP_T;

typedef struct
{	
	int			IpAddr;				//Net device ip（4）
	uint16		subAddr;			//Sub device addr（2）
	uint8		level;				//设备恢复等级(1)
}DEVICE_RECOVER_REQ_T;

typedef struct
{	
	int			IpAddr;				//Net device ip（4）
	uint16		subAddr;			//Sub device addr（2）
	uint8		level;				//设备恢复等级(1)
	uint8		result;				//Result（1）
}DEVICE_RECOVER_RSP_T;

typedef struct
{	
	int			IpAddr;				//Net device ip（4）
	uint16		subAddr;			//Sub device addr（2）
	uint8		checkType;			//设备恢复等级(1)
}DEVICE_SINGLE_LINKING_REQ_T;

typedef struct
{	
	int			IpAddr;				//Net device ip（4）
	uint16		subAddr;			//Sub device addr（2）
	uint8		result;				//Result（1）
}DEVICE_SINGLE_LINKING_RSP_T;

typedef struct
{	
	int			IpAddr;									//Net device ip（4）
	uint8		subAddrNum;							//Sub device addr num（1）
	uint16		subAddr[SUB_DEVICE_MAX_NUM];			//Sub device addr（2）
}DEVICE_MULTIPLE_LINKING_START_REQ_T;

typedef struct
{
	uint8		result;				//Result（1）
}DEVICE_MULTIPLE_LINKING_START_RSP_T;

typedef struct
{	
	uint16	addr;									//Sub device addr（2）
	uint8	result;									//linking result
}DEVICE_LINKING_RESULT_T;


typedef struct
{	
	int			IpAddr;									//Net device ip（4）
	uint8		subAddrNum;							//Sub device addr num（1）
	DEVICE_LINKING_RESULT_T subDevice[SUB_DEVICE_MAX_NUM];
}DEVICE_MULTIPLE_LINKING_REPORT_REQ_T;

typedef struct
{	
	uint8 result;
}DEVICE_MULTIPLE_LINKING_REPORT_RSP_T;
#pragma pack()

// Define Object Function - Public----------------------------------------------
void DeviceManageSingleLinking(UDP_MSG_TYPE *pdata);
void DeviceManageMultipleLinkingStart(UDP_MSG_TYPE *pdata);
void DeviceManageMultipleLinkingReport(uint8 deviceNum, DEVICE_LINKING_RESULT_T* pDevice);
void DeviceManageRebootProcess(UDP_MSG_TYPE *pdata);
void DeviceManageRecover(UDP_MSG_TYPE *pdata);

// Define Object Function - Private---------------------------------------------
int DeviceRecoverToDefault(int recover_scene);


#endif


