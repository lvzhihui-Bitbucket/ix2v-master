/**
  ******************************************************************************
  * @file    obj_RequestSn.h
  * @author  cao
  * @version V00.01.00
  * @date    2016.5.31
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 

#ifndef _obj_Get_Ph_MFG_SN_H
#define _obj_Get_Ph_MFG_SN_H

#include "task_survey.h"

// Define Object Property-------------------------------------------------------
//��ȡ2411�����ݽṹ
typedef struct                         
{
   unsigned char family_code;			//8bit
   unsigned char serial_number[6];		//48bit
   unsigned char crc_code;				//8bit
}DS2411_SN;

#define SN_SourceType_NONE    "NONE"
#define SN_SourceType_DS2411  "DS2411"
#define SN_SourceType_MCU     "Tiny1616"
#define SN_SourceType_Rand    "Rand"


//������ǰʹ�õ����к���Դö��
typedef enum
{
	SN_Type_NONE = 0,
	SN_Type_DS2411,
	SN_Type_Tiny1616,
	SN_Type_Rand,
} SN_Type_e;

//����������нṹ��
typedef struct                         
{
    char            serial_number[21];    //21byte
    SN_Type_e       type;               //��ȡ���к�����
}MY_SN;

// Define Object Function - Public----------------------------------------------
//�����ʼ��
void GetPhMFGSnInit(void);

//�ṩ���ⲿ��ȡ����������к�
int API_GetLocalSn(char* pSixByeSn);
char* API_GetLocalSnString(void);

//�ṩ���ⲿ�������绷��׼����֮����Ҫ�ӷ�������ȡ���������к�
int Api_Network_GetSn(char* netWorkDevice);

// Define Object Function - Private---------------------------------------------
//�ڲ��ӿڣ���ȡ2411���к�
static int ReadDs2411Sn(char* snString);

//�ڲ��ӿڣ��򴮿ڷ�����ȡ���кŵ�����
static int ReadTiny1616Sn(char* snString);

//�ڲ��ӿڣ���ȡ������к�
static int ReadRandSn(char* snString);

//�ڲ��ӿڣ������кű����ڴ�������һ�˵�CPU
static int SetTiny1616Sn(char* snString);

//�ڲ��ӿڣ��ж��Ƿ������кţ�ʹ�����������ȡ���кŵ�ʱ�����ж���û�����кţ�
int IfHasSn(void);

#endif


