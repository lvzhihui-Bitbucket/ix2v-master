
#ifndef _HTTP_CLIENT_H_
#define _HTTP_CLIENT_H_

int api_http_get(const char* url, char* rsp_buf, int* rsp_len);
int api_http_post(const char* url, const char* request, char* rsp_buf, int* rsp_len);
int api_http_put(const char* url, const char* request, char* rsp_buf, int* rsp_len);
int api_http_delete(char* url, char* rsp_buf, int* rsp_len);

#endif
