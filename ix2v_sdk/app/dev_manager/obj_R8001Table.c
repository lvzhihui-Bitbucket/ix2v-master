#include <stdio.h>
#include "cJSON.h"
#include "obj_R8001Table.h"
#include "obj_SearchIpByFilter.h"
#include "obj_GetInfoByIp.h"
#include "obj_SYS_VER_INFO.h"
#include "obj_ProgIpByIp.h"
#include "obj_GetIpByNumber.h"
#include "task_Event.h"

cJSON *R8001Tb=NULL;
cJSON* getR8001Item(char *roomnum)
{
    cJSON *where;
	cJSON *item_array;
	cJSON *item;
    if(R8001Tb==NULL)
    {
        R8001Tb = GetJsonFromFile(R8001TablePath);
    }
    item_array=cJSON_CreateArray();
    where = cJSON_CreateObject();
    if(roomnum !=NULL)
    {
        cJSON_AddStringToObject(where, R8001_KEY_BD_RM_MS, roomnum); 
    }
    else
    {
        cJSON_AddStringToObject(where, R8001_KEY_CHECK_RESULT, "-"); 
    }
    
    if(API_TB_SelectBySort(R8001Tb, item_array, where, 0)==0)
        return NULL;
	item=cJSON_Duplicate(cJSON_GetArrayItem(item_array,0),1);
    cJSON_Delete(where);
	cJSON_Delete(item_array);
	return item;
}

int setR8001Result(char *roomnum, char *ip)
{
    cJSON *item;
    cJSON *where;
	int ret = 0;
    char ip_str[20];
    item = getR8001Item(roomnum);
    if(item == NULL)
        return 0;
    if(ip == NULL)
    {
        cJSON_ReplaceItemInObjectCaseSensitive(item, R8001_KEY_CHECK_RESULT, cJSON_CreateString("use"));
        //ShellCmdPrintf("setR8001Result rm=%s use\n",roomnum);
    }  
    else
    {
        GetJsonDataPro(item,R8001_KEY_IP_ADDR,ip_str);
        if(!strcmp(ip_str, ip))
        {
            cJSON_ReplaceItemInObjectCaseSensitive(item, R8001_KEY_CHECK_RESULT, cJSON_CreateString("ok"));
            //ShellCmdPrintf("setR8001Result ip=%s ok\n",ip_str);
        }
        else
        {
            cJSON_ReplaceItemInObjectCaseSensitive(item, R8001_KEY_CHECK_RESULT, cJSON_CreateString("err"));
            //ShellCmdPrintf("setR8001Result ip=%s err\n",ip_str);
        }
    }  
    

    where = cJSON_CreateObject();
	cJSON_AddStringToObject(where, R8001_KEY_BD_RM_MS, roomnum);
	if(API_TB_Update(R8001Tb, where, item))
		ret = 1;
	cJSON_Delete(where);
	return ret;
}

void r8001Check_save(void)
{
	if(R8001Tb!=NULL)
	{
		SetJsonToFile(R8001CheckPath,R8001Tb);		
	}
}

SearchIpRspData onlineDevice;
void ProgByR8001(void)
{
    int i;
    cJSON *item;
	DeviceInfo devInfo;
    PROG_IP_ADDR_REQ_DATA_T ipinfo;
    API_SearchIpByFilter("9999", TYPE_IM, 10, 0, &onlineDevice);
    //ShellCmdPrintf("ProgByR8001 onlinecnt=%d\n", onlineDevice.deviceCnt);
    if(R8001Tb==NULL)
    {
        R8001Tb = GetJsonFromFile(R8001TablePath);
    }
    
    for(i = 0; i < onlineDevice.deviceCnt; i++)
    {
        memset(&devInfo, 0, sizeof(DeviceInfo));
        memset(&ipinfo, 0, sizeof(PROG_IP_ADDR_REQ_DATA_T));
        if(getR8001Item(onlineDevice.data[i].BD_RM_MS) != NULL)
        {
            setR8001Result(onlineDevice.data[i].BD_RM_MS, NULL);
            continue;//已编写
        }    
        item = getR8001Item(NULL);
        if(item !=NULL)
        {
            GetJsonDataPro(item,R8001_KEY_BD_RM_MS,devInfo.BD_RM_MS);
            GetJsonDataPro(item,R8001_KEY_NAME1,devInfo.name1);
            GetJsonDataPro(item,R8001_KEY_LOCAL,devInfo.Local);
            GetJsonDataPro(item,R8001_KEY_GLOBAL,devInfo.Global);
            GetJsonDataPro(item,R8001_KEY_NAME2,devInfo.name2_utf8);
            GetJsonDataPro(item,R8001_KEY_IP_STATIC,ipinfo.IP_ASSIGNED);
            GetJsonDataPro(item,R8001_KEY_IP_ADDR,ipinfo.IP_ADDR);
            GetJsonDataPro(item,R8001_KEY_IP_MASK,ipinfo.IP_MASK);
            GetJsonDataPro(item,R8001_KEY_IP_GATEWAY,ipinfo.IP_GATEWAY);
            strcpy(devInfo.MFG_SN, onlineDevice.data[i].MFG_SN);
            strcpy(ipinfo.MFG_SN, onlineDevice.data[i].MFG_SN);
            API_ProgInfoByIp(onlineDevice.data[i].Ip, 1, &devInfo);
            if(IxProgIpAddrProcess(onlineDevice.data[i].Ip, ipinfo, 1)==0)
            {
                //ShellCmdPrintf("ProgByR8001 rm=%s ip=%s \n", devInfo.BD_RM_MS, ipinfo.IP_ADDR);
                setR8001Result(devInfo.BD_RM_MS, NULL);
            }
        }        
    }
    //ShellCmdPrintf("ProgByR8001 totalcnt=%d\n", i);
}

void CheckByR8001(void)
{
    int i;
    char ip_str[20];
    memset(&onlineDevice, 0, sizeof(SearchIpRspData));
    API_SearchIpByFilter("9999", TYPE_IM, 10, 256, &onlineDevice);
    for(i = 0; i < onlineDevice.deviceCnt; i++)
    {
        memset( ip_str, 0, 20);	
		ConvertIpInt2IpStr( onlineDevice.data[i].Ip, ip_str );
        //ShellCmdPrintf("CheckByR8001 rm=%s ip=%s\n", onlineDevice.data[i].BD_RM_MS, ip_str);
        setR8001Result(onlineDevice.data[i].BD_RM_MS, ip_str);
    }
    if(onlineDevice.deviceCnt)
    {
        r8001Check_save();
    }
}

void OnlineSearchTest(void)
{
    int i;
    char ip_str[20];
    memset(&onlineDevice, 0, sizeof(SearchIpRspData));
    API_SearchIpByFilter("9999", TYPE_IM, 10, 256, &onlineDevice);
    //ShellCmdPrintf("OnlineSearchTest onlinecnt=%d\n", onlineDevice.deviceCnt);
    for(i = 0; i < onlineDevice.deviceCnt; i++)
    {
        memset( ip_str, 0, 20);	
		ConvertIpInt2IpStr( onlineDevice.data[i].Ip, ip_str );
        //ShellCmdPrintf("OnlineSearchTest rm=%s ip=%s\n", onlineDevice.data[i].BD_RM_MS, ip_str);
    }
}

int EventIXProgSetup(cJSON *cmd)
{
    char* eventName = cJSON_GetStringValue(cJSON_GetObjectItemCaseSensitive(cmd, EVENT_KEY_EventName));
	if(eventName)
	{
		if(!strcmp(eventName, EventIXProg))
		{
			ProgByR8001();
            if(R8001Tb!=NULL)
            {
                SetJsonToFile(R8001TablePath,R8001Tb);		
            }
		}
		else if(!strcmp(eventName, EventIXProgCheck))
        {
            CheckByR8001();
        }
        else if(!strcmp(eventName, EventIXSearch))
        {
            OnlineSearchTest();
        }
	}
	
	return 1;
}
