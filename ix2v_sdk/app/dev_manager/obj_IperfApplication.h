
// lzh_20181025_s

#ifndef _obj_IperfApplication_H_
#define _obj_IperfApplication_H_

#include "utility.h"
#include "cJSON.h"

int EventIperfTestStartCallback(cJSON *cmd);
int API_IperfTestStart(int clientIp, int serverIp);

#endif
// lzh_20181025_e


