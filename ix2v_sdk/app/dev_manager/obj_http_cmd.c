
#include "http_client.h"
#include "obj_http_cmd.h"

#define MAX_RSP_LENGTH  4096


/// @brief http_command_request
/// @param url      - target（IX-RLC） ip addr
/// @param cmd      - get/post command string
/// @param request  - request data（json format）
/// @return         - result（json format）
cJSON* http_command_request(const char*url, const char* cmd, const char* request)
{
    cJSON* json_rsp;
    int  response_len;
    char response[MAX_RSP_LENGTH];
    char urlbuffer[MAX_RSP_LENGTH];
    // unlock
    if( !strcmp(cmd,API_CMD_UNLOCK1_POST) )
    {
        memset( response, 0, MAX_RSP_LENGTH);
        memset( urlbuffer, 0, MAX_RSP_LENGTH);
        response_len = MAX_RSP_LENGTH;
        sprintf( urlbuffer, "%s/%s", url, cmd);
        printf("url:%s request:%s\n",urlbuffer,request);
        if( api_http_post(urlbuffer, request, response, &response_len) == 0 )
        {
            printf("%s\n",response);
            return cJSON_Parse(response);
        }
        else
        {
            printf("[rsp err!]\n");
            return NULL;
        }
    }
    else
        return NULL;
}

/*
eg: 
request:    http 192.168.243.244 set unlock1 {"LOCK_ID":1,"TIMING":6}
response:   unlock 1 set OK / unlock 1 set err
*/ 
int ShellCmd_Http(cJSON *cmd)
{
    char* ptr;
    cJSON* json_dat;
    cJSON* json_rsp;
	char* url       = GetShellCmdInputString(cmd, 1);
	char* opt       = GetShellCmdInputString(cmd, 2);
	char* dir       = GetShellCmdInputString(cmd, 3);
	char* request   = GetShellCmdInputString(cmd, 4);

    if( url == NULL || dir == NULL )
    {
		ShellCmdPrintf("http paras error!\n");
        return 0;
    }
    printf("%s %s %s %s\n",url,opt,dir,request);
	if(!strcmp(dir, "unlock1") || !strcmp(dir, "UNLOCK1"))
	{
        if( !strcmp(opt,"set") || !strcmp(opt,"SET") )
        {
            json_rsp = http_command_request(url,API_CMD_UNLOCK1_POST,request);
            if( json_rsp == NULL )
            {
                ShellCmdPrintf("unlock 1 set ER!\n");
            }
            else
            {
                ShellCmdPrintf("unlock 1 set OK!\n");
                ptr = cJSON_Print(json_rsp);
                if( ptr != NULL )
                {
                    ShellCmdPrintf("%s\n",ptr);
                    free(ptr);
                }
                cJSON_Delete(json_rsp);
            }
        }
        else
        {
            json_rsp = http_command_request(url,API_CMD_UNLOCK1_GET,NULL);
            if( json_rsp == NULL )
            {
                ShellCmdPrintf("unlock 1 set ER!\n");
            }
            else
            {
                ShellCmdPrintf("unlock 1 set OK!\n");
                ptr = cJSON_Print(json_rsp);
                if( ptr != NULL )
                {
                    ShellCmdPrintf("%s\n",ptr);
                    free(ptr);
                }
                cJSON_Delete(json_rsp);
            }
        }
	}
	else
	{
		ShellCmdPrintf("http cmd error!\n");
	}
	return 1;
}

