/**
 ******************************************************************************
 * @file    obj_IXG_Manage.c
 * @author  czb
 * @version V00.01.00
 * @date    2023.02.15
 * @brief
 ******************************************************************************
 * @attention
 *
 *
 * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
 ******************************************************************************
 */
#include "OSTIME.h"
#include "obj_IoInterface.h"
#include <pthread.h>
#include "cJSON.h"
#include "utility.h"
#include "obj_PublicUnicastCmd.h"
#include "task_Event.h"
#include "define_string.h"
#include "define_file.h"
#include "vtk_udp_stack_device_update.h"
#include "obj_PublicInformation.h"
#include "obj_IXG_Manage.h"
#include "obj_TableSurver.h"
#include "obj_IXS_Rules.h"
#include "obj_GetDeviceTypeAndArea.h"

static pthread_mutex_t search_IXG_lock = PTHREAD_MUTEX_INITIALIZER;

int API_IXG_SearchAdd(void)
{
	cJSON *ipTb;
	cJSON *element;
	int ret = 0;

	pthread_mutex_lock(&search_IXG_lock);
	ipTb = cJSON_CreateArray();
	IXS_Search(API_Para_Read_String2(IX_NT), NULL, GetSysVerInfo_bd(), "IXG", ipTb);
	pthread_mutex_unlock(&search_IXG_lock);

	//获取DXG_ID
	cJSON_ArrayForEach(element, ipTb)
	{
		int ip = inet_addr(GetEventItemString(element, IX2V_IP_ADDR));
		cJSON *ixgId = API_GetRemotePb(ip, PB_IXG_ADDR_INT_DATA);
		if(cJSON_IsNumber(ixgId))
		{
			cJSON_AddNumberToObject(element, IX2V_IXG_ID, ixgId->valueint);
		}
		cJSON_Delete(ixgId);
	}

	//根据IXG_ID按升序排序
	//SortJsonTable(ipTb, IX2V_IXG_ID, 0);

	cJSON_ArrayForEach(element, ipTb)
	{
		//没有IXG_ID的不允许添加
		if(cJSON_GetObjectItemCaseSensitive(element, IX2V_IXG_ID))
		{
			IXG_RecordAddGW_ID(element);
			if(API_TB_AddByName(TB_NAME_IXG_LIST, element))
			{
				ret++;
			}
			else
			{
				cJSON *Where = cJSON_CreateObject();
				cJSON_AddItemToObject(Where, IX2V_MFG_SN, cJSON_Duplicate(cJSON_GetObjectItemCaseSensitive(element, IX2V_MFG_SN), 1));
				API_TB_UpdateByName(TB_NAME_IXG_LIST, Where, element);
				cJSON_Delete(Where);
			}
		}
	}

	cJSON_ArrayForEach(element, ipTb)
	{
		//没有IXG_ID的不允许添加
		if(cJSON_GetObjectItemCaseSensitive(element, IX2V_IXG_ID))
		{
			//刷新DXG分机列表
			API_UpdateIxgDtImList(inet_addr(GetEventItemString(element, IX2V_IP_ADDR)));
		}
	}

	cJSON_Delete(ipTb);

	API_Event_By_Name(Event_IXGListUpdate);

	return ret;
}

int API_UpdateIxgList2(void)
{
	int ret;
	cJSON *ipTb;
	cJSON *element;
	API_TB_DeleteByName(TB_NAME_IXG_LIST, NULL);
	
	pthread_mutex_lock(&search_IXG_lock);
	ipTb = cJSON_CreateArray();
	IXS_Search(API_Para_Read_String2(IX_NT), NULL, GetSysVerInfo_bd(), "IXG", ipTb);
	pthread_mutex_unlock(&search_IXG_lock);

	//获取DXG_ID
	cJSON_ArrayForEach(element, ipTb)
	{
		int ip = inet_addr(GetEventItemString(element, IX2V_IP_ADDR));
		cJSON *ixgId = API_GetRemotePb(ip, PB_IXG_ADDR_INT_DATA);
		if(cJSON_IsNumber(ixgId))
		{
			cJSON_AddNumberToObject(element, IX2V_IXG_ID, ixgId->valueint);
		}
		cJSON_Delete(ixgId);
	}

	cJSON_ArrayForEach(element, ipTb)
	{
		//没有IXG_ID的不允许添加
		if(cJSON_GetObjectItemCaseSensitive(element, IX2V_IXG_ID))
		{
			IXG_RecordAddGW_ID(element);
			if(API_TB_AddByName(TB_NAME_IXG_LIST, element))
			{
				ret++;
			}
			else
			{
				cJSON *Where = cJSON_CreateObject();
				cJSON_AddItemToObject(Where, IX2V_MFG_SN, cJSON_Duplicate(cJSON_GetObjectItemCaseSensitive(element, IX2V_MFG_SN), 1));
				API_TB_UpdateByName(TB_NAME_IXG_LIST, Where, element);
				cJSON_Delete(Where);
			}
		}
	}
	cJSON_Delete(ipTb);

	API_Event_By_Name(Event_IXGListUpdate);
	
	return ret;
}

int API_IXG_CheckIn(void)
{
	int ret = 0;
	sleep(2);
	cJSON* ipTb = cJSON_CreateArray();
	//bprintf("11111\n");
	if(IXS_Search(API_Para_Read_String2(IX_NT), NULL, GetSysVerInfo_bd(), "DS", ipTb) > 0)
	{
		//bprintf("11111\n");
		cJSON* element;
		cJSON* record = cJSON_CreateObject();		
		cJSON_AddStringToObject(record, IX2V_EventName, EventIXGCheckIn);
		cJSON_AddNumberToObject(record, IX2V_IXG_ID, GetIXGDIP());
		cJSON_AddStringToObject(record, IX2V_IP_ADDR, GetSysVerInfo_IP_by_device(API_Para_Read_String2(IX_NT)));
		cJSON_AddStringToObject(record, IX2V_IX_ADDR, GetSysVerInfo_BdRmMs());
		cJSON_AddStringToObject(record, IX2V_MFG_SN, GetSysVerInfo_Sn());
		cJSON_AddStringToObject(record, IX2V_IX_TYPE, DeviceTypeToString(GetSysVerInfo_MyDeviceType()));
		cJSON_AddStringToObject(record, IX2V_UpTime, API_PublicInfo_Read_String(PB_UP_TIME));
		cJSON_AddStringToObject(record, IX2V_Platform, IX2V);
		printf_json(ipTb, "222222");
		cJSON_ArrayForEach(element, ipTb)
		{
			//bprintf("11111%s\n",GetEventItemString(element, IX2V_IP_ADDR));
			if(API_XD_EventJson(inet_addr(GetEventItemString(element, IX2V_IP_ADDR)), record))
			{
				ret++;
			}
		}
		cJSON_Delete(record);
	}
	cJSON_Delete(ipTb);

	return ret;
}

int API_UpdateIxgList(void)
{
	int ret;
	API_TB_DeleteByName(TB_NAME_IXG_LIST, NULL);
	ret = API_IXG_SearchAdd();
	return ret;
}

int API_UpdateIxgDtImList(int ip)
{
	int ret;
	cJSON* event = cJSON_CreateObject();
	cJSON* dtIm; 
	cJSON* imRange; 
	int i;
	char temp[8];

	cJSON_AddStringToObject(event, IX2V_EventName, EventUpdateDT_IM_List);
	dtIm = cJSON_AddObjectToObject(event, IX2V_DT_IM);
	cJSON_AddStringToObject(event, IX2V_IP_ADDR, GetSysVerInfo_IP_by_device(GetNetDeviceNameByTargetIp(ip)));
	#if 0		
	for(i = 0; i <= 8; i++)
	{
		snprintf(temp, 8, "BDU_%d", i);
		imRange = cJSON_AddArrayToObject(dtIm, i == 0 ? IX2V_DT_IM : temp);
		cJSON_AddItemToArray(imRange, cJSON_CreateNumber(1));
		cJSON_AddItemToArray(imRange, cJSON_CreateNumber(32));
	}
	cJSON_AddNumberToObject(event, IX2V_TIME, 600);	//10分钟刷新一次
	#else
	//不搜素BDU，不定时刷新
	imRange = cJSON_AddArrayToObject(dtIm, IX2V_DT_IM);
	cJSON_AddItemToArray(imRange, cJSON_CreateNumber(1));
	cJSON_AddItemToArray(imRange, cJSON_CreateNumber(32));
	#endif

	ret = API_XD_EventJson(ip, event);

	cJSON_Delete(event);

	return ret;
}
int API_CheckIXGIdRepeat(cJSON *result)
{
	int ret=0;
	int self_id=0;
	cJSON *ipTb = cJSON_CreateArray();
	char *ipString;
	int ip;
	cJSON *ixgId;
	cJSON *element;
	cJSON *cmp_element;
	if(GetSysVerInfo_MyDeviceType()==TYPE_IXG)
		self_id=API_PublicInfo_Read_Int(PB_IXG_ADDR_INT_DATA);
	pthread_mutex_lock(&search_IXG_lock);
	ipTb = cJSON_CreateArray();
	IXS_Search(API_Para_Read_String2(IX_NT), NULL, GetSysVerInfo_bd(), "IXG", ipTb);
	pthread_mutex_unlock(&search_IXG_lock);
	int size=cJSON_GetArraySize(ipTb);
	if(size==0)
	{
		cJSON_Delete(ipTb);
		return ret;
	}
	if(size==1&&self_id==0)
	{
		cJSON_Delete(ipTb);
		return ret;
	}
	cJSON_ArrayForEach(element, ipTb)
	{
		ipString = GetEventItemString(element, IX2V_IP_ADDR);
		ip = inet_addr(ipString);

		ixgId = API_GetRemotePb(ip, PB_IXG_ADDR_INT_DATA);
		if(ixgId)
		{
			cJSON_AddNumberToObject(element, IX2V_IXG_ID, ixgId->valueint);
		}
		cJSON_Delete(ixgId);
	}
	SortTableView(ipTb,IX2V_IXG_ID,0);
	
	int i;
	int ixg_id=0;
	int repeat_id=0;
	for(i=0;i<(size-1);i++)
	{
		element=cJSON_GetArrayItem(ipTb,i);
		cmp_element=cJSON_GetArrayItem(ipTb,i+1);
		if((ixg_id=GetEventItemInt(element,IX2V_IXG_ID))==GetEventItemInt(cmp_element,IX2V_IXG_ID))
		{
			if(ixg_id!=repeat_id)
			{
				ret++;
				if(result)
				{
					repeat_id=ixg_id;
					cJSON_AddItemToArray(result,cJSON_CreateNumber(ixg_id));
				}
			}
		}
		else if(ixg_id!=repeat_id&&ixg_id==self_id)
		{
			ret++;
			if(result)
			{
				repeat_id=ixg_id;
				cJSON_AddItemToArray(result,cJSON_CreateNumber(ixg_id));
			}
		}
	}
	cJSON_Delete(ipTb);
	return ret;
}
/*********************************************************************************************************
**  End Of File
*********************************************************************************************************/
