#include <stdio.h>   
#include <stdlib.h>   
#include <string.h>   
#include <curl/curl.h>  

struct memory_size
{
    char *memory;
    size_t size;
};
static size_t store_callback(void *contents, size_t size, size_t nmemb, void *userp)
{
    size_t realsize = size * nmemb;
    struct memory_size *mem = (struct memory_size *)userp;
    char *ptr = realloc(mem->memory, mem->size + realsize + 1); 
    if( ptr == NULL )  { printf("not enough memory\n"); return 0;}
    mem->memory = ptr;
    memcpy(&(mem->memory[mem->size]), contents, realsize);
    mem->size += realsize;
    mem->memory[mem->size] = 0;
    return realsize;
}

int http_request_method(const char* url, const char* method, const char* request, struct memory_size* response)
{
    CURLcode res;
    long response_code;
    CURL* curl = curl_easy_init();
    if (curl) 
    {
        struct curl_slist* header_list = NULL;
        header_list = curl_slist_append(header_list, "Content-Type:application/json");
        curl_easy_setopt(curl, CURLOPT_HTTPHEADER, header_list);
        curl_easy_setopt(curl, CURLOPT_HEADER, 0);
        curl_easy_setopt(curl, CURLOPT_URL, url);
        if (method == "POST") 
        {
            curl_easy_setopt(curl, CURLOPT_POSTFIELDS, request);
        }
        else if (method == "PUT") 
        {
            curl_easy_setopt(curl, CURLOPT_POSTFIELDS, request);
            curl_easy_setopt(curl, CURLOPT_CUSTOMREQUEST, method);
        }
        else if (method == "DELETE") 
        {
            curl_easy_setopt(curl, CURLOPT_CUSTOMREQUEST, method);
        }
        curl_easy_setopt(curl, CURLOPT_VERBOSE, 0);
        curl_easy_setopt(curl, CURLOPT_READFUNCTION, NULL);
        curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, store_callback);
        curl_easy_setopt(curl, CURLOPT_WRITEDATA, (void*)response);
	    curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0);
	    curl_easy_setopt(curl, CURLOPT_SSL_VERIFYHOST, 0); 
        curl_easy_setopt(curl, CURLOPT_NOSIGNAL, 1);
        curl_easy_setopt(curl, CURLOPT_CONNECTTIMEOUT, 6);
        curl_easy_setopt(curl, CURLOPT_TIMEOUT, 10);

        res = curl_easy_perform(curl);
        if (res != CURLE_OK) 
        {
            printf("curl_easy_perform() failed: %s\n", curl_easy_strerror(res));
            curl_easy_cleanup(curl);
            return -1;
        }
        else
        {
            curl_easy_getinfo(curl, CURLINFO_RESPONSE_CODE, &response_code);
            printf("HTTP Response Code: %ld\n", response_code);            
            curl_easy_cleanup(curl);
            return 0;
        }
    }
    else
    {
        return -1;
    }
}

int api_http_get(const char* url, char* rsp_buf, int* rsp_len)
{
    int maxlen = 0;
    int ret = -1;
    struct memory_size rsp_info;
    rsp_info.memory = malloc(1);
    rsp_info.size = 0;
    if( http_request_method(url, "GET", NULL, &rsp_info) == 0 )
    {
        ret = 0;
        maxlen = *rsp_len;
        if( rsp_info.size < maxlen ) maxlen = rsp_info.size;
        memcpy( rsp_buf, rsp_info.memory, maxlen);
        *rsp_len = maxlen;
    }
    free(rsp_info.memory);
    return ret;
}

int api_http_post(const char* url, const char* request, char* rsp_buf, int* rsp_len)
{
    int maxlen;
    int ret = -1;
    struct memory_size rsp_info;
    rsp_info.memory = malloc(1);
    rsp_info.size = 0;
    if( http_request_method(url, "POST", request, &rsp_info) == 0 )
    {
        ret = 0;
        maxlen = *rsp_len;
        if( rsp_info.size < maxlen ) maxlen = rsp_info.size;
        memcpy( rsp_buf, rsp_info.memory, maxlen);
        *rsp_len = maxlen;
    }
    free(rsp_info.memory);
    return ret;
}

int api_http_put(const char* url, const char* request, char* rsp_buf, int* rsp_len)
{
    int maxlen;
    int ret = -1;
    struct memory_size rsp_info;
    rsp_info.memory = malloc(1);
    rsp_info.size = 0;
    if( http_request_method(url, "PUT", request, &rsp_info) == 0 )
    {
        ret = 0;
        maxlen = *rsp_len;
        if( rsp_info.size < maxlen ) maxlen = rsp_info.size;
        memcpy( rsp_buf, rsp_info.memory, maxlen);
        *rsp_len = maxlen;
    }
    free(rsp_info.memory);
    return ret;
}

int api_http_delete(char* url, char* rsp_buf, int* rsp_len)
{
    int maxlen;
    int ret = -1;
    struct memory_size rsp_info;
    rsp_info.memory = malloc(1);
    rsp_info.size = 0;
    if( http_request_method(url, "DELETE", NULL, &rsp_info) == 0 )
    {
        ret = 0;
        maxlen = *rsp_len;
        if( rsp_info.size < maxlen ) maxlen = rsp_info.size;
        memcpy( rsp_buf, rsp_info.memory, maxlen);
        *rsp_len = maxlen;
    }
    free(rsp_info.memory);
    return ret;
}
