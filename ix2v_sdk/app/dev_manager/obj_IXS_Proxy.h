/**
 ******************************************************************************
 * @file    obj_IXS_Proxy.h
 * @author  czb
 * @version V00.01.00
 * @date    2023.02.15
 * @brief
 ******************************************************************************
 * @attention
 *
 *
 * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
 ******************************************************************************
 */

#ifndef _obj_IXS_Proxy_H
#define _obj_IXS_Proxy_H

#include "cJSON.h"
#include "utility.h"

typedef enum
{
	SearchWaitNet,
	SearchServer,
	SearchClient,
} IXS_Proxy_Type_e;

typedef enum
{
	R8001,
	Search,
	R8001AndSearch,
} IXS_Proxy_Tb_Select_e;

typedef struct
{
	pthread_mutex_t lock;
	IXS_Proxy_Type_e serverType;		//服务类型
	int searchEnable;					//
	int serverIp;						//服务端IP
	int ixsProxyHbCnt;					//服务器心跳计数
	int initFlag;						//初始化标记
	cJSON *devTable;					//设备表
	int devCnt;							//设备数量
	int newDevCnt;						//设备数量
	char Md5[33];						//当前设备表的MD5码
	char newMd5[33];					//服务端存在的设备表的MD5码
	char lastTime[50];					//最后搜索列表时间
	char timeSinceLastInform;			//从最后收到心跳开始计时到现在的时间，单位秒
} IXS_Proxy_t;


//IXS代理初始化
void IXS_ProxyInit(void);

//重新初始化IXS代理
void IXS_ProxyReinit(void);

//IXS网络连接成功回调函数
int IXS_ProxyNetConnectedCallback(cJSON *cmd);

//接收到服务器通知
int IXS_ProxyReceiveUpdateTbInform(int ip, char* data);

//请求更新列表事件回调函数
int IXS_ProxyReqUpdateTb_CallBack(cJSON *event);

//获取搜索设备列表
static cJSON* IXS_ProxyGetSearchDevTb(void);

//获取设备列表
static cJSON* IXS_ProxyGetDevTb(IXS_Proxy_Tb_Select_e tb);
//获取设备列表
int IXS_ProxyGetTb(cJSON** result, pthread_mutex_t* lock, IXS_Proxy_Tb_Select_e tb);

//获取搜索状态
cJSON* IXS_ProxyGetState(void);

//检测IXS列表是否改变，如果改变则取列表
void IXS_ProxyCheckIfListChange(void);
#endif

/*********************************************************************************************************
**  End Of File
*********************************************************************************************************/