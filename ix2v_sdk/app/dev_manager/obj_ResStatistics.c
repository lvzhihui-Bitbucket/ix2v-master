/**
  ******************************************************************************
  * @file    obj_ResStatistics.c
  * @author  cao
  * @version V00.01.00
  * @date    2023.11.29
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 

#include <sys/sysinfo.h>
#include "obj_PublicInformation.h"
#include "obj_TableSurver.h"
#include "utility.h"

void TableStatisticsInit(const char* tbName)
{
    if(tbName)
    {
        char temp[100];
        char recordName[100];
        char modifiedName[100];
        char descName[100];

        snprintf(recordName, 100, "%s_RECORDS", tbName);
        snprintf(modifiedName, 100, "%s_LAST_MODIFIED", tbName);
        snprintf(descName, 100, "%s_TB_DESC", tbName);

        int resRecords = API_TB_CountByName(tbName, NULL);
        cJSON* desc = API_TB_GetDescByName(tbName);
        desc = (cJSON_IsObject(desc) ? cJSON_Duplicate(desc, 1) : cJSON_CreateObject());

        if((resRecords != GetEventItemInt(desc, recordName)) || (!strcmp(GetEventItemString(desc, modifiedName), "")))
        {
            cJSON_ReplaceOrAddItemInObjectCaseSensitive(desc, recordName, cJSON_CreateNumber(resRecords));
            cJSON_ReplaceOrAddItemInObjectCaseSensitive(desc, modifiedName, cJSON_CreateString(GetCurrentTime(temp, 100, "%Y-%m-%d %H:%M:%S")));

            char* descString = GetEventItemString(desc, descName);
            if(!strcmp(descString, ""))
            {
                snprintf(temp,100, "List %d", resRecords);
                cJSON_ReplaceOrAddItemInObjectCaseSensitive(desc, modifiedName, cJSON_CreateString(temp));
            }

            API_TB_ReplaceDescByName(tbName, desc);
        }
        cJSON_Delete(desc);
    }
}

void API_TableModificationInform(const char* tbName, const char* descString)
{
    if(tbName)
    {
        char temp[100];
        char recordName[100];
        char modifiedName[100];
        char descName[100];
        snprintf(recordName, 100, "%s_RECORDS", tbName);
        snprintf(modifiedName, 100, "%s_LAST_MODIFIED", tbName);
        snprintf(descName, 100, "%s_TB_DESC", tbName);

        int resRecords = API_TB_CountByName(tbName, NULL);
        cJSON* desc = API_TB_GetDescByName(tbName);
        desc = (cJSON_IsObject(desc) ? cJSON_Duplicate(desc, 1) : cJSON_CreateObject());
        cJSON_ReplaceOrAddItemInObjectCaseSensitive(desc, recordName, cJSON_CreateNumber(resRecords));
        cJSON_ReplaceOrAddItemInObjectCaseSensitive(desc, modifiedName, cJSON_CreateString(GetCurrentTime(temp, 100, "%Y-%m-%d %H:%M:%S")));
        if(descString)
        {
            cJSON_ReplaceOrAddItemInObjectCaseSensitive(desc, descName, cJSON_CreateString(descString));
        }
        API_TB_ReplaceDescByName(tbName, desc);
        cJSON_Delete(desc);
    }
}

void SatHybridCheckStatisticsInit(void)
{
    char temp[100];
    int checkRecords = API_TB_CountByName(TB_NAME_HYBRID_SAT_CHECK, NULL);
    cJSON* desc = API_TB_GetDescByName(TB_NAME_HYBRID_SAT_CHECK);
    desc = (cJSON_IsObject(desc) ? cJSON_Duplicate(desc, 1) : cJSON_CreateObject());

    if((checkRecords == 0) && (!strcmp(GetEventItemString(desc, PB_SAT_DH_HYBRID_CHECK_TIME), "")))
    {
        cJSON_ReplaceOrAddItemInObjectCaseSensitive(desc, PB_SAT_DH_HYBRID_CHECK_TIME, cJSON_CreateString(GetCurrentTime(temp, 100, "%Y-%m-%d %H:%M:%S")));
        cJSON_ReplaceOrAddItemInObjectCaseSensitive(desc, PB_SAT_DH_HYBRID_CHECK_ONLINE, cJSON_CreateNumber(0));
        cJSON_ReplaceOrAddItemInObjectCaseSensitive(desc, PB_SAT_DH_HYBRID_CHECK_OFFLINE, cJSON_CreateNumber(0));
        cJSON_ReplaceOrAddItemInObjectCaseSensitive(desc, PB_SAT_DH_HYBRID_CHECK_ADDED, cJSON_CreateNumber(0));
        API_TB_ReplaceDescByName(TB_NAME_HYBRID_SAT_CHECK, desc);
    }
    cJSON_Delete(desc);
}


void API_SatHybridCheckModificationInform(void)
{
    char temp[100];
	int checkRecords;
    cJSON* desc = API_TB_GetDescByName(TB_NAME_HYBRID_SAT_CHECK);
    desc = (cJSON_IsObject(desc) ? cJSON_Duplicate(desc, 1) : cJSON_CreateObject());
    cJSON_ReplaceOrAddItemInObjectCaseSensitive(desc, PB_SAT_DH_HYBRID_CHECK_TIME, cJSON_CreateString(GetCurrentTime(temp, 100, "%Y-%m-%d %H:%M:%S")));
    cJSON* where = cJSON_CreateObject();
    cJSON_AddStringToObject(where, "STATE", "ONLINE");
    checkRecords = API_TB_CountByName(TB_NAME_HYBRID_SAT_CHECK, where);
    cJSON_ReplaceOrAddItemInObjectCaseSensitive(desc, PB_SAT_DH_HYBRID_CHECK_ONLINE, cJSON_CreateNumber(checkRecords));

    cJSON_ReplaceItemInObjectCaseSensitive(where, "STATE", cJSON_CreateString("OFFLINE"));
    checkRecords = API_TB_CountByName(TB_NAME_HYBRID_SAT_CHECK, where);
    cJSON_ReplaceOrAddItemInObjectCaseSensitive(desc, PB_SAT_DH_HYBRID_CHECK_OFFLINE, cJSON_CreateNumber(checkRecords));

    cJSON_ReplaceItemInObjectCaseSensitive(where, "STATE", cJSON_CreateString("ADDED"));
    checkRecords = API_TB_CountByName(TB_NAME_HYBRID_SAT_CHECK, where);
    cJSON_ReplaceOrAddItemInObjectCaseSensitive(desc, PB_SAT_DH_HYBRID_CHECK_ADDED, cJSON_CreateNumber(checkRecords));

    API_TB_ReplaceDescByName(TB_NAME_HYBRID_SAT_CHECK, desc);
    cJSON_Delete(desc);
}

void AptMatCheckStatisticsInit(void)
{
    char temp[100];
    int checkRecords = API_TB_CountByName(TB_NAME_APT_MAT_CHECK, NULL);
    cJSON* desc = API_TB_GetDescByName(TB_NAME_APT_MAT_CHECK);
    desc = (cJSON_IsObject(desc) ? cJSON_Duplicate(desc, 1) : cJSON_CreateObject());

    if((checkRecords == 0) && (!strcmp(GetEventItemString(desc, PB_APT_MAT_CHECK_TIME), "")))
    {
        cJSON_ReplaceOrAddItemInObjectCaseSensitive(desc, PB_APT_MAT_CHECK_TIME, cJSON_CreateString(GetCurrentTime(temp, 100, "%Y-%m-%d %H:%M:%S")));
        cJSON_ReplaceOrAddItemInObjectCaseSensitive(desc, PB_APT_MAT_CHECK_ONLINE, cJSON_CreateNumber(0));
        cJSON_ReplaceOrAddItemInObjectCaseSensitive(desc, PB_APT_MAT_CHECK_OFFLINE, cJSON_CreateNumber(0));
        cJSON_ReplaceOrAddItemInObjectCaseSensitive(desc, PB_APT_MAT_CHECK_ADDED, cJSON_CreateNumber(0));
        API_TB_ReplaceDescByName(TB_NAME_APT_MAT_CHECK, desc);
    }
    cJSON_Delete(desc);
}


void API_AptMatCheckModificationInform(void)
{
    char temp[100];
	int checkRecords;
    cJSON* desc = API_TB_GetDescByName(TB_NAME_APT_MAT_CHECK);
    desc = (cJSON_IsObject(desc) ? cJSON_Duplicate(desc, 1) : cJSON_CreateObject());
    cJSON_ReplaceOrAddItemInObjectCaseSensitive(desc, PB_APT_MAT_CHECK_TIME, cJSON_CreateString(GetCurrentTime(temp, 100, "%Y-%m-%d %H:%M:%S")));
    cJSON* where = cJSON_CreateObject();
    cJSON_AddStringToObject(where, "STATE", "ONLINE");
    checkRecords = API_TB_CountByName(TB_NAME_APT_MAT_CHECK, where);
    cJSON_ReplaceOrAddItemInObjectCaseSensitive(desc, PB_APT_MAT_CHECK_ONLINE, cJSON_CreateNumber(checkRecords));

    cJSON_ReplaceItemInObjectCaseSensitive(where, "STATE", cJSON_CreateString("OFFLINE"));
    checkRecords = API_TB_CountByName(TB_NAME_APT_MAT_CHECK, where);
    cJSON_ReplaceOrAddItemInObjectCaseSensitive(desc, PB_APT_MAT_CHECK_OFFLINE, cJSON_CreateNumber(checkRecords));

    cJSON_ReplaceItemInObjectCaseSensitive(where, "STATE", cJSON_CreateString("ADDED"));
    checkRecords = API_TB_CountByName(TB_NAME_APT_MAT_CHECK, where);
    cJSON_ReplaceOrAddItemInObjectCaseSensitive(desc, PB_APT_MAT_CHECK_ADDED, cJSON_CreateNumber(checkRecords));

    API_TB_ReplaceDescByName(TB_NAME_APT_MAT_CHECK, desc);
    cJSON_Delete(desc);
}