
/**
  ******************************************************************************
  * @file    obj_Certificate.c
  * @author  lzh
  * @version V00.01.00
  * @date    2018.10.27
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 


#include <sys/sysinfo.h>
#include "define_file.h"
#include "cJSON.h"
#include "define_string.h"
#include "obj_TableSurver.h"


int API_CreatePrivatePasswordTable(const char* defaultPassword)
{
	int ret = 0;
	cJSON* View = cJSON_CreateArray();
	cJSON* record;
	cJSON* element;
	cJSON* local;
	int retCnt = 0;
	
	API_TB_DeleteByName(TB_NAME_PRIVATE_PWD_BY_L_NBR, NULL);

	if(API_TB_SelectBySortByName(TB_NAME_R8001, View, NULL, 0))
	{
		cJSON_ArrayForEach(element, View)
		{
			local = cJSON_GetObjectItemCaseSensitive(element, IX2V_L_NBR);
			if(cJSON_IsString(local))
			{
				record = cJSON_CreateObject();
				cJSON_AddItemToObject(record, IX2V_IX_ADDR, cJSON_Duplicate(cJSON_GetObjectItemCaseSensitive(element, IX2V_IX_ADDR), 1));
				cJSON_AddItemToObject(record, IX2V_L_NBR, cJSON_Duplicate(local, 1));
				cJSON_AddStringToObject(record, DM_KEY_CODE, defaultPassword ? defaultPassword : "");
				ret = API_TB_AddByName(TB_NAME_PRIVATE_PWD_BY_L_NBR, record);
				if(ret)
				{
					retCnt = ret;
				}
				cJSON_Delete(record);
			}
		}
		if(retCnt)
		{
			API_TB_SaveByName(TB_NAME_PRIVATE_PWD_BY_L_NBR);
		}
	}
	cJSON_Delete(View);

	return retCnt;
}