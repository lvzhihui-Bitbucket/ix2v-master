/**
  ******************************************************************************
  * @file    obj_RLC_Unlock.c
  * @author  zxj
  * @version V00.01.00
  * @date    2012.08.22
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2012 V-Tec</center></h2>
  ******************************************************************************
  */ 

#include "obj_GetIpByNumber.h"
#include "cJSON.h"

#pragma pack(1)
typedef struct
{
	char op_code;
	char json_buff[200];
}RLCUnlockReqMsg_t;
#pragma pack()

#define UnlockStatus				"UnlockStatus"
#define UnlockPara				"UnlockPara"
#define UnlockPara_Mode			"Mode"
#define UnlockPara_Channel		"CH"
#define UnlockPara_Timing			"Timing"
#define UnlockPara_Retrig			"Retrig"
#define UnlockPara_ProtecionTiming	"ProtecionTiming"
#define UnlockPara_RLCAddr		"RLCAddr"
#define UnlockPara_DsAddr			"DSAddr"

int Send_RLCUnlockReq(char *target_num,int ch,int mode,int timing,int protection_timing,int retrig)
{
	GetIpRspData data;
	RLCUnlockReqMsg_t send_msg;
	send_msg.op_code= 0;
	data.cnt = 0;
	API_GetIpNumberFromNet(target_num, NULL,NULL, 1, 1,&data);
	if(data.cnt==0)
	{
		return -1;
	}
	cJSON *para_obj;
	cJSON *unlock_para;
	char *para_str;
	para_obj = cJSON_CreateObject();
	if(para_obj==NULL)
		return -1;
	{
		unlock_para=cJSON_AddObjectToObject(para_obj,UnlockPara);
		if(unlock_para==NULL)
			return -1;
		cJSON_AddNumberToObject(unlock_para,UnlockPara_Mode,mode);
		cJSON_AddNumberToObject(unlock_para,UnlockPara_Channel,ch);
		cJSON_AddNumberToObject(unlock_para,UnlockPara_Timing,timing);
		cJSON_AddNumberToObject(unlock_para,UnlockPara_ProtecionTiming,protection_timing);
		cJSON_AddNumberToObject(unlock_para,UnlockPara_Retrig,retrig);
		cJSON_AddStringToObject(unlock_para,UnlockPara_RLCAddr,target_num);
		cJSON_AddStringToObject(unlock_para,UnlockPara_DsAddr,GetSysVerInfo_BdRmMs());
		
		para_str=cJSON_Print(para_obj);
		if(para_str==NULL)
		{
			cJSON_Delete(para_obj);
			return -1;
		}
		strncpy(send_msg.json_buff,para_str,199);
		free(para_str);
		cJSON_Delete(para_obj);

		return Send_RLCUnlock_VtkUnicast(data.Ip[0],(char*)&send_msg,strlen(send_msg.json_buff)+2);
	}
}

int Send_RLCUnlockInform(char *ds_num,int ch)
{
	GetIpRspData data;
	RLCUnlockReqMsg_t send_msg;
	send_msg.op_code= 0;
	data.cnt = 1;
	API_GetIpNumberFromNet(ds_num, NULL,NULL, 2, 1,&data);
	if(data.cnt==0)
	{
		return -1;
	}
	cJSON *para_obj;
	cJSON *unlock_para;
	char *para_str;
	para_obj = cJSON_CreateObject();
	if(para_obj==NULL)
		return -1;
	
	cJSON_AddNumberToObject(para_obj,UnlockStatus,1);
	unlock_para=cJSON_AddObjectToObject(para_obj,UnlockPara);
	if(unlock_para==NULL)
		return -1;
	
	cJSON_AddStringToObject(unlock_para,UnlockPara_RLCAddr,GetSysVerInfo_BdRmMs());
	cJSON_AddStringToObject(unlock_para,UnlockPara_DsAddr,ds_num);
	cJSON_AddNumberToObject(unlock_para,UnlockPara_Channel,0);
	
	para_str=cJSON_Print(para_obj);
	if(para_str==NULL)
	{
		cJSON_Delete(para_obj);
		return -1;
	}
	strncpy(send_msg.json_buff,para_str,199);
	free(para_str);
	cJSON_Delete(para_obj);

	return Send_RLCUnlock_VtkUnicast(data.Ip[0],(char*)&send_msg,strlen(send_msg.json_buff)+2);
	
}

/*********************************************************************************************************
**  End Of File
*********************************************************************************************************/
