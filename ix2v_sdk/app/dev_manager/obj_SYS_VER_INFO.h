/**
  ******************************************************************************
  * @file    obj_SYS_VER_INFO.h
  * @author  cao
  * @version V00.01.00
  * @date    2016.5.31
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 

#ifndef _obj_SYS_VER_INFO_H
#define _obj_SYS_VER_INFO_H

#define NET_WLAN0						"wlan0"
#define NET_ETH0						"eth0"   
#define NET_USB0						"usb0"

// Define Object Property-------------------------------------------------------

int GetStm8lSwVerFromStm8l(void);

const char* GetNetDeviceNameByTargetIp(int targetIp);

const char* GetSysVerInfo_IP(void);

const char* GetSysVerInfo_IP_by_device(char* net_device_name);

const char* GetSysVerInfo_mask_by_device(char* net_device_name);
const char* GetSysVerInfo_gateway_by_device(char* net_device_name);

const char* GetSysVerInfo_mac_by_device(char* net_device_name);

const char* GetSysVerInfo_BdRmMs(void);

const char* GetSysVerInfo_bd(void);
const char* GetSysVerInfo_rm(void);
const char* GetSysVerInfo_GlobalNum(void);
const char* GetSysVerInfo_LocalNum(void);
const char* GetSysVerInfo_name(void);
const char* GetSysVerInfo_Sn(void);
int GetSysVerInfo_MyDeviceType(void);
const char* DeviceTypeToString(int type);

long GetSystemBootTime(void);
int GetSysVerInfo_WlanEn(void);
int GetNetMode(void);

void SetSysVerInfo_BdRmMs(const char* bd_rm_ms);
void SetSysVerInfo_GlobalNum(const char* number);
void SetSysVerInfo_LocalNum(const char* number);
void SetSysVerInfo_name(const char* name);

// Define Object Function - Private---------------------------------------------
char* getDateFromMacro( const char* timeIn, char *timeOut);
char* getTimeFromMacro( const char* timeIn, char *timeOut);


#endif


