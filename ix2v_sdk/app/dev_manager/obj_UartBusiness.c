/**
  ******************************************************************************
  * @file    obj_UartBusiness.c
  * @author  czb
  * @version V00.01.00
  * @date    2022.06.15
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 

#include <sys/wait.h>
#include "cJSON.h"
#include "vdp_uart.h"
#include "obj_UartBusiness.h"
#include "task_survey.h"
#include <time.h>
#include "elog.h"

const char* DT_CMD_TYPE_TB[] = {
	"OLD_CMD",
	"NEW_CMD",
};

const char* DT_ADDR_TB[] = {
	"REV",
	"REV",
	"REV",
	"REV",
	"REV",
	"REV",
	"REV",
	"REV",
	"REV",
	"REV",
	"REV",
	"REV",
	"BDU_GATEWAY1",
	"BDU_GATEWAY2",
	"BDU_GATEWAY3",
	"BDU_GATEWAY4",
	"BDU_GATEWAY5",
	"BDU_GATEWAY6",
	"BDU_GATEWAY7",
	"BDU_GATEWAY8",
	"REV",
	"REV",
	"REV",
	"REV",
	"BUS",
	"REV",
	"REV",
	"REV",
	"IPG",
	"REV",
	"REV",
	"REV",
	"REV",
	"REV",
	"REV",
	"REV",
	"REV",
	"REV",
	"REV",
	"REV",
	"REV",
	"REV",
	"REV",
	"REV",
	"REV",
	"REV",
	"REV",
	"REV",
	"REV",
	"REV",
	"REV",
	"REV",
	"DS-1",
	"DS-2",
	"DS-3",
	"DS-4",
	"BROAD",
	"REV",
	"REV",
	"REV",
	"GL-1",
	"GL-2",
	"GL-3",
	"GL-4",
	"IP-0",
	"IP-1",
	"IP-2",
	"IP-3",
	"IP-4",
	"IP-5",
	"IP-6",
	"IP-7",
	"IP-8",
	"REV",
	"REV",
	"REV",
	"REV",
	"REV",
	"REV",
	"REV",
	"REV",
	"REV",
	"REV",
	"REV",
	"REV",
	"REV",
	"REV",
	"REV",
	"REV",
	"REV",
	"REV",
	"REV",
	"REV",
	"REV",
	"REV",
	"REV",
	"DT-RLC1",
	"DT-RLC2",
	"DT-RLC3",
	"DT-RLC4",
	"DT-RLC5",
	"DT-RLC6",
	"DT-RLC7",
	"DT-RLC8",
	"DT-SCU1",
	"DT-SCU2",
	"DT-SCU3",
	"DT-SCU4",
	"DT-QSW1",
	"DT-QSW2",
	"DT-QSW3",
	"DT-QSW4",
	"DT-ACC",
	"REV",
	"REV",
	"REV",
	"BDU_ROUTER1",
	"BDU_ROUTER2",
	"BDU_ROUTER3",
	"BDU_ROUTER4",
	"BDU_ROUTER5",
	"BDU_ROUTER6",
	"BDU_ROUTER7",
	"BDU_ROUTER8",
	"REV",
	"REV",
	"REV",
	"REV",
	"IM(00)",
	"IM(00.1)",
	"IM(00.2)",
	"IM(00.3)",
	"IM(01)",
	"IM(01.1)",
	"IM(01.2)",
	"IM(01.3)",
	"IM(02)",
	"IM(02.1)",
	"IM(02.2)",
	"IM(02.3)",
	"IM(03)",
	"IM(03.1)",
	"IM(03.2)",
	"IM(03.3)",
	"IM(04)",
	"IM(04.1)",
	"IM(04.2)",
	"IM(04.3)",
	"IM(05)",
	"IM(05.1)",
	"IM(05.2)",
	"IM(05.3)",
	"IM(06)",
	"IM(06.1)",
	"IM(06.2)",
	"IM(06.3)",
	"IM(07)",
	"IM(07.1)",
	"IM(07.2)",
	"IM(07.3)",
	"IM(08)",
	"IM(08.1)",
	"IM(08.2)",
	"IM(08.3)",
	"IM(09)",
	"IM(09.1)",
	"IM(09.2)",
	"IM(09.3)",
	"IM(10)",
	"IM(10.1)",
	"IM(10.2)",
	"IM(10.3)",
	"IM(11)",
	"IM(11.1)",
	"IM(11.2)",
	"IM(11.3)",
	"IM(12)",
	"IM(12.1)",
	"IM(12.2)",
	"IM(12.3)",
	"IM(13)",
	"IM(13.1)",
	"IM(13.2)",
	"IM(13.3)",
	"IM(14)",
	"IM(14.1)",
	"IM(14.2)",
	"IM(14.3)",
	"IM(15)",
	"IM(15.1)",
	"IM(15.2)",
	"IM(15.3)",
	"IM(16)",
	"IM(16.1)",
	"IM(16.2)",
	"IM(16.3)",
	"IM(17)",
	"IM(17.1)",
	"IM(17.2)",
	"IM(17.3)",
	"IM(18)",
	"IM(18.1)",
	"IM(18.2)",
	"IM(18.3)",
	"IM(19)",
	"IM(19.1)",
	"IM(19.2)",
	"IM(19.3)",
	"IM(20)",
	"IM(20.1)",
	"IM(20.2)",
	"IM(20.3)",
	"IM(21)",
	"IM(21.1)",
	"IM(21.2)",
	"IM(21.3)",
	"IM(22)",
	"IM(22.1)",
	"IM(22.2)",
	"IM(22.3)",
	"IM(23)",
	"IM(23.1)",
	"IM(23.2)",
	"IM(23.3)",
	"IM(24)",
	"IM(24.1)",
	"IM(24.2)",
	"IM(24.3)",
	"IM(25)",
	"IM(25.1)",
	"IM(25.2)",
	"IM(25.3)",
	"IM(26)",
	"IM(26.1)",
	"IM(26.2)",
	"IM(26.3)",
	"IM(27)",
	"IM(27.1)",
	"IM(27.2)",
	"IM(27.3)",
	"IM(28)",
	"IM(28.1)",
	"IM(28.2)",
	"IM(28.3)",
	"IM(29)",
	"IM(29.1)",
	"IM(29.2)",
	"IM(29.3)",
	"IM(30)",
	"IM(30.1)",
	"IM(30.2)",
	"IM(30.3)",
	"IM(31)",
	"IM(31.1)",
	"IM(31.2)",
	"IM(31.3)",
};

const char* DT_G_ADDR_TB[] = {
	"00|",
	"01|GA1_VIDEO_REQUEST",
	"02|GA2_VIDEO_SERVICE",
	"03|GA3_MEM_PLAYBACK",
	"04|GA4_MEM_REPORT",
	"05|GA5_MEM_ORDER",
	"06|GA6_RTC_SERVICE",
	"07|GA7_RTC_CLIENT",
	"08|GA8_RTC_SET",
	"09|GA9_LIGHT_CONTROL",
	"0A|GA10_LIGHT_REPORT",
	"0B|GA11_UNLOCK2_CONTROL",
	"0C|GA12_UNLOCK2_REPORT",
	"0D|GA13_UNLOCK1_CONTROL",
	"0E|GA14_UNLOCK1_REPORT",
	"0F|GA15_HEARTBEAT",
	"10|GA16_UNLOCK3_CONTROL",
	"11|GA17_UNLOCK3_REPORT",
	"12|GA18_UNLOCK4_CONTROL",
	"13|GA19_UNLOCK4_REPORT",
	"14|GA20_UNLOCK5_CONTROL",
	"15|GA21_UNLOCK5_REPORT",
	"16|GA22_UNLOCK6_CONTROL",
	"17|GA23_UNLOCK6_REPORT",
	"18|GA24_UNLOCK7_CONTROL",
	"19|GA25_UNLOCK7_REPORT",
	"1A|GA26_UNLOCK8_CONTROL",
	"1B|GA27_UNLOCK8_REPORT",
	"1C|GA28_UNLOCK_ACTION_REPORT",
	"1D|GA29_BROADCASTING",
	"1E|GA30_ALAMR_INFORM",
	"1F|GA31_ALARM_SMS_RESPONSE",
	"20|GA32_TRANSFER_TEL_INFORM",
	"21|GA33_TRANSFER_TEL_SMS_RESPONSE",
	"22|GA34_DOOR_STATE_REPORT",
	"23|GA35_SMS",
	"24|GA36_CARD_TRANSMIT",
	"25|GA36_UNLOCK_RESPONSE",
	"26|",
	"27|",
	"28|GA40_QSW_REPORT",
	"29|GA41_FISH_EYE_REPORT",
	"2A|GA42_FISH_EYE_CONTROL",
	"2B|GA43_MOVE_REPORT",
	"2C|GA44_PIR_SET_WRITE",
	"2D|GA45_PIR_SET_READ",
	"2E|GA46_PIR_SET_READ_RESPONSE",
	"2F|GA47_IPG_DEBUG",
	"30|GA48_SKS_RLC_S_CONTROL",
	"31|GA49_SKS_RLC_S_RESPONSE",
	"32|GA50_SKS_RLC_L_CONTROL",
	"33|GA51_SKS_RLC_L_RESPONSE",
	"34|",
	"35|",
	"36|",
	"37|",
	"38|",
	"39|",
	"3A|",
	"3B|",
	"3C|",
	"3D|",
	"3E|",
	"3F|",
	"40|",
	"41|",
	"42|",
	"43|",
	"44|",
	"45|",
	"46|",
	"47|",
	"48|",
	"49|",
	"4A|",
	"4B|",
	"4C|",
	"4D|",
	"4E|",
	"4F|",
	"50|",
	"51|",
	"52|",
	"53|",
	"54|",
	"55|",
	"56|",
	"57|",
	"58|",
	"59|",
	"5A|",
	"5B|",
	"5C|",
	"5D|",
	"5E|",
	"5F|",
	"60|",
	"61|",
	"62|",
	"63|",
	"64|",
	"65|GA101_CALL_INVITE",
	"66|GA102_CALL_STATE",
	"67|GA103_CALL_TRYING",
	"68|",
	"69|",
	"6A|",
	"6B|",
	"6C|",
	"6D|",
	"6E|",
	"6F|",
	"70|",
	"71|",
	"72|",
	"73|",
	"74|",
	"75|",
	"76|",
	"77|",
	"78|",
	"79|",
	"7A|",
	"7B|",
	"7C|",
	"7D|",
	"7E|",
	"7F|",
	"80|",
	"81|",
	"82|",
	"83|",
	"84|",
	"85|",
	"86|",
	"87|",
	"88|",
	"89|",
	"8A|",
	"8B|",
	"8C|",
	"8D|",
	"8E|",
	"8F|",
	"90|",
	"91|",
	"92|",
	"93|",
	"94|",
	"95|",
	"96|",
	"97|",
	"98|",
	"99|",
	"9A|",
	"9B|",
	"9C|",
	"9D|",
	"9E|",
	"9F|",
	"A0|",
	"A1|",
	"A2|",
	"A3|",
	"A4|",
	"A5|",
	"A6|",
	"A7|",
	"A8|",
	"A9|",
	"AA|",
	"AB|",
	"AC|",
	"AD|",
	"AD|",
	"AF|",
	"B0|",
	"B1|",
	"B2|",
	"B3|",
	"B4|",
	"B5|",
	"B6|",
	"B7|",
	"B8|",
	"B9|",
	"BA|",
	"BB|",
	"BC|",
	"BD|",
	"BE|",
	"BF|",
	"C0|",
	"C1|",
	"C2|",
	"C3|",
	"C4|",
	"C5|",
	"C6|",
	"C7|",
	"C8|",
	"C9|GA201_CALL_INVITE",
	"CA|GA202_CALL_STATE",
	"CB|GA203_CALL_TRYING",
	"CC|",
	"CD|",
	"CE|",
	"CF|",
	"D0|",
	"D1|",
	"D2|",
	"D3|GA211_TABLE",
	"D4|",
	"D5|",
	"D6|",
	"D7|",
	"D8|",
	"D9|",
	"DA|",
	"DB|",
	"DC|",
	"DD|",
	"DE|",
	"DF|",
	"E0|",
	"E1|",
	"E2|",
	"E3|",
	"E4|",
	"E5|",
	"E6|",
	"E7|",
	"E8|",
	"E9|",
	"EA|",
	"EB|",
	"EC|",
	"ED|",
	"EE|",
	"EF|",
	"F0|",
	"F1|",
	"F2|",
	"F3|",
	"F4|",
	"F5|",
	"F6|",
	"F7|",
	"F8|",
	"F9|",
	"FA|",
	"FB|",
	"FC|",
	"FD|",
	"FE|",
	"FF|",

};

const char* OLD_DT_CMD_TB[] = {
	"00|",
	"01|",
	"02|",
	"03|",
	"04|",
	"05|",
	"06|",
	"07|",
	"08|",
	"09|",
	"0A|",
	"0B|",
	"0C|",
	"0D|",
	"0E|",
	"0F|",
	"10|NET_CALL",
	"11|READ_PARAMETER",
	"12|READ_PARAMETER_RESULT",
	"13|WRITE_PARAMETER",
	"14|WRITE_PARAMETER_RESULT",
	"15|",
	"16|",
	"17|",
	"18|",
	"19|",
	"1A|",
	"1B|",
	"1C|",
	"1D|",
	"1E|",
	"1F|",
	"20|",
	"21|",
	"22|",
	"23|",
	"24|",
	"25|",
	"26|",
	"27|",
	"28|",
	"29|",
	"2A|",
	"2B|",
	"2C|",
	"2D|",
	"2E|",
	"2F|",
	"30|E_VOLTAGE",
	"31|E_MODEL_EDITION",
	"32|NAMELIST_APPOINT",
	"33|E_COLLIGATE",
	"34|TPC_SAVE_TELEPHONE",
	"35|TPC_ASK_TELEPHONE",
	"36|TPC_ECHO_TELEPHONE",
	"37|TPC_DIVERT_START",
	"38|",
	"39|",
	"3A|",
	"3B|",
	"3C|",
	"3D|",
	"3E|",
	"3F|",
	"40|NAMELIST_BROAD",
	"41|",
	"42|CAMERA_SET",
	"43|TIME_SET",
	"44|ZONE_SET",
	"45|",
	"46|",
	"47|",
	"48|",
	"49|",
	"4A|",
	"4B|",
	"4C|",
	"4D|",
	"4E|",
	"4F|",
	"50|STATE_RESET",
	"51|DEVICE_CALL_GL_ON(����)",
	"52|DEVICE_CALL_GL_OFF(����)",
	"53|GL_CALL_DEVICE_ON(����)",
	"54|GL_CALL_DEVICE_OFF(����)",
	"55|MR_CALL_ST_ON",
	"56|MR_CALL_ST_OFF",
	"57|ST_MON_ON",
	"58|ST_MON_OFF",
	"59|ST_CALL_ST_ON",
	"5A|ST_CALL_ST_OFF",
	"5B|INTERCOM_ON",
	"5C|INTERCOM_OFF",
	"5D|NAMELIST_ON",
	"5E|NAMELIST_OFF",
	"5F|LAOHUA_ON1",
	"60|LAOHUA_ON2",
	"61|LAOHUA_ON3",
	"62|MR_CALL_ST_TALK",
	"63|LAOHUA_OFF",
	"64|AE_RESTART",
	"65|LAMP_CONTROL",
	"66|DEBUG_REPORT",
	"67|",
	"68|",
	"69|",
	"6A|",
	"6B|",
	"6C|",
	"6D|",
	"6E|",
	"6F|",
	"70|MAIN_CALL(ϵͳ��)",
	"71|S_CANCEL",
	"72|MR_LINKING_ST",
	"73|ASK_VOLTAGE_TEST",
	"74|ASK_VOLTAGE",
	"75|ASK_MODEL_EDITION",
	"76|RECEIPT(ϵͳ��)",
	"77|MOVE_MON",
	"78|MAIN_CALL_TEST",
	"79|ASK_COLLIGATE",
	"7A|ASK_VOLTAGE_STATE",
	"7B|MAIN_CALL_GROUP(������)",
	"7C|MAIN_CANCEL_GROUP(������)",
	"7D|MAIN_CALL_ONCE_L(������)",
	"7E|RECEIPT_SINGLE(������)",
	"7F|",
	"80|ST_TALK",
	"81|ST_UNLOCK",
	"82|ST_CLOSE",
	"83|SUB_RECEIPT",
	"84|STATE_DISVISIT",
	"85|ST_LINK_MR",
	"86|MON_ON",
	"87|MON_OFF",
	"88|MON_ON_APPOINT",
	"89|MON_OFF_APPOINT",
	"8A|VOLTAGE_OK",
	"8B|VOLTAGE_ERROR",
	"8C|LET_MR_CALLME",
	"8D|ST_UNLOCK_SECOND",
	"8E|ST_ASK_NAMELIST",
	"8F|",
	"90|INTERCOM_HN_CALL",
	"91|INTERCOM_HN_TALK",
	"92|INTERCOM_HN_CLOSE",
	"93|",
	"94|",
	"95|",
	"96|",
	"97|",
	"98|INTERCOM_HJ_CALL",
	"99|INTERCOM_HJ_TALK",
	"9A|INTERCOM_HJ_CLOSE",
	"9B|",
	"9C|",
	"9D|",
	"9E|",
	"9F|",
	"A0|",
	"A1|",
	"A2|",
	"A3|",
	"A4|",
	"A5|",
	"A6|",
	"A7|",
	"A8|LET_DIDONG_ONCE",
	"A9|LET_WORK",
	"AA|LET_SPEAK",
	"AB|",
	"AC|",
	"AD|",
	"AD|",
	"AF|",
	"B0|DCU_LAMP_STATE",
	"B1|LAMP_STATE_ON",
	"B2|LAMP_STATE_OFF",
	"B3|DCU_UNLOCK",
	"B4|DCU_LAMP",
	"B5|",
	"B6|",
	"B7|",
	"B8|",
	"B9|",
	"BA|",
	"BB|",
	"BC|",
	"BD|",
	"BE|",
	"BF|",
	"C0|",
	"C1|",
	"C2|",
	"C3|",
	"C4|",
	"C5|",
	"C6|",
	"C7|",
	"C8|",
	"C9|",
	"CA|",
	"CB|",
	"CC|",
	"CD|",
	"CE|",
	"CF|",
	"D0|",
	"D1|",
	"D2|",
	"D3|",
	"D4|",
	"D5|",
	"D6|",
	"D7|",
	"D8|",
	"D9|",
	"DA|",
	"DB|",
	"DC|",
	"DD|",
	"DE|",
	"DF|",
	"E0|",
	"E1|",
	"E2|",
	"E3|",
	"E4|",
	"E5|",
	"E6|",
	"E7|",
	"E8|",
	"E9|",
	"EA|",
	"EB|",
	"EC|",
	"ED|",
	"EE|",
	"EF|",
	"F0|SUB_MODEL",
	"F1|SUB_MODEL_1",
	"F2|FUTE_AL_MSG",
	"F3|FUTD_AL_MSG",
	"F4|FUTE_AL",
	"F5|FUTE_MSG",
	"F6|TPC_IM_EMU_INFORM",
	"F7|",
	"F8|GP_LINKING",
	"F9|GP_RECEIPT",
	"FA|",
	"FB|",
	"FC|",
	"FD|",
	"FE|",
	"FF|",
};

const char* NEW_DT_CMD_TB[] = {
	"00|APCI_GROUP_VALUE_READ",
	"01|",
	"02|",
	"03|",
	"04|",
	"05|",
	"06|",
	"07|",
	"08|",
	"09|",
	"0A|",
	"0B|",
	"0C|",
	"0D|",
	"0E|",
	"0F|",
	"10|APCI_GROUP_VALUE_RESPONSE",
	"11|",
	"12|",
	"13|",
	"14|",
	"15|",
	"16|",
	"17|",
	"18|",
	"19|",
	"1A|",
	"1B|",
	"1C|",
	"1D|",
	"1E|",
	"1F|",
	"20|APCI_GROUP_VALUE_WRITE",
	"21|",
	"22|",
	"23|",
	"24|",
	"25|",
	"26|",
	"27|",
	"28|",
	"29|",
	"2A|",
	"2B|",
	"2C|",
	"2D|",
	"2E|",
	"2F|",
	"30|APCI_INDIVIDUAL_ADDR_WRITE",
	"31|APCI_INDIVIDUAL_ADDR_READ",
	"32|APCI_INDIVIDUAL_ADDR_RESPONSE",
	"33|",
	"34|",
	"35|",
	"36|",
	"37|",
	"38|",
	"39|",
	"3A|",
	"3B|",
	"3C|",
	"3D|",
	"3E|",
	"3F|",
	"40|APCI_RM_REQUEST",
	"41|APCI_RM_REP_REQUEST",
	"42|APCI_RM_QUIT",
	"43|APCI_RM_OK",
	"44|APCI_RM_EXIT",
	"45|APCI_RM_UP",
	"46|APCI_RM_DOWN",
	"47|APCI_RM_LAST",
	"48|APCI_RM_NEXT",
	"49|APCI_RM_DISPLAY",
	"4A|APCI_RM_ENTER_INPUT",
	"4B|APCI_RM_EXIT_INPUT",
	"4C|APCI_RM_INPUT_RES",
	"4D|",
	"4E|",
	"4F|",
	"50|",
	"51|",
	"52|",
	"53|",
	"54|",
	"55|",
	"56|",
	"57|APCI_DIVERT_START_INFORM",
	"58|",
	"59|",
	"5A|",
	"5B|",
	"5C|",
	"5D|",
	"5E|",
	"5F|",
	"60|APCI_INDIVIDUAL_ADDR_LINK",
	"61|",
	"62|",
	"63|",
	"64|",
	"65|",
	"66|",
	"67|",
	"68|",
	"69|",
	"6A|",
	"6B|",
	"6C|",
	"6D|",
	"6E|",
	"6F|",
	"70|",
	"71|",
	"72|",
	"73|",
	"74|",
	"75|APCI_PROG_STATE_READ",
	"76|APCI_PROG_STATE_RESPONSE",
	"77|APCI_PROG_STATE_WRITE",
	"78|",
	"79|",
	"7A|",
	"7B|",
	"7C|",
	"7D|",
	"7E|",
	"7F|",
	"80|APCI_RM_M_REQ",
	"81|APCI_RM_M_RSP",
	"82|APCI_RM_M_QUIT",
	"83|APCI_RM_M_UP",
	"84|APCI_RM_M_DN",
	"85|APCI_RM_M_LAST",
	"86|APCI_RM_M_NEXT",
	"87|APCI_RM_M_EXIT",
	"88|APCI_RM_M_OK",
	"89|APCI_RM_M_TP",
	"8A|",
	"8B|",
	"8C|",
	"8D|",
	"8E|",
	"8F|",
	"90|",
	"91|",
	"92|",
	"93|",
	"94|",
	"95|",
	"96|",
	"97|",
	"98|",
	"99|",
	"9A|",
	"9B|",
	"9C|",
	"9D|",
	"9E|",
	"9F|",
	"A0|",
	"A1|",
	"A2|",
	"A3|",
	"A4|",
	"A5|",
	"A6|",
	"A7|",
	"A8|",
	"A9|",
	"AA|",
	"AB|",
	"AC|",
	"AD|",
	"AD|",
	"AF|",
	"B0|APCI_GROUP_LONG_READ",
	"B1|APCI_GROUP_LONG_RESPONSE",
	"B2|APCI_GROUP_LONG_WRITE",
	"B3|APCI_PROPERTY_LONG_READ",
	"B4|APCI_PROPERTY_LONG_RESPONSE",
	"B5|APCI_PROPERTY_LONG_WRITE",
	"B6|",
	"B7|",
	"B8|",
	"B9|",
	"BA|",
	"BB|",
	"BC|",
	"BD|",
	"BE|",
	"BF|",
	"C0|",
	"C1|",
	"C2|",
	"C3|",
	"C4|",
	"C5|",
	"C6|",
	"C7|",
	"C8|",
	"C9|",
	"CA|",
	"CB|",
	"CC|",
	"CD|",
	"CE|",
	"CF|",
	"D0|APCI_BLOCK",
	"D1|",
	"D2|",
	"D3|",
	"D4|",
	"D5|",
	"D6|",
	"D7|",
	"D8|",
	"D9|",
	"DA|",
	"DB|",
	"DC|",
	"DD|",
	"DE|",
	"DF|",
	"E0|APCI_RESTART",
	"E1|",
	"E2|",
	"E3|",
	"E4|",
	"E5|",
	"E6|",
	"E7|",
	"E8|",
	"E9|",
	"EA|",
	"EB|",
	"EC|",
	"ED|",
	"EE|",
	"EF|",
	"F0|APCI_DIVERT_SYNC",
	"F1|APCI_FE_ANGLE_OFFSET",
	"F2|APCI_DS_QTY_SYNC",
	"F3|APCI_CAM_QTY_SYNC",
	"F4|",
	"F5|APCI_PROPERTY_VAULE_READ",
	"F6|APCI_PROPERTY_VAULE_RESPONSE",
	"F7|APCI_PROPERTY_VAULE_WRITE",
	"F8|APCI_CHECK_DEVICE_ACK",
	"F9|APCI_CHECK_DEVICE_RSP",
	"FA|APCI_CHECK_MODEL_ACK",
	"FB|APCI_IPC_SEARCH_ON",
	"FC|APCI_IPG_LINK",
	"FD|APCI_IPG_STATE",
	"FE|APCI_FAST_LINKING",
	"FF|",
};


const char *GetMyTime(void)
{
    static char myTime[24] = { 0 };

    time_t cur_t;
    struct tm cur_tm;

    time(&cur_t);
    localtime_r(&cur_t, &cur_tm);

    //strftime(myTime, sizeof(myTime), "%Y-%m-%d %T", &cur_tm);
    strftime(myTime, sizeof(myTime), "%T", &cur_tm);

    return myTime;
}

const Dec2Str_T halEventTable[] = 
{
	{EXIT_KEY1, "HAL_EVENT_EXIT_KEY1"},
	{EXIT_KEY2, "HAL_EVENT_EXIT_KEY2"},
	{KEY_A, "HAL_EVENT_KEY_A"},
	{KEY_B, "HAL_EVENT_KEY_B"},

	{0, NULL},
};

const Dec2Str_T keyStatusTable[] = 
{
	{KEY_STATUS_PRESS, "KEY_STATUS_PRESS"},
	{KEY_STATUS_RELEASE, "KEY_STATUS_RELEASE"},
	{KEY_STATUS_PRESS_3S, "KEY_STATUS_PRESS_3S"},
	{KEY_STATUS_LONG_PRESS_RELEASE, "KEY_STATUS_LONG_PRESS_RELEASE"},

	{0, NULL},
};

const Dec2Str_T UART_CMD_TB[] = 
{
	{UART_TYPE_N2S_SND_DT_CMD, "SND_DT_CMD"},
	{UART_TYPE_S2N_REC_DT_CMD, "REC_DT_CMD"},

	{UART_TYPE_S2N_SND_RESULT, "SND_RESULT"},
	{UART_TYPE_N2S_PT2259_CTRL, "UART_TYPE_N2S_PT2259_CTRL"},
	{UART_TYPE_N2S_EXTRINGER_CTRL, "UART_TYPE_N2S_EXTRINGER_CTRL"},
	{UART_TYPE_S2N_DIP_STATUS, "UART_TYPE_S2N_DIP_STATUS"},
	{UART_TYPE_S2N_DOORBELL_STATUS, "UART_TYPE_S2N_DOORBELL_STATUS"},

	{UART_TYPE_N2S_APPLY_DIP_STATUS, "UART_TYPE_N2S_APPLY_DIP_STATUS"},
	{UART_TYPE_S2N_REPLY_DIP_STATUS, "UART_TYPE_S2N_REPLY_DIP_STATUS"},
	{UART_TYPE_N2S_APPLY_DOORBELL_STATUS, "UART_TYPE_N2S_APPLY_DOORBELL_STATUS"},
	{UART_TYPE_S2N_REPLY_DOORBELL_STATUS, "UART_TYPE_S2N_REPLY_DOORBELL_STATUS"},
	{UART_TYPE_N2S_LINKING, "UART_TYPE_N2S_LINKING"},

	{UART_TYPE_N2S_BUS_ACK_SND, "UART_TYPE_N2S_BUS_ACK_SND"},
	{UART_TYPE_S2N_BUS_ACK_RCV, "UART_TYPE_S2N_BUS_ACK_RCV"},
	{UART_TYPE_S2N_TOUCHKEY_STATUS, "UART_TYPE_S2N_TOUCHKEY_STATUS"},
	{UART_TYPE_N2S_LED_CTRL, "UART_TYPE_N2S_LED_CTRL"},
	{UART_TYPE_N2S_REPOTR_APP_STATUS, "UART_TYPE_N2S_REPOTR_APP_STATUS"},

	{UART_TYPE_N2S_NOTIFY_STATE, "UART_TYPE_N2S_NOTIFY_STATE"},
	{UART_TYPE_S2N_REQUEST_STATE, "UART_TYPE_S2N_REQUEST_STATE"},
	{UART_TYPE_S2N_NOTIFY_RESET, "UART_TYPE_S2N_NOTIFY_RESET"},
	{UART_TYPE_N2S_RESET, "UART_TYPE_N2S_RESET"},
	{UART_TYPE_N2S_MUTE_CTRL, "UART_TYPE_N2S_MUTE_CTRL"},

	{UART_TYPE_N2S_WIFI_CTRL, "UART_TYPE_N2S_WIFI_CTRL"},
	{UART_TYPE_N2S_TALK_STATE, "UART_TYPE_N2S_TALK_STATE"},
	{UART_TYPE_STM8_VERSION_GET, "UART_TYPE_STM8_VERSION_GET"},
	{UART_TYPE_STM8_VERSION_ASK, "UART_TYPE_STM8_VERSION_ASK"},
	{UART_TYPE_LOCAL_TIME_GET, "UART_TYPE_LOCAL_TIME_GET"},

	{UART_TYPE_LOCAL_TIME_UPDATE, "UART_TYPE_LOCAL_TIME_UPDATE"},
	{UART_TYPE_BEEP_C, "UART_TYPE_BEEP_C"},
	{UART_TYPE_SEND_REBOOT_LOG, "UART_TYPE_SEND_REBOOT_LOG"},
	{UART_TYPE_GET_REBOOT_LOG, "UART_TYPE_GET_REBOOT_LOG"},
	{UART_TYPE_GET_REBOOT_LOG_RSP, "UART_TYPE_GET_REBOOT_LOG_RSP"},

	{UART_TYPE_FIRMWARE_M2S, "UART_TYPE_FIRMWARE_M2S"},
	{UART_TYPE_FIRMWARE_S2M, "UART_TYPE_FIRMWARE_S2M"},
	{UART_TYPE_GetSn, "UART_TYPE_GetSn"},
	{UART_TYPE_GetSn_RSP, "UART_TYPE_GetSn_RSP"},
	{UART_TYPE_SetSn, "UART_TYPE_SetSn"},
	{UART_TYPE_SetSn_RSP, "UART_TYPE_SetSn_RSP"},
	{UART_TYPE_AMP_MUTE, "UART_TYPE_AMP_MUTE"},

	{UART_TYPE_MEM_ACCESS_REQ, "UART_TYPE_MEM_ACCESS_REQ"},		//soc->mcu ִ��������
	{UART_TYPE_MEM_ACCESS_RSP, "UART_TYPE_MEM_ACCESS_RSP"},		//soc->mcu io�ڿ���
	{UART_TYPE_LOOP_BACK, "UART_TYPE_LOOP_BACK"},		//soc->mcu PWM�ڿ���
	{UART_TYPE_R_M_CMD_REP, "UART_TYPE_R_M_CMD_REP"},		//soc->mcu LED����
	{UART_TYPE_S_M_CMD_REQ, "UART_TYPE_S_M_CMD_REQ"},		//soc->mcu LED����
	{UART_TYPE_S_M_CMD_RSP, "UART_TYPE_S_M_CMD_RSP"},		//soc->mcu LED����

	{UART_TYPE_OUTPUT_ACT, "UART_TYPE_OUTPUT_ACT"},		//soc->mcu ִ��������
	{UART_TYPE_OUTPUT_BIN, "UART_TYPE_OUTPUT_BIN"},		//soc->mcu io�ڿ���
	{UART_TYPE_OUTPUT_PWM, "UART_TYPE_OUTPUT_PWM"},		//soc->mcu PWM�ڿ���
	{UART_TYPE_OUTPUT_LED, "UART_TYPE_OUTPUT_LED"},		//soc->mcu LED����

	{UART_TYPE_INTPUT_EVT_KEY, "UART_TYPE_INTPUT_EVT_KEY"},	//mcu->soc KEY�����¼�
	{UART_TYPE_INTPUT_EVT_BIN, "UART_TYPE_INTPUT_EVT_BIN"},	//mcu->soc bin�����¼�
	{UART_TYPE_INTPUT_EVT_DIP, "UART_TYPE_INTPUT_EVT_DIP"},	//mcu->soc DIP�����¼�
	{UART_TYPE_INTPUT_EVT_ADC, "UART_TYPE_INTPUT_EVT_ADC"},	//mcu->soc ADC�����¼�

	{UART_TYPE_INTPUT_READ_KEY_REQ, "UART_TYPE_INTPUT_READ_KEY_REQ"},	//soc->mcu KEY��ȡ����
	{UART_TYPE_INTPUT_READ_KEY_RSP, "UART_TYPE_INTPUT_READ_KEY_RSP"},	//mcu->soc KEY��ȡ�ظ�
	{UART_TYPE_INTPUT_READ_BIN_REQ, "UART_TYPE_INTPUT_READ_BIN_REQ"},	//soc->mcu BIN��ȡ����
	{UART_TYPE_INTPUT_READ_BIN_RSP, "UART_TYPE_INTPUT_READ_BIN_RSP"},	//mcu->soc BIN��ȡ�ظ�
	{UART_TYPE_INTPUT_READ_DIP_REQ, "UART_TYPE_INTPUT_READ_DIP_REQ"},	//soc->mcu DIP��ȡ����
	{UART_TYPE_INTPUT_READ_DIP_RSP, "UART_TYPE_INTPUT_READ_DIP_RSP"},	//mcu->soc DIP��ȡ�ظ�
	{UART_TYPE_INTPUT_READ_ADC_REQ, "UART_TYPE_INTPUT_READ_ADC_REQ"},	//soc->mcu ADC��ȡ����
	{UART_TYPE_INTPUT_READ_ADC_RSP, "UART_TYPE_INTPUT_READ_ADC_RSP"},	//mcu->soc ADC��ȡ�ظ�

	{0, NULL},
};

const Dec2Str_T UART_TASK_ID_TB[] = 
{
	{TASK_TYPE_MFG_SN, 			"TASK_TYPE_MFG_SN"},
	{TASK_TYPE_MEM_ACESS, 		"TASK_TYPE_MEM_ACESS"},
	{TASK_TYPE_OUTPUT, 			"TASK_TYPE_OUTPUT"},
	{TASK_TYPE_INPUT_READ, 		"TASK_TYPE_INPUT_READ"},
	{TASK_TYPE_INPUT_EVT, 		"TASK_TYPE_INPUT_EVT"},
	{TASK_TYPE_ZONE, 			"TASK_TYPE_ZONE"},
	{TASK_TYPE_REST, 			"TASK_TYPE_REST"},
	{TASK_TYPE_HEART_BEAT, 		"TASK_TYPE_HEART_BEAT"},
	{TASK_TYPE_REBOOT, 			"TASK_TYPE_REBOOT"},
	{TASK_TYPE_ISP, 			"TASK_TYPE_ISP"},
	{TASK_TYPE_MCU_INFO, 		"TASK_TYPE_MCU_INFO"},
	{TASK_TYPE_DTS, 			"TASK_TYPE_DTS"},
	{TASK_TYPE_MDS, 			"TASK_TYPE_MDS"},
	{TASK_TYPE_RFID, 			"TASK_TYPE_RFID"},
	{TASK_TYPE_DTS_ADDR_ER, 	"TASK_TYPE_DTS_ADDR_ER"},
	{TASK_TYPE_DTS_CHSUM_ER, 	"TASK_TYPE_DTS_CHSUM_ER"},
	{TASK_TYPE_DTS_HEAD_ER, 	"TASK_TYPE_DTS_HEAD_ER"},
	{TASK_TYPE_MDS_CHSUM_ER, 	"TASK_TYPE_MDS_CHSUM_ER"},
	{TASK_TYPE_MDS_HEAD_ER, 	"TASK_TYPE_MDS_HEAD_ER"},

	{0, NULL},
};



static char* Dec2Str(Dec2Str_T* table, int dec)
{
	int i;

	for(i = 0; table[i].str != NULL; i++)
	{
		if(table[i].num == dec)
		{
			return table[i].str;
		}
	}

	return NULL;
}

static void ParseDT_CMD(cJSON* json, char* cmd_data, char cmd_len)
{
	char tempData[PACKET_LENGTH*2+1];
	int dataLen;
	int i;

	cJSON_AddStringToObject(json, UART_DT_SA, DT_ADDR_TB[cmd_data[0]]);
	if(cmd_len < 5)	//��ָ��
	{
		cJSON_AddStringToObject(json, UART_DT_DA, DT_ADDR_TB[cmd_data[1]]);
		cJSON_AddStringToObject(json, UART_DT_APCI_EASY, OLD_DT_CMD_TB[cmd_data[2]]);
		dataLen = 0;
	}
	else	//��ָ�������ָ��
	{
		cJSON_AddStringToObject(json, UART_DT_DA, (cmd_data[4]&0x80) ? DT_G_ADDR_TB[cmd_data[1]] : DT_ADDR_TB[cmd_data[1]]);
		cJSON_AddStringToObject(json, UART_DT_APCI_EASY, (cmd_data[4]&0x40) ? NEW_DT_CMD_TB[cmd_data[2]] : OLD_DT_CMD_TB[cmd_data[2]]);
		dataLen = (cmd_data[4]&0x40) ? cmd_data[4]&0x0F : cmd_data[4];
	}
	cJSON_AddNumberToObject(json, UART_DT_DATA_LEN, dataLen);

	for(i = 0; i < dataLen && i < PACKET_LENGTH; i++)
	{
		sprintf(tempData+2*i, "%02X", cmd_data[4+i]);	
	}
	tempData[2*i] = 0;
	cJSON_AddStringToObject(json, UART_DT_DATA, tempData);
}


cJSON* UartDataToJson(char cmd, char* pbuf, unsigned int len )
{
	cJSON *json = cJSON_CreateObject();
	char tempData[PACKET_LENGTH*2+1];
	int i;
	char* cmdStr = Dec2Str(UART_CMD_TB, cmd);
	char mac_type;
	char cmd_type;
	char cmd_len;
	char taskId;
	char* cmd_data;

	if(cmdStr)
	{
		cJSON_AddStringToObject(json, UART_CMD, cmdStr);
	}
	else
	{
		cJSON_AddNumberToObject(json, UART_CMD, cmd);
	}

	if(len && pbuf)
	{
		if(cmd > 100)
		{
			taskId = pbuf[--len];
		}

		switch (cmd)
		{
			case UART_TYPE_N2S_SND_DT_CMD:
				mac_type = pbuf[0];
				cmd_type = pbuf[1];
				cmd_len = len - 4;
				cmd_data = pbuf+2;
				ParseDT_CMD(json, cmd_data, cmd_len);
				break;

			case UART_TYPE_S2N_REC_DT_CMD:
				cmd_len = len - 2;
				cmd_data = pbuf;
				ParseDT_CMD(json, cmd_data, cmd_len);
				break;

			case UART_TYPE_INTPUT_EVT_KEY:
				cJSON_AddStringToObject(json, UART_HAL_EVENT, Dec2Str(halEventTable, pbuf[1]));
				cJSON_AddStringToObject(json, UART_KEY_STATUS, Dec2Str(keyStatusTable, pbuf[0]));
				cJSON_AddStringToObject(json, UART_HAL_ID, GetMyTime());
				break;

			default:
				cmd_len = len;
				for(i = 0; i < cmd_len && i < PACKET_LENGTH; i++)
				{
					sprintf(tempData+2*i, "%02X", pbuf[i]);	
				}
				tempData[2*i] = 0;
				cJSON_AddStringToObject(json, UART_DATA, tempData);
				break;
		}

		if(cmd > 100)
		{
			cmd_data = Dec2Str(UART_TASK_ID_TB, taskId);
			if(cmd_data)
			{
				cJSON_AddStringToObject(json, UART_TASK_ID, cmd_data);
			}
			else
			{
				cJSON_AddNumberToObject(json, UART_TASK_ID, taskId);
			}
		}
	}

	return json;
}


typedef struct
{
    pthread_mutex_t 	lock;
	unsigned char 		task_id;
    cJSON*				table;
}UartSyncTable_T;

static UartSyncTable_T uartSyncTable = {.lock = PTHREAD_MUTEX_INITIALIZER, .task_id = 0, .table = NULL};

static unsigned char CreateUartSyncTaskId(unsigned char taskId)
{
	char taskIdName[5];
	unsigned char ret = 0;
	cJSON* element;
	int timeCnt;
	p_Loop_vdp_common_buffer pSyncQ;

	pthread_mutex_lock(&uartSyncTable.lock);

	if(uartSyncTable.table == NULL)
	{
		uartSyncTable.table = cJSON_CreateObject();
	}

	if(++uartSyncTable.task_id >= 200)
	{
		uartSyncTable.task_id = 1;
	}

	if(taskId == 0)
	{
		taskId = uartSyncTable.task_id;
	}
	pthread_mutex_unlock(&uartSyncTable.lock);

	snprintf(taskIdName, 5, "%03d", taskId);
	timeCnt = 0;

	do
	{
		pthread_mutex_lock(&uartSyncTable.lock);
		if((element = cJSON_GetObjectItemCaseSensitive(uartSyncTable.table, taskIdName)) == NULL)
		{
			pSyncQ = malloc(sizeof(Loop_vdp_common_buffer));
			cJSON_AddNumberToObject(uartSyncTable.table, taskIdName, (int)pSyncQ);
			init_vdp_common_queue(pSyncQ, PACKET_LENGTH, NULL, NULL);
			ret = taskId;
		}
		pthread_mutex_unlock(&uartSyncTable.lock);

		if(ret == 0)
		{
			usleep(100*1000);
			if(++timeCnt > 30)
			{
				log_v("Uart sync get task id error!!!!!!!!!!!!!!!!!!!!!");
				break;
			}
		}
		else
		{
			break;
		}
	}
	while(element);

	return ret;
}

static void DeleteUartSyncTaskId(unsigned char taskId)
{
	char taskIdName[5];
	cJSON* element;

	snprintf(taskIdName, 5, "%03d", taskId);
	pthread_mutex_lock(&uartSyncTable.lock);
	if(element = cJSON_GetObjectItemCaseSensitive(uartSyncTable.table, taskIdName))
	{
		if(element->valueint)
		{
			exit_vdp_common_queue((p_Loop_vdp_common_buffer)element->valueint);
			free(element->valueint);
		}
		cJSON_DeleteItemFromObjectCaseSensitive(uartSyncTable.table, taskIdName);
	}
	pthread_mutex_unlock(&uartSyncTable.lock);
}


static p_Loop_vdp_common_buffer GetSyncBufByTaskId(unsigned char taskId)
{
	char taskIdName[5];
	p_Loop_vdp_common_buffer ret = NULL;
	cJSON* element;

	snprintf(taskIdName, 5, "%03d", taskId);
	pthread_mutex_lock(&uartSyncTable.lock);
	if(element = cJSON_GetObjectItemCaseSensitive(uartSyncTable.table, taskIdName))
	{
		ret = (p_Loop_vdp_common_buffer)element->valueint;
	}
	pthread_mutex_unlock(&uartSyncTable.lock);
	return ret;
}

//����1����Ӧ�����ݣ�����0��û������Ӧ��
int api_uart_send_pack_and_wait_rsp( unsigned char cmd, char* inbuf, unsigned int inLen,char* outbuf, unsigned int* outLen, int timeout)
{
	
#if defined(PID_IX611) || defined(PID_IX622)
	return 0;
#endif
	p_Loop_vdp_common_buffer pSycQ;
	p_vdp_common_buffer output;
	unsigned char taskId;
	char sendData[PACKET_LENGTH];
	int ret = 0;
	int timeCnt;
	int len;

	if(inLen == 0 || inbuf == NULL)
	{
		inLen = 0;
	}
	else
	{
		memcpy(sendData, inbuf, inLen);
	}

	if(timeout)
	{
		taskId = CreateUartSyncTaskId(0);
	}
	else
	{
		taskId = 0;
	}

	sendData[inLen++] = taskId;

	LogSendUartCmd("send2", cmd, sendData, inLen);
	uart_send_pack(cmd, sendData, inLen);

	if(taskId)
	{
		pSycQ = GetSyncBufByTaskId(taskId);
		for(timeCnt = 0; timeCnt < timeout; timeCnt+=20)
		{
			usleep(20*1000);
			len = pop_vdp_common_queue(pSycQ, &output, 1);
			if(len)
			{
				if(output[0] == cmd+1)
				{
					if(outbuf && outLen)
					{
						memcpy(outbuf, output, len);
						*outLen = len;
					}
					ret = 1;
					break;
				}
				purge_vdp_common_queue(pSycQ);
			}
		}
		DeleteUartSyncTaskId(taskId);
	}

	return ret;
}
static pthread_mutex_t mds_send_lock = PTHREAD_MUTEX_INITIALIZER;
//����1����Ӧ�����ݣ�����0��û������Ӧ��
int api_uart_send_mds_packet(char* inbuf, unsigned int inLen, int timeout)
{
	#define MDS_SEND_RETRY_DELAY 	300
	p_Loop_vdp_common_buffer pSycQ;
	p_vdp_common_buffer output;
	int outLen;
	unsigned char taskId;
	char sendData[PACKET_LENGTH];
	char rcvData[PACKET_LENGTH];
	int ret = 0;
	int timeCnt;
	pthread_mutex_lock(&mds_send_lock);
	if(inLen == 0 || inbuf == NULL)
	{
		inLen = 0;
	}
	else
	{
		memcpy(sendData, inbuf, inLen);
	}

	if(timeout > 0)
	{
		taskId = CreateUartSyncTaskId(TASK_TYPE_MDS);
	}
	else
	{
		taskId = 0;
	}

	sendData[inLen++] = taskId;

	uart_send_pack(UART_TYPE_S_M_CMD_REQ, sendData, inLen);
	LogSendUartCmd("MDS", UART_TYPE_S_M_CMD_REQ, sendData, inLen);
	//PrintCurrentTime(90001);
	if(taskId)
	{
		pSycQ = GetSyncBufByTaskId(taskId);
		for(timeCnt = 0; timeCnt < timeout; timeCnt+=30)
		{
			usleep(30*1000);
			outLen = pop_vdp_common_queue(pSycQ, &output, 1);
			if(outLen)
			{
				if(output[0] == UART_TYPE_S_M_CMD_RSP)
				{
					if(output[1] == 0x03 || output[1] == 0x00)
					{
						//����æ����ʧ��
						usleep(MDS_SEND_RETRY_DELAY*1000);
						timeCnt+=MDS_SEND_RETRY_DELAY;
						//PrintCurrentTime(90002);
						uart_send_pack(UART_TYPE_S_M_CMD_REQ, sendData, inLen);
						LogSendUartCmd("resend", UART_TYPE_S_M_CMD_REQ, sendData, inLen);
					}
					else if(output[1] == 0x0A)
					{
						ret = 1;
						timeCnt = timeout;
					}
				}
				purge_vdp_common_queue(pSycQ);
			}
		}
		DeleteUartSyncTaskId(taskId);
	}
	pthread_mutex_unlock(&mds_send_lock);
	return ret;
}

//���ڽ��յ�Ӧ�����ݰ��Ĵ���
void UartRecvSyncPacketProcess(unsigned char taskId,  char* pbuf, int len)
{
	p_Loop_vdp_common_buffer pSyncBuf;
	pSyncBuf = GetSyncBufByTaskId(taskId);
	if(pSyncBuf)
	{
		push_vdp_common_queue(pSyncBuf, pbuf, len);
	}
}

//���ڽ��յ��¼�����Ĵ���
void UartRecvHalEventProcess(cJSON* halEvent)
{
	char* halEventName = cJSON_GetStringValue(cJSON_GetObjectItemCaseSensitive(halEvent, UART_HAL_EVENT));
	cJSON* businessEvent = cJSON_Parse(cJSON_GetStringValue(API_Para_Read_Public(halEventName)));
	cJSON* element;

	if(cJSON_IsObject(businessEvent))
	{
		cJSON_ArrayForEach(element, halEvent)
		{
			if(element->string != NULL && strcmp(element->string, UART_HAL_EVENT))
			{
				cJSON_AddItemToObject(businessEvent, element->string, cJSON_Duplicate(element, 1));
			}
		}
		API_Event_Json(businessEvent);
	}

	cJSON_Delete(businessEvent);
}



//������������
#include "OSTIME.h"
typedef struct                         
{
	OS_TIMER	timer;;
	int timerCnt;
}HB_T;

static HB_T	hbRun;

//��������ʱ����������
void Stm8HeartBeatServiceTimerCallback(void)
{
	if(hbRun.timerCnt++ > 6)	//60�뷢��һ������
	{
		api_uart_send_pack(UART_TYPE_N2S_LINKING, NULL, NULL);  
		////CPU_Ratio_Measure();
	}

	if(hbRun.timerCnt)
	{
		OS_SetTimerPeriod(&hbRun.timer, 10000/25);
		OS_RetriggerTimer(&hbRun.timer);
	}
}

//������������ʼ��
void Stm8HeartBeatServiceInit(void)
{
	//OS_CreateTimer(&hbRun.timer, Stm8HeartBeatServiceTimerCallback, 4);
	OS_CreateTimerOneThread(&hbRun.timer, Stm8HeartBeatServiceTimerCallback, 4);
	OS_StartTimer(&hbRun.timer);

	GetStm8lSwVerFromStm8l();
}

void RefreshHearBeatTimer(void)
{
	hbRun.timerCnt = 0;
	OS_SetTimerPeriod(&hbRun.timer, 4);
	OS_RetriggerTimer(&hbRun.timer);
}

int GetHBtimerCnt(void)
{
	return hbRun.timerCnt;
}

/*********************************************************************************************************
**  End Of File
*********************************************************************************************************/

