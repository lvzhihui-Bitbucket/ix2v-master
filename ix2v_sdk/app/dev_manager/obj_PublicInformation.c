/**
  ******************************************************************************
  * @file    obj_PublicInformation.c
  * @author  czb
  * @version V00.01.00
  * @date    2022.2.11
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 

#include <stdio.h>
#include <sys/sysinfo.h>
#include <errno.h>

#include "obj_PublicInformation.h"
#include "define_file.h"
#include "utility.h"
#include "obj_TableSurver.h"
#include "obj_IoInterface.h"

static PUB_INFO_RUN_S publicInfo = {.info = NULL, .lock = PTHREAD_MUTEX_INITIALIZER};
char* TimeToString(int time);

static void PublicInfo_Lock(void)
{
    pthread_mutex_lock(&publicInfo.lock);
}

static void PublicInfo_Unlock(void)
{
    pthread_mutex_unlock(&publicInfo.lock);
}

long GetSystemBootTime(void)
{
	struct sysinfo info;
	
	if (sysinfo(&info)) 
	{
		fprintf(stderr, "Failed to get sysinfo, errno:%u, reason:%s\n", errno, strerror(errno));
		return -1;
	}

	return info.uptime;
}

int API_LoadPublicInfoFile(const char* filePath)
{
    PublicInfo_Lock();

    publicInfo.info = GetJsonFromFile(filePath);
    if(publicInfo.info == NULL)
    {
        dprintf("%s error!!!!\n", filePath);
        PublicInfo_Unlock();
        return 0;
    }

    if(publicInfo.info == NULL)
    {
        publicInfo.info = cJSON_CreateObject();
    }
    PublicInfo_Unlock();
	API_PublicInfo_Write_String(PB_SYS_UP_TIME, TimeToString(GetSystemBootTime()));
    return 1;
}

char* TimeToString(int time)
{
	static char display[100];
	snprintf(display, 100, "%d:%02d:%02d", time/3600, (time%3600)/60, time%60);
	return display;
}

int UpdatePB_Process(const cJSON* pbValue);

const cJSON * API_PublicInfo_Read(const char* key)
{
	cJSON * ret = NULL;

    PublicInfo_Lock();
	if(key && key[0])
	{
		ret = MyCjson_GetObjectItem(publicInfo.info, key);
	}
	else
	{
		ret = publicInfo.info;
	}

	if(ret)
	{
		//如果PB在更新范围内，则更新
		if(UpdatePB_Process(ret))
		{
			if(key && key[0])
			{
				ret = MyCjson_GetObjectItem(publicInfo.info, key);
			}
			else
			{
				ret = publicInfo.info;
			}
		}
	}

    PublicInfo_Unlock();
    return ret;
}

int API_PublicInfo_Read_Int(const char* key)
{
	int value = 0;

	cJSON * ret = API_PublicInfo_Read(key);
	if(cJSON_IsNumber(ret))
	{
		value = ret->valuedouble;
	}

    return value;
}

const char* API_PublicInfo_Read_String(const char* key)
{
	char* value = NULL;

	cJSON * ret = API_PublicInfo_Read(key);
	if(cJSON_IsString(ret))
	{
		value = cJSON_GetStringValue(ret);
	}

    return value;
}

PublicInfo_bool API_PublicInfo_Read_Bool(const char* key)
{
	PublicInfo_bool value = 0;

	cJSON * ret = API_PublicInfo_Read(key);
	if(cJSON_IsTrue(ret))
	{
		value = 1;
	}

    return value;
}

int API_PublicInfo_Write(const char* key, const cJSON* value)
{
	cJSON * parent = NULL;

	if(value == NULL || key == NULL)
	{
		return 0;
	}

    PublicInfo_Lock();

	parent = MyCjson_GetObjectParent(publicInfo.info, key);

	if(parent == NULL)
	{
		cJSON_AddItemToObject(publicInfo.info, key, cJSON_Duplicate(value, 1));
	}
	else
	{
        cJSON_ReplaceItemInObjectCaseSensitive(parent, key, cJSON_Duplicate(value, 1));
	}
    PublicInfo_Unlock();

    return 1;
}

int API_PublicInfo_Write_Int(const char* key, int value)
{
	int ret;

	cJSON * jsonValue = cJSON_CreateNumber(value);
	ret = API_PublicInfo_Write(key, jsonValue);
	cJSON_Delete(jsonValue);

    return ret;
}
int API_PublicInfo_Write_String(const char* key, const char* value)
{
	int ret;

	cJSON * jsonValue = cJSON_CreateString(value);
	ret = API_PublicInfo_Write(key, jsonValue);
	cJSON_Delete(jsonValue);

    return ret;
}
int API_PublicInfo_Write_Bool(const char* key, PublicInfo_bool value)
{
	int ret;

	cJSON * jsonValue = cJSON_CreateBool(value);
	ret = API_PublicInfo_Write(key, jsonValue);
	cJSON_Delete(jsonValue);

    return ret;
}


typedef cJSON* (*CREATE_PB)(void);
typedef struct
{	
	char*		pbName;		//PB名字
	CREATE_PB	createPb;	//即时生成PB值
}UPDATE_PB_S;

static cJSON* CreatePB_UP_TIME(void)
{
    return cJSON_CreateString(TimeToString(GetSystemBootTime()));
}

static cJSON* CreatePB_SDcardCapacity(void)
{
   	char buffer[100];
	snprintf(buffer, 100, "%d MB", GetDirFreeSizeInMByte(DISK_SDCARD));
    return cJSON_CreateString(buffer);
}

static cJSON* CreatePB_NandCapacity(void)
{
   	char buffer[100];
	snprintf(buffer, 100, "%d MB", GetDirFreeSizeInMByte("/mnt/"));
    return cJSON_CreateString(buffer);
}
static cJSON* CreatePB_DATE(void)
{
    time_t rawtime;
    struct tm info;
   	char buffer[100];

    time(&rawtime);
    localtime_r(&rawtime,&info);
	strftime(buffer, 100, "%Y-%m-%d", &info);
    return cJSON_CreateString(buffer);
}

static cJSON* CreatePB_TIME(void)
{
    time_t rawtime;
    struct tm info;
   	char buffer[100];

    time(&rawtime);
    localtime_r(&rawtime,&info);
	strftime(buffer, 100, "%H:%M:%S", &info);


    return cJSON_CreateString(buffer);
}

static cJSON* CreatePB_SYS_MemAvailable(void)
{
	char buff[500];
	char memAvailable[100] = {0};
	char *pos1;
	FILE *pf=fopen("/proc/meminfo","r");
	while(fgets(buff,500,pf)!=NULL)
	{
		if(strstr(buff,"MemAvailable:"))
		{
	        sscanf(buff, "%*s %[0-9] kB", memAvailable);
			strcat(memAvailable, " kB");
			break;
		}
	}
	fclose(pf);

	return cJSON_CreateString(memAvailable);
}
static cJSON* CreatePB_DIVERT_ON_CONT(void)
{
	int cont=API_TB_CountByName(TB_NAME_TB_Divert_On,NULL);
	 //bprintf("111111111:%d\n",cont);
    return cJSON_CreateNumber(cont);
}
static cJSON*CreatePB_Linphone_state(void)
{
	int sipstate=Get_SipSer_State();
	char *sipstate_str="no connection";
	  switch (sipstate)
	    {
		    case 1:
			sipstate_str="register ok";
		        break;
		    case 2:
		       sipstate_str="verify fail";
		        break;
		    case 0:
		        sipstate_str="no connection";
		        break;
		    default:
		        break;
	    }	
	  //bprintf("111111111:%s\n",sipstate_str);
	  return cJSON_CreateString(sipstate_str);
}

static cJSON* CreatePB_ABOUT_PAGE(const char* ioAboutConfig)
{
	cJSON *ret = cJSON_CreateArray();
	cJSON *retValue  = cJSON_CreateObject();
	cJSON_AddItemToArray(ret, retValue);
	if(ioAboutConfig)	
	{
		cJSON* aboutConfig = API_Para_Read_Public(ioAboutConfig);
		aboutConfig = cJSON_GetArrayItem(aboutConfig, 0);
		cJSON *value;
		cJSON *element;
		cJSON_ArrayForEach(element, aboutConfig)
		{
			if(element->string && cJSON_IsString(element))
			{
				if(!strcmp(element->valuestring, "IO"))
				{
					value = API_Para_Read_Public(element->string);
					cJSON_AddItemToObject(retValue, element->string, value ? cJSON_Duplicate(value, 1) : cJSON_CreateNull());
				}
				else if(!strcmp(element->valuestring, "PB"))
				{
					value = MyCjson_GetObjectItem(publicInfo.info, element->string);
					if(UpdatePB_Process(value))
					{
						value = MyCjson_GetObjectItem(publicInfo.info, element->string);
					}
					cJSON_AddItemToObject(retValue, element->string, value ? cJSON_Duplicate(value, 1) : cJSON_CreateNull());
				}
				//读取旧的IO参数
				else
				{
					//char ioValue[500] = {0};
					//API_Event_IoServer_InnerRead_All(element->valuestring, ioValue);
					//cJSON_AddStringToObject(retValue, element->string, ioValue);
				}
			}
		}
	}
	return ret;
}
static cJSON* CreatePB_MANAGER_ABOUT_PAGE(void)
{
	return CreatePB_ABOUT_PAGE(MANAGER_ABOUT_CONFIG);
}

static cJSON* CreatePB_USER_ABOUT_PAGE(void)
{
	return CreatePB_ABOUT_PAGE(USER_ABOUT_CONFIG);
}

static cJSON* CreatePB_IXS_Interval(void)
{
	char tempString[10];
	snprintf(tempString, 10, "%ds", GetIXS_ProxyRecInformTime());
	return cJSON_CreateString(tempString);
}

static cJSON* CreatePB_System_State(void)
{
	char* systemStateString;
	cJSON* ret = NULL;
	if(systemStateString = MyCjson_GetObjectString(publicInfo.info, PB_System_State))
	{
		//不在启动中
		if(strcmp(systemStateString, "Starting"))
		{
			char* callServerStateString = MyCjson_GetObjectString(publicInfo.info, PB_CALL_SER_STATE);
			if(callServerStateString != NULL)
			{
				//在待机状态下
				if(!strcmp(callServerStateString, "Wait"))
				{
					ret = cJSON_CreateString("Standby");
				}
				else
				{
					ret = cJSON_CreateString("Calling");
				}
			}
		}
		else
		{
			ret = cJSON_CreateString(systemStateString);
		}
	}
	else
	{
		ret = cJSON_CreateString("");
	}

	return ret;
}

static int EOC_State_UpdateTime=0;
static char eoc_state_buff[20]={0};
static char eoc_ver_buff[40]={0};
static char eoc_mac_buff[18]={0};

static int EOC_Diag_Debug_UpdateTime=0;
static cJSON *EOC_Diag_Debug_Save=NULL;
static void EOC_Diag_Debug_Update(void)
{
	int cur_time=time(NULL);
	if(EOC_Diag_Debug_Save==NULL||abs(cur_time-EOC_Diag_Debug_UpdateTime)>=3)
	{
		if(eoc_mac_buff[0]==0)
		{
			if(API_VTK_Get_Eoc_Link_State2("eth0",eoc_mac_buff,eoc_ver_buff)<=0)
				return;
		}
		if(EOC_Diag_Debug_Save)
			cJSON_Delete(EOC_Diag_Debug_Save);
		EOC_Diag_Debug_Save=NULL;
		EOC_Diag_Debug_Save=API_VTK_Get_Eoc_Diag_Debug("eth0",eoc_mac_buff);
		EOC_Diag_Debug_UpdateTime=cur_time;
	}
	
}
static cJSON *CreatePB_EOC_auth_times(void)
{
	
	
	EOC_Diag_Debug_Update();
	cJSON *item=cJSON_GetObjectItemCaseSensitive(EOC_Diag_Debug_Save,PB_EOC_auth_times);
	if(item)
		return cJSON_CreateNumber(item->valuedouble);
	else
		return cJSON_CreateNumber(-1);
}
static cJSON *CreatePB_EOC_online_time(void)
{
	
	
	EOC_Diag_Debug_Update();
	cJSON *item=cJSON_GetObjectItemCaseSensitive(EOC_Diag_Debug_Save,PB_EOC_online_time);
	if(item)
		return cJSON_CreateNumber(item->valuedouble);
	else
		return cJSON_CreateNumber(-1);
}
static cJSON *CreatePB_EOC_max_group(void)
{
	
	int para_data;
	if(eoc_mac_buff[0]==0)
	{
		if(API_VTK_Get_Eoc_Link_State2("eth0",eoc_mac_buff,eoc_ver_buff)<=0)
		{
			return cJSON_CreateString("-");
		}
	}
	if(API_VTK_Get_Eoc_Parameter("eth0",eoc_mac_buff,"/inka.conf", "igmp", "max_group_member_num",&para_data)==0)
	{
		return cJSON_CreateNumber(para_data);
	}
	return cJSON_CreateString("-");
}
static void EOC_State_Update(void)
{
	int cur_time=time(NULL);
	if(abs(cur_time-EOC_State_UpdateTime)>=3)
	{
		char eoc_mac[18]={0};
		char version[128] = { 0 };
		CreateEocMacBySn(eoc_mac);
		int link_state=API_VTK_Get_Eoc_Link_State2("eth0",eoc_mac,version);
		printf("2222222:%d:%s\n",link_state,eoc_mac?eoc_mac:"null");
		if(link_state<0)
		{
			strcpy(eoc_state_buff,"unlink"); 
			if(eoc_mac[0]!=0)
				if(API_VTK_Get_Eoc_Mac_Ver("eth0",NULL,NULL)==0)
					strcpy(eoc_state_buff,"linked"); 
		}
		else if(link_state==0)
			strcpy(eoc_state_buff,"unauth"); //ShellCmdPrintf ("unauth\n");
		else if(link_state==1)
			strcpy(eoc_state_buff,"slave"); //ShellCmdPrintf ("slave\n");
		else if(link_state==2)
			strcpy(eoc_state_buff,"master"); //ShellCmdPrintf ("master\n");
		if(link_state>=0)
		{
			//CheckEocMacBySn(eoc_mac);
			strcpy(eoc_mac_buff,eoc_mac);
			strcpy(eoc_ver_buff,version);
		}
		else
		{
			//API_PublicInfo_Write_String("EOC_Mac_State","Unkown");
			strcpy(eoc_mac_buff,"");
			strcpy(eoc_ver_buff,"");
		}
		EOC_State_UpdateTime=cur_time;
	}
}
static cJSON *CreatePB_EOC_State(void)
{
	
	
	EOC_State_Update();
	
	return cJSON_CreateString(eoc_state_buff);
}
static cJSON *CreatePB_EOC_Mac(void)
{
	EOC_State_Update();
	
	return cJSON_CreateString(eoc_mac_buff);
}
static cJSON *CreatePB_EOC_Ver(void)
{
	EOC_State_Update();
	
	return cJSON_CreateString(eoc_ver_buff);
}

UPDATE_PB_S UPDATE_PB_TAB[] = {
	{PB_UP_TIME, CreatePB_UP_TIME},
	{PB_SDcardCapacity, CreatePB_SDcardCapacity},
	{PB_NandCapacity, CreatePB_NandCapacity},
	{PB_DATE, CreatePB_DATE},
	{PB_TIME, CreatePB_TIME},
	{PB_SYS_MemAvailable, CreatePB_SYS_MemAvailable},
	{PB_DIVERT_ON_CONT, CreatePB_DIVERT_ON_CONT},
	{PB_LINPHONE_STATE, CreatePB_Linphone_state},
	{PB_IXS_Interval, CreatePB_IXS_Interval},

	#ifndef IX_LINK
	{PB_EOC_state, CreatePB_EOC_State},
	{PB_EOC_Mac, CreatePB_EOC_Mac},
	{PB_EOC_auth_times, CreatePB_EOC_auth_times},
	{PB_EOC_online_time, CreatePB_EOC_online_time},
	{PB_EOC_max_group, CreatePB_EOC_max_group},
	#endif


	/*----------------------------------------------*/
	// ！！！！！请注意，PB里面读取别的PB值，必须放在最后
	// ！！！！！请注意，PB里面读取别的PB值，必须放在最后
	// ！！！！！请注意，PB里面读取别的PB值，必须放在最后
	{PB_System_State, CreatePB_System_State},
	{PB_MANAGER_ABOUT_PAGE, CreatePB_MANAGER_ABOUT_PAGE},
	{PB_USER_ABOUT_PAGE, CreatePB_USER_ABOUT_PAGE},
	// ！！！！！请注意，PB里面读取别的PB值，必须放在最后
	// ！！！！！请注意，PB里面读取别的PB值，必须放在最后
	// ！！！！！请注意，PB里面读取别的PB值，必须放在最后
};
const int UPDATE_PB_TAB_CNT = sizeof(UPDATE_PB_TAB)/sizeof(UPDATE_PB_TAB[0]);

//返回：1 -- 有更新， 0 -- 无更新
int UpdatePB_Process(const cJSON* pbValue)
{
	int index;
	int ret = 0;
	cJSON * parent = NULL;
	cJSON* readPbValue = cJSON_Duplicate(pbValue, 1); 
	for(index = 0; index < UPDATE_PB_TAB_CNT; index++)
	{
		//如果读取的内容中包含pbName，则更新内容
		if(MyCjson_GetObjectItem(readPbValue, UPDATE_PB_TAB[index].pbName))
		{
			parent = MyCjson_GetObjectParent(publicInfo.info, UPDATE_PB_TAB[index].pbName);
			if(parent)
			{
				cJSON_ReplaceItemInObjectCaseSensitive(parent, UPDATE_PB_TAB[index].pbName, UPDATE_PB_TAB[index].createPb());
				ret = 1;
			}
		}
	}
	cJSON_Delete(readPbValue);

	return ret;
}
/*********************************************************************************************************
**  End Of File
*********************************************************************************************************/

