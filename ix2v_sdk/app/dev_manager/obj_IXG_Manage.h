/**
 ******************************************************************************
 * @file    obj_IXG_Manage.h
 * @author  czb
 * @version V00.01.00
 * @date    2023.02.15
 * @brief
 ******************************************************************************
 * @attention
 *
 *
 * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
 ******************************************************************************
 */

#ifndef _obj_IXG_Manage_H
#define _obj_IXG_Manage_H

#include "cJSON.h"
#include "utility.h"

int API_IXG_CheckIn(void);
int API_IXG_SearchAdd(void);
int API_UpdateIxgList(void);
int API_UpdateIxgDtImList(int ip);

#endif

/*********************************************************************************************************
**  End Of File
*********************************************************************************************************/