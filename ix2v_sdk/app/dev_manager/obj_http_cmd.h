
#ifndef _OBJ_HTTP_CMD_H_
#define _OBJ_HTTP_CMD_H_

#include "cJSON.h"   
#include "utility.h"

//http command
// unlock 
#define API_CMD_UNLOCK1_GET					"api_device/rlc/unlock/1"
#define API_CMD_UNLOCK1_POST			    "api_device/rlc/unlock/1"
// lamp on
#define API_CMD_lamp1_GET					"api_device/rlc/lamp/1"
#define API_CMD_lamp1_POST					"api_device/rlc/lamp/1"
// about
#define API_CMD_ABOUT_GET					"api_device/rlc/about"
// reboot
#define API_CMD_REBOOT_POST					"api_device/rlc/reboot"

int ShellCmd_Http(cJSON *cmd);

/// @brief http_command_request
/// @param url      - target（IX-RLC） ip addr
/// @param cmd      - get/post command string
/// @param request  - request data（json format）
/// @return         - result（json format）
cJSON* http_command_request(const char*url, const char* cmd, const char* request);

#endif
