/**
  ******************************************************************************
  * @file    obj_PasswordCheck.h
  * @author  lzh
  * @version V00.01.00
  * @date    2018.10.27
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 

typedef enum
{
    PSW_TYPE_NONE = 0,
    PSW_TYPE_INSTALLER,
    PSW_TYPE_MANAGE,
    PSW_TYPE_UNLOCK1,
    PSW_TYPE_UNLOCK2,
    PSW_TYPE_PRIVATE_UNLOCK,
    PSW_TYPE_VM_USER,
}PSW_TYPE;

PSW_TYPE InputPasswordVerify(char* pwd, int autoUnlockLen);