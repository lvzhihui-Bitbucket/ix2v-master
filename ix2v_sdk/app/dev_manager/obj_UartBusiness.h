/**
  ******************************************************************************
  * @file    obj_UartBusiness.h
  * @author  cao
  * @version V00.01.00
  * @date    2022.06.15
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 

#ifndef _obj_UartBusiness_H
#define _obj_UartBusiness_H

#include "OSTIME.h"


// Define Object Property-------------------------------------------------------
#define UART_CMD		"CMD"
#define UART_DATA		"DATA"
#define UART_TASK_ID	"TASK_ID"

#define UART_HAL_EVENT	"HAL_EVENT"
#define UART_KEY_STATUS	"KEY_STATUS"
#define UART_HAL_ID	    "HAL_ID"

#define UART_DT_CMD_TYPE		"CMD_TYPE"
#define UART_DT_SA		      "SA"
#define UART_DT_DA		      "DA"
#define UART_DT_APCI_EASY		"APCI_EASY"
#define UART_DT_DATA_LEN		"DATA_LEN"
#define UART_DT_DATA		    "DATA"




typedef struct                         
{
   int num;
   char* str;
}Dec2Str_T;

//key_id:
typedef enum
{
  EXIT_KEY1  = 0,
  EXIT_KEY2,
  KEY_A,
  KEY_B,
}key_id_t;

//key_status:
typedef enum
{
  KEY_STATUS_PRESS = 0,
  KEY_STATUS_RELEASE,  
  KEY_STATUS_PRESS_3S,
  KEY_STATUS_LONG_PRESS_RELEASE,
}key_status_t;


cJSON* UartDataToJson(char cmd, char* pbuf, unsigned int len );
void UartRecvSyncPacketProcess(unsigned char taskId,  char* pbuf, int len);
void UartRecvHalEventProcess(cJSON* halEvent);
//返回1，有应答数据，返回0，没有数据应答
int api_uart_send_pack_and_wait_rsp( unsigned char cmd, char* inbuf, unsigned int inLen,char* outbuf, unsigned int* outLen, int timeout);

#endif


