
/**
  ******************************************************************************
  * @file    obj_Certificate.c
  * @author  lzh
  * @version V00.01.00
  * @date    2018.10.27
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 

#include <sys/sysinfo.h>
#include "obj_Certificate.h"
#include "define_file.h"
#include "cJSON.h"
#include "obj_SYS_VER_INFO.h"
#include "obj_PublicInformation.h"
#include "obj_TableSurver.h"
#include "task_Event.h"
#include "obj_IoInterface.h"

#define	IX2V_PLATFORM			1		//IX2V：1; IX:0

#define CERT_INFO				"CERT_INFO"
#define DCFG_DATE					"DCFG_DATE"
#define CERT_DATE					"CERT_DATE"

#define CERT_ADDR				"CERT_ADDR"
#define CERT_ETH0				"CERT_ETH0"

#if IX2V_PLATFORM

#else
	#define Static_ip_setting       "Static_ip_setting"
	#define IP_Address              "IP_Address"
	#define SubnetMask              "SubnetMask"
	#define DefaultRoute            "DefaultRoute"
	#define Lan_policy              "Lan_policy"
	#define IX_ADDR                 "IX_ADDR"
	#define IO_MFG_SN               "MFG_SN"
#endif

static pthread_mutex_t	certLock = PTHREAD_MUTEX_INITIALIZER;

static int CertLog(const char* type, const char* info)
{
	int ret;
	char temp[100];
	cJSON* log = cJSON_CreateObject();
	cJSON_AddStringToObject(log, "DATE", GetCurrentTime(temp, 100, "%Y-%m-%d %H:%M:%S"));
	cJSON_AddStringToObject(log, "TYPE", type ? type : "");
	cJSON_AddStringToObject(log, "INFO", info ? info : "");
	ret = API_TB_AddByName(TB_NAME_CertLog, log);
	cJSON_Delete(log);

	return ret;
}

//生成dcfg文件，name目前只支持"CERT_ETH0"
int API_Create_DCFG_SELF(const char* name, ...)
{
	char temp[100];
	char* keyName;
	va_list valist;
	cJSON* certEth0;
	MakeDir(CERT_DIR);

	pthread_mutex_lock( &certLock);
    cJSON* dcfg = GetJsonFromFile(CERT_DIR CERT_DCFG_SELF_NAME);
	if(dcfg == NULL)
	{
		dcfg = cJSON_CreateObject();
	}

	//增加编辑信息
	cJSON_DeleteItemFromObjectCaseSensitive(dcfg, CERT_INFO);
	cJSON* info = cJSON_AddObjectToObject(dcfg, CERT_INFO);
	cJSON_AddStringToObject(info, DCFG_DATE, GetCurrentTime(temp, 100, "%Y-%m-%d %H:%M:%S"));

	//增加房号
	cJSON_DeleteItemFromObjectCaseSensitive(dcfg, CERT_ADDR);
	cJSON* addr = cJSON_AddObjectToObject(dcfg, CERT_ADDR);
	cJSON_AddStringToObject(addr, IX_ADDR, GetSysVerInfo_BdRmMs());

	va_start(valist, name);
	for(keyName = name; keyName; keyName = va_arg(valist, char*))
	{
		if(!strcmp(keyName, CERT_ETH0))
		{
			cJSON_DeleteItemFromObjectCaseSensitive(dcfg, CERT_ETH0);
			certEth0 = cJSON_AddObjectToObject(dcfg, CERT_ETH0);
			cJSON_AddStringToObject(certEth0, Lan_policy, "Static");
			cJSON* staticIP = cJSON_AddObjectToObject(certEth0, Static_ip_setting);
			cJSON_AddStringToObject(staticIP, IP_Address, GetSysVerInfo_IP_by_device(NET_ETH0));
			cJSON_AddStringToObject(staticIP, SubnetMask, GetSysVerInfo_mask_by_device(NET_ETH0));
			cJSON_AddStringToObject(staticIP, DefaultRoute, GetSysVerInfo_gateway_by_device(NET_ETH0));
		}
		else if(strcmp(keyName, CERT_INFO) && strcmp(keyName, CERT_ADDR))
		{
			cJSON_DeleteItemFromObjectCaseSensitive(dcfg, keyName);
			cJSON* ioPara = API_Para_Read_Public(keyName);
			if(ioPara)
			{
				cJSON_AddItemToObject(dcfg, keyName, cJSON_Duplicate(ioPara, 1));
			}
		}
	}
	va_end(valist);

	SetJsonToFile(CERT_DIR CERT_DCFG_SELF_NAME, dcfg);
	pthread_mutex_unlock(&certLock);

	keyName = cJSON_PrintUnformatted(dcfg);
	CertLog("Dcfg create", keyName);
	if(keyName)
	{
		free(keyName);
	}
	cJSON_Delete(dcfg);

	return 1;
}

static int CertCheckIO(cJSON* cert)
{
	int ret = 1;

	#if IX2V_PLATFORM
    cJSON *current_element = NULL;

    if(cert == NULL)
    {
        return ret;
    }
    current_element = cert;

    while(current_element != NULL)
    {
        if(current_element->string)
        {
			if(API_Para_Read_Public(current_element->string))
			{
				if(!API_IO_Check(current_element->string, current_element))
				{
					return 0;
				}
			}
        }
        else if(cJSON_IsObject(current_element))
        {
            if(!CertCheckIO(current_element->child))
            {
                return 0;
            }
        }
        current_element = current_element->next;
    }
	#endif

    return ret;
}

static int CertJsonCheck(cJSON* cert)
{
	int ret = 0;
	char logInfo[200];
	if(cert == NULL)
	{
		//没有文件
		CertLog("Cert error", "No CERT file");
	    return ret;
	}

	cJSON* info = cJSON_GetObjectItemCaseSensitive(cert, CERT_INFO);
	if(info == NULL)
	{
		//没有信息，校验不通过
		CertLog("CERT_INFO error", "No CERT_INFO");
	    return ret;
	}

	cJSON* addr = cJSON_GetObjectItemCaseSensitive(cert, CERT_ADDR);
	if(addr == NULL)
	{
		//没有地址，校验不通过
		CertLog("CERT_ADDR error", "No CERT_ADDR");
	    return ret;
	}

	if(!CertCheckIO(cert))
	{
		//IO校验不通过
		CertLog("CERT_IO error", "IO range error");
	    return ret;
	}

	cJSON* sn = cJSON_GetObjectItemCaseSensitive(cert, IO_MFG_SN);
	if(sn == NULL || sn->valuestring == NULL || strcmp(GetSysVerInfo_Sn(), sn->valuestring))
	{
		//SN校验不通过
		snprintf(logInfo, 200, "Cert MFG_SN:%s, my MFG_SN:%s", sn->valuestring, GetSysVerInfo_Sn());
		CertLog("CERT_MFG_SN error", logInfo);
	    return ret;
	}

    return 1;
}

//dcfg文件转换为Cert文件
int API_Create_CERT(const char* dcfgName)
{
	int ret = 0;
	char file[200];

	pthread_mutex_lock(&certLock);
	snprintf(file, 200, "%s/%s", CERT_DIR, dcfgName);
    cJSON* cert = GetJsonFromFile(file);

	while (cJSON_GetObjectItemCaseSensitive(cert, IO_MFG_SN))
	{
		cJSON_DeleteItemFromObjectCaseSensitive(cert, IO_MFG_SN);
	}

	cJSON_AddStringToObject(cert, IO_MFG_SN, GetSysVerInfo_Sn());

	//文件检查正确
	if(CertJsonCheck(cert))
	{
		cJSON* info = cJSON_GetObjectItemCaseSensitive(cert, CERT_INFO);
		cJSON_AddStringToObject(info, CERT_DATE, GetCurrentTime(file, 100, "%Y-%m-%d %H:%M:%S"));
		SetJsonToFile(CERT_FILE, cert);
		ret = 1;

		char* certInfo = cJSON_PrintUnformatted(cert);
		CertLog("Cert create", certInfo);
		if(certInfo)
		{
			free(certInfo);
		}
	}
	pthread_mutex_unlock(&certLock);

	cJSON_Delete(cert);

	return ret;
}

//返回-1，修改失败； 0，无需修改； 1，修改成功
static int Check_IXADDR(const cJSON* certAddr, int correctEnable)
{
	int setFlag = 0;
	char info[200];

	cJSON* ixAddr = cJSON_GetObjectItemCaseSensitive(certAddr, IX_ADDR);
	if(ixAddr && ixAddr->valuestring)
	{
		#if IX2V_PLATFORM
			char* pbIxAddr = API_PublicInfo_Read_String(PB_IX_ADDR);
			if(strcmp(GetSysVerInfo_BdRmMs(), ixAddr->valuestring))
			{
				setFlag = 1;
				snprintf(info, 200, "Cert:%s IO:%s", ixAddr->valuestring, GetSysVerInfo_BdRmMs());
				CertLog("CERT_ADDR IX_ADDR error", info);
			}
			else if(pbIxAddr && strcmp(pbIxAddr, ixAddr->valuestring))
			{
				setFlag = 2;
				snprintf(info, 200, "Cert:%s PB:%s", ixAddr->valuestring, pbIxAddr);
				CertLog("CERT_ADDR IX_ADDR error", info);
			}

			if(setFlag)
			{
				if(correctEnable)
				{
					snprintf(info, 200, "Modify IX_ADDR: %s --> %s", (setFlag == 1 ? GetSysVerInfo_BdRmMs(): pbIxAddr), ixAddr->valuestring);
					CertLog("CERT_ADDR IX_ADDR error", info);
					SetSysVerInfo_BdRmMs(ixAddr->valuestring);
					API_PublicInfo_Write_String(PB_IX_ADDR, ixAddr->valuestring);
				}
				setFlag = 1;
			}
		#else
			if(strcmp(GetSysVerInfo_BdRmMs(), ixAddr->valuestring))
			{
				setFlag = 1;
				if(setFlag)
				{
					if(correctEnable)
					{
						snprintf(info, 200, "Modify IX_ADDR: %s --> %s", GetSysVerInfo_BdRmMs(), ixAddr->valuestring);
						CertLog("CERT_ADDR IX_ADDR error", info);
						SetSysVerInfo_BdRmMs(ixAddr->valuestring);
					}
					else
					{
						snprintf(info, 200, "Cert:%s IO:%s", ixAddr->valuestring, GetSysVerInfo_BdRmMs());
						CertLog("CERT_ADDR IX_ADDR error", info);
					}
				}
			}
		#endif
	}

	return setFlag;
}

//返回-1，修改失败； 0，无需修改； 1，修改成功
static int Check_IO_PARA(const cJSON* certPara, int correctEnable)
{
	int setFlag = 0;
	char info[200];
	char errorInfo[200];
	char certValueString[200];
	char ioValueString[200];

	if(certPara && certPara->string)
	{
		cJSON* iovalue = API_Para_Read_Public(certPara->string);
		if(iovalue && !cJSON_Compare(iovalue, certPara, 1))
		{
			setFlag = 1;

			cJSON_PrintPreallocated(certPara, certValueString, 200, 0);
			cJSON_PrintPreallocated(iovalue, ioValueString, 200, 0);
			snprintf(info, 200, "Cert:%s IO:%s", certValueString, ioValueString);
			snprintf(errorInfo, 200, "Cert IO PARA %s error", certPara->string);
			CertLog(errorInfo, info);
		}

		if(setFlag)
		{
			setFlag = 0;
			if(correctEnable)
			{
				snprintf(info, 200, "Modify %s: %s --> %s", certPara->string, certValueString, ioValueString);
				snprintf(errorInfo, 200, "Cert IO PARA %s error", certPara->string);
				CertLog(errorInfo, info);
				setFlag = API_Para_Write_PublicByName(certPara->string, certPara);
				if(!setFlag)
				{
					setFlag = -1;
				}
			}
		}
	}

	return setFlag;
}


//Go Live之后10秒钟回调，检测结果是否有错。
static int CheckGoLiveResultCallback(int time)
{
	cJSON* event = cJSON_CreateObject();
	cJSON_AddStringToObject(event, EVENT_KEY_EventName, EventCertConfig);
	cJSON_AddStringToObject(event, "OPCODE", "CheckGoLiveResult");
	API_Event_Json(event);
	cJSON_Delete(event);
	return 2;
}


//返回-1，修改失败； 0，无需修改； 1，修改成功
static int Check_ETH0(const cJSON* certEth0, int correctEnable)
{
	int setFlag = 0;
	cJSON* certStaticIP;
	char info[200];

	API_Del_TimingCheck(CheckGoLiveResultCallback);

	cJSON* certPolicy = cJSON_GetObjectItemCaseSensitive(certEth0, Lan_policy);
	if(certPolicy == NULL || certPolicy->valuestring == NULL)
	{
		CertLog("CERT_ETH0 error", "Cert Lan_policy error!");
		return setFlag;
	}
	certStaticIP = cJSON_GetObjectItemCaseSensitive(certEth0, Static_ip_setting);
	if(!cJSON_IsObject(certStaticIP))
	{
		CertLog("CERT_ETH0 error", "Cert Static_ip_setting error!");
		return setFlag;
	}

	cJSON* certIp = cJSON_GetObjectItemCaseSensitive(certStaticIP, IP_Address);
	cJSON* certMask = cJSON_GetObjectItemCaseSensitive(certStaticIP, SubnetMask);
	cJSON* certRoute = cJSON_GetObjectItemCaseSensitive(certStaticIP, DefaultRoute);
	if(!cJSON_IsString(certIp) || !cJSON_IsString(certMask) || !cJSON_IsString(certRoute))
	{
		CertLog("CERT_ETH0 error", "Cert Static_ip_setting error!");
		return setFlag;
	}

	#if IX2V_PLATFORM
		char* policy = API_Para_Read_String2(Lan_policy);
		if(policy == NULL)
		{
			CertLog("CERT_ETH0 error", "No IO Lan_policy");
			return setFlag;
		}
		
		//证书是静态IP
		if(!strcmp("Static", certPolicy->valuestring))
		{
			if(strcmp(policy, certPolicy->valuestring))
			{
				setFlag = 1;
				snprintf(info, 200, "Cert Lan_policy: %s, IO Lan_policy: %s", certPolicy->valuestring, policy);
				CertLog("CERT_ETH0 error", info);
			}
			else
			{
				int localIp = GetLocalIpByDevice(NET_ETH0);
				int localRoute = GetLocalGatewayByDevice(NET_ETH0);
				int localMask = GetLocalMaskAddrByDevice(NET_ETH0);
				if(localIp != inet_addr(certIp->valuestring))
				{
					setFlag = 1;
					snprintf(info, 200, "Cert IP_Address: %s, Local IP_Address: %s", certIp->valuestring, my_inet_ntoa2(localIp));
					CertLog("CERT_ETH0 error", info);
				}
				else if(localMask != inet_addr(certMask->valuestring))
				{
					setFlag = 1;
					snprintf(info, 200, "Cert SubnetMask: %s, Local SubnetMask: %s", certMask->valuestring, my_inet_ntoa2(localMask));
					CertLog("CERT_ETH0 error", info);
				}
				else if(localRoute != inet_addr(certRoute->valuestring))
				{
					setFlag = 1;
					snprintf(info, 200, "Cert DefaultRoute: %s, Local DefaultRoute: %s", certRoute->valuestring, my_inet_ntoa2(localRoute));
					CertLog("CERT_ETH0 error", info);
				}
			}

			if(setFlag)
			{	
				if(correctEnable)
				{
					API_Para_Write_String(Lan_policy, certPolicy->valuestring);
					API_Para_Write_Public(certStaticIP);
					if(API_Set_Lan_Config(certEth0) < 0)
					{
						setFlag = -1;
						CertLog("CERT_ETH0 error", "API_Set_Lan_Config error!");
					}
					else
					{
						snprintf(info, 200, "Set Lan_policy: %s, IP_Address: %s, DefaultRoute: %s, SubnetMask: %s", certPolicy->valuestring, certIp->valuestring, certRoute->valuestring, certMask->valuestring);
						CertLog("CERT_ETH0 error", info);
						API_Add_TimingCheck(CheckGoLiveResultCallback, 10);
					}
				}
			}
		}
		//证书是动态IP
		else
		{
			if(strcmp(policy, certPolicy->valuestring))
			{
				setFlag = 1;
				snprintf(info, 200, "Cert Lan_policy: %s, IO Lan_policy: %s", certPolicy->valuestring, policy);
				CertLog("CERT_ETH0 error", info);
				if(setFlag && correctEnable)
				{
					API_Para_Write_String(Lan_policy, certPolicy->valuestring);
					if(API_Set_Lan_Config(certEth0) < 0)
					{
						setFlag = -1;
						CertLog("CERT_ETH0 error", "API_Set_Lan_Config error!");
					}
					else
					{
						snprintf(info, 200, "Set Lan_policy: %s", certPolicy->valuestring);
						CertLog("CERT_ETH0 error", info);
						API_Add_TimingCheck(CheckGoLiveResultCallback, 10);
					}
				}
			}
		}

	#else
		//证书是静态IP
		if(!strcmp("Static", certPolicy->valuestring))
		{
			if(api_nm_if_get_lan_mode() || API_GetIOLanMode())
			{
				setFlag = 1;
				snprintf(info, 200, "Cert Lan_policy: %s, IO Lan_policy: %s", certPolicy->valuestring, "AutoIpAndDHCP");
				CertLog("CERT_ETH0 error", info);
			}
			else
			{
				int localIp = GetLocalIpByDevice(NET_ETH0);
				int localRoute = GetLocalGatewayByDevice(NET_ETH0);
				int localMask = GetLocalMaskAddrByDevice(NET_ETH0);
				if(localIp != inet_addr(certIp->valuestring))
				{
					setFlag = 1;
					snprintf(info, 200, "Cert IP_Address: %s, Local IP_Address: %s", certIp->valuestring, my_inet_ntoa2(localIp));
					CertLog("CERT_ETH0 error", info);
				}
				else if(localMask != inet_addr(certMask->valuestring))
				{
					setFlag = 1;
					snprintf(info, 200, "Cert SubnetMask: %s, Local SubnetMask: %s", certMask->valuestring, my_inet_ntoa2(localMask));
					CertLog("CERT_ETH0 error", info);
				}
				else if(localRoute != inet_addr(certRoute->valuestring))
				{
					setFlag = 1;
					snprintf(info, 200, "Cert DefaultRoute: %s, Local DefaultRoute: %s", certRoute->valuestring, my_inet_ntoa2(localRoute));
					CertLog("CERT_ETH0 error", info);
				}
			}

			if(setFlag && correctEnable)
			{	
				snprintf(info, 200, "Set Lan_policy: %s, IP_Address: %s, DefaultRoute: %s, SubnetMask: %s", certPolicy->valuestring, certIp->valuestring, certRoute->valuestring, certMask->valuestring);
				CertLog("CERT_ETH0 error", info);
				SetNetWork(NULL, certIp->valuestring, certMask->valuestring, certRoute->valuestring);
				NM_LAN_IPAssignedSet(0);
				API_Add_TimingCheck(CheckGoLiveResultCallback, 10);
			}
		} 
		//证书是动态IP
		else if((!api_nm_if_get_net_mode()) || (!API_GetIOLanMode()))
		{
			setFlag = 1;
			snprintf(info, 200, "Cert Lan_policy: %s, IO Lan_policy: %s", certPolicy->valuestring, "Static");
			CertLog("CERT_ETH0 error", info);
			if(correctEnable)
			{
				snprintf(info, 200, "Set Lan_policy: %s", certPolicy->valuestring);
				CertLog("CERT_ETH0 error", info);
				NM_LAN_IPAssignedSet(1);
				API_Add_TimingCheck(CheckGoLiveResultCallback, 10);
			}
		}
	#endif

	return setFlag;
}

//返回-1，修改失败； 0，无需修改； 1，修改成功
static int Go_Live(const cJSON* cert)
{
	int ret = 1;
    cJSON *element = NULL;
	cJSON* certStaticIP;
	int setFlag;
	int mastChange = 0;

	cJSON_ArrayForEach(element, cert)
	{
		setFlag = 0;
		if(element->string)
		{
			//设置房号
			if(!strcmp(element->string, CERT_ADDR))
			{
				setFlag = Check_IXADDR(element, 1);
			}
			//设置ETH0
			else if(!strcmp(element->string, CERT_ETH0))
			{
				setFlag = Check_ETH0(element, 1);
			}
			//除了ETH0、房号和INFO，剩下的都是IO参数
			else if(strcmp(element->string, CERT_INFO))
			{
				setFlag = Check_IO_PARA(element, 1);
			}

			if(setFlag)
			{
				//已经修改过参数
				mastChange = 1;
			}

			if(setFlag == -1)
			{
				ret = -1;
				break;
			}
		}
	}

	//无需修改参数
	if(!mastChange)
	{
		ret = 0;
	}

	return ret;
}

int API_CheckGoLiveResult(void)
{
    cJSON *element = NULL;
	cJSON* certStaticIP;
	pthread_mutex_lock(&certLock);
    cJSON* cert = GetJsonFromFile(CERT_FILE);
	if(CertJsonCheck(cert))
	{
		cJSON_ArrayForEach(element, cert)
		{
			if(element->string)
			{
				//检测ETH0
				if(!strcmp(element->string, CERT_ETH0))
				{
					Check_ETH0(element, 0);
				}
			}
		}
	}
	cJSON_Delete(cert);

	pthread_mutex_unlock(&certLock);

	return 1;
}


//返回-1，Go_Live失败； 0，无文件或者文件错误； 1，Go_Live成功
int API_Go_Live(void)
{
	int ret = 0;

	pthread_mutex_lock(&certLock);
    cJSON* cert = GetJsonFromFile(CERT_FILE);
	if(cert)
	{
		//文件检查正确
		if(CertJsonCheck(cert))
		{
			ret = Go_Live(cert);
			API_PublicInfo_Write_String(PB_CERT_STATE, "Delivered");
			if(ret == 1)
			{
				API_PublicInfo_Write_String(PB_CERT_ERROR_STATE, "Cert file legal");
			}
			else if(ret == 0)
			{
				API_PublicInfo_Write_String(PB_CERT_ERROR_STATE, "Cert file legal");
				ret = 1;
			}
			else if(ret == -1)
			{
				API_PublicInfo_Write_String(PB_CERT_ERROR_STATE, "Go live error");
				CertLog("Go live", "error");
			}
		}
		else
		{
			API_PublicInfo_Write_String(PB_CERT_STATE, "Configurable");
			API_PublicInfo_Write_String(PB_CERT_ERROR_STATE, "Cert file error");
		}
	}
	else
	{
		API_PublicInfo_Write_String(PB_CERT_STATE, "Configurable");
		API_PublicInfo_Write_String(PB_CERT_ERROR_STATE, "No cert file");
	}
	pthread_mutex_unlock(&certLock);

	cJSON_Delete(cert);

	return ret;
}

static int Check_CERT_TimerCallback(int timing)
{
	cJSON* event = cJSON_CreateObject();
	cJSON_AddStringToObject(event, EVENT_KEY_EventName, EventCertConfig);
	cJSON_AddStringToObject(event, "OPCODE", "GoLive");
	API_Event_Json(event);
	cJSON_Delete(event);
	return 1;
}

//删除Cert文件，写PB状态，清除定时检测
int API_Delete_CERT(void)
{
	pthread_mutex_lock(&certLock);
	API_Del_TimingCheck(Check_CERT_TimerCallback);
	DeleteFileProcess(NULL, CERT_FILE);
	API_PublicInfo_Write_String(PB_CERT_STATE, "Configurable");
	API_PublicInfo_Write_String(PB_CERT_ERROR_STATE, "No cert file");
	pthread_mutex_unlock(&certLock);
	CertLog("Delete cert", NULL);
	return 1;
}


//返回Go live的结果；启动定时检测环境是否变化，time单位是秒，最小10秒
int API_Check_CERT(void)
{
	int ret = 0;
	int time = API_Para_Read_Int(CERT_GoLive_Timer);
	ret = API_Go_Live();
	if(ret)
	{
		API_Add_TimingCheck(Check_CERT_TimerCallback, time < 10 ? 10 : time);
	}
	else
	{
		API_Del_TimingCheck(Check_CERT_TimerCallback);
	}

	return ret;
}