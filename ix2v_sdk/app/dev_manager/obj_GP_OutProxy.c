/**
  ******************************************************************************
  * @file    obj_GP_OutProxy.c
  * @author  czb
  * @version V00.01.00
  * @date    2022.06.15
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 

#include <time.h>
#include <sys/wait.h>
#include "cJSON.h"
#include "vdp_uart.h"
#include "obj_GP_OutProxy.h"
#include "obj_IoInterface.h"
#include "utility.h"
#include "obj_PublicInformation.h"
#include "task_Event.h"
#include "obj_MyCtrlLed.h"
#include "obj_gpio.h"
#include "remote_id_access.h"
/*
"GP_OUT_DATA":
{
   "OUT_TYPE":"bin"/"pwm"/"led"/"actuator",
   "OUT_NAME": "AU_PWR_EN",
}

------------
bin���͵�����
   "OUTPUT": true/false,

actuator���͵�����
   "OUTPUT": true/false,
       
"pwm"���͵�����
   "PWM_MODE":"PWM_DIM_OFF/ON/..."
   "PWM_DUTY"��int
   
"led"���͵�����
   "LED_MODE":"MSG_LED_ON/..."

-------------
*/



const GPIO_PROXY_ATOI strToIntTable[] = {
	{"RL1", RL1},

	#if defined(PID_IX850)||defined(PID_IX821)	//只有850支持第二把锁
	{"RL2", RL2},
	#endif		

	#if 0//fndef PID_IX850
	{"AU_PWR_EN", AU_PWR_EN},
	{"AU_SWITCH", AU_SWITCH},
	#endif

	{"SENSOR_PWN_EN",SENSOR_PWN_EN},
	#ifdef PID_IX850
	{"EOC_RST",EOC_RST},
	{"EOC_ROLE",EOC_ROLE},
	#endif
	{"NIGHT_VISION", NIGHT_VISION},

	{"PWM_OFF", PWM_OFF},
	{"PWM_ON", PWM_ON},
	{"PWM_DIM_OFF", PWM_DIM_OFF},
	{"PWM_DIM_ON", PWM_DIM_ON},

	{"LED_1", LED_1},
	{"LED_2", LED_2},

	{"MSG_LED_ON", MSG_LED_ON},
	{"MSG_LED_OFF", MSG_LED_OFF},
	{"MSG_LED_FLASH_SLOW", MSG_LED_FLASH_SLOW},
	{"MSG_LED_FLASH_FAST", MSG_LED_FLASH_FAST},
	{"MSG_LED_FLASH_IRREGULAR", MSG_LED_FLASH_IRREGULAR},
	
	{"IXG_DT_AU_SW", IXG_DT_AU_SW},
	{"IXG_DT_VD_SW", IXG_DT_VD_SW},
	{"LCD_PWM_CTRL", LCD_PWM_CTRL},
	{NULL, 0},
};

static int GPIO_ProxyAtoI(char* string)
{
	int i;

	for(i = 0; string != NULL; i++)
	{
		if(strToIntTable[i].str == NULL)
		{
			return -1;
		}
		else if(!strcmp(string, strToIntTable[i].str))
		{
			return strToIntTable[i].num;
		}
	}

	return -1;
}


int IX2V_GP_OUT(cJSON* ctrlData)
{
	char cmd;
	char data[POOL_LENGTH];
	unsigned char len;
	int ret = 0;
	int stringToInt;
	char* type = cJSON_GetStringValue(cJSON_GetObjectItemCaseSensitive(ctrlData, GP_DATA_KEY_OUT_TYPE));
	char* name = cJSON_GetStringValue(cJSON_GetObjectItemCaseSensitive(ctrlData, GP_DATA_KEY_OUT_NAME));

	if(type == NULL || name == NULL)
	{
		return ret;
	}

	stringToInt = GPIO_ProxyAtoI(name);
	if(stringToInt == -1)
	{
		return ret;
	}
	
	if(!strcmp(type, GP_OUT_TYPE_BIN))
	{
		cmd = UART_TYPE_OUTPUT_BIN;
		len = 2;

		data[0] = stringToInt;
		data[1] = cJSON_IsTrue(cJSON_GetObjectItemCaseSensitive(ctrlData, GP_DATA_KEY_OUTPUT)) ? 1 : 0;
		ret = 1;
		api_uart_send_pack_and_wait_rsp(cmd, data, len, NULL, NULL, 0);
	}
	else if(!strcmp(type, GP_OUT_TYPE_ACT))
	{

#if defined(PID_IX611) || defined(PID_IX622)
		if( cJSON_IsTrue(cJSON_GetObjectItemCaseSensitive(ctrlData, GP_DATA_KEY_OUTPUT)) )
			UNLOCK_SET()
		else
			UNLOCK_RESET()
#else		
		cmd = UART_TYPE_OUTPUT_ACT;
		len = 2;
		data[0] = stringToInt;
		data[1] = cJSON_IsTrue(cJSON_GetObjectItemCaseSensitive(ctrlData, GP_DATA_KEY_OUTPUT)) ? 1 : 0;
		ret = 1;
		api_uart_send_pack_and_wait_rsp(cmd, data, len, NULL, NULL, 0);
#endif		
	}
	else if(!strcmp(type, GP_OUT_TYPE_PWM))
	{
#if defined(PID_IX611) || defined(PID_IX622)
		if( GPIO_ProxyAtoI(cJSON_GetStringValue(cJSON_GetObjectItemCaseSensitive(ctrlData, GP_DATA_KEY_PWM_MODE))) == PWM_DIM_ON )
			NIGHTVIEW_TURN_ON()
		else
			NIGHTVIEW_TURN_OFF()
#else
		cmd = UART_TYPE_OUTPUT_PWM;
		len = 3;
		data[0]=stringToInt;
		data[1]=GPIO_ProxyAtoI(cJSON_GetStringValue(cJSON_GetObjectItemCaseSensitive(ctrlData, GP_DATA_KEY_PWM_MODE)));
		if(GetJsonDataPro(ctrlData, GP_DATA_KEY_PWM_DUTY,&stringToInt))
			data[2]=stringToInt;
		else
			data[2]=0;
		ret = 1;
		api_uart_send_pack_and_wait_rsp(cmd, data, len, NULL, NULL, 0);
#endif		
	}
	else if(!strcmp(type, GP_OUT_TYPE_LED))
	{
		cmd = UART_TYPE_OUTPUT_LED;
		len = 3;
		data[0] = stringToInt;
		char* led_mode = cJSON_GetStringValue(cJSON_GetObjectItemCaseSensitive(ctrlData, GP_DATA_KEY_LED_MODE));
		data[1] = GPIO_ProxyAtoI(led_mode);
		data[2] = 0;//����
		ret = 1;
		api_uart_send_pack_and_wait_rsp(cmd, data, len, NULL, NULL, 0);
	}

	return ret;
}

const char* RL_NAME[RL_NUMBER] = {"RL1", "RL2"};
static ActuatorCtrlRun actuatorRun;
#define ActuatorCtrTimerPeriod		100

int API_Event_LockAction(const char* name, int ctrl)
{
	int ret = 0;
	cJSON *event = cJSON_CreateObject();
	cJSON_AddStringToObject(event, EVENT_KEY_EventName, EventLockAction);
	cJSON_AddStringToObject(event, "name", name);
	cJSON_AddBoolToObject(event, "ctrl", ctrl);
	API_Event_Json(event);
	cJSON_Delete(event);

	return ret;
}

void SetLockState(char* name, int ctrl)
{

	if(!strcmp(name, "RL1"))
	{
		API_PublicInfo_Write_Bool(PB_LOCK1_STATE, ctrl);
		API_Event_LockAction(name, ctrl);
	}
	else if(!strcmp(name, "RL2"))
	{
		API_PublicInfo_Write_Bool(PB_LOCK2_STATE, ctrl);
		API_Event_LockAction(name, ctrl);
	}
	// lzh_20240827_s
	else if( !strcmp(name, "REMOTE_UNLOCK_OK") || !strcmp(name, "REMOTE_UNLOCK_DENY") || !strcmp(name, "REMOTE_UNLOCK_ER") )
	{
		printf("----------SetLockState----------name[%s]-------------------\n",name);
		API_PublicInfo_Write_Bool(PB_LOCK1_STATE, ctrl);
		API_Event_LockAction(name, ctrl);
	}
	// lzh_20240827_e
}

void SetDtLinkState(int state,char *link_type)
{
	API_PublicInfo_Write_Bool(PB_DT_LINK_STATE, state);
	if(state)
		API_PublicInfo_Write_String(PB_DT_LINK_TYPE, link_type);
	else
		API_PublicInfo_Write_String(PB_DT_LINK_TYPE, "");
	API_Event_By_Name(EventLinkState);
}


void Actuator_Timer_Callback(void)
{
	int i;
	int timerFlag;
	cJSON* ctrlData;

	for(timerFlag = 0, i = 0; i < RL_NUMBER; i++)
	{
		actuatorRun.actuator[i].timerCnt++;
		if(actuatorRun.actuator[i].timing)
		{
			if(actuatorRun.actuator[i].timing <= ActuatorCtrTimerPeriod*actuatorRun.actuator[i].timerCnt)
			{
				actuatorRun.actuator[i].timing = 0;
				actuatorRun.actuator[i].phy_ctrl=!actuatorRun.actuator[i].phy_ctrl;
				ctrlData = cJSON_CreateObject();
				cJSON_AddStringToObject(ctrlData, GP_DATA_KEY_OUT_TYPE, GP_OUT_TYPE_ACT);
				cJSON_AddStringToObject(ctrlData, GP_DATA_KEY_OUT_NAME, actuatorRun.actuator[i].name);
				cJSON_AddItemToObject(ctrlData, GP_DATA_KEY_OUTPUT, cJSON_CreateBool(actuatorRun.actuator[i].phy_ctrl));
				IX2V_GP_OUT(ctrlData);
				cJSON_Delete(ctrlData);
				SetLockState(actuatorRun.actuator[i].name, 0);
			}
			else
			{
				timerFlag = 1;
			}
		}
	}

	if(timerFlag)
	{
		actuatorRun.timerFlag = 1;
		OS_RetriggerTimer(&actuatorRun.timer);
	}
	else
	{
		OS_StopTimer(&actuatorRun.timer);	
		actuatorRun.timerFlag = 0;
	}
}

void Init_Actuator_ctrl(void)
{
	int i;

	for(i = 0; i < RL_NUMBER; i++)
	{
		actuatorRun.actuator[i].name = RL_NAME[i];
		actuatorRun.actuator[i].state = 0;
		actuatorRun.actuator[i].timerCnt = 0;
		actuatorRun.actuator[i].timing = 0;
	}
	OS_CreateTimer(&actuatorRun.timer, Actuator_Timer_Callback, ActuatorCtrTimerPeriod/25);	//25ms
}

int API_Actuator_ctrl(char* name, int ctrl, int timing)//timing��λ��
{
	int i;
	ActuatorData *pData;
	cJSON* ctrlData;
	cJSON* rlCfg;
	int phy_ctrl;
	char *rl_mode;
	int ret = 0;
	if(name == NULL)
	{
		return ret;
	}


	rlCfg = API_Para_Read_Public(name);
	if(timing == 0)
	{
		timing = API_GetObjectInt(rlCfg, RL_TIMER);
	}
	rl_mode=API_GetObjectString(rlCfg, RL_MODE);
	if(rl_mode==NULL)
		phy_ctrl=ctrl;
	else
	{
		if(!strcmp(rl_mode,"0-NO") || !strcmp(rl_mode,"0"))
		{
			#if defined(PID_IX622)
			if(unlock_mode_get()!=0)
				unlock_mode_set(0);
			#endif
			phy_ctrl=ctrl;
		}
		else
		{
			#if defined(PID_IX622)
			if(unlock_mode_get()!=1)
				unlock_mode_set(1);
			#endif
			//unlock_mode_set(1);
			phy_ctrl=!ctrl;
		}
	}
	unlock_mode_get();
	for(i = 0; i < RL_NUMBER; i++)
	{
		if(!strcmp(actuatorRun.actuator[i].name, name))
		{
			pData = &actuatorRun.actuator[i];
			actuatorRun.actuator[i].timerCnt = 0;
			actuatorRun.actuator[i].timing = ctrl ? (timing*1000) : 0;
			actuatorRun.actuator[i].phy_ctrl=phy_ctrl;
			ctrlData = cJSON_CreateObject();
			cJSON_AddStringToObject(ctrlData, GP_DATA_KEY_OUT_TYPE, GP_OUT_TYPE_ACT);
			cJSON_AddStringToObject(ctrlData, GP_DATA_KEY_OUT_NAME, name);
			cJSON_AddItemToObject(ctrlData, GP_DATA_KEY_OUTPUT, cJSON_CreateBool(phy_ctrl));
			IX2V_GP_OUT(ctrlData);
			cJSON_Delete(ctrlData);
			SetLockState(name, ctrl);

			if(!actuatorRun.timerFlag && actuatorRun.actuator[i].timing)
			{
				OS_RetriggerTimer(&actuatorRun.timer);
			}
			ret = 1;
			break;
		}
	}

	return ret;
}

int Event_LockReq(cJSON *cmd)
{
	char name[100];
	char keyStatus[100] = {0};
	char source[100] = {0};
	int ctrl = 0;
	int time = 0;
	int ret = 0;
	//printf_json(cmd, __func__);
	// lzh_20240827_s
	if( is_remote_device_enable() )
	{
		char remote_call_card[20];
		memset(remote_call_card,0,sizeof(remote_call_card));
		API_Para_Read_String(RemoteCallCard,remote_call_card);
		return thirdpart_remote_id_access(remote_call_card);
	}
	// lzh_20240827_e

	GetJsonDataPro(cmd, "LOCK_NBR", name);
	GetJsonDataPro(cmd, "LOCK_PROCESS", &ctrl);
	time = GetEventItemInt(cmd, "TIME");
	GetJsonDataPro(cmd, "KEY_STATUS", keyStatus);
	GetJsonDataPro(cmd, IX2V_SOURCE, source);

	if(keyStatus[0]  && strcmp(keyStatus, "KEY_STATUS_PRESS"))
	{
		return ret;
	}

	char* lockName = NULL;
	if(!strcmp(name, "RL1"))
	{
		lockName = "LK1";
	}
	else if(!strcmp(name, "RL2"))
	{
		lockName = "LK2";
	}
	else
	{
		return ret;
	}

	cJSON* lockPara = API_Para_Read_Public(lockName);
	if(API_GetObjectInt(lockPara, "ENABLE"))
	{
		char* lockType = API_GetObjectString(lockPara, "TYPE");
		if(lockType)
		{
			if(!strcmp(lockType, "INT"))
			{
				char* intLockName = API_GetObjectString(lockPara, "INT");
				if(intLockName)
				{
					if(!time)
					{
						time = API_GetObjectInt(API_Para_Read_Public(name), RL_TIMER);
					}
					ret = API_Actuator_ctrl(intLockName, ctrl, time);
					//内部锁启动录像
					if(ctrl && ret)
					{
						char message[100];
						if(API_Para_Read_Int(UnlockRecordEnable))
						{
							cJSON* oneUnlockRecord = cJSON_CreateObject();
							cJSON_AddStringToObject(oneUnlockRecord, IX2V_UNLOCK_TIME, GetCurrentTime(message, 100, "%Y-%m-%d %H:%M:%S"));
							cJSON_AddStringToObject(oneUnlockRecord, IX2V_LOCK_NBR, name);
							cJSON_AddStringToObject(oneUnlockRecord, IX2V_SOURCE, source);
							API_TB_AddByName(IX2V_UNLOCK_RECORD_TB, oneUnlockRecord);
							cJSON_Delete(oneUnlockRecord);
						}
					}
					#ifndef IX_LINK
					#if defined(PID_IX850)||defined(PID_IX821)
					#include "stack212.h"
					char go_buff[2];
					int go_addr_index;
					if(ctrl)
					{
						go_addr_index=atoi(GetSysVerInfo_BdRmMs()+8)-1;
						if(go_addr_index<4)
						{
							unsigned short go_addr[]=
							{
								GA13_UNLOCK1_CONTROL,
								GA11_UNLOCK2_CONTROL,
								GA16_UNLOCK3_CONTROL,
								GA18_UNLOCK4_CONTROL,
								GA20_UNLOCK5_CONTROL,
								GA22_UNLOCK6_CONTROL,
								GA24_UNLOCK7_CONTROL,
								GA26_UNLOCK8_CONTROL
							};
							go_addr_index=go_addr_index*2;
							if(strcmp(name, "RL2")==0)
								go_addr_index+=1;
							go_buff[0]=1;
							go_buff[1]=time;
							AI_PushOneCmdIntoQueue(OS_Q_STACK_FRAME_TASK_TRS_APT_A, go_addr[go_addr_index], APCI_GROUP_VALUE_WRITE, 1, 2,go_buff);
						}
					}
					#endif
					#if defined(PID_IX622) || defined(PID_IX611)
					if(ctrl)
					{
						int addr_index;
						addr_index=atoi(GetSysVerInfo_BdRmMs()+8)-1;
						if(addr_index<4)
						{
							char cmd_line[100];
							#if 0
							char *pcmd[]=
							{
								"d2",
								"c3",
								"b4",
								"a5",
								"96",
								"69",
								"5a",
								"4b"
							};
							#endif
							addr_index=addr_index*2;
							if(strcmp(name, "RL2")==0)
								addr_index+=1;
							//go_buff[0]=1;
							//go_buff[1]=time;
							
							//sprintf(cmd_line,"echo %s > /dev/dtsnd",pcmd[addr_index]);
							sprintf(cmd_line,"echo \"unlock %d\" > /dev/dtsnd",addr_index+1);
							system(cmd_line);
							usleep(300*1000);
							system(cmd_line);
							//AI_PushOneCmdIntoQueue(OS_Q_STACK_FRAME_TASK_TRS_APT_A, go_addr[go_addr_index], APCI_GROUP_VALUE_WRITE, 1, 2,go_buff);
						}
					}
					#endif
					#endif
				}
			}
			//IX_RLC只有开锁，没有关锁
			else if(ctrl && !strcmp(lockType, "IX_RLC"))
			{
				if(!time)
				{
					time = API_GetObjectInt(lockPara, "TIMER");
				}
				char* target = API_GetObjectString(lockPara, "ADDR");
				int ch = API_GetObjectInt(lockPara, "CH");
				char* modeString = API_GetObjectString(lockPara, "MODE");
				int mode;
				if(modeString && (!strcmp(modeString,"0-NO") || !strcmp(modeString,"0")))
				{
					mode = 0;
				}
				else
				{
					mode = 1;
				}
				int protection_timing = API_GetObjectInt(lockPara, "PROTECTION_TIMING");
				int retrig = API_GetObjectInt(lockPara, "RETRIG");

				Send_RLCUnlockReq(target, ch, mode, time, protection_timing, retrig);
				ret = 1;
				SetLockState(name, ctrl);
			}			
		}

		#if defined(PID_IX611) || defined(PID_IX622)||defined(PID_IX821)
		if(ctrl && ret)
		{
			API_MyLedCtrl(MY_LED_LOCK, MY_LED_ON, time < 5 ? 5 : time);
		}
		#endif
		dprintf("name=%s, ctrl=%d, time=%ds\n", name, ctrl, time);
	}

	return 1;
}

int API_LED_ctrl(char* name, char* led_mode)
{
	cJSON* ctrlData;
	int ret;

	ctrlData = cJSON_CreateObject();
	cJSON_AddStringToObject(ctrlData, GP_DATA_KEY_OUT_TYPE, GP_OUT_TYPE_LED);
	cJSON_AddStringToObject(ctrlData, GP_DATA_KEY_OUT_NAME, name);
	cJSON_AddStringToObject(ctrlData, GP_DATA_KEY_LED_MODE, led_mode);
	ret = IX2V_GP_OUT(ctrlData);
	cJSON_Delete(ctrlData);

	return ret;
}

static int LED1_OFF(int timing)
{
	API_LED_ctrl("LED_1", "MSG_LED_OFF");
	return 2;
}

static int LED2_OFF(int timing)
{
	API_LED_ctrl("LED_2", "MSG_LED_OFF");
	return 2;
}
						

int API_LED_ctrl2(char* name, char* led_mode, int time)
{

	if(name && led_mode)
	{
		API_LED_ctrl(name, led_mode);
		if(time && strcmp(led_mode, "MSG_LED_OFF"))
		{
			if(!strcmp(name, "LED_1"))
			{
				API_Add_TimingCheck(LED1_OFF, time);
			}
			else if(!strcmp(name, "LED_2"))
			{
				API_Add_TimingCheck(LED2_OFF, time);
			}
		}
	}

	return 1;
}

int API_Bin_ctrl(char* name, int state)
{
	cJSON* ctrlData;
	int ret;

	ctrlData = cJSON_CreateObject();
	cJSON_AddStringToObject(ctrlData, GP_DATA_KEY_OUT_TYPE, GP_OUT_TYPE_BIN);
	cJSON_AddStringToObject(ctrlData, GP_DATA_KEY_OUT_NAME, name);
	if(state)
		cJSON_AddTrueToObject(ctrlData,GP_DATA_KEY_OUTPUT);
	else
		cJSON_AddFalseToObject(ctrlData,GP_DATA_KEY_OUTPUT);
	ret = IX2V_GP_OUT(ctrlData);
	cJSON_Delete(ctrlData);

	return ret;
}

int API_NV_ctrl(int sw,int duty)
{
	cJSON* ctrlData;
	int ret;

	ctrlData = cJSON_CreateObject();
	cJSON_AddStringToObject(ctrlData, GP_DATA_KEY_OUT_TYPE, GP_OUT_TYPE_PWM);
	cJSON_AddStringToObject(ctrlData, GP_DATA_KEY_OUT_NAME, "NIGHT_VISION");
	if(sw==1)
	{
		cJSON_AddStringToObject(ctrlData, GP_DATA_KEY_PWM_MODE, "PWM_DIM_ON");
		cJSON_AddNumberToObject(ctrlData, GP_DATA_KEY_PWM_DUTY, duty);
		VideoEventLog("NIGHT_VISION ON");
	}
	else
	{
		cJSON_AddStringToObject(ctrlData, GP_DATA_KEY_PWM_MODE, "PWM_DIM_OFF");
		cJSON_AddNumberToObject(ctrlData, GP_DATA_KEY_PWM_DUTY, 0);
		VideoEventLog("NIGHT_VISION OFF");
	}
printf("----------------------debug 5--------------------\n");
	
	ret = IX2V_GP_OUT(ctrlData);
	cJSON_Delete(ctrlData);

	return ret;
}
int API_LCD_ctrl(int sw,int duty)
{
	cJSON* ctrlData;
	int ret;

	ctrlData = cJSON_CreateObject();
	cJSON_AddStringToObject(ctrlData, GP_DATA_KEY_OUT_TYPE, GP_OUT_TYPE_PWM);
	cJSON_AddStringToObject(ctrlData, GP_DATA_KEY_OUT_NAME, "LCD_PWM_CTRL");
	if(sw==1)
	{
		cJSON_AddStringToObject(ctrlData, GP_DATA_KEY_PWM_MODE, "PWM_DIM_ON");
		cJSON_AddNumberToObject(ctrlData, GP_DATA_KEY_PWM_DUTY, duty);
	}
	else
	{
		cJSON_AddStringToObject(ctrlData, GP_DATA_KEY_PWM_MODE, "PWM_DIM_OFF");
		cJSON_AddNumberToObject(ctrlData, GP_DATA_KEY_PWM_DUTY, 0);
	}
	
	ret = IX2V_GP_OUT(ctrlData);
	cJSON_Delete(ctrlData);

	return ret;
}

	
/*********************************************************************************************************
**  End Of File
*********************************************************************************************************/

