/**
  ******************************************************************************
  * @file    obj_GetDeviceTypeAndArea.h
  * @author  cao
  * @version V00.01.00
  * @date    2016.5.31
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 
	  //cao_20170304  整个文件

#ifndef _obj_GetDeviceTypeAndArea_H
#define _obj_GetDeviceTypeAndArea_H

typedef enum
{
	TYPE_ALL = 0,
	TYPE_IM,
	TYPE_OS,
	TYPE_DS,
	TYPE_GL,
	TYPE_VM,
	TYPE_IM_BAK,
	TYPE_DT_IM,
	TYPE_AC,
	TYPE_IXG,
} Type_e;

typedef enum
{
	CommonArea,
	SameBuilding,
	OtherBuilding,
	SameRoom, // zfz_20190601
} Area_e;

// Define Object Property-------------------------------------------------------
typedef struct
{	
	Type_e	type;				//设备类型
	Area_e	area;				//设备区域
}DeviceTypeAndArea_T;

// Define Object Function - Public----------------------------------------------
DeviceTypeAndArea_T GetDeviceTypeAndAreaByNumber(const char* bd_rm_ms);


// Define Object Function - Private---------------------------------------------


#endif


