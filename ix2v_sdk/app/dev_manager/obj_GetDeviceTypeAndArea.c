/**
  ******************************************************************************
  * @file    obj_GetDeviceTypeAndArea.c
  * @author  czb
  * @version V00.01.00
  * @date    2016.5.31
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 

#include "obj_GetDeviceTypeAndArea.h"
#include "obj_SYS_VER_INFO.h"

DeviceTypeAndArea_T GetDeviceTypeAndAreaByNumber(const char* bd_rm_ms)
{
	DeviceTypeAndArea_T ret;
	int MS_Nbr;

	MS_Nbr = atoi(bd_rm_ms+8);
	
	//BD_Nbr=0000
	if(!memcmp(bd_rm_ms, "0000", 4))
	{
		//RM_Nbr=0000 小区主机和管理机
		if(!memcmp(bd_rm_ms+4, "0000", 4))
		{

			ret.area = CommonArea;
			if(MS_Nbr <= 48 && MS_Nbr >= 1)
			{
				ret.type = TYPE_DS;
			}
			else if(MS_Nbr <= 98 && MS_Nbr >= 51)
			{
				ret.type = TYPE_GL;
			}
		}			
	}
	//（BD_Nbr != 0000）
	else 
	{
		if(!memcmp(bd_rm_ms, GetSysVerInfo_bd(), 4))
		{
			if( !memcmp(bd_rm_ms+4, GetSysVerInfo_rm(), 4) ) // zfz_20190601
			{
				ret.area = SameRoom;
			}
			else
			{
				ret.area = SameBuilding;
			}
		}
		else
		{
			ret.area = OtherBuilding;
		}

		//RM_Nbr=0000 单元主机和管理机
		if(!memcmp(bd_rm_ms+4, "0000", 4))
		{

			if(MS_Nbr <= 48 && MS_Nbr >= 1)
			{
				ret.type = TYPE_DS;
			}
			else if(MS_Nbr <= 98 && MS_Nbr >= 51)
			{
				ret.type = TYPE_GL;
			}
		}
		//RM_Nbr!=0000 IM OS VM
		else
		{
			if(MS_Nbr <= 32 && MS_Nbr >= 1)
			{
				ret.type = TYPE_IM;
			}
			else if(MS_Nbr <= 72 && MS_Nbr >= 51)
			{
				ret.type = TYPE_OS;
			}
			else if(MS_Nbr == 50)
			{
				ret.type = TYPE_VM;
			}
		}
	}
	
	return ret;
}



/*********************************************************************************************************
**  End Of File
*********************************************************************************************************/

