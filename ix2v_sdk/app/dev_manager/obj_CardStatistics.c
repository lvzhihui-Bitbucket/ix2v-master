/**
  ******************************************************************************
  * @file    obj_CardStatistics.c
  * @author  cao
  * @version V00.01.00
  * @date    2023.11.29
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 

#include <sys/sysinfo.h>
#include "obj_PublicInformation.h"
#include "obj_TableSurver.h"
#include "utility.h"

void cJSON_ReplaceOrAddItemInObjectCaseSensitive(cJSON* parent, const char* string, cJSON* obj)
{
    if(!obj || !cJSON_IsObject(parent))
    {
        return;
    }

    if(cJSON_GetObjectItemCaseSensitive(parent, string))
    {
        cJSON_ReplaceItemInObjectCaseSensitive(parent, string, obj);
    }
    else
    {
        cJSON_AddItemToObject(parent, string, obj);
    }
}


void CardStatisticsInit(void)
{
    char temp[100];
    int cardNumber = API_TB_CountByName(TB_NAME_CardTable, NULL);
    cJSON* desc = API_TB_GetDescByName(TB_NAME_CardTable);
    desc = (cJSON_IsObject(desc) ? cJSON_Duplicate(desc, 1) : cJSON_CreateObject());

    int ioCardNumber = GetEventItemInt(desc, PB_CardNumber);
    if((cardNumber != ioCardNumber) || (!strcmp(GetEventItemString(desc, PB_CardLastModificationDate), "")))
    {
        cJSON_ReplaceOrAddItemInObjectCaseSensitive(desc, PB_CardNumber, cJSON_CreateNumber(cardNumber));
        cJSON_ReplaceOrAddItemInObjectCaseSensitive(desc, PB_CardLastModificationDate, cJSON_CreateString(GetCurrentTime(temp, 100, "%Y-%m-%d %H:%M:%S")));
        ioCardNumber = GetEventItemInt(desc, PB_CardSwipesNumber);
        if(!ioCardNumber)
        {
            cJSON_ReplaceOrAddItemInObjectCaseSensitive(desc, PB_CardSwipesNumber, cJSON_CreateNumber(ioCardNumber));
        }
        ioCardNumber = GetEventItemInt(desc, PB_CardSwipesSuccessfulNumber);
        if(!ioCardNumber)
        {
            cJSON_ReplaceOrAddItemInObjectCaseSensitive(desc, PB_CardSwipesSuccessfulNumber, cJSON_CreateNumber(ioCardNumber));
        }
        API_TB_ReplaceDescByName(TB_NAME_CardTable, desc);
    }
    cJSON_Delete(desc);
}

//卡数据有改变，通知
void API_CardModificationInform(void)
{
    char temp[100];
	int cardNumber = API_TB_CountByName(TB_NAME_CardTable, NULL);
    cJSON* desc = API_TB_GetDescByName(TB_NAME_CardTable);
    desc = (cJSON_IsObject(desc) ? cJSON_Duplicate(desc, 1) : cJSON_CreateObject());
    cJSON_ReplaceOrAddItemInObjectCaseSensitive(desc, PB_CardNumber, cJSON_CreateNumber(cardNumber));
    cJSON_ReplaceOrAddItemInObjectCaseSensitive(desc, PB_CardLastModificationDate, cJSON_CreateString(GetCurrentTime(temp, 100, "%Y-%m-%d %H:%M:%S")));
    API_TB_ReplaceDescByName(TB_NAME_CardTable, desc);
    cJSON_Delete(desc);
}

//刷卡通知，swipesResult: 1成功, 0失败
void API_CardswipesInform(int swipesResult)
{
    cJSON* desc = API_TB_GetDescByName(TB_NAME_CardTable);
    desc = (cJSON_IsObject(desc) ? cJSON_Duplicate(desc, 1) : cJSON_CreateObject());
    int cardswipesNumber = GetEventItemInt(desc, PB_CardSwipesNumber);

    cJSON_ReplaceOrAddItemInObjectCaseSensitive(desc, PB_CardSwipesNumber, cJSON_CreateNumber(cardswipesNumber+1));
    if(swipesResult)
    {
        cardswipesNumber = GetEventItemInt(desc, PB_CardSwipesSuccessfulNumber);
        cJSON_ReplaceOrAddItemInObjectCaseSensitive(desc, PB_CardSwipesSuccessfulNumber, cJSON_CreateNumber(cardswipesNumber+1));
    }
    API_TB_ReplaceDescByName(TB_NAME_CardTable, desc);
    cJSON_Delete(desc);
}