/**
  ******************************************************************************
  * @file    obj_GP_OutProxy.c
  * @author  czb
  * @version V00.01.00
  * @date    2022.06.15
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 

#include "cJSON.h"
#include "obj_DT_CheckOnline.h"
#include "task_survey.h"
#include "obj_lvgl_msg.h"
#include "task_CallServer.h"
#include "obj_APCIService.h"
#include "define_string.h"
#include "obj_IoInterface.h"
#include "obj_PublicInformation.h"
#include "obj_TableSurver.h"

cJSON *Get_DT_CheckResult(void);

static Loop_vdp_common_buffer	vdp_DT_CheckOnline_mesg_queue;
static Loop_vdp_common_buffer	vdp_DT_CheckOnline_sync_queue;
vdp_task_t				task_DT_CheckOnline = {.task_run_flag = 0, .task_name = "CheckOnline"};

static void DT_CheckOnline_mesg_data_process(char* msg_data, int len)
{
	cJSON* data = cJSON_Parse(msg_data);
	cJSON* min = cJSON_GetObjectItemCaseSensitive(data, IX2V_CallBack);
	cJSON* max;
	cJSON* dtIm;
	CheckResult callback = cJSON_IsNumber(min) ? min->valueint : NULL;
	int i, j, result;
	char temp[8];

	//MyPrintJson("data", data);

	if(cJSON_IsObject(data))
	{
		Init_DT_CheckResult();
		int noticeBduCheckImFlag;
		for(noticeBduCheckImFlag = 0, i = 8; i >= 0; i--)
		{
			snprintf(temp, 8, "BDU_%d", i);
			dtIm = cJSON_GetObjectItemCaseSensitive(data, i == 0 ? IX2V_DT_IM : temp);
			
			if(cJSON_IsArray(dtIm) && cJSON_GetArraySize(dtIm) == 2 && cJSON_IsNumber(min = cJSON_GetArrayItem(dtIm, 0)) && cJSON_IsNumber(max = cJSON_GetArrayItem(dtIm, 1)))
			{
				if(i == 0)
				{
					unsigned short dtAddr;
					//不一定有BDU在线
					if(!noticeBduCheckImFlag)
					{
						for(dtAddr = 1; dtAddr <= 8&&!API_PublicInfo_Read_Bool(PB_DT_LINK_STATE); dtAddr++)
						{
							if(API_Check_BDU_Online(dtAddr))
							{
								noticeBduCheckImFlag = 1;
								API_Para_Write_Int(BDU_Enable, 1);
								break;
							}
						}
					}

					//一定没有BDU在线
					if(!noticeBduCheckImFlag)
					{
						API_Para_Write_Int(BDU_Enable, 0);
						for(j = min->valueint; j <= max->valueint&&!API_PublicInfo_Read_Bool(PB_DT_LINK_STATE); j++)
						{
							dtAddr = ((j >= 32) ? 0 : (4* j)) + 0x80;
							
							result = AI_AppointFastLinkWithEventAck(dtAddr);
							if(callback)
							{
								(callback)(data, 0, j, result);
							}

							if(min->valueint == max->valueint)
							{
								break;
							}
							usleep(10*1000);
						}
					}
				}
				else
				{
					if(API_Check_BDU_Online(i))
					{
						if(!noticeBduCheckImFlag)
						{
							noticeBduCheckImFlag = 1;
							usleep(200*1000);
							API_Notice_BDU_CheckImOnline();
							API_Para_Write_Int(BDU_Enable, 1);
							sleep(7);
						}
						result = API_Get_BDU_ImOnlineResult(i);
						if(!result)
						{
							result = API_Get_BDU_ImOnlineResult(i);
						}
					}
					else
					{
						result = 0;
					}
					usleep(200*1000);

					for(j = min->valueint; j <= max->valueint; j++)
					{
						if(callback)
						{
							(callback)(data, i, j,  (result >> ((j-1)%32)) & 1);
						}

						if(min->valueint == max->valueint)
						{
							break;
						}
					}
				}
			}
		}
	}

	cJSON_Delete(data);
	API_PublicInfo_Write(PB_LastSearchResult,cJSON_Duplicate(Get_DT_CheckResult(),1));
	API_PublicInfo_Write_Bool(PB_InSearching, 0);
	
}

static int DT_CheckOnline(cJSON* dtIm, CheckResult resultCallback)
{
	char* jsonString = NULL;
	if(cJSON_IsObject(dtIm))
	{
		cJSON_AddNumberToObject(dtIm, IX2V_CallBack, (int)resultCallback);
		jsonString = cJSON_PrintUnformatted(dtIm);
		cJSON_DeleteItemFromObjectCaseSensitive(dtIm, IX2V_CallBack);
	}
	else if(cJSON_IsArray(dtIm))
	{
		cJSON* data = cJSON_CreateObject();
		cJSON_AddItemToObject(data, IX2V_DT_IM, cJSON_Duplicate(dtIm, 1));
		cJSON_AddNumberToObject(data, IX2V_CallBack, (int)resultCallback);
		jsonString = cJSON_PrintUnformatted(data);
		cJSON_Delete(data);
	}

	if(jsonString)
	{
		push_vdp_common_queue(task_DT_CheckOnline.p_msg_buf, jsonString, strlen(jsonString) + 1);
		free(jsonString);
		return 1;
	}

	return 0;
}

int DT_CheckOnlineState(void)
{
	return task_DT_CheckOnline.task_run_flag;
}


//更新DT分机列表
static int DT_IM_ListUpdate(short addr, int online)
{
	int changeFlag = 0;
	cJSON* record = cJSON_CreateObject();
	cJSON_AddNumberToObject(record, IX2V_DT_IM, addr);
	if(online)
	{
		if(API_TB_AddByName(TB_NAME_DT_IM, record))
		{
			changeFlag = 1;
		}
	}
	else
	{
		if(API_TB_DeleteByName(TB_NAME_DT_IM, record))
		{
			changeFlag = 1;
		}
	}
	cJSON_Delete(record);

	return changeFlag;
}

//IXG更新DT分机
static cJSON* DT_CheckResult = NULL;
static int CheckResultCallback(cJSON* dtIm, char bduId, char addr, int result)
{
	cJSON* bduIdJson;
	cJSON* online;
	cJSON* offline;
	cJSON* addrJson;
	cJSON* min;
	cJSON* max;

	if(bduId > 8 || bduId < 0 || addr > 32 || addr < 1)
	{
		return 0;
	}

	DT_IM_ListUpdate(bduId*100+addr, result);

	if(!DT_CheckResult)
	{
		DT_CheckResult = cJSON_CreateObject();
	}

	if(bduId == 0)
	{
		bduIdJson = cJSON_GetObjectItemCaseSensitive(DT_CheckResult, IX2V_DT_IM);
		if(!bduIdJson)
		{
			bduIdJson = cJSON_AddObjectToObject(DT_CheckResult, IX2V_DT_IM);
		}
	}
	else if(bduId >= 1 && bduId <= 8)
	{
		char temp[8];
		snprintf(temp, 8, "BDU_%d", bduId);
		bduIdJson = cJSON_GetObjectItemCaseSensitive(DT_CheckResult, temp);
		if(!bduIdJson)
		{
			bduIdJson = cJSON_AddObjectToObject(DT_CheckResult, temp);
		}
	}

	online = cJSON_GetObjectItemCaseSensitive(bduIdJson, IX2V_ONLINE);
	if(!online)
	{
		online = cJSON_AddArrayToObject(bduIdJson, IX2V_ONLINE);
	}
	offline = cJSON_GetObjectItemCaseSensitive(bduIdJson, IX2V_OFFLINE);
	if(!offline)
	{
		offline = cJSON_AddArrayToObject(bduIdJson, IX2V_OFFLINE);
	}

	addrJson = cJSON_CreateNumber(addr);
	if(result)
	{
		MyJSON_AddArrayItem(online, addrJson);
	}
	else
	{
		MyJSON_AddArrayItem(offline, addrJson);
	}

	cJSON_Delete(addrJson);

	return 1;
}
cJSON *Get_DT_CheckResult(void)
{
	return DT_CheckResult;
}
void Init_DT_CheckResult(void)
{
	if(DT_CheckResult)
		cJSON_Delete(DT_CheckResult);
	DT_CheckResult=NULL;
}
int task_DT_CheckOnlineInit(void)
{
	int ret = 1;

	init_vdp_common_queue(&vdp_DT_CheckOnline_mesg_queue, 500, (msg_process)DT_CheckOnline_mesg_data_process, &task_DT_CheckOnline);
	init_vdp_common_queue(&vdp_DT_CheckOnline_sync_queue, 100, NULL, 								  &task_DT_CheckOnline);
	init_vdp_common_task(&task_DT_CheckOnline, MSG_ID_DT_CheckOnline, vdp_public_task, &vdp_DT_CheckOnline_mesg_queue, &vdp_DT_CheckOnline_sync_queue);
	task_DT_CheckOnline.task_StartCompleted = 1;

	return ret;
}

int API_DT_IM_Search(cJSON* dtIm)
{
	int ret = 0;

	if(!API_PublicInfo_Read_Bool(PB_InSearching))
	{
		API_PublicInfo_Write_Bool(PB_InSearching, 1);
		ret = DT_CheckOnline(dtIm, CheckResultCallback);
	}

	return ret;
}



/***************************************************************************************************************/
int DT_CheckOnlineResult(char addr, int result)
{
	DT_CHECK_RESULT_T check;
	dprintf("addr=0x%02x, result=%d\n", addr, result);
	vtk_lvgl_lock();
	lv_msg_send(MSG_DT_CHECK_ONLINE_RESULT, &check);
	vtk_lvgl_unlock();
}

void CB_DT_CheckOnline(void * s, lv_msg_t * m)
{
	vtk_lvgl_lock();
	LV_UNUSED(s);

    if(lv_msg_get_id(m) == MSG_DT_CHECK_ONLINE_REQ)
	{

    }
	vtk_lvgl_unlock();
}

void CB_DT_CheckOnlineResult(void * s, lv_msg_t * m)
{
	vtk_lvgl_lock();
	LV_UNUSED(s);

    if(lv_msg_get_id(m) == MSG_DT_CHECK_ONLINE_RESULT)
	{
		DT_CHECK_RESULT_T *check = lv_msg_get_payload(m);
		StatisticalCheckImOnlineResult(check);
		M4_DisplayDtImOnline(check);
		XD_ReportDtImOnline(check);
    }
	vtk_lvgl_unlock();
}


void send_lv_msg_check_online(void)
{
	short callAddr = 0x0084;
	vtk_lvgl_lock();
	lv_msg_send(MSG_DT_CALL_REQ, &callAddr);
	vtk_lvgl_unlock();
}

void CB_DT_Call(void * s, lv_msg_t * m)
{
	vtk_lvgl_lock();
	LV_UNUSED(s);

    if(lv_msg_get_id(m) == MSG_DT_CALL_REQ)
	{
		#if 0
		DT_CALL_RESULT_T callResult; 
		short * callAddr  = lv_msg_get_payload(m);
		cJSON *call_para,*target;
		char temp[10];

		call_para=cJSON_CreateObject();
		target=cJSON_AddObjectToObject(call_para,CallPara_Target);
		sprintf(temp,"0x%02x", *callAddr);
		cJSON_AddStringToObject(target,CallTarget_DtAddr,temp);
		callResult.result = API_CallServer_Start(DxMainCall, call_para);
		callResult.addr = *callAddr;
		lv_msg_send(MSG_DT_CALL_RESULT, &callResult);

		cJSON_Delete(call_para);
		#endif
    }
	vtk_lvgl_unlock();
}

void CB_DT_CallResult(void * s, lv_msg_t * m)
{
	vtk_lvgl_lock();
	LV_UNUSED(s);

    if(lv_msg_get_id(m) == MSG_DT_CALL_RESULT)
	{
		DT_CALL_RESULT_T *check = lv_msg_get_payload(m);
		dprintf("addr=0x%x,result=%d\n", check->addr, check->result);
    }
	vtk_lvgl_unlock();
}

/*********************************************************************************************************
**  End Of File
*********************************************************************************************************/

