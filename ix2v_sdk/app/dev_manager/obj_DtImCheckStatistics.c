/**
  ******************************************************************************
  * @file    obj_DtImCheckStatistics.c
  * @author  cao
  * @version V00.01.00
  * @date    2023.11.29
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 

#include <sys/sysinfo.h>
#include "obj_PublicInformation.h"
#include "obj_TableSurver.h"
#include "utility.h"

void DtImCheckStatisticsInit(void)
{
    char temp[100];
    int checkRecords = API_TB_CountByName(TB_NAME_DT_IM_CHECK, NULL);
    cJSON* desc = API_TB_GetDescByName(TB_NAME_DT_IM_CHECK);
    desc = (cJSON_IsObject(desc) ? cJSON_Duplicate(desc, 1) : cJSON_CreateObject());

    if((checkRecords == 0) && (!strcmp(GetEventItemString(desc, PB_DT_IM_RESULT_TIME), "")))
    {
        cJSON_ReplaceOrAddItemInObjectCaseSensitive(desc, PB_DT_IM_RESULT_TIME, cJSON_CreateString(GetCurrentTime(temp, 100, "%Y-%m-%d %H:%M:%S")));
        cJSON_ReplaceOrAddItemInObjectCaseSensitive(desc, PB_DT_IM_RESULT_ONLINE, cJSON_CreateNumber(0));
        cJSON_ReplaceOrAddItemInObjectCaseSensitive(desc, PB_DT_IM_RESULT_OFFLINE, cJSON_CreateNumber(0));
        cJSON_ReplaceOrAddItemInObjectCaseSensitive(desc, PB_DT_IM_RESULT_ADDED, cJSON_CreateNumber(0));
        API_TB_ReplaceDescByName(TB_NAME_DT_IM_CHECK, desc);
    }
    cJSON_Delete(desc);
}

//DT_IM 重新检测 通知
void API_DtImCheckModificationInform(void)
{
    char temp[100];
	int checkRecords;
    cJSON* desc = API_TB_GetDescByName(TB_NAME_DT_IM_CHECK);
    desc = (cJSON_IsObject(desc) ? cJSON_Duplicate(desc, 1) : cJSON_CreateObject());
    cJSON_ReplaceOrAddItemInObjectCaseSensitive(desc, PB_DT_IM_RESULT_TIME, cJSON_CreateString(GetCurrentTime(temp, 100, "%Y-%m-%d %H:%M:%S")));
    cJSON* where = cJSON_CreateObject();
    cJSON_AddStringToObject(where, "STATE", "ONLINE");
    checkRecords = API_TB_CountByName(TB_NAME_DT_IM_CHECK, where);
    cJSON_ReplaceOrAddItemInObjectCaseSensitive(desc, PB_DT_IM_RESULT_ONLINE, cJSON_CreateNumber(checkRecords));

    cJSON_ReplaceItemInObjectCaseSensitive(where, "STATE", cJSON_CreateString("OFFLINE"));
    checkRecords = API_TB_CountByName(TB_NAME_DT_IM_CHECK, where);
    cJSON_ReplaceOrAddItemInObjectCaseSensitive(desc, PB_DT_IM_RESULT_OFFLINE, cJSON_CreateNumber(checkRecords));

    cJSON_ReplaceItemInObjectCaseSensitive(where, "STATE", cJSON_CreateString("ADDED"));
    checkRecords = API_TB_CountByName(TB_NAME_DT_IM_CHECK, where);
    cJSON_ReplaceOrAddItemInObjectCaseSensitive(desc, PB_DT_IM_RESULT_ADDED, cJSON_CreateNumber(checkRecords));

    API_TB_ReplaceDescByName(TB_NAME_DT_IM_CHECK, desc);
    cJSON_Delete(desc);
}
