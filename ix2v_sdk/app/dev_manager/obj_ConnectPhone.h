/**
  ******************************************************************************
  * @file    obj_ConnectPhone.h
  * @author  czb
  * @version V00.01.00
  * @date    2022.5.17
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 
#ifndef _obj_ConnectPhone_H
#define _obj_ConnectPhone_H

typedef struct
{
	int 	  	saveWifiSwitch;
	int 		saveXdState;
	int 		state;
	char* 		ssid1;
	pthread_mutex_t	lock;
}IX850S_CONNECT_PHONE_RUN_S;

#endif

void IX850S_StartConnectPhoneProcess(void);

/*********************************************************************************************************
**  End Of File
*********************************************************************************************************/

