/**
  ******************************************************************************
  * @file    obj_GP_OutProxy.c
  * @author  czb
  * @version V00.01.00
  * @date    2022.06.15
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 

#include "OSTIME.h"
#include "obj_IoInterface.h"
#include <pthread.h>
#include "elog.h"
#include "utility.h"

#define TIMING_CHECK_OBJ_MAX	100

//返回1：计时器要清零，返回0：继续计时， 返回2：删除定时器
typedef int (*TimeCheckCallbak)(int);

typedef struct
{	
	int	timing;			//计时
	int	timeLimit;		//定时
	TimeCheckCallbak	callback;
}TimingCheckObj_t;

typedef struct
{	
	TimingCheckObj_t	obj[TIMING_CHECK_OBJ_MAX];
	int objCnt;
	OS_TIMER timer;
	pthread_mutex_t	lock;	
}TimingCheckRun_t;

static TimingCheckRun_t timingCheckRun = {.lock = PTHREAD_MUTEX_INITIALIZER};

static int Add_TimingCheck(TimeCheckCallbak callback, int timeLimit);
static int Del_TimingCheck(TimeCheckCallbak callback);

int API_Add_TimingCheck(TimeCheckCallbak callback, int timeLimit);
int API_Del_TimingCheck(TimeCheckCallbak callback);

static void TimingCheckCallback(void)
{
	int i;
	int result = 0;
	int objCnt;
	int timing;
	int timeLimit;
	TimeCheckCallbak callback;

	OS_RetriggerTimer(&timingCheckRun.timer);

    pthread_mutex_lock(&timingCheckRun.lock);
	objCnt = timingCheckRun.objCnt;
    pthread_mutex_unlock(&timingCheckRun.lock);

	for(i = 0; i < objCnt; )
	{
		pthread_mutex_lock(&timingCheckRun.lock);
		timingCheckRun.obj[i].timing++;
		timing = timingCheckRun.obj[i].timing;
		callback = timingCheckRun.obj[i].callback;
		timeLimit = timingCheckRun.obj[i].timeLimit;
		pthread_mutex_unlock(&timingCheckRun.lock);

		result = 0;
		if((callback) && (timing >= timeLimit))
		{
			result = (*callback)(timing);
		}

		pthread_mutex_lock(&timingCheckRun.lock);
		switch (result)
		{
			case 1:		//计数清零
				timingCheckRun.obj[i++].timing = 0;
				break;
			case 2:		//删除回调
				Del_TimingCheck(callback);
				objCnt = timingCheckRun.objCnt;
				break;
			
			default:
				i++;
				break;
		}
		pthread_mutex_unlock(&timingCheckRun.lock);
	}
}

int InternetTimeAutoUpdate(int timing);
int Check4GModules(int timing);
int RTC_DT_Server(int timing);
int IXGOnlineBrd_Callback(int timing);
int EOC_CheckTimerCallback(int timing);

void TimingCheckInit(void)
{
	timingCheckRun.objCnt = 0;
	OS_CreateTimer(&timingCheckRun.timer, TimingCheckCallback, 1000/25);		//一秒检测一次
	OS_StartTimer(&timingCheckRun.timer);

	API_Add_TimingCheck(InternetTimeAutoUpdate, 1);
	API_Add_TimingCheck(Check4GModules, 1);
	//API_Add_TimingCheck(RTC_DT_Server, 60);
	//API_Add_TimingCheck(EOC_CheckTimerCallback, 5);
}

static int Add_TimingCheck(TimeCheckCallbak callback, int timeLimit)
{
	int ret = 0;
	int i;

	if(timeLimit <= 0)
	{
		return ret;
	}
	
	for(i = 0; i < timingCheckRun.objCnt; i++)
	{
		if(timingCheckRun.obj[i].callback == callback)
		{
			timingCheckRun.obj[i].timing = 0;
			timingCheckRun.obj[i].timeLimit = timeLimit;
			break;
		}
	}

	if((i >= timingCheckRun.objCnt) && (timingCheckRun.objCnt < TIMING_CHECK_OBJ_MAX))
	{
		timingCheckRun.obj[timingCheckRun.objCnt].timing = 0;
		timingCheckRun.obj[timingCheckRun.objCnt].timeLimit = timeLimit;
		timingCheckRun.obj[timingCheckRun.objCnt].callback = callback;
		timingCheckRun.objCnt++;
		ret = 1;
	}
	return ret;
}

static int Del_TimingCheck(TimeCheckCallbak callback)
{
	int i, j;
	int ret = 0;

	for(i = 0; i < timingCheckRun.objCnt; i++)
	{
		if(timingCheckRun.obj[i].callback == callback)
		{
			if(i != timingCheckRun.objCnt-1)
			{
				for(j = i+1; j < timingCheckRun.objCnt; j++, i++)
				{
					timingCheckRun.obj[i] = timingCheckRun.obj[j];
				}
			}
			timingCheckRun.objCnt--;
			ret = 1;
			break;
		}
	}

	return ret;
}

int API_Add_TimingCheck(TimeCheckCallbak callback, int timeLimit)
{
	int ret;

    pthread_mutex_lock(&timingCheckRun.lock);
	ret = Add_TimingCheck(callback, timeLimit);
    pthread_mutex_unlock(&timingCheckRun.lock);

	return ret;
}

int API_Del_TimingCheck(TimeCheckCallbak callback)
{
	int ret;
    pthread_mutex_lock(&timingCheckRun.lock);
	ret = Del_TimingCheck(callback);
    pthread_mutex_unlock(&timingCheckRun.lock);

	return ret;
}
#if defined(PID_IX821)
int sync_time_from_vtkdevice(void)
{
	int ret=-1;
	char timezone[5]={0};
	char IpAddr[20];
	char ix_addr[11];
	cJSON *im_list;
	sprintf(ix_addr,"%s000101",GetSysVerInfo_bd());
	im_list=GetIXSByIX_ADDR(ix_addr);
	printf_json(im_list, "1212");
	if(cJSON_GetArraySize(im_list)==0)
	{
		cJSON_Delete(im_list);
		im_list=GetDevicesByIX_TYPE("IM");
		printf_json(im_list, "121201");
		if(cJSON_GetArraySize(im_list)==0)
		{
			if(atoi(GetSysVerInfo_BdRmMs()+8)!=1)
			{
				cJSON_Delete(im_list);
				sprintf(ix_addr,"%s000001",GetSysVerInfo_bd());
				im_list=GetIXSByIX_ADDR(ix_addr);
				printf_json(im_list, "12120112");
			}
		}
	}
	
	if(cJSON_GetArraySize(im_list)>0)
	{	
		if(GetJsonDataPro(cJSON_GetArrayItem(im_list,0), IX2V_IP_ADDR, IpAddr))
		{
			if(API_ReadOldPara(inet_addr(IpAddr),"1048",timezone)==1)
			{
				int tz=atoi(timezone);
				if(strcmp(GetEventItemString(cJSON_GetArrayItem(im_list,0), IX2V_Platform),"IX2V")!=0)
					tz-=12;
				if(tz!=API_Para_Read_Int(TIME_ZONE))
					set_timezone(tz);
				bprintf("API_timezone:%s:%d\n",timezone,tz);
				if(sync_time_from_sntp_server(inet_addr(IpAddr), tz)==0)
					ret=0;
			}
		}

	}
	cJSON_Delete(im_list);
	return ret;
}
#endif
int InternetTimeAutoUpdate(int timing)
{
	int ret = 1;
	static int timezone_init=0;
	
	if(API_Para_Read_Int(TIME_AutoUpdateEnable))
	{
		if(timing >= API_Para_Read_Int(TIME_UpdateTiming))
		{
			 if(sync_time_from_sntp_server(inet_addr(API_Para_Read_String2(TIME_SERVER)), API_Para_Read_Int(TIME_ZONE))!=0)
			 {
				#if defined(PID_IX821)
				sync_time_from_vtkdevice();
				#endif
			 }
		}
		else
		{
			ret = 0;
		}
	}
	if(timezone_init==0&&(ret==1||timing>=30))
	{
		timezone_init=1;
		set_timezone(API_Para_Read_Int(TIME_ZONE));
		if(ret==0&&API_Para_Read_Int(TIME_AutoUpdateEnable))
		{
			if(sync_time_from_sntp_server(inet_addr(API_Para_Read_String2(TIME_SERVER)), API_Para_Read_Int(TIME_ZONE))!=0)
			{
				#if defined(PID_IX821)
				sync_time_from_vtkdevice();
				#endif
			}

		}
	}
	return ret;
}

static int DhcpcClose( char* netDevice )
{
	FILE *pf = NULL;
	char linestr[250];
	char temp[100];

	if(pf = popen( "ps | grep udhcpc -i", "r" ))
	{
		while( fgets( linestr, 250, pf ) != NULL )
		{
			if(strstr( linestr, "grep" ) == NULL )
			{
				snprintf(temp, 100, "udhcpc -i %s", netDevice);
				if(strstr( linestr, temp) != NULL )	//  udhcpc -i usb0 -R
				{
					snprintf(temp, 100, "kill %s", strtok(linestr, " " ));
					pox_system(temp);
				}
			}
		}
		pclose( pf );
	}

	return 1;
}

static int DhcpcOpen( char* netDevice )
{
	char KO[300];
	FILE *pf = NULL;
	char linestr[250];
	int ret = 0;
	
	int send_discover_cnt = 0;

	snprintf( KO, 300, "udhcpc -i %s -R", netDevice);
	
	if(pf = popen( KO, "r" ))
	{
		while( fgets( linestr, 250, pf ) != NULL )
		{	
			if( strstr( linestr, "adding dns" ) || strstr( linestr, "obtained" ))
			{
				log_d( "Udhcpc ok ------------ \n" );
				ret = 1;
				break;
			}
			else if(strstr( linestr, "Sending discover" ))
			{
				send_discover_cnt++;
				log_d( "send_discover_cnt = %d---- \n", send_discover_cnt);
				if( send_discover_cnt >= 5)  // follow code invalid!!!
				{
					send_discover_cnt = 0;
					break;
				}
			}
			else
			{
				send_discover_cnt++;
				log_d( "error:send_discover_cnt = %d---- \n", send_discover_cnt);
				if( send_discover_cnt >= 10)  // follow code invalid!!!
				{
					send_discover_cnt = 0;
					break;
				}
			}
		}
		pclose( pf );
	}

	return ret;
}

int CheckNetDevcieExist( char* netDevice)
{
	FILE *pf = NULL;
	char linestr[250];

	if(netDevice)
	{
		if(pf=fopen("/proc/net/dev", "r"))
		{
			while(fgets( linestr, 250, pf ))
			{
				if(strstr(linestr, netDevice))
				{
					fclose(pf);
					return 1;
				}
			}
			fclose(pf);
		}
	}

	return 0;
}

int Check4GModules(int timing)
{
	static int checkTime = 2;
	int ret = 1;

	if(timing >= checkTime)
	{
		if(CheckNetDevcieExist("usb0") && GetLocalIpByDevice("usb0") < 0)
		{
			DhcpcClose("usb0");
			if(DhcpcOpen("usb0"))
			{
				checkTime = 300;
				SipNetworkManage_4GConnected();
			}
			else
			{
				checkTime = 5;
			}
		}
	}
	else
	{
		ret = 0;
	}

	return ret;
}




/*********************************************************************************************************
**  End Of File
*********************************************************************************************************/

