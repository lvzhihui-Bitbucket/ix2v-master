/**
  ******************************************************************************
  * @file    obj_SYS_VER_INFO.c
  * @author  czb
  * @version V00.01.00
  * @date    2016.5.31
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 

#include <stdio.h>
#include <sys/sysinfo.h>
#include <time.h>
#include <errno.h>

#include "vdp_uart.h"
#include "obj_SYS_VER_INFO.h"
#include "obj_IoInterface.h"
#include "obj_GetDeviceTypeAndArea.h"
#include "obj_PublicInformation.h"

#ifdef STR_FOR_CODE_VERSION
#undef STR_FOR_CODE_VERSION
#endif
#define STR_FOR_CODE_VERSION			"V1.0.0"

#ifdef NET_WLAN0
#undef NET_WLAN0
#endif
#define NET_WLAN0						"wlan0"   

#ifdef NET_ETH0
#undef NET_ETH0
#endif
#define NET_ETH0						"eth0"   

static int WaitGetStm8lSwVerFlag;
static int STM8_CODE_VERSION = 0;

int GetStm8CodeVersion(void)
{
	return STM8_CODE_VERSION;
}

char* getDateFromMacro( const char* timeIn, char *timeOut)
{
    char s_month[5];
    int month, day, year;
    static const char month_names[] = "JanFebMarAprMayJunJulAugSepOctNovDec";

    sscanf(timeIn, "%s %d %d", s_month, &day, &year);

    month = ((int)strstr(month_names, s_month) - (int)month_names)/3;

	snprintf(timeOut, 7, "%2.2d%2.2d%2.2d", year-2000, month+1, day);
	
    return timeOut;
}

char* getTimeFromMacro( const char* timeIn, char *timeOut)
{	
	int hour, min, second, i;
	
	sscanf(timeIn, "%d:%d:%d", &hour, &min, &second);
	snprintf(timeOut, 7, "%2.2d%2.2d", hour, min);

    return timeOut;
}


int GetStm8lSwVerFromStm8l(void)
{
	int i;
	
	WaitGetStm8lSwVerFlag = 1;
	
	for(i = 20; i > 0; i--)
	{
		api_uart_send_pack(UART_TYPE_STM8_VERSION_GET, NULL, 0);
		usleep(100*1000);
		if(!WaitGetStm8lSwVerFlag)
		{
			break;
		}
	}
	return !WaitGetStm8lSwVerFlag;	
}

void ReceiveStm8lSwVerFromStm8l(unsigned char* buff)
{
	char temp1[10];
	char temp2[10];
	char tempBuffer[60];

	WaitGetStm8lSwVerFlag = 0; 
	STM8_CODE_VERSION = buff[1];

	snprintf(tempBuffer, 60, "%s.%s%s", STR_FOR_CODE_VERSION, getDateFromMacro(__DATE__, temp1), getTimeFromMacro(__TIME__, temp2));
	API_PublicInfo_Write_String(PB_FW_VER, tempBuffer);
    API_Para_Write_String(FW_VER, tempBuffer);

	snprintf(tempBuffer, 60, "%02d", STM8_CODE_VERSION);
	API_PublicInfo_Write_String(PB_MCU_VER, tempBuffer);
    API_Para_Write_String(MCU_VER, tempBuffer);
}

const char* GetNetDeviceNameByTargetIp(int targetIp)
{  
	int wlanMask = inet_addr(GetSysVerInfo_mask_by_device(NET_WLAN0));
	int wlanIp = inet_addr(GetSysVerInfo_IP_by_device(NET_WLAN0));
	int lanMask = inet_addr(GetSysVerInfo_mask_by_device(NET_ETH0));
	int lanIp = inet_addr(GetSysVerInfo_IP_by_device(NET_ETH0));
	if((targetIp & lanMask) == (lanIp & lanMask))
	{
		dprintf("targetIp = %s, device=%s\n", my_inet_ntoa2(targetIp), NET_ETH0);
		return  NET_ETH0;
	}
	if(GetNetMode()&&(targetIp & wlanMask) == (wlanIp & wlanMask))
	{
		dprintf("targetIp = %s, device=%s\n", my_inet_ntoa2(targetIp), NET_WLAN0);
		return  NET_WLAN0;
	}
	
	return NET_ETH0; 
}  
const char* GetNetDeviceNameByTargetIp2(int targetIp)
{  
	int wlanMask = inet_addr(GetSysVerInfo_mask_by_device(NET_WLAN0));
	int wlanIp = inet_addr(GetSysVerInfo_IP_by_device(NET_WLAN0));
	int lanMask = inet_addr(GetSysVerInfo_mask_by_device(NET_ETH0));
	int lanIp = inet_addr(GetSysVerInfo_IP_by_device(NET_ETH0));
	if((targetIp & wlanMask) == (wlanIp & wlanMask))
	{
		dprintf("targetIp = %s, device=%s\n", my_inet_ntoa2(targetIp), NET_WLAN0);
		return  NET_WLAN0;
	}
	if((targetIp & lanMask) == (lanIp & lanMask))
	{
		dprintf("targetIp = %s, device=%s\n", my_inet_ntoa2(targetIp), NET_ETH0);
		return  NET_ETH0;
	}
	
	
	return NET_ETH0; 
}  
const char* GetSysVerInfo_IP(void)
{
	return API_PublicInfo_Read_String(PB_LAN_IP);
}

const char* GetSysVerInfo_IP_by_device(char* net_device_name)
{
	char* ip;

	if(net_device_name == NULL || net_device_name[0] == 0 || !strcmp(net_device_name, NET_ETH0))
	{
		ip = API_PublicInfo_Read_String(PB_LAN_IP);
	}
	else
	{
		ip = API_PublicInfo_Read_String(PB_WLAN_IP);
	}

	return ip;
}

const char* GetSysVerInfo_mask_by_device(char* net_device_name)
{
	char* mask;
	
	if(net_device_name == NULL || net_device_name[0] == 0 || !strcmp(net_device_name, NET_ETH0))
	{
		mask = API_PublicInfo_Read_String(PB_LAN_MASK);
	}
	else
	{
		mask = API_PublicInfo_Read_String(PB_WLAN_MASK);
	}
	
	return mask;
}

const char* GetSysVerInfo_gateway_by_device(char* net_device_name)
{
	char* gateway;
	
	if(net_device_name == NULL || net_device_name[0] == 0 || !strcmp(net_device_name, NET_ETH0))
	{
		gateway = API_PublicInfo_Read_String(PB_LAN_GATEWAY);
	}
	else
	{
		gateway = API_PublicInfo_Read_String(PB_WLAN_GATEWAY);
	}
	
	return gateway;
}

const char* GetSysVerInfo_mac_by_device(char* net_device_name)
{
	char* mac;
	
	if(net_device_name == NULL || net_device_name[0] == 0 || !strcmp(net_device_name, NET_ETH0))
	{
		mac = API_PublicInfo_Read_String(PB_LAN_MAC);
	}
	else
	{
		mac = API_PublicInfo_Read_String(PB_WLAN_MAC);
	}

	return mac;
}

int GetMyIpByTargetIp(int targetIp)
{  
	return inet_addr(GetSysVerInfo_IP_by_device(GetNetDeviceNameByTargetIp(targetIp)));
}  

const char* GetSysVerInfo_BdRmMs(void)
{
	return API_Para_Read_String2(IX_ADDR);
}

const char* GetSysVerInfo_bd(void)
{
	static char	bd_rm_ms[11] = {0};
	API_Para_Read_String(IX_ADDR, bd_rm_ms);
	bd_rm_ms[4] = 0;
	return bd_rm_ms;
}

const char* GetSysVerInfo_rm(void)
{
	static char	bd_rm_ms[11] = {0};
	API_Para_Read_String(IX_ADDR, bd_rm_ms);
	bd_rm_ms[8] = 0;
	return (bd_rm_ms + 4);
}

const char* GetSysVerInfo_BdRm(void)
{
	static char	bd_rm_ms[11] = {0};
	API_Para_Read_String(IX_ADDR, bd_rm_ms);
	bd_rm_ms[8] = 0;
	return bd_rm_ms;
}
const char* GetSysVerInfo_Ms(void)
{
	static char	bd_rm_ms[11] = {0};
	API_Para_Read_String(IX_ADDR, bd_rm_ms);
	return bd_rm_ms+8;
}


void SetSysVerInfo_BdRmMs(const char* bd_rm_ms)
{
	API_Para_Write_String(IX_ADDR, bd_rm_ms);
	API_PublicInfo_Write_String(PB_IX_ADDR, bd_rm_ms);
}

const char* GetSysVerInfo_GlobalNum(void)
{
	return API_Para_Read_String2(G_NBR);;
}

void SetSysVerInfo_GlobalNum(const char* number)
{
	API_Para_Write_String(G_NBR, number);
	API_PublicInfo_Write_String(PB_G_NBR, number);
}


const char* GetSysVerInfo_LocalNum(void)
{
	return API_Para_Read_String2(L_NBR);
}

void SetSysVerInfo_LocalNum(const char* number)
{
	API_Para_Write_String(L_NBR, number);
	API_PublicInfo_Write_String(PB_L_NBR, number);
}

void IO_SetIX_ADDR(cJSON *old, cJSON *new)
{
    if(!cJSON_Compare(old, new, 1))
    {
		API_PublicInfo_Write_String(PB_IX_ADDR, GetSysVerInfo_BdRmMs());
		if(CheckDeviceType("DH"))
		{
			if(!strcmp(GetSysVerInfo_BdRm(), "00990000"))
			{
				if(!strcmp(GetSysVerInfo_Ms(), "01"))
				{
					API_Para_Write_String(EOC_WORK_MODE, "master");
				}
				else
				{
					API_Para_Write_String(EOC_WORK_MODE, "slave");
				}
			}
		}
		IM_ProgReport();
    }
}

void IO_SetIX_NAME(cJSON *old, cJSON *new)
{
    if(!cJSON_Compare(old, new, 1))
    {
		API_PublicInfo_Write_String(PB_IX_NAME, GetSysVerInfo_name());
		IM_ProgReport();
    }
}

void IO_SetL_NBR(cJSON *old, cJSON *new)
{
    if(!cJSON_Compare(old, new, 1))
    {
		API_PublicInfo_Write_String(PB_L_NBR, GetSysVerInfo_LocalNum());
		IM_ProgReport();
    }
}

void IO_SetG_NBR(cJSON *old, cJSON *new)
{
    if(!cJSON_Compare(old, new, 1))
    {
		API_PublicInfo_Write_String(PB_G_NBR, GetSysVerInfo_GlobalNum());
		IM_ProgReport();
    }
}

const char* GetSysVerInfo_name(void)
{
	return API_Para_Read_String2(IX_NAME);;
}

void SetSysVerInfo_name(const char* name)
{
	API_Para_Write_String(IX_NAME, name);
	API_PublicInfo_Write_String(PB_IX_NAME, name);
}

int GetSysVerInfo_MyDeviceType(void)
{
	DeviceTypeAndArea_T type_area=GetDeviceTypeAndAreaByNumber(API_PublicInfo_Read_String(PB_IX_ADDR));
	return type_area.type;
}

const char* DeviceTypeToString(int type)
{
	char* ret = NULL;
	switch (type)
	{
		case TYPE_IM:
			ret = "IM";
			break;
		
		case TYPE_DS:
			ret = "DS";
			break;
		case TYPE_OS:
			ret = "OS";
			break;
		case TYPE_GL:
			ret = "GL";
			break;
		case TYPE_VM:
			ret = "VM";
			break;
		case TYPE_AC:
			ret = "AC";
			break;
		case TYPE_IXG:
			ret = "IXG";
			break;
	}

	return ret;
}

int GetSysVerInfo_WlanEn(void)
{
	//return sysVerInfo.wlan_en;
}

int GetNetMode(void)
{
	char ixNet[21] = {0};
	API_Para_Read_String(IX_NT, ixNet);
	if(!strcmp(ixNet, NET_WLAN0))
	{
		return 1;
	}
	else
	{
		return 0;
	}
}

int GetIXNetIp(void)
{
	int ip;
	ip = inet_addr(GetSysVerInfo_IP_by_device(API_Para_Read_String2(IX_NT)));
	return ip;
}
const char* GetSysVerInfo_Sn(void)
{
	return API_PublicInfo_Read_String(PB_MFG_SN);
}
int GetIXGDIP(void)
{
	int dip=API_PublicInfo_Read_Int(PB_IXG_ADDR_INT_DATA);
	if(dip<=0)
		return 1;
	else
		return dip;
}
int IfIXGMapAddr(char *bd_rm_ms,int len,char *map_addr)
{
	char buff[5]={0};
	int addr;
	if(len<8)
	{
		return 0;
	}
	if(memcmp(bd_rm_ms,GetSysVerInfo_bd(),4)!=0)
		return 0;
	memcpy(buff,bd_rm_ms+4,4);
	addr=atoi(buff);
	if(IfBDUMode())
	{
		if(addr/1000==GetIXGDIP())
		{
			int bdu_id=(addr%1000)/100;
			int im_id=addr%100;
			if(bdu_id>=1&&bdu_id<=8&&im_id>=1&&im_id<=32)
			{
				if(map_addr!=NULL)
				sprintf(map_addr,"%s%04d01",GetSysVerInfo_bd(),addr);
				
				return (addr-GetIXGDIP()*1000);
			}
		}
	}
	else
	{
		if(addr>=(GetIXGDIP()*100+1)&&addr<=(GetIXGDIP()*100+99))
		{
			if(map_addr!=NULL)
				sprintf(map_addr,"%s%04d01",GetSysVerInfo_bd(),addr);
				
			return (addr-GetIXGDIP()*100);
		}
	}
	return 0;
}
int IfIXGDtAddrToIxAddr(int dt_addr,char *ix_addr)
{
	int temp;
	if(IfBDUMode())
	{
		temp=GetIXGDIP()*1000+dt_addr;
		
	}
	else
	{
		if(dt_addr>32)
		{
			temp=((dt_addr&0x7f)>>2);
			if(temp==0)
				temp=32;
		}
		else
			temp=dt_addr;
		temp=GetIXGDIP()*100+temp;
	}
	sprintf(ix_addr,"%s%04d01",GetSysVerInfo_bd(),temp);
	return 0;
}
//#endif
/*********************************************************************************************************
**  End Of File
*********************************************************************************************************/

