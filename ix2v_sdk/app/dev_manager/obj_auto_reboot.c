
// lzh_20181025_s
/**
  ******************************************************************************
  * @file    obj_auto_reboot.c
  * @author  lzh
  * @version V00.01.00
  * @date    2018.10.27
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 

#include <sys/sysinfo.h>

#include "obj_auto_reboot.h"
#include "obj_PublicInformation.h"
//#include "obj_IoInterface.h"

#if 0
static time_t GetSystemBootTime(void)
{	
	struct sysinfo info;		
	if (sysinfo(&info)) 	
	{		
		fprintf(stderr, "Failed to get sysinfo, errno:%u, reason:%s\n", errno, strerror(errno));
		return -1;	
	}	
	return info.uptime;
}
#endif
#define	AUTO_REBOOT_POLLING_PERIOD		11//15			// 定时扫描查询时间	(mininute)

#define AUTO_REBOOT_EVERYDAY_HOUR		3			// 每天定时复位时间点 - 小时
#define	AUTO_REBOOT_EVERYDAY_MINUTE		20			// 每天定时复位时间段 - 分钟

#define AUTO_REBOOT_SYSTICK_MAX			(7*24*3600)	// 设别上电后运行最大时间 (second)


AUTO_REBOOT_T	my_auto_reboot;

// 上电检查执行标志，仅仅执行一次
static int		poweron_auto_reboot_invaliable = 0;
static OS_TIMER AutoReboot_timer;
void AutoReboot_Callback(void);
void auto_reboot_init( int every_day_enable)
{
	// 读取io参数
	my_auto_reboot.every_day_enable	= every_day_enable;
	//auto_reboot_everyday_is_enable();

	my_auto_reboot.second_counter	= 0;
	pthread_mutex_init( &my_auto_reboot.lock, 0);
	
	//my_auto_reboot.pcallback = pcall;
	
	// 防止时间总是3点钟10分以内左右，则上电后时间点若为2:50-3:10期间不复位
	if( my_auto_reboot.every_day_enable )
	{
		time_t cur_time;	
		struct tm *ltime;
		time(&cur_time);
		ltime = localtime(&cur_time);		
		if( ( ltime->tm_hour == (AUTO_REBOOT_EVERYDAY_HOUR-1) && ltime->tm_min >=50 ) || ( ltime->tm_hour == AUTO_REBOOT_EVERYDAY_HOUR && ltime->tm_min <= 10 ) )
		{		
			poweron_auto_reboot_invaliable = 1;
		}
		else
		{
			poweron_auto_reboot_invaliable = 0;
		}
	}
	OS_CreateTimer( &AutoReboot_timer, AutoReboot_Callback, 60*1000/25);
	OS_RetriggerTimer( &AutoReboot_timer );
}
#if 0
void auto_reboot_deinit(void)
{
	my_auto_reboot.pcallback = NULL;	
}

// 使能每天自动重启
// enable:  0/disable, 1/enable
// return：0/ok，-1/err
int auto_reboot_everyday_enable( int enable)
{
	char temp[20];
	pthread_mutex_lock(&my_auto_reboot.lock);
	my_auto_reboot.every_day_enable	= enable;

	sprintf(temp, "%d", (my_auto_reboot.every_day_enable ? 1 : 0));

	pthread_mutex_unlock(&my_auto_reboot.lock);	
	API_Event_IoServer_InnerWrite_All(AUTO_REBOOT, temp);
	return 0;
}
#endif
int auto_reboot_everyday_is_enable( void )
{

	my_auto_reboot.every_day_enable = API_Para_Read_Int("AUTO_REBOOT_EN");
		
	
	return my_auto_reboot.every_day_enable;
}

#if 0
// 特定场合: eg:测试时设置时间后定时器归零，等待15分钟后会复位
void auto_reboot_polling_reset( void )
{
	pthread_mutex_lock(&my_auto_reboot.lock);
	my_auto_reboot.second_counter = 0;
	pthread_mutex_unlock(&my_auto_reboot.lock);	
}
#endif
// 定时60s polling一次
// return：1/reboot ok，-1/Err, 0/have no reboot
int auto_reboot_polling(void)
{
	int every_day_enable;
	char* systemState;
	// 每15分钟一次
	pthread_mutex_lock(&my_auto_reboot.lock);
	if( ++my_auto_reboot.second_counter < AUTO_REBOOT_POLLING_PERIOD )
	{
		pthread_mutex_unlock(&my_auto_reboot.lock);
		return 0;
	}
	my_auto_reboot.second_counter = 0;
	//every_day_enable = auto_reboot_everyday_is_enable();//my_auto_reboot.every_day_enable;	
	pthread_mutex_unlock(&my_auto_reboot.lock);
	every_day_enable = auto_reboot_everyday_is_enable();
	time_t cur_time;
	time_t uptime;
	// 1、判断system tick
	uptime = GetSystemBootTime();

	printf("auto_reboot_polling: system tick is %d\n", cur_time);
	
	if( uptime >= AUTO_REBOOT_SYSTICK_MAX || every_day_enable)
	//if(every_day_enable)
	{
		struct tm *ltime;
		time(&cur_time);
		ltime = localtime(&cur_time);
		
		if((ltime->tm_year-100>=23||uptime >= AUTO_REBOOT_SYSTICK_MAX)&&ltime->tm_hour == AUTO_REBOOT_EVERYDAY_HOUR && ltime->tm_min <= AUTO_REBOOT_EVERYDAY_MINUTE )
		{		
			if( poweron_auto_reboot_invaliable )
				poweron_auto_reboot_invaliable = 0;
			else
				goto prepare_reboot;
		}
	}
	
	return 0;
	
	prepare_reboot:
	// 关屏幕状态下再进行复位处理
	//if( GetMenuOnState() == 0 )
	systemState = API_PublicInfo_Read_String(PB_System_State);
	if(systemState && !strcmp(systemState, "Standby"))
	{		
		//SaveRebootTimesToFile();
		//(*my_auto_reboot.pcallback)();
		XDRemoteDisconnect();
		HardwareRestart("auto_reboot");
		usleep(2000*1000);
	}	
	return 1;
}
void AutoReboot_Callback(void)
{
	auto_reboot_polling();
	OS_RetriggerTimer( &AutoReboot_timer );	
}

///////////////////////////////////////////////
#include "cJSON.h"
#include "task_Event.h"
#include "obj_IoInterface.h"
void auto_test_item_parse(cJSON *item)
{
	int cnt=1;
	int i;
	GetJsonDataPro(item,"cnt" ,&cnt);
	cJSON *cmd_array=cJSON_GetObjectItemCaseSensitive(item,"cmd");
	if(!cJSON_IsArray(cmd_array))
		return;
	int size=cJSON_GetArraySize(cmd_array);
	cJSON *one_cmd;
	char type[20];
	cJSON *ext_para;
	while(cnt--)
	{
		for(i=0;i<size;i++)
		{
			one_cmd=cJSON_GetArrayItem(cmd_array,i);
			GetJsonDataPro(one_cmd,"type" ,type);
			ext_para=cJSON_GetObjectItemCaseSensitive(one_cmd,"para");
			if(strcmp(type,"delay")==0)
			{
				//bprintf("11111wait=%d\n",ext_para->valueint);
				if(ext_para)
					sleep(ext_para->valueint);
				//bprintf("22222wait=%d\n",ext_para->valueint);
			}
			else if(strcmp(type,"event")==0)	
			{	
				if(ext_para)
					API_Event_Json(ext_para);
			}
			else if(strcmp(type,"rlog")==0)
			{
				if(ext_para)
				{
					cJSON *target=cJSON_GetObjectItemCaseSensitive(ext_para,"target");
					cJSON *cfg=cJSON_GetObjectItemCaseSensitive(ext_para,"cfg");
					if(target)
					{
						API_Para_Write_PublicByName(TLogIP_ADDR,target->valuestring);
						TLogStart();
					}
					if(cfg)
					{
						API_Para_Write_PublicByName(TLogEventCfg,cfg);
					}
				}
			}
			else if(strcmp(type,"rlog_end")==0)
			{
				TLogStop();
			}
			else if(strcmp(type,"revent")==0)
			{

			}
			else if(strcmp(type,"test_end")==0)
			{
				
				cJSON *para_temp=cJSON_CreateArray();
				API_Para_Write_PublicByName(AutoTestPara,para_temp);
				cJSON_Delete(para_temp);
			}
			else if(strcmp(type,"struct")==0)
			{
				if(ext_para)
					AutoTest_Process(ext_para);
			}
		}
		
	}
}
int AutoTest_Process(cJSON *test_para)
{
	if(!cJSON_IsArray(test_para))
		return -1;
	int size=cJSON_GetArraySize(test_para);
	int i;
	cJSON *one_item;
	for(i=0;i<size;i++)
	{
		one_item=cJSON_GetArrayItem(test_para,i);
		auto_test_item_parse(one_item);
	}
	return 1;
}
int AutoTest_Callback(cJSON *cmd)
{
	cJSON *test_para=cJSON_GetObjectItemCaseSensitive(cmd,"test_para");
	if(test_para)
		AutoTest_Process(test_para);
}

void AutoTest_Init(void)
{
	static int init_flag=0;
	if(init_flag)
		return;
	init_flag=1;
	cJSON* test_para = API_Para_Read_Public(AutoTestPara);
	if(cJSON_GetArraySize(test_para)>0)
	{
		cJSON *event=cJSON_CreateObject();
		if(event)
		{
			cJSON_AddStringToObject(event, EVENT_KEY_EventName,EventAutoTest);
			cJSON_AddItemToObject(event,"test_para",cJSON_Duplicate(test_para,1));
			API_Event_Json(event);
			cJSON_Delete(event);
		}
		
	}
}
// lzh_20181025_e


