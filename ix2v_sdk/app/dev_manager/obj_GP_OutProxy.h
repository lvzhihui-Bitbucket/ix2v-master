/**
  ******************************************************************************
  * @file    obj_GP_OutProxy.h
  * @author  cao
  * @version V00.01.00
  * @date    2022.06.15
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 

#ifndef _obj_GP_OutProxy_H
#define _obj_GP_OutProxy_H

#include "OSTIME.h"


// Define Object Property-------------------------------------------------------
#define GP_OUT_TYPE_BIN	    "bin"
#define GP_OUT_TYPE_PWM	    "pwm"
#define GP_OUT_TYPE_LED	    "led"
#define GP_OUT_TYPE_ACT	    "actuator"

#define GP_DATA_KEY_OUT_TYPE	"OUT_TYPE"
#define GP_DATA_KEY_OUT_NAME	"OUT_NAME"
#define GP_DATA_KEY_OUTPUT	    "OUTPUT"
#define GP_DATA_KEY_PWM_MODE	"PWM_MODE"
#define GP_DATA_KEY_PWM_DUTY	"PWM_DUTY"
#define GP_DATA_KEY_LED_MODE	"LED_MODE"

#define	RL_NUMBER					2

typedef enum
{
    RL1 = 0,
    RL2,
}act_id_t;

typedef enum
{
#if 0//#ifndef PID_IX850
    AU_PWR_EN = 0,
    AU_SWITCH,
 #endif
	SENSOR_PWN_EN,
	#ifdef PID_IX850
	EOC_RST,	
	EOC_ROLE,	
	#endif
}bin_id_t;

typedef enum
{
	IXG_DT_AU_SW=0,
	IXG_DT_VD_SW,	
}ixg_bin_id_t;

typedef enum
{
    NIGHT_VISION = 0,
	LCD_PWM_CTRL,	
}pwm_id_t;

typedef enum
{
    PWM_OFF = 0,
    PWM_ON,
    PWM_DIM_OFF,
    PWM_DIM_ON,
}pwm_mode_t;

typedef enum
{
    LED_1 = 0,
    LED_2,
}led_id_t;

typedef enum
{
    MSG_LED_ON = 0,
    MSG_LED_OFF,
    MSG_LED_FLASH_SLOW,
    MSG_LED_FLASH_FAST,
    MSG_LED_FLASH_IRREGULAR,
}led_mode_t;


typedef struct                         
{
   char* str;
   int num;
}GPIO_PROXY_ATOI;

typedef struct                         
{
    char* name;
    int state;
    int timerCnt;
    int timing;
	int phy_ctrl;
}ActuatorData;

typedef struct                         
{
    OS_TIMER timer;
    int timerFlag;
    ActuatorData actuator[RL_NUMBER];
}ActuatorCtrlRun;


// Define Object Function - Public----------------------------------------------
int IX2V_GP_OUT(cJSON* ctrlData);
void SetLockState(char* name, int ctrl);

#endif


