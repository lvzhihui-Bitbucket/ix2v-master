
/**
  ******************************************************************************
  * @file    obj_ListDataset.c
  * @author  lzh
  * @version V00.01.00
  * @date    2018.10.27
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 

#include <pthread.h>
#include <sys/sysinfo.h>
#include "utility.h"
#include "define_file.h"
#include "cJSON.h"
#include "obj_PublicInformation.h"
#include "obj_TableSurver.h"
#include "obj_IXS_Proxy.h"
#include "define_string.h"
#include "obj_IoInterface.h"
#include "task_Event.h"

typedef struct
{
	int					key;
	cJSON*				data;
	pthread_mutex_t		lock;
}ListDatasetT;

static ListDatasetT list = {.key = 1, .data = NULL, .lock = PTHREAD_MUTEX_INITIALIZER};


static int DXG_ListToDT_IM_List(void)
{
	cJSON* dtImRecord;
	cJSON* dxgList = cJSON_CreateArray();
	if(!dxgList)
	{
		dprintf("cJSON_CreateArray error\n");
		return 0;
	}
	
	API_TB_DeleteByName(TB_NAME_NEW_DT_IM, NULL);
	if(API_TB_SelectBySortByName(TB_NAME_IXG_LIST, dxgList, NULL, 0))
	{
		cJSON* dxg;
		cJSON_ArrayForEach(dxg, dxgList)
		{
			cJSON* dxgIX_ADDR = cJSON_GetObjectItemCaseSensitive(dxg, "IX_ADDR");
			cJSON* gwId = cJSON_GetObjectItemCaseSensitive(dxg, "GW_ID");
			cJSON* dxgImList = cJSON_GetObjectItemCaseSensitive(dxg, "DT_IM");
			if(cJSON_IsArray(dxgImList) && cJSON_IsNumber(gwId) && cJSON_IsString(dxgIX_ADDR) && strlen(dxgIX_ADDR->valuestring) == 10)
			{
				cJSON* im;
				cJSON_ArrayForEach(im, dxgImList)
				{
					if(cJSON_IsNumber(im))
					{
						char ixAddr[20];
						snprintf(ixAddr, 20, "%.4s%02d%02d01", dxgIX_ADDR->valuestring, gwId->valueint, im->valueint);
						dtImRecord = cJSON_CreateObject();
						cJSON_AddStringToObject(dtImRecord, "IX_ADDR", ixAddr);
						cJSON_AddItemToObject(dtImRecord, "GW_ADDR", cJSON_Duplicate(dxgIX_ADDR, 1));
						cJSON_AddStringToObject(dtImRecord, "IX_TYPE", "IM");
						cJSON_AddStringToObject(dtImRecord, "CONNECT", "DT");
						cJSON_AddItemToObject(dtImRecord, "GW_ID", cJSON_Duplicate(gwId, 1));
						cJSON_AddItemToObject(dtImRecord, "IM_ID", cJSON_Duplicate(im, 1));
						API_TB_AddByName(TB_NAME_NEW_DT_IM, dtImRecord);
						cJSON_Delete(dtImRecord);
					}
				}
			}
		}
	}

	cJSON_Delete(dxgList);

	return 1;
}

//给IXG记录添加“GW_ID”字段
int IXG_RecordAddGW_ID(cJSON* record)
{
	int gwId;
	cJSON* ixgId = cJSON_GetObjectItemCaseSensitive(record, IX2V_IXG_ID);
	if(cJSON_IsNumber(ixgId))
	{
		cJSON_DeleteItemFromObjectCaseSensitive(record, IX2V_GW_ID);
		cJSON_AddNumberToObject(record, IX2V_GW_ID, ixgId->valueint);
		return 1;
	}

	return 0;
}

static int ListMeetTheConditions(const cJSON* record, cJSON* Where)
{
    cJSON* value;
    cJSON* elementWhere;
	if(!Where)
	{
		return 1;
	}

	if(!cJSON_GetArraySize(Where))
	{
		return 1;
	}

    cJSON_ArrayForEach(elementWhere, Where)
    {
        value = cJSON_GetObjectItemCaseSensitive(record, elementWhere->string);
        if(cJSON_Compare(value, elementWhere, 1))
        {
            return 1;
        }
    }
    
    return 0;
}

/*
https://2easy-wiki.atlassian.net/wiki/spaces/IX2021RELEASE/pages/1909325845/LIST+Dataset+05-20
输入参数
{
  "ListSource": {
    "IXS":false,
    "DXG":false,
    "RES":true
    },
  "OnlineMark":false,
  "ListFilter": {"GW_ADDR":""},
  "ListType":{"IX_TYPE":"IM"},
  "ListSort":"IX_ADDR"/"",
  "ListOrder":"INC/DEC"
}
*/
#if defined(PID_IX821) || defined(PID_IX622) || defined(PID_IX611)
int GetMenuType(void)
{
	return 0;
}
#endif
void ListOutput(cJSON* para)
{
	cJSON* element;
	cJSON* record;
	cJSON* output = cJSON_CreateArray();
	if(output)
	{
		cJSON* dxgList = cJSON_CreateArray();
		cJSON* resList = cJSON_CreateArray();
		cJSON* vmList = cJSON_CreateArray();
		cJSON* ixsList = NULL;

		cJSON* listSource = cJSON_GetObjectItemCaseSensitive(para, "ListSource");
		cJSON* ixsListEnable = cJSON_GetObjectItemCaseSensitive(listSource, "IXS");
		cJSON* dxgListEnable = cJSON_GetObjectItemCaseSensitive(listSource, "DXG");
		cJSON* resListEnable = cJSON_GetObjectItemCaseSensitive(listSource, "RES");
		cJSON* onlineMark = cJSON_GetObjectItemCaseSensitive(para, "OnlineMark");
		cJSON* filter1 = cJSON_GetObjectItemCaseSensitive(para, "ListFilter");
		cJSON* filter2 = cJSON_GetObjectItemCaseSensitive(para, "ListType");
		cJSON* sort = cJSON_GetObjectItemCaseSensitive(para, "ListSort");
		cJSON* order = cJSON_GetObjectItemCaseSensitive(para, "ListOrder");

		//复制IXS记录到输出列表
		if(cJSON_IsTrue(ixsListEnable))
		{
			IXS_ProxyGetTb(&ixsList, NULL, Search);
			cJSON_ArrayForEach(element, ixsList)
			{
				record = cJSON_Duplicate(element, 1);
				cJSON_AddStringToObject(record, "ListType", "IXS");
				if(cJSON_IsTrue(onlineMark))
				{
					cJSON_AddItemToObject(record, "Online", cJSON_CreateTrue());
				}
				cJSON_AddItemToArray(output, record);
			}
		}

		//复制DT分机记录到输出列表
		if(cJSON_IsTrue(dxgListEnable))
		{
			API_TB_SelectBySortByName(TB_NAME_NEW_DT_IM, dxgList, NULL, 0);
			cJSON_ArrayForEach(element, dxgList)
			{
				record = cJSON_Duplicate(element, 1);
				cJSON_AddStringToObject(record, "ListType", "DXG_IM");
				if(cJSON_IsTrue(onlineMark))
				{
					cJSON_AddItemToObject(record, "Online", cJSON_CreateTrue());
				}
				cJSON_AddItemToArray(output, record);
			}
		}

		//复制RES记录到输出列表（剔除重复记录）
		if(cJSON_IsTrue(resListEnable))
		{
			if(cJSON_IsTrue(onlineMark))
			{
				if(!ixsList)
				{
					IXS_ProxyGetTb(&ixsList, NULL, Search);
				}
				if(cJSON_GetArraySize(dxgList) == 0)
				{
					API_TB_SelectBySortByName(TB_NAME_NEW_DT_IM, dxgList, NULL, 0);
				}
			}

			int listType = GetMenuType();
			API_TB_SelectBySortByName(listType ? TB_NAME_APT_MAT : TB_NAME_HYBRID_MAT, resList, NULL, 0);

			cJSON_ArrayForEach(element, resList)
			{
				cJSON* ixAddr = cJSON_GetObjectItemCaseSensitive(element, IX2V_IX_ADDR);
				cJSON* where = cJSON_CreateObject();
				cJSON* view = cJSON_CreateArray();
				cJSON_AddItemToObject(where, IX2V_IX_ADDR, cJSON_Duplicate(ixAddr, 1));
				if(!API_TB_SelectBySort(output, view, where, 0))
				{
					record = cJSON_Duplicate(element, 1);
					cJSON_AddStringToObject(record, "ListType", listType ? "APT_MAT":"HYBRID_MAT");
					
					if(cJSON_IsTrue(onlineMark))
					{
						if(API_TB_SelectBySort(ixsList, view, where, 0) || API_TB_SelectBySort(dxgList, view, where, 0))
						{
							cJSON_AddItemToObject(record, "Online", cJSON_CreateTrue());
						}
					}
					cJSON_AddItemToArray(output, record);
				}
				cJSON_Delete(where);
				cJSON_Delete(view);
			}
			if(API_Para_Read_Int(SIP_ENABLE)==1)
			{
				API_TB_SelectBySortByName(TB_NAME_VM_MAT, vmList, NULL, 0);
				cJSON_ArrayForEach(element, vmList)
				{
					record = cJSON_Duplicate(element, 1);
					cJSON_AddStringToObject(record, "ListType", "VM_MAT");
					if(cJSON_IsTrue(onlineMark))
					{
						cJSON_AddItemToObject(record, "Online", cJSON_CreateTrue());
					}
					cJSON_AddItemToArray(output, record);
				}
			}
		}

		if(ixsList)
		{
			cJSON_Delete(ixsList);
		}
		cJSON_Delete(dxgList);
		cJSON_Delete(resList);
		cJSON_Delete(vmList);

		//删除不符合条件的记录
		int outputCnt = cJSON_GetArraySize(output);
		while(outputCnt > 0)
		{
			outputCnt--;
			element = cJSON_GetArrayItem(output, outputCnt);
			if((!ListMeetTheConditions(element, filter1)) || (!ListMeetTheConditions(element, filter2)))
			{
				cJSON_DeleteItemFromArray(output, outputCnt);
			}
		}

		//排序
		if(cJSON_IsString(sort) && cJSON_IsString(order))
		{
			if(strlen(sort->valuestring))
			{
				if(!strcmp(order->valuestring, "INC"))
				{
					SortTableView(output, sort->valuestring, 0);
				}
				else if(!strcmp(order->valuestring, "DEC"))
				{
					SortTableView(output, sort->valuestring, 1);
				}
			}
		}

		//加上Key值
		pthread_mutex_lock(&list.lock);
		cJSON_ArrayForEach(element, output)
		{
			cJSON_AddNumberToObject(element, "ListKey", list.key++);
		}
		cJSON_Delete(list.data);
		list.data = output;
		API_PublicInfo_Write_String(PB_DATASET_TIME, get_time_string());		
		API_PublicInfo_Write(PB_DATASET_PARA, para);
		API_Event_By_Name(Event_DatasetUpdate);
		pthread_mutex_unlock(&list.lock);
	}
}

/*列表更新
updateTable:选择更新的列表,IXS,DXG,RES
*/
static void DataSetListUpdate(char* updateTable)
{
	if(!updateTable)
	{
		dprintf("DataSetListUpdate: update table error\n");
		return;
	}

	
	if(API_PublicInfo_Read_Bool(PB_DATASET_LOCK))
	{
		dprintf("DataSetListUpdate: Update lock, can't be update\n");
		return;
	}

	cJSON* para = cJSON_Parse(API_Para_Read_String2(DATASET_PARA));
	if(!para)
	{
		dprintf("DataSetListUpdate: DATASET_PARA error\n");
		return;
	}

	cJSON* listSource = cJSON_GetObjectItemCaseSensitive(para, "ListSource");
	cJSON* ixsListEnable = cJSON_GetObjectItemCaseSensitive(listSource, "IXS");
	cJSON* dxgListEnable = cJSON_GetObjectItemCaseSensitive(listSource, "DXG");
	cJSON* resListEnable = cJSON_GetObjectItemCaseSensitive(listSource, "RES");
	cJSON* onlineMark = cJSON_GetObjectItemCaseSensitive(para, "OnlineMark");

	if(!strcmp(updateTable, "RES"))
	{
		if(!cJSON_IsTrue(resListEnable))
		{
			//dprintf("DataSetListUpdate: No need to update\n");
			return;
		}
	}
	else if(!strcmp(updateTable, "IXS"))
	{
		//不包括ixs列表，也不需要检查在线
		if(!cJSON_IsTrue(ixsListEnable) && !cJSON_IsTrue(onlineMark))
		{
			//dprintf("DataSetListUpdate: No need to update\n");
			return;
		}
	}
	else if(!strcmp(updateTable, "DXG"))
	{
		//不包括dxg列表，也不需要检查在线
		if(!cJSON_IsTrue(dxgListEnable) && !cJSON_IsTrue(onlineMark))
		{
			//dprintf("DataSetListUpdate: No need to update\n");
			return;
		}
	}
	else
	{
		dprintf("DataSetListUpdate: update table error\n");
		return;
	}

	ListOutput(para);
	cJSON_Delete(para);
}

//上电更新Dataset list
void InitDataSetList(void)
{
	cJSON* para = cJSON_Parse(API_Para_Read_String2(DATASET_PARA));
	if(!para)
	{
		dprintf("DataSetListUpdate: DATASET_PARA error\n");
		return;
	}
	ListOutput(para);
	cJSON_Delete(para);
}

//返回的JSON使用后，需要释放
//返回的JSON使用后，需要释放
//返回的JSON使用后，需要释放
//返回的JSON使用后，需要释放
cJSON* GetDataSetList(void)
{
	cJSON* output = NULL;

	pthread_mutex_lock(&list.lock);
	output = cJSON_Duplicate(list.data, 1);
	pthread_mutex_unlock(&list.lock);

	return output;
}


int ShellCmd_Dataset(cJSON *cmd)
{
	cJSON* output = NULL;
	char* operation = GetShellCmdInputString(cmd, 1);
	if(!strcmp(operation, "update"))
	{
		cJSON* jsonPara = cJSON_Parse(GetShellCmdInputString(cmd, 2));
		if(!jsonPara)
		{
			jsonPara = cJSON_Parse(API_Para_Read_String2(DATASET_PARA));
		}
		ListOutput(jsonPara);
		cJSON_Delete(jsonPara);
	}

	output = GetDataSetList();
	ShellCmdPrintf("Dataset time :%s\n", API_PublicInfo_Read_String(PB_DATASET_TIME));
	ShellCmdPrintf("Dataset para :\n");
	ShellCmdPrintJson(API_PublicInfo_Read(PB_DATASET_PARA));
	ShellCmdPrintf("Dataset:\n");
	ShellCmdPrintJson(output);

	cJSON_Delete(output);

	return 1;
}


//IXS表有更新：更新dataset list
int IXS_ListUpdateForDataset(cJSON *cmd)
{
	DataSetListUpdate("IXS");
	return 1;
}

//IXG表有更新：更新DT分机表并更新dataset list
int DXG_ListUpdateForDataset(cJSON *cmd)
{
	DXG_ListToDT_IM_List();
	DataSetListUpdate("DXG");
	return 1;
}

//RES表有更新：更新dataset list
int RES_ListUpdateForDataset(cJSON *cmd)
{
	if(!GetMenuType())
	{
		DXG_ListToDT_IM_List();
	}
	DataSetListUpdate("RES");
	return 1;
}

cJSON *ListFilterByHybridASSO(cJSON* source_list)
{
	cJSON *asso_array=API_Para_Read_Public(ASSO);
	int asso_size=cJSON_GetArraySize(asso_array);
	int i;
	cJSON *where;
	cJSON *view=NULL;
	if(asso_size>0)
	{
		view=cJSON_CreateArray();
		cJSON *asso_item;
		where = cJSON_CreateObject();
		cJSON *gw_id=cJSON_AddNumberToObject(where, "GW_ID", 0);
		API_TB_SelectBySort(source_list, view, where, 0);
		for(i=0;i<asso_size;i++)
		{
			asso_item=cJSON_GetArrayItem(asso_array,i);
			if(asso_item->valueint)
			{
				cJSON_SetNumberValue(gw_id,asso_item->valueint);
				API_TB_SelectBySort(source_list, view, where, 0);
			}
			else
			{
				cJSON *element;
				cJSON_ArrayForEach(element, source_list)
				{
					char* ixType = GetEventItemString(element,  "IX_TYPE");
					char* ixAddr = GetEventItemString(element,  "IX_ADDR");
					if(!strcmp(ixType, "IM") && strlen(ixAddr) == 10 && !strncmp(ixAddr+4, "00", 2))
					{
						cJSON_AddItemToArray(view, cJSON_Duplicate(element,1));
					}
				}
			}
		}
		cJSON_Delete(where);
	}
	else
		view=cJSON_Duplicate(source_list,1);

	return view;
}

void IO_SetMenuType(cJSON *old, cJSON *new)
{
    if(!cJSON_Compare(old, new, 1))
    {
		API_Event_By_Name(Event_MAT_Update);
    }
}