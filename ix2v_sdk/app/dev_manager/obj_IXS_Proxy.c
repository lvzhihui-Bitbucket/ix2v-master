/**
 ******************************************************************************
 * @file    obj_IXS_Proxy.c
 * @author  czb
 * @version V00.01.00
 * @date    2023.02.15
 * @brief
 ******************************************************************************
 * @attention
 *
 *
 * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
 ******************************************************************************
 */
#include "OSTIME.h"
#include "obj_IoInterface.h"
#include <pthread.h>
#include "cJSON.h"
#include "utility.h"
#include "obj_PublicUnicastCmd.h"
#include "task_Event.h"
#include "define_string.h"
#include "define_file.h"
#include "vtk_udp_stack_device_update.h"
#include "obj_PublicInformation.h"
#include "obj_IXS_Proxy.h"
#include "obj_TableSurver.h"

static IXS_Proxy_t ixsRun;

static char* GetServerTypeString(IXS_Proxy_Type_e type)
{
	char* ret;

	switch(type)
	{
		case SearchServer:
			ret = "SearchServer";
			break;
		case SearchClient:
			ret = "SearchClient";
			break;
		
		default:
			ret = "SearchWaitNet";
			break;
	}

	return ret;
}

static int IXS_ProxyRoleIsServer(void)
{
	char* searchRole;
	int ret = 0;

	searchRole = API_Para_Read_String2(Search_Role);
	if(searchRole && !strcmp(searchRole, "Server"))
	{
		ret =1;
	}

	return ret;
}

static int IXS_UpdateTbEnable(int time)
{
	pthread_mutex_lock(&ixsRun.lock);
	ixsRun.searchEnable = 1;
	pthread_mutex_unlock(&ixsRun.lock);

	return 2;
}

static int IXS_UpdateTbTimer(int time)
{
	API_Event_By_Name(Event_IXS_ReqUpdateTb);
	return 2;
}


static int IXS_ProxyUpdateTbInform(void)
{
	cJSON* sendData = cJSON_CreateObject();
	char* sendString;

	cJSON_AddStringToObject(sendData, "MD5", ixsRun.newMd5);
	cJSON_AddStringToObject(sendData, "TIME", ixsRun.lastTime);
	cJSON_AddNumberToObject(sendData, "DEV_CNT", ixsRun.newDevCnt);

    //添加系统类型，系统类型存在IO参数中
	char* sysType = API_Para_Read_String2(SysType);
    cJSON_AddStringToObject(sendData, "STC", sysType ? sysType : "UNKNOWN");

	sendString = cJSON_PrintUnformatted(sendData);

	api_udp_device_update_send_data(inet_addr(DEVICE_SEARCH_MULTICAST_ADDR), htons(UPDATE_DEV_TABLE_INFORM), sendString, strlen(sendString)+1);
	free(sendString);
	cJSON_Delete(sendData);

	return 1;
}

static int IXS_SetMyselfAsServer(void)
{
	ixsRun.serverType = SearchServer;
	ixsRun.serverIp = 0;
	API_PublicInfo_Write_String(PB_SEARCH_SERVER_TYPE, GetServerTypeString(ixsRun.serverType));
	API_PublicInfo_Write_String(PB_SEARCH_SERVER_IP, my_inet_ntoa2(ixsRun.serverIp));
	IXS_ProxyUpdateTbInform();
	API_Event_By_Name(Event_IXS_ReqUpdateTb);
	return 1;
}

static int IXS_ProxyHearbeat(int time)
{
	int timeCnt;
	pthread_mutex_lock(&ixsRun.lock);

	switch (ixsRun.serverType)
	{
		case SearchServer:
			//发送心跳指令
			IXS_ProxyUpdateTbInform();
			break;

		case SearchClient:
			timeCnt = API_Para_Read_Int(Search_Client_TimeoutCnt);
			timeCnt = (timeCnt < 3 ? 3 : timeCnt);
			if(++ixsRun.ixsProxyHbCnt >= timeCnt)
			{
				if(IXS_ProxyRoleIsServer())
				{
					API_Para_Write_Int(Search_Last_Server_Enable, 1);
					IXS_SetMyselfAsServer();
				}
			}
			break;

		case SearchWaitNet:
			break;
		
		default:
			break;
	}

	pthread_mutex_unlock(&ixsRun.lock);
	return 1;
}

static int IXS_ProxyRecInformTimer(int time)
{
	pthread_mutex_lock(&ixsRun.lock);
	ixsRun.timeSinceLastInform++;
	pthread_mutex_unlock(&ixsRun.lock);
	return 0;
}

int GetIXS_ProxyRecInformTime(void)
{
	int ret;
	pthread_mutex_lock(&ixsRun.lock);
	ret = ixsRun.timeSinceLastInform;
	pthread_mutex_unlock(&ixsRun.lock);
	return ret;
}

void IXS_ProxyInit(void)
{
	cJSON* infoTb;
	cJSON* element;
	char tempString[100];

	ixsRun.serverIp = 0;
	ixsRun.devTable = NULL;
	ixsRun.devCnt = 0;
	ixsRun.Md5[0] = 0;
	ixsRun.newMd5[0] = 0;
	ixsRun.lastTime[0] = 0;
	ixsRun.searchEnable = 1;
	ixsRun.initFlag = 0;
	ixsRun.serverType = SearchWaitNet;
	ixsRun.timeSinceLastInform = 0;
	API_Add_TimingCheck(IXS_ProxyRecInformTimer, 1);
	pthread_mutex_init(&ixsRun.lock, 0);

	snprintf(tempString, 100, "%s/%s", UserResFolder, DeviceTableTftpFileName);
	infoTb = GetJsonFromFile(tempString);
	if(infoTb)
	{
		API_TB_DeleteByName(TB_NAME_DEV_TB, NULL);
		cJSON_ArrayForEach(element, infoTb)
		{
			API_TB_AddByName(TB_NAME_DEV_TB, element);
		}

		CalculateJsonMd5(infoTb, tempString);

		pthread_mutex_lock(&ixsRun.lock);
		ixsRun.devTable = infoTb;
		ixsRun.devCnt = cJSON_GetArraySize(infoTb);
		ixsRun.newDevCnt = ixsRun.devCnt;
		snprintf(ixsRun.lastTime, 50, "%s", get_time_string());
		snprintf(ixsRun.Md5, 33, "%s", tempString);
		snprintf(ixsRun.newMd5, 33, "%s", tempString);
		
		API_Event_By_Name(Event_IXS_ReportTbChanged);
		pthread_mutex_unlock(&ixsRun.lock);
	}

	pthread_mutex_lock(&ixsRun.lock);
	API_PublicInfo_Write_String(PB_SEARCH_SERVER_TYPE, GetServerTypeString(ixsRun.serverType));
	API_PublicInfo_Write_String(PB_SEARCH_SERVER_IP, my_inet_ntoa2(ixsRun.serverIp));
	API_PublicInfo_Write_String(PB_SEARCH_LIST_MD5, ixsRun.newMd5);
	API_PublicInfo_Write_String(PB_SEARCH_MY_LIST_MD5, ixsRun.Md5);
	API_PublicInfo_Write_Int(PB_SEARCH_LIST_SIZE, ixsRun.newDevCnt);
	API_PublicInfo_Write_String(PB_SEARCH_LIST_TIME, ixsRun.lastTime);
	API_PublicInfo_Write_Int(PB_IXS_Interval, ixsRun.timeSinceLastInform);
	pthread_mutex_unlock(&ixsRun.lock);
}

//重新初始化IXS代理
void IXS_ProxyReinit(void)
{
	pthread_mutex_lock(&ixsRun.lock);
	cJSON_Delete(ixsRun.devTable);
	API_Del_TimingCheck(IXS_UpdateTbTimer);
	API_Del_TimingCheck(IXS_UpdateTbEnable);
	API_Del_TimingCheck(IXS_ProxyHearbeat);
	pthread_mutex_unlock(&ixsRun.lock);
	IXS_ProxyInit();
	API_Event_By_Name(EventNetLinkState);
}


//IXS网络连接成功回调函数
int IXS_ProxyNetConnectedCallback(cJSON *cmd)
{
	//网络连接，开始设置搜索服务器
	char* lanState = API_PublicInfo_Read_String(PB_SUB_LAN_STATE);
	char* wlanState = API_PublicInfo_Read_String(PB_SUB_WLAN_STATE);
	if((lanState && !strcmp(lanState, "Connected")) || (wlanState && !strcmp(wlanState, "Connected")))
	{
		pthread_mutex_lock(&ixsRun.lock);
		if(!ixsRun.initFlag)
		{
			ixsRun.initFlag = 1;
			ixsRun.serverType = SearchClient;
			ixsRun.serverIp = 0;
			API_Add_TimingCheck(IXS_ProxyHearbeat, API_Para_Read_Int(Search_Server_Inform_Timer));

			if(IXS_ProxyRoleIsServer())
			{
				//上次做服务器，马上作为服务器
				if(API_Para_Read_Int(Search_Last_Server_Enable))
				{
					IXS_SetMyselfAsServer();
				}
			}
		}
		pthread_mutex_unlock(&ixsRun.lock);
	}
}

int IXS_ProxyReceiveUpdateTbInform(int ip, char* data)
{
	char* net = API_Para_Read_String2(IX_NT);
	int mask = GetLocalMaskAddrByDevice(net);
	int myIp = GetLocalIpByDevice(net);
	if((myIp & mask) == (ip & mask))
	{
		cJSON* rData = cJSON_Parse(data);
		cJSON* devCnt;
		pthread_mutex_lock(&ixsRun.lock);
		char* md5 = cJSON_GetStringValue(cJSON_GetObjectItemCaseSensitive(rData, "MD5"));
		if(md5)
		{
			snprintf(ixsRun.newMd5, 33, "%s", md5);
			snprintf(ixsRun.lastTime, 50, "%s", cJSON_GetStringValue(cJSON_GetObjectItemCaseSensitive(rData, "TIME")));
			devCnt = cJSON_GetObjectItemCaseSensitive(rData, "DEV_CNT");
			ixsRun.newDevCnt = devCnt ? devCnt->valueint : 0;

			ixsRun.serverType = SearchClient;
			ixsRun.timeSinceLastInform = 0;
			ixsRun.ixsProxyHbCnt = 0;
			ixsRun.serverIp = ip;
			if(API_Para_Read_Int(Search_Last_Server_Enable))
			{
				API_Para_Write_Int(Search_Last_Server_Enable, 0);
			}
			API_PublicInfo_Write_String(PB_SEARCH_SERVER_TYPE, GetServerTypeString(ixsRun.serverType));
			API_PublicInfo_Write_String(PB_SEARCH_SERVER_IP, my_inet_ntoa2(ixsRun.serverIp));
			API_PublicInfo_Write_String(PB_SEARCH_LIST_MD5, ixsRun.newMd5);
			API_PublicInfo_Write_Int(PB_SEARCH_LIST_SIZE, ixsRun.newDevCnt);
			API_PublicInfo_Write_String(PB_SEARCH_LIST_TIME, ixsRun.lastTime);

			#ifdef PID_IX821
			if(strcmp(ixsRun.newMd5,ixsRun.Md5)!=0)
				API_Event_By_Name(EventM4LEDDisplayImOnline);
			#endif
		}
		API_PublicInfo_Write_String(PB_ServerSysType, GetEventItemString(rData, "STC"));
		pthread_mutex_unlock(&ixsRun.lock);

		cJSON_Delete(rData);
	}

	return 1;
}

static int SortJsonDevTable(const cJSON* table, const char* key1, const char* key2, int order)
{
	int recordCnt;
	int i, j, ifExchange;
	cJSON* record1;
	cJSON* record2;

	if(!cJSON_IsArray(table) || key1 == NULL)
	{
		return 0;
	}

	recordCnt = cJSON_GetArraySize(table);
    for (i=0; i < recordCnt-1; i++) /* 外循环为排序趟数，recordCnt个数进行recordCnt-1趟 */
	{
		/* 内循环为每趟比较的次数，第i趟比较recordCnt-i次 */
        for (j = 0; j < recordCnt-1-i; j++) 
		{
			record1 = cJSON_GetObjectItemCaseSensitive(cJSON_GetArrayItem(table, j), key1);
			record2 = cJSON_GetObjectItemCaseSensitive(cJSON_GetArrayItem(table, j+1), key1);
			if(record1->type !=  record1->type)
			{
				return 0;
			}
			else
			{
				ifExchange = 0;
				if(cJSON_IsNumber(record1))
				{
					//降序 || 升序，交换位置
					if(record1->valuedouble == record2->valuedouble)
					{
						ifExchange = 2;
					}
					else if(((order) && (record1->valuedouble < record2->valuedouble)) || 
							((!order) && (record1->valuedouble > record2->valuedouble)))
					{
						ifExchange = 1;
					}
				}
				else if (cJSON_IsString(record1))
				{
					if(strcmp(record1->valuestring, record2->valuestring) == 0)
					{
						ifExchange = 2;
					}
					//降序 || 升序，交换位置
					else if(((order) && (strcmp(record1->valuestring, record2->valuestring) < 0)) || 
					   ((!order) && (strcmp(record1->valuestring, record2->valuestring) > 0)))
					{
						ifExchange = 1;
					}
				}
				else
				{
					return 0;
				}

				if(ifExchange == 2)
				{
					ifExchange = 0;
					record1 = cJSON_GetObjectItemCaseSensitive(cJSON_GetArrayItem(table, j), key2);
					record2 = cJSON_GetObjectItemCaseSensitive(cJSON_GetArrayItem(table, j+1), key2);
					if(record1->type !=  record1->type)
					{
						return 0;
					}
					else
					{
						if(cJSON_IsNumber(record1))
						{
							//降序 || 升序，交换位置
							if(((order) && (record1->valuedouble < record2->valuedouble)) || 
								((!order) && (record1->valuedouble > record2->valuedouble)))
							{
								ifExchange = 1;
							}
						}
						else if (cJSON_IsString(record1))
						{
							//降序 || 升序，交换位置
							if(((order) && (strcmp(record1->valuestring, record2->valuestring) < 0)) || 
							((!order) && (strcmp(record1->valuestring, record2->valuestring) > 0)))
							{
								ifExchange = 1;
							}
						}
						else
						{
							return 0;
						}
					}
				}

				if(ifExchange)
				{
					record1 = cJSON_DetachItemFromArray(table, j);
					cJSON_InsertItemInArray(table, j+1, record1);
				}
			}
		}
    }

	return 1;
}

static int IXS_SendRemoteEvent(int ip, const char* eventName)
{
	int ret = 0;
	cJSON *event = cJSON_CreateObject();
	cJSON_AddStringToObject(event, EVENT_KEY_EventName, eventName);
	cJSON_AddStringToObject(event, IX2V_SOURCE, GetSysVerInfo_IP_by_device(GetNetDeviceNameByTargetIp(ip)));
	ret = API_XD_EventJson(ip, event);
	cJSON_Delete(event);
	return ret;
}

int IXS_ProxyReqUpdateTb_CallBack(cJSON *event)
{
	cJSON *ipTb;
	cJSON *element;
	cJSON *infoTb;
	int searchEnable;
	int serverType;
	int serverIp;
	char tempString[100] = {0};
	int listChangeFlag = 0;
	char* net;
	char* ipString;

	pthread_mutex_lock(&ixsRun.lock);
	serverType = ixsRun.serverType;
	serverIp = ixsRun.serverIp;
	searchEnable = ixsRun.searchEnable;
	pthread_mutex_unlock(&ixsRun.lock);

	switch (serverType)
	{
		case SearchServer:
			if(searchEnable)
			{
				pthread_mutex_lock(&ixsRun.lock);
				ixsRun.searchEnable = 0;
				pthread_mutex_unlock(&ixsRun.lock);

				net = API_Para_Read_String2(IX_NT);
				ipTb = cJSON_CreateArray();
				cJSON_AddItemToArray(ipTb, GetMyJsonInfo(net));
				IXS_Search(net, NULL, GetShellCmdJsonString(event, IX2V_BD_NBR), GetShellCmdJsonString(event, IX2V_IX_TYPE), ipTb);
				//根据IX2V_IX_ADDR按升序排序,如果IX2V_IX_ADDR相同则按mfg_sn
				SortJsonDevTable(ipTb, IX2V_IX_ADDR, IX2V_MFG_SN, 0);

				infoTb = cJSON_CreateArray();
				IXS_Info(ipTb, infoTb);
				cJSON_Delete(ipTb);

				//删除UpTime字段
				cJSON_ArrayForEach(element, infoTb)
				{
					cJSON_DeleteItemFromObjectCaseSensitive(element, IX2V_UpTime);
				}
				
				CalculateJsonMd5(infoTb, tempString);

				pthread_mutex_lock(&ixsRun.lock);
				snprintf(ixsRun.lastTime, 50, "%s", get_time_string());
				API_PublicInfo_Write_String(PB_SEARCH_LIST_TIME, ixsRun.lastTime);

				//设备表变化，删掉旧表，使用新表
				if(strcmp(tempString, ixsRun.Md5))
				{
					cJSON_Delete(ixsRun.devTable);
					ixsRun.devTable = infoTb;
					ixsRun.devCnt = cJSON_GetArraySize(infoTb);
					ixsRun.newDevCnt = ixsRun.devCnt;
					snprintf(ixsRun.Md5, 33, "%s", tempString);
					snprintf(ixsRun.newMd5, 33, "%s", tempString);

					snprintf(tempString, 100, "%s/%s", UserResFolder, DeviceTableTftpFileName);

					SetJsonToFile(tempString, ixsRun.devTable);
					API_TB_DeleteByName(TB_NAME_DEV_TB, NULL);
					cJSON_ArrayForEach(element, infoTb)
					{
						if(!API_TB_AddByName(TB_NAME_DEV_TB, element))
						{
							dprintf("add DEV_TB error\n");
						}
					}

					API_PublicInfo_Write_String(PB_SEARCH_LIST_MD5, ixsRun.newMd5);
					API_PublicInfo_Write_String(PB_SEARCH_MY_LIST_MD5, ixsRun.Md5);
					API_PublicInfo_Write_Int(PB_SEARCH_LIST_SIZE, ixsRun.newDevCnt);
					infoTb = NULL;
					listChangeFlag = 1;
				}
				pthread_mutex_unlock(&ixsRun.lock);

				if(infoTb)
				{
					cJSON_Delete(infoTb);
				}

				//搜索列表最小时间间隔定时
				API_Add_TimingCheck(IXS_UpdateTbEnable, API_Para_Read_Int(Search_Device_Min_Timer));
				API_Add_TimingCheck(IXS_UpdateTbTimer, API_Para_Read_Int(Search_Device_Timer));
			}

			if(listChangeFlag)
			{
				pthread_mutex_lock(&ixsRun.lock);
				IXS_ProxyUpdateTbInform();
				pthread_mutex_unlock(&ixsRun.lock);
				#ifdef PID_IX821
				API_Event_By_Name(EventM4LEDDisplayImOnline);
				#endif
			}
			ipString = GetEventItemString(event,  IX2V_SOURCE);
			if(ipString[0])
			{
				IXS_SendRemoteEvent(inet_addr(ipString), listChangeFlag ? Event_IXS_ReportTbChanged : Event_IXS_ReportTbUnchanged);
			}
			API_Event_By_Name(listChangeFlag ? Event_IXS_ReportTbChanged : Event_IXS_ReportTbUnchanged);
			break;

		case SearchClient:
			IXS_SendRemoteEvent(serverIp, Event_IXS_ReqUpdateTb);
			API_Del_TimingCheck(IXS_UpdateTbTimer);
			break;

		case SearchWaitNet:
			API_Del_TimingCheck(IXS_UpdateTbTimer);
			break;
	}
	return 1;
}

int IXS_UpdateData_CallBack(cJSON *event)
{
	char tempString[100] = {0};
	int listChangeFlag = 0;
	int i;
	const char* RECORD_KEY_STRING[] = { IX2V_Platform, IX2V_MFG_SN, IX2V_IP_ADDR, IX2V_IX_ADDR, IX2V_G_NBR, IX2V_L_NBR, 
										IX2V_IX_NAME, IX2V_IX_TYPE, IX2V_IX_Model, IX2V_DevModel, IX2V_FW_VER, IX2V_HW_VER, NULL};
	cJSON* record = cJSON_GetObjectItemCaseSensitive(event, "information");

	pthread_mutex_lock(&ixsRun.lock);
	if(record && (ixsRun.serverType == SearchServer))
	{
		char* mfgSn = GetEventItemString(record,  IX2V_MFG_SN);
		if(strlen(mfgSn) == 12)
		{
			cJSON* where = cJSON_CreateObject();
			cJSON* view = cJSON_CreateArray();
			cJSON_AddStringToObject(where, IX2V_MFG_SN, mfgSn);

			cJSON* newRecord = cJSON_CreateObject();
			if(API_TB_SelectBySort(ixsRun.devTable, view, where, 0))
			{
				for(i = 0; RECORD_KEY_STRING[i]; i++)
				{
					char* value = GetEventItemString(record,  RECORD_KEY_STRING[i]);
					if(value[0])
					{
						cJSON_AddStringToObject(newRecord, RECORD_KEY_STRING[i], value);
					}
				}

				if(API_TB_Update(ixsRun.devTable, where, newRecord))
				{
					listChangeFlag = 1;
				}
			}
			else
			{
				for(i = 0; RECORD_KEY_STRING[i]; i++)
				{
					cJSON_AddStringToObject(newRecord, RECORD_KEY_STRING[i], GetEventItemString(record,  RECORD_KEY_STRING[i]));
				}

				if(API_TB_Add(ixsRun.devTable, newRecord))
				{
					listChangeFlag = 1;
				}
			}

			if(listChangeFlag)
			{
				CalculateJsonMd5(ixsRun.devTable, tempString);
				if(strcmp(tempString, ixsRun.Md5))
				{
					cJSON* element;
					ixsRun.devCnt = cJSON_GetArraySize(ixsRun.devTable);
					ixsRun.newDevCnt = ixsRun.devCnt;
					snprintf(ixsRun.Md5, 33, "%s", tempString);
					snprintf(ixsRun.newMd5, 33, "%s", tempString);

					snprintf(tempString, 100, "%s/%s", UserResFolder, DeviceTableTftpFileName);

					SetJsonToFile(tempString, ixsRun.devTable);
					API_TB_DeleteByName(TB_NAME_DEV_TB, NULL);
					cJSON_ArrayForEach(element, ixsRun.devTable)
					{
						if(!API_TB_AddByName(TB_NAME_DEV_TB, element))
						{
							dprintf("add DEV_TB error\n");
						}
					}

					API_PublicInfo_Write_String(PB_SEARCH_LIST_MD5, ixsRun.newMd5);
					API_PublicInfo_Write_String(PB_SEARCH_MY_LIST_MD5, ixsRun.Md5);
					API_PublicInfo_Write_Int(PB_SEARCH_LIST_SIZE, ixsRun.newDevCnt);
					IXS_ProxyUpdateTbInform();
					#ifdef PID_IX821
					API_Event_By_Name(EventM4LEDDisplayImOnline);
					#endif
				}
			}
			cJSON_Delete(where);
			cJSON_Delete(view);
			cJSON_Delete(newRecord);
		}
	}
	pthread_mutex_unlock(&ixsRun.lock);
	return 1;
}

static cJSON* IXS_ProxyGetSearchDevTb(void)
{
	cJSON* table = NULL;
	pthread_mutex_lock(&ixsRun.lock);
	table = cJSON_Duplicate(ixsRun.devTable, 1);
	//服务端有更新设备表
	if(strcmp(ixsRun.Md5, ixsRun.newMd5))
	{
		API_Event_By_Name(Event_IXS_GetRemoteTb);
	}
	pthread_mutex_unlock(&ixsRun.lock);

	return table;
}

int IXS_GetRemoteTb_CallBack(cJSON *event)
{
	pthread_mutex_lock(&ixsRun.lock);
	int serverIp = ixsRun.serverIp;
	int getTableFlag = ((strcmp(ixsRun.Md5, ixsRun.newMd5)) ? 1 : 0);
	pthread_mutex_unlock(&ixsRun.lock);
	if(getTableFlag)
	{
		cJSON* tmepTable = NULL;
		char tempMd5[100] = {0};
		int getTableSuccessFlag = 0;
		if(API_Para_Read_Int(GetSearchListType))
		{
			tmepTable = cJSON_CreateArray();
			if(API_RemoteTableSelect(ixsRun.serverIp, TB_NAME_DEV_TB, tmepTable, NULL, 0))
			{
				CalculateJsonMd5(tmepTable, tempMd5);
				getTableSuccessFlag = 1;
			}
		}
		else
		{
			if(!Api_TftpReadFileFromDevice(ixsRun.serverIp, UserResFolder, DeviceTableTftpFileName, UserResFolder, DeviceTableTftpFileName))
			{
				snprintf(tempMd5, 100, "%s/%s", UserResFolder, DeviceTableTftpFileName);
				tmepTable = GetJsonFromFile(tempMd5);
				CalculateJsonMd5(tmepTable, tempMd5);
				getTableSuccessFlag = 1;
			}
		}

		if(getTableSuccessFlag)
		{
			//设备表变化，删掉旧表，使用新表
			pthread_mutex_lock(&ixsRun.lock);
			if(strcmp(tempMd5, ixsRun.Md5))
			{
				cJSON_Delete(ixsRun.devTable);
				ixsRun.devTable = tmepTable;
				strcpy(ixsRun.Md5, tempMd5);
				API_PublicInfo_Write_String(PB_SEARCH_MY_LIST_MD5, ixsRun.Md5);
				ixsRun.devCnt = cJSON_GetArraySize(tmepTable); 
				API_Event_By_Name(Event_IXS_ReportTbChanged);
				tmepTable = NULL;
				#ifdef PID_IX821
				API_Event_By_Name(EventM4LEDDisplayImOnline);
				#endif
			}
			pthread_mutex_unlock(&ixsRun.lock);
		}

		if(tmepTable)
		{
			cJSON_Delete(tmepTable);
		}
	}
	
	return 1;
}

static cJSON* IXS_ProxyGetDevTb(IXS_Proxy_Tb_Select_e tb)
{
	cJSON* retTb;
	cJSON* searchTb;
	cJSON* searchElement;
	cJSON* r8001Element;
	cJSON* newRecord;
	int modifyFlag;

	switch (tb)
	{
		case R8001:
			pthread_mutex_lock(&ixsRun.lock);
			retTb = cJSON_CreateArray();
			API_TB_SelectBySortByName(TB_NAME_R8001, retTb, NULL, 0);
			pthread_mutex_unlock(&ixsRun.lock);
			break;
		case Search:
			retTb = IXS_ProxyGetSearchDevTb();
			break;

		default:
			pthread_mutex_lock(&ixsRun.lock);
			retTb = cJSON_CreateArray();
			API_TB_SelectBySortByName(TB_NAME_R8001, retTb, NULL, 0);
			pthread_mutex_unlock(&ixsRun.lock);

			searchTb = IXS_ProxyGetSearchDevTb();

			cJSON_ArrayForEach(searchElement, searchTb)
			{
				modifyFlag = 0;
				newRecord = cJSON_Duplicate(searchElement, 1);

				//在8001表中，则修改表
				cJSON_ArrayForEach(r8001Element, retTb)
				{
					cJSON* r8001Sn = cJSON_GetObjectItemCaseSensitive(r8001Element, IX2V_MFG_SN);
					//有序列号对比序列号
					if(r8001Sn)
					{
						if(cJSON_Compare(r8001Sn, cJSON_GetObjectItemCaseSensitive(searchElement, IX2V_MFG_SN), 1))
						{
							modifyFlag = 1;
							break;
						}
					}
					//没有序列号对比房号
					else
					{
						if(cJSON_Compare(cJSON_GetObjectItemCaseSensitive(r8001Element, IX2V_IX_ADDR), cJSON_GetObjectItemCaseSensitive(searchElement, IX2V_IX_ADDR), 1))
						{
							modifyFlag = 1;
							break;
						}
					}
				}

				//在8001表中，用搜索结果替换静态表记录
				if(modifyFlag)
				{
					cJSON_ReplaceItemViaPointer(retTb, r8001Element, newRecord);
				}
				//不在8001表中，则添加到表
				else
				{
					if(retTb == NULL)
					{
						retTb = cJSON_CreateArray();
					}
					cJSON_AddItemToArray(retTb, newRecord);
				}
			}
			cJSON_Delete(searchTb);
			break;
	}

	return retTb;
}



int IXS_ProxyUpdateDevTbRsp(int ip, PublicUnicastCmdData dataIn, PublicUnicastCmdData* dataOut)
{
	cJSON* event = cJSON_CreateObject();
	cJSON_AddStringToObject(event, EVENT_KEY_EventName, Event_IXS_ReqUpdateTb);
	cJSON_AddStringToObject(event, IX2V_SOURCE, my_inet_ntoa2(ip));
	API_Event_Json(event);
	cJSON_Delete(event);
	return 0;
}

cJSON* IXS_ProxyGetState(void)
{
	static cJSON* IXS_ProxyState = NULL;

	if(IXS_ProxyState)
	{
		cJSON_Delete(IXS_ProxyState);
	}

	IXS_ProxyState = cJSON_CreateObject();

	pthread_mutex_lock(&ixsRun.lock);
	cJSON_AddStringToObject(IXS_ProxyState, "SERVER_TYPE", GetServerTypeString(ixsRun.serverType));
	cJSON_AddStringToObject(IXS_ProxyState, "SERVER_IP", my_inet_ntoa2(ixsRun.serverIp));
	cJSON_AddNumberToObject(IXS_ProxyState, "NOW_CNT", ixsRun.devCnt);
	cJSON_AddNumberToObject(IXS_ProxyState, "NEW_CNT", ixsRun.newDevCnt);
	cJSON_AddStringToObject(IXS_ProxyState, "NOW_MD5", ixsRun.Md5);
	cJSON_AddStringToObject(IXS_ProxyState, "NEW_MD5", ixsRun.newMd5);
	cJSON_AddStringToObject(IXS_ProxyState, "TIME", ixsRun.lastTime);
	pthread_mutex_unlock(&ixsRun.lock);

	return IXS_ProxyState;
}

int IXS_ProxyGetTb(cJSON** result, pthread_mutex_t* lock, IXS_Proxy_Tb_Select_e tb)
{
	if(lock)
	{
		pthread_mutex_lock(lock);
		//cJSON_Delete(*result);
		*result = IXS_ProxyGetDevTb(tb);
		pthread_mutex_unlock(lock);
	}
	else
	{
		//cJSON_Delete(*result);
		*result = IXS_ProxyGetDevTb(tb);
	}

	return 1;
}

void IXS_ProxyCheckIfListChange(void)
{
	pthread_mutex_lock(&ixsRun.lock);
	//服务端有更新设备表
	if(strcmp(ixsRun.Md5, ixsRun.newMd5))
	{
		API_Event_By_Name(Event_IXS_GetRemoteTb);
	}
	pthread_mutex_unlock(&ixsRun.lock);
}

void IM_ProgReport(void)
{
	char* serverIpString = API_PublicInfo_Read_String(PB_SEARCH_SERVER_IP);
	if(serverIpString)
	{
		int serverIp = inet_addr(serverIpString);
		cJSON* event = cJSON_CreateObject();
		cJSON_AddStringToObject(event, EVENT_KEY_EventName, EventIM_PROG_REPORT);
		cJSON_AddItemToObject(event, "information", GetMyJsonInfo(GetNetDeviceNameByTargetIp(serverIp)));
		serverIp ? API_XD_EventJson2(serverIp, event) : API_Event_Json(event);
		cJSON_Delete(event);
	}
}

/*********************************************************************************************************
**  End Of File
*********************************************************************************************************/