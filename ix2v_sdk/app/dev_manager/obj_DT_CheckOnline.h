/**
  ******************************************************************************
  * @file    obj_DT_CheckOnline.h
  * @author  cao
  * @version V00.01.00
  * @date    2022.06.15
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 

#include "cJSON.h"

#ifndef _obj_DT_CheckOnline_H
#define _obj_DT_CheckOnline_H

// Define Object Property-------------------------------------------------------
typedef int(*CheckResult)(cJSON* checkIm, char bduId, char im, int result);

typedef struct                         
{
    int cnt;
    unsigned char addr[100];
    CheckResult resultCallback;
}DT_CHECK_DATA_T;

typedef struct                         
{
    unsigned char addr;
    int result;
}DT_CHECK_RESULT_T;

typedef struct                         
{
    unsigned char addr;
    int result;
}DT_CALL_RESULT_T;

// Define Object Function - Public----------------------------------------------
int API_DT_IM_Search(cJSON* dtIm);
int DT_CheckOnlineState(void);

#endif


