/**
  ******************************************************************************
  * @file    obj_DeviceManage.c
  * @author  czb
  * @version V00.01.00
  * @date    2016.5.31
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 
	  //cao_20170304  整个文件

#include "obj_DeviceManage.h"
#include "vtk_udp_stack_c5_ipc_cmd.h"
#include "obj_GetInfoByIp.h"
#include "obj_IX_Report.h"
#include "obj_SearchIxProxy.h"
#include "cJSON.h"
#include "define_file.h"
#include "elog.h"
#include "task_Event.h"
#include "obj_IoInterface.h"
#include "obj_TableSurver.h"
#include "obj_Certificate.h"
#include "obj_PublicInformation.h"
#include "obj_IXS_Proxy.h"

#ifdef NET_WLAN0
#undef NET_WLAN0
#endif
#define NET_WLAN0						"wlan0"   

#ifdef NET_ETH0
#undef NET_ETH0
#endif
#define NET_ETH0						"eth0"   

static cJSON *rebootFlagJson = NULL;
int deviceLinkingReportIp;
int deviceRemoteManageFlag = 0;

int GetDeviceRemoteManageFlag(void)
{
	return deviceRemoteManageFlag;
}

int SetDeviceRemoteManageFlag(int flag)
{
	deviceRemoteManageFlag = flag;
}

int DeviceRecoverToDefault(int recover_scene)
{
	struct in_addr temp;
	int ip;
	char ip_str[20] = {0};
	
	if(recover_scene == 0 ||recover_scene == 1 ||recover_scene == 2)
	{
		//IX2V test API_Event_IoServer_Factory_Default();
	}
	
	if(recover_scene == 0 ||recover_scene == 1 ||recover_scene == 2)
	{
		//IX2V test call_record_delete_all();
	}

	if(recover_scene == 1 ||recover_scene == 2)
	{
		//IX2V test API_ResourceTb_ClearWithFail();
	}
}


void DeviceManageSingleLinking(UDP_MSG_TYPE *pdata)
{
	DEVICE_SINGLE_LINKING_REQ_T *pRequest = (DEVICE_SINGLE_LINKING_REQ_T*)pdata->pbuf;
	DEVICE_SINGLE_LINKING_RSP_T response;
	
	if(pRequest->IpAddr == GetLocalIpByDevice(GetNetDeviceNameByTargetIp(pRequest->IpAddr)))
	{
		memcpy(&response, pRequest, sizeof(DEVICE_SINGLE_LINKING_REQ_T));
		//本机在线
		if(ntohs(pRequest->subAddr) == 0)
		{
			response.result = 0;
		}
		//检测其它设备
		else
		{
			response.result = 1;
		}
		api_udp_c5_ipc_send_rsp(pdata->target_ip, pdata->cmd|0x80, pdata->id, &response, sizeof(DEVICE_SINGLE_LINKING_RSP_T));
	}
	
}

void DeviceManageMultipleLinkingStart(UDP_MSG_TYPE *pdata)
{
	DEVICE_MULTIPLE_LINKING_START_REQ_T *pRequest = (DEVICE_MULTIPLE_LINKING_START_REQ_T*)pdata->pbuf;
	DEVICE_MULTIPLE_LINKING_START_RSP_T response;
	uint8 i;

	if(pRequest->IpAddr == GetLocalIpByDevice(GetNetDeviceNameByTargetIp(pRequest->IpAddr)))
	{
		deviceLinkingReportIp = pdata->target_ip;
		
		//不能接受多设备检测
		response.result = 1;
			
		api_udp_c5_ipc_send_rsp(pdata->target_ip, pdata->cmd|0x80, pdata->id, &response, sizeof(DEVICE_MULTIPLE_LINKING_START_RSP_T));
	}
}

void DeviceManageMultipleLinkingReport(uint8 deviceNum, DEVICE_LINKING_RESULT_T* pDevice)
{
	DEVICE_MULTIPLE_LINKING_REPORT_REQ_T request;
	DEVICE_MULTIPLE_LINKING_REPORT_RSP_T response;
	uint8 i;
	
	request.IpAddr = GetLocalIpByDevice(GetNetDeviceNameByTargetIp(deviceLinkingReportIp));;
	request.subAddrNum = deviceNum;
	memcpy(&request.subDevice, pDevice, deviceNum);
	for(i = 0; i < deviceNum; i++)
	{
		request.subDevice[i].addr = htons(request.subDevice[i].addr);
	}
	api_udp_c5_ipc_send_req(deviceLinkingReportIp, CMD_DEVIC_MULTIPLE_LINKING_REPORT_REQ, &request, sizeof(DEVICE_MULTIPLE_LINKING_REPORT_REQ_T) , &response, sizeof(DEVICE_MULTIPLE_LINKING_REPORT_RSP_T));
}

int DeviceManageRebootRequest(int ip, DEVICE_REBOOT_REQ_T data, DEVICE_REBOOT_RSP_T* result)
{
	int resultLen = sizeof(DEVICE_REBOOT_RSP_T);
	
	return api_udp_c5_ipc_send_req(ip, CMD_DEVIC_REBOOT_REQ, &data, sizeof(DEVICE_REBOOT_REQ_T) , result, &resultLen);
}


void WriteRebootFlag(int* ip, int cnt)
{
	SearchIxProxyRspData ixProxyData;
	char *string = NULL;
	cJSON *wlanJson = NULL;
	cJSON *lanJson = NULL;
	cJSON *reportJson = NULL;
	int i;
		
	if(rebootFlagJson == NULL)
	{
		rebootFlagJson = GetJsonFromFile(REBOOT_FLAG_PATH);
		if(rebootFlagJson == NULL)
		{
			rebootFlagJson = cJSON_CreateObject();
			cJSON_AddItemToObject(rebootFlagJson, NET_WLAN0, wlanJson = cJSON_CreateArray());
			cJSON_AddItemToObject(rebootFlagJson, NET_ETH0, lanJson = cJSON_CreateArray());
		}
	}
	else
	{
		wlanJson = cJSON_GetObjectItemCaseSensitive(rebootFlagJson, NET_WLAN0);
		lanJson = cJSON_GetObjectItemCaseSensitive(rebootFlagJson, NET_ETH0);
	}

	if(cnt == 0 || ip == NULL)
	{
		API_SearchIxProxyLinked(NULL, 10, &ixProxyData, 2);
		if(ixProxyData.deviceCnt == 0)
		{
			dprintf("ixProxyData.deviceCnt=%d\n", ixProxyData.deviceCnt);
			return;
		}
		else
		{
			cnt = ixProxyData.deviceCnt;
			dprintf("ixProxyData.deviceCnt=%d\n", ixProxyData.deviceCnt);
			for(i = 0; i < cnt; i++)
			{
				reportJson = (strcmp(GetNetDeviceNameByTargetIp(inet_addr(ixProxyData.data[i].ip_addr)), NET_ETH0) ? wlanJson : lanJson);
				cJSON_AddItemToArray(reportJson, cJSON_CreateString(ixProxyData.data[i].ip_addr));
				dprintf("ixProxyData.data[%d].ip_addr=%s\n", i, ixProxyData.data[i].ip_addr);
			}
		}
	}
	else
	{
		cnt = (cnt > 10 ? 10 : cnt);
		
		for(i = 0; i < cnt; i++)
		{
			reportJson = (strcmp(GetNetDeviceNameByTargetIp(ip[i]), NET_ETH0) ? wlanJson : lanJson);
			cJSON_AddItemToArray(reportJson, cJSON_CreateString(my_inet_ntoa2(ip[i])));
		}
	}
	
	MakeDir(TEMP_Folder);
	SetJsonToFile(REBOOT_FLAG_PATH, rebootFlagJson);
}

void RebootReport(char* netDeviceName)
{
	cJSON *wlanJson = NULL;
	cJSON *lanJson = NULL;
	cJSON *reportJson = NULL;
	int cnt, saveFileFlag;
	int ip;
	char* ipString;
	char message[100];

	rebootFlagJson = GetJsonFromFile(REBOOT_FLAG_PATH);
	if(rebootFlagJson == NULL)
	{
		rebootFlagJson = cJSON_CreateObject();
		cJSON_AddItemToObject(rebootFlagJson, NET_WLAN0, wlanJson = cJSON_CreateArray());
		cJSON_AddItemToObject(rebootFlagJson, NET_ETH0, lanJson = cJSON_CreateArray());
	}
	else
	{
		wlanJson = cJSON_GetObjectItemCaseSensitive(rebootFlagJson, NET_WLAN0);
		lanJson = cJSON_GetObjectItemCaseSensitive(rebootFlagJson, NET_ETH0);
	}

	if(!strcmp(netDeviceName, NET_ETH0))
	{
		reportJson = lanJson;
	}
	else
	{
		reportJson = wlanJson;
	}

	saveFileFlag = cJSON_GetArraySize(reportJson);

	while(cnt = cJSON_GetArraySize(reportJson))
	{
		ipString = cJSON_GetStringValue(cJSON_GetArrayItem(reportJson, cnt-1));
		dprintf("------------------------------------ IxReport reboot ip=%s\n", ipString);
		ip = inet_addr(ipString);

		snprintf(message, 100, "%s restart completed.", GetSysVerInfo_name());
		API_Ring2(ip, message, 5);

		IxReport(ip, REPORT_REBOOT_REQ);

		cJSON_DeleteItemFromArray(reportJson, cnt-1);
	}
	
	if(saveFileFlag)
	{
		SetJsonToFile(REBOOT_FLAG_PATH, rebootFlagJson);
	}
}



void DeviceManageRebootProcess(UDP_MSG_TYPE *pdata)
{
	
	DEVICE_REBOOT_REQ_T *pRequest = (DEVICE_REBOOT_REQ_T*)pdata->pbuf;
	DEVICE_REBOOT_RSP_T response;

	if(pRequest->IpAddr == GetLocalIpByDevice(GetNetDeviceNameByTargetIp(pRequest->IpAddr)))
	{
		//重启本机
		if(ntohs(pRequest->subAddr) == 0)
		{
			if(GetDeviceRemoteManageFlag())
			{
				return;
			}
			SetDeviceRemoteManageFlag(1);
			memcpy(&response, pRequest, sizeof(DEVICE_REBOOT_REQ_T));
			response.result = 0;
			response.time = htons(40);		//40s
			api_udp_c5_ipc_send_rsp(pdata->target_ip, pdata->cmd|0x80, pdata->id, &response, sizeof(DEVICE_REBOOT_RSP_T));
			WriteRebootFlag(&(pdata->target_ip), 1);
			//IX2V test SendRebootLogToStm8(11, GetCurMenuCnt());//dh_20200303
			HardwareRestart("Other device requess");
			SetDeviceRemoteManageFlag(0);
		}
		//重启其它设备
		else
		{
			memcpy(&response, pRequest, sizeof(DEVICE_REBOOT_REQ_T));
			response.result = 2;
			response.time = 0xFFFF;
			api_udp_c5_ipc_send_rsp(pdata->target_ip, pdata->cmd|0x80, pdata->id, &response, sizeof(DEVICE_REBOOT_RSP_T));
		}
	}
}

void DeviceManageRecover(UDP_MSG_TYPE *pdata)
{
	
	DEVICE_RECOVER_REQ_T *pRequest = (DEVICE_RECOVER_REQ_T*)pdata->pbuf;
	DEVICE_RECOVER_RSP_T response;

	if(pRequest->IpAddr == GetLocalIpByDevice(GetNetDeviceNameByTargetIp(pRequest->IpAddr)))
	{
		//恢复本机
		if(ntohs(pRequest->subAddr) == 0)
		{
			memcpy(&response, pRequest, sizeof(DEVICE_RECOVER_REQ_T));
			
			DeviceRecoverToDefault(2);

			response.result = 0;	//成功
			
			api_udp_c5_ipc_send_rsp(pdata->target_ip, pdata->cmd|0x80, pdata->id, &response, sizeof(DEVICE_RECOVER_RSP_T));
			
			SoftRestar("Restore to default");
		}
		//恢复其它设备
		else
		{
			memcpy(&response, pRequest, sizeof(DEVICE_REBOOT_REQ_T));
			response.result = 1;
			api_udp_c5_ipc_send_rsp(pdata->target_ip, pdata->cmd|0x80, pdata->id, &response, sizeof(DEVICE_RECOVER_RSP_T));
		}
	}
}
//czn_20170329_s
uint8 DeviceManageSingleLinkingReq(int target_ip, uint16 subAddr)
{
	DEVICE_SINGLE_LINKING_REQ_T request;
	DEVICE_SINGLE_LINKING_RSP_T response = {0};
	unsigned int rlen = sizeof(DEVICE_SINGLE_LINKING_RSP_T);
	request.IpAddr = target_ip;
	request.subAddr = htons(subAddr);
	request.checkType = 0;
	if(api_udp_c5_ipc_send_req(target_ip, CMD_DEVIC_SINGLE_LINKING_REQ, &request, sizeof(DEVICE_SINGLE_LINKING_REQ_T), &response, &rlen) == 0)
	{
		printf("!*!*!*!&!!!!!!ip = %08x saddr = %04x result = %d\n",response.IpAddr,response.subAddr,response.result);
		if(request.IpAddr ==  response.IpAddr && request.subAddr == response.subAddr && response.result == 0)
		{
			//设备在线
			return 0;
		}
		else
		{
			//不在线
			return 1;
		}
	}
	else
	{
		return 2;//ipdevice offline 
	}
}
//czn_20170329_e

int SoftRestar(char* reqRebootInfo)
{
	if(IsMyProxyLinked())
	{
		SetIxProxyRebootFlag(1);
		return -1;
	}
	else
	{
		log_i("IX2V Request reboot :%s", reqRebootInfo ? reqRebootInfo : "");
		system("date >> /mnt/nand1-2/UserData/hwreboot");
		char cmd[200];
		sprintf(cmd,"echo %s >> /mnt/nand1-2/UserData/hwreboot",reqRebootInfo ? reqRebootInfo : "unkown");
		system(cmd);
		sync();
		API_Event_NameAndMsg(EventWillReboot, reqRebootInfo);

		sync();
		usleep(2000*1000);
		WriteRebootFlag(NULL, 0);
		api_notify_global_reset();
		SetIxProxyRebootFlag(0);
		usleep(3000*1000);
		return system("reboot");
	}
}

int SoftForceRestart(char* reqRebootInfo)
{
	if(IsMyProxyLinked())
	{
		SetIxProxyRebootFlag(1);
		return -1;
	}
	else
	{
		log_i("IX2V Request reboot :%s", reqRebootInfo ? reqRebootInfo : "");
		system("date >> /mnt/nand1-2/UserData/hwreboot");
		char cmd[200];
		sprintf(cmd,"echo %s >> /mnt/nand1-2/UserData/hwreboot",reqRebootInfo ? reqRebootInfo : "unkown");
		system(cmd);
		sync();
		API_Event_NameAndMsg(EventWillReboot, reqRebootInfo);

		WriteRebootFlag(NULL, 0);
		sync();
		sleep(2);
		api_notify_global_reset();
		SetIxProxyRebootFlag(0);
		sleep(3);

		return system("reboot");
	}
}

void HardwareRestart(char* reqRebootInfo)
{

	if(IsMyProxyLinked())
	{
		SetIxProxyRebootFlag(1);
		return -1;
	}
	else
	{
		log_i("IX2V Request reboot :%s", reqRebootInfo ? reqRebootInfo : "");
		system("date >> /mnt/nand1-2/UserData/hwreboot");
		char cmd[200];
		sprintf(cmd,"echo %s >> /mnt/nand1-2/UserData/hwreboot",reqRebootInfo ? reqRebootInfo : "unkown");
		system(cmd);
		sync();
		API_Event_NameAndMsg(EventWillReboot, reqRebootInfo);

		SetIxProxyRebootFlag(1);
		WriteRebootFlag(NULL, 0);
		api_notify_global_reset();
		SetIxProxyRebootFlag(0);
		sleep(3);
		
		return system("reboot");
	}
}

int SatHibridProgByRecord(const cJSON* record, int checkIxAddr)
{
	int ret = 1;
	
	if(checkIxAddr)
	{
		char* myIxAddr = GetSysVerInfo_BdRmMs();
		if(myIxAddr && !strcmp(GetEventItemString(record, "IX_ADDR"), myIxAddr))
		{
			ret = 1;
		}
		else
		{
			ret = 0;
		}
	}

	if(ret)
	{
		cJSON* ioWriteRecord = cJSON_CreateObject();
		cJSON_AddItemToObject(ioWriteRecord, SysType, cJSON_Duplicate(cJSON_GetObjectItemCaseSensitive(record, "SYS_TYPE"), 1));
		cJSON_AddItemToObject(ioWriteRecord, ASSO, cJSON_Duplicate(cJSON_GetObjectItemCaseSensitive(record, "ASSO"), 1));
		cJSON_AddItemToObject(ioWriteRecord, IX_NAME, cJSON_Duplicate(cJSON_GetObjectItemCaseSensitive(record, "IX_NAME"), 1));
		cJSON_AddItemToObject(ioWriteRecord, G_NBR, cJSON_Duplicate(cJSON_GetObjectItemCaseSensitive(record, "G_NBR"), 1));
		cJSON_AddItemToObject(ioWriteRecord, L_NBR, cJSON_Duplicate(cJSON_GetObjectItemCaseSensitive(record, "L_NBR"), 1));

		if(!checkIxAddr)
		{
			cJSON_AddItemToObject(ioWriteRecord, IX_ADDR, cJSON_Duplicate(cJSON_GetObjectItemCaseSensitive(record, "IX_ADDR"), 1));
		}

		ret = API_Para_Write_Public(ioWriteRecord);
		cJSON_Delete(ioWriteRecord);
	}

	return ret;
}

int SyncTableFromRemote(int ip, char* tbName)
{
	int ret = 0;
	char tempString[200];

	if(tbName)
	{
		snprintf(tempString, 200, "TB_DESC_%s", tbName);

		cJSON* tbDesc = API_GetRemotePb(ip, tempString);
		if(tbDesc)
		{
			cJSON* record = NULL;
			cJSON* view = cJSON_CreateArray();
			for(int i = 0; i < 2; i++)
			{
				if(ret = API_RemoteTableSelect(ip, tbName, view, NULL, 0))
				{
					break;
				}
			}

			if(ret)
			{
				ret = 1;
				API_TB_DeleteByName(tbName, NULL);
				cJSON_ArrayForEach(record, view)
				{
					API_TB_AddByName(tbName, record);
				}
				API_TB_ReplaceDescByName(tbName, tbDesc);
				if(!strcmp(tbName, TB_NAME_HYBRID_MAT))
				{
					API_Event_By_Name(Event_MAT_Update);
				}
			}

			cJSON_Delete(tbDesc);
			cJSON_Delete(view);
		}
	}

	return ret;
}

int EventDH_HybridProgOnlineCallback(cJSON* cmd)
{
	int result = 0;

	char* targetIpString = GetEventItemString(cmd, "TARGET_IP_ADDR");
	int ip = inet_addr(targetIpString);
	cJSON* record = cJSON_GetObjectItemCaseSensitive(cmd, "PROG_RECORD");
	cJSON* table = cJSON_GetObjectItemCaseSensitive(cmd, "SYNC_TABLE");
	cJSON* tableElement;
	char* ixAddr;
	char* certState = API_PublicInfo_Read_String(PB_CERT_STATE);

	if(certState && !strcmp(certState, "Delivered"))
	{
		API_Delete_CERT();
	}

	if(record)
	{
		ixAddr = GetEventItemString(record, "IX_ADDR");
		result = SatHibridProgByRecord(record, 1);
		if(result && table)
		{
			cJSON_ArrayForEach(tableElement, table)
			{
				result = SyncTableFromRemote(ip, tableElement->valuestring);
				if(!result)
				{
					dprintf("SyncTableFromRemote error:tb=%s\n", tableElement->valuestring);
					break;
				}
			}
		}
	}
	else
	{
		ixAddr = GetSysVerInfo_BdRmMs();
		if(table)
		{
			cJSON_ArrayForEach(tableElement, table)
			{
				result = SyncTableFromRemote(ip, tableElement->valuestring);
				if(!result)
				{
					dprintf("SyncTableFromRemote error:tb=%s\n", tableElement->valuestring);
					break;
				}
			}
		}

		if(result)
		{
			result = 0;
			cJSON* view = cJSON_CreateArray();
			API_TB_SelectBySortByName(TB_NAME_HYBRID_SAT, view, NULL, 0);
			cJSON_ArrayForEach(tableElement, view)
			{
				if(ixAddr && !strcmp(GetEventItemString(tableElement, "IX_ADDR"), ixAddr))
				{
					result = SatHibridProgByRecord(tableElement, 1);
					break;
				}
			}
			cJSON_Delete(view);
		}
	}

	if(result)
	{
		result = ((API_Create_DCFG_SELF(SysType, NULL) && API_Create_CERT(CERT_DCFG_SELF_NAME) && API_Check_CERT()) ? 1 : 0);
	}

	cJSON* reportEvent = cJSON_CreateObject();
	cJSON_AddStringToObject(reportEvent, IX2V_EventName, EventDH_HybridProgOnlineReport);
	if(table)
	{
		cJSON_AddItemToObject(reportEvent, "SYNC_TABLE", cJSON_Duplicate(table, 1));
	}
	record = cJSON_AddObjectToObject(reportEvent, "PROG_RECORD");
	cJSON_AddStringToObject(reportEvent, "RESULT", result ? "success" : "error");

	cJSON_AddStringToObject(record, "IX_ADDR", ixAddr);
	cJSON_AddItemToObject(record, "SYS_TYPE", cJSON_Duplicate(API_Para_Read_Public(SysType), 1));
	cJSON_AddItemToObject(record, "ASSO", cJSON_Duplicate(API_Para_Read_Public(ASSO), 1));
	cJSON_AddItemToObject(record, "IX_NAME", cJSON_Duplicate(API_Para_Read_Public(IX_NAME), 1));
	cJSON_AddItemToObject(record, "G_NBR", cJSON_Duplicate(API_Para_Read_Public(G_NBR), 1));
	cJSON_AddItemToObject(record, "L_NBR", cJSON_Duplicate(API_Para_Read_Public(L_NBR), 1));
	API_XD_EventJson(inet_addr(GetEventItemString(cmd, "REPORT_IP_ADDR")), reportEvent);

	cJSON_Delete(reportEvent);
	return 1;
}

int IM_ResgistCallBack(cJSON *event)
{
	char tempString[100] = {0};
	cJSON* record = cJSON_GetObjectItemCaseSensitive(event, "information");
	char* ixaddr = GetEventItemString(record,  IX2V_IX_ADDR);

	snprintf(tempString, 100, "%s resgist\n", ixaddr);
	API_TIPS_Ext_Time(tempString, 3);

	API_Event_IXCallAction("IxMainCall", ixaddr, 1);
	return 1;
}

/*
{
	"Platform":	"IX2V",
	"MFG_SN":	"-",
	"IP_ADDR":	"192.168.243.43",
	"IX_ADDR":	"0098010701",
	"G_NBR":	"",
	"L_NBR":	"",
	"IXG_ID"	:	1,
	"IXG_IM_ID"	:	7,
	"IX_NAME":	"IXG1-IM7",
	"IX_TYPE":	"IM",
	"IX_Model":	"IXG-IM",
	"DevModel":	"IXG-IM",
	"FW_VER":	"-",
	"HW_VER":	"-"
}
*/

cJSON* GetDataSetListByIX_ADDR(char* ixAddr)
{
	cJSON* devTb = NULL;
	cJSON* Where = cJSON_CreateObject();
	cJSON* View = cJSON_CreateArray();
	char buff[20]={0};
	if(strlen(ixAddr)==10&&memcmp(ixAddr+8,"00",2)==0)
		memcpy(buff,ixAddr,8);
	else
		strcpy(buff,ixAddr);
		
	cJSON_AddStringToObject(Where, IX2V_IX_ADDR, buff);
	//printf_json(Where, ixAddr);
	//IXS_ProxyGetTb(&devTb, NULL, R8001AndSearch);
	devTb=GetDataSetList();
	//printf_json(devTb, ixAddr);
	API_TB_SelectBySort(devTb, View, Where, 0);
	cJSON_Delete(Where);
	cJSON_Delete(devTb);

	return View;
}

cJSON* GetDevicesByIX_TYPE(char* ixType)
{
	cJSON* devTb = NULL;
	cJSON* Where = cJSON_CreateObject();
	cJSON* View = cJSON_CreateArray();

	cJSON_AddStringToObject(Where, IX2V_IX_TYPE, ixType);

	IXS_ProxyGetTb(&devTb, NULL, R8001AndSearch);

	API_TB_SelectBySort(devTb, View, Where, 0);
	cJSON_Delete(Where);
	cJSON_Delete(devTb);

	return View;
}

cJSON *R8001_G_L_NBRSearch(char *input)
{
	cJSON* devTb = NULL;
	cJSON* asso_filter_tb = NULL;
	//cJSON* Where = cJSON_CreateObject();
	cJSON *node;
	cJSON* View = cJSON_CreateArray();
	char num_buff[100];
	char addr[11];
	int size,i;
	
	//IXS_ProxyGetTb(&devTb, NULL, R8001);
	devTb=GetDataSetList();
	size=cJSON_GetArraySize(devTb);
	int g_search_allow=API_Para_Read_Int(G_NBR_SEARCH_ALLOW);
	int l_search_allow=API_Para_Read_Int(L_NBR_SEARCH_ALLOW);
	if((g_search_allow==0&&l_search_allow==0)||l_search_allow)
	{
		asso_filter_tb=ListFilterByHybridASSO(devTb);
		cJSON_ArrayForEach(node, asso_filter_tb)
		{
			if(GetJsonDataPro(node, IX2V_L_NBR, num_buff)&&GetJsonDataPro(node, IX2V_IX_ADDR, addr))
			{
				
				if(strcmp(input,num_buff)==0)
				{
					cJSON_AddItemToArray(View,cJSON_Duplicate(node,1));
					continue;
				}
			}
		}
	}
	
	if(g_search_allow)
	{
		for(i=0;i<size;i++)
		{
			node=cJSON_GetArrayItem(devTb,i);
			
			{
				if(GetJsonDataPro(node, IX2V_G_NBR, num_buff))
				{
					if(strcmp(input,num_buff)==0)
					{
						cJSON_AddItemToArray(View,cJSON_Duplicate(node,1));
						continue;
					}
				}
			}
		}
	}
	
	cJSON_Delete(devTb);
	cJSON_Delete(asso_filter_tb);
	
	if(cJSON_GetArraySize(View)==0)
	{
		cJSON_Delete(View);
		View=NULL;
	}

	return View;
}
int If_SearchByixAddr(char *ix_addr)
{
	int rev=1;
	//printf("11111111111:%s\n",ix_addr);
	int g_search_allow=API_Para_Read_Int(G_NBR_SEARCH_ALLOW);
	int l_search_allow=API_Para_Read_Int(L_NBR_SEARCH_ALLOW);
	if(l_search_allow==0&&g_search_allow==0)
		return 1;
	cJSON *list=GetDataSetListByIX_ADDR(ix_addr);
	cJSON *node;
	char num_buff[100]={0};
	cJSON_ArrayForEach(node, list)
	{
		if(GetJsonDataPro(node, IX2V_L_NBR, num_buff)&&strlen(num_buff)>0&&strcmp(num_buff,"-")!=0)
		{
			rev=0;
			break;
		}
		if(GetJsonDataPro(node, IX2V_G_NBR, num_buff)&&strlen(num_buff)>0&&strcmp(num_buff,"-")!=0)
		{
			rev=0;
			break;
		}
	}
	//printf_json(list, num_buff);
	cJSON_Delete(list);
	return rev;
}
cJSON* GetIXSByIX_ADDR(char* ixAddr)
{
	cJSON* devTb = NULL;
	cJSON* Where = cJSON_CreateObject();
	cJSON* View = cJSON_CreateArray();

	cJSON_AddStringToObject(Where, IX2V_IX_ADDR, ixAddr);

	IXS_ProxyGetTb(&devTb, NULL, R8001AndSearch);
	cJSON* record = cJSON_CreateObject();
	cJSON_AddItemToArray(View, record);
	cJSON_AddStringToObject(record, IX2V_IP_ADDR, "");
	cJSON_AddStringToObject(record, IX2V_IX_ADDR, "");
	cJSON_AddStringToObject(record, IX2V_IX_TYPE, "");
	//devTb=GetDataSetList();
	if(devTb)
	{
		API_TB_SelectBySort(devTb, View, Where, 0);

		cJSON_Delete(devTb);
	}
	cJSON_Delete(Where);
	cJSON_DeleteItemFromArray(View,0);
	return View;
}

#if	defined(PID_IX850)
int ProgReport_CallBack(cJSON *event)
{
	char tempString[100] = {0};
	snprintf(tempString, 100, "%s prog update\n", GetEventItemString(event,  IX2V_IX_ADDR));
	API_TIPS_Ext_Time(tempString, 3);
}
#endif


/*********************************************************************************************************
**  End Of File
*********************************************************************************************************/

