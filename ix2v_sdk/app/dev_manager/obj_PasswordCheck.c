
/**
  ******************************************************************************
  * @file    obj_PasswordCheck.c
  * @author  lzh
  * @version V00.01.00
  * @date    2018.10.27
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 


#include <sys/sysinfo.h>
#include "obj_IoInterface.h"
#include "obj_PasswordCheck.h"
#include "obj_TableSurver.h"

static int CheckPassword(char* pwd, char* input, int autoUnlockLen)
{
	int ret = 0;
	if(pwd && input)
	{
		int pwdLen = strlen(pwd);
		int inputLen = strlen(input);
		if(pwdLen > 0 && inputLen > 0)
		{
			if(autoUnlockLen > pwdLen)
			{
				if(inputLen == pwdLen && !strncmp(pwd, input, pwdLen))
				{
					ret = 1;
				}
			}
			else
			{
				if(inputLen == autoUnlockLen && !strncmp(pwd, input, inputLen))
				{
					ret = 1;
				}
			}
		}
	}

	return ret;
}

static int IX850S_CheckPrivatePwd(char* input, int input_Len)
{
	int ret = 0;

	if(input == NULL || (input_Len<=4))
	{
		return ret;
	}
	char pwd_buff[5]={0};
	char nbr_buff[20]={0};
	memcpy(pwd_buff,input+input_Len-4,4);
	memcpy(nbr_buff,input,input_Len-4);
	cJSON* Where = cJSON_CreateObject();
	cJSON_AddStringToObject(Where, "L_NBR", nbr_buff);
	cJSON_AddStringToObject(Where, "CODE", pwd_buff);
	cJSON* view = cJSON_CreateArray();
	cJSON* element;

	if(API_TB_SelectBySortByName(TB_NAME_PRIVATE_PWD_BY_L_NBR, view, Where, 0))
	{
		ret=1;
	}

	cJSON_Delete(Where);
	cJSON_Delete(view);
	return ret;
}

PSW_TYPE InputPasswordVerify(char* pwd, int autoUnlockLen)
{
	int ret = PSW_TYPE_NONE;

	if(!pwd || !strlen(pwd))
	{
		return ret;
	}

	if(CheckPassword("2299", pwd, autoUnlockLen) || CheckPassword(API_Para_Read_String2(InstallerPassword), pwd, autoUnlockLen))
	{
		ret = PSW_TYPE_INSTALLER;
	}
	else if(CheckPassword(API_Para_Read_String2(ManagePassword), pwd, autoUnlockLen))
	{
		ret = PSW_TYPE_MANAGE;
	}
	else if(CheckPassword(API_Para_Read_String2(Unlock1Pwd1), pwd, autoUnlockLen) || CheckPassword(API_Para_Read_String2(Unlock1Pwd2), pwd, autoUnlockLen))
	{
		ret = PSW_TYPE_UNLOCK1;
	}
	else if(CheckPassword(API_Para_Read_String2(Unlock2Pwd1), pwd, autoUnlockLen) || CheckPassword(API_Para_Read_String2(Unlock2Pwd2), pwd, autoUnlockLen))
	{
		ret = PSW_TYPE_UNLOCK2;
	}
	else if(IX850S_CheckPrivatePwd(pwd, strlen(pwd)))
	{
		ret = PSW_TYPE_PRIVATE_UNLOCK;
	}
	#ifdef PID_IX850
	else if(VMUserPsw_Verify(pwd, strlen(pwd)))
	{
		ret = PSW_TYPE_VM_USER;
	}
	#endif

	return ret;
}