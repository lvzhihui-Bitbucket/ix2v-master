/**
  ******************************************************************************
  * @file    obj_GP_OutProxy.c
  * @author  czb
  * @version V00.01.00
  * @date    2022.06.15
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 

#include "utility.h"
#include "task_Beeper.h"

static char* FACTORY_DEFAULT_FILE[] = {
	"/mnt/nand1-2/Settings/*",
	"/mnt/nand1-2/UserData/*",
	"/mnt/nand1-2/UserRes/*",
	"/mnt/nand1-2/Backup/*",
	NULL,
};
static const int FACTORY_DEFAULT_FILE_CNT = sizeof(FACTORY_DEFAULT_FILE)/sizeof(FACTORY_DEFAULT_FILE[0]);


int FactoryDefaultProcess(void)
{
	int i;

	StopTimingSaveTable();
	StopIOSaveFile();
	for(i = 0; FACTORY_DEFAULT_FILE[i] != NULL; i++)
	{
		DeleteFileProcess(NULL, FACTORY_DEFAULT_FILE[i]);
	}
	BEEP_CONFIRM();
	HardwareRestart("FactoryDefault");
	return 1;
}




/*********************************************************************************************************
**  End Of File
*********************************************************************************************************/

