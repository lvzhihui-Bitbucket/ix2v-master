/**
  ******************************************************************************
  * @file    obj_ConnectPhone.c
  * @author  czb
  * @version V00.01.00
  * @date    2022.5.17
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 

#include <dirent.h>
#include <sys/stat.h>
#include "utility.h"
#include "task_Event.h"
#include "obj_ConnectPhone.h"
#include "task_WiFiConnect.h"
#include "obj_PublicInformation.h"
#include "elog.h"
#include "define_string.h"
#include "obj_IoInterface.h"
#include "obj_lvgl_msg.h"

static IX850S_CONNECT_PHONE_RUN_S phoneConnect = {.state = 0, .lock = PTHREAD_MUTEX_INITIALIZER};
static void ConnectPhoneTip(const char* string, char* data)
{
	#if	defined(PID_IX850)
	static void* informHander = NULL;
	if(informHander)
	{
		API_TIPS_Close_Ext(informHander);
		informHander = NULL;
	}
	informHander = API_TIPS_Ext_Prefix(string, data);
	#endif
}

static int StopPhoneConnect(int timing);
static void ConnectPhone_inform_callback(int inform_code,void *ext_para)
{
	char inform[100];

	dprintf("inform_code = %d, ext_para = %s\n", inform_code, ext_para);

	pthread_mutex_lock(&phoneConnect.lock);
	switch(inform_code)
	{
		case WLAN_INFORM_CODE_DISCONNECT:
			if(phoneConnect.state)
			{
				//连接失败
				phoneConnect.state = 0;
				API_PublicInfo_Write_Int(PB_CONNECT_PHONE_STATE, phoneConnect.state);
				API_Del_TimingCheck(StopPhoneConnect);
				API_Del_Wlan_Inform_Callback(ConnectPhone_inform_callback);
				//snprintf(inform, 100, "Connect %s error", phoneConnect.ssid1);
				ConnectPhoneTip("Connect error", phoneConnect.ssid1);
			}
			break;

		case WLAN_INFORM_CODE_CONNECTING:
			if(phoneConnect.state == 1 && ext_para)
			{
				if(phoneConnect.ssid1 && !strcmp(phoneConnect.ssid1, ext_para))
				{
					phoneConnect.state = 2;
					API_PublicInfo_Write_Int(PB_CONNECT_PHONE_STATE, phoneConnect.state);
					//snprintf(inform, 100, "Connecting to %s", phoneConnect.ssid1);
					ConnectPhoneTip("Connecting", phoneConnect.ssid1);
				}
			}
			break;

		case WLAN_INFORM_CODE_CONNECTED:
			if(phoneConnect.state == 2 && ext_para)
			{
				if(phoneConnect.ssid1 && !strcmp(phoneConnect.ssid1, ext_para))
				{
					API_Del_Wlan_Inform_Callback(ConnectPhone_inform_callback);
					API_Del_TimingCheck(StopPhoneConnect);
					//连接成功
					phoneConnect.state = 3;
					API_PublicInfo_Write_Int(PB_CONNECT_PHONE_STATE, phoneConnect.state);
					//snprintf(inform, 100, "Connected to %s", phoneConnect.ssid1);
					ConnectPhoneTip("Connected", phoneConnect.ssid1);

					phoneConnect.saveXdState = IsXDRemoteConnect();
					if(phoneConnect.saveXdState)
					{
						XDRemoteDisconnect();
					}

					if(XDRemoteConnect("{\"IXD_NT\":\"wlan0\"}"))
					{
						API_Para_Write_Int(IXD_AutoConnect, 1);
						phoneConnect.state = 4;
						API_PublicInfo_Write_Int(PB_CONNECT_PHONE_STATE, phoneConnect.state);
						snprintf(inform, 100, "XD Connected to cloud.");
					}
					else
					{
						snprintf(inform, 100, "XD Connect to cloud error.");
						//恢复原来的状态
						if(!phoneConnect.saveXdState)
						{
							XDRemoteDisconnect();
							API_PublicInfo_Write_Int(PB_CONNECT_PHONE_STATE, 0);
						}
					}

					ConnectPhoneTip(inform, NULL);
					phoneConnect.state = 0;
				}
			}
			break;	
	}

	pthread_mutex_unlock(&phoneConnect.lock);
}

static int StopPhoneConnect(int timing)
{
	char inform[100];
	if(timing >= 30)
	{
		if(phoneConnect.saveWifiSwitch)
		{
			API_Wlan_Open(NULL);
		}
		else
		{
			API_Wlan_Close();
		}

		//snprintf(inform, 100, "Connect timeout %s", phoneConnect.ssid1);
		ConnectPhoneTip("Connect timeout", phoneConnect.ssid1);

		//连接超时
		phoneConnect.state = 0;
		API_PublicInfo_Write_Int(PB_CONNECT_PHONE_STATE, phoneConnect.state);
		API_Del_Wlan_Inform_Callback(ConnectPhone_inform_callback);
		return 2;
	}

	return 0;
}

void IX850S_StartConnectPhoneProcess(void)
{
	char inform[100];

	if(!phoneConnect.state)
	{
		phoneConnect.ssid1 = API_Para_Read_String2(Mobile_SSID_NAME1);
		if(phoneConnect.ssid1 != NULL && phoneConnect.ssid1[0] != 0)
		{
			//snprintf(inform, 100, "Connect to %s start.", phoneConnect.ssid1);
			ConnectPhoneTip("Connect start", phoneConnect.ssid1);

			API_Wlan_Close();
			usleep(100*1000);
			cJSON* wifiPara = cJSON_CreateArray();
			cJSON* para1 = cJSON_CreateObject();
			cJSON_AddStringToObject(para1, "SSID_NAME", phoneConnect.ssid1);
			cJSON_AddStringToObject(para1, "SSID_PWD", API_Para_Read_String2(Mobile_SSID_PWD1));
			cJSON_AddItemToArray(wifiPara, para1);

			char* wifiSw = API_PublicInfo_Read_String(PB_WLAN_SW);

			pthread_mutex_lock(&phoneConnect.lock);

			phoneConnect.saveWifiSwitch = ((wifiSw == NULL || !strcmp(wifiSw, "OFF")) ? 0 : 1);
			phoneConnect.state = 1;
			API_PublicInfo_Write_Int(PB_CONNECT_PHONE_STATE, phoneConnect.state);

			API_Add_Wlan_Inform_Callback(ConnectPhone_inform_callback);
			//MyPrintJson("wifiPara", wifiPara);
			API_Wlan_Open(wifiPara);
			cJSON_Delete(wifiPara);
			API_Add_TimingCheck(StopPhoneConnect, 30);
			pthread_mutex_unlock(&phoneConnect.lock);
		}
		else
		{
			snprintf(inform, 100, "SSID is empty.");
			ConnectPhoneTip(inform, NULL);
		}
	}
	else
	{
		//snprintf(inform, 100, "Connect to %s is busy.", phoneConnect.ssid1);
		ConnectPhoneTip("Connect busy", phoneConnect.ssid1);
	}
}

static void InstallProcess(void* data)
{
	API_Event_By_Name(EventUpdateStart);
} 

void MenuInstallProcess(void* data)
{
	if(data)
	{
		if(!strcmp(data, "Connect phone"))
		{
			IX850S_StartConnectPhoneProcess();
		}
		else if(!strcmp(data, "install"))
		{
			API_MSGBOX("About to install", 2, InstallProcess, NULL);
		}
	}
}

/*********************************************************************************************************
**  End Of File
*********************************************************************************************************/

