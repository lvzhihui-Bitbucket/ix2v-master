/**
 ******************************************************************************
 * @file    obj_IXS_Rules.h
 * @author  czb
 * @version V00.01.00
 * @date    2023.02.15
 * @brief
 ******************************************************************************
 * @attention
 *
 *
 * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
 ******************************************************************************
 */

#ifndef _obj_IXS_Rules_H
#define _obj_IXS_Rules_H

#include "cJSON.h"
#include "utility.h"

int IXS_Search(const char* net, cJSON* searchCtrl, const char* BD_Nbr, const char* deviceType, const cJSON* resultTable);

int IXS_Info(const cJSON* ipTable, const cJSON* resultTable);

int IXS_GetByNBR(const char* net, cJSON* searchCtrl, const char* bdRmMs, const char* input, const cJSON* resultTable);

int IXS_GetByMFG_SN(const char* MFG_SN, const cJSON* resultTable);

#endif

/*********************************************************************************************************
**  End Of File
*********************************************************************************************************/