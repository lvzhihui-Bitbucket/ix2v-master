/**
  ******************************************************************************
  * @file    obj_IPHeartBeatService.c
  * @author  czb
  * @version V00.01.00
  * @date    2017.4.25
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 
#include "obj_ThreadHeartBeatService.h"
#include "cJSON.h"
#include "obj_PublicInformation.h"
//#include "task_VideoMenu.h"
#include "ak_drv_wdt.h"
#include "elog.h"
#include "obj_IoInterface.h"
//#define Linphone_Heartbeat_Mask		0x01
//#define AllThread_Heartbeat_Mask	Linphone_Heartbeat_Mask


pthread_mutex_t ThreadHeartbeat_Lock = PTHREAD_MUTEX_INITIALIZER;
static int heart_err_cnt=0;
static OS_TIMER ThreadHeartbeat_timer;

//ThreadHeartbeat_Callback_Def  ThreadHeartbeat_Callback = NULL;
static int ThreadHeartbeat_Mask = 0;
#if 0
void SendAllThreadHeartbeatApply(void)
{
	printf("SendAllThreadHeartbeatApply !!!!!!!!!!!!!\n");
	pthread_mutex_lock(&ThreadHeartbeat_Lock);
	
	API_linphonec_Apply_Heartbeat();
	
	API_CallserverThreadHeartbeatReq();
	//API_Survey_ThreadHeartbeatReq();
	
	ThreadHeartbeat_Mask = AllThread_Heartbeat_Mask;
	//ThreadHeartbeat_Callback = Finish_Callback;
	
	
	pthread_mutex_unlock(&ThreadHeartbeat_Lock);
}
#endif
void RecvThreadHeartbeatReply(int Mask)
{
	pthread_mutex_lock(&ThreadHeartbeat_Lock);
	log_d("ThreadHeartbeat_Mask= %04x,Recv: %04x!!!!!!!!!!!!!\n",ThreadHeartbeat_Mask,Mask);
	ThreadHeartbeat_Mask &= (~Mask);
	
	if(ThreadHeartbeat_Mask == 0)
	{
		heart_err_cnt=0;
		log_d("RecvThreadHeartbeatReply Finish!!!!!!!!!!!!!\n");
		#if 0
		if(ThreadHeartbeat_Callback != NULL)
		{
			(*ThreadHeartbeat_Callback)();
			printf("RecvThreadHeartbeatReply Finish!!!!!!!!!!!!!\n");
			ThreadHeartbeat_Callback = NULL;
		}
		#endif
		#if defined(PID_IX611) || defined(PID_IX622)
		ak_drv_wdt_feed();
		#endif
	}
	pthread_mutex_unlock(&ThreadHeartbeat_Lock);	
}

void ThreadHeartbeat_Callback(void)
{
	
	pthread_mutex_lock(&ThreadHeartbeat_Lock);
	if(ThreadHeartbeat_Mask!=0)
	{
		if(++heart_err_cnt>=3)
		{
			ThreadHeartbeat_Mask &= (~Linphone_Heartbeat_Mask);
			if(ThreadHeartbeat_Mask!=0)
			{
				char buff[100];
				sprintf(buff,"Heartbeat mask=[%04x]",ThreadHeartbeat_Mask);
				HardwareRestart(buff);
				pthread_mutex_unlock(&ThreadHeartbeat_Lock);
				return;
			}
			else
			{
				API_TIPS_Ext_Time("sip process failure",60);
				heart_err_cnt=0;
			}
		}
	}
	else
		heart_err_cnt=0;
	ThreadHeartbeat_Mask = AllThread_Heartbeat_Mask;
	#if	defined(PID_IX850)
	if(API_Para_Read_Int(SIP_ENABLE))
		API_linphonec_Apply_Heartbeat();
	else
		ThreadHeartbeat_Mask &= (~Linphone_Heartbeat_Mask);
	#else
	ThreadHeartbeat_Mask &= (~Linphone_Heartbeat_Mask);
	#endif
	API_CallserverThreadHeartbeatReq();
	api_subscriber_server_heartbeat();
	#if 0
	#if	!defined(PID_IX850)
	API_vbu_HeartBeatReq();
	#else
	ThreadHeartbeat_Mask &= (~VideoBu_Heartbeat_Mask);
	#endif
	#endif
	pthread_mutex_unlock(&ThreadHeartbeat_Lock);
	//CPU_Ratio_Measure();
	cJSON*one_ins=GetSystemStat();
	if(one_ins!=NULL)
	{
		API_PublicInfo_Write(PB_SystemStat,one_ins);	
		cJSON_Delete(one_ins);
	}
	
	OS_RetriggerTimer( &ThreadHeartbeat_timer );	
		
}
void ThreadHeartbeat_Init(void)
{
	heart_err_cnt=0;
	ThreadHeartbeat_Mask=0;
	//OS_CreateTimer( &ThreadHeartbeat_timer, ThreadHeartbeat_Callback, 60*1000/25);
	OS_CreateTimerOneThread( &ThreadHeartbeat_timer, ThreadHeartbeat_Callback, 60*1000/25);
	#if defined(PID_IX611) || defined(PID_IX622)
	ak_drv_wdt_open(180);	// range[1s-357s]
	#endif
	OS_RetriggerTimer( &ThreadHeartbeat_timer );	
}
/*********************************************************************************************************
**  End Of File
*********************************************************************************************************/

