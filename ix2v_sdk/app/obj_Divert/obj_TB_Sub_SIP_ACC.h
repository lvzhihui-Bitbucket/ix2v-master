#ifndef _obj_TB_Sub_SIP_ACC_h
#define _obj_TB_Sub_SIP_ACC_h
//#define IDAT_TYPE				"Type"
//#define IDAT_ADDR				"ADDR"
#define IDAT_DT_ADDR				"DT_ADDR"
#define IDAT_IX_ADDR				"IX_ADDR"
#define IDAT_PWD					"PWD"
#define IDAT_PHONE_ACC				"Phone_Acc"
#define IDAT_PHONE_PWD			"Phone_Pwd"
#define IDAT_DIVERT_OPTION			"Divert_Option"
#define IDAT_REMARK				"Remark"
typedef enum
{
	DivertAccType_DtIm=0,
	DivertAccType_IxIm,	
	//DivertAccType_Vm,
	//DivertAccType_IXGIm,
	DivertAccType_Max
}DivertAccType_t;

//extern const char *DivertAccType_str[];
#endif
