#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>      /* basic system data types */
#include <error.h>
#include "utility.h"
#include "linphone_interface.h"
#include "linphone_becall_process.h"
#include "cJSON.h"
#include "elog_forcall.h"
#include "obj_IoInterface.h"
#include "vtk_tcp_auto_reg.h"
#include "obj_TB_Sub_SIP_ACC.h"
#include "define_file.h"
#include "obj_TB_Divert_On.h"
#include "define_Command.h"
#include "obj_PublicInformation.h"
#include "stack212.h"
#include "task_Event.h"

#define TB_Divert_On	"TB_Divert_On"

cJSON *sip_divert_on_tb_create_dtitem(char *addr)
{
	cJSON *item=NULL;
	item=cJSON_CreateObject();
	
	if(item!=NULL)
	{
		//if(type!=NULL)
		//	cJSON_AddStringToObject(item,IDAT_TYPE,type);
		if(addr!=NULL)
			cJSON_AddStringToObject(item,IDAT_DT_ADDR,addr);
	}
	
	return item;
}

cJSON *sip_divert_on_tb_create_ixitem(char *addr)
{
	cJSON *item=NULL;
	item=cJSON_CreateObject();
	
	if(item!=NULL)
	{
		//if(type!=NULL)
		//	cJSON_AddStringToObject(item,IDAT_TYPE,type);
		if(addr!=NULL)
			cJSON_AddStringToObject(item,IDAT_IX_ADDR,addr);
	}
	
	return item;
}
int judge_divert_on_by_addr(DivertAccType_t type, char *im_addr)
{
	cJSON *where;
	cJSON *item_array;
	cJSON *item;
	//char buff[10];
	if(type>=DivertAccType_Max)
		return NULL;
	if(type==DivertAccType_DtIm)
		where=sip_divert_on_tb_create_dtitem(im_addr);
	else
		where=sip_divert_on_tb_create_ixitem(im_addr);
	//sprintf(buff,"%s",im_addr);
	//where=sip_divert_on_tb_create_item(DivertAccType_str[type],im_addr);
	item_array=cJSON_CreateArray();
	if(API_TB_SelectBySortByName(TB_Divert_On,item_array,where,0)==0)
	{
		cJSON_Delete(item_array);
		cJSON_Delete(where);
		return 0;
	}
	//item=cJSON_Duplicate(cJSON_GetArrayItem(item_array,0),1);
	cJSON_Delete(where);
	cJSON_Delete(item_array);
	return 1;
}

int del_divert_on_by_addr(DivertAccType_t type, char *im_addr)
{
	cJSON *where;
	//char buff[10];

	
	int ret = 0;
	if(type>=DivertAccType_Max)
		return -1;
	//sprintf(buff,"%s",im_addr);
	if(type==DivertAccType_DtIm)
		where=sip_divert_on_tb_create_dtitem(im_addr);
	else
		where=sip_divert_on_tb_create_ixitem(im_addr);
	//where=sip_divert_on_tb_create_item(DivertAccType_str[type],im_addr);
	API_TB_DeleteByName(TB_Divert_On,where);

	cJSON_Delete(where);

	//sip_im_acc_tb_save();
	return 0;
}

int add_divert_on_to_tb(DivertAccType_t type,char *im_addr)
{
	cJSON *item;
	//char buff[10];
	int ret=0;
	
	if(type>=DivertAccType_Max)
		return -1;
	//sprintf(buff,"%s",im_addr);
	if(type==DivertAccType_DtIm)
		item=sip_divert_on_tb_create_dtitem(im_addr);
	else
		item=sip_divert_on_tb_create_ixitem(im_addr);
	//item=sip_divert_on_tb_create_item(DivertAccType_str[type],im_addr);
	API_TB_AddByName(TB_Divert_On,item);
	cJSON_Delete(item);
	
	//sip_im_acc_tb_save();
	return ret;
}

int SetImDivertOn(int type,char *addr)
{
	char *sip_net=API_PublicInfo_Read_String(PB_SIP_CONNECT_NETWORK);
	if(sip_net==NULL||strcmp(sip_net,"NONE")==0||API_Para_Read_Int(SIP_ENABLE)==0)
	{
		return 3;
	}
	if(get_sip_dev_acc_from_tb(type,addr,NULL,NULL)<0)
	{
		return 2;
	}
	if(judge_divert_on_by_addr(type,addr)==0)
	{
		add_divert_on_to_tb(type,addr);
	}
	
	return 1;
}

int SetImDivertOff(int type,char *addr)
{
	del_divert_on_by_addr(type,addr);
	return 0;
}

int SetDtImDivertOn(int dt_addr)
{
	char buff[20];
	int temp;
	if(dt_addr>32)
	{
		temp=((dt_addr&0x7f)>>2);
		if(temp==0)
			temp=32;
	}
	else
		temp=dt_addr;
	sprintf(buff,"%d",temp);
	return SetImDivertOn(DivertAccType_DtIm,buff);
}

int SetDtImDivertOff(int dt_addr)
{
	char buff[20];
	int temp;
	if(dt_addr>32)
	{
		temp=((dt_addr&0x7f)>>2);
		if(temp==0)
			temp=32;
	}
	else
		temp=dt_addr;
	sprintf(buff,"%d",temp);
	return SetImDivertOff(DivertAccType_DtIm,buff);
}

int SetIxImDivertOn(char *addr)
{
	char buff[20];
	if(strlen(addr)>8)
	{
		memcpy(buff,addr,8);
		buff[8]=0;	
	}
	else
	{
		strcpy(buff,addr);
	}
	strcat(buff,"01");
	return SetImDivertOn(DivertAccType_IxIm,buff);
}

int SetIxImDivertOff(char *addr)
{
	char buff[20];
	if(strlen(addr)>8)
	{
		memcpy(buff,addr,8);
		buff[8]=0;	
	}
	else
	{
		strcpy(buff,addr);
	}
	strcat(buff,"01");
	return SetImDivertOff(DivertAccType_IxIm,buff);
}

int judge_dtim_divert_on_by_addr(int dt_addr)
{
	char buff[20];
	int temp;
	if(dt_addr>32)
	{
		temp=((dt_addr&0x7f)>>2);
		if(temp==0)
			temp=32;
	}
	else
		temp=dt_addr;
	sprintf(buff,"%d",temp);
	
	return judge_divert_on_by_addr(DivertAccType_DtIm,buff);
}

int judge_ixim_divert_on_by_addr(char *addr)
{
	#if 0
	char buff[20];
	if(strlen(addr)>8)
	{
		memcpy(buff,addr,8);
		buff[8]=0;	
	}
	else
	{
		strcpy(buff,addr);
	}
	#endif
	char buff[20];
	if(strlen(addr)>8)
	{
		memcpy(buff,addr,8);
		buff[8]=0;	
	}
	else
	{
		strcpy(buff,addr);
	}
	strcat(buff,"01");
	return judge_divert_on_by_addr(DivertAccType_IxIm,buff);
}


int IxDsDivertStateSetCallback(cJSON *cmd)
{
	char* eventName = GetEventItemString(cmd, EVENT_KEY_EventName);
	char* state = GetEventItemString(cmd, "STATE");
	char* setdev = GetEventItemString(cmd, "TARGET");
	char *info=GetEventItemString(cmd, "MSG");
	int ret=0;
	char state_ret[20]={0};
	char info_ret[20]={0};
	//printf("111111111IxDsDivertStateSetCallback:%s:%s\n",setdev,state);
	#if 1
    if(eventName && !strcmp(eventName, EventSetDivertState))
    {
        if(state)
        {
            if(!strcmp(state, "ON"))
            {
            		//if(judge_ixim_divert_on_by_addr(setdev)
            		ret=SetIxImDivertOn(setdev);
                	if(ret==1)
                	{
				strcpy(state_ret,"ON");
			}
			else
			{
				strcpy(state_ret,"OFF");
				if(ret==3)
				{
					strcpy(info_ret,"SIP ISSUE");	
				}
				else if(ret==2)
				{
					strcpy(info_ret,"NO ACC");	
				}	
			}
            }
            else if(!strcmp(state, "OFF"))
            {
            		ret=SetIxImDivertOff(setdev);
               	strcpy(state_ret,"OFF");
            }
		else if(!strcmp(state, "CHECK"))	
		{
			if(judge_ixim_divert_on_by_addr(setdev))
			{
				char *sip_net=API_PublicInfo_Read_String(PB_SIP_CONNECT_NETWORK);
				if(sip_net==NULL||strcmp(sip_net,"NONE")==0||API_Para_Read_Int(SIP_ENABLE)==0)
				{
					strcpy(state_ret,"OFF");
					strcpy(info_ret,"SIP ISSUE");
				}
				else if(get_sip_dev_acc_from_tb(DivertAccType_IxIm,setdev,NULL,NULL)<0)
				{
					strcpy(state_ret,"OFF");
					strcpy(info_ret,"NO ACC");
				}
				else
					strcpy(state_ret,"ON");
			}
			else
				strcpy(state_ret,"OFF");
		}
		else if(!strcmp(state, "RESET"))	
		{
			strcpy(state_ret,"RESET");
			if(info!=NULL&&strlen(info)>0)
			{
				if(ch_sip_ix_im_phone_pwd2(setdev,info)==0)
				{
					//printf("1111111111reset_sip_IX_im_phone_pwd:ok\n");
					strcpy(info_ret,"SUCC");
				}
				else
				{
					//printf("22222222222reset_sip_IX_im_phone_pwd:err\n");
					strcpy(info_ret,"FAIL");
				}
			}
			else
			{
				if(reset_sip_ix_im_phone_pwd(setdev)==0)
				{
					//printf("1111111111reset_sip_IX_im_phone_pwd:ok\n");
					strcpy(info_ret,"SUCC");
				}
				else
				{
					//printf("22222222222reset_sip_IX_im_phone_pwd:err\n");
					strcpy(info_ret,"FAIL");
				}
			}
		}
		else if(!strcmp(state, "CREATE"))
		{
			char sip_acc[60];
			char sip_pwd[40];
			int im_addr;
			memcpy(sip_acc,setdev+4,4);
			sip_acc[4]=0;
			im_addr=atoi(sip_acc);
			strcpy(state_ret,"CREATE");
			if(!API_Para_Read_Int(SIP_ENABLE))
			{
				strcpy(info_ret,"SIP_DIS");
			}
			else

			{
				if(add_sip_ix_im_acc(im_addr,sip_acc,sip_pwd)<0)
					strcpy(info_ret,"ADD_FAIL");
				else
					strcpy(info_ret,"ADD_SUCC");
			}
				
		}
		else
			return 1;
		API_SetDivertState(GetSysVerInfo_BdRmMs(),setdev,setdev,state_ret,info_ret);
        }
    }
	#endif
	return 1;
}
