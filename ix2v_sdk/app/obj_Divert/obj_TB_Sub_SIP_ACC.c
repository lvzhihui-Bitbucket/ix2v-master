#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>      /* basic system data types */
#include <error.h>

#include "linphone_interface.h"
#include "linphone_becall_process.h"
#include "cJSON.h"
#include "elog_forcall.h"
#include "obj_IoInterface.h"
#include "vtk_tcp_auto_reg.h"
#include "obj_TB_Sub_SIP_ACC.h"
#include "define_file.h"
#include "obj_PublicInformation.h"
#include "task_Event.h"

#define TB_Sub_SIP_ACC	"TB_Sub_SIP_ACC"
//static cJSON *sip_manager=NULL;
cJSON *IMDivertAccTb=NULL;
#if 0
const char *DivertAccType_str[DivertAccType_Max]=
{
	"DT-IM",
	"IX-IM",
	"VM",
	"IXG-IM",
};
#endif
void sip_manager_init(void)
{
	//sip_manager=API_Para_Read_Public(SipConfig);
}

int get_sip_master_acc(char *sip_acc)
{
	return API_Para_Read_String(SIP_ACCOUNT,sip_acc);
}
int get_sip_master_pwd(char *sip_pwd)
{
	return API_Para_Read_String(SIP_PASSWORD,sip_pwd);
}

int get_sip_master_ser(char *sip_ser)
{
	return API_Para_Read_String(SIP_SERVER,sip_ser);
}

int set_sip_master_acc(char *sip_acc)
{
	return API_Para_Write_String(SIP_ACCOUNT,sip_acc);
}
int set_sip_master_pwd(char *sip_pwd)
{
	return API_Para_Write_String(SIP_PASSWORD,sip_pwd);
}
int set_sip_master_ser(char *sip_ser)
{
	return API_Para_Write_String(SIP_SERVER,sip_ser);
}
int get_valid_sn(char *sn)
{
	int sn_type = SourceStringToType(cJSON_GetStringValue(API_Para_Read_Public(IO_MFG_SN_Source)));
	if(sn_type!=1&&sn_type!=2)
	{
		return 0;
	}
	if(API_Para_Read_String(IO_MFG_SN,sn))
	{
		if(strlen(sn)>0)
			return 1;
	}

	return 0;
}
int get_default_sip_master_username( char *acc)
{
	return get_valid_sn(acc);
}
void create_default_sip_pwd(const char *acc,char *pwd)
{
	unsigned char md5[16];
	unsigned short password;
	StringMd5_Calculate(acc,md5);
	password = (md5[14]<<8) | md5[15];
	sprintf(pwd,"%d",password);
}

int sip_acc_use_restore_default(char *sip_acc,char *sip_pwd)
{
	int ret;
	char defaut_ser[100]={0};
	char reserve_ser[100]={0};
	
	API_Para_Read_String(SIP_DEFAULT_SER,defaut_ser);
	API_Para_Read_String(SIP_RESERVE_SER,reserve_ser);
	
	remote_account_manage( ACCOUNT_MANAGE_CMD_CLEARUL2,reserve_ser,sip_acc,sip_pwd,NULL );
	remote_account_manage( ACCOUNT_MANAGE_CMD_CLEARUL2,defaut_ser,sip_acc,sip_pwd,NULL );
	ret=remote_account_manage( ACCOUNT_MANAGE_CMD_RESTORE_DEFAULT,reserve_ser,sip_acc,sip_pwd,NULL );
	ret|=remote_account_manage( ACCOUNT_MANAGE_CMD_RESTORE_DEFAULT,defaut_ser,sip_acc,sip_pwd,NULL );
	
	
	if( (ret == 1) || (ret == 0) )	// ok :  生成新账号, 恢复了缺省密码
	{
		ret = 0;
	}
	else if(ret == -2)		// 账号密码相同，已存在了
	{
		ret = 0;
	}
	else
	{
		//ret = 0;
		ret = -1;
	}
	//czn_20181227
	return ret;
}
int sip_acc_use_clear_rul(char *sip_acc,char *sip_pwd)
{
	char sip_ser[80];
	
	API_Para_Read_String(SIP_SERVER,sip_ser);

	remote_account_manage( ACCOUNT_MANAGE_CMD_CLEARUL2,sip_ser,sip_acc,sip_pwd,NULL );

	return 0;
}

int sip_master_use_default(void)
{
	char sip_acc[40];
	char sip_pwd[40];
	char sip_ser[80];
	if(get_default_sip_master_username(sip_acc)!=1)
		return -1;
	create_default_sip_pwd(sip_acc,sip_pwd);

	if(sip_acc_use_restore_default(sip_acc,sip_pwd)==0)
	{
		API_Para_Read_String(SIP_DEFAULT_SER,sip_ser);
		set_sip_master_ser(sip_ser);
		set_sip_master_acc(sip_acc);
		set_sip_master_pwd(sip_pwd);
		API_linphonec_Register_SipCurAccount(sip_ser,sip_acc,sip_pwd);
		//return 0;
	}
	else
		return -1;
	sip_acc[1]='e';
	create_default_sip_pwd(sip_acc,sip_pwd);
	if(sip_acc_use_restore_default(sip_acc,sip_pwd)==0)
	{
		API_Para_Write_String(SIP_DIVERT,sip_acc);
		API_Para_Write_String(SIP_DIV_PWD,sip_pwd);
		return 0;
	}

	return -1;
}

int sip_master_rereg(void)
{
	char sip_acc[40];
	char sip_pwd[40];
	char sip_ser[80];
	//if(get_default_sip_master_username(sip_acc)!=1)
	//	return -1;
	//create_default_sip_pwd(sip_acc,sip_pwd);

	//if(sip_acc_use_restore_default(sip_acc,sip_pwd)==0)
	{
		//API_Para_Read_String(SIP_DEFAULT_SER,sip_ser);
		
		get_sip_master_ser(sip_ser);
		get_sip_master_acc(sip_acc);
		get_sip_master_pwd(sip_pwd);
		sip_acc_use_clear_rul(sip_acc,sip_pwd);
		API_linphonec_Register_SipCurAccount(sip_ser,sip_acc,sip_pwd);
		return 0;
	}


}

void sip_dt_im_acc_clear_rul(int im_addr)
{
	char sip_acc[40];
	char sip_pwd[40];
	if(get_sip_dt_im_acc_from_tb(im_addr,sip_acc,sip_pwd)==0)
	{
		sip_acc_use_clear_rul(sip_acc,sip_pwd);
	}
}

void sip_ix_im_acc_clear_rul(int im_addr)
{
	char sip_acc[40];
	char sip_pwd[40];
	if(get_sip_ix_im_acc_from_tb(im_addr,sip_acc,sip_pwd)==0)
	{
		sip_acc_use_clear_rul(sip_acc,sip_pwd);
	}
}
cJSON *sip_dtim_acc_tb_create_item(char *addr,char *phone_acc, char *phone_pwd)
{
	cJSON *item=NULL;
	item=cJSON_CreateObject();
	
	if(item!=NULL)
	{
		//if(type!=NULL)
		//	cJSON_AddStringToObject(item,IDAT_TYPE,type);
		if(addr!=NULL)
			cJSON_AddStringToObject(item,IDAT_DT_ADDR,addr);
		//if(pwd!=NULL)
		//	cJSON_AddStringToObject(item,IDAT_PWD,pwd);
		if(phone_acc!=NULL)
			cJSON_AddStringToObject(item,IDAT_PHONE_ACC,phone_acc);
		if(phone_pwd!=NULL)
			cJSON_AddStringToObject(item,IDAT_PHONE_PWD,phone_pwd);
		//if(divert_option!=NULL)
		//	cJSON_AddStringToObject(item,IDAT_DIVERT_OPTION,divert_option);
		//if(remark!=NULL)
		//	cJSON_AddStringToObject(item,IDAT_REMARK,remark);
	}
	
	return item;
}
cJSON *sip_ixim_acc_tb_create_item(char *addr,char *phone_acc, char *phone_pwd)
{
	cJSON *item=NULL;
	item=cJSON_CreateObject();
	
	if(item!=NULL)
	{
		//if(type!=NULL)
		//	cJSON_AddStringToObject(item,IDAT_TYPE,type);
		if(addr!=NULL)
			cJSON_AddStringToObject(item,IDAT_IX_ADDR,addr);
		//if(pwd!=NULL)
		//	cJSON_AddStringToObject(item,IDAT_PWD,pwd);
		if(phone_acc!=NULL)
			cJSON_AddStringToObject(item,IDAT_PHONE_ACC,phone_acc);
		if(phone_pwd!=NULL)
			cJSON_AddStringToObject(item,IDAT_PHONE_PWD,phone_pwd);
		//if(divert_option!=NULL)
		//	cJSON_AddStringToObject(item,IDAT_DIVERT_OPTION,divert_option);
		//if(remark!=NULL)
		//	cJSON_AddStringToObject(item,IDAT_REMARK,remark);
	}
	
	return item;
}

cJSON* get_sip_dev_acc_item_by_addr(DivertAccType_t type, char *im_addr)
{
	cJSON *where;
	cJSON *item_array;
	cJSON *item;
	//char buff[20];
	if(type>=DivertAccType_Max)
		return NULL;
	//sprintf(buff,"%s",im_addr);
	if(type==DivertAccType_DtIm)
		where=sip_dtim_acc_tb_create_item(im_addr,NULL,NULL);
	else
		where=sip_ixim_acc_tb_create_item(im_addr,NULL,NULL);
	item_array=cJSON_CreateArray();
	if(API_TB_SelectBySortByName(TB_Sub_SIP_ACC,item_array,where,0)==0)
	{
		cJSON_Delete(item_array);
		cJSON_Delete(where);
		return NULL;
	}
	item=cJSON_Duplicate(cJSON_GetArrayItem(item_array,0),1);
	cJSON_Delete(where);
	cJSON_Delete(item_array);
	return item;
}
int update_sip_dev_acc_item_by_addr(DivertAccType_t type, char *im_addr,cJSON *item)
{
	cJSON *where;
	//char buff[10];
	int ret = 0;
	if(type>=DivertAccType_Max)
		return -1;
	//sprintf(buff,"%s",im_addr);
	if(type==DivertAccType_DtIm)
		where=sip_dtim_acc_tb_create_item(im_addr,NULL,NULL);
	else
		where=sip_ixim_acc_tb_create_item(im_addr,NULL,NULL);
	if(API_TB_UpdateByName(TB_Sub_SIP_ACC, where, item))
		ret = 1;
	cJSON_Delete(where);
	
	return ret;
}

void del_sip_dev_acc_by_addr(DivertAccType_t type, char *im_addr)
{
	cJSON *where;
	//char buff[10];

	
	int ret = 0;
	if(type>=DivertAccType_Max)
		return -1;
	//sprintf(buff,"%s",im_addr);
	if(type==DivertAccType_DtIm)
		where=sip_dtim_acc_tb_create_item(im_addr,NULL,NULL);
	else
		where=sip_ixim_acc_tb_create_item(im_addr,NULL,NULL);
	API_TB_DeleteByName(TB_Sub_SIP_ACC,where);

	cJSON_Delete(where);

	//sip_im_acc_tb_save();
}
int modefy_sip_dev_phone_pwd(DivertAccType_t type,char *im_addr,const char *sip_pwd)
{
	cJSON *item;
	//char buff[10];
	int ret;
	
	if(type>=DivertAccType_Max)
		return -1;
	//sprintf(buff,"%s",im_addr);
	if(type==DivertAccType_DtIm)
		item=sip_dtim_acc_tb_create_item(im_addr,NULL,sip_pwd);
	else
		item=sip_ixim_acc_tb_create_item(im_addr,NULL,sip_pwd);
	//item=sip_im_acc_tb_create_item(DivertAccType_str[type],im_addr,NULL,NULL,sip_pwd,NULL,NULL);
	ret=update_sip_dev_acc_item_by_addr(type,im_addr,item);
	cJSON_Delete(item);
	//sip_im_acc_tb_save();
	return ret;
}
int add_sip_dev_acc_to_tb(DivertAccType_t type,char *im_addr,const char *sip_acc,const char *sip_pwd)
{
	cJSON *item;
	//char buff[10];
	int ret;
	
	if(type>=DivertAccType_Max)
		return -1;
	//sprintf(buff,"%s",im_addr);
	if(type==DivertAccType_DtIm)
		item=sip_dtim_acc_tb_create_item(im_addr,sip_acc,sip_pwd);
	else
		item=sip_ixim_acc_tb_create_item(im_addr,sip_acc,sip_pwd);
	//item=sip_im_acc_tb_create_item(DivertAccType_str[type],im_addr,NULL,sip_acc,sip_pwd,"-",NULL);
	if(update_sip_dev_acc_item_by_addr(type,im_addr,item)>0)
		ret=1;
	else
		ret=API_TB_AddByName(TB_Sub_SIP_ACC,item);
	cJSON_Delete(item);
	
	//sip_im_acc_tb_save();
	return ret;
}

int get_sip_dev_acc_from_tb(DivertAccType_t type,char *im_addr,char *sip_acc,char *sip_pwd)
{
	cJSON *item;
	int ret=0;
	#if 0
	if(IMDivertAccTb==NULL)
	{
		if(sip_im_acc_tb_load()<0)
		{
			return -1;
		}
	}
	#endif
	item=get_sip_dev_acc_item_by_addr(type,im_addr);
	if(item==NULL)
		return -1;
	if(sip_acc!=NULL)
	{
		if(GetJsonDataPro(item,IDAT_PHONE_ACC,sip_acc)==0)
			ret=-1;
	}
	if(sip_pwd!=NULL)
	{
		if(GetJsonDataPro(item,IDAT_PHONE_PWD,sip_pwd)==0)
			ret=-1;
	}
	cJSON_Delete(item);
	return ret;
}
int get_sip_dt_im_acc_from_tb(int im_addr, char *sip_acc,char *sip_pwd)
{
	char addr[10];
	int temp;
	if(im_addr>32)
	{
		temp=((im_addr&0x7f)>>2);
		if(temp==0)
			temp=32;
	}
	else
		temp=im_addr;
	sprintf(addr,"%d",temp);
	return get_sip_dev_acc_from_tb(DivertAccType_DtIm,addr,sip_acc,sip_pwd);
}
int get_sip_ix_im_acc_from_tb(int im_addr, char *sip_acc,char *sip_pwd)
{
	char addr[20];
	sprintf(addr,"%s%04d01",GetSysVerInfo_bd(),im_addr);
	return get_sip_dev_acc_from_tb(DivertAccType_IxIm,addr,sip_acc,sip_pwd);
}
int get_sip_ix_im_acc_from_tb2(char  *im_addr, char *sip_acc,char *sip_pwd)
{
	//char addr[20];
	//sprintf(addr,"%s%04d",GetSysVerInfo_bd(),im_addr);
	char buff[20];
	if(strlen(im_addr)>8)
	{
		memcpy(buff,im_addr,8);
		buff[8]=0;	
	}
	else
	{
		strcpy(buff,im_addr);
	}
	strcat(buff,"01");
	return get_sip_dev_acc_from_tb(DivertAccType_IxIm,buff,sip_acc,sip_pwd);
}
int add_sip_ix_im_acc(int im_addr,char *sip_acc,char *sip_pwd)
{
	//char sip_acc[60];
	//char sip_pwd[40];
	char master_acc[40];
	char addr[20];
	
	if(get_default_sip_master_username(master_acc)!=1)
		return -1;
	
	sprintf(sip_acc,"%s%04d",master_acc,im_addr);
	sprintf(addr,"%s%04d01",GetSysVerInfo_bd(),im_addr);
	
	create_default_sip_pwd(sip_acc,sip_pwd);
	if(sip_acc_use_restore_default(sip_acc,sip_pwd)==0)
	{
		add_sip_dev_acc_to_tb(DivertAccType_IxIm,addr,sip_acc,sip_pwd);
		return 0;
	}
	return -2;
}
int add_sip_dt_im_acc(int im_addr,char *sip_acc,char *sip_pwd)
{
	//char sip_acc[60];
	//char sip_pwd[40];
	char master_acc[40];
	char addr[10];

	int temp;
	
	if(im_addr>32)
	{
		temp=((im_addr&0x7f)>>2);
		if(temp==0)
			temp=32;
	}
	else
		temp=im_addr;
	//sprintf(addr,"%d",temp);
	
	if(get_default_sip_master_username(master_acc)!=1)
		return -1;
	
	sprintf(sip_acc,"%s%04d",master_acc,temp);
	sprintf(addr,"%d",temp);
	
	create_default_sip_pwd(sip_acc,sip_pwd);
	if(sip_acc_use_restore_default(sip_acc,sip_pwd)==0)
	{
		add_sip_dev_acc_to_tb(DivertAccType_DtIm,addr,sip_acc,sip_pwd);
		return 0;
	}
	return -2;
}
int del_sip_ix_im_acc_by_addr(int im_addr)
{
	char addr[20];
	
	sprintf(addr,"%s%04d01",GetSysVerInfo_bd(),im_addr);
	
	
	del_sip_dev_acc_by_addr(DivertAccType_IxIm,addr);
	return 0;
	
}
int del_sip_dt_im_acc_by_addr(int im_addr)
{
	char addr[20];
	int temp;
	
	if(im_addr>32)
	{
		temp=((im_addr&0x7f)>>2);
		if(temp==0)
			temp=32;
	}
	else
		temp=im_addr;
	sprintf(addr,"%d",temp);
	
	del_sip_dev_acc_by_addr(DivertAccType_DtIm,addr);
	return 0;
	
}
int ch_sip_dt_im_phone_pwd(int im_addr,char *new_pwd)
{
	char sip_acc[60];
	char sip_pwd[40];
	char addr[10];
	int temp;
	
	if(im_addr>32)
	{
		temp=((im_addr&0x7f)>>2);
		if(temp==0)
			temp=32;
	}
	else
		temp=im_addr;
	sprintf(addr,"%d",temp);
	if(get_sip_dev_acc_from_tb(DivertAccType_DtIm,addr,sip_acc,sip_pwd)<0)
		return -1;
	
	if(sip_acc_use_restore_default(sip_acc,new_pwd)==0)
	{
		if(modefy_sip_dev_phone_pwd(DivertAccType_DtIm,addr,new_pwd)>=0)	
			return 0;
	}
	return -2;
}
int ch_sip_ix_im_phone_pwd(int im_addr,char *new_pwd)
{
	char sip_acc[60];
	char sip_pwd[40];
	char addr[10];
	sprintf(addr,"%s%04d01",GetSysVerInfo_bd(),im_addr);
	if(get_sip_dev_acc_from_tb(DivertAccType_IxIm,addr,sip_acc,sip_pwd)<0)
		return -1;
	
	if(sip_acc_use_restore_default(sip_acc,new_pwd)==0)
	{
		if(modefy_sip_dev_phone_pwd(DivertAccType_IxIm,addr,new_pwd)>=0)	
			return 0;
	}
	return -2;
}

int ch_sip_ix_im_phone_pwd2(char *im_addr,char *new_pwd)
{
	char sip_acc[60];
	char sip_pwd[40];
	//char addr[10];
	//sprintf(addr,"%s%04d01",GetSysVerInfo_bd(),im_addr);
	char buff[20];
	if(strlen(im_addr)>8)
	{
		memcpy(buff,im_addr,8);
		buff[8]=0;	
	}
	else
	{
		strcpy(buff,im_addr);
	}
	strcat(buff,"01");
	if(get_sip_dev_acc_from_tb(DivertAccType_IxIm,buff,sip_acc,sip_pwd)<0)
		return -1;
	
	if(sip_acc_use_restore_default(buff,new_pwd)==0)
	{
		if(modefy_sip_dev_phone_pwd(DivertAccType_IxIm,buff,new_pwd)>=0)	
			return 0;
	}
	return -2;
}
int reset_sip_dt_im_phone_pwd(int im_addr)
{
	char sip_acc[60];
	char sip_pwd[40];
	char addr[10];
	int temp;
	//char new_pwd[10];
	if(im_addr>32)
	{
		temp=((im_addr&0x7f)>>2);
		if(temp==0)
			temp=32;
	}
	else
		temp=im_addr;
	sprintf(addr,"%d",temp);
	if(get_sip_dev_acc_from_tb(DivertAccType_DtIm,addr,sip_acc,NULL)<0)
		return -1;
	create_default_sip_pwd(sip_acc,sip_pwd);
	if(sip_acc_use_restore_default(sip_acc,sip_pwd)==0)
	{
		if(modefy_sip_dev_phone_pwd(DivertAccType_DtIm,addr,sip_pwd)>=0)	
			return 0;
	}
	return -1;
}

int reset_sip_ix_im_phone_pwd(char  *im_addr)
{
	char sip_acc[60];
	char sip_pwd[40];
	//char addr[10];
	char buff[20];
	if(strlen(im_addr)>8)
	{
		memcpy(buff,im_addr,8);
		buff[8]=0;	
	}
	else
	{
		strcpy(buff,im_addr);
	}
	strcat(buff,"01");
	//sprintf(addr,"%s%04d01",GetSysVerInfo_bd(),im_addr);
	if(get_sip_dev_acc_from_tb(DivertAccType_IxIm,buff,sip_acc,NULL)<0)
		return -1;
	create_default_sip_pwd(sip_acc,sip_pwd);
	if(sip_acc_use_restore_default(sip_acc,sip_pwd)==0)
	{
		if(modefy_sip_dev_phone_pwd(DivertAccType_IxIm,buff,sip_pwd)>=0)	
			return 0;
	}
	return -2;
}

char* create_sip_phone_qrcode(const char *acc,const char *pwd)
{

	char QRcodeList[200];
	char sip_ser[80];
	char master_acc[40];
	char master_pwd[40];
	//char im_pwd[40];
	//char im_acc[40];
	get_sip_master_ser(sip_ser);
	get_sip_master_acc(master_acc);
	get_sip_master_pwd(master_pwd);
	//if(get_sip_im_acc_from_tb(im_addr,im_acc,im_pwd)<0)
	//	return NULL;
	
	char *qrcode= malloc(2000);
	if(qrcode==NULL)
		return  NULL;

	qrcode[0] = 0;

	snprintf(QRcodeList,200,"%s%s%s","<TYPE>","IX","</TYPE>");
	strcat(qrcode, QRcodeList);

	snprintf(QRcodeList,200,"%s%s%s","<VER>","2.0","</VER>");
	strcat(qrcode, QRcodeList);
	
	snprintf(QRcodeList,200,"%s%s%s","<USER>",acc,"</USER>");
	strcat(qrcode, QRcodeList);
	
	snprintf(QRcodeList,200,"%s%s%s","<PSW>",pwd,"</PSW>");	
	strcat(qrcode, QRcodeList);
	
	//snprintf(QRcodeList,200,"%s%s%s","<DOMAIN>",sipCfg.serverToIpFlag == 0 ? sipCfg.serverIp : sipCfg.server,"</DOMAIN>");	
	snprintf(QRcodeList,200,"%s%s%s","<DOMAIN>",sip_ser,"</DOMAIN>");
	strcat(qrcode, QRcodeList);
	
	snprintf(QRcodeList,200,"%s%s%s","<HOME ID>",master_acc,"</HOME ID>");		
	strcat(qrcode, QRcodeList);
	
	snprintf(QRcodeList,200,"%s%s%s","<PSW2>",master_pwd,"</PSW2>");	
	strcat(qrcode, QRcodeList);

	snprintf(QRcodeList,200,"%s%s%s","<CALL CODE>","1000","</CALL CODE>");
	strcat(qrcode, QRcodeList);

	strcat(qrcode, "<string-array name=\"List\">");

	//len = strlen(qrcode);
	//maxLen = 2000-strlen("</string-array>");
	snprintf(QRcodeList, 200, "<item>%d,%s,%s,%s,%s</item>", 1, "DS1", master_acc, "11", "1028");
	strcat(qrcode, QRcodeList);
	
	strcat(qrcode, "</string-array>");

	return qrcode;
}

int ShellCmd_SipManage(cJSON *cmd)
{
	char *para=GetShellCmdInputString(cmd, 1);
	char sip_pwd[40];
	char sip_acc[40];
	char addr[20];
	int type;
	int im_addr;
	cJSON *jpara;
	cJSON *set_para;
	char *qr_code;
	
	
	if(para==NULL)
	{
		ShellCmdPrintf("hava no para");
		return 1;
	}
	if(strcmp(para,"state")==0)
	{	
	#if 0
		jpara=API_Get_Lan_State();
		char* temp;
		temp=cJSON_Print(jpara);
		if(temp==NULL)
		{
			ShellCmdPrintf("Get State Fail");
			return 1;
		}
		ShellCmdPrintf(temp);
		free(temp);
	#endif
	}
	else if(strcmp(para,"get_master")==0)
	{
		jpara=API_Para_Read_Public(SipConfig);
		char* temp;
		temp=cJSON_Print(jpara);
		if(temp==NULL)
		{
			ShellCmdPrintf("Get State Fail");
			return 1;
		}
		ShellCmdPrintf(temp);
		free(temp);
	
	}
	else if(strcmp(para,"get_tb")==0)
	{
	#if 0
		if(IMDivertAccTb==NULL)
		{
			if(sip_im_acc_tb_load()<0)
			{
				ShellCmdPrintf("load tb fail");
				return 1;
			}
		}
		ShellCmdPrintJson(IMDivertAccTb);
	#endif
	}
	else if(strcmp(para,"add_tb")==0)
	{
		para=GetShellCmdInputString(cmd, 5);
		if(para==NULL)
		{
			ShellCmdPrintf("para err");
			return 1;
		}
		strcpy(sip_pwd,para);
		para=GetShellCmdInputString(cmd, 4);
		if(para==NULL)
		{
			ShellCmdPrintf("para err");
			return 1;
		}
		strcpy(sip_acc,para);
		para=GetShellCmdInputString(cmd, 3);
		if(para==NULL)
		{
			ShellCmdPrintf("para err");
			return 1;
		}
		strcpy(addr,para);
		para=GetShellCmdInputString(cmd, 2);
		if(para==NULL)
		{
			ShellCmdPrintf("para err");
			return 1;
		}
		add_sip_dev_acc_to_tb(atoi(para),addr,sip_acc,sip_pwd);
		//ShellCmdPrintJson(IMDivertAccTb);
	}
	else if(strcmp(para,"add")==0)
	{
		
		para=GetShellCmdInputString(cmd, 2);
		if(para==NULL)
		{
			ShellCmdPrintf("para err");
			return 1;
		}
		type=atoi(para);
		para=GetShellCmdInputString(cmd,3);
		if(para==NULL)
		{
			ShellCmdPrintf("para err");
			return 1;
		}
		strcpy(addr,para);
		if(type==0)
		{
			if(add_sip_dt_im_acc(atoi(addr),sip_acc,sip_pwd)<0)
			{
				ShellCmdPrintf("add fail");
				return 1;	
			}
			ShellCmdPrintf("add ok,dt im-%s:acc=%s,pwd=%s",addr,sip_acc,sip_pwd);
			
		}
		else if(type==1)
		{
			if(add_sip_ix_im_acc(atoi(addr),sip_acc,sip_pwd)<0)
			{
				ShellCmdPrintf("add fail");
				return 1;	
			}
			ShellCmdPrintf("add ok,ix im-%s:acc=%s,pwd=%s",addr,sip_acc,sip_pwd);
		}
		else
		{
			ShellCmdPrintf("add fail");
			return 1;	
		}
		qr_code=create_sip_phone_qrcode(sip_acc,sip_pwd);
		if(qr_code)
		{
			ShellCmdPrintf("qr:\n%s",qr_code);
			free(qr_code);
		}
		#if 0
		if(get_sip_dev_acc_from_tb(type,addr,sip_acc,sip_pwd)>=0)
		{
			ShellCmdPrintf("already have acc,[%s]im-%s:acc=%s,pwd=%s",DivertAccType_str[type],addr,sip_acc,sip_pwd);
			return 1;
		}
		if(add_sip_dt_im_acc(im_addr,sip_acc,sip_pwd)<0)
		{
			ShellCmdPrintf("add fail");
			return 1;
		}
		
		ShellCmdPrintf("add ok,im-%d:acc=%s,pwd=%s",im_addr,sip_acc,sip_pwd);
		#endif
		//ShellCmdPrintJson(IMDivertAccTb);
	}
	
	else if(strcmp(para,"get_acc")==0)
	{

		para=GetShellCmdInputString(cmd, 2);
		if(para==NULL)
		{
			ShellCmdPrintf("para err");
			return 1;
		}
		type=atoi(para);
		para=GetShellCmdInputString(cmd, 3);
		if(para==NULL)
		{
			ShellCmdPrintf("para err");
			return 1;
		}
		im_addr=atoi(para);
		if(type==0)
		{
			if(get_sip_dt_im_acc_from_tb(im_addr,sip_acc,sip_pwd)<0)
			{
				ShellCmdPrintf("have no acc");
				return 1;
			}
			ShellCmdPrintf("dt-im-%d:acc=%s,pwd=%s",im_addr,sip_acc,sip_pwd);
		}
		else if(type==1)
		{
			if(get_sip_ix_im_acc_from_tb(im_addr,sip_acc,sip_pwd)<0)
			{
				ShellCmdPrintf("have no acc");
				return 1;
			}
			ShellCmdPrintf("ix-im-%d:acc=%s,pwd=%s",im_addr,sip_acc,sip_pwd);
		}
		else
		{
			ShellCmdPrintf("add fail");
			return 1;	
		}
		qr_code=create_sip_phone_qrcode(sip_acc,sip_pwd);
		if(qr_code)
		{
			ShellCmdPrintf("qr:\n%s",qr_code);
			free(qr_code);
		}
	}
	#if 0
	else if(strcmp(para,"ch_pwd_tb")==0)
	{
		para=GetShellCmdInputString(cmd, 3);
		if(para==NULL)
		{
			ShellCmdPrintf("para err");
			return 1;
		}
		strcpy(sip_pwd,para);
		
		para=GetShellCmdInputString(cmd, 2);
		if(para==NULL)
		{
			ShellCmdPrintf("para err");
			return 1;
		}
		im_addr=atoi(para);
		modefy_sip_im_phone_pwd(im_addr,sip_pwd);
		ShellCmdPrintJson(IMDivertAccTb);
	}
	#endif
	else if(strcmp(para,"ch_pwd")==0)
	{
		para=GetShellCmdInputString(cmd, 4);
		if(para==NULL)
		{
			ShellCmdPrintf("para err");
			return 1;
		}
		strcpy(sip_pwd,para);
		
		para=GetShellCmdInputString(cmd, 3);
		if(para==NULL)
		{
			ShellCmdPrintf("para err");
			return 1;
		}
		im_addr=atoi(para);
		para=GetShellCmdInputString(cmd, 2);
		if(para==NULL)
		{
			ShellCmdPrintf("para err");
			return 1;
		}
#if 1
		type=atoi(para);
		if(type==0)
		{
			if(ch_sip_dt_im_phone_pwd(im_addr,sip_pwd)<0)
			{
				ShellCmdPrintf("change fail");
				return 1;
			}
		}
		if(type==1)
		{
			if(ch_sip_ix_im_phone_pwd(im_addr,sip_pwd)<0)
			{
				ShellCmdPrintf("change fail");
				return 1;
			}
		}
		//MyTablePrint(IMDivertAccTb);
#else
		//ShellCmdPrintJson(IMDivertAccTb);
#endif
	}
	else if(strcmp(para,"del")==0)
	{
		//MyTablePrint(IMDivertAccTb);

		para=GetShellCmdInputString(cmd, 3);
		if(para==NULL)
		{
			ShellCmdPrintf("para err");
			return 1;
		}
		im_addr=atoi(para);
		para=GetShellCmdInputString(cmd, 2);
		if(para==NULL)
		{
			ShellCmdPrintf("para err");
			return 1;
		}
		type=atoi(para);
		if(type==0)
		{
			if(del_sip_dt_im_acc_by_addr(im_addr)<0)
			{
				ShellCmdPrintf("del fail");
				return 1;
			}
		}
		if(type==1)
		{
			if(del_sip_ix_im_acc_by_addr(im_addr)<0)
			{
				ShellCmdPrintf("del fail");
				return 1;
			}
		}
	}
	else if(strcmp(para,"divert_on")==0)
	{
		//MyTablePrint(IMDivertAccTb);

		para=GetShellCmdInputString(cmd, 3);
		if(para==NULL)
		{
			ShellCmdPrintf("para err");
			return 1;
		}
		im_addr=atoi(para);
#if 1
		para=GetShellCmdInputString(cmd, 2);
		if(para==NULL)
		{
			ShellCmdPrintf("para err");
			return 1;
		}
		type=atoi(para);
		if(type==0)
		{
			if(SetDtImDivertOn(im_addr)!=1)
			{
				ShellCmdPrintf("SET fail");
				return 1;
			}
		}
		if(type==1)
		{
			sprintf(addr,"%s%04d01",GetSysVerInfo_bd(),im_addr);
			if(SetIxImDivertOn(addr)!=1)
			{
				ShellCmdPrintf("SET fail");
				return 1;
			}
		}
	}
	else if(strcmp(para,"divert_off")==0)
	{
		//MyTablePrint(IMDivertAccTb);

		para=GetShellCmdInputString(cmd, 3);
		if(para==NULL)
		{
			ShellCmdPrintf("para err");
			return 1;
		}
		im_addr=atoi(para);
		para=GetShellCmdInputString(cmd, 2);
		if(para==NULL)
		{
			ShellCmdPrintf("para err");
			return 1;
		}
		type=atoi(para);
		if(type==0)
		{
			if(SetDtImDivertOff(im_addr)!=1)
			{
				ShellCmdPrintf("SET fail");
				return 1;
			}
		}
		if(type==1)
		{
			sprintf(addr,"%s%04d01",GetSysVerInfo_bd(),im_addr);
			if(SetIxImDivertOff(addr)!=1)
			{
				ShellCmdPrintf("SET fail");
				return 1;
			}
		}
#else

		del_sip_im_acc_by_addr(im_addr);
		ShellCmdPrintJson(IMDivertAccTb);
#endif
	}
	else if(strcmp(para,"use_default")==0)
	{
		if(sip_master_use_default()<0)
		{
			ShellCmdPrintf("act fail");
			return 1;
		}
		ShellCmdPrintf("act ok");
		
	}
	else if(strcmp(para,"re_reg")==0)
	{
		para=GetShellCmdInputString(cmd, 2);
		if(para!=NULL)
		{
			set_sip_master_ser(para);
		}
		sip_master_rereg();
		ShellCmdPrintf("act ok");
	}
	else if(strcmp(para,"clear_rul")==0)
	{
		para=GetShellCmdInputString(cmd, 3);
		if(para==NULL)
		{
			ShellCmdPrintf("para err");
			return 1;
		}
		im_addr=atoi(para);
		para=GetShellCmdInputString(cmd, 2);
		if(para==NULL)
		{
			ShellCmdPrintf("para err");
			return 1;
		}
		type=atoi(para);
		if(type==0)
		{
			sip_dt_im_acc_clear_rul(im_addr);
			ShellCmdPrintf("act ok");
		}
		if(type==1)
		{
			sip_ix_im_acc_clear_rul(im_addr);
			ShellCmdPrintf("act ok");
		}	
	}
	else if(strcmp(para,"ipg_mode")==0)
	{
		para=GetShellCmdInputString(cmd, 2);
		if(para==NULL)
		{
			ShellCmdPrintf("para err");
			return 1;
		}
		Set_IpgCompMode(atoi(para));
	}
	
	return 0;	
}

int Event_SipManage_Callback(cJSON *cmd)
{
	#ifdef PID_IX850
	char *para=GetShellCmdInputString(cmd, 1);
	char sip_pwd[40];
	char sip_acc[40];
	char addr[20];
	int type;
	int im_addr;
	cJSON *jpara;
	cJSON *set_para;
	char *qr_code;
	
	
	char *ctrl=GetEventItemString(cmd,"Ctrl");
	void (*callback)(void);
	callback=GetEventItemInt(cmd,"Callback");
	if(strcmp(ctrl,"enable")==0)
	{	
		
	}
	else if(strcmp(ctrl,"disable")==0)
	{	
		
	}
	else if(strcmp(ctrl,"use_default")==0)
	{	
		if(sip_master_use_default()<0)
		{
			API_SettingErrorTip("FAIL");
		}
		else
		{
			API_Event_NameAndMsg(EventMenuSettingProcess, "SettingRefresh", NULL);
			API_SettingSuccTip("OK");
		}
	}
	else if(strcmp(ctrl,"re_reg")==0)
	{	
		sip_master_rereg();
		int try_cnt=12;
		while(--try_cnt>0)
		{	
			if(Get_SipAccount_State())
			{
				API_SettingSuccTip("OK");
				break;
			}
			else if(Get_SipReg_ErrCode()&&try_cnt<6)
			{
				if(Get_SipReg_ErrCode()==401)
					API_SettingErrorTip("Verify error");
				else
				{
					API_SettingErrorTip("FAIL");
				}
				break;
			}
			sleep(1);
		}
		if(try_cnt<=0)
			API_SettingErrorTip("Timeout");
	}
	else if(strcmp(ctrl,"create_acc_by_res")==0)
	{
		if(strcmp(API_PublicInfo_Read_String(PB_SIP_CONNECT_NETWORK),"NONE")==0)
		{
			API_TIPS_Ext_Time("Have no internet",2);
			return;
		}
		cJSON *res_list=GetDataSetList();
		cJSON *one_item;
		int size=cJSON_GetArraySize(res_list);
		int i;
		char disp[100];
		printf_json(res_list, "1112");
		if(size==0)
		{
			API_TIPS_Ext_Time("Have no res",2);
		}
		else
		{
			API_TIPS_Mode("Create sub acc start");
			usleep(100*1000);
		}
		for(i=0;i<size;i++)
		{
			MainMenu_Reset_Time();
			one_item=cJSON_GetArrayItem(res_list,i);
			GetJsonDataPro(one_item,"IX_ADDR", sip_pwd);
			if(memcmp(sip_pwd+8,"49",2)==0)
				continue;
				
			memcpy(addr,sip_pwd+4,4);
			addr[4]=0;
			im_addr=atoi(addr);
			printf_json(one_item, sip_pwd);
			if(get_sip_ix_im_acc_from_tb(im_addr,sip_acc,sip_pwd)==0)
			{
				sprintf(disp,"im(%d) have already acc",im_addr);
				API_TIPS_Mode_Fresh(NULL, disp);
				usleep(500*1000);
				continue;
			}
			if(add_sip_ix_im_acc(im_addr,sip_acc,sip_pwd)<0)
			{
				sprintf(disp,"create im(%d) acc fail",im_addr);
				
			}
			else
			{
				sprintf(disp,"create im(%d) acc success",im_addr);
			}
			API_TIPS_Mode_Fresh(NULL, disp);
			if(i==(size-1))
				usleep(500*1000);
		}
		API_TIPS_Mode_Close(NULL);
		cJSON_Delete(res_list);
		if(callback)
			(*callback)();
		
	}
	else if(strcmp(ctrl,"add_acc")==0)
	{

	}
	else if(strcmp(ctrl,"del_acc")==0)
	{

	}
	else if(strcmp(ctrl,"divert_on")==0)
	{

	}
	else if(strcmp(ctrl,"divert_off")==0)
	{

	}
	#endif
	#if 0
	else if(strcmp(para,"get_master")==0)
	{
		jpara=API_Para_Read_Public(SipConfig);
		char* temp;
		temp=cJSON_Print(jpara);
		if(temp==NULL)
		{
			ShellCmdPrintf("Get State Fail");
			return 1;
		}
		ShellCmdPrintf(temp);
		free(temp);
	
	}
	else if(strcmp(para,"get_tb")==0)
	{
	#if 0
		if(IMDivertAccTb==NULL)
		{
			if(sip_im_acc_tb_load()<0)
			{
				ShellCmdPrintf("load tb fail");
				return 1;
			}
		}
		ShellCmdPrintJson(IMDivertAccTb);
	#endif
	}
	else if(strcmp(para,"add_tb")==0)
	{
		para=GetShellCmdInputString(cmd, 5);
		if(para==NULL)
		{
			ShellCmdPrintf("para err");
			return 1;
		}
		strcpy(sip_pwd,para);
		para=GetShellCmdInputString(cmd, 4);
		if(para==NULL)
		{
			ShellCmdPrintf("para err");
			return 1;
		}
		strcpy(sip_acc,para);
		para=GetShellCmdInputString(cmd, 3);
		if(para==NULL)
		{
			ShellCmdPrintf("para err");
			return 1;
		}
		strcpy(addr,para);
		para=GetShellCmdInputString(cmd, 2);
		if(para==NULL)
		{
			ShellCmdPrintf("para err");
			return 1;
		}
		add_sip_dev_acc_to_tb(atoi(para),addr,sip_acc,sip_pwd);
		//ShellCmdPrintJson(IMDivertAccTb);
	}
	else if(strcmp(para,"add")==0)
	{
		
		para=GetShellCmdInputString(cmd, 2);
		if(para==NULL)
		{
			ShellCmdPrintf("para err");
			return 1;
		}
		type=atoi(para);
		para=GetShellCmdInputString(cmd,3);
		if(para==NULL)
		{
			ShellCmdPrintf("para err");
			return 1;
		}
		strcpy(addr,para);
		if(type==0)
		{
			if(add_sip_dt_im_acc(atoi(addr),sip_acc,sip_pwd)<0)
			{
				ShellCmdPrintf("add fail");
				return 1;	
			}
			ShellCmdPrintf("add ok,dt im-%s:acc=%s,pwd=%s",addr,sip_acc,sip_pwd);
			
		}
		else if(type==1)
		{
			if(add_sip_ix_im_acc(atoi(addr),sip_acc,sip_pwd)<0)
			{
				ShellCmdPrintf("add fail");
				return 1;	
			}
			ShellCmdPrintf("add ok,ix im-%s:acc=%s,pwd=%s",addr,sip_acc,sip_pwd);
		}
		else
		{
			ShellCmdPrintf("add fail");
			return 1;	
		}
		qr_code=create_sip_phone_qrcode(sip_acc,sip_pwd);
		if(qr_code)
		{
			ShellCmdPrintf("qr:\n%s",qr_code);
			free(qr_code);
		}
		#if 0
		if(get_sip_dev_acc_from_tb(type,addr,sip_acc,sip_pwd)>=0)
		{
			ShellCmdPrintf("already have acc,[%s]im-%s:acc=%s,pwd=%s",DivertAccType_str[type],addr,sip_acc,sip_pwd);
			return 1;
		}
		if(add_sip_dt_im_acc(im_addr,sip_acc,sip_pwd)<0)
		{
			ShellCmdPrintf("add fail");
			return 1;
		}
		
		ShellCmdPrintf("add ok,im-%d:acc=%s,pwd=%s",im_addr,sip_acc,sip_pwd);
		#endif
		//ShellCmdPrintJson(IMDivertAccTb);
	}
	
	else if(strcmp(para,"get_acc")==0)
	{

		para=GetShellCmdInputString(cmd, 2);
		if(para==NULL)
		{
			ShellCmdPrintf("para err");
			return 1;
		}
		type=atoi(para);
		para=GetShellCmdInputString(cmd, 3);
		if(para==NULL)
		{
			ShellCmdPrintf("para err");
			return 1;
		}
		im_addr=atoi(para);
		if(type==0)
		{
			if(get_sip_dt_im_acc_from_tb(im_addr,sip_acc,sip_pwd)<0)
			{
				ShellCmdPrintf("have no acc");
				return 1;
			}
			ShellCmdPrintf("dt-im-%d:acc=%s,pwd=%s",im_addr,sip_acc,sip_pwd);
		}
		else if(type==1)
		{
			if(get_sip_ix_im_acc_from_tb(im_addr,sip_acc,sip_pwd)<0)
			{
				ShellCmdPrintf("have no acc");
				return 1;
			}
			ShellCmdPrintf("ix-im-%d:acc=%s,pwd=%s",im_addr,sip_acc,sip_pwd);
		}
		else
		{
			ShellCmdPrintf("add fail");
			return 1;	
		}
		qr_code=create_sip_phone_qrcode(sip_acc,sip_pwd);
		if(qr_code)
		{
			ShellCmdPrintf("qr:\n%s",qr_code);
			free(qr_code);
		}
	}
	#if 0
	else if(strcmp(para,"ch_pwd_tb")==0)
	{
		para=GetShellCmdInputString(cmd, 3);
		if(para==NULL)
		{
			ShellCmdPrintf("para err");
			return 1;
		}
		strcpy(sip_pwd,para);
		
		para=GetShellCmdInputString(cmd, 2);
		if(para==NULL)
		{
			ShellCmdPrintf("para err");
			return 1;
		}
		im_addr=atoi(para);
		modefy_sip_im_phone_pwd(im_addr,sip_pwd);
		ShellCmdPrintJson(IMDivertAccTb);
	}
	#endif
	else if(strcmp(para,"ch_pwd")==0)
	{
		para=GetShellCmdInputString(cmd, 4);
		if(para==NULL)
		{
			ShellCmdPrintf("para err");
			return 1;
		}
		strcpy(sip_pwd,para);
		
		para=GetShellCmdInputString(cmd, 3);
		if(para==NULL)
		{
			ShellCmdPrintf("para err");
			return 1;
		}
		im_addr=atoi(para);
		para=GetShellCmdInputString(cmd, 2);
		if(para==NULL)
		{
			ShellCmdPrintf("para err");
			return 1;
		}
#if 1
		type=atoi(para);
		if(type==0)
		{
			if(ch_sip_dt_im_phone_pwd(im_addr,sip_pwd)<0)
			{
				ShellCmdPrintf("change fail");
				return 1;
			}
		}
		if(type==1)
		{
			if(ch_sip_ix_im_phone_pwd(im_addr,sip_pwd)<0)
			{
				ShellCmdPrintf("change fail");
				return 1;
			}
		}
		//MyTablePrint(IMDivertAccTb);
#else
		//ShellCmdPrintJson(IMDivertAccTb);
#endif
	}
	else if(strcmp(para,"del")==0)
	{
		//MyTablePrint(IMDivertAccTb);

		para=GetShellCmdInputString(cmd, 3);
		if(para==NULL)
		{
			ShellCmdPrintf("para err");
			return 1;
		}
		im_addr=atoi(para);
		para=GetShellCmdInputString(cmd, 2);
		if(para==NULL)
		{
			ShellCmdPrintf("para err");
			return 1;
		}
		type=atoi(para);
		if(type==0)
		{
			if(del_sip_dt_im_acc_by_addr(im_addr)<0)
			{
				ShellCmdPrintf("del fail");
				return 1;
			}
		}
		if(type==1)
		{
			if(del_sip_ix_im_acc_by_addr(im_addr)<0)
			{
				ShellCmdPrintf("del fail");
				return 1;
			}
		}
	}
	else if(strcmp(para,"divert_on")==0)
	{
		//MyTablePrint(IMDivertAccTb);

		para=GetShellCmdInputString(cmd, 3);
		if(para==NULL)
		{
			ShellCmdPrintf("para err");
			return 1;
		}
		im_addr=atoi(para);
#if 1
		para=GetShellCmdInputString(cmd, 2);
		if(para==NULL)
		{
			ShellCmdPrintf("para err");
			return 1;
		}
		type=atoi(para);
		if(type==0)
		{
			if(SetDtImDivertOn(im_addr)!=1)
			{
				ShellCmdPrintf("SET fail");
				return 1;
			}
		}
		if(type==1)
		{
			sprintf(addr,"%s%04d01",GetSysVerInfo_bd(),im_addr);
			if(SetIxImDivertOn(addr)!=1)
			{
				ShellCmdPrintf("SET fail");
				return 1;
			}
		}
	}
	else if(strcmp(para,"divert_off")==0)
	{
		//MyTablePrint(IMDivertAccTb);

		para=GetShellCmdInputString(cmd, 3);
		if(para==NULL)
		{
			ShellCmdPrintf("para err");
			return 1;
		}
		im_addr=atoi(para);
		para=GetShellCmdInputString(cmd, 2);
		if(para==NULL)
		{
			ShellCmdPrintf("para err");
			return 1;
		}
		type=atoi(para);
		if(type==0)
		{
			if(SetDtImDivertOff(im_addr)!=1)
			{
				ShellCmdPrintf("SET fail");
				return 1;
			}
		}
		if(type==1)
		{
			sprintf(addr,"%s%04d01",GetSysVerInfo_bd(),im_addr);
			if(SetIxImDivertOff(addr)!=1)
			{
				ShellCmdPrintf("SET fail");
				return 1;
			}
		}
#else

		del_sip_im_acc_by_addr(im_addr);
		ShellCmdPrintJson(IMDivertAccTb);
#endif
	}
	else if(strcmp(para,"use_default")==0)
	{
		if(sip_master_use_default()<0)
		{
			ShellCmdPrintf("act fail");
			return 1;
		}
		ShellCmdPrintf("act ok");
		
	}
	else if(strcmp(para,"re_reg")==0)
	{
		para=GetShellCmdInputString(cmd, 2);
		if(para!=NULL)
		{
			set_sip_master_ser(para);
		}
		sip_master_rereg();
		ShellCmdPrintf("act ok");
	}
	else if(strcmp(para,"clear_rul")==0)
	{
		para=GetShellCmdInputString(cmd, 3);
		if(para==NULL)
		{
			ShellCmdPrintf("para err");
			return 1;
		}
		im_addr=atoi(para);
		para=GetShellCmdInputString(cmd, 2);
		if(para==NULL)
		{
			ShellCmdPrintf("para err");
			return 1;
		}
		type=atoi(para);
		if(type==0)
		{
			sip_dt_im_acc_clear_rul(im_addr);
			ShellCmdPrintf("act ok");
		}
		if(type==1)
		{
			sip_ix_im_acc_clear_rul(im_addr);
			ShellCmdPrintf("act ok");
		}	
	}
	else if(strcmp(para,"ipg_mode")==0)
	{
		para=GetShellCmdInputString(cmd, 2);
		if(para==NULL)
		{
			ShellCmdPrintf("para err");
			return 1;
		}
		Set_IpgCompMode(atoi(para));
	}
	#endif
	return 0;	
}
/////////////////////////////////////
