
#include <ortp/ortp.h>
#include <list.h>
#include "ortp/payloadtype.h"

#include "rtp_utility.h"
#include "rtp_video_send.h"
#include "elog_forcall.h"
//#include "../video_service/ip_camera_control/encoder_vin/capture_procsee.h"
//#include "../task_Sundry/task_Power/task_Power.h"

//////////////////////////////////////////////////////////////////////////////////////////////////////
#define TYPE_FU_A 		28    /*fragmented unit 0x1C*/
#define TYPE_FU265_A 	49    /*fragmented unit 0x31*/

static inline void nal_header_init(uint8_t *h, uint8_t nri, uint8_t type)
{
	*h=((nri&0x3)<<5) | (type & ((1<<5)-1));
}

static inline uint8_t nal_header_get_type(const uint8_t *h)
{
	return (*h) & ((1<<5)-1);
}

static inline uint8_t nal_header_get_nri(const uint8_t *h)
{
	return ((*h) >> 5) & 0x3;
}

static inline void h265_nal_header_init(uint8_t *h, uint8_t nri, uint8_t type)
{
	*h=((type<<1)&0x7f);
}

static inline uint8_t h265_nal_header_get_type(const uint8_t *h)
{
	return (((*h)>>1)&0x3F);
}

static inline uint8_t h265_nal_header_get_nri(const uint8_t *h)
{
	return 1;
}

typedef enum nal_unit_type_e
{
    NAL_UNKNOWN     = 0x00000000,
    NAL_SLICE       = 0x00000001,
    NAL_SLICE_DPA   = 0x00000002,
    NAL_SLICE_DPB   = 0x00000003,
    NAL_SLICE_DPC   = 0x00000004,
    NAL_SLICE_IDR   = 0x00000005,    /* ref_idc != 0 */
    NAL_SEI         = 0x00000006,    /* ref_idc == 0 */
    NAL_SPS         = 0x00000007,
    NAL_PPS         = 0x00000008,
    NAL_AUD         = 0x00000009,
    NAL_FILLER      = 0x0000000C,
    /* ref_idc == 0 for 6,9,10,11,12 */

    NAL265_SLICE_IDR   = 19,
    NAL265_SEI         = 39,
    NAL265_SPS         = 33,
    NAL265_PPS         = 34,
    NAL265_VPS         = 32,
    NAL265_FILLER      = 38,	
} nal_type;

typedef struct h264_nal_t {

    nal_type  i_type;        /* nal_unit_type_e */
    uint32_t  i_ipoffset;
    uint32_t  i_spsoffset;   /* nal sps offset*/
    uint32_t  i_ppsoffset;   /* nal pps offset*/
    uint32_t  i_iplen;
    uint32_t  i_spslen;      /* nal sps lenght*/
    uint32_t  i_ppslen;      /* nal pps lenght*/
    /* If param->b_annexb is set, Annex-B bytestream with startcode.
     * Otherwise, startcode is replaced with a 4-byte size.
     * This size is the size used in mp4/similar muxing; it is equal to i_payload-4 
    */
    uint8_t  *p_payload;

} h264_nal_t;

static void h264_xnals_init( h264_nal_t *xnals )
{
	xnals->i_type       = 0xffff0000;
	xnals->i_ipoffset   = 0;
	xnals->i_iplen      = 0;
	xnals->i_spsoffset  = 0;
	xnals->i_spslen     = 0;
	xnals->i_ppsoffset  = 0;
	xnals->i_ppslen     = 0;
}

//static uint8_t *s_pu8IFrameBuf = NULL;
//static uint32_t s_u32IFrameBufSize = 0;

static void h264_nals_to_msgb( uint8_t *Buffer, uint32_t num_nals, queue_t* nalus ) 
{
	uint32_t uNextSyncWordAdr=0, uBitLen;
	uint32_t uSyncWord = 0x01000000;
	uint32_t uCurFrame = NAL_UNKNOWN;
	uint8_t  u_type;
	uint32_t uFrameLen;
	nal_type eFrameMask;
	uint32_t uFrameOffset;
	h264_nal_t  nals;
	mblk_t *m;

	h264_xnals_init(&nals);

	nals.p_payload  = Buffer;

	//printf("nals.p_payload[0]  = %d.\n", nals.p_payload[0]);
	//printf("nals.p_payload[1]  = %d.\n", nals.p_payload[1]);
	//printf("nals.p_payload[2]  = %d.\n", nals.p_payload[2]);
	//printf("nals.p_payload[3]  = %d.\n", nals.p_payload[3]);
	//printf("nals.i_iplen       = %d.\n", num_nals);
	//printf("nals:%02x %02x %02x %02x %02x %02x %02x\n", nals.i_type,nals.i_ipoffset,nals.i_spsoffset,nals.i_ppsoffset,nals.i_iplen,nals.i_spslen,nals.i_ppslen);
	//int i;
	//for(i=0;i<8;i++)
	//	printf("%02x ",Buffer[i]);
	//printf("\n");
	if((nals.p_payload[4] & 0x1F) == NAL_SLICE_IDR) 
	{
		// It is I-frame
		nals.i_iplen = num_nals;
		nals.i_type = NAL_SLICE_IDR;
		m=allocb(nals.i_iplen + 10, 0);		
		memcpy(m->b_wptr, (Buffer + 4), nals.i_iplen-4);
		m->b_wptr += nals.i_iplen-4;
		putq(nalus,m);
		//printf("---------------\n");
		return;

	} 
	else if ((nals.p_payload[4] & 0x1F) == NAL_SLICE) 
	{
		// It is P-frame
		nals.i_iplen = num_nals;
		nals.i_type = NAL_SLICE;
		m=allocb(nals.i_iplen + 10, 0);		
		memcpy(m->b_wptr, (Buffer + 4), nals.i_iplen-4);
		m->b_wptr += nals.i_iplen-4;
		putq(nalus,m);
		//printf("0000000000000\n");
		return;

	} 
	else 
	{
		uBitLen = (num_nals - 4); //4:sync word len
		while( uBitLen ) 
		{
			if(memcmp(nals.p_payload + uNextSyncWordAdr, &uSyncWord, sizeof(uint32_t)) == 0) 
			{
				u_type = nals.p_payload[uNextSyncWordAdr + 4] & 0x1F;
				
				if (uCurFrame == NAL_SPS) 
				{
					nals.i_spslen = uNextSyncWordAdr - nals.i_spsoffset;
					//printf("uBitLen::nals.i_spslen SPS     = %d.\n", nals.i_spslen);
				} 
				else if (uCurFrame == NAL_PPS) 
				{
					nals.i_ppslen = uNextSyncWordAdr - nals.i_ppsoffset;
					//printf("uBitLen::nals.i_ppslen PPS     = %d.\n", nals.i_ppslen);
				}
				
				if(u_type == NAL_SPS) 
				{
					uCurFrame = NAL_SPS;
					nals.i_spsoffset = uNextSyncWordAdr;
					nals.i_type |= NAL_SPS;
					//printf("uBitLen::nals.i_spsoffset SPS  = %d.\n", nals.i_spsoffset);
					//printf("uBitLen::nals.i_type      SPS  = 0x%x.\n", nals.i_type);
				} 
				else if(u_type == NAL_PPS) 
				{
					uCurFrame =  NAL_PPS;
					nals.i_ppsoffset = uNextSyncWordAdr;
					nals.i_type |= NAL_PPS;
					//printf("uBitLen::nals.i_ppsoffset PPS  = %d.\n", nals.i_ppsoffset);
					//printf("uBitLen::nals.i_type      PPS  = 0x%x.\n", nals.i_type);
				} 
				else if(u_type == NAL_SLICE_IDR) 
				{
					nals.i_ipoffset = uNextSyncWordAdr;
					nals.i_iplen = num_nals - uNextSyncWordAdr;
					nals.i_type |= NAL_SLICE_IDR;
					//printf("uBitLen::nals.i_ipoffset IDR   = %d.\n", nals.i_ipoffset);
					//printf("uBitLen::nals.i_iplen    IDR   = %d.\n", nals.i_iplen);
					//printf("uBitLen::nals.i_type     IDR   = 0x%x.\n", nals.i_type);
					break;
				} 
				else if(u_type == NAL_SLICE) 
				{
					nals.i_ipoffset = uNextSyncWordAdr;
					nals.i_iplen = num_nals - uNextSyncWordAdr;
					nals.i_type |= NAL_SLICE;
					//printf("uBitLen::nals.i_ipoffset P    = %d.\n", nals.i_ipoffset);
					//printf("uBitLen::nals.i_iplen    P    = %d.\n", nals.i_iplen);
					//printf("uBitLen::nals.i_type     P    = 0x%x.\n", nals.i_type);
					break;
				}
			}
			uNextSyncWordAdr ++;
			uBitLen --;
		}

		if(uBitLen == 0) 
		{
			printf("Unknown H264 frame \n");
			uNextSyncWordAdr = 0;
			return;
		}
	}

	#if 0
	if(num_nals > s_u32IFrameBufSize)
	{
		if(s_pu8IFrameBuf)
		{
			free(s_pu8IFrameBuf);
			s_u32IFrameBufSize = 0;
		}
		s_pu8IFrameBuf = malloc(num_nals + 100);
		if(s_pu8IFrameBuf)
		{
			s_u32IFrameBufSize = num_nals;
		}
		else{
			s_u32IFrameBufSize = 0;
		}
	}

	if(s_pu8IFrameBuf == NULL)
	{
		printf("h264_nals_to_msgb: s_pu8IFrameBuf is null\n");
		return;
	}
	
	memcpy(s_pu8IFrameBuf, Buffer, num_nals);
	#endif
	if (nals.i_type & NAL_SLICE) 
	{
		eFrameMask = nals.i_type;
		while(eFrameMask) 
		{
			if(eFrameMask & NAL_SPS)
			{
				uFrameOffset = nals.i_spsoffset + 4;
				uFrameLen = nals.i_spslen - 4 ;
				eFrameMask &= (~NAL_SPS);
			}
			else if (eFrameMask & NAL_PPS)
			{
				uFrameOffset = nals.i_ppsoffset + 4;
				uFrameLen = nals.i_ppslen - 4;
				eFrameMask &= (~NAL_PPS);
			}
			else
			{
				uFrameOffset = nals.i_ipoffset + 4;
				uFrameLen = nals.i_iplen - 4;
				eFrameMask = 0;
			}
			m=allocb(uFrameLen + 10, 0);
			//memcpy(m->b_wptr, (s_pu8IFrameBuf + uFrameOffset), uFrameLen);
			memcpy(m->b_wptr, (Buffer + uFrameOffset), uFrameLen);
			m->b_wptr += uFrameLen;
			putq(nalus,m);
			//printf("11111111111111:%04x\n",eFrameMask);
		}
	} 
	else 
	{
		uFrameOffset = nals.i_ipoffset + 4;
		uFrameLen = nals.i_iplen - 4;
		m=allocb(uFrameLen + 10, 0);		
		//memcpy(m->b_wptr, (s_pu8IFrameBuf + uFrameOffset), uFrameLen);
		memcpy(m->b_wptr, (Buffer + uFrameOffset), uFrameLen);
		m->b_wptr += uFrameLen;
		putq(nalus,m);
		//printf("2222222222222\n");
	}
}
typedef struct h265_nal_t {

    nal_type  i_type;        /* nal_unit_type_e */
    uint32_t  i_ipoffset;
    uint32_t  i_vpsoffset;   /* nal vps offset*/
    uint32_t  i_spsoffset;   /* nal sps offset*/
    uint32_t  i_ppsoffset;   /* nal pps offset*/
    uint32_t  i_iplen;
	
    uint32_t  i_vpslen;      /* nal vps lenght*/
    uint32_t  i_spslen;      /* nal sps lenght*/
    uint32_t  i_ppslen;      /* nal pps lenght*/
    /* If param->b_annexb is set, Annex-B bytestream with startcode.
     * Otherwise, startcode is replaced with a 4-byte size.
     * This size is the size used in mp4/similar muxing; it is equal to i_payload-4 
    */
    uint8_t  *p_payload;

} h265_nal_t;

static void h265_xnals_init( h265_nal_t *xnals )
{
	xnals->i_type       = 0xffff0000;
	xnals->i_ipoffset   = 0;
	xnals->i_iplen      = 0;
	xnals->i_vpsoffset  = 0;
	xnals->i_vpslen     = 0;
	xnals->i_spsoffset  = 0;
	xnals->i_spslen     = 0;
	xnals->i_ppsoffset  = 0;
	xnals->i_ppslen     = 0;
}

static void h265_nals_to_msgb( uint8_t *Buffer, uint32_t num_nals, queue_t* nalus )
{
	uint32_t uNextSyncWordAdr=0, uBitLen;
	uint32_t uSyncWord = 0x01000000;
	uint32_t uCurFrame = NAL_UNKNOWN;
	uint8_t  u_type;
	uint32_t uFrameLen;
	nal_type eFrameMask;
	uint32_t uFrameOffset;
	h265_nal_t  nals;
	mblk_t *m;

	h265_xnals_init(&nals);

	nals.p_payload  = Buffer;
	//int i;
	//for(i=0;i<8;i++)
	//	printf("%02x ",Buffer[i]);
	//printf("\n");
	if( ((nals.p_payload[4]&0x7E)>>1) == NAL265_SLICE_IDR) 
	{
		// It is I-frame
		nals.i_iplen = num_nals;
		nals.i_type = NAL265_SLICE_IDR;
		m=allocb(nals.i_iplen + 10, 0);		
		memcpy(m->b_wptr, (Buffer + 4), nals.i_iplen-4);
		m->b_wptr += nals.i_iplen-4;
		putq(nalus,m);

		//printf("h265_nals_to_msgb 1111111111111111\n");
		return;

	} 
	else if ( ((nals.p_payload[4]&0x7E)>>1) == NAL_SLICE ) 
	{
		// It is P-frame
		nals.i_iplen = num_nals;
		nals.i_type = NAL_SLICE;
		m=allocb(nals.i_iplen + 10, 0);		
		memcpy(m->b_wptr, (Buffer + 4), nals.i_iplen-4);
		m->b_wptr += nals.i_iplen-4;
		putq(nalus,m);

		//printf("h265_nals_to_msgb 2222222222222222222\n");
		return;

	} 
	else 
	{
		uBitLen = (num_nals - 4); //4:sync word len
		while( uBitLen ) 
		{
			if(memcmp(nals.p_payload + uNextSyncWordAdr, &uSyncWord, sizeof(uint32_t)) == 0) 
			{
				u_type = ((nals.p_payload[uNextSyncWordAdr+4]&0x7E)>>1);
				//printf("h265_nals_to_msgb 2222222225555555 %d\n",u_type);
				if (uCurFrame == NAL265_VPS) 
				{
					nals.i_vpslen = uNextSyncWordAdr - nals.i_vpsoffset;
				} 				
				else if (uCurFrame == NAL265_SPS) 
				{
					nals.i_spslen = uNextSyncWordAdr - nals.i_spsoffset;
				} 
				else if (uCurFrame == NAL265_PPS) 
				{
					nals.i_ppslen = uNextSyncWordAdr - nals.i_ppsoffset;
				}
				
				if(u_type == NAL265_VPS) 
				{
					uCurFrame = NAL265_VPS;
					nals.i_vpsoffset = uNextSyncWordAdr;
					nals.i_type |= NAL265_VPS;
				} 
				else if(u_type == NAL265_SPS) 
				{
					uCurFrame = NAL265_SPS;
					nals.i_spsoffset = uNextSyncWordAdr;
					nals.i_type |= NAL265_SPS;
				} 
				else if(u_type == NAL265_PPS) 
				{
					uCurFrame =  NAL265_PPS;
					nals.i_ppsoffset = uNextSyncWordAdr;
					nals.i_type |= NAL265_PPS;
				} 
				else if(u_type == NAL265_SLICE_IDR) 
				{
					nals.i_ipoffset = uNextSyncWordAdr;
					nals.i_iplen = num_nals - uNextSyncWordAdr;
					nals.i_type |= NAL265_SLICE_IDR;
					break;
				} 
				else if(u_type == NAL_SLICE) 
				{
					nals.i_ipoffset = uNextSyncWordAdr;
					nals.i_iplen = num_nals - uNextSyncWordAdr;
					nals.i_type |= NAL_SLICE;
					break;
				}
			}
			uNextSyncWordAdr ++;
			uBitLen --;
		}

		if(uBitLen == 0) 
		{
			printf("Unknown H265 frame \n");
			uNextSyncWordAdr = 0;
			return;
		}
	}
	#if 0
	if(num_nals > s_u32IFrameBufSize)
	{
		if(s_pu8IFrameBuf)
		{
			free(s_pu8IFrameBuf);
			s_u32IFrameBufSize = 0;
		}
		s_pu8IFrameBuf = malloc(num_nals + 100);
		if(s_pu8IFrameBuf)
		{
			s_u32IFrameBufSize = num_nals;
		}
		else{
			s_u32IFrameBufSize = 0;
		}
	}

	if(s_pu8IFrameBuf == NULL)
	{
		printf("h265_nals_to_msgb: s_pu8IFrameBuf is null\n");
		return;
	}

	memcpy(s_pu8IFrameBuf, Buffer, num_nals);
	#endif
	if (nals.i_type & NAL_SLICE) 
	{	
		eFrameMask = nals.i_type;
		while(eFrameMask) 
		{
			//dprintf("h265_nals_to_msgb 444444444444444\n");
			if(eFrameMask & NAL265_VPS)
			{
				uFrameOffset = nals.i_vpsoffset + 4;
				uFrameLen = nals.i_vpslen - 4 ;
				eFrameMask &= (~NAL265_VPS);
			}
			else if(eFrameMask & NAL265_SPS)
			{
				uFrameOffset = nals.i_spsoffset + 4;
				uFrameLen = nals.i_spslen - 4 ;
				eFrameMask &= (~NAL265_SPS);
			}
			else if (eFrameMask & NAL265_PPS)
			{
				uFrameOffset = nals.i_ppsoffset + 4;
				uFrameLen = nals.i_ppslen - 4;
				eFrameMask &= (~NAL265_PPS);
			}
			else
			{
				uFrameOffset = nals.i_ipoffset + 4;
				uFrameLen = nals.i_iplen - 4;
				eFrameMask = 0;
			}
			m=allocb(uFrameLen + 10, 0);
			//memcpy(m->b_wptr, (s_pu8IFrameBuf + uFrameOffset), uFrameLen);
			memcpy(m->b_wptr, (Buffer + uFrameOffset), uFrameLen);
			m->b_wptr += uFrameLen;
			putq(nalus,m);
			//printf("h265_nals_to_msgb 3333333333333333uFrameLen=%d,uFrameOffset=%d\n", uFrameLen, uFrameOffset);
		}
	} 
	else 
	{
		//printf("h265_nals_to_msgb 555555555555555\n");
		uFrameOffset = nals.i_ipoffset + 4;
		uFrameLen = nals.i_iplen - 4;
		m=allocb(uFrameLen + 10, 0);		
		//memcpy(m->b_wptr, (s_pu8IFrameBuf + uFrameOffset), uFrameLen);
		memcpy(m->b_wptr, (Buffer + uFrameOffset), uFrameLen);
		m->b_wptr += uFrameLen;
		putq(nalus,m);
		//printf("h265_nals_to_msgb 66666666666666666\n");
	}	
	//dprintf("h265_nals_to_msgb 777777777777777777\n");
}
static void send_packet(RtpSession* s,uint32_t ts, mblk_t *m, bool_t marker)
{
	mblk_t *header;
	header = rtp_session_create_packet(s, 12, NULL, 0);
	rtp_set_markbit(header, marker);
	header->b_cont = m;
	rtp_session_sendm_with_ts(s, header, ts);	// this function will call "freemsg(im)" to free
}

static mblk_t *prepend_fu_indicator_and_header(mblk_t *m, uint8_t indicator,	bool_t start, bool_t end, uint8_t type)
{
	mblk_t *h=allocb(2,0);
	h->b_wptr[0]=indicator;
	h->b_wptr[1]=((start&0x1)<<7)|((end&0x1)<<6)|type;
	h->b_wptr+=2;
	h->b_cont=m;
	if (start) m->b_rptr++;/*skip original nalu header */
	return h;
}

static mblk_t *h265_prepend_fu_indicator_and_header(mblk_t *m, uint8_t indicator,	bool_t start, bool_t end, uint8_t type)
{
	mblk_t *h=allocb(3,0);
	h->b_wptr[0]=indicator;
	h->b_wptr[1]=1;
	h->b_wptr[2]=((start&0x1)<<7)|((end&0x1)<<6)|type;
	h->b_wptr+=3;
	h->b_cont=m;
	if (start) m->b_rptr += 2;/*skip original nalu header */
	return h;		
}

static void frag_nalu_and_send(RtpSession* psession, uint32_t ts, mblk_t *nalu, bool_t marker, int maxsize)
{
	mblk_t *m;
	int payload_max_size = maxsize-2;/*minus FUA header*/
	uint8_t fu_indicator;
	uint8_t type=nal_header_get_type(nalu->b_rptr);
	uint8_t nri=nal_header_get_nri(nalu->b_rptr);
	bool_t start=TRUE;

	nal_header_init(&fu_indicator,nri,TYPE_FU_A);
	while( nalu->b_wptr-nalu->b_rptr > payload_max_size )
	{
		m=dupb(nalu);
		nalu->b_rptr += payload_max_size;
		m->b_wptr = nalu->b_rptr;
		m=prepend_fu_indicator_and_header(m,fu_indicator,start,FALSE,type);
		send_packet(psession,ts,m,FALSE);
		start=FALSE;
	}
	/*send last packet */
	m=prepend_fu_indicator_and_header(nalu,fu_indicator,FALSE,TRUE,type);
	send_packet(psession,ts,m,marker);
}

static void h265_frag_nalu_and_send(RtpSession* psession, uint32_t ts, mblk_t *nalu, bool_t marker, int maxsize)
{
	mblk_t *m;
	int payload_max_size = maxsize-3;/*minus FUA header*/
	uint8_t fu_indicator;
	uint8_t type=h265_nal_header_get_type(nalu->b_rptr);
	uint8_t nri=h265_nal_header_get_nri(nalu->b_rptr);
	bool_t start=TRUE;
	
	h265_nal_header_init(&fu_indicator,nri,TYPE_FU265_A);
	while( nalu->b_wptr-nalu->b_rptr > payload_max_size )
	{
		m=dupb(nalu);
		nalu->b_rptr += payload_max_size;
		m->b_wptr = nalu->b_rptr;
		m=h265_prepend_fu_indicator_and_header(m,fu_indicator,start,FALSE,type);
		send_packet(psession,ts,m,FALSE);
		start=FALSE;
	}
	/*send last packet */
	m=h265_prepend_fu_indicator_and_header(nalu,fu_indicator,FALSE,TRUE,type);
	send_packet(psession,ts,m,marker);	
}

/////////////////////////////////////////////////////////////////////////////////////////////

//queue_t nalus;

/*process NALUs and pack them into rtp payloads */
#define DEFAULT_MAX_PAYLOAD_SIZE 1200
static void rfc3984_pack( RtpSession* 	psession, queue_t *naluq, uint32_t ts )
{
	mblk_t *m;
	int sz;
	bool_t end;
	while( (m=getq(naluq)) != NULL )
	{
		end = qempty(naluq);
		
		sz = m->b_wptr - m->b_rptr;
	
		if( sz > DEFAULT_MAX_PAYLOAD_SIZE )
		{
			//printf("Sending FU-A packets,len[%d]\n",sz);
			frag_nalu_and_send(psession,ts,m,end,DEFAULT_MAX_PAYLOAD_SIZE);
		}
		else
		{
			//printf("Sending Single NAL,len[%d]\n",sz);
			send_packet(psession,ts,m,end);
		}
	}
}

static void h265_rfc3984_pack( RtpSession* 	psession, queue_t *naluq, uint32_t ts )
{
	mblk_t *m;
	int sz;
	bool_t end;
	while( (m=getq(naluq)) != NULL )
	{
		end = qempty(naluq);
		
		sz = m->b_wptr - m->b_rptr;
	
		if( sz > DEFAULT_MAX_PAYLOAD_SIZE )
		{
			h265_frag_nalu_and_send(psession,ts,m,end,DEFAULT_MAX_PAYLOAD_SIZE);
		}
		else
		{
			//printf("Sending Single NAL,len[%d]\n",sz);
			send_packet(psession,ts,m,end);
		}
	}
}

/////////////////////////////////////////////////////////////////////////////////////////////
#if 0
static RtpSession* 	pvdsession = NULL;
static char			vd_server[20];
static short		vd_port 	= 0;

static pthread_t	vd_send_tid;
static queue_t 		vd_send_nalus;
static int			vd_queue_size;
static pthread_mutex_t vd_send_nalus_lck = PTHREAD_MUTEX_INITIALIZER;
#endif
#if 0
typedef struct
{
	RtpSession* 	pvdsession;
	char			vd_server[20];
	short		vd_port;

	pthread_t	vd_send_tid;
	queue_t 		vd_send_nalus;
	queue_t 		nalus;
	int			vd_queue_size;
	int 			pkt_send_flag;
	int			state;
	int 			videoType;
	pthread_mutex_t vd_send_nalus_lck;
}VtkRtpSendObj_T;
#endif
typedef struct
{
	struct list_head one_node;
	VtkRtpSendObj_T VtkRtpSend;
}VtkRtpSendUniSub_T;

struct list_head VtkRtpSendUni_List;
pthread_mutex_t VtkRtpSendUni_List_lock;

VtkRtpSendObj_T	VtkRtpSend_WAN={0};
VtkRtpSendObj_T	VtkRtpSend_LAN={0};

int rfc3984_pack_send(VtkRtpSendObj_T *VtkRtpSendObj, unsigned char *pack, int size, int ts)
{
	h264_nals_to_msgb(pack, size, &VtkRtpSendObj->nalus);
	rfc3984_pack(VtkRtpSendObj->pvdsession,&VtkRtpSendObj->nalus,ts);
}
int h265_rfc3984_pack_send(VtkRtpSendObj_T *VtkRtpSendObj, unsigned char *pack, int size, int ts)
{
	h265_nals_to_msgb(pack, size, &VtkRtpSendObj->nalus);
	h265_rfc3984_pack(VtkRtpSendObj->pvdsession,&VtkRtpSendObj->nalus,ts);
}
static void user_assign_payload_type(PayloadType *const_pt, int number)
{
	PayloadType *pt;
	pt=payload_type_clone(const_pt);
	pt->user_data=(void*)((long)number);
	rtp_profile_set_payload(&av_profile,number,pt);
}

RtpSession* rtpsession_new_s(char* addr, short port)
{
	RtpSession *session;
	session=rtp_session_new(RTP_SESSION_SENDONLY);	
	rtp_session_set_scheduling_mode(session,0);
	rtp_session_set_blocking_mode(session,0);
	rtp_session_set_remote_addr(session,addr,port);
	//rtp_session_set_payload_type(session,34);
	//rtp_session_set_payload_type(session,102);
	rtp_session_set_payload_type(session,96);
	printf("rtpsession_new_s OK!!!\n");
	return session;
}

int set_high_prio(void)
{
	struct sched_param param;
	int policy=SCHED_RR;
	memset(&param,0,sizeof(param));
	int result=0;
	int min_prio, max_prio;

	min_prio = sched_get_priority_min(policy);
	max_prio = sched_get_priority_max(policy);
	param.sched_priority=max_prio;
	if((result=pthread_setschedparam(pthread_self(),policy, &param))) 
	{
		if (result==EPERM)
		{
			/*
				The linux kernel has 
				sched_get_priority_max(SCHED_OTHER)=sched_get_priority_max(SCHED_OTHER)=0.
				As long as we can't use SCHED_RR or SCHED_FIFO, the only way to increase priority of a calling thread
				is to use setpriority().
			*/
			if (setpriority(PRIO_PROCESS,0,-20)==-1)
			{
				printf("setpriority() failed: %s, nevermind.",strerror(errno));
				result = -1;
			}
			else
			{
				printf("priority increased to maximum.");
			}
		}
		else
		{
			printf("Set pthread_setschedparam failed: %s",strerror(result));
			result = -1;
		}
	} 
	else 
	{
		printf("priority set to %s and value (%i)",policy==SCHED_FIFO ? "SCHED_FIFO" : "SCHED_RR", param.sched_priority);
	}
	return result;
}

/* the goal of that function is to return a absolute timestamp closest to real time, with respect of given packet_ts, which is a relative to an undefined origin*/
void* vd_send_dat_process(void *arg)
{
	VtkRtpSendObj_T* VtkRtpSendObj = (VtkRtpSendObj_T*)arg;
	mblk_t *im;	

	int size;
	int user_ts;
	struct timespec ts;	
	uint64_t orig;
	uint64_t tick_time;
	uint64_t curtime;
	uint64_t realtime;
	int64_t  diff;	

	//set_high_prio();
	
	clock_gettime(CLOCK_MONOTONIC,&ts);
	orig = (ts.tv_sec*1000LL) + (ts.tv_nsec+500000LL)/1000000LL;
	tick_time = 0;
	while(VtkRtpSendObj->state)
	{
		pthread_mutex_lock( &VtkRtpSendObj->vd_send_nalus_lck );
		if( (im = getq(&VtkRtpSendObj->vd_send_nalus)) != NULL ) 
		{
			user_ts = tick_time*90;
			size = im->b_wptr-im->b_rptr;
			if(VtkRtpSendObj->videoType)
				h265_rfc3984_pack_send(VtkRtpSendObj,im->b_rptr,size,user_ts);
			else
				rfc3984_pack_send(VtkRtpSendObj,im->b_rptr,size,user_ts);				
			freemsg(im);			
			//printf("------rfc3984_pack_send[len=%d][ts=%d] type=%d------\n",size,user_ts,VtkRtpSendObj->videoType);				
		}
		pthread_mutex_unlock( &VtkRtpSendObj->vd_send_nalus_lck );	
		tick_time += 20;
		while(1)
		{
			clock_gettime(CLOCK_MONOTONIC,&ts);
			curtime = (ts.tv_sec*1000LL) + (ts.tv_nsec+500000LL)/1000000LL;
			realtime = curtime-orig;		
			diff = tick_time-realtime;
			if( diff <= 0 )
				break;
			ts.tv_sec=0;
			ts.tv_nsec=(int)diff*1000000LL;
			nanosleep(&ts,NULL);
		}
	}
}

/////////////////////////////////////////////////////////////////////////////////////////////
// interface for send video stream
/////////////////////////////////////////////////////////////////////////////////////////////

// func:
//		create one rtp session with port and ts_inc
// para:
//		pserver:	rtp session instance ptr
//		port:		rtp send port
//		ts_inc:		rtp send timestamp increase gap
// return:
//		0/ok, -1/error
int start_video_rtp_sender(VtkRtpSendObj_T *VtkRtpSendObj, char* pserver, short port, int ts_inc)
{
	pthread_mutex_init(&VtkRtpSendObj->vd_send_nalus_lck,0);
	pthread_mutex_lock( &VtkRtpSendObj->vd_send_nalus_lck );

	if( VtkRtpSendObj->pvdsession == NULL )
	{	
		ortp_init();
		ortp_scheduler_init();
		//user_assign_payload_type(&payload_type_h264,96);
		user_assign_payload_type(&payload_type_h264,103);

		strcpy(VtkRtpSendObj->vd_server,pserver);
		VtkRtpSendObj->vd_port 	= port;
		VtkRtpSendObj->pvdsession = rtpsession_new_s(VtkRtpSendObj->vd_server,VtkRtpSendObj->vd_port);

		qinit(&VtkRtpSendObj->nalus);
		qinit(&VtkRtpSendObj->vd_send_nalus);
		VtkRtpSendObj->vd_queue_size = 0;
		VtkRtpSendObj->videoType=0;
		//printf("----------------start_video_rtp_sender[%s][%d]----------------------\n",VtkRtpSendObj->vd_server,VtkRtpSendObj->vd_port);

		//open_video_capture_device(CAPTURE_OPEN_TYPE_FOR_H264,0);
		//SetH264EncVideoPara(1);
		//API_POWER_VIDEO_ON();
		//API_LocalCaptureOn(0);
		//LinPhone_BeCalled_Start_Monitor();
		VtkRtpSendObj->state=1;
		pthread_create(&VtkRtpSendObj->vd_send_tid, NULL, vd_send_dat_process, (void*)VtkRtpSendObj);

		pthread_mutex_unlock( &VtkRtpSendObj->vd_send_nalus_lck );				
		return 0;
	}
	else
	{
		pthread_mutex_unlock( &VtkRtpSendObj->vd_send_nalus_lck );		
		return -1;
	}
}

int start_video_rtp_sender_wan(char* pserver, short port, int ts_inc)
{
	int ret;
	ret=start_video_rtp_sender(&VtkRtpSend_WAN,pserver,port,ts_inc);
	rtp_sender_start_pkt_send_wan();
	//API_LocalCaptureOn(0);
	//LinPhone_BeCalled_Start_Monitor();
	return ret;
}

int start_video_rtp_sender_lan(char* pserver, short port, int ts_inc)
{
	int ret;
	ret=start_video_rtp_sender(&VtkRtpSend_LAN,pserver,port,ts_inc);
	rtp_sender_start_pkt_send_lan();
	//API_LocalCaptureOn(0);
	//LinPhone_BeCalled_Start_Monitor();
	return ret;
}
// func:
//		delete one rtp session
// para:
//		none
// return:
//		0/ok, -1/error
int stop_video_rtp_sender(VtkRtpSendObj_T *VtkRtpSendObj)
{
	mblk_t *im;
	pthread_mutex_lock( &VtkRtpSendObj->vd_send_nalus_lck );
	VtkRtpSendObj->pkt_send_flag = 0;
	if( VtkRtpSendObj->pvdsession != NULL )
	{
		//pthread_cancel(VtkRtpSendObj->vd_send_tid);
		VtkRtpSendObj->state=0;
		pthread_mutex_unlock( &VtkRtpSendObj->vd_send_nalus_lck );
		pthread_join(VtkRtpSendObj->vd_send_tid,NULL);
		pthread_mutex_lock( &VtkRtpSendObj->vd_send_nalus_lck );
		printf("----------------stop_video_rtp_sender[%s][%d]----------------------\n",VtkRtpSendObj->vd_server,VtkRtpSendObj->vd_port);
		while( (im = getq(&VtkRtpSendObj->vd_send_nalus)) != NULL ) 
		{
			freemsg(im);	
		}
		while( (im = getq(&VtkRtpSendObj->nalus)) != NULL ) 
		{
			freemsg(im);	
		}
		flushq(&VtkRtpSendObj->nalus,0);
		flushq(&VtkRtpSendObj->vd_send_nalus,0);
		
		rtp_session_destroy(VtkRtpSendObj->pvdsession);
		VtkRtpSendObj->pvdsession = NULL;

		ortp_exit();

		//close_video_capture_device(CAPTURE_OPEN_TYPE_FOR_H264,0);
		//API_POWER_VIDEO_OFF();
		//API_LocalCaptureOff();

		pthread_mutex_unlock( &VtkRtpSendObj->vd_send_nalus_lck );		
		return 0;
	}
	else
	{
		pthread_mutex_unlock( &VtkRtpSendObj->vd_send_nalus_lck );	
		return -1;
	}
}
int stop_video_rtp_sender_wan(void)
{
	return stop_video_rtp_sender(&VtkRtpSend_WAN);
}

int stop_video_rtp_sender_lan(void)
{
	return stop_video_rtp_sender(&VtkRtpSend_LAN);
}
// lzh_20210222_s			

// func:
//		send one video packet data
// para:
//		buf:	data pointer
//		len:	data length
//		tick:	timestamp, if 0, use thread timestamp
// return:
//		0/ok, -1/error

int rtp_sender_send_with_ts(VtkRtpSendObj_T *VtkRtpSendObj,char *buf, int len, int tick ,int vdtype)
{
	pthread_mutex_lock( &VtkRtpSendObj->vd_send_nalus_lck );
	//printf("rtp_sender_send_with_ts vdtype=%d,size=%d,send_flag=%d,tick=%d\n",vdtype,len,VtkRtpSendObj->pkt_send_flag,tick);
	if( VtkRtpSendObj->pvdsession != NULL&&VtkRtpSendObj->pkt_send_flag)
	{
		VtkRtpSendObj->videoType=vdtype;
		if( !tick )
		{
			mblk_t *im;
			im = allocb(len, 0);
			memcpy(im->b_wptr, buf, len);
			im->b_wptr += len;	
			putq(&VtkRtpSendObj->vd_send_nalus,im);
			VtkRtpSendObj->vd_queue_size++;
		}
		else
		{
			if(VtkRtpSendObj->videoType)
				h265_rfc3984_pack_send(VtkRtpSendObj,buf,len,tick*90);
			else	
				rfc3984_pack_send(VtkRtpSendObj,buf,len,tick*90);
		}
		pthread_mutex_unlock( &VtkRtpSendObj->vd_send_nalus_lck );
		return 0;
	}
	else
	{
		pthread_mutex_unlock( &VtkRtpSendObj->vd_send_nalus_lck );
		return -1;
	}
}
// lzh_20210222_e			

int rtp_sender_send_with_ts_wan(char *buf, int len, int tick ,int vdtype)
{
	return rtp_sender_send_with_ts(&VtkRtpSend_WAN,buf,len,tick,vdtype);
}

int rtp_sender_send_with_ts_lan(char *buf, int len, int tick ,int vdtype)
{
	return rtp_sender_send_with_ts(&VtkRtpSend_LAN,buf,len,tick,vdtype);
}

void rtp_sender_set_pkt_send_flag(VtkRtpSendObj_T *VtkRtpSendObj,int new_status)
{
	VtkRtpSendObj->pkt_send_flag = new_status;
}

void rtp_sender_start_pkt_send_wan(void)
{
	rtp_sender_set_pkt_send_flag(&VtkRtpSend_WAN,1);
}
void rtp_sender_start_pkt_send_lan(void)
{
	rtp_sender_set_pkt_send_flag(&VtkRtpSend_LAN,1);
}

void rtp_sender_pause_pkt_send_wan(void)
{
	rtp_sender_set_pkt_send_flag(&VtkRtpSend_WAN,0);
}
void rtp_sender_pause_pkt_send_lan(void)
{
	rtp_sender_set_pkt_send_flag(&VtkRtpSend_LAN,0);
}

void rtp_sender_unicast_list_init(void)
{
	pthread_mutex_init(&VtkRtpSendUni_List_lock,0);
	INIT_LIST_HEAD(&VtkRtpSendUni_List);
}
static int uni_pkt_cnt=0;
void rtp_sender_unicast_list_clear(void)
{
	struct list_head *p_node,*p_node_temp;
	pthread_mutex_lock( &VtkRtpSendUni_List_lock);
	uni_pkt_cnt=0;
	list_for_each_safe(p_node,p_node_temp,&VtkRtpSendUni_List)
	{
		VtkRtpSendUniSub_T* pnode = list_entry(p_node,VtkRtpSendUniSub_T,one_node);
		list_del_init(&pnode->one_node);
		stop_video_rtp_sender(&pnode->VtkRtpSend);
		pthread_mutex_destroy(&pnode->VtkRtpSend.vd_send_nalus_lck);
		free(pnode);
	}
	pthread_mutex_unlock( &VtkRtpSendUni_List_lock);
}

int rtp_sender_unicast_subscribe(char* pserver, short port)
{
	int rev;
	struct list_head *p_node,*p_node_temp;
	
	list_for_each_safe(p_node,p_node_temp,&VtkRtpSendUni_List)
	{
		VtkRtpSendUniSub_T* pnode = list_entry(p_node,VtkRtpSendUniSub_T,one_node);
		if(strcmp(pnode->VtkRtpSend.vd_server,pserver)==0)
			return 0;
	}
	pthread_mutex_lock( &VtkRtpSendUni_List_lock);
	VtkRtpSendUniSub_T *VtkRtpSendUniSub= malloc(sizeof(VtkRtpSendUniSub_T));
	if(VtkRtpSendUniSub!=NULL)
	{
		memset(&VtkRtpSendUniSub->VtkRtpSend,0,sizeof(VtkRtpSendObj_T));
		INIT_LIST_HEAD(&VtkRtpSendUniSub->one_node);
		start_video_rtp_sender(&VtkRtpSendUniSub->VtkRtpSend,pserver,port,0);
		VtkRtpSendUniSub->VtkRtpSend.pkt_send_flag=1;
		list_add_tail(&VtkRtpSendUniSub->one_node,&VtkRtpSendUni_List); 
		rev=0;
		printf("rtp_sender_unicast_subscribe succ ip:%s,port:%d\n",pserver,port);
	}
	else
		rev=-1;
	pthread_mutex_unlock( &VtkRtpSendUni_List_lock);
	return rev;
}

void rtp_sender_send_with_ts_uni(char *buf, int len, int tick ,int vdtype)
{
	struct list_head *p_node,*p_node_temp;
	pthread_mutex_lock( &VtkRtpSendUni_List_lock);
	list_for_each_safe(p_node,p_node_temp,&VtkRtpSendUni_List)
	{
		VtkRtpSendUniSub_T* pnode = list_entry(p_node,VtkRtpSendUniSub_T,one_node);
		if(uni_pkt_cnt++%50==0)
			printf("rtp_sender_send_with_ts_uni %d\n",uni_pkt_cnt);
		rtp_sender_send_with_ts(&pnode->VtkRtpSend,buf,len,tick,vdtype);	
	}
	pthread_mutex_unlock( &VtkRtpSendUni_List_lock);
}
#if 0
static int		vd_encode_h265 = 0;
void SetRtpVedioEncodeH265(int vedioType)
{
	vd_encode_h265 = vedioType;
	//dprintf("----------------vd_encode_h265 =%d----------------------\n", vd_encode_h265);
}
#endif
int h264_data_check( uint8_t *Buffer, uint32_t num_nals) 
{
	uint32_t uNextSyncWordAdr=0, uBitLen;
	uint32_t uSyncWord = 0x01000000;
	uint32_t uCurFrame = NAL_UNKNOWN;
	uint8_t  u_type;
	uint32_t uFrameLen;
	nal_type eFrameMask;
	uint32_t uFrameOffset;
	h264_nal_t  nals;
	//mblk_t *m;

	h264_xnals_init(&nals);

	nals.p_payload  = Buffer;

	//printf("nals.p_payload[0]  = %d.\n", nals.p_payload[0]);
	//printf("nals.p_payload[1]  = %d.\n", nals.p_payload[1]);
	//printf("nals.p_payload[2]  = %d.\n", nals.p_payload[2]);
	//printf("nals.p_payload[3]  = %d.\n", nals.p_payload[3]);
	//printf("nals.i_iplen       = %d.\n", num_nals);
	//printf("nals:%02x %02x %02x %02x %02x %02x %02x\n", nals.i_type,nals.i_ipoffset,nals.i_spsoffset,nals.i_ppsoffset,nals.i_iplen,nals.i_spslen,nals.i_ppslen);
	//int i;
	//for(i=0;i<8;i++)
	//	printf("%02x ",Buffer[i]);
	//printf("\n");
	if((nals.p_payload[4] & 0x1F) == NAL_SLICE_IDR) 
	{
		// It is I-frame
		nals.i_iplen = num_nals;
		nals.i_type = NAL_SLICE_IDR;
		//m=allocb(nals.i_iplen + 10, 0);		
		//memcpy(m->b_wptr, (Buffer + 4), nals.i_iplen-4);
		//m->b_wptr += nals.i_iplen-4;
		//putq(nalus,m);
		//printf("h264_data_check111111111111\n");
		return 0;

	} 
	else if ((nals.p_payload[4] & 0x1F) == NAL_SLICE) 
	{
		// It is P-frame
		nals.i_iplen = num_nals;
		nals.i_type = NAL_SLICE;
		//m=allocb(nals.i_iplen + 10, 0);		
		//memcpy(m->b_wptr, (Buffer + 4), nals.i_iplen-4);
		//m->b_wptr += nals.i_iplen-4;
		//putq(nalus,m);
		//printf("0000000000000\n");
		//printf("h264_data_check22222222222\n");
		return 0;

	} 
	else 
	{
		uBitLen = (num_nals - 4); //4:sync word len
		while( uBitLen ) 
		{
			if(memcmp(nals.p_payload + uNextSyncWordAdr, &uSyncWord, sizeof(uint32_t)) == 0) 
			{
				u_type = nals.p_payload[uNextSyncWordAdr + 4] & 0x1F;
				
				if (uCurFrame == NAL_SPS) 
				{
					nals.i_spslen = uNextSyncWordAdr - nals.i_spsoffset;
					//printf("uBitLen::nals.i_spslen SPS     = %d.\n", nals.i_spslen);
				} 
				else if (uCurFrame == NAL_PPS) 
				{
					nals.i_ppslen = uNextSyncWordAdr - nals.i_ppsoffset;
					//printf("uBitLen::nals.i_ppslen PPS     = %d.\n", nals.i_ppslen);
				}
				
				if(u_type == NAL_SPS) 
				{
					uCurFrame = NAL_SPS;
					nals.i_spsoffset = uNextSyncWordAdr;
					nals.i_type |= NAL_SPS;
					//printf("uBitLen::nals.i_spsoffset SPS  = %d.\n", nals.i_spsoffset);
					//printf("uBitLen::nals.i_type      SPS  = 0x%x.\n", nals.i_type);
				} 
				else if(u_type == NAL_PPS) 
				{
					uCurFrame =  NAL_PPS;
					nals.i_ppsoffset = uNextSyncWordAdr;
					nals.i_type |= NAL_PPS;
					//printf("uBitLen::nals.i_ppsoffset PPS  = %d.\n", nals.i_ppsoffset);
					//printf("uBitLen::nals.i_type      PPS  = 0x%x.\n", nals.i_type);
				} 
				else if(u_type == NAL_SLICE_IDR) 
				{
					nals.i_ipoffset = uNextSyncWordAdr;
					nals.i_iplen = num_nals - uNextSyncWordAdr;
					nals.i_type |= NAL_SLICE_IDR;
					//printf("uBitLen::nals.i_ipoffset IDR   = %d.\n", nals.i_ipoffset);
					//printf("uBitLen::nals.i_iplen    IDR   = %d.\n", nals.i_iplen);
					//printf("uBitLen::nals.i_type     IDR   = 0x%x.\n", nals.i_type);
					break;
				} 
				else if(u_type == NAL_SLICE) 
				{
					nals.i_ipoffset = uNextSyncWordAdr;
					nals.i_iplen = num_nals - uNextSyncWordAdr;
					nals.i_type |= NAL_SLICE;
					//printf("uBitLen::nals.i_ipoffset P    = %d.\n", nals.i_ipoffset);
					//printf("uBitLen::nals.i_iplen    P    = %d.\n", nals.i_iplen);
					//printf("uBitLen::nals.i_type     P    = 0x%x.\n", nals.i_type);
					break;
				}
			}
			uNextSyncWordAdr ++;
			uBitLen --;
		}

		if(uBitLen == 0) 
		{
			//printf("Unknown H264 frame \n");
			//log_w("h264_data_check:Unknown H264 frame\n");
			uNextSyncWordAdr = 0;
			return -1;
		}
	}
	//log_d("h264_data_check:OK\n");
	return 0;
	#if 0
	if(num_nals > s_u32IFrameBufSize)
	{
		if(s_pu8IFrameBuf)
		{
			free(s_pu8IFrameBuf);
			s_u32IFrameBufSize = 0;
		}
		s_pu8IFrameBuf = malloc(num_nals + 100);
		if(s_pu8IFrameBuf)
		{
			s_u32IFrameBufSize = num_nals;
		}
		else{
			s_u32IFrameBufSize = 0;
		}
	}

	if(s_pu8IFrameBuf == NULL)
	{
		printf("h264_nals_to_msgb: s_pu8IFrameBuf is null\n");
		return;
	}
	
	memcpy(s_pu8IFrameBuf, Buffer, num_nals);
	#endif
	#if 0
	if (nals.i_type & NAL_SLICE) 
	{
		eFrameMask = nals.i_type;
		while(eFrameMask) 
		{
			if(eFrameMask & NAL_SPS)
			{
				uFrameOffset = nals.i_spsoffset + 4;
				uFrameLen = nals.i_spslen - 4 ;
				eFrameMask &= (~NAL_SPS);
			}
			else if (eFrameMask & NAL_PPS)
			{
				uFrameOffset = nals.i_ppsoffset + 4;
				uFrameLen = nals.i_ppslen - 4;
				eFrameMask &= (~NAL_PPS);
			}
			else
			{
				uFrameOffset = nals.i_ipoffset + 4;
				uFrameLen = nals.i_iplen - 4;
				eFrameMask = 0;
			}
			m=allocb(uFrameLen + 10, 0);
			//memcpy(m->b_wptr, (s_pu8IFrameBuf + uFrameOffset), uFrameLen);
			memcpy(m->b_wptr, (Buffer + uFrameOffset), uFrameLen);
			m->b_wptr += uFrameLen;
			putq(nalus,m);
			//printf("11111111111111:%04x\n",eFrameMask);
		}
	} 
	else 
	{
		uFrameOffset = nals.i_ipoffset + 4;
		uFrameLen = nals.i_iplen - 4;
		m=allocb(uFrameLen + 10, 0);		
		//memcpy(m->b_wptr, (s_pu8IFrameBuf + uFrameOffset), uFrameLen);
		memcpy(m->b_wptr, (Buffer + uFrameOffset), uFrameLen);
		m->b_wptr += uFrameLen;
		putq(nalus,m);
		//printf("2222222222222\n");
	}
	#endif
}

int h265_data_check( uint8_t *Buffer, uint32_t num_nals)
{
	uint32_t uNextSyncWordAdr=0, uBitLen;
	uint32_t uSyncWord = 0x01000000;
	uint32_t uCurFrame = NAL_UNKNOWN;
	uint8_t  u_type;
	uint32_t uFrameLen;
	nal_type eFrameMask;
	uint32_t uFrameOffset;
	h265_nal_t  nals;
	//mblk_t *m;

	h265_xnals_init(&nals);

	nals.p_payload  = Buffer;
	//int i;
	//for(i=0;i<8;i++)
	//	printf("%02x ",Buffer[i]);
	//printf("\n");
	if( ((nals.p_payload[4]&0x7E)>>1) == NAL265_SLICE_IDR) 
	{
		// It is I-frame
		nals.i_iplen = num_nals;
		nals.i_type = NAL265_SLICE_IDR;
		//m=allocb(nals.i_iplen + 10, 0);		
		//memcpy(m->b_wptr, (Buffer + 4), nals.i_iplen-4);
		//m->b_wptr += nals.i_iplen-4;
		//putq(nalus,m);

		//printf("h265_nals_to_msgb(nals.i_iplen=%d)1111111111111111\n",nals.i_iplen);
		return 0;

	} 
	else if ( ((nals.p_payload[4]&0x7E)>>1) == NAL_SLICE ) 
	{
		// It is P-frame
		nals.i_iplen = num_nals;
		nals.i_type = NAL_SLICE;
		//m=allocb(nals.i_iplen + 10, 0);		
		//memcpy(m->b_wptr, (Buffer + 4), nals.i_iplen-4);
		//m->b_wptr += nals.i_iplen-4;
		//putq(nalus,m);

		//printf("h265_nals_to_msgb(nals.i_iplen=%d) 2222222222222222222\n",nals.i_iplen);
		return 0;

	} 
	else 
	{
		uBitLen = (num_nals - 4); //4:sync word len
		while( uBitLen ) 
		{
			if(memcmp(nals.p_payload + uNextSyncWordAdr, &uSyncWord, sizeof(uint32_t)) == 0) 
			{
				u_type = ((nals.p_payload[uNextSyncWordAdr+4]&0x7E)>>1);
				//printf("h265_nals_to_msgb 2222222225555555 %d\n",u_type);
				if (uCurFrame == NAL265_VPS) 
				{
					nals.i_vpslen = uNextSyncWordAdr - nals.i_vpsoffset;
				} 				
				else if (uCurFrame == NAL265_SPS) 
				{
					nals.i_spslen = uNextSyncWordAdr - nals.i_spsoffset;
				} 
				else if (uCurFrame == NAL265_PPS) 
				{
					nals.i_ppslen = uNextSyncWordAdr - nals.i_ppsoffset;
				}
				
				if(u_type == NAL265_VPS) 
				{
					uCurFrame = NAL265_VPS;
					nals.i_vpsoffset = uNextSyncWordAdr;
					nals.i_type |= NAL265_VPS;
				} 
				else if(u_type == NAL265_SPS) 
				{
					uCurFrame = NAL265_SPS;
					nals.i_spsoffset = uNextSyncWordAdr;
					nals.i_type |= NAL265_SPS;
				} 
				else if(u_type == NAL265_PPS) 
				{
					uCurFrame =  NAL265_PPS;
					nals.i_ppsoffset = uNextSyncWordAdr;
					nals.i_type |= NAL265_PPS;
				} 
				else if(u_type == NAL265_SLICE_IDR) 
				{
					nals.i_ipoffset = uNextSyncWordAdr;
					nals.i_iplen = num_nals - uNextSyncWordAdr;
					nals.i_type |= NAL265_SLICE_IDR;
					break;
				} 
				else if(u_type == NAL_SLICE) 
				{
					nals.i_ipoffset = uNextSyncWordAdr;
					nals.i_iplen = num_nals - uNextSyncWordAdr;
					nals.i_type |= NAL_SLICE;
					break;
				}
			}
			uNextSyncWordAdr ++;
			uBitLen --;
		}

		if(uBitLen == 0) 
		{
			//log_w("h265_data_check:Unknown H265 frame");
			uNextSyncWordAdr = 0;
			return -1;
		}
	}
	//log_d("h265_data_check:ok");
	return 0;
	#if 0
	if(num_nals > s_u32IFrameBufSize)
	{
		if(s_pu8IFrameBuf)
		{
			free(s_pu8IFrameBuf);
			s_u32IFrameBufSize = 0;
		}
		s_pu8IFrameBuf = malloc(num_nals + 100);
		if(s_pu8IFrameBuf)
		{
			s_u32IFrameBufSize = num_nals;
		}
		else{
			s_u32IFrameBufSize = 0;
		}
	}

	if(s_pu8IFrameBuf == NULL)
	{
		printf("h265_nals_to_msgb: s_pu8IFrameBuf is null\n");
		return;
	}

	memcpy(s_pu8IFrameBuf, Buffer, num_nals);
	#endif
	#if 0
	if (nals.i_type & NAL_SLICE) 
	{	
		eFrameMask = nals.i_type;
		while(eFrameMask) 
		{
			//dprintf("h265_nals_to_msgb 444444444444444\n");
			if(eFrameMask & NAL265_VPS)
			{
				uFrameOffset = nals.i_vpsoffset + 4;
				uFrameLen = nals.i_vpslen - 4 ;
				eFrameMask &= (~NAL265_VPS);
			}
			else if(eFrameMask & NAL265_SPS)
			{
				uFrameOffset = nals.i_spsoffset + 4;
				uFrameLen = nals.i_spslen - 4 ;
				eFrameMask &= (~NAL265_SPS);
			}
			else if (eFrameMask & NAL265_PPS)
			{
				uFrameOffset = nals.i_ppsoffset + 4;
				uFrameLen = nals.i_ppslen - 4;
				eFrameMask &= (~NAL265_PPS);
			}
			else
			{
				uFrameOffset = nals.i_ipoffset + 4;
				uFrameLen = nals.i_iplen - 4;
				eFrameMask = 0;
			}
			m=allocb(uFrameLen + 10, 0);
			//memcpy(m->b_wptr, (s_pu8IFrameBuf + uFrameOffset), uFrameLen);
			memcpy(m->b_wptr, (Buffer + uFrameOffset), uFrameLen);
			m->b_wptr += uFrameLen;
			putq(nalus,m);
			//printf("h265_nals_to_msgb 3333333333333333uFrameLen=%d,uFrameOffset=%d\n", uFrameLen, uFrameOffset);
		}
	} 
	else 
	{
		//printf("h265_nals_to_msgb 555555555555555\n");
		uFrameOffset = nals.i_ipoffset + 4;
		uFrameLen = nals.i_iplen - 4;
		m=allocb(uFrameLen + 10, 0);		
		//memcpy(m->b_wptr, (s_pu8IFrameBuf + uFrameOffset), uFrameLen);
		memcpy(m->b_wptr, (Buffer + uFrameOffset), uFrameLen);
		m->b_wptr += uFrameLen;
		putq(nalus,m);
		//printf("h265_nals_to_msgb 66666666666666666\n");
	}	
	#endif
	//dprintf("h265_nals_to_msgb 777777777777777777\n");
}