
#include "rtp_utility.h"
#include "elog_forcall.h"
#if 0
int rtp_process_set_high_prio(void)
{
	struct sched_param param;
	int policy=SCHED_RR;
	memset(&param,0,sizeof(param));
	int result=0;
	int min_prio, max_prio;

	min_prio = sched_get_priority_min(policy);
	max_prio = sched_get_priority_max(policy);
	param.sched_priority=max_prio;
	if((result=pthread_setschedparam(pthread_self(),policy, &param))) 
	{
		if (result==EPERM)
		{
			/*
				The linux kernel has 
				sched_get_priority_max(SCHED_OTHER)=sched_get_priority_max(SCHED_OTHER)=0.
				As long as we can't use SCHED_RR or SCHED_FIFO, the only way to increase priority of a calling thread
				is to use setpriority().
			*/
			if (setpriority(PRIO_PROCESS,0,-20)==-1)
			{
				printf("setpriority() failed: %s, nevermind.",strerror(errno));
				result = -1;
			}
			else
			{
				printf("priority increased to maximum.");
			}
		}
		else
		{
			printf("Set pthread_setschedparam failed: %s",strerror(result));
			result = -1;
		}
	} 
	else 
	{
		printf("priority set to %s and value (%i)",policy==SCHED_FIFO ? "SCHED_FIFO" : "SCHED_RR", param.sched_priority);
	}
	return result;
}
#endif
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
RtpSession* rtp_session_new_one(int rcv_type, char* addr, short port, PayloadType payload)
{
	int payload_int = 0;

	RtpSession *session;
	ortp_init();
	ortp_scheduler_init();
	ortp_set_log_level_mask(0);//(ORTP_DEBUG|ORTP_MESSAGE|ORTP_WARNING|ORTP_ERROR|ORTP_FATAL);
	if( rcv_type == 1 )
	{
		session=rtp_session_new(RTP_SESSION_RECVONLY);		
		
		rtp_session_set_scheduling_mode(session,0);
		rtp_session_set_blocking_mode(session,0);
		rtp_session_set_rtp_socket_recv_buffer_size(session,200000);
		rtp_session_set_remote_addr(session,addr,port);
		rtp_session_set_payload_type(session,payload_int);
		
		//rtp_session_enable_adaptive_jitter_compensation(session, TRUE);
		//rtp_session_set_symmetric_rtp(session, TRUE);
		rtp_session_enable_adaptive_jitter_compensation(session, FALSE);
		rtp_session_set_symmetric_rtp(session, FALSE);
		
		JBParameters jbp;
		rtp_session_get_jitter_buffer_params(session,&jbp);
		jbp.max_packets=100;
		rtp_session_set_jitter_buffer_params(session,&jbp);
		//rtp_session_set_rtp_socket_recv_buffer_size(session,20000);
		rtp_session_set_jitter_compensation(session,10);	// x ms 
		
		printf("rtpsession_new_r[%s:%d-%d] OK!!!\n",addr,port,payload_int);			
	}
	else if( rcv_type == 2 )
	{
		session=rtp_session_new(RTP_SESSION_SENDRECV);		
		
		rtp_session_set_scheduling_mode(session,0);
		rtp_session_set_blocking_mode(session,0);
		
		rtp_session_set_remote_addr(session,addr,port);
		rtp_session_set_payload_type(session,payload_int);
		
		rtp_session_enable_adaptive_jitter_compensation(session, TRUE);
		rtp_session_set_symmetric_rtp(session, TRUE);
		
		JBParameters jbp;
		rtp_session_get_jitter_buffer_params(session,&jbp);
		jbp.max_packets=20;
		rtp_session_set_jitter_buffer_params(session,&jbp);
		rtp_session_set_rtp_socket_recv_buffer_size(session,200000);
		rtp_session_set_jitter_compensation(session,100);	// x ms 
		
		printf("rtpsession_new_rw[%s:%d-%d] OK!!!\n",addr,port,payload_int);
	}
	else
	{		
		session=rtp_session_new(RTP_SESSION_SENDONLY);	
		rtp_session_set_scheduling_mode(session,0);
		rtp_session_set_blocking_mode(session,0);
		rtp_session_set_remote_addr(session,addr,port);
		rtp_session_set_payload_type(session,payload_int);
		
		printf("rtpsession_new_s[%s:%d-%d] OK!!!\n",addr,port,payload_int);
	}
	return session;
}

int rtp_session_del_one(RtpSession* psession)
{
	rtp_session_destroy(psession);
	ortp_exit();
	return 0;
}
#if 1
#define IP_UDP_OVERHEAD (20+8)
typedef enum {
	RTP_SESSION_RECV_SYNC=1,	/* the rtp session is synchronising in the incoming stream */
	RTP_SESSION_FIRST_PACKET_DELIVERED=1<<1,
	RTP_SESSION_SCHEDULED=1<<2,/* the scheduler controls this session*/
	RTP_SESSION_BLOCKING_MODE=1<<3, /* in blocking mode */
	RTP_SESSION_RECV_NOT_STARTED=1<<4,	/* the application has not started to try to recv */
	RTP_SESSION_SEND_NOT_STARTED=1<<5,  /* the application has not started to send something */
	RTP_SESSION_IN_SCHEDULER=1<<6,	/* the rtp session is in the scheduler list */
	RTP_SESSION_USING_EXT_SOCKETS=1<<7, /* the session is using externaly supplied sockets */
	RTP_SOCKET_CONNECTED=1<<8,
	RTCP_SOCKET_CONNECTED=1<<9,
	RTP_SESSION_USING_TRANSPORT=1<<10,
	RTCP_OVERRIDE_LOST_PACKETS=1<11,
	RTCP_OVERRIDE_JITTER=1<<12,
	RTCP_OVERRIDE_DELAY=1<<13
}RtpSessionFlags;
static void update_recv_bytes(RtpSession*s, int nbytes){
#ifdef ORTP_INET6
	int overhead=(s->rtp.sockfamily==AF_INET6) ? IP6_UDP_OVERHEAD : IP_UDP_OVERHEAD;
#else
	int overhead=IP_UDP_OVERHEAD;
#endif
	if (s->rtp.recv_bytes==0){
		gettimeofday(&s->rtp.recv_bw_start,NULL);
	}
	s->rtp.recv_bytes+=nbytes+overhead;
}
//#define is_would_block_error(errnum)	(errnum==EWOULDBLOCK || errnum==EAGAIN)

#ifndef UTILS_H
#define UTILS_H

#include "ortp/event.h"

struct _OList {
	struct _OList *next;
	struct _OList *prev;
	void *data;
};

typedef struct _OList OList;


#define o_list_next(elem) ((elem)->next)

OList * o_list_append(OList *elem, void * data);
OList * o_list_remove(OList *list, void *data);
OList * o_list_free(OList *elem);
OList *o_list_remove_link(OList *list, OList *elem);


#define INT_TO_POINTER(truc)	((long)(long)(truc))
#define POINTER_TO_INT(truc)	((int)(long)(truc))

typedef struct _dwsplit_t{
#ifdef ORTP_BIGENDIAN
	uint16_t hi;
	uint16_t lo;
#else
	uint16_t lo;
	uint16_t hi;
#endif
} dwsplit_t;

typedef union{
	dwsplit_t split;
	uint32_t one;
} poly32_t;

#ifdef ORTP_BIGENDIAN
#define hton24(x) (x)
#else
#define hton24(x) ((( (x) & 0x00ff0000) >>16) | (( (x) & 0x000000ff) <<16) | ( (x) & 0x0000ff00) )
#endif
#define ntoh24(x) hton24(x)

#if defined(WIN32) || defined(_WIN32_WCE)
#define is_would_block_error(errnum)	(errnum==WSAEWOULDBLOCK)
#else
#define is_would_block_error(errnum)	(errnum==EWOULDBLOCK || errnum==EAGAIN)
#endif

void ortp_ev_queue_put(OrtpEvQueue *q, OrtpEvent *ev);

uint64_t ortp_timeval_to_ntp(const struct timeval *tv);

#endif
#define RTP_SEQ_IS_GREATER(seq1,seq2)\
	((uint16_t)((uint16_t)(seq1) - (uint16_t)(seq2))< (uint16_t)(1<<15))
void rtp_putq1(queue_t *q, mblk_t *mp)
{
	mblk_t *tmp;
	rtp_header_t *rtp=(rtp_header_t*)mp->b_rptr,*tmprtp;
	/* insert message block by increasing time stamp order : the last (at the bottom)
		message of the queue is the newest*/
	ortp_debug("rtp_putq(): Enqueuing packet with ts=%i and seq=%i",rtp->timestamp,rtp->seq_number);
	
	if (qempty(q)) {
		putq(q,mp);
		return;
	}
	if(rtp->seq_number==0)
	{
		putq(q,mp);
		return;
	}
	putq(q,mp);
	//return;
	#if 0
	tmp=qlast(q);
	/* we look at the queue from bottom to top, because enqueued packets have a better chance
	to be enqueued at the bottom, since there are surely newer */
	while (!qend(q,tmp))
	{
		tmprtp=(rtp_header_t*)tmp->b_rptr;
		ortp_debug("rtp_putq(): Seeing packet with seq=%i",tmprtp->seq_number);
		
 		if (rtp->seq_number == tmprtp->seq_number)
 		{
 			/* this is a duplicated packet. Don't queue it */
 			//ortp_debug("rtp_putq: duplicated message.");
 			#if 0
 			printf("rtp_putq: duplicated message.seq=%i\n",tmprtp->seq_number);
 			freemsg(mp);
 			return;
			#else
			break;
			#endif
		}else if (RTP_SEQ_IS_GREATER(rtp->seq_number,tmprtp->seq_number)){

			printf("rtp_putq: insq=%i:%i\n",tmprtp->seq_number,rtp->seq_number);
			insq(q,tmp->b_next,mp);
			return;
 		}
		tmp=tmp->b_prev;
	}
	/* this packet is the oldest, it has to be 
	placed on top of the queue */
	insq(q,qfirst(q),mp);
	#endif
	
}
static bool_t queue_packet1(queue_t *q, int maxrqsz, mblk_t *mp, rtp_header_t *rtp, int *discarded)
{
	mblk_t *tmp;
	int header_size;
	*discarded=0;
	header_size=RTP_FIXED_HEADER_SIZE+ (4*rtp->cc);
	if ((mp->b_wptr - mp->b_rptr)==header_size){
		//ortp_debug("Rtp packet contains no data.");
		printf("Rtp packet contains no data.\n");
		(*discarded)++;
		freemsg(mp);
		return FALSE;
	}
	/* and then add the packet to the queue */
	
	rtp_putq1(q,mp);
	/* make some checks: q size must not exceed RtpStream::max_rq_size */
	while (q->q_mcount > maxrqsz)
	{
		/* remove the oldest mblk_t */
		tmp=getq(q);
		if (mp!=NULL)
		{
			printf("rtp_putq: Queue is full. Discarding with maxrqsz=%i,seq=%i\n",maxrqsz,((rtp_header_t*)mp->b_rptr)->seq_number);
			freemsg(tmp);
			(*discarded)++;
		}
	}
	return TRUE;
}

void rtp_session_rtp_parse1(RtpSession *session, mblk_t *mp, uint32_t local_str_ts, struct sockaddr *addr, socklen_t addrlen)
{
	int i;
	rtp_header_t *rtp;
	int msgsize;
	RtpStream *rtpstream=&session->rtp;
	rtp_stats_t *stats=&rtpstream->stats;
	
	msgsize=mp->b_wptr-mp->b_rptr;

	if (msgsize<RTP_FIXED_HEADER_SIZE){
		//ortp_warning("Packet too small to be a rtp packet (%i)!",msgsize);
		printf("Packet too small to be a rtp packet (%i)!\n",msgsize);
		rtpstream->stats.bad++;
		ortp_global_stats.bad++;
		freemsg(mp);
		return;
	}
	rtp=(rtp_header_t*)mp->b_rptr;
	if (rtp->version!=2)
	{
		/* try to see if it is a STUN packet */
		uint16_t stunlen=*((uint16_t*)(mp->b_rptr + sizeof(uint16_t)));
		stunlen = ntohs(stunlen);
		if (stunlen+20==mp->b_wptr-mp->b_rptr){
			/* this looks like a stun packet */
			if (session->eventqs!=NULL){
				OrtpEvent *ev=ortp_event_new(ORTP_EVENT_STUN_PACKET_RECEIVED);
				OrtpEventData *ed=ortp_event_get_data(ev);
				ed->packet=mp;
				ed->ep=rtp_endpoint_new(addr,addrlen);
				ed->info.socket_type = OrtpRTPSocket;
				rtp_session_dispatch_event(session,ev);
				return;
			}
		}
		/* discard in two case: the packet is not stun OR nobody is interested by STUN (no eventqs) */
		//ortp_debug("Receiving rtp packet with version number !=2...discarded");
		printf("Receiving rtp packet with version number !=2...discarded\n");
		stats->bad++;
		ortp_global_stats.bad++;
		freemsg(mp);
		return;
	}

	/* only count non-stun packets. */
	ortp_global_stats.packet_recv++;
	stats->packet_recv++;
	ortp_global_stats.hw_recv+=msgsize;
	stats->hw_recv+=msgsize;
	session->rtp.hwrcv_since_last_SR++;

	
	/* convert all header data from network order to host order */
	rtp->seq_number=ntohs(rtp->seq_number);
	rtp->timestamp=ntohl(rtp->timestamp);
	rtp->ssrc=ntohl(rtp->ssrc);
	/* convert csrc if necessary */
	if (rtp->cc*sizeof(uint32_t) > (uint32_t) (msgsize-RTP_FIXED_HEADER_SIZE)){
		//ortp_debug("Receiving too short rtp packet.");
		printf("Receiving too short rtp packet.\n");
		stats->bad++;
		ortp_global_stats.bad++;
		freemsg(mp);
		return;
	}

#ifndef PERF
	/* Write down the last RTP/RTCP packet reception time. */
	gettimeofday(&session->last_recv_time, NULL);
#endif

	for (i=0;i<rtp->cc;i++)
		rtp->csrc[i]=ntohl(rtp->csrc[i]);
	/*the goal of the following code is to lock on an incoming SSRC to avoid
	receiving "mixed streams"*/
	if (session->ssrc_set){
		/*the ssrc is set, so we must check it */
		if (session->rcv.ssrc!=rtp->ssrc){
			if (session->inc_ssrc_candidate==rtp->ssrc){
				session->inc_same_ssrc_count++;
			}else{
				session->inc_same_ssrc_count=0;
				session->inc_ssrc_candidate=rtp->ssrc;
			}
			if (session->inc_same_ssrc_count>=session->rtp.ssrc_changed_thres){

				/* store the sender rtp address to do symmetric RTP */
				if (!session->use_connect){
					if (session->rtp.socket>0 && session->symmetric_rtp){
						/* store the sender rtp address to do symmetric RTP */
						memcpy(&session->rtp.rem_addr,addr,addrlen);
						session->rtp.rem_addrlen=addrlen;
					}
				}
				session->rtp.rcv_last_ts = rtp->timestamp;
				session->rcv.ssrc=rtp->ssrc;
				rtp_signal_table_emit(&session->on_ssrc_changed);
			}else{
				/*discard the packet*/
				//ortp_debug("Receiving packet with unknown ssrc.");
				printf("Receiving packet with unknown ssrc.\n");
				stats->bad++;
				ortp_global_stats.bad++;
				freemsg(mp);
				return;
			}
		}
    else{
			/* The SSRC change must not happen if we still receive
			ssrc from the initial source. */
			session->inc_same_ssrc_count=0;
		}

	}else{
		session->ssrc_set=TRUE;
		session->rcv.ssrc=rtp->ssrc;

		if (!session->use_connect){
			if (session->rtp.socket>0 && session->symmetric_rtp){
				/* store the sender rtp address to do symmetric RTP */
				memcpy(&session->rtp.rem_addr,addr,addrlen);
				session->rtp.rem_addrlen=addrlen;
			}
		}
	}
	
	/* update some statistics */
	{
		poly32_t *extseq=(poly32_t*)&rtpstream->hwrcv_extseq;
		if (rtp->seq_number>extseq->split.lo){
			extseq->split.lo=rtp->seq_number;
		}else if (rtp->seq_number<200 && extseq->split.lo>((1<<16) - 200)){
			/* this is a check for sequence number looping */
			extseq->split.lo=rtp->seq_number;
			extseq->split.hi++;
		}
		/* the first sequence number received should be initialized at the beginning, so that the first receiver reports contains valid loss rate*/
		if (stats->packet_recv==1){
			rtpstream->hwrcv_seq_at_last_SR=rtp->seq_number;
		}
	}
	
	/* check for possible telephone events */
	if (rtp->paytype==session->rcv.telephone_events_pt){
		queue_packet1(&session->rtp.tev_rq,session->rtp.max_rq_size,mp,rtp,&i);
		stats->discarded+=i;
		ortp_global_stats.discarded+=i;
		return;
	}
	
	/* check for possible payload type change, in order to update accordingly our clock-rate dependant
	parameters */
	if (session->hw_recv_pt!=rtp->paytype){
		rtp_session_update_payload_type(session,rtp->paytype);
	}
	
	jitter_control_new_packet(&session->rtp.jittctl,rtp->timestamp,local_str_ts);

	if (session->flags & RTP_SESSION_FIRST_PACKET_DELIVERED) {
		/* detect timestamp important jumps in the future, to workaround stupid rtp senders */
		if (RTP_TIMESTAMP_IS_NEWER_THAN(rtp->timestamp,session->rtp.rcv_last_ts+session->rtp.ts_jump)){
			//ortp_debug("rtp_parse: timestamp jump ?");
			printf("rtp_parse: timestamp jump ?\n");
			rtp_signal_table_emit2(&session->on_timestamp_jump,(long)&rtp->timestamp);
		}
		else if (RTP_TIMESTAMP_IS_STRICTLY_NEWER_THAN(session->rtp.rcv_last_ts,rtp->timestamp)){
			/* don't queue packets older than the last returned packet to the application*/
			/* Call timstamp jumb in case of
			 * large negative Ts jump or if ts is set to 0
			*/
			
			if ( RTP_TIMESTAMP_IS_STRICTLY_NEWER_THAN(session->rtp.rcv_last_ts, rtp->timestamp + session->rtp.ts_jump) ){
				//ortp_warning("rtp_parse: negative timestamp jump");
				printf("rtp_parse: negative timestamp jump\n");
				rtp_signal_table_emit2(&session->on_timestamp_jump,
							(long)&rtp->timestamp);
			}
			//ortp_debug("rtp_parse: discarding too old packet (ts=%i)",rtp->timestamp);
			printf("rtp_parse: discarding too old packet (ts=%i)\n",rtp->timestamp);
			freemsg(mp);
			stats->outoftime++;
			ortp_global_stats.outoftime++;
			return;
		}
	}
	
	if (queue_packet1(&session->rtp.rq,session->rtp.max_rq_size,mp,rtp,&i))
		jitter_control_update_size(&session->rtp.jittctl,&session->rtp.rq);
	stats->discarded+=i;
	ortp_global_stats.discarded+=i;
}
int rtp_session_rtp_recv1 (RtpSession * session, uint32_t user_ts)
{
	int error;
	ortp_socket_t sockfd=session->rtp.socket;
#ifdef ORTP_INET6
	struct sockaddr_storage remaddr;
#else
	struct sockaddr remaddr;
#endif
	socklen_t addrlen = sizeof (remaddr);
	mblk_t *mp;
	
	//if ((sockfd==(ortp_socket_t)-1) && !rtp_session_using_transport(session, rtp)) return -1;  /*session has no sockets for the moment*/

	while (1)
	{
		//printf("%s:%d\n",__func__,__LINE__);
		bool_t sock_connected=!!(session->flags & RTP_SOCKET_CONNECTED);

		if (session->rtp.cached_mp==NULL)
			 session->rtp.cached_mp = msgb_allocator_alloc(&session->allocator,session->recv_buf_size);
		mp=session->rtp.cached_mp;
		if (sock_connected){
			error=rtp_session_rtp_recv_abstract(sockfd, mp, 0, NULL, NULL);
		}
		#if 0
		else if (rtp_session_using_transport(session, rtp)) {
			error = (session->rtp.tr->t_recvfrom)(session->rtp.tr, mp, 0,
				  (struct sockaddr *) &remaddr,
				  &addrlen);
		ize = recvfrom( pusr_dat->Receive.Sd, (unsigned char*)&recv_fragment, sizeof(udp_fragment_t), 0, (struct sockaddr*)&pusr_dat->Receive.addr, &addr_len );
		}
		#endif
		else { 
			//printf("%s:%d\n",__func__,__LINE__);
			error = rtp_session_rtp_recv_abstract(sockfd, mp, 0,
				  (struct sockaddr *) &remaddr,
				  &addrlen);
			//printf("%s:%d\n",__func__,__LINE__);
		}
		if (error > 0){
			//printf("%s:%d\n",__func__,__LINE__);
			if (session->use_connect){
				/* In the case where use_connect is false, symmetric RTP is handled in rtp_session_rtp_parse() */
				if (session->symmetric_rtp && !sock_connected){
					/* store the sender rtp address to do symmetric RTP */
					memcpy(&session->rtp.rem_addr,&remaddr,addrlen);
					session->rtp.rem_addrlen=addrlen;
					//printf("%s:%d\n",__func__,__LINE__);
					#if 0
					if (try_connect(sockfd,(struct sockaddr*)&remaddr,addrlen))
						session->flags|=RTP_SOCKET_CONNECTED;
					#endif
				}
			}
			//printf("%s:%d\n",__func__,__LINE__);
			mp->b_wptr+=error;
			if (session->net_sim_ctx)
				{
				printf("%s:%d\n",__func__,__LINE__);
				mp=rtp_session_network_simulate(session,mp);
				}
			/* then parse the message and put on jitter buffer queue */
			if (mp){
				
				update_recv_bytes(session,mp->b_wptr-mp->b_rptr);
				//printf("%s:%d:%d:%d\n",__func__,__LINE__,session->rtp.recv_bytes,rtp_get_timestamp(mp));
				//printf("%s:%d\n",__func__,__LINE__);
				rtp_session_rtp_parse1(session, mp, user_ts, (struct sockaddr*)&remaddr,addrlen);	
				//printf("%s:%d\n",__func__,__LINE__);
			}
			session->rtp.cached_mp=NULL;
			//printf("%s:%d\n",__func__,__LINE__);
			/*for bandwidth measurements:*/
		}
		else
		{
			//printf("%s:%d\n",__func__,__LINE__);
			int errnum;
			if (error==-1 && !is_would_block_error((errnum=getSocketErrorCode())) )
			{
				//printf("%s:%d\n",__func__,__LINE__);
				#if 0
				if (session->on_network_error.count>0){
					rtp_signal_table_emit3(&session->on_network_error,(long)"Error receiving RTP packet",INT_TO_POINTER(getSocketErrorCode()));
				}else ortp_warning("Error receiving RTP packet: %s, err num  [%i],error [%i]",getSocketError(),errnum,error);
#ifdef __ios
				/*hack for iOS and non-working socket because of background mode*/
				if (errnum==ENOTCONN){
					/*re-create new sockets */
					rtp_session_set_local_addr(session,session->rtp.sockfamily==AF_INET ? "0.0.0.0" : "::0",session->rtp.loc_port,session->rtcp.loc_port);
				}
#endif
			#endif
			}else{
			//printf("%s:%d\n",__func__,__LINE__);
				/*EWOULDBLOCK errors or transports returning 0 are ignored.*/
				if (session->net_sim_ctx){
					//printf("%s:%d\n",__func__,__LINE__);
					/*drain possible packets queued in the network simulator*/
					mp=rtp_session_network_simulate(session,NULL);
					if (mp){
						/* then parse the message and put on jitter buffer queue */
						update_recv_bytes(session,msgdsize(mp));
						rtp_session_rtp_parse1(session, mp, user_ts, (struct sockaddr*)&session->rtp.rem_addr,session->rtp.rem_addrlen);						
					}
					//printf("%s:%d\n",__func__,__LINE__);
				}
			}
			/* don't free the cached_mp, it will be reused next time */
			return error;
		}
	}
	return error;
}
#endif

static inline uint32_t jitter_control_get_compensated_timestamp(JitterControl *obj , uint32_t user_ts){
	return (uint32_t)( (int64_t)user_ts+obj->slide-(int64_t)obj->adapt_jitt_comp_ts);
}

mblk_t * rtp_session_recvm_with_ts1(RtpSession * session, uint32_t user_ts,int *flag)
{
	mblk_t *mp = NULL;
	rtp_header_t *rtp;
	uint32_t ts;
	uint32_t packet_time;
	//RtpScheduler *sched=session->sched;
	RtpStream *stream=&session->rtp;
	int rejected=0;
	bool_t read_socket=TRUE;

	/* if we are scheduled, remember the scheduler time at which the application has
	 * asked for its first timestamp */

	if (session->flags & RTP_SESSION_RECV_NOT_STARTED)
	{
		*flag|=0x10;
		session->rtp.rcv_query_ts_offset = user_ts;
		/* Set initial last_rcv_time to first recv time. */
		if ((session->flags & RTP_SESSION_SEND_NOT_STARTED)
		|| session->mode == RTP_SESSION_RECVONLY){
		
			gettimeofday(&session->last_recv_time, NULL);
		}
		if (session->flags & RTP_SESSION_SCHEDULED)
		{
			printf("%s:%d\n",__func__,__LINE__);
			//session->rtp.rcv_time_offset = sched->time_;
			//ortp_message("setting snd_time_offset=%i",session->rtp.snd_time_offset);
		}
		rtp_session_unset_flag (session,RTP_SESSION_RECV_NOT_STARTED);
	}else{
		/*prevent reading from the sockets when two 
		consecutives calls for a same timestamp*/
		if (user_ts==session->rtp.rcv_last_app_ts)
		{
			read_socket=FALSE;
			*flag|=0x20;
		}
	}
	session->rtp.rcv_last_app_ts = user_ts;
	if (read_socket){
		
		if(rtp_session_rtp_recv1 (session, user_ts)>=0)
			*flag|=0x1;
		rtp_session_rtcp_recv(session);
	}
	/* check for telephone event first */
	mp=getq(&session->rtp.tev_rq);
	if (mp!=NULL){
		int msgsize=msgdsize(mp);
		ortp_global_stats.recv += msgsize;
		stream->stats.recv += msgsize;
		rtp_signal_table_emit2(&session->on_telephone_event_packet,(long)mp);
		rtp_session_check_telephone_events(session,mp);
		freemsg(mp);
		mp=NULL;
	}
	
	/* then now try to return a media packet, if possible */
	/* first condition: if the session is starting, don't return anything
	 * until the queue size reaches jitt_comp */
	
	if (session->flags & RTP_SESSION_RECV_SYNC)
	{
		queue_t *q = &session->rtp.rq;
		if (qempty(q))
		{
			ortp_debug ("Queue is empty.");
			goto end;
		}
		*flag|=0x2;
		rtp = (rtp_header_t *) qfirst(q)->b_rptr;
		session->rtp.rcv_ts_offset = rtp->timestamp;
		session->rtp.rcv_last_ret_ts = user_ts;	/* just to have an init value */
		session->rcv.ssrc = rtp->ssrc;
		/* delete the recv synchronisation flag */
		rtp_session_unset_flag (session, RTP_SESSION_RECV_SYNC);
	}

	/*calculate the stream timestamp from the user timestamp */
	ts = jitter_control_get_compensated_timestamp(&session->rtp.jittctl,user_ts);
	//if(session->rtp.rq.q_mcount>10)
	{
	if (session->rtp.jittctl.enabled==TRUE){
		if (session->permissive)
			mp = rtp_getq_permissive(&session->rtp.rq, ts,&rejected);
		else{
			mp = rtp_getq(&session->rtp.rq, ts,&rejected);
		}
	}else 
		{
		*flag|=0x4;
	mp=getq(&session->rtp.rq);/*no jitter buffer at all*/
		}

	}
	//else
	//	mp=NULL;
	
	stream->stats.outoftime+=rejected;
	ortp_global_stats.outoftime+=rejected;

	goto end;

      end:
	if (mp != NULL)
	{
		int msgsize = msgdsize (mp);	/* evaluate how much bytes (including header) is received by app */
		uint32_t packet_ts;
		ortp_global_stats.recv += msgsize;
		stream->stats.recv += msgsize;
		rtp = (rtp_header_t *) mp->b_rptr;
		packet_ts=rtp->timestamp;
		ortp_debug("Returning mp with ts=%i", packet_ts);
		/* check for payload type changes */
		if (session->rcv.pt != rtp->paytype)
		{
			printf("%s:%d\n",__func__,__LINE__);
			//payload_type_changed_notify(session, rtp->paytype);
		}
		/* update the packet's timestamp so that it corrected by the 
		adaptive jitter buffer mechanism */
		if (session->rtp.jittctl.adaptive){
			*flag|=0x8;
			uint32_t changed_ts;
			/* only update correction offset between packets of different
			timestamps*/
			if (packet_ts!=session->rtp.rcv_last_ts)
				jitter_control_update_corrective_slide(&session->rtp.jittctl);
			changed_ts=packet_ts+session->rtp.jittctl.corrective_slide;
			rtp->timestamp=changed_ts;
			/*ortp_debug("Returned packet has timestamp %u, with clock slide compensated it is %u",packet_ts,rtp->timestamp);*/
		}
		session->rtp.rcv_last_ts = packet_ts;
		if (!(session->flags & RTP_SESSION_FIRST_PACKET_DELIVERED)){
			rtp_session_set_flag(session,RTP_SESSION_FIRST_PACKET_DELIVERED);
		}
	}
	else
	{
		ortp_debug ("No mp for timestamp queried");
	}
	rtp_session_rtcp_process_recv(session);
	
	if (session->flags & RTP_SESSION_SCHEDULED)
	{
		printf("%s:%d\n",__func__,__LINE__);
		#if 0
		/* if we are in blocking mode, then suspend the calling process until timestamp
		 * wanted expires */
		/* but we must not block the process if the timestamp wanted by the application is older
		 * than current time */
		wait_point_lock(&session->rcv.wp);
		packet_time =
			rtp_session_ts_to_time (session,
				     user_ts -
				     session->rtp.rcv_query_ts_offset) +
			session->rtp.rcv_time_offset;
		ortp_debug ("rtp_session_recvm_with_ts: packet_time=%i, time=%i",packet_time, sched->time_);
		
		if (TIME_IS_STRICTLY_NEWER_THAN (packet_time, sched->time_))
		{
			wait_point_wakeup_at(&session->rcv.wp,packet_time, (session->flags & RTP_SESSION_BLOCKING_MODE)!=0);
			session_set_clr(&sched->r_sessions,session);
		}
		else session_set_set(&sched->r_sessions,session);	/*to unblock _select() immediately */
		wait_point_unlock(&session->rcv.wp);
		#endif
	}
	return mp;
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void* rtp_session_dat_process(void *arg)
{
	ONE_RTP_SESSION* rtp_session = (ONE_RTP_SESSION*)arg;
	mblk_t *im;	

	int user_ts;
	int user_ts_save=0;
	struct timespec ts;	
	uint64_t orig;
	uint64_t tick_time;
	uint64_t curtime;
	uint64_t realtime;
	int64_t  diff;	
	int flag=0;
	int r_cnt=0;
	int s_cnt=0;

	rtp_process_set_high_prio();
	
	clock_gettime(CLOCK_MONOTONIC,&ts);
	orig = (ts.tv_sec*1000LL) + (ts.tv_nsec+500000LL)/1000000LL;
	tick_time = 0;
	rtp_session->state=RTP_RUN;
	//log_i("au rtp thread start");
	printf("au rtp thread start====session->recv_buf_size=%d\n",rtp_session->psession->recv_buf_size);
	//rtp_session->psession->rtp.jittctl.enabled=0;
	while(rtp_session->state==RTP_RUN)
	//while(1)
	{
		user_ts = tick_time*rtp_session->ts;
		// sendonly
		//if(tick_time%20==0)
			{
		pthread_mutex_lock( &rtp_session->lck );
		if( (im = getq(&rtp_session->nalus)) != NULL ) 
		{
			pthread_mutex_unlock( &rtp_session->lck );
			//printf("rtp_session_sendm_with_ts,ts=%d\n", user_ts );
			s_cnt++;
			rtp_session_sendm_with_ts(rtp_session->psession,im,user_ts);
		}
		else
			pthread_mutex_unlock( &rtp_session->lck );
			}
		// recvonly
		if( rtp_session->type != 0 )
		{
		#if 0
		user_ts_save=rtp_session_rtp_recv1(rtp_session->psession, user_ts);
		printf("rtp_session_recvm_with_ts,ts=%d:%d:%d session->rtp.rq=%d\n", 160, user_ts,user_ts_save,rtp_session->psession->rtp.rq.q_mcount );
		#endif
		#if 1
			flag=0;
			if(tick_time%20==0)
			{
				while( (im = rtp_session_recvm_with_ts1(rtp_session->psession, user_ts,&flag) ) != NULL ) 
				{
					// exclude rtp head
					mblk_set_timestamp_info(im, rtp_get_timestamp(im));
					mblk_set_marker_info(im, rtp_get_markbit(im));
					mblk_set_payload_type(im, rtp_get_payload_type(im));
					//if(r_cnt%50==0)
					//	printf("ts=%d:%d:%d cnt=%d:%d\n", rtp_get_timestamp(im), rtp_get_seqnumber(im),user_ts-user_ts_save,s_cnt,r_cnt);
					rtp_get_payload(im, &im->b_rptr);
					// payload data process
					
					if( rtp_session->cb_process )
					{
						(*rtp_session->cb_process)(im,user_ts);
					}
					//else
						freemsg (im);
					user_ts_save=user_ts;
					flag=0;
					r_cnt++;
					
				}
				
				
			}
			//else
			//	printf("1111flag=%x\n", flag );
		#endif
		}

		tick_time += 10;	// 20ms per frame, fps 50
		while(1)
		{
			clock_gettime(CLOCK_MONOTONIC,&ts);
			curtime = (ts.tv_sec*1000LL) + (ts.tv_nsec+500000LL)/1000000LL;
			realtime = curtime-orig;		
			diff = tick_time-realtime;
			if( diff <= 0 )
				break;
			ts.tv_sec=0;
			ts.tv_nsec=(int)diff*1000000LL;
			nanosleep(&ts,NULL);
			//printf("......\n");
		}
	}
}

/********************************************************************************
@function:
	start_one_rtp_process: start one rtp process
@parameters:
    type:       0/sendonly, 1/recvonly, 2/sendrecv
    pserver:    server ip address string
    port:       server ip port
    PayloadType: payload type
    ts:         timestamp
    cb:         received data callback pointer
@return:
    NULL/error, ONE_RTP_SESSION instance pointer
********************************************************************************/
ONE_RTP_SESSION* start_one_rtp_process(int type, char* pserver, short port, PayloadType payload, int ts, cb_rtp_proc cb)
{
	ONE_RTP_SESSION* rtp_session;
	rtp_session = malloc(sizeof(ONE_RTP_SESSION));	
	if(rtp_session == NULL)
	{
		return NULL;
	}
	pthread_mutex_init(&rtp_session->lck,0);

	rtp_session->type	= type;
	strcpy(rtp_session->ip,pserver);
	rtp_session->port 	= port;
	rtp_session->ts		= ts;
	rtp_session->psession = rtp_session_new_one(type,pserver,port,payload);
	rtp_session->cb_process = cb;

	qinit(&rtp_session->nalus);
	rtp_session->queue_size = 0;
	rtp_session->state=RTP_IDLE;
	rtp_session->state=RTP_START;
	pthread_create(&rtp_session->tid, NULL, rtp_session_dat_process, (void*)rtp_session);

	return rtp_session;
}

/********************************************************************************
@function:
	stop_one_rtp_process: stop one rtp process
@parameters:
    rtp_session:    instance pointer
@return:
	0/ok, -1/error
********************************************************************************/
int stop_one_rtp_process(ONE_RTP_SESSION* rtp_session)
{
	int wait_cnt=0;
	if( rtp_session == NULL )
		return -1;
	#if 1
	 while( rtp_session->state != RTP_RUN)
	    {
	        usleep(100*1000);
	        if( ++wait_cnt > 50 )
	        {
	            rtp_session->state = RTP_IDLE;
	            return -1;
	        }
	    }
	     rtp_session->state   = RTP_STOP;
		  usleep(100*1000);
	#else
	pthread_cancel(rtp_session->tid);
	#endif
	pthread_join(rtp_session->tid,NULL);
	rtp_session->state = RTP_IDLE;
	flushq(&rtp_session->nalus,0);
	rtp_session_del_one(rtp_session->psession);

	free(rtp_session);

	rtp_session = NULL;
	log_d("au rtp thread stop");
	return 0;
}

/********************************************************************************
@function:
	send_one_rtp_pack: send one rtp packet
@parameters:
    rtp_session:    instance pointer
    buf:            data pointer
    len:            data length
    tick:           data timestamp
@return:
	0/ok, -1/error
********************************************************************************/
int send_one_rtp_pack(ONE_RTP_SESSION* rtp_session, char *buf, int len, int tick)
{
	if( rtp_session == NULL )
		return -1;
	mblk_t *im;
	im = rtp_session_create_packet(rtp_session, 12, buf, len);
	if( !tick )
	{
		pthread_mutex_lock( &rtp_session->lck );
		putq(&rtp_session->nalus,im);
		rtp_session->queue_size++;
		pthread_mutex_unlock( &rtp_session->lck );
	}
	else
	{
		rtp_session_sendm_with_ts(rtp_session->psession,im,tick*rtp_session->ts);
	}
	return 0;
}
