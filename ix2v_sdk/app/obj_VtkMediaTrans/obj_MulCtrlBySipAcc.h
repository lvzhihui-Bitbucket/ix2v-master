/**
  ******************************************************************************
  * @file    obj_Tip_Ring.h
  * @author  cao
  * @version V00.01.00
  * @date    2016.5.31
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 

#ifndef _obj_MulCtrlBySipAcc_H
#define _obj_MulCtrlBySipAcc_H

#define MulCtrlBySipAccData_Max	128
#define RTP_VIDEO_MULTICAST_ADDRESS			"236.6.9.1"
//#define RTP_VIDEO_MULTICAST_ADDRESS			"192.168.1.198"
#define RTP_AUDIO_MULTICAST_ADDRESS			"236.6.9.2"
#define RTP_VIDEO_MULTICAST_DEFAULT_PORT	"28080"
#define RTP_AUDIO_MULTICAST_DEFAULT_PORT	"28081"

#define MulCtrlBySipAcc_CtrlCode_ReqVideoRtp			1
#define MulCtrlBySipAcc_CtrlCode_RspVideoRtp			2
#define MulCtrlBySipAcc_CtrlCode_ReqStopVideoRtp		3
#define MulCtrlBySipAcc_CtrlCode_RspStopVideoRtp		4
// Define Object Property-------------------------------------------------------
#pragma pack(1)  //指定按1字节对齐
//设备TIP申请数据包

typedef struct
{
	char sip_acc[20];
	uint16 control_code;
	uint16 data_len;
	uint8 data_buff[MulCtrlBySipAccData_Max];
} MulCtrlBySipAcc_Req_T;

typedef struct
{
	char sip_acc[20];
	uint16 control_code;
	uint16 result;
	uint16 data_len;
	uint8 data_buff[MulCtrlBySipAccData_Max];
} MulCtrlBySipAcc_Rsp_T;
#pragma pack()

// Define Object Function - Public----------------------------------------------
void MulCtrlBySipAccReqProcess(int target_ip, MulCtrlBySipAcc_Req_T* request);

// Define Object Function - Private---------------------------------------------


#endif


