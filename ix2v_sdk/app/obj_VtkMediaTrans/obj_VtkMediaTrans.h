/**
  ******************************************************************************
  * @file    task_ListUpdate.h
  * @author  zeng
  * @version V1.0.0
  * @date    2012.08.15
  * @brief   This file contains the headers of the power task_3.
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2012 V-Tec</center></h2>
  ******************************************************************************
  */
#ifndef obj_VtkMediaTrans_H
#define obj_VtkMediaTrans_H

#include "../task_survey/task_survey.h"		//cao_20151118

#define TCP_PACK_MAX_LEN		256

#define ViRtp_Ins					2

typedef struct
{
	VDP_MSG_HEAD head;
	char ser_ip[21];
	char acc[21];
	char pwd[21];
	char related_acc[21];
}VtkMediaTrans_Msg_t;

typedef struct
{
	VDP_MSG_HEAD head;
	int tcpDataLen;
	char tcpData[TCP_PACK_MAX_LEN];
}VtkMediaTrans_Msg_TCP_t;

typedef struct
{
	int state;
	int transfer_flag;
	int trans_tcp_fd;
	int record_source;		//0,local,1,linphone
	int timer;
	int trans_period;
	int start_time;
	char ser_ip[21];
	char acc[21];
	char pwd[21];
	char related_acc[21];
	// lzh_20190723_s
	short	au_port;
	short 	vd_port;
	// lzh_20190723_e
	// lzh_20201220_s
	int	relink;
	// lzh_20201220_e	
}VtkMediaTrans_Run_t;

/*******************************************************************************
                         Define task event flag
*******************************************************************************/

/*******************************************************************************
                  Define task vars, structures and macro
*******************************************************************************/



#endif
