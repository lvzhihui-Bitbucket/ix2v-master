
#ifndef _RTP_VIDEO_SEND_H_
#define _RTP_VIDEO_SEND_H_
#include <ortp/ortp.h>
#include <list.h>
#include "ortp/payloadtype.h"

typedef struct
{
	RtpSession* 	pvdsession;
	char			vd_server[20];
	short		vd_port;

	pthread_t	vd_send_tid;
	queue_t 		vd_send_nalus;
	queue_t 		nalus;
	int			vd_queue_size;
	int 			pkt_send_flag;
	int			state;
	int 			videoType;
	pthread_mutex_t vd_send_nalus_lck;
}VtkRtpSendObj_T;
// func:
//		create one rtp session with port and ts_inc, if start the video stream to send, call this
// para:
//		pserver:	rtp session instance ptr
//		port:		rtp send port
//		ts_inc:		rtp send timestamp increase gap
// return:
//		0/ok, -1/error
//int start_video_rtp_sender(VtkRtpSendObj_T *VtkRtpSendObj,char* pserver, short port, int ts_inc);
int start_video_rtp_sender_wan(char* pserver, short port, int ts_inc);
int start_video_rtp_sender_lan(char* pserver, short port, int ts_inc);
// func:
//		delete one rtp session, if stop the video stream send, call this
// para:
//		none
// return:
//		0/ok, -1/error
//int stop_video_rtp_sender(VtkRtpSendObj_T *VtkRtpSendObj);
int stop_video_rtp_sender_wan(void);
int stop_video_rtp_sender_lan(void);

// lzh_20210222_s			
// func:
//		send one video packet data, if get one frame data, call this to send
// para:
//		buf:	data pointer
//		len:	data length
//		tick:	timestamp, if 0, use thread timestamp
// return:
//		0/ok, -1/error
//int rtp_sender_send_with_ts(VtkRtpSendObj_T *VtkRtpSendObj,char *buf, int len, int tick );
// lzh_20210222_e	
int rtp_sender_send_with_ts_wan(char *buf, int len, int tick,int vdtype);
int rtp_sender_send_with_ts_lan(char *buf, int len, int tick,int vdtype);

void rtp_sender_start_pkt_send_wan(void);
void rtp_sender_start_pkt_send_lan(void);

void rtp_sender_pause_pkt_send_wan(void);
void rtp_sender_pause_pkt_send_lan(void);

void rtp_sender_unicast_list_init(void);
void rtp_sender_unicast_list_clear(void);
int rtp_sender_unicast_subscribe(char* pserver, short port);
void rtp_sender_send_with_ts_uni(char *buf, int len, int tick,int vdtype);
#endif


