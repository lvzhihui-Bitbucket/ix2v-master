/**
  ******************************************************************************
  * @file    task_ListUpdate.c
  * @author  lvzhihui
  * @version V1.0.0
  * @date    2016.04.15
  * @brief   This file contains the functions of task_ListUpdate
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2012 V-Tec</center></h2>
  ******************************************************************************
  */

#include "obj_VtkMediaTrans.h"
//#include "../video_service/ip_camera_control/encoder_vin/jpegcodec.h"
//#include "../task_io_server/vdp_IoServer_Data.h"
#include "vtk_media_tcp_transfer.h"
#include "elog_forcall.h"
#include "cJSON.h"

//#include "../task_VideoMenu/task_VideoMenu.h"

VtkMediaTrans_Run_t VtkMediaTrans_Run = {0};

#define MSG_TYPE_VtkMediaTrans_StartJpgPush  		0
#define MSG_TYPE_VtkMediaTrans_StopJpgPush  		1
#define MSG_TYPE_VtkMediaTrans_PushReq 				2
#define MSG_TYPE_VtkMediaTrans_JpgCaptureFinish 		3
#define MSG_TYPE_VtkMediaTrans_RcdSourceChange 		4
#define MSG_TYPE_VtkMediaTrans_PushReq 				5
#define MSG_TYPE_VtkMediaTrans_TcpCtrl  			6
#define MSG_TYPE_VtkMediaTrans_TcpDisconnect  		7
#define MSG_TYPE_VtkMediaTrans_StartViRtp  			8
#define MSG_TYPE_VtkMediaTrans_StartIPCRtp  			9
#define MSG_TYPE_VtkMediaTrans_StartLocalRtp  			10


Loop_vdp_common_buffer	vdp_VtkMediaTrans_mesg_queue;
Loop_vdp_common_buffer	vdp_VtkMediaTrans_sync_queue;
vdp_task_t				task_VtkMediaTrans;
vdp_task_t 				task_VtkMediaTcpCtrl = {0};	//cao_20210317

void vdp_VtkMediaTrans_mesg_data_process(char* msg_data, int len);
void* vdp_VtkMediaTrans_task( void* arg );
void timer_vtkmediatrans_callback(void);
void* vdp_VtkMediaTcpCtrl_task( void);

void vtk_TaskInit_VtkMediaTrans(int priority)
{
	init_vdp_common_queue(&vdp_VtkMediaTrans_mesg_queue, 500, (msg_process)vdp_VtkMediaTrans_mesg_data_process, &task_VtkMediaTrans);
	init_vdp_common_queue(&vdp_VtkMediaTrans_sync_queue, 100, NULL, &task_VtkMediaTrans);
	init_vdp_common_task(&task_VtkMediaTrans, MSG_ID_VtkMediaTrans, vdp_VtkMediaTrans_task, &vdp_VtkMediaTrans_mesg_queue, &vdp_VtkMediaTrans_sync_queue);
}

void exit_vdp_VtkMediaTrans_task(void)
{
	exit_vdp_common_queue(&vdp_VtkMediaTrans_mesg_queue);
	exit_vdp_common_queue(&vdp_VtkMediaTrans_sync_queue);
	exit_vdp_common_task(&task_VtkMediaTrans);	
}

void* vdp_VtkMediaTrans_task( void* arg )
{
	vdp_task_t*	 ptask 		= (vdp_task_t*)arg;
	p_vdp_common_buffer pdb = 0;
	int	size;
	thread_log_add("%s",__func__);
	rtp_sender_unicast_list_init();
	ptask->task_StartCompleted = 1;
	while( ptask->task_run_flag )
	{
		size = pop_vdp_common_queue( ptask->p_msg_buf, &pdb, VDP_QUEUE_POLLING_TIME);
		if( size > 0 )
		{
			(*ptask->p_msg_buf->process)(pdb,size);
			purge_vdp_common_queue( ptask->p_msg_buf );
		}
	}
	return 0;
}

//cao_20210317_s
void* vdp_VtkMediaTcpCtrl_task( void)
{
	while(1)
	{
		if(VtkMediaTrans_Run.state)
		{
			if(VtkMediaTrans_Run.trans_tcp_fd)
			{
				while(VtkMediaTrans_Run.state&&VtkMediaTrans_Run.trans_tcp_fd)
				{
					if(RecvTry_VtkTcpCtrlReq(VtkMediaTrans_Run.trans_tcp_fd)<0)
					{
						task_VtkMediaTcpCtrl.task_run_flag=0;
						return 0;
					}
					continue;
				}
				//dprintf("111111111111111111111111111 vdp_VtkMediaTcpCtrl_task\n");
				break;
			}
		}
		else 
		{
			if(VtkMediaTrans_Run.relink)
			{
				// check if relink the command "Api_media_transfer_account_req2";
				if( (VtkMediaTrans_Run.relink==2) && VtkMediaTrans_Run.trans_tcp_fd )
				{
					VtkMediaTrans_Run.state = 2;
				}
				// otherwise relink the command "Api_media_transfer_cennect_req2"
				
				VtkMediaTrans_Run.relink = 0;
				API_VtkMediaTrans_StartJpgPush(2,VtkMediaTrans_Run.ser_ip,VtkMediaTrans_Run.acc,VtkMediaTrans_Run.pwd,VtkMediaTrans_Run.related_acc);
				task_VtkMediaTcpCtrl.task_run_flag=0;
				break;
			}
		}

		//dprintf("2222222222222222222222222222222 vdp_VtkMediaTcpCtrl_task\n");
		usleep(100*1000);
	}
	task_VtkMediaTcpCtrl.task_run_flag=0;
	return 0;
}
//cao_20210317_e


int StartVtkMediaTcpCtrl(void)
{
	if(!task_VtkMediaTcpCtrl.task_run_flag)
	{
		task_VtkMediaTcpCtrl.task_run_flag = 1;
		pthread_create(&task_VtkMediaTcpCtrl.task_pid, NULL, vdp_VtkMediaTcpCtrl_task, NULL);
	}
}


int StopVtkMediaTcpCtrl(void)
{
	if(task_VtkMediaTcpCtrl.task_run_flag)
	{
		//pthread_cancel(task_VtkMediaTcpCtrl.task_pid);
		pthread_join(task_VtkMediaTcpCtrl.task_pid,NULL);
		task_VtkMediaTcpCtrl.task_run_flag = 0;
	}
}

static void RTP_ChannelChangeReq(short channel)
{
	char dat[128];
	int  dat_len;
	COMMU_PACK_RTP_CTRL_REQ rtpCtrlReq;
	
	rtpCtrlReq.VIDEO_CHANNEL = channel;
	rtpCtrlReq.DATA_LEN = 6;
	rtpCtrlReq.AUDIO_CHANNEL = 0;
	rtpCtrlReq.RESERVE = 0;

	dat_len = sizeof(rtpCtrlReq) - sizeof(rtpCtrlReq.head) - sizeof(rtpCtrlReq.ctrl_cmd);

	send_VtkTcpReq(VtkMediaTrans_Run.trans_tcp_fd, APP_CTRL_REQ, RTP_CHANNEL_CHANGE, (char*)&rtpCtrlReq.DATA_LEN, dat_len);
}

void vdp_VtkMediaTrans_mesg_data_process(char* msg_data,int len)
{
	VtkMediaTrans_Msg_t* pMsgVtkMediaTrans = (VtkMediaTrans_Msg_t*)msg_data;
	VtkMediaTrans_Msg_TCP_t* pMsgVtkMediaTransTcpCtrl = (VtkMediaTrans_Msg_TCP_t*)msg_data;
	COMMU_PACK_CONTROL_REQ* pTcpCtrl_pkt = (COMMU_PACK_CONTROL_REQ*)pMsgVtkMediaTransTcpCtrl->tcpData;
	COMMU_PACK_APP_NOTIFY_MEDIA_STATUS_REQ* ptrAppPack = (COMMU_PACK_APP_NOTIFY_MEDIA_STATUS_REQ*)pMsgVtkMediaTransTcpCtrl->tcpData;
	COMMU_PACK_SERVER_NOTIFY *ptrPack = (COMMU_PACK_SERVER_NOTIFY*)pMsgVtkMediaTransTcpCtrl->tcpData;
	static int rtp_state=0;
	static int rtp_local_flag=0;
	static time_t rtp_local_time=0;
	static char rtp_local_ip[20]={0};
	static char rtp_local_port[6]={0};
	
	
	switch (pMsgVtkMediaTrans->head.msg_type)
	{
		case MSG_TYPE_VtkMediaTrans_StartJpgPush:	
			dprintf("......MSG_TYPE_VtkMediaTrans_StartJpgPush..... VtkMediaTrans_Run.state=%d,VtkMediaTrans_Run.trans_tcp_fd=%d\n", VtkMediaTrans_Run.state, VtkMediaTrans_Run.trans_tcp_fd);
			if(VtkMediaTrans_Run.state == 0 )
			{
				VtkMediaTrans_Run.state = 1;

				StartVtkMediaTcpCtrl();
				
				strcpy(VtkMediaTrans_Run.ser_ip,pMsgVtkMediaTrans->ser_ip);
				strcpy(VtkMediaTrans_Run.acc,pMsgVtkMediaTrans->acc);
				strcpy(VtkMediaTrans_Run.related_acc,pMsgVtkMediaTrans->related_acc);
				strcpy(VtkMediaTrans_Run.pwd,pMsgVtkMediaTrans->pwd);
				rtp_state=0;
				//rtp_local_flag=0;
				// lzh_20201110_s
				char* portstr = strstr(VtkMediaTrans_Run.ser_ip,":");
				if( portstr != NULL ) portstr[0] = 0;
				// lzh_20201110_e
				
				
				VtkMediaTrans_Run.trans_tcp_fd = Api_media_transfer_cennect_req2(VtkMediaTrans_Run.ser_ip,VtkMediaTrans_Run.acc,VtkMediaTrans_Run.pwd,VtkMediaTrans_Run.related_acc,&VtkMediaTrans_Run.au_port,&VtkMediaTrans_Run.vd_port);
				if(VtkMediaTrans_Run.trans_tcp_fd != 0)
				{
					// audio media init
					// lzh_20210702_s
					#if 0
					extern int audio_rtp_outside_flag;
					if( !audio_rtp_outside_flag )
					{
						dprintf("!!!!!!!!set linphone audio rtp port\n");
						API_linphonec_apply_start_press_to_talk(!API_linphonec_is_vdout_enable(),VtkMediaTrans_Run.ser_ip,VtkMediaTrans_Run.au_port);
					}
					else
					#endif
					{
						dprintf("!!!!!!!!set outside audio rtp port\n");
						au_service_set_rtp(VtkMediaTrans_Run.ser_ip,VtkMediaTrans_Run.au_port);
					}

					// video media init
					printf("!!!!!!!!set linphone audio rtp type\n");
					RTP_ChannelChangeReq(API_linphonec_is_vdout_enable() ? 0 : 1);
					if( !API_linphonec_is_vdout_enable() )
					{
						#if 0
						dprintf("!!!!!!!!start_video_rtp_sender\n");
						if( start_video_rtp_sender_wan(VtkMediaTrans_Run.ser_ip,VtkMediaTrans_Run.vd_port,0) == -1 )
						{
							dprintf("!!!!!!!!start err,stop_video_rtp_sender and start again\n");
							stop_video_rtp_sender_wan();
							start_video_rtp_sender_wan(VtkMediaTrans_Run.ser_ip,VtkMediaTrans_Run.vd_port,0);
						}
						#endif
					}						
					dprintf("!!!!!!!!creat media push successfull\n");		
					log_d("VtkMediaTrans:creat rtp/tcp succ");
					// lzh_20210702_e
				}
				else
				{
					log_w("VtkMediaTrans:creat tcp fail");
					VtkMediaTrans_Run.state = 0;
					// lzh_20201220_s
					// set relink flag 1 for command "Api_media_transfer_cennect_req2"
					VtkMediaTrans_Run.relink = 1;	
					// lzh_20201220_e
					dprintf("!!!!!!!!creat media push fail\n");
				}					
			}
			else if(VtkMediaTrans_Run.state == 2 && VtkMediaTrans_Run.trans_tcp_fd!=0 )
			{
				VtkMediaTrans_Run.state = 1;
				
				strcpy(VtkMediaTrans_Run.ser_ip,pMsgVtkMediaTrans->ser_ip);
				strcpy(VtkMediaTrans_Run.acc,pMsgVtkMediaTrans->acc);
				strcpy(VtkMediaTrans_Run.related_acc,pMsgVtkMediaTrans->related_acc);
				strcpy(VtkMediaTrans_Run.pwd,pMsgVtkMediaTrans->pwd);
				rtp_state=0;
				rtp_local_flag=0;
				// lzh_20201110_s
				char* portstr = strstr(VtkMediaTrans_Run.ser_ip,":");
				if( portstr != NULL ) portstr[0] = 0;
				// lzh_20201110_e
				
				VtkMediaTrans_Run.trans_tcp_fd = Api_media_transfer_account_req2(VtkMediaTrans_Run.trans_tcp_fd,VtkMediaTrans_Run.ser_ip,VtkMediaTrans_Run.acc,VtkMediaTrans_Run.pwd,VtkMediaTrans_Run.related_acc,&VtkMediaTrans_Run.au_port,&VtkMediaTrans_Run.vd_port);
				
				if(VtkMediaTrans_Run.trans_tcp_fd != 0)
				{
					// audio media init
					// lzh_20210702_s
					#if 0
					extern int audio_rtp_outside_flag;
					if( !audio_rtp_outside_flag )
					{
						dprintf("!!!!!!!!set linphone audio rtp port\n");
						API_linphonec_apply_start_press_to_talk(!API_linphonec_is_vdout_enable(),VtkMediaTrans_Run.ser_ip,VtkMediaTrans_Run.au_port);
					}
					else
					#endif
					{
						dprintf("!!!!!!!!set outside audio rtp port\n");
						au_service_set_rtp(VtkMediaTrans_Run.ser_ip,VtkMediaTrans_Run.au_port);
					}

					// video media init
					dprintf("!!!!!!!!set linphone audio rtp type\n");
					RTP_ChannelChangeReq(API_linphonec_is_vdout_enable() ? 0 : 1);
					if( !API_linphonec_is_vdout_enable() )
					{
						//start_video_rtp_sender_wan(VtkMediaTrans_Run.ser_ip,VtkMediaTrans_Run.vd_port,0);						
					}

					dprintf("!!!!!!!!creat media push successfull\n");
					// lzh_20210702_e
				}
				else
				{
					VtkMediaTrans_Run.state = 0;
					// lzh_20201220_s
					// set relink flag 1 for command "Api_media_transfer_account_req2"
					VtkMediaTrans_Run.relink = 2;	
					// lzh_20201220_e					
					dprintf("!!!!!!!!creat media push fail\n");
				}
			}
			break;
			
		case MSG_TYPE_VtkMediaTrans_StopJpgPush:
			dprintf("......MSG_TYPE_VtkMediaTrans_StopJpgPush..... VtkMediaTrans_Run.state=%d,VtkMediaTrans_Run.trans_tcp_fd=%d\n", VtkMediaTrans_Run.state, VtkMediaTrans_Run.trans_tcp_fd);
			if(VtkMediaTrans_Run.state)
			{
				VtkMediaTrans_Run.state = 0;
				StopVtkMediaTcpCtrl();

				if(VtkMediaTrans_Run.trans_tcp_fd!=0)
				{
					api_rtp_del_all_send_node(ViRtp_Ins);
					api_rtp_del_all_send_node(1);
					api_ipc_rtp_del_all_send_node();
					Stop_OneIpc_Rtp(0);
					rtp_local_flag=0;
					//api_rtp_del_one_send_node(2,0,VtkMediaTrans_Run.ser_ip,VtkMediaTrans_Run.vd_port);
					//stop_video_rtp_sender_wan();
					//stop_video_rtp_sender_lan();
					//rtp_sender_unicast_list_clear();
					//API_LocalCaptureOff();
					Api_media_transfer_discennect(VtkMediaTrans_Run.trans_tcp_fd);

					// lzh_20201201_s
					API_linphonec_apply_start_press_to_talk(!API_linphonec_is_vdout_enable(),NULL,0);
					// lzh_20201201_e
					VtkMediaTrans_Run.trans_tcp_fd = 0;
					
					// lzh_20210702_s
					//extern int audio_rtp_outside_flag;
					//if( audio_rtp_outside_flag )
					{
						//API_talk_off();			
					}
					// lzh_20210702_e
				}
				log_d("VtkMediaTrans:close rtp/tcp");
			}
			// lzh_20201220_s
			// reset relink flag
			VtkMediaTrans_Run.relink = 0;
			// lzh_20201220_e								
			break;
			
		case MSG_TYPE_VtkMediaTrans_PushReq:
			if( VtkMediaTrans_Run.state == 0 )
			{
				VtkMediaTrans_Run.state = 2;
				
				strcpy(VtkMediaTrans_Run.ser_ip,		pMsgVtkMediaTrans->ser_ip);
				strcpy(VtkMediaTrans_Run.acc,			pMsgVtkMediaTrans->acc);
				strcpy(VtkMediaTrans_Run.related_acc,	pMsgVtkMediaTrans->related_acc);
				strcpy(VtkMediaTrans_Run.pwd,			pMsgVtkMediaTrans->pwd);

				// lzh_20201110_s
				char* portstr = strstr(VtkMediaTrans_Run.ser_ip,":");
				if( portstr != NULL ) portstr[0] = 0;
				// lzh_20201110_e
				
 				short push_result;
				VtkMediaTrans_Run.trans_tcp_fd =Api_device_connect_push_req(VtkMediaTrans_Run.ser_ip,VtkMediaTrans_Run.acc,VtkMediaTrans_Run.pwd,VtkMediaTrans_Run.related_acc,&push_result);
				if(VtkMediaTrans_Run.trans_tcp_fd != 0)
				{
					OS_SignalEvent(1, GetTaskAccordingMsgID(pMsgVtkMediaTrans->head.msg_source_id));
				}
				else
				{
					OS_SignalEvent(0, GetTaskAccordingMsgID(pMsgVtkMediaTrans->head.msg_source_id));
				}
				
			}
			break;
			
		case MSG_TYPE_VtkMediaTrans_TcpDisconnect:
			recv_linphone_close();
			API_linphonec_Close();
			api_ipc_rtp_del_all_send_node();
			rtp_local_flag=0;
			break;
			
		case MSG_TYPE_VtkMediaTrans_TcpCtrl:
			//printf("1111111111111pTcpCtrl_pkt->head.cmd_type=%d\n",pTcpCtrl_pkt->head.cmd_type);
			if(pTcpCtrl_pkt->head.cmd_type == DEV_CONTROL_REQ )
			{
				log_d("VtkMediaTrans:recv ctrl cmd[%d]",pTcpCtrl_pkt->ctrl_cmd);
				vtkTcpControlReq_Process(VtkMediaTrans_Run.trans_tcp_fd, pTcpCtrl_pkt->ctrl_cmd, pTcpCtrl_pkt->ctrl_dat, pMsgVtkMediaTransTcpCtrl->tcpDataLen - TCPCTRL_PACK_HEAD_LENTH);
			}
			else if(pTcpCtrl_pkt->head.cmd_type == SERVER_NOTIFY2DEVICE_TYPE )
			{				
				if( ptrPack->notify_cmd == 0x0001 )
				{
					// when app connect ok,then send the control code to im send i-frame at once
					//extern void trigger_send_key_frame(void);
					//trigger_send_key_frame();
				}
			}
			else if(pTcpCtrl_pkt->head.cmd_type == APP_CTRL_REQ )
			{				
				if( ptrAppPack->notify_cmd == MEDIA_ERR_NOTIFY_REQ_CODE )
				{
					COMMU_PACK_APP_NOTIFY_MEDIA_STATUS_RSP appRSP;
					appRSP.head.cmd_type	= APP_CTRL_RSP;
					appRSP.head.dev_type	= 0;
					appRSP.data_len 		= 8;
					appRSP.notify_cmd		= MEDIA_ERR_NOTIFY_RSP_CODE;
					appRSP.reserve			= ptrAppPack->reserve;
					appRSP.audio_status 	= ptrAppPack->audio_status;
					appRSP.video_status 	= ptrAppPack->video_status;
					if( ptrAppPack->data_len != 6 )
						appRSP.result		= 1;
					else
					{
						appRSP.result		= 0;
						API_linphonec_apply_update_audio_video(ptrAppPack->audio_status,ptrAppPack->video_status);
					}
					send(VtkMediaTrans_Run.trans_tcp_fd, (char*)&appRSP, sizeof(appRSP),0);		
				}
			}		
			break;
		case MSG_TYPE_VtkMediaTrans_StartViRtp:
			if(VtkMediaTrans_Run.state&&VtkMediaTrans_Run.trans_tcp_fd!=0)
			{
				if(api_rtp_add_one_send_node(ViRtp_Ins,0,VtkMediaTrans_Run.ser_ip,VtkMediaTrans_Run.vd_port)>0)
				{
					log_d("VtkMediaTrans:Add ViRtp Succ [%s:%d]",VtkMediaTrans_Run.ser_ip,VtkMediaTrans_Run.vd_port);
				}
				else
				{
					log_w("VtkMediaTrans:Add ViRtp Fail [%s:%d]",VtkMediaTrans_Run.ser_ip,VtkMediaTrans_Run.vd_port);
				}
				rtp_state=1;
				if(rtp_local_flag&&(time(NULL)-rtp_local_time)<=6)
				{
					api_rtp_add_one_send_node(1,0,rtp_local_ip,atoi(rtp_local_port));
					rtp_local_flag=0;
				}
			}
			break;
		case MSG_TYPE_VtkMediaTrans_StartIPCRtp:
			if(VtkMediaTrans_Run.state&&VtkMediaTrans_Run.trans_tcp_fd!=0)
			{
				api_ipc_rtp_add_one_send_node(pMsgVtkMediaTrans->head.msg_sub_type,0,VtkMediaTrans_Run.ser_ip,VtkMediaTrans_Run.vd_port);
				rtp_state=2;
				if(rtp_local_flag&&(time(NULL)-rtp_local_time)<=6)
				{
					//rtp_sender_unicast_subscribe(rtp_local_ip,atoi(rtp_local_port));
					//rtp_sender_unicast_subscribe("192.168.243.75",atoi(rtp_local_port));
					api_ipc_rtp_add_one_send_node(0,0,rtp_local_ip,atoi(rtp_local_port));
					rtp_local_flag=0;
				}
			}
			break;
		case MSG_TYPE_VtkMediaTrans_StartLocalRtp:
			if(VtkMediaTrans_Run.state&&VtkMediaTrans_Run.trans_tcp_fd!=0&&rtp_state==1)
			{
				rtp_local_flag=0;
				api_rtp_add_one_send_node(1,0,pMsgVtkMediaTrans->ser_ip,atoi(pMsgVtkMediaTrans->acc));
			}
			else if(VtkMediaTrans_Run.state&&VtkMediaTrans_Run.trans_tcp_fd!=0&&rtp_state==2)
			{
				rtp_local_flag=0;
				api_ipc_rtp_add_one_send_node(0,0,pMsgVtkMediaTrans->ser_ip,atoi(pMsgVtkMediaTrans->acc));
				//rtp_sender_unicast_subscribe(rtp_local_ip,atoi(rtp_local_port));
				//rtp_sender_unicast_subscribe("192.168.243.75",atoi(rtp_local_port));
			}
			else
			{

				rtp_local_flag=1;
				rtp_local_time=time(NULL);
				strcpy(rtp_local_ip,pMsgVtkMediaTrans->ser_ip);
				strcpy(rtp_local_port,pMsgVtkMediaTrans->acc);	
			}
			break;
			
		default:
			break;
	}
}

int API_VtkMediaTrans_StartJpgPush(int start_mode,char *ser_ip,char *acc,char *pwd,char *related_acc)
{
	VtkMediaTrans_Msg_t msg;

	msg.head.msg_source_id 	= 0;
	msg.head.msg_target_id 	= MSG_ID_VtkMediaTrans;
	msg.head.msg_type 		= MSG_TYPE_VtkMediaTrans_StartJpgPush;
	msg.head.msg_sub_type 	= start_mode;
	
	strcpy(msg.ser_ip,ser_ip);
	strcpy(msg.acc,acc);
	strcpy(msg.pwd,pwd);
	strcpy(msg.related_acc,related_acc);
	
	dprintf(" ...........................API_VtkMediaTrans_StartJpgPush................... \n");
	
	// 压入本地队列
	return push_vdp_common_queue(&vdp_VtkMediaTrans_mesg_queue, (char*)&msg, sizeof(msg));
}


int API_VtkMediaTrans_StopJpgPush(void)
{
	VDP_MSG_HEAD msg;

	msg.msg_source_id 	= 0;
	msg.msg_target_id 	= MSG_ID_VtkMediaTrans;
	msg.msg_type 		= MSG_TYPE_VtkMediaTrans_StopJpgPush;
	msg.msg_sub_type 	= 0;
	// 压入本地队列
	return push_vdp_common_queue(&vdp_VtkMediaTrans_mesg_queue, (char*)&msg, sizeof(VDP_MSG_HEAD));
}

int API_VtkMediaTrans_TcpCtrl(char *data, int dataLen)
{
	VtkMediaTrans_Msg_TCP_t msg;

	msg.head.msg_source_id 	= 0;
	msg.head.msg_target_id 	= MSG_ID_VtkMediaTrans;
	msg.head.msg_type 		= MSG_TYPE_VtkMediaTrans_TcpCtrl;
	msg.head.msg_sub_type 	= 0;

	dataLen = (dataLen > TCP_PACK_MAX_LEN)? TCP_PACK_MAX_LEN : dataLen;
	if(dataLen != 0 && data != NULL)
	{
		memcpy(msg.tcpData ,data, dataLen);
	}
	else
	{
		dataLen = 0;
	}
	msg.tcpDataLen = dataLen;

	printf(" ...........................API_VtkMediaTrans_TcpCtrl................... \n");
	
	// 压入本地队列
	return push_vdp_common_queue(&vdp_VtkMediaTrans_mesg_queue, (char*)&msg, sizeof(msg)-TCP_PACK_MAX_LEN + dataLen);
}

int API_VtkMediaTrans_TcpDisconnect(void)
{
	VtkMediaTrans_Msg_TCP_t msg;

	msg.head.msg_source_id 	= 0;
	msg.head.msg_target_id 	= MSG_ID_VtkMediaTrans;
	msg.head.msg_type 		= MSG_TYPE_VtkMediaTrans_TcpDisconnect;
	msg.head.msg_sub_type 	= 0;
	msg.tcpDataLen = 0;
	
	dprintf(" ...........................API_VtkMediaTrans_TcpDisconnect................... \n");
	
	// 压入本地队列
	return push_vdp_common_queue(&vdp_VtkMediaTrans_mesg_queue, (char*)&msg, sizeof(msg)-TCP_PACK_MAX_LEN);
}

int API_VtkMediaTrans_StartViRtp(void)
{
	VtkMediaTrans_Msg_TCP_t msg;

	msg.head.msg_source_id 	= 0;
	msg.head.msg_target_id 	= MSG_ID_VtkMediaTrans;
	msg.head.msg_type 		= MSG_TYPE_VtkMediaTrans_StartViRtp;
	msg.head.msg_sub_type 	= 0;
	msg.tcpDataLen = 0;
	
	dprintf(" ...........................API_VtkMediaTrans_StartViRtp................... \n");
	
	// 压入本地队列
	return push_vdp_common_queue(&vdp_VtkMediaTrans_mesg_queue, (char*)&msg, sizeof(msg)-TCP_PACK_MAX_LEN);
}

int API_VtkMediaTrans_StartIPCRtp(int ch)
{
	VtkMediaTrans_Msg_TCP_t msg;

	msg.head.msg_source_id 	= 0;
	msg.head.msg_target_id 	= MSG_ID_VtkMediaTrans;
	msg.head.msg_type 		= MSG_TYPE_VtkMediaTrans_StartIPCRtp;
	msg.head.msg_sub_type 	= ch;
	msg.tcpDataLen = 0;
	
	dprintf(" ...........................API_VtkMediaTrans_StartViRtp................... \n");
	
	// 压入本地队列
	return push_vdp_common_queue(&vdp_VtkMediaTrans_mesg_queue, (char*)&msg, sizeof(msg)-TCP_PACK_MAX_LEN);
}
int API_VtkMediaTrans_StartLocalRtp(char *target_ip,char *target_port)
{
	VtkMediaTrans_Msg_t msg;

	msg.head.msg_source_id 	= 0;
	msg.head.msg_target_id 	= MSG_ID_VtkMediaTrans;
	msg.head.msg_type 		= MSG_TYPE_VtkMediaTrans_StartLocalRtp;
	msg.head.msg_sub_type 	= 0;
	
	strcpy(msg.ser_ip,target_ip);
	strcpy(msg.acc,target_port);
	//strcpy(msg.pwd,pwd);
	//strcpy(msg.related_acc,related_acc);
	
	dprintf(" ...........................API_VtkMediaTrans_StartJpgPush................... \n");
	
	// 压入本地队列
	return push_vdp_common_queue(&vdp_VtkMediaTrans_mesg_queue, (char*)&msg, sizeof(msg));
}
int API_VtkMediaTrans_VtkPushReq(char *ser_ip,char *acc,char *pwd,char *related_acc)
{
// lzh_20201220_s
#if 0  // disable the api
	VtkMediaTrans_Msg_t msg;

	msg.head.msg_source_id 	=  GetMsgIDAccordingPid(pthread_self());
	msg.head.msg_target_id 	= MSG_ID_VtkMediaTrans;
	msg.head.msg_type 		= MSG_TYPE_VtkMediaTrans_PushReq;
	msg.head.msg_sub_type 	= 0;
	
	strcpy(msg.ser_ip,ser_ip);
	strcpy(msg.acc,acc);
	strcpy(msg.pwd,pwd);
	strcpy(msg.related_acc,related_acc);

	OS_WaitSingleEventTimed(1,1);
	// 压入本地队列
	push_vdp_common_queue(&vdp_VtkMediaTrans_mesg_queue, (char*)&msg, sizeof(msg));

	return OS_WaitSingleEventTimed(1,3000);
#else
	return 0;
#endif
// lzh_20201220_s
}
cJSON *GetVtkMediaTransState(void)
{
	static cJSON *state=NULL;
	char *cur_calltype_str,*cur_state_str;
	

	if(state!=NULL)
		cJSON_Delete(state);
	
	state=cJSON_CreateObject();
	if(state==NULL)
		return NULL;
	if(VtkMediaTrans_Run.state==0)
	{
		cur_state_str="Idle";
		cJSON_AddStringToObject(state,"STATE",cur_state_str);
		return state;
	}
	else if(VtkMediaTrans_Run.state&&VtkMediaTrans_Run.trans_tcp_fd)
	{
		cur_state_str="Linked";
		
	}
	else
	{
		cur_state_str="Ulinked";
	}
	cJSON_AddStringToObject(state,"STATE",cur_state_str);
	cJSON_AddStringToObject(state,"SER",VtkMediaTrans_Run.ser_ip);
	cJSON_AddStringToObject(state,"ACC",VtkMediaTrans_Run.acc);
	cJSON_AddStringToObject(state,"RELATE",VtkMediaTrans_Run.related_acc);
	
	return state;
}
int GetVtkMediaTransAuPort(void)
{
	return VtkMediaTrans_Run.au_port;
}
