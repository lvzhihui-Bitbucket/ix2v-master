/**
  ******************************************************************************
  * @file    obj_Tip_Ring.c
  * @author  czb
  * @version V00.01.00
  * @date    2016.5.31
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 
	  //cao_20170304  整个文件


#include "vtk_udp_stack_device_update.h"
#include "obj_MulCtrlBySipAcc.h"
#include "elog_forcall.h"
#include "obj_VtkMediaTrans.h"


//接收到RING指令的处理
void MulCtrlBySipAccReqProcess(int target_ip, MulCtrlBySipAcc_Req_T* request)
{
#if 1
	MulCtrlBySipAcc_Rsp_T respond;
	uint8 i;
	char sip_acc[40];
	char *pch;
	int len=0;
	int vi_type=0,ai_type=0;
	char vi_ip[16],ai_ip[16];
	char vi_port[6]=RTP_VIDEO_MULTICAST_DEFAULT_PORT;
	char ai_port[6]=RTP_AUDIO_MULTICAST_DEFAULT_PORT;
	struct in_addr temp_addr;
	
	//Get_SipConfig_Account(sip_acc);
	get_sip_master_acc(sip_acc);
	//printf("----------MulCtrlBySipAcc:sip:%s,self sip:%s,ctrl code:%d\n",request->sip_acc,sip_acc,request->control_code);
	if(strcmp(request->sip_acc,sip_acc)==0)
	{

		if(request->control_code == MulCtrlBySipAcc_CtrlCode_ReqVideoRtp)
		{
			if(request->data_len>0)
			{
				
				pch=request->data_buff;
				while(len<request->data_len)
				{
					if(memcmp(pch,"vi-u:",strlen("vi-u:"))==0)
					{
						vi_type=1;
						
						if(strlen(pch)>strlen("vi-u:"))
						{
							strcpy(vi_ip,pch+strlen("vi-u:"));
						}
						else
						{
							temp_addr.s_addr = target_ip;
							strcpy(vi_ip,inet_ntoa(temp_addr));	
						}
					}
					if(memcmp(pch,"vi-m:",strlen("vi-m:"))==0)
					{
						vi_type=0;
						strcpy(vi_ip,RTP_VIDEO_MULTICAST_ADDRESS);
					}
					
					if(memcmp(pch,"vp:",strlen("vp:"))==0)
					{
						if(strlen(pch)>strlen("vp:"))
						{
							strcpy(vi_port,pch+strlen("vp:"));
						}
						else
						{
							strcpy(vi_port,RTP_VIDEO_MULTICAST_DEFAULT_PORT);	
						}
					}
					
					if(memcmp(pch,"ai-u:",strlen("ai-u:"))==0)
					{
						ai_type=1;
						if(strlen(pch)>strlen("ai-u:"))
						{
							strcpy(ai_ip,pch+strlen("ai-u:"));
						}
						else
						{
							temp_addr.s_addr = target_ip;
							strcpy(ai_ip,inet_ntoa(temp_addr));	
						}
					}
					if(memcmp(pch,"ai-m:",strlen("ai-m:"))==0)
					{
						ai_type=0;
						strcpy(ai_ip,RTP_AUDIO_MULTICAST_ADDRESS);
					}
					if(memcmp(pch,"ap:",strlen("ap:"))==0)
					{
						if(strlen(pch)>strlen("ap:"))
						{
							strcpy(ai_port,pch+strlen("ap:"));
						}
						else
						{
							strcpy(ai_port,RTP_AUDIO_MULTICAST_DEFAULT_PORT);	
						}
					}
					len+=(strlen(pch)+1);
					pch+=(strlen(pch)+1);
						
				}
				
			}
			else
			{
				vi_type=1;
				ai_type=1;
				temp_addr.s_addr = target_ip;
				strcpy(vi_ip,inet_ntoa(temp_addr));
				strcpy(ai_ip,inet_ntoa(temp_addr));
				strcpy(vi_port,RTP_VIDEO_MULTICAST_DEFAULT_PORT);
				strcpy(ai_port,RTP_AUDIO_MULTICAST_DEFAULT_PORT);
			}
			strcpy(respond.sip_acc,sip_acc);
			respond.control_code=MulCtrlBySipAcc_CtrlCode_RspVideoRtp;
			respond.result=0;
			respond.data_len=0;
			
			//snprintf(respond.data_buff,MulCtrlBySipAccData_Max-respond.data_len,"vi-m:%s",RTP_VIDEO_MULTICAST_ADDRESS);
			if(vi_type ==1)
			{
				strcpy(respond.data_buff,"vi-u:");
			}
			else
			{
				strcpy(respond.data_buff,"vi-m:");
				strcat(respond.data_buff,vi_ip);
			}
			respond.data_len+=(strlen(respond.data_buff+respond.data_len)+1);

			snprintf(respond.data_buff+respond.data_len,MulCtrlBySipAccData_Max-respond.data_len,"vp:%s",vi_port);
			respond.data_len+=(strlen(respond.data_buff+respond.data_len)+1);

			//snprintf(respond.data_buff+respond.data_len,MulCtrlBySipAccData_Max-respond.data_len,"ai-u:");
			if(ai_type ==1)
			{
				strcpy(respond.data_buff+respond.data_len,"ai-u:");
			}
			else
			{
				strcpy(respond.data_buff+respond.data_len,"ai-m:");
				strcat(respond.data_buff+respond.data_len,ai_ip);
			}
			respond.data_len+=(strlen(respond.data_buff+respond.data_len)+1);

			snprintf(respond.data_buff+respond.data_len,MulCtrlBySipAccData_Max-respond.data_len,"ap:%s",ai_port);
			respond.data_len+=(strlen(respond.data_buff+respond.data_len)+1);
			//printf("MulCtrlBySipAcc:ReqVideoRtp succ!!!!!!!!!!\n");
			api_udp_device_update_send_data( target_ip, htons(MUL_CTRL_BY_SIPACC_RSP), (char*)&respond, respond.data_len+26 );
			if(vi_type==1)
			{
				//rtp_sender_unicast_subscribe(vi_ip,atoi(vi_port));
				//trigger_send_key_frame();
				#if 0
				if(linphone_becall_if_ipcmon())
					api_ipc_rtp_add_one_send_node(0,0,vi_ip,atoi(vi_port));
				else
				{
					if(api_rtp_add_one_send_node(1,0,vi_ip,atoi(vi_port))>0)
					{
						log_d("VtkMediaTrans:Add wifi-direct Succ [%s:%s]",vi_ip,vi_port);
					}
					else
					{
						log_w("VtkMediaTrans:Add wifi-direct Fail [%s:%s]",vi_ip,vi_port);
					}
				}
				#endif
				API_VtkMediaTrans_StartLocalRtp(vi_ip,vi_port);
			}
			else
			{
				//start_video_rtp_sender_lan(vi_ip,atoi(vi_port),0);
			}
		}
		else if(request->control_code == MulCtrlBySipAcc_CtrlCode_ReqStopVideoRtp)
		{
			strcpy(respond.sip_acc,sip_acc);
			respond.control_code=MulCtrlBySipAcc_CtrlCode_RspStopVideoRtp;
			respond.result=0;
			respond.data_len=0;
			//rtp_sender_pause_pkt_send_wan();
			api_udp_device_update_send_data( target_ip, htons(MUL_CTRL_BY_SIPACC_RSP), (char*)&respond, respond.data_len+26 );
		}
		
	}
	else
	{
		printf("ReqVideoRtp fail!!!!!!!!!!\n");
	}
#endif
}
/*********************************************************************************************************
**  End Of File
*********************************************************************************************************/

