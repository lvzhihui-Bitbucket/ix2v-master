
#ifndef _RTP_UTILITY_H_
#define _RTP_UTILITY_H_

#include <sys/types.h>
#include <sys/time.h>
#include <sys/ioctl.h>
#include <sys/stat.h>
#include <sys/socket.h>   
#include <sys/un.h>

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <unistd.h>
#include <error.h>
#include <signal.h>
#include <pthread.h>
#include <semaphore.h>
#include <sched.h>
#include <fcntl.h>
#include <errno.h>
#include <time.h> 
#include <netinet/in.h>    
#include <netinet/if_ether.h>   
#include <net/if.h>   
#include <net/if_arp.h>   
#include <arpa/inet.h>     
#include <assert.h>
#include <sys/timeb.h>
#include <sys/mman.h>
#include <sys/ipc.h>
#include <sys/resource.h>
#include <unistd.h>
#include <dirent.h>

#include <ortp/ortp.h>
#include "ortp/payloadtype.h"

#define mblk_set_timestamp_info(m,ts) (m)->reserved1=(ts);
#define mblk_get_timestamp_info(m)    ((m)->reserved1)
#define mblk_set_marker_info(m,bit)   (m)->reserved2=((m)->reserved2|bit)
#define mblk_get_marker_info(m)	      ((m)->reserved2&0x1)
#define mblk_set_rate(m,bits)         (m)->reserved2=((m)->reserved2|(bits)<<1)
#define mblk_get_rate(m)              (((m)->reserved2>>1)&0x3)
#define mblk_set_payload_type(m,bits) (m)->reserved2=((m)->reserved2|(bits<<3))
#define mblk_get_payload_type(m)      (((m)->reserved2>>3)&0x7F)
#define mblk_set_precious_flag(m,bit)    (m)->reserved2=(m)->reserved2|((bit & 0x1)<<10) /*use to prevent mirroring*/
#define mblk_get_precious_flag(m)    (((m)->reserved2)>>10 & 0x1)

typedef void (*cb_rtp_proc)(mblk_t *im, int timestamp);

typedef struct
{
    RtpSession* 	psession;
    int             type;
    char			ip[20];
    short		    port;
    pthread_t	    tid;
    queue_t 		nalus;
    int			    queue_size;
    int             ts;
    cb_rtp_proc     cb_process;
    pthread_mutex_t lck;
	int state;
} ONE_RTP_SESSION;

enum
{
	RTP_IDLE=0,
	RTP_START,
	RTP_RUN,
	RTP_STOP
};
/********************************************************************************
@function:
	start_one_rtp_process: start one rtp process
@parameters:
    type:       0/sendonly, 1/recvonly, 2/sendrecv
    pserver:    server ip address string
    port:       server ip port
    PayloadType: payload type
    ts:         timestamp
    cb:         received data callback pointer
@return:
    NULL/error, ONE_RTP_SESSION instance pointer
********************************************************************************/
ONE_RTP_SESSION* start_one_rtp_process(int type, char* pserver, short port, PayloadType payload, int ts, cb_rtp_proc cb);

/********************************************************************************
@function:
	stop_one_rtp_process: stop one rtp process
@parameters:
    rtp_session:    instance pointer
@return:
	0/ok, -1/error
********************************************************************************/
int stop_one_rtp_process(ONE_RTP_SESSION* rtp_session);

/********************************************************************************
@function:
	send_one_rtp_pack: send one rtp packet
@parameters:
    rtp_session:    instance pointer
    buf:            data pointer
    len:            data length
    tick:           data timestamp
@return:
	0/ok, -1/error
********************************************************************************/
int send_one_rtp_pack(ONE_RTP_SESSION* rtp_session, char *buf, int len, int tick);

#endif


