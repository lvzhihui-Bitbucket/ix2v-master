#include "task_survey.h"

#include "obj_multi_timer.h"
#include "sys_msg_process.h"
#include "obj_business_manage.h"
#include "task_CallServer.h"
#include "vtk_udp_stack_c5_ipc_cmd.h"
#include "obj_VtkUnicastCommandInterface.h"

void MainCall_Break_Process(void);
void BeMainCall_Break_Process(void);
void IntercomCall_Break_Process(void);
void BeIntercomCall_Break_Process(void);
void InnerCall_Break_Process(void);
void BeInnerCall_Break_Process(void);
void SipCallTest_Break_Process(void);
void BeSipCall_Break_Process(void);
void MonitorDs_Break_Process(void);
void MonitorIpc_Break_Process(void);
void BeMonitor_Break_Process(void);
void SipWaitInput_Break_Process(void);
void MonitorQuad_Break_Process(void);

//czn_20190104_s
pthread_mutex_t Business_State_Lock = PTHREAD_MUTEX_INITIALIZER;
#define Business_Max_Busy_Time		600

#pragma pack(1)
typedef struct
{
	int state;
	int business_type;
	char partner_rm_num[11];
	time_t time;
}Business_Run_Stru;

typedef struct
{
	int op_code;		//0,free,1,hold
	char partner_rm_num[11];
}CallLink_Modify_Msg_T;
#pragma pack()

typedef struct
{
	int business_type;
	int level;
}Business_Level_Stru;

typedef struct
{
	int 	business_type;
	void (*callback)(void);
}Business_Break_Callback_Stru;

const Business_Level_Stru Business_Level_List[] =
{
	{Business_State_MainCall,0},
	{Business_State_BeMainCall,0},
	{Business_State_IntercomCall,1},
	{Business_State_BeIntercomCall,1},
	{Business_State_InnerCall,1},
	{Business_State_BeInnerCall,1},
	{Business_State_SipCallTest,1},
	{Business_State_BeSipCall,1},
	{Business_State_MonitorDs,2},
	{Business_State_MonitorIpc,2},
	{Business_State_BeMonitor,2},
	{Business_State_SipWaitInput,3},
	{Business_State_MonitorQuad,2},
};

const Business_Level_List_Length = sizeof(Business_Level_List)/sizeof(Business_Level_List[0]);

const Business_Break_Callback_Stru Business_Break_Callback_List[] =
{
	{Business_State_MainCall,MainCall_Break_Process},
	{Business_State_BeMainCall,BeMainCall_Break_Process},
	{Business_State_IntercomCall,IntercomCall_Break_Process},
	{Business_State_BeIntercomCall,BeIntercomCall_Break_Process},
	{Business_State_InnerCall,InnerCall_Break_Process},
	{Business_State_BeInnerCall,BeInnerCall_Break_Process},
	{Business_State_SipCallTest,SipCallTest_Break_Process},
	{Business_State_BeSipCall,BeSipCall_Break_Process},
	{Business_State_MonitorDs,MonitorDs_Break_Process},
	{Business_State_MonitorIpc,MonitorIpc_Break_Process},
	{Business_State_BeMonitor,BeMonitor_Break_Process},
	{Business_State_SipWaitInput,SipWaitInput_Break_Process},
	{Business_State_MonitorQuad,MonitorQuad_Break_Process},
};

const Business_Break_Callback_List_Length = sizeof(Business_Break_Callback_List)/sizeof(Business_Break_Callback_List[0]);
	
Business_Run_Stru Business_Run = {0};

int JustIfBreakCurBusiness(int business_type)// 0,fail,1,ok
{
	int i;
	int flag = 0;
	int cur_lev = 0,req_lev = 0;
	
	for(i = 0;i < Business_Level_List_Length;i++)
	{
		if(Business_Level_List[i].business_type == Business_Run.business_type)
		{
			cur_lev = Business_Level_List[i].level;
			flag |= 0x01;
		}
		
		if(Business_Level_List[i].business_type == business_type)
		{
			req_lev = Business_Level_List[i].level;
			flag |= 0x02;
		}
		
		if(flag == 0x03)
			break;
	}

	if(req_lev < cur_lev)
		return 1;

	return 0;
}

int JustIfBreakrRemoteBusiness(int remote_business_type,int business_type)// 0,fail,1,ok
{
	int i;
	int flag = 0;
	int cur_lev = 0,req_lev = 0;
	
	for(i = 0;i < Business_Level_List_Length;i++)
	{
		if(Business_Level_List[i].business_type == remote_business_type)
		{
			cur_lev = Business_Level_List[i].level;
			flag |= 0x01;
		}
		
		if(Business_Level_List[i].business_type == business_type)
		{
			req_lev = Business_Level_List[i].level;
			flag |= 0x02;
		}
		
		if(flag == 0x03)
			break;
	}

	if(req_lev < cur_lev)
		return 1;

	return 0;
}


void Business_Break_Process(void)
{
	int i;
	void (*break_callback)(void);
	
	for(i = 0,break_callback = NULL;i < Business_Break_Callback_List_Length;i++)
	{
		if(Business_Break_Callback_List[i].business_type == Business_Run.business_type)
		{
			break_callback = Business_Break_Callback_List[i].callback;
			break;
		}
	}
	
	if(break_callback != NULL)
		(*break_callback)();
}

int API_Business_Request(int business_type)	// 1 ok,0 fail	//czn_20190111
{
	time_t cur_time = time(NULL);
	bprintf("API_Business_Request type=%d\n", business_type);
	
	pthread_mutex_lock(&Business_State_Lock);
	
	if(Business_Run.state == Business_Idle )//||Business_Run.business_state == business_type)
	{
		Business_Run.business_type = business_type;
		Business_Run.state = Business_InUse;
		Business_Run.time = cur_time;
		pthread_mutex_unlock(&Business_State_Lock);
		return 1;
	}

	if(abs(cur_time - Business_Run.time)>Business_Max_Busy_Time)
	{
		Business_Break_Process();
		Business_Run.business_type = business_type;
		Business_Run.state = Business_InUse;
		Business_Run.time = cur_time;
		pthread_mutex_unlock(&Business_State_Lock);
		return 1;
	}
	if(JustIfBreakCurBusiness(business_type) == 1)
	{
		Business_Break_Process();
		Business_Run.business_type = business_type;
		Business_Run.state = Business_InUse;
		Business_Run.time = cur_time;
		pthread_mutex_unlock(&Business_State_Lock);
		return 1;
	}
	pthread_mutex_unlock(&Business_State_Lock);
	return 0;

}

void API_Business_Close(int business_type)
{
	pthread_mutex_lock(&Business_State_Lock);
	if(Business_Run.state !=Business_Hold && Business_Run.business_type == business_type)
	{
		Business_Run.state = Business_Idle;
	}
	pthread_mutex_unlock(&Business_State_Lock);
}

void API_Business_Reset(void)
{
	pthread_mutex_lock(&Business_State_Lock);
	
	Business_Run.state = Business_Idle;
	
	pthread_mutex_unlock(&Business_State_Lock);
}

void MainCall_Break_Process(void)
{
	//API_CallServer_LocalBye(CallServer_Run.call_type);
	usleep(1000000);
}
void BeMainCall_Break_Process(void)
{
	//API_CallServer_LocalBye(CallServer_Run.call_type);
	usleep(1000000);
}
void IntercomCall_Break_Process(void)
{
	//API_CallServer_LocalBye(CallServer_Run.call_type);
	usleep(1000000);
}
void BeIntercomCall_Break_Process(void)
{
	//API_CallServer_LocalBye(CallServer_Run.call_type);
	usleep(1000000);
}
void InnerCall_Break_Process(void)
{
	//API_CallServer_LocalBye(CallServer_Run.call_type);
	usleep(1000000);
}
void BeInnerCall_Break_Process(void)
{
	//API_CallServer_LocalBye(CallServer_Run.call_type);
	usleep(1000000);
}
void SipCallTest_Break_Process(void)
{
	//API_CallServer_RemoteBye(IxCallScene5,NULL);//API_CallServer_LocalBye(CallServer_Run.call_type);
	usleep(1000000);
}
void BeSipCall_Break_Process(void)
{
	//API_CallServer_LocalBye(CallServer_Run.call_type);
	usleep(1000000);
}
void MonitorDs_Break_Process(void)
{
	
}
void MonitorIpc_Break_Process(void)
{
	
}
void MonitorQuad_Break_Process(void)
{
}

int Get_Business_State(void)	
{
	return Business_Run.business_type;
}


void BeMonitor_Break_Process(void)
{
	;
}
void SipWaitInput_Break_Process(void)
{
	//linphone_becalled_dtlink_create();
}

int JustIsBusinessHold(void)
{
	if(Business_Run.state == Business_Hold)
		return 1;

	return 0;
}
void SetBusinessPartner(char *rm_num)
{
	pthread_mutex_lock(&Business_State_Lock);
	
	strcpy(Business_Run.partner_rm_num,rm_num);
	
	pthread_mutex_unlock(&Business_State_Lock);
}
void SetBusinessHold(char *partner)
{
	pthread_mutex_lock(&Business_State_Lock);
	//if(Business_Run.state == Business_InUse)
	if(memcmp(Business_Run.partner_rm_num,partner,10) == 0)
		Business_Run.state = Business_Hold;
	pthread_mutex_unlock(&Business_State_Lock);
}

void FreeBusinessHold(char *partner)
{
	pthread_mutex_lock(&Business_State_Lock);
	if(Business_Run.state == Business_Hold&&memcmp(Business_Run.partner_rm_num,partner,10) == 0)
	{
		Business_Run.state = Business_Idle;
	}
	pthread_mutex_unlock(&Business_State_Lock);
}

void Recv_CallLink_Req(int source_ip,int id)
{
	Business_Run_Stru Business_Run_temp;
	Business_Run_temp = Business_Run;
	//time_t cur_time = time(NULL);
	
	
		//if(Get_NoDisturbSetting()!=0)
	//	Business_Run_temp.state = Business_NoDisturb;
	//if(Business_Run.state != Business_Idle && abs(time(NULL) - Business_Run.time)>Business_Max_Busy_Time)
		Business_Run_temp.state = Business_Idle;
	
	api_udp_c5_ipc_send_rsp(source_ip,VTK_CMD_LINK_REP_1081,id,(char *)&Business_Run_temp,sizeof(Business_Run_Stru));
}

int Check_TargetDev_CallLink(int target_ip,int business_type)
{
	char link_type = 0;
	Business_Run_Stru target_link;
	
	int len = sizeof(Business_Run_Stru);
	
	
	if( api_udp_c5_ipc_send_req( target_ip,VTK_CMD_LINK_1001,(char*)&link_type,1, (char*)&target_link, &len) == -1 )
	{
		return -1;
	}

	if(target_link.state == Business_Idle)
		return 0;

	if(target_link.state == Business_NoDisturb)
		return -2;

	if(JustIfBreakrRemoteBusiness(target_link.business_type,business_type) == 1)
		return 0;
	else
		return -3;
}

int SendRemoteCallLinkHoldReq(int target_ip)
{
	CallLink_Modify_Msg_T send_msg;
	send_msg.op_code = 1;
	strcpy(send_msg.partner_rm_num,GetSysVerInfo_BdRmMs());

	api_udp_c5_ipc_send_data(target_ip,CMD_CALL_MON_LINK_MODIFY_REQ,(char*)&send_msg,sizeof(CallLink_Modify_Msg_T));
}

int SendRemoteCallLinkFreeReq(int target_ip)
{
	CallLink_Modify_Msg_T send_msg;
	send_msg.op_code = 0;
	strcpy(send_msg.partner_rm_num,GetSysVerInfo_BdRmMs());

	api_udp_c5_ipc_send_data(target_ip,CMD_CALL_MON_LINK_MODIFY_REQ,(char*)&send_msg,sizeof(CallLink_Modify_Msg_T));
}

void Recv_CallLinkMondifyReq(char *buff,int len)
{
	CallLink_Modify_Msg_T *pmsg = (CallLink_Modify_Msg_T *)buff;

	if(pmsg->op_code == 0)
	{
		FreeBusinessHold(pmsg->partner_rm_num);
	}
	else if(pmsg->op_code == 1)
	{
		SetBusinessHold(pmsg->partner_rm_num);
	}
}



//czn_20190104_e


