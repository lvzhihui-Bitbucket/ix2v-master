

#include "task_survey.h"
#include "define_file.h"
#include "elog.h"
#include "task_Event.h"
#include "task_Shell.h"
#include "obj_TableSurver.h"

Loop_vdp_common_buffer	vdp_controller_mesg_queue;
Loop_vdp_common_buffer	vdp_controller_sync_queue;
vdp_task_t				task_control = {.task_name = "survey"};

void survey_mesg_data_process(char* msg_data, int len);
void* survey_mesg_task( void* arg );

void vtk_TaskInit_survey( unsigned char priority )
{
	init_vdp_common_queue(&vdp_controller_mesg_queue, 2000, (msg_process)survey_mesg_data_process, &task_control);
	init_vdp_common_queue(&vdp_controller_sync_queue, 100, NULL, 								  &task_control);
	init_vdp_common_task(&task_control, MSG_ID_survey, survey_mesg_task, &vdp_controller_mesg_queue, &vdp_controller_sync_queue);
}

void vtk_TaskExit_survey(void)
{
	exit_vdp_common_queue(&vdp_controller_mesg_queue);
	exit_vdp_common_queue(&vdp_controller_sync_queue);
	exit_vdp_common_task(&task_control);
}

void* survey_mesg_task( void* arg )
{
	vdp_task_t*	 ptask 			= (vdp_task_t*)arg;
	p_vdp_common_buffer pdb 	= 0;
	int	size;
	LoadSearchTimeoutParameter();
	card_tb_load();
	CardStatisticsInit();
	TableStatisticsInit(TB_NAME_HYBRID_MAT);
	TableStatisticsInit(TB_NAME_HYBRID_SAT);	
	TableStatisticsInit(TB_NAME_APT_MAT);	
	TableStatisticsInit(TB_NAME_VM_MAT);	
	SatHybridCheckStatisticsInit();
	AptMatCheckStatisticsInit();
	DtImCheckStatisticsInit();
	ptask->task_StartCompleted = 1;
	while( ptask->task_run_flag )
	{
		size = pop_vdp_common_queue(ptask->p_msg_buf, &pdb, VDP_QUEUE_POLLING_TIME);
		if( size > 0 )
		{
			(*ptask->p_msg_buf->process)(pdb,size);
			purge_vdp_common_queue( ptask->p_msg_buf );
		}
	}
	return 0;
}

void survey_mesg_data_process(char* msg_data,int len)
{
	VDP_MSG_HEAD* pVdpUdp = (VDP_MSG_HEAD*)msg_data;

	// �ж��Ƿ�Ϊϵͳ����
	if( pVdpUdp->msg_target_id == MSG_ID_survey )
	{
		survey_sys_message_processing(msg_data,len);
	}
	else
	{
		vdp_task_t* pVdpTask = GetTaskAccordingMsgID(pVdpUdp->msg_target_id);
		if( pVdpTask != NULL )
		{
			push_vdp_common_queue(pVdpTask->p_msg_buf,msg_data,len);
		}
	}
}

// ����: �ȴ�ҵ��ͬ��Ӧ��
// ����: pBusiness - ͬ���ȴ�ʱ�Ĺ���Ķ��У�business_type - �ȴ���ҵ�����ͣ� data - �õ������ݣ�plen - ������Ч����
// ����: 0 - ���س�ʱ��1 -  ��������
int WaitForBusinessACK( p_Loop_vdp_common_buffer pBusiness, unsigned char business_type,  char* data, int* plen, int timeout )
{
	VDP_MSG_HEAD* pbusiness_buf;
	p_vdp_common_buffer pdb = 0;
	int size;
	vdp_task_t*	powner;	
	
	int ret = 0;
	
	size = pop_vdp_common_queue( pBusiness,&pdb,timeout);
	if( size > 0  )
	{		
		pbusiness_buf = (VDP_MSG_HEAD*)pdb;

		powner = (vdp_task_t*)pBusiness->powner;
			
		if( pbusiness_buf->msg_type  == (business_type|COMMON_RESPONSE_BIT) )
		{				
			// �õ�����
			*plen = size;
			memcpy( data, pdb, size );
			//dprintf("task %d wait ack ok!\n",powner->task_id);
			ret = 1;
		}

		purge_vdp_common_queue( pBusiness );
	}
	return ret;
}

vdp_task_t* GetTaskAccordingMsgID(unsigned char msg_id)
{
	vdp_task_t* ptaskobj = NULL;
	
	switch( msg_id )
	{
		case MSG_ID_CallServer:
			ptaskobj = &task_callserver;				
			break;
		#if 0
		case MSG_ID_IpBeCalled:
			ptaskobj = &task_ipbecalled;				
			break;	
		case MSG_ID_IpCalller:
			ptaskobj = &task_ipcaller;				
			break;
		#endif
		
		case MSG_ID_DtCalller:
			ptaskobj = &task_dtcaller;				
			break;
			
		case MSG_ID_DEBUG_SBU:
			//ptaskobj = &task_debug_sbu;
			break;
		case MSG_ID_SUNDRY:
			ptaskobj = &task_sundry;				
			break;
		case MSG_ID_survey:
			ptaskobj = &task_control; 			
			break;
		case MSG_ID_Audio:
			ptaskobj = &task_audio;				
			break;
		case MSG_ID_StackAI:
			ptaskobj = &task_StackAI;				
			break;	

		case MSG_ID_SipCalller:
			ptaskobj = &task_sipcaller;	

		case MSG_ID_DT_CheckOnline:
			ptaskobj = &task_DT_CheckOnline;
			break;
		case MSG_ID_EVENT:
			ptaskobj = &eventRun.task;
			break;
		case MSG_ID_EVENT_LOOP:
			ptaskobj = &eventRun.loopTask;
			break;
		case MSG_ID_VideoBusiness:
			ptaskobj = &task_vbu;
			break;	
	}	
	return ptaskobj;
}

const vdp_task_t* 	task_tab[] =
{
	&task_control,
	&task_callserver,		
	&task_dtcaller,
	&task_sundry,
	&task_audio,
	&task_StackAI,

	&task_sipcaller,
	&task_DT_CheckOnline,
	&eventRun.task,
	&eventRun.loopTask,
	&task_vbu,
	&shellRun.task,
	NULL,
};

int GetMsgIDAccordingPid( pthread_t pid )
{
	int i;
	for( i = 0; ; i++ )
	{
		if( task_tab[i] == NULL )		//czn_20161008	
			return 0;
		if( task_tab[i]->task_pid == pid )
		{
			//dprintf("get pid=%lu msg id=%d\n",task_tab[i]->task_pid,task_tab[i]->task_id);
			return task_tab[i]->task_id;
		}
	}
	return 0;
}

vdp_task_t* OS_GetTaskID(void)
{
	int i;
	int pid = pthread_self();

	for( i = 0; ; i++ )
	{
		if( task_tab[i] == NULL )
		{
			//log_w("Task table has no pid:%d", pid);
			return NULL;
		}

		if( task_tab[i]->task_pid == pid)
		{
			return task_tab[i];
		}
	}
}

const char* OS_GetTaskName(void)
{
	int i;
	for( i = 0; ; i++ )
	{
		if( task_tab[i] == NULL )
		{
			//log_e("Task table has no pid:%d", pthread_self());
			return NULL;
		}

		if( task_tab[i]->task_pid == pthread_self())
		{
			return task_tab[i]->task_name;
		}
	}
}

const vdp_task_t* 	task_StartTab[] =
{
	&task_vbu,
	&task_control,
	&task_callserver,
	&task_sipcaller,		
	&task_sundry,
	&task_audio,
	&eventRun.task,
	&task_menu,
	NULL,
};

int OS_GetIfTaskStartCompleted(void)
{
	int i;
	for(i = 0; task_StartTab[i]; i++)
	{
		if(task_StartTab[i] == NULL)
			break;
		if(!task_StartTab[i]->task_StartCompleted)
		{
			dprintf("task_name:%s, task_id = %d is not start completed.\n", task_StartTab[i]->task_name? task_StartTab[i]->task_name : "NULL", task_StartTab[i]->task_id);
			return 0;
		}
	}

	dprintf("Start Completed\n");
	return 1;
}



OS_TASK_EVENT OS_WaitSingleEventTimed(OS_TASK_EVENT event, int time)
{
	p_vdp_common_buffer pdb = 0;
	OS_TASK_EVENT ret = 0x00;
	vdp_task_t* task = OS_GetTaskID();
	int size;

	if(task && task->p_syc_buf)
	{
		size = pop_vdp_common_queue(task->p_syc_buf,&pdb, time);
		if( size > 0)
		{		
			if(*pdb & event)
			{
				ret =  *pdb;
			}
			purge_vdp_common_queue(task->p_syc_buf);
		}
	}
	else
	{
		//usleep(1000*time);
	}
	
	return ret;
}

OS_TASK_EVENT OS_WaitSingleEventTimed2(OS_TASK_EVENT event, int time)
{
	p_vdp_common_buffer pdb = 0;
	OS_TASK_EVENT ret = 0x00;
	vdp_task_t* task = OS_GetTaskID();
	int timeUs;
	int size;

	if(task && task->p_syc_buf)
	{
		for(timeUs = 0; timeUs < time || time == 0; timeUs += 10)
		{
			size = pop_vdp_common_queue(task->p_syc_buf,&pdb, 1);
			if( size > 0)
			{		
				if(*pdb & event)
				{
					ret =  *pdb;
					break;
				}
				purge_vdp_common_queue(task->p_syc_buf);
			}
			usleep(9*1000);
		}
	}
	
	return ret;
}

// 0 --OK/ 1 --ERR
int OS_SignalEvent(OS_TASK_EVENT event, vdp_task_t* taskTcb)
{
	return push_vdp_common_queue(taskTcb->p_syc_buf, (char*)&event, 1);
}

/****************************************************************************************************************************
 * @fn:		API_add_message_to_suvey_queue
 *
 * @brief:	������Ϣ���ַ��������
 *
 * @param:  pdata 			- ����ָ��
 * @param:  len 			- ���ݳ���
 *
 * @return: 0/ok, 1/full
****************************************************************************************************************************/
int	API_add_message_to_suvey_queue( char* pdata, unsigned int len )
{
	if(task_control.task_run_flag == 0)
	{
		return -1;
	}
	return push_vdp_common_queue(task_control.p_msg_buf, pdata, len);
}

int API_one_message_to_suvey_queue( int msg, int sub_msg )
{
	VDP_MSG_HEAD	head;
	head.msg_source_id	= MSG_ID_survey;
	head.msg_target_id	= MSG_ID_survey;
	head.msg_type		= msg;
	head.msg_sub_type	= sub_msg;	
	return push_vdp_common_queue(task_control.p_msg_buf, &head, sizeof(VDP_MSG_HEAD) );
}



