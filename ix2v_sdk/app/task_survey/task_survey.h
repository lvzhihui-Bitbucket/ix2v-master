
#ifndef _VDP_CONTROLLER_H
#define _VDP_CONTROLLER_H

#include "utility.h"
#include "unix_socket.h"

typedef	unsigned char OS_TASK_EVENT;

#define		VDP_DAT_BUF_LEN					800
#define 	VDP_THREAD_CYCLE_TIME			600
#define 	VDP_QUEUE_POLLING_TIME			0  //1000		// ����polling�ĵȴ�ʱ��

//��ϢĿ�������(0-127)��
#define MSG_ID_hal				1		// hal����
#define MSG_ID_SUNDRY			2		// Ӳ����������
#define MSG_ID_survey			10		// ��Ϣ�ַ���������
#define MSG_ID_Menu				11		// �˵����������Ϣ
#define MSG_ID_IOServer			12		// IO server
#define MSG_ID_udp_stack		13		// udp stack
#define MSG_ID_GOServer			14		// GO server
#define MSG_ID_WiFi				15		//WIFI
#define MSG_ID_DT_CheckOnline	16		//检测DT设备在线

#define MSG_ID_StackAI			20		// StackAI
#define MSG_ID_DHCP				21		// DHCP
#define MSG_ID_UART				22		// UART
//#define MSG_ID_CACHE			23		// CACHE
#define MSG_ID_IX_PROXY			24		// IX_Builder����
#define MSG_ID_IX_PROXY_CLIENT	25		// IX_Builder����
#define MSG_ID_SHELL			26		//SHELL
#define MSG_ID_EVENT			27		//Event
#define MSG_ID_EVENT_LOOP		28		//Event_LOOP
#define MSG_ID_MDS				29		//MDS
#define MSG_ID_VideoBusiness		30
#define MSG_ID_XD2				31		

#define MSG_ID_DEBUG_SBU		120		// ���Է���

//czn_20160418_s
//czn_20170518_s
#define MSG_ID_CALLER			112		// phone������Ϣ
#define MSG_ID_BECALLED			113		// phone������

#define MSG_ID_CallServer		111		// 
#define MSG_ID_IpCalller			112		// 
#define MSG_ID_IpBeCalled		113
#define MSG_ID_DtCalller			114		// 
#define MSG_ID_DtBeCalled		115
//czn_20170518_e
#define MSG_ID_LocalMonitor		116



#define MSG_ID_CALL_LIST		150		//cao_20160429

#define MSG_ID_RSUPDATER		121			//czn_20170214

#define MSG_ID_OtaClientIfc		122			//czn_20181219

#define MSG_ID_Autotest			123			//czn_20190412

#define MSG_ID_ResSync			124			//czn_20190520

#define MSG_ID_ListUpdate		125

#define MSG_ID_VtkMediaTrans	126			//czn_20190701

#define MSG_ID_IPC_BUSINESS		127
#define MSG_ID_DS_MONITOR		128
#define MSG_ID_Audio			129

#define MSG_ID_SipCalller			130		// 

#define MSG_ID_video_reliability			131		// 

#define CALL_SURVEY_IDLE			0
#define CALL_SURVEY_ASCALLER		1
#define CALL_SURVEY_ASBECALLED		2
//lzh_20160503_s
#define CALL_SURVEY_MONITOR			0		//czn_20161220
//lzh_20160503_s

extern unsigned char Call_Survey_State;
//czn_20160418_e

typedef struct
{
	// ��Ϣ����ͷ
	unsigned char 	msg_target_id;		// ��Ϣ��Ŀ�����
	unsigned char 	msg_source_id;		// ��Ϣ��Դ����
	unsigned char 	msg_type;			// ��Ϣ������
	unsigned char  	msg_sub_type;		 //��Ϣ��������
} VDP_MSG_HEAD;

typedef struct 
{
	VDP_MSG_HEAD	head;
	int				target_ip;
	unsigned short 	node_id;		//czn_20170328
	int 			cmd;
	int 			id;	
	int 			len;
	char 			pbuf[VDP_DAT_BUF_LEN];
} UDP_MSG_TYPE;
//czn_20170518_s
#define DATA_MAX	40

typedef struct 
{
	VDP_MSG_HEAD	head;
	uint16	address;						//?????
	uint16	target_address;					//????
	uint8	command_mode;					//????:  0=???,1=???
	uint8	command_type;					//????
	uint8	data_len;						//????
	uint8	data_buf[DATA_MAX];
} DTSTACK_MSG_TYPE;
//czn_20170518_e
typedef struct 
{
	VDP_MSG_HEAD	head;
	int 			wparam;
	int 			lparam;
} SYS_BRD_MSG_TYPE;

void vtk_TaskInit_survey( unsigned char priority );

extern vdp_task_t	task_control;
extern vdp_task_t	task_sundry;
extern vdp_task_t task_audio;
extern vdp_task_t task_callserver;
extern vdp_task_t task_ipbecalled;
extern vdp_task_t task_ipcaller;
extern vdp_task_t task_dtcaller;
extern vdp_task_t task_StackAI;
extern vdp_task_t task_sipcaller;
extern vdp_task_t task_DT_CheckOnline;
extern vdp_task_t task_menu;
extern vdp_task_t task_vbu;


vdp_task_t* GetTaskAccordingMsgID(unsigned char msg_id);
int GetMsgIDAccordingPid( pthread_t pid );
vdp_task_t* OS_GetTaskID(void);

OS_TASK_EVENT OS_WaitSingleEventTimed(OS_TASK_EVENT event, int time);
#define OS_WaitSingleEvent(event)				OS_WaitSingleEventTimed(event, 0)
#define OS_WaitEventTimed(event, time)			OS_WaitSingleEventTimed(event, time)

// ����: �ȴ�ҵ��ͬ��Ӧ��
// ����: pBusiness - ͬ���ȴ�ʱ�Ĺ���Ķ��У�business_type - �ȴ���ҵ�����ͣ� data - �õ������ݣ�plen - ������Ч����
// ����: 0 - ���س�ʱ��1 -  ��������
int WaitForBusinessACK( p_Loop_vdp_common_buffer pBusiness, unsigned char business_type,  char* data, int* plen, int timeout );


/****************************************************************************************************************************
 * @fn:		API_add_message_to_suvey_queue
 *
 * @brief:	������Ϣ���ַ��������
 *
 * @param:  pdata 			- ����ָ��
 * @param:  len 			- ���ݳ���
 *
 * @return: 0/ok, 1/full
****************************************************************************************************************************/
int	API_add_message_to_suvey_queue( char* pdata, unsigned int len );

int API_one_message_to_suvey_queue( int msg, int sub_msg );

#define VIDEO_SEVICE_NOTIFY_REMOTE_ON()		API_one_message_to_suvey_queue(SURVEY_MSG_VIDEO_SERVICE_ON,0)
#define VIDEO_SEVICE_NOTIFY_REMOTE_OFF()	API_one_message_to_suvey_queue(SURVEY_MSG_VIDEO_SERVICE_OFF,0)
#define VIDEO_CLIENT_NOTIFY_REMOTE_ON()		API_one_message_to_suvey_queue(SURVEY_MSG_VIDEO_CLIENT_ON,0)
#define VIDEO_CLIENT_NOTIFY_REMOTE_OFF()		API_one_message_to_suvey_queue(SURVEY_MSG_VIDEO_CLIENT_OFF,0)
#define VIDEO_CLIENT_NOTIFY_REMOTE_BEOFF()		API_one_message_to_suvey_queue(SURVEY_MSG_VIDEO_CLIENT_BEOFF,0)

#endif

