
#include "task_survey.h"
#include "task_Sundry.h"
#include "sys_msg_process.h"
#include "vtk_udp_stack_c5_ipc_cmd.h"
#include "define_Command.h"

/****************************************************************************************************************************
 * @fn:		survey_sys_message_processing
 *
 * @brief:	公共服务消息处理
 *
 * @param:  pdata 			- 数据指针
 * @param:  len 			- 数据长度
 *
 * @return: 0/ok, 1/full
****************************************************************************************************************************/
int	survey_sys_message_processing( char* pdata, int len )
{
	VDP_MSG_HEAD* 	pMsg 		= (VDP_MSG_HEAD*)pdata;			// 内部消息头
	UDP_MSG_TYPE*	pUdpType	= (UDP_MSG_TYPE*)pdata;
	DTSTACK_MSG_TYPE* dtStackMsg = (UDP_MSG_TYPE*)pdata;;
	
	// 处理hal的相关命令
	switch( pMsg->msg_source_id )
	{
		case MSG_ID_udp_stack:
			UdpSurvey_Filter_Prcessing(pUdpType);
			API_VtkUnicastCmd_Analyzer(pUdpType->node_id,pUdpType->target_ip,pUdpType->cmd,pUdpType->id,pUdpType->pbuf,pUdpType->len);
			
			break;
		case MSG_ID_StackAI:		//czn_20170518
			if(dtStackMsg->command_mode == MODE_NEW_COM)
			{
				NewCommand_Process((DTSTACK_MSG_TYPE*)pdata);
			}
			else
			{
				DtCommand_Analyzer((DTSTACK_MSG_TYPE*)pdata);
			}
			break;
		default:
			break;
	}			
}

int UdpSurvey_Filter_Prcessing( UDP_MSG_TYPE *pUdpType )
{
			//cao_20170304
	switch( pUdpType->cmd )
	{
		case CMD_DEVIC_SINGLE_LINKING_REQ:
			DeviceManageSingleLinking(pUdpType);
			break;
		//多个设备在线检测开始
		case CMD_DEVIC_MULTIPLE_LINKING_START_REQ:
			DeviceManageMultipleLinkingStart(pUdpType);
			break;			
		//设备重启
		case CMD_DEVIC_REBOOT_REQ:
			DeviceManageRebootProcess(pUdpType);
			break;
			
		//设备恢复出厂设置
		case CMD_DEVIC_RECOVER_REQ:
			DeviceManageRecover(pUdpType);
			break;
		case CMD_CALL_MON_LINK_REQ:
			//recv_ip_mon_link_req(pUdpType);
			if(pUdpType->pbuf[0]==0x34)//if( pMonLink->mon_type== GlMonMr )
			{
				bprintf("Get_mon_Link_request\n");
				if( recv_ip_mon_link_req(pUdpType) == 0 )
				{
					bprintf("recv_ip_mon_link_req, send rsp ok!\n");
				}
			}
			else
			{					
				//Get_Call_Link_Respones(pUdpType);	
				Recv_CallLink_Req(pUdpType->target_ip,pUdpType->id);	//czn_20190527
			}
			break;
			
		case CMD_IPERF_START_REQ:
			response_iperf_client_start((void*)pUdpType);
			break;
	}

}

