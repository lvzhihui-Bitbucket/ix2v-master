
#include "vtk_udp_stack_device_update.h"
#include "task_Shell.h"
#include "obj_MulCtrlBySipAcc.h"
#include "obj_IoInterface.h"
#include "elog.h"

device_update_instance_t		one_device_update_ins;
Loop_vdp_common_buffer			device_update_msg_queue;
pthread_t						device_update_process_pid;

device_update_instance_t		wlan_one_device_update_ins;
Loop_vdp_common_buffer			wlan_device_update_msg_queue;
pthread_t						wlan_device_update_process_pid;


extern int send_one_udp_data( int sock_fd, struct sockaddr_in sock_target_addr, char *data, int length);
extern int push_udp_common_queue( p_loop_udp_common_buffer pobj, p_udp_common_buffer data, unsigned int length);		//czn_20170712
extern int pop_udp_common_queue(p_loop_udp_common_buffer pobj, p_udp_common_buffer* pdb, int timeout);
extern int purge_udp_common_queue(p_loop_udp_common_buffer pobj);

int wlan_device_update_udp_recv_anaylasis( char* buf, int len);
int lan_device_update_udp_recv_anaylasis( char* buf, int len);

void* 		device_update_msg_process( void* arg );
void* 		wlan_device_update_msg_process(void* arg );

int 		device_update_inner_recv_anaylasis( char* buf, int len );



// udp通信初始化
int init_device_update_instance( void )
{
	
	init_one_udp_comm_rt_buff( 		&one_device_update_ins.udp, 15000,	lan_device_update_udp_recv_anaylasis, 15000, NULL );	//czn_20170712
	init_one_udp_comm_rt_type_ext( 	&one_device_update_ins.udp, "device_update eth0", UDP_RT_TYPE_MULTICAST,DEVICE_SEARCH_UDP_BOARDCAST_PORT, DEVICE_SEARCH_UDP_BOARDCAST_PORT,DEVICE_SEARCH_MULTICAST_ADDR, NET_ETH0);

	// init business rsp wait array
	init_one_send_array(&one_device_update_ins.waitrsp_array);
	
 	init_vdp_common_queue(&device_update_msg_queue,100,(msg_process)device_update_inner_recv_anaylasis,NULL);	
 	if( pthread_create(&device_update_process_pid, 0, device_update_msg_process, &device_update_msg_queue) )
	{
		dev_update_printf("Create task thread Failure,%s\n", strerror(errno));
	}

	SetSocketRcvBufferSize(one_device_update_ins.udp.sock_rcv_fd, 600*1024);

	dev_update_printf("eth0 init_one_device_update ok!...............\n");	
	
	//wlan0
	init_one_udp_comm_rt_buff( 		&wlan_one_device_update_ins.udp, 15000,	wlan_device_update_udp_recv_anaylasis, 30000, NULL );	//czn_20170712
	init_one_udp_comm_rt_type_ext( 	&wlan_one_device_update_ins.udp, "device_update wlan0", UDP_RT_TYPE_MULTICAST,DEVICE_SEARCH_UDP_BOARDCAST_PORT, DEVICE_SEARCH_UDP_BOARDCAST_PORT,DEVICE_SEARCH_MULTICAST_ADDR, NET_WLAN0);

	// init business rsp wait array
	init_one_send_array(&wlan_one_device_update_ins.waitrsp_array);
	
 	init_vdp_common_queue(&wlan_device_update_msg_queue,100,(msg_process)device_update_inner_recv_anaylasis,NULL);	
 	if( pthread_create(&wlan_device_update_process_pid, 0, wlan_device_update_msg_process, &wlan_device_update_msg_queue) )
	{
		dev_update_printf("Create task thread Failure,%s\n", strerror(errno));
	}

	//SetSocketRcvBufferSize(wlan_one_device_update_ins.udp.sock_rcv_fd, 1200*1024);

	dev_update_printf("wlan0 init_one_device_update ok!...............\n");	

	return 0;
}

// lzh_20171008_s
int rejoin_multicast_group(void)
{
	lan_rejoin_multicast_group();
	wlan_rejoin_multicast_group();
	return 0;
}
// lzh_20171008_e

int lan_rejoin_multicast_group(void)
{
	int ret;
	//if(!CheckDeviceType("DH"))
	{
		leave_multicast_group(one_device_update_ins.udp.net_device_name, one_device_update_ins.udp.sock_rcv_fd, inet_addr(DEVICE_SEARCH_MULTICAST_ADDR) );
		usleep(10*1000);	
	}
	ret = join_multicast_group(one_device_update_ins.udp.net_device_name, one_device_update_ins.udp.sock_rcv_fd, inet_addr(DEVICE_SEARCH_MULTICAST_ADDR) );

	return ret;
}

int wlan_rejoin_multicast_group(void)
{
	int ret;
	
	leave_multicast_group(wlan_one_device_update_ins.udp.net_device_name, wlan_one_device_update_ins.udp.sock_rcv_fd, inet_addr(DEVICE_SEARCH_MULTICAST_ADDR) );
	usleep(10*1000);	
	ret = join_multicast_group(wlan_one_device_update_ins.udp.net_device_name, wlan_one_device_update_ins.udp.sock_rcv_fd, inet_addr(DEVICE_SEARCH_MULTICAST_ADDR) );

	return ret;
}


#define DEVICE_UPDATE_POLLING_MS		100
#define DEVICE_UPDATE_POLLING_SHORT_MS	5
void* device_update_msg_process(void* arg )
{
	p_Loop_vdp_common_buffer	pmsgqueue 	= (p_Loop_vdp_common_buffer)arg;
	p_vdp_common_buffer pdb 	= 0;
	int pollingTime = DEVICE_UPDATE_POLLING_MS;

	while( 1 )
	{	
		int size;
		size = pop_vdp_common_queue( pmsgqueue,&pdb,pollingTime );
		if( size > 0 )
		{
			(*pmsgqueue->process)(pdb,size);
			purge_vdp_common_queue( pmsgqueue );
			pollingTime = DEVICE_UPDATE_POLLING_MS;
		}
		else
		{
			pollingTime = DEVICE_UPDATE_POLLING_SHORT_MS;
		}
		// 100ms定时查询
		poll_all_business_recv_array( &one_device_update_ins.waitrsp_array, pollingTime);
	}	
	return 0;	
}

void* wlan_device_update_msg_process(void* arg )
{
	p_Loop_vdp_common_buffer	pmsgqueue 	= (p_Loop_vdp_common_buffer)arg;
	p_vdp_common_buffer pdb 	= 0;
	int pollingTime = DEVICE_UPDATE_POLLING_MS;

	while( 1 )
	{	
		int size;
		size = pop_vdp_common_queue( pmsgqueue,&pdb,pollingTime );
		if( size > 0 )
		{
			(*pmsgqueue->process)(pdb,size);
			purge_vdp_common_queue( pmsgqueue );
			pollingTime = DEVICE_UPDATE_POLLING_MS;
		}
		else
		{
			pollingTime = DEVICE_UPDATE_POLLING_SHORT_MS;
		}
		// 100ms定时查询
		poll_all_business_recv_array( &wlan_one_device_update_ins.waitrsp_array, pollingTime );
	}	
	return 0;	
}

// inner接收命令业务分析
int device_update_inner_recv_anaylasis( char* buf, int len )
{
	return 0;
}

int device_update_udp_recv_anaylasis_cnt = 0;
// udp接收命令
int device_update_udp_recv_anaylasis(char* netDeviceName, char* buf, int len)
{	
	ext_pack_buf *pbuf = (ext_pack_buf*)buf;

	device_search_package	*package	= (device_search_package*)pbuf->dat;
	int						dat_len		= len - 8 - sizeof(device_search_head)-sizeof(device_search_cmd);
	//bprintf("222222:%d\n",len);
	if( len>=6&&!memcmp(&package->head, "SWITCH", 6))
	{		
		api_udp_device_update_recv_callback(netDeviceName, pbuf->ip, htons(package->cmd.cmd), package->buf.dat,dat_len);
	}
	else
	{
		if(!GetShellTaskState())
		{
			API_ShellCommand(pbuf->ip, SourceUDP, "task -o");
		}

		int stringLen = 0;
		char datStr[IP8210_BUFFER_MAX+1];
		stringLen = (len-8 > IP8210_BUFFER_MAX ? IP8210_BUFFER_MAX : len-8);
		memcpy(datStr, pbuf->dat, stringLen);
		datStr[stringLen] = 0;
		API_ShellCommand(pbuf->ip, SourceUDP, datStr);
	}	

	return 0;
}

int wlan_device_update_udp_recv_anaylasis( char* buf, int len)
{	
	ext_pack_buf *pbuf = (ext_pack_buf*)buf;
	device_search_package	*package	= (device_search_package*)pbuf->dat;
	if( len>=6&&!memcmp(&package->head, "SWITCH", 6))
	{
		return 0;
	}
	return device_update_udp_recv_anaylasis(NET_WLAN0, buf, len);
}

int lan_device_update_udp_recv_anaylasis( char* buf, int len)
{	
	return device_update_udp_recv_anaylasis(NET_ETH0, buf, len);
}

// 通过udp:25000端口发送数据，不等待业务应答
int api_udp_device_update_send_data(int target_ip, unsigned short cmd, const char* pbuf, unsigned int len )
{
	device_update_instance_t*		pOne_device_update_ins;
		
	ext_pack_buf			send_pack;
	device_search_package*	psend_pack = (device_search_package*)send_pack.dat;	

	if(target_ip == 0 || target_ip == -1)
	{
		return -1;
	}

	char ipChar[16];
	char *netDeviceName;

	// initial head
	psend_pack->head.s 	= 'S';
	psend_pack->head.w 	= 'W';
	psend_pack->head.i 	= 'I';
	psend_pack->head.t 	= 'T';
	psend_pack->head.c 	= 'C';
	psend_pack->head.h 	= 'H';
	// cmd
	psend_pack->cmd.cmd	= cmd;
	// dat
	memcpy( psend_pack->buf.dat, pbuf, len );
	// len
	send_pack.len	= sizeof(device_search_head) + sizeof(device_search_cmd) + len;

	// ip
	send_pack.ip	= target_ip;

	netDeviceName = GetNetDeviceNameByTargetIp(target_ip);

	// 加入发送队列
	if(target_ip == inet_addr(DEVICE_SEARCH_MULTICAST_ADDR))
	{
		if(GetNetMode() == NM_MODE_LAN)
		{
			//lan_rejoin_multicast_group();
			pOne_device_update_ins = &one_device_update_ins;
		}
		else
		{
			//wlan_rejoin_multicast_group();
			pOne_device_update_ins = &wlan_one_device_update_ins;
		}
	}
	else if(!strcmp(netDeviceName, NET_WLAN0))
	{
		pOne_device_update_ins = &wlan_one_device_update_ins;
	}
	else if(!strcmp(netDeviceName, NET_ETH0))
	{
		pOne_device_update_ins = &one_device_update_ins;
	}

	push_udp_common_queue( &(pOne_device_update_ins->udp.tmsg_buf), (p_udp_common_buffer)&send_pack, sizeof(ext_pack_buf)-IP8210_BUFFER_MAX+send_pack.len );

	return 0;
}

//0成功，非零失败
int api_udp_device_update_send_data_by_device(char* net_device_name, int target_ip, unsigned short cmd, const char* pbuf, unsigned int len )
{
	ext_pack_buf			send_pack;
	device_search_package*	psend_pack = (device_search_package*)send_pack.dat;	
	int ret;
	char ipChar[16];

	if(target_ip == 0 || target_ip == -1)
	{
		return -1;
	}

	// initial head
	psend_pack->head.s 	= 'S';
	psend_pack->head.w 	= 'W';
	psend_pack->head.i 	= 'I';
	psend_pack->head.t 	= 'T';
	psend_pack->head.c 	= 'C';
	psend_pack->head.h 	= 'H';
	// cmd
	psend_pack->cmd.cmd	= cmd;
	// dat
	memcpy( psend_pack->buf.dat, pbuf, len );
	// len
	send_pack.len	= sizeof(device_search_head) + sizeof(device_search_cmd) + len;

	// ip
	send_pack.ip	= target_ip;

	// 加入发送队列
	if(net_device_name && !strcmp(net_device_name, NET_WLAN0))
	{
		//printf( "api_udp_device_update_send_data_by_device device=%s, ip = %s, len=%d, cmd=0x%04X\n", NET_WLAN0, my_inet_ntoa(target_ip, ipChar), len, cmd);
		ret = push_udp_common_queue( &wlan_one_device_update_ins.udp.tmsg_buf, (p_udp_common_buffer)&send_pack, sizeof(ext_pack_buf)-IP8210_BUFFER_MAX+send_pack.len );
	}
	else
	{
		//printf( "api_udp_device_update_send_data_by_device device=%s, ip = %s, len=%d, cmd=0x%04X\n", NET_ETH0, my_inet_ntoa(target_ip, ipChar), len, cmd);
		ret = push_udp_common_queue( &one_device_update_ins.udp.tmsg_buf, (p_udp_common_buffer)&send_pack, sizeof(ext_pack_buf)-IP8210_BUFFER_MAX+send_pack.len );
	}

	return ret;
}

// 通过udp:25000端口发送数据，不等待业务应答
int api_udp_device_update_send_data2(int target_ip, unsigned short cmd, const char* pbuf, unsigned int len )
{
	IXReq_T* pReqData;
	
	pReqData = (IXReq_T*)pbuf;
	
	if(target_ip == 0 || target_ip == -1)
	{
		return -1;
	}

	if(target_ip == inet_addr(DEVICE_SEARCH_MULTICAST_ADDR))
	{
		if(GetNetMode() == NM_MODE_LAN)
		{
			pReqData->sourceIp = inet_addr(GetSysVerInfo_IP_by_device(NET_ETH0));
		}
		else
		{
			pReqData->sourceIp = inet_addr(GetSysVerInfo_IP_by_device(NET_WLAN0));
		}
	}
	else
	{
		pReqData->sourceIp = inet_addr(GetSysVerInfo_IP_by_device(GetNetDeviceNameByTargetIp(target_ip)));
	}
	api_udp_device_update_send_data(target_ip, cmd,  pbuf, len);
	return 0;
}


// 通过udp:25007端口发送数据包后，并等待业务应答，得到业务应答的数据
int api_udp_device_update_send_req(int target_ip, unsigned short cmd, char* psbuf, int slen , char *prbuf, unsigned int *prlen)
{	
	return 0;
}

// 接收到udp:25007端口数据包后给出的业务应答
int api_udp_device_update_send_rsp( int target_ip, unsigned short cmd, int id, const char* pbuf, unsigned int len )
{	
	return 0;
}


// 接收到udp:25007端口数据包的回调函数
int api_udp_device_update_recv_callback(char* netDeviceName, int target_ip, unsigned short cmd, char* pbuf, unsigned int len )
{
	static int saveLen;
	static int saveTarget_ip;
	static int saveCmd;
	static char saveBuf[1024] = {0};
	static long long saveLastTime = 0;
	long long currentTime;

	currentTime = time_since_last_call(-1);
	//PrintCurrentTime(11111);
	log_d("recv udp port 25007, ip=%s, len=%d, cmd=0x%04X\n", my_inet_ntoa2(target_ip), len, cmd);
	//printf("recv udp port 25007, ip=%s, len=%d, cmd=0x%04X\n", my_inet_ntoa2(target_ip), len, cmd);
	
	MyElogHex(ELOG_LVL_DEBUG, pbuf, len);

	//100毫秒内，相同包不处理
	if(saveLen == len && saveTarget_ip == target_ip && saveCmd == cmd && !memcmp(saveBuf, pbuf, len > 1024 ? 1024 : len) && currentTime - saveLastTime < 100* 1000)
	{
		return 0;
	}

	saveLen = len;
	saveTarget_ip = target_ip;
	saveCmd = cmd;
	saveLastTime = currentTime;
	memcpy(saveBuf, pbuf, len > 1024 ? 1024 : len);

	#if 0
	char *netDeviceName;
	netDeviceName = GetNetDeviceNameByTargetIp(target_ip);
	if(strcmp(netDeviceName,NET_WLAN0)==0)
		ResetNetworkWLANCheck();
	else
		ResetNetworkCardCheck();
	
	#endif
	//PrintCurrentTime(22222);
	switch( cmd )
	{
		case DEVICE_IP_HEART_BEAT_SERVICE:
			//ReceiveHeartBeat();
			break;
		case GET_IP_BY_NUMBER_REQ:
			ReceiveGetIpByNumberFromNetReq(target_ip, pbuf);
			break;
		case GET_IP_BY_NUMBER_RSP:
			ReceiveGetIpByNumberFromNetRsp(target_ip, pbuf);
			break;

		case GET_IP_BY_MFG_SN_REQ:
			ReceiveGetIpByMFG_SNReq(target_ip, pbuf);
			break;
		case GET_IP_BY_MFG_SN_RSP:
			ReceiveGetIpByMFG_SNRsp(target_ip, pbuf);
			break;
			
		case GET_INFO_BY_IP_REQ:
			ReceiveGetInfoReq(target_ip, pbuf);
			break;
		case GET_INFO_BY_IP_RSP:
			ReceiveGetInfoRsp(target_ip, pbuf, len);
			break;

		case PROG_INFO_BY_IP_REQ:
			ReceiveProgInfoReq(pbuf);
			break;
		case PROG_INFO_BY_IP_RSP:
			ReceiveProgInfoRsp(pbuf);
			break;

		case SEARCH_IP_BY_FILTER_REQ:
			ReceiveSearchIpByFilterReq(target_ip, pbuf);
			break;
			
		case SEARCH_IP_BY_FILTER_RSP:
			ReceiveSearchIpByFilterRsp(pbuf);
			break;
		
		case GET_ABOUT_BY_IP_REQ:
			ReceiveGetAboutReq(target_ip, pbuf);
			break;
		case GET_ABOUT_BY_IP_RSP:
			ReceiveGetAboutRsp(pbuf);
			break;
		case CMD_REMOTE_UPGRADE_REQ:
			ReceiveRemoteUpgradeReq(target_ip, pbuf);
			break;
			
		case CMD_REMOTE_UPGRADE_RSP:
			ReceiveRemoteUpgradeRsp(pbuf);
			break;
			
		case CMD_REMOTE_SOFTWARE_UPDATE_REQ:		//czn_20190506
			Recieve_RemoteAllDevSoftwareUpdateReq(pbuf);
			break;	
							
		case CMD_RES_UPLOAD_REQ:		//czn_20190520
		case CMD_RES_UPLOAD_RSP:
		case CMD_RES_DOWNLOAD_REQ:
		case CMD_RES_DOWNLOAD_RSP:
		case CMD_RES_TRANS_RESULT:
			//IX2V test RecvResSyncCmd_Process(target_ip,cmd,pbuf);
			break;
		case SEARCH_IP_BY_FILTER2_REQ:
			ReceiveSearchIpByFilter2Req(target_ip, pbuf);
			break;
			
		case CMD_BACKUP_AND_RESTORE_REQ:
			ReceiveBackupAndRestoreCmdReq(target_ip, pbuf);
			break;
			
		case CMD_BACKUP_AND_RESTORE_RSP:
			ReceiveBackupAndRestoreCmdRsp(pbuf);
			break;
			
		case CMD_UNLOCK_STATE_REPORT:
			ReceiveUnlockReport(pbuf);
			break;

		case SEARCH_IX_PROXY_REQ:
			ReceiveSearchIxProxyReq(target_ip, pbuf, len);
			break;

		case SEARCH_IX_PROXY_LINKED_REQ:
			ReceiveSearchIxProxyLinkedReq(target_ip, pbuf);
			break;
			
		case SEARCH_IX_PROXY_LINKED_RSP:
			ReceiveIxProxyLinkedRsp(pbuf);
			break;
			
		case IX_REQ_Prog_Call_Nbr_REQ:
			ReceiveIxReqCallNbrCmdReq(pbuf);
			break;
		
		case IX_REQ_Prog_Call_Nbr_RSP:
			ReceiveIxReqCallNbrCmdRsp(pbuf);
			break;

		case IX_REQ_Ring_REQ:
			ReceiveIxReqRingCmdReq(pbuf);
			break;

		case IX_REQ_Ring_RSP:
			ReceiveIxReqRingCmdRsp(pbuf);
			break;

		case IX_PROG_IP_ADDR_REQ:
			ReceiveIxProgIpAddrCmdReq(pbuf);
			break;
			
		case IX_PROG_IP_ADDR_RSP:
			ReceiveIxProgIpAddrCmdRsp(pbuf);
			break;
		case IX_Report_REQ:
			ReceiveIxReportCmdReq(pbuf);
			break;
		case IX_Report_RSP:
			ReceiveIxReportCmdRsp(pbuf);
			break;
		
		case Backup_REQ:
		case Restore_REQ:
		case IX_DEVICE_RES_DOWNLOAD_REQ:
			ReceivePublicUdpCmdReq(cmd, pbuf);
			break;

		case TFTP_WRITE_FILE_REQ:
		case TFTP_READ_FILE_REQ:
		case FileManagement_REQ:
		case TFTP_CHECK_FILE_REQ:
		case SHELL_REQ:
		case EVENT_REQ:
		case GET_PB_BY_IP_REQ:
		case GET_RW_IO_BY_IP_REQ:
		case UPDATE_DEV_TABLE_REQ:
		case TABLE_PROCESS_REQ:
		case SET_DIVERT_STATE_REQ:
		case GET_QR_CODE_REQ:
		case IX_Ring2_REQ:
			ReceivePublicUnicastCmdReq(netDeviceName, cmd, pbuf, len);
			break;

		case TFTP_WRITE_FILE_RSP:
		case TFTP_READ_FILE_RSP:
		case FileManagement_RSP:
		case TFTP_CHECK_FILE_RSP:
		case SHELL_RSP:
		case EVENT_RSP:
		case GET_PB_BY_IP_RSP:
		case GET_RW_IO_BY_IP_RSP:
		case UPDATE_DEV_TABLE_RSP:
		case TABLE_PROCESS_RSP:
		case SET_DIVERT_STATE_RSP:
		case GET_QR_CODE_RSP:
			ReceivePublicUnicastCmdRsp(cmd, pbuf);
			break;

		case Backup_RSP:
		case Restore_RSP:
		case IX_DEVICE_RES_DOWNLOAD_RSP:
			ReceivePublicUdpCmdRsp(cmd, pbuf);
			break;

		case GET_MERGE_INFO_BY_IP_REQ:
			ReceiveGetMergeInfoReq(target_ip, pbuf);
			break;
			
		case GET_MERGE_INFO_BY_IP_RSP:
			ReceiveGetMergeInfoRsp(pbuf);
			break;
					
		case SetTimeServer_REQ:
			ReceiveSetTimeServerCmdReq(pbuf);
			break;	

		case SetTimeServer_RSP:
			ReceiveSetTimeServerCmdRsp(pbuf);
			break;

		case SEARCH_SERVICE_REQ:
			ReceiveMulticastCmdReq(netDeviceName, cmd, pbuf);
			break;

		case SEARCH_SERVICE_RSP:
			ReceiveMulticastCmdRsp(cmd, pbuf);
			break;
			
		case MUL_CTRL_BY_SIPACC_REQ:
			MulCtrlBySipAccReqProcess(target_ip, (MulCtrlBySipAcc_Req_T*)pbuf);
			break;	

		case UPDATE_DEV_TABLE_INFORM:
			IXS_ProxyReceiveUpdateTbInform(target_ip, pbuf);
			break;
			
		case CMD_SIP_CONFIG_REQ:
			ReceiveSipConfigCmdReq(target_ip, pbuf);
			break;
			
		case CMD_SIP_CONFIG_RSP:
			ReceiveSipConfigCmdRsp(pbuf);
			break;
	}
	//PrintCurrentTime(333333);
	return 0;
}

int Udp25007UnlimitedSend(int target_ip, const char* pbuf, int len)
{
	device_update_instance_t*		pOne_device_update_ins;
	ext_pack_buf					send_pack;
	int ret;
		
	int sendLen;

	if(target_ip == 0 || target_ip == -1)
	{
		return -1;
	}

	send_pack.ip	= target_ip;

	char *netDeviceName;
	// ip

	netDeviceName = GetNetDeviceNameByTargetIp2(target_ip);

	// 加入发送队列
	if(target_ip == inet_addr(DEVICE_SEARCH_MULTICAST_ADDR))
	{
		if(GetNetMode() == NM_MODE_LAN)
		{
			//lan_rejoin_multicast_group();
			pOne_device_update_ins = &one_device_update_ins;
		}
		else
		{
			//wlan_rejoin_multicast_group();
			pOne_device_update_ins = &wlan_one_device_update_ins;
		}
	}
	else if(!strcmp(netDeviceName, NET_WLAN0))
	{
		pOne_device_update_ins = &wlan_one_device_update_ins;
	}
	else if(!strcmp(netDeviceName, NET_ETH0))
	{
		pOne_device_update_ins = &one_device_update_ins;
	}

	for(sendLen = 0; sendLen < len; sendLen += send_pack.len)
	{
		send_pack.len = (len - sendLen > IP8210_BUFFER_MAX) ? IP8210_BUFFER_MAX : len - sendLen;
		memcpy(send_pack.dat, pbuf + sendLen, send_pack.len);
		
		for(ret = 0; ret < 3 && push_udp_common_queue( &(pOne_device_update_ins->udp.tmsg_buf), (p_udp_common_buffer)&send_pack, sizeof(ext_pack_buf)-IP8210_BUFFER_MAX+send_pack.len ); ret++)
		{
			dprintf("UDP send buffer limit.\n");
			usleep(100*1000);
		}

		if(len > IP8210_BUFFER_MAX)
		{
			usleep(100*1000);
		}
	}

	return 0;
}

int api_udp_shell_send_data(int target_ip, const char* pbuf)
{
	return Udp25007UnlimitedSend(target_ip, pbuf, strlen(pbuf));
}
