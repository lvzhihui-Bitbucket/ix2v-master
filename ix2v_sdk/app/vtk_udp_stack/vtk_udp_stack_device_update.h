
#ifndef _VTK_UDP_STACK_DEVICE_UPDATE_H
#define _VTK_UDP_STACK_DEVICE_UPDATE_H

#include "vtk_udp_stack_class_ext.h"

#define PUBLIC_UDP_CMD_REQ_DATA_LEN				1000
#define PUBLIC_MUL_RSP_UDP_CMD_REQ_DATA_LEN		1000

#define	VDP_PRINTF_DEVICE_UPDATE

#ifdef	VDP_PRINTF_DEVICE_UPDATE
#define	dev_update_printf(fmt,...)	printf("[DECVICE_UPDATE]-["__FILE__"]-[%04d]-"fmt"",__LINE__,##__VA_ARGS__)
#else
#define	dev_update_printf(fmt,...)
#endif

typedef enum
{
	NM_MODE_LAN=0,
	NM_MODE_WLAN,
}NM_MODE_E;


#define CLIENT_MAX_NUM		5					//最多允许5个客户端同时发送相同指令

#define DEVICE_TYPE								"DT-IPG24"			// 设备类型

#define DEVICE_SEARCH_MULTICAST_ADDR			"236.6.6.1"			// 组播地址
#define DEVICE_SEARCH_UDP_BOARDCAST_PORT		25007			// 组播播搜索所有设备

#define DEVICE_RING_REQUEST						0x9A00			// 设备ring 请求
#define DEVICE_SEARCH_CMD_READ_REQ				0x9A01			// 设备查找命令请求
#define DEVICE_SEARCH_CMD_READ_RSP				0x9A02			// 设备查找命令应答
#define DEVICE_SEARCH_CMD_WRITE_REQ				0x9B01			// 设备更新命令请求
#define DEVICE_SEARCH_CMD_WRITE_RSP				0x9B02			// 设备更新命令应答
#define DEVICE_REQUEST_WRITE_NODE_ID_REQ		0x9C02			// 设备更新NODE ID请求
#define DEVICE_REQUEST_WRITE_NODE_ID_RSP		0x9C03			// 设备更新NODE ID应答
#define DEVICE_TIP_REQUEST						0x9D01			// 设备tip 请求
#define DEVICE_IP_HEART_BEAT_SERVICE			0x9F01			//IP设备心跳服务

#define GET_IP_BY_NUMBER_REQ					0xA001			//GET设备IP BY NUMBER请求
#define GET_IP_BY_NUMBER_RSP					0xA002			//GET设备IP BY NUMBER应答

#define GET_IP_BY_MFG_SN_REQ					0xA003			//GET设备IP BY MGF_SN请求
#define GET_IP_BY_MFG_SN_RSP					0xA004			//GET设备IP BY MGF_SN应答

#define GET_INFO_BY_IP_REQ						0xA005			//获取设备信息请求
#define GET_INFO_BY_IP_RSP						0xA006			//获取设备信息应答

#define SEARCH_IP_BY_FILTER_REQ					0xA007			//SEARCH设备IP BY MGF_SN请求
#define SEARCH_IP_BY_FILTER_RSP					0xA008			//SEARCH设备IP BY MGF_SN应答

#define PROG_INFO_BY_IP_REQ						0xA009			//写设备信息请求
#define PROG_INFO_BY_IP_RSP						0xA00A			//写取设备信息应答

#define GET_ABOUT_BY_IP_REQ						0xA00B			//获取设备ABOUT信息请求
#define GET_ABOUT_BY_IP_RSP						0xA00C			//获取设备ABOUT信息应答

#define CMD_REMOTE_UPGRADE_REQ					0xA00D			//远程更新请求
#define CMD_REMOTE_UPGRADE_RSP					0xA00E			//远程更新应答

#define CMD_REMOTE_SOFTWARE_UPDATE_REQ			0xA00F			//远程update 请求		//czn_20190506
#define CMD_REMOTE_SOFTWARE_UPDATE_RSP			0xA010			//远程update 应答

#define MS_SYNC_REQ					0xA011	//czn_20190221		
#define MS_SYNC_RSP						0xA012
	#define MS_SYNC_OP_SetCallScene		0
	#define MS_SYNC_OP_List					1
	#define MS_SYNC_OP_CallRecord			2
	#define MS_SYNC_OP_SipConfig			3

#define CMD_SIP_CONFIG_REQ						0xA013			//sip 配置请求
#define CMD_SIP_CONFIG_RSP						0xA014			//sip 配置应答

#define GET_GetCallBtnBindingState_REQ			0xA015			//获取设备绑定状态请求
#define GET_GetCallBtnBindingState_RSP			0xA016			//获取设备绑定状态应答

#define CallBtnBinding_REQ						0xA017			//设备绑定状态请求
#define CallBtnBinding_RSP						0xA018			//设备绑定状态应答


#define CMD_ASK_DEBUG_STATE_REQ					0xA019			//询问设备是否在debug模式请求
#define CMD_ASK_DEBUG_STATE_RSP					0xA01A			//询问设备是否在debug模式应答

#define CMD_EXIT_DEBUG_STATE_REQ				0xA01B			//退出debug模式

//czn_20190520_s
#define CMD_RES_UPLOAD_REQ						0xA01C
#define CMD_RES_UPLOAD_RSP						0xA01D	
#define CMD_RES_DOWNLOAD_REQ					0xA01E	
#define CMD_RES_DOWNLOAD_RSP					0xA01F
#define CMD_RES_TRANS_RESULT					0xA020
//czn_20190520_e

#define SEARCH_IP_BY_FILTER2_REQ				0xA021			//SEARCH设备IP
#define CMD_CHANGE_INSTALL_PWD_REQ				0xA022			//批量修改工程密码
#define CMD_CHANGE_INSTALL_PWD_RSP				0xA023			//批量修改工程密码

#define CMD_BACKUP_AND_RESTORE_REQ				0xA024			//恢复和备份请求
#define CMD_BACKUP_AND_RESTORE_RSP				0xA025			//恢复和备份应答

#define CMD_UNLOCK_STATE_REPORT					0xA026			//开锁状态汇报

#define SEARCH_IX_PROXY_REQ						0xA027			//SEARCH_IX_PROXY_REQ
#define SEARCH_IX_PROXY_RSP						0xA028			//SEARCH_IX_PROXY_REQ

#define SEARCH_IX_PROXY_LINKED_REQ				0xA029			//SEARCH_IX_PROXY_LINKED_REQ
#define SEARCH_IX_PROXY_LINKED_RSP				0xA02A			//SEARCH_IX_PROXY_LINKED_REQ

#define IX_REQ_Prog_Call_Nbr_REQ				0xA02B			//IX设备申请房号请求
#define IX_REQ_Prog_Call_Nbr_RSP				0xA02C			//IX设备申请房号请求的应答

#define IX_REQ_Tip_REQ							0xA02D			//IX设备tip请求
#define IX_REQ_Tip_RSP							0xA02E			//IX设备tip应答

#define IX_REQ_Ring_REQ							0xA02F			//IX设备Ring请求
#define IX_REQ_Ring_RSP							0xA030			//IX设备Ring应答

#define IX_PROG_IP_ADDR_REQ						0xA031			//IX设备修改IX设备IP地址请求
#define IX_PROG_IP_ADDR_RSP						0xA032			//IX设备修改IX设备IP地址应答

#define IX_Report_REQ							0xA033			//IX设备报告请求
#define IX_Report_RSP							0xA034			//IX设备报告应答

#define IX_DEVICE_RES_DOWNLOAD_REQ				0xA035			//远程RES_DOWNLOAD 请求
#define IX_DEVICE_RES_DOWNLOAD_RSP				0xA036			//远程RES_DOWNLOAD 应答

#define GET_MERGE_INFO_BY_IP_REQ				0xA037			//获取设备信息请求
#define GET_MERGE_INFO_BY_IP_RSP				0xA038			//获取设备信息应答

#define NOTICE_IXMINI_REQ						0xA039			//开锁通知电梯控制器
#define NOTICE_IXMINI_RSP						0xA03A			//开锁通知电梯控制器应答

#define SettingFromR8001_REQ					0xA03B			//SettingFromR8001请求
#define SettingFromR8001_RSP					0xA03C			//SettingFromR8001应答
	
#define SetTimeServer_REQ						0xA03D			//设置时间服务器请求
#define SetTimeServer_RSP						0xA03E			//设置时间服务器应答

#define Backup_REQ								0xA03F			//备份请求
#define Backup_RSP								0xA040			//备份应答

#define Restore_REQ								0xA041			//恢复请求
#define Restore_RSP								0xA042			//恢复应答

#define FileManagement_REQ						0xA043			//文件管理请求
#define FileManagement_RSP						0xA044			//文件恢复应答

#define TFTP_WRITE_FILE_REQ						0xA045			//TFTP发送文件
#define TFTP_WRITE_FILE_RSP						0xA046			//

#define TFTP_READ_FILE_REQ						0xA047			//TFTP接收文件
#define TFTP_READ_FILE_RSP						0xA048			//

#define TFTP_CHECK_FILE_REQ						0xA049			//TFTP接收文件
#define TFTP_CHECK_FILE_RSP						0xA04A			//

#define SHELL_REQ								0xA04B			//shell请求
#define SHELL_RSP								0xA04C			//shell应答

#define SEARCH_SERVICE_REQ						0xA04D			//搜索服务请求
#define SEARCH_SERVICE_RSP						0xA04E			//搜索服务应答

#define GET_PB_BY_IP_REQ						0xA04F			//获取设备看板信息请求
#define GET_PB_BY_IP_RSP						0xA050			//获取设备看板信息应答

#define GET_RW_IO_BY_IP_REQ						0xA051			//读写IO参数请求
#define GET_RW_IO_BY_IP_RSP						0xA052			//读写IO参数应答

#define EVENT_REQ								0xA053			//Event请求
#define EVENT_RSP								0xA054			//Event应答

#define UPDATE_DEV_TABLE_REQ					0xA055			//请求更新设备表
#define UPDATE_DEV_TABLE_RSP					0xA056			//请求更新设备表应答
#define UPDATE_DEV_TABLE_INFORM					0xA057			//设备表更新通知

#define TABLE_PROCESS_REQ						0xA058			//远程表处理请求
#define TABLE_PROCESS_RSP						0xA059			//远程表处理应答

#define SET_DIVERT_STATE_REQ					0xA05A			//设置转呼状态请求
#define SET_DIVERT_STATE_RSP					0xA05B			//设置转呼状态应答

#define GET_QR_CODE_REQ							0xA05C			//获取二维码字符串请求
#define GET_QR_CODE_RSP							0xA05D			//获取二维码字符串应答

#define IX_Ring2_REQ							0xA05E			//Ring2请求
#define IX_Ring2_RSP							0xA05F			//Ring2应答


#define MUL_CTRL_BY_SIPACC_REQ					0x8888
#define MUL_CTRL_BY_SIPACC_RSP					0x8889

#define UDP_WAIT_RSP_TIME						20				//等待应答时间 ms
#define UDP_Resend_TIME							500				//UDP点播重发 ms

#define BusinessWaitUdpTime						1				//1 秒

#pragma pack(1)

//ip head struct define
typedef struct
{
	char 	s;
	char 	w;
	char 	i;
	char 	t;
	char 	c;
	char 	h;
} device_search_head;

typedef struct
{
	unsigned short	cmd;
} device_search_cmd;

// 设备发现请求数据包
typedef struct
{
	char		req_source_zero[6];		// 保留6个字节0
	int		req_source_ip;			// 请求PC端的ip地址
	char		req_target_zero[6];		// 保留6个字节0
	int		req_target_ip_seg;		// 请求PC端的ip地址所在的网段
	char		mfgSn[12];				//序列号
	uint16	ipNodeId;				//节点ID
	char		ipDeviceType[18];			// 设备类型
} device_search_read_req;

// 设备发现应答数据包
typedef struct
{
	char		req_source_zero[6];		// 保留6个字节0
	int		req_source_ip;			// 请求PC端的ip地址
	char		req_target_zero[6];		// 保留6个字节0
	int		rsp_target_ip;				// 应答设备的ip地址
	int		rsp_target_mask;			// 应答设备的mask掩码
	int		rsp_target_gateway;		// 应答设备的网关
	char		rsp_target_mac[6];		// 应答设备的mac地址
	char		mfgSn[12];				//序列号
	uint16	ipNodeId;				//节点ID
	char		ipDeviceType[18];			// 设备类型
	uint8	version;					
	uint8	ipCfgLimit;				
} device_search_read_rsp;

// 设备更新请求数据包
typedef struct
{
	int		req_target_old_ip;		// 指定设备的原ip地址
	int		req_target_new_ip;		// 指定设备的新ip地址
	int		req_target_new_mask;	// 指定设备的新mask掩码
	int		req_target_new_gateway;	// 指定设备的新网关
	char		req_target_new_mac[6];	// 指定设备的新mac
	char		mfgSn[12];				//序列号
	uint16	ipNodeId;				//节点ID
	char		ipDeviceType[18];			// 设备类型
} device_search_write_req;

// 设备更新应答数据包
typedef struct
{
	int		rsp_target_old_ip;		// 应答设备的原ip地址
	int		rsp_target_new_ip;		// 应答设备的新ip地址
	int		rsp_target_new_mask;	// 应答设备的新mask掩码
	int		rsp_target_new_gateway;	// 应答设备的新网关
	char		rsp_target_new_mac[6];	// 应答设备的新mac
	char		mfgSn[12];				//序列号
	uint16	ipNodeId;				//节点ID
	char		ipDeviceType[18];			// 设备类型
} device_search_write_rsp;

typedef struct
{
	device_search_head	head;
	device_search_cmd	cmd;
	union
	{
		char						dat[1];
		device_search_read_req		read_req;
		device_search_read_rsp		read_rsp;
		device_search_write_req		write_req;
		device_search_write_rsp		write_rsp;		
	}buf;
} device_search_package;

typedef struct
{
	int				rand;											//随机数
	int				sourceIp;										//源ip
	char			data[PUBLIC_UDP_CMD_REQ_DATA_LEN+1];			//information
} PublicUdpCmdReq;

typedef struct
{
	int						rand;				//随机数
	int						sourceIp;			//源ip
	int						result;				//result
} PublicUdpCmdRsp;

typedef struct
{
	int				waitFlag;			//等待回应标记
	int				rand;				//随机数
	int				sourceIp;			//源ip
	int				result;
}PublicUdpCmdRun;

typedef struct
{
	int				rand;											//随机数
	int				sourceIp;										//源ip
	int				dataLen;										//后面的数据长度
	char			data[PUBLIC_MUL_RSP_UDP_CMD_REQ_DATA_LEN];
} PublicMulRspUdpCmdReq;

typedef struct
{
	int						rand;				//随机数
	int						sourceIp;			//源ip
	int						packetNo;			//应答包序号
	int						packetTotal;		//应答总数
	int						result;				//result
	int 					dataLen;			//后面的数据长度
	char					data[PUBLIC_MUL_RSP_UDP_CMD_REQ_DATA_LEN];
} PublicMulRspUdpCmdRsp;

typedef int (*MulRspCmdProcess)(const char*, char**, int*);
typedef struct
{
	int					no;					//应答包序号
	int					dataLen;			//数据长度
	char*				data;				//接收应答数据
}PublicMulRspPacket;

typedef struct
{
	int					waitFlag;			//等待回应标记
	int					rand;				//随机数
	int					sourceIp;			//源ip
	int					result;
	MulRspCmdProcess	process;			//处理函数
	int					packetCnt;			//应答总数
	sem_t 				sem;
	PublicMulRspPacket*	packet				//应答包					
}PublicMulRspUdpCmdRun;

typedef struct
{
	int				rand;				//随机数
	int				sourceIp;			//源ip
}IXReq_T;
#pragma pack()

typedef struct
{
	udp_comm_rt 		udp;				//	udp交互模板实例
	send_sem_id_array	waitrsp_array;		// 	业务应答同步队列	
}device_update_instance_t;

// 初始化udp:25000的服务实例
int init_device_update_instance( void );

// 通过udp:25007端口发送数据
int api_udp_device_update_send_data(int target_ip, unsigned short cmd, const char* pbuf, unsigned int len );
int api_udp_device_update_send_data_by_device(char* net_device_name, int target_ip, unsigned short cmd, const char* pbuf, unsigned int len );

// 通过udp:25007端口发送数据包后，并等待业务应答，得到业务应答的数据
int api_udp_device_update_send_req(int target_ip, unsigned short cmd, char* psbuf, int slen , char *prbuf, unsigned int *prlen);

// 接收到udp:25007端口数据包后给出的业务应答
int api_udp_device_update_send_rsp( int target_ip, unsigned short cmd, int id, const char* pbuf, unsigned int len );

// 接收到udp:25007端口数据包的回调函数
int api_udp_device_update_recv_callback(char* netDeviceName, int target_ip, unsigned short cmd, char* pbuf, unsigned int len );
#endif

