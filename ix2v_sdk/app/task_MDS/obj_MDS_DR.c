/**
  ******************************************************************************
  * @file    obj_MDS_DR.c
  * @author  czb
  * @version V00.01.00
  * @date    2022.5.17
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 

#include <stdio.h>
#include <sys/sysinfo.h>
#include "task_MDS.h"
#include "obj_AiLayer.h"
#include "obj_MDS_Output.h"
#include "vdp_uart.h"
#include "obj_PublicInformation.h"
#include "obj_MDS_Input.h"
#include "obj_MDS_DR.h"
#include "elog.h"
#include "task_Event.h"
#include "task_Beeper.h"

static DR_RUN drRun = {.state = DR_IDLE};

int GetDrState(void)
{
    return drRun.state;
}

static int DisplayIdelGif(int timing)
{
    if(IsMDS_Online(DR_MODEL_ADDR))
	{
        DR_DisplayDialoge(FONT_8X16,GIF_END_RETURN,GIF_BRIGHT_DEF,1,GIF_SPEED_SLOW,0,0,GIF_STANDBY);
    }
    else
    {
        API_MDS_CheckOneOnline(DR_MODEL_ADDR);
    }
    log_v("DisplayIdelGif------------------------");
    return 1;
}

int SetDrState(DR_STATE state)
{
    drRun.state = state;
    if(drRun.state == DR_IDLE)
    {
        API_Add_TimingCheck(DisplayIdelGif, 10);
    }
    else
    {
        API_Del_TimingCheck(DisplayIdelGif);
    }

    return state;
}


void DR_Print( DR_FONT_TYPE font, DR_DISP_DIR_TYPE dir, DR_ALIGN_TYPE align, DR_CURSOR_TYPE cursor, DR_SCROLL_TYPE scroll, 
              unsigned char startLn, unsigned char startCol, unsigned char endLn, unsigned char endCol, char* str)
{

    unsigned char buf[50];
    buf[0] = font | (dir << 1) | (align << 2) | (cursor << 4) | (scroll << 6); // mode
    buf[1] = startLn | (startCol << 4); // startPos
    buf[2] = endLn | (endCol << 4); // endPos    
    strcpy((char*)buf+3,str);
    
    API_ExtModel_Send_Cmd_Ack(DR_MODEL_ADDR,CMD_MODEL_DR_PRINT,buf,strlen(str)+4);
}

void DR_Write( DR_FONT_TYPE font, DR_DISP_DIR_TYPE dir, DR_ALIGN_TYPE align, DR_CURSOR_TYPE cursor, DR_SCROLL_TYPE scroll, 
              unsigned char startLn, unsigned char startCol, unsigned char endLn, unsigned char endCol, char* str)
{
    unsigned char buf[50];
    buf[0] = font | (dir << 1) | (align << 2) | (cursor << 4) | (scroll << 6); // mode
    buf[1] = startLn | (startCol << 4); // startPos
    buf[2] = endLn | (endCol << 4); // endPos    
    strcpy((char*)buf+3,str);
    
    API_ExtModel_Send_Cmd_Ack(DR_MODEL_ADDR,CMD_MODEL_DR_WRITE,buf,strlen(str)+4);
}
void DR_ClearAll(void)
{
    API_ExtModel_Send_Cmd_Ack(DR_MODEL_ADDR,CMD_MODEL_DR_CLR_ALL,NULL,0);
}

void DR_ClearString(DR_FONT_TYPE font,unsigned char startLn,unsigned char startCol,unsigned char endLn, unsigned char endCol)
{
    unsigned char buf[5];
    buf[0] = font; // mode
    buf[1] = startLn | (startCol << 4); // startPos
    buf[2] = endLn | (endCol << 4); // endPos
    
    API_ExtModel_Send_Cmd_Ack(DR_MODEL_ADDR,CMD_MODEL_DR_CLR,buf,3);
}

void DR_SetCurosr(DR_FONT_TYPE font,DR_ALIGN_TYPE align,DR_CURSOR_TYPE cursor,unsigned char startLn,unsigned char startCol,unsigned char endLn, unsigned char endCol)
{
    unsigned char buf[5];
    buf[0] = font | (align << 2) | (cursor << 4); // mode
    buf[1] = startLn | (startCol << 4); // startPos
    buf[2] = endLn | (endCol << 4); // endPos
    
    API_ExtModel_Send_Cmd_Ack(DR_MODEL_ADDR,CMD_MODEL_DR_CURSOR_SET,buf,3);
}

void DR_DisplayGif(DR_FONT_TYPE font, GIF_END_TYPE endType, GIF_BRIGHT_MODE bright, unsigned char loop, GIF_SPEED_TYPE speed, unsigned char x, unsigned char y, unsigned char index)
{
    unsigned char buf[10];
    buf[0] = x;
    buf[1] = y;
    buf[2] = index;
    buf[3] = font | (endType << 1) | (bright << 3) | (loop << 4) | (speed << 7); // mode
    API_ExtModel_Send_Cmd_Ack(DR_MODEL_ADDR,CMD_MODEL_DR_DISP_GIF,buf,4);
}
void DR_DisplayDialoge(DR_FONT_TYPE font, GIF_END_TYPE endType, GIF_BRIGHT_MODE bright, unsigned char loop, GIF_SPEED_TYPE speed, unsigned char x, unsigned char y, unsigned char index)
{
    unsigned char buf[10];
    buf[0] = x;
    buf[1] = y;
    buf[2] = index;
    buf[3] = font | (endType << 1) | (bright << 3) | (loop << 4) | (speed << 7); // mode
    API_ExtModel_Send_Cmd_Ack(DR_MODEL_ADDR,CMD_MODEL_DR_DISP_DIALOGE,buf,4);
}
void DR_DisplayBmp(DR_FONT_TYPE font, unsigned char x, unsigned char y, unsigned char index)
{
    unsigned char buf[10];
    buf[0] = x;
    buf[1] = y;
    buf[2] = index;
    buf[3] = font; // mode
    API_ExtModel_Send_Cmd_Ack(DR_MODEL_ADDR,CMD_MODEL_DR_DISP_BMP,buf,4);
}

void DR_DisplayTimeLapse(DR_FONT_TYPE font, TIME_LAPSE_MODE mode, unsigned char ln, unsigned char col, unsigned short initTime)
{
    unsigned char buf[10];
    buf[0] = font | (mode << 1); // mode
    buf[1] = ln | (col << 4); 
    buf[2] = initTime&0xff;
    buf[3] = (initTime>>8);
    API_ExtModel_Send_Cmd_Ack(DR_MODEL_ADDR,CMD_MODEL_DR_TIME_LAPSE_ON,buf,4);
}

void DR_StandbyProcess(void)
{
    SetDrState(DR_IDLE);
    DR_ClearAll();
}


void DR_MenuInputProcess(DR_Display_mode mode, char* input, int len)
{
	char disp[50];

    //显示超过DR_STRING_MAX_LENGTH，DR显示不出来，截取后DR_STRING_MAX_LENGTH位。
    if(len > DR_STRING_MAX_LENGTH)
    {
        input = input + len - DR_STRING_MAX_LENGTH;
        len = DR_STRING_MAX_LENGTH;
    }

    //dprintf("DR_MenuInputProcess ----------------------mode=%d\n", mode);
    switch( mode )
    {
        case STANDBY:
            if(GetDrState() != CALL_INUSE)
            {
                DR_StandbyProcess();
            }
            break;

        case INPUT_SETTING_CODE:
            SetDrState(MK_INUSE);
            if( len )
            {
                memcpy(disp, input, len);
                disp[len] = 0;
            }
            else
            {
                strcpy(disp, "Set");
                len = strlen("Set");
            }

            if( len > 2 )
            {
                DR_Print(FONT_5X7,DISP_DIR_L2R,ALIGN_CENTER,CURSOR_BLINK,SCROLL_OFF_RIGHT,0,0,1,3,disp);
            }
            else
            {
                DR_Print(FONT_8X16,DISP_DIR_L2R,ALIGN_CENTER,CURSOR_BLINK,SCROLL_OFF_RIGHT,0,0,0,2,disp);
            }
            break;

        case INPUT_ROOM_NUM:
            SetDrState(MK_INUSE);
            if( len )
            {
                memcpy(disp, input, len);
                disp[len] = 0;
                if( len > 2 )
                {
                    DR_Print(FONT_5X7,DISP_DIR_L2R,ALIGN_CENTER,CURSOR_ON,SCROLL_OFF_RIGHT,0,0,1,3,disp);
                }
                else
                {
                    DR_Print(FONT_8X16,DISP_DIR_L2R,ALIGN_CENTER,CURSOR_ON,SCROLL_OFF_RIGHT,0,0,0,2,disp);
                }
            }
            break;
        case INPUT_PASSWORD:
            SetDrState(MK_INUSE);
            if( len )
            {
                memset(disp, '*', len);
                disp[len] = 0;
                if( len > 2 )
                {
                    DR_Print(FONT_5X7,DISP_DIR_L2R,ALIGN_CENTER,CURSOR_ON,SCROLL_OFF_RIGHT,0,0,1,3,disp);
                }
                else
                {
                    DR_Print(FONT_8X16,DISP_DIR_L2R,ALIGN_CENTER,CURSOR_ON,SCROLL_OFF_RIGHT,0,0,0,2,disp);
                }
            }
            else
            {
                DR_ClearAll();
                DR_SetCurosr(FONT_8X16,ALIGN_CENTER,CURSOR_ON,0,0,0,2);
            }
            break;

        case INPUT_M_PASSWORD:
            SetDrState(MK_INUSE);
            if( len )
            {
                memset(disp, '*', len);
                disp[len] = 0;
                if( len > 2 )
                {
                    DR_Print(FONT_5X7,DISP_DIR_L2R,ALIGN_CENTER,CURSOR_BLINK,SCROLL_OFF_RIGHT,0,0,1,3,disp);
                }
                else
                {
                    DR_Print(FONT_8X16,DISP_DIR_L2R,ALIGN_CENTER,CURSOR_BLINK,SCROLL_OFF_RIGHT,0,0,0,2,disp);
                }
            }
            else
            {
                DR_ClearAll();
                DR_SetCurosr(FONT_8X16,ALIGN_CENTER,CURSOR_BLINK,0,0,0,2);
            }
            break;

        case PASSWORD_ERROR:
        case M_PASSWORD_ERROR:
            DR_DisplayDialoge(FONT_8X16,GIF_END_RETURN,GIF_BRIGHT_DEF,1,GIF_SPEED_FAST,0,0,GIF_ERROR);
            break;

        case DISPLAY_SETTING_VALUE:
            SetDrState(MK_INUSE);
            if( len )
            {
                memcpy(disp, input, len);
                disp[len] = 0;
                DR_Print(FONT_5X7,DISP_DIR_L2R,ALIGN_LEFT,CURSOR_OFF,SCROLL_LEFT,0,0,1,3,disp);
            }
            break;
        case CHANGE_UNLOCK_PASSWORD:
            if(len == 1)
            {
                DR_DisplaySettingValue("Succ", "Unlock1Pwd1");
            }
            else if(len == 2)
            {
                DR_DisplaySettingValue("Succ", "Unlock1Pwd2");
            }
            else if(len == 3)
            {
                DR_DisplaySettingValue("Succ", "Unlock2Pwd1");
            }
            else if(len == 4)
            {
                DR_DisplaySettingValue("Succ", "Unlock2Pwd2");
            }
            break;
    }
}

void DR_DisplaySettingValue(char* name, char* value)
{
    SetDrState(MK_INUSE);
    DR_Print(FONT_5X7,DISP_DIR_L2R,ALIGN_LEFT,CURSOR_OFF,SCROLL_OFF_LEFT,0,0,0,3,name);
    DR_Print(FONT_5X7,DISP_DIR_L2R,ALIGN_LEFT,CURSOR_OFF,SCROLL_LEFT,1,0,1,3,value);
}


void DR_CallDisplayProcess(DR_Display_mode mode, void* data)
{
    char disp[30];
    unsigned short time;

    if(GetDrState() == MK_INUSE)
    {
        dprintf("DR_CallDisplayProcess -----------------------MK_INUSE\n");
        return;
    }

    switch(mode)
    {
    	case STANDBY:
            DR_StandbyProcess();
            break;

        case CALLING_START:
            SetDrState(CALL_INUSE);
            if(data)	
            {
                DR_Print(FONT_5X7,DISP_DIR_L2R,ALIGN_CENTER,CURSOR_OFF,SCROLL_LEFT,1,0,1,3, data);
            }
            DR_DisplayBmp(FONT_5X7,0,0,0);
            break;

        case CALLING_BUSY:
            DR_StandbyProcess();
            DR_DisplayDialoge(FONT_8X16,GIF_END_RETURN,GIF_BRIGHT_DEF,1,GIF_SPEED_FAST,0,0,GIF_WAIT);
            break;

        case CALLING_ERROR:
            DR_StandbyProcess();
            DR_DisplayDialoge(FONT_8X16,GIF_END_RETURN,GIF_BRIGHT_DEF,1,GIF_SPEED_FAST,0,0,GIF_ERROR);
            break;

        case CALLING_CANCEL:
            DR_StandbyProcess();
            DR_DisplayDialoge(FONT_8X16,GIF_END_RETURN,GIF_BRIGHT_DEF,1,GIF_SPEED_FAST,0,0,GIF_CANCEL);
            break;

        case CALLING_WAIT:
            SetDrState(CALL_INUSE);
            if(data)	
            {
                DR_Print(FONT_5X7,DISP_DIR_L2R,ALIGN_CENTER,CURSOR_OFF,SCROLL_LEFT,1,0,1,3, data);
            }
            DR_DisplayBmp(FONT_5X7,0,0,0);
            break;
            
        case CALLING_TALK:
            SetDrState(CALL_INUSE);
            DR_DisplayGif(FONT_5X7,GIF_END_RETURN,GIF_BRIGHT_DEF,0,GIF_SPEED_SLOW,0,0,0);
            DR_DisplayTimeLapse(FONT_5X7, TIME_LAPSE_COUNT_DN, 1, 0, 90);
            break;
    }
}

void DR_UnlockDisplayProcess(DR_Display_mode mode)
{
    switch(mode)
    {
        case DR_UNLOCK1:
            DR_DisplayDialoge(FONT_8X16,GIF_END_RETURN,GIF_BRIGHT_DEF,1,GIF_SPEED_SLOW,0,0,GIF_UNLOCK1);
            break;
        case DR_UNLOCK2:
            DR_DisplayDialoge(FONT_8X16,GIF_END_RETURN,GIF_BRIGHT_DEF,1,GIF_SPEED_SLOW,0,0,GIF_UNLOCK2);
            break;
    }
}


int DR_DisplayEventLock(cJSON *cmd)
{
    char* name = cJSON_GetStringValue(cJSON_GetObjectItemCaseSensitive(cmd, "name"));
    if(name && cJSON_IsTrue(cJSON_GetObjectItemCaseSensitive(cmd, "ctrl")))
    {
        if(!strcmp(name, "RL1"))
        {
    		DR_UnlockDisplayProcess(DR_UNLOCK1);
        }
        else if(!strcmp(name, "RL2"))
        {
    		DR_UnlockDisplayProcess(DR_UNLOCK2);
        }
    }

	return 1;
}

int DR_DisplayEventCall(cJSON *cmd)
{
    char* eventName = cJSON_GetStringValue(cJSON_GetObjectItemCaseSensitive(cmd, EVENT_KEY_EventName));
    char* callserverState = cJSON_GetStringValue(API_PublicInfo_Read(PB_CALL_SER_STATE));
    int Call_Part = API_PublicInfo_Read_Bool(PB_CALL_PART);
   // char* Call_Tar_DtAddr = cJSON_GetStringValue(API_PublicInfo_Read(PB_CALL_TAR_DTADDR));
    char roomNumber[10];
    int number;
    
    if(callserverState == NULL)
    {
        return 0;
    }

    //dprintf("callserverState = %s, Call_Part = %d, Call_Tar_DtAddr = %s\n", callserverState, Call_Part, Call_Tar_DtAddr);

    if(eventName && !strcmp(eventName, EventCall))
    {
        char* callEvent = cJSON_GetStringValue(cJSON_GetObjectItemCaseSensitive(cmd, "CALL_EVENT"));
        if(callEvent)
        {
            if(!strcmp(callEvent, "Calling_Start"))
            {
                //DR_CallDisplayProcess(CALLING_START, NULL);
            }
            else if(!strcmp(callEvent, "Calling_Busy"))
            {
                DR_CallDisplayProcess(CALLING_BUSY, NULL);
            }
            else if(!strcmp(callEvent, "Calling_Error"))
            {
                DR_CallDisplayProcess(CALLING_ERROR, NULL);
            }
            else if(!strcmp(callEvent, "Calling_Cancel"))
            {
                DR_CallDisplayProcess(CALLING_CANCEL, NULL);
            }
        }
    }
    else if(callserverState)
    {
        if(!strcmp(callserverState, "Wait"))
        {
            DR_CallDisplayProcess(STANDBY, NULL);
        }
        else if(Call_Part && !strcmp(callserverState, "Ring"))
        {
        	#ifdef PID_IX821
        	char* Call_Tar_ixaddr = cJSON_GetStringValue(API_PublicInfo_Read(PB_CALL_TAR_IXADDR));
		if(Call_Tar_ixaddr)
		{
			//IXAddrMappingM4Key(Call_Tar_ixaddr);
			sprintf(roomNumber,"%02d",IXAddrMappingM4Key(Call_Tar_ixaddr));
			DR_CallDisplayProcess(CALLING_WAIT, roomNumber);
		}
		#endif
            /*
            number = strtoul(Call_Tar_DtAddr, NULL, 0);
            number=((number-0x80)>>2);
            number = ((number==0 || number > 32)? 32 : number);
            sprintf(roomNumber,"%02d",number);

            DR_CallDisplayProcess(CALLING_WAIT, roomNumber);
            */
        }
        else if(Call_Part && (!strcmp(callserverState, "Ack") || !strcmp(callserverState, "RemoteAck")))
        {
            DR_CallDisplayProcess(CALLING_TALK, NULL);
        }
    }

	return 1;
}

void DR_DisplayCardSetup(int* state, char* cardnum, char* room)
{
    char disp[50];
    SetDrState(MK_INUSE);
    if(state)
    {
        DR_ClearAll();
        if(room)
        {            
            if(*state == 1)
            {
                snprintf(disp, 12, "R:%s", room);
                DR_Print(FONT_5X7,DISP_DIR_L2R,ALIGN_LEFT,CURSOR_OFF,SCROLL_OFF_LEFT,0,0,0,3,disp);
                snprintf(disp, 12, "C:%d", get_card_num(room));
                DR_Write(FONT_5X7,DISP_DIR_L2R,ALIGN_LEFT,CURSOR_OFF,SCROLL_LEFT,1,0,1,3,disp);
            }
            else if(*state == 2)
            {
                strcpy(disp, "DEL");
                DR_Print(FONT_5X7,DISP_DIR_L2R,ALIGN_LEFT,CURSOR_OFF,SCROLL_OFF_LEFT,0,0,0,2,disp);
                snprintf(disp, 12, "R:%s", room);
                DR_Write(FONT_5X7,DISP_DIR_L2R,ALIGN_LEFT,CURSOR_OFF,SCROLL_LEFT,1,0,1,3,disp);
            }            
        }
        else
        {
            if(*state == 1)
            {
                strcpy(disp, "ADD");
            }
            else if(*state == 2)
            {
                strcpy(disp, "DEL");
            }
            DR_Print(FONT_5X7,DISP_DIR_L2R,ALIGN_LEFT,CURSOR_OFF,SCROLL_OFF_LEFT,0,0,0,2,disp);
            snprintf(disp, 12, "C:%d", get_card_num(NULL));
            DR_Write(FONT_5X7,DISP_DIR_L2R,ALIGN_LEFT,CURSOR_OFF,SCROLL_LEFT,1,0,1,3,disp);
        }
            
    }
    if(cardnum)
    {
        snprintf(disp, 12, "%s", cardnum);
        DR_Write(FONT_5X7,DISP_DIR_L2R,ALIGN_LEFT,CURSOR_OFF,SCROLL_LEFT,1,0,1,3,disp);
    }
    
}

void DR_CardSetupResult(int result, int* state, char* room)
{
    char disp[50];
    SetDrState(MK_INUSE);
    DR_ClearAll();
    if(result == 0)
    {
        DR_DisplayDialoge(FONT_8X16,GIF_END_RETURN,GIF_BRIGHT_DEF,1,GIF_SPEED_FAST,0,0,GIF_ERROR);
    }
    else
    {
        DR_DisplayDialoge(FONT_8X16,GIF_END_RETURN,GIF_BRIGHT_DEF,1,GIF_SPEED_FAST,0,0,GIF_OK);
    }
    if(state)
    {
        usleep(300*1000);
        if(*state == 1)
        {
            strcpy(disp, "ADD");
        }
        else if(*state == 2)
        {
            strcpy(disp, "DEL");
        }
        DR_Print(FONT_5X7,DISP_DIR_L2R,ALIGN_LEFT,CURSOR_OFF,SCROLL_OFF_LEFT,0,0,0,2,disp);
        snprintf(disp, 12, "C:%d", get_card_num(room));
        DR_Write(FONT_5X7,DISP_DIR_L2R,ALIGN_LEFT,CURSOR_OFF,SCROLL_LEFT,1,0,1,3,disp);
    }
    
}

void DR_DisplayManageCardSet(int state, char* cardnum)
{
    char disp[50];
    SetDrState(MK_INUSE);
    DR_ClearAll();
	 if(state == 2)
	 {
	 	strcpy(disp, "M:-");
		DR_Print(FONT_5X7,DISP_DIR_L2R,ALIGN_LEFT,CURSOR_OFF,SCROLL_OFF_LEFT,0,0,0,2,disp);
		return;
	 }
    if(state == 0)
    {
        strcpy(disp, "M:a");
    }
    else if(state == 1)
    {
        strcpy(disp, "M:d");
    }
    DR_Print(FONT_5X7,DISP_DIR_L2R,ALIGN_LEFT,CURSOR_OFF,SCROLL_OFF_LEFT,0,0,0,2,disp);
    snprintf(disp, 12, "%s", cardnum);
    DR_Write(FONT_5X7,DISP_DIR_L2R,ALIGN_LEFT,CURSOR_OFF,SCROLL_LEFT,1,0,1,3,disp);
}

void DR_CardSetupRestore(void)
{
    char disp[50];
    SetDrState(MK_INUSE);
    DR_ClearAll();
    strcpy(disp, "Restore");
    DR_Write(FONT_5X7,DISP_DIR_L2R,ALIGN_LEFT,CURSOR_OFF,SCROLL_LEFT,1,0,1,3,disp);
}

void DR_CardSetupExit(void)
{
    DR_ClearAll();
    DR_StandbyProcess();
}
/*********************************************************************************************************
**  End Of File
*********************************************************************************************************/

