/**
  ******************************************************************************
  * @file    obj_MDS_Input.c
  * @author  czb
  * @version V00.01.00
  * @date    2022.5.17
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 

#include <stdio.h>
#include <sys/sysinfo.h>
#include "task_MDS.h"
#include "obj_AiLayer.h"
#include "obj_MDS_Input.h"
#include "obj_MDS_Output.h"
#include "task_CallServer.h"
#include "task_Beeper.h"
#include "task_Event.h"
#include "obj_PublicInformation.h"

extern MDS_RUN_S MDS_Run;

void API_Event_IXCallAction(const char* CallType, const char* roomid, int callKey)
{
	cJSON *call_event;
	cJSON *call_tar;
	call_event=cJSON_CreateObject();
	cJSON_AddStringToObject(call_event, EVENT_KEY_EventName,EventBECall);
	cJSON_AddStringToObject(call_event, "CallType",CallType); //"IxMainCall");
	cJSON_AddStringToObject(call_event, "From","Menu");
	cJSON_AddNumberToObject(call_event, PB_IX_CALL_KEY, callKey);
	call_tar=cJSON_AddObjectToObject(call_event,"Target");


	cJSON_AddStringToObject(call_tar, PB_CALL_TAR_INPUT, roomid);	//"55"
	API_Event_Json(call_event);
	cJSON_Delete(call_event);
	return;
}


/*
ixaddrAndIp格式：[{"IxDevNum":"0099005501","IxDevIP":"192.168.243.60"}]
*/
void API_Event_IXCallGroupAction(const cJSON* ixaddrAndIp)
{
	cJSON *call_event;
	cJSON *call_tar;
	call_event=cJSON_CreateObject();
	cJSON_AddStringToObject(call_event, EVENT_KEY_EventName,EventBECall);
	cJSON_AddStringToObject(call_event, "CallType","IxMainCall");
	cJSON_AddStringToObject(call_event, "From","IX611 Key");

	call_tar=cJSON_AddObjectToObject(call_event,"Target");
	cJSON_AddItemToObject(call_tar, PB_CALL_TAR_IxDev, cJSON_Duplicate(ixaddrAndIp, 1));

	API_Event_Json(call_event);
	cJSON_Delete(call_event);

	return;
}


int API_Event_CallAction(const char* source, const char* callData)
{
	int ret = 0;
	cJSON *event = cJSON_CreateObject();
	cJSON_AddStringToObject(event, EVENT_KEY_EventName, EventCallAction);
	cJSON_AddStringToObject(event, "Source", source);
	cJSON_AddStringToObject(event, "CallData", callData);
	API_Event_Json(event);
	cJSON_Delete(event);

	return ret;
}

void MDS_InputProcess(void *ptrMsgDat)
{	
	MDS_INPUT_S input;
	M_AI_DATA *ptrCMdIo = (M_AI_DATA *)ptrMsgDat;
  	unsigned char device_sa = ptrCMdIo->sa;
	char tempString[10];
	char cardNum[21];
	int cardState = API_PublicInfo_Read_Int(PB_CardSetup_State);
	M4LEDDisplayBySort_stop();
	if( device_sa < MK_MODEL_ADDR ) // S4 address
	{
		input.inputType 		= MDS_M4;
		if(API_PublicInfo_Read_Int(PB_M4_KEY_SORT)==1)
		{
			if(API_PublicInfo_Read_Int(PB_KEY_MODULE_TYPE)==1)
			{
				int key_id[4]={3,4,1,2};
				if(device_sa%2)
				{
					input.keyData = (device_sa-1)*4 + key_id[(ptrCMdIo->cmd & 0x3F)-1];
				}
				else
					input.keyData = (device_sa+1)*4 +key_id[(ptrCMdIo->cmd & 0x3F)-1];
			}
			else
				input.keyData = device_sa*4 + 5-(ptrCMdIo->cmd & 0x3F);
		}
		else
			input.keyData = device_sa*4 + (ptrCMdIo->cmd & 0x3F);
		input.keyStatus = (ptrCMdIo->cmd & 0xC0)>>6;
		if(input.keyStatus == 0)
		{
			BEEP_KEY();
		}
		dprintf("M4 %d %d\n", input.keyData, input.keyStatus);
	}
	else if( device_sa == MK_MODEL_ADDR ) // MK address
	{
		input.inputType 		= MDS_MK;
		input.keyData = ptrCMdIo->cmd;
		BEEP_KEY();
		dprintf("Mk %c\n", input.keyData);
	}
	else if( device_sa == DR_MODEL_ADDR ) // DR
	{
		if( ptrCMdIo->cmd == CMD_MODEL_RF_ID )
		{
			input.inputType = MDS_DR;
			if( ptrCMdIo->data[0] == 2 ) // ID  0x2 0x78 0x0 0x33 0x79 0x23
			{
				memcpy(input.cardData, ptrCMdIo->data+2, 4);
			}
			else if( ptrCMdIo->data[0] == 1 ) // IC 0x1 0x72 0x0D 0xBB 0x91
			{
				memcpy(input.cardData, ptrCMdIo->data+1, 4);
			}
			//BEEP_KEY();
		}

		dprintf("DR 0x%02x%02x%02x%02x\n", input.cardData[0],input.cardData[1],input.cardData[2],input.cardData[3]);
	}
	else
	{
		dprintf("Input Error!\n");
		return;
	}
	
	switch (input.inputType)
	{
		case MDS_MK:
			KeyPad_Manage(input.keyData);
			break;
	
		case MDS_M4:
			if(input.keyStatus)
			{
				snprintf(tempString, 10, "%02d", input.keyData);
				if(cardState)
				{
					Send_CardSetupRoom(tempString);
				}
				else
				{
					API_Event_CallAction("M4", tempString);
				}
			}
			break;

		case MDS_DR:
			//API_Swiping_Card(input.cardData, 4);
			snprintf(cardNum, 20, "%u", (unsigned int)((input.cardData[0]<<24) + (input.cardData[1]<<16) + (input.cardData[2]<<8)+input.cardData[3]));
			Send_CardSwip(cardNum);
			break;
		
		default:
			break;
	}
}

void SwipeUnlock(int lock_id)
{
	API_Event_Unlock(lock_id==1?"RL1":"RL2", "Card");
}

/*********************************************************************************************************
**  End Of File
*********************************************************************************************************/

