/**
  ******************************************************************************
  * @file    obj_MDS_DR.h
  * @author  czb
  * @version V00.01.00
  * @date    2022.5.17
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 

#ifndef _obj_MDS_DR_H
#define _obj_MDS_DR_H

#include "RTOS.h"
#include "utility.h"

// Define Object Property-------------------------------------------------------
#define DR_STRING_MAX_LENGTH		11

typedef enum
{
	STANDBY,
	CALLING_START,
	CALLING_TALK,
	DR_UNLOCK1,
	DR_UNLOCK2,
	INPUT_ROOM_NUM,
	INPUT_PASSWORD,
	INPUT_SETTING_CODE,
	DISPLAY_SETTING_VALUE,
	ADD_CARD,
	DELETE_CARD,
	FORMAT_CARD,
	CALLING_BUSY,
	CALLING_ERROR,
    CALLING_CANCEL,
    CALLING_WAIT,
	PASSWORD_ERROR,
	INPUT_M_PASSWORD,
	M_PASSWORD_ERROR,
	CHANGE_UNLOCK_PASSWORD,
}DR_Display_mode;

typedef enum
{
    FONT_5X7,
    FONT_8X16,
}DR_FONT_TYPE;

typedef enum
{
    DISP_DIR_L2R,
    DISP_DIR_R2L,
}DR_DISP_DIR_TYPE;

typedef enum
{
    ALIGN_CENTER,
    ALIGN_LEFT,
    ALIGN_RIGHT,
}DR_ALIGN_TYPE;

typedef enum
{
    CURSOR_OFF,
    CURSOR_ON,
    CURSOR_BLINK,
}DR_CURSOR_TYPE;

typedef enum
{
    SCROLL_OFF_RIGHT, // 不滚动,超长截取右边部分内容
    SCROLL_OFF_LEFT, // 不滚动,超长截取左边部分内容
    SCROLL_LEFT,
    SCROLL_RIGHT,
}DR_SCROLL_TYPE;

typedef enum
{
    GIF_END_RETURN,
    GIF_END_KEEP_LAST_FRAME,
}GIF_END_TYPE;

typedef enum
{
    GIF_BRIGHT_DEF,
    GIF_BRIGHT_DIM_1,
    GIF_BRIGHT_DIM_2,
}GIF_BRIGHT_MODE;

typedef enum
{
    GIF_SPEED_SLOW,
    GIF_SPEED_FAST,
}GIF_SPEED_TYPE;

typedef enum
{
    TIME_LAPSE_COUNT_UP,
    TIME_LAPSE_COUNT_DN,
}TIME_LAPSE_MODE;

typedef enum
{
    GIF_UNLOCK1,
	GIF_2EASY,
	GIF_CANCEL,
	GIF_ERROR,
	GIF_OK,
	GIF_WAIT,
	GIF_HOUSE,
    GIF_UNLOCK2,
    GIF_STANDBY,
}GIF_8X10_INDEX;

typedef enum
{
    GIF_TALK,
}GIF_5X7_INDEX;

typedef enum
{
    DR_IDLE = 0,
    MK_INUSE,
    CALL_INUSE,
}DR_STATE;

typedef struct
{
    DR_STATE state;
}DR_RUN;

void DR_Write( DR_FONT_TYPE font, DR_DISP_DIR_TYPE dir, DR_ALIGN_TYPE align, DR_CURSOR_TYPE cursor, DR_SCROLL_TYPE scroll, 
              unsigned char startLn, unsigned char startCol, unsigned char endLn, unsigned char endCol, char* str);

    
void DR_MenuInputProcess(DR_Display_mode mode, char* input, int len);
void DR_CallDisplayProcess(DR_Display_mode mode, void* data);
void DR_UnlockDisplayProcess(DR_Display_mode mode);

#endif


