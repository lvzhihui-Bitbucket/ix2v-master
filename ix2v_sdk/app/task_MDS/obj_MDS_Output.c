/**
  ******************************************************************************
  * @file    obj_MDS_Output.c
  * @author  czb
  * @version V00.01.00
  * @date    2022.5.17
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 

#include <stdio.h>
#include <sys/sysinfo.h>
#include "task_MDS.h"
#include "obj_AiLayer.h"
#include "obj_MDS_Output.h"
#include "vdp_uart.h"
#include "obj_PublicInformation.h"
#include "obj_MDS_Input.h"
#include "elog.h"
#include "task_Event.h"
#include "obj_MDS_DR.h"

extern MDS_RUN_S MDS_Run;

#define MDS_SEND_TIMEOUT_MS 	300

static unsigned char M_GetChecksum(L2_FRAME *ptrL2Frame,unsigned char save_cmp)
{
	unsigned char i,checksum;
  	checksum = ptrL2Frame->da ^ ptrL2Frame->cmd;
  	if( ptrL2Frame->length.bit.len )
    {
        checksum ^= ptrL2Frame->length.byte;
    }
	for( i = 0; i < ptrL2Frame->length.bit.len; i++ )
	{
		checksum ^= ptrL2Frame->ext_dat.old_dat[i];
	}
	checksum ^= 0xff;
	
	if( save_cmp )
	{
		ptrL2Frame->cks = checksum;
		return 1;
	}
	else
	{
		return( (ptrL2Frame->cks == checksum)?1:0 );
	}
}

static unsigned char M_Ph_DataWrite(MAC_TYPE macType, unsigned char *pdat,unsigned char len,unsigned char uNew)
{
	unsigned char send_buf[200];
	send_buf[0] = macType;		// mac_type
	send_buf[1] = uNew;		// new/old type
	memcpy( send_buf+2, pdat, len );
	
	return api_uart_send_mds_packet(send_buf, len+2, MDS_SEND_TIMEOUT_MS);;
}

static int Packaging_ExtModel_Data( unsigned char addr, unsigned char cmd, uint8* data, unsigned char len , char* pOutData)
{
	L2_FRAME	l2TrsFrameBuf;
	
	//长度异常处理
	if( len > MAX_FRAME_LENGTH ) len = MAX_FRAME_LENGTH;

	//初始化基本参数
	l2TrsFrameBuf.sa				    	= 0;
	l2TrsFrameBuf.da				    	= addr;
	l2TrsFrameBuf.cmd				    	= cmd;
	
	l2TrsFrameBuf.ext_dat.new_type.sada	= 0;
	
	l2TrsFrameBuf.length.bit.ack_req 	= 1;
	l2TrsFrameBuf.length.bit.daf 		= 0;		//0: 设备地址，1：组地址
	l2TrsFrameBuf.length.bit.newcmd 	= 1;
	l2TrsFrameBuf.length.bit.rep	 	= 1;	
	l2TrsFrameBuf.length.bit.len 		= len;
	
	//保存数据
    if(data && len)
    {
        memcpy(l2TrsFrameBuf.ext_dat.old_dat, data, len);
    }

	l2TrsFrameBuf.pack_len = l2TrsFrameBuf.length.bit.len + 3;
	if( l2TrsFrameBuf.length.bit.len )
    {
        l2TrsFrameBuf.pack_len++;
    }
	    
	//保存checksum
	M_GetChecksum(&l2TrsFrameBuf,1);

	pOutData[0] = MAX_INTERVAL_BETWEEN_NOR;		// mac_type
	pOutData[1] = 2;		                    // new/old type
	memcpy( pOutData+2, &l2TrsFrameBuf.da, l2TrsFrameBuf.pack_len);
	
    return l2TrsFrameBuf.pack_len + 2;
}


unsigned char API_ExtModel_Send_Cmd_Ack( unsigned char addr, unsigned char cmd, uint8* data, unsigned char len )
{
	char send_buf[200];
    int sendLen;

    	if(IsMDS_Online(addr)||addr<16)
	{
		//长度异常处理
		if( len > MAX_FRAME_LENGTH ) len = MAX_FRAME_LENGTH;

		sendLen = Packaging_ExtModel_Data(addr, cmd, data, len, send_buf);
		api_uart_send_mds_packet(send_buf, sendLen, IsMDS_Online(addr)?MDS_SEND_TIMEOUT_MS:0);
	}
	
	return 1;
}

unsigned char API_ExtModel_Broadcast_Cmd( unsigned char cmd, uint8* data, unsigned char len )
{    
	unsigned char i;
	L2_FRAME	l2TrsFrameBuf;
	
	//长度异常处理
	if( len > MAX_FRAME_LENGTH ) len = MAX_FRAME_LENGTH;
		

	//初始化基本参数
	l2TrsFrameBuf.sa				    	= 0;
	l2TrsFrameBuf.da				    	= 0x88;
	l2TrsFrameBuf.cmd				    	= cmd;
	
	l2TrsFrameBuf.ext_dat.new_type.sada	= 0;
	
	l2TrsFrameBuf.length.bit.ack_req 	= 0;
	l2TrsFrameBuf.length.bit.daf 		= 0;		//0: 设备地址，1：组地址
	l2TrsFrameBuf.length.bit.newcmd 	= 1;
	l2TrsFrameBuf.length.bit.rep	 	= 1;	
	l2TrsFrameBuf.length.bit.len 		= len;
	
    
    //保存数据
	//保存数据
	for( i = 0; i < l2TrsFrameBuf.length.bit.len; i++ )
	{		
		l2TrsFrameBuf.ext_dat.old_dat[i] = data[i];
	}
    
	l2TrsFrameBuf.pack_len = l2TrsFrameBuf.length.bit.len + 3;

	if( l2TrsFrameBuf.length.bit.len )
    {
        l2TrsFrameBuf.pack_len++;
    }
	    
	//保存checksum
	M_GetChecksum(&l2TrsFrameBuf,1);
	
	M_Ph_DataWrite( MAX_INTERVAL_BETWEEN_BRD, (unsigned char*)&l2TrsFrameBuf.da, l2TrsFrameBuf.pack_len, 2 );
}


int API_ExtModel_CheckOnline(char addr)
{
	char packData[200];
    int packLen;

	packLen = Packaging_ExtModel_Data(addr, CMD_MODEL_GET_VERSION_REQ, NULL, 0, packData);

	return api_uart_send_mds_packet(packData, packLen, MDS_SEND_TIMEOUT_MS);
}


int API_MK_LED(EXT_LED_STATE state)
{
	char ctrl = 0;

	switch (state)
	{
		case EXT_LED_ON:
			ctrl |= MODEL_KEY_BL_ON;
			break;
		
		case EXT_LED_FLASH_SLOW:
			ctrl |= MODEL_KEY_BL_SLOW_FLASH;
			break;

		case EXT_LED_FLASH_FAST:
			ctrl |= MODEL_KEY_BL_FAST_FLASH;
			break;

		default:
			ctrl |= MODEL_KEY_BL_OFF;
			break;
	}

	ctrl |= MODEL_KEY_BL_BRIGHT2;

    //dprintf("ctrl=0x%x\n", ctrl);

	return API_ExtModel_Send_Cmd_Ack(MK_MODEL_ADDR, CMD_MODEL_KEY_BL, &ctrl, 1);
}

int API_M4_LED(int ledNum, EXT_LED_STATE state)
{
	char ctrl = 0;
    char m4Addr;
    char ledId;

    if(ledNum%4)
    {
        m4Addr = ledNum/4;
	if(API_PublicInfo_Read_Int(PB_M4_KEY_SORT)==0)
        	ledId = ledNum%4;
	else
	{
		if(API_PublicInfo_Read_Int(PB_KEY_MODULE_TYPE)==1)
		{
			int id_tb[4]={3,4,1,2};
			if(m4Addr%2)
				m4Addr=m4Addr-1;
			else
				m4Addr=m4Addr+1;
			ledId=id_tb[(ledNum%4-1)];
		}
		else
			ledId=5-(ledNum%4);
	}
    }
    else
    {
        m4Addr = (ledNum/4) - 1;
		
	if(API_PublicInfo_Read_Int(PB_M4_KEY_SORT)==0)	
        	ledId = 4;
	else
	{
		if(API_PublicInfo_Read_Int(PB_KEY_MODULE_TYPE)==1)
		{
			int id_tb[4]={3,4,1,2};
			if(m4Addr%2)
				m4Addr=m4Addr-1;
			else
				m4Addr=m4Addr+1;
			ledId=id_tb[3];
		}
		else
			ledId=1;
	}
    }

	switch (ledId)
	{
		case 1:
			ctrl |= MODEL_KEY_BL1;
			break;

		case 2:
			ctrl |= MODEL_KEY_BL2;
			break;

		case 3:
			ctrl |= MODEL_KEY_BL3;
			break;

		case 4:
			ctrl |= MODEL_KEY_BL4;
			break;
	}

	switch (state)
	{
		case EXT_LED_ON:
			ctrl |= MODEL_KEY_BL_ON;
			break;
		
		case EXT_LED_FLASH_SLOW:
			ctrl |= MODEL_KEY_BL_SLOW_FLASH;
			break;

		case EXT_LED_FLASH_FAST:
			ctrl |= MODEL_KEY_BL_FAST_FLASH;
			break;

		default:
			ctrl |= MODEL_KEY_BL_OFF;
			break;
	}

    dprintf("m4Addr=%d, ctrl=0x%x\n", m4Addr, ctrl);

	return API_ExtModel_Send_Cmd_Ack(m4Addr, CMD_MODEL_KEY_BL, &ctrl, 1);
}

int API_M4_ALL_LED(char addr, EXT_LED_STATE state)
{
	char ctrl = 0;

    ctrl |= MODEL_KEY_BL1;
    ctrl |= MODEL_KEY_BL2;
    ctrl |= MODEL_KEY_BL3;
    ctrl |= MODEL_KEY_BL4;

	switch (state)
	{
		case EXT_LED_ON:
			ctrl |= MODEL_KEY_BL_ON;
			break;
		
		case EXT_LED_FLASH_SLOW:
			ctrl |= MODEL_KEY_BL_SLOW_FLASH;
			break;

		case EXT_LED_FLASH_FAST:
			ctrl |= MODEL_KEY_BL_FAST_FLASH;
			break;

		default:
			ctrl |= MODEL_KEY_BL_OFF;
			break;
	}

	return API_ExtModel_Send_Cmd_Ack(addr, CMD_MODEL_KEY_BL, &ctrl, 1);
}

int API_M4_PART_LED(char addr, char ctrl_para,EXT_LED_STATE state)
{
	char ctrl = 0;

   // ctrl |= MODEL_KEY_BL1;
    //ctrl |= MODEL_KEY_BL2;
   // ctrl |= MODEL_KEY_BL3;
   // ctrl |= MODEL_KEY_BL4;
	ctrl=ctrl_para;
	switch (state)
	{
		case EXT_LED_ON:
			ctrl |= MODEL_KEY_BL_ON;
			break;
		
		case EXT_LED_FLASH_SLOW:
			ctrl |= MODEL_KEY_BL_SLOW_FLASH;
			break;

		case EXT_LED_FLASH_FAST:
			ctrl |= MODEL_KEY_BL_FAST_FLASH;
			break;

		default:
			ctrl |= MODEL_KEY_BL_OFF;
			break;
	}

	return API_ExtModel_Send_Cmd_Ack(addr, CMD_MODEL_KEY_BL, &ctrl, 1);
}

int API_ALL_M4_LED(EXT_LED_STATE state)
{
	char ctrl = 0;

    ctrl |= MODEL_KEY_BL1;
    ctrl |= MODEL_KEY_BL2;
    ctrl |= MODEL_KEY_BL3;
    ctrl |= MODEL_KEY_BL4;

	switch (state)
	{
		case EXT_LED_ON:
			ctrl |= MODEL_KEY_BL_ON;
			break;
		
		case EXT_LED_FLASH_SLOW:
			ctrl |= MODEL_KEY_BL_SLOW_FLASH;
			break;

		case EXT_LED_FLASH_FAST:
			ctrl |= MODEL_KEY_BL_FAST_FLASH;
			break;

		default:
			ctrl |= MODEL_KEY_BL_OFF;
			break;
	}

	return API_ExtModel_Broadcast_Cmd(CMD_MODEL_KEY_BL, &ctrl, 1);
}

static int drM4DisplayStandbyTime;
static int DR_M4_DisplayStandby(int timing)
{
	if(timing >= drM4DisplayStandbyTime)
	{
		drM4DisplayStandbyTime += 30;
		API_MDS_Standby();
		return 1;
	}

	return 0;
}


/*********************************************************************************************************
**  End Of File
*********************************************************************************************************/

