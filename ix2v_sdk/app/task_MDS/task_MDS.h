/**
  ******************************************************************************
  * @file    task_MDS.h
  * @author  czb
  * @version V00.01.00
  * @date    2022.5.17
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 

#ifndef _task_MDS_H
#define _task_MDS_H

#include "RTOS.h"
#include "utility.h"

// Define Object Property-------------------------------------------------------
//协议栈接口层APCI定义
// S4_ADDR: 0x00~0x7F
#define MK_MODEL_ADDR       0x80
#define DR_MODEL_ADDR       0x81
#define MODEL_BRD_ADDR      0x88

#define CMD_MODEL_GET_VERSION_REQ       0x09
#define CMD_MODEL_GET_VERSION_RSP       0x0A

#define CMD_MODEL_RF_ID             0x0D
#define CMD_MODEL_ON_LINE_SEARCH    0x0E
#define CMD_MODEL_KEY_BL            0x2F
#define CMD_MODEL_RESTART           0x30
#define CMD_MODEL_TEST_ON           0x31
#define CMD_MODEL_TEST_OFF          0x32

#define CMD_MODEL_DR_PRINT              0x10
#define CMD_MODEL_DR_WRITE              0x11
#define CMD_MODEL_DR_CLR                0x12
#define CMD_MODEL_DR_CLR_ALL            0x13
#define CMD_MODEL_DR_CURSOR_SET         0x14
#define CMD_MODEL_DR_CURSOR_ON          0x15
#define CMD_MODEL_DR_CURSOR_OFF         0x16
#define CMD_MODEL_DR_CURSOR_BLINK       0x17
#define CMD_MODEL_DR_CURSOR_NO_BLINK    0x18
#define CMD_MODEL_DR_RESUME             0x19
#define CMD_MODEL_DR_HIDE               0x1A
#define CMD_MODEL_DR_SCROLL_ON          0x1B
#define CMD_MODEL_DR_SCROLL_OFF         0x1C
#define CMD_MODEL_DR_DISP_BMP           0x1D
#define CMD_MODEL_DR_DISP_GIF           0x1E
#define CMD_MODEL_DR_DISP_BMP_DAT       0x1F
#define CMD_MODEL_DR_TIME_LAPSE_ON      0x20
#define CMD_MODEL_DR_TIME_LAPSE_OFF     0x21
#define CMD_MODEL_DR_DISP_DIALOGE       0x22

#define MODEL_KEY_BL1   0x10
#define MODEL_KEY_BL2   0x20
#define MODEL_KEY_BL3   0x40
#define MODEL_KEY_BL4   0x80

#define MODEL_KEY_BL_OFF        0x00
#define MODEL_KEY_BL_ON         0x01
#define MODEL_KEY_BL_FAST_FLASH 0x02
#define MODEL_KEY_BL_SLOW_FLASH 0x03

#define MODEL_KEY_BL_BRIGHT1    0x00
#define MODEL_KEY_BL_BRIGHT2    0x04
#define MODEL_KEY_BL_BRIGHT3    0x08

#define MDS_MSG_LEN_MAX    100
#define MDS_MODULE_MAX    20

typedef enum
{
    MDS_INPUT = 0,
    MDS_DISP_STANDBY,
}MDS_MSG_TYPE;

typedef struct 
{
    MDS_MSG_TYPE	type;
    char data[MDS_MSG_LEN_MAX];
} MDS_MSG_T;

typedef struct 
{
    char* name;
    int addr;
    int version;
    int online;
} MODULE_STATE_T;

typedef struct
{
    vdp_task_t			  		    task;
    Loop_vdp_common_buffer  	msgQ;
    MODULE_STATE_T onlineState[MDS_MODULE_MAX];
    int extModuleCnt;
    unsigned long long lastTime;
	pthread_mutex_t	lock;
}MDS_RUN_S;

int	API_MDS_RcvUart(MDS_MSG_TYPE msgType, char* pdata, int len);

#endif


