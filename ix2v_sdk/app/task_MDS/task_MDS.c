/**
  ******************************************************************************
  * @file    task_MDS.c
  * @author  czb
  * @version V00.01.00
  * @date    2022.5.17
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 

#include <stdio.h>
#include <sys/sysinfo.h>
#include "task_MDS.h"
#include "obj_AiLayer.h"
#include "task_survey.h"
#include "obj_MDS_KeyPad.h"
#include "obj_MDS_Output.h"
#include "obj_MDS_Input.h"
#include "obj_MDS_DR.h"
#include "obj_PublicInformation.h"

MDS_RUN_S MDS_Run = {.lastTime = 0};

static void MDS_WriteStateToPB(void)
{	
	int i;
    cJSON * moduls = cJSON_CreateArray();

	pthread_mutex_lock(&MDS_Run.lock);
	for(i = 0; i < MDS_Run.extModuleCnt; i++)
	{
		if(MDS_Run.onlineState[i].online)
		{
		    cJSON * oneModuls = cJSON_CreateObject();
			cJSON_AddStringToObject(oneModuls, "name", MDS_Run.onlineState[i].name);
			cJSON_AddNumberToObject(oneModuls, "addr", MDS_Run.onlineState[i].addr);
			cJSON_AddNumberToObject(oneModuls, "version", MDS_Run.onlineState[i].version);
			cJSON_AddBoolToObject(oneModuls, "online", MDS_Run.onlineState[i].online);
			cJSON_AddItemToArray(moduls, oneModuls);
		}
	}
	pthread_mutex_unlock(&MDS_Run.lock);

	API_PublicInfo_Write(PB_MDS_MODULS, moduls);
	cJSON_Delete(moduls);
}

static void MDS_GetVersionProcess(void *ptrMsgDat)
{	
	int i;
	M_AI_DATA *ptrCMdIo = (M_AI_DATA *)ptrMsgDat;
	
	pthread_mutex_lock(&MDS_Run.lock);
	for(i = 0; i < MDS_Run.extModuleCnt; i++)
	{
		if(MDS_Run.onlineState[i].addr == ptrCMdIo->sa)
		{
			MDS_Run.onlineState[i].version = ptrCMdIo->data[0];
			MDS_Run.onlineState[i].online = 1;
			break;
		}
	}
	pthread_mutex_unlock(&MDS_Run.lock);

	if(i < MDS_Run.extModuleCnt)
	{
		MDS_WriteStateToPB();
	}
}

void MDS_CmdRspProcess(void *ptrMsgDat)
{	
	M_AI_DATA *ptrCMdIo = (M_AI_DATA *)ptrMsgDat;
	switch (ptrCMdIo->cmd)
	{
		case CMD_MODEL_GET_VERSION_RSP:
			MDS_GetVersionProcess(ptrMsgDat);
			break;

		default:
			MDS_InputProcess(ptrMsgDat);
			break;
	}
}

static void MDS_OnlineReport(void *ptrMsgDat)
{	
	M_AI_DATA *ptrCMdIo = (M_AI_DATA *)ptrMsgDat;
	int i;
	int updateFlag = 0;

	//收到指令，说明模块在线
	pthread_mutex_lock(&MDS_Run.lock);
	for(i = 0; i < MDS_Run.extModuleCnt; i++)
	{
		if(MDS_Run.onlineState[i].addr == ptrCMdIo->sa)
		{
			if(MDS_Run.onlineState[i].online == 0)
			{
				MDS_Run.onlineState[i].online = 1;
				updateFlag = 1;
			}
			break;
		}
	}
	pthread_mutex_unlock(&MDS_Run.lock);

	if(updateFlag)
	{
		MDS_WriteStateToPB();
	}
}

static void vdp_event_mesg_data_process(char* msg_data, int len)
{
	MDS_MSG_T *pMsg = (MDS_MSG_T*)msg_data;

	switch (pMsg->type)
	{
		case MDS_INPUT:
			MDS_CmdRspProcess(pMsg->data);
			MDS_OnlineReport(pMsg->data);
			break;

		case MDS_DISP_STANDBY:
			if(IfKeyPadIdle())
			{
				DR_StandbyProcess();
			}
			M4_CallDisplayProcess(STANDBY, 0);
			break;

		default:
			break;
	}
}


static void MSD_StateInit(void)
{
	pthread_mutex_lock(&MDS_Run.lock);
	for(MDS_Run.extModuleCnt = 0; MDS_Run.extModuleCnt < 16; MDS_Run.extModuleCnt++)
	{
		MDS_Run.onlineState[MDS_Run.extModuleCnt].name = "M4";
		MDS_Run.onlineState[MDS_Run.extModuleCnt].addr = MDS_Run.extModuleCnt;
		MDS_Run.onlineState[MDS_Run.extModuleCnt].version = 0;
		MDS_Run.onlineState[MDS_Run.extModuleCnt].online = 0;
	}

	MDS_Run.onlineState[MDS_Run.extModuleCnt].name = "MK";
	MDS_Run.onlineState[MDS_Run.extModuleCnt].addr = MK_MODEL_ADDR;
	MDS_Run.onlineState[MDS_Run.extModuleCnt].version = 0;
	MDS_Run.onlineState[MDS_Run.extModuleCnt].online = 0;
	MDS_Run.extModuleCnt++;

	MDS_Run.onlineState[MDS_Run.extModuleCnt].name = "DR";
	MDS_Run.onlineState[MDS_Run.extModuleCnt].addr = DR_MODEL_ADDR;
	MDS_Run.onlineState[MDS_Run.extModuleCnt].version = 0;
	MDS_Run.onlineState[MDS_Run.extModuleCnt].online = 0;
	MDS_Run.extModuleCnt++;

	pthread_mutex_unlock(&MDS_Run.lock);

	KeyPad_Manage_Init(1);
}

static void* vdp_MDS_task( void* arg )
{
	vdp_task_t*	 ptask 		= (vdp_task_t*)arg;
	p_vdp_common_buffer pdb 	= 0;
	int	size;

	API_MDS_CheckOnline();
	DR_StandbyProcess();
	ptask->task_StartCompleted = 1;
	while( ptask->task_run_flag )
	{
		size = pop_vdp_common_queue( ptask->p_msg_buf, &pdb, VDP_QUEUE_POLLING_TIME);
		if( size > 0 )
		{
			(*ptask->p_msg_buf->process)(pdb,size);
			purge_vdp_common_queue( ptask->p_msg_buf );
		}
	}
	return 0;
}


int vtk_TaskInit_MDS(void)
{
	init_vdp_common_queue(&MDS_Run.msgQ, 1000, (msg_process)vdp_event_mesg_data_process, &MDS_Run.task);
	init_vdp_common_task(&MDS_Run.task, MSG_ID_MDS, vdp_MDS_task, &MDS_Run.msgQ, NULL);
	MSD_StateInit();
}


int	API_MDS_RcvUart(MDS_MSG_TYPE msgType, char* pdata, int len)
{
	if(MDS_Run.task.task_run_flag == 0 || len > MDS_MSG_LEN_MAX)
	{
		return -1;
	}

	MDS_MSG_T msg;
	msg.type = msgType;
	if(len && pdata)
	{
		memcpy(msg.data, pdata, len);
	}

	return push_vdp_common_queue(MDS_Run.task.p_msg_buf, &msg, sizeof(msg) - MDS_MSG_LEN_MAX + len);
}

int	API_MDS_Standby(void)
{
	if(MDS_Run.task.task_run_flag == 0)
	{
		return -1;
	}

	MDS_MSG_T msg;
	msg.type = MDS_DISP_STANDBY;

	return push_vdp_common_queue(MDS_Run.task.p_msg_buf, &msg, sizeof(msg.type));
}

int IsMDS_Online(int addr)
{
	int i;
	int ret = 0;

	pthread_mutex_lock(&MDS_Run.lock);
	for(i = 0; i < MDS_Run.extModuleCnt; i++)
	{
		if(MDS_Run.onlineState[i].addr == addr)
		{
			ret = MDS_Run.onlineState[i].online;
			break;
		}
	}
	pthread_mutex_unlock(&MDS_Run.lock);

	return ret;
}

int	API_MDS_CheckOneOnline(int addr)
{
	int online, i;

	for(online = 0, i = 0; i < MDS_Run.extModuleCnt; i++)
	{
		if(MDS_Run.onlineState[i].addr == addr)
		{
			pthread_mutex_lock(&MDS_Run.lock);
			online = MDS_Run.onlineState[i].online;
			pthread_mutex_unlock(&MDS_Run.lock);
			if(!online)
			{
				online = API_ExtModel_CheckOnline(addr);
				if(online)
				{
					pthread_mutex_lock(&MDS_Run.lock);
					MDS_Run.onlineState[i].online = online;
					pthread_mutex_unlock(&MDS_Run.lock);
					MDS_WriteStateToPB();
				}
			}
			break;
		}
	}

	return online;
}

int	API_MDS_CheckOnline(void)
{
	int i;
	int online;

	for(i = 0; i < MDS_Run.extModuleCnt; i++)
	{
		online = API_ExtModel_CheckOnline(MDS_Run.onlineState[i].addr);
		pthread_mutex_lock(&MDS_Run.lock);
		MDS_Run.onlineState[i].online = online;
		pthread_mutex_unlock(&MDS_Run.lock);
	}
	MDS_WriteStateToPB();

	//第二次检测DR模块，以免检测漏掉
	API_MDS_CheckOneOnline(DR_MODEL_ADDR);
}


/*********************************************************************************************************
**  End Of File
*********************************************************************************************************/

