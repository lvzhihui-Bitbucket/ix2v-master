/**
  ******************************************************************************
  * @file    obj_MDS_KeyPad.c
  * @author  czb
  * @version V00.01.00
  * @date    2022.5.17
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 

#include "cJSON.h"
#include "OSTIME.h"
#include "obj_MDS_KeyPad.h"
#include "obj_MDS_Output.h"
#include "task_CallServer.h"
#include "obj_MDS_DR.h"
#include "task_Beeper.h"
#include "define_file.h"
#include "obj_IoInterface.h"
#include "obj_UnitSR.h"
#include "task_Event.h"
#include "obj_KeypadLed.h"
#include "obj_gpio.h"
#include "obj_TableSurver.h"

static KEY_MANAGE_RUN	    KeyPad_manage_run = {.pwdFree = 0, .settingCodeCfg = NULL};

static void KeypadBacklightCtrl(int onOff);
static int CheckPrivateUnlockPwd(char* pwd, int pwdLen);
static int SettingCodeSaveProcess(char* settingCode);

static int Callback_Keypad_Manage(int timing)
{
	if(timing >= 1)
	{
		if((KeyPad_manage_run.key_bl_all==0&&KeyPad_manage_run.key_bl)
			||(KeyPad_manage_run.key_bl_all==1&&KeyPad_manage_run.state!=KEY_MANAGE_PASSWORD_INPUT)
			||KeyPad_manage_run.key_input_len)	
		{
			KeyPad_manage_run.key_free_cnt++;
			if (KeyPad_manage_run.key_free_cnt>=10)	//10S
			{
				if(KeyPad_manage_run.key_bl_all==1)
					KeyPad_manage_run.key_bl=1;
				else
					KeyPad_manage_run.key_bl=0;
				KeyPad_manage_run.key_free_cnt = 0;
				if(KeyPad_manage_run.key_input_len)
				{
					API_EventDebug(EventInputCancel, "Timeout cancel: state: %s; input:%s", (KeyPad_manage_run.state == KEY_MANAGE_PASSWORD_INPUT) ? "PASSWORD_INPUT" : "SETTING_CODE_INPUT", KeyPad_manage_run.key_input_buf);
				}
				KeyPad_Manage_Init(0);
				if(KeyPad_manage_run.key_bl_all==1)
					return 1;
				return 2;
			}			
		}
		return 1;
	}
	return 0;
}

//10分钟退出免密模式
static int DisablePwdFreeTimer(int timing)
{
	DisablePwdFree(NULL);
	return 2;
}

int EnablePwdFree(char* value)
{
	int time;
	KeyPad_manage_run.pwdFree = 1;
	time = ((value == NULL) ? 600 : atoi(value));
	time = (time <= 0 ? 600 : time);

	API_Add_TimingCheck(DisablePwdFreeTimer, time);
	return 1;
}

int DisablePwdFree(char* value)
{
	KeyPad_manage_run.pwdFree = 0;

	return 1;
}

int SetPrivatePwd(char* value)
{
	int ret;
	cJSON* Where;
	cJSON* newitem;
	cJSON* View;
	int id, inputLen;
	char idString[4];
	inputLen = strlen(value);

	if(value == NULL || inputLen != (3 + API_Para_Read_Int(AutoUnlockPwdLen)))
	{
		return 0;
	}

	snprintf(idString, 4, "%s", value);
	id = atoi(idString);

    Where = cJSON_CreateObject();
	cJSON_AddNumberToObject(Where, "ID", id);

	newitem = cJSON_CreateObject();
	cJSON_AddStringToObject(newitem, "IX_ADDR", "");
	cJSON_AddNumberToObject(newitem, "ID", id);
	cJSON_AddStringToObject(newitem, "CODE", value+3);
	cJSON_AddStringToObject(newitem, "NOTE", "");

	View = cJSON_CreateArray();
	cJSON_AddItemToArray(View, newitem);

	if(API_TB_SelectBySortByName("PRIVATE_PWD", View, Where, 0))
	{
		newitem = cJSON_GetArrayItem(View, 1);
		cJSON_ReplaceItemInObjectCaseSensitive(newitem, "CODE", cJSON_CreateString(value+3));
		ret = API_TB_UpdateByName("PRIVATE_PWD", Where, newitem);
	}
	else
	{
		ret = API_TB_AddByName("PRIVATE_PWD", newitem);
	}

	cJSON_Delete(Where);
	cJSON_Delete(View);

	return ret;
}

int SettingCodeFirmwareUpdate(char* value)
{
	return API_Event_UpdateStart(API_Para_Read_String2(FW_UpdateServer), API_Para_Read_String2(FW_UpdateCode), 1, "Setting code");
}

int SettingCodeFactoryDefault(char* value)
{
	FactoryDefaultProcess();
	sleep(3);
	return 1;
}

int SettingCodeReboot(char* value)
{
	BEEP_CONFIRM();
	HardwareRestart("SettingCode");
	return 1;
}

//检测是否可以出外网
int SettingCodeCheckNetwork(char* value)
{
	char* sipNetwork = API_PublicInfo_Read_String(PB_SIP_CONNECT_NETWORK);
	if(sipNetwork)
	{
		#if defined(PID_IX611)
		if(GetKeyType())
		{
			if(!strcmp(sipNetwork, "LAN") || !strcmp(sipNetwork, "4G") || !strcmp(sipNetwork, "WLAN"))
			{
				API_Beep(BEEP_TYPE_DL1DI1);
			}
			else
			{
				API_Beep(BEEP_TYPE_DI3);
			}
			sleep(1);
		}
		else
		{
			if(!strcmp(sipNetwork, "LAN") || !strcmp(sipNetwork, "4G") || !strcmp(sipNetwork, "WLAN"))
			{
				API_KeypadLedCtrl(LED_BLUE, KEYPAD_LED_ON);
			}
			else
			{
				API_KeypadLedCtrl(LED_RED, KEYPAD_LED_ON);
			}
			sleep(1);
			KeypadBacklightCtrl(1);
		}
		#endif
		return 1;
	}

	return 0;
}

int SettingCodeCall(char* value)
{
	API_Event_IXCallAction("IxMainCall", value, 1);
	return 1;
}

//检测IP获取方式
int SettingCodeCheckIP_Policy(char* value)
{
	char* policy = API_PublicInfo_Read_String(PB_LAN_POLICY);
	if(policy)
	{
		#if defined(PID_IX611)
		if(GetKeyType())
		{
			if(!strcmp(policy, "DHCP"))
			{
				API_Beep(BEEP_TYPE_DL1);
			}
			else if(!strcmp(policy, "AutoIP"))
			{
				API_Beep(BEEP_TYPE_DL1DI1);
			}
			else if(!strcmp(policy, "StaticIP"))
			{
				API_Beep(BEEP_TYPE_DI3);
			}
			sleep(1);
		}
		else
		{
			if(!strcmp(policy, "DHCP"))
			{
				API_KeypadLedCtrl(LED_BLUE, KEYPAD_LED_ON);
			}
			else if(!strcmp(policy, "AutoIP"))
			{
				API_KeypadLedCtrl(LED_RED, KEYPAD_LED_ON);
			}
			else if(!strcmp(policy, "StaticIP"))
			{
				API_KeypadLedCtrl(LED_GREEN, KEYPAD_LED_ON);
			}
			sleep(1);
			KeypadBacklightCtrl(1);
		}
		#endif
		return 1;
	}

	return 0;
}

//Pin内网
int SettingCodePing(char* value)
{
	char ipString[20];
	char tempString[12];
	int ip = inet_addr(GetSysVerInfo_IP());
	int valueLen = 0;
	int i,j, ret;

	if(value == NULL || (valueLen = strlen(value)) == 0)
	{
		return 0;
	}

	snprintf(tempString, 13, "%03d%03d%03d%03d", ip&0xFF, (ip>>8)&0xFF, (ip>>16)&0xFF, (ip>>24)&0xFF);
	if(valueLen <= 12)
	{
		for(i = 12 - valueLen, j = 0; i < 12; i++)
		{
			tempString[i] = value[j++];
		}
		strcpy(ipString, "000.000.000.000");
		memcpy(ipString, tempString, 3);
		memcpy(ipString+4, tempString+3, 3);
		memcpy(ipString+8, tempString+6, 3);
		memcpy(ipString+12, tempString+9, 3);
		ret = PingNetwork(ipString);
	#if defined(PID_IX611)
		if(GetKeyType())
		{
			if(ret)
			{
				API_Beep(BEEP_TYPE_DL1DI1);
			}
			else
			{
				API_Beep(BEEP_TYPE_DI3);
			}
			sleep(1);
		}
		else
		{
			if(!ret)
			{
				API_KeypadLedCtrl(LED_BLUE, KEYPAD_LED_ON);
			}
			else
			{
				API_KeypadLedCtrl(LED_RED, KEYPAD_LED_ON);
			}
			sleep(1);
			KeypadBacklightCtrl(1);
		}
	#endif

		return 1;
	}
	else
	{
		return 0;
	}
}

//设置类似IP的IO参数
static int SetSimilarToIp(char* ioName, char* value)
{
	char ipString[20];

	if(value == NULL || strlen(value) != 12)
	{
		return 0;
	}

	strcpy(ipString, "000.000.000.000");
	memcpy(ipString, value, 3);
	memcpy(ipString+4, value+3, 3);
	memcpy(ipString+8, value+6, 3);
	memcpy(ipString+12, value+9, 3);

	return API_Para_Write_String(ioName, ipString);
}

//设置LAN网卡IP
int SettingCodeSetLanIp(char* value)
{
	return SetSimilarToIp(IP_Address, value);
}
//设置LAN网卡SubnetMask
int SettingCodeSetLanSubnetMask(char* value)
{
	return SetSimilarToIp(SubnetMask, value);
}
//设置LAN网卡Route
int SettingCodeSetLanRoute(char* value)
{
	return SetSimilarToIp(DefaultRoute, value);
}

//设置AutoIpMask
int SettingCodeSetAutoIpMask(char* value)
{
	return SetSimilarToIp(Autoip_IPMask, value);
}
//设置AutoIpGateway
int SettingCodeSetAutoIpGateway(char* value)
{
	return SetSimilarToIp(Autoip_IPGateway, value);
}
//设置AutoIpSegment
int SettingCodeSetAutoIpSegment(char* value)
{
	return SetSimilarToIp(Autoip_IPSegment, value);
}

static int SetLanByPolicy(char* policy)
{
	int ret;
	cJSON *set_para =  cJSON_CreateObject();
	cJSON_AddStringToObject(set_para, Lan_policy, policy);
	ret = API_Set_Lan_Config(set_para);
	cJSON_Delete(set_para);
	return ret;
}
//设置IP_Policy
int SettingCodeSetIP_Policy(char* value)
{
	int ret = 0;
	if(value && strlen(value) > 0)
	{
		if(atoi(value))
		{
			ret = API_Para_Write_String(Lan_policy, "Static");
		}
		else
		{
			ret = API_Para_Write_String(Lan_policy, "AutoIPAndDHCP");
		}
		if(ret)
		{
			SetLanByPolicy(API_Para_Read_String2(Lan_policy));
		}
	}

	return ret;
}

//恢复Lan设置
int SettingCodeRestoreLan(char* value)
{
	int ret = 0;
	ret = API_Para_Restore_Default(LANConfig);
	if(ret)
	{
		SetLanByPolicy(API_Para_Read_String2(Lan_policy));
	}

	return ret;
}
#if defined(PID_IX611)
int SettingCodeSetVoiceLanguage(char* value)
{
	int ret = 0;
	int i;
	int str_len;
	char	path[100];
	char	displayName[30];
	if(value && (str_len=strlen(value)) > 0)
	{
		if(GetVoiceLanguageListCnt()==0)
			VoiceLanguageListInit();
		for(i=0;i<GetVoiceLanguageListCnt();i++)
		{
			if(GetVoiceLanguageInfo(i,path,displayName)==0)
			{
				printf("%s:1111%s:%s \n",value,path,displayName);
				if(memcmp(displayName,value,str_len)==0)
				{
					ret = API_Para_Write_String(VOICE_LANGUAGE_SELECT_DIR, path);
					break;
				}
				
			}
		}
		
	}


	return ret;
}
#endif

//无条件更新
int SettingCodeUpdate(char* value)
{
	return API_Event_UpdateStart(API_Para_Read_String2(FW_UpdateServer), value, 0, "Setting code");
}

static void KeypadBacklightCtrl(int onOff)
{
	if(onOff)
	{
		#if defined( PID_IX611 )
		if(!strcmp(API_PublicInfo_Read_String(PB_CALL_SER_STATE), "Wait"))
		{
			if(GetKeyType())
			{
				if(KeyPad_manage_run.state == KEY_MANAGE_PASSWORD_INPUT)
				{
					API_KeypadLedCtrl(LED_MK_BL, KEYPAD_LED_ON);
				}
				else
				{
					API_KeypadLedCtrl(LED_MK_BL, KEYPAD_LED_FLASH_SLOW);
				}
			}
			else
			{
				if(KeyPad_manage_run.state == KEY_MANAGE_PASSWORD_INPUT)
				{
					API_KeypadLedCtrl(LED_BLUE, KEYPAD_LED_ON);
				}
				else
				{
					API_KeypadLedCtrl(LED_PURPLE, KEYPAD_LED_ON);
				}
			}
		}
		#else
		API_MK_LED(EXT_LED_ON);
		#endif
	}
	else
	{
		#if defined( PID_IX611 )
		if(!strcmp(API_PublicInfo_Read_String(PB_CALL_SER_STATE), "Wait"))
		{
			if(GetKeyType())
			{
				API_KeypadLedCtrl(LED_MK_BL, KEYPAD_LED_OFF);
			}
			else
			{
				API_KeypadLedCtrl(LED_BLUE, KEYPAD_LED_OFF);
			}
		}
		#else
		API_MK_LED(EXT_LED_OFF);
		#endif
	}
}

static void KeypadMenuCtrl(void)
{
	if(KeyPad_manage_run.state == KEY_MANAGE_PASSWORD_INPUT)
	{
		#if	defined(PID_IX850)
		CodePadDisplay(STANDBY, NULL, 0);
		#endif
	}
	else
	{
		#if	defined(PID_IX850)
		CodePadDisplay(STANDBY, NULL, 0);
		#endif
	}
}
/*------------------------------------------------------------------------
								���������ʼ��
------------------------------------------------------------------------*/
void KeyPad_Manage_Init(int init)	//read
{
	#ifdef PID_IX821
	KeyPad_manage_run.state = KEY_MANAGE_RoomNumber_INPUT;
	#else
	KeyPad_manage_run.state = (KeyPad_manage_run.pwdFree ? KEY_MANAGE_SettingCode_INPUT : KEY_MANAGE_PASSWORD_INPUT);
	#endif
	KeyPad_manage_run.pwdLen = API_Para_Read_Int(AutoUnlockPwdLen);
	KeyPad_manage_run.key_bl_all=API_Para_Read_Int(BL_ON_ALAWYS);
	key_input_init();	
	#if	defined(PID_IX821)
	DR_MenuInputProcess(STANDBY, NULL, 0);
	#endif
	if(init)
	{
		KeyPad_manage_run.key_bl=0;
		KeyPad_manage_run.key_free_cnt = 0;
		if(KeyPad_manage_run.settingCodeCfg == NULL)
		{
			KeyPad_manage_run.settingCodeCfg = GetJsonFromFile(SettingCode_File_Name);
		}
		API_Add_TimingCheck(Callback_Keypad_Manage, 1);
		API_AddSettingCallbackFunction("EnablePwdFree", EnablePwdFree);
		API_AddSettingCallbackFunction("DisablePwdFree", DisablePwdFree);
		API_AddSettingCallbackFunction("SetPrivatePwd", SetPrivatePwd);
		API_AddSettingCallbackFunction("SettingCodeFirmwareUpdate", SettingCodeFirmwareUpdate);
		API_AddSettingCallbackFunction("SettingCodeFactoryDefault", SettingCodeFactoryDefault);
		API_AddSettingCallbackFunction("SettingCodeReboot", SettingCodeReboot);
		API_AddSettingCallbackFunction("SettingCodeCheckNetwork", SettingCodeCheckNetwork);	
		API_AddSettingCallbackFunction("SettingCodeCall", SettingCodeCall);
		API_AddSettingCallbackFunction("SettingCodeCheckIP_Policy", SettingCodeCheckIP_Policy);
		API_AddSettingCallbackFunction("SettingCodePing", SettingCodePing);
		API_AddSettingCallbackFunction("SettingCodeSetLanIp", SettingCodeSetLanIp);
		API_AddSettingCallbackFunction("SettingCodeSetLanSubnetMask", SettingCodeSetLanSubnetMask);
		API_AddSettingCallbackFunction("SettingCodeSetLanRoute", SettingCodeSetLanRoute);
		API_AddSettingCallbackFunction("SettingCodeSetAutoIpMask", SettingCodeSetAutoIpMask);
		API_AddSettingCallbackFunction("SettingCodeSetAutoIpGateway", SettingCodeSetAutoIpGateway);
		API_AddSettingCallbackFunction("SettingCodeSetAutoIpSegment", SettingCodeSetAutoIpSegment);
		API_AddSettingCallbackFunction("SettingCodeSetIP_Policy", SettingCodeSetIP_Policy);
		API_AddSettingCallbackFunction("SettingCodeRestoreLan", SettingCodeRestoreLan);

		#if defined(PID_IX611)
		API_AddSettingCallbackFunction("SettingCodeSetVoiceLanguage", SettingCodeSetVoiceLanguage);
		#endif

		API_AddSettingCallbackFunction("SettingCodeUpdate", SettingCodeUpdate);
	}
	
	if(KeyPad_manage_run.key_bl_all)
	{
		SetKeyPadBackLightState(1);
	}

	if(KeyPad_manage_run.key_bl)
	{
		KeypadBacklightCtrl(1);
	}
	else
	{
		KeypadBacklightCtrl(0);
	}
	KeypadMenuCtrl();
}

void API_AddSettingCallbackFunction(char* name, SettingCallback fun)
{
	if(KeyPad_manage_run.settingCallbackTable == NULL)
	{
		KeyPad_manage_run.settingCallbackTable = cJSON_CreateObject();
	}
	while(cJSON_GetObjectItemCaseSensitive(KeyPad_manage_run.settingCallbackTable, name))
	{
		cJSON_DeleteItemFromObjectCaseSensitive(KeyPad_manage_run.settingCallbackTable, name);
	}
	cJSON_AddNumberToObject(KeyPad_manage_run.settingCallbackTable, name, (int)fun);
}

SettingCallback API_GetSettingCallbackFunction(char* name)
{
	cJSON * fun;
	SettingCallback ret = 0;

	fun = cJSON_GetObjectItemCaseSensitive(KeyPad_manage_run.settingCallbackTable, name);
	if(fun)
	{
		ret = fun->valueint;
	}

	return ret;
}

static char* GetCfgValueBySettingCode(char* settingCode, char* keyword)
{
	cJSON* para;
	cJSON* ret;
	char* retStr;
	char* pbioStr;

	if(settingCode == NULL || strlen(settingCode) != 4)
	{
		return NULL;
	}

	switch(settingCode[0])
	{
		case '1':
		case '2':
		case '3':
			para = cJSON_GetObjectItemCaseSensitive(cJSON_GetObjectItemCaseSensitive(KeyPad_manage_run.settingCodeCfg, "IO"), settingCode+1);
			ret = cJSON_GetObjectItemCaseSensitive(para, keyword);
			retStr = cJSON_GetStringValue(ret);
			break;

		case '4':
			para = cJSON_GetObjectItemCaseSensitive(cJSON_GetObjectItemCaseSensitive(KeyPad_manage_run.settingCodeCfg, "PB"), settingCode+1);
			ret = cJSON_GetObjectItemCaseSensitive(para, keyword);
			retStr = cJSON_GetStringValue(ret);
			break;

		default:
			para = cJSON_GetObjectItemCaseSensitive(cJSON_GetObjectItemCaseSensitive(KeyPad_manage_run.settingCodeCfg, "OTHERS"), settingCode);
			ret = cJSON_GetObjectItemCaseSensitive(para, keyword);
			retStr = cJSON_GetStringValue(ret);
			break;
	}

	return retStr;
}


static int DisplayPBIOBySettingCode(char* settingCode)
{
	char value[100];
	cJSON* para;
	char* name;
	char* dispStr;
	if(settingCode == NULL || strlen(settingCode) != 4)
	{
		return 0;
	}

	name = GetCfgValueBySettingCode(settingCode, "Name");
	if(name == NULL)
	{
		return 0;
	}

	dispStr = GetCfgValueBySettingCode(settingCode, "Display");
	if(dispStr == NULL)
	{
		dispStr = name;
	}
	
	switch(settingCode[0])
	{
		case '1':
		case '2':
		case '3':
			para = API_Para_Read_Public(name);
			break;
		case '4':
			para = API_PublicInfo_Read(name);
			break;

		default:
			return 0;
	}

	if(cJSON_IsString(para))
	{
		snprintf(value, 12, "%s", cJSON_GetStringValue(para));
	}
	else if(cJSON_IsNumber(para))
	{
		snprintf(value, 12, "%d", para->valueint);
	}
	else if(cJSON_IsTrue(para))
	{
		snprintf(value, 12, "true");
	}
	else if(cJSON_IsFalse(para))
	{
		snprintf(value, 12, "false");
	}
	else
	{
		return 0;
	}

	DR_DisplaySettingValue(dispStr, value);
	#ifdef PID_IX850
	char disp_menu[100];
	sprintf(disp_menu,"%s:%s",dispStr, value);
	CodePadDisplay(DISPLAY_SETTING_VALUE,disp_menu, settingCode[0]-'0');
	#endif
	return 1;
}

static int SetIOValueBySettingCode(char* code, char* value)
{
	cJSON* para;
	char* name;
	int ret = 0;

	if(code == NULL || strlen(code) != 4 || code[0] != '2')
	{
		return 0;
	}

	name = GetCfgValueBySettingCode(code, "Name");
	if(name == NULL)
	{
		return 0;
	}

	para = API_Para_Read_Public(name);

	if(cJSON_IsString(para))
	{
		#if defined(PID_IX611)&&defined(DH_LINK)
		if(strcmp(name,IX_ADDR)==0&&value&&strlen(value)<=2&&atoi(value)<=48)
		{
			char ix_addr_buff[11]={"0099000000"};
			memcpy(ix_addr_buff+10-strlen(value),value,strlen(value));
			ret = API_Para_Write_String(name, ix_addr_buff);
		}
		else
			ret = API_Para_Write_String(name, value);
		#else
		ret = API_Para_Write_String(name, value);
		#endif
	}
	else if(cJSON_IsNumber(para) || cJSON_IsBool(para))
	{
		ret = API_Para_Write_Int(name, atoi(value));
	}
	else
	{
		return 0;
	}

	DisplayPBIOBySettingCode(code);

	return ret;
}

static int RestoreIOValueBySettingCode(char* code)
{
	cJSON* para;
	char* name;
	int ret = 0;

	if(code == NULL || strlen(code) != 4 || code[0] != '3')
	{
		return 0;
	}

	name = GetCfgValueBySettingCode(code, "Name");
	if(name == NULL)
	{
		return 0;
	}


	para = API_Para_Read_Public(name);

	if(cJSON_IsString(para) || cJSON_IsNumber(para) || cJSON_IsBool(para))
	{
		ret = API_Para_Restore_Default(name);
	}
	else
	{
		return 0;
	}

	DisplayPBIOBySettingCode(code);

	return ret;
}

static int OthersSettingCodeProcess(char* code, char* value)
{
	cJSON* para;
	char* callbackStr;
	char* dispStr;
	int ret = 0;
	SettingCallback settingCallback;

	if(code == NULL || strlen(code) != 4 || ('1' <= code[0] && code[0] <= '4'))
	{
		return 0;
	}

	callbackStr = GetCfgValueBySettingCode(code, "Callback");
	if(callbackStr == NULL)
	{
		return 0;
	}

	dispStr = GetCfgValueBySettingCode(code, "Display");
	if(dispStr == NULL)
	{
		dispStr = callbackStr;
	}

	settingCallback = API_GetSettingCallbackFunction(callbackStr);
	if(settingCallback == 0)
	{
		return 0;
	}

	ret = (*settingCallback)(value);

	DR_DisplaySettingValue(dispStr, ret ? "Succ" : "Error");

	return ret;
}


static int SettingCodeSaveProcess(char* settingCode)
{
	char code[5];
	char value[50];
	int ret = 0;
	int settingCodeLen;

	if(settingCode == NULL)
	{
		return ret;
	}

	settingCodeLen = strlen(settingCode);

	if(settingCodeLen >= 4)
	{
		memcpy(code, settingCode, 4);
		code[4] = 0;
		strcpy(value, settingCode+4);

		dprintf("code=%s, value=%s\n", code, value);
	
		if(code[0] < '1' || code[0] > '4')
		{
			char* convertCodeStr = GetCfgValueBySettingCode(code, "ConvertCode");
			if(convertCodeStr)
			{
				snprintf(code, 5, "%s", convertCodeStr);
				dprintf("convertCodeStr=%s, value=%s\n", code, value);
			}
		}

		switch (code[0])
		{
			case '1':
			case '4':
				ret = DisplayPBIOBySettingCode(code);
				break;

			case '2':
				ret = SetIOValueBySettingCode(code, value);
				break;

			case '3':
				ret = RestoreIOValueBySettingCode(code);
				break;
			
			default:
				ret = OthersSettingCodeProcess(code, value);
				break;
		}
	}

	//if(!ret)
	//{
	//	ret = ChangeUnlockPassword(settingCode, settingCodeLen);
	//}

	return ret;
}


/*------------------------------------------------------------------------
								���������ʼ��
------------------------------------------------------------------------*/
void key_input_init(void)	//read	
{
	KeyPad_manage_run.key_input_len=0;
	memset(KeyPad_manage_run.key_input_buf,0,INPUT_MAX_LEN+1);
}

void DR_DisplayMessage(char* message)
{
    if(message)
    {
		API_Add_TimingCheck(Callback_Keypad_Manage, 1);
        DR_MenuInputProcess(DISPLAY_SETTING_VALUE, message, strlen(message));
    }
}

int GetKeyPadBackLightState(void)
{
	return KeyPad_manage_run.key_bl;
}

int SetKeyPadBackLightState(state)
{
	if(state && !KeyPad_manage_run.key_bl)
	{
		KeypadBacklightCtrl(1);
		API_Add_TimingCheck(Callback_Keypad_Manage, 1);
	}
	KeyPad_manage_run.key_bl = state;
}

/*------------------------------------------------------------------------
						MK ������
------------------------------------------------------------------------*/
void KeyPad_Manage(unsigned char keyValue)
{
	KeyPad_manage_run.key_free_cnt = 0;
	if(!KeyPad_manage_run.key_bl)
	{
		KeyPad_manage_run.key_bl=1;
		API_Add_TimingCheck(Callback_Keypad_Manage, 1);
		KeypadBacklightCtrl(1);
	}

	int cardState = API_PublicInfo_Read_Int(PB_CardSetup_State);
	if(cardState)
	{
		CardSetupRoom_Input_Process(keyValue);
		return;
	}

	switch (KeyPad_manage_run.state)
	{
		#ifdef PID_IX821
		case KEY_MANAGE_RoomNumber_INPUT:
			RoomNumber_Input_Process(keyValue);
			break;
		#endif
			
		case KEY_MANAGE_PASSWORD_INPUT:
			Password_Input_Process(keyValue);
			break;
		case KEY_MANAGE_SettingCode_INPUT:
			SettingCode_Input_Process(keyValue);
			break;
	}
}

void DT_CallStart(int callNum)
{
	cJSON *call_para,*target;
	char temp[10];
	char sip_acc[40];

	callNum = ((callNum >= 32 || callNum < 0)? 0 : callNum);
	
	call_para=cJSON_CreateObject();
	target=cJSON_AddObjectToObject(call_para,CallPara_Target);
	sprintf(temp,"0x%02x", 0x80 + 4*callNum);
	cJSON_AddStringToObject(target,CallTarget_DtAddr,temp);

	callNum = ((callNum == 0)? 32 : callNum);
	if(judge_dtim_divert_on_by_addr(callNum)&&get_sip_dt_im_acc_from_tb(callNum, sip_acc,NULL)>=0)
	{
		//if(judge_dtim_divert_on_by_addr(callNum))
			target=cJSON_AddObjectToObject(call_para,CallPara_Divert);
		//else
		//	target=cJSON_AddObjectToObject(call_para,CallPara_Divert2);
		cJSON_AddStringToObject(target,CallTarget_SipAcc,sip_acc);
	}

	//��ʾ���з���
	snprintf(temp, 10, "%02d", callNum);
	DR_CallDisplayProcess(CALLING_START, temp);

	//��ʼ����
	API_CallServer_Start(DxMainCall,call_para);
	cJSON_Delete(call_para);
}

static int IsCallTalkState(void)
{
	int ret = 0;
    
	if(API_PublicInfo_Read_Bool(PB_CALL_PART))
	{
		char* callserverState = cJSON_GetStringValue(API_PublicInfo_Read(PB_CALL_SER_STATE));
		if(callserverState)
		{
			if((!strcmp(callserverState, "Ack") || !strcmp(callserverState, "RemoteAck")))
			{
				ret = 1;
			}
		}
	}

	return ret;
}
#if !defined(PID_IX821)
static int GetCallNumber(void)
{
	int callNumber = -1;
	int callNumber_hex;
    
	char* Call_Tar_DtAddr = cJSON_GetStringValue(API_PublicInfo_Read(PB_CALL_TAR_DTADDR));
	if(Call_Tar_DtAddr)
	{
		callNumber_hex = strtoul(Call_Tar_DtAddr,NULL,0);
		callNumber=((callNumber_hex&0x7f)>>2);
		if(callNumber==0)
			callNumber=32;
		//callNumber = atoi(Call_Tar_DtAddr);
	}

	return callNumber;
}
#else

static int GetCallNumber(void)
{
	int callNumber = -1;
	int callNumber_hex;
    
	char* Call_Tar_IxAddr = cJSON_GetStringValue(API_PublicInfo_Read(PB_CALL_TAR_IXADDR));
	if(Call_Tar_IxAddr)
		return IXAddrMappingM4Key(Call_Tar_IxAddr);

	return callNumber;
}
#endif
#if	defined(PID_IX850)
int EventCallbackCallAction(cJSON* event)
{
	char* callData = cJSON_GetStringValue(cJSON_GetObjectItemCaseSensitive(event, "CallData"));
	char* source	=cJSON_GetStringValue(cJSON_GetObjectItemCaseSensitive(event, "Source"));
	char* act_type=cJSON_GetStringValue(cJSON_GetObjectItemCaseSensitive(event, "ActionType"));
	cJSON *target_list=cJSON_GetObjectItemCaseSensitive(event, "TargetList");
	char *call_input;
	if(callData)
	{
		char* callServerStateString = API_PublicInfo_Read_String(PB_CALL_SER_STATE);
		if(callServerStateString == NULL)
		{
			return 0;
		}
		
		
		if(act_type==NULL||strcmp(act_type,"Start")==0)
		{
			if(strcmp(callServerStateString, "Wait"))
			{
				call_input=API_PublicInfo_Read_String(PB_CALL_TAR_INPUT);
				if(((call_input!=NULL&&callData!=NULL&&strcmp(call_input,callData)==0)&&IsCallTalkState() == 0)||(source!=NULL&&strcmp(source,"Menu")))
				{
					API_CallServer_LocalBye();
				}
				else if(call_input!=NULL&&callData!=NULL&&strcmp(call_input,callData)==0)
				{
					API_CallServer_Redail();
				}

			}
			else
			{
				IXMainCall_CallStart(callData,target_list);
			}
		}
		else if(strcmp(act_type,"Cancel")==0)
		{
			API_CallServer_LocalBye();
		}
	}
}
#elif defined(PID_IX821)
int  ifKeyMapping(int key_num)
{
	cJSON *view=cJSON_CreateArray();
	cJSON *where=cJSON_CreateObject();
	cJSON_AddNumberToObject(where,"KEY_ID",key_num);
	if(API_TB_SelectBySortByName(TB_NAME_KEYID_MAPPING, view, where, 0))
	{
		cJSON *one_item;
		cJSON_ArrayForEach(one_item,view)
		{
			int ds_id=GetEventItemInt(one_item, "DS_ID");
			if(ds_id==0||ds_id==atoi(GetSysVerInfo_BdRmMs()+8))
			{
				//if(GetJsonDataPro(one_item, "IX_ADDR", Addr))
				{
					cJSON_Delete(view);
					cJSON_Delete(where);
					return 1;
				}
			}
		}
	}
	cJSON_Delete(view);
	cJSON_Delete(where);
	return 0;
}
int M4KeyMappingAddr_ByInput(int key_num,char *Addr)
{
	cJSON *view=cJSON_CreateArray();
	cJSON *where=cJSON_CreateObject();
	cJSON_AddNumberToObject(where,"KEY_ID",key_num);
	if(API_TB_SelectBySortByName(TB_NAME_KEYID_MAPPING, view, where, 0))
	{
		cJSON *one_item;
		cJSON_ArrayForEach(one_item,view)
		{
			int ds_id=GetEventItemInt(one_item, "DS_ID");
			if(ds_id==0||ds_id==atoi(GetSysVerInfo_BdRmMs()+8))
			{
				if(GetJsonDataPro(one_item, "IX_ADDR", Addr))
				{
					cJSON_Delete(view);
					cJSON_Delete(where);
					return 0;
				}
			}
		}
	}
	cJSON_Delete(view);
	cJSON_Delete(where);
	if(API_PublicInfo_Read_Int(PB_M4_KEY_MAPPING)==0)
	{
		if(Addr)
			sprintf(Addr,"%s%04d",GetSysVerInfo_bd(),key_num);
	}
	else
	{
		int dxg_id=(key_num-1)/32+1;
		int im_id;
		im_id=key_num%32;
		if(im_id==0)
			im_id=32;
		if(Addr)
			sprintf(Addr,"%s%02d%02d",GetSysVerInfo_bd(),dxg_id,im_id);
	}
	return 0;
}

int IXAddrMappingM4Key(char *Addr)
{
	char buff[5]={0};
	int addr_int;
	cJSON *view=cJSON_CreateArray();
	cJSON *where=cJSON_CreateObject();
	cJSON_AddStringToObject(where,"IX_ADDR",Addr);
	if(API_TB_SelectBySortByName(TB_NAME_KEYID_MAPPING, view, where, 0))
	{
		int key_id;
		cJSON *one_item;
		cJSON_ArrayForEach(one_item,view)
		{
			int ds_id=GetEventItemInt(one_item, "DS_ID");
			if(ds_id==0||ds_id==atoi(GetSysVerInfo_BdRmMs()+8))
			{
				if(GetJsonDataPro(one_item, "KEY_ID", &key_id))
				{
					cJSON_Delete(view);
					cJSON_Delete(where);
					return key_id;
				}
			}
		}
	}
	cJSON_Delete(view);
	cJSON_Delete(where);
	
	memcpy(buff,Addr+4,4);
	addr_int=atoi(buff);
	bprintf("222222222:%s\n",Addr);
	if(API_PublicInfo_Read_Int(PB_M4_KEY_MAPPING)==0)
	{
		return addr_int;
	}
	else
	{
		int dxg_id=addr_int/100;
		int im_id=addr_int%100;
		return (dxg_id-1)*32+im_id;
	}
	return 0;
}

int EventCallbackCallAction(cJSON* event)
{
	char* callData = cJSON_GetStringValue(cJSON_GetObjectItemCaseSensitive(event, "CallData"));
	if(callData)
	{
		bprintf("11111111 %s\n",callData);
	#if 1
		char* callServerStateString = API_PublicInfo_Read_String(PB_CALL_SER_STATE);
		if(callServerStateString == NULL)
		{
			return 0;
		}

		if(!strcmp(callData, "*"))
		{
			//���ڴ���״̬��
			if(strcmp(callServerStateString, "Wait"))
			{
				API_CallServer_LocalBye();
			}
			
			//���ϵͳæ

		}
		else if(!strcmp(callData, "#"))
		{
			if(!strcmp(callServerStateString, "Ring"))
			{
				API_CallServer_Redail();
			}
		}
		else
		{
			int roomNbr = atoi(callData);
			#if 1
			int callNbr = GetCallNumber();
	
			if(strcmp(callServerStateString, "Wait"))
			{
				//����ͨ��״̬�����Ҳ�����ͬ����
				if(callNbr != roomNbr && IsCallTalkState() == 0)
				{
					API_CallServer_LocalBye();
				}
				else if(callNbr == roomNbr)
				{
					API_CallServer_Redail();
				}
				return 0;
			}
			
			#endif
			char addr[11];
			if(M4KeyMappingAddr_ByInput(roomNbr,addr)==0)
			{
				
				cJSON *call_event=cJSON_CreateObject();
				cJSON_AddStringToObject(call_event, EVENT_KEY_EventName,EventBECall);
				cJSON_AddStringToObject(call_event, "CallType","IxMainCall");
				cJSON_AddStringToObject(call_event, "From","Menu");
				cJSON *call_tar=cJSON_AddObjectToObject(call_event,"Target");
				cJSON_AddStringToObject(call_tar, PB_CALL_TAR_INPUT,addr);
				API_Event_Json(call_event);
				
			}
			
		}
	#endif
	}
}
#else
int EventCallbackCallAction(cJSON* event)
{
	char* callData = cJSON_GetStringValue(cJSON_GetObjectItemCaseSensitive(event, "CallData"));
	if(callData)
	{
		bprintf("11111111 %s\n",callData);
	#if 1
		char* callServerStateString = API_PublicInfo_Read_String(PB_CALL_SER_STATE);
		if(callServerStateString == NULL)
		{
			return 0;
		}

		if(!strcmp(callData, "*"))
		{
			//���ڴ���״̬��
			if(strcmp(callServerStateString, "Wait"))
			{
				API_CallServer_LocalBye();
			}
			
			//���ϵͳæ
			char* linkType = API_PublicInfo_Read_String(PB_DT_LINK_TYPE);
			if(linkType)
			{
				if(strcmp(linkType, "Call") && strcmp(linkType, "Intercom") && strcmp(linkType, "Playback"))
				{
					//dprintf("222222222222 keypad * API_SR_Deprived\n");
					API_SR_Deprived();
				}
			}
		}
		else if(!strcmp(callData, "#"))
		{
			if(!strcmp(callServerStateString, "Ring"))
			{
				API_CallServer_Redail();
			}
		}
		else
		{
			int roomNbr = atoi(callData);
			int callNbr = GetCallNumber();
	
			if(strcmp(callServerStateString, "Wait"))
			{
				//����ͨ��״̬�����Ҳ�����ͬ����
				if(callNbr != roomNbr && IsCallTalkState() == 0)
				{
					API_CallServer_LocalBye();
				}
				else if(callNbr == roomNbr)
				{
					API_CallServer_Redail();
				}
			}
			else
			{
				DT_CallStart(roomNbr);
			}
		}
	#endif
	}
}
#endif
int EventCallActionFilter(cJSON* event)
{
	char* callData = cJSON_GetStringValue(cJSON_GetObjectItemCaseSensitive(event, "CallData"));
	if(callData)
	{
		if(!strcmp(callData, "*"))
		{
			return 1;
		}
		else
		{
			if(IfCallServerBusy())
			{
				if(GetCallNumber() == atoi(callData))
				{
					return 1;
				}
				return 0;
			}
		}
	}

	return 1;
}


static void IX611_KeypadLedResult(int result)
{
	#if defined(PID_IX611)
	if(result)
	{
		if(GetKeyType())
		{
			usleep(500*1000);
			BEEP_CONFIRM();
		}
		else
		{
			API_KeypadLedCtrl(LED_GREEN, KEYPAD_LED_ON);
			usleep(500*1000);
			if(API_Para_Read_Int(KeypadSoundEnable))
			{
				BEEP_CONFIRM();
			}
		}
	}
	else
	{
		if(GetKeyType())
		{
			usleep(500*1000);
			BEEP_ERROR();
		}
		else
		{
			API_KeypadLedCtrl(LED_RED, KEYPAD_LED_ON);
			usleep(500*1000);
			if(API_Para_Read_Int(KeypadSoundEnable))
			{
				BEEP_ERROR();
			}
		}
	}
	KeypadBacklightCtrl(1);
	#endif
}

static void KeypadInputResult(int result)
{
	#if defined(PID_IX611)
	IX611_KeypadLedResult(result);
	#endif
}

int CheckPwd(char* pwd, char* input, int autoUnlockLen)
{
	int ret = 0;
	if(pwd && input)
	{
		int pwdLen = strlen(pwd);
		int inputLen = strlen(input);
		if(pwdLen > 0 && inputLen > 0)
		{
			if(autoUnlockLen > pwdLen)
			{
				if(inputLen == pwdLen && !strncmp(pwd, input, pwdLen))
				{
					ret = 1;
				}
			}
			else
			{
				if(inputLen == autoUnlockLen && !strncmp(pwd, input, inputLen))
				{
					ret = 1;
				}
			}
		}
	}

	return ret;
}

int UnlockPasswordVerify(char* pwd, int autoUnlockLen)
{
	int ret = 0;

	if(CheckPwd(API_Para_Read_String2(Unlock1Pwd1), pwd, autoUnlockLen) || CheckPwd(API_Para_Read_String2(Unlock1Pwd2), pwd, autoUnlockLen))
	{
		ret = 1;
	}
	else if(CheckPwd(API_Para_Read_String2(Unlock2Pwd1), pwd, autoUnlockLen) || CheckPwd(API_Para_Read_String2(Unlock2Pwd2), pwd, autoUnlockLen))
	{
		ret = 2;
	}
	else if(CheckPrivateUnlockPwd(pwd, autoUnlockLen))
	{
		ret = 3;
	}

	return ret;
}

static int PasswordVerify(char* pwd)
{
	int ret = 0;

	if(!pwd || !strlen(pwd))
	{
		return ret;
	}

	if(CheckPwd("2288", pwd, KeyPad_manage_run.pwdLen) || CheckPwd(API_Para_Read_String2(PRJ_PWD), pwd, KeyPad_manage_run.pwdLen))
	{
		ret = 1;
	}
	else
	{
		if(ret = UnlockPasswordVerify(pwd, KeyPad_manage_run.pwdLen))
		{
			ret++;
		}
	}
	return ret;
}

/*------------------------------------------------------------------------
						�������봦��
------------------------------------------------------------------------*/
void Password_Input_Process(unsigned char key_value)	
{
	int inputOver = 0;

	if(key_value=='*')
	{
		if(KeyPad_manage_run.key_input_len)
		{
			API_EventDebug(EventInputCancel, "Press * cancel: state: %s; input:%s", (KeyPad_manage_run.state == KEY_MANAGE_PASSWORD_INPUT) ? "PASSWORD_INPUT" : "SETTING_CODE_INPUT", KeyPad_manage_run.key_input_buf);
		}
		KeyPad_Manage_Init(0);
	}
	else if(key_value=='#')	
	{	
		if(KeyPad_manage_run.key_input_len >= 4)
		{
			inputOver = 1;
		}
		else
		{
			KeypadInputResult(0);
			key_input_init();
		}
	}
	else if(key_value>='0' && key_value<='9') //��������
	{
		if(KeyPad_manage_run.key_input_len<INPUT_MAX_LEN)	
		{
			if(KeyPad_manage_run.key_input_len==0)
			{
				#ifndef DH_LINK
				API_trigOneRec();
				#endif
			}
			KeyPad_manage_run.key_input_buf[KeyPad_manage_run.key_input_len]=key_value;
			KeyPad_manage_run.key_input_len++;
			#ifdef PID_IX821
			DR_MenuInputProcess(INPUT_PASSWORD, KeyPad_manage_run.key_input_buf, KeyPad_manage_run.key_input_len);
			#endif
			#ifdef PID_IX850
			CodePadDisplay(INPUT_PASSWORD,KeyPad_manage_run.key_input_buf, KeyPad_manage_run.key_input_len);
			#endif
			if(KeyPad_manage_run.key_input_len >= KeyPad_manage_run.pwdLen)
			{
				inputOver = 1;
			}
		}
		else	
		{
			KeypadInputResult(0);
			key_input_init();
		}
	}

	if(inputOver)
	{
		int passwordCheckFlag = PasswordVerify(KeyPad_manage_run.key_input_buf);

		switch (passwordCheckFlag)
		{
			case 1:
				KeyPad_manage_run.state = KEY_MANAGE_SettingCode_INPUT;
				KeypadInputResult(1);
				#ifdef PID_IX821
				DR_MenuInputProcess(INPUT_SETTING_CODE, KeyPad_manage_run.key_input_buf, KeyPad_manage_run.key_input_len);
				#endif
				break;
			case 2:
			case 4:
				KeypadInputResult(1);
				API_Event_Unlock("RL1", "PWD");
				API_Event_NameAndMsg(EventUnlockCodeReport, "Code Unlock Success");
				break;
			case 3:
				KeypadInputResult(1);
				API_Event_Unlock("RL2", "PWD");
				API_Event_NameAndMsg(EventUnlockCodeReport, "Code Unlock Success");
				break;
			case 0:
				KeypadInputResult(0);
				API_Event_NameAndDebugAndMsg(EventUnlockCodeReport, KeyPad_manage_run.key_input_buf, "Code Unlock Error");
				#ifdef PID_IX821
				DR_MenuInputProcess(PASSWORD_ERROR, NULL, 0);
				#endif
				break;
		}
		key_input_init();
	}

}

/*------------------------------------------------------------------------
					SettingCode���봦��
------------------------------------------------------------------------*/
void SettingCode_Input_Process(unsigned char key_value)	//read
{
	if(key_value=='*')	
	{
		if(KeyPad_manage_run.key_input_len)
		{
			API_EventDebug(EventInputCancel, "Press * cancel: state: %s; input:%s", (KeyPad_manage_run.state == KEY_MANAGE_PASSWORD_INPUT) ? "PASSWORD_INPUT" : "SETTING_CODE_INPUT", KeyPad_manage_run.key_input_buf);
		}
		KeyPad_Manage_Init(0);
	}
	else if	(key_value=='#')	//��Ч���봦��
	{
		if(KeyPad_manage_run.key_input_len)
		{
			if(SettingCodeSaveProcess(KeyPad_manage_run.key_input_buf))
			{
				KeypadInputResult(1);
			}
			else
			{
				KeypadInputResult(0);
			}
			key_input_init();
		}
	}
	else if	(key_value>='0' && key_value<='9') //��������
	{
		if(KeyPad_manage_run.key_input_len<INPUT_MAX_LEN)	
		{
			KeyPad_manage_run.key_input_buf[KeyPad_manage_run.key_input_len]=key_value;
			KeyPad_manage_run.key_input_len++;
			#ifdef PID_IX821
			DR_MenuInputProcess(INPUT_SETTING_CODE, KeyPad_manage_run.key_input_buf, KeyPad_manage_run.key_input_len);
			#endif
			#ifdef PID_IX850
			CodePadDisplay(INPUT_SETTING_CODE,KeyPad_manage_run.key_input_buf, KeyPad_manage_run.key_input_len);
			#endif
		}
		else	
		{
			KeypadInputResult(0);
			key_input_init();
		}
	}
}


int ChangeOneUnlockPassword(char* input, int len, char* UnlockIoName)
{
	char* unlockPwd = API_Para_Read_String2(UnlockIoName);
	if(unlockPwd == NULL)
	{
		return 0;
	}

	int pwdLen = strlen(unlockPwd);
	int newPwdLen = (len - pwdLen)/2;

	//���볤�Ȳ���
	if(input == NULL || len == 0 || unlockPwd == NULL || len < pwdLen || (len - pwdLen)%2 || newPwdLen != API_Para_Read_Int(AutoUnlockPwdLen))
	{
		return 0;
	}

	//���������벻һ��
	if(strncmp(input+pwdLen, input+pwdLen+newPwdLen, newPwdLen))
	{
		return 0;
	}

	//�������ж���ȷ
	if(!strncmp(input, unlockPwd, pwdLen))
	{
		API_Para_Write_String(UnlockIoName, input+pwdLen+newPwdLen);
		return 1;
	}
	else
	{
		return 0;
	}
}

int ChangeUnlockPassword(char* input, int len)
{
	if(ChangeOneUnlockPassword(input, len, Unlock1Pwd1))
	{
		return 1;
	}
	else if(ChangeOneUnlockPassword(input, len, Unlock1Pwd2))
	{
		return 2;
	}
	else if(ChangeOneUnlockPassword(input, len, Unlock2Pwd1))
	{
		return 3;
	}
	else if(ChangeOneUnlockPassword(input, len, Unlock2Pwd2))
	{
		return 4;
	}
	else
	{
		return 0;
	}
}

static int CheckPrivateUnlockPwd(char* pwd, int pwdLen)
{
	int ret = 0;

	if(pwd == NULL || pwdLen == 0)
	{
		return ret;
	}

	cJSON* Where = cJSON_CreateObject();
	cJSON_AddStringToObject(Where, "CODE", pwd);
	cJSON* view = cJSON_CreateArray();
	cJSON* element;

	if(API_TB_SelectBySortByName("PRIVATE_PWD", view, Where, 0))
	{
		cJSON_ArrayForEach(element, view)
		{
			if(CheckPwd(cJSON_GetStringValue(cJSON_GetObjectItemCaseSensitive(element, "CODE")), pwd, pwdLen))
			{
				ret = 1;
				break;
			}
		}
	}

	cJSON_Delete(Where);
	cJSON_Delete(view);
	return ret;
}

unsigned char Get_Setting_State(void)
{
	//return (KeyPad_manage_run.state);
}


unsigned char IfKeyPadIdle(void)
{
	return (KeyPad_manage_run.key_input_len == 0) ? 1 : 0;
}

void CardSetupRoom_Input_Process(unsigned char key_value)	
{
	char tempInput[INPUT_MAX_LEN+1];

	if(key_value=='*')
	{
		Send_CardSetupRoom("*");
		key_input_init();
	}
	else if(key_value=='#')	
	{	
		if(KeyPad_manage_run.key_input_len>=1 && KeyPad_manage_run.key_input_len<=2)
		{
			Send_CardSetupRoom(KeyPad_manage_run.key_input_buf);
		}
		key_input_init();
	}
	else if(key_value>='0' && key_value<='9') //��������
	{
		if(KeyPad_manage_run.key_input_len<INPUT_MAX_LEN)	
		{
			KeyPad_manage_run.key_input_buf[KeyPad_manage_run.key_input_len]=key_value;
			KeyPad_manage_run.key_input_len++;
		}
		else	
		{
			key_input_init();
		}
	}
}
#if defined(PID_IX821)
void RoomNumber_Input_Process(unsigned char key_value)	
{
	char tempInput[INPUT_MAX_LEN+1];
	char state;									//??????

	if(key_value=='*')
	{
		if(KeyPad_manage_run.key_input_len)
		{
			KeyPad_Manage_Init(0);
		}
		else
		{
			API_Event_CallAction("MK", "*");
		}
	}
	else if(key_value=='#')	
	{	
		if(!KeyPad_manage_run.key_input_len)
		{
			char* callServerStateString = API_PublicInfo_Read_String(PB_CALL_SER_STATE);
			if(callServerStateString && !strcmp(callServerStateString, "Ring"))
			{
				API_Event_CallAction("MK", "#");
			}
			else
			{
			#if 0
				if(API_Para_Read_Int(KeypadDefaultUnlockEnable))
				{
					KeypadEnterSettingState();
				}
				else
			#endif
				{
					KeyPad_manage_run.state = KEY_MANAGE_PASSWORD_INPUT;
					DR_MenuInputProcess(INPUT_PASSWORD, KeyPad_manage_run.key_input_buf, KeyPad_manage_run.key_input_len);
				}
			}
		}
		else
		{
			strcpy(tempInput, KeyPad_manage_run.key_input_buf);
			KeyPad_Manage_Init(0);
			
			API_Event_CallAction("MK", tempInput);				// dx
		}
	}
	else if(key_value>='0' && key_value<='9') //��������
	{
		if(KeyPad_manage_run.key_input_len<INPUT_MAX_LEN)	
		{
			KeyPad_manage_run.key_input_buf[KeyPad_manage_run.key_input_len]=key_value;
			KeyPad_manage_run.key_input_len++;
			DR_MenuInputProcess(INPUT_ROOM_NUM, KeyPad_manage_run.key_input_buf, KeyPad_manage_run.key_input_len);
		}
		else	
		{
			BEEP_ERROR();
			KeyPad_Manage_Init(0);
		}
	}
}
#endif
/*********************************************************************************************************
**  End Of File
*********************************************************************************************************/






		
		
		




