/**
  ******************************************************************************
  * @file    obj_MDS_M4_Output.c
  * @author  czb
  * @version V00.01.00
  * @date    2022.5.17
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 

#include <stdio.h>
#include <sys/sysinfo.h>
#include "task_MDS.h"
#include "obj_AiLayer.h"
#include "obj_MDS_Output.h"
#include "vdp_uart.h"
#include "obj_PublicInformation.h"
#include "obj_MDS_Input.h"
#include "obj_MDS_DR.h"
#include "elog.h"
#include "task_Event.h"
#include "obj_IoInterface.h"

static int callNumber=0;

void M4_CallDisplayProcess(DR_Display_mode mode, int number)
{
    int i;

    switch(mode)
    {
    	case STANDBY:
		if(API_PublicInfo_Read_Int(PB_M4_KEY_MAPPING)!=0||API_Para_Read_Int(M4DispImOnline)==0)
		{
			API_ALL_M4_LED(EXT_LED_ON);
		}
		else
		{
			API_M4_LED(callNumber, EXT_LED_ON);	
			API_Event_By_Name(EventM4LEDDisplayImOnline);
			API_Event_By_Name(Event_IXS_ReqUpdateTb);
		}
        	//API_ALL_M4_LED(EXT_LED_ON);
            //for(i = 0; i < 16 && IsMDS_Online(i); i++)
            //{
            //    API_M4_ALL_LED(i, EXT_LED_ON);
            //}
            break;

        case CALLING_START:
            callNumber = number;
            API_M4_LED(number, EXT_LED_FLASH_FAST);
            break;

        case CALLING_BUSY:
            API_M4_LED(callNumber, EXT_LED_ON);
            break;

        case CALLING_ERROR:
            //
            if(API_PublicInfo_Read_Int(PB_M4_KEY_MAPPING)!=0||API_Para_Read_Int(M4DispImOnline)==0)
		{
			API_M4_LED(callNumber, EXT_LED_ON);
		}
		else
		{
			//API_M4_LED(callNumber, EXT_LED_ON);	
			API_Event_By_Name(EventM4LEDDisplayImOnline);
			API_Event_By_Name(Event_IXS_ReqUpdateTb);
		}
            break;

        case CALLING_CANCEL:
            API_M4_LED(callNumber, EXT_LED_ON);
            break;

        case CALLING_WAIT:
            callNumber = number;
            API_M4_LED(callNumber, EXT_LED_FLASH_FAST);
            break;
            
        case CALLING_TALK:
            API_M4_LED(callNumber, EXT_LED_FLASH_SLOW);
            break;
    }
}


int M4_DisplayEventCall(cJSON *cmd)
{
    char* eventName = cJSON_GetStringValue(cJSON_GetObjectItemCaseSensitive(cmd, EVENT_KEY_EventName));
    char* callserverState = cJSON_GetStringValue(API_PublicInfo_Read(PB_CALL_SER_STATE));
    int Call_Part = API_PublicInfo_Read_Bool(PB_CALL_PART);
    char* Call_Tar_DtAddr = cJSON_GetStringValue(API_PublicInfo_Read(PB_CALL_TAR_DTADDR));
    
    if(callserverState == NULL)
    {
        return 0;
    }

    //dprintf("callserverState = %s, Call_Part = %d, Call_Tar_DtAddr = %s\n", callserverState, Call_Part, Call_Tar_DtAddr);

    if(eventName && !strcmp(eventName, EventCall))
    {
        char* callEvent = cJSON_GetStringValue(cJSON_GetObjectItemCaseSensitive(cmd, "CALL_EVENT"));
        if(callEvent)
        {
            if(!strcmp(callEvent, "Calling_Busy"))
            {
                M4_CallDisplayProcess(CALLING_BUSY, 0);
            }
            else if(!strcmp(callEvent, "Calling_Error"))
            {
                M4_CallDisplayProcess(CALLING_ERROR, 0);
            }
            else if(!strcmp(callEvent, "Calling_Cancel"))
            {
                M4_CallDisplayProcess(CALLING_CANCEL, 0);
            }
        }
    }
    else if(callserverState)
    {
        if(!strcmp(callserverState, "Wait"))
        {
            M4_CallDisplayProcess(STANDBY, 0);
        }
        else if(Call_Part && !strcmp(callserverState, "Ring"))
        {
        	 int callNumber = 0;
        	#ifdef PID_IX821
		char* Call_Tar_ixaddr = cJSON_GetStringValue(API_PublicInfo_Read(PB_CALL_TAR_IXADDR));
		if(Call_Tar_ixaddr)
		{
			callNumber=IXAddrMappingM4Key(Call_Tar_ixaddr);
		
		}	
		#else
            char* Call_Tar_DtAddr = cJSON_GetStringValue(API_PublicInfo_Read(PB_CALL_TAR_DTADDR));
            if(Call_Tar_DtAddr)
            {
                callNumber = strtoul(Call_Tar_DtAddr,NULL,0);
                callNumber=((callNumber&0x7f)>>2);
                callNumber = (callNumber ==0 ? 32 : callNumber);
            }
		#endif
            M4_CallDisplayProcess(CALLING_WAIT, callNumber);
        }
        else if(Call_Part && (!strcmp(callserverState, "Ack") || !strcmp(callserverState, "RemoteAck")))
        {
            M4_CallDisplayProcess(CALLING_TALK, 0);
        }
    }

	return 1;
}

static int M4LEDDisplayBySort_start_flag=-1;
static int M4LEDDisplayBySort_stop_flag=0;
static pthread_mutex_t  M4LEDDisplay_lock = PTHREAD_MUTEX_INITIALIZER;
int M4LEDDisplayBySort_Process(cJSON *cmd)
{
	#if defined(PID_IX821)
	//cJSON *mds_online=API_PublicInfo_Read(PB_MDS_MODULS);
	int i;
	//static int start_flag=0;
	//static int stop_flag=0;
	
	int save_led1_state=-1;
	
	if(M4LEDDisplayBySort_start_flag>0)
	{
		return 0;
	}
	pthread_mutex_lock(&M4LEDDisplay_lock);
	M4LEDDisplayBySort_start_flag=1;
	M4LEDDisplayBySort_stop_flag=0;
	
	//API_M4_LED(0,EXT_LED_FLASH_FAST);
	API_ALL_M4_LED(EXT_LED_OFF);
	usleep(200*1000);
	for(i=0;i<16;i++)
	{
		
		//tick_time += 250;
		
		PrintCurrentTime(20000+i);
		//if(i%4==0)
		//	API_MDS_CheckOneOnline(i/4);
		//PrintCurrentTime(20100+i);
		API_M4_LED(i+1,EXT_LED_ON);
		//usleep(250*1000);
		//PrintCurrentTime(20200+i);
		if(ifKeyMapping(i+1))
		{
			if(save_led1_state!=1)
				API_LED_ctrl("LED_1", "MSG_LED_ON");
			save_led1_state=1;
		}
		else
		{
			if(save_led1_state!=0)
				API_LED_ctrl("LED_1", "MSG_LED_OFF");
			save_led1_state=0;
		}
		//PrintCurrentTime(20300+i);
		usleep(250*1000);
		#if 0
		clock_gettime(CLOCK_MONOTONIC,&ts);
		curtime = (ts.tv_sec*1000LL) + (ts.tv_nsec+500000LL)/1000000LL;
		realtime = curtime-orig;		
		diff = tick_time-realtime;
		if( diff <= 0 )
			continue;
		ts.tv_sec=0;
		ts.tv_nsec=(int)diff*1000000LL;
		nanosleep(&ts,NULL);
		#endif
		if(M4LEDDisplayBySort_stop_flag)
			break;
	}
	M4LEDDisplayBySort_start_flag=0;
	pthread_mutex_unlock(&M4LEDDisplay_lock);
	
	API_ALL_M4_LED(EXT_LED_ON);

	API_Event_By_Name(EventSnErrDisp);
	API_Event_By_Name(EventM4LEDDisplayImOnline);
	#endif
}

void M4LEDDisplayBySort_stop(void)
{	
	M4LEDDisplayBySort_stop_flag=1;
}
#ifdef PID_IX821
int M4LEDDisplayImOnline_Process(cJSON *cmd)
{
	char ix_addr[11];
	cJSON *im_list;
	//static char m4_led_state[64]={0};
	int i;
	int off_flag=0;
	int on_flag=0;
	int ledId=0;
	int m4Addr=0;
	int m4sort;
	int m4_or_m8;
	int id_tb[4]={3,4,1,2};
	if(M4LEDDisplayBySort_start_flag<0)
		return 0;
	if(API_PublicInfo_Read_Int(PB_M4_KEY_MAPPING)!=0||API_Para_Read_Int(M4DispImOnline)==0)
	{
		if(M4LEDDisplayBySort_start_flag==0)
			API_ALL_M4_LED(EXT_LED_ON);
		return 0;
	}
	m4sort=API_PublicInfo_Read_Int(PB_M4_KEY_SORT);
	m4_or_m8=API_PublicInfo_Read_Int(PB_KEY_MODULE_TYPE);
	sprintf(ix_addr,"%s000101",GetSysVerInfo_bd());
	pthread_mutex_lock(&M4LEDDisplay_lock);
	M4LEDDisplayBySort_stop_flag=0;
	char *call_state=API_PublicInfo_Read_String(PB_CALL_SER_STATE);
	if(call_state)
	{
		  if(strcmp(call_state, "Wait")!=0)	
		  {
			pthread_mutex_unlock(&M4LEDDisplay_lock);
			return 0;
		  }
	}
	//API_ALL_M4_LED(EXT_LED_ON);
	//usleep(100*1000);
	for(i=0;i<64;i++)
	{
		if(i>=32&&i%4==0)
		{
			if(API_MDS_CheckOneOnline(i/4)==0)
				break;
		}
		if(i%4==0)
		{
			off_flag=0;
			on_flag=0;
			m4Addr = (i+1)/4;
			if(m4sort==1&&m4_or_m8==1)
			{
				if(m4Addr%2)
					m4Addr=m4Addr-1;
				else
					m4Addr=m4Addr+1;
			}
		}
		if(m4sort==0)
	        	ledId = (i+1)%4==0?4:((i+1)%4);
		else
		{
			if(m4_or_m8==1)
			{
				ledId=id_tb[(i+1)%4==0?(4-1):((i+1)%4-1)];
			}
			else
				ledId=5-((i+1)%4==0?4:((i+1)%4));
		}
		if(M4KeyMappingAddr_ByInput(i+1,ix_addr)==0)
		{
			if(strlen(ix_addr)==8)
			{
				strcat(ix_addr,"01");
			}
			im_list=GetIXSByIX_ADDR(ix_addr);
			if(cJSON_GetArraySize(im_list)==0)
			{

				off_flag|=(MODEL_KEY_BL1<<((ledId-1)%4));
				#if 0
				if(m4_led_state[i]!=0)
				{
					API_M4_LED(i+1,EXT_LED_OFF);
					usleep(100*1000);
				}
				m4_led_state[i]=0;
				#endif
			}
			else	if(cJSON_GetArraySize(im_list)>=1)
			{
				on_flag|=(MODEL_KEY_BL1<<((ledId-1)%4));
			#if 0
				if(m4_led_state[i]!=1)
				{
					API_M4_LED(i+1,EXT_LED_ON);
					usleep(100*1000);
				}
				m4_led_state[i]=1;
			#endif
			}
			#if 0
			else
				API_M4_LED(i+1,EXT_LED_FLASH_SLOW);
			#endif
			cJSON_Delete(im_list);

			if(i%4==3&&off_flag)
			{
				API_M4_PART_LED(m4Addr,off_flag,EXT_LED_OFF);
				usleep(100*1000);
			}
			if(i%4==3&&on_flag)
			{
				API_M4_PART_LED(m4Addr,on_flag,EXT_LED_ON);
				usleep(100*1000);
			}
		}
		
		if(M4LEDDisplayBySort_stop_flag)
			break;
	}
	pthread_mutex_unlock(&M4LEDDisplay_lock);
}
#endif
/*********************************************************************************************************
**  End Of File
*********************************************************************************************************/

