/**
  ******************************************************************************
  * @file    obj_MDS_Output.h
  * @author  czb
  * @version V00.01.00
  * @date    2022.5.17
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 

#ifndef _obj_MDS_Output_H
#define _obj_MDS_Output_H

#include "RTOS.h"
#include "utility.h"

// Define Object Property-------------------------------------------------------
typedef enum       //lyx 20170721
{
    EXT_LED_ON,
    EXT_LED_OFF,
    EXT_LED_FLASH_SLOW,
    EXT_LED_FLASH_FAST,
}EXT_LED_STATE;


int API_MK_LED(EXT_LED_STATE state);

#endif


