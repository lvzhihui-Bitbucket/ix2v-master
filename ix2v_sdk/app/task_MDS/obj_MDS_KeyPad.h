/**
  ******************************************************************************
  * @file    obj_MDS_KeyPad.h
  * @author  czb
  * @version V00.01.00
  * @date    2022.5.17
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 

#ifndef _obj_MDS_KeyPad_H
#define _obj_MDS_KeyPad_H
#include "cJSON.h"

// Define Object Property-------------------------------------------------------
#define INPUT_MAX_LEN		20	//���������Ŀ
#define ERROR_CNT			10	//����������(����������������)

#define	KEY_MANAGE_RoomNumber_INPUT	      0	
#define	KEY_MANAGE_PASSWORD_INPUT		      1
#define	KEY_MANAGE_SettingCode_INPUT		  2

typedef struct
{
	uint8 state;									//����״̬
	uint8 key_free_cnt;
	uint8 key_input_buf[INPUT_MAX_LEN+1];		//���뻺��
	uint8 key_input_len;							//���볤��
	uint8 key_bl;
	uint8 key_bl_all;
  int pwdFree;                    //免密码状态
  int pwdLen;                    //开锁密码长度
  cJSON* settingCodeCfg;
  cJSON* settingCallbackTable;

}KEY_MANAGE_RUN;

typedef int (*SettingCallback)(char*);

// Define Object Function - Public----------------------------------------------

// Define Object Function - Private---------------------------------------------


#endif
