/**
  ******************************************************************************
  * @file    obj_MDS_Input.h
  * @author  czb
  * @version V00.01.00
  * @date    2022.5.17
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2016 V-Tec</center></h2>
  ******************************************************************************
  */ 

#ifndef _obj_MDS_Input_H
#define _obj_MDS_Input_H

#include "RTOS.h"
#include "utility.h"

// Define Object Property-------------------------------------------------------
typedef enum
{
    MDS_M4 = 0,
    MDS_MK,
    MDS_DR,
}MDS_INPUT_TYPE;

typedef struct
{
	MDS_INPUT_TYPE inputType;
	int keyStatus;
	int keyData;
	char cardData[4];
}MDS_INPUT_S;

typedef struct
{
    int mkLedState;
}MDS_INPUT_RUN_S;

#pragma pack(1)	//stm32_stack
typedef struct
{
	unsigned char sa;
	unsigned char da;
	unsigned char cmd;
	unsigned char cks;
	union
	{
		unsigned char byte;
		struct
		{
			unsigned char len:4;		//bit3.2.1.0:	长度计数
			unsigned char ack_req:1;	//bit4:	        ACK请求，	0/无请求ACK，1/请求目标地址的ACK
			unsigned char rep:1;		//bit5:	        首次发送，	0/重新发送，1/首次发送
			unsigned char newcmd:1;		//bit6:		    新指令包格式
			unsigned char daf:1;		//bit7: 		地址类型， 0/设备地址，1/组地址
		}bit;
	}length;
	unsigned char data[MAX_FRAME_LENGTH];
} M_AI_DATA;
#pragma pack()	//stm32_stack

#endif


