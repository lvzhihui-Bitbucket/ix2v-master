
/**
  ******************************************************************************
  * @file    l2_layer.c
  * @author  lv
  * @version V1.0.0
  * @date    2012.06.20
  * @brief   This file contains the functions of l2_layer.c
  ******************************************************************************
  *	@协议链路层处理程序
  *	@一个数据帧的可靠发送或是接收
  * <h2><center>&copy; COPYRIGHT 2012 V-Tec</center></h2>
  ******************************************************************************
  */ 

#include "stack212.h"
#include "vdp_uart.h"
#include "obj_L2Layer.h"
#include "define_Command.h"
#include "elog_forcall.h"

#define MICRO_L2_NORMAL_TRS_OVER	{		\
			l2TrsFrameBuf.frameCallback();	\
			l2Trs = L2_TRS_NONE;			\
			l2Status = L2_STAT_READ; }

// lzh_20140508_s
//unsigned char cmdType = 0;		//命令类型：0/旧指令，1/新指令
unsigned char cmdType = 0;		//命令类型：0/旧指令，1/新指令, 2/新长指令
// lzh_20140508_e

L2_ADDR_TYPE myAddr=DS1_ADDRESS;

volatile L2_RCV_FLAG l2ComRcvFlag;			//链路层接收通信标志定义，链路层负责SET，应用层负责RESET
volatile L2_TRS_FLAG l2ComTrsFlag;			//链路层发送通信标志定义，链路层负责SET，应用层负责RESET

COM_RCV_FLAG phTmpRcvCom;
COM_TRS_FLAG phTmpTrsCom;

MAC_TYPE	l2MacType;
L2_STAT 		l2Status;
L2_ERR		l2Error;
L2_FRAME	l2TrsFrameBuf;
L2_FRAME	l2RcvFrameBuf;

L2_RETRS	l2ReTrs;
L2_RCV_STAT	l2Rcv;
L2_TRS_STAT	l2Trs;

OS_U32 macTiming;		//媒体控制定时

unsigned char l2BusMonEnable;

OS_TIMER TimerTrs;

//lzh_20130416
unsigned char F_L2_SEND_NEW_CMD = 0;

//MAC控制的类型 lzh_20130415
const unsigned short MAC_ACCESS_TYPE_TAB[] =
{
	80,			//MAX_BUS_IDLE_TIME 		- 总线最大空闲等待时间（单位：25ms）,之后都可以发送数据			//Min - 0
	4,			//MIN_INTERVAL_SEND_ACK 	- 发送ACK需要等待总线空闲的最小时间（单位：ms）					//Min - 1 (收到数据包后给出ACK应答)
	20,			//MAX_INTERVAL_WAIT_ACK		- 发送APPLY申请命令等待ACK应答的最大时间间隔（单位：ms）		//Min - 2 (发送数据包后等待ACK的时间)
	30,			//MAX_INTERVAL_BETWEEN_NOR	- 通用数据帧连续发送的时间间隔（单位：ms）						//Min - 3 (重发正常数据包)
	40,			//MAX_INTERVAL_BETWEEN_BRD	- 广播数据帧连续发送的时间间隔（单位：ms）						//Min - 4 (重发广播数据包)
	50,			//MAX_INTERVAL_SEND_BLK		- 发送BLOCK的时间												//Min - 5 (发送block)
	60,			//MIN_INTERVAL_SEND_RLY		- 发送REPLY数据帧需要等待总线空闲的最小时间（单位：ms）			//Min - 6 (新指令的回复数据包)
	70,			//MIN_INTERVAL_SEND_ALY		- 发送APPLY申请命令需要等待总线空闲的最小时间（单位：ms）		//Min - 7 (新指令的请求数据包)
	//特殊指令mac定制
	16,			//MIN_REPLAY_SUBMODEL		- 针对旧主机DMR11和DT591的回复SubReceipt的时间					//Min - 8 (旧指令的SubModel)
	//lzh_20140508_s
	20,
	//lzh_20140508_e		
};

/*******************************************************************************************
 * @fn:		L2_Initial
 *
 * @brief:	链路层初始化
 *			1.链路层通信标志复位
 *			2.链路层状态机复位
 *			3.链路层接收状态机复位
 *			4.链路层发送状态机复位
 * 			5.重发对象复位
 *			6.MAC复位
 *			7.关闭监控模式
 *
 * @param:  LocalAddr - 本机物理地址
 * @param:  L2_GRP_TYPE - 本机主地址
 * @param:  rcvCallback - 接收数据回调函数
 * @param:  trsCallback - 发送数据回调函数
 *
 * @return: none
 *******************************************************************************************/
void L2_Initial(L2_ADDR_TYPE LocalAddr,L2_GRP_TYPE GroupAddr,ptrFuncion rcvCallback,ptrFuncion trsCallback)
{
	//链路层通信标志复位
	l2ComRcvFlag = R_RCV_NONE;
	l2ComTrsFlag = T_TRS_NONE;
	//链路层状态机复位
	l2Status = L2_STAT_READ;	
	//链路层接收状态机复位
	l2Rcv = L2_RCV_NONE;
	//链路层发送状态机复位
	l2Trs = L2_TRS_NONE;
	//重发对象复位
	L2ReTrsInit(0,MAX_BUS_IDLE_TIME);
	//MAC复位
	l2MacType = MAX_BUS_IDLE_TIME;
	//初始化本机地址和组地址

	//myAddr = LocalAddr;
	
	OS_CreateTimer(&TimerTrs, TImerTrsStatusCallBack, 800/25);	//czn_20170809
	
    //初始化回调函数
    l2TrsFrameBuf.frameCallback = trsCallback;
    l2RcvFrameBuf.frameCallback = rcvCallback;
}

/*******************************************************************************************
 * @fn：	TImerTrsStatusCallBack
 *
 * @brief:	发送状态的超时定时器，单次触发
 *
 * @param:  none
 *
 * @return: none
 *******************************************************************************************/
void TImerTrsStatusCallBack(void)
{
	OS_StopTimer(&TimerTrs);
	L2_LayerMonitorService();  	
}



/*******************************************************************************************
 * @fn:		MACTimingReset
 *
 * @brief:	MAC定时复位
 *
 * @param:  none
 *
 * @return: none
 *******************************************************************************************/
void MACTimingReset(void)
{
	//macTiming  = OS_GetTime32();
}

/*******************************************************************************************
 * @fn:		MACProcess
 *
 * @brief:	媒体访问控制
 *
 * @param:  type - 媒体访问类型
 *
 * @return: none
 *******************************************************************************************/
void MACProcess(MAC_TYPE type)
{
  	OS_U32 macPeriod;

	l2MacType = type;

#if 0	
	macPeriod = OS_GetTime32() - macTiming;
	
	if( macPeriod < MAC_ACCESS_TYPE_TAB[type] )
	{
		OS_Delay( MAC_ACCESS_TYPE_TAB[type] - macPeriod );
	}
#endif

}

/*******************************************************************************************
 * @fn:		L2ReTrsInit
 *
 * @brief:	数据重发对象初始化
 *
 * @param:  en - 重发允许
 * @param:  type - 媒体访问类型
 *
 * @return: 0 不许访问，1 允许访问
 *******************************************************************************************/
void L2ReTrsInit(unsigned char en,MAC_TYPE type)
{
	l2ReTrs.enable = en;
	l2ReTrs.interval = MAC_ACCESS_TYPE_TAB[type];
	l2ReTrs.times = 1;
}

/*******************************************************************************************
 * @fn:		IsJust2ReTranslate
 *
 * @brief:	判断是否需要重发
 *
 * @param:  none
 *
 * @return: 0 无重发访问，1 允许重发
 *******************************************************************************************/
unsigned char IsJust2ReTranslate(void)
{
	if( l2ReTrs.enable )
	{
		if( l2ReTrs.times )
		{
			l2ReTrs.times--;
			return 1;
		}
		else
		{
			return 0;
		}
	}
	else
	{
		return 0;
	}
}

/*******************************************************************************************
 * @fn:		L2_LayerRead
 *
 * @brief:	链路层数据读状态处理
 *			
 * @param:  none
 *
 * @return: none
 *******************************************************************************************/
void L2_LayerRead(void)
{
	//扫描发送结果
	Ph_PollingWriteResult(&phTmpTrsCom);  

	//printf("L2_LayerRead,l2Rcv=%d---\n",l2Rcv);
	
	switch( l2Rcv )
	{
		case L2_RCV_NONE:
			
			l2RcvFrameBuf.pack_len = Ph_PollingReadResult((unsigned char*)&l2RcvFrameBuf.sa,&phTmpRcvCom);

			//printf("l2RcvFrameBuf.pack_len=%d,phTmpRcvCom=%d---\n",l2RcvFrameBuf.pack_len,phTmpRcvCom);
			
			//得到有效数据
			if( phTmpRcvCom == COM_RCV_DAT )
			{
				if( l2RcvFrameBuf.sa == L_Ctrl_Block )
				{
					//lzh_20130413					
					//置通信标志，进入等待应用层取数状态
					L2_RCV_FLAG_ENABLE(R_BLK_IND,L2_ERR_NONE)
					l2Rcv = L2_RCV_OVER;
					goto l2_rcv_over_process;						
				}
				else
				{
					//地址匹配: 本机物理地址且需要应答
					#if 0
					if( L2_IsNeedAck() && (!l2BusMonEnable) )	//lzh_20130403
					{
						//发送ACK前延时4ms lzh_20130425
						//OS_Delay( MAC_ACCESS_TYPE_TAB[MIN_INTERVAL_SEND_ACK] );
						//发送ACK应答
						Ph_AckWrite();
						l2Rcv = L2_RCV_TRS_ACK_WAIT;
					}
					else
					#endif
					{
						//置通信标志，进入等待应用层取数状态
						L2_RCV_FLAG_ENABLE(R_DAT_IND,L2_ERR_NONE)
						l2Rcv = L2_RCV_OVER;
						goto l2_rcv_over_process;
					}
				}
			}
			else if( phTmpRcvCom == COM_RCV_ERR )
			{
				//置通信标志，进入等待应用层取数状态
				L2_RCV_FLAG_ENABLE(R_ERR,L2_PH_BUS_ERROR)
				l2Rcv = L2_RCV_OVER;
				
				goto l2_rcv_over_process;					
			}		  
		  	break;
		case L2_RCV_TRS_ACK_WAIT:
			//printf("L2_RCV_TRS_ACK_WAIT:phTmpTrsCom=%d\n",phTmpTrsCom);
			//等待是否发送完成
			// lzh_20161207_s			
			//if( phTmpTrsCom == COM_TRS_ACK )
			if( phTmpTrsCom == COM_TRS_DAT )
			// lzh_20161207_e
			{
				//置通信标志，进入等待应用层取数状态
				L2_RCV_FLAG_ENABLE(R_DAT_IND,L2_ERR_NONE)
				l2Rcv = L2_RCV_OVER;	
				goto l2_rcv_over_process;
			}
			else
			{
				//置通信标志，进入等待应用层取数状态
				L2_RCV_FLAG_ENABLE(R_ERR,L2_ERR_NONE)
				l2Rcv = L2_RCV_OVER;	
				goto l2_rcv_over_process;			  	
			}
			break;
		case L2_RCV_OVER:
			l2_rcv_over_process:
			//接收完毕回调函数触发应用接口层读取数据
			l2RcvFrameBuf.frameCallback();				
			//应用层取数后返回到接收状态
			l2Rcv = L2_RCV_NONE;	  
			break;
	}

}

/*******************************************************************************************
 * @fn:		L2_LayerWrite
 *
 * @brief:	链路层数据写状态处理
 *			
 * @param:  none
 *
 * @return: none
 *******************************************************************************************/			
void L2_LayerWrite(void)
{
	//发送同时需要回扫接收的数据
	l2RcvFrameBuf.pack_len = Ph_PollingReadResult((unsigned char*)&l2RcvFrameBuf.sa,&phTmpRcvCom);	
	if( phTmpRcvCom == COM_RCV_DAT )
	{
		L2_RCV_FLAG_ENABLE(R_DAT_IND,L2_ERR_NONE)	//czn_20170809
		l2RcvFrameBuf.frameCallback();	
		//登记通信结果（发送失败），并进入发送结束状态
		L2_TRS_FLAG_ENABLE(T_ERR,L2_PH_BUS_ERROR)
		l2Trs = L2_TRS_DAT_OVER;	
		//goto L2_Normal_Trs_Over;
		MICRO_L2_NORMAL_TRS_OVER
	}
	//printf("l2Trs===============%d\n");
 	switch( l2Trs )
	{
		//1.等待块发送完成	  
		case L2_TRS_BLK_WAIT:
			//扫描发送结果
			Ph_PollingWriteResult(&phTmpTrsCom);
			//等待是否发送完成
			if( phTmpTrsCom == COM_TRS_DAT )
			{	
				L2_TRS_FLAG_ENABLE(T_BLK_REQ,L2_ERR_NONE)
				l2Trs = L2_TRS_BLK_OVER;
				goto L2_Block_Trs_Over;
			}
			else if( phTmpTrsCom == COM_TRS_ERR )
			{
				//登记通信结果（发送失败），并进入发送结束状态
				L2_TRS_FLAG_ENABLE(T_BLK_REQ,L2_PH_BUS_ERROR)
				l2Trs = L2_TRS_BLK_OVER;					
				goto L2_Block_Trs_Over;
			}
			break;
		case L2_TRS_BLK_OVER:
			L2_Block_Trs_Over:
			//lzh_20130413
			//发送完毕回调函数触发应用接口层读取数据发送结果
			l2TrsFrameBuf.frameCallback();																		  
			//登记通信结果并进入发送结束状态
			l2Trs = L2_TRS_NONE;
			l2Status = L2_STAT_READ;				  	
		  	break;

		//2.等待广播发送完成		
		case L2_TRS_BRD_WAIT:
			//扫描第一次发送结果
			Ph_PollingWriteResult(&phTmpTrsCom);
			//等待是否发送完成
			if( phTmpTrsCom == COM_TRS_DAT )
			{	
				//查询是否需要发送第二次
				if( l2ReTrs.enable && (!l2BusMonEnable) )	//lzh_20130403
				{				
					//等待40ms后发送第二次
					//OS_Delay(l2ReTrs.interval);
					Ph_DataWrite( (unsigned char*)&l2TrsFrameBuf.sa, l2TrsFrameBuf.pack_len, 0 );					
					l2Trs = L2_TRS_BRD_REP;
				}
				else
				{
					//登记通信结果(已发送数据申请)，并进入发送结束状态
					L2_TRS_FLAG_ENABLE(T_DAT_REQ,L2_ERR_NONE)
					l2Trs = L2_TRS_BRD_OVER;
					goto L2_Boardcast_Trs_Over;
				}
			}
			else// if( phTmpTrsCom == COM_TRS_ERR )
			{
				//登记通信结果（发送失败），并进入发送结束状态
				L2_TRS_FLAG_ENABLE(T_ERR,L2_PH_BUS_ERROR)
				l2Trs = L2_TRS_BRD_OVER;				
				goto L2_Boardcast_Trs_Over;
			}		  
		  	break;
		case L2_TRS_BRD_REP:
			//扫描发送结果
			Ph_PollingWriteResult(&phTmpTrsCom);
			//等待是否发送完成
			if( phTmpTrsCom == COM_TRS_DAT )
			{	
				//登记通信结果(已发送数据申请)，并进入发送结束状态
				L2_TRS_FLAG_ENABLE(T_DAT_REQ,L2_ERR_NONE)					
				l2Trs = L2_TRS_BRD_OVER;
				goto L2_Boardcast_Trs_Over;
			}
			else// if( phTmpTrsCom == COM_TRS_ERR )
			{
				//登记通信结果（发送失败），并进入发送结束状态
				L2_TRS_FLAG_ENABLE(T_ERR,L2_PH_BUS_ERROR)
				l2Trs = L2_TRS_BRD_OVER;					
				goto L2_Boardcast_Trs_Over;
			}				  
			break;
		case L2_TRS_BRD_OVER:
			L2_Boardcast_Trs_Over:
			//发送完毕回调函数触发应用接口层读取数据发送结果
			l2TrsFrameBuf.frameCallback();																		  
			//登记通信结果并进入发送结束状态
			l2Trs = L2_TRS_NONE;					
			l2Status = L2_STAT_READ;				  	
		  	break;
			
		//3.等待数据发送完成
		case L2_TRS_DAT_WAIT:
			//扫描发送结果
			Ph_PollingWriteResult(&phTmpTrsCom);
			//等待是否发送完成
			//printf("phTmpTrsCom = %d ,l2ReTrs.enable =%d,l2BusMonEnable = %d\n",phTmpTrsCom,l2ReTrs.enable,l2BusMonEnable);
			// lzh_20181116_s
			if(  phTmpTrsCom == COM_TRS_DAT_HAVE_ACK )
			{
				L2_TRS_FLAG_ENABLE(T_DAT_CON,L2_ERR_NONE)
				l2Trs = L2_TRS_DAT_OVER;
				MICRO_L2_NORMAL_TRS_OVER				
			}
			else if( phTmpTrsCom == COM_TRS_DAT )
			//if( phTmpTrsCom == COM_TRS_DAT )
			// lzh_20181116_e
			{
				//查询是否需要ACK应答信号
				if( l2ReTrs.enable && (!l2BusMonEnable) )	//lzh_20130403
				{
					//ResetRcv();
					l2Trs = L2_TRS_RCV_ACK;	//发送完成后进入等待ACK信号状态	
					//启动定时器，10ms后触发
					// lzh_20161207_s
					OS_SetTimerPeriod(&TimerTrs,MAC_ACCESS_TYPE_TAB[MAX_INTERVAL_WAIT_ACK]/25+3);		//czn_20170816
					OS_RetriggerTimer(&TimerTrs);
					//printf("OS_RetriggerTimer(&TimerTrs);\n");
					// lzh_20161207_e
				}
				else
				{
					//登记通信结果(已发送数据申请)，并进入发送结束状态
					L2_TRS_FLAG_ENABLE(T_DAT_REQ,L2_ERR_NONE)
					l2Trs = L2_TRS_DAT_OVER;
					//goto L2_Normal_Trs_Over;
					MICRO_L2_NORMAL_TRS_OVER					
				}
			}
			else //if( phTmpTrsCom == COM_TRS_ERR )
			{
				//登记通信结果（发送失败），并进入发送结束状态
				L2_TRS_FLAG_ENABLE(T_ERR,L2_PH_BUS_ERROR)
				l2Trs = L2_TRS_DAT_OVER;	
				//goto L2_Normal_Trs_Over;
				MICRO_L2_NORMAL_TRS_OVER				
			}		  
			break;
		case L2_TRS_RCV_ACK:
			//等待目标方的ACK信号
			if( phTmpRcvCom == COM_RCV_ACK )
			{					
				l2ReTrs.enable = 0;
				//登记通信结果（收到应答信号），并进入发送结束状态
				L2_TRS_FLAG_ENABLE(T_DAT_CON,L2_ERR_NONE)
				l2Trs = L2_TRS_DAT_OVER;
				//goto L2_Normal_Trs_Over;
				MICRO_L2_NORMAL_TRS_OVER				
			}
			else
			{				
				//是否需要重发
				if( IsJust2ReTranslate() )
				{	
					//printf("Ph_DataWrite;\n");
					Ph_DataWrite( (unsigned char*)&l2TrsFrameBuf.sa, l2TrsFrameBuf.pack_len, 1 );
					//再次进入发送流程
					l2Trs = L2_TRS_DAT_WAIT;
					l2Error = L2_ERR_NONE;
				}
				else if( !l2ReTrs.times )
				{
					//登记通信结果（发送失败），并进入发送结束状态
					L2_TRS_FLAG_ENABLE(T_ERR,L2_ACK_TIMEROUT)
					l2Trs = L2_TRS_DAT_OVER;
					//goto L2_Normal_Trs_Over;
					MICRO_L2_NORMAL_TRS_OVER					
				}
			}			  
			break;			
		case L2_TRS_DAT_OVER:
			//L2_Normal_Trs_Over:
			MICRO_L2_NORMAL_TRS_OVER
			break;
			
		//lzh_20130328: 单独发送ACK结束
		case L2_TRS_ACK_WAIT:
			//等待是否发送完成
			if( phTmpTrsCom == COM_TRS_ACK )
			{
				//置通信标志，进入等待应用层取数状态
				L2_TRS_FLAG_ENABLE(T_ACK_TST,L2_ERR_NONE)
				//goto L2_Normal_Trs_Over;
				MICRO_L2_NORMAL_TRS_OVER					
			}
			else
			{
				//置通信标志，进入等待应用层取数状态
				L2_TRS_FLAG_ENABLE(T_ERR,L2_ERR_NONE)
				//goto L2_Normal_Trs_Over;			  	
				MICRO_L2_NORMAL_TRS_OVER					
			}
			break;
	}
}

/*******************************************************************************************
 * @fn:		L2_LayerMonitorService
 *
 * @brief:	链路层监控服务程序，维护状态机（物理层的接收或是发送结束来驱动调用）
 *			
 * @param:  none
 *
 * @return: none
 *******************************************************************************************/
void L2_LayerMonitorService(void)
{	
	switch( l2Status )
	{
		//读数据状态
		case L2_STAT_READ:
		  	L2_LayerRead();
			break;
			
		//写数据状态
		case L2_STAT_WRITE:
		  	L2_LayerWrite();
			break;

		defalut:
			printf("!!!!!!!!!!!!!!!!!!!dt bus test l2Status = %d\n",(int)l2Status);			
			l2Status = L2_STAT_READ;
			break;
	}	
}

/*******************************************************************************************
 * @fn:		GetChecksum
 *
 * @brief:	计算一个数据包的checksum
 *			
 * @param:  ptrL2Frame - 帧类型数据包指针
 * @param:  save_cmp - 1 更新CKS，0 得到CKS比较的结果
 *
 * @return: none
 *******************************************************************************************/
unsigned char GetChecksum(L2_FRAME *ptrL2Frame,unsigned char save_cmp)
{
	unsigned char i,checksum;
	//区分新旧指令包
    	// lzh_20140508_s
	if( (ptrL2Frame->length.bitLDatCtrl.LFlag0000 == 0) && (ptrL2Frame->length.bitLDatCtrl.LFlag10 == 2) )
	{
	  	checksum = ptrL2Frame->sa ^ ptrL2Frame->da ^ ptrL2Frame->cmd ^ ptrL2Frame->length.byte ^ ptrL2Frame->ext_dat.long_type.sada ^ ptrL2Frame->ext_dat.long_type.long_len.llength;
		for( i = 0; i < ptrL2Frame->ext_dat.long_type.long_len.long_len_bit.llen; i++ )
		{
			checksum ^= ptrL2Frame->ext_dat.long_type.dat[i];
		}
		checksum ^= 0xff;		
		//dprintf("GetChecksum 1111111111111111111111111111111111111111111111111111111111111111111111\n");
	}
	else if( ptrL2Frame->length.bit.newcmd )
	{
	//if( ptrL2Frame->length.bit.newcmd )		
	//{
	// lzh_20140508_e	
	  	checksum = ptrL2Frame->sa ^ ptrL2Frame->da ^ ptrL2Frame->cmd ^ ptrL2Frame->length.byte;
		for( i = 0; i < ptrL2Frame->length.bit.len; i++ )
		{
			checksum ^= ptrL2Frame->ext_dat.old_dat[i];
		}
		checksum ^= 0xff;
	}
	else
	{
	  	checksum = ptrL2Frame->sa + ptrL2Frame->da + ptrL2Frame->cmd;
		if( ptrL2Frame->length.byte >= 5 )
		{
		  	checksum += ptrL2Frame->length.byte;
			for( i = 0; i < ptrL2Frame->length.byte-5; i++ )
			{
				checksum += ptrL2Frame->ext_dat.old_dat[i];
			}
		}
	}
	if( save_cmp )
	{
		ptrL2Frame->cks = checksum;
		return 1;
	}
	else
	{
		return( (ptrL2Frame->cks == checksum)?1:0 );
	}
}

/*******************************************************************************************
 * @fn:		UpdateL2DataWrite
 *
 * @brief:	更新L2的发送数据缓冲区
 *			
 * @param:  ptrDat  - 转发数据地址指针，需要转化为AI_DATA数据格式
 *
 * @return: none
 *******************************************************************************************/
void UpdateL2DataWrite(unsigned char *ptrDat)
{
	unsigned char i;
	AI_DATA *ptrAI;

    //lzh_20140508_s
	AI_LONG_DATA *ptrAILong;    
	ptrAILong = (AI_LONG_DATA *)ptrDat;
    //lzh_20140508_e

	ptrAI = (AI_DATA *)ptrDat;
    
	//初始化基本参数
	l2TrsFrameBuf.sa				    	= ptrAI->head.sa;
	l2TrsFrameBuf.da				    	= ptrAI->head.da;
	l2TrsFrameBuf.cmd				    	= ptrAI->head.cmd;
	
    //区分新旧指令类型
    //lzh_20140508_s
    if( (ptrAILong->head.length.bitLDatCtrl.LFlag0000 == 0) && (ptrAILong->head.length.bitLDatCtrl.LFlag10 == 2) )
    {
        l2TrsFrameBuf.sa				    				= ptrAILong->head.sa;
        l2TrsFrameBuf.da				    				= ptrAILong->head.da;
        l2TrsFrameBuf.cmd				    				= ptrAILong->head.cmd;
        l2TrsFrameBuf.ext_dat.long_type.sada				= ptrAILong->head.sada;

        l2TrsFrameBuf.ext_dat.long_type.long_len.llength	= ptrAILong->ldata.long_len.long_len_bit.llen;
        l2TrsFrameBuf.ext_dat.long_type.long_len.long_len_bit.ldaf	= ptrAILong->ldata.long_len.long_len_bit.ldaf;

        //控制字段直接copy
        l2TrsFrameBuf.length.byte							= ptrAILong->head.length.byte;

        //保存数据
        for( i = 0; i < ptrAILong->ldata.long_len.long_len_bit.llen; i++ )
        {
            l2TrsFrameBuf.ext_dat.long_type.dat[i] = ptrAILong->ldata.dat[i];
        }

        //包总长度需要加7个长指令数据包头
        l2TrsFrameBuf.pack_len = ptrAILong->ldata.long_len.long_len_bit.llen + 7;

        //保存新命令类型
        L2_SetCmdType(2);
    }
    else if( ptrAI->head.length.bit.newcmd )
    //if( ptrAI->head.length.bit.newcmd )
    //lzh_20140508_e
    {
		l2TrsFrameBuf.ext_dat.new_type.sada	= ptrAI->head.sada;
		l2TrsFrameBuf.length.byte			= ptrAI->head.length.byte;
		
		//首次发送
		l2TrsFrameBuf.length.bit.rep = 1;
		
		//长度加上扩展地址一个字节		
		l2TrsFrameBuf.length.bit.len += 1;
		
		//保存数据
		for( i = 0; i < ptrAI->head.length.bit.len; i++ )
		{		
			l2TrsFrameBuf.ext_dat.new_type.dat[i] = ptrAI->data[i];
		}
		
		//包总长度需要加6个基本字节
		l2TrsFrameBuf.pack_len = ptrAI->head.length.bit.len + 6;				
		
		//保存新命令类型
		L2_SetCmdType(1);
    }
    else
    {
		//无扩展数据
		l2TrsFrameBuf.length.byte = 0;
		
		//有扩展数据需要加上基本数据和长度本身共5个字节
		if( ptrAI->head.length.byte ) 
		{
			l2TrsFrameBuf.length.byte	= ptrAI->head.length.byte + 5;
			l2TrsFrameBuf.pack_len 	= l2TrsFrameBuf.length.byte;
		}
		else
			l2TrsFrameBuf.pack_len 	= 4;
			
		//保存数据并得到checksum
		for( i = 0; i < ptrAI->head.length.byte; i++ )
		{		
			l2TrsFrameBuf.ext_dat.old_dat[i] = ptrAI->data[i];
		}
				
		//保存旧命令类型
		L2_SetCmdType(0);
    }
	//保存checksum
	GetChecksum(&l2TrsFrameBuf,1); 	
}
static int l2LastSendTime=0;


/*******************************************************************************************
 * @fn:		L2_DataWrite
 *
 * @brief:	申请发送一正常数据包
 *			
 * @param:  macType	- MAC定时类型
 * @param:	uNew 	- 发送新指令
 *
 * @return: none
 *******************************************************************************************/
unsigned char L2_DataWrite(	MAC_TYPE macType, unsigned char uNew )
{
	//static int l2_timer=0;
	//已在发送或是监控模式下不允许发送
	if( l2Status != L2_STAT_READ)
	{
		 if((time(NULL)-l2LastSendTime)<10)
		 {
			l2Error = L2_WRITE_WAIT;	
			return 0;
		 }
		MICRO_L2_NORMAL_TRS_OVER
	}
	
	Ph_DataWrite( (unsigned char*)&l2TrsFrameBuf.sa, l2TrsFrameBuf.pack_len, uNew );
	l2LastSendTime=time(NULL);
	F_L2_SEND_NEW_CMD = uNew;
		
	//更新重发标志
	if( l2TrsFrameBuf.length.bit.newcmd )
	{
		l2TrsFrameBuf.length.bit.rep = 0;
		GetChecksum(&l2TrsFrameBuf,1);
	}
	//进入发送流程
	l2Status = L2_STAT_WRITE;
	l2Trs = L2_TRS_DAT_WAIT;
	l2Error = L2_ERR_NONE;
		//初始化重发对象
        //lzh_s_20131023        旧指令不允许重发
        if( uNew )
			// lzh_20170109_s
			//L2ReTrsInit(l2TrsFrameBuf.length.bit.ack_req,MAX_INTERVAL_BETWEEN_NOR);
			L2ReTrsInit(0,MAX_INTERVAL_BETWEEN_NOR);
			// lzh_20170109_e
        else
		    L2ReTrsInit(0,MAX_INTERVAL_BETWEEN_NOR);
        //lzh_e_20131023
	return 1;
}

/*******************************************************************************************
 * @fn:		L2_BoardcastWrite
 *
 * @brief:	申请发送一广播数据包
 *			
 * @param:  none
 *
 * @return: 1 成功，0 失败
 *******************************************************************************************/
unsigned char L2_BoardcastWrite( void )
{
	//已在发送或是监控模式下不允许发送
	if( l2Status != L2_STAT_READ )
	{
		 if((time(NULL)-l2LastSendTime)<10)
		 {
			l2Error = L2_WRITE_WAIT;	
			return 0;
		 }
		 MICRO_L2_NORMAL_TRS_OVER
	}
	
	Ph_DataWrite( (unsigned char*)&l2TrsFrameBuf.sa, l2TrsFrameBuf.pack_len, 0 );
	l2LastSendTime=time(NULL);
	F_L2_SEND_NEW_CMD = 0;
		
	//更新重发标志
	if( l2TrsFrameBuf.length.bit.newcmd )
	{
		l2TrsFrameBuf.length.bit.rep = 0;
		GetChecksum(&l2TrsFrameBuf,1);
	}
	//进入发送流程
	l2Status = L2_STAT_WRITE;
	l2Trs = L2_TRS_BRD_WAIT;
	l2Error = L2_ERR_NONE;
	//初始化重发对象,广播指令需要重发
	L2ReTrsInit(1,MAX_INTERVAL_BETWEEN_BRD);
	return 1;
}

/*******************************************************************************************
 * @fn:		L2_PollingWriteResult
 *
 * @brief:	轮询当前链路层，得到发送数据结果通信标志，若有数据则放到pdat的地址中
 *			调用物理层的发送结果，作为LLC的控制逻辑
 *
 * @param:  flag - 发送状态指针
 *
 * @return: 1 成功，0 失败
 *******************************************************************************************/
unsigned char L2_PollingWriteResult(L2_TRS_FLAG *flag)
{
	*flag = l2ComTrsFlag;
	//读取数据后清除标志
	l2ComTrsFlag = T_TRS_NONE;
	return 1;
}

/*******************************************************************************************
 * @fn:		L2_DataRead
 *
 * @brief:	轮询链路层，得到接收通信标志，若有数据则放到pdat的地址中
 *
 * @param:  pdat - 存放数据的指针；/
 * @param:  flag  - 接收通信标志
 *
 * @return: 1 成功，0 失败
 *******************************************************************************************/
unsigned char L2_DataRead(unsigned char *pdat,L2_RCV_FLAG *flag)
{
	unsigned char i,len=0;
	AI_DATA *ptrAI;
    // lzh_20140508_s
	AI_LONG_DATA *ptrAI_LDat;
	// lzh_20140508_e
	
	ptrAI = (AI_DATA *)pdat;
    // lzh_20140508_s
	ptrAI_LDat = (AI_LONG_DATA *)pdat;
	// lzh_20140508_e	
	
	if( l2ComRcvFlag == R_DAT_IND )
	{
		//判断新指令还是旧指令
		if( l2RcvFrameBuf.pack_len == 4 )
		{
			l2RcvFrameBuf.length.byte = 0;
			L2_SetCmdType(0);
		}
        // lzh_20140508_s
		else if( (l2RcvFrameBuf.length.bitLDatCtrl.LFlag0000 == 0) && (l2RcvFrameBuf.length.bitLDatCtrl.LFlag10 == 2) )
		{
			L2_SetCmdType(2);
		}
		// lzh_20140508_e			
		else if( !l2RcvFrameBuf.length.bit.newcmd )
		{
			L2_SetCmdType(0);
		}
		else
			L2_SetCmdType(1);

		log_d("L2_DataRead:%02d",l2RcvFrameBuf.da);
		//地址匹配: 监控状态、本机组地址、本机物理地址、广播地址都上传数据
		if( l2BusMonEnable || 
		   ( l2RcvFrameBuf.length.bit.daf && l2RcvFrameBuf.length.bit.newcmd ) ||	//lzh_20121212
			 //lzh_20150729_s				 
			( (l2RcvFrameBuf.length.bitLDatCtrl.LFlag0000 == 0) && (l2RcvFrameBuf.length.bitLDatCtrl.LFlag10 == 2) && l2RcvFrameBuf.ext_dat.long_type.long_len.long_len_bit.ldaf ) ||
			//lzh_20150729_e			   
		   ( l2RcvFrameBuf.da == myAddr ||l2RcvFrameBuf.da==IPG_ADDRESS||l2RcvFrameBuf.da==IXG_ADDRESS ) || 
		   ( (l2RcvFrameBuf.da&0xfc) == (myAddr&0xfc) ) || 
			   //源地址和本机地址相同也需要上传: 如从分机摘机等发送给主机的ST-TALK等命令
		   ( (l2RcvFrameBuf.sa&0xfc) == (myAddr&0xfc) ) || 
		   ( l2RcvFrameBuf.da == BROADCAST_ADDR ) ||
		   ( (l2RcvFrameBuf.cmd >= BROADCAST_CMD_LOW) && (l2RcvFrameBuf.cmd <= BROADCAST_CMD_HIGH) ) ||		//广播指令的数据也要上传数据			 
 		   ( ((l2RcvFrameBuf.da&0x40)==(myAddr&0x40))&&((l2RcvFrameBuf.cmd>=DR_SYS_CMD_START)&&(l2RcvFrameBuf.cmd<=DR_SYS_CMD_END)) ) //lzh_20121203 :单户型系统命令解析:目标地址为0x80或0xc0,命令为单户型命令类型			   
		  )
		{
			//比较checksum是否正确
			if( GetChecksum(&l2RcvFrameBuf,0) )
			{
                // lzh_20140508_s
				if( cmdType == 2 )
				{
					ptrAI_LDat->head.sa 	= l2RcvFrameBuf.sa;
					ptrAI_LDat->head.da 	= l2RcvFrameBuf.da;
					ptrAI_LDat->head.sada 	= l2RcvFrameBuf.ext_dat.long_type.sada;
					ptrAI_LDat->head.cmd	= l2RcvFrameBuf.cmd;
					//得到长指令的控制字
					ptrAI_LDat->head.length.byte = l2RcvFrameBuf.length.byte;
					//得到长指令的扩展数据包长度
					ptrAI_LDat->ldata.long_len.llength = l2RcvFrameBuf.ext_dat.long_type.long_len.llength;
					
					for( i = 0; i < ptrAI_LDat->ldata.long_len.long_len_bit.llen; i++ )
					{
						ptrAI_LDat->ldata.dat[i] = l2RcvFrameBuf.ext_dat.long_type.dat[i];
					}
					//返回头7个字节的长数据头+扩展数据个数
					len = 7 + ptrAI_LDat->ldata.long_len.long_len_bit.llen;
				}
				//新指令
				else if( cmdType == 1)
				//if( cmdType )
				// lzh_20140508_e
				{
					ptrAI->head.sa 		= l2RcvFrameBuf.sa;
					ptrAI->head.da 		= l2RcvFrameBuf.da;
					ptrAI->head.sada 	= l2RcvFrameBuf.ext_dat.new_type.sada;
					ptrAI->head.cmd		= l2RcvFrameBuf.cmd;
					
					//计数扩展数据的长度，新指令需要减去一个扩展地址的长度			
					if( l2RcvFrameBuf.length.bit.len )
						ptrAI->head.length.byte = l2RcvFrameBuf.length.byte - 1;
					else
						ptrAI->head.length.byte	= l2RcvFrameBuf.length.byte;
					
					for( i = 0; i < ptrAI->head.length.bit.len; i++ )
					{
						ptrAI->data[i] = l2RcvFrameBuf.ext_dat.new_type.dat[i];
					}
					//返回头6个字节+扩展数据个数
					len = 5 + ptrAI->head.length.bit.len;										
				}
				else
				{
					ptrAI->head.sa 		= l2RcvFrameBuf.sa;
					ptrAI->head.da 		= l2RcvFrameBuf.da;
					ptrAI->head.sada 	= 0;
					ptrAI->head.cmd		= l2RcvFrameBuf.cmd;
					//计数扩展数据的长度，旧指令需要减去前5个固定字节
					//lzh_20121120
					if( l2RcvFrameBuf.length.byte >= 5 )
					{
						ptrAI->head.length.byte = l2RcvFrameBuf.length.byte - 4;	//保留地址到数据区
						ptrAI->data[0] = l2RcvFrameBuf.length.byte;
						for( i = 0; i < ptrAI->head.length.byte-1; i++ )
						{
							ptrAI->data[i+1] = l2RcvFrameBuf.ext_dat.old_dat[i];
						}
					}
					else
						ptrAI->head.length.byte = 0;
					//返回头4个字节+扩展数据个数				
					len = 4 + ptrAI->head.length.byte;						
				}
			}
			else
			{
				L2_RCV_FLAG_ENABLE(R_ERR,L2_LRC_ERROR)
			}
		}
		//非本机地址不上传
		else
		{
			//比较checksum是否正确
			if( GetChecksum(&l2RcvFrameBuf,0) )
			{			
				L2_RCV_FLAG_ENABLE(R_RCV_NONE,L2_ERR_NONE)
			}
			else
			{
				L2_RCV_FLAG_ENABLE(R_ERR,L2_LRC_ERROR)
			}
		}
	}
	//lzh_20130413
	if( l2ComRcvFlag == R_BLK_IND )
	{
		len = 1;
	}	
	*flag = l2ComRcvFlag;
	//读取数据后清除标志
	l2ComRcvFlag = R_RCV_NONE;
	
	//lzh_20130403
	if( l2BusMonEnable )
		return 0;
	else
		return len;
}

/*******************************************************************************************
 * @fn:		L2_BlkWrite
 *
 * @brief:	块写服务
 *
 * @param:  none
 *
 * @return: 1 发送成功，0 发送失败
 *******************************************************************************************/
unsigned char L2_BlkWrite(void)
{
	uint8 block_temp;	//lzh_20130323
	
	//已在发送或是监控模式下不允许发送
	if( l2Status != L2_STAT_READ )
	{
		l2Error = L2_WRITE_WAIT;
		return 0;
	}
}

/*******************************************************************************************
 * @fn:		L2_GetLastError
 *
 * @brief:	得到链路层错误状态
 *
 * @param:  none
 *
 * @return: l2Error 错误状态
 *******************************************************************************************/
L2_ERR L2_GetLastError(void)
{
	return l2Error;
}

/*******************************************************************************************
 * @fn:		L2_GetMyAddress
 *
 * @brief:	返回当前设备的物理地址
 *
 * @param:  none
 *
 * @return: myAddr 本机地址
 *******************************************************************************************/
L2_ADDR_TYPE L2_GetMyAddress(void)
{
	return myAddr;
}

/*******************************************************************************************
 * @fn:		L2_IsNewCmdType
 *
 * @brief:	得到当前指令类型
 *
 * @param:  none
 *
 * @return: cmdType 指令类型
 *******************************************************************************************/
unsigned char L2_IsNewCmdType(void)
{
	return cmdType;
}

/*******************************************************************************************
 * @fn:		L2_SetCmdType
 *
 * @brief:	发送时设置当前指令系统
 *
 * @param:  cmdType - 指令类型
 *
 * @return: none
 *******************************************************************************************/
void L2_SetCmdType(unsigned char type)
{
	cmdType = type;
}

//lzh_20130328
/*******************************************************************************************
 * @fn:		L2_AckWrite
 *
 * @brief:	Ack写服务
 *
 * @param:  none
 *
 * @return: 1 发送成功，0 发送失败
 *******************************************************************************************/
unsigned char L2_AckWrite(void)
{
	//已在发送或是监控模式下不允许发送
	if( l2Status != L2_STAT_READ )
	{		
		l2Error = L2_WRITE_WAIT;
		return 0;
	}
}

/*******************************************************************************************
 * @fn:		L2_IsNeedAck
 *
 * @brief:	判断接收数据是否需要应答
 *
 * @param:  none
 *
 * @return: 1 需要应答，0 不需要应答
 *******************************************************************************************/
unsigned char L2_IsNeedAck(void)
{
    // lzh_20140508_s
	if( (l2RcvFrameBuf.length.bitLDatCtrl.LFlag0000 == 0) && (l2RcvFrameBuf.length.bitLDatCtrl.LFlag10 == 2) )
	{		
		if( (!l2BusMonEnable) && (!l2RcvFrameBuf.ext_dat.long_type.long_len.long_len_bit.ldaf) && (l2RcvFrameBuf.da == myAddr) && l2RcvFrameBuf.length.bit.ack_req )
		{
			//比较checksum是否正确
		  	return GetChecksum(&l2RcvFrameBuf,0);
		}
		else
			return 0;
	}
	else if( l2RcvFrameBuf.length.bit.newcmd )
	//if( l2RcvFrameBuf.length.bit.newcmd )
	// lzh_20140508_e
	{
		//地址匹配: 监控状态、本机组地址、本机物理地址、广播地址都上传数据
		if( (!l2BusMonEnable) && (!l2RcvFrameBuf.length.bit.daf) && (l2RcvFrameBuf.da == myAddr) && l2RcvFrameBuf.length.bit.ack_req )
		{
			//比较checksum是否正确
		  	return GetChecksum(&l2RcvFrameBuf,0);
		}
		else
			return 0;
	}
	else
		return 0;
}


