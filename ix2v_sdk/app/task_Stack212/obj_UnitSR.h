/**
  ******************************************************************************
  * @file    obj_SR.h
  * @author  zxj
  * @version V00.01.00
  * @date    2012.08.18
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2012 V-Tec</center></h2>
  ******************************************************************************
  */ 

#ifndef _obj_UnitSR_H
#define _obj_UnitSR_H

//#include "RTOS.h"



//SR_TYPE(功能类型)
#define SYSTEM			9
#define INNER_BRD       7
#define INTERCOM_BRD    6
#define CALLING			5
#define MONITOR			4
#define MEM_PLAY_BACK	3
#define NAMELIST_CALL	2
#define SR_INNER_CALL	1

//获取链路创建指令
#define TAKE_CREATE_COMD			0	
//获取链路释放指令
#define TAKE_CLOSE_COMD				1	
//获取链路释放指令
#define TAKE_LEVEL					2	

//链路使用申请应答类型:
#define	SR_ENABLE_DIRECT	0		//可直接使用
#define	SR_ENABLE_DEPRIVED	1		//可被剥夺使用
#define	SR_DISABLE			2		//不可使用(正在使用,且使用者优先级别高)

//链路控制的状态
#define SR_ROUTING_STATE_IDLE		0		//无动作
#define SR_ROUTING_STATE_CREAT		1		//链路创建中
#define SR_ROUTING_STATE_CLOSE		2		//链路关闭中
#define SR_ROUTING_STATE_DEPRIVED	3		//链路剥夺中
#define SR_ROUTING_STATE_TALKING	4		//通话占用申请中



//链路状态管理
typedef struct SR_STATE_STRU
{	
	uint8	in_use;			//链路状态 0-空闲,1=占用
	uint16	manager_IA;		//链路管理者IA
	uint8	function;		//功能类型 或 功能优先级编码
	uint8	talking;		//通话占用标志
//read_change //暂时无用	uint32	on_time;		//链路占用定时,仅系统防卫者使用
}	SR_STATE_C;
extern SR_STATE_C	SR_State;

//运行状态
typedef struct SR_ROUTING_STRU
{	
	uint8	stage;			//工作状态
	uint8	control;		//0-非当前链路管理者, 1-当前链路管理者
}	SR_ROUTING_C;
extern SR_ROUTING_C 	SR_Routing;

//表
typedef struct SR_COMMAND_STRU
{
	uint8	sr_type;			//SR功能类型
	uint8	SR_creat_command;	//链路创建指令
	uint8	SR_close_command;	//链路释放指令
	uint8	level;				//优先级	
}	SR_COMMAND;

//表
typedef struct SR_TYPE_STRU
{
	uint8	call_type;			//呼叫类型
	uint8	sr_type;			//SR功能类型
}	SR_TYPE;


// Define Object Property-------------------------------------------------------
#define WAIT_TIME_MS	500		//500ms


// Define Object Function - Public----------------------------------------------
uint8 Take_SR_Type(uint8 call_type);
uint8 Take_Sr_Comd(uint8 sr_type,uint8 get_type);
uint8 SR_Routing_Create(uint8 call_type);
uint8 SR_Routing_Close(uint8 call_type);
uint8 SR_Routing_Deprived(void);
uint8 SR_Routing_Talking(void);

void SR_SetState_Locate(uint8 sr_type,uint16 SR_mastery_IA);
void SR_ClearState_DeLocate(void);
void SR_SetState_Talking(void);
void SR_ClearState_Talking(void);

void vtk_ObjectInit_SR(void);

// Define Object Function - Private---------------------------------------------



// Define API-------------------------------------------------------------------
	
	/*uint8 API_Take_SrType_FromComd(uint8 creat_comd)
	入口:  
		creat_comd(创建链路的指令类型)

	出口:
		SR功能类型
	*/
	uint8 API_Take_SrType_FromComd(uint8 creat_comd);

	/*uint8 API_Take_SR_Type(uint8 call_type);
	入口:	无

	处理:	创建链路_CALLER

	出口:
		0x00 = 创建成功
		0x01 = 创建失败
	*/
	#define API_Take_SR_Type(call_type) 			\
			Take_SR_Type(call_type)

	/*uint8 API_SR_CallerCreate(void)
	入口:	无

	处理:	创建链路_CALLER

	出口:
		0x00 = 创建成功
		0x01 = 创建失败
	*/
	#define API_SR_CallerCreate() 			\
			SR_Routing_Create(Caller_Run.call_type)

	/*uint8 API_SR_CallerClose(void)
	入口:	无

	处理:	释放链路_CALLER

	出口:
		0x00 = 释放成功
		0x01 = 释放失败
	*/
	#define API_SR_CallerClose() 			\
			SR_Routing_Close(Caller_Run.call_type)

	/*uint8 API_SR_MonitorCreate(void)
	入口:	无

	处理:	创建链路_MONITOR

	出口:
		0x00 = 创建成功
		0x01 = 创建失败
	*/
	#define API_SR_MonitorCreate() 			\
			SR_Routing_Create(CT11_ST_MONITOR)

	/*uint8 API_SR_MonitorClose(void)
	入口:	无

	处理:	释放链路_CALLER

	出口:
		0x00 = 释放成功
		0x01 = 释放失败
	*/
	#define API_SR_MonitorClose() 			\
			SR_Routing_Close(CT11_ST_MONITOR)


	/*uint8 API_SR_BeCalledClose(void)
	入口:	无

	处理:	释放链路_BeCalled

	出口:
		0x00 = 释放成功
		0x01 = 释放失败
	*/
	#define API_SR_BeCalledClose() 			\
			SR_Routing_Close(BeCalled_Run.call_type)


	/*uint8 API_SR_Talking(void)
	入口:	无

	处理:	通话占用

	出口:
		0x00 = 成功
		0x01 = 失败
	*/
	#define API_SR_Talking()	SR_Routing_Talking()


	/*uint8 API_SR_Deprived(void)
	入口:	无

	处理:	强制释放链路

	出口:
		0x00 = 成功
		0x01 = 失败
	*/
	#define API_SR_Deprived()	SR_Routing_Deprived()

////////////////////////////////////////////////////////////////////////////////
                
    	/*uint8 API_SR_Request(uint8 call_type)
	入口:	呼叫类型

	处理:	根据呼叫类型,进行链路申请

	出口:	
			SR_ENABLE_DIRECT	0		//可直接使用
			SR_ENABLE_DEPRIVED	1		//可被剥夺使用
			SR_DISABLE			2		//不可使用(正在使用,且使用者优先级别高)
	*/
	uint8 API_SR_Request(uint8 call_type);

	/*void API_SR_SetState_Locate(uint8 sr_type,uint16 SR_mastery_IA)
	入口:	SR功能类型,SR管理者之IA

	处理:	状态维护_链路使用

	出口:	无
	*/
//	#define API_SR_SetState_Locate(sr_type, sr_mastery_IA) 			\
//			SR_SetState_Locate(sr_type, sr_mastery_IA)

	/*void API_SR_ClearState_DeLocate(void);
	入口:	无

	处理:	状态维护_链路释放

	出口:	无
	*/
//	#define API_SR_ClearState_DeLocate()	SR_ClearState_DeLocate()

	/*void API_SR_SetState_Talking(void)
	入口:	无

	处理:	状态维护_通话占用

	出口:	无
	*/
	#define API_SR_SetState_Talking()	SR_SetState_Talking()


#endif
