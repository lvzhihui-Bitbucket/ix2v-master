/**
  ******************************************************************************
  * @file    ai_layer.c
  * @author  lv
  * @version V1.0.0
  * @date    2012.06.20
  * @brief   This file contains the functions of ai_layer.c
  ******************************************************************************
  *	@Э��Ӧ�ò㴦������
  *	@ͨ�ø�ʽ���ݰ��Ĵ�����
  *		1)IO Server APCIָ���
  *		2)��ָ���
  * <h2><center>&copy; COPYRIGHT 2012 V-Tec</center></h2>
  ******************************************************************************
  */ 

#ifndef _A_LEVEL_H
#define _A_LEVEL_H

#include "RTOS.h"
#include "obj_L2Layer.h"

// Define Object Property

//Э��ջ�ӿڲ㷢�͵���·�����Ϣͷ�ṹ����
#pragma pack(1)	//stm32_stack
typedef struct
{
	//B1
	unsigned char sa;
	//B2
	unsigned char da;
	//B3
	unsigned char sada;
	//B4
	unsigned char cmd;
	//B5
	union
	{
		unsigned char byte;
		struct
		{
			unsigned char len:4;		//bit3.2.1.0:	���ȼ���
			unsigned char ack_req:1;	//bit4:	        ACK����	0/������ACK��1/����Ŀ���ַ��ACK
			unsigned char rep:1;		//bit5:	        �״η��ͣ�	0/���·��ͣ�1/�״η���
			unsigned char newcmd:1;		//bit6:		    ��ָ�����ʽ
			unsigned char daf:1;		//bit7: 		��ַ���ͣ� 0/�豸��ַ��1/���ַ
		}bit;
        // lzh_20140508_s
		struct
		{
			unsigned char LFlag0000:4;	//bit3.2.1.0:	��ָ�ָ��ı�ʶ(0000)
			unsigned char ack_req:1;	//bit4:	        ACK����	0/������ACK��1/����Ŀ���ַ��ACK
			unsigned char rep:1;		//bit5:	        �״η��ͣ�	0/���·��ͣ�1/�״η���
			unsigned char LFlag10:2;	//bit7.6: 		��ָ�ָ��ı�ʶ(10)
		}bitLDatCtrl;
		// lzh_20140508_e
	}length;
} AI_HEAD;

//Ӧ�ò�����·��Ľ����ṹ
typedef struct
{
	AI_HEAD head;
	unsigned char data[MAX_FRAME_LENGTH];
} AI_DATA;

// lzh_20140508_s
typedef struct
{
	AI_HEAD head;
	struct
	{
		union
		{
			unsigned char llength;
			struct
			{
				unsigned char llen:7;
				unsigned char ldaf:1;
			}long_len_bit;
		}long_len;
		unsigned char dat[MAX_FRAME_LENGTH-1];
	}ldata;
} AI_LONG_DATA;
// lzh_20140508_e

#pragma pack()	//stm32_stack


//�ص��������壬�����û���չ����
typedef struct
{
	void (*BlockHookFunction)(void);
	unsigned char flag;
} AI_HOOK;

//Ӧ�ò㹤��ģʽ����
typedef enum
{
	AI_STAT_IDLE = 0,	//������ģʽ
	AI_STAT_WRITE,		//д����ģʽ
} AI_STAT;

//Ӧ�ò����ģʽ����
typedef enum
{
	AI_RCV_NONE = 0,		//��ȡ����״̬
	//��
	AI_RCV_IO_TRS_RES,		//���յ�IO��IND����뷢��RES״̬
	AI_RCV_IO_WAIT_RCON,	//�ȴ�����RES��ɺ��RCON
	AI_RCV_IO_OVER,			//����IO���ݽ���
	//���յ�GO�������������
	AI_RCV_GO_W_IND,		//����GOд��������
	AI_RCV_GO_W_IND_OVER,	//����GOд�����������
	//���յ�GO�������������
	AI_RCV_GO_R_REQ,		//����GO����������
	AI_RCV_GO_R_REQ_RES,	//�ȴ�GO Serverȡ����
	AI_RCV_GO_R_REQ_RCON,	//�ȴ�����Res���ݽ���
	AI_RCV_GO_R_REQ_OVER,	//����GO���������
	//���յ�������
	AI_RCV_BLK_IND,			//���յ�������
	AI_RCV_BLK_OVER,		//���յ������������
} AI_RCV_STAT;

//Ӧ�ò㷢��ģʽ����
typedef enum
{
	AI_TRS_NONE = 0,			//δ��������״̬
	//����IO���������������
	AI_TRS_IO_R_WAIT_LCON,		//������IO��������󣬵ȴ�LCON
	AI_TRS_IO_R_WAIT_ACON,		//����ȷ�Ϻ󣬵ȴ�Ŀ�귽��ҵ���źţ������У�
	AI_TRS_IO_R_DAT_OVER,		//�������ݽ������ȴ�ȡ��״̬�˳�
	//����GO�������������	
	AI_TRS_IO_WAIT_LCON,		//������IO���ݺ󣬵ȴ�LCON
	AI_TRS_IO_DAT_OVER,		//�������ݽ������ȴ�ȡ��״̬�˳�
	
	//����GO�������������
	AI_TRS_GO_WAIT_LCON,		//������GO��������󣬵ȴ�LCON
	AI_TRS_GO_WAIT_ACON,		//����ȷ�Ϻ󣬵ȴ�Ŀ�귽��ҵ���źţ�����û�У�
	AI_TRS_GO_DAT_OVER,		//�������ݽ������ȴ�ȡ��״̬�˳�
	//����GO�������������
	AI_TRS_GO_UPDATE,		//GO���º�ͬ���������豸
	AI_TRS_GO_UPDATE_OVER,	//GO���º�ͬ���������豸����
	//������
	AI_TRS_BLK_WAIT_LCON,	//�����˿�����󣬵ȴ�LCON
	AI_TRS_BLK_OVER,		//�����˿�����󣬵ȴ�ȡ��״̬
	//
	AI_TRS_OVER_ERR,
} AI_TRS_STAT;

//Ӧ�ò������������
typedef enum
{	AI_ERR_NONE = 0,	//����֡����
	AI_LCON_TIMEROUT,	//�����������ݰ���ȴ�LCON��ʱ
	AI_ACON_TIMEROUT,	//�����������ݰ���ȴ�ACON��ʱ
	AI_TRS_WAIT,		//��·�㷢�͵ȴ���
	AI_L2_ERROR,		//��·�����
} AI_ERR;

//Ӧ�ò�������н�������û�����ͨ�ű�־��
typedef enum
{
	AI_R_RCV_NONE = 0,	//���շ�����
	AI_R_BLK_IND,		//Ӧ�ò���յ������ָ��
	AI_R_DAT_IND,		//Ӧ�ò���յ�����֡
	AI_R_ERR,			//Ӧ�ò���յ�����ָ��
} AI_RCV_FLAG;

//Ӧ�ò㷢�����н�������û�����ͨ�ű�־��
typedef enum
{
	AI_T_TRS_NONE = 0,	//���շ�����
	AI_T_BLK_REQ,		//Ӧ�ò㷢���˿����ָ��
	AI_T_DAT_LCON,		//Ӧ�ò㷢������������
	AI_T_DAT_ACON,		//Ӧ�ò���յ�ACONӦ��
	AI_T_ERR,			//��·�㷢�����ݴ���
} AI_TRS_FLAG;

extern AI_STAT aiStatus;

// Define Object Function - Private

// Define Object Function - Public

//Э��ջӦ�ýӿڲ��ṩ����·��Ľ��ջص�����
void L2NotifyCallbackRcvProcess(void);
//Э��ջӦ�ýӿڲ��ṩ����·��ķ��ͷ����źŻص�����
void L2NotifyCallbackTrsEchoProcess(void);

//Э��ջӦ�ýӿڲ���շ���
void AI_LayerRcvService(void);
//Э��ջӦ�ýӿڲ㷢�ͷ���
void AI_LayerTrsPolling(void);

//Ӧ�ýӿڲ�ͨ�Žӿڷ���

//����һ����ָ���ʽ����
unsigned char SendOneOldCommand(unsigned char boardcast,unsigned char target_addr,unsigned char cmd,unsigned char *ptrDat,unsigned char len);
//����һ����ָ���ʽ����
unsigned char SendOneNewCommand(unsigned char daf, unsigned char rsp, unsigned char ack, unsigned short target_addr,unsigned char cmd,unsigned char *ptrDat,unsigned char len);

//lzh_20130328 //����һ��ACK�����ź�
unsigned char SendOneNewAck(void);
//lzh_20130413	//����һ��block�ź�
unsigned char SendOneBlock(void);
#endif
