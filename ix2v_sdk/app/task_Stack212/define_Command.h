/**
  ******************************************************************************
  * @file    define_Command.h
  * @author  zxj
  * @version V00.01.00
  * @date    2012.08.18
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2012 V-Tec</center></h2>
  ******************************************************************************
  */ 

#ifndef _define_Command_H
#define _define_Command_H


/*========================================================================
						��ָ����ض���.read
========================================================================*/
//��Ӱģʽ
#define	MEMO_AUTO				0
#define MEMO_MANUAL				1

//�㲥��ַ
#define	BROADCAST_ADDRESS		0x38

//��һ��������ַ
#define	DS1_ADDRESS				0x34
//�ڶ���������ַ
#define	DS2_ADDRESS				0x35
//������������ַ
#define	DS3_ADDRESS				0x36
//���ĸ�������ַ
#define	DS4_ADDRESS				0x37

#define	IPG_ADDRESS				0x2C
#define	IXG_ADDRESS				0x2D

//�������ĵ�ַ
#define	LOCAL_GL_ADDRESS		0x3C

//��Ⱥ(0~15)��Ӱ��ַ
#define MEMO_GROUP_0_15_ADDRESS		0x50
#define MEMO_GROUP_16_31_ADDRESS	0x54

//����ģʽ
#define DS_SYSTEM				0			//ϵͳ��
#define DS_SINGLE				1			//������

//ָ��ģʽ
#define MODE_OLD_COM			0			//��ָ��
#define MODE_NEW_COM			1			//��ָ��

//�������ʹ���
#define	CT01_GL_CALL_ST			1		//�������ĺ��зֻ�
#define	CT02_GL_CALL_DS			2		//�������ĺ��е�Ԫ����
#define	CT03_GL_CALL_CDS		3		//�������ĺ���С������
#define	CT04_GL_MONITOR_DS		4		//�������ļ��ӵ�Ԫ����
#define	CT05_GL_MONITOR_CDS		5		//�������ļ���С������

#define CT06_CDS_CALL_ST		6		//С���������зֻ�
#define CT07_CDS_CALL_GL		7		//С���������й�������

#define CT08_DS_CALL_ST			8		//��Ԫ�������зֻ�
#define CT09_DS_CALL_GL			9		//��Ԫ�������й�������

#define CT10_ST_CALL_GL			10		//�ֻ����й�������
#define	CT11_ST_MONITOR			11		//�ֻ�����
#define	CT12_ST_MEM_PLAY		12		//�ֻ���ѯ��Ӱ(������Ӱ)
#define CT13_INTERCOM_CALL		13		//����ͨ��
#define	CT14_INNER_CALL			14		//����ͨ��

//task_event
#define	RESPONSE_OK			(1<<1)
#define	RESPONSE_ERR		(1<<0)


/*========================================================================
							ָ���.read
========================================================================*/
#define E_VOLTAGE				0x30
#define E_MODEL_EDITION			0x31
#define NAMELIST_APPOINT		0x32
#define E_COLLIGATE				0x33

#define TPC_SET_TEL_NBR			0x34		//S->TPC	long cmd
#define TPC_GET_TEL_NBR			0x35		//S->TPC	long cmd
#define TPC_DAT_TEL_NBR			0x36		//TPC->S	long cmd
#define TPC_DIVERT_START		0x37		//S->M		long cmd
#define TPC_MSG_DEBUG			0x38		//TPC-S		long cmd

#define NAMELIST_BROAD			0x40
#define MON_TIMER				0x41
#define CAMERA_SET				0x42
#define CLOCK_SET				0x43
#define SET_SERIAL_NUM          0x44 // 20151126

#define STATE_RESET				0x50		
#define DEVICE_CALL_GL_ON		0x51
#define DEVICE_CALL_GL_OFF		0x52
#define GL_CALL_DEVICE_ON		0x53
#define GL_CALL_DEVICE_OFF		0x54
#define MR_CALL_ST_ON			0x55		//�������зֻ�״̬ͨ��
#define MR_CALL_ST_OFF			0x56		//�������зֻ�״̬ͨ��
#define ST_MON_ON				0x57		//�ֻ�����״̬ͨ��
#define ST_MON_OFF				0x58		//�ֻ�����״̬ͨ��
#define ST_CALL_ST_ON			0x59		//����ͨ��״̬ͨ��
#define ST_CALL_ST_OFF			0x5a		//����ͨ��״̬ͨ��
#define INTERCOM_ON				0x5b		//����ͨ��״̬ͨ��
#define INTERCOM_OFF			0x5c		//����ͨ��״̬ͨ��
#define NAMELIST_ON				0x5d		//NameList����Ⱥ����ʼ
#define NAMELIST_OFF			0x5e		//NameList����Ⱥ������	
#define LAOHUA_ON1				0x5f
#define LAOHUA_ON2				0x60
#define LAOHUA_ON3				0x61
#define MR_CALL_ST_TALK			0x62		//�������зֻ�����ͨ��״̬ͨ��
#define LAOHUA_OFF				0x63
#define AE_RESTART				0x64		//ǿ�Ƹ�λ

#define MAIN_CALL				0x70		//�������зֻ�
#define S_CANCEL				0x71		//����ȡ�����зֻ�
#define MR_LINKING_ST			0x72		//�������ֻ��Ƿ�����
#define ASK_VOLTAGE_TEST		0x73		
#define ASK_VOLTAGE				0x74
#define ASK_MODEL_EDITION		0x75
#define RECEIPT					0x76		//������Ӧ��ָ��(ϵͳ������)
#define MOVE_MON				0x77
#define MAIN_CALL_TEST			0x78
#define ASK_COLLIGATE			0x79
#define ASK_VOLTAGE_STATE		0x7a
#define MAIN_CALL_GROUP			0x7b		//����������������Ⱥ�ӷֻ�
#define MAIN_CANCEL_GROUP		0x7c		//����������������,ʹδժ��֮�ֻ��˳�����
#define MAIN_CALL_ONCE_L		0x7d		//����������������Ⱥ���ֻ�
#define RECEIPT_SINGLE			0x7e		//������Ӧ��ָ��(����������)

#define ST_TALK					0x80
#define ST_UNLOCK				0x81
#define ST_CLOSE				0x82
#define SUB_RECEIPT				0x83		//�ֻ���Ӧ��ָ��
#define STATE_DISVISIT			0x84		
#define ST_LINK_MR				0x85		//�ֻ���������Ƿ�����
#define MON_ON					0x86
#define MON_OFF					0x87
#define MON_ON_APPOINT			0x88
#define MON_OFF_APPOINT			0x89
#define VOLTAGE_OK				0x8a
#define VOLTAGE_ERROR			0x8b
#define LET_MR_CALLME			0x8c		//ʹ�������б��豸
#define ST_UNLOCK_SECOND		0x8d		
#define ST_ASK_NAMELIST			0x8e		//ʹ��������NameList����

#define INNER_CALL				0x90		//��������ͨ��
#define INNER_TALK				0x91		//����ͨ��_����ժ��
#define INNER_CLOSE				0x92		//����ͨ��_�һ�

#define INTERCOM_CALL			0x98		//��������ͨ��
#define INTERCOM_TALK			0x99		//����ͨ��_����ժ��
#define INTERCOM_CLOSE			0x9a		//����ͨ��_�һ�

#define CALL_TRANSFER_ON		0xA0		//S->S: 
#define CALL_TRANSFER_TALK		0xA1		//S->S: 

#define LET_DIDONG_ONCE			0xa8		//�÷ֻ�������һ��	
#define LET_WORK				0xA9		//�÷ֻ��������״̬(������Ƶ)
#define LET_TALKING				0xAA		//�÷ֻ�����ͨ��״̬(�൱��ժ������)


//zxj_20130405	//���ȷ��ʹ��GO, ������3����������
#define PLAYBACK_LAST			0xB0
#define PLAYBACK_NEXT			0xB1
#define PLAYBACK_DELETE			0xB2


#define A_PropertyValue_Read		0xe0
#define A_PropertyValue_Write		0xe1
#define A_PropertyValue_Response	0xe2

#define SUB_MODEL				0xf0
#define SUB_MODEL_1				0xf1
#define FUTE_AL_MSG				0xf2
#define FUTD_AL_MSG				0xf3
#define FUTE_AL					0xf4
#define FUTE_MSG				0xf5


#define TPC_IM_KEEP_CALLING_STATE	0xF6    //TPC-M  short cmd
#define GP_LINKING					0xF8	//S->TPC	short cmd
#define GP_RECEIPT					0xF9	//TPC->S	short cmd

#define DIVERT_SET_REQ				0x16
#define DIVERT_SET_RSP				0x17


//RM-CMD///////////////////////////////
#define RM_SET_MENU 	0x18		
#define RM_GET_MENU 	0xAB
#define RSP_RM_MENU 	0x19

#define RM_REQ_KEY 		0x1A
#define RM_REQ_MENU 	0x1B
#define RM_IO_CTRL 		0x1C

#define RM_ANSWER 		0xAC
#define RM_CLOSE 		0xAD

#define RM_ACK 			0xAE

#define IXG_ON_LINE		0x67

#define DT_4_EXT		0x15

typedef enum
{
  RM_IDLE=0,
  RM_MENU_MAIN,
  RM_MENU_CALL,
  RM_MENU_NAMELIST,
  RM_MENU_INTERCOM, 
  RM_MENU_BE_INTERCOM,
  RM_MENU_MONITOR,
  RM_MENU_MEM,
  RM_MENU_DIVERT_INFO,
  RM_MENU_ABOUT,
  RM_MENU_SETTING, // 11 items
}RM_MENU_ID;

typedef enum
{
	RM_KEY_UP = 0,
	RM_KEY_DN,
	RM_KEY_LOCK,
	RM_KEY_BACK,
	RM_KEY_DIVERT,
	RM_KEY_MUTE,
	
	RM_KEY_UP_3S,
	RM_KEY_DN_3S,
	RM_KEY_LOCK_3S,
	RM_KEY_BACK_3S,
	RM_KEY_DIVERT_3S,
	RM_KEY_MUTE_3S,
}RM_KEY_ID;

typedef enum
{
	RM_IO_MUTE_OFF = 0,
	RM_IO_MUTE_ON,
	
	RM_IO_BRIGHT_TOGGLE,
    RM_IO_MISSED_CALL_OFF,
    RM_IO_MISSED_CALL_ON,
     RM_IO_TALK_OFF,
    RM_IO_TALK_ON,
} RM_IO_ID;

#pragma pack(1)
typedef union
{
	unsigned char byte;
	struct
	{
		unsigned char video_para:1;
		unsigned char ring_para:2;
		unsigned char talk_en:1;
		unsigned char menu_id:4;	
	}bit;

}RM_SET_MENU_INFO;

typedef union
{
	unsigned char byte;
	struct
	{
		unsigned char screen_para:2;
		unsigned char mute_state:1;
		unsigned char divert_state:1;
		unsigned char menu_id:4;	
	}bit;

}RM_INFO;

typedef union
{
	unsigned char byte;
	struct
	{
		unsigned char screen_para:2;
		unsigned char mute_state:1;
		unsigned char divert_state:1;
		unsigned char key_id:4;	
	}bit;

}KEY_INFO;
#pragma pack()


////////////////////////////////////////



#endif
