
/**
  ******************************************************************************
  * @file    task_StackAI.c
  * @author  lv
  * @version V1.0.0
  * @date    2012.06.20
  * @brief   This file contains the functions of task_StackAI.c
  */ 

#include "stack212.h"
#include "obj_APCIService.h"
#include "obj_AiLayer.h"
#include "elog.h"

//#include "task_IoServer.h"
//#include "../task_io_server/vdp_IoServer_Data.h"
//#include "../task_io_server/vdp_IoServer_State.h"		
#include "task_survey.h"
#include "vdp_uart.h"
#include "task_Power.h"

extern AI_STAT aiStatus;
extern unsigned char l2BusMonEnable;
extern STACK_REPORT_CONTROL stackReport;
//�����ص���ʱ������ lzh_20130325
OS_TIMER TimerMute;

Loop_vdp_common_buffer	vdp_StackAI_Trs_queue;
Loop_vdp_common_buffer	vdp_StackAI_Rcv_queue;
Loop_vdp_common_buffer	vdp_StackAI_sync_queue;
vdp_task_t				task_StackAI;


// lzh_20160601_e

void* vdp_StackAI_task( void* arg );
	
void vtk_TaskInit_StackAI(int priority)
{
	init_vdp_common_queue(&vdp_StackAI_Trs_queue, 500, NULL,&task_StackAI);
	init_vdp_common_queue(&vdp_StackAI_Rcv_queue, 500, NULL,&task_StackAI);
	init_vdp_common_queue(&vdp_StackAI_sync_queue, 500, NULL,&task_StackAI);
	dprintf("vdp_StackAI_task starting............\n");
	usleep(100000);
	init_vdp_common_task(&task_StackAI, MSG_ID_StackAI, vdp_StackAI_task, NULL, &vdp_StackAI_sync_queue);
	dprintf("vdp_StackAI_task starting............\n");
	#if 0
	char buff[5];
	int addr;
	API_Event_IoServer_InnerRead_All(DX_IM_ADDR, buff);
	addr=atoi(buff);
	if(addr<1||addr>31)
		addr=0;
	myAddr = 0x80|(addr<< 2);
	#endif
	L2_Initial(myAddr,0x00,L2NotifyCallbackRcvProcess,L2NotifyCallbackTrsEchoProcess);

	// 8 = N329��STM8��ȡDIP״̬
	//api_uart_send_pack(UART_TYPE_N2S_APPLY_DIP_STATUS, NULL, 0);

	//���ɷ��ͳ�ʱ��ʱ�� //lzh_20130325
	OS_CreateTimer(&TimerMute,TImerMuteCallBack,500/25);
	dprintf("vdp_StackAI_task starting............\n");
}


void exit_vdp_StackAI_task(void)
{
	exit_vdp_common_queue(&vdp_StackAI_Trs_queue);
	exit_vdp_common_queue(&vdp_StackAI_Rcv_queue);
	exit_vdp_common_queue(&vdp_StackAI_sync_queue);
	exit_vdp_common_task(&task_StackAI);
}

void* vdp_StackAI_task( void* arg )
{
	OS_TASK_EVENT MyEvents;
	vdp_task_t*	 ptask 			= (vdp_task_t*)arg;
	p_vdp_common_buffer pdb 	= 0;
	thread_log_add(__func__);
	vtk_ObjectInit_SR();
	ptask->task_StartCompleted = 1;
	while( ptask->task_run_flag )
	{
		//�ȴ����ջ��Ƿ����¼�
		MyEvents = OS_WaitSingleEventTimed(OS_TASK_EVENT_STACK_RCV_QUEUE|OS_TASK_EVENT_STACK_TRS_QUEUE,500);
		// 1. �������յ�Э��ջ������
		//if( MyEvents&OS_TASK_EVENT_STACK_RCV_QUEUE )
		{
			RcvQueueProcess();
		}
		
		// 2. �������������ת����Ϣ
		if( MyEvents&OS_TASK_EVENT_STACK_TRS_QUEUE )
		{
			AVOID_NOISE_ENABLE	//czn_20170817
			//API_POWER_STACK_MUTE_ON();
			//usleep(100*1000);
			TrsQueueProcess();
			AVOID_NOISE_DISABLE
		}
	}
	return 0;
}



void TimerMuteRetrigger(void)
{
	OS_RetriggerTimer(&TimerMute);	
}
void TImerMuteCallBack(void)
{
 	SPK_MuteOff();
}

/*******************************************************************************************
 * @fn��	CheckTrsStatusAndWaitingForTrsResult
 *
 * @brief:	����Ƿ�Ϊ����״̬�����ȴ����͵Ľ��
 * 			1)���ͽ�������Ҫ����Ƿ��͸�����EventӦ��
 * 			2)���ͽ�������Ҫ���ת���������Ƿ���Ҫ�ȴ�receipt����
 *
 * @param:  none
 *
 * @return: none
 *******************************************************************************************/
void CheckTrsStatusAndWaitingForTrsResult(void)
{	
	while( aiStatus == AI_STAT_WRITE )
	{
		OS_TASK_EVENT MyEvents;
		// �ȴ���������¼�(��ֹ��������ʱ0.6��)
		MyEvents = OS_WaitSingleEventTimed(OS_TASK_EVENT_STACK_TRS_RESULT,600);	//lzh_20130112
		// ��ȡ���ͽ��������idle״̬
		AI_LayerTrsPolling();
					
		if( MyEvents&OS_TASK_EVENT_STACK_TRS_RESULT )
		{
			#include "obj_AiLayer.h"
			extern AI_TRS_STAT aiTrsFlag;

            //lzh_20140114	//�豸���߿��ټ��
            if( aiTrsFlag != AI_TRS_OVER_ERR )
    			TrsResultResponseCallback(1);
            else
    			TrsResultResponseCallback(0);
		}
		else
		{
			TrsResultResponseCallback(0);
		}		
	}
}

/*******************************************************************************************
 * @fn��	RcvQueueProcess
 *
 * @brief:	���ն������ݴ���
 *
 * @param:  none
 *
 * @return: none
 *******************************************************************************************/
void RcvQueueProcess(void)
{
	//lzh_20140508_s	
	//char len,myData[sizeof(StackFrameQ_TaskIO)];	
	char len,myData[sizeof(StackFrameQ_StackIO)];	
    //lzh_20140508_e
	void *ptrData;		
	StackFrameQ_StackIO *ptrStackData;	
    //lzh_20140508_s	
    StackFrameQ_StackIO_LongData *ptrStackLongData;
    //lzh_20140508_e
  
	//��ȡ��Ϣ��������
	while(len = pop_vdp_common_queue( &vdp_StackAI_Rcv_queue, &ptrData, 1))
	{
		memcpy(&myData,ptrData,len);
		purge_vdp_common_queue(&vdp_StackAI_Rcv_queue);	
		if( myData[0] == OS_Q_STACK_FRAME_STACK_RCV )
		{
			ptrStackData = (StackFrameQ_StackIO*)myData;
			//1.1�������ͣ���������֡
			if( ptrStackData->datType == STACK_CMD_RECEIVE_NORMAL )
			{
				//1.1.1 ��ָ���
                 //lzh_20140508_s
                ptrStackLongData = (StackFrameQ_StackIO_LongData*)ptrStackData;
				if( (ptrStackLongData->datBuf.head.length.bitLDatCtrl.LFlag0000 == 0 ) && (ptrStackLongData->datBuf.head.length.bitLDatCtrl.LFlag10==2) )
                {
					StackAPCILongDataInputProcess(&ptrStackLongData->datBuf);
                }
				else if( ptrStackData->datBuf.head.length.bit.newcmd )
				//if( ptrStackData->datBuf.head.length.bit.newcmd )
                //lzh_20140508_e
				{
					StackAPCIInputProcess(&ptrStackData->datBuf);	
				}			
				//1.1.2 ��ָ���
				else
				{
					//����ָ�����ת����Survey����
					StackOldCommandInputProcess(&ptrStackData->datBuf);
				}
			}					
		}
		//���з��͵ȴ����ͽ�� lzh_20130410
		while( aiStatus == AI_STAT_WRITE )
		{
			OS_WaitSingleEventTimed(OS_TASK_EVENT_STACK_TRS_RESULT,600);
			AI_LayerTrsPolling();
		}		
	}	
}

/*******************************************************************************************
 * @fn��	TrsQueueProcess
 *
 * @brief:	���Ͷ������ݴ���
 *
 * @param:  none
 *
 * @return: none
 *******************************************************************************************/
#define WAIT_AWHILE_WHEN_BUSY	200
void TrsQueueProcess(void)
{
	unsigned char ret;
	char len,myData[sizeof(StackFrameQ_TaskIO)];	
	void *ptrData;		
	StackFrameQ_TaskIO 	*ptrTaskData;

	//��ȡ��Ϣ��������
	while(len = pop_vdp_common_queue( &vdp_StackAI_Trs_queue, &ptrData, 1))
	{
		memcpy(&myData,ptrData,len);
		purge_vdp_common_queue(&vdp_StackAI_Trs_queue);	
	        // lzh_20130123	//zxj_bj
	        //cao_20161105 Block_Get();
        
		if((myData[0] == OS_Q_STACK_FRAME_TASK_TRS_BRD_A) 	||
			(myData[0] == OS_Q_STACK_FRAME_TASK_TRS_BRD_B) 	||
			(myData[0] == OS_Q_STACK_FRAME_TASK_TRS_APT_A) 	||
			(myData[0] == OS_Q_STACK_FRAME_TASK_TRS_APT_B)  )
		{
			ptrTaskData = (StackFrameQ_TaskIO*)myData;
			
			//2.1ת����ָ�stack
			if( !ptrTaskData->cmdType )
			{
				//����ת�����������͵õ��Ƿ�Ϊ�㲥����
				if( (myData[0] == OS_Q_STACK_FRAME_TASK_TRS_BRD_A ) ||
					(myData[0] == OS_Q_STACK_FRAME_TASK_TRS_BRD_B ) )
				{
					ret = 1;
					if( !StackOldCommandOutputProcess(ptrTaskData,1) )
					{
						usleep(1000*WAIT_AWHILE_WHEN_BUSY);
						ret = StackOldCommandOutputProcess(ptrTaskData,1);
					}
				}
				else
				{
					ret = 1;
					if( !StackOldCommandOutputProcess(ptrTaskData,0) )
					{
						usleep(1000*WAIT_AWHILE_WHEN_BUSY);
						ret = StackOldCommandOutputProcess(ptrTaskData,0);
					}
				}	
				if( ret )
				{
					//������ҪEventӦ���TCBָ��
					if( (myData[0] == OS_Q_STACK_FRAME_TASK_TRS_BRD_B) || (myData[0] == OS_Q_STACK_FRAME_TASK_TRS_APT_B) )
					{
						//��Ҫ�ȴ�Ӧ����Ҫ��������
						if( myData[0] == OS_Q_STACK_FRAME_TASK_TRS_BRD_B )
						{
							SetWaitResponse(ptrTaskData->ptrTCB,ptrTaskData->cmd,1);
						}
						else
						{
							SetWaitResponse(ptrTaskData->ptrTCB,ptrTaskData->cmd,0);
						}
					}
					else
					{
						//����Ҫ�ȴ�Ӧ����Ҫ��������
						if( myData[0] == OS_Q_STACK_FRAME_TASK_TRS_BRD_A )
						{
							SetWaitResponse(0,ptrTaskData->cmd,1);
						}
						else
						{
							SetWaitResponse(0,ptrTaskData->cmd,0);						
						}
					}						
				}
			}
			//2.1ת����ָ�stack
			else
			{
				ret = 1;
				if( !StackNewCommandOutputProcess(ptrTaskData) )
				{
					usleep(1000*WAIT_AWHILE_WHEN_BUSY);	
					ret = StackNewCommandOutputProcess(ptrTaskData);
				}
				if( ret )
				{
					SetWaitResponse(ptrTaskData->ptrTCB,0,0);
				}
			}					
		}
		//��ⷢ�ͽ��
		CheckTrsStatusAndWaitingForTrsResult();
	}	
}

/*******************************************************************************************
 * @fn��	AI_PushOneCmdIntoQueue
 *
 * @brief:	AI�ṩAPI -- ����һ���������
 *
 * @param:  msgtype -- ��Ϣ���ͣ�addr -- ���͵�Ŀ���ַ��cmd -- ���͵����ptrDat -- ��չ������ָ�룻len -- ��չ���ݳ���
 *
 * @return: 0/ er��1/ ok
 *******************************************************************************************/
unsigned char AI_PushOneCmdIntoQueue(unsigned char msgtype,unsigned short addr,unsigned char cmd, unsigned char newCmd, unsigned char len, unsigned char *ptrDat)	//lzh_20121124
{
//#if !defined(PID_IX850) && !defined(PID_IX611) && !defined(PID_IX622)&&!defined(PID_IX821)

	StackFrameQ_TaskIO CmdPushInForSend;

	CmdPushInForSend.msgType	= msgtype;							//���͵���Ϣ����
	CmdPushInForSend.ptrTCB		= OS_GetTaskID();					//���������ID
	CmdPushInForSend.addr		= addr;								//Ŀ���ַ
	CmdPushInForSend.cmdType	= newCmd;							//0:��ָ������,1:��ָ������
	CmdPushInForSend.cmd		= cmd;								//ָ��
	CmdPushInForSend.len		= len;								//���ݳ���
	
	if( len ) 
	{
		memcpy(CmdPushInForSend.dat,ptrDat,CmdPushInForSend.len);
	}
	
	unsigned char ret;
	ret =  push_vdp_common_queue(&vdp_StackAI_Trs_queue,(unsigned char*)&CmdPushInForSend,10+CmdPushInForSend.len);	//stack_stm32
	
	OS_SignalEvent(OS_TASK_EVENT_STACK_TRS_QUEUE,&task_StackAI);
	return ret;	
//#endif		
}

/*******************************************************************************************
 * @fn��	AI_BoardCastWithoutEventAck
 *
 * @brief:	AI�ṩAPI -- ���͹㲥�������ȴ�EventӦ��
 *
 * @param:  addr -- ���͵�Ŀ���ַ��cmd -- ���͵����ptrDat -- ��չ������ָ�룻len -- ��չ���ݳ���
 *
 * @return: 0/ er��1/ ok
 *******************************************************************************************/

unsigned char AI_BoardCastWithoutEventAck(unsigned short addr,unsigned char cmd, unsigned char len, unsigned char *ptrDat)	//lzh_20121122
{
#if !defined(PID_IX850) && !defined(PID_IX611) && !defined(PID_IX622)&&!defined(PID_IX821)
	AI_PushOneCmdIntoQueue(OS_Q_STACK_FRAME_TASK_TRS_BRD_A,addr,cmd,0,len,ptrDat);
#endif	
	return 1;
}

/*******************************************************************************************
 * @fn��	AI_BoardCastWithEventAck
 *
 * @brief:	AI�ṩAPI -- ���͹㲥������ȴ�EventӦ��
 *
 * @param:  addr -- ���͵�Ŀ���ַ��cmd -- ���͵����ptrDat -- ��չ������ָ�룻len -- ��չ���ݳ���
 *
 * @return: 0/ er��1/ ok
 *******************************************************************************************/
unsigned char AI_BoardCastWithEventAck(unsigned short addr, unsigned char cmd, unsigned char len, unsigned char *ptrDat )	//lzh_20121122
{
#if !defined(PID_IX850) && !defined(PID_IX611) && !defined(PID_IX622)&&!defined(PID_IX821)
	vdp_task_t* task = OS_GetTaskID();
	if(task && task->p_syc_buf)
	{
		p_vdp_common_buffer pdb = 0;
		int cnt =0;
		while(pop_vdp_common_queue(task->p_syc_buf,&pdb, 1)>0&&cnt++<1000)
		{
			purge_vdp_common_queue(task->p_syc_buf);
		}
		
	}
	AI_PushOneCmdIntoQueue(OS_Q_STACK_FRAME_TASK_TRS_BRD_B,addr,cmd,0,len,ptrDat);

	//Power����ȴ�Ӧ���ڼ�����Լ�������
	OS_WaitEventTimed(TASK_STACK_EVENT_ACK_OK|TASK_STACK_EVENT_ACK_ER,5000);
	
#endif	
	return 1;
}

/*******************************************************************************************
 * @fn��	AI_AppointWithoutEventAck
 *
 * @brief:	AI�ṩAPI -- ����ָ���������ȴ�EventӦ��
 *
 * @param:  addr -- ���͵�Ŀ���ַ��cmd -- ���͵����ptrDat -- ��չ������ָ�룻len -- ��չ���ݳ���
 *
 * @return: 0/ er��1/ ok
 *******************************************************************************************/
unsigned char AI_AppointWithoutEventAck(unsigned short addr,unsigned char cmd, unsigned char len, unsigned char *ptrDat)	//lzh_20121122
{
#if !defined(PID_IX850) && !defined(PID_IX611) && !defined(PID_IX622)&&!defined(PID_IX821)
	AI_PushOneCmdIntoQueue(OS_Q_STACK_FRAME_TASK_TRS_APT_A,addr,cmd,0,len,ptrDat);
#endif
	return 1;	
}

/*******************************************************************************************
 * @fn��	AI_BoardCastWithEventAck
 *
 * @brief:	AI�ṩAPI -- ����ָ��������ȴ�EventӦ��
 *
 * @param:  addr -- ���͵�Ŀ���ַ��cmd -- ���͵����ptrDat -- ��չ������ָ�룻len -- ��չ���ݳ���
 *
 * @return: 0/ er��1/ ok
 *******************************************************************************************/
unsigned char AI_AppointWithEventAck(unsigned short addr,unsigned char cmd, unsigned char len, unsigned char *ptrDat)	//lzh_20121122
{
#if !defined(PID_IX850) && !defined(PID_IX611) && !defined(PID_IX622)&&!defined(PID_IX821)
	AI_PushOneCmdIntoQueue(OS_Q_STACK_FRAME_TASK_TRS_APT_B,addr,cmd,0,len,ptrDat);
	
	//Power����ȴ�Ӧ���ڼ�����Լ�������
	OS_WaitEventTimed(TASK_STACK_EVENT_ACK_OK|TASK_STACK_EVENT_ACK_ER,5000);
#endif	
	return 1;	
}

// lyx 20160325
/*******************************************************************************************
 * @fn��	AI_AppointFastLinkWithEventAck
 *
 * @brief:	AI�ṩAPI -- ���ٲ����ⲿ�豸���ߣ����ȴ�EventӦ��
 *
 * @param:  addr -- ���͵�Ŀ���ַ
 *
 * @return: 0/ off line ��1/ on line
 *******************************************************************************************/
unsigned char AI_AppointFastLinkWithEventAck(unsigned short addr)
{
#if !defined(PID_IX850) && !defined(PID_IX611) && !defined(PID_IX622)&&!defined(PID_IX821)

	OS_TASK_EVENT MyEvents = 0;

	while(OS_WaitEventTimed(TASK_STACK_EVENT_ACK_OK|TASK_STACK_EVENT_ACK_ER,1));

	AI_PushOneCmdIntoQueue(OS_Q_STACK_FRAME_TASK_TRS_APT_B,addr,APCI_FAST_LINKING,1,0,NULL);
	
	MyEvents = OS_WaitSingleEventTimed2(TASK_STACK_EVENT_ACK_OK|TASK_STACK_EVENT_ACK_ER, 500);

	// lzh_20181116_s
#if 1
	if( check_aiTrsFlag_is_AI_TRS_IO_R_WAIT_ACON() )
		return 1;
	else
		return 0;
#else
	if (MyEvents & TASK_STACK_EVENT_ACK_ER) // ok
	{
		
		return 1;
	}
	else
	{
		return 0;
	}
#endif
	// lzh_20181116_e

#endif	
}
#define ONLINE_CHECK			0
#define ONLINE_GET				1

static int bduCheckOnlineState = 0;
static vdp_task_t* bduCheckOnlineTask = NULL;
static int bduCheckType = 0;
static char bduCheckResult[6];

void Set_BDU_Check_Result(char* data)
{
	log_d("Set_BDU_Check_Result bduCheckType = %d, data = 0x%02x %02x %02x %02x %02x %02x\n", bduCheckType, data[0], data[1], data[2], data[3], data[4], data[5]);

	if(bduCheckOnlineState)
	{
		if(bduCheckType >= 0 && bduCheckType <= 16)
		{
			if(data)
			{
				memcpy(bduCheckResult, data, 6);
			}

			if(bduCheckOnlineTask)
			{
				OS_SignalEvent(TASK_STACK_EVENT_GO_OK, bduCheckOnlineTask);
			}
		}
	}
}

//检测BDU在线
int API_Check_BDU_Online(int bduId)
{
	OS_TASK_EVENT MyEvents = 0;
	char buf[2];
	int ret = 0;
	int timeCnt = 70;

	if(bduId < 1 || bduId > 8)
	{
		return ret;
	}

	while(bduCheckOnlineState && timeCnt)
	{
		usleep(100);
		timeCnt--;
	}
	
	if(!bduCheckOnlineState)
	{
		bduCheckOnlineState = 1;
		bduCheckOnlineTask = OS_GetTaskID();
		bduCheckType = bduId;
		memset(bduCheckResult, 0, 6);

		while(OS_WaitEventTimed(TASK_STACK_EVENT_GO_OK, 1));

		buf[0] = ONLINE_CHECK;
		buf[1] = bduId;
		log_d("API_Check_BDU_Online bduId = %d, MyEvents = %d\n", bduId, MyEvents);

		API_GroupValue_Write(COBID55_ONLINE_DEVICE_REQ, 4, buf);
		
		MyEvents = OS_WaitSingleEventTimed(TASK_STACK_EVENT_GO_OK, 1000);
		ret = (bduId == bduCheckResult[1] ? 1 : 0);

		log_d("API_Check_BDU_Online bduId = %d, MyEvents = %d, bduCheckResult[1]=%d\n", bduId, MyEvents, bduCheckResult[1]);

		bduCheckOnlineState = 0;
		bduCheckOnlineTask = NULL;
	}

	return ret;
}

//获取BDU的分机在线结果
int API_Get_BDU_ImOnlineResult(int bduId)
{
	OS_TASK_EVENT MyEvents = 0;
	char buf[2];
	int ret = 0;
	int timeCnt = 70;

	if(bduId < 1 || bduId > 8)
	{
		return ret;
	}

	while(bduCheckOnlineState && timeCnt)
	{
		usleep(100);
		timeCnt--;
	}
	
	if(!bduCheckOnlineState)
	{
		bduCheckOnlineState = 1;
		bduCheckOnlineTask = OS_GetTaskID();
		bduCheckType = bduId+8;
		memset(bduCheckResult, 0, 6);

		while(OS_WaitEventTimed(TASK_STACK_EVENT_GO_OK, 1));

		buf[0] = ONLINE_GET;
		buf[1] = bduId+8;
		API_GroupValue_Write(COBID55_ONLINE_DEVICE_REQ, 4, buf);
		
		MyEvents = OS_WaitSingleEventTimed(TASK_STACK_EVENT_GO_OK, 1000);

		if(bduCheckType == bduCheckResult[1])
		{
			ret = bduCheckResult[5];
			ret = (ret<<8) + bduCheckResult[4];
			ret = (ret<<8) + bduCheckResult[3];
			ret = (ret<<8) + bduCheckResult[2];
		}
		
		bduCheckOnlineState = 0;
		bduCheckOnlineTask = NULL;
	}

	return ret;
}


//通知所有BDU检测自己的分机
int API_Notice_BDU_CheckImOnline(void)
{
	OS_TASK_EVENT MyEvents = 0;
	char buf[2];
	int ret = 0;
	int timeCnt = 70;

	buf[0] = ONLINE_CHECK;
	buf[1] = 0;
	API_GroupValue_Write(COBID55_ONLINE_DEVICE_REQ, 4, buf);
	
	return 1;
}


//API for IoServer
/*******************************************************************************************
 * @fn��	API_PropertyValue_Read
 *
 * @brief:	AI�ṩAPI -- ͨ��Э��ջ����IO Server��ȡ����������ȴ�EventӦ��
 *
 * @param:  property_id -- ��ȡ������ID��no_of_elem -- ��ȡ�����Ը�����start_index -- ��ȡ�����Կ�ʼƫ������device_address -- ��ȡ���豸��ַ
 *
 * @return: 0/ ok��1/ er
 *******************************************************************************************/
unsigned char API_PropertyValue_Read(unsigned short property_id, unsigned char no_of_elem, unsigned char start_index, unsigned short device_address)
{
#if !defined(PID_IX850) && !defined(PID_IX611) && !defined(PID_IX622)&&!defined(PID_IX821)

	unsigned char dattmp[4];
	
	dattmp[0] = property_id>>8;
	dattmp[1] = property_id&0xff;
	dattmp[2] = no_of_elem;
	dattmp[3] = start_index;
  
	AI_PushOneCmdIntoQueue(OS_Q_STACK_FRAME_TASK_TRS_APT_A,device_address,APCI_PROPERTY_VAULE_READ,1,4,dattmp);

	if (OS_WaitEventTimed(TASK_STACK_EVENT_ACK_OK|TASK_STACK_EVENT_ACK_ER,5000))
	{	
		return 0;
	}
	else
#endif
	{	
		return 1;
	}
}

/*******************************************************************************************
 * @fn��	API_PropertyValue_Response
 *
 * @brief:	AI�ṩAPI -- ͨ��Э��ջ����IO Server��Ӧ��ȡ��������Ļظ������ȴ�EventӦ��
 *
 * @param:  property_id -- ��Ӧ��ȡ������ID��no_of_elem -- ��Ӧ��ȡ�����Ը�����start_index -- ��Ӧ��ȡ�����Կ�ʼƫ������device_address -- ��Ӧ���豸��ַ��ptr_Data -- ��Ӧ��ȡ������ָ��
 *
 * @return: 0/ ok��1/ er
 *******************************************************************************************/
unsigned char API_PropertyValue_Response(unsigned short property_id, unsigned char no_of_elem, unsigned char start_index, unsigned short device_address, unsigned char *ptr_Data)
{
#if !defined(PID_IX850) && !defined(PID_IX611) && !defined(PID_IX622)&&!defined(PID_IX821)
  	unsigned char i,j;
	unsigned char dattmp[MAX_FRAME_LENGTH];
	
	dattmp[0] = property_id>>8;
	dattmp[1] = property_id&0xff;
	dattmp[2] = no_of_elem;
	dattmp[3] = start_index;
	for( i = 4,j = 0; (j < no_of_elem) && (i < MAX_FRAME_LENGTH); i++,j++ )
	{
	  	dattmp[i] = ptr_Data[j];
	}
	AI_PushOneCmdIntoQueue(OS_Q_STACK_FRAME_TASK_TRS_APT_A,device_address,APCI_PROPERTY_VAULE_RESPONSE,1,i,dattmp);

	if (OS_WaitEventTimed(TASK_STACK_EVENT_ACK_OK|TASK_STACK_EVENT_ACK_ER,5000))
	{	
		return 0;
	}
	else
#endif	
	{	
		return 1;
	}
}

/*******************************************************************************************
 * @fn��	API_PropertyValue_Write
 *
 * @brief:	AI�ṩAPI -- ͨ��Э��ջ����IO Server��Ӧ��ȡ��������Ļظ������ȴ�EventӦ��
 *
 * @param:  property_id -- д�������ID��no_of_elem -- д������Ը�����start_index -- д������Կ�ʼƫ������device_address -- д����豸��ַ��ptr_Data -- д���ȡ������ָ��
 *
 * @return: 0/ ok��1/ er
 *******************************************************************************************/
unsigned char API_PropertyValue_Write(unsigned short property_id, unsigned char no_of_elem, unsigned char start_index, unsigned short device_address, unsigned char *ptr_Data)
{
#if !defined(PID_IX850) && !defined(PID_IX611) && !defined(PID_IX622)&&!defined(PID_IX821)
	
  	unsigned char i,j;
	unsigned char dattmp[MAX_FRAME_LENGTH];
	
	dattmp[0] = property_id>>8;
	dattmp[1] = property_id&0xff;
	dattmp[2] = no_of_elem;
	dattmp[3] = start_index;
	for( i = 4,j = 0; (j < no_of_elem) && (i < MAX_FRAME_LENGTH); i++,j++ )
	{
	  	dattmp[i] = ptr_Data[j];
	}
	AI_PushOneCmdIntoQueue(OS_Q_STACK_FRAME_TASK_TRS_APT_A,device_address,APCI_PROPERTY_VAULE_WRITE,1,i,dattmp);

	if (OS_WaitEventTimed(TASK_STACK_EVENT_ACK_OK|TASK_STACK_EVENT_ACK_ER,5000))
	{	
		return 0;
	}
	else
#endif		
	{	
		return 1;
	}
}

//API for GoServer lzh_20121212
/*******************************************************************************************
 * @fn��	API_GroupValue_Read
 *
 * @brief:	AI�ṩAPI -- ͨ��Э��ջ����GO Server��ȡ��������
 *
 * @param:  group_id -- ���ַͨ����
 *
 * @return: 0/ ok��1/ er
 *******************************************************************************************/
unsigned char API_GroupValue_Read(unsigned short group_id)
{
#if !defined(PID_IX850) && !defined(PID_IX611) && !defined(PID_IX622)&&!defined(PID_IX821)

	AI_PushOneCmdIntoQueue(OS_Q_STACK_FRAME_TASK_TRS_APT_A,group_id,APCI_GROUP_VALUE_READ,1,0,NULL);

	OS_WaitEventTimed(TASK_STACK_EVENT_ACK_OK|TASK_STACK_EVENT_ACK_ER,5000);
#endif	
	return 0;
}

/*******************************************************************************************
 * @fn��	API_GroupValue_Response
 *
 * @brief:	AI�ṩAPI -- ͨ��Э��ջ����GO Server��Ӧ��ȡ��������Ļظ�
 *
 * @param:  group_id -- ����ַͨ���ţ�length -- ��չ���ݳ��ȣ�ptr_Data -- ��Ӧ������ָ��
 *
 * @return: 0/ ok��1/ er
 *******************************************************************************************/
unsigned char API_GroupValue_Response(unsigned short group_id, unsigned char length, unsigned char *ptr_Data)
{  
#if !defined(PID_IX850) && !defined(PID_IX611) && !defined(PID_IX622)&&!defined(PID_IX821)

	AI_PushOneCmdIntoQueue(OS_Q_STACK_FRAME_TASK_TRS_APT_A,group_id,APCI_GROUP_VALUE_RESPONSE,1,length,ptr_Data);

	OS_WaitEventTimed(TASK_STACK_EVENT_ACK_OK|TASK_STACK_EVENT_ACK_ER,5000);

#endif	
	return 0;
}

/*******************************************************************************************
 * @fn��	API_GroupValue_Write
 *
 * @brief:	AI�ṩAPI -- ͨ��Э��ջ����GO Server��Ӧ��ȡ��������Ļظ�
 *
 * @param:  group_id -- ����ַͨ���ţ�length -- ��չ���ݳ��ȣ�ptr_Data -- ��Ӧ������ָ��
 *
 * @return: 0/ ok��1/ er
 *******************************************************************************************/
unsigned char API_GroupValue_Write(unsigned short group_id, unsigned char length, unsigned char *ptr_Data)
{
#if !defined(PID_IX850) && !defined(PID_IX611) && !defined(PID_IX622)&&!defined(PID_IX821)

	AI_PushOneCmdIntoQueue(OS_Q_STACK_FRAME_TASK_TRS_APT_A,group_id,APCI_GROUP_VALUE_WRITE,1,length,ptr_Data);

	OS_WaitEventTimed(TASK_STACK_EVENT_ACK_OK|TASK_STACK_EVENT_ACK_ER, 5000);
#endif	
	return 0;
}
int Api_StartOneRouterCall(int router_id,int room_id)
{
	uint8 group_value[6];
	group_value[0] = VSIP_VERSION ;	//??
	group_value[1] = 0x00;			//???_H
	group_value[2] = GetRouterCallSourceAddr();//myAddr;		//???_L
	group_value[3] = router_id;	//????_H
	group_value[4] = (0x80|(room_id<<2));			//????_L
	group_value[5] = CT08_DS_CALL_ST;
	//????
	#if !defined(PID_IX850) && !defined(PID_IX611) && !defined(PID_IX622)&&!defined(PID_IX821)
	AI_PushOneCmdIntoQueue(OS_Q_STACK_FRAME_TASK_TRS_APT_A,GA101_CALL_INVITE,APCI_GROUP_VALUE_WRITE,1,6,group_value);
	#endif
	
}

int Api_CloseOneRouterCall(int router_id,int room_id)
{
	uint8 group_value[8];
	group_value[0] = VSIP_VERSION;	//??
	group_value[1] = 0x00;			//???_H
	group_value[2] = GetRouterCallSourceAddr();//myAddr;		//???_L
	group_value[3] = router_id;	//????_H
	group_value[4] =  (0x80|(room_id<<2));			//????_L
	group_value[5] = CT08_DS_CALL_ST;							//????
	group_value[6] = VSIP_BYE_CALLER;									//????: ??????
	group_value[7] = 0xff;											//????: 0xff???????
	//????
	#if !defined(PID_IX850) && !defined(PID_IX611) && !defined(PID_IX622)&&!defined(PID_IX821)
	AI_PushOneCmdIntoQueue(OS_Q_STACK_FRAME_TASK_TRS_APT_A,GA102_CALL_STATE,APCI_GROUP_VALUE_WRITE,1,6,group_value);
	#endif
	
}
int Api_RouterCallToTalk(int router_id,int room_id)
{
	uint8 group_value[8];
	group_value[0] = VSIP_VERSION;	//??
	group_value[1] = 0x00;			//???_H
	group_value[2] = GetRouterCallSourceAddr();//myAddr;		//???_L
	group_value[3] = router_id;	//????_H
	group_value[4] =  (0x80|(room_id<<2));			//????_L
	group_value[5] = CT08_DS_CALL_ST;							//????
	group_value[6] = VSIP_ACK;									//????: ??????
	group_value[7] = 0xff;											//????: 0xff???????
	//????
	#if !defined(PID_IX850) && !defined(PID_IX611) && !defined(PID_IX622)&&!defined(PID_IX821)
	AI_PushOneCmdIntoQueue(OS_Q_STACK_FRAME_TASK_TRS_APT_A,GA102_CALL_STATE,APCI_GROUP_VALUE_WRITE,1,6,group_value);
	#endif
	
}


int Api_RouterCallToBeClose(int router_id,int room_id)
{
	uint8 group_value[8];
	group_value[0] = VSIP_VERSION;	//??
	group_value[1] = 0x00;			//???_H
	group_value[2] = GetRouterCallSourceAddr();//myAddr;		//???_L
	group_value[3] = router_id;	//????_H
	group_value[4] =  (0x80|(room_id<<2));			//????_L
	group_value[5] = CT08_DS_CALL_ST;							//????
	group_value[6] = VSIP_OK_201;									//????: ??????
	group_value[7] = 0xff;											//????: 0xff???????
	//????
	#if !defined(PID_IX850) && !defined(PID_IX611) && !defined(PID_IX622)&&!defined(PID_IX821)
	AI_PushOneCmdIntoQueue(OS_Q_STACK_FRAME_TASK_TRS_APT_A,GA102_CALL_STATE,APCI_GROUP_VALUE_WRITE,1,6,group_value);
	#endif
	
}
//int DFSystem_Flag=0;
#if 0
int ifDFSystem(void)
{
	return DFSystem_Flag;
}
#endif
void SetDFSystem(void)
{
	//DFSystem_Flag=1;
}