
#ifndef _OBJ_APCISERVICE_H
#define _OBJ_APCISERVICE_H

//Э��ջ�ӿڲ�APCI����
#define APCI_GROUP_VALUE_READ           0x00    
#define APCI_GROUP_VALUE_RESPONSE       0x10    //0x1x
#define APCI_GROUP_VALUE_WRITE          0x20    //0x2x
#define APCI_PROPERTY_VAULE_READ        0xF5
#define APCI_PROPERTY_VAULE_RESPONSE    0xF6
#define APCI_PROPERTY_VAULE_WRITE       0xF7
#define APCI_INDIVIDUAL_ADDR_WRITE      0x30
#define APCI_INDIVIDUAL_ADDR_READ       0x31
#define APCI_INDIVIDUAL_ADDR_RESPONSE   0x32
#define APCI_PROG_STATE_READ            0x75
#define APCI_PROG_STATE_RESPONSE        0x76
#define APCI_PROG_STATE_WRITE           0x77

#define APCI_INDIVIDUAL_ADDR_LINK       0x60    //0x6x
#define APCI_RESTART                    0xE0    //system reset
#define APCI_BLOCK                      0xD0    //���ֽ�

// 20171109
#define APCI_RM_REQUEST                 0x40
#define APCI_RM_REP_REQUEST             0x41
#define APCI_RM_QUIT                    0x42
#define APCI_RM_OK                      0x43
#define APCI_RM_EXIT                    0x44
#define APCI_RM_UP                      0x45
#define APCI_RM_DOWN                    0x46
#define APCI_RM_LAST                    0x47
#define APCI_RM_NEXT                    0x48
#define APCI_RM_DISPLAY                 0x49
#define APCI_RM_ENTER_INPUT             0x4A
#define APCI_RM_EXIT_INPUT              0x4B
#define APCI_RM_INPUT_RES               0x4C
#define APCI_DX_SET_ON                  0x4E // 20220810
#define APCI_DX_SET_OFF                 0x4F // 20220810

#define APCI_DIVERT_GET_ACOUNT		    0x50 // �ֻ���IPG��ȡSIP�˺ż�����
#define APCI_DIVERT_RSP_GET_ACOUNT		0x51 // IPG��ֻ��ظ�SIP�˺ż�����
#define APCI_DIVERT_START			    0x52 // �ֻ���IPG��������ת�����ʹ��Ĭ���˺�
#define APCI_DIVERT_RSP_START			0x53 // IPG��ֻ��ظ�ת�������֪�ɹ����
#define APCI_DIVERT_START_SPEC		    0x54 // �ֻ���IPG��������ת�����ʹ��ָ���˺�
#define APCI_DIVERT_RSP_START_SPEC		0x55 // IPG��ֻ��ظ�ת�������֪�ɹ����
#define APCI_DIVERT_RESET			    0x56 // VSIP������״̬���

#define APCI_DIVERT_START_INFORM 		0x57	//czn_20181117

#define APCI_RM_M_REQ                   0x80
#define APCI_RM_M_RSP                   0x81
#define APCI_RM_M_QUIT                  0x82
#define APCI_RM_M_UP                    0x83
#define APCI_RM_M_DN                    0x84
#define APCI_RM_M_LAST                  0x85
#define APCI_RM_M_NEXT                  0x86
#define APCI_RM_M_EXIT                  0x87
#define APCI_RM_M_OK                    0x88
#define APCI_RM_M_TP                    0x89
#define APCI_DX_SET_TP                  0x9F // 20220810


#define APCI_DIVERT_SYNC                0xF0
#define APCI_FE_ANGLE_OFFSET            0xF1
#define APCI_DS_QTY_SYNC                0xF2
#define APCI_CAM_QTY_SYNC               0xF3

#define APCI_IPC_SEARCH_ON              0xFB
#define APCI_IPG_LINK 					0xFC
#define APCI_IPG_STATE 					0xFD
#define	APCI_FAST_LINKING				0xFE // lyx 20160325

#define	GA1_VIDEO_REQUEST		1	//??????
#define GA2_VIDEO_SERVICE		2	//????????
#define GA3_MEM_PLAYBACK	3	//��Ӱ���Ų���(�Ϸ�,�·�,ɾ����)
#define GA4_MEM_REPORT		4	//��Ӱ��Ϣ����(δ����Ӱ��,����Ӱ��)
#define GA5_MEM_ORDER		5	//������Ӱ
	#define	MEMO_AUTO				0
	#define MEMO_MANUAL				1
	#define MEMO_DELETE_BYROOM		2

#define GA6_RTC_SERVICE		6	//时钟同步发出
#define GA7_RTC_CLIENT		7	//时钟同步申请
#define GA8_RTC_SET			8	//时钟设置

#define GA11_UNLOCK2_CONTROL	11	//??2??
#define GA12_UNLOCK2_REPORT		12	//??2????(??)
#define GA13_UNLOCK1_CONTROL	13	//??1??
#define GA14_UNLOCK1_REPORT		14	//??1????(??)
#define GA15_HEARTBEAT			15	//??
#define GA16_UNLOCK3_CONTROL	16	//??3??
#define GA17_UNLOCK3_REPORT		17	//??3????(??)
#define GA18_UNLOCK4_CONTROL	18	//??4??
#define GA19_UNLOCK4_REPORT		19	//??4????(??)
#define GA20_UNLOCK5_CONTROL	20	//??5??
#define GA21_UNLOCK5_REPORT		21	//??5????(??)
#define GA22_UNLOCK6_CONTROL	22	//??6??
#define GA23_UNLOCK6_REPORT		23	//??6????(??)
#define GA24_UNLOCK7_CONTROL	24	//??7??
#define GA25_UNLOCK7_REPORT		25	//??7????(??)
#define GA26_UNLOCK8_CONTROL	26	//??8??
#define GA27_UNLOCK8_REPORT		27	//??8????(??)
#define GA28_UNLOCK_ACTION_REPORT	28	//?????(??/??)	//20140911

#define COBID55_ONLINE_DEVICE_REQ		55	//主机对BDU及其下的分机检查在线情况
#define COBID56_ONLINE_DEVICE_RSP		56	//BDU向主机回复BDU及其下的分机检查在线情况

#define GA204_INTERCOMCALL_INVITE		204	//����_����
#define GA205_INTERCOMCALL_STATE		205	//����_״̬����
#define GA206_INTERCOMCALL_TRYING		206	//����_������,��ȴ�

#define GA101_CALL_INVITE		101	//??_??
#define GA102_CALL_STATE		102	//??_????
#define GA103_CALL_TRYING		103	//??_???,???


#define COBID101_CALL_INVITE	101	//??_??
#define COBID102_CALL_STATE		102	//??_????
#define COBID103_CALL_TRYING	103	//??_???,???

#define VSIP_VERSION			1	//VSIP?????
#define VSIP_TRYING_100				0	//???????INVITE??, ????.
#define VSIP_RINGING_180				1	//???????INVITE??, ???RINGING??
#define VSIP_OK_200					2	//??????,??????
#define VSIP_ACK						3	//???????200/OK, ??????
#define VSIP_BYE_BECALLED			4	//??????
#define VSIP_BYE_CALLER				5	//????????
#define VSIP_OK_201					6	//?????BYE_BECALLED???
#define VSIP_OK_202					7	//?????BYE_CALLER???
#define VSIP_ERROR				8	//????(?????)
#define VSIP_ROOM_ID					9	//??????IPG???,IPG??????   //IPG_2018

#define	VIDEO_SERVICE_SERVER_ID		0x1F
#define	VIDEO_SERVICE_ON			0x80


// Define Object Property

// Define Object Function - Private

//����Э��ջ����������ָ��
unsigned char StackAPCIInputProcess(void *ptrMsgDat);
//����������Ҫת������ָ��
unsigned char StackNewCommandOutputProcess(void *ptrMsgDat);

// lzh_20140508_s
unsigned char StackAPCILongDataInputProcess(void *ptrMsgDat);
// lzh_20140508_e

// Define Object Function - Public

//����IO Server�����͵ķ��񣺶�ȡ����
unsigned char API_PropertyValue_Read_From_Stack(unsigned short property_id, unsigned char no_of_elem, unsigned char start_index, unsigned short device_address);
//����IO Server�����͵ķ�����Ӧ����
unsigned char API_PropertyValue_Response_From_Stack(unsigned short property_id, unsigned char no_of_elem, unsigned char start_index, unsigned short device_address, unsigned char *ptr_Data);
//����IO Server�����͵ķ���д�����
unsigned char API_PropertyValue_Write_From_Stack(unsigned short property_id, unsigned char no_of_elem, unsigned char start_index, unsigned short device_address, unsigned char *ptr_Data);

#endif
