/**
  ******************************************************************************
  * @file    stack212.h
  * @author  lv
  * @version V1.0.0
  * @date    2012.06.20
  * @brief   This file contains the functions of stack
  ******************************************************************************
  * @ע��
  *	@��ͷ�ļ�ΪЭ��ջ�����ú�����ͷ�ļ���ʹ��˵�����£�
  *	@1.��Ҫ����Э��ջ�ĵط���Ҫ���ø�ͷ�ļ�: 
		1)main.c����Ҫ����vtk_TaskProcessEvent_StackAI���񣬵���vtk_TaskInit_StackAI( taskID++ );
		2)hal_board_cfg.h�м����������,������ҪЭ��ջ���ܣ�
			#ifndef HAL_STACK_212
			#define HAL_STACK_212 OTRUE
			#endif
		  hal_drivers.c�м���������䣺	
			����ͷ�ļ���
			#if (defined HAL_STACK_212) && (HAL_STACK_212 == OTRUE)
			  #include "stack212.h"
			#endif
			//����Polling����
			#if (defined HAL_STACK_212) && (HAL_STACK_212 == OTRUE)
				AI_LayerTrsPolling(); 
			#endif
		3)TimeBase.c����Ҫ����STACK212_MONITOR_SERVICE����������
  *	@2.��ͷ�ļ��ж����˵�����Ϣ�����ݽṹ�������������ͳ����Ϣ
  *	@3.���������Ķ���
  * <h2><center>&copy; COPYRIGHT 2012 V-Tec</center></h2>
  ******************************************************************************
  */ 

#ifndef _STACK212_H
#define _STACK212_H

#include "define_Command.h"
#include "obj_AiLayer.h"
#include "obj_L2Layer.h"
#include "obj_APCIService.h"
#include "obj_CmdDispatch.h"
#include "task_StackAI.h"

/********************************************************************************************************
//Э��ջͨ�Ź�����ض���
********************************************************************************************************/
extern uint8 vtk_TaskID_StackAI;
extern uint8 Survey_Task_ID;

//IO Server������ID�ض���
#define IO_SERVER_TASK_ID	0 //ram_IoServer.id
//Survey�������ض���
#define SURVEY_TASK_ID		0 //Survey_Task_ID

#define OS_TASK_EVENT_RESPONSE_OK		(1<<0)
#define OS_TASK_EVENT_RESPONSE_ER		(1<<1)
/********************************************************************************************************
//����ͳ�ƹ�����ض���
********************************************************************************************************/
#define STACK_DEBUG_REPORT

#define MSG_UARTCOMM_SENDLONG  0x05
#define MSG_UARTCOMM_SENDSHORT 0x06
#define MSG_UARTCOMM_RECV      0x07
extern uint8 Rs485_TaskID;

//��������
extern unsigned char EOS_Stack_Trace;
extern unsigned char EOS_Stack_BusMon;
//UART�������ĵ�����ź���Ϣ����
#define STACK_UART_PROCESS_TASK_ID		vtk_TaskID_Rs485 //	TestMsgUartReportApp_TaskID		//UartComm_TaskID
#define STACK_UART_PROCESS_TASK_MSG_T	MSG_UARTCOMM_SENDSHORT  //UART_PROCESS_DEBUG				//MSG_UARTCOMM_SENDSHORT
#define STACK_UART_PROCESS_TASK_MSG_R	MSG_UARTCOMM_RECV

//UARTͨ�������������Ϣ���Ͷ��壺
//״̬����
#define TP1_EOS_STAT_CONTROL		0x01		
#define TP1_EOS_STAT_CONTROL_SUB1		0x01	//����EOSͳ�ƣ�����1��0/ֹͣ���ܷ�����1/�������ܷ���
#define TP1_EOS_STAT_CONTROL_SUB2		0x02	//ֹͣEOSͳ��
#define TP1_EOS_STAT_CONTROL_START		0x03	//����Stack���٣�����1������BusMon��0/��������1/����
#define TP1_EOS_STAT_CONTROL_STOP		0x04	//ֹͣStack����
#define TP1_EOS_STAT_CONTROL_SUB3		0x05	//��ȡ��ǰ״̬��
												//����1��0/δ����ͳ�ƣ�1/����ͳ�ƣ�
												//����2��0/δ�������ܷ�����1/�������ܷ���
												//����3���ϱ�ʱ������1��60�룬default��3��
												//����4��0/δ����Stack���٣�1/����Stack����
												//����5��0/δ����BusMonģʽ��1/����BusMonģʽ
//����״̬ͳ��
#define	TP2_EOS_TASK				0x02
#define TP2_EOS_TASK_STAT				0x01
#define TP2_EOS_TASK_EOS_STAT			0x02

//Э��ջͳ��
#define TP3_EOS_STACK				0x03
#define TP3_EOS_STACK_SEND_RESULT		0x01	//�������ݼ����
#define TP3_EOS_STACK_INDICATION		0x02	//���յ�������
#define TP3_EOS_STACK_ERROR				0x03	//������
#define TP3_EOS_STACK_BUSMON			0x04	//BUSMON״̬����Ϣ
#define TP3_EOS_STACK_PH_BUSSTATE		0x05	//������״̬��Ϣ

//Ӧ�ýӿڲ����(PC����)
#define TP4_EOS_SERVICE_INDICATION	0x04
#define TP4_EOS_SERVICE_INDICATION_SUB1	0x01

//������Ϣ
#define TP5_EOS_DEBUG				0x05
#define TP5_EOS_DEBUG_PRINTF			0x01

//PC��Ϣ
#define TP6_EOS_PC				0x06
#define TP6_EOS_PC_TO_DEVICE			0x01
#define TP6_EOS_PC_FM_DEVICE			0x02

//��������
#define TP7_EOS_PROTERTY		0x07
#define TP7_EOS_PROTERTY_READ			0x01
#define TP7_EOS_PROTERTY_WRITE			0x02

//PCѯ����������
#define TP8_EOS_TASK_NAME		0x08
#define TP8_EOS_TASK_NAME_SUB1			0x01

//��ȡEOS������Ϣ
#define TP9_EOS_BASE			0x09
#define TP9_EOS_BASE_SUB1				0x01	//����1��index(1/�豸���ͣ�2/CPU��3/Clock��4/BSP)������2��DP��STRING��14chars��

//���͸�UARTͨ�������������Ϣ�ṹ��
typedef struct
{
	//��Ϣͷ
	osal_event_vtk_t hdr;
	//Э��ջ��Ϣ����
	unsigned char MType;
	unsigned char SType;
	//Э��ջ��Ϣ����
	unsigned char DAT[MAX_FRAME_LENGTH]; 
} STACK_REPORT_MSG;

//L2_STATE REPORT SEND&RESULT
typedef struct
{
	msgpkt_uartcomm_sendshortcmd_t header;
	uint8 newcmd;
	uint32 systick;
	uint8 lastmac;
	uint8 sendresult;
	uint8 DAT[MAX_FRAME_LENGTH];
} STACK_REPORT_MSG_TRS_SEND_RESULT;

//L2_STATE REPORT INDICATION
typedef struct
{
	msgpkt_uartcomm_sendshortcmd_t header;
	uint8 newcmd;
	uint32 systick;
	uint8 lastmac;
	uint8 DAT[MAX_FRAME_LENGTH];
} STACK_REPORT_MSG_TRS_INDICATION;

//L2_STATE REPORT ERROR
typedef struct
{
	msgpkt_uartcomm_sendshortcmd_t header;
	uint8 newcmd;
	uint32 systick;
	uint8 lastmac;
	L2_ERR err;
} STACK_REPORT_MSG_TRS_ERROR;

//L2_STATE REPORT BUSMON
typedef struct
{
	msgpkt_uartcomm_sendshortcmd_t header;
	uint8 newcmd;
	uint32 systick;
	uint8 lastmac;
	uint8 DAT[MAX_FRAME_LENGTH];
} STACK_REPORT_MSG_TRS_BUS_MON;

//Э��ջ���淢�Ϳ���:
typedef struct
{
	unsigned char enable;
	unsigned char ack;
} STACK_REPORT_CONTROL;

//Print Debug 
typedef struct
{
	msgpkt_uartcomm_sendshortcmd_t header;
	uint8 DAT[MAX_FRAME_LENGTH];
} PRINT_DEBUG_MSG;

//PC Command
typedef struct
{
	msgpkt_uartcomm_sendshortcmd_t header;
	uint8 DAT[MAX_FRAME_LENGTH];
} PC_COMMAND_MSG;

void TP3_EOS_STACK_Ph_BusState(void);
void TP3_EOS_STACK_L2_Report(unsigned char type);

#endif
