/**
  ******************************************************************************
  * @file    obj_SR_Routing.c
  * @author  zxj
  * @version V00.01.00
  * @date    2012.08.18
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2012 V-Tec</center></h2>
  ******************************************************************************
  */ 
	  
#if 1

#include "task_survey.h"
#include "define_Command.h"	
#include "obj_UnitSR.h"
#include "stack212.h"
//#include "task_IoServer.h"

OS_TIMER timer_clear_sr;
int clear_sr_timer;
static int SR_Request=0;
SR_ROUTING_C 	SR_Routing;

pthread_mutex_t SR_Lock = PTHREAD_MUTEX_INITIALIZER;

//根据呼叫类型,获取SR功能类型
const SR_TYPE TABLE_CallType_SrType[] = //呼叫类型,功能类型
{		
	{CT01_GL_CALL_ST		,	CALLING},			//管理中心呼叫分机
	{CT02_GL_CALL_DS		,	CALLING},			//管理中心呼叫单元主机
	{CT03_GL_CALL_CDS		,	CALLING},			//管理中心呼叫小区主机
	{CT04_GL_MONITOR_DS		,	CALLING},			//管理中心监视单元主机
	{CT05_GL_MONITOR_CDS	,	CALLING},			//管理中心监视小区主机

	{CT06_CDS_CALL_ST		,	CALLING},			//小区主机呼叫分机
	{CT07_CDS_CALL_GL		,	CALLING},			//小区主机呼叫管理中心

	{CT08_DS_CALL_ST		,	CALLING},			//单元主机呼叫分机
	{CT09_DS_CALL_GL		,	CALLING},			//单元主机呼叫管理中心

	{CT10_ST_CALL_GL		,	NAMELIST_CALL},		//分机呼叫管理中心
	{CT11_ST_MONITOR		,	MONITOR},			//分机监视
	{CT12_ST_MEM_PLAY		,	MEM_PLAY_BACK},		//分机查询留影(公共留影)
	{CT13_INTERCOM_CALL		,	NAMELIST_CALL},		//户户通话
	{CT14_INNER_CALL		,	SR_INNER_CALL},		//户内通话
    //{CT15_INNER_BRD         ,   INNER_BRD},         //户内广播
   // {CT16_INTERCOM_BRD      ,   INTERCOM_BRD},      //户户广播
};
const uint8 CallType_SrType_NUMBERS = sizeof( TABLE_CallType_SrType ) / sizeof( TABLE_CallType_SrType[0] );

//根据SR功能类型,获取其链路占用或释放的指令
const SR_COMMAND TABLE_SrType_SrComd[] = //SR功能类型,链路创建指令,链路释放指令,优先级	//read_change
{		
	{CALLING		,	MR_CALL_ST_ON	,	MR_CALL_ST_OFF		,	3	},	//主机呼叫分机
	{SR_INNER_CALL	,	INTERCOM_ON		,	INTERCOM_OFF		,	1	},	//户内通话
	{NAMELIST_CALL	,	ST_CALL_ST_ON	,	ST_CALL_ST_OFF		,	2	},	//户户通话
	{MONITOR		,	ST_MON_ON		,	ST_MON_OFF			,	1	},	//分机监视
	{MEM_PLAY_BACK	,	ST_MON_ON		,	ST_MON_OFF			,	1	},	//分机查看留影
    {INNER_BRD      ,   INTERCOM_ON     ,   INTERCOM_OFF        ,   1   },  //户内广播
    {INTERCOM_BRD   ,   ST_CALL_ST_ON   ,   ST_CALL_ST_OFF      ,   2   },  //户户广播
};
const uint8 SrType_SrComd_NUMBERS = sizeof( TABLE_SrType_SrComd ) / sizeof( TABLE_SrType_SrComd[0] );

/*-----------------------------------------------
	根据呼叫类型,获取SR功能类型
入口:  
	call_type = 呼叫类型

出口:
	sr_type(SR功能类型)
-----------------------------------------------*/
uint8 Take_SR_Type(uint8 call_type)	//read
{
	uint8 search_count;
		
	//根据呼叫类型,找出匹配类型在表中的位置
	for	(search_count = 0; search_count < CallType_SrType_NUMBERS; search_count++)
	{
		if	(TABLE_CallType_SrType[search_count].call_type == call_type)
		{
			break;
		}
	}

	if	(search_count < CallType_SrType_NUMBERS)
	{
		return (TABLE_CallType_SrType[search_count].sr_type);	//返回该呼叫类型的SR功能类型
	}
	else	
	{
		return (0);		//无相应的呼叫类型,返回0
	}
}

/*-----------------------------------------------
	根据SR功能类型,返回相应的链路创建指令 或 释放指令 或 优先级别
入口:  
	sr_type = SR功能类型
	flag_creat_close:
		= 0 = 返回创建指令
		= 1 = 返回释放指令
		= 2 = 返回优先级别 

出口:
	与sr_type适配的链路创建指令或释放指令
-----------------------------------------------*/
uint8 Take_Sr_Comd(uint8 sr_type,uint8 get_type)	//read
{
	uint8 search_count;
		
	//根据SR功能类型,找出匹配类型在表中的位置
	for	(search_count = 0; search_count < SrType_SrComd_NUMBERS; search_count++)
	{
		if	(TABLE_SrType_SrComd[search_count].sr_type == sr_type)
		{
			break;
		}
	}

	if	(search_count < SrType_SrComd_NUMBERS)
	{
		if	(get_type == TAKE_CLOSE_COMD)	//释放指令
		{
			return (TABLE_SrType_SrComd[search_count].SR_close_command);	//返回该呼叫类型的释放链路指令
		}
		else if (get_type == TAKE_CREATE_COMD)	//创建指令
		{
			return (TABLE_SrType_SrComd[search_count].SR_creat_command);	//返回该呼叫类型的创建链路指令
		}			
		else if (get_type == TAKE_LEVEL)	//优先级别
		{
			return (TABLE_SrType_SrComd[search_count].level);	//返回该呼叫类型的创建链路指令
		}			
		else
		{
			return (0);		//无相应类型,返回0
		}	
	}
	else	
	{
		return (0);		//无相应的功能类型,返回0
	}
}

/*-----------------------------------------------
	根据创建链路的指令,得到SR功能类型
入口:  
	creat_comd(创建链路的指令类型)

出口:
	SR功能类型
-----------------------------------------------*/
uint8 API_Take_SrType_FromComd(uint8 creat_comd)	//read
{
	uint8 search_count;
		
	//根据SR功能类型,找出匹配类型在表中的位置
	for	(search_count = 0; search_count < SrType_SrComd_NUMBERS; search_count++)
	{
		if	(TABLE_SrType_SrComd[search_count].SR_creat_command == creat_comd)
		{
			break;
		}
	}

	if	(search_count < SrType_SrComd_NUMBERS)
	{
		return (TABLE_SrType_SrComd[search_count].sr_type);	//返回该呼叫类型的释放链路指令
	}
	else	
	{
		return (0);		//无相应的呼叫类型,返回0
	}
}



/*------------------------------------------------------------------------
							链路创建
入口:
	无

返回:
	0 = 创建成功
	1 = 失败失败
------------------------------------------------------------------------*/
extern L2_ADDR_TYPE myAddr;

uint8 SR_Routing_Create(uint8 call_type)	//read
{
	uint8 create_result;
	uint8 sr_type;
	pthread_mutex_lock(&SR_Lock);
	sr_type = Take_SR_Type(call_type);	//根据呼叫类型,获取SR功能类型
	create_result = API_Stack_BRD_With_ACK(Take_Sr_Comd(sr_type, TAKE_CREATE_COMD));	
	SR_Request=0;
	if (create_result) {	//创建成功
		SR_SetState_Locate(sr_type ,myAddr);								//状态维护: 链路创建
		SR_Routing.control = TRUE;											//链路控制者
		pthread_mutex_unlock(&SR_Lock);
		return (0);
	}
	else	//创建失败
	{
		pthread_mutex_lock(&SR_Lock);
		return (1);
	}
}

/*------------------------------------------------------------------------
							链路释放
入口:
	无

返回:
	0 = 释放成功
	1 = 释放失败
------------------------------------------------------------------------*/
uint8 SR_Routing_Close(uint8 call_type)	//read
{
	uint8 create_result;
	uint8 sr_type;
	pthread_mutex_lock(&SR_Lock);	
	sr_type = Take_SR_Type(call_type);	//根据呼叫类型,获取SR功能类型
	create_result = API_Stack_BRD_With_ACK(Take_Sr_Comd(sr_type, TAKE_CLOSE_COMD));	

	if (create_result) {	//释放成功
		SR_ClearState_DeLocate();	//状态维护: 链路释放
		pthread_mutex_unlock(&SR_Lock);
		return (0);
	}
	else	//释放失败
	{
		SR_ClearState_DeLocate();	//状态维护: 链路释放	//read_change
		pthread_mutex_unlock(&SR_Lock);
		return (1);
	}
}

/*------------------------------------------------------------------------
						链路剥夺(强制释放)
入口:
	Message_SR = 消息指针

返回:
	0 = 成功
	1 = 失败
------------------------------------------------------------------------*/
uint8 SR_Routing_Deprived(void)	//read
{
	uint8 create_result=0;
	pthread_mutex_lock(&SR_Lock);	
	if(SR_Request)
	{
		pthread_mutex_unlock(&SR_Lock);	
		return 1;
	}
	SR_Request=1;
	{
		create_result = API_Stack_BRD_With_ACK(STATE_RESET);
		
		if(OS_GetTaskID()==NULL)
			usleep(500*1000);
	}
	if(SR_State.in_use)
	{
		if(SR_State.function==MONITOR&&SR_State.manager_IA==myAddr)
		{
			//ClearMonSrRequest();
			//ClearPlaybackSrRequest();
			API_vbu_ClearMonSr();
		}

		if(SR_State.function==MEM_PLAY_BACK&&SR_State.manager_IA==myAddr)
		{
			
		}
		usleep(800*1000);
	}
	SR_Request=0;
	if (create_result) {	//释放成功
		SR_ClearState_DeLocate();	//状态维护: 链路释放
		pthread_mutex_unlock(&SR_Lock);
		return (0);
	}
	else	//释放失败
	{
		SR_ClearState_DeLocate();	//状态维护: 链路释放	
		pthread_mutex_unlock(&SR_Lock);
		return (1);
	}
}
int Get_SR_Request(void)
{
	return SR_Request;
}
/*------------------------------------------------------------------------
						通话占用
入口:
	无

返回:
	0 = 成功
	1 = 失败
------------------------------------------------------------------------*/
uint8 SR_Routing_Talking(void)	//read
{
	uint8 create_result;
	pthread_mutex_lock(&SR_Lock);	
	create_result = API_Stack_BRD_With_ACK(MR_CALL_ST_TALK);

	if (create_result) {	//释放成功
		SR_SetState_Talking();	//状态维护: 通话占用
		pthread_mutex_unlock(&SR_Lock);
		return (0);
	}
	else	//释放失败
	{
		pthread_mutex_unlock(&SR_Lock);
		return (1);
	}
}

////////////////////////////////////////////////////////////////////////////////

SR_STATE_C 		SR_State;

/*------------------------------------------------------------------------
						链路使用申请
入口:  
call_type = 呼叫类型

返回:
SR_ENABLE_DIRECT	0		//可直接使用
SR_ENABLE_DEPRIVED	1		//可被剥夺使用
SR_DISABLE			2		//不可使用(正在使用,且使用者优先级别高)
------------------------------------------------------------------------*/
uint8 API_SR_Request(uint8 call_type)	//read
{
	uint8 use_level;
	uint8 apply_level;	

	pthread_mutex_lock(&SR_Lock);
	//判断SR是否空闲
	if	(SR_State.in_use == FALSE)	
	{
		SR_Request=1;
		pthread_mutex_unlock(&SR_Lock);
		return (SR_ENABLE_DIRECT);		//SR空闲,可直接使用
	}
	
	use_level = Take_Sr_Comd(SR_State.function, TAKE_LEVEL);			//取得当前正在使用的优先级
	apply_level = Take_Sr_Comd(Take_SR_Type(call_type), TAKE_LEVEL);	//取得申请的优先级
	
	//比较优先级
	if	(use_level >= apply_level)		
	{
		SR_Request=0;
		pthread_mutex_unlock(&SR_Lock);
		return (SR_DISABLE);			//不可使用(正在使用,且使用者优先级别高)
	}
	else	
	{
		SR_Request=0;
		pthread_mutex_unlock(&SR_Lock);
		return (SR_ENABLE_DEPRIVED);	//可被剥夺使用(正在使用,但使用者优先级别低)
	}

}

/*------------------------------------------------------------------------
						链路状态维护_链路占用
入口:  
call_type = 呼叫类型
SR_mastery_IA = 链路创建者(管理者)
------------------------------------------------------------------------*/
void SR_SetState_Locate(uint8 sr_type,uint16 SR_mastery_IA)	//read
{
	char link_type[20];
	SR_State.in_use = TRUE;					//链路占用
	if(sr_type==CALLING)
	{
		if(SR_State.manager_IA==myAddr)
		{
			strcpy(link_type,"Call");
		}
		else
			strcpy(link_type,"Intercom");
	}
	else if(sr_type==MONITOR)
	{
		if(Get_Playback_State())
			strcpy(link_type,"Playback");
		else
			strcpy(link_type,"Mon");
	}
	else
	{
		strcpy(link_type,"Unkown");
	}
		
	SetDtLinkState(SR_State.in_use,link_type);
	SR_State.manager_IA = SR_mastery_IA;	//链路管理者IA
	SR_State.function = sr_type;			//功能类型 或 功能优先级编码
//zxj	SR_State.on_time = osal_GetSystemClock();//链路占用定时,仅系统防卫者使用
	
	clear_sr_timer = 0;
	OS_RetriggerTimer(&timer_clear_sr);
	#if 0
	uint8 slaveMaster;
	API_Event_IoServer_InnerRead_All(MASTER_SLAVE_SET, (uint8*)&slaveMaster);
	if(slaveMaster == 0)
	{
		SendCmd_LinkState_Broadcast(SR_State.in_use,SR_State.manager_IA,SR_State.function,SR_State.talking,0);
	}
	#endif
	//linphone_becalled_dtlink_create();	//czn_20170922

}
void API_SR_SetState_Locate(uint8 sr_type,uint16 SR_mastery_IA) 
{
	pthread_mutex_lock(&SR_Lock);
	SR_SetState_Locate(sr_type,SR_mastery_IA);
	pthread_mutex_unlock(&SR_Lock);
}
/*------------------------------------------------------------------------
						链路状态维护_链路释放
------------------------------------------------------------------------*/
void SR_ClearState_DeLocate(void)	//read
{
	if(SR_State.in_use==TRUE)
	{
		SR_State.in_use = FALSE;		//链路释放
		SetDtLinkState(SR_State.in_use,NULL);	
		SR_State.talking = FALSE;		//通话释放

		SR_Routing.control = FALSE;		//非链路控制者
		clear_sr_timer=0;
		OS_StopTimer(&timer_clear_sr);	
	}
	#if 0
	uint8 slaveMaster;
	API_Event_IoServer_InnerRead_All(MASTER_SLAVE_SET, (uint8*)&slaveMaster);
	if(slaveMaster == 0)
	{
		SendCmd_LinkState_Broadcast(SR_State.in_use,SR_State.manager_IA,SR_State.function,SR_State.talking,0);
	}
	#endif
}
void API_SR_ClearState_DeLocate(void)
{
	pthread_mutex_lock(&SR_Lock);
	SR_ClearState_DeLocate();
	pthread_mutex_unlock(&SR_Lock);
}
/*------------------------------------------------------------------------
						链路状态维护_通话占用
------------------------------------------------------------------------*/
void SR_SetState_Talking(void)	//read
{
	SR_State.talking = TRUE;		//通话占用
}


/*------------------------------------------------------------------------
						链路状态维护_通话释放
------------------------------------------------------------------------*/
void SR_ClearState_Talking(void)	//read
{
	SR_State.talking = FALSE;		//通话释放
}


void Overtime_Clear_SR(void)
{
	pthread_mutex_lock(&SR_Lock);
	if (SR_State.in_use == TRUE)
	{	
		clear_sr_timer++;	//????
		SR_Request=0;
		if (clear_sr_timer >= 600)
		{
			//API_Survey_MsgInform(INFORM_SR_OVERTIME);	//??INFORM?Survey,?????Survey??
			SR_ClearState_DeLocate();
			OS_StopTimer(&timer_clear_sr);	
		}
		else
		{
			OS_RetriggerTimer(&timer_clear_sr);
		}
	}
	else
	{
	#if 0
		if (clear_sr_timer++ >= 5&&Get_Playback_State()==0&&Get_QuadMonState()==0)
		{
			API_Stack_BRD_With_ACK(STATE_RESET);
			OS_StopTimer(&timer_clear_sr);	
		}
		else
		{
			OS_RetriggerTimer(&timer_clear_sr);
		}	
	#endif
	}
	pthread_mutex_unlock(&SR_Lock);
}

// 链路任务始化(其实非任务)
void vtk_ObjectInit_SR(void)	//read
{
	//状态初始化
	SR_State.in_use = FALSE;		//链路空闲
	SetDtLinkState(SR_State.in_use,NULL);
	SR_State.manager_IA = FALSE;	//链路管理者IA
	SR_State.function = FALSE;		//功能类型 或 功能优先级编码
	SR_State.talking = FALSE;		//通话占用标志
//read_change	SR_State.on_time = FALSE;		//链路占用定时,仅系统防卫者使用

	SR_Routing.stage = SR_ROUTING_STATE_IDLE;		//链路无动作
	SR_Routing.control = FALSE;						//非链路控制者

	OS_CreateTimer(&timer_clear_sr, Overtime_Clear_SR, 1000/25);
}			

#endif

/*********************************************************************************************************
**  End Of File
*********************************************************************************************************/
