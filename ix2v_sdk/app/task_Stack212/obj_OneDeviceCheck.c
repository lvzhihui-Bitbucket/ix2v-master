/**
  ******************************************************************************
  * @file    obj_OneDeviceCheck.c
  * @author  cao
  * @version V00.01.00
  * @date    2016.06.01
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2012 V-Tec</center></h2>
  ******************************************************************************
  */ 

#include "obj_OneDeviceCheck.h"
#include "define_Command.h"
#include "task_StackAI.h"
//#include "task_IoServer.h"

uint8 waitForResponseEventFlag;
vdp_task_t *waitingForResponseTaskTCB;
#if 0
/*------------------------------------------------------------------------
			根据设备类型及逻辑地址, 获得其物理地址
入口:  
		设备类型及逻辑地址
处理:

返回:
		0 没找到，1 找到
------------------------------------------------------------------------*/
uint8  GetPhysicalByLogica(uint16 logicAddr, uint16* physicAddr, uint8* instruction)
{
	uint8  imMode = IM_MODE_NORMAL;
	uint8	ret = 0;
	
	//分机	RT = 0～9
	if(0 < logicAddr && logicAddr < 256)
	{
		*instruction = INSTRUCTION_TYPE_MR_LINKING_ST;
		switch(imMode)
		{
			case IM_MODE_NORMAL:
				if(logicAddr == 32)
				{
					*physicAddr = 0x80;
					ret = 1;
				}
				else if(logicAddr <= 31)
				{
					*physicAddr = 0x80 + logicAddr * 4;
					ret = 1;
				}
				break;
			case IM_MODE_DJ:
			case IM_MODE_ROUTER:
				break;
		}
	}
	//主机	RT = 10
	else if(320 <= logicAddr && logicAddr <= 323)
	{
		*instruction = INSTRUCTION_TYPE_ST_LINKING_MR;
		*physicAddr = 0x34 + logicAddr - 320;
		ret = 1;
	}
	//单元中心	RT = 11
	else if(352 <= logicAddr && logicAddr <= 355)
	{
		*instruction = INSTRUCTION_TYPE_MR_LINKING_ST;
		*physicAddr = 0x3C + logicAddr - 352;
		ret = 1;
	}
	//小区主机	RT = 12
	else if(384 <= logicAddr && logicAddr <= 387)
	{
		*instruction = INSTRUCTION_TYPE_ST_LINKING_MR;
		*physicAddr = 0x14 + logicAddr - 384;
		ret = 1;
	}
	//BDU_GATEWAY	RT = 13
	//DT-BUS(RT=14)
	//DT-IPC(RT=15)
	//IP管理中心(RT=16)
	//DT-SC6(RT=17)
	else if(384 <= logicAddr && logicAddr < 576)
	{
		return ret;
	}
	//DT-RLC(RT=18)
	else if(576 <= logicAddr && logicAddr <= 593)
	{
		*instruction = INSTRUCTION_TYPE_IO_SERVER;
		*physicAddr = 0x60 + logicAddr - 576;
		ret = 1;
	}	
	//DT-SCU(RT=19)
	else if(608 <= logicAddr && logicAddr <= 611)
	{
		*instruction = INSTRUCTION_TYPE_IO_SERVER;
		*physicAddr = 0x68 + logicAddr - 608;
		ret = 1;
	}	
	//DT-QSW(RT=20)
	else if(640 <= logicAddr && logicAddr <= 643)
	{
		*instruction = INSTRUCTION_TYPE_IO_SERVER;
		*physicAddr = 0x6C + logicAddr - 640;
		ret = 1;
	}	

	//BDU_Router(RT=22)
	else if(704 <= logicAddr && logicAddr <= 711)
	{
		*instruction = INSTRUCTION_TYPE_IO_SERVER;
		*physicAddr = 0x74 + logicAddr - 704;
		ret = 1;
	}	
	//DT-ACC(RT=21)
	else
	{
		;
	}
	
	return ret;
}

/*------------------------------------------------------------------------
					单个设备通用检测
入口:  
		设备类型和逻辑地址
处理:

返回:
		0 不在线， 1 在线， 2 不支持检测。
------------------------------------------------------------------------*/
uint8 DeviceLinking_Standard_Processing(uint16 logicAddr)
{
	uint16	physicAddr;
	uint8	instruction;
	uint8	tempPhysical[2];
	
	if(GetPhysicalByLogica(logicAddr, &physicAddr, &instruction))
	{
		#if 0
		if(AI_AppointFastLinkWithEventAck(physicAddr))
		{
			return 1;
		}
		else
		#endif
		{
			switch(instruction)
			{
				case INSTRUCTION_TYPE_MR_LINKING_ST:
					return InstructionCheck(physicAddr, MR_LINKING_ST);

				case INSTRUCTION_TYPE_ST_LINKING_MR:
					return InstructionCheck(physicAddr, ST_LINK_MR);
				#if 0	
				case INSTRUCTION_TYPE_IO_SERVER:
					
					if(API_IoServer_Device_Check(physicAddr, DeviceAdd, 2, 0, tempPhysical))
					{
						return 0;
					}
					else
					{
						return 1;
					}
				#endif
				default:
					return 2;
			}
		}
	}
	else
	{
		return 2;
	}
}
#endif
/*------------------------------------------------------------------------
					（旧指令）检测
入口:  
		物理地址, 指令
处理:

返回:
		
------------------------------------------------------------------------*/
#if 1
uint8 InstructionCheck(uint16 physical, uint8 cmd)
{
	OS_TASK_EVENT 	MyEvents;
	int cnt=0;
	while(OS_WaitSingleEventTimed(0xff, 1)>0&&cnt++<100)
	{

		;
	}
	waitForResponseEventFlag = 1;
	waitingForResponseTaskTCB = OS_GetTaskID();

	API_Stack_APT_Without_ACK(physical, cmd);
	
	//分机（DT16S）至少等600ms，主机（DT606）至少等300ms,QSW至少580ms，SCU（600ms）,小区主机（DMR18S）300ms
	MyEvents = OS_WaitSingleEventTimed(RESPONSE_OK|RESPONSE_ERR,  1200);	
	waitForResponseEventFlag = 0;

	if (MyEvents & RESPONSE_OK) 
	{
		return 1;
	}
	else
	{
		return 0;
	}
}
/*------------------------------------------------------------------------
					（旧指令）检测结果回应
入口:  
		无
处理:

返回:
		
------------------------------------------------------------------------*/
void InstructionCheckResponse(void)
{
	if (waitForResponseEventFlag)
	{
		OS_SignalEvent(RESPONSE_OK, waitingForResponseTaskTCB);
	}
}

#endif
/*********************************************************************************************************
**  End Of File
*********************************************************************************************************/
