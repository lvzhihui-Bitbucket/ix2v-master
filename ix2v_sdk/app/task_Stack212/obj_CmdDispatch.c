
/**
  ******************************************************************************
  * @file    obj_CmdDispatch.c
  * @author  lv
  * @version V1.0.0
  * @date    2012.06.20
  * @brief   This file contains the functions of obj_CmdDispatch.c
  ******************************************************************************
  */ 

#include "stack212.h"
//#include "task_IoServer.h"
//#include "obj_BeCalled_data.h" // 20140819
#include "elog_forcall.h"
#include "task_CallServer.h"
#include "obj_UnitSR.h"
#include "obj_APCIService.h"
#include "obj_IoInterface.h"
#include "task_Event.h"

extern MAC_TYPE	l2MacType;

//应用层等待应答处理数据类型
WAIT_RESPONSE WaitResponse;

//收到协议栈指令后需要给出sub_receipt应答的指令表
const unsigned char SEND_SUB_RECEIPT_CMD_TAB[] =		//will add
{
	MAIN_CALL,
	MAIN_CALL_ONCE_L,
	MAIN_CALL_TEST,
	LET_DIDONG_ONCE,
	LET_WORK,
	LET_TALKING,
};

//收到协议栈指令后需要给出receipt应答的指令表
const unsigned char SEND_RECEIPT_CMD_TAB[] =			//will add
{
	//INTERCOM_CALL,	//czn_20170601
	ST_TALK,			//receipt
	ST_UNLOCK,			//receipt
	ST_CLOSE,			//receipt
	ST_LINK_MR,			//receipt	
	MON_ON,				//receipt
	MON_OFF,			//receipt
	MON_ON_APPOINT,		//receipt
	MON_OFF_APPOINT,	//receipt
	ST_UNLOCK_SECOND,	//receipt
	
	INTERCOM_CALL,		//receipt
};

//转发命令后需要等待receipt或是sub_receipt应答的指令表
const unsigned char WAIT_RECEIPT_CMD_TAB[] =			//will add
{
	ST_TALK,			//receipt
	ST_UNLOCK,			//receipt
	ST_CLOSE,			//receipt
	ST_LINK_MR,			//receipt	
	MON_ON,				//receipt
	MON_OFF,			//receipt
	MON_ON_APPOINT,		//receipt
	MON_OFF_APPOINT,	//receipt
	ST_UNLOCK_SECOND,	//receipt
	
	INTERCOM_CALL,		//receipt
	//wait_sub_receipt
	MAIN_CALL,
	MAIN_CALL_ONCE_L,
	MAIN_CALL_TEST,
	LET_DIDONG_ONCE,
	LET_WORK,
	LET_TALKING,
	MR_LINKING_ST,
};

void API_Survey_MsgOldCommand(uint16 source_address , uint16 target_address , uint8 cmd, uint8 data_length, uint8* data);
/*******************************************************************************************
 * @fn：	WhichReceiptShouldBeSend
 *
 * @brief:	收到协议栈指令后，解析是否需要给出应答：
 *
 * @param:  cmd - 	检测该命令是否需要给出Receipt 或 Receipt_Single 应答
 *
 * @return: 0/无需应答，1/给出receipt应答，2/给出sub_receipt应答
 *******************************************************************************************/
unsigned char WhichReceiptShouldBeGiven(unsigned char cmd)
{	
	unsigned char i;
	//搜索该命令是否需要给出receipt应答
	for( i = 0; i < sizeof(SEND_RECEIPT_CMD_TAB); i++ )
	{
		if( SEND_RECEIPT_CMD_TAB[i] == cmd )
		{
			return 1;
		}
	}
	//搜索该命令是否需要给出sub_receipt应答
	for( i = 0; i < sizeof(SEND_SUB_RECEIPT_CMD_TAB); i++ )
	{
		if( SEND_SUB_RECEIPT_CMD_TAB[i] == cmd )
		{
			return 2;
		}
	}
	return 0;
}

/*******************************************************************************************
 * @fn：	SetWaitResponse
 *
 * @brief:	转发指令后，启动应答机制：
 *			1）ptrTCB：	发送Event到WaitResponse.ptrTCBResponse所指向的任务
 *			2）cmd：	检测该命令是否需要等待Receipt 或 Receipt_Single 应答
 *
 * @param:  ptrTCB - 需要回复Event的任务ID
 * @param:  cmd - 	检测命令是否需要Receipt 或 Receipt_Single 应答
 * @param:  brd - 	重发的命令为广播命令还是定点命令（0：定点，1：广播）
 *
 * @return: none
 *******************************************************************************************/
void SetWaitResponse(vdp_task_t *ptrTCB,unsigned char cmd,unsigned char brd)
{	
	//需要回复Event ACK的task ID
	WaitResponse.ptrTCBResponse = ptrTCB;
	
	//检测需要回复receipt ACK的命令
	unsigned char i;
	for( i = 0; i < sizeof(WAIT_RECEIPT_CMD_TAB); i++ )
	{
		if( WAIT_RECEIPT_CMD_TAB[i] == cmd )
			break;		
	}
	
	//匹配成功后初始化重发次数
	if( i != sizeof(WAIT_RECEIPT_CMD_TAB) )
	{
		WaitResponse.reEnable = 1;
		WaitResponse.reSendCnt = 2;			//lzh_20130410
		WaitResponse.reBroadcast = brd;
	}
}

/*******************************************************************************************
 * @fn：	ResetWaitResponse
 *
 * @brief:	复位重发机制
 *
 * @param:  none
 *
 * @return: none
 *******************************************************************************************/
void ResetWaitResponse(void)
{	
	WaitResponse.reEnable = 0;
	WaitResponse.reSendCnt = 0;	
	WaitResponse.reBroadcast = 0;
}

/*******************************************************************************************
 * @fn：	TrsResultResponseCallback
 *
 * @brief:	针对发送结果，给相应TCB的回复
 *
 * @param:  rspOK - 0/err, 1/OK
 *
 * @return: none
 *******************************************************************************************/
void TrsResultResponseCallback(unsigned char rspOK)
{	
	// 1. 发送结果后发送Event ACK
	if( WaitResponse.ptrTCBResponse )
	{
		if( rspOK )
			OS_SignalEvent(OS_TASK_EVENT_RESPONSE_OK,WaitResponse.ptrTCBResponse);
		else
			OS_SignalEvent(OS_TASK_EVENT_RESPONSE_ER,WaitResponse.ptrTCBResponse);
		
		WaitResponse.ptrTCBResponse = 0;
	}
	
	// 2. 等待Receipt or Receipt_Single，1秒超时后重发数据
	if( WaitResponse.reEnable && WaitResponse.reSendCnt )
	{
		OS_TASK_EVENT MyEvents;
		
		MyEvents = OS_WaitSingleEventTimed(OS_TASK_EVENT_STACK_RCV_QUEUE,1000);	//czn_20170816++++
		//printf("3333333333333333ForTrsResult\n");
		// 收到数据包解析，若收到有Receipt应答后复位
		if( MyEvents&OS_TASK_EVENT_STACK_RCV_QUEUE )
		{
			RcvQueueProcess();
		}
		//是否重发数据包
		if( WaitResponse.reSendCnt-- )
		{
			// 调用重发命令为广播还是定点
			unsigned char result;
			if( WaitResponse.reBroadcast )
			{
				result = L2_BoardcastWrite();
			}
			else
			{
				//lzh_20130425
				result = L2_DataWrite(l2MacType,F_L2_SEND_NEW_CMD);
			}			
			// 重发命令的结果：发送错误，停止重发			
			if( !result )
			{
				ResetWaitResponse();
			}			
			else
			{
				aiStatus = AI_STAT_WRITE;
			}
		}
	}
}

/*******************************************************************************************
 * @fn:		StackOldCommandInputProcess
 *
 * @brief:	处理协议栈传上来的旧指令
 *
 * @param:  ptrMsgDat - 消息数据指针
 *
 * @return: 0/失败，1/OK
 *******************************************************************************************/
unsigned char ttetemp1;
unsigned char ttetemp2;
//#include "../task_io_server/vdp_IoServer_Data.h"
//#include "task_IoServer.h"
//#include "task_DXMonitor.h"
unsigned char StackOldCommandInputProcess(void *ptrMsgDat)
{	
	AI_DATA *ptrCMdIo = (AI_DATA *)ptrMsgDat;
	uint8 tranferset;
	char buff[5];
	log_d("dt stack recv:%02x%02x%02x",ptrCMdIo->head.sa,ptrCMdIo->head.da,ptrCMdIo->head.cmd);
	switch( ptrCMdIo->head.cmd )
	{
		// 主机的联机指令，直接处理
		case MR_LINKING_ST:
			if( myAddr == ptrCMdIo->head.da )
			{
				//zfz 20140819
				#if 0
				if((tranferset = Get_NoDisturbSetting()) == 0)	//czn_20170805
				{
					API_Event_IoServer_InnerRead_All(CallScene_SET, buff);
					tranferset = atoi(buff);
				}
				if(tranferset== 1 || tranferset == 2)
				//if( !GetDoNotDisturbState() ) 	//无启动免扰功能
				{
					break;
				}
				#endif
				//if( !GetDoNotDisturbState() ) 	//无启动免扰功能
				{
					SendOneOldCommand(2,ptrCMdIo->head.sa,SUB_MODEL,0,0);
					//API_Event_Maincall_Close();
				}
			}
			break;
		case SUB_RECEIPT:
		case SUB_MODEL:	
		// 收到主机的回复命令后，停止重发机制，并且发送到主机
		case RECEIPT_SINGLE:
			//ds_mode =  DS_SINGLE;	//单户型主机呼叫
		case RECEIPT:		
			if( myAddr == ptrCMdIo->head.da )
			{
				InstructionCheckResponse();	//czn_20170717
				//复位重发次数机制
				ResetWaitResponse();
				//API_Survey_MsgOldCommand_4byte(ptrCMdIo->head.sa,ptrCMdIo->head.cmd);
				API_Survey_MsgOldCommand(ptrCMdIo->head.sa,ptrCMdIo->head.da, ptrCMdIo->head.cmd,0,NULL);
			}
			break;
			
		// 启动Namelist更新
		case NAMELIST_ON:
			//API_NamelistUpdate_Start();
			break;
			
		// Namelist更新结束
		case NAMELIST_OFF:
			//API_NamelistUpdate_End();
			break;
			
		// Namelist更新数据
		case NAMELIST_BROAD:

		
//lzh_20121203			if( ptrCMdIo->head.length.bit.len > 5 )
			if( (ptrCMdIo->head.length.bit.newcmd && ptrCMdIo->head.length.bit.len > 5) || (!ptrCMdIo->head.length.bit.newcmd && ptrCMdIo->head.length.byte > 5) )	//lzh_20121203
				
				
			{
				//externd data: [0]:Package Length; [1]:NameList Number; [2...]NameList Data
				//API_NameListUpdate_List( ptrCMdIo->data[1]-0x30, ptrCMdIo->data[0]-5, ptrCMdIo->data+1 );
			}
			break;
		default:
			//MAIN_CALL 命令过滤：呼叫目标地址的扩展地址必须小于本机的扩展地址
			if( (ptrCMdIo->head.cmd == MAIN_CALL ) && ( (ptrCMdIo->head.da&0x03) > (myAddr&0x03)) )
				break;

			//INTERCOM_HJ_CALL 命令过滤：
			// 1. 呼叫地址为0x3c,且本机地址非0x3c
			// 2. 呼叫目标非0x3c, 呼叫地址的主地址必须不同于本机的主地址
			if( (ptrCMdIo->head.cmd == INTERCOM_CALL ) )
			{
				if( ( ( ptrCMdIo->head.da==0x3c ) && (myAddr != 0x3c) ) 
				   	|| ( ( ptrCMdIo->head.da!=0x3c ) && (ptrCMdIo->head.sa&0xfc)==(myAddr&0xfc) )	//lzh_20121120
				  )
				{
					ttetemp1 = ptrCMdIo->head.sa;
					ttetemp2 = ptrCMdIo->head.da;
					break;
				}
                
                //zfz 20140819
//                if( GetDoNotDisturbState() || GetIntercomDisableFlag() ) 	//启动免扰功能
//                {
//                    break;
//                }
			}

			//MAIN_CALL_ONCE_L 单户系统主呼叫命令过滤：目标地址的不能为0x80且不能为0x80
			if( (ptrCMdIo->head.cmd == MAIN_CALL_ONCE_L ) && ( (myAddr!=0x80)&&(myAddr!=0xc0)) )
			{
				break;
			}
			
			//MAIN_CALL_GROUP 单户系统群呼叫命令过滤：目标地址的为0x80或是0xC0
			if( (ptrCMdIo->head.cmd == MAIN_CALL_GROUP ) && ( (myAddr==0x80)||(myAddr==0xc0)) )
				{
					break;
				}
				
			//S_CANCEL 命令过滤：呼叫目标地址等于本机的地址
			if( (ptrCMdIo->head.cmd == S_CANCEL ) && ( (ptrCMdIo->head.da) != (myAddr)) )
				break;			

			//ASK_VOLTAGE 命令过滤：呼叫目标地址等于本机的地址
			if( (ptrCMdIo->head.cmd == ASK_VOLTAGE ) && ( (ptrCMdIo->head.da) != (myAddr)) )
				break;

			//MAIN_CALL_TEST 命令过滤：呼叫目标地址等于本机的地址
			if( (ptrCMdIo->head.cmd == MAIN_CALL_TEST ) && ( (ptrCMdIo->head.da) != (myAddr)) )
				break;

			//需要给出相应的应答
			if( WhichReceiptShouldBeGiven(ptrCMdIo->head.cmd) == 1 )
			{
				if( myAddr == ptrCMdIo->head.da )
				{
					SendOneOldCommand(0,ptrCMdIo->head.sa,RECEIPT,0,0);
				}
			}
			else if( WhichReceiptShouldBeGiven(ptrCMdIo->head.cmd) == 2 )
			{
				if( myAddr == ptrCMdIo->head.da )
				{				
					SendOneOldCommand(0,ptrCMdIo->head.sa,SUB_RECEIPT,0,0);					
				}
			}
			//命令上传
            // 20131021
			//API_Survey_MsgOldCommand_4byte(ptrCMdIo->head.sa,ptrCMdIo->head.cmd);
            if( ptrCMdIo->head.length.byte )	//czn_20170518
			{
				API_Survey_MsgOldCommand(ptrCMdIo->head.sa,ptrCMdIo->head.da,ptrCMdIo->head.cmd,ptrCMdIo->head.length.byte-1,ptrCMdIo->data+1);	//增加目标地址
			}				
			else
			{
				API_Survey_MsgOldCommand(ptrCMdIo->head.sa,ptrCMdIo->head.da, ptrCMdIo->head.cmd,0,NULL);	//增加目标地址
			}            
			break;
	}

	return TRUE;
}

/*******************************************************************************************
 * @fn:		StackOldCommandOutputProcess
 *
 * @brief:	处理task需要转发的旧指令
 *
 * @param:  ptrMsgDat - 消息数据指针
 * @param:  brd - 0:定点命令，1:广播命令
 *
 * @return: 0/失败，1/OK
 *******************************************************************************************/
unsigned char StackOldCommandOutputProcess(void *ptrMsgDat,unsigned char brd)
{
	StackFrameQ_TaskIO *ptrCmdOutputForSend;
	
	ptrCmdOutputForSend = (StackFrameQ_TaskIO*)ptrMsgDat;

	return( SendOneOldCommand(brd,ptrCmdOutputForSend->addr,ptrCmdOutputForSend->cmd,ptrCmdOutputForSend->dat,ptrCmdOutputForSend->len) );
}

void API_Survey_MsgOldCommand(uint16 source_address , uint16 target_address , uint8 cmd, uint8 data_length, uint8* data)
{
	DTSTACK_MSG_TYPE	rec_msg_from_stack;
	
	rec_msg_from_stack.head.msg_source_id	= MSG_ID_StackAI;
	rec_msg_from_stack.head.msg_target_id	= MSG_ID_survey;
	rec_msg_from_stack.head.msg_type		= 0;
	rec_msg_from_stack.head.msg_sub_type	= 0;
	
	rec_msg_from_stack.address			= source_address;	//???
	rec_msg_from_stack.target_address		= target_address;	//????
	rec_msg_from_stack.command_mode		= MODE_OLD_COM;		//?????
	rec_msg_from_stack.command_type		= cmd;				//??
	if(data_length > 0 && data != NULL)
	{
		if(data_length <= DATA_MAX)
		{	
			rec_msg_from_stack.data_len	= data_length;		//????
		}
		else
		{
			rec_msg_from_stack.data_len	= DATA_MAX;			//????
		}
		memcpy(rec_msg_from_stack.data_buf,data,rec_msg_from_stack.data_len);
	}

	API_add_message_to_suvey_queue((char*)&rec_msg_from_stack, sizeof(DTSTACK_MSG_TYPE) ); 
}

void API_Survey_MsgNewCommand(uint16 source_address , uint16 target_address , uint8 cmd, uint8 data_length, uint8* data)
{
	DTSTACK_MSG_TYPE	rec_msg_from_stack;
	
	rec_msg_from_stack.head.msg_source_id	= MSG_ID_StackAI;
	rec_msg_from_stack.head.msg_target_id	= MSG_ID_survey;
	rec_msg_from_stack.head.msg_type		= 0;
	rec_msg_from_stack.head.msg_sub_type	= 0;
	
	rec_msg_from_stack.address			= source_address;	//???
	rec_msg_from_stack.target_address		= target_address;	//????
	rec_msg_from_stack.command_mode		= MODE_NEW_COM;		//?????
	rec_msg_from_stack.command_type		= cmd;				//??
	if(data_length > 0 && data != NULL)
	{
		if(data_length <= DATA_MAX)
		{	
			rec_msg_from_stack.data_len	= data_length;		//????
		}
		else
		{
			rec_msg_from_stack.data_len	= DATA_MAX;			//????
		}
		memcpy(rec_msg_from_stack.data_buf,data,rec_msg_from_stack.data_len);
	}

	API_add_message_to_suvey_queue((char*)&rec_msg_from_stack, sizeof(DTSTACK_MSG_TYPE) ); 
}

void DtCommand_Analyzer(DTSTACK_MSG_TYPE *Message_command)
{
	
	Global_Addr_Stru addr;
	uint8 tranferset;
	int i;
	char buff[5];
	cJSON *key_event;
	if(Message_command->command_mode==MODE_NEW_COM)
	{
		#if 0
		switch(Message_command->command_type)
		{
			case APCI_GROUP_VALUE_WRITE:
 		  	//API_GoServer_BeWrite(device_da,device_sa, ptrCMdIo->head.length.bit.len, &ptrCMdIo->data[0] );		//czn_20170719
				Go_write_process(Message_command->target_address,Message_command->address, Message_command->data_buf, Message_command->data_len);
				break;
		}
		#endif
	}
	else
	{
		switch(Message_command->command_type)
		{
			case MR_LINKING_ST:
				#if 0
				if((tranferset = Get_NoDisturbSetting()) == 0)	//czn_20170805
				{
					API_Event_IoServer_InnerRead_All(CallScene_SET, buff);
					tranferset = atoi(buff);
				}
				if(tranferset== 1 || tranferset == 2)
				//if( !GetDoNotDisturbState() ) 	//无启动免扰功能
				{
					break;
				}
				#endif
				{
			 		API_Stack_APT_Without_ACK(Message_command->address ,SUB_MODEL);
				}
				break;
			case MAIN_CALL_ONCE_L:
			case MAIN_CALL_GROUP:
			case MAIN_CALL:
				//if(CallServer_Run.state == CallServer_Wait)
				{
					//addr.gatewayid = 0;
					//addr.ip = GetLocalIp();
					//addr.rt = 10;
					//addr.code = Message_command->address - DS1_ADDRESS;
					//API_CallServer_DXInvite(DxCallScene1_Master,Message_command->address);
				}
				break;
				
			case S_CANCEL:
				if(CallServer_Run.state != CallServer_Wait)	//will_change	
				{
					//addr.gatewayid = 0;
					//addr.ip = GetLocalIp();
					//addr.rt = 10;
					//addr.code = Message_command->address - DS1_ADDRESS;
					//API_CallServer_RemoteBye(DxCallScene1_Master,NULL);
				}		
				break;
			#if 1	
			case INTERCOM_CALL:
				#if 0
				//if(CallServer_Run.state == CallServer_Wait)
				
				API_Event_IoServer_InnerRead_All(IntercomEnable, buff);
				if(atoi(buff) == 1 || Message_command->address == 0x3c)
				{
					//addr = DtAddr_Trs2_IpGlobalAddr(Message_command->address);
					API_CallServer_DXInvite(DxCallScene2_Master,Message_command->address);
				}
				#endif
				break;
					
			case INTERCOM_CLOSE:
				if(CallServer_Run.state != CallServer_Wait)	//will_change	
				{
					//addr = DtAddr_Trs2_IpGlobalAddr(Message_command->address);
					//API_CallServer_RemoteBye(CallServer_Run.call_type,NULL);
				}			
				break;
			case ST_CLOSE:
				if(CallServer_Run.state != CallServer_Wait&& CallServer_Run.call_type == DxMainCall)	//will_change	
				{
					//addr = DtAddr_Trs2_IpGlobalAddr(Message_command->address);
					API_CallServer_RemoteBye();
				}
				if(CallServer_Run.state != CallServer_Wait&& CallServer_Run.call_type == IXGMainCall)	//will_change	
				{
					//addr = DtAddr_Trs2_IpGlobalAddr(Message_command->address);
					API_CallServer_LocalBye();
				}	
				break;
				
			case INTERCOM_TALK:
				#if 0
				if( (CallServer_Run.state == CallServer_Ring) &&  ((CallServer_Run.call_type == DxCallScene3)||(CallServer_Run.call_type == DxCallScene4_Master)) )
				{
					//addr = DtAddr_Trs2_IpGlobalAddr(Message_command->address);
					//API_CallServer_RemoteAck(CallServer_Run.call_type,NULL);
				}
				#endif
				break;
			
			#endif	
			case MR_CALL_ST_ON:		//主叫呼叫分机
				
			case ST_MON_ON:			//分机监视
					
			case DEVICE_CALL_GL_ON:
			case GL_CALL_DEVICE_ON:
			case ST_CALL_ST_ON:		//户户通话
			case INTERCOM_ON:		//户内通话
				//czn_20170328	if(IsCoverThisSr(Message_command) == 0)
				{
					//API_SR_SetState_Locate(API_Take_SrType_FromComd(Message_command->command_type), Message_command->address);		
				}
				API_SR_SetState_Locate(API_Take_SrType_FromComd(Message_command->command_type), Message_command->address);
				break;

			case MR_CALL_ST_OFF:		//主叫呼叫分机
			case ST_MON_OFF:			//分机监视
			case STATE_RESET:			//强制释放
			case ST_CALL_ST_OFF:		//户户通话
			case INTERCOM_OFF:			//户内通话
			case DEVICE_CALL_GL_OFF:	//read_change
			case GL_CALL_DEVICE_OFF: 	//read_change
			
				API_SR_ClearState_DeLocate();
				
				if(CallServer_Run.state != CallServer_Wait)
				{
					API_CallServer_DtSr_Disconnect();		//czn_20171030
					//addr = DtAddr_Trs2_IpGlobalAddr(Message_command->address);
					//API_CallServer_RemoteBye(CallServer_Run.call_type,&addr);
				}
				API_vbu_ClearMonSr();
				#if 0
				else if(Mon_Run.state != MON_STATE_PHONE_NEXT && Mon_Run.state != MON_STATE_OFF)//czn_20161226
				{
					API_Event_DXMonitor_Cancel();
				}
				#endif
				/*if (CallServer_Run.call_mode == IPGVTKCALL_CALLINGPLACE_WITHBJ)
				{
					API_Phone_VTKBjSrClear();
				}
				if(Business_Run.calling_place == IPGVTKCALL_CALLINGPLACE_WITHZJ)
				{
					API_Phone_VTKZjSrClear();
				}
			
				*/
				break;	
			case RECEIPT:	//说明: IPC采用RECEIPT回复,故不能使用此指令判断主机类型(系统型/单户型)
				#if 0
				if( (CallServer_Run.state == CallServer_Invite) && ((CallServer_Run.call_type == DxCallScene3)||(CallServer_Run.call_type == DxCallScene4_Master))  )
				{
					API_CallServer_InviteOk(CallServer_Run.call_type);
				}
						//czn_20170717
				else if( Mon_Run.state == MON_LINKING )
				{
					OS_SignalEvent(MON_LINK_OK, &task_localmon);
				}	
				#endif
				break;
			//case SUB_MODEL:
			case SUB_MODEL_1:	
			case SUB_RECEIPT	:
				if( CallServer_Run.state == CallServer_Invite&& CallServer_Run.call_type == DxMainCall)
				{
					API_CallServer_InviteOk();
				}
				if( CallServer_Run.state != CallServer_Wait&& CallServer_Run.call_type == IXGMainCall)
				{
					API_CallServer_InviteOk();
				}
				break;
			case RECEIPT_SINGLE:	
				#if 0
				 if( Mon_Run.state == MON_LINKING )
				{
					OS_SignalEvent(MON_LINK_OK, &task_localmon);
				}
				 #endif
				break;

			case MAIN_CANCEL_GROUP:	
			case ST_TALK:
			case INNER_TALK:
				if( CallServer_Run.state != CallServer_Wait&& CallServer_Run.call_type == IXGMainCall)
				{
					API_CallServer_LocalAck();
				}
				#if 1
				else if(CallServer_Run.state == CallServer_Ring || CallServer_Run.state == CallServer_Invite)
				{
					//addr = DtAddr_Trs2_IpGlobalAddr(Message_command->address);
					API_CallServer_RemoteAck();	
				}
				else
				{
					//Video_MonTalk_Request();
					API_vbu_MonTalkCmd();
				}
				#endif
				break;

			case INNER_CALL:
				#if 0
				//addr = DtAddr_Trs2_IpGlobalAddr(Message_command->address);
				API_CallServer_DXInvite(DxCallScene6_Master,Message_command->address);
				#endif
				break;
				

			case LET_DIDONG_ONCE:	//czn20171030
				//API_CallServer_Redail(DxCallScene1_Master);
				break;	
			case ST_UNLOCK:
				log_d("RECV ST_UNLOCK");
				if( CallServer_Run.state != CallServer_Wait&& CallServer_Run.call_type == DxMainCall)
				{
					API_CallServer_RemoteUnlock1();
				}
				else
					MonitorToUnlock(1);
				break;
			case ST_UNLOCK_SECOND:
				if( CallServer_Run.state != CallServer_Wait&& CallServer_Run.call_type == DxMainCall)
				{
					API_CallServer_RemoteUnlock2();
				}
				else
					MonitorToUnlock(2);
				break;
			case MON_ON	:
				//Video_MonOn_Request();
				API_vbu_MonOnCmd(Message_command->address);
				break;
			case MON_OFF:
				//Video_MonOff_Request();
				API_vbu_MonOffCmd();
				break;
			case LET_MR_CALLME:
				DtCallBackCmd_process(Message_command->address);
				break;
			
			case DIVERT_SET_REQ:
				printf("DIVERT_SET_REQ len = %d: ",Message_command->data_len);
				for( i = 0; i < Message_command->data_len; i++ )
				{
					printf(" %02x ",Message_command->data_buf[i]);	
				}
				printf("\n"); 
				if(Message_command->data_buf[0]==0)
				{
					buff[0]=SetDtImDivertOff(Message_command->address);
					API_Stack_APT_Without_ACK_Data(Message_command->address,DIVERT_SET_RSP,1,&buff[0]);
				}
				else if(Message_command->data_buf[0]==1)
				{
					buff[0]=SetDtImDivertOn(Message_command->address);
					API_Stack_APT_Without_ACK_Data(Message_command->address,DIVERT_SET_RSP,1,&buff[0]);
				}
				else if(Message_command->data_buf[0]==2)
				{
					API_Stack_APT_Without_ACK(Message_command->address,RECEIPT);
					if(reset_sip_dt_im_phone_pwd(Message_command->address)==0)
					{
						printf("1111111111reset_sip_dt_im_phone_pwd:ok\n");
						buff[0]=6;//ok
						
					}
					else
					{
						printf("22222222222reset_sip_dt_im_phone_pwd:err\n");
						buff[0]=5;//err
					}
					API_Stack_APT_Without_ACK_Data(Message_command->address,DIVERT_SET_RSP,1,&buff[0]);
				}
				break;
		}
	}
}

#pragma pack(1)
typedef struct
{
	unsigned char 	wday;
	unsigned char		mon;
	unsigned char		mday;
	unsigned char		year;
	unsigned char		hour;
	unsigned char		min;
	unsigned char		sec;
	unsigned char		mode;
}Rtc_t;
#pragma pack()
#define BCD2HEX(bcd)	(((bcd)&0x0F)+(((bcd)>>4)*10))
#define HEX2BCD(hex)	((((hex)/10)<<4)+((hex)%10))

static Rtc_t GetRtcTime(void)
{
    time_t timeSec;
    struct tm *pTm;
  	Rtc_t rtcTime;

    time(&timeSec);
    pTm=localtime(&timeSec);

    rtcTime.year = HEX2BCD(pTm->tm_year-100);
    rtcTime.mon = HEX2BCD(pTm->tm_mon+1);
    rtcTime.mday = HEX2BCD(pTm->tm_mday);
    rtcTime.hour = HEX2BCD(pTm->tm_hour);
    rtcTime.min = HEX2BCD(pTm->tm_min);
    rtcTime.sec = HEX2BCD(pTm->tm_sec);
    rtcTime.wday = HEX2BCD(pTm->tm_wday);
    rtcTime.mode = 1;

    bprintf("year=%x, mon=%x, mday=%x, hour=%x, min=%x, sec=%x\n", rtcTime.year, rtcTime.mon, rtcTime.mday, rtcTime.hour, rtcTime.min, rtcTime.sec);
    return rtcTime;
}

static void SetRtcTime(Rtc_t time)
{
    char cmd[100];
	  FILE* fd;

    snprintf( cmd, sizeof(cmd),  "date -s \"20%02d-%02d-%02d %02d:%02d:%02d\"\n", BCD2HEX(time.year), BCD2HEX(time.mon), BCD2HEX(time.mday), BCD2HEX(time.hour), BCD2HEX(time.min), BCD2HEX(time.sec));

    dprintf( "%s\n",cmd);
    
    if( (fd = popen( cmd, "r" )) == NULL )
    {
      dprintf( "set_system_date_time error:%s\n",strerror(errno) );
      return;
    }		
    pclose( fd );

    if( (fd = popen( "hwclock -w", "r" )) == NULL )
    {
      dprintf( "set_system_date_time error:%s\n",strerror(errno) );
      return;
    }
    pclose( fd );
}

void RTC_DT_ServerSendTime(void)
{
	if(API_Para_Read_Int(TIME_DT_ServerEnable))
	{
		Rtc_t rtcTime = GetRtcTime();
		API_GroupValue_Write(GA6_RTC_SERVICE, sizeof(rtcTime), &rtcTime);
	}
}

void NewCommand_Process(DTSTACK_MSG_TYPE *Message_command)
{
	int da = Message_command->target_address;
	int sa = Message_command->address;
	char *data = Message_command->data_buf;
	int len = Message_command->data_len;
	Rtc_t *pRtcTime = (Rtc_t*)data;

	if(Message_command->command_type == APCI_GROUP_VALUE_READ || Message_command->command_type == APCI_GROUP_VALUE_WRITE || Message_command->command_type == APCI_GROUP_VALUE_RESPONSE)
	{
		switch (Message_command->target_address)
		{
			case GA7_RTC_CLIENT:
				RTC_DT_ServerSendTime();
				break;

			case GA8_RTC_SET:
				//2000年1月1日特殊，用来设置时区
				if(pRtcTime->year == 0 && pRtcTime->mon == 0x01 && pRtcTime->mday == 0x01)
				{
					API_Para_Write_Int(TIME_AutoUpdateEnable, 1);
					if(pRtcTime->hour == 0)
					{
						API_Para_Write_Int(TIME_ZONE, 0 - pRtcTime->min);
					}
					else if(pRtcTime->min == 0)
					{
						API_Para_Write_Int(TIME_ZONE, pRtcTime->hour);
					}
					sync_time_from_sntp_server(inet_addr(API_Para_Read_String2(TIME_SERVER)), API_Para_Read_Int(TIME_ZONE));
				}
				else
				{
					API_Para_Write_Int(TIME_AutoUpdateEnable, 0);
					if(API_Para_Read_Int(TIME_DT_AutoUpdateEnable))
					{
						SetRtcTime(*pRtcTime);
					}
				}
				RTC_DT_ServerSendTime();
				break;

			case GA6_RTC_SERVICE:
				if(API_Para_Read_Int(TIME_DT_AutoUpdateEnable))
				{
					SetRtcTime(*pRtcTime);
				}
				break;

			case GA204_INTERCOMCALL_INVITE:
				IpgIntercomCallInvite_Process(sa,Message_command->data_buf,Message_command->data_len);
				break;

			case GA5_MEM_ORDER:
				API_vbu_GoCmd(Message_command->address,Message_command->target_address,Message_command->data_buf,Message_command->data_len);
				break;
			case GA1_VIDEO_REQUEST:
				API_vbu_GoCmd(Message_command->address,Message_command->target_address,Message_command->data_buf,Message_command->data_len);
				break;
			
			case GA3_MEM_PLAYBACK:
				API_vbu_GoCmd(Message_command->address,Message_command->target_address,Message_command->data_buf,Message_command->data_len);
				break;
			case COBID56_ONLINE_DEVICE_RSP:
				Set_BDU_Check_Result(data);
				break;
			
			default:
				break;
		}
	}
}
