
/**
  ******************************************************************************
  * @file    obj_APCIService.c
  * @author  lv
  * @version V1.0.0
  * @date    2012.11.26
  * @brief   This file contains the functions of obj_APCIService.c
  ******************************************************************************
  */ 

#include "stack212.h"

#include "elog.h"
#include "sntp_s.h"


/*******************************************************************************************
 * @fn:		StackAPCIInputProcess
 *
 * @brief:	����Э��ջ��������APCI����ָ�
 *
 * @param:  ptrMsgDat - ��Ϣ����ָ��
 *
 * @return: 0/ʧ�ܣ�1/OK
 *******************************************************************************************/
unsigned char StackAPCIInputProcess(void *ptrMsgDat)
{	
  	unsigned short device_sa,device_da;
	unsigned short property_id;
	unsigned char no_of_elem, start_index;
	int pointx,pointy;
	AI_DATA *ptrCMdIo = (AI_DATA *)ptrMsgDat;

			int i, len;
			unsigned char* ptrDat;;
	
	device_sa		= ptrCMdIo->head.sada&0xf0;
	device_sa		<<= 4;
	device_sa		|= ptrCMdIo->head.sa;
	device_da		= ptrCMdIo->head.sada&0x0f;
	device_da		<<= 8;
	device_da		|= ptrCMdIo->head.da;
	log_d("APCI Cmd:%02x,sa:%02x,da:%02x",ptrCMdIo->head.cmd,device_sa,device_da);
	//API_Survey_MsgNewCommand(device_sa,device_da,ptrCMdIo->head.cmd, ptrCMdIo->head.length.bit.len,&ptrCMdIo->data[0]);
	switch( ptrCMdIo->head.cmd )
	{
		//Property:	�����ȡ��������
		case APCI_PROPERTY_VAULE_READ:
			if( myAddr == device_da )
			{
			  	//��ָ�����ݰ��ĳ��ȱ�����ڻ����4 �� Property_ID, no_of_elem, start_elem
			 	if( ptrCMdIo->head.length.bit.len >= 4 )
				{
				  	property_id	= (ptrCMdIo->data[0]<<8)|ptrCMdIo->data[1];
				  	no_of_elem	= ptrCMdIo->data[2];
				  	start_index = ptrCMdIo->data[3];
				  	//API_IoServer_BeRead(device_sa, property_id, no_of_elem, start_index);
				}
			}		  	
		  	break;
		//Property:	��Ӧ������ȡ����
		case APCI_PROPERTY_VAULE_RESPONSE:
			#if 0	//will_add
			if( myAddr == device_da )
			{
			  	//��ָ�����ݰ��ĳ��ȱ�����ڻ����4 �� Property_ID, no_of_elem, start_elem
			 	if( ptrCMdIo->head.length.bit.len >= 4 )
				{
				  	property_id	= (ptrCMdIo->data[0]<<8)|ptrCMdIo->data[1];
				  	no_of_elem	= ptrCMdIo->data[2];
				  	start_index = ptrCMdIo->data[3];
 				  	API_IoServer_BeResponse(device_sa, property_id, no_of_elem, start_index, &ptrCMdIo->data[4] );
				}
			}	
			#endif
		  	break;
		//Property:	���󱾻�д�����
		case APCI_PROPERTY_VAULE_WRITE:
			if( myAddr == device_da )
			{
			  	//��ָ�����ݰ��ĳ��ȱ�����ڻ����4�� Property_ID, no_of_elem, start_elem
			 	if( ptrCMdIo->head.length.bit.len >= 4 )
				{
				  	property_id	= (ptrCMdIo->data[0]<<8)|ptrCMdIo->data[1];
				  	no_of_elem	= ptrCMdIo->data[2];
				  	start_index = ptrCMdIo->data[3];
 				  	//API_IoServer_BeWrite(device_sa, property_id, no_of_elem, start_index, &ptrCMdIo->data[4] );
				}
			}		  
		  	break;
			
		//Group Value	lzh_20121212
		case APCI_GROUP_VALUE_READ:
		case APCI_GROUP_VALUE_RESPONSE:
		case APCI_GROUP_VALUE_WRITE:

 		  	//API_GoServer_BeWrite(device_da,device_sa, ptrCMdIo->head.length.bit.len, &ptrCMdIo->data[0] );		//czn_20170719
			//Go_write_process(device_da,device_sa, &ptrCMdIo->data[0], ptrCMdIo->head.length.bit.len);

			API_Survey_MsgNewCommand(device_sa, device_da, ptrCMdIo->head.cmd, ptrCMdIo->head.length.bit.len, &ptrCMdIo->data[0]);
			break;
			
		//Addr program
		case APCI_INDIVIDUAL_ADDR_WRITE:
		case APCI_INDIVIDUAL_ADDR_READ:
		case APCI_INDIVIDUAL_ADDR_RESPONSE:
			break;
		
		//Check Addr Program status
		case APCI_PROG_STATE_READ:
		case APCI_PROG_STATE_RESPONSE:
		case APCI_PROG_STATE_WRITE:
			break;
		
		//Linking 
		case APCI_INDIVIDUAL_ADDR_LINK:
		//case APCI_RESTART:
			break;
		case APCI_IPG_LINK:
			//log_d("APCI_IPG_LINK:%02x,sa:%02x,da:%02x",ptrCMdIo->head.cmd,device_sa,device_da);
			IpgOnlineBrd_Send();
			break;
		case APCI_RM_M_REQ:
			//IpgRmReq_Process(device_sa,ptrCMdIo->data[0]);
			API_vbu_RmReq(device_sa,device_da,ptrCMdIo->data,ptrCMdIo->head.length);
			break;	
		case APCI_RM_M_EXIT:
			//IpgRmExit_Process(device_sa);
			API_vbu_RmCtrl(ptrCMdIo->head.cmd ,device_sa,device_da,ptrCMdIo->data,ptrCMdIo->head.length);
			break;
		case APCI_RM_M_OK:
			IpgRmTp_Process(device_sa,-1,-1);
			break;
		case APCI_RM_M_TP:
			pointx = ptrCMdIo->data[0];
			pointx = (pointx<<8)|ptrCMdIo->data[1];
			pointy = ptrCMdIo->data[2];
			pointy = (pointy<<8)|ptrCMdIo->data[3];
			IpgRmTp_Process(device_sa,pointx,pointy);
			break;
		case APCI_RM_INPUT_RES:
			IpgRmInput_Process(device_sa,&ptrCMdIo->data[2],ptrCMdIo->data[1]);
			break;
		case APCI_DIVERT_START:
			IpgDivertStart_Process(device_sa);
			break;

		case APCI_DX_SET_TP:
			DebugTouchpad((ptrCMdIo->data[0]<<8) | ptrCMdIo->data[1], (ptrCMdIo->data[2]<< 8) | ptrCMdIo->data[3]);
			break;

		#if 0	//will_add
		case APCI_RM_REP_REQUEST:
		case APCI_RM_QUIT:
		case APCI_RM_ENTER_INPUT:	
			if( myAddr == device_da)
            {
				RmCmdProcess(ptrCMdIo->head.cmd, ptrCMdIo->head.length.bit.len, &ptrCMdIo->data[0]);
			}
			break;
			
		case APCI_RM_DISPLAY:
			if( myAddr == device_da )
			{
				API_RM_Display( &ptrCMdIo->data[0], ptrCMdIo->head.length.bit.len );
			}
			break;
		#endif
	}
	return TRUE;
}

/*******************************************************************************************
 * @fn:		StackNewCommandOutputProcess
 *
 * @brief:	����task��Ҫת������ָ��
 *
 * @param:  ptrMsgDat - ��Ϣ����ָ��
 *
 * @return: 0/ʧ�ܣ�1/OK
 *******************************************************************************************/
unsigned char StackNewCommandOutputProcess(void *ptrMsgDat)
{
  	unsigned char ret;
	StackFrameQ_TaskIO *ptrCmdOutputForSend;
	
	ptrCmdOutputForSend = (StackFrameQ_TaskIO*)ptrMsgDat;

	switch( ptrCmdOutputForSend->cmd )
	{
		case APCI_PROPERTY_VAULE_READ:
		  	ret = SendOneNewCommand(0,0,1,ptrCmdOutputForSend->addr,ptrCmdOutputForSend->cmd,ptrCmdOutputForSend->dat,ptrCmdOutputForSend->len );
		  	break;
		case APCI_PROPERTY_VAULE_RESPONSE:
		  	ret = SendOneNewCommand(0,1,1,ptrCmdOutputForSend->addr,ptrCmdOutputForSend->cmd,ptrCmdOutputForSend->dat,ptrCmdOutputForSend->len );
		  	break;
		case APCI_PROPERTY_VAULE_WRITE:
		  	ret = SendOneNewCommand(0,0,1,ptrCmdOutputForSend->addr,ptrCmdOutputForSend->cmd,ptrCmdOutputForSend->dat,ptrCmdOutputForSend->len );
		  break;

		//lzh 20121212
		case APCI_GROUP_VALUE_READ:
		  	ret = SendOneNewCommand(1,0,0,ptrCmdOutputForSend->addr,ptrCmdOutputForSend->cmd,ptrCmdOutputForSend->dat,ptrCmdOutputForSend->len );
		  	break;
		case APCI_GROUP_VALUE_RESPONSE:
		  	ret = SendOneNewCommand(1,1,0,ptrCmdOutputForSend->addr,ptrCmdOutputForSend->cmd,ptrCmdOutputForSend->dat,ptrCmdOutputForSend->len );
		  	break;
		case APCI_GROUP_VALUE_WRITE:
		  	ret = SendOneNewCommand(1,0,0,ptrCmdOutputForSend->addr,ptrCmdOutputForSend->cmd,ptrCmdOutputForSend->dat,ptrCmdOutputForSend->len );
		  break;
		  
		// 20171109	 
        case APCI_FAST_LINKING: 
		case APCI_RM_REQUEST:
		case APCI_RM_QUIT:
		case APCI_DIVERT_RESET:
		case APCI_RM_M_REQ:
		case APCI_IPG_LINK: 
		case APCI_DIVERT_SYNC:
		case APCI_DS_QTY_SYNC:
		case APCI_CAM_QTY_SYNC:
			ret = SendOneNewCommand(0,0,1,ptrCmdOutputForSend->addr,ptrCmdOutputForSend->cmd,ptrCmdOutputForSend->dat,ptrCmdOutputForSend->len );
			break;

		case APCI_RM_OK:
		case APCI_RM_EXIT:
		case APCI_RM_UP:
		case APCI_RM_DOWN:
		case APCI_RM_LAST:
		case APCI_RM_NEXT:
		case APCI_RM_INPUT_RES:
		case APCI_RM_EXIT_INPUT:
		case APCI_DIVERT_GET_ACOUNT:
		case APCI_DIVERT_START:
		case APCI_RM_M_EXIT:
		case APCI_RM_M_TP:
		case APCI_IPC_SEARCH_ON:
		case APCI_DX_SET_ON:
		case APCI_DX_SET_OFF:
			ret = SendOneNewCommand(0,0,0,ptrCmdOutputForSend->addr,ptrCmdOutputForSend->cmd,ptrCmdOutputForSend->dat,ptrCmdOutputForSend->len );
			break;

		case APCI_DIVERT_START_INFORM:		//czn_20181117
			ret = SendOneNewCommand(0,0,1,ptrCmdOutputForSend->addr,ptrCmdOutputForSend->cmd,ptrCmdOutputForSend->dat,ptrCmdOutputForSend->len );
			break;
		case APCI_RM_M_RSP:		//czn_20181117
		case APCI_RM_ENTER_INPUT:	
		case APCI_RM_M_QUIT:
		case APCI_IPG_STATE:
		case APCI_DIVERT_RSP_START:	
			ret = SendOneNewCommand(0,0,1,ptrCmdOutputForSend->addr,ptrCmdOutputForSend->cmd,ptrCmdOutputForSend->dat,ptrCmdOutputForSend->len );
			break;

	}
	return ret;
}

// lzh_20140508_s
/*******************************************************************************************
 * @fn:		StackAPCILongDataInputProcess
 *
 * @brief:	����Э��ջ��������APCI����ָ�
 *
 * @param:  ptrMsgDat - ��Ϣ����ָ��
 *
 * @return: 0/ʧ�ܣ�1/OK
 *******************************************************************************************/
//AI_LONG_DATA tempdatbuf;
unsigned char StackAPCILongDataInputProcess(void *ptrMsgDat)
{	
  	unsigned short device_sa,device_da;
	AI_LONG_DATA *ptrCMdIo = (AI_LONG_DATA *)ptrMsgDat;
	
	device_sa		= ptrCMdIo->head.sada&0xf0;
	device_sa		<<= 4;
	device_sa		|= ptrCMdIo->head.sa;
	device_da		= ptrCMdIo->head.sada&0x0f;
	device_da		<<= 8;
	device_da		|= ptrCMdIo->head.da;

	log_d("APCILong Cmd:%02x,sa:%02x,da:%02x",ptrCMdIo->head.cmd,device_sa,device_da);
	switch( ptrCMdIo->head.cmd )
	{			
		case APCI_RM_DISPLAY:
            if( myAddr == device_da )
            {
                //will_add API_RM_Display( &ptrCMdIo->ldata.dat[0], ptrCMdIo->ldata.long_len.llength );
            }
            break;
	}
	return TRUE;
}
// lzh_20140508_e


