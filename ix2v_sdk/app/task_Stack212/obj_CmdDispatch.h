
#ifndef _OBJ_CMDDISPATCH_H
#define _OBJ_CMDDISPATCH_H

#include "utility.h"
#include "task_survey.h"

typedef struct
{
	vdp_task_t 		*ptrTCBResponse;		//�ظ� Event ACK������IDָ��
	unsigned char 	reEnable;				//�ط�ʹ�ܱ�־
	unsigned char 	reSendCnt;				//�ȴ� Receipt/Receipt_Single���ط�����
	unsigned char 	reBroadcast;			//0���ط�Ϊָ�����1���ط�Ϊ�㲥����
} WAIT_RESPONSE;

extern WAIT_RESPONSE WaitResponse;

// Define Object Property

// Define Object Function - Private

unsigned char WhichReceiptShouldBeGiven(unsigned char cmd);

void SetWaitResponse(vdp_task_t *ptrTCB,unsigned char cmd,unsigned char brd);
void ResetWaitResponse(void);
void TrsResultResponseCallback(unsigned char rspOK);

// Define Object Function - Public

//����Э��ջ�������ľ�����
unsigned char StackOldCommandInputProcess(void *ptrMsgDat);

//����task��Ҫת���ľ�ָ��
unsigned char StackOldCommandOutputProcess(void *ptrMsgDat,unsigned char brd);

void DtCommand_Analyzer(DTSTACK_MSG_TYPE *Message_command);
void NewCommand_Process(DTSTACK_MSG_TYPE *Message_command);
#endif
