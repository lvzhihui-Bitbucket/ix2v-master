/**
  ******************************************************************************
  * @file    task_StackAI.c
  * @author  lv
  * @version V1.0.0
  * @date    2012.09.20
  * @brief   This file contains the functions of task_StackAI.c
  */ 

#ifndef _TASK_STACKAI_H
#define _TASK_STACKAI_H

#include "msg_Type.h"
#include "obj_L2Layer.h"
#include "obj_AiLayer.h"
#include "task_survey.h"

// Define Task EventFlags
/*
�¼����壺Stackת�������ݣ�֪ͨAI
 */
 
 //lzh_20130413
//�ظ���ȷ�¼���־
#define TASK_STACK_EVENT_ACK_OK			( 1 << 1 )
//�ظ������¼���־
#define TASK_STACK_EVENT_ACK_ER			( 1 << 0 )

//���տ�ʼ�¼���־
#define TASK_STACK_EVENT_RCV_START		( 1 << 2 )
//���Ϳ�ʼ�¼���־
#define TASK_STACK_EVENT_TRS_START		( 1 << 3 )
//�������ʱ���־
#define TASK_STACK_EVENT_TRS_STOP		( 1 << 4 )

#define OS_TASK_EVENT_STACK_TRS_RESULT		(1<<2)
#define OS_TASK_EVENT_STACK_TRS_QUEUE		(1<<3)
#define OS_TASK_EVENT_STACK_RCV_QUEUE		(1<<4)
#define TASK_STACK_EVENT_GO_OK				(1<<5)

// Define Task Vars and Structures

/*------------------------------------------------------------------------------------------
//task_AI�Ķ�Э��ջ�������������֡�շ�����
------------------------------------------------------------------------------------------*/
/*
��Ϣ���壺Task��Ҫ�������ݣ�֪ͨAIת��(��AI����)����Ӧ���ݽṹΪ��StackFrameQ_TaskIO
 */
#define OS_Q_STACK_FRAME_TASK_TRS_BRD_A		0x01	//����Ӧ��
#define OS_Q_STACK_FRAME_TASK_TRS_BRD_B		0x02	//��ҪӦ��
#define OS_Q_STACK_FRAME_TASK_TRS_APT_A		0x03	//����Ӧ��
#define OS_Q_STACK_FRAME_TASK_TRS_APT_B		0x04	//��ҪӦ��

//lzh_20130328
#define OS_Q_STACK_FRAME_TASK_TRS_ACK_A		0x05	//���Ͳ���ACK
//lzh_20130413
#define OS_Q_STACK_FRAME_TASK_TRS_BLK_A		0x06	//����block
/*
��Ϣ���壺Stack���յ����ݣ�֪ͨAI(��AI����)����Ӧ�����ݽṹ��StackFrameQ_StackIO
 */
#define OS_Q_STACK_FRAME_STACK_RCV			0x04

#pragma pack(1)	//stm32_stack
//task_AI�Ķ���������Ľ������ݽṹ
typedef struct
{
	//��Ϣ����
	unsigned char msgType;
	//Դ�����ַ
	vdp_task_t *ptrTCB;
	//Ŀ���ַ(Output)/Դ��ַ(Input)
	unsigned short addr;
	//�¾�ָ��
	unsigned char cmdType;	//0:old,1:new
	//ָ��
	unsigned char cmd;
	//���ݰ�����
	unsigned char len;
	//����
	unsigned char dat[MAX_FRAME_LENGTH];
	
} StackFrameQ_TaskIO;

//task_AI�Ķ�Э��ջ�Ľ������ݽṹ
typedef struct
{
	//��Ϣ����
	unsigned char msgType;
	//��������
	unsigned char datType;
	//����
	AI_DATA	datBuf;
	
} StackFrameQ_StackIO;

//lzh_20140508_s
typedef struct
{
	//��Ϣ����
	unsigned char msgType;
	//��������
	unsigned char datType;
	//����
	AI_LONG_DATA  datBuf;
	
} StackFrameQ_StackIO_LongData;
//lzh_20140508_e

#pragma pack()	//stm32_stack


// Define Task 3 items
extern Loop_vdp_common_buffer	vdp_StackAI_Trs_queue;
extern Loop_vdp_common_buffer	vdp_StackAI_Rcv_queue;
extern Loop_vdp_common_buffer	vdp_StackAI_sync_queue;

void vtk_TaskInit_StackAI( int Priority );

// Define Task others
void CheckTrsStatusAndWaitingForTrsResult(void);
void RcvQueueProcess(void);
void TrsQueueProcess(void);
unsigned char AI_PushOneCmdIntoQueue(unsigned char msgtype,unsigned short addr,unsigned char cmd, unsigned char newCmd, unsigned char len, unsigned char *ptrDat);	//lzh_20121124
// Define Task public API
unsigned char AI_BoardCastWithEventAck(unsigned short addr,unsigned char cmd, unsigned char len, unsigned char *ptrDat);		//lzh_20121122
unsigned char AI_BoardCastWithoutEventAck(unsigned short addr,unsigned char cmd, unsigned char len, unsigned char *ptrDat);	//lzh_20121122
unsigned char AI_AppointWithEventAck(unsigned short addr,unsigned char cmd,  unsigned char len, unsigned char *ptrDat);		//lzh_20121122
unsigned char AI_AppointWithoutEventAck(unsigned short addr,unsigned char cmd,  unsigned char len, unsigned char *ptrDat);	//lzh_20121122	

#define API_Stack_BRD_Without_ACK(cmd) \
		AI_PushOneCmdIntoQueue(OS_Q_STACK_FRAME_TASK_TRS_BRD_A, BROADCAST_ADDR, cmd, 0, 0, NULL )

#define API_Stack_BRD_With_ACK(cmd) \
		AI_BoardCastWithEventAck(BROADCAST_ADDR, cmd, 0, NULL )

#define API_Stack_APT_Without_ACK(addr,cmd) \
		AI_PushOneCmdIntoQueue(OS_Q_STACK_FRAME_TASK_TRS_APT_A, addr, cmd, 0, 0, NULL )

#define API_Stack_APT_With_ACK(addr,cmd) \
		AI_AppointWithEventAck(addr, cmd, 0, NULL )

//lzh_20121122
#define API_Stack_BRD_Without_ACK_Data(cmd,len,ptr_dat) \
		AI_PushOneCmdIntoQueue(OS_Q_STACK_FRAME_TASK_TRS_BRD_A, BROADCAST_ADDR, cmd, 0, len, ptr_dat )

#define API_Stack_APT_Without_ACK_Data(addr,cmd,len,ptr_dat) \
		AI_PushOneCmdIntoQueue(OS_Q_STACK_FRAME_TASK_TRS_APT_A, addr, cmd, 0, len, ptr_dat )

//lzh_20121124
unsigned char API_PropertyValue_Read(unsigned short property_id, unsigned char no_of_elem, unsigned char start_index, unsigned short device_address);
unsigned char API_PropertyValue_Response(unsigned short property_id, unsigned char no_of_elem, unsigned char start_index, unsigned short device_address, unsigned char *ptr_Data);
unsigned char API_PropertyValue_Write(unsigned short property_id, unsigned char no_of_elem, unsigned char start_index, unsigned short device_address, unsigned char *ptr_Data);

//lzh_20121128
//lzh_20130325
void TimerMuteRetrigger(void);

#define AVOID_NOISE_ENABLE	    SPK_MuteOn();

#define AVOID_NOISE_DISABLE	    TimerMuteRetrigger();	//SPK_MuteOff();	//lzh_20130325

//lzh_20121212
unsigned char API_GroupValue_Read(unsigned short group_id);
unsigned char API_GroupValue_Response(unsigned short group_id, unsigned char length, unsigned char *ptr_Data);
unsigned char API_GroupValue_Write(unsigned short group_id, unsigned char length, unsigned char *ptr_Data);

//lzh_20130328
#define API_Stack_BRD_ACK() \
		AI_PushOneCmdIntoQueue(OS_Q_STACK_FRAME_TASK_TRS_ACK_A, BROADCAST_ADDR, 0, 0, 0, NULL )

//lzh_20130413
#define API_Stack_BRD_BLK() \
		AI_PushOneCmdIntoQueue(OS_Q_STACK_FRAME_TASK_TRS_BLK_A, BROADCAST_ADDR, 0, 0, 0, NULL )

#define API_Stack_New_APT(addr,cmd) \
		AI_PushOneCmdIntoQueue(OS_Q_STACK_FRAME_TASK_TRS_APT_B, addr, cmd, 1, 0, NULL )


#define API_Stack_New_APT_Data(addr,cmd,len,ptr_dat) \
		AI_PushOneCmdIntoQueue(OS_Q_STACK_FRAME_TASK_TRS_APT_B, addr, cmd, 1, len, ptr_dat )

#define API_Stack_New_BRD_Without_ACK(cmd) \
		AI_PushOneCmdIntoQueue(OS_Q_STACK_FRAME_TASK_TRS_BRD_A, BROADCAST_ADDR, cmd, 1, 0, NULL )		

//stack_stm32
void TImerMuteCallBack(void);
			
int API_Get_BDU_ImOnlineResult(int bduId);
int API_Check_BDU_Online(int bduId);
			
#endif


