

#include <stdio.h>
#include <strings.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <sys/time.h>
#include <stdlib.h>
#include <memory.h>
#include <arpa/inet.h>
#include <arpa/nameser.h>
#include <netinet/in.h>
#include <net/if.h>
#include <netdb.h>
#include <poll.h>
#include <pthread.h>
#include <error.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <resolv.h>

#include "vtk_tcp_auto_reg.h"
//#include "task_IoServer.h"
#include "vtk_udp_stack_class.h"
#include "obj_IoInterface.h"
/*
Checksum前面所有数据包的累加和再乘以端口号（8848）
return: checksum
*/
int calculate_checksum( unsigned char* pbuffer, int len )
{
	int i;
	int checksum = 0;
	for( i = 0; i < len; i++ )
	{
		checksum += pbuffer[i];
	}
	return (VTK_AUTO_REG_SERVER_PORT*checksum);
}


/*
ip_str:	connect server ip address
port:		connect port
psockfd:	connect ok return fd
return:	 0/ok, -1/err
*/
int connect_with_timeout(char* ip_str, int port, int t_timeout, int* psockfd )
{
	int fd = 0;
	struct sockaddr_in	addr;
	fd_set fdr, fdw;
	struct timeval timeout;
	int err = 0;
	int errlen = sizeof(err);
	

	fd = socket(AF_INET,SOCK_STREAM,0);
	
	if (fd < 0) 
	{
		printf("create socket failed\n");
		return -1;
	}

	int m_loop = 1;
	if( setsockopt(fd, SOL_SOCKET, SO_REUSEADDR, &m_loop, sizeof(m_loop)) == -1 )
	{
		eprintf("sockfd =%d, Setsockopt SOL_SOCKET::SO_REUSEADDR Failure!\n", fd);
		return -1;
	}
	#if 1
	struct sockaddr_in	sin;
	struct ifreq ifr;
	//char sip_net_sel[5];
	bzero(&sin, sizeof(sin));	
	sin.sin_family = AF_INET;
	sin.sin_addr.s_addr = INADDR_ANY;
	sin.sin_port = htons(port);
	memset(ifr.ifr_name, 0, IFNAMSIZ);
	//API_Event_IoServer_InnerRead_All(SIP_NetworkSetting, sip_net_sel);
	//strcpy(sip_net_sel,"0");
	
	char *sip_route;
	char *sip_route_name;
	
	sip_route=API_Para_Read_String2(SIP_NetworkSetting);
	if(sip_route==NULL)
		sip_route_name=NULL;
	else if(strcmp(sip_route,"LAN")==0)
		sip_route_name="eth0";
	else if(strcmp(sip_route,"WLAN")==0)
		sip_route_name="wlan0";
	else if(strcmp(sip_route,"4G")==0)
		sip_route_name="usb0";
	else
		sip_route_name=NULL;
	if(sip_route_name!=NULL&&GetLocalGatewayByDevice(sip_route_name)!=-1)
		sprintf(ifr.ifr_name, "%s", sip_route_name);
	else
	{
		if(GetLocalGatewayByDevice("eth0")!=-1)
			sprintf(ifr.ifr_name, "%s", "eth0");
		else if(GetLocalGatewayByDevice("wlan0")!=-1)
			sprintf(ifr.ifr_name, "%s", "wlan0");
		else if(GetLocalGatewayByDevice("usb0")!=-1)
			sprintf(ifr.ifr_name, "%s", "usb0");
		else
		{
			close(fd);
			return -1;
		}
	}

	if(setsockopt(fd, SOL_SOCKET, SO_BINDTODEVICE, (char *)&ifr, sizeof(struct ifreq)))
	{
		eprintf("Setsockopt SOL_SOCKET::SO_BINDTODEVICE Failure!\n");
		close(fd);
		return -1;
	}
	#if 0
	if(bind(fd, (struct sockaddr *) &sin, sizeof(sin)) == -1)
	{
		eprintf("fail to bind\n");
		close(fd);
		return -1;
	}
	#endif
	#endif
	bzero(&addr, sizeof(addr));
	addr.sin_family = AF_INET;
	addr.sin_port = htons(port);
	inet_pton(AF_INET, ip_str, &addr.sin_addr);
	//addr.sin_addr.s_addr = inet_addr(ip_str);

	/*设置套接字为非阻塞*/
	int flags = fcntl(fd, F_GETFL, 0);
	if (flags < 0) 
	{
		fprintf(stderr, "Get flags error:%s\n", strerror(errno));
		close(fd);
		return -1;
	}
	flags |= O_NONBLOCK;
	if (fcntl(fd, F_SETFL, flags) < 0) 
	{
		fprintf(stderr, "Set flags error:%s\n", strerror(errno));
		close(fd);
		return -1;
	}
	
	/*阻塞情况下linux系统默认超时时间为75s*/
	int rc = connect(fd, (struct sockaddr*)&addr, sizeof(addr));
	if (rc != 0) 
	{
		if (errno == EINPROGRESS) 
		{
			printf("Doing connection.\n");
			/*正在处理连接*/
			FD_ZERO(&fdr);
			FD_ZERO(&fdw);
			FD_SET(fd, &fdr);
			FD_SET(fd, &fdw);
			timeout.tv_sec = t_timeout;
			timeout.tv_usec = 0;
			rc = select(fd + 1, &fdr, &fdw, NULL, &timeout);
			printf("rc is: %d\n", rc);
			/*select调用失败*/
			if (rc < 0) 
			{
				fprintf(stderr, "connect error:%s\n", strerror(errno));
				close(fd);
				return -1;
			}
		
			/*连接超时*/
			if (rc == 0) 
			{
				fprintf(stderr, "Connect timeout.\n");
				close(fd);
				return -1;
			}
			/*[1] 当连接成功建立时，描述符变成可写,rc=1*/
			if (rc == 1 && FD_ISSET(fd, &fdw)) 
			{
				goto connect_success;
			}
			//usleep(100*1000);
			//goto connect_success;
			/*[2] 当连接建立遇到错误时，描述符变为即可读，也可写，rc=2 遇到这种情况，可调用getsockopt函数*/
			#if 1
			if (rc == 2) 
			{
				if (getsockopt(fd, SOL_SOCKET, SO_ERROR, &err, &errlen) == -1) 
				{
					fprintf(stderr, "getsockopt(SO_ERROR): %s", strerror(errno));
					close(fd);
					return -1;
				}

				if (err) 
				{
					errno = err;
					fprintf(stderr, "connect error:%s %s:%d\n", strerror(errno),ip_str,port);
					close(fd);
					return -1;
				}
			}
			#endif
		} 
		close(fd);
		fprintf(stderr, "connect failed, error:%s.\n", strerror(errno));
		return -1;
	} 

	connect_success:

	printf("Connect success %s:%d\n",ip_str,port);
	
	*psockfd = fd;
	
	return 0;
}
static int connect_with_timeout_ifrname(char *ifrname,char* ip_str, int port, int t_timeout, int* psockfd )
{
	int fd = 0;
	struct sockaddr_in	addr;
	fd_set fdr, fdw;
	struct timeval timeout;
	int err = 0;
	int errlen = sizeof(err);
	

	fd = socket(AF_INET,SOCK_STREAM,0);
	
	if (fd < 0) 
	{
		printf("create socket failed\n");
		return -1;
	}

	int m_loop = 1;
	if( setsockopt(fd, SOL_SOCKET, SO_REUSEADDR, &m_loop, sizeof(m_loop)) == -1 )
	{
		eprintf("sockfd =%d, Setsockopt SOL_SOCKET::SO_REUSEADDR Failure!\n", fd);
		return -1;
	}
	#if 1
	struct sockaddr_in	sin;
	struct ifreq ifr;
	//char sip_net_sel[5];
	bzero(&sin, sizeof(sin));	
	sin.sin_family = AF_INET;
	sin.sin_addr.s_addr = INADDR_ANY;
	sin.sin_port = htons(port);
	memset(ifr.ifr_name, 0, IFNAMSIZ);
	//API_Event_IoServer_InnerRead_All(SIP_NetworkSetting, sip_net_sel);
	//strcpy(sip_net_sel,"0");
	
	#endif
	strcpy(ifr.ifr_name,ifrname);
	//ifrname
	if(setsockopt(fd, SOL_SOCKET, SO_BINDTODEVICE, (char *)&ifr, sizeof(struct ifreq)))
	{
		eprintf("Setsockopt SOL_SOCKET::SO_BINDTODEVICE Failure!\n");
		close(fd);
		return -1;
	}
	
	
	bzero(&addr, sizeof(addr));
	addr.sin_family = AF_INET;
	addr.sin_port = htons(port);
	inet_pton(AF_INET, ip_str, &addr.sin_addr);
	//addr.sin_addr.s_addr = inet_addr(ip_str);

	/*设置套接字为非阻塞*/
	int flags = fcntl(fd, F_GETFL, 0);
	if (flags < 0) 
	{
		fprintf(stderr, "Get flags error:%s\n", strerror(errno));
		close(fd);
		return -1;
	}
	flags |= O_NONBLOCK;
	if (fcntl(fd, F_SETFL, flags) < 0) 
	{
		fprintf(stderr, "Set flags error:%s\n", strerror(errno));
		close(fd);
		return -1;
	}
	
	/*阻塞情况下linux系统默认超时时间为75s*/
	int rc = connect(fd, (struct sockaddr*)&addr, sizeof(addr));
	if (rc != 0) 
	{
		if (errno == EINPROGRESS) 
		{
			printf("Doing connection.\n");
			/*正在处理连接*/
			FD_ZERO(&fdr);
			FD_ZERO(&fdw);
			FD_SET(fd, &fdr);
			FD_SET(fd, &fdw);
			timeout.tv_sec = t_timeout;
			timeout.tv_usec = 0;
			rc = select(fd + 1, &fdr, &fdw, NULL, &timeout);
			printf("rc is: %d\n", rc);
			/*select调用失败*/
			if (rc < 0) 
			{
				fprintf(stderr, "connect error:%s\n", strerror(errno));
				close(fd);
				return -1;
			}
		
			/*连接超时*/
			if (rc == 0) 
			{
				fprintf(stderr, "Connect timeout.\n");
				close(fd);
				return -1;
			}
			/*[1] 当连接成功建立时，描述符变成可写,rc=1*/
			if (rc == 1 && FD_ISSET(fd, &fdw)) 
			{
				goto connect_success;
			}
			//usleep(100*1000);
			//goto connect_success;
			/*[2] 当连接建立遇到错误时，描述符变为即可读，也可写，rc=2 遇到这种情况，可调用getsockopt函数*/
			#if 1
			if (rc == 2) 
			{
				if (getsockopt(fd, SOL_SOCKET, SO_ERROR, &err, &errlen) == -1) 
				{
					fprintf(stderr, "getsockopt(SO_ERROR): %s", strerror(errno));
					close(fd);
					return -1;
				}

				if (err) 
				{
					errno = err;
					fprintf(stderr, "connect error:%s %s:%d\n", strerror(errno),ip_str,port);
					close(fd);
					return -1;
				}
			}
			#endif
		} 
		close(fd);
		fprintf(stderr, "connect failed, error:%s.\n", strerror(errno));
		return -1;
	} 

	connect_success:

	printf("Connect success %s:%d\n",ip_str,port);
	
	*psockfd = fd;
	
	return 0;
}
int check_connect_to_server(char *ifrname, char* ip_str)
{
	int socket_fd, err, num, recv_len;

	// connect
	err = connect_with_timeout_ifrname(ifrname,ip_str, VTK_AUTO_REG_SERVER_PORT, 8, &socket_fd);
	
	if(err == 0)
	{
		close( socket_fd );		
		printf("server [%s]connect ok\n",ip_str);
		return 0;
	}
	else
	{
		printf("server [%s]connect error\n",ip_str);
	}		
	return -1;
}
/*
ip_str:		server ip addresss string
port:		server port
psend_buf:	send data buffer
send_len:		send data buffer length
precv_buf:	receive data buffer
precv_len:	receive data buffer length

return:		0/ok, 1/connect err, 2/write error, 3/receive error
*/
int connect_send_to_server(char* ip_str, int port, char* psend_buf, int send_len, char* precv_buf, int* precv_len )
{
	static int connect_times = 0;
	int return_value = 0;
	int socket_fd, err, num, recv_len;

	// connect
	err = connect_with_timeout(ip_str, port, 3, &socket_fd);
	
	if(err == 0)
	{
		// 设置套接字为阻塞方式
		int flags = fcntl(socket_fd, F_GETFL, 0);
		if (flags < 0) 
		{
			printf("Get flags error:%s\n", strerror(errno));
			close(socket_fd);
			return -1;
		}
		flags &= ~O_NONBLOCK;
		if (fcntl(socket_fd, F_SETFL, flags) < 0) 
		{
			printf("Set flags error:%s\n", strerror(errno));
			close(socket_fd);
			return -1;
		}
		
 		struct timeval tv_out;
    	tv_out.tv_sec = 3;
    	tv_out.tv_usec = 0;
    	setsockopt(socket_fd, SOL_SOCKET, SO_RCVTIMEO, &tv_out, sizeof(tv_out));
		
		printf("connect %d times ok! [server:%s]\n",++connect_times,ip_str);
	
		// send
		num = write(socket_fd, (char *)psend_buf, send_len);

		
		if (num <= 0) {
			printf("send error\n");
			return 2;
		}

		// recv
		recv_len = *precv_len;
		if( precv_buf == NULL || recv_len == 0 )
		{
			return_value = 0;	
			printf("send over\n");			
		}
		else
		{
			num = recv(socket_fd, precv_buf, recv_len, 0);


			if (num > 0) // success
			{
				*precv_len = num;
				return_value = 0;
				printf("send and recv over\n");			
			}
			else
			{
				return_value = 3;
				printf("recv error\n");
			}
		}
		usleep(10*1000);
		close( socket_fd );		
	}
	else
	{
		printf("connect error\n");
		return_value = 1;
	}		
	return return_value;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////

/*
host_name:		主机名称，如: 2easyip.com, 2easyip.cn
ip:				IP地址，网络字节序
ip_str:			IP地址字符串

return:			-1/err, 0/ok
*/
typedef struct
{
	struct hostent h;
	char hostName[65];
	int flag;		// 0: unstart, 1:ok, -1:error
} GetHostDat_t;

pthread_t task_gethostbyname;
pthread_attr_t attr_gethostbyname;
GetHostDat_t host;

int task_gethostbyname_process(void)
{
#if 1
	struct hostent* h;
	
	h = gethostbyname(host.hostName);
	
	if(h != NULL)
	{
		host.h = *h;
		host.flag = 1;
	}
	else
	{
		host.flag = -1;
	}
#endif	
	return 0;
}


// 0/ok, -1/err
int parse_remote_server( const char* host_name, int* ip, char* ip_str  )
{
#if 1
	struct in_addr sin_addr;
	char * ipaddr;
	GetHostDat_t	task_GetHost_dat;
	int timeCnt = 0;
	struct hostent* h;

	memset(&host, 0, sizeof(host));
	host.flag = 0;
	memcpy(host.hostName, host_name, strlen(host_name) > 64 ? 64 : strlen(host_name));

	// lzh_20201110_s
	char* ptr = strstr(host.hostName,":");
	if( ptr != NULL ) ptr[0] = '\0';
	// lzh_20201110_e
	
	pthread_attr_init(&attr_gethostbyname);
	// lzh_20181016_s
	pthread_attr_setdetachstate(&attr_gethostbyname,PTHREAD_CREATE_DETACHED); 	//  *** 从主线程分离，防止内存泄漏
	// lzh_20181016_e	
	if( pthread_create(&task_gethostbyname, &attr_gethostbyname,(void*)task_gethostbyname_process, NULL ) != 0 )
	{
		printf( "Create task_gethostbyname pthread error! \n" );
		return -1;
	}
	
	while(!host.flag)
	{
		usleep(100*1000);
		if(++timeCnt >= 30)
		{
			timeCnt = 0;
			pthread_cancel(task_gethostbyname);
			break;
		}
	}

	if(host.flag == 1)
	{
		h = &host.h;
	}
	else
	{
		h = NULL;
		printf("parse_remote_server error\n");
	}
		
	if (h == NULL )
	{
		if( ip != NULL )
		{
			*ip = ntohl( 0x7F000001L);
		}
		return -1;
	}
	else
	{
		sin_addr = *(struct in_addr*)h->h_addr;
		if( ip != NULL )
		{
			*ip = ntohl( sin_addr.s_addr );
		}
		if( ip_str != NULL )
		{
			ipaddr = inet_ntoa(sin_addr);
			//printf("last ip:%s host_name:%s, ip:%s\n", ip_str, host_name, ipaddr);
			// lzh_20201110_s
			//strcpy(ip_str,ipaddr);
			char* port = strstr(host_name,":");
			if( port == NULL ) 
				strcpy(ip_str,ipaddr);
			else
				sprintf(ip_str,"%s%s",ipaddr,port);
			printf("host_name:%s, ip_str:%s\n", host_name, ip_str);
			// lzh_20201110_e
		}
		return 0;
	}
#endif	
}


/*
cmd:				command
server_ip:		server
sip_account:		sip account string
sip_psw:			sip account password
sip_psw2:			sip account password2 (new password)

return:			-1/net err, 0/reg ok, 1/already reg, 2/package err
*/
#if 0

int remote_account_manage( int cmd, char* server_ip, char* sip_account, char* sip_psw, char* sip_psw2 )
{	
#if 1
	SERVICE_PACK_INFO one_pack_req;
	SERVICE_PACK_INFO one_pack_rsp;

	char	ip_str[100];
	int send_len,recv_len;
	int ret_value;

	if(server_ip[0] == 0 || server_ip == NULL)
	{
		return -1;
	}
		
	switch( cmd )
	{
		// old commands:
		case ACCOUNT_MANAGE_CMD_REGISTER:
		case ACCOUNT_MANAGE_CMD_CLEARUL:
			one_pack_req.auto_reg_req.cmd			= cmd;
			strncpy( one_pack_req.auto_reg_req.sip_account, sip_account, 16 );
			one_pack_req.auto_reg_req.sip_password 	= atoi(sip_psw);
			one_pack_req.auto_reg_req.state			= 0;
			send_len = sizeof(AUTOREG_REQ);

			// prepare for response
			recv_len = sizeof(AUTOREG_RSP);			
			memset( &one_pack_rsp, 0, sizeof(one_pack_rsp) );

			// 先进行域名转换检测，得到最终的ip地址，若转换失败，则使用缺省的ip地址: 47.91.88.33
			if( parse_remote_server(server_ip, NULL, ip_str ) < 0 )
			{
				printf("remote_account_manage[%s],parse_remote_server err!\n",server_ip);
				return -1;
			}
			ret_value = connect_send_to_server(ip_str, VTK_AUTO_REG_SERVER_PORT, (char*)&one_pack_req.auto_reg_req, send_len, (char*)&one_pack_rsp.auto_reg_rsp, &recv_len );
			
			printf("remote_account_manage[%s][%s],cmd=%d,ret=%d,state=%d\n",server_ip,ip_str,cmd,ret_value,one_pack_rsp.auto_reg_rsp.state);
			
			if( ret_value == 0 )
			{
				if( one_pack_rsp.auto_reg_rsp.state == 0 )
					return 0;
				else
					return 1;			
			}
			else
				return -1;
						
			break;

		// new commands:
		case ACCOUNT_MANAGE_CMD_UNREGISTER:
			one_pack_req.unregister_req.cmd		= cmd;
			strncpy( one_pack_req.unregister_req.sip_account, sip_account, 20 );
			strncpy( one_pack_req.unregister_req.sip_password, sip_psw, 16 );
			one_pack_req.unregister_req.state	= 0;
			one_pack_req.unregister_req.checksum			= calculate_checksum((unsigned char*)&one_pack_req.unregister_req,sizeof(UNREGISTER_REQ)-4);
			send_len = sizeof(UNREGISTER_REQ);

			// prepare for response
			recv_len = sizeof(UNREGISTER_RSP);			
			memset( &one_pack_rsp, 0, sizeof(UNREGISTER_RSP) );

			// 先进行域名转换检测，得到最终的ip地址，若转换失败，则使用缺省的ip地址: 47.91.88.33
			if( parse_remote_server(server_ip, NULL, ip_str ) < 0 )
			{
				printf("remote_account_manage[%s],parse_remote_server err!\n",server_ip);
				return -1;
			}
			ret_value = connect_send_to_server(ip_str, VTK_AUTO_REG_SERVER_PORT, (char*)&one_pack_req.unregister_req, send_len, (char*)&one_pack_rsp.unregister_rsp, &recv_len );
			
			printf("remote_account_manage[%s][%s],cmd=%d,ret=%d,state=%d\n",server_ip,ip_str,cmd,ret_value,one_pack_rsp.unregister_rsp.state);
			
			if( ret_value )
			{
				ret_value =  -5;		
			}	
			/*
			// return：
			// 0：删除帐号ok
			// -1：数据包校验错误
			// -2：账号不存在
			// -3：账号已存在, 密码错误
			// -4：数据库操作无效
			*/			
			if( one_pack_rsp.unregister_rsp.checksum != calculate_checksum((unsigned char*)&one_pack_rsp.unregister_rsp,sizeof(UNREGISTER_RSP)-4) )
			{				
				ret_value =  -6;	
			}
			else
			{
				ret_value = one_pack_rsp.unregister_rsp.state;
			}
		
			return ret_value;
			
			break;
			
		case ACCOUNT_MANAGE_CMD_CHANGEPSW:
			// request initial
			one_pack_req.change_psw_req.cmd		= cmd;
			strncpy( one_pack_req.change_psw_req.sip_account, sip_account, 20 );
			strncpy( one_pack_req.change_psw_req.sip_password, sip_psw, 16 );
			strncpy( one_pack_req.change_psw_req.sip_new_password, sip_psw2, 16 );
			one_pack_req.change_psw_req.state	= 0;
			one_pack_req.change_psw_req.checksum = calculate_checksum((unsigned char*)&one_pack_req.change_psw_req,sizeof(CHANGE_PASSWORD_REQ)-4);
			send_len = sizeof(CHANGE_PASSWORD_REQ);
			
			// prepare for response
			recv_len = sizeof(CHANGE_PASSWORD_RSP);			
			memset( &one_pack_rsp, 0, sizeof(one_pack_rsp) );
			
			// 先进行域名转换检测，得到最终的ip地址，若转换失败，则使用缺省的ip地址: 47.91.88.33
			if( parse_remote_server(server_ip, NULL, ip_str ) < 0 )
			{
				printf("remote_account_manage[%s],parse_remote_server err!\n",server_ip);
				return -1;
			}
			ret_value = connect_send_to_server(ip_str, VTK_AUTO_REG_SERVER_PORT, (char*)&one_pack_req.change_psw_req, send_len, (char*)&one_pack_rsp.auto_reg_rsp, &recv_len );
			
			printf("remote_account_manage[%s][%s],cmd=%d,ret=%d,state=%d\n",server_ip,ip_str,cmd,ret_value,one_pack_rsp.auto_reg_rsp.state);
			
			if( ret_value )
			{
				ret_value =  -5;		
			}	
			/*
			// return：
			// 0：更改密码ok
			// -1：数据包校验错误
			// -2：账号不存在
			// -3：帐号密码错误
			// -4：数据库操作无效
			*/
			else if( one_pack_rsp.change_psw_rsq.checksum != calculate_checksum((unsigned char*)&one_pack_rsp.change_psw_rsq,sizeof(CHANGE_PASSWORD_RSP)-4) )
			{				
				ret_value =  -6;	
			}
			else
			{
				ret_value = one_pack_rsp.change_psw_rsq.state;
			}
		
			printf_change_divert_psw_result( server_ip, ret_value );
			
			return ret_value;
			
			break;
			
		case ACCOUNT_MANAGE_CMD_CLEARUL2:
			one_pack_req.clear_ul_req.cmd		= cmd;
			strncpy( one_pack_req.clear_ul_req.sip_account, sip_account, 20 );
			strncpy( one_pack_req.clear_ul_req.sip_password, sip_psw, 16 );
			one_pack_req.clear_ul_req.state	= 0;
			one_pack_req.clear_ul_req.checksum			= calculate_checksum((unsigned char*)&one_pack_req.clear_ul_req,sizeof(CLEAR_UL_REQ)-4);
			send_len = sizeof(CLEAR_UL_REQ);

			// prepare for response
			recv_len = sizeof(CLEAR_UL_RSP);			
			memset( &one_pack_rsp, 0, sizeof(one_pack_rsp) );

			// 先进行域名转换检测，得到最终的ip地址，若转换失败，则使用缺省的ip地址: 47.91.88.33
			if( parse_remote_server(server_ip, NULL, ip_str ) < 0 )
			{
				printf("remote_account_manage[%s],parse_remote_server err!\n",server_ip);
				return -1;
			}
			ret_value = connect_send_to_server(ip_str, VTK_AUTO_REG_SERVER_PORT, (char*)&one_pack_req.clear_ul_req, send_len, (char*)&one_pack_rsp.clear_ul_rsp, &recv_len );
			
			printf("remote_account_manage[%s][%s],cmd=%d,ret=%d,state=%d\n",server_ip,ip_str,cmd,ret_value,one_pack_rsp.clear_ul_rsp.state);
			
			if( ret_value )
			{
				ret_value =  -5;		
			}	
			/*
			// return：
			// 0-x：清除UL个数
			// -1：数据包校验错误
			// -2：账号不存在
			// -3：账号已存在, 密码错误
			// -4：数据库操作无效
			*/			
			if( one_pack_rsp.clear_ul_rsp.checksum != calculate_checksum((unsigned char*)&one_pack_rsp.clear_ul_rsp,sizeof(CLEAR_UL_RSP)-4) )
			{				
				ret_value =  -6;	
			}
			else
			{
				ret_value = one_pack_rsp.clear_ul_rsp.state;
			}

		
			printf_clear_url_result( server_ip, ret_value );
			
			return ret_value;
			
			
			break;

		case ACCOUNT_MANAGE_CMD_RESTORE_DEFAULT:	
			one_pack_req.restore_default_req.cmd		= cmd;
			strncpy( one_pack_req.restore_default_req.sip_account, sip_account, 20 );
			strncpy( one_pack_req.restore_default_req.sip_password, sip_psw, 16 );
			one_pack_req.restore_default_req.state	= 0;
			one_pack_req.restore_default_req.checksum			= calculate_checksum((unsigned char*)&one_pack_req.restore_default_req,sizeof(RESTORE_DEFAULT_REQ)-4);
			send_len = sizeof(RESTORE_DEFAULT_REQ);

			// prepare for response
			recv_len = sizeof(RESTORE_DEFAULT_RSP);			
			memset( &one_pack_rsp, 0, sizeof(one_pack_rsp) );

			// 先进行域名转换检测，得到最终的ip地址，若转换失败，则使用缺省的ip地址: 47.91.88.33
			if( parse_remote_server(server_ip, NULL, ip_str ) < 0 )
			{
				printf("remote_account_manage[%s],parse_remote_server err!\n",server_ip);
				return -1;
			}
			ret_value = connect_send_to_server(ip_str, VTK_AUTO_REG_SERVER_PORT, (char*)&one_pack_req.restore_default_req, send_len, (char*)&one_pack_rsp.restore_default_rsp, &recv_len );
			
			printf("remote_account_manage[%s][%s],cmd=%d,ret=%d,state=%d\n",server_ip,ip_str,cmd,ret_value,one_pack_rsp.restore_default_rsp.state);
			
			if( ret_value )
			{
				ret_value =  -5;		
			}	
			/*
			// return：
			// 0：账号已存在，恢复了缺省密码
			// 1：账号不存在，创建了新账号
			// -1：数据包校验错误
			// -2：账号已存在, 已为缺省密码
			// -3：数据库操作无效			
			*/
			if( one_pack_rsp.restore_default_rsp.checksum != calculate_checksum((unsigned char*)&one_pack_rsp.restore_default_rsp,sizeof(RESTORE_DEFAULT_RSP)-4) )
			{				
				ret_value =  -6;	
			}
			else
			{
				ret_value = one_pack_rsp.restore_default_rsp.state;
			}

			printf_restore_default_psw_result( server_ip, ret_value );
		
			return ret_value;
			
			break;
	}
#endif	
}

#else

// lzh_20201110_s
int remote_account_manage( int cmd, char* server_ip, char* sip_account, char* sip_psw, char* sip_psw2 )
{	
	SERVICE_PACK_INFO one_pack_req;
	SERVICE_PACK_INFO one_pack_rsp;

	char	ip_str[100];
	int send_len,recv_len;
	int ret_value;

	if(server_ip[0] == 0 || server_ip == NULL)
	{
		return -1;
	}

	char server_ip_tmp[100];
	memset(server_ip_tmp,0,100);
	strcpy(server_ip_tmp,server_ip);
	char* ptr = strstr(server_ip_tmp,":");
	if( ptr != NULL ) ptr[0] = '\0';
	
	switch( cmd )
	{
		// old commands:
		case ACCOUNT_MANAGE_CMD_REGISTER:
		case ACCOUNT_MANAGE_CMD_CLEARUL:
			one_pack_req.auto_reg_req.cmd			= cmd;
			strncpy( one_pack_req.auto_reg_req.sip_account, sip_account, 16 );
			one_pack_req.auto_reg_req.sip_password 	= atoi(sip_psw);
			one_pack_req.auto_reg_req.state			= 0;
			send_len = sizeof(AUTOREG_REQ);

			// prepare for response
			recv_len = sizeof(AUTOREG_RSP);			
			memset( &one_pack_rsp, 0, sizeof(one_pack_rsp) );

			// 先进行域名转换检测，得到最终的ip地址，若转换失败，则使用缺省的ip地址: 47.91.88.33
			if( parse_remote_server(server_ip_tmp, NULL, ip_str ) < 0 )
			{
				printf("remote_account_manage[%s],parse_remote_server err!\n",server_ip_tmp);
				return -1;
			}
			ret_value = connect_send_to_server( ip_str, VTK_AUTO_REG_SERVER_PORT, (char*)&one_pack_req.auto_reg_req, send_len, (char*)&one_pack_rsp.auto_reg_rsp, &recv_len );
			
			printf("remote_account_manage[%s][%s],cmd=%d,ret=%d,state=%d\n",server_ip_tmp,ip_str,cmd,ret_value,one_pack_rsp.auto_reg_rsp.state);
			
			if( ret_value == 0 )
			{
				if( one_pack_rsp.auto_reg_rsp.state == 0 )
					return 0;
				else
					return 1;			
			}
			else
				return -1;
						
			break;

		// new commands:
		case ACCOUNT_MANAGE_CMD_UNREGISTER:
			one_pack_req.unregister_req.cmd		= cmd;
			strncpy( one_pack_req.unregister_req.sip_account, sip_account, 20 );
			strncpy( one_pack_req.unregister_req.sip_password, sip_psw, 16 );
			one_pack_req.unregister_req.state	= 0;
			one_pack_req.unregister_req.checksum			= calculate_checksum((unsigned char*)&one_pack_req.unregister_req,sizeof(UNREGISTER_REQ)-4);
			send_len = sizeof(UNREGISTER_REQ);

			// prepare for response
			recv_len = sizeof(UNREGISTER_RSP);			
			memset( &one_pack_rsp, 0, sizeof(UNREGISTER_RSP) );

			// 先进行域名转换检测，得到最终的ip地址，若转换失败，则使用缺省的ip地址: 47.91.88.33
			if( parse_remote_server(server_ip_tmp, NULL, ip_str ) < 0 )
			{
				printf("remote_account_manage[%s],parse_remote_server err!\n",server_ip);
				return -1;
			}
			ret_value = connect_send_to_server( ip_str, VTK_AUTO_REG_SERVER_PORT, (char*)&one_pack_req.unregister_req, send_len, (char*)&one_pack_rsp.unregister_rsp, &recv_len );
			
			printf("remote_account_manage[%s][%s],cmd=%d,ret=%d,state=%d\n",server_ip_tmp,ip_str,cmd,ret_value,one_pack_rsp.unregister_rsp.state);
			
			if( ret_value )
			{
				ret_value =  -5;		
			}	
			/*
			// return：
			// 0：删除帐号ok
			// -1：数据包校验错误
			// -2：账号不存在
			// -3：账号已存在, 密码错误
			// -4：数据库操作无效
			*/			
			if( one_pack_rsp.unregister_rsp.checksum != calculate_checksum((unsigned char*)&one_pack_rsp.unregister_rsp,sizeof(UNREGISTER_RSP)-4) )
			{				
				ret_value =  -6;	
			}
			else
			{
				ret_value = one_pack_rsp.unregister_rsp.state;
			}
		
			return ret_value;
			
			break;
			
		case ACCOUNT_MANAGE_CMD_CHANGEPSW:
			// request initial
			one_pack_req.change_psw_req.cmd		= cmd;
			strncpy( one_pack_req.change_psw_req.sip_account, sip_account, 20 );
			strncpy( one_pack_req.change_psw_req.sip_password, sip_psw, 16 );
			strncpy( one_pack_req.change_psw_req.sip_new_password, sip_psw2, 16 );
			one_pack_req.change_psw_req.state	= 0;
			one_pack_req.change_psw_req.checksum = calculate_checksum((unsigned char*)&one_pack_req.change_psw_req,sizeof(CHANGE_PASSWORD_REQ)-4);
			send_len = sizeof(CHANGE_PASSWORD_REQ);
			
			// prepare for response
			recv_len = sizeof(CHANGE_PASSWORD_RSP);			
			memset( &one_pack_rsp, 0, sizeof(one_pack_rsp) );
			
			// 先进行域名转换检测，得到最终的ip地址，若转换失败，则使用缺省的ip地址: 47.91.88.33
			if( parse_remote_server(server_ip_tmp, NULL, ip_str ) < 0 )
			{
				printf("remote_account_manage[%s],parse_remote_server err!\n",server_ip_tmp);
				return -1;
			}
			ret_value = connect_send_to_server( ip_str, VTK_AUTO_REG_SERVER_PORT, (char*)&one_pack_req.change_psw_req, send_len, (char*)&one_pack_rsp.auto_reg_rsp, &recv_len );
			
			printf("remote_account_manage[%s][%s],cmd=%d,ret=%d,state=%d\n",server_ip_tmp,ip_str,cmd,ret_value,one_pack_rsp.auto_reg_rsp.state);
			
			if( ret_value )
			{
				ret_value =  -5;		
			}	
			/*
			// return：
			// 0：更改密码ok
			// -1：数据包校验错误
			// -2：账号不存在
			// -3：帐号密码错误
			// -4：数据库操作无效
			*/
			else if( one_pack_rsp.change_psw_rsq.checksum != calculate_checksum((unsigned char*)&one_pack_rsp.change_psw_rsq,sizeof(CHANGE_PASSWORD_RSP)-4) )
			{				
				ret_value =  -6;	
			}
			else
			{
				ret_value = one_pack_rsp.change_psw_rsq.state;
			}
		
			printf_change_divert_psw_result( server_ip_tmp, ret_value );
			
			return ret_value;
			
			break;
			
		case ACCOUNT_MANAGE_CMD_CLEARUL2:
			one_pack_req.clear_ul_req.cmd		= cmd;
			strncpy( one_pack_req.clear_ul_req.sip_account, sip_account, 20 );
			strncpy( one_pack_req.clear_ul_req.sip_password, sip_psw, 16 );
			one_pack_req.clear_ul_req.state	= 0;
			one_pack_req.clear_ul_req.checksum			= calculate_checksum((unsigned char*)&one_pack_req.clear_ul_req,sizeof(CLEAR_UL_REQ)-4);
			send_len = sizeof(CLEAR_UL_REQ);

			// prepare for response
			recv_len = sizeof(CLEAR_UL_RSP);			
			memset( &one_pack_rsp, 0, sizeof(one_pack_rsp) );

			// 先进行域名转换检测，得到最终的ip地址，若转换失败，则使用缺省的ip地址: 47.91.88.33
			if( parse_remote_server(server_ip_tmp, NULL, ip_str ) < 0 )
			{
				printf("remote_account_manage[%s],parse_remote_server err!\n",server_ip_tmp);
				return -1;
			}
			ret_value = connect_send_to_server( ip_str, VTK_AUTO_REG_SERVER_PORT, (char*)&one_pack_req.clear_ul_req, send_len, (char*)&one_pack_rsp.clear_ul_rsp, &recv_len );
			
			printf("remote_account_manage[%s][%s],cmd=%d,ret=%d,state=%d\n",server_ip_tmp,ip_str,cmd,ret_value,one_pack_rsp.clear_ul_rsp.state);
			
			if( ret_value )
			{
				ret_value =  -5;		
			}	
			/*
			// return：
			// 0-x：清除UL个数
			// -1：数据包校验错误
			// -2：账号不存在
			// -3：账号已存在, 密码错误
			// -4：数据库操作无效
			*/			
			if( one_pack_rsp.clear_ul_rsp.checksum != calculate_checksum((unsigned char*)&one_pack_rsp.clear_ul_rsp,sizeof(CLEAR_UL_RSP)-4) )
			{				
				ret_value =  -6;	
			}
			else
			{
				ret_value = one_pack_rsp.clear_ul_rsp.state;
			}

		
			printf_clear_url_result( server_ip_tmp, ret_value );
			
			return ret_value;
			
			
			break;

		case ACCOUNT_MANAGE_CMD_RESTORE_DEFAULT:	
			one_pack_req.restore_default_req.cmd		= cmd;
			strncpy( one_pack_req.restore_default_req.sip_account, sip_account, 20 );
			strncpy( one_pack_req.restore_default_req.sip_password, sip_psw, 16 );
			one_pack_req.restore_default_req.state	= 0;
			one_pack_req.restore_default_req.checksum			= calculate_checksum((unsigned char*)&one_pack_req.restore_default_req,sizeof(RESTORE_DEFAULT_REQ)-4);
			send_len = sizeof(RESTORE_DEFAULT_REQ);

			// prepare for response
			recv_len = sizeof(RESTORE_DEFAULT_RSP);			
			memset( &one_pack_rsp, 0, sizeof(one_pack_rsp) );

			// 先进行域名转换检测，得到最终的ip地址，若转换失败，则使用缺省的ip地址: 47.91.88.33
			if( parse_remote_server(server_ip_tmp, NULL, ip_str ) < 0 )
			{
				printf("remote_account_manage[%s],parse_remote_server err!\n",server_ip_tmp);
				return -1;
			}
			ret_value = connect_send_to_server( ip_str, VTK_AUTO_REG_SERVER_PORT, (char*)&one_pack_req.restore_default_req, send_len, (char*)&one_pack_rsp.restore_default_rsp, &recv_len );
			
			printf("remote_account_manage[%s][%s],cmd=%d,ret=%d,state=%d\n",server_ip_tmp,ip_str,cmd,ret_value,one_pack_rsp.restore_default_rsp.state);
			
			if( ret_value )
			{
				ret_value =  -5;	
				return ret_value;
			}	
			/*
			// return：
			// 0：账号已存在，恢复了缺省密码
			// 1：账号不存在，创建了新账号
			// -1：数据包校验错误
			// -2：账号已存在, 已为缺省密码
			// -3：数据库操作无效			
			*/
			if( one_pack_rsp.restore_default_rsp.checksum != calculate_checksum((unsigned char*)&one_pack_rsp.restore_default_rsp,sizeof(RESTORE_DEFAULT_RSP)-4) )
			{				
				ret_value =  -6;	
			}
			else
			{
				ret_value = one_pack_rsp.restore_default_rsp.state;
			}

			printf_restore_default_psw_result( server_ip_tmp, ret_value );
		
			return ret_value;
			
			break;
		case ACCOUNT_MANAGE_CMD_APPLY_NEW_ACCOUNT:	
			one_pack_req.apply_new_account_req.cmd		= cmd;
			strncpy( one_pack_req.apply_new_account_req.apply_base, sip_account, 20 );
			one_pack_req.apply_new_account_req.apply_id =  rand();
			one_pack_req.apply_new_account_req.state	= 0;
			one_pack_req.apply_new_account_req.checksum			= calculate_checksum((unsigned char*)&one_pack_req.apply_new_account_req,sizeof(APPLY_NEW_ACCOUNT_REQ)-4);
			send_len = sizeof(APPLY_NEW_ACCOUNT_REQ);

			// prepare for response
			recv_len = sizeof(APPLY_NEW_ACCOUNT_RSP);			
			memset( &one_pack_rsp, 0, sizeof(one_pack_rsp) );

			// 先进行域名转换检测，得到最终的ip地址，若转换失败，则使用缺省的ip地址: 47.91.88.33
			if( parse_remote_server(server_ip_tmp, NULL, ip_str ) < 0 )
			{
				printf("remote_account_manage[%s],parse_remote_server err!\n",server_ip_tmp);
				return -1;
			}
			ret_value = connect_send_to_server( ip_str, VTK_AUTO_REG_SERVER_PORT, (char*)&one_pack_req.apply_new_account_req, send_len, (char*)&one_pack_rsp.apply_new_account_rsp, &recv_len );
			
			printf("remote_account_manage[%s][%s],cmd=%d,ret=%d,state=%d:len:%d:%d,acc:%s:%s:%s\n",server_ip_tmp,ip_str,cmd,ret_value,one_pack_rsp.apply_new_account_rsp.state,sizeof(APPLY_NEW_ACCOUNT_RSP),recv_len,one_pack_req.apply_new_account_req.apply_base,one_pack_rsp.apply_new_account_rsp.apply_base,one_pack_rsp.apply_new_account_rsp.sip_account);
			
			if( ret_value!=0 )
			{
				ret_value =  -5;		
			}
			else
			{
				/*
				// return：
				// 0：账号已存在，恢复了缺省密码
				// 1：账号不存在，创建了新账号
				// -1：数据包校验错误
				// -2：账号已存在, 已为缺省密码
				// -3：数据库操作无效			
				*/
				if( one_pack_rsp.apply_new_account_rsp.checksum != calculate_checksum((unsigned char*)&one_pack_rsp.apply_new_account_rsp,sizeof(APPLY_NEW_ACCOUNT_RSP)-4) )
				{				
					ret_value =  -6;	
				}
				else
				{
					ret_value = one_pack_rsp.apply_new_account_rsp.state;
				}
				if(ret_value == 0&&sip_psw!=NULL)
				{
					strcpy(sip_psw,one_pack_rsp.apply_new_account_rsp.sip_account);
				}
				//printf_restore_default_psw_result( server_ip, ret_value );
			}
			return ret_value;
			
			break;	
	}
}
// lzh_20201110_e

#endif

/*
// 0：更改密码ok
// -1：数据包校验错误
// -2：账号不存在
// -3：帐号密码错误
// -4：数据库操作无效
// -5：不能连接到服务器
// -6：服务器返回的数据包checksum错误	
*/
void printf_change_divert_psw_result( char* sip_server, int result )
{
	switch( result )
	{
		case 0:
			printf("change password[%s],modified current account password ok!\n",sip_server);				
			break;
		case -1:
			printf("change password[%s],server notified that request package checksum err!\n",sip_server);
			break;			
		case -2:
			printf("change password[%s],current account just not exist, CANNOT modify password!\n",sip_server);				
			break;
		case -3:
			printf("change password[%s],current account password error, CANNOT modify password!\n",sip_server);				
			break;			
		case -4:
			printf("change password[%s],database operating err!\n",sip_server);				
			break;			
		case -5:
			printf("change password[%s],CANNOT connect to server!\n",sip_server);				
			break;
		case -6:
			printf("change password[%s],received package checksum err!\n",sip_server); 			
			break;
	}	
}

/*
// 0-x：清除UL个数
// -1：数据包校验错误
// -2：账号不存在
// -3：账号已存在, 密码错误
// -4：数据库操作无效
// -5：不能连接到服务器
// -6：服务器返回的数据包checksum错误	
*/			
void printf_clear_url_result( char* sip_server, int result )
{
	switch( result )
	{
		case -1:
			printf("clear url[%s],server notified that request package checksum err!\n",sip_server);
			break;			
		case -2:
			printf("clear url[%s],current account just not exist, CANNOT clear location!\n",sip_server);	
			break;
		case -3:
			printf("clear url[%s],current account password error, CANNOT clear location!\n",sip_server);				
			break;			
		case -4:
			printf("clear url[%s],database operating err!\n",sip_server);				
			break;			
		case -5:
			printf("clear url[%s],CANNOT connect to server!\n",sip_server);				
			break;
		case -6:
			printf("clear url[%s],received package checksum err!\n",sip_server); 			
			break;
		default:
			printf("clear url[%s],clear locations[%d] ok!\n",sip_server,result); 			
			break;
	}	
}

/*
// 0：账号已存在，恢复了缺省密码
// 1：账号不存在，创建了新账号
// -1：数据包校验错误
// -2：账号已存在, 已为缺省密码
// -3：数据库操作无效	
// -4：不能连接到服务器
// -5：服务器返回的数据包checksum错误	
*/
void printf_restore_default_psw_result( char* sip_server, int result )
{
	switch( result )
	{
		case 0:
			printf("restore_default_psw[%s],account exist, restore account password ok!\n",sip_server);
			break;	
		case 1:
			printf("restore_default_psw[%s],account NOT exist, CREATE one new account ok!\n",sip_server);
			break;
		case -1:
			printf("restore_default_psw[%s],server notified that request package checksum err!\n",sip_server);
			break;			
		case -2:
			printf("restore_default_psw[%s],account exist, ALREADY default password!\n",sip_server);	
			break;
		case -3:
			printf("restore_default_psw[%s],database operating err!\n",sip_server);				
			break;			
		case -4:
			printf("restore_default_psw[%s],CANNOT connect to server!\n",sip_server); 			
			break;
		case -5:
			printf("restore_default_psw[%s],received package checksum err!\n",sip_server);			
			break;
	}	
}

