#include "obj_lan_manager.h"
//#include "task_IoServer.h"
#include "define_file.h"
#include "unix_socket.h"
#include "obj_autoip_client.h"
#include "cJSON.h"
#include "obj_IoInterface.h"
extern cJSON* API_Get_Wlan_State(void);
extern cJSON* API_Get_Lan_State(void);
cJSON *GetNmAllState(void)
{
	static cJSON *all_state=NULL;
	int i;
	cJSON *one_ins;
	
	char buff[50];
	
	if(all_state!=NULL)
		cJSON_Delete(all_state);
	
	all_state=cJSON_CreateObject();
	if(all_state==NULL)
		return NULL;
	one_ins=API_Get_Wlan_State();
	if(one_ins!=NULL)
		cJSON_AddItemReferenceToObject(all_state,"Wlan",one_ins);
	
	one_ins=API_Get_Lan_State();
	if(one_ins!=NULL)
		cJSON_AddItemReferenceToObject(all_state,"Wlan",one_ins);
	
	
	return all_state;
}

int ShellCmd_Wlan(cJSON *cmd)
{
	char *para=GetShellCmdInputString(cmd, 1);
	cJSON *jpara;
	
	if(para==NULL)
	{
		ShellCmdPrintf("hava no para");
		return 1;
	}
	if(strcmp(para,"state")==0)
	{	
		jpara=API_Get_Wlan_State();
		char* temp;
		temp=cJSON_Print(jpara);
		if(temp==NULL)
		{
			ShellCmdPrintf("Get State Fail");
			return 1;
		}
		ShellCmdPrintf(temp);
		free(temp);
	}
	else if(strcmp(para,"open")==0)
	{
		int callscene;
		update_shell_op_time();
		
		para=GetShellCmdInputString(cmd, 2);
		
		if(para==NULL)
		{
			ShellCmdPrintf("open wlan by io-para");
			API_Wlan_Open(NULL);
			return 0;
		}
			
		jpara=cJSON_Parse(para);
		if(jpara!=NULL)
		{
			API_Wlan_Open(jpara);
			ShellCmdPrintf("open wlan by appoint-para");
			cJSON_Delete(jpara);
		}
		else
		{
			ShellCmdPrintf("json para is err");
		}
	}
	else if(strcmp(para,"search")==0)
	{
		update_shell_op_time();
		API_Wlan_Search();
	}
	else if(strcmp(para,"get_ssid")==0)
	{
		jpara=get_wlan_ssid();
		if(jpara==NULL)
		{
			ShellCmdPrintf("have no ssid para");
			return 0;
		}
		char* temp;
		temp=cJSON_Print(jpara);
		if(temp==NULL)
		{
			ShellCmdPrintf("Get ssid Fail");
			return 1;
		}
		ShellCmdPrintf(temp);
		free(temp);	
	}
	else if(strcmp(para,"save_ssid")==0)
	{
		jpara=get_wlan_ssid();
		if(jpara==NULL||!cJSON_IsArray(jpara))
		{
			ShellCmdPrintf("have no ssid para");
			return 0;
		}
		char* temp;
		temp=cJSON_Print(jpara);
		if(temp==NULL)
		{
			ShellCmdPrintf("Get ssid Fail");
			return 1;
		}
		ShellCmdPrintf(temp);
		free(temp);
		cJSON *ssid_para;
		ssid_para=cJSON_CreateObject();
		cJSON_AddItemReferenceToObject(ssid_para,WIFI_SSID_PARA,jpara);
		API_Para_Write_Public(ssid_para);
		cJSON_Delete(ssid_para);
	}
	else if(strcmp(para,"add_ssid")==0)
	{
		update_shell_op_time();
		para=GetShellCmdInputString(cmd, 2);
		
		if(para==NULL)
		{
			ShellCmdPrintf("have no ssid para");
			return 0;
		}
		
		jpara=cJSON_Parse(para);
		if(jpara!=NULL)
		{
			API_Wlan_ConnectSsid(jpara, 0);
			ShellCmdPrintf("add ssid");
			cJSON_Delete(jpara);
		}
		else
		{
			ShellCmdPrintf("json para is err");
		}	
	}
	else if(strcmp(para,"close")==0)
	{
		update_shell_op_time();
		API_Wlan_Close();
	}
	else if(strcmp(para,"force")==0) 
	{
		para=GetShellCmdInputString(cmd, 2);
		if(para!=NULL)
		{
			if(Api_NetRoute_EnterForce(para)==0)
			{
				ShellCmdPrintf("force to %s ok",para);
			}
			else
			{
				ShellCmdPrintf("force to %s fail",para);
			}	
		}
	}
	else if(strcmp(para,"sipc")==0)
	{
		cJSON *search = cJSON_CreateArray();
		para=GetShellCmdInputString(cmd, 2);
		IpcDeviceDiscovery(para!=NULL?atoi(para):1000, NULL, search);
		char* viewStr = cJSON_Print(search);
		ShellCmdPrintf("Ipc search result:%s\n", viewStr);
		if(viewStr)
		{
			free(viewStr);
		}
		cJSON_Delete(search);
	}
	else if(strcmp(para,"beep")==0)
	{
		#include "task_Beeper.h"
		BEEP_KEY();
	}
	return 0;
}

int ShellCmd_Lan(cJSON *cmd)
{
	char *para=GetShellCmdInputString(cmd, 1);
	cJSON *jpara;
	cJSON *set_para;
	
	if(para==NULL)
	{
		ShellCmdPrintf("hava no para");
		return 1;
	}
	if(strcmp(para,"state")==0)
	{	
		jpara=API_Get_Lan_State();
		char* temp;
		temp=cJSON_Print(jpara);
		if(temp==NULL)
		{
			ShellCmdPrintf("Get State Fail");
			return 1;
		}
		ShellCmdPrintf(temp);
		free(temp);
	}
	else if(strcmp(para,"auto")==0)
	{
		para=GetShellCmdInputString(cmd, 2);
		set_para=cJSON_CreateObject();
		cJSON_AddStringToObject(set_para,Lan_policy,Lan_Policy_AutoIPAndDHCP_Str);
		
		if(para==NULL)
		{
			ShellCmdPrintf("set lan to auto-ip by io-para");
			API_Set_Lan_Config(set_para);
			cJSON_Delete(set_para);
			return 1;
		}
			
		jpara=cJSON_Parse(para);
		if(jpara!=NULL)
		{
			cJSON_AddItemToObject(set_para,Auto_ip_setting,jpara);
			API_Set_Lan_Config(set_para);
			ShellCmdPrintf("set lan to auto-ip by appoint-para");
			
		}
		else
		{
			ShellCmdPrintf("json para is err");
		}
		cJSON_Delete(set_para);
	}
	else if(strcmp(para,"static")==0)
	{
		para=GetShellCmdInputString(cmd, 2);
		set_para=cJSON_CreateObject();
		cJSON_AddStringToObject(set_para,Lan_policy,Lan_Policy_Static_Str);
		
		if(para==NULL)
		{
			ShellCmdPrintf("set lan to static-ip by io-para");
			API_Set_Lan_Config(set_para);
			cJSON_Delete(set_para);
			return 1;
		}
			
		jpara=cJSON_Parse(para);
		if(jpara!=NULL)
		{
			cJSON_AddItemToObject(set_para,Static_ip_setting,jpara);
			API_Set_Lan_Config(set_para);
			ShellCmdPrintf("set lan to static-ip by appoint-para");
			
		}
		else
		{
			ShellCmdPrintf("json para is err");
		}
		cJSON_Delete(set_para);
	}
	else if(strcmp(para,"get_para")==0)
	{
		jpara=get_lan_para();
		if(jpara==NULL)
		{
			ShellCmdPrintf("get_para fail1");
			return 0;
		}
		char* temp;
		temp=cJSON_Print(jpara);
		if(temp==NULL)
		{
			ShellCmdPrintf("get_para fail2");
			return 1;
		}
		ShellCmdPrintf(temp);
		free(temp);	
	}
	else if(strcmp(para,"save_para")==0)
	{
		jpara=get_lan_para();
		if(jpara==NULL)
		{
			ShellCmdPrintf("get_para fail1");
			return 0;
		}
		char* temp;
		temp=cJSON_Print(jpara);
		if(temp==NULL)
		{
			ShellCmdPrintf("get_para fail2");
			return 1;
		}
		ShellCmdPrintf(temp);
		free(temp);
		API_Para_Write_Public(jpara);	
	}
	else if(strcmp(para,"t1")==0)
	{
		int addr=atoi(GetShellCmdInputString(cmd, 2));
		int delay=atoi(GetShellCmdInputString(cmd,3));
		int online=API_ExtModel_CheckOnline(addr);
		ShellCmdPrintf("API_ExtModel_CheckOnline:%d\n",online);
		usleep(delay*1000);
		online=API_ExtModel_CheckOnline(addr);
		ShellCmdPrintf("API_ExtModel_CheckOnline:%d\n",online);
	}
	else if(strcmp(para,"sip_n")==0)
	{
		ShellCmdPrintf("sip_n:%s state=%d,.time=%d",Get_CurSipRoute(),Get_SipNetwork_state(),Get_SipNetwork_timer());
	}
	return 0;
}

void wifi_save_ssid(void)
{
	cJSON *jpara=get_wlan_ssid();
	cJSON *ssid_para;
		ssid_para=cJSON_CreateObject();
		cJSON_AddItemReferenceToObject(ssid_para,WIFI_SSID_PARA,jpara);
		API_Para_Write_Public(ssid_para);
		cJSON_Delete(ssid_para);
}