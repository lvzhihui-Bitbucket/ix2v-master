/**
  ******************************************************************************
  * @file    task_DHCP.c
  * @author  zxj
  * @version V00.01.00
  * @date    2012.08.22
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2012 V-Tec</center></h2>
  ******************************************************************************
  */ 
  //czn_20181227
  
#include "obj_lan_manager.h"
//#include "task_IoServer.h"
#include "define_file.h"
#include "unix_socket.h"
#include "obj_autoip_client.h"
#include "cJSON.h"
#include "obj_SYS_VER_INFO.h"
#include "obj_IoInterface.h"
#include "obj_FileManageUtility.h"
#include "obj_PublicInformation.h"
#include "task_Event.h"
#include "elog_forcall.h"

Loop_vdp_common_buffer  	vdp_LanManager_mesg_queue;
Loop_vdp_common_buffer  	vdp_LanManager_mesg_sync_queue;
vdp_task_t			  		task_LanManager;

OS_TIMER					LanCheckInternetTimer;
int						IpActionType = 0;	//0,unkown,1,dhcp,2,autoip,3,static ip,
int						LanInternetState = 0;
int 					LaninternetTimeUpdateFlag;
int						IpRepeatFlag = 0;
static int lan_sync_wait_flag=0;
static char lan_gw_save[20]={0};
static int Lan_Link_State=1;
//lan_config_t	lan_config;
OS_TIMER					CheckLanInternetTimer;
cJSON *last_autoip_config=NULL;
cJSON *last_staticip_config=NULL;
cJSON *io_autoip_config=NULL;
cJSON *io_staticip_config=NULL;
void LanCheckInternetTimerCallback(void);
int LanCheckInternet_Process(void);
char*  get_lan_gw_save(void);

int MyCjson_datapro(cJSON *findJson, char *name,void *data);

void vdp_LanManager_mesg_data_process(char* msg_data, int len);
void* vdp_LanManager_task( void* arg );
void Lan_Flush(void);
int Update_Lan_Config(cJSON *Lan_Config);
int Load_Lan_Config_FromIo(void);
//int SetLanStaticPara( char *IP_Address,char *SubnetMask,char *DefaultRoute);
int SetLanStaticJsonPara( cJSON *para);
extern cJSON * MyCjson_GetObjectItem(cJSON *findJson, char *name);
		  
void vtk_TaskInit_LanManager(int priority)
{
	init_vdp_common_queue(&vdp_LanManager_mesg_queue, 1000, (msg_process)vdp_LanManager_mesg_data_process, &vdp_LanManager_mesg_queue);
	init_vdp_common_queue(&vdp_LanManager_mesg_sync_queue, 200, NULL, 								  		&task_LanManager);
	init_vdp_common_task(&task_LanManager, MSG_ID_DHCP, vdp_LanManager_task, &vdp_LanManager_mesg_queue, &vdp_LanManager_mesg_sync_queue);
	
	bprintf("vdp_DHCP_task starting............\n");
}

void exit_vdp_LanManager_task(void)
{
	exit_vdp_common_queue(&vdp_LanManager_mesg_queue);
	exit_vdp_common_task(&task_LanManager);
}



void* vdp_LanManager_task( void* arg )
{
	vdp_task_t*  ptask		  = (vdp_task_t*)arg;
	p_vdp_common_buffer pdb	  = 0;
	int size;
	thread_log_add(__func__);
	//sleep(2);
	init_autoip_client();
	SipNetwork_Manager_Init();
	int try_cnt=0;
	char eoc_mac[18]={0};
	int eoc_link_state=-1;
	
	usleep(100*1000*(((unsigned int)MyRand()%100)+1));
	init_lan_mac();
	
	
	#ifdef IX_LINK
	API_PublicInfo_Write_String("EOC_state","unlink"); 
	API_PublicInfo_Write_String("EOC_Mac_State","Unkown");
	API_PublicInfo_Write_String("EOC_Mac","");
	API_PublicInfo_Write_String("EOC_Ver","");
	#else
	CreateEocMacBySn(eoc_mac);
	while(try_cnt++<15)
	{
		if((eoc_link_state=UpateEocState(eoc_mac))>0)
		{
			usleep(100*1000);
			//SetEocMacBySn();
			break;
		}
		
		sleep(1);
	}
	if(eoc_link_state<0)
	{
		if(API_VTK_Get_Eoc_Mac_Ver("eth0",NULL,NULL)==0)
		{
			API_PublicInfo_Write_String("EOC_state","linked"); 
		}
	}
	
	CheckEocSimulateFlashBackupFile(eoc_mac);
	UpdateEocParameter(eoc_mac);
	#endif
	Lan_Link_State=LanCheckLink();
	//printf("^^^^^^^^^%s:%d:%d\n",__func__,__LINE__,Lan_Link_State);
	if(Lan_Link_State)
	{
		Dhcp_Autoip_Close();
		usleep(200*1000);
		Load_Lan_Config_FromIo();
	}
	else
		API_PublicInfo_Write(PB_LAN_STATE,API_Get_Lan_State());
	
	OS_CreateTimer(&LanCheckInternetTimer,LanCheckInternetTimerCallback,60000/25);
	OS_RetriggerTimer(&LanCheckInternetTimer);
	ptask->task_StartCompleted = 1;	
	//UpateEocState();
	
	API_Event_By_Name(EventSnErrDisp);
	while( ptask->task_run_flag )
	{
		size = pop_vdp_common_queue( ptask->p_msg_buf, &pdb, VDP_QUEUE_POLLING_TIME);
		//size = pop_vdp_common_queue( ptask->p_msg_buf, &pdb, 1000*30);
		if( size > 0 )
		{
			(*ptask->p_msg_buf->process)(pdb,size);

			purge_vdp_common_queue( ptask->p_msg_buf );
		}
		#if 0
		else
		{
			api_uart_send_pack(12, NULL, NULL);  
		}
		#endif
	}
	return 0;
}


 
void vdp_LanManager_mesg_data_process(char* msg_data,int len)
{
	MSG_LAN_MANAGER* msg;
	cJSON *config=NULL;
	int act_type;
	int temp;
	char eoc_mac[18]={0};
	int try_cnt=0;
	int eoc_link_state=-1;
	//static int reconfig_cnt=0;
	
	msg = (MSG_LAN_MANAGER*)msg_data;
	

	switch(msg->head.msg_type )
	{
		case MSG_TYPE_LanManager_SetConfig:
			config=cJSON_Parse(msg->config_json);
			printf("MSG_TYPE_LanManager_SetConfig:\n%s\n",msg->config_json);
			if(config!=NULL)
			{
				printf("MSG_TYPE_LanManager_SetConfig:22222222222\n");
				Update_Lan_Config(config);
				//Dhcp_Autoip_CheckIP();
				cJSON_Delete(config);
			}
			//Lan_Flush();
			break;
		case MSG_TYPE_LanManager_AutoIPStateUpdate:
			SetIpActionType(msg->head.msg_sub_type);
			update_lan_gw_save();
			Dhcp_Autoip_CheckIP();
			//Api_Network_GetSn(NET_ETH0);
			SipNetworkManage_LanConnected();
			API_PublicInfo_Write(PB_LAN_STATE,API_Get_Lan_State());
			API_Event_By_Name(EventNetLinkState);
			#if 0
			if(IpActionType==2)
			{
				char last_ip[16]={0};
				API_Para_Read_String(Autoip_LastIP, last_ip);
				if(strcmp(last_ip,GetSysVerInfo_IP())!=0)
				{
					API_Para_Write_String(Autoip_LastIP, GetSysVerInfo_IP());
				}
			}
			#endif
			break;
		case MSG_TYPE_LanManager_CheckIPRsp:
			if(msg->head.msg_sub_type>0)
			{
				if(IpActionType == 2)	
				{
					API_Para_Write_String(Autoip_LastIP, "-");
					cJSON_DeleteItemFromObject(last_autoip_config,Autoip_LastIP);
					printf("!!!!!!autoip restart-------\n");
					Dhcp_Autoip_Close();
					SetIpActionType(3);
					sleep(1);
					SetIpActionType(0);
					act_type=Dhcp_Autoip_Open_JsonPara(last_autoip_config);
					if(act_type>0)
					{
						SetIpActionType(act_type);
						API_PublicInfo_Write(PB_LAN_STATE,API_Get_Lan_State());
						API_Event_By_Name(EventNetLinkState);
						Dhcp_Autoip_CheckIP();
					}
					LanInternetState = 0;
					IpRepeatFlag = 0;
				}
				else
				{
					IpRepeatFlag = 1;
				}
			}
			else
			{
				IpRepeatFlag = 0;
				if(IpActionType == 2)
				{
					char last_ip[16]={0};
					API_Para_Read_String(Autoip_LastIP, last_ip);
					if(strcmp(last_ip,GetSysVerInfo_IP())!=0)
					{
						API_Para_Write_String(Autoip_LastIP, GetSysVerInfo_IP());
					}
				}
			}
			break;
		case MSG_TYPE_LanManager_InternetCheck:
			if(Lan_Link_State)
			{
				if(LanCheckInternet_Process())
				{
					SetNetworkIsStarted(NET_ETH0,1);
				}
			}
			temp=LanCheckLink();
			if(temp!=Lan_Link_State)
			{
				Lan_Link_State=temp;
				if(Lan_Link_State)
				{
					Dhcp_Autoip_Close();
					usleep(200*1000);
					Load_Lan_Config_FromIo();
				}
				else
				{
					SipNetworkManage_LanDisconnected();
					API_PublicInfo_Write(PB_LAN_STATE,API_Get_Lan_State());
					API_Event_By_Name(EventNetLinkState);
				}
			}
			else if(Lan_Link_State)
			{
				if(GetIpActionType()==1&&strcmp(Get_CurSipRoute(),"LAN")==0)	
				{
					if(PingNetwork(get_lan_gw_save()) !=0)
						Api_Sip_ReReg();
				}
				if(GetIpActionType()==0)
				{
					Dhcp_Autoip_Close();
					usleep(200*1000);
					Load_Lan_Config_FromIo();
					//printf("^^^^^^^^^%s:%d:%d\n",__func__,__LINE__,GetIpActionType());
					if(GetIpActionType()==0)
					{
						//printf("^^^^^^^^^%s:%d:%d\n",__func__,__LINE__,GetIpActionType());
						HardwareRestart("lan state error");
					}
				}
				else
					CheckLanPbState();
				//UpateEocState();
					
			}
			break;
		case MSG_TYPE_LanManager_Reload:
			init_lan_mac();
				

			#ifdef IX_LINK
			API_PublicInfo_Write_String("EOC_state","unlink"); 
			API_PublicInfo_Write_String("EOC_Mac_State","Unkown");
			API_PublicInfo_Write_String("EOC_Mac","");
			API_PublicInfo_Write_String("EOC_Ver","");
			#else
			CreateEocMacBySn(eoc_mac);

			while(try_cnt++<15)
			{
				if((eoc_link_state=UpateEocState(eoc_mac))>0)
				{
					usleep(100*1000);
					//SetEocMacBySn();
					break;
				}
				
				sleep(1);
			}
			UpdateEocParameter(eoc_mac);
			#endif
			API_Event_By_Name(EventSnErrDisp);
			Dhcp_Autoip_Close();
			usleep(200*1000);
			Load_Lan_Config_FromIo();
			break;
	}
	
	
}
void CheckLanPbState(void)
{
	static int cnt=0;
	static int err_cnt=0;
	if(++cnt>=5)
	{
		cnt=0;
		cJSON *pb_state=API_PublicInfo_Read(PB_LAN_STATE);
		cJSON *lan_state=API_Get_Lan_State();
		if(!cJSON_Compare(pb_state,lan_state,0))
		{
			bprintf("pb lan state error!!\n");
			log_e("pb lan state error!!");
			err_cnt++;
		}
		else
		{
			//bprintf("pb lan state ok!!\n");
			err_cnt=0;
		}
		if(err_cnt>=2)
		{
			Load_Lan_Config_FromIo();
			err_cnt=0;
		}
	}
	else if(cnt==1)
	{
		Dhcp_Autoip_CheckIP();
	}
}
void set_lan_sync_wait(int flag)
{
	lan_sync_wait_flag=flag;
}

int get_lan_sync_wait(void)
{
	return lan_sync_wait_flag;
}

char*  get_lan_gw_save(void)
{
	return lan_gw_save;
}
void update_lan_gw_save(void)
{
	int gateway;
	gateway = GetLocalGatewayByDevice(NET_ETH0);
	if(gateway != -1)
	{
		my_inet_ntoa(gateway, lan_gw_save);
	}
	//strcpy(lan_gw_save,gw);
}
/************************************
@Lan_Config:
Lan_policy:"Static"/"AutoIPAndDHCP"
Auto_ip_setting(@Lan_policy="AutoIPAndDHCP"):
{
  Autoip_IPMask:"XXX.XXX.XXX.XXX",
  Autoip_IPGateway:"XXX.XXX.XXX.XXX",
  Autoip_IPSegment:"XXX.XXX.XXX.XXX",
}
Static_ip_setting(@Lan_policy="Static"):
{
  IP_Address:"XXX.XXX.XXX.XXX",
  SubnetMask:"XXX.XXX.XXX.XXX",
  DefaultRoute:"XXX.XXX.XXX.XXX"
}

EXAMPLE:
{
    "LANConfig1":{
        	"Lan_policy":"Static",
        	"Static_ip_setting":{
  		"IP_Address":"192.168.243.25",
  		"SubnetMask":"255.255.255.0",
  		"DefaultRoute":"192.168.243.200"
		}
    },
    "LANConfig2":{
        	"Lan_policy":"AutoIPAndDHCP",
	"Auto_ip_setting": {
		  "Autoip_IPMask":"255.255.240.0",
		  "Autoip_IPGateway":"192.168.240.200",
		  "Autoip_IPSegment":"192.168.240.0",
		}
    }	
}
*************************************/
int API_Set_Lan_Config(cJSON *Lan_Config)
{
	MSG_LAN_MANAGER *pmsg;	
	char *config_str=NULL;
	int rev;
	config_str=cJSON_Print(Lan_Config);
	if(config_str==NULL)
	{
		dprintf("API_Set_Lan_Config config_str is NULL\n");
		return -1;
	}
	
	pmsg=(MSG_LAN_MANAGER *)malloc(sizeof(VDP_MSG_HEAD)+strlen(config_str)+1);

	if(pmsg==NULL)
	{
		dprintf("API_Set_Lan_Config pmsg is NULL\n");
		return -1;
	}
	//UdhcpcClose();
	strcpy(pmsg->config_json,config_str);
	free(config_str);
	
	pmsg->head.msg_source_id	= GetMsgIDAccordingPid(pthread_self());
	pmsg->head.msg_target_id	= MSG_ID_DHCP;
	pmsg->head.msg_type		= MSG_TYPE_LanManager_SetConfig;
	pmsg->head.msg_sub_type	= 0;
	//vdp_task_t* pTask = OS_GetTaskID();
	//strcpy(pmsg->config_json,config_str);
	
	
	rev=push_vdp_common_queue(&vdp_LanManager_mesg_queue,  (char*)pmsg, sizeof(VDP_MSG_HEAD)+strlen(pmsg->config_json)+1);
	if(rev)
	{
		dprintf("API_Set_Lan_Config vdp_LanManager_mesg_queue is full\n");
	}
	
	free(pmsg);
	return rev;

}

int API_Set_Lan_Reload(void)
{
	VDP_MSG_HEAD msg;
	
	int rev;
	
	
	msg.msg_source_id	= GetMsgIDAccordingPid(pthread_self());
	msg.msg_target_id	= MSG_ID_DHCP;
	msg.msg_type		= MSG_TYPE_LanManager_Reload;
	msg.msg_sub_type	= 0;
	//vdp_task_t* pTask = OS_GetTaskID();
	//strcpy(pmsg->config_json,config_str);
	
	
	rev=push_vdp_common_queue(&vdp_LanManager_mesg_queue,  (char*)&msg, sizeof(VDP_MSG_HEAD));
	if(rev)
	{
		dprintf("API_Set_Lan_Config vdp_LanManager_mesg_queue is full\n");
	}
	
	return rev;

}
/*******************************
return:
{
"LAN-SW":"ON"/"OFF"
"LAN-STATE"(@"LAN-SW"="ON"):"Connecting":"Connected"
"LAN-IP-Policy"(@"LAN-STATE"="Connected"):"DHCP"/"AutoIP"/"StaticIP"
"LAN-IP_Addr"(@"LAN-STATE"="Connected"):
{
  IP_Address:"XXX.XXX.XXX.XXX",
  SubnetMask:"XXX.XXX.XXX.XXX",
  DefaultRoute:"XXX.XXX.XXX.XXX"
  MACAddr:"XX:XX:XX:XX:XX:XX"
}
}
*******************************/
 
cJSON* API_Get_Lan_State(void)
{
	static cJSON *lan_state=NULL;
	cJSON *lan_ip;
	char addr_buff[21];
	char temp[6];
	int gateway;
	
	if(lan_state!=NULL)
		cJSON_Delete(lan_state);
	
	lan_state=cJSON_CreateObject();
	if(lan_state==NULL)
		return NULL;
	cJSON_AddStringToObject(lan_state,Lan_State_Key_Sw,"ON");
	if(Lan_Link_State==0)
	{
		cJSON_AddStringToObject(lan_state,Lan_State_Key_State,"Disconneted");
		return lan_state;
	}
	if(GetIpActionType()==0)
	{
		//cJSON_AddStringToObject(lan_state,Lan_State_Key_IPPolicy,LAN_IP_Policy_None);
		cJSON_AddStringToObject(lan_state,Lan_State_Key_State,"Connecting");
		return lan_state;
	}
	cJSON_AddStringToObject(lan_state,Lan_State_Key_State,"Connected");
	if(GetIpActionType()==1)
	{
		cJSON_AddStringToObject(lan_state,Lan_State_Key_IPPolicy,LAN_IP_Policy_DHCP);
	}
	else if(GetIpActionType()==2)
	{
		cJSON_AddStringToObject(lan_state,Lan_State_Key_IPPolicy,LAN_IP_Policy_AutoIP);
	}
	else if(GetIpActionType()==3)
	{
		cJSON_AddStringToObject(lan_state,Lan_State_Key_IPPolicy,LAN_IP_Policy_StaticIP);
	}

	
	lan_ip=cJSON_AddObjectToObject(lan_state,"LAN-IP_Addr");
	if(lan_ip!=NULL)
	{
		my_inet_ntoa(GetLocalIpByDevice(NET_ETH0), addr_buff);
		cJSON_AddStringToObject(lan_ip,Lan_State_Key_IPAddr,addr_buff);
		
		my_inet_ntoa(GetLocalMaskAddrByDevice(NET_ETH0), addr_buff);
		cJSON_AddStringToObject(lan_ip,Lan_State_Key_SubnetMask,addr_buff);

		//gateway = GetLocalGatewayByDevice(NET_ETH0);
		//if(gateway != -1)
		{
			my_inet_ntoa(gateway, addr_buff);
			cJSON_AddStringToObject(lan_ip,Lan_State_Key_DefaultRoute,get_lan_gw_save());
		}

		GetLocalMacByDevice(NET_ETH0, temp);
		snprintf(addr_buff, 18, "%02x:%02x:%02x:%02x:%02x:%02x", temp[0], temp[1], temp[2], temp[3], temp[4], temp[5]);
		cJSON_AddStringToObject(lan_ip,Lan_State_Key_MACAddr,addr_buff);
	}
	return lan_state;
}

int Update_Lan_Config(cJSON *Lan_Config)
{
	char lan_policy[50];
	cJSON *config;
	p_vdp_common_buffer pdb	  = 0;
	MSG_LAN_MANAGER* msg;
	int act_type;
	int ret;
	
	if(MyCjson_datapro(Lan_Config,Lan_policy,lan_policy)!=0)
		return -1;
	if(strcmp(lan_policy,Lan_Policy_AutoIPAndDHCP_Str)==0)
	{
		io_autoip_config=API_Para_Read_Public(Auto_ip_setting);
		Dhcp_Autoip_Close();
		usleep(200*1000);
		SetIpActionType(0);
		if(last_autoip_config!=NULL);
		{
			cJSON_Delete(last_autoip_config);
			last_autoip_config=NULL;
		}
		
		if(MyCjson_datapro(Lan_Config,Auto_ip_setting,&config)==0)
		{
			last_autoip_config=cJSON_Duplicate(config,1);
			if((act_type=Dhcp_Autoip_Open_JsonPara(config))<0)
				return -1;
			
		}
		else
		{
			last_autoip_config=cJSON_Duplicate(io_autoip_config,1);
			if((act_type=Dhcp_Autoip_Open_JsonPara(io_autoip_config))<0)
				return -1;
			
		}
		usleep(100);
		SetIpActionType(act_type);
		update_lan_gw_save();
		
		SipNetworkManage_LanConnected();
		API_PublicInfo_Write(PB_LAN_STATE,API_Get_Lan_State());
		API_Event_By_Name(EventNetLinkState);
		Dhcp_Autoip_CheckIP();
	}
	else if(strcmp(lan_policy,Lan_Policy_Static_Str)==0)
	{
		io_staticip_config=API_Para_Read_Public(Static_ip_setting);
		Dhcp_Autoip_Close();
		usleep(200*1000);
		SetIpActionType(3);
		if(last_staticip_config!=NULL);
		{
			cJSON_Delete(last_staticip_config);
			last_staticip_config=NULL;
		}
		if(MyCjson_datapro(Lan_Config,Static_ip_setting,&config)==0)
		{
			last_staticip_config=cJSON_Duplicate(config,1);
			ret=SetLanStaticJsonPara(config);
			update_lan_gw_save();
			SipNetworkManage_LanConnected();
			API_PublicInfo_Write(PB_LAN_STATE,API_Get_Lan_State());
			API_Event_By_Name(EventNetLinkState);
			return ret;
		}
		else
		{
			last_staticip_config=cJSON_Duplicate(io_staticip_config,1);
			ret=SetLanStaticJsonPara(io_staticip_config);
			update_lan_gw_save();
			SipNetworkManage_LanConnected();
			API_PublicInfo_Write(PB_LAN_STATE,API_Get_Lan_State());
			API_Event_By_Name(EventNetLinkState);
			return ret;
		}
	}
	return -1;
}
int Load_Lan_Config_FromIo(void)
{
	cJSON *item;
	//char *pjson_str;
	char lan_policy[50];
	p_vdp_common_buffer pdb	  = 0;
	MSG_LAN_MANAGER* msg;
	int act_type;
	int ret;
	//printf("^^^^^^^^^%s:%d:%d\n",__func__,__LINE__,Lan_Link_State);
	if(API_Para_Read_String(Lan_policy, lan_policy)!=1)
		return -1;
	
	if(strcmp(lan_policy,Lan_Policy_AutoIPAndDHCP_Str)==0)
	{
		//printf("^^^^^^^^^%s:%d:%d\n",__func__,__LINE__,Lan_Link_State);
		io_autoip_config=API_Para_Read_Public(Auto_ip_setting);
		if(io_autoip_config==NULL)
		{
			return -1;
		}
		char last_ip[16]={0};
		API_Para_Read_String(Autoip_LastIP, last_ip);
		if(strlen(last_ip)>=7)
		{	
			//char mac[18];
			char mask[16]={0};
			char gw[16]={0};
			cJSON *last_conf;
			last_conf=cJSON_CreateObject();
			cJSON_AddStringToObject(last_conf,IP_Address,last_ip);
			API_Para_Read_String(Autoip_IPMask, mask);
			cJSON_AddStringToObject(last_conf,SubnetMask,mask);
			API_Para_Read_String(Autoip_IPGateway, gw);
			cJSON_AddStringToObject(last_conf,DefaultRoute,gw);
			SetLanStaticJsonPara(last_conf);
			cJSON_Delete(last_conf);
			SetIpActionType(2);
			API_PublicInfo_Write(PB_LAN_STATE,API_Get_Lan_State());
			API_Event_By_Name(EventNetLinkState);
			//printf("-----------------Set LastIP--------------\n	");
		}
		SetIpActionType(0);
		if(last_autoip_config!=NULL);
		{
			cJSON_Delete(last_autoip_config);
			last_autoip_config=NULL;
		}
		last_autoip_config=cJSON_Duplicate(io_autoip_config,1);
		
		if((act_type=Dhcp_Autoip_Open_JsonPara(io_autoip_config))<0)
		{
			//printf("^^^^^^^^^%s:%d:%d\n",__func__,__LINE__,Lan_Link_State);
			return -1;
		}
		SetIpActionType(act_type);
		update_lan_gw_save();
		usleep(100);
		SipNetworkManage_LanConnected();
		API_PublicInfo_Write(PB_LAN_STATE,API_Get_Lan_State());
		API_Event_By_Name(EventNetLinkState);
		Dhcp_Autoip_CheckIP();
		if(IpActionType==2)
		{
			char last_ip[16]={0};
			API_Para_Read_String(Autoip_LastIP, last_ip);
			if(strcmp(last_ip,GetSysVerInfo_IP())!=0)
			{
				API_Para_Write_String(Autoip_LastIP, GetSysVerInfo_IP());
			}
		}
	}
	else if(strcmp(lan_policy,Lan_Policy_Static_Str)==0)
	{
		io_staticip_config=API_Para_Read_Public(Static_ip_setting);
		if(io_staticip_config==NULL)
		{
			return -1;
		}
		SetIpActionType(3);
		ret=SetLanStaticJsonPara(io_staticip_config);
		update_lan_gw_save();
		SipNetworkManage_LanConnected();
		API_PublicInfo_Write(PB_LAN_STATE,API_Get_Lan_State());
		API_Event_By_Name(EventNetLinkState);
		return ret;
	}
	return -1;
	
}


void LanAutoIPStateUpdate(int state)
{
	VDP_MSG_HEAD msg;
	msg.msg_source_id 	= 0;
	msg.msg_target_id 	= MSG_ID_DHCP;
	msg.msg_type 		= MSG_TYPE_LanManager_AutoIPStateUpdate;
	msg.msg_sub_type 	= state;
	//printf("^^^^^^^^^%s:%d:%d\n",__func__,__LINE__,get_lan_sync_wait());
	if(get_lan_sync_wait())
		push_vdp_common_queue(&vdp_LanManager_mesg_sync_queue,  (char*)&msg, sizeof(VDP_MSG_HEAD));
	else
		push_vdp_common_queue(&vdp_LanManager_mesg_queue,  (char*)&msg, sizeof(VDP_MSG_HEAD));
}

void LanAutoIPCloseRsp(int state)
{
	VDP_MSG_HEAD msg;
	msg.msg_source_id 	= 0;
	msg.msg_target_id 	= MSG_ID_DHCP;
	msg.msg_type 		= MSG_TYPE_LanManager_AutoIPCloseRsp;
	msg.msg_sub_type 	= state;
	push_vdp_common_queue(&vdp_LanManager_mesg_sync_queue,  (char*)&msg, sizeof(VDP_MSG_HEAD));
}

void LanCheckIPRsp(int state)
{
	VDP_MSG_HEAD msg;
	msg.msg_source_id 	= 0;
	msg.msg_target_id 	= MSG_ID_DHCP;
	msg.msg_type 		= MSG_TYPE_LanManager_CheckIPRsp;
	msg.msg_sub_type 	= state;
	push_vdp_common_queue(&vdp_LanManager_mesg_queue,  (char*)&msg, sizeof(VDP_MSG_HEAD));
}
void LanAutoIPCheckVerRsp(int state)
{
	VDP_MSG_HEAD msg;
	msg.msg_source_id 	= 0;
	msg.msg_target_id 	= MSG_ID_DHCP;
	msg.msg_type 		= MSG_TYPE_LanManager_CheckVerRsp;
	msg.msg_sub_type 	= state;
	push_vdp_common_queue(&vdp_LanManager_mesg_queue,  (char*)&msg, sizeof(VDP_MSG_HEAD));
}
////////////////////////////////////////////////////////



int GetIpActionType(void)	//0,unkown,1,dhcp,2,autoip,3,static ip,
{
	return IpActionType;
}

void SetIpActionType(int type)	//0,unkown,1,dhcp,2,autoip,3,static ip,
{
	IpActionType = type;
}

void SetTimeUpdateCnt(int cnt)
{
	//internetTimeUpdateFlag = cnt;
}




int GetInternetState(void)
{
	return LanInternetState;
}

int GetIpRepeatFlag(void)
{
	return IpRepeatFlag;
}

int get_lan_link(void)
{
	return 1;
}

#if 0
int SetLanStaticPara( char *IP_Address,char *SubnetMask,char *DefaultRoute)
{
	FILE* file   = NULL;
	char line[100];
	
	if( (file=fopen(ROUTE_TEMP,"w"))==NULL)
	{
		eprintf( "SetNetWork error:%s\n",strerror(errno) );
		return 0;
	}
	
	fprintf( file, "#!/bin/sh\n");
	fprintf( file, "/sbin/ifconfig eth0 down\n");
		
	fprintf( file, "/sbin/ifconfig eth0 %s #ip\n", IP_Address);
	

	
	fprintf( file, "/sbin/ifconfig eth0 netmask %s #netmask\n", SubnetMask);
	
			// check parameter
			
	fprintf( file, "/sbin/ifconfig eth0 up\n");	
	
	fprintf( file,  "/sbin/route add default gw %s dev eth0 #gateway\n", DefaultRoute);
	
	
	fclose( file );
	chmod(ROUTE_TEMP, S_IRUSR|S_IWUSR|S_IXUSR);
	if( (file = popen(ROUTE_TEMP, "r")) == NULL )
	{
		eprintf( "ReSetNetWork error:%s\n",strerror(errno) );
		return 0;
	}

	pclose( file );
	remove(ROUTE_TEMP);
	file= NULL;
	return 1;
}
#endif
int SetLanMac( char *mac_addr)
{
	FILE* file   = NULL;
	char line[100];
	
	if( (file=fopen(ROUTE_TEMP,"w"))==NULL)
	{
		eprintf( "SetNetWork error:%s\n",strerror(errno) );
		return 0;
	}
	
	fprintf( file, "#!/bin/sh\n");
	fprintf( file, "/sbin/ifconfig eth0 hw ether %s #mac\n", mac_addr);
	
	fclose( file );
	chmod(ROUTE_TEMP, S_IRUSR|S_IWUSR|S_IXUSR);
	if( (file = popen(ROUTE_TEMP, "r")) == NULL )
	{
		eprintf( "ReSetNetWork error:%s\n",strerror(errno) );
		return 0;
	}

	pclose( file );
	remove(ROUTE_TEMP);
	file= NULL;
	return 1;
}
void init_lan_mac(void)
{
	int sn[6];
	char sn_str[50];
	char mac[50];
	int cnt=0;
	int sn_type = SourceStringToType(cJSON_GetStringValue(API_Para_Read_Public(IO_MFG_SN_Source)));
	while(sn_type==0)
	{
		if(cnt++>50)
			break;
		usleep(100*1000);
		sn_type = SourceStringToType(cJSON_GetStringValue(API_Para_Read_Public(IO_MFG_SN_Source)));
	}
	
	if(cnt<50)
	{
		usleep(100*1000);
		API_Para_Read_String(IO_MFG_SN,sn_str);
		sscanf(sn_str, "%02x%02x%02x%02x%02x%02x", &sn[0], &sn[1], &sn[2], &sn[3], &sn[4], &sn[5]);
		#if 0
		int ret = -1;
		FILE *fp=fopen("/mnt/nand1-2/Settings/sn","rb");
		//mySn.type = SN_Type_NONE;
		if(fp!=NULL)
		{

			if(fread(sn, 1, 6, fp))
			{
				//int i;
				//mySn.checkCode=0;
				//for(i=0;i<6;i++)
				//	mySn.checkCode+=mySn.serial_number[i];
				//mySn.type = SN_Type_Tiny1616;
				ret =0;
			}
			fclose(fp);
		}
		#endif
		
		if(sn[0]!=0x16&&sn[5]==0x16)
		{
			snprintf(mac, 50, "%02x:%02x:%02x:%02x:%02x:%02x", sn[5], sn[4], sn[3], sn[2], sn[1], sn[0]);
		}
		else
			snprintf(mac, 50, "%02x:%02x:%02x:%02x:%02x:%02x", sn[0], sn[1], sn[2], sn[3], sn[4], sn[5]);
		
		//printf("111111111init_lan_mac:%s\n",mac);
		SetLanMac(mac);
		if(API_Para_Read_String(IO_EOC_MAC,mac)==1)
		{
			//API_VTK_Set_Eoc_Mac("eth0",mac);
		}
	}
}
int SetLanStaticJsonPara( cJSON *para)
{
	FILE* file   = NULL;
	char line[100];
	char buff[50];
	int rev=0;
	
	if( (file=fopen(ROUTE_TEMP,"w"))==NULL)
	{
		eprintf( "SetNetWork error:%s\n",strerror(errno) );
		return 0;
	}
	
	fprintf( file, "#!/bin/sh\n");
	fprintf( file, "/sbin/ifconfig eth0 down\n");
	if(MyCjson_datapro(para,IP_Address,buff)!=0)
	{
		if(MyCjson_datapro(io_staticip_config,IP_Address,buff)!=0)
		{
			rev=-1;
			goto SetLanStaticJsonPara_Err;
		}
	}	
	fprintf( file, "/sbin/ifconfig eth0 %s #ip\n", buff);
	

	if(MyCjson_datapro(para,SubnetMask,buff)!=0)
	{
		if(MyCjson_datapro(io_staticip_config,SubnetMask,buff)!=0)
		{
			rev=-1;
			goto SetLanStaticJsonPara_Err;
		}
	}	
	fprintf( file, "/sbin/ifconfig eth0 netmask %s #netmask\n", buff);
	
			// check parameter
			
	fprintf( file, "/sbin/ifconfig eth0 up\n");	
	if(MyCjson_datapro(para,DefaultRoute,buff)!=0)
	{
		if(MyCjson_datapro(io_staticip_config,DefaultRoute,buff)!=0)
		{
			rev=-1;
			goto SetLanStaticJsonPara_Err;
		}
	}	
	fprintf( file,  "/sbin/route add default gw %s dev eth0 #gateway\n", buff);
	
	
	fclose( file );
	chmod(ROUTE_TEMP, S_IRUSR|S_IWUSR|S_IXUSR);
	if( (file = popen(ROUTE_TEMP, "r")) == NULL )
	{
		eprintf( "ReSetNetWork error:%s\n",strerror(errno) );
		
		rev=-1;
	}
	//Api_Network_GetSn(NET_ETH0);
	
SetLanStaticJsonPara_Err:
	pclose( file );
	remove(ROUTE_TEMP);
	file= NULL;
	update_lan_gw_save();
	return rev;
}

void lan_manager_test_get_state(void)
{
			//if(pbuf[1]==1&&pbuf[2]==1)
			{
				char *lan_status_str;
				cJSON *lan_status;
				lan_status=API_Get_Lan_State();
				if(lan_status!=NULL)
				{
					lan_status_str= cJSON_Print(lan_status);
					cJSON_Delete(lan_status);
					if(lan_status_str!=NULL)
					{
						printf("lan_state:\n%s\n",lan_status_str);
						free(lan_status_str);
					}
					
					
				}
			}
}
void lan_manager_test_set_config(void)
{
	cJSON *lan_config,*lan_config_arr;
	char *lan_config_str;
	static int set_index=0;
	int set_num;
	
	lan_config_arr=GetJsonFromFile("/mnt/nand1-1/App/res/LanManagerParaTest.json");
	if(lan_config_arr!=0)
	{
		set_num=cJSON_GetArraySize(lan_config_arr);
		if(set_index>=set_num)
			set_index=0;
		lan_config=cJSON_GetArrayItem(lan_config_arr,set_index);
		API_Set_Lan_Config(lan_config);
		set_index++;
		cJSON_Delete(lan_config_arr);
	}
}
int MyCjson_datapro(cJSON *findJson, char *name,void *data)
{
    cJSON *current_element = NULL;
    //cJSON *ret = NULL;
	int *rev_int=(int*)data;
	char *rev_str=(char*)data;
	cJSON **rev_obj=(cJSON **)data;
	int ret;
    if ((findJson == NULL) || (name == NULL))
    {
        return -1;
    }

    current_element = findJson;

    while(current_element != NULL)
    {
        if(current_element->string != NULL && !strcmp(name, current_element->string))
        {
			if(cJSON_IsNumber(current_element))
			{
				*rev_int=(int)current_element->valuedouble;
				//printf("get para_int (%s):%d\n",name,*rev_int);
				return 0;
			}
			if(cJSON_IsString(current_element))
			{
				strcpy(rev_str,current_element->valuestring);
				//printf("get para_str (%s):%s\n",name,rev_str);
				return 0;
			}
			if(cJSON_IsObject(current_element))
			{
				*rev_obj=current_element;
				return 0;
			}
            return -1;
        }
        else if(cJSON_IsObject(current_element))
        {
            ret = MyCjson_datapro(current_element->child, name,data);
            if(ret == 0)
            {
                return ret;
            }
        }
        current_element = current_element->next;
    }
    return -1;
}
cJSON *get_lan_para(void)
{
	static cJSON *lan_para=NULL;
	if(lan_para!=NULL)
		cJSON_Delete(lan_para);
	lan_para=NULL;
	if(GetIpActionType()==0)
		return NULL;
	lan_para=cJSON_CreateObject();
	if(GetIpActionType()==1||GetIpActionType()==2)
	{
		cJSON_AddStringToObject(lan_para,Lan_policy,Lan_Policy_AutoIPAndDHCP_Str);
		cJSON_AddItemReferenceToObject(lan_para,Auto_ip_setting,last_autoip_config);
	}
	else
	{
		cJSON_AddStringToObject(lan_para,Lan_policy,Lan_Policy_Static_Str);
		cJSON_AddItemReferenceToObject(lan_para,Static_ip_setting,last_staticip_config);
	}
	return lan_para;
}

void LanCheckInternetTimerCallback(void)
{
	VDP_MSG_HEAD	msg;	

	
	//UdhcpcClose();
	
	msg.msg_source_id	= 0;//GetMsgIDAccordingPid(pthread_self());
	msg.msg_target_id	= MSG_ID_DHCP;
	msg.msg_type		= MSG_TYPE_LanManager_InternetCheck;
	msg.msg_sub_type	= 0;
	//vdp_task_t* pTask = OS_GetTaskID();

	
	push_vdp_common_queue(&vdp_LanManager_mesg_queue,  &msg, sizeof(VDP_MSG_HEAD));

	OS_RetriggerTimer(&LanCheckInternetTimer);
	
}
int LanCheckInternet_Process(void)
{
	char sip_net_sel[5];
	lan_rejoin_multicast_group();
	//API_Event_IoServer_InnerRead_All(SIP_NetworkSetting, sip_net_sel);
	//if(atoi(sip_net_sel)==0)
	{
		if(!PingNetwork("47.106.104.38")||!PingNetwork("47.91.88.33"))
		{
			if(LanInternetState == 0)
			{
				//internetTimeUpdateFlag = 3;
			}
			
			LanInternetState = 1;
			//API_Event_IoServer_InnerRead_All(SIP_NetworkSetting, sip_net_sel);
			//if(atoi(sip_net_sel)==0)
			{
				//AutoRegistration();
				//rejoin_multicast_group();
			}
			//ResetNetworkCardCheck();
		}
		else
		{
			LanInternetState = 0;
		}
	}
	return LanInternetState;
}

int LanCheckLink(void)
{
	FILE *pf = popen("ifconfig eth0","r");
	int prtline = 0;
	char linestr[250];
	int	nw_status = 0;
	
	//sip_status = Get_SipAccount_State();
	
	if(pf == NULL)
	{
		bprintf("open pipe error\n");
		return 0;
	}
	while(fgets(linestr,250,pf) != NULL)
	{
		if(strstr(linestr,"RUNNING") != NULL)
		{
			nw_status = 1;
			break;
		}
	}
	pclose(pf);
	return nw_status;
}
int UpateEocState(char *eoc_mac)
{
	//char eoc_mac[18]={0};
	char version[128] = { 0 };
	
	int link_state=API_VTK_Get_Eoc_Link_State2("eth0",eoc_mac,version);
	//printf("2222222:%d:%s\n",link_state,eoc_mac?eoc_mac:"null");
	if(link_state<0)
		API_PublicInfo_Write_String("EOC_state","unlink"); 
	else if(link_state==0)
		API_PublicInfo_Write_String("EOC_state","unauth"); //ShellCmdPrintf ("unauth\n");
	else if(link_state==1)
		API_PublicInfo_Write_String("EOC_state","slave"); //ShellCmdPrintf ("slave\n");
	else if(link_state==2)
		API_PublicInfo_Write_String("EOC_state","master"); //ShellCmdPrintf ("master\n");
	if(link_state>=0)
	{
		CheckEocMacBySn(eoc_mac);
		API_PublicInfo_Write_String("EOC_Mac",eoc_mac);
		API_PublicInfo_Write_String("EOC_Ver",version);
	}
	else
	{
		API_PublicInfo_Write_String("EOC_Mac_State","Unkown");
		API_PublicInfo_Write_String("EOC_Mac","");
		API_PublicInfo_Write_String("EOC_Ver","");
	}
	return link_state;
}
/*********************************************************************************************************
**  End Of File
*********************************************************************************************************/
