/**
  ******************************************************************************
  * @file    task_DHCP.c
  * @author  zxj
  * @version V00.01.00
  * @date    2012.08.22
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2012 V-Tec</center></h2>
  ******************************************************************************
  */ 
  //czn_20181227
  
#include "obj_autoip_client.h"
//#include "task_IoServer.h"
#include "define_file.h"
#include "unix_socket.h"
#include "obj_IoInterface.h"
#include "cJSON.h"
#include "obj_lan_manager.h"
////////////////////////////////////////////////////////
unix_socket_t	autoip_cliet_socket;



void autoip_client_socket_recv_data(char* pbuf, int len);

int init_autoip_client( void )
{
	init_unix_socket(&autoip_cliet_socket,0,LOCAL_AUTOIP_SERVICE_FILE,autoip_client_socket_recv_data);		// ��������
	create_unix_socket_create(&autoip_cliet_socket);
	usleep( 100000 );

	dprintf("init_autoip_client OK............\n");
	return 0;
}

void autoip_client_socket_recv_data(char* pbuf, int len)
{
	AUTOIP_RSP_STRU* pcmd = (AUTOIP_RSP_STRU*)pbuf;
	//MSG_DHCP	msg;
	switch( pcmd->cmd )
	{
		case S2C_AUTOIP_OPEN_RSP:
			//printf("S2C_AUTOIP_OPEN_RSP : %d\n",pcmd->rsp_state);
			break;
			
		case S2C_AUTOIP_CLOSE_RSP:
			//printf("S2C_AUTOIP_CLOSE_RSP : %d\n",pcmd->rsp_state);
			LanAutoIPCloseRsp(pcmd->rsp_state);
			break;

		case S2C_AUTOIP_STATE_UPDATE:
			//printf("S2C_AUTOIP_STATE_UPDATE : %d\n",pcmd->rsp_state);
				
			//UdhcpcClose();
			
			LanAutoIPStateUpdate(pcmd->rsp_state);
			break;	
		#if 1
		case S2C_AUTOIP_CHECKIP_RSP:		//czn_20190604
			LanCheckIPRsp(pcmd->rsp_state);
			break;
		#endif	
		case S2C_AUTOIP_CHECKVER_RSP:
			LanAutoIPCheckVerRsp(pcmd->rsp_state);
			break;
			
	}	
}


#if 0
int Dhcp_Autoip_Open(char *ifn,int dhcp_timout,int autoip_sleep,char *Autoip_IPMask,char *Autoip_IPGateway,char *Autoip_IPSegment)
{
	AUTOIP_CMD_STRU send_cmd;

	send_cmd.cmd = C2S_AUTOIP_OPEN_REQ;
	send_cmd.dhcp_timeout = dhcp_timout;
	send_cmd.autoip_sleep = autoip_sleep;
	strcpy(send_cmd.ifname,ifn);
		//czn_20190408_s
	//API_Event_IoServer_InnerRead_All(Autoip_IPSegment, send_cmd.ip_segment);
	//API_Event_IoServer_InnerRead_All(Autoip_IPMask, send_cmd.ip_mask);
	//API_Event_IoServer_InnerRead_All(Autoip_IPGateway, send_cmd.ip_gateway);
	//czn_20190408_e
	if(Autoip_IPMask==NULL||Autoip_IPSegment==NULL||Autoip_IPGateway==NULL)
		return -1;
	strcpy(send_cmd.ip_segment,Autoip_IPSegment);
	strcpy(send_cmd.ip_mask,Autoip_IPMask);
	strcpy(send_cmd.ip_gateway,Autoip_IPGateway);

	return unix_socket_send_data( &autoip_cliet_socket, (char*)&send_cmd, sizeof(AUTOIP_CMD_STRU));
}
#endif
extern cJSON *io_autoip_config;
extern cJSON *last_autoip_config;
extern Loop_vdp_common_buffer  	vdp_LanManager_mesg_sync_queue;
int Dhcp_Autoip_Open_JsonPara(cJSON *para)
{
	AUTOIP_CMD_STRU send_cmd;
	int dhcp_timout,autoip_sleep;
	char buff[50];
	p_vdp_common_buffer pdb	  = 0;
	MSG_LAN_MANAGER* msg;
	int act_type;
	printf_json(para, __func__);
	memset(&send_cmd,0,sizeof(AUTOIP_CMD_STRU));
	send_cmd.cmd = C2S_AUTOIP_OPEN_REQ;
	if(MyCjson_datapro(para,DHCP_timout,&dhcp_timout)!=0)
	{
		if(MyCjson_datapro(io_autoip_config,DHCP_timout,&dhcp_timout)!=0)
			dhcp_timout=6;
	}
	if(MyCjson_datapro(para,AUTOIP_sleep,&autoip_sleep)!=0)
	{
		if(MyCjson_datapro(io_autoip_config,AUTOIP_sleep,&autoip_sleep)!=0)
			autoip_sleep=300;
	}
	send_cmd.dhcp_timeout = dhcp_timout;
	send_cmd.autoip_sleep = autoip_sleep;
	if(MyCjson_datapro(para,LanInterface_name,buff)!=0)
	{
		if(MyCjson_datapro(io_autoip_config,LanInterface_name,buff)!=0)
			strcpy(buff,"eth0");
	}
	strcpy(send_cmd.ifname,buff);
	
	if(MyCjson_datapro(para,Autoip_IPSegment,buff)!=0)
	{
		if(MyCjson_datapro(io_autoip_config,Autoip_IPSegment,buff)!=0)
			return -1;
	}
	strcpy(send_cmd.ip_segment,buff);
	if(MyCjson_datapro(para,Autoip_IPMask,buff)!=0)
	{
		if(MyCjson_datapro(io_autoip_config,Autoip_IPMask,buff)!=0)
			return -1;
	}
	strcpy(send_cmd.ip_mask,buff);
	if(MyCjson_datapro(para,Autoip_IPGateway,buff)!=0)
	{
		if(MyCjson_datapro(io_autoip_config,Autoip_IPGateway,buff)!=0)
			return -1;
	}
	strcpy(send_cmd.ip_gateway,buff);
	if(MyCjson_datapro(para,Autoip_LastIP,buff)==0)
	{
		if(strlen(buff)>=7)
		{
			strcpy(send_cmd.last_ip,buff);
		}
	}
	int ver=Dhcp_Autoip_Checkver();
	if(ver<4)
	{
		strcpy(send_cmd.ip_dns,"8.8.8.8");
	}
	while(pop_vdp_common_queue( &vdp_LanManager_mesg_sync_queue, &pdb, 1)>0)
	{
		purge_vdp_common_queue(&vdp_LanManager_mesg_sync_queue);
	}
	if(unix_socket_send_data( &autoip_cliet_socket, (char*)&send_cmd, sizeof(AUTOIP_CMD_STRU))==0)
		return -1;
	set_lan_sync_wait(1);
	//ShellCmdPrintf("^^^^^^^^^%s:%d:\n",__func__,__LINE__);
	if(pop_vdp_common_queue( &vdp_LanManager_mesg_sync_queue, &pdb, 1000*15)>0)
	{
		msg = (MSG_LAN_MANAGER*)pdb;
		if(msg->head.msg_type== MSG_TYPE_LanManager_AutoIPStateUpdate)
		{
			//ShellCmdPrintf("^^^^^^^^^%s:%d:%d\n",__func__,__LINE__,msg->head.msg_sub_type);
			//SetIpActionType(msg->head.msg_sub_type);
			//Dhcp_Autoip_CheckIP();
			act_type=msg->head.msg_sub_type;
			purge_vdp_common_queue(&vdp_LanManager_mesg_sync_queue);
			set_lan_sync_wait(0);
			return act_type;
		}
	}
	set_lan_sync_wait(0);
	return -1;
}
int Dhcp_Autoip_Close(void)
{
	AUTOIP_CMD_STRU send_cmd;
	p_vdp_common_buffer pdb	  = 0;
	MSG_LAN_MANAGER* msg;
	int rev;
	send_cmd.cmd = C2S_AUTOIP_CLOSE_REQ;
	send_cmd.dhcp_timeout = 0;
	send_cmd.autoip_sleep = 0;
	
	while(pop_vdp_common_queue( &vdp_LanManager_mesg_sync_queue, &pdb, 1)>0)
	{
		purge_vdp_common_queue(&vdp_LanManager_mesg_sync_queue);
	}
	if(unix_socket_send_data( &autoip_cliet_socket, (char*)&send_cmd, sizeof(AUTOIP_CMD_STRU))==0)
		return -1;
	if(pop_vdp_common_queue( &vdp_LanManager_mesg_sync_queue, &pdb, 1000*5)>0)
	{
		msg = (MSG_LAN_MANAGER*)pdb;
		if(msg->head.msg_type== MSG_TYPE_LanManager_AutoIPCloseRsp)
		{
			rev=msg->head.msg_sub_type;
			//SetIpActionType(msg->head.msg_sub_type);
			//Dhcp_Autoip_CheckIP();
			purge_vdp_common_queue(&vdp_LanManager_mesg_sync_queue);
			return rev;
		}
	}
	return -1;
}
int CheckSameIP(char *checkip)
{
	char cmd[200];
	sprintf(cmd,"arping -D -c 3 %s",checkip);
	FILE *pf = popen(cmd,"r");
	//int prtline = 0;
	char linestr[250];
	char *pstr1,*pstr2;
	int rev = 0;
	//int sameip_num;
	//int	nw_status = 0;
	//static int nw_status_save = 0xff;
	
	if(pf == NULL)
	{
		//ShellCmdPrintf("open pipe error\n");
		//OS_RetriggerTimer(&timer_NwStatus_Polling);
		return;
	}
	while(fgets(linestr,250,pf) != NULL)
	{
		if((pstr1=strstr(linestr,"Received")) != NULL)
		{
			pstr1+=strlen("Received ");
			if(*pstr1 != '0')
			{
				//*pstr2 = 0;
				rev = 1;//atoi(pstr1);
			}
			//nw_status = 1;
			//break;
		}
		//ShellCmdPrintf("!!!!!!!!!CheckSameIP:%s,%d\n",linestr,rev);
	}
	
	pclose(pf);

	return rev;
	//OS_RetriggerTimer(&timer_NwStatus_Polling);
}

int Dhcp_Autoip_CheckIP(void)
{
	AUTOIP_CMD_STRU send_cmd;
	AUTOIP_RSP_STRU rsp_cmd;
	//send_cmd.cmd = C2S_AUTOIP_CHECKIP_REQ;
	//send_cmd.dhcp_timeout = dhcp_timout;
	//send_cmd.autoip_sleep = autoip_sleep;
	//strcpy(send_cmd.ifname,ifn);
		//czn_20190408_s
	//API_Event_IoServer_InnerRead_All(Autoip_IPSegment, send_cmd.ip_segment);
	//API_Event_IoServer_InnerRead_All(Autoip_IPMask, send_cmd.ip_mask);
	//API_Event_IoServer_InnerRead_All(Autoip_IPGateway, send_cmd.ip_gateway);
	//czn_20190408_e
	//strcpy(send_cmd.ip_segment,GetSysVerInfo_IP());
	my_inet_ntoa(GetLocalIpByDevice("eth0"), send_cmd.ip_segment);
	rsp_cmd.rsp_state=CheckSameIP(GetSysVerInfo_IP());
	rsp_cmd.cmd=S2C_AUTOIP_CHECKIP_RSP;
	autoip_client_socket_recv_data((char*)&rsp_cmd,sizeof(AUTOIP_RSP_STRU));
	//unix_socket_send_data( &autoip_cliet_socket, (char*)&send_cmd, sizeof(AUTOIP_CMD_STRU));
}
#if 0
int Dhcp_Autoip_CheckIP(void)
{
	AUTOIP_CMD_STRU send_cmd;
	char buff[20];

	send_cmd.cmd = C2S_AUTOIP_CHECKIP_REQ;
	//send_cmd.dhcp_timeout = dhcp_timout;
	//send_cmd.autoip_sleep = autoip_sleep;
	//strcpy(send_cmd.ifname,ifn);
		//czn_20190408_s
	//API_Event_IoServer_InnerRead_All(Autoip_IPSegment, send_cmd.ip_segment);
	//API_Event_IoServer_InnerRead_All(Autoip_IPMask, send_cmd.ip_mask);
	//API_Event_IoServer_InnerRead_All(Autoip_IPGateway, send_cmd.ip_gateway);
	//czn_20190408_e
	//strcpy(send_cmd.ip_segment,GetSysVerInfo_IP());
	if(MyCjson_datapro(last_autoip_config,LanInterface_name,buff)!=0)
	{
		if(MyCjson_datapro(io_autoip_config,LanInterface_name,buff)!=0)
			strcpy(buff,"eth0");
	}
	my_inet_ntoa(GetLocalIpByDevice(buff), send_cmd.ip_segment);
	unix_socket_send_data( &autoip_cliet_socket, (char*)&send_cmd, sizeof(AUTOIP_CMD_STRU));
}
#endif
int Dhcp_Autoip_Checkver(void)
{
	AUTOIP_CMD_STRU send_cmd;
	int dhcp_timout,autoip_sleep;
	char buff[50];
	p_vdp_common_buffer pdb	  = 0;
	VDP_MSG_HEAD *msg;
	int act_type;
	send_cmd.cmd = C2S_AUTOIP_CHECKVER_REQ;
	
	while(pop_vdp_common_queue( &vdp_LanManager_mesg_sync_queue, &pdb, 1)>0)
	{
		purge_vdp_common_queue(&vdp_LanManager_mesg_sync_queue);
	}
	if(unix_socket_send_data( &autoip_cliet_socket, (char*)&send_cmd, sizeof(AUTOIP_CMD_STRU))==0)
		return -1;
	//set_lan_sync_wait(1);
	//printf("^^^^^^^^^%s:%d:\n",__func__,__LINE__);
	if(pop_vdp_common_queue( &vdp_LanManager_mesg_sync_queue, &pdb, 200)>0)
	{
		msg = (VDP_MSG_HEAD*)pdb;
		if(msg->msg_type== MSG_TYPE_LanManager_CheckVerRsp)
		{
			//printf("^^^^^^^^^%s:%d:%d\n",__func__,__LINE__,msg->msg_sub_type);
			//SetIpActionType(msg->head.msg_sub_type);
			//Dhcp_Autoip_CheckIP();
			act_type=msg->msg_sub_type;
			purge_vdp_common_queue(&vdp_LanManager_mesg_sync_queue);
			//set_lan_sync_wait(0);
			return act_type;
		}
	}
	
	return 0;
}

/*********************************************************************************************************
**  End Of File
*********************************************************************************************************/
