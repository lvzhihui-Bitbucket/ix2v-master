/**
  ******************************************************************************
  * @file    task_Unlock.h
  * @author  zxj
  * @version V00.01.00
  * @date    2012.08.22
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2012 V-Tec</center></h2>
  ******************************************************************************
  */ 

#ifndef _task_DHCP_H
#define _task_DHCP_H

#include "RTOS.h"
#include "OSQ.h"
#include "OSTIME.h"
#include "task_survey.h"


#define	C2S_AUTOIP_OPEN_REQ			0X01
#define	C2S_AUTOIP_CLOSE_REQ			0X02
#define	S2C_AUTOIP_STATE_UPDATE		0X03
#define	C2S_AUTOIP_CHECKIP_REQ		0X04
#define	C2S_AUTOIP_CHECKVER_REQ		0X05
#define	S2C_AUTOIP_OPEN_RSP			0X81
#define	S2C_AUTOIP_CLOSE_RSP			0X82
#define	S2C_AUTOIP_CHECKIP_RSP		0X84
#define	S2C_AUTOIP_CHECKVER_RSP		0X85

#pragma pack(1)

typedef struct
{
	unsigned char cmd;
	unsigned char ifname[10];
	unsigned char dhcp_timeout;
	unsigned short autoip_sleep;
	char ip_segment[16];		//czn_20190408
	char ip_mask[16];
	char ip_gateway[16];
	char ip_dns[16];
	char last_ip[16];
}AUTOIP_CMD_STRU;

typedef struct
{
	unsigned char cmd;
	unsigned char rsp_state;
}AUTOIP_RSP_STRU;

#pragma pack()

#endif
