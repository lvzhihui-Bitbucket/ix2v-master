/**
  ******************************************************************************
  * @file    task_Unlock.h
  * @author  zxj
  * @version V00.01.00
  * @date    2012.08.22
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2012 V-Tec</center></h2>
  ******************************************************************************
  */ 

#ifndef _task_Lan_Manager_H
#define _task_Lan_Manager_H

#include "RTOS.h"
#include "OSQ.h"
#include "OSTIME.h"
#include "task_survey.h"
#include "cJSON.h"

// Define Task Vars and Structures----------------------------------------------

#define	DHCP_PRINTF

#ifdef	DHCP_PRINTF
#define	DHCP_printf(fmt,...)	printf("[UDP]-["__FILE__"]-[%04d]-"fmt"",__LINE__,##__VA_ARGS__)
#else
#define	DHCP_printf(fmt,...)
#endif

// Define task interface-------------------------------------------------------------
	//resource semaphores------------------------

	//MailBoxes----------------------------------
#pragma pack(1)
typedef struct
{	
	VDP_MSG_HEAD 	head;
	char config_json[];
} MSG_LAN_MANAGER;
#pragma pack()

#define Lan_Policy_Static_Str				"Static"
#define Lan_Policy_AutoIPAndDHCP_Str		"AutoIPAndDHCP"

#define LAN_IP_Policy_None			"None"
#define LAN_IP_Policy_DHCP			"DHCP"
#define LAN_IP_Policy_AutoIP			"AutoIP"
#define LAN_IP_Policy_StaticIP			"StaticIP"

#define Lan_State_Key_Sw			"LAN-SW"
#define Lan_State_Key_State			"LAN-STATE"
#define Lan_State_Key_IPPolicy		"LAN-IP-Policy"
#define Lan_State_Key_IPAddr			"LAN-IP_Address"
#define Lan_State_Key_SubnetMask	"LAN-SubnetMask"
#define Lan_State_Key_DefaultRoute 	"LAN-DefaultRoute"
#define Lan_State_Key_MACAddr		"LAN-MACAddr"

//msg_type

#define MSG_TYPE_LanManager_SetConfig		0
#define MSG_TYPE_LanManager_AutoIPStateUpdate	1
#define MSG_TYPE_LanManager_CheckIPRsp	2
#define MSG_TYPE_LanManager_AutoIPCloseRsp	3
#define MSG_TYPE_LanManager_InternetCheck		4
#define MSG_TYPE_LanManager_CheckVerRsp	5
#define MSG_TYPE_LanManager_Reload		6

// Define Task 2 items----------------------------------------------------------
void vtk_TaskInit_LanManager(int priority);
int API_Set_Lan_Config(cJSON *Lan_Config);
cJSON* API_Get_Lan_State(void);



#endif
                                                                                                                                                                                                                                                                                                                                                                                  