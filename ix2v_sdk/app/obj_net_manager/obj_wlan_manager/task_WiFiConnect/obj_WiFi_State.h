/**
  ******************************************************************************
  * @file    obj_Unlock_State.h
  * @author  zxj
  * @version V00.01.00
  * @date    2012.08.22
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2012 V-Tec</center></h2>
  ******************************************************************************
  */ 

#ifndef _obj_WiFi_State_H
#define _obj_WiFi_State_H

#include "OSTIME.h"
#include "RTOS.h"
#include "iwlist.h"
#include "cJSON.h"
#define WIFI_SSID_LENGTH	50
#define WIFI_PWD_LENGTH		32


// Define Object Property-------------------------------------------------------
typedef struct UNLOCK_RUN_STRU	
{
	OS_TIMER			connectPollingTimer;//定时检测wifi连接状态
	unsigned int		wifiSwitch;			//0 wifi关闭， 1 wifi开启
	unsigned int		wifiConnect;		//0断开， 1连接中， 2连接成功
	unsigned int		wifiSearch;			//1正在搜索列表， 0搜索列表结束
	struct wifi_info 	curWifi; 			//当前连接的wifi信息
	struct wifi_data 	wifiData;			//搜索的wifi列表
	//char				saveSsid[3][WIFI_SSID_LENGTH+1];	//保存最近连接的3个wifi
	//char				savePwd[3][WIFI_PWD_LENGTH+1];		//保存最近连接的3个wifi
	cJSON			*ssid_array;
	char				saveCurSsid[WIFI_SSID_LENGTH+1];	//保存当前wifi
	char				saveCurPwd[WIFI_PWD_LENGTH+1];		//保存当前wifi密码
	unsigned int		connectWifiNum;		//连接保存的3个wifi中的第几个
	unsigned int		updateReportFlag;	//固件更新报告标记
	unsigned int		pollingCycle;		//定时检测周期 s
	unsigned int		dhcpState;			//DHCP状态
	unsigned int		networkEnable;		//外网是否通
	//cao_20181020_s
	char				connectingSsid[WIFI_SSID_LENGTH+1];
	//cao_20181020_e
	int					stopConneting;
	unsigned int		tryConnetTime;
}WIFI_RUN;
extern WIFI_RUN wifiRun;


// Define Object Function - Public----------------------------------------------



// Define Object Function - Private---------------------------------------------


#endif
