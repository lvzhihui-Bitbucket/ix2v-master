/**
  ******************************************************************************
  * @file    task_Unlock.c
  * @author  zxj
  * @version V00.01.00
  * @date    2012.08.22
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2012 V-Tec</center></h2>
  ******************************************************************************
  */ 
#include "task_WiFiConnect.h"
//#include "task_IoServer.h"
//#include "task_VideoMenu.h"
#include "cJSON.h"
#include "obj_SYS_VER_INFO.h"
#include "obj_IoInterface.h"
#include "task_Shell.h"
#include "obj_PublicInformation.h"
//#include "iwlist.h"
	  
Loop_vdp_common_buffer  	vdp_WiFi_mesg_queue;
Loop_vdp_common_buffer  	vdp_WiFi_sync_queue;
vdp_task_t			  		task_WiFi;
	  
void vdp_WiFi_mesg_data_process(char* msg_data, int len);
void* vdp_WiFi_task( void* arg );
void Init_Wlan_Inform_Callback_List(void);
int Add_Wlan_Inform_Callback(Wlan_Fun_Callback_t callback_fun);
int Del_Wlan_Inform_Callback(Wlan_Fun_Callback_t callback_fun);
void wlan_inform_callback_test(int inform_code,void *ext_para);
int Load_Wlan_Config_FromIo(void);
static char wlan_gw_save[20]={0};

struct list_head Wlan_Inform_Callback_List;

void vtk_TaskInit_WiFi(int priority)
{
	init_vdp_common_queue(&vdp_WiFi_mesg_queue, 500, (msg_process)vdp_WiFi_mesg_data_process, &task_WiFi);
	init_vdp_common_queue(&vdp_WiFi_sync_queue, 500, NULL,									&task_WiFi);
	init_vdp_common_task(&task_WiFi, MSG_ID_WiFi, vdp_WiFi_task, &vdp_WiFi_mesg_queue, &vdp_WiFi_sync_queue);
	
	wifi_printf("vdp_WiFi_task starting............\n");
}

void exit_vdp_WiFi_task(void)
{
	exit_vdp_common_queue(&vdp_WiFi_mesg_queue);
	exit_vdp_common_queue(&vdp_WiFi_sync_queue);
	exit_vdp_common_task(&task_WiFi);
}
  
void* vdp_WiFi_task( void* arg )
{
	vdp_task_t*  ptask		  = (vdp_task_t*)arg;
	p_vdp_common_buffer pdb	  = 0;
	int size;
	
	WiFiStateInit();
	Init_Wlan_Inform_Callback_List();
	API_Add_Wlan_Inform_Callback(wlan_inform_callback_test);
	API_PublicInfo_Write(PB_WLAN_STATE,API_Get_Wlan_State());
	Load_Wlan_Config_FromIo();
	ptask->task_StartCompleted = 1;
	while( ptask->task_run_flag )
	{
		size = pop_vdp_common_queue( ptask->p_msg_buf, &pdb, VDP_QUEUE_POLLING_TIME);
		if( size > 0 )
		{
			(*ptask->p_msg_buf->process)(pdb,size);

			purge_vdp_common_queue( ptask->p_msg_buf );
		}
	}
	return 0;
}

 int wifiTaskIsEanble;

void WifiSycResponse( MSG_WIFI* msg)
{
	MSG_WIFI  data;

	data.head.msg_target_id = msg->head.msg_source_id;
	data.head.msg_source_id	= msg->head.msg_target_id;			
	data.head.msg_type		= (msg->head.msg_type|COMMON_RESPONSE_BIT);
	data.head.msg_sub_type	= 0;

	vdp_task_t* pTask = GetTaskAccordingMsgID(data.head.msg_target_id);	
	if((pTask != NULL) && (pTask != &task_WiFi))
	{
		push_vdp_common_queue(pTask->p_syc_buf, (char*)&data, sizeof(data.head));
	}
}
#if 0
char* CreateWifiJsonData(char* ssid, char* pwd)
{
    cJSON *root = NULL;
	char *string = NULL;


    root = cJSON_CreateObject();
	
	cJSON_AddStringToObject(root, "ssid", ssid);
	cJSON_AddStringToObject(root, "pwd", pwd);

	string = cJSON_Print(root);

	cJSON_Delete(root);

	return string;
}

int ParseWifiJsonData(const char* json, char* ssid, char* pwd)
{
    int status = 0;

    /* ����һ�����ڽ����� cJSON �ṹ */
    cJSON *root = cJSON_Parse(json);
    if (root == NULL)
    {
        const char *error_ptr = cJSON_GetErrorPtr();
        if (error_ptr != NULL)
        {
            fprintf(stderr, "Error before: %s\n", error_ptr);
        }
        status = 0;
        goto end;
    }

	if(ssid != NULL)
	{
		ParseJsonString(root, "ssid", ssid, 15);
	}
	
	if(pwd != NULL)
	{
		ParseJsonString(root, "pwd", pwd, 12);
	}
	
    status = 1;

end:
	
    cJSON_Delete(root);
	
    return status;
}
#endif
void vdp_WiFi_mesg_data_process(char* msg_data,int len)
{
	MSG_WIFI* msg;
	int i;
	char* wifiData;

	msg = (MSG_WIFI*)msg_data;
	wifiTaskIsEanble = 0;

	switch(msg->head.msg_type )
	{
		case MSG_TYPE_WIFI_OPEN:
			//WiFiOpen();
			Wlan_Open(msg);
			break;
		case MSG_TYPE_WIFI_CLOSE:
			Wlan_Close(msg);
			SipNetworkManage_WlanDisconnected();
			// lzh_20181025_s			
			//WifiSycResponse(msg);
			//WifiSycResponse(msg);
			// lzh_20181025_e	
			break;
		case MSG_TYPE_WIFI_CONNECT:
			Wlan_ConnectSsid(msg);
			break;
		case MSG_TYPE_WIFI_INGNORE:
			Wlan_IngnoreSsid(msg);
			break;
		case MSG_TYPE_WIFI_SEARCH:
			WiFiSearch();
			break;
		case MSG_TYPE_WIFI_POLLING:
			//if(!IfCallServerBusy())
			{
				start_wpa_supplicant_server("wlan0");
				WiFiPolling();
			}
			#if 0
			else
			{
				if(wifiRun.wifiConnect != 2)
				{
					stop_wpa_supplicant_server();
				}
			}
			#endif
			break;
		case MSG_TYPE_WIFI_ADD_CALLBACK:
			Add_Wlan_Inform_Callback(msg->callback);
			break;
		case MSG_TYPE_WIFI_DEL_CALLBACK:
			Del_Wlan_Inform_Callback(msg->callback);
			break;
	}
	
	wifiTaskIsEanble = 1;
}

static int IfWifiIsBusy(void)
{
	uint8 wait_s_count;
	
	if (wifiTaskIsEanble)	//�������
	{
		;
	}
	else
	{
		wait_s_count = 10+200;	//���ʱ20S
		while (wait_s_count > 10)	
		{
			usleep(100000);			//ÿ����ʱ100ms
			wait_s_count--;
			if (wifiTaskIsEanble)
			{
				wait_s_count = 0;		
			}
		}	
		if (wait_s_count != 0)
		{
			return (1);	//����ʧ��
		}
	}
	return 0;
}


int JudgeIfWifiConnected(void)
{
	if(wifiRun.wifiSwitch&&wifiRun.wifiConnect==2)
	{
		return 1;
	}
	return 0;
}

char*  get_wlan_gw_save(void)
{
	return wlan_gw_save;
}
void update_wlan_gw_save(void)
{
	int gateway;
	gateway = GetLocalGatewayByDevice(NET_WLAN0);
	if(gateway != -1)
	{
		my_inet_ntoa(gateway, wlan_gw_save);
	}
	//strcpy(lan_gw_save,gw);
}
/******************************
@ssid_array:
ssid_name&pwd array
one item:
{
  			"SSID_NAME":"XXX",
      			"SSID_PWD":"XXXXXXXX"
}

Example:
[
  		{
  			"SSID_NAME":"VTK2",
      			"SSID_PWD":"123456789"
  		},
  		{
  			"SSID_NAME":"ASUS",
      			"SSID_PWD":"123456789"
  		}
]
*******************************/
int API_Wlan_Open(cJSON *ssid_array)
{
	MSG_WIFI *pmsg;	
	char *config_str=NULL;
	int msg_len;
	int rev;
	if(ssid_array!=NULL)
	{
		config_str=cJSON_Print(ssid_array);
		if(config_str==NULL)
			return -1;
		msg_len=sizeof(VDP_MSG_HEAD)+sizeof(Wlan_Fun_Callback_t)+strlen(config_str)+1;
		pmsg=(MSG_WIFI *)malloc(msg_len);
		if(pmsg==NULL)
			return -1;
		strcpy(pmsg->config_json,config_str);
		free(config_str);
	}
	else
	{
		msg_len=sizeof(VDP_MSG_HEAD)+sizeof(Wlan_Fun_Callback_t)+1;
		pmsg=(MSG_WIFI *)malloc(msg_len);
		pmsg->config_json[0]=0;
	}
	//UdhcpcClose();
	pmsg->callback=NULL;
	
	pmsg->head.msg_source_id	= GetMsgIDAccordingPid(pthread_self());
	pmsg->head.msg_target_id	= MSG_ID_WiFi;
	pmsg->head.msg_type		= MSG_TYPE_WIFI_OPEN;
	pmsg->head.msg_sub_type	= 0;
	//vdp_task_t* pTask = OS_GetTaskID();
	//strcpy(pmsg->config_json,config_str);
	
	
	rev=push_vdp_common_queue(&vdp_WiFi_mesg_queue,  (char*)pmsg, msg_len);
	free(pmsg);
	return rev;

}

int API_Wlan_Close(void)
{
	MSG_WIFI *pmsg;	
	char *config_str=NULL;
	int msg_len;
	int rev;
	
	msg_len=sizeof(VDP_MSG_HEAD)+sizeof(Wlan_Fun_Callback_t)+1;
	pmsg=(MSG_WIFI *)malloc(msg_len);
	pmsg->config_json[0]=0;
	
	//UdhcpcClose();
	pmsg->callback=NULL;
	
	pmsg->head.msg_source_id	= GetMsgIDAccordingPid(pthread_self());
	pmsg->head.msg_target_id	= MSG_ID_WiFi;
	pmsg->head.msg_type		= MSG_TYPE_WIFI_CLOSE;
	pmsg->head.msg_sub_type	= 0;
	//vdp_task_t* pTask = OS_GetTaskID();
	//strcpy(pmsg->config_json,config_str);
	
	
	rev=push_vdp_common_queue(&vdp_WiFi_mesg_queue,  (char*)pmsg, msg_len);
	free(pmsg);
	return rev;
}

int API_Wlan_Search(void)
{
	MSG_WIFI *pmsg;	
	char *config_str=NULL;
	int msg_len;
	int rev;
	
	msg_len=sizeof(VDP_MSG_HEAD)+sizeof(Wlan_Fun_Callback_t)+1;
	pmsg=(MSG_WIFI *)malloc(msg_len);
	pmsg->config_json[0]=0;
	//UdhcpcClose();
	pmsg->callback=NULL;
	
	pmsg->head.msg_source_id	= GetMsgIDAccordingPid(pthread_self());
	pmsg->head.msg_target_id	= MSG_ID_WiFi;
	pmsg->head.msg_type		= MSG_TYPE_WIFI_SEARCH;
	pmsg->head.msg_sub_type	= 0;
	//vdp_task_t* pTask = OS_GetTaskID();
	//strcpy(pmsg->config_json,config_str);
	
	
	rev=push_vdp_common_queue(&vdp_WiFi_mesg_queue,  (char*)pmsg, msg_len);
	
	free(pmsg);
	return rev;
}

/******************************
@ssid_array:
ssid_name&pwd array
only contain one item:
[
{
  			"SSID_NAME":"XXX",
      			"SSID_PWD":"XXXXXXXX"
}
]
*******************************/
int API_Wlan_ConnectSsid(cJSON *ssid_array, int ifSave)
{
	MSG_WIFI *pmsg;	
	char *config_str=NULL;
	int msg_len;
	int rev;
	if(ssid_array!=NULL)
	{
		config_str=cJSON_Print(ssid_array);
		if(config_str==NULL)
			return -1;
		msg_len=sizeof(VDP_MSG_HEAD)+sizeof(Wlan_Fun_Callback_t)+strlen(config_str)+1;
		pmsg=(MSG_WIFI *)malloc(msg_len);
		if(pmsg==NULL)
			return -1;
		strcpy(pmsg->config_json,config_str);
		free(config_str);
	}
	else
	{
		return -1;
	}
	//UdhcpcClose();
	pmsg->callback=NULL;
	
	pmsg->head.msg_source_id	= GetMsgIDAccordingPid(pthread_self());
	pmsg->head.msg_target_id	= MSG_ID_WiFi;
	pmsg->head.msg_type		= MSG_TYPE_WIFI_CONNECT;
	pmsg->head.msg_sub_type	= ifSave;
	//vdp_task_t* pTask = OS_GetTaskID();
	//strcpy(pmsg->config_json,config_str);
	
	
	rev=push_vdp_common_queue(&vdp_WiFi_mesg_queue,  (char*)pmsg, msg_len);
	
	free(pmsg);
	return rev;
}
/******************************
@ssid_array:
ssid_name&pwd array
only contain one item:
[
{
  			"SSID_NAME":"XXX",
      			"SSID_PWD":"XXXXXXXX"
}
]
*******************************/
int API_Wlan_IngnoreSsid(cJSON *ssid_array)
{
	MSG_WIFI *pmsg;	
	char *config_str=NULL;
	int msg_len;
	int rev;
	if(ssid_array!=NULL)
	{
		config_str=cJSON_Print(ssid_array);
		if(config_str==NULL)
			return -1;
		msg_len=sizeof(VDP_MSG_HEAD)+sizeof(Wlan_Fun_Callback_t)+strlen(config_str)+1;
		pmsg=(MSG_WIFI *)malloc(msg_len);
		if(pmsg==NULL)
			return -1;
		strcpy(pmsg->config_json,config_str);
		free(config_str);
	}
	else
	{
		return -1;
	}
	//UdhcpcClose();
	pmsg->callback=NULL;
	
	pmsg->head.msg_source_id	= GetMsgIDAccordingPid(pthread_self());
	pmsg->head.msg_target_id	= MSG_ID_WiFi;
	pmsg->head.msg_type		= MSG_TYPE_WIFI_INGNORE;
	pmsg->head.msg_sub_type	= 0;
	//vdp_task_t* pTask = OS_GetTaskID();
	//strcpy(pmsg->config_json,config_str);
	
	
	rev=push_vdp_common_queue(&vdp_WiFi_mesg_queue,  (char*)pmsg, msg_len);
	
	free(pmsg);
	return rev;
}

int API_Add_Wlan_Inform_Callback(Wlan_Fun_Callback_t func)
{
	MSG_WIFI *pmsg;	
	char *config_str=NULL;
	int msg_len;
	int rev;
	
	msg_len=sizeof(VDP_MSG_HEAD)+sizeof(Wlan_Fun_Callback_t)+1;
	pmsg=(MSG_WIFI *)malloc(msg_len);
	pmsg->config_json[0]=0;
	//UdhcpcClose();
	pmsg->callback=func;
	
	pmsg->head.msg_source_id	= GetMsgIDAccordingPid(pthread_self());
	pmsg->head.msg_target_id	= MSG_ID_WiFi;
	pmsg->head.msg_type		= MSG_TYPE_WIFI_ADD_CALLBACK;
	pmsg->head.msg_sub_type	= 0;
	//vdp_task_t* pTask = OS_GetTaskID();
	//strcpy(pmsg->config_json,config_str);
	
	
	rev=push_vdp_common_queue(&vdp_WiFi_mesg_queue,  (char*)pmsg, msg_len);
	
	free(pmsg);
	return rev;
}

int API_Del_Wlan_Inform_Callback(Wlan_Fun_Callback_t func)
{
	MSG_WIFI *pmsg;	
	char *config_str=NULL;
	int msg_len;
	int rev;
	
	msg_len=sizeof(VDP_MSG_HEAD)+sizeof(Wlan_Fun_Callback_t)+1;
	pmsg=(MSG_WIFI *)malloc(msg_len);
	pmsg->config_json[0]=0;
	//UdhcpcClose();
	pmsg->callback=func;
	
	pmsg->head.msg_source_id	= GetMsgIDAccordingPid(pthread_self());
	pmsg->head.msg_target_id	= MSG_ID_WiFi;
	pmsg->head.msg_type		= MSG_TYPE_WIFI_DEL_CALLBACK;
	pmsg->head.msg_sub_type	= 0;
	//vdp_task_t* pTask = OS_GetTaskID();
	//strcpy(pmsg->config_json,config_str);
	
	
	rev=push_vdp_common_queue(&vdp_WiFi_mesg_queue,  (char*)pmsg, msg_len);
	
	free(pmsg);
	return rev;
}

/****************************
return:
{
"WLAN-SW":"ON"/"OFF"
"WLAN-STATE"(@"WLAN-SW"="ON"):"Connecting":"Connected"
"WLAN-IP-Policy"(@"WLAN-STATE"="Connected"):"DHCP"/"StaticIP"
"WLAN-IP_Addr"(@"WLAN-STATE"="Connected"):
{
  IP_Address:"XXX.XXX.XXX.XXX",
  SubnetMask:"XXX.XXX.XXX.XXX",
  DefaultRoute:"XXX.XXX.XXX.XXX"
  MACAddr:"XX:XX:XX:XX:XX:XX"
}
}
************************/
int GetWifiQuality(uint8 quality)
{
	int level=0;
	level|=quality;
	if(quality&0x80)
		level|=0xffffff00;
	if(level >= -55)
	{
		return 4;
	}
	else if(level >= -70)
	{
		
		return 3;
	}
	else if(level >= -85)
	{
		
		return 2;
	}
	else
	{

		return 1;
	}

}
cJSON* API_Get_Wlan_State(void)
{
	static cJSON *wlan_state=NULL;
	cJSON *wlan_ip;
	cJSON *wifi_ssid_pwd;
	cJSON *wifi_info;
	char addr_buff[21];
	char temp[6];
	int gateway;
	
	if(wlan_state!=NULL)
		cJSON_Delete(wlan_state);
	
	wlan_state=cJSON_CreateObject();
	if(wlan_state==NULL)
		return NULL;
	if(wifiRun.wifiSwitch==0)
	{
		cJSON_AddStringToObject(wlan_state,Wlan_State_Key_Sw,"OFF");
		return wlan_state;
	}
	cJSON_AddStringToObject(wlan_state,Wlan_State_Key_Sw,"ON");
	
	if(wifiRun.wifiConnect<2)
	{
		//cJSON_AddStringToObject(lan_state,Lan_State_Key_IPPolicy,LAN_IP_Policy_None);
		//cJSON_AddStringToObject(wlan_state,Wlan_State_Key_State,"Connecting");
		cJSON_AddStringToObject(wlan_state,Wlan_State_Key_State,"Disconneted");
		return wlan_state;
	}
	cJSON_AddStringToObject(wlan_state,Wlan_State_Key_State,"Connected");
	
	cJSON_AddStringToObject(wlan_state,Wlan_State_Key_IPPolicy,"DHCP");
	

	
	wlan_ip=cJSON_AddObjectToObject(wlan_state,"WLAN-IP_Addr");
	if(wlan_ip!=NULL)
	{
		my_inet_ntoa(GetLocalIpByDevice(NET_WLAN0), addr_buff);
		cJSON_AddStringToObject(wlan_ip,Wlan_State_Key_IPAddr,addr_buff);
		
		my_inet_ntoa(GetLocalMaskAddrByDevice(NET_WLAN0), addr_buff);
		cJSON_AddStringToObject(wlan_ip,Wlan_State_Key_SubnetMask,addr_buff);

		//gateway = GetLocalGatewayByDevice(NET_WLAN0);
		//if(gateway != -1)
		{
			//my_inet_ntoa(gateway, addr_buff);
			cJSON_AddStringToObject(wlan_ip,Wlan_State_Key_DefaultRoute,get_wlan_gw_save());
		}

		GetLocalMacByDevice(NET_WLAN0, temp);
		snprintf(addr_buff, 18, "%02x:%02x:%02x:%02x:%02x:%02x", temp[0], temp[1], temp[2], temp[3], temp[4], temp[5]);
		cJSON_AddStringToObject(wlan_ip,Wlan_State_Key_MACAddr,addr_buff);
	}
	#if 0
	wifi_ssid_pwd=cJSON_AddObjectToObject(wlan_state,"SSID&PWD");
	if(wifi_ssid_pwd!=NULL)
	{
		cJSON_AddStringToObject(wifi_ssid_pwd,"SSID_NAME",wifiRun.saveCurSsid);
		//cJSON_AddStringToObject(wifi_ssid_pwd,"SSID_PWD",wifiRun.saveCurPwd);
	}
	#endif
	cJSON_AddStringToObject(wlan_state,"SSID_NAME",wifiRun.saveCurSsid);
	#if 0
	wifi_info=cJSON_AddObjectToObject(wlan_state,"WIFI-INFO");
	if(wifi_info!=NULL)
	{
		int level=0;
		level|=wifiRun.curWifi.LEVEL;
		if(level&0x80)
			level|=0xffffff00;
		cJSON_AddNumberToObject(wifi_info,"QUALITY",level);
		
	}
	#endif
	int level=0;
	level|=wifiRun.curWifi.LEVEL;
	if(level&0x80)
		level|=0xffffff00;
	cJSON_AddNumberToObject(wlan_state,"QUALITY",level);
	cJSON_AddNumberToObject(wlan_state,"QUALITY_LEVEL",GetWifiQuality(wifiRun.curWifi.LEVEL));
	return wlan_state;
}	
int Load_Wlan_Config_FromIo(void)
{
	cJSON *ssid_array;
	//char *pjson_str;
	char lan_policy[50];
		
	if(API_Para_Read_Int(WIFI_SWITCH))
	{
		ssid_array=API_Para_Read_Public(WIFI_SSID_PARA);
		if(ssid_array!=NULL)
			WiFiOpen(ssid_array);
	}
	return 0;
}
void Wlan_Open(MSG_WIFI* msg)
{	
	cJSON *ssid_array;
	if(msg->config_json[0]!=0)
	{
		ssid_array=cJSON_Parse(msg->config_json);
	}
	else
	{
		ssid_array=API_Para_Read_Public(WIFI_SSID_PARA);
	}
	if(ssid_array==NULL)
		return;
	WiFiOpen(ssid_array);
}

void Wlan_Close(MSG_WIFI* msg)
{	
	WiFiClose();
}

void Wlan_ConnectSsid(MSG_WIFI* msg)
{	
	cJSON *ssid_array;
	char			wifi_ssid[WIFI_SSID_LENGTH+1];
	char			wifi_pwd[WIFI_PWD_LENGTH+1];
	int index;
	int ret=0;
	if(wifiRun.wifiSwitch==0)
		return;
	if(msg->config_json[0]!=0)
	{
		ssid_array=cJSON_Parse(msg->config_json);
	}
	else
	{
		return;
	}
	if(ssid_array==NULL)
		return;
	if(cJSON_IsArray(ssid_array))
	{
		if(Get_OneSsidItem(ssid_array,0,wifi_ssid,wifi_pwd)!=0)
			ret=-1;
	}
	else
	{
		if(MyCjson_datapro(ssid_array,SSID_NAME,wifi_ssid)!=0)
			ret=-1;
		if(MyCjson_datapro(ssid_array,SSID_PWD,wifi_pwd)!=0)
			wifi_pwd[0]=0;
	}
	if(ret==0)
	{
		start_wpa_supplicant_server("wlan0");
		index=JudgeIfContainSsid(wifiRun.ssid_array,wifi_ssid);
		if(index>0)
		{
			cJSON_DeleteItemFromArray(wifiRun.ssid_array,index-1);
		}
		cJSON_InsertItemInArray(wifiRun.ssid_array,0,cJSON_Duplicate(cJSON_IsArray(ssid_array)?cJSON_GetArrayItem(ssid_array,0):ssid_array,1));
		
		//是否需要保存
		if(msg->head.msg_sub_type)
		{
			API_Para_Write_PublicByName(WIFI_SSID_PARA, wifiRun.ssid_array);
		}

		if(WiFiConnect(wifi_ssid,wifi_pwd) != 0)
		{
			
			//API_add_Inform_with_data_to_VideoMenu_queue(MSG_7_BRD_SUB_WIFI_CONNECT_FAILED, wifiData, strlen(wifiData)+1);
			
		}
	}
	cJSON_Delete(ssid_array);
}

void Wlan_IngnoreSsid(MSG_WIFI* msg)
{
	cJSON *ssid_array;
	char			wifi_ssid[WIFI_SSID_LENGTH+1];
	char			wifi_pwd[WIFI_PWD_LENGTH+1];
	int index;
	if(msg->config_json[0]!=0)
	{
		ssid_array=cJSON_Parse(msg->config_json);
	}
	else
	{
		return;
	}
	if(ssid_array==NULL)
		return;
	if(Get_OneSsidItem(ssid_array,0,wifi_ssid,wifi_pwd)==0)
	{
		start_wpa_supplicant_server("wlan0");
		index=JudgeIfContainSsid(wifiRun.ssid_array,wifi_ssid);
		if(index>0)
		{
			cJSON_DeleteItemFromArray(wifiRun.ssid_array,index-1);
			if(IfWifiIsConnected(wifi_ssid))
			{
				WiFiDisconnect();	
			}
		}
	
	}
	cJSON_Delete(ssid_array);
}


int Add_Wlan_Inform_Callback(Wlan_Fun_Callback_t callback_fun)
{
	struct list_head *plist;
	Wlan_Inform_Callback_List_t* pnode;
	list_for_each(plist,&Wlan_Inform_Callback_List)
	{
		pnode = list_entry(plist,Wlan_Inform_Callback_List_t,list);
		if(pnode->callback_fun==callback_fun)
			return 0;
	}
	pnode=malloc(sizeof(Wlan_Inform_Callback_List_t));
	if(pnode==NULL)
		return -1;
	pnode->callback_fun=callback_fun;
	list_add_tail(&pnode->list,&Wlan_Inform_Callback_List);
	return 0;
}
int Del_Wlan_Inform_Callback(Wlan_Fun_Callback_t callback_fun)
{
	struct list_head *plist;
	Wlan_Inform_Callback_List_t* pnode;
	list_for_each(plist,&Wlan_Inform_Callback_List)
	{
		pnode = list_entry(plist,Wlan_Inform_Callback_List_t,list);
		if(pnode->callback_fun==callback_fun)
		{
			list_del_init(&pnode->list); 
			free(pnode);
			return 0;
		}	
	}
	return 0;
}

void Init_Wlan_Inform_Callback_List(void)
{
	INIT_LIST_HEAD(&Wlan_Inform_Callback_List); 
}

void Wlan_Inform_Callback_Action(int inform_code,void *ext_para)
{
	struct list_head *plist;
	Wlan_Inform_Callback_List_t* pnode;
	list_for_each(plist,&Wlan_Inform_Callback_List)
	{
		pnode = list_entry(plist,Wlan_Inform_Callback_List_t,list);
		if(pnode->callback_fun!=NULL)
			(*pnode->callback_fun)(inform_code,ext_para);
	}
}

void wlan_manager_test_get_state(void)
{
			//if(pbuf[1]==1&&pbuf[2]==1)
			{
				char *wlan_status_str;
				cJSON *wlan_status;
				wlan_status=API_Get_Wlan_State();
				if(wlan_status!=NULL)
				{
					wlan_status_str= cJSON_Print(wlan_status);
					cJSON_Delete(wlan_status);
					if(wlan_status_str!=NULL)
					{
						printf("lan_state:\n%s\n",wlan_status_str);
						free(wlan_status_str);
					}
					
					
				}
			}
}
void wlan_manager_test_set_config(void)
{
	cJSON *wlan_config_arr;
	char *lan_config_str;
	static int set_index=0;
	//int set_num;
	if(set_index==0)
	{
		
		API_Wlan_Open(NULL);	
	}
	#if 1
	else if(set_index==1)
	{
		wlan_config_arr=GetJsonFromFile("/mnt/nfs/WanManagerParaTest.json");
		if(wlan_config_arr!=0)
		{
			API_Wlan_Open(wlan_config_arr);
		}
	}
	#endif
	else if(set_index<4)
	{
		API_Wlan_Search();
	}
	
	else
	{
		API_Wlan_Close();
	}
	if(++set_index>=5)
	{
		set_index=0;
	}
}
static time_t shell_op_time=0;
void update_shell_op_time(void)
{
	shell_op_time=time(NULL);
}
void wlan_inform_callback_test(int inform_code,void *ext_para)
{
	cJSON *wlan_result;

	struct wifi_data *search_result;
	time_t now=time(NULL);
	int i;
	int shell_op_flag=0;
	if((now-shell_op_time)<300)
	{
		shell_op_flag=1;
	}
	switch(inform_code)
	{
		case WLAN_INFORM_CODE_OPEN:
			printf("!!!!!!!!!wlan_inform:open\n");
			if(shell_op_flag)
				ShellCmdPrintf("wlan_inform:open");
			break;
		case WLAN_INFORM_CODE_CLOSE:
			printf("!!!!!!!!!wlan_inform:close\n");
			if(shell_op_flag)
				ShellCmdPrintf("wlan_inform:close");
			break;
		case WLAN_INFORM_CODE_DISCONNECT:
			printf("!!!!!!!!!wlan_inform:disconnect\n");
			
			//lv_msg_send(MSG_WLAN_RESULT,"disconnect");

			if(shell_op_flag)
				ShellCmdPrintf("wlan_inform:disconnect");
			break;	
		case WLAN_INFORM_CODE_CONNECTING:
			printf("!!!!!!!!!wlan_inform:connecting:%s\n",(char*)ext_para);

			//lv_msg_send(MSG_WLAN_RESULT,(char*)ext_para);

			if(shell_op_flag)
				ShellCmdPrintf("wlan_inform:connecting:%s",(char*)ext_para);
			break;	
		case WLAN_INFORM_CODE_CONNECTED:
			printf("!!!!!!!!!wlan_inform:connected:%s\n",(char*)ext_para);
			vtk_lvgl_lock();
			lv_msg_send(MSG_WLAN_RESULT,(char*)ext_para);
			vtk_lvgl_unlock();
			if(shell_op_flag)
				ShellCmdPrintf("wlan_inform:connected:%s",(char*)ext_para);
			Api_Network_GetSn(NET_WLAN0);
			break;	
		case WLAN_INFORM_CODE_SEARCHING:
			printf("!!!!!!!!!wlan_inform:searching\n");
			if(shell_op_flag)
				ShellCmdPrintf("wlan_inform:searching");
			break;	
		case WLAN_INFORM_CODE_SEARCH_OVER:
			search_result=(struct wifi_data *)ext_para;
			printf("!!!!!!!!!wlan_inform:search_over\n");
			if(shell_op_flag)
				ShellCmdPrintf("wlan_inform:search_over");
				
			vtk_lvgl_lock();

			wlan_result = cJSON_CreateArray();

			for(i=0;i<search_result->DataCnt;i++)
			{
				printf("wifi %d/%d:%s\n",i+1,search_result->DataCnt,search_result->WifiInfo[i].ESSID);
				
				//lv_msg_send(MSG_WLANCONFIG_RESULT,search_result->WifiInfo[i].ESSID);
				//20240719 yxc add
				cJSON_AddItemToArray(wlan_result,cJSON_CreateString(search_result->WifiInfo[i].ESSID));
				
				if(shell_op_flag){
					ShellCmdPrintf("wifi %d/%d:%s",i+1,search_result->DataCnt,search_result->WifiInfo[i].ESSID);					
				}
					
			}
			//20240719 yxc add
			vtk_lvgl_lock();
			lv_msg_send(MSG_WLANCONFIG_RESULT,wlan_result);
			vtk_lvgl_unlock();
			cJSON_Delete(wlan_result);

			if(wifiRun.wifiConnect==2)
				lv_msg_send(MSG_WLAN_RESULT,(char*)wifiRun.saveCurSsid);
			vtk_lvgl_unlock();
			break;	
			
		default:
			break;
	}
}

cJSON *get_wlan_ssid(void)
{
	if(wifiRun.wifiSwitch==0)
		return NULL;
	return wifiRun.ssid_array;
}

static int EnWifiMute=1;
int SetWifiMute(int val)
{
	EnWifiMute=val;
}
int IfEnWifiMute(void)
{
	return EnWifiMute;
}

static int WifiMute_State_Save= 0;
static cJSON *WifiMute_Ssid_Save=NULL;

int WifiMute_Process(cJSON *cmd)
{
	char *pstr;
	if(IfEnWifiMute()==0)
		return 1;
	if(strcmp("Ack",API_PublicInfo_Read_String(PB_CALL_SER_STATE))==0)
	{
		if(API_PublicInfo_Read_Bool(PB_CALL_SIP)==0)
		{
			if((pstr=API_PublicInfo_Read_String(Wlan_State_Key_Sw))!=NULL)
			{
				if(strcmp(pstr,"ON")==0)
				{
					WifiMute_State_Save=1;
					if(get_wlan_ssid()!=NULL)
					{
						if(WifiMute_Ssid_Save!=NULL)
							cJSON_Delete(WifiMute_Ssid_Save);
						WifiMute_Ssid_Save=cJSON_Duplicate(get_wlan_ssid(),1);
					}
					API_Wlan_Close();
				}
			}	
		}
	}
	else if(strcmp("Wait",API_PublicInfo_Read_String(PB_CALL_SER_STATE))==0)
	{
		if(WifiMute_State_Save==1)
		{
			WifiMute_State_Save=0;
			API_Wlan_Open(WifiMute_Ssid_Save);
		}
	}
	return 1;
}
/*********************************************************************************************************
**  End Of File
*********************************************************************************************************/
