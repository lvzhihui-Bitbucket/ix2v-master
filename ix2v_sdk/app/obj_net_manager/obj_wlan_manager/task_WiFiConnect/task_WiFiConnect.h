/**
  ******************************************************************************
  * @file    task_Unlock.h
  * @author  zxj
  * @version V00.01.00
  * @date    2012.08.22
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2012 V-Tec</center></h2>
  ******************************************************************************
  */ 

#ifndef _task_WiFi_H
#define _task_WiFi_H

#include "RTOS.h"
#include "obj_WiFi_State.h"
#include "task_survey.h"
#include <list.h>

// Define Task Vars and Structures----------------------------------------------

#define	WIFI_PRINTF

#ifdef	WIFI_PRINTF
#define	wifi_printf(fmt,...)	printf("[UDP]-["__FILE__"]-[%04d]-"fmt"",__LINE__,##__VA_ARGS__)
#else
#define	wifi_printf(fmt,...)
#endif

// Define task interface-------------------------------------------------------------
	//resource semaphores------------------------
typedef void (*Wlan_Fun_Callback_t)(int,void*);

typedef struct
{ 
	Wlan_Fun_Callback_t	callback_fun;		
 	struct list_head 		list;
} Wlan_Inform_Callback_List_t;


#define WLAN_INFORM_CODE_OPEN				0x01		//wifi��
#define WLAN_INFORM_CODE_CLOSE			0x02		//wifi�ر�
#define WLAN_INFORM_CODE_DISCONNECT		0x03		//wifi�Ͽ�
#define WLAN_INFORM_CODE_CONNECTING		0x04		//wifi��������
#define WLAN_INFORM_CODE_CONNECTED		0x05		//wifi�������
#define WLAN_INFORM_CODE_SEARCHING		0x06		//wifi��������
#define WLAN_INFORM_CODE_SEARCH_OVER		0x07		//wifi��������
#define WLAN_INFORM_CODE_CONNECTED_POLLING		0x08

#define Wlan_State_Key_Sw			"WLAN-SW"
#define Wlan_State_Key_State			"WLAN-STATE"
#define Wlan_State_Key_IPPolicy		"WLAN-IP-Policy"
#define Wlan_State_Key_IPAddr		"WLAN-IP_Address"
#define Wlan_State_Key_SubnetMask	"WLAN-SubnetMask"
#define Wlan_State_Key_DefaultRoute 	"WLAN-DefaultRoute"
#define Wlan_State_Key_MACAddr		"WLAN-MACAddr"

	//MailBoxes----------------------------------
#pragma pack(1)	
typedef struct	
{	
	VDP_MSG_HEAD 	head;
	Wlan_Fun_Callback_t	callback;
	char config_json[];
	//char			wifi_ssid[WIFI_SSID_LENGTH+1];
	//char			wifi_pwd[WIFI_PWD_LENGTH+1];
} MSG_WIFI;
#pragma pack()

//msg_type
#define MSG_TYPE_WIFI_OPEN			0
#define MSG_TYPE_WIFI_CLOSE			1
#define MSG_TYPE_WIFI_CONNECT		2
#define MSG_TYPE_WIFI_DISCONNECT		3
#define MSG_TYPE_WIFI_SEARCH			4
#define MSG_TYPE_WIFI_POLLING			5
#define MSG_TYPE_WIFI_DELETE_SSID		6
#define MSG_TYPE_WIFI_ADD_SSID			7
#define MSG_TYPE_WIFI_CLEAR_SSID			8
#define MSG_TYPE_WIFI_INGNORE				9
#define MSG_TYPE_WIFI_ADD_CALLBACK			10
#define MSG_TYPE_WIFI_DEL_CALLBACK			11


#define MSG_WLANCONFIG_RESULT      22
#define MSG_WLAN_RESULT            24
// Define Task 2 items----------------------------------------------------------
void vtk_TaskInit_WiFi(int priority);

#define SSID_NAME	"SSID_NAME"
#define SSID_PWD		"SSID_PWD"


// Define Task others-----------------------------------------------------------



// Define API-------------------------------------------------------------------

int API_Wlan_Open(cJSON *ssid_array);
int API_Wlan_Close(void);
int API_Wlan_Search(void);
int API_Wlan_ConnectSsid(cJSON *ssid_array, int ifSave);
int API_Wlan_IngnoreSsid(cJSON *ssid_array);
int API_Add_Wlan_Inform_Callback(Wlan_Fun_Callback_t func);
int API_Del_Wlan_Inform_Callback(Wlan_Fun_Callback_t func);
cJSON* API_Get_Wlan_State(void);

#endif
