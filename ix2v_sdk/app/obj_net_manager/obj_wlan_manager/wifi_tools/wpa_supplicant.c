#include <stdio.h>
#include <libgen.h>
#include <unistd.h>
#include <math.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <linux/wireless.h>
#include <linux/if.h>
#include <linux/sockios.h>


#include "wpa_supplicant.h"
#include "define_file.h"


//#define   ENABLE_DHCP_REMOTE_PROCESS
//#define	ENABLE_WIFI_REMOTE_PROCESS

#ifdef ENABLE_DHCP_REMOTE_PROCESS
int wifi_if_UdhcpcClose( void );
int wifi_if_Udhcpc( char* wlan );
#endif

#ifdef ENABLE_WIFI_REMOTE_PROCESS
int wifi_if_start_wpa_supplicant_server( char* wlan );
int wifi_if_stop_wpa_supplicant_server( void );
int wifi_if_change_wpa_supplicant_configure( void );
int wifi_if_SetSupplicantConfig( char* ssid, char* key );
int wifi_if_SupplicantDisconnect( void );
int wifi_if_SupplicantConnect( char* wlan );
int wifi_if_WifiStatusQuery( char* wlan, struct wifi_info* info );
#endif


/***************************************************************************************************
 *  加载 usb wifi 驱动
 *  入口 .ko 驱动文件名称
 *  0/ok  其他/error
 */
int InsmodUsbWifi( char* ko )
{
	char KO[300];
	FILE *pf = NULL;
	char linestr[250];
	
	memset( KO, 0, sizeof(KO) );
	strcat( KO, "insmod " );
	strcat( KO, WIFI_PATH );
	strcat( KO, ko );
	
	system( KO );
	
	return 0;
}

/***************************************************************************************************
 *  移除 usb wifi 驱动
 *  入口 .ko 驱动文件名称
 *  0/ok  其他/error
 */
int RmmodUsbWifi( char* ko )
{
	char KO[300];
	FILE *pf = NULL;
	char linestr[250];
	
	memset( KO, 0, sizeof(KO) );
	strcat( KO, "rmmod " );
	strcat( KO, WIFI_PATH );
	strcat( KO, ko );
	system( KO );
	
	return 0;
}

/***************************************************************************************************
 *  加载 wlan 网卡
 *  入口 wlan 网卡名称
 *  0/ok  其他/error
 */
int IfconfigWlanUp( char* wlan )
{
	char KO[300];
	FILE *pf = NULL;
	char linestr[250];
	int ret = -1;
	
	memset( KO, 0, sizeof(KO) );
	strcat( KO, "ifconfig " );
	strcat( KO, wlan );
	strcat( KO, " up" );
	
	printf( "%s ----IfconfigWlanUp---- %d\n", KO, sizeof(KO) );
	
	if( (pf = popen( KO, "r" )) == NULL )
	{
		printf( "open ifconfig error \n" );
		ret = -1;
		goto err;
	}
	
	pclose( pf );
	pf = NULL;
	
	memset( KO, 0, sizeof(KO) );
	strcat( KO, "ifconfig" );
	
	if( (pf = popen( KO, "r" )) == NULL )
	{
		printf( "open ifconfig 1 error \n" );
		ret = -1;
		goto err;
	}
	
	while( fgets( linestr, 250, pf ) != NULL )
	{
		if( strstr( linestr, "wlan" ) != NULL )
		{
			printf( "wlan up ok ------------ \n" );
			ret = 0;
		}
	}
	
	err:
	{
		if( pf != NULL )
			pclose( pf );
		pf = NULL;
	}
	
	return ret;
}

/***************************************************************************************************
 *  移除 wlan 网卡
 *  入口 wlan 网卡名称
 *  0/ok  其他/error
 */
int IfconfigWlanDown( char* wlan )
{
	char KO[300];
	FILE *pf;
	char linestr[250];
	int ret = -1;
	
	memset( KO, 0, sizeof(KO) );
	strcat( KO, "ifconfig " );
	strcat( KO, wlan );
	strcat( KO, " down" );
	
	printf( "%s ----IfconfigWlanUp---- %d\n", KO, sizeof(KO) );
	
	if( (pf = popen( KO, "r" )) == NULL )
	{
		printf( "open ifconfig error \n" );
		ret = -1;
		goto err;
	}
	
	pclose( pf );
	pf = NULL;
	
	memset( KO, 0, sizeof(KO) );
	strcat( KO, "ifconfig" );
	
	if( (pf = popen( KO, "r" )) == NULL )
	{
		printf( "open ifconfig 1 error \n" );
		ret = -1;
		goto err;
	}
	
	while( fgets( linestr, 250, pf ) != NULL )
	{
		if( strstr( linestr, "wlan" ) != NULL )
		{
			goto err;
		}
		// lzh_20181122_s
		usleep(2*1000);
		// lzh_20181122_e
	}
	
	printf( "wlan down ok ------------ \n" );
	ret = 0;
	
	err:
	{
		if( pf != NULL )
			pclose( pf );
		pf = NULL;
	}
	
	return ret;
}

int wpaSupplicantServerState = 0;	// cao_20171220

// lzh_20171118_s
int start_wpa_supplicant_server( char* wlan )
{
	// cao_20171220
	if(wpaSupplicantServerState)
	{
		return 0;
	}
	wpaSupplicantServerState = 1;


#ifndef ENABLE_WIFI_REMOTE_PROCESS
	char KO[300];
	FILE *pf = NULL;
	char linestr[250];
	int ret = -1;
	//"wpa_supplicant -B -dd -i " // -dd 为显示调试信息, -B 表示在后台运行
	//"/mnt/nand1-2/wifi/wpa_supplicant -Dnl80211 -i wlan0 -c /mnt/nand1-2/wifi/wpa_supplicant.conf -B"
	snprintf(KO, 300, "%swpa_supplicant -Dnl80211 -B -i %s -c %sSupplicant.conf", WIFI_PATH, wlan, WIFI_PATH);
	system( KO );
	usleep( 20 * 1000 );
	//"/mnt/nand1-2/wifi/wpa_cli status"
	snprintf(KO, 300, "%swpa_cli status", WIFI_PATH);

	//cao_20181020_s
	int cnt = 10;
	//cao_20181020_e

	while( cnt )
	{
		if( (pf = popen( KO, "r" )) == NULL )
		{
			printf( "open wpa_supplicant error \n" );
			ret = -1;
			goto err;
		}
		
		while( fgets( linestr, 250, pf ) != NULL )
		{
			if( strstr( linestr, "wpa_state=COMPLETED" ) != NULL )
			{
				printf( "wpa_state=COMPLETED ok ------------ \n" );
				ret = 0;
				break;
			}
			// lzh_20181122_s
			usleep(2*1000);
			// lzh_20181122_e
		}
		pclose( pf );
		pf = NULL;
		
		if( ret == 0 )
			break;
		else
			cnt--;
		usleep( 1000 * 1000 );

		if(GetWifiStopConnecting())
		{
			cnt = 0;
		}
	}
	err:
	{
		if( pf != NULL )
			pclose( pf );
		pf = NULL;
	}
	return ret;
#else
	return wifi_if_start_wpa_supplicant_server( wlan );	
#endif
}


int change_wpa_supplicant_configure( void )
{	
#ifndef ENABLE_WIFI_REMOTE_PROCESS
	char KO[300];
	FILE *pf = NULL;
	char linestr[250];
	int ret = -1;
	
	memset( KO, 0, sizeof(KO) );
	strcat( KO, WIFI_PATH );
	strcat( KO, "wpa_cli reconfigure" );
	
	//printf( "%s ----Supplicant change configure---- %d\n", KO, sizeof(KO) );
	
	if( (pf = popen( KO, "r" )) == NULL )
	{
		//printf( "open KO error \n" );
		ret = -1;
		goto err;
	}
	
	while( fgets( linestr, 250, pf ) != NULL )
	{
		printf( "%s\n",linestr );	// tests
		if( strstr( linestr, "wpa_state=COMPLETED" ) != NULL )
		{
			//printf( "change configure,wpa_state=COMPLETED ok ------------ \n" );
			ret = 0;
			break;
		}
		// lzh_20181122_s
		usleep(2*1000);
		// lzh_20181122_e		
	}
	
	err:
	{
		if( pf != NULL )
			pclose( pf );
		pf = NULL;
	}	
	return ret;	

#else
	wifi_if_change_wpa_supplicant_configure();	
#endif
}	

int stop_wpa_supplicant_server( void )
{
	// cao_20171220
	if(!wpaSupplicantServerState)
	{
		return 0;
	}
	wpaSupplicantServerState = 0;
	
#ifndef ENABLE_WIFI_REMOTE_PROCESS

	char KO[300];
	FILE *pf = NULL;
	char linestr[250];
	int ret = -1;
	
	memset( KO, 0, sizeof(KO) );
	strcat( KO, WIFI_PATH );
	strcat( KO, "wpa_cli terminate" );
	
	printf( "%s ----SupplicantDisconnect---- %d\n", KO, sizeof(KO) );
	
	if( (pf = popen( KO, "r" )) == NULL )
	{
		printf( "open KO error \n" );
		ret = -1;
		goto err;
	}
	
	while( fgets( linestr, 250, pf ) != NULL )
	{
		if( strstr( linestr, "OK" ) != NULL )
		{
			printf( "Selected interface 'wlan0' ok \n" );
			ret = 0;
		}
		// lzh_20181122_s
		usleep(2*1000);
		// lzh_20181122_e
	}
	
	err:
	{
		if( pf != NULL )
			pclose( pf );
		pf = NULL;
	}
	
	return ret;

#else

	return wifi_if_stop_wpa_supplicant_server();

#endif
}

// lzh_20171118_e

/***************************************************************************************************
 *  设置 supplicant 参数
 *  入口 无线 ssid 无线 密码
 *  0/ok  其他/error
 */
int SetSupplicantConfig( char* ssid, char* key )
{
#ifndef ENABLE_WIFI_REMOTE_PROCESS

	char KO[300];
	FILE *pf = NULL;

	// lzh_20171121_s		// 密碼不夠8位時，驱动会显示异常并且需要重新加载服务器
	char key_ok[64];
	memset( key_ok, 0, 64);
	int key_len = strlen(key);	
	strcpy( key_ok, key );
	if( key_len < 8 )
	{
		memset( key_ok + key_len, ' ', 8-key_len );
	}
	//printf( "key_ok length=%d\n",strlen(key_ok) );
	// lzh_20171121_e
	
	memset( KO, 0, sizeof(KO) );
	strcat( KO, WIFI_PATH );
	strcat( KO, "Supplicant.conf" );
	
	if( (pf = fopen( KO, "w+" )) == NULL )
	{
		printf( "open" );
		printf( KO );
		printf( "error \n" );
		return -1;
	}
	
	fprintf( pf, "%s\n", "ctrl_interface=/var/run/wpa_supplicant" );
	fprintf( pf, "%s\n", "ap_scan=1" );
	fprintf( pf, "%s\n", "network={" );
	fputs( "ssid=\"", pf );
	fprintf( pf, "%s", ssid );
	fprintf( pf, "%s\n", "\"" );
	fputs( "psk=\"", pf );
	fprintf( pf, "%s", key_ok );
	fprintf( pf, "%s\n", "\"" );
	fprintf( pf, "%s\n", "}" );
	
	fclose( pf );
	sync();	
	return 0;

#else

	return wifi_if_SetSupplicantConfig( ssid, key );

#endif
}

/***************************************************************************************************
 *  wlan 断开
 *  none
 *  0/ok  其他/error
 */
int SupplicantDisconnect( void )
{
#ifndef ENABLE_WIFI_REMOTE_PROCESS

	/*
	char KO[300];
	FILE *pf = NULL;
	char linestr[250];
	int ret = -1;
	
	memset( KO, 0, sizeof(KO) );
	strcat( KO, WIFI_PATH );
	strcat( KO, "wpa_cli terminate" );
	
	printf( "%s ----SupplicantDisconnect---- %d\n", KO, sizeof(KO) );
	
	if( (pf = popen( KO, "r" )) == NULL )
	{
		printf( "open KO error \n" );
		ret = -1;
		goto err;
	}
	
	while( fgets( linestr, 250, pf ) != NULL )
	{
		if( strstr( linestr, "OK" ) != NULL )
		{
			printf( "Selected interface 'wlan0' ok \n" );
			ret = 0;
		}
	}
	
	err:
	{
		if( pf != NULL )
			pclose( pf );
		pf = NULL;
	}
	
	return ret;
	*/
	return 0;

#else
	return wifi_if_SupplicantDisconnect();
#endif
}

// lzh_20171117_s
#include "signal.h"
typedef void (*sighandler_t)(int);
int pox_system(const char *cmd_line)
{
   int ret = 0;
   sighandler_t old_handler;

   old_handler = signal(SIGCHLD, SIG_DFL);
   ret = system(cmd_line);
   signal(SIGCHLD, old_handler);

   return ret;
}
// lzh_20171117_e

/***************************************************************************************************
 *  wlan 连接
 *  入口 wlan 网卡名称
 *  0/ok  其他/error
 */
int SupplicantConnect( char* wlan )
{
#ifndef ENABLE_WIFI_REMOTE_PROCESS

	char KO[300];
	FILE *pf = NULL;
	char linestr[250];
	int ret = -1;

	int cnt = 5;
	memset( KO, 0, sizeof(KO) );
	strcat( KO, WIFI_PATH );
	strcat( KO, "wpa_cli status" );

	while( cnt )
	{
		if( (pf = popen( KO, "r" )) == NULL )
		{
			printf( "open wpa_supplicant error \n" );
			ret = -1;
			goto err;
		}
		
		while( fgets( linestr, 250, pf ) != NULL )
		{
			if( strstr( linestr, "wpa_state=COMPLETED" ) != NULL )
			{
				printf( "wpa_state=COMPLETED ok ------------ \n" );
				ret = 0;
				break;
			}
			// lzh_20181122_s
			usleep(2*1000);
			// lzh_20181122_e
		}

		pclose( pf );
		pf = NULL;
		
		if( ret == 0 )
			break;
		else
			cnt--;
		usleep( 1000 * 1000 );
	}
	
	err:
	{
		if( pf != NULL )
			pclose( pf );
		pf = NULL;
	}
	
	return ret;
#else
	return wifi_if_SupplicantConnect(wlan);
#endif	
}

/***************************************************************************************************
 *  设置 supplicant 参数
 *  入口 wlan 网卡名称
 *  0/ok  其他/error
 */
int WlanUdhcpcClose( void )
{
#ifndef ENABLE_DHCP_REMOTE_PROCESS
	char KO[300];
	FILE *pf = NULL;
	char *p = NULL;
	char linestr[250];
	int ret = -1;
	int pidnum;
	
	memset( KO, 0, sizeof(KO) );
	strcat( KO, "ps | grep udhcpc -i" );
	
	printf( "%s ----UdhcpcClose---- %d\n", KO, sizeof(KO) );
	
	if( (pf = popen( KO, "r" )) == NULL )
	{
		printf( "open UdhcpcClose error \n" );
		ret = -1;
		goto err;
	}
	
	while( fgets( linestr, 250, pf ) != NULL )
	{
		if( strstr( linestr, "grep" ) != NULL )
		{
			;
		}
		else
			if( strstr( linestr, "udhcpc -i" ) != NULL )	//  udhcpc -i wlan0 -R
			{
				p = strtok( linestr, " " );
				pidnum = atoi( p );
				//kill( (pid_t) pidnum, 0 );
				
				memset( KO, 0, sizeof(KO) );
				strcat( KO, "kill " );
				strcat( KO, p );
				//system( KO );
				pox_system( KO );
				SetWiFiDhcpState(0);
				printf( "kill Udhcpc ok --------%d---- \n", pidnum );
			}
	}
	
	err:
	{
		if( pf != NULL )
			pclose( pf );
		pf = NULL;
	}
	SetWiFiDhcpState(0);
	return ret;
#else
	return wifi_if_UdhcpcClose();
#endif
}

/***************************************************************************************************
 *  设置 supplicant 参数
 *  入口 wlan 网卡名称
 *  0/ok  其他/error
 */
int WlanUdhcpc( char* wlan )
{
#ifndef ENABLE_DHCP_REMOTE_PROCESS
	char KO[300];
	FILE *pf = NULL;
	char linestr[250];
	int ret = -1;
	
	snprintf( KO, 300, "udhcpc -i %s -R", wlan);

	printf( "%s ----Udhcpc---- %d\n", KO, sizeof(KO) );
	
	if( (pf = popen( KO, "r" )) == NULL )
	{
		printf( "open wpa_supplicant error \n" );
		ret = -1;
		goto err;
	}
	
	SetWiFiDhcpState(1);
	
	printf( "%s1 ----Udhcpc state = %d---- \n", KO, ret );
	int send_discover_cnt = 0;
	while( fgets( linestr, 250, pf ) != NULL )
	{	
		printf( "%s2 ----Udhcpc state = %d---- \n", linestr, ret );
		if( strstr( linestr, "adding dns" ) != NULL )
		{
			printf( "Udhcpc ok ------------ \n" );
			ret = 0;
		}
		else if(strstr( linestr, "Sending discover" ) != NULL )
		{
			send_discover_cnt++;
			printf( "send_discover_cnt = %d---- \n", send_discover_cnt);
			if( send_discover_cnt >= 5)  // follow code invalid!!!
			{
				send_discover_cnt = 0;
				WlanUdhcpcClose();
				printf( "%s5 ----Udhcpc state = %d---- \n", KO, ret );
				break;
			}
		}
		else
		{
		#if 1
			send_discover_cnt++;
			printf( "error:send_discover_cnt = %d---- \n", send_discover_cnt);
			if( send_discover_cnt >= 10)  // follow code invalid!!!
			{
				send_discover_cnt = 0;
				WlanUdhcpcClose();
				printf( "%s5 ----Udhcpc state = %d---- \n", KO, ret );
				break;
			}
		#endif
		}
	}

	printf( "%s3 ----Udhcpc state = %d---- \n", KO, ret );
	
	err:
	{
		if( pf != NULL )
			pclose( pf );
		pf = NULL;
	}
	printf( "%s4 ----Udhcpc state = %d---- \n", KO, ret );
	return ret;
#else
	return wifi_if_Udhcpc( wlan );
#endif
}

/***************************************************************************************************
 *  查询wifi是否已连接，和信号强度
 *  入口 wlan 网卡名称，信号强度
 *  0/ok  其他/error
 */
int WifiStatusQuery( char* wlan, struct wifi_info* info )
{
#ifndef ENABLE_WIFI_REMOTE_PROCESS
	char KO[300];
	FILE *pf = NULL;
	char linestr[250];
	int ret = -1;
	char* temp;
	int lev_flag=0;
	memset( KO, 0, sizeof(KO) );
	strcat( KO, WIFI_PATH );
	strcat( KO, "wpa_cli status" );
	
	info->QUALITY = 0;
	info->NOISE = 0;
	memset( info->MAC, 0, sizeof(info->MAC) );

	if( (pf = popen( KO, "r" )) == NULL )
	{
		printf( "open wpa_cli status error \n" );
		ret = WPA_STATE_ERROR;
		return ret;
	}

	while( fgets( linestr, 250, pf ) != NULL )
	{
		if( strstr( linestr, "bssid=" ) != NULL )
		{
			temp = strtok( linestr, "=" );
			temp = strtok( NULL, "=" );

			strcpy( info->MAC, temp );
			//printf( "MAC= ------------ %s \n", info->MAC );
		}
		else if( strstr( linestr, "ssid=" ) != NULL )
		{
			temp = strtok( linestr, "=" );
			temp = strtok( NULL, "=" );
			temp[strlen(temp)-1] = 0;
			strcpy( info->ESSID, temp );
			//printf( "ESSID= ------------ %s \n", info->ESSID );
		}
		else if( strstr( linestr, "signal_level=" ) != NULL )
		{
			temp = strtok( linestr, "=" );
			temp = strtok( NULL, "=" );
			info->LEVEL = atoi( temp );
			if(info->LEVEL > 100) info->LEVEL = info->LEVEL*2/5;

			//printf( "22222222222222signal_level= --------------%d:%s\n", info->LEVEL,temp );
			lev_flag=1;
		}
		else if( strstr( linestr, "wpa_state=COMPLETED" ) != NULL )
		{
			ret = WPA_STATE_COMPLETED;
		}
		else if( strstr( linestr, "wpa_state=SCANNING" ) != NULL )
		{
			ret = WPA_STATE_SCANNING;
		}
		else if( strstr( linestr, "wpa_state=ASSOCIATING" ) != NULL )
		{
			ret = WPA_STATE_ASSOCIATING;
		}
		else if( strstr( linestr, "wpa_state=ASSOCIATED" ) != NULL )
		{
			ret = WPA_STATE_ASSOCIATED;
		}
		else if( strstr( linestr, "wpa_state=DISCONNECTED" ) != NULL )
		{
			ret = WPA_STATE_DISCONNECTED;
		}
		
		// lzh_20181122_s
		usleep(2*1000);
		// lzh_20181122_e
	}

	if( pf != NULL )
		pclose( pf );
	pf = NULL;
	if(ret==WPA_STATE_COMPLETED&&lev_flag==0)
	{
		memset( KO, 0, sizeof(KO) );
		strcat( KO, WIFI_PATH );
		strcat( KO, "iwconfig " );
		strcat( KO, wlan);
		if( (pf = popen( KO, "r" )) == NULL )
		{
			printf( "open iwconfig error \n" );
		}
		else
		{
			char *temp1;
			while( fgets( linestr, 250, pf ) != NULL )
			{
				if((temp=strstr(linestr,"Signal level="))!=NULL)
				{
					temp+=strlen("Signal level=");
					temp1 = strstr( temp, " " );
					*temp1=0;
					info->LEVEL = atoi( temp);
					//if(info->LEVEL > 100) info->LEVEL = info->LEVEL*2/5;

					printf( "333333333signal_level= --------------%d:%s\n", info->LEVEL,temp );
				}
			}
			pclose( pf );
		}
	}
		
	return ret;
	
#else

	return wifi_if_WifiStatusQuery( wlan, info );

#endif
}





