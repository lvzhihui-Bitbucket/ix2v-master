/*
 * util.c
 *
 *  Created on: 2016��11��29��
 *      Author: root
 */

#include <stdio.h>
#include <ctype.h>
#include <math.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <linux/wireless.h>

#include "util.h"

#define G (1e9)
#define M (1e6)
#define K (1e3)

inline void usage( char *name )
{
	printf( "usage: %s <interface>\n", name );
	printf( "e.g.: %s wlan0\n", name );
	printf( "\n" );
}

void mac_addr( struct sockaddr *addr, char *buffer )
{
	sprintf( buffer, "%02X:%02X:%02X:%02X:%02X:%02X", (unsigned char) addr->sa_data[0], (unsigned char) addr->sa_data[1], (unsigned char) addr->sa_data[2], (unsigned char) addr->sa_data[3],
			(unsigned char) addr->sa_data[4], (unsigned char) addr->sa_data[5] );
}


void double2string( double num, char *buffer )
{
	char scale = '\0';
	
	if( num >= G )
	{
		scale = 'G';
		num /= G;
	}
	else
		if( num >= M )
		{
			scale = 'M';
			num /= M;
		}
		else
			if( num >= K )
			{
				scale = 'K';
				num /= K;
			}
	
	sprintf( buffer, "%g %c", num, scale );
}

void iw_essid_escape( char * dest, const char * src, const int slen )
{
	const unsigned char * s = (const unsigned char *) src;
	const unsigned char * e = s + slen;
	char * d = dest;
	
	/* Look every character of the string */
	while( s < e )
	{
		int isescape;
		
		/* Escape the escape to avoid ambiguity.
		 * We do a fast path test for performance reason. Compiler will
		 * optimise all that ;-) */
		if( *s == '\\' )
		{
			/* Check if we would confuse it with an escape sequence */
			if( (e - s) > 4 && (s[1] == 'x') && (isxdigit( s[2] )) && (isxdigit( s[3] )) )
			{
				isescape = 1;
			}
			else
				isescape = 0;
		}
		else
			isescape = 0;
		
		/* Is it a non-ASCII character ??? */
		if( isescape || !isascii( *s ) || iscntrl( *s ) )
		{
			/* Escape */
			sprintf( d, "\\x%02X", *s );
			d += 4;
		}
		else
		{
			/* Plain ASCII, just copy */
			*d = *s;
			d++;
		}
		s++;
	}
	
	/* NUL terminate destination */
	*d = '\0';
}

void encoding_key( char * buffer, int buflen, const unsigned char * key, /* Must be unsigned */
int key_size, int key_flags )
{
	int i;
	
	/* Check buffer size -> 1 bytes => 2 digits + 1/2 separator */
	if( (key_size * 3) > buflen )
	{
		snprintf( buffer, buflen, "<too big>" );
		return;
	}
	
	/* Is the key present ??? */
	if( key_flags & IW_ENCODE_NOKEY )
	{
		/* Nope : print on or dummy */
		if( key_size <= 0 )
			strcpy( buffer, "on" ); /* Size checked */
		else
		{
			strcpy( buffer, "**" ); /* Size checked */
			buffer += 2;
			for( i = 1; i < key_size; i++ )
			{
				if( (i & 0x1) == 0 )
					strcpy( buffer++, "-" ); /* Size checked */
				strcpy( buffer, "**" ); /* Size checked */
				buffer += 2;
			}
		}
	}
	else
	{
		/* Yes : print the key */
		sprintf( buffer, "%.2X", key[0] ); /* Size checked */
		buffer += 2;
		for( i = 1; i < key_size; i++ )
		{
			if( (i & 0x1) == 0 )
				strcpy( buffer++, "-" ); /* Size checked */
			sprintf( buffer, "%.2X", key[i] ); /* Size checked */
			buffer += 2;
		}
	}
}

static void iw_print_ie_unknown( unsigned char *iebuf, int buflen )
{
	int ielen = iebuf[1] + 2;
	int i;
	
	if( ielen > buflen )
		ielen = buflen;
	
	printf( "Unknown: " );
	for( i = 0; i < ielen; i++ )
		printf( "%02X", iebuf[i] );
	printf( "\n" );
}

/*------------------------------------------------------------------*/
/*
 * Print the name corresponding to a value, with overflow check.
 */
static void iw_print_value_name( unsigned int value, const char * names[], const unsigned int num_names )
{
	if( value >= num_names )
		printf( " unknown (%d)", value );
	else
		printf( " %s", names[value] );
}

/* Values for the IW_IE_CIPHER_* in GENIE */
static const char * iw_ie_cypher_name[] =
{ "none", "WEP-40", "TKIP", "WRAP", "CCMP", "WEP-104", };
#define IW_ARRAY_LEN(x) (sizeof(x)/sizeof((x)[0]))

#define	IW_IE_CYPHER_NUM	IW_ARRAY_LEN(iw_ie_cypher_name)

/* Values for the IW_IE_KEY_MGMT_* in GENIE */
static const char * iw_ie_key_mgmt_name[] =
{ "none", "802.1x", "PSK", };

#define	IW_IE_KEY_MGMT_NUM	IW_ARRAY_LEN(iw_ie_key_mgmt_name)

inline void iw_print_ie_wpa( unsigned char * iebuf, int buflen )
{
	int ielen = iebuf[1] + 2;
	int offset = 2; /* Skip the IE id, and the length. */
	unsigned char wpa1_oui[3] =
	{ 0x00, 0x50, 0xf2 };
	unsigned char wpa2_oui[3] =
	{ 0x00, 0x0f, 0xac };
	unsigned char * wpa_oui;
	int i;
	unsigned short ver = 0;
	unsigned short cnt = 0;
	
	if( ielen > buflen )
		ielen = buflen;
	
#if 0
	/* Debugging code. In theory useless, because it's debugged ;-) */
	printf("IE raw value %d [%02X", buflen, iebuf[0]);
	for(i = 1; i < buflen; i++)
	printf(":%02X", iebuf[i]);
	printf("]\n");
#endif
	
	switch( iebuf[0] )
	{
		case 0x30: /* WPA2 */
			/* Check if we have enough data */
			if( ielen < 4 )
			{
				iw_print_ie_unknown( iebuf, buflen );
				return;
			}
			
			wpa_oui = wpa2_oui;
			break;
			
		case 0xdd: /* WPA or else */
			wpa_oui = wpa1_oui;
			
			/* Not all IEs that start with 0xdd are WPA.
			 * So check that the OUI is valid. Note : offset==2 */
			if( (ielen < 8) || (memcmp( &iebuf[offset], wpa_oui, 3 ) != 0) || (iebuf[offset + 3] != 0x01) )
			{
				iw_print_ie_unknown( iebuf, buflen );
				return;
			}
			
			/* Skip the OUI type */
			offset += 4;
			break;
			
		default:
			return;
	}
	
	/* Pick version number (little endian) */
	ver = iebuf[offset] | (iebuf[offset + 1] << 8);
	offset += 2;
	
	if( iebuf[0] == 0xdd )
		printf( "WPA Version %d\n", ver );
	if( iebuf[0] == 0x30 )
		printf( "IEEE 802.11i/WPA2 Version %d\n", ver );
	
	/* From here, everything is technically optional. */

	/* Check if we are done */
	if( ielen < (offset + 4) )
	{
		/* We have a short IE.  So we should assume TKIP/TKIP. */
		printf( "                        Group Cipher : TKIP\n" );
		printf( "                        Pairwise Cipher : TKIP\n" );
		return;
	}
	
	/* Next we have our group cipher. */
	if( memcmp( &iebuf[offset], wpa_oui, 3 ) != 0 )
	{
		printf( "                        Group Cipher : Proprietary\n" );
	}
	else
	{
		printf( "                        Group Cipher :" );
		iw_print_value_name( iebuf[offset + 3], iw_ie_cypher_name, IW_IE_CYPHER_NUM );
		printf( "\n" );
	}
	offset += 4;
	
	/* Check if we are done */
	if( ielen < (offset + 2) )
	{
		/* We don't have a pairwise cipher, or auth method. Assume TKIP. */
		printf( "                        Pairwise Ciphers : TKIP\n" );
		return;
	}
	
	/* Otherwise, we have some number of pairwise ciphers. */
	cnt = iebuf[offset] | (iebuf[offset + 1] << 8);
	offset += 2;
	printf( "                        Pairwise Ciphers (%d) :", cnt );
	
	if( ielen < (offset + 4 * cnt) )
		return;
	
	for( i = 0; i < cnt; i++ )
	{
		if( memcmp( &iebuf[offset], wpa_oui, 3 ) != 0 )
		{
			printf( " Proprietary" );
		}
		else
		{
			iw_print_value_name( iebuf[offset + 3], iw_ie_cypher_name,
			IW_IE_CYPHER_NUM );
		}
		offset += 4;
	}
	printf( "\n" );
	
	/* Check if we are done */
	if( ielen < (offset + 2) )
		return;
	
	/* Now, we have authentication suites. */
	cnt = iebuf[offset] | (iebuf[offset + 1] << 8);
	offset += 2;
	printf( "                        Authentication Suites (%d) :", cnt );
	
	if( ielen < (offset + 4 * cnt) )
		return;
	
	for( i = 0; i < cnt; i++ )
	{
		if( memcmp( &iebuf[offset], wpa_oui, 3 ) != 0 )
		{
			printf( " Proprietary" );
		}
		else
		{
			iw_print_value_name( iebuf[offset + 3], iw_ie_key_mgmt_name,
			IW_IE_KEY_MGMT_NUM );
		}
		offset += 4;
	}
	printf( "\n" );
	
	/* Check if we are done */
	if( ielen < (offset + 1) )
		return;
	
	/* Otherwise, we have capabilities bytes.
	 * For now, we only care about preauth which is in bit position 1 of the
	 * first byte.  (But, preauth with WPA version 1 isn't supposed to be
	 * allowed.) 8-) */
	if( iebuf[offset] & 0x01 )
	{
		printf( "                       Preauthentication Supported\n" );
	}
}

inline void iw_print_gen_ie( unsigned char *buffer, int buflen )
{
	int offset = 0;
	
	/* Loop on each IE, each IE is minimum 2 bytes */
	while( offset <= (buflen - 2) )
	{
		printf( "IE: " );
		
		/* Check IE type */
		switch( buffer[offset] )
		{
			case 0xdd: /* WPA1 (and other) */
			case 0x30: /* WPA2 */
				iw_print_ie_wpa( buffer + offset, buflen );
				break;
			default:
				iw_print_ie_unknown( buffer + offset, buflen );
		}
		/* Skip over this IE to the next one in the list. */
		offset += buffer[offset + 1] + 2;
	}
}
