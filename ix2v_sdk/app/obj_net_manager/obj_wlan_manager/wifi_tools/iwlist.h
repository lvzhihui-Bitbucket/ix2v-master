/*
 * iwlist.h
 *
 *  Created on: 2016年11月29日
 *      Author: root
 */

#ifndef APP_WIFI_TOOLS_IWLIST_H_
#define APP_WIFI_TOOLS_IWLIST_H_

#define MAX_LENGTH  	20480
#define TMP_LENGTH		256
#define MAX_COUNT  		5

#define ESSID_LEN		50

#define DEG				0

#if DEG
#define	DEG_P			printf
#else
#define	DEG_P
#endif

struct wifi_info
{
	char MAC[17];			// mac 地址
	char ESSID[ESSID_LEN];	// ssid 名称
	char MODE[12];			// 路由器工作模式
	char BIT_RATE[12];		// 无线带宽
	char IEEE[30];			// 路由器加密模式 wpa1/2
	char QUALITY;			// 信号质量
	unsigned char LEVEL;	// 信号幅度
	char NOISE;				// 信号噪声
};

struct wifi_data
{
	int DataCnt;
	struct wifi_info WifiInfo[30];
};

int iwlist( const char* pwlan, struct wifi_data* pWifiData );

#endif /* APP_WIFI_TOOLS_IWLIST_H_ */
