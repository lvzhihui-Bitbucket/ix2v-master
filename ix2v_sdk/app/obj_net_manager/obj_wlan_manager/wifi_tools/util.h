/*
 * util.h
 *
 *  Created on: 2016��11��29��
 *      Author: root
 */

#ifndef APP_UTIL_H_
#define APP_UTIL_H_

#include <linux/wireless.h>

inline void usage( char *name );

void mac_addr( struct sockaddr *addr, char *buffer );

//inline double freq2double( struct iw_freq *freq );
void double2string( double num, char *buffer );

void iw_essid_escape( char *dest, const char *src, const int slen );
void encoding_key( char *buffer, int buflen, const unsigned char *key, int key_size, int key_flags );

inline void iw_print_gen_ie( unsigned char *buffer, int buflen );

#endif /* APP_UTIL_H_ */
