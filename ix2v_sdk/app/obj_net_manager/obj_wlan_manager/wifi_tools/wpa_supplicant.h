/*
 * wpa_supplicant.h
 *
 *  Created on: 2016��12��7��
 *      Author: root
 */

#ifndef APP_WIFI_TOOLS_WPA_SUPPLICANT_H_
#define APP_WIFI_TOOLS_WPA_SUPPLICANT_H_

#include "iwlist.h"

/////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////
#define WPA_STATE_ERROR					(-1)
#define WPA_STATE_COMPLETED				0
#define WPA_STATE_SCANNING				1
#define WPA_STATE_ASSOCIATING			2
#define WPA_STATE_ASSOCIATED			3
#define WPA_STATE_DISCONNECTED			4



int InsmodUsbWifi( char* ko );
int RmmodUsbWifi( char* ko );
int IfconfigWlanUp( char* wlan );
int IfconfigWlanDown( char* wlan );

/////////////////////////////////////////////////////
int start_wpa_supplicant_server( char* wlan );
int stop_wpa_supplicant_server( void );
int change_wpa_supplicant_configure( void );
int SetSupplicantConfig( char* ssid, char* key );
int SupplicantDisconnect( void );
int SupplicantConnect( char* wlan );
int WlanUdhcpcClose( void );
int WlanUdhcpc( char* wlan );
int WifiStatusQuery( char* wlan, struct wifi_info* info );

#endif /* APP_WIFI_TOOLS_WPA_SUPPLICANT_H_ */
