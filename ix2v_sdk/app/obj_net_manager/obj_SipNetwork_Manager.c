//////////////////////////////////////////////
#include "task_Shell.h"
#include "cJSON.h"
#include "obj_IoInterface.h"
#include "obj_PublicInformation.h"
#include "elog_forcall.h"
#include "obj_lvgl_msg.h"
#define	SipNetwork_Connected_LAN		1
#define	SipNetwork_Connected_WLAN		2
#define	SipNetwork_Connected_4G		4
pthread_mutex_t SipNetwork_Manager_lock = PTHREAD_MUTEX_INITIALIZER;
OS_TIMER SipNetwork_Manager_timer;
static int SipNetwork_Connented=0;
static char CurSipRoute[]={"NONE"};
static char SaveSipRoute[]={"NONE"};
static int Be_Set_SipRoute=0;
static int Force_Set_SipRoute=0;
static int SipNetwork_state=0;
static int SipNetwork_timer=0;

enum
{
	SipNetwork_Link=0,
	SipNetwork_Fail,
	SipNetwork_OK,
	//SipNetwork_ILL,
};
#if 0
static const char *SipRoute_Name[]=
{
	"LAN",
	"WLAN",	
	"4G",
};
#endif
char *Get_CurSipRoute(void)
{
	return CurSipRoute;
}
int Get_SipNetwork_state(void)
{
	return SipNetwork_state;
}
int Get_SipNetwork_timer(void)
{
	return SipNetwork_timer;
}
void SetSipNetwork(char *net)
{
	char *del_inf1;
	char *del_inf2;
	char *set_inf;
	char *set_gw;
	
	if(strcmp(net,"LAN")==0)
	{
		del_inf1="wlan0";
		del_inf2="usb0";
		set_inf="eth0";
		set_gw=get_lan_gw_save();
	}
	else if(strcmp(net,"WLAN")==0)
	{
		del_inf1="eth0";
		del_inf2="usb0";
		set_inf="wlan0";
		set_gw=get_wlan_gw_save();
	}
	else if(strcmp(net,"4G")==0)
	{
		del_inf1="wlan0";
		del_inf2="eth0";
		set_inf="usb0";
		set_gw="192.168.0.1";
	}
	else
	{
		return;
	}
	DelDefaultGatewayByDevice(del_inf1);
	DelDefaultGatewayByDevice(del_inf2);
	SetDefaultGatewayByDevice(set_inf, set_gw);
	
}
#if 0
int SipNetworkManage(char *cur_net)
{
	int ret=-1;
	//cJSON *sip_net=NULL;
	
	if(strcmp(cur_net,"LAN")==0)
	{
		if(SipNetwork_Connented&SipNetwork_Connected_LAN)
		{
			SetSipNetwork("LAN");
			ret=0;
			//sip_net=cJSON_CreateString("LAN");
		}
		
	}
	else if(strcmp(cur_net,"WLAN")==0)
	{
		if(SipNetwork_Connented&SipNetwork_Connected_WLAN)
		{
			SetSipNetwork("WLAN");
			ret=0;
			//sip_net=cJSON_CreateString("WLAN");
		}
	}
	else if(strcmp(cur_net,"4G")==0)
	{
		if(SipNetwork_Connented&SipNetwork_Connected_4G)
		{
			SetSipNetwork("4G");
			ret=0;
			//sip_net=cJSON_CreateString("4G");
		}
	}
	#if 0
	if(sip_net==NULL)
		sip_net=cJSON_CreateString("UNKOWN");
	API_PublicInfo_Write(PB_SIP_CONNECT_NETWORK,sip_net);
	cJSON_Delete(sip_net);
	#endif
	return ret;
}
#endif
#if 1
void SipNetworkUpdate(char *upate_net,char *best_net)
{
	int set_upate_net_flag=0;
	int set_cur_net_flag=0;
	if(Force_Set_SipRoute)
		return;
	if(strcmp(upate_net,best_net)==0)
	{
		//if(strcmp(CurSipRoute,upate_net)==0)	
		{
			
			set_upate_net_flag=1;
		}
	}
	else
	{
		if(strcmp(CurSipRoute,"NONE")==0)
		{
			if(SipNetwork_timer>40)
				set_upate_net_flag=1;
		}
		else if(SipNetwork_state==SipNetwork_Fail)
		{
			set_upate_net_flag=1;
		}
		else
		{
			set_cur_net_flag=1;
		}
	}
	
	if(set_upate_net_flag==1)
	{
		strcpy(CurSipRoute,upate_net);
		SipNetwork_state=SipNetwork_Link;
		SipNetwork_timer=0;
		SetSipNetwork(upate_net);
		Api_Sip_ReReg();
	}
	if(set_cur_net_flag==1)
	{
		//strcpy(CurSipRoute,upate_net);
		//SipNetwork_timer=0;
		SetSipNetwork(CurSipRoute);
		//Api_Sip_ReReg();
	}
	OS_RetriggerTimer(&SipNetwork_Manager_timer);
}
void SipNetwork_Check(void)
{
	cJSON *sip_net;
	char *set_net=NULL;
	char *route_io;
	int route_io_mask=0;
	int cur_route_mask=0;
	if(Force_Set_SipRoute)
	{
		if(SipNetwork_timer++>900)
		{
			Force_Set_SipRoute=0;
			SipNetwork_timer=0;
			if(strcmp(CurSipRoute,SaveSipRoute)!=0)
			{
				strcpy(CurSipRoute,SaveSipRoute);
				API_PublicInfo_Write_String(PB_SIP_CONNECT_NETWORK,"NONE");
				SipNetwork_timer=0;
				SipNetwork_state=SipNetwork_Link;
				SetSipNetwork(SaveSipRoute);
			}
			Api_Sip_ReReg();
		}
		else
		{
			if(SipNetwork_state==SipNetwork_Link)
			{
				if(Get_SipSer_State()>0)
				{
					SipNetwork_state=SipNetwork_OK;
					//SipNetwork_timer=0;
					//sip_net=cJSON_CreateString(CurSipRoute);
					API_PublicInfo_Write_String(PB_SIP_CONNECT_NETWORK,CurSipRoute);
				}
			}
		}
		return;
	}
	route_io=API_Para_Read_String2(SIP_NetworkSetting);
	if(strcmp(route_io,"WLAN")==0)
		route_io_mask= SipNetwork_Connected_WLAN;
	else if(strcmp(route_io,"4G")==0)
		route_io_mask= SipNetwork_Connected_4G;
	else
		route_io_mask= SipNetwork_Connected_LAN;
	if(strcmp(CurSipRoute,"WLAN")==0)
		cur_route_mask= SipNetwork_Connected_WLAN;
	else if(strcmp(CurSipRoute,"4G")==0)
		cur_route_mask= SipNetwork_Connected_4G;
	else
		cur_route_mask= SipNetwork_Connected_LAN;
	if(strcmp(CurSipRoute,"NONE")==0)
	{
		if(SipNetwork_timer==0)
		{
			sip_net=cJSON_CreateString("NONE");
			API_PublicInfo_Write(PB_SIP_CONNECT_NETWORK,sip_net);
			cJSON_Delete(sip_net);
		}
		SipNetwork_timer++;
		if(SipNetwork_timer>60)
		{
			if(SipNetwork_Connented&route_io_mask)
			{
				set_net=route_io;
			}
			else if(SipNetwork_Connented&SipNetwork_Connected_LAN)
			{
				set_net="LAN";
			}
			else if(SipNetwork_Connented&SipNetwork_Connected_WLAN)
			{
				set_net="WLAN";
			}
			else if(SipNetwork_Connented&SipNetwork_Connected_4G)
			{
				set_net="4G";
			}
		}
	}
	else if(SipNetwork_state==SipNetwork_OK)
	{
		if((SipNetwork_Connented&cur_route_mask)==0)
		{
			Api_Sip_ReReg();
			SipNetwork_state=SipNetwork_Fail;
			SipNetwork_timer=40;
			sip_net=cJSON_CreateString("NONE");
			API_PublicInfo_Write(PB_SIP_CONNECT_NETWORK,sip_net);
			cJSON_Delete(sip_net);
		}
		else if(Get_SipSer_State()>0)
		{
			if(strcmp(route_io,CurSipRoute)!=0&&(SipNetwork_Connented&route_io_mask))
			{
				if(SipNetwork_timer++>(60*60*24))
				{
					set_net=route_io;
				}
			}
		}
		else
		{
			SipNetwork_state=SipNetwork_Link;
			SipNetwork_timer++;
		}
	}
	else if(SipNetwork_state==SipNetwork_Link)
	{
		if(Get_SipSer_State()>0)
		{
			SipNetwork_state=SipNetwork_OK;
			SipNetwork_timer=0;
			sip_net=cJSON_CreateString(CurSipRoute);
			API_PublicInfo_Write(PB_SIP_CONNECT_NETWORK,sip_net);
			cJSON_Delete(sip_net);
			if(Be_Set_SipRoute)
			{
				char rsp[10]={0};
				Be_Set_SipRoute=0;
				rsp[0]=0;
				strcpy(rsp+1,CurSipRoute);
				//vtk_lvgl_lock();
				CB_SET_SipRouteRespond(rsp);//lv_msg_send(MSG_SETTING_SIP_ROUTE_RSP, rsp);
				//vtk_lvgl_unlock();
			}
		}
		else
		{
			SipNetwork_timer++;
			if(SipNetwork_timer==15)
				Api_Sip_ReReg();
			if(SipNetwork_timer==30)
				Api_Sip_ReReg();
			if(SipNetwork_timer>40)
			{
				SipNetwork_state=SipNetwork_Fail;
				sip_net=cJSON_CreateString("NONE");
				API_PublicInfo_Write(PB_SIP_CONNECT_NETWORK,sip_net);
				cJSON_Delete(sip_net);
				if(Be_Set_SipRoute)
				{
					char rsp[10]={0};
					Be_Set_SipRoute=0;
					rsp[0]=1;
					strcpy(rsp+1,CurSipRoute);
					//vtk_lvgl_lock();
					CB_SET_SipRouteRespond(rsp);//lv_msg_send(MSG_SETTING_SIP_ROUTE_RSP, rsp);
					//vtk_lvgl_unlock();
				}
			}
		}
	}
	else if(SipNetwork_state==SipNetwork_Fail)
	{
		if(Get_SipSer_State()>0)
		{
			SipNetwork_state=SipNetwork_OK;
			SipNetwork_timer=0;
			sip_net=cJSON_CreateString(CurSipRoute);
			API_PublicInfo_Write(PB_SIP_CONNECT_NETWORK,sip_net);
			cJSON_Delete(sip_net);
		}
		else
		{
			SipNetwork_timer++;
			if(SipNetwork_timer>60)
			{
				#if 0
				if(strcmp(CurSipRoute,"WLAN")==0)
					cur_route_mask= SipNetwork_Connected_WLAN;
				else if(strcmp(CurSipRoute,"4G")==0)
					cur_route_mask= SipNetwork_Connected_4G;
				else
					cur_route_mask= SipNetwork_Connected_LAN;
				#endif
				if((SipNetwork_Connented)==cur_route_mask)
				{
					if(SipNetwork_timer>600)
					{
						set_net=CurSipRoute;
					}
				}
				else
				{	
					int i;
				
					for(i=0;i<2;i++)
					{
						cur_route_mask= (cur_route_mask*2);
						if(cur_route_mask>SipNetwork_Connected_4G)
							cur_route_mask=SipNetwork_Connected_LAN;
						if(SipNetwork_Connented&cur_route_mask)
						{
							if(cur_route_mask&SipNetwork_Connected_LAN)
							{
								set_net="LAN";
							}
							else if(cur_route_mask&SipNetwork_Connected_WLAN)
							{
								set_net="WLAN";
							}
							else if(cur_route_mask&SipNetwork_Connected_4G)
							{
								set_net="4G";
							}
							break;
						}
					}
				}
				
				
			}
		}
	}
	if(set_net!=NULL)
	{
		strcpy(CurSipRoute,set_net);
		SipNetwork_state=SipNetwork_Link;
		SipNetwork_timer=0;
		SetSipNetwork(set_net);
		Api_Sip_ReReg();
	}
}
#endif
void SipNetworkManage_WlanConnected(void)
{
	pthread_mutex_lock( &SipNetwork_Manager_lock);
	SipNetwork_Connented|=SipNetwork_Connected_WLAN;
	//SipNetworkManage(API_Para_Read_String2(SIP_NetworkSetting));
	SipNetworkUpdate("WLAN",API_Para_Read_String2(SIP_NetworkSetting));
	pthread_mutex_unlock( &SipNetwork_Manager_lock);
}
void SipNetworkManage_WlanDisconnected(void)
{
	pthread_mutex_lock( &SipNetwork_Manager_lock);
	SipNetwork_Connented&=(~SipNetwork_Connected_WLAN);
	//SipNetworkManage(API_Para_Read_String2(SIP_NetworkSetting));
	pthread_mutex_unlock( &SipNetwork_Manager_lock);
}
void SipNetworkManage_LanConnected(void)
{
	pthread_mutex_lock( &SipNetwork_Manager_lock);
	SipNetwork_Connented|=SipNetwork_Connected_LAN;
	//SipNetworkManage(API_Para_Read_String2(SIP_NetworkSetting));
	SipNetworkUpdate("LAN",API_Para_Read_String2(SIP_NetworkSetting));
	pthread_mutex_unlock( &SipNetwork_Manager_lock);
}
void SipNetworkManage_LanDisconnected(void)
{
	pthread_mutex_lock( &SipNetwork_Manager_lock);
	SipNetwork_Connented&=(~SipNetwork_Connected_LAN);
	//SipNetworkManage(API_Para_Read_String2(SIP_NetworkSetting));
	pthread_mutex_unlock( &SipNetwork_Manager_lock);
}
void SipNetworkManage_4GConnected(void)
{
	pthread_mutex_lock( &SipNetwork_Manager_lock);
	SipNetwork_Connented|=SipNetwork_Connected_4G;
	//SipNetworkManage(API_Para_Read_String2(SIP_NetworkSetting));
	SipNetworkUpdate("4G",API_Para_Read_String2(SIP_NetworkSetting));
	pthread_mutex_unlock( &SipNetwork_Manager_lock);
}
void SipNetworkManage_4GDisconnected(void)
{
	pthread_mutex_lock( &SipNetwork_Manager_lock);
	SipNetwork_Connented&=(~SipNetwork_Connected_4G);
	//SipNetworkManage(API_Para_Read_String2(SIP_NetworkSetting));
	pthread_mutex_unlock( &SipNetwork_Manager_lock);
}
int API_Change_SipNetwork(char *net_name)
{
	int ret;
	pthread_mutex_lock( &SipNetwork_Manager_lock);
	//SipNetwork_Connented|=SipNetwork_Connected_4G;
	//ret=SipNetworkManage(net_name);
	pthread_mutex_unlock( &SipNetwork_Manager_lock);
	
	return ret;
}
void SipNetwork_Manager_timer_callback(void)
{
	pthread_mutex_lock( &SipNetwork_Manager_lock);
	SipNetwork_Check();
	pthread_mutex_unlock( &SipNetwork_Manager_lock);
	//printf("sip_n:%s state=%d,.time=%d\n",Get_CurSipRoute(),Get_SipNetwork_state(),Get_SipNetwork_timer());
	OS_RetriggerTimer(&SipNetwork_Manager_timer);
}
void SipNetwork_Manager_Init(void)
{
	OS_CreateTimer( &SipNetwork_Manager_timer, SipNetwork_Manager_timer_callback, 1000/25);
	OS_RetriggerTimer(&SipNetwork_Manager_timer);
}

void CB_SET_SipRoute(char  *set_route)
{
	//LV_UNUSED(s);
//vtk_lvgl_lock();
   // if(lv_msg_get_id(m) == MSG_SETTING_SIP_ROUTE)
	{
		//char  *set_route = lv_msg_get_payload(m);
		if(set_route==NULL)
			return;
		pthread_mutex_lock( &SipNetwork_Manager_lock);
		
		cJSON *sip_net=cJSON_CreateString("NONE");
		API_PublicInfo_Write(PB_SIP_CONNECT_NETWORK,sip_net);
		cJSON_Delete(sip_net);
		
		strcpy(SaveSipRoute,CurSipRoute);
		strcpy(CurSipRoute,set_route);
		SipNetwork_state=SipNetwork_Link;
		SipNetwork_timer=0;
		Be_Set_SipRoute=1;
		SetSipNetwork(set_route);
		Api_Sip_ReReg();
		
		pthread_mutex_unlock( &SipNetwork_Manager_lock);	
    }
//	vtk_lvgl_unlock();
}
int Api_NetRoute_EnterForce(char *net_name)
{
	int route_mask=0;
	int ret=-1;
	pthread_mutex_lock( &SipNetwork_Manager_lock);
	if(strcmp(net_name,"WLAN")==0)
		route_mask= SipNetwork_Connected_WLAN;
	else if(strcmp(net_name,"4G")==0)
		route_mask= SipNetwork_Connected_4G;
	else if(strcmp(net_name,"LAN")==0)
		route_mask= SipNetwork_Connected_LAN;
	if(SipNetwork_Connented&route_mask)
	{
		if(Force_Set_SipRoute==0)
		{
			Force_Set_SipRoute=1;
			strcpy(SaveSipRoute,CurSipRoute);
		}
		SipNetwork_timer=0;
		if(strcmp(CurSipRoute,net_name)!=0)
		{
			strcpy(CurSipRoute,net_name);
			API_PublicInfo_Write_String(PB_SIP_CONNECT_NETWORK,"NONE");
			SipNetwork_state=SipNetwork_Link;
			SetSipNetwork(net_name);
			usleep(200*1000);
			Api_Sip_ReReg();
		}
		ret=0;
	}
	pthread_mutex_unlock( &SipNetwork_Manager_lock);
	return ret;
}
void Api_NetRoute_ExitForce(void)
{
	pthread_mutex_lock( &SipNetwork_Manager_lock);
	if(Force_Set_SipRoute)
	{
		Force_Set_SipRoute=0;
		SipNetwork_timer=0;
		
		if(strcmp(CurSipRoute,SaveSipRoute)!=0)
		{
			strcpy(CurSipRoute,SaveSipRoute);
			API_PublicInfo_Write_String(PB_SIP_CONNECT_NETWORK,"NONE");
			SipNetwork_state=SipNetwork_Link;
			SetSipNetwork(SaveSipRoute);
		}
		Api_Sip_ReReg();
	}
	pthread_mutex_unlock( &SipNetwork_Manager_lock);
}