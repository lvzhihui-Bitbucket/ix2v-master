 

#ifndef _obj_CardSetup_H
#define _obj_CardSetup_H


// Define Object Property-------------------------------------------------------
typedef enum       
{
    Card_Normal,
    Card_Add,
    Card_Del,
    Card_Manage,
}CARD_SETUP_STATE;

typedef struct
{
	CARD_SETUP_STATE state;	
    int timeExitCnt;
    char roomnum[5];
    char manageCard[21];
}CARD_MANAGE_RUN;

#endif


