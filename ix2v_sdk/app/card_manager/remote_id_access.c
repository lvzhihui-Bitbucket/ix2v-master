
#include "cJSON.h"
#include "obj_CardSetup.h"
#include "obj_CardTable.h"
#include "elog.h"
#include "task_Event.h"
#include "obj_PublicInformation.h"
#include "task_Beeper.h"
#include "obj_MyCtrlLed.h"
#include "obj_IoInterface.h"
#include "obj_GP_OutProxy.h"

void SwipeUnlock(int lock_id);
int thirdpart_remote_public(char* cardnum);

#define REMOTE_RCV_BUFFER_MAX		64

typedef struct {
	unsigned char type;			// 类型 	固定 0x17
	unsigned char func;			// 功能号 	固定 0x40
	unsigned char rev1;			// 保留1 	固定 0x00
	unsigned char rev2;			// 保留2 	固定 0x00
	unsigned char dev_sn1;		// 设备序列号1-3B 	低
	unsigned char dev_sn2;		// 设备序列号2-B6 	
	unsigned char dev_sn3;		// 设备序列号3-4A 	
	unsigned char dev_sn4;		// 设备序列号4-0D  	高		0x0D4AB63B 十进制为223000123
	unsigned char gate;			// 门号，1~4, 缺省为1
	unsigned char gates[3];		// (一对多设备)梯控使用—另说明
	unsigned char rev3[8];		//  保留3 	固定 0x00
	unsigned char id_sn1;		// 卡号B1 (必须大于0, 且不为全FF)
	unsigned char id_sn2;		// 卡号B2 (必须大于0, 且不为全FF)
	unsigned char id_sn3;		// 卡号B3 (必须大于0, 且不为全FF)
	unsigned char id_sn4;		// 卡号B4 (必须大于0, 且不为全FF)
	unsigned char rev4[4];		//  保留4 	固定 0x00
	unsigned char opt;			// V6.58开始 [模拟刷卡的操作]=0x00 表示进门的请求 =0x01 表示出门的请求
	unsigned char rev5[3];		//  保留5 	固定 0x00
	unsigned char limit;		// V6.62开始 (2015-12-08 15:32:28)= 0x5A 不受设备内的权限约束
	unsigned char rev6[31];		//  保留6 	固定 0x00
} REMOTE_CONTROL_REQ_T;

typedef struct {
	unsigned char type;			// 类型 	固定 0x17
	unsigned char func;			// 功能号 	固定 0x40
	unsigned char rev1;			// 保留1 	固定 0x00
	unsigned char rev2;			// 保留2 	固定 0x00
	unsigned char dev_sn1;		// 设备序列号1-3B 	低
	unsigned char dev_sn2;		// 设备序列号2-B6 	
	unsigned char dev_sn3;		// 设备序列号3-4A 	
	unsigned char dev_sn4;		// 设备序列号4-0D  	高		0x0D4AB63B 十进制为223000123
	unsigned char success;		// 返回：0-失败，1-成功
	unsigned char fails; 		// 失败时, 具体信息返回的值为 Reason原因, 查看 开发指南 刷卡记录 信息
	unsigned char rev3[54];		//  保留3 	固定 0x00
} REMOTE_CONTROL_RSP_T;

static REMOTE_CONTROL_REQ_T	remote_req;
static REMOTE_CONTROL_RSP_T	remote_rsp;

#define REMOTE_CONTROLLER_PORT			60000

static int  REMOTE_CONTROL_DEV_GATE		= 1;
static char REMOTE_CONTROL_DEV_ID[4] 	= { 0x3b, 0xB6, 0x4A, 0x0D };
static char REMOTE_CONTROL_DEV_IP[18] 	= "192.168.243.111";
static char REMOTE_CONTROL_DEV_NET[10] 	= "eth0";

int is_remote_device_enable(void)
{
	return API_Para_Read_Int(RemoteDeviceEN);
}

void init_remote_device(void)
{
	char remote_device_sn[20];
	memset(remote_device_sn,0,sizeof(remote_device_sn));
	API_Para_Read_String(RemoteDeviceSN,remote_device_sn);
	// get device id
	unsigned long hex_val = strtoul(remote_device_sn,NULL,16);
	REMOTE_CONTROL_DEV_ID[0] = (hex_val>>0)&0xff;
	REMOTE_CONTROL_DEV_ID[1] = (hex_val>>8)&0xff;
	REMOTE_CONTROL_DEV_ID[2] = (hex_val>>16)&0xff;
	REMOTE_CONTROL_DEV_ID[3] = (hex_val>>24)&0xff;
	// get device ip
	API_Para_Read_String(RemoteDeviceIP,REMOTE_CONTROL_DEV_IP);	
	// get device gate
	REMOTE_CONTROL_DEV_GATE = API_Para_Read_Int(RemoteDeviceGate);
	//
	printf("init remote device ip[%s], device sn[%s], device gate[%d]\n",REMOTE_CONTROL_DEV_IP,remote_device_sn,REMOTE_CONTROL_DEV_GATE);
}

void init_remote_send_req(const char* devnum, int gate, const char* cardnum)
{
	memset(&remote_req, 0, sizeof(remote_req));	
	remote_req.type		= 0x17;
	remote_req.func		= 0x40;
	remote_req.dev_sn1	= devnum[0];
	remote_req.dev_sn2	= devnum[1];
	remote_req.dev_sn3	= devnum[2];
	remote_req.dev_sn4	= devnum[3];
	remote_req.id_sn1	= cardnum[0];
	remote_req.id_sn2	= cardnum[1];
	remote_req.id_sn3	= cardnum[2];
	remote_req.id_sn4	= cardnum[3];
	remote_req.gate		= gate;
	remote_req.opt		= 1;
	remote_req.limit	= 0x00;
	//remote_req.limit	= 0x5A;
}

int thirdpart_remote_id_access(char* cardnum)
{
	// initial remote device
	init_remote_device();

	return thirdpart_remote_public(cardnum);
}

int thirdpart_remote_public(char* cardnum)
{
	char REMOTE_CONTROL_CARD_ID[4];

	int try_cnt,err_result;
	int ret, sock_fd;

	// get card id
	unsigned long dec_val = strtoul(cardnum,NULL,10);
	REMOTE_CONTROL_CARD_ID[0] = (dec_val>>0)&0xff;
	REMOTE_CONTROL_CARD_ID[1] = (dec_val>>8)&0xff;
	REMOTE_CONTROL_CARD_ID[2] = (dec_val>>16)&0xff;
	REMOTE_CONTROL_CARD_ID[3] = (dec_val>>24)&0xff;

	printf("remote apply with card id[%d]\n",dec_val);

	// initial req pack
	init_remote_send_req(REMOTE_CONTROL_DEV_ID, REMOTE_CONTROL_DEV_GATE, REMOTE_CONTROL_CARD_ID);

	// initial socket
	sock_fd = create_trs_udp_socket(REMOTE_CONTROL_DEV_NET);

	// send to remote controller
	struct sockaddr_in id_ctrl_addr;
	id_ctrl_addr.sin_family 		= AF_INET;
	id_ctrl_addr.sin_port			= htons(REMOTE_CONTROLLER_PORT);
	inet_pton(AF_INET, REMOTE_CONTROL_DEV_IP, &id_ctrl_addr.sin_addr);

	// recv form remote controller
	int recvcnt;
	struct sockaddr_in remote_rcv_addr;	
	struct timeval timeout;
    timeout.tv_sec 	= 0; 			// s
    timeout.tv_usec = 500 * 1000; 	// ms
    setsockopt(sock_fd, SOL_SOCKET, SO_RCVTIMEO, (const char *)&timeout, sizeof(timeout));
	int rcv_addr_len = sizeof(remote_rcv_addr);

	for( try_cnt = 0; try_cnt < 2; try_cnt++ )
	{
		ret = sendto(sock_fd, &remote_req, sizeof(remote_req), 0, (struct sockaddr*)&id_ctrl_addr,sizeof(struct sockaddr_in));	
		if( ret == -1 )
		{
			printf("can not send data from socket! sock_fd = %d, ip = %s, port=%d, errno:%d,means:%s\n", sock_fd, my_inet_ntoa2(id_ctrl_addr.sin_addr.s_addr), id_ctrl_addr.sin_port, errno,strerror(errno));
			err_result = -1;
			usleep(100*1000);
			continue;
		}

		recvcnt = recvfrom(sock_fd, &remote_rsp, REMOTE_RCV_BUFFER_MAX, MSG_WAITALL, (struct sockaddr*)&remote_rcv_addr, &rcv_addr_len);
		if( recvcnt <= 0 )
		{
			printf("receive err from remote controller %d\n",try_cnt);
			err_result = -2;
			continue;
		}
		// recieve ok
		else if( recvcnt == REMOTE_RCV_BUFFER_MAX )
		{
			printf("CHECK B0:[%02x-%02x]\n",remote_req.type,	remote_rsp.type);
			printf("CHECK B1:[%02x-%02x]\n",remote_req.func,	remote_rsp.func);
			printf("CHECK B2:[%02x-%02x]\n",remote_req.rev1,	remote_rsp.rev1);
			printf("CHECK B3:[%02x-%02x]\n",remote_req.rev2,	remote_rsp.rev2);
			printf("CHECK B4:[%02x-%02x]\n",remote_req.dev_sn1,	remote_rsp.dev_sn1);
			printf("CHECK B5:[%02x-%02x]\n",remote_req.dev_sn2,	remote_rsp.dev_sn2);
			printf("CHECK B6:[%02x-%02x]\n",remote_req.dev_sn3,	remote_rsp.dev_sn3);
			printf("CHECK B7:[%02x-%02x]\n",remote_req.dev_sn4,	remote_rsp.dev_sn4);
			printf("CHECK B8:[%02x-%02x]\n",remote_req.gate,	remote_rsp.success);
			if(1) // memcmp(&remote_rsp, &remote_req, 8) == 0 )
			{
				if( remote_rsp.success == 1 )
				{
					printf("receive controller allow unlock\n");
					err_result = 0;
					break;
				}
				else
				{
					printf("receive controller NOT allow unlock\n");
					err_result = 1;
					break;
				}
			}
			else
			{
				printf("receive pack type error\n");
				err_result = 2;
				break;
			}
		}
		else
		{
			printf("receive pack length error\n");
			err_result = 3;
			break;
		}
	}
	if( try_cnt == 2 ) err_result = -3;

	// 开锁ok提示
	if( err_result == 0 )
	{
		SetLockState("REMOTE_UNLOCK_OK", 1);
	}
	// 开锁禁止提示
	else if( err_result == 1 )
	{
		SetLockState("REMOTE_UNLOCK_DENY", 1);
	}
	// 开锁错误提示
	else
	{
		SetLockState("REMOTE_UNLOCK_ER", 1);
	}
	return err_result;
}

/*
eg: 
request:    remote_unlock 192.168.243.244 123456789 1 123456
response:   remot unlock xxx
*/ 
void ShellCmd_remote_unlock(cJSON *cmd)
{
	int result;
    char* ptr;
    cJSON* json_dat;
    cJSON* json_rsp;
	char* devip     = GetShellCmdInputString(cmd, 1);
	char* devid     = GetShellCmdInputString(cmd, 2);
	char* gate      = GetShellCmdInputString(cmd, 3);
	char* cardid    = GetShellCmdInputString(cmd, 4);

    if( devip == NULL || devid == NULL )
    {
		ShellCmdPrintf("remote_unlock paras error!\n");
        return -8;
    }
    printf("%s %s %s %s\n",devip,devid,gate,cardid);

	if( !is_remote_device_enable() )
	{
		ShellCmdPrintf("remote_unlock io HAVE NO REMOTE Device!\n");
        return -7;
	}

	// get device ip
	strcpy(REMOTE_CONTROL_DEV_IP,devip);
	// get device id
	unsigned long hex_val = strtoul(devid,NULL,16);
	REMOTE_CONTROL_DEV_ID[0] = (hex_val>>0)&0xff;
	REMOTE_CONTROL_DEV_ID[1] = (hex_val>>8)&0xff;
	REMOTE_CONTROL_DEV_ID[2] = (hex_val>>16)&0xff;
	REMOTE_CONTROL_DEV_ID[3] = (hex_val>>24)&0xff;
	// get get id
	REMOTE_CONTROL_DEV_GATE = atoi(gate);

	// unlock process
	result = thirdpart_remote_public(cardid);
	if( result == 0 )
	{
		ShellCmdPrintf("remote unlock ok\n");
	}
	else if( result == 1 )
	{
		ShellCmdPrintf("remote unlock NOT allow\n");
	}
	else if( result == 2 )
	{
		ShellCmdPrintf("remote unlock rcv type err\n");
	}
	else if( result == 3 )
	{
		ShellCmdPrintf("remote unlock rcv length err\n");
	}
	else if( result == -1 )
	{
		ShellCmdPrintf("remote unlock snd err\n");
	}
	else if( result == -2 )
	{
		ShellCmdPrintf("remote unlock rcv failed\n");
	}
	else if( result == -3 )
	{
		ShellCmdPrintf("remote unlock snd timeout\n");
	}
	return 1;
}

