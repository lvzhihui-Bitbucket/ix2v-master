#include <unistd.h>
#include <stdio.h>
#include "cJSON.h"
#include "obj_CardTable.h"
#include "obj_PublicInformation.h"
#include "obj_TableSurver.h"

//static cJSON *CardDataTb=NULL;


cJSON *card_tb_create_item(char *cardnum,char *property,char *roomnum, char *date,char *name)
{
	cJSON *item=NULL;
	item=cJSON_CreateObject();
	//char room[5];
	//int len;
	if(item!=NULL)
	{
		if(cardnum!=NULL)
		{
			#if 0
			len = strlen(cardnum);
			if( len < 10 )
			{
				memset(carddata,0,11);
				memset(carddata,'0',10-len);
				strcat(carddata,cardnum);
			}
			else
			{
				sprintf(carddata,"%s",cardnum);
			}
			#endif
			cJSON_AddStringToObject(item,DAT_CARD_NUM,cardnum);
		}
		if(property!=NULL)
			cJSON_AddStringToObject(item,DAT_PROPERTY,property);
		if(roomnum!=NULL)
		{
			#if 0
			len = strlen(roomnum);
			if( len < 4 )
			{
				memset(room,0,5);
				memset(room,'0',4-len);
				strcat(room,roomnum);
			}
			else
			{
				sprintf(room,"%s",roomnum);
			}
			cJSON_AddStringToObject(item,DAT_ROOM_NUM,room);
			#endif
			cJSON_AddNumberToObject(item,DAT_ROOM_NUM,atoi(roomnum));
		}
		if(date!=NULL)
			cJSON_AddStringToObject(item,DAT_DATE,date);
		if(name!=NULL)
			cJSON_AddStringToObject(item,DAT_NAME,name);
	}
	
	return item;
}
int card_tb_load(void)
{
	#if 0
	CardDataTb=API_TB_LoadFromFile(CardDataTbPath,NULL,NULL,NULL,NULL,NULL);
	if(CardDataTb==NULL)
	{
		cJSON *tpl=card_tb_create_item("","","","","");
		cJSON *data=cJSON_CreateArray();
		CardDataTb=API_TB_CreateNew(data,NULL,NULL,tpl,NULL);
		
		cJSON_Delete(tpl);
		cJSON_Delete(data);
		if(CardDataTb==NULL)
			return -1;
	}
	#endif
	char carddata[50];
	get_master_card("add", carddata);
	API_PublicInfo_Write_String(PB_MasterAdd, carddata);
	get_master_card("del", carddata);
	API_PublicInfo_Write_String(PB_MasterDel, carddata);
	API_PublicInfo_Write_Int(PB_UserCards, get_card_num(NULL));
	return 0;
}
void card_tb_save(void)
{
	#if 0
	if(CardDataTb!=NULL)
	{
		SetJsonToFile(CardDataTbPath,CardDataTb);		
	}
	#endif
}
void card_tb_backup(void)
{
	char cmd_line[200];
	if(access(CardDataTbPath, F_OK) == 0)
	{
		snprintf(cmd_line, 200,"cp %s %s", CardDataTbPath, CardDataBakPath);
		system(cmd_line);
		sync();
	}
}
int card_tb_restore(void)
{
	char cmd_line[200];
	if(access(CardDataBakPath, F_OK) == 0)
	{
		#if 0
		snprintf(cmd_line, 200,"cp %s %s", CardDataTbPath, CardDataBakPath2);
		system(cmd_line);
		sync();
		snprintf(cmd_line, 200,"cp %s %s", CardDataBakPath, CardDataTbPath);
		system(cmd_line);
		sync();
		snprintf(cmd_line, 200,"cp %s %s", CardDataBakPath2, CardDataBakPath);
		system(cmd_line);
		sync();
		remove(CardDataBakPath2);
		#endif
		snprintf(cmd_line, 200,"cp %s %s", CardDataBakPath, CardDataTbPath);
		system(cmd_line);
		sync();
		card_tb_load();
		return 1;
	}
	return 0;
}

cJSON* get_card_item_by_field(char *cardnum,char *property,char *roomnum, char *date,char *name)
{
	cJSON *where;
	cJSON *item_array;
	cJSON *item;

	where=card_tb_create_item(cardnum,property,roomnum,date,name);
	item_array=cJSON_CreateArray();
	if(API_TB_SelectBySortByName(TB_NAME_CardTable,item_array,where,0)==0)
		return NULL;
	item=cJSON_Duplicate(cJSON_GetArrayItem(item_array,0),1);
	cJSON_Delete(where);
	cJSON_Delete(item_array);
	return item;
}
int update_card_item_by_property(char *property,cJSON *item)
{
	cJSON *where;
	int ret = 0;
	where=card_tb_create_item(NULL,property,NULL,NULL,NULL);
	if(API_TB_UpdateByName(TB_NAME_CardTable, where, item))
		ret = 1;
	cJSON_Delete(where);
	
	return ret;
}

int add_card_to_tb(char *cardnum,char *property,char *roomnum, char *date,char *name)
{
	cJSON *item;
	int ret;
	time_t t;
	struct tm *tblock; 	
	t = time(NULL); 
	tblock=localtime(&t);
	char buff[50];
	sprintf( buff,"%02d%02d%02d",(tblock->tm_year-100)>0?(tblock->tm_year-100):0,tblock->tm_mon+1,tblock->tm_mday);

	item = get_card_item_by_field(cardnum, NULL, NULL, NULL, NULL);
	if(item == NULL)
	{
		item=card_tb_create_item(cardnum,property,roomnum,buff,name);
		if(API_TB_CountByName(TB_NAME_CardTable,NULL)<1000)
			ret=API_TB_AddByName(TB_NAME_CardTable,item);
		else
			ret = 0;
		cJSON_Delete(item);		
		//card_tb_save();
	}
	else
	{
		ret = 0;
		cJSON_Delete(item);	
	}
	
	return ret;
}

int del_card_by_field(char *cardnum,char *property,char *roomnum, char *date,char *name)
{
	cJSON *where;
	
	int ret = 0;
	where=card_tb_create_item(cardnum,property,roomnum,date,name);
	ret = API_TB_DeleteByName(TB_NAME_CardTable,where);

	cJSON_Delete(where);

	//card_tb_save();
	return ret;
}

int get_card_property(char *cardnum)
{
	cJSON *item = get_card_item_by_field(cardnum, NULL, NULL, NULL, NULL);
	int ret = 0;
	char property[10];

	if(item != NULL)
	{
		GetJsonDataPro(item,DAT_PROPERTY,property);
		if(strcmp(property,"add") == 0)
		{
			ret = 1;
		}
		else if(strcmp(property,"del") == 0)
		{
			ret = 2;
		}
		cJSON_Delete(item);
	}
	return ret;
}

int Check_CardContact_Exist(int id, char* cardnum)
{	
	char buff[11];
	sprintf(buff,"%s0000%02d",GetSysVerInfo_bd(), id);
	if(!strcmp(buff, GetSysVerInfo_BdRmMs()))
	{
		return 0;
	}
	cJSON *list=GetIXSByIX_ADDR(buff);
	//cJSON *list=GetIxsIpByIX_ADDR(buff);
	int ret=0;
	if(cJSON_GetArraySize(list)>0)
	{
		char* ipString = GetEventItemString(cJSON_GetArrayItem(list,0), "IP_ADDR");
		cJSON *wh=cJSON_CreateObject();
		cJSON* View=cJSON_CreateArray();
		cJSON_AddStringToObject(wh,"CARD_NUM",cardnum);

		ret = API_RemoteTableSelect(inet_addr(ipString), TB_NAME_CardTable, View, wh, 0);
		printf("Check_CardContact_Exist ixaddr=%s ret=%d %s\n",ipString, ret,cardnum);
		if(ret>0)
		{
			printf_json(View, __func__);
			char property[10]={0};
			GetJsonDataPro(cJSON_GetArrayItem(View,0),DAT_PROPERTY,property);
			if(strcmp(property,"add") == 0)
			{
				ret = 0;
			}
			else if(strcmp(property,"del") == 0)
			{
				ret = 0;
			}
		}
		cJSON_Delete(wh);
		cJSON_Delete(View);
	}
	if(list)
		cJSON_Delete(list);
	printf("Check_CardContact_Exist ixaddr=%s ret=%d \n",buff, ret);
	return ret;
}

int check_card_exist(char *cardnum)
{
	int ret = -1;
	cJSON *item = get_card_item_by_field(cardnum, NULL, NULL, NULL, NULL);
	if(item != NULL)
	{
		ret = 0;
		cJSON_Delete(item);
	}
	
	return ret;
}

int get_card_num(char *roomnum)
{
	int num = 0;
	cJSON *where;
		
	if(roomnum == NULL || *roomnum == 0 || !strcmp(roomnum, "ALL"))
	{
		where=card_tb_create_item(NULL,"-",NULL,NULL,NULL);
		num = API_TB_CountByName(TB_NAME_CardTable, where);
	}
	else
	{
		where=card_tb_create_item(NULL,NULL,roomnum,NULL,NULL);
		num = API_TB_CountByName(TB_NAME_CardTable, where);
	}
	cJSON_Delete(where);
	return num;
}

int add_managecard_to_tb(char *cardnum1,char *cardnum2)
{
	cJSON *item;
	int ret;
	time_t t;
	struct tm *tblock; 	
	t = time(NULL); 
	tblock=localtime(&t);
	char buff[50];
	sprintf( buff,"%02d%02d%02d",(tblock->tm_year-100)>0?(tblock->tm_year-100):0,tblock->tm_mon+1,tblock->tm_mday);
	
	item=card_tb_create_item(cardnum1,"add","1",buff,"-");
	if(update_card_item_by_property("add",item)>0)
		ret=1;
	else
		ret=API_TB_AddByName(TB_NAME_CardTable,item);

	item=card_tb_create_item(cardnum2,"del","1",buff,"-");
	if(update_card_item_by_property("del",item)>0)
		ret=1;
	else
		ret=API_TB_AddByName(TB_NAME_CardTable,item);	
	cJSON_Delete(item);
	
	//card_tb_save();
	return ret;
}

void get_master_card(char *property, char *cardnum)
{
	cJSON *item = get_card_item_by_field(NULL, property, NULL, NULL, NULL);
	if(item != NULL)
	{
		GetJsonDataPro(item,DAT_CARD_NUM,cardnum);
		cJSON_Delete(item);
	}
}