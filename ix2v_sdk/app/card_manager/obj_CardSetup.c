
#include "cJSON.h"
#include "obj_CardSetup.h"
#include "obj_CardTable.h"
#include "elog.h"
#include "task_Event.h"
#include "obj_PublicInformation.h"
#include "task_Beeper.h"
#include "obj_MyCtrlLed.h"
// lzh_20240827_s
#include "remote_id_access.h"
#include "define_file.h"
#include "obj_IoInterface.h"
// lzh_20240827_e

	#ifdef PID_IX821
	#define	DisplayCardSetup(a, b, c)			DR_DisplayCardSetup(a, b, c)		
	#define	DisplayManageCardSet(a, b)		DR_DisplayManageCardSet(a, b)	
	#define	DisplayCardSetupResult(a, b, c)		DR_CardSetupResult(a, b, c)	
	#define	DisplayCardSetupExit()			DR_CardSetupExit()	
	#define	DisplayCardSetupRestore()			DR_CardSetupRestore()	
	#else
	#define	DisplayCardSetup(a, b, c)			
	#define	DisplayManageCardSet(a, b)			
	#define	DisplayCardSetupResult(a, b, c)		
	#define	DisplayCardSetupExit()				
	#define	DisplayCardSetupRestore()
	#endif

#if defined(PID_IX850) || defined(PID_IX622)||defined(PID_IX821)
	#define	CardBeepError()			BEEP_ERROR()
#else
	#define	CardBeepError()			
#endif

#if	defined(PID_IX622)
	#define	CardManageEnableLed()				API_MyLedCtrl(MY_LED_NAMEPLATE, MY_LED_FLASH_FAST, 0)
	#define	CardManageExitLed()					API_MyLedCtrl(MY_LED_NAMEPLATE, MY_LED_ON, 0)
#else
	#define	CardManageEnableLed()
	#define	CardManageExitLed()
#endif

static int eventCardReportIp = 0;

static CARD_MANAGE_RUN cardManage = {.state = Card_Normal, .timeExitCnt = 0};

static int Callback_timeExit(int timing)
{
	if(timing >= 1)
	{
		cardManage.timeExitCnt++;
		if (cardManage.timeExitCnt>=10)	//10
		{
			Send_CardSetupState(0);
			return 2;
		}
		return 1;
	}
	return 0;
}
void EnterCardSetup(int state)
{
	API_PublicInfo_Write_Int(PB_CardSetup_State, state);
	cardManage.state = (CARD_SETUP_STATE) state;
	cardManage.roomnum[0] = 0;
	cardManage.timeExitCnt = 0;
	API_Add_TimingCheck(Callback_timeExit, 1);
#ifndef PID_IX622
	DisplayCardSetup(&cardManage.state, NULL, NULL);
	API_Event_By_Name(EventTouchKeyDisplay);
#else
	DisplayCardSetup(&cardManage.state, NULL, NULL);
	BEEP_SET_ENTRY();
	API_Event_By_Name(EventCardSetupLed);	
#endif	
}
void ExitCardSetup(int unsave)
{
	API_PublicInfo_Write_Int(PB_CardSetup_State, 0);
	cardManage.state = Card_Normal;
#ifndef PID_IX622
	DisplayCardSetupExit();
	API_Event_By_Name(EventTouchKeyDisplay);
#else
	//BEEP_SET_EXIT();
	DisplayCardSetupExit();
	API_Event_By_Name(EventCardSetupLed);	
#endif	
	if(unsave == 0)
	{
		card_tb_backup();
		card_tb_save();
	}	
}

void EditCardData(char *cardnum, char *roomnum)
{
	int ret;
	if(cardManage.state == Card_Add)
	{
		if(cardnum)
		{
			ret = add_card_to_tb(cardnum, "-", cardManage.roomnum[0]==0? "1":cardManage.roomnum, "-", "-");
			sleep(1);
			DisplayCardSetupResult(ret, &cardManage.state, cardManage.roomnum);
			Send_CardSetupInfo(cardManage.state, cardnum, cardManage.roomnum, ret);
		}
	}
	else if(cardManage.state == Card_Del)
	{
		if(cardnum)
		{
			ret = del_card_by_field(cardnum, NULL, NULL, NULL, NULL);
			sleep(1);
			DisplayCardSetupResult(ret, &cardManage.state, cardManage.roomnum);
			Send_CardSetupInfo(cardManage.state, cardnum, cardManage.roomnum, ret);
		}
		else if(roomnum)
		{
			if(cardManage.roomnum[0]==0)
			{
				DisplayCardSetup(&cardManage.state, NULL, "ALL");
				ret = del_card_by_field(NULL, "-", NULL, NULL, NULL);
				sleep(1);
				DisplayCardSetupResult(ret, NULL, NULL);
				Send_CardSetupInfo(cardManage.state, NULL, "ALL", ret);
				sleep(1);
				ExitCardSetup(0);
			}
			else
			{
				ret = del_card_by_field(NULL, NULL, roomnum, NULL, NULL);
				DisplayCardSetupResult(ret, &cardManage.state, NULL);
				Send_CardSetupInfo(cardManage.state, NULL, roomnum, ret);
			}
		}
	}

}

void Send_CardReportEvent(char *cardnum)
{
	cJSON *send_event=cJSON_CreateObject();
	cJSON_AddStringToObject(send_event, "EventName","EventCardIdReport");
	cJSON_AddStringToObject(send_event, "CARD_NUM",cardnum);
	API_XD_EventJson(eventCardReportIp, send_event);
	cJSON_Delete(send_event);
}

int EventCardIdReportCallback(cJSON *cmd)
{
	char* eventName = cJSON_GetStringValue(cJSON_GetObjectItemCaseSensitive(cmd, EVENT_KEY_EventName));
	if(eventName && !strcmp(eventName, EventCardIdReport))
	{
		if(GetEventItemInt(cmd, "STATE"))
		{
			eventCardReportIp = inet_addr(GetEventItemString(cmd, IX2V_IP_ADDR));
			CardManageEnableLed();
		}
		else
		{
			eventCardReportIp = 0;
			CardManageExitLed();
		}
	}
}

int EventCardSwip(cJSON *cmd)
{
	int ret;
	int cardProperty;
	char* cardnum;
    char* eventName = cJSON_GetStringValue(cJSON_GetObjectItemCaseSensitive(cmd, EVENT_KEY_EventName));
	if(eventName && !strcmp(eventName, EventCard))
	{
		cardManage.timeExitCnt = 0;
		cardnum = cJSON_GetStringValue(cJSON_GetObjectItemCaseSensitive(cmd, DAT_CARD_NUM));
		// lzh_20240827_s
		if( is_remote_device_enable() )
		{
			return thirdpart_remote_id_access(cardnum);
		}
		// lzh_20240827_e
		if(eventCardReportIp)
		{
			#if defined(PID_IX622)||defined(PID_IX821)
			BEEP_SHOW_CARD();
			#endif
			Send_CardReportEvent(cardnum);
			return;
		}
		
		if(cardManage.state == Card_Manage)
		{
			
			if(cardManage.manageCard[0] == 0)
			{
				#if defined(PID_IX622)||defined(PID_IX821)
				BEEP_SHOW_CARD();
				#endif
				DisplayManageCardSet(0, cardnum);
				strncpy(cardManage.manageCard, cardnum, 20);
			}
			else
			{
				#if defined(PID_IX622)||defined(PID_IX821)
				BEEP_INFORM();
				#endif
				DisplayManageCardSet(1, cardnum);
				if(strcmp(cardManage.manageCard, cardnum))
				{
					add_managecard_to_tb(cardManage.manageCard, cardnum);
				}
			}
			return;
		}

		cardProperty = get_card_property(cardnum);
		if(cardProperty > 0)
		{
			//ShellCmdPrintf("EventCardSwip1 state=%d room=%s\n",cardManage.state, cardManage.roomnum);
			if(cardManage.state == Card_Normal)
			{
				if(cardProperty==Card_Add&&ifExitKeyLongPress())
				{
					time_t t;
					struct tm tblock;	
					time(&t); 
					localtime_r(&t, &tblock);
					//tblock=localtime(&t);
					char bak_file[200];
					char *backup_path;
					cJSON *tb_list;
					tb_list=cJSON_CreateArray();
					cJSON_AddItemToArray(tb_list,cJSON_CreateString("CardTable"));
					if(Judge_SdCardLink() == 0)
					{
						//backup_path=NAND_Backup_Path;
						//snprintf(bak_file, 200, "%sBAK.TB.tar", backup_path);
						cJSON_Delete(tb_list);
						BEEP_ERROR();
						return;
					}
					else
					{
						backup_path=SDCARD_Backup_Path;
						create_multi_dir(backup_path);
						snprintf(bak_file, 200, "%sBAK.TB.20%02d%02d%02d%02d%02d.tar", backup_path, tblock.tm_year-100,tblock.tm_mon+1,tblock.tm_mday,tblock.tm_hour,tblock.tm_min);
					}
					Api_Backup(tb_list,bak_file);
					cJSON_Delete(tb_list);
				}
				else
				{
					cardManage.state = (CARD_SETUP_STATE) cardProperty;
					Send_CardSetupState(cardManage.state);
				}
			}
			else
			{
				#if defined(PID_IX622)||defined(PID_IX821)
				BEEP_SHOW_CARD();
				#endif
				if(cardManage.state == Card_Del && cardProperty == 1)//删除确认
				{
					EditCardData(NULL, cardManage.roomnum);
				}
				else if(cardManage.state == Card_Add && cardProperty == 2)//备份操作
				{
					DisplayCardSetupRestore();
					ret = card_tb_restore();
					sleep(1);
					DisplayCardSetupResult(ret, NULL, NULL);
					sleep(1);
					ExitCardSetup(1);
				}
				else
				{
					cardManage.state = Card_Normal;
					Send_CardSetupState(cardManage.state);
				}
			}
			
		}
		else	
		{
			if(cardManage.state == Card_Normal)
			{
				int swipesResult;
				if(check_card_exist(cardnum) == 0)
				{
					ShellCmdPrintf("EventCardSwip %s to unlock\n", cardnum);//开锁等处理
					SwipeUnlock(1);
					swipesResult = 1;
				}
				else
				{
					cJSON *cardContact=API_Para_Read_Public(CardContactPara);
					cJSON* element;
					int cnt,i;
					if(cardContact)
					{
						cnt = cJSON_GetArraySize(cardContact);
						for(i = 0; i < cnt; i++)
						{
							element = cJSON_GetArrayItem(cardContact,i);
							if(cJSON_IsNumber(element))
							{
								ret = Check_CardContact_Exist(element->valueint, cardnum);
								if(ret)
								{
									ShellCmdPrintf("EventCardSwip %s to unlock\n", cardnum);//开锁等处理
									SwipeUnlock(1);
									swipesResult = 1;
									break;
								}
							}
						}
					}
					if(ret==0)
					{
						CardBeepError();
						ShellCmdPrintf("EventCardSwip %s not exist\n", cardnum);
						swipesResult = 0;
					}
				}
				
				API_CardswipesInform(swipesResult);
			}
			else
			{
				#if defined(PID_IX622)||defined(PID_IX821)
				BEEP_SHOW_CARD();
				#endif
				//ShellCmdPrintf("EventCardSwip %s to edit\n", cardnum);
				DisplayCardSetup(NULL, cardnum, NULL);
				EditCardData(cardnum, NULL);
			}
		}
	}
}

int EventCardSetupState(cJSON *cmd)
{
    char* eventName = cJSON_GetStringValue(cJSON_GetObjectItemCaseSensitive(cmd, EVENT_KEY_EventName));
	if(eventName && !strcmp(eventName, EventCardState))
	{
		int cardState = GetEventItemInt(cmd, "CardState");
		switch (cardState)
		{
			case 1:
				API_Event_NameAndMsg(Event_XD_RSP_Card_Management, "Add user card");
				EnterCardSetup(cardState);
				break;

			case 2:
				API_Event_NameAndMsg(Event_XD_RSP_Card_Management, "Del user card");
				EnterCardSetup(cardState);
				break;

			case 3:
				API_Event_NameAndMsg(Event_XD_RSP_Card_Management, "Add manage card");
				API_PublicInfo_Write_Int(PB_CardSetup_State, 3);
				API_Event_By_Name(EventManageCard);
				break;
				
			default:
				API_Event_NameAndMsg(Event_XD_RSP_Card_Management, "Exit card management");
				ExitCardSetup(0);
				break;
		}
	}

	return 1;
}

int EventCardSetupRoom(cJSON *cmd)
{
	char* room;
    char* eventName = cJSON_GetStringValue(cJSON_GetObjectItemCaseSensitive(cmd, EVENT_KEY_EventName));
	if(eventName && !strcmp(eventName, EventCardRoom))
	{
		room = cJSON_GetStringValue(cJSON_GetObjectItemCaseSensitive(cmd, DAT_ROOM_NUM));
		if(!strcmp(room, "*"))
		{
			Send_CardSetupState(0);
		}
		else
		{
			cardManage.timeExitCnt = 0;
			strncpy(cardManage.roomnum, room, 4);
			ShellCmdPrintf("EventCardSetupRoom = %s\n", cardManage.roomnum);
			DisplayCardSetup(&cardManage.state, NULL, cardManage.roomnum);
			
		}
	}
}

int EventCardSetupInfo(cJSON *cmd)
{

}

static int Callback_ManageCardExit(int timing)
{
	if(timing >= 1)
	{
		cardManage.timeExitCnt++;
		if (cardManage.timeExitCnt>=8)	//5
		{
			CardManageExitLed();
			ExitCardSetup(0);
			return 2;
		}
		return 1;
	}
	return 0;
}
int EventManageCardSet(cJSON *cmd)
{
	cardManage.state = Card_Manage;
	cardManage.manageCard[0] = 0;
	cardManage.timeExitCnt = 0;
	API_Add_TimingCheck(Callback_ManageCardExit, 1);
	CardManageEnableLed();
	API_Event_By_Name(EventTouchKeyDisplay);
	#ifdef PID_IX821
	DR_DisplayManageCardSet(2,NULL);
	#endif
}

void Send_CardSwip(char *cardnum)
{
	char *event_str;
	cJSON *event;
	event=cJSON_CreateObject();
	if(event!=NULL)
	{
		cJSON_AddStringToObject(event, EVENT_KEY_EventName,EventCard);
		cJSON_AddStringToObject(event, DAT_CARD_NUM ,cardnum);
		API_Event_Json(event);
		cJSON_Delete(event);
	}	
}

void Send_CardSetupState(int state)
{
	char *event_str;
	cJSON *event;
	event=cJSON_CreateObject();
	if(event!=NULL)
	{
		cJSON_AddStringToObject(event, EVENT_KEY_EventName,EventCardState);
		cJSON_AddNumberToObject(event, "CardState" ,state);
		API_Event_Json(event);
		cJSON_Delete(event);
	}	
}

void Send_CardSetupRoom(char *room)
{
	char *event_str;
	cJSON *event;
	event=cJSON_CreateObject();
	if(event!=NULL)
	{
		cJSON_AddStringToObject(event, EVENT_KEY_EventName,EventCardRoom);
		cJSON_AddStringToObject(event, DAT_ROOM_NUM ,room);
		API_Event_Json(event);
		cJSON_Delete(event);
	}	
}

void Send_CardSetupInfo(int state, char *cardnum, char *room, int result)
{
	char *event_str;
	cJSON *event;
	event=cJSON_CreateObject();
	if(event!=NULL)
	{
		cJSON_AddStringToObject(event, EVENT_KEY_EventName, EventCardReport);
		cJSON_AddNumberToObject(event, "CardState" ,state);
		if(cardnum)
			cJSON_AddStringToObject(event, DAT_CARD_NUM ,cardnum);
		if(room)
			cJSON_AddStringToObject(event, DAT_ROOM_NUM ,room);
		cJSON_AddNumberToObject(event, "Result" ,result);
		API_Event_Json(event);
		cJSON_Delete(event);
	}	
}

int ShellCmd_Card(cJSON *cmd)
{
	char *para=GetShellCmdInputString(cmd, 1);
	if(para==NULL)
	{
		ShellCmdPrintf("hava no para");
		return 1;
	}
	if(strcmp(para,"swipe")==0)
	{
		char *cardnum=GetShellCmdInputString(cmd, 2);
		if(cardnum==NULL)
		{
			ShellCmdPrintf("hava no cardnum");
			return 1;
		}
		Send_CardSwip(cardnum);
	}
	else if(strcmp(para,"state")==0)
	{
		char *state=GetShellCmdInputString(cmd, 2);
		if(state==NULL)
		{
			ShellCmdPrintf("hava no state");
			return 1;
		}
		if(atoi(state)>2)
		{
			ShellCmdPrintf("state must be <=2\n");
			return 1;
		}	
		Send_CardSetupState(atoi(state));
	}
	else if(strcmp(para,"room")==0)
	{
		char *room=GetShellCmdInputString(cmd, 2);
		if(room==NULL)
		{
			ShellCmdPrintf("hava no room");
			return 1;
		}
		Send_CardSetupRoom(room);
	}
	else if(strcmp(para,"manager")==0)
	{
		API_PublicInfo_Write_Int(PB_CardSetup_State, 3);
		API_Event_By_Name(EventManageCard);
	}
	else if(strcmp(para,"restore")==0)
	{
		card_tb_restore();
	}
	return 0;	
}
