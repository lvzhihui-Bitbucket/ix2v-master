#ifndef _obj_CardTable_h
#define _obj_CardTable_h

#define CardDataTbPath	    "/mnt/nand1-2/UserRes/cardnum.json"
#define CardDataBakPath	    "/mnt/nand1-2/UserRes/cardnum_bak.json"
#define CardDataBakPath2	"/mnt/nand1-2/UserRes/cardnum_bak2.json"
#define DAT_CARD_NUM				"CARD_NUM"
#define DAT_PROPERTY				"PROPERTY"
#define DAT_ROOM_NUM				"ROOM"
#define DAT_DATE				"DATE"
#define DAT_NAME				"NAME"

int card_tb_load(void);
void card_tb_save(void);
int add_card_to_tb(char *cardnum,char *property,char *roomnum, char *date,char *name);
int del_card_by_field(char *cardnum,char *property,char *roomnum, char *date,char *name);
int get_card_property(char *cardnum);
int check_card_exist(char *cardnum);
int get_card_num(char *roomnum);
int add_managecard_to_tb(char *cardnum1,char *cardnum2);
void get_master_card(char *property, char *cardnum);

#endif
